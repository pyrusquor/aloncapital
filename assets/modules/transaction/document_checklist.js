"use strict";
var KTDatatablesLandInventoryDocumentChecklist = function () {

	var _initLandInventoryDocumentChecklist = function () {

		var _table = $('#_land_inventory_document_table');

		_table.DataTable({
			order: [[ 1, 'desc']],
			pagingType: 'full_numbers',
			lengthMenu: [5, 10, 25, 50, 100],
			pageLength : 5,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: false,
			deferRender: true,
			dom:	
						// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
						"<'row'<'col-sm-12 col-md-6'l>>" +
						"<'row'<'col-sm-12'tr>>" +
						"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			language: {
				'lengthMenu': 'Show _MENU_',
        'infoFiltered': '(filtered from _MAX_ total records)'
			},
			columnDefs: [
				{
					targets: [0, 1, 4, 5, 6, 7],
					className: 'dt-center',
				},
				{
					targets: [0, 6, 7],
					orderable: false,
				}
			],
			drawCallback: function ( settings ) {

				$('span#_total').text(settings.fnRecordsTotal()+' TOTAL');
			}
		});
	}

	var _selectProp = function () {

		var _table    = $('#_land_inventory_document_table').DataTable();

		$('input#select-all').on( 'click', function () {

			if ( this.checked ) {

				$('input._select').each( function () {

					this.checked = true;
				});
			} else {

				$('input._select').each( function () {

					this.checked = false;
				});
			}
		});

		$('#_land_inventory_document_table tbody').on( 'change', 'input._select', function () {

			if ( this.checked == false ) {

				$('input#select-all').prop('checked', false);
			}
		});
	}

	var _add_ons = function () {

		var dTable = $('#_land_inventory_document_table').DataTable();

		$('#_search').on( 'keyup', function () {

			dTable.search($(this).val()).draw();
		});

		$('#kt_datepicker').datepicker({
    	orientation: "bottom left",
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

		$('button#_advance_search_btn').on( 'click', function () {

			$('div#_batch_upload').removeClass('show');
		});

		$('button#_batch_upload_btn').on( 'click', function () {

			$('div#_advance_search').removeClass('show');
		});
	}

	var _filterDocumentChecklist = function () {

		var dTable = $('#_land_inventory_document_table').DataTable();

		function _colFilter ( n ) {

			dTable.column(n).search($('#_column_'+n).val()).draw();
		}

		$('._filter').on( 'keyup change clear', function () {

			_colFilter($(this).data('column'));
		});
	}

	return {

		init: function () {

			_initLandInventoryDocumentChecklist();
			_add_ons();
			_filterDocumentChecklist();
			_selectProp();
		}
	};
}();

jQuery(document).ready(function() {

    KTDatatablesLandInventoryDocumentChecklist.init();
});