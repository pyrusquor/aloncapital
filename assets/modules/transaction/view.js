"use strict";

// Class definition
var TransactionView = function () {
	// Base elements
	var wizardEl;
	var formEl;
	var validator;
	var wizard;
	
	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		wizard = new KTWizard('transaction_view', {
			startStep: 1,
			// clickableSteps: false
		});

		// Validation before going to next page
		// wizard.on('beforeNext', function(wizardObj) {
		// 	if (validator.form() !== true) {
		// 		wizardObj.stop();  // don't go to the next step
		// 	}
		// });

		// wizard.on('beforePrev', function(wizardObj) {
		// 	if (validator.form() !== true) {
		// 		wizardObj.stop();  // don't go to the next step
		// 	}
		// });

		// Change event
		wizard.on('change', function(wizard) {
			KTUtil.scrollTop();	
		});
	}

	// var initValidation = function() {
	// 	validator = formEl.validate({
	// 		// Validate only visible fields
	// 		ignore: ":hidden",

	// 		// Validation rules
	// 		rules: {
	// 			//= Step 1
	// 			address1: {
	// 				required: true 
	// 			}
	// 		},
			
	// 		// Display error  
	// 		invalidHandler: function(event, validator) {	 
	// 			KTUtil.scrollTop();

	// 			swal.fire({
	// 				"title": "", 
	// 				"text": "There are some errors in your submission. Please correct them.", 
	// 				"type": "error",
	// 				"confirmButtonClass": "btn btn-secondary"
	// 			});
	// 		},

	// 		// Submit valid form
	// 		submitHandler: function (form) {
				
	// 		}
	// 	});   
	// }

    var initValidation = function () {
        var formEl = $('#form_modal');

        validator = formEl.validate({
            // Validate only visible fields
            ignore: ':hidden',

            // Validation rules
            rules: {
                refund: {
                    required: true,
                },
                reason: {
                    required: true,
                },
            },
            // Display error
            invalidHandler: function (event, validator) {
                swal.fire({
                    title: '',
                    text:
                        'There are some errors in your submission. Please correct them.',
                    type: 'error',
                    confirmButtonClass: 'btn btn-secondary',
                });
            },
            // Submit valid form
            submitHandler: function (form) {},
        });
    };

	var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formEl.ajaxSubmit({
					success: function() {
						KTApp.unprogress(btn);
						//KTApp.unblock(formEl);

						swal.fire({
							"title": "", 
							"text": "The application has been successfully submitted!", 
							"type": "success",
							"confirmButtonClass": "btn btn-secondary"
						});
					}
				});
			}
		});
	}

	var confirmDelete = function () {
        $(document).on("click", ".collection_payment", function () {
            var id = $(this).data("id");
			var action = $(this).data("action");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, " + action + " it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "transaction_payment/" + action + "_payment/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            id: id
                        },
                        success: function (res) {
                            if (res.status) {
								location.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Payment "  + action + " successfully cancelled.",
                        "error"
                    );
                }
            });
        });
    };

	var loadMortgageSched = function () {
		$('#mortgage_schedule_nav').one( "click", function() {
			var id = $(this).attr("data-id");
			var cp = $(this).attr("data-cp");
			var pt = $(this).attr("data-pt");
			var af = $(this).attr("data-af");
			
			KTApp.block('#kt_content', {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Processing...',
				css: {
					padding: 0,
					margin: 0,
					width: '30%',
					top: '40%',
					left: '35%',
					textAlign: 'center',
					color: '#000',
					border: '3px solid #aaa',
					backgroundColor: '#fff',
					cursor: 'wait',
				},
			});
			$.ajax({
				url: base_url + 'transaction/load_mortgage_schedule/',
				type: 'POST',
				dataType: 'TEXT',
				data: {
					id: id,
					cp: cp,
					pt: pt,
					af: af
				},
				success: function (res){
                        $('#mortgage_schedule_view').html(res);
                        KTApp.unblock('#kt_content');

				}
			})
				
			
		  });
	}

	var processTransaction = function () {
        $(document).on('click', '.process_transaction', function () {
            var btn = $(this);
            var id = btn.data('id');
            var type = btn.data('type');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, please proceed.',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url:
                            base_url +
                            'transaction/process_transaction/' +
                            type +
                            '/' +
                            id,
                        // type: 'GET',
                        dataType: 'JSON',
                        data: { id: id },
                        success: function (res) {
                            $('#modal_process .modal-content').html(res.html);
                            $('#modal_process').modal('show');

                            formRepeaterAdhocFee();
                            formRepeaterPromo();
                            getModalTCP();
                            getModalLA();
                            getModalCP();
                            datepicker();
                            modalSubmit();
                            initValidation();
                            if (type == 'financing_scheme') {
                                getFinancingSchemeCategories();
                                setFinancingSchemesByCategory();
                                getDiscount();
                            }

                            if (type == 'discount') {
                                getDiscount();
                            }

                            if (res.info) {
                                var info = $('#info');
                                if (info) {
                                    info.text(JSON.stringify(res.info));
                                }
                            }

                            // if (res.status) {
                            // 	btn.closest('div.kt-portlet').remove();
                            // 	swal.fire(
                            // 		'Deleted!',
                            // 		res.message,
                            // 		'success'
                            // 	);
                            // } else {
                            // 	swal.fire(
                            // 		'Oops!',
                            // 		res.message,
                            // 		'error'
                            // 	)
                            // }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });

        $(document).on('click', '#confirmation', function () {
            var btn = $('[data-action="action-submit"]');
            var type = $('#type').val();
            var username = $('#username').val();
            var password = $('#password').val();
            var info = $('#info').val();

            $.ajax({
                url: base_url + 'transaction_confirmation/confirm',
                type: 'post',
                dataType: 'JSON',
                data: {
                    type: type,
                    username: username,
                    password: password,
                    info: info,
                },
                success: function (res) {
                    if (res.status) {
                        var formEl = $('#form_modal');
                        if (validator.form()) {
                            // See: src\js\framework\base\app.js
                            KTApp.progress(btn);
                            //KTApp.block(formEl);

                            // See: http://malsup.com/jquery/form/#ajaxSubmit
                            formEl.ajaxSubmit({
                                type: 'POST',
                                dataType: 'JSON',
                                success: function (response) {
                                    if (response.status) {
                                        swal.fire({
                                            title: 'Success!',
                                            text: response.message,
                                            type: 'success',
                                        }).then(function () {
                                            if (response.data.info) {
                                                var data = JSON.parse(
                                                    response.data.info
                                                );
                                            }
                                            if (response.type == 'change_property') {
                                                window.location.replace(
                                                    base_url + 'transaction/form/' + data.transaction_id + '/property'
                                                );
                                            } else if(response.type == 'update_transaction') {
                                                window.location.replace(
                                                    base_url + 'transaction/form/' + response.transaction_id
                                                );
                                            } else if(response.type == "recompute_schedule") {
                                                window.location.replace(
                                                    base_url +  'transaction/process_mortgage/' + response.transaction_id + '/save/recompute',
                                                );
                                            } else if(response.type == 'delete_transaction') {
                                                $.ajax({
                                                    url: base_url + 'transaction/delete/' + response.transaction_id,
                                                    type: 'POST',
                                                    dataType: 'JSON',
                                                    data: {
                                                        id: response.transaction_id,
                                                    },
                                                    success: function (res) {
                                                        if (res.status) {
                                                            btn.closest('div.kt-portlet').remove();
                                                            swal.fire('Deleted!', res.message, 'success');
                                                            window.location.replace(
                                                                base_url +
                                                                    'transaction'
                                                            );
                                                        } else {
                                                            swal.fire('Oops!', res.message, 'error');
                                                        }
                                                    },
                                                });
                                            } else {
                                                window.location.replace(
                                                    base_url +
                                                        'transaction/view/' +
                                                        response.transaction_id
                                                );
                                            }
                                        });
                                    } else {
                                        swal.fire(
                                            'Oops!',
                                            response.message,
                                            'error'
                                        );
                                    }
                                },
                            });
                        }
                    } else {
                        swal.fire('Oops!', res.message, 'error');
                    }
                },
            });
        });
    };

    var modalSubmit = function () {
        var formEl = $('#form_modal');
        var btn = $('[data-action="action-submit"]');
        var type = $('#type').val();
        var auth = $('#confirmation');

        btn.on('click', function (e) {
            e.preventDefault();

            var info = $('#form_modal').serialize();

            if (isNaN(auth)) {
                $.ajax({
                    url: base_url + 'transaction_confirmation/confirm_process',
                    type: 'post',
                    dataType: 'JSON',
                    data: info,
                    success: function (res) {
                        $('#modal_process .modal-content').html(res.html);
                        $('#modal_process').modal('show');
                    },
                });
            } else {
                if (validator.form()) {
                    // See: src\js\framework\base\app.js
                    KTApp.progress(btn);
                    //KTApp.block(formEl);

                    // See: http://malsup.com/jquery/form/#ajaxSubmit
                    formEl.ajaxSubmit({
                        type: 'POST',
                        dataType: 'JSON',
                        success: function (response) {
                            if (response.status) {
                                swal.fire({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                }).then(function () {
                                    window.location.replace(
                                        base_url + 'transaction'
                                    );
                                });
                            } else {
                                swal.fire('Oops!', response.message, 'error');
                            }
                        },
                    });
                }
            }
        });
    };

	var formRepeaterAdhocFee = function () {
        $('#form_modal #fees_modal_form_repeater').repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width', '100%');
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    setTimeout(function () {
                        computeAdhocFees();
                        getAdhocSchedule();
                        getAH();
                    }, 1000);

                    $(this).slideUp(deleteElement);
                    $('.select2-container').remove();
                    Select.init();
                    $('.select2-container').css('width', '100%');
                }
            },
        });
    };

    var getAH = function (val = 0) {
        amount = 0;
        $('#form_modal .feesID').each(function (index, element) {
            amount =
                parseFloat(amount) +
                parseFloat($('.feesAmount').eq(index).val());
        });

        adhoc_fee_amount = formatCurrency(amount);

        $('#total_fee').val(adhoc_fee_amount);
    };

	var formRepeaterPromo = function () {
        $('#form_modal #promos_modal_form_repeater').repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width', '100%');
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    setTimeout(function () {
                        getModalTCP();
                        setTimeout(function () {
                            computeModalPromos();
                            setTimeout(function () {
                                getModalCP();
                                getModalLA();
                            }, 1000);
                        }, 1000);
                    }, 1000);

                    $(this).slideUp(deleteElement);
                    $('.select2-container').remove();
                    Select.init();
                    $('.select2-container').css('width', '100%');
                }
            },
        });
    };

    var getModalTCP = function (val = 0) {
        try {
            total_selling_price = parseFloat(
                $('#form_modal #total_selling_price').val()
            );

            promo_amount = 0;
            total_promo_amount = 0;

            $('.promoPrice').each(function (index, element) {
                promo_amount = parseFloat($(element).val());
                total_promo_amount += promo_amount;
            });

            if (isNaN(total_promo_amount)) {
                total_promo_amount = 0;
            }

            total_contract_price = total_selling_price + total_promo_amount;

            $('#form_modal #collectible_price').val(
                formatCurrency(total_contract_price)
            );
        } catch (err) {}
    };

    var computeModalPromos = function () {
        try {
            total_selling_price = parseFloat(
                $('#form_modal #total_selling_price').val()
            );
            total_contract_price = parseFloat(
                $('#form_modal #total_contract_price').val()
            );
            collectible_price = parseFloat(
                $('#form_modal #collectible_price').val()
            );
            loan_amount_deduction = parseFloat(
                $('#form_modal #loan_amount_deduction').val()
            );

            promo_amount = 0;
            promos = [];
            tcp_deductions = [];
            loan_deductions = [];
            loan_deduction_amount = 0;
            loans = [];

            $('.promoID')
                .find('option:selected')
                .each(function (index, element) {
                    // Deduct to values
                    // 1 = Deduct to Loan
                    // 2 = Deduct to TCP (Total Collectible Price)
                    var value = $(element).val();
                    var amount = $(element)
                        .closest('[data-repeater-item]')
                        .find('.promoPrice')
                        .val();

                    if (value == 1) {
                        loan_deductions.push(amount);
                    } else if (value == 2) {
                        tcp_deductions.push(amount);
                    }
                });
        } catch (err) {}
    };

    var getModalCP = function (val = 0) {
        try {
            total_contract_price = parseFloat(
                $('#form_modal #total_contract_price').val()
            );
            // promo_amount = 0;
            // total_promo_amount = 0;
            loan_amount = 0;
            tcp_amount = 0;

            $('.promoPrice').each(function (index, element) {
                var deduct_to = $(element)
                    .closest('[data-repeater-item]')
                    .find('.promoID')
                    .val();
                var value = $(element).val();

                if (deduct_to == 1) {
                    loan_amount += parseFloat(value);
                } else if (deduct_to == 2) {
                    tcp_amount += parseFloat(value);
                }
            });

            collectible_price = total_contract_price - tcp_amount;

            $('#form_modal #loan_amount_deduction').val(
                formatCurrency(loan_amount)
            );
            $('#form_modal #collectible_price').val(
                formatCurrency(collectible_price)
            );
        } catch (err) {}
    };

    var getModalLA = function (val = 0) {
        try {
            amount = 0;
            $('.promoID').each(function (index, element) {
                var deduct_to = $(element).val();
                var value = $(element)
                    .closest('[data-repeater-item')
                    .find('.promoPrice')
                    .val();
                if (deduct_to == 2) {
                    amount += parseFloat(value);
                }
            });
            loan_amount = formatCurrency(amount);

            $('#form_modal #loan_amount_deduction').val(loan_amount);
        } catch (err) {}
    };

	var AccountingEntriesTable = function () {
		var table = $('#accounting_entries_table');

		// begin first table
		table.DataTable({
			order: [[1, 'desc']],
			pagingType: 'full_numbers',
			lengthMenu: [5, 10, 25, 50, 100],
			responsive: true,
			pageLength: 10,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			serverMethod: 'post',
			deferRender: true,
			ajax: {
				url: base_url + 'accounting_entries/showAccountingEntries',
				type: 'POST',
				data: {
					where: {
						payee_type: 'buyers',
						payee_type_id: payee_type_id,
						property_id: property_id
					}
				}
			},
			dom:
				"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6 text-right'<'btn-block'B>>>" +
				// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			buttons: [],
			language: {
				lengthMenu: 'Show _MENU_',
				infoFiltered: '(filtered from _MAX_ total records)',
			},

			columns: [
				{
					data: 'id',
				},
				{
					data: 'company',
				},
				{
					data: 'project',
				},
				{
					data: 'payee_type',
				},
				{
					data: 'payee_name',
				},
				{
					data: 'or_number',
				},
				{
					data: 'invoice_number',
				},
				{
					data: 'journal_type',
				},
				{
					data: 'payment_date',
				},
				{
					data: 'cr_total',
				},
				{
					data: 'remarks',
				},
				{
					data: 'is_approve',
				},
				{
					data: 'Actions',
					responsivePriority: -1,
				},
			],
			columnDefs: [
				{
					targets: -1,
					title: 'Actions',
					searchable: false,
					orderable: false,
					render: function (data, type, row, meta) {
						return (
							`<a href="` +
							base_url +
							`accounting_entries/view/` +
							row.id +
							`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
						)
					},
				},
				{
					targets: 7,
					render: function (data, type, full, meta) {
						var journal_type = {
							1: {
								type: 'Journal Voucher',
							},
							2: {
								type: 'Sales Journal',
							},
							3: {
								type: 'Cash Receipt Journal',
							},
							4: {
								type: 'Purchase Journal',
							},
							5: {
								type: 'Cash Payment Journal',
							},
						};

						if (typeof journal_type[data] === 'undefined') {
							return ``;
						}
						return journal_type[data].type;
					},
				},
				{
					targets: 11,
					render: function (data, type, full, meta) {
						if (data == 0) {
							return 'Pending';
						} else if (data == 1) {
							return 'Approved';
						} else {
							return 'Disapproved';
						}
					},
				},
			],
			drawCallback: function (settings) {
				$('#total').text(settings.fnRecordsTotal() + ' TOTAL');
			},
		});

		var oTable = table.DataTable();
		$('#generalSearch').keyup(function () {
			oTable.search($(this).val()).draw();
		});
	};

    const datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });
    
        $('.datePicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });
    
        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
        $('.monthPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'mm',
            viewMode: 'months',
            minViewMode: 'months',
            autoclose: true,
        });
        $('.dayPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'dd',
            viewMode: 'days',
            minViewMode: 'days',
            autoclose: true,
        });
        $('.kt_datepicker').datepicker({
            orientation: 'bottom left',
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: 'yyyy-mm-dd',
        });
    };
    var syncArisPayments = function() {
		$(document).on('click', ".sync_aris_payments", function() {
			var id = $(this).data('id');

			KTApp.block('#kt_content', {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Syncing Payments from Aris...',
				css: {
					padding: 0,
					margin: 0,
					width: '30%',
					top: '40%',
					left: '35%',
					textAlign: 'center',
					color: '#000',
					border: '3px solid #aaa',
					backgroundColor: '#fff',
					cursor: 'wait',
				},
			});

			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, please proceed',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true,
			}).then(function (result) {
				if(result.value) {
					$.ajax({
						url: base_url + 'transaction_payment/sync_aris_collections/0/0/0/0/' + id + '/',
						dataType: 'JSON',
						success: function (response) {
							KTApp.unblock('#kt_content');
							if(response.status) {
								if(response.status == 1) {
                                    window.location.reload()
									swal.fire({
										title: 'Success!',
										text: response.message,
										type: 'success',
									})
								} else {
									swal.fire({
										title: 'Success!',
										text: response.message,
										type: 'success',
									})
								}
								
							} else {
								swal.fire('Oops!', response.message, 'error');
							}
						},
					})
				} else{
					KTApp.unblock('#kt_content');
				} 
			})
		});
	}

	return {
		// public functions
		init: function() {
			wizardEl = KTUtil.get('transaction_view');
			formEl = $('#kt_form');

			initWizard();
            syncArisPayments();
			confirmDelete(); 
			loadMortgageSched();
			modalSubmit();
			processTransaction();
			formRepeaterAdhocFee();
			formRepeaterPromo();
			getModalCP();
            getModalTCP();
            getModalLA();
            getModalCP();
			// initValidation();
			// initSubmit();
			AccountingEntriesTable();
            datepicker();
		}
	};
}();

$('.create_arnote').on('click', function () {
    let id = $(this).data('id');
    let modal = $('#create_ar_note_modal');
    let transactionIdInput = $('#transaction_id');

    // open modal
    $(modal).modal('show');
    $(transactionIdInput).val(id);
});

$('#createArNoteForm').on('submit', function (e) {
    e.preventDefault();

    let transaction_id = $('#transaction_id').val();
    let remarks = $('#remarks').val();

    $.ajax({
        url: base_url + 'ticketing/form/' + transaction_id,
        type: 'POST',
        dataType: 'JSON',
        data: {
            transaction_id: transaction_id,
            remarks: remarks,
            category: 'ar_notes',
        },
        success: function (response) {
            if (response.status) {
                swal.fire({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                }).then(function () {
                    window.location.replace(base_url + 'transaction/view/'+transaction_id);
                });
            } else {
                swal.fire('Oops!', response.message, 'error');
            }
        },
    });
});
$(document).on('click', '.update_transaction_stage', function () {
    let id = $(this).data('id');
    let modal = $('#update_transaction_stage_modal');
    let transactionIdInput = $('#td_transaction_id');

    // open modal
    $(modal).modal('show');
    $(transactionIdInput).val(id);
});

$(document).on('click', '.update_transaction_loan_stage', function () {
    let id = $(this).data('id');
    let modal = $('#update_transaction_loan_stage_modal');
    let transactionIdInput = $('#td_transaction_id');

    // open modal
    $(modal).modal('show');
    $(transactionIdInput).val(id);
});
jQuery(document).ready(function() {	
	TransactionView.init();
});