// Class definition
var TransactionCreate = (function () {
    var arrows;

    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }
    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.datePicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true,
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
    };

    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
            Select.init();
            additionalInfo();
        });

        wizard.on('beforePrev', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
                additionalInfo();
            }
        });

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
            if (wizard.currentStep == 4) {
                var seller_val = $('#seller_id').val();

                if (seller_val == 0) {
                    KTUtil.scrollTop();
                    wizard.goPrev();
                    swal.fire({
                        title: '',
                        text: 'Please add a seller.',
                        type: 'error',
                        confirmButtonClass: 'btn btn-secondary',
                    });
                }
            }
        });
    };

    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ':hidden',

            // Validation rules
            rules: {
                'property[project_id]': {
                    required: true,
                },
                'property[model_id]': {
                    required: true,
                },
                'property[interior_id]': {
                    required: true,
                },
                'property[lot_area]': {
                    required: true,
                },
                'property[floor_area]': {
                    required: true,
                },
                'property[lot_price_per_sqm]': {
                    required: true,
                },
                'property[total_lot_price]': {
                    required: true,
                },
                'property[model_price]': {
                    required: true,
                },
                'property[property_id]': {
                    required: true,
                },
                'property[total_selling_price]': {
                    required: true,
                    notEqual: "0.00"
                },
                'property[total_contract_price]': {
                    required: true,
                    notEqual: "0.00"
                },
                'property[collectible_price]': {
                    required: true,
                    notEqual: "0.00"
                },
                'property[commissionable_value]': {
                    required: true,
                },
                'property[commissionable_amount]': {
                    required: true,
                },
                'billing[reservation_date]': {
                    required: true,
                },
                'billing[expiration_date]': {
                    required: true,
                },
                'billing[financing_scheme_id]': {
                    required: true,
                },
                'client[client_id]': {
                    required: true,
                },
                'client[client_type]': {
                    required: true,
                },
                'seller[seller_id]': {
                    required: true,
                },
                'seller[seller_position_id]': {
                    required: true,
                },
                'seller[sellers_rate]': {
                    required: true,
                },
                'seller[sellers_rate_amount]': {
                    required: true,
                },
                seller_id: {
                    required: true,
                },
                client_id: {
                    required: true,
                },
                ra_number: {
                    required: true,
                },
            },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
                swal.fire({
                    title: '',
                    text:
                        'There are some errors in your submission. Please correct them.',
                    type: 'error',
                    confirmButtonClass: 'btn btn-secondary',
                });
            },
            // Submit valid form
            submitHandler: function (form) {},
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function (e) {
            e.preventDefault();

            var client_val = $('#client_id').val();

            if (client_val == 0) {
                KTUtil.scrollTop();
                swal.fire({
                    title: '',
                    text: 'Please add a buyer.',
                    type: 'error',
                    confirmButtonClass: 'btn btn-secondary',
                });
                return;
            }

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    type: 'POSt',
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: 'Success!',
                                text: response.message,
                                type: 'success',
                            }).then(function () {
                                window.location.replace(
                                    base_url + 'transaction'
                                );
                            });
                        } else {
                            swal.fire('Oops!', response.message, 'error');
                        }
                    },
                });
            }
        });
    };

    // Public functions
    return {
        init: function () {
            datepicker();
            // formRepeater();

            wizardEl = KTUtil.get('kt_wizard_v3');
            formEl = $('#form_transaction');

            initWizard();
            initValidation();
            initSubmit();
        },
    };
})();

// Initialization
jQuery(document).ready(function () {
    TransactionCreate.init();
});

// jQuery(document).on("change", "select#seller_id", function (e) {
//   e.preventDefault();
//   var seller_id = jQuery(this).val();
//   getSellerPosition(seller_id);
// });

function getSellerPosition(id, index) {
    $.ajax({
        url: base_url + 'seller/get_seller_position/' + id,
        type: 'GET',
        data: { id, id },
        dataType: 'json',
        success: function (data) {
            var seller = data;
            var sellerPositionEl = $('select#seller_position_id');

            $(sellerPositionEl[index]).val(seller.seller_position_id);
        },
        timeout: 10000,
        async: false,
    });
}

const financingSchemeCategories = document.querySelector(
    '#financing_scheme_categories'
);

const financingSchemes = document.querySelector('#financing_scheme_id');
const financingSchemeType = document.querySelector('#scheme_type');
const totalContractPrice = document.querySelector('#total_contract_price');

const getFinancingSchemeCategories = () => {
    $.ajax({
        url: base_url + 'financing_scheme_categories/get_all',
        type: 'GET',
        success: function (data) {
            const categories = JSON.parse(data);
            categories.data.map((category) => {
                let opt = document.createElement('option');

                opt.value = category.id;
                opt.innerHTML = category.name;
                if (financingSchemeCategories) {
                    financingSchemeCategories.appendChild(opt);
                }
            });
        },
    });
};

const setFinancingSchemesByCategory = () => {
    $('#financing_scheme_categories').on('change', function () {
        categoryID = $(this).val();
        $.ajax({
            url: base_url + 'financing_scheme/financing_scheme_by_category',
            type: 'POST',
            data: {
                categoryID,
                categoryID,
            },
            success: function (data) {
                const fSchemes = JSON.parse(data);
                financingSchemes.innerHTML = '';

                try {
                    fSchemes.map((scheme) => {
                        let opt = document.createElement('option');

                        opt.value = scheme.id;
                        opt.innerHTML = scheme.name + ' - ' + scheme.scheme_type;
                        financingSchemes.appendChild(opt);
                    });
                    loadScheme();
                } catch (error) {
                    let opt = document.createElement('option');
                    financingSchemes.innerHTML = '';
                    opt.value = '';
                    opt.innerHTML = 'No record found';
                    financingSchemes.appendChild(opt);
                }
            },
        });
    });
};

const setFinancingSchemeType = () => {
    $('#financing_scheme_id').on('change', function () {
        id = $(this).val();
        $.ajax({
            url: base_url + 'financing_scheme/get_detail',
            type: 'POST',
            data: {
                id,
                id,
            },
            success: function (data) {
                const fScheme = JSON.parse(data);
                const scheme_type_id = fScheme[0].scheme_type;
                const tcp = totalContractPrice.value;
                financingSchemeType.value = scheme_type_id;

                setTimeout(function () {
                    let periodAmounts = document.querySelectorAll(
                        '#period_amount'
                    );

                    if(Object.keys(periodAmounts).length !== 0) {
                        let reservation = periodAmounts[0].value;
                        let downpayment = periodAmounts[1].value;
                        let loan = periodAmounts[2].value;
                        computeBilling(
                            scheme_type_id,
                            tcp,
                            reservation,
                            downpayment,
                            loan
                        );
                    }
                    // switch (scheme_type_id) {
                    //   case "2":
                    //     loan = zeroDownpayment(reservation, tcp);
                    //     periodAmounts[2].value = loan;
                    //     break;
                    //   case "3":
                    //     downpayment = fixedLoanAmount(reservation, loan);
                    //     periodAmounts[1].value = downpayment;
                    //     break;
                    //   case "4":
                    //     loan = fixedDownPayment(reservation, downpayment, tcp);
                    //     periodAmounts[2].value = loan;
                    //     break;
                    // }
                }, 500);
            },
        });
    });
};

function computeBilling(scheme_type_id, tcp, re, dp, loan) {
    let periodAmounts = document.querySelectorAll('#period_amount');

    switch (scheme_type_id) {
        case '2': // zero downpayment
            // loan = tcp - reservation
            periodAmounts[2].value = parseFloat(tcp - re).toFixed(2);
            break;
        case '3': // fixed loan amount
            // downpayment = loan + reservation
            periodAmounts[1].value = parseFloat(loan + re).toFixed(2);
            break;
        case '4': // fixed downpayment
            // loan = tcp - (downpayment + reservation)
            periodAmounts[2].value = parseFloat(tcp - (dp + re)).toFixed(2);
            break;
        case '5': // Cash payment
            // loan = loan - reservation
            periodAmounts[2].value = parseFloat(tcp - re).toFixed(2);
            break;
    }
}

// function zeroDownpayment(re, tcp) {
//   return parseFloat(tcp) - parseFloat(re);
// }

// function fixedLoanAmount(re, loan) {
//   return parseFloat(loan) + parseFloat(re);
// }

// function fixedDownPayment(re, dp, tcp) {
//   return parseFloat(tcp) - (parseFloat(dp) + parseFloat(re));
// }

$(document).on('change keyup blur', '#period_amount', function () {
    let periodAmounts = document.querySelectorAll('#period_amount');
    const tcp = totalContractPrice.value;
    let reservation = periodAmounts[0].value;
    let downpayment = periodAmounts[1].value;
    let loan = periodAmounts[2].value;
    let scheme_type_id = document.getElementById('scheme_type').value;
    computeBilling(scheme_type_id, tcp, reservation, downpayment, loan);
});

var addSellerBtn = $('#add_seller_btn');

$(addSellerBtn).on('click', function () {
    setTimeout(() => {
        initializeFormSellerRepeater();
    }, 500);
});

function initializeFormSellerRepeater() {
    var sellerEl = $('select#seller_id');
    var sellerPositionEl = $('select#seller_position_id');

    $(sellerEl).on('change', function () {
        var active = '';
        $(sellerPositionEl).each(function (index) {
            var name = $(this).attr('name');
            var i = name.charAt(7);
            active = i;
        });
        getSellerPosition($(this).val(), active);
    });
}

function getProperties(project_id) {
    const propertySelectElm = document.querySelector('#property_id');

    $(propertySelectElm).empty();

    $.ajax({
        url: base_url + 'transaction/get_properties',
        type: 'POST',
        data: {
            project_id,
        },
        success: function (data) {
            const { data: properties } = JSON.parse(data);

            if (properties.length > 0) {
                properties.sort(function (a, b) {
                    var textA = a.name.toUpperCase();
                    var textB = b.name.toUpperCase();
                    return textA < textB ? -1 : textA > textB ? 1 : 0;
                });
            }

            properties.map((property) => {
                const { id, name } = property;

                const optionEl = document.createElement('option');
                optionEl.value = id;
                optionEl.innerHTML = `${name}`;

                propertySelectElm.appendChild(optionEl);
            });
        },
    });
}

$('#project_id').on('change', function () {
    var project_id = $(this).val();

    getProperties(project_id);
});

$('#wht_rate').on('change', function () {
    var collectible_price = parseFloat($('#collectible_price').val());
    var wht_rate = parseFloat($(this).val());
    var wht_amount = $('#wht_amount');

    if (!collectible_price) {
        collectible_price = 0;
    }

    $(wht_amount).val(wht_rate * collectible_price);
});

$('#property_id').on('change', function () {
    setTimeout(() => {
        getSubTypeCat();
    }, 800);
});

$('#project_id').on('change', function () {
    setTimeout(() => {
        getSubTypeCat();
    }, 800);
});

var getSubTypeCat = function () {
    var project_id = $('#project_id').val();
    $.ajax({
        url: base_url + 'transaction/get_house_model',
        type: 'POST',
        dataType: 'JSON',
        data: {
            project_id: project_id,
        },
        success: function (res) {
            if (res) {
                var sub_type_id = res.data.sub_type_id;

                $('#property_types').val(sub_type_id);
            }
        },
    });
};

$(document).on(
    'change',
    '#financing_scheme_type, #reservation_amount, #downpayment, #downpayment_terms, #downpayment_interest_rate, #loan, #loan_terms, #loan_interest_rate',
    function () {
        let category_id = $('#financing_scheme_categories').val();
        let financing_scheme_type = $('#financing_scheme_type').val();
        let reservation_amount = $('#reservation_amount').val();
        let downpayment = $('#downpayment').val();
        let downpayment_terms = $('#downpayment_terms').val();
        let downpayment_interest_rate = $('#downpayment_interest_rate').val();
        let loan = $('#loan').val();
        let loan_terms = $('#loan_terms').val();
        let loan_interest_rate = $('#loan_interest_rate').val();
        $.ajax({
            url: base_url + 'financing_scheme/financing_scheme_query',
            type: 'POST',
            data: {
                financing_scheme_type: financing_scheme_type,
                reservation_amount: reservation_amount,
                downpayment: downpayment,
                downpayment_terms: downpayment_terms,
                downpayment_interest_rate: downpayment_interest_rate,
                loan: loan,
                loan_terms: loan_terms,
                loan_interest_rate: loan_interest_rate,
                category_id: category_id,
            },
            success: function (data) {
                const fSchemes = JSON.parse(data);
                financingSchemes.innerHTML = '';
                try {
                    fSchemes.map((scheme) => {
                        let opt = document.createElement('option');

                        opt.value = scheme.id;
                        opt.innerHTML = scheme.name;
                        financingSchemes.appendChild(opt);
                    });
                    loadScheme();
                } catch (error) {
                    let opt = document.createElement('option');
                    financingSchemes.innerHTML = '';
                    opt.value = '';
                    opt.innerHTML = 'No record found';
                    financingSchemes.appendChild(opt);
                }
            },
        });
    }
);

var getHouseModel = function (project_id, property_id) {
    var selectedProjectId = $('#project_id').val();
    var selectedPropId = $('#property_types').val();

    var houseModelEl = $('#house_model_id');

    if (!project_id) {
        project_id = selectedProjectId;
    }

    if (!property_id) {
        property_id = selectedPropId;
    }

    $(houseModelEl).empty();

    $.ajax({
        url: base_url + 'property/get_all_house_models',
        type: 'POST',
        data: {
            project_id,
            property_id,
        },
        success: function (data) {
            const { data: properties } = JSON.parse(data);
            if (properties) {
                properties.map((property) => {
                    const { id, name } = property;

                    const optionEl = document.createElement('option');
                    optionEl.value = id;
                    optionEl.innerHTML = `${name}`;

                    $(houseModelEl).append(optionEl);
                });
            }
        },
    });
};

$('#project_id').on('change', function () {
    setTimeout(() => {
        var project_id = $('#project_id').val();
        var property_id = $('#property_types').val();
        getHouseModel(project_id, property_id);
    }, 2000);
});
$('#property_types').on('change', function () {
    setTimeout(() => {
        var project_id = $('#project_id').val();
        var property_id = $('#property_types').val();
        getHouseModel(project_id, property_id);
    }, 2000);
});

getHouseModel();

initializeFormSellerRepeater();
getFinancingSchemeCategories();
setFinancingSchemesByCategory();
setFinancingSchemeType();


jQuery.validator.addMethod("notEqual", function(value, element, param) {
  return this.optional(element) || value != param;
}, "Please specify a different (non-default) value");