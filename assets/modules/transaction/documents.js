"use strict";
var KTDatatablesLandInventoryDocumentChecklist = function () {
    var ids = [];

    var _add_ons = function () {

        $('#checklist').select2({
            placeholder: "Select Checklist"
        });

        // Private functions


        $('._start_date').datepicker({
            orientation: "bottom left",
            todayBtn: "linked",
            clearBtn: true,
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
        });

        
    }

    function column_dates () {
        $("#start_date").on("change", function() {
            var start_date_val = $(this).val();

            ids.forEach(function (id) {
                const input_id = `#date_${id}`;

                if($(input_id)[0]) {
                    $(input_id).val(start_date_val);
                }
            })
        })
    }

    function column_categories () {
        $("#category").on("change", function() {
            var category_val = $(this).val();

            ids.forEach(function (id) {
                const input_id = `#category_${id}`;

                if($(input_id)[0]) {
                    $(input_id).val(category_val);
                }
            })
        })
    }

    function column_td_stage () {
        $("#transaction_document_stage_id").on("change", function() {
            var stage_val = $(this).val();
            ids.forEach(function (id) {
                const input_id = `#td_stage_id_${id}`;
                console.log(input_id);

                if($(input_id)[0]) {
                    $(input_id).val(stage_val);
                }
            })
        })
    }

    var _get_document_checklist = function () {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "0",
            "extendedTimeOut": "0",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $('#checklist').on('change', function () {

            var _value = $(this).val();
            var _li_id = $(this).data('li_id');

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: base_url + 'transaction/get_filtered_document_checklist',
                data: {
                    value: _value,
                    li_id: _li_id
                },
                beforeSend: function () {

                    KTApp.blockPage({
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: 'Processing...'
                    });
                },
                success: function (_respo) {

                    if (_respo._status === 1) {

                        $('div#_filtered_document_checklist').html(_respo._html);

                        $('#_document_checklist').DataTable({
                            order: [
                                [1, 'desc']
                            ],
                            pagingType: 'full_numbers',
                            lengthMenu: [5, 10, 25, 50, 100],
                            pageLength: 5,
                            responsive: true,
                            searchDelay: 500,
                            processing: true,
                            serverSide: false,
                            deferRender: true,
                            dom:
                                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
                                "<'row'<'col-sm-12 col-md-6'l>>" +
                                "<'row'<'col-sm-12'tr>>" +
                                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                            language: {
                                'lengthMenu': 'Show _MENU_',
                                'infoFiltered': '(filtered from _MAX_ total records)'
                            },
                            columnDefs: [{
                                    targets: [0, 1, 4, 5, 6, 7, 8],
                                    className: 'dt-center',
                                },
                                {
                                    targets: [0],
                                    orderable: false,
                                },
                                {
                                    targets: [7, 8],
                                    visible: false,
                                },
                                {
                                    targets:5,
                                    render: function (data, type, full, meta) {
                                        var id = full[1];

                                        if (ids.includes(id) === false) ids.push(id);

                                        return data
                                    },
                                },
                            ]
                        });
                    } else {

                        toastr.error(_respo._msg, "Failed");
                    }

                    KTApp.unblockPage();
                }
            }).fail(function () {

                KTApp.unblockPage();

                toastr.error('Please refresh the page and try again.', 'Oops!');
            });
        });
    }

    var _filterDocumentChecklist = function () {

        var dTable = $('#_transaction_document_table').DataTable();

        function _colFilter(n) {

            dTable.column(n).search($('#_column_' + n).val()).draw();
        }

        $('._filter').on('keyup change clear', function () {

            _colFilter($(this).data('column'));
        });
    }

    return {

        init: function () {

            _add_ons();
            _get_document_checklist();
            _filterDocumentChecklist();
            column_dates();
            column_categories();
            column_td_stage();
        }
    };
}();

jQuery(document).ready(function () {

    KTDatatablesLandInventoryDocumentChecklist.init();
});

