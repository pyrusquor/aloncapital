'use strict';
var Transaction = (function () {

    var initial_search = function () {
         var id = $('#search_by_transaction_id').val();
        $('#generalSearch').val(id).trigger('blur');
    }

    var confirmDelete = function () {
        $(document).on('click', '.remove_transaction', function () {
            var id = $(this).data('id');
            var btn = $(this);

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'transaction/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                btn.closest('div.kt-portlet').remove();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var processTransaction = function () {
        $(document).on('click', '.process_transaction', function () {
            var btn = $(this);
            var id = btn.data('id');
            var type = btn.data('type');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, please proceed.',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url:
                            base_url +
                            'transaction/process_transaction/' +
                            type +
                            '/' +
                            id,
                        // type: 'GET',
                        dataType: 'JSON',
                        data: { id: id },
                        success: function (res) {
                            if (type == 'change_seller') {
                                
                                $('#extra_large_modal_process .modal-content').html(res.html);
                                $('#extra_large_modal_process').modal('show');
                                initSellerForm()
                            } else if (type == 'change_commission_setup') {

                                $('#modal_process .modal-content').html(res.html);
                                $('#modal_process').modal('show');
                                initChangeCommissionSetup()
                            } else {

                                $('#modal_process .modal-content').html(res.html);
                                $('#modal_process').modal('show');
                            }

                            formRepeater()
                            formRepeaterAdhocFee();
                            formRepeaterPromo();
                            getModalTCP();
                            getModalLA();
                            getModalCP();
                            datepicker();
                            modalSubmit();
                            initValidation();

                            if (type == 'financing_scheme') {
                                getFinancingSchemeCategories();
                                setFinancingSchemesByCategory();
                                getDiscount();
                            }

                            if (type == 'discount') {
                                getDiscount();
                            }

                            if (res.info) {
                                var info = $('#info');
                                if (info) {
                                    info.text(JSON.stringify(res.info));
                                }
                            }

                            // if (res.status) {
                            // 	btn.closest('div.kt-portlet').remove();
                            // 	swal.fire(
                            // 		'Deleted!',
                            // 		res.message,
                            // 		'success'
                            // 	);
                            // } else {
                            // 	swal.fire(
                            // 		'Oops!',
                            // 		res.message,
                            // 		'error'
                            // 	)
                            // }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });

        $(document).on('click', '#confirmation', function () {
            var btn = $('[data-action="action-submit"]');
            var type = $('#type').val();
            var username = $('#username').val();
            var password = $('#password').val();
            var info = $('#info').val();

            $.ajax({
                url: base_url + 'transaction_confirmation/confirm',
                type: 'post',
                dataType: 'JSON',
                data: {
                    type: type,
                    username: username,
                    password: password,
                    info: info,
                },
                success: function (res) {
                    if (res.status) {
                        var formEl = $('#form_modal');
                        if (validator.form()) {
                            // See: src\js\framework\base\app.js
                            KTApp.progress(btn);
                            //KTApp.block(formEl);

                            // See: http://malsup.com/jquery/form/#ajaxSubmit
                            formEl.ajaxSubmit({
                                type: 'POST',
                                dataType: 'JSON',
                                success: function (response) {
                                    if (response.status) {
                                        swal.fire({
                                            title: 'Success!',
                                            text: response.message,
                                            type: 'success',
                                        }).then(function () {
                                            if (response.data.info) {
                                                var data = JSON.parse(
                                                    response.data.info
                                                );
                                            }
                                            if (response.type == 'change_property') {
                                                window.location.replace(
                                                    base_url + 'transaction/form/' + data.transaction_id + '/property'
                                                );
                                            } else if(response.type == 'update_transaction') {
                                                window.location.replace(
                                                    base_url + 'transaction/form/' + response.transaction_id
                                                );
                                            } else if(response.type == "recompute_schedule") {
                                                window.location.replace(
                                                    base_url +  'transaction/process_mortgage/' + response.transaction_id + '/save/recompute',
                                                );
                                            } else if(response.type == 'delete_transaction') {
                                                $.ajax({
                                                    url: base_url + 'transaction/delete/' + response.transaction_id,
                                                    type: 'POST',
                                                    dataType: 'JSON',
                                                    data: {
                                                        id: response.transaction_id,
                                                    },
                                                    success: function (res) {
                                                        if (res.status) {
                                                            btn.closest('div.kt-portlet').remove();
                                                            swal.fire('Deleted!', res.message, 'success');
                                                            window.location.replace(
                                                                base_url +
                                                                    'transaction'
                                                            );
                                                        } else {
                                                            swal.fire('Oops!', res.message, 'error');
                                                        }
                                                    },
                                                });
                                            } else if (response.type == 'change_buyer') {
                                                // alert('Change Buyer')
                                            } else {
                                                window.location.replace(
                                                    base_url +
                                                        'transaction/view/' +
                                                        response.transaction_id
                                                );
                                            }
                                        });
                                    } else {
                                        swal.fire(
                                            'Oops!',
                                            response.message,
                                            'error'
                                        );
                                    }
                                },
                            });
                        }
                    } else {
                        swal.fire('Oops!', res.message, 'error');
                    }
                },
            });
        });
    };

    var modalSubmit = function () {
        var formEl = $('#form_modal');
        var btn = $('[data-action="action-submit"]');
        var type = $('#type').val();
        var auth = $('#confirmation');

        btn.on('click', function (e) {
            e.preventDefault();

            var info = $('#form_modal').serialize();

            if (type == 'change_buyer') {
                var client_type_ids = $('.client_type').map((i, e) => e.value).get()

                if (!client_type_ids.includes('1')) {
    
                    swal.fire(
                        'Oops!',
                        'Please include a principal buyer.',
                        'error'
                    );
                    return;
                }
            }

            if (isNaN(auth)) {
                $.ajax({
                    url: base_url + 'transaction_confirmation/confirm_process',
                    type: 'post',
                    dataType: 'JSON',
                    data: info,
                    success: function (res) {
                        // $('#modal_process .modal-content').html(res.html);
                        // $('#modal_process').modal('show');

                        if (type == 'change_seller') {
                                
                            // $('#extra_large_modal_process .modal-content').html(res.html);
                            $('#extra_large_modal_process').modal('hide');
                            $('#modal_process .modal-content').html(res.html);
                            $('#modal_process').modal('show');
                        } else {

                            $('#modal_process .modal-content').html(res.html);
                            $('#modal_process').modal('show');
                        }
                    },
                });
            } else {
                if (validator.form()) {
                    // See: src\js\framework\base\app.js
                    KTApp.progress(btn);
                    //KTApp.block(formEl);

                    // See: http://malsup.com/jquery/form/#ajaxSubmit
                    formEl.ajaxSubmit({
                        type: 'POST',
                        dataType: 'JSON',
                        success: function (response) {
                            if (response.status) {
                                swal.fire({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                }).then(function () {
                                    window.location.replace(
                                        base_url + 'transaction'
                                    );
                                });
                            } else {
                                swal.fire('Oops!', response.message, 'error');
                            }
                        },
                    });
                }
            }
        });
    };

    var uploadSubmit = function () {
        var formEl = $('#_upload_form');
        var btn = $('#_upload_form_btn');

        btn.on('click', function (e) {
            e.preventDefault();

            var doctype = $('#_doc_type').val();

            $('#_doc_type_val').val(doctype);
            // See: src\js\framework\base\app.js
            KTApp.progress(btn);
            //KTApp.block(formEl);

            // See: http://malsup.com/jquery/form/#ajaxSubmit
            formEl.ajaxSubmit({
                type: 'POST',
                dataType: 'JSON',
                success: function (response) {
                    if (response.status) {
                        swal.fire({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                        }).then(function () {
                            window.location.replace(base_url + 'transaction');
                        });
                    } else {
                        swal.fire('Oops!', response.message, 'error');
                    }
                },
            });
        });
    };

    var initValidation = function () {
        var formEl = $('#form_modal');

        validator = formEl.validate({
            // Validate only visible fields
            ignore: ':hidden',

            // Validation rules
            rules: {
                refund: {
                    required: true,
                },
                reason: {
                    required: true,
                },
            },
            // Display error
            invalidHandler: function (event, validator) {
                swal.fire({
                    title: '',
                    text:
                        'There are some errors in your submission. Please correct them.',
                    type: 'error',
                    confirmButtonClass: 'btn btn-secondary',
                });
            },
            // Submit valid form
            submitHandler: function (form) {},
        });
    };

    var _add_ons = function () {
        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });
    };

    var generalSearch = function () {
        // General Search
        function load_data(keyword) {
            var page_num = page_num ? page_num : 0;

            var keyword = keyword ? keyword : '';
            $.ajax({
                url: base_url + 'transaction/paginationData/' + page_num,
                method: 'POST',
                data: 'page=' + page_num + '&keyword=' + keyword,
                success: function (html) {
                    KTApp.block('#kt_content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: 'Processing...',
                        css: {
                            padding: 0,
                            margin: 0,
                            width: '30%',
                            top: '40%',
                            left: '35%',
                            textAlign: 'center',
                            color: '#000',
                            border: '3px solid #aaa',
                            backgroundColor: '#fff',
                            cursor: 'wait',
                        },
                    });

                    setTimeout(function () {
                        $('#transactionContent').html(html);

                        KTApp.unblock('#kt_content');
                    }, 2000);
                },
            });
        }

        $('#generalSearch').blur(function () {
            var keyword = $(this).val();

            if (keyword !== '') {
                load_data(keyword);
            } else {
                load_data();
            }
        });
    };

    var advanceFilter = function () {
        $('#advanceSearch').submit(function (e) {
            e.preventDefault();
            var form_values = $('#advanceSearch').serialize();

            filter(form_values);
        });

        function filter(values) {
            var page_num = page_num ? page_num : 0;
            var values = values ? values : '';
            $.ajax({
                url: base_url + 'transaction/paginationData/' + page_num,
                method: 'POST',
                data: 'page=' + page_num + '&' + values,
                success: function (html) {
                    $('#filterModal').modal('hide');
                    KTApp.block('#kt_content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: 'Processing...',
                        css: {
                            padding: 0,
                            margin: 0,
                            width: '30%',
                            top: '40%',
                            left: '35%',
                            textAlign: 'center',
                            color: '#000',
                            border: '3px solid #aaa',
                            backgroundColor: '#fff',
                            cursor: 'wait',
                        },
                    });

                    setTimeout(function () {
                        $('#transactionContent').html(html);

                        KTApp.unblock('#kt_content');
                    }, 500);
                },
            });
        }
    };

    var _exportUploadCSV = function () {
        $('#_export_csv').validate({
            rules: {
                update_existing_data: {
                    required: true,
                },
            },
            messages: {
                update_existing_data: {
                    required: 'File type is required',
                },
            },
            invalidHandler: function (event, validator) {
                event.preventDefault();

                // var alert   =   $('#form_msg');
                // alert.closest('div.form-group').removeClass('kt-hide').show();
                KTUtil.scrollTop();

                toastr.options = {
                    closeButton: true,
                    debug: false,
                    newestOnTop: false,
                    progressBar: false,
                    positionClass: 'toast-top-right',
                    preventDuplicates: false,
                    onclick: null,
                    showDuration: '300',
                    hideDuration: '1000',
                    timeOut: '5000',
                    extendedTimeOut: '1000',
                    showEasing: 'swing',
                    hideEasing: 'linear',
                    showMethod: 'fadeIn',
                    hideMethod: 'fadeOut',
                };

                toastr.error(
                    'Please check your fields',
                    'Something went wrong'
                );
            },
            submitHandler: function (_frm) {
                _frm[0].submit();
            },
        });

        $('#_upload_form').validate({
            rules: {
                csv_file: {
                    required: true,
                },
            },
            messages: {
                csv_file: {
                    required: 'CSV file is required',
                },
            },
            invalidHandler: function (event, validator) {
                event.preventDefault();

                // var alert   =   $('#form_msg');
                // alert.closest('div.form-group').removeClass('kt-hide').show();
                KTUtil.scrollTop();

                toastr.options = {
                    closeButton: true,
                    debug: false,
                    newestOnTop: false,
                    progressBar: false,
                    positionClass: 'toast-top-right',
                    preventDuplicates: false,
                    onclick: null,
                    showDuration: '300',
                    hideDuration: '1000',
                    timeOut: '5000',
                    extendedTimeOut: '1000',
                    showEasing: 'swing',
                    hideEasing: 'linear',
                    showMethod: 'fadeIn',
                    hideMethod: 'fadeOut',
                };

                toastr.error(
                    'Please check your fields',
                    'Something went wrong'
                );
            },
            submitHandler: function (_frm) {
                _frm[0].submit();
            },
        });
    };

    var _initUploadGuideModal = function () {
        var _table = $('#_upload_guide_modal');

        _table.DataTable({
            order: [[0, 'asc']],
            pagingType: 'full_numbers',
            lengthMenu: [3, 5, 10, 25, 50, 100],
            pageLength: 5,
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: false,
            deferRender: true,
            dom:
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l>>" +
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            columnDefs: [
                {
                    targets: [0, 2, 3, 5],
                    className: 'dt-center',
                },
                {
                    targets: [0, 2, 3, 4, 5],
                    orderable: false,
                },
                {
                    targets: [4, 5],
                    searchable: false,
                },
                {
                    targets: 0,
                    searchable: false,
                    visible: false,
                },
            ],
        });
    };

    var bulkDelete = function () {
        $(document.body).on('change', '.delete_check', function () {
            if ($('.delete_check:checked').length) {
                $('#bulkDelete').removeAttr('disabled');
            } else {
                $('#bulkDelete').attr('disabled', 'disabled');
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];

            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'transaction/bulkDelete/',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    Swal.fire({
                                        title: 'Deleted!',
                                        text: res.message,
                                        type: 'success',
                                    }).then((result) => {
                                        // Reload the Page
                                        location.reload();
                                    });
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;

    // Private functions


    // Daterangepicker Init
    var daterangepickerInit = function () {
        if ($('#kt_transaction_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_transaction_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                // range = start.format("MMM D") + " - " + end.format("MMM D");
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                },
                opens: 'left',
                // ranges: {
                // 	'Today': [moment(), moment()],
                // 	'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                // 	'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                // 	'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                // 	'This Month': [moment().startOf('month'), moment().endOf('month')],
                // 	'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                // }
            },
            cb
        );

        cb(start, end, '');

        $(picker).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(
                picker.startDate.format('MM/DD/YYYY') +
                    ' - ' +
                    picker.endDate.format('MM/DD/YYYY')
            );
        });

        $(picker).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
    };

    // Daterangepicker Init
    var daterangepickerInit2 = function () {
        if ($('#kt_transaction_daterangepicker2').length == 0) {
            return;
        }

        var picker = $('#kt_transaction_daterangepicker2');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                // range = start.format("MMM D") + " - " + end.format("MMM D");
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                },
                opens: 'left',
                // ranges: {
                // 	'Today': [moment(), moment()],
                // 	'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                // 	'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                // 	'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                // 	'This Month': [moment().startOf('month'), moment().endOf('month')],
                // 	'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                // }
            },
            cb
        );

        cb(start, end, '');

        $(picker).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(
                picker.startDate.format('MM/DD/YYYY') +
                    ' - ' +
                    picker.endDate.format('MM/DD/YYYY')
            );
        });

        $(picker).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
    };

    var formRepeaterPromo = function () {
        $('#form_modal #promos_modal_form_repeater').repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width', '100%');
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    setTimeout(function () {
                        getModalTCP();
                        setTimeout(function () {
                            computeModalPromos();
                            setTimeout(function () {
                                getModalCP();
                                getModalLA();
                            }, 1000);
                        }, 1000);
                    }, 1000);

                    $(this).slideUp(deleteElement);
                    $('.select2-container').remove();
                    Select.init();
                    $('.select2-container').css('width', '100%');
                }
            },
        });
    };

    var getModalTCP = function (val = 0) {
        try {
            total_selling_price = parseFloat(
                $('#form_modal #total_selling_price').val()
            );

            promo_amount = 0;
            total_promo_amount = 0;

            $('.promoPrice').each(function (index, element) {
                promo_amount = parseFloat($(element).val());
                total_promo_amount += promo_amount;
            });

            if (isNaN(total_promo_amount)) {
                total_promo_amount = 0;
            }

            total_contract_price = total_selling_price + total_promo_amount;

            $('#form_modal #collectible_price').val(
                formatCurrency(total_contract_price)
            );
        } catch (err) {}
    };

    var computeModalPromos = function () {
        try {
            total_selling_price = parseFloat(
                $('#form_modal #total_selling_price').val()
            );
            total_contract_price = parseFloat(
                $('#form_modal #total_contract_price').val()
            );
            collectible_price = parseFloat(
                $('#form_modal #collectible_price').val()
            );
            loan_amount_deduction = parseFloat(
                $('#form_modal #loan_amount_deduction').val()
            );

            promo_amount = 0;
            promos = [];
            tcp_deductions = [];
            loan_deductions = [];
            loan_deduction_amount = 0;
            loans = [];

            $('.promoID')
                .find('option:selected')
                .each(function (index, element) {
                    // Deduct to values
                    // 1 = Deduct to Loan
                    // 2 = Deduct to TCP (Total Collectible Price)
                    var value = $(element).val();
                    var amount = $(element)
                        .closest('[data-repeater-item]')
                        .find('.promoPrice')
                        .val();

                    if (value == 1) {
                        loan_deductions.push(amount);
                    } else if (value == 2) {
                        tcp_deductions.push(amount);
                    }
                });
        } catch (err) {}
    };

    var getModalCP = function (val = 0) {
        try {
            total_contract_price = parseFloat(
                $('#form_modal #total_contract_price').val()
            );
            // promo_amount = 0;
            // total_promo_amount = 0;
            loan_amount = 0;
            tcp_amount = 0;

            $('.promoPrice').each(function (index, element) {
                var deduct_to = $(element)
                    .closest('[data-repeater-item]')
                    .find('.promoID')
                    .val();
                var value = $(element).val();

                if (deduct_to == 1) {
                    loan_amount += parseFloat(value);
                } else if (deduct_to == 2) {
                    tcp_amount += parseFloat(value);
                }
            });

            collectible_price = total_contract_price - tcp_amount;

            $('#form_modal #loan_amount_deduction').val(
                formatCurrency(loan_amount)
            );
            $('#form_modal #collectible_price').val(
                formatCurrency(collectible_price)
            );
        } catch (err) {}
    };

    var getModalLA = function (val = 0) {
        try {
            amount = 0;
            $('.promoID').each(function (index, element) {
                var deduct_to = $(element).val();
                var value = $(element)
                    .closest('[data-repeater-item')
                    .find('.promoPrice')
                    .val();
                if (deduct_to == 2) {
                    amount += parseFloat(value);
                }
            });
            loan_amount = formatCurrency(amount);

            $('#form_modal #loan_amount_deduction').val(loan_amount);
        } catch (err) {}
    };

    var formRepeaterAdhocFee = function () {
        $('#form_modal #fees_modal_form_repeater').repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width', '100%');
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    setTimeout(function () {
                        computeAdhocFees();
                        getAdhocSchedule();
                        getAH();
                    }, 1000);

                    $(this).slideUp(deleteElement);
                    $('.select2-container').remove();
                    Select.init();
                    $('.select2-container').css('width', '100%');
                }
            },
        });
    };

    var getAH = function (val = 0) {
        amount = 0;
        $('#form_modal .feesID').each(function (index, element) {
            amount =
                parseFloat(amount) +
                parseFloat($('.feesAmount').eq(index).val());
        });

        adhoc_fee_amount = formatCurrency(amount);

        $('#total_fee').val(adhoc_fee_amount);
    };

    var clickPrevMonth = function (){
        $("#prev_month").on("click", function () {
            summary_sales('monthly',1);
        });

    };

    var clickThisMonth = function (){
        $("#this_month").on("click", function () {
            summary_sales('monthly',0);
        });

    };

    var clickThisWeek = function (){
        $("#this_week").on("click", function () {
            summary_sales('weekly',0);
        });

    };
    var clickPrevWeek = function (){
        $("#prev_week").on("click", function () {
            summary_sales('weekly',1);
        });

    };

    var summary_sales = function (d,e) {
        if (typeof e ==="undefined"){
            e=0;
        };
        if(d=='daily'){

            $.ajax({
                url: base_url + "transaction/total_day_sales",
                type: "POST",
                dataType: "JSON",
                data : {filter:e},
                success: function (res) {
                    $("#daily_sales").html(res.amount);
                    $("#daily_count").html(res.count+" contract(s)");
                },
            });

        }else if(d=='weekly'){

            $.ajax({
                url: base_url + "transaction/total_week_sales",
                type: "POST",
                dataType: "JSON",
                data : {filter:e},
                success: function (res) {
                    $("#weekly_sales").html(res.amount);
                    $("#weekly_count").html(res.count+" contract(s)");
                    $("#daterange_weekly").html(res.week);
                },
            });

        }else if(d=='monthly'){

            $.ajax({
                url: base_url + "transaction/total_month_sales",
                type: "POST",
                dataType: "JSON",
                data : {filter:e},
                success: function (res) {
                    $("#monthly_sales").html(res.amount);
                    $("#monthly_count").html(res.count+" contract(s)");
                    $("#daterange_monthly").html(res.month);
                },
            });

        }
    };

    var load_sales_filters = function (){

        var project_id = localStorage.getItem('sales_dashboard_id');
        var project_name = localStorage.getItem('sales_dashboard_name');
        var month = localStorage.getItem('current_month');
        var year = localStorage.getItem('current_year');
        var sales_reservation_date = localStorage.getItem('sales_reservation_date');
    
    
        if(year!==null){
            $('#month').val(month);
            $('#year').val(year);
            $('#project_id').append(new Option(project_name, project_id,false,true));
            TransactionPagination();
    
        } else if(sales_reservation_date!==null){
            $('#project_id').append(new Option(project_name, project_id,false,true));
            $('#kt_transaction_daterangepicker').val(sales_reservation_date);
            TransactionPagination();
    
        }
        localStorage.removeItem('sales_dashboard_id');
        localStorage.removeItem('current_month');
        localStorage.removeItem('current_year');
        localStorage.removeItem('sales_reservation_date');
        localStorage.removeItem('sales_dashboard_id');
        localStorage.removeItem('sales_dashboard_name');
    
    }

    var load_properties_filters = function (){
        var project_id = localStorage.getItem('properties_dashboard_id');
        var project_name = localStorage.getItem('properties_dashboard_name');
        var reservation_date = localStorage.getItem('properties_reservation_date');
        if(reservation_date!==null){
            $('#project_id').append(new Option(project_name, project_id,false,true));
            $('#kt_transaction_daterangepicker').val(reservation_date);
            TransactionPagination();
        }
        localStorage.removeItem('properties_dashboard_name');
        localStorage.removeItem('properties_dashboard_id');
        localStorage.removeItem('properties_reservation_date');

    }
    var load_sellers_filters = function (){
        var project_id = localStorage.getItem('sellers_project_id');
        var project_name = localStorage.getItem('sellers_project_name');
        var seller_id = localStorage.getItem('sellers_id');
        var seller_name = localStorage.getItem('sellers_name');
        var reservation_date = localStorage.getItem('sellers_reservation_date');
        if(reservation_date!==null){
            $('#project_id').append(new Option(project_name, project_id,false,true));
            $('#seller_id').append(new Option(seller_name, seller_id,false,true));
            $('#kt_transaction_daterangepicker').val(reservation_date);
            TransactionPagination();
        }
        localStorage.removeItem('sellers_project_id');
        localStorage.removeItem('sellers_project_name');
        localStorage.removeItem('sellers_reservation_date');
        localStorage.removeItem('sellers_id');
        localStorage.removeItem('sellers_name');

    }
    var load_general_filters = function (){
        var seller_id = localStorage.getItem('general_buyer_id');
        var seller_name = localStorage.getItem('general_buyer_name');
        if(seller_id!==null){
            $('#seller_id').append(new Option(seller_name, seller_id,false,true));
            TransactionPagination();
        }
        localStorage.removeItem('sellers_project_id');
        localStorage.removeItem('sellers_project_name');
        localStorage.removeItem('sellers_reservation_date');
        localStorage.removeItem('sellers_id');
        localStorage.removeItem('sellers_name');
        localStorage.removeItem('general_buyer_id');
        localStorage.removeItem('general_buyer_name');
    }
    const datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });
    
        $('.datePicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });
    
        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
        $('.monthPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'mm',
            viewMode: 'months',
            minViewMode: 'months',
            autoclose: true,
        });
        $('.dayPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'dd',
            viewMode: 'days',
            minViewMode: 'days',
            autoclose: true,
        });
        $('.kt_datepicker').datepicker({
            orientation: 'bottom left',
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: 'yyyy-mm-dd',
        });
    };
    var load_select2 = function (){
        $('#modal_process, #extra_large_modal_process').on('shown.bs.modal', function(e) { 
            $('.suggests_on_load_modal').select2({
                minimumInputLength: 1,
                dropdownParent: $(this),
            })
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            load_select2()
            modalSubmit();
            confirmDelete();
            _add_ons();
            generalSearch();
            advanceFilter();
            _exportUploadCSV();
            _initUploadGuideModal();
            bulkDelete();
            datepicker();
            processTransaction();
            daterangepickerInit();
            daterangepickerInit2();
            formRepeaterAdhocFee();
            formRepeaterPromo();
            computeModalPromos();
            getModalCP();
            getModalTCP();
            getModalLA();
            getModalCP();
            uploadSubmit();
            summary_sales();
            clickPrevMonth();
            clickThisMonth();
            clickThisWeek();
            clickPrevWeek();
            load_sales_filters();
            summary_sales('daily',0);
            summary_sales('monthly',0);
            summary_sales('weekly',0);
            load_sales_filters();
            load_properties_filters();
            load_sellers_filters();
            load_general_filters();
            initial_search();
        },
    };

    
})();



jQuery(document).ready(function () {
    Transaction.init();
});


function arNotesButtons() {
    const arNotesBtn = document.querySelectorAll('#view_ar_notes');
    function arNotesViewed(transaction_id, el) {
        $.ajax({
            url: base_url + 'ticketing/update_ticket_status/' + transaction_id,
            type: 'POST',
            dataType: 'JSON',
            success: function (response) {
                if (response.status) {
                    // Update view count
                    $(el).find('span').html(0);
                } else {
                    return;
                }
            },
        });
    }

    arNotesBtn.forEach((btn, key) => {
        let length = arNotesBtn.length;
        let count = 0;

        btn.addEventListener('click', function (e) {
            const id = $(this).data('transactionid');

            arNotesViewed(id, this);
            count++;
        });

        if (count === length) {
            $('.view_ar_note').trigger('click');
        }
    });
}

arNotesButtons();

function initilizePaginationLinks(ul) {
    let links = $(ul).find('a');

    $(links).each(function (index) {
        $(this).on('click', function () {
            setTimeout(() => {
                arNotesButtons();
            }, 700);
        });
    });
}

let ul = $('.kt-pagination__links');

initilizePaginationLinks(ul);

function TransactionPagination(page_num) {
    page_num = page_num ? page_num : 0;

    var keyword = $('#generalSearch').val();

    const general_status = $('#general_status').val();
    const collection_status = $('#collection_status').val();
    const documentation_status = $('#documentation_status').val();
    const reference = $('#reference').val();
    const project_id = $('#project_id').val();
    const property_id = $('#property_id').val();
    const period = $('#period').val();
    const buyer_id = $('#buyer_id').val();
    const seller_id = $('#seller_id').val();
    const reservation_date = $('#kt_transaction_daterangepicker').val();
    const due_for_payment_date = $('#kt_transaction_daterangepicker2').val();
    const month = $('#month').val();
    const year = $('#year').val();
    const personnel_id = $('#personnel_id').val();

    const greater_than = $('#greater_than').val();
    const equal_to = $('#equal_to').val();
    const lower_than = $('#lower_than').val();

    const dp_greater_than = $('#dp_greater_than').val();
    const dp_equal_to = $('#dp_equal_to').val();
    const dp_lower_than = $('#dp_lower_than').val();

    $.ajax({
        url: base_url + 'transaction/paginationData/' + page_num,
        method: 'POST',
        data:
            'page=' +
            page_num +
            '&keyword=' +
            keyword +
            '&general_status=' +
            general_status +
            '&collection_status=' +
            collection_status +
            '&documentation_status=' +
            documentation_status +
            '&reference=' +
            reference +
            '&project_id=' +
            project_id +
            '&property_id=' +
            property_id +
            '&period=' +
            period +
            '&buyer_id=' +
            buyer_id +
            '&seller_id=' +
            seller_id +
            '&month=' +
            month +
            '&year=' +
            year +
            '&reservation_date=' +
            reservation_date +
            '&due_for_payment_date=' +
            due_for_payment_date +
            '&personnel_id=' +
            personnel_id  +
            '&greater_than=' +
            greater_than +
            '&equal_to=' +
            equal_to +
            '&lower_than=' +
            lower_than  +
            '&dp_greater_than=' +
            dp_greater_than +
            '&dp_equal_to=' +
            dp_equal_to +
            '&dp_lower_than=' +
            dp_lower_than,
        success: function (html) {
            KTApp.block('#kt_content', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Processing...',
                css: {
                    padding: 0,
                    margin: 0,
                    width: '30%',
                    top: '40%',
                    left: '35%',
                    textAlign: 'center',
                    color: '#000',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait',
                },
            });

            setTimeout(function () {
                $('#transactionContent').html(html);

                KTApp.unblock('#kt_content');

                let ul = $('.kt-pagination__links');
                initilizePaginationLinks(ul);
            }, 500);
        },
    });
}

const financingSchemeType = document.querySelector('#scheme_type');
const totalContractPrice = document.querySelector('#total_contract_price');

// Get discount from promos module
const getDiscount = () => {
    const financingSchemeDiscount = document.querySelector('#fs_discount');
    if (financingSchemeDiscount) {
        $.ajax({
            url: base_url + 'promos/get_all',
            type: 'GET',
            success: function (data) {
                const discounts = JSON.parse(data);

                discounts.data.map((discount) => {
                    let opt = document.createElement('option');

                    opt.value = discount.id;
                    opt.setAttribute('data-value', discount.value);
                    opt.setAttribute('data-deduct-to', discount.deduct_to);
                    opt.setAttribute('data-value-type', discount.value_type);
                    opt.innerHTML = discount.name;
                    financingSchemeDiscount.appendChild(opt);
                });
            },
        });
    }
};

// Get financing scheme categories
const getFinancingSchemeCategories = () => {
    const financingSchemeCategories = document.querySelector(
        '#financing_scheme_categories'
    );

    $.ajax({
        url: base_url + 'financing_scheme_categories/get_all',
        type: 'GET',
        success: function (data) {
            const categories = JSON.parse(data);
            categories.data.map((category) => {
                let opt = document.createElement('option');

                opt.value = category.id;
                opt.innerHTML = category.name;
                financingSchemeCategories.appendChild(opt);
            });
        },
    });
};

const setFinancingSchemesByCategory = () => {
    $('#financing_scheme_categories').on('change', function () {
        var categoryID = $(this).val();
        const financingSchemes = document.querySelector('#financing_scheme_id');
        document.getElementById('financing_scheme_id').disabled = false;
        $.ajax({
            url: base_url + 'financing_scheme/financing_scheme_by_category',
            type: 'POST',
            data: {
                categoryID,
            },
            success: function (data) {
                const fSchemes = JSON.parse(data);
                financingSchemes.innerHTML = '';
                let opt = document.createElement('option');
                opt.value = '';
                opt.innerHTML = 'Select Financing Scheme...';
                financingSchemes.appendChild(opt);
                try {
                    fSchemes.map((scheme) => {
                        let opt = document.createElement('option');

                        if (typeof scheme.values != 'undefined') {
                            // Reservation billing info base in financing scheme values
                            opt.setAttribute(
                                'data-re-term',
                                scheme.values[0]['period_term']
                            );
                            opt.setAttribute(
                                'data-re-interest',
                                scheme.values[0]['period_interest_percentage']
                            );
                            opt.setAttribute(
                                'data-re-amount',
                                scheme.values[0]['period_rate_amount']
                            );
                            // Downpayment billing info base in financing scheme values
                            opt.setAttribute(
                                'data-dp-term',
                                scheme.values[1]['period_term']
                            );
                            opt.setAttribute(
                                'data-dp-interest',
                                scheme.values[1]['period_interest_percentage']
                            );
                            opt.setAttribute(
                                'data-dp-amount',
                                scheme.values[1]['period_rate_amount']
                            );


                            if ( !isNaN(scheme.values[2]) ) {
                                // Loan billing info base in financing scheme values
                                opt.setAttribute(
                                    'data-loan-term',
                                    scheme.values[2]['period_term']
                                );
                                opt.setAttribute(
                                    'data-loan-interest',
                                    scheme.values[2]['period_interest_percentage']
                                );
                                opt.setAttribute(
                                    'data-loan-amount',
                                    scheme.values[2]['period_rate_amount']
                                );
                            }
                           
                        }

                        opt.value = scheme.id;
                        opt.innerHTML = scheme.name;
                        opt.setAttribute('data-scheme', scheme.scheme_type);
                        financingSchemes.appendChild(opt);
                    });
                } catch (error) {
                    let opt = document.createElement('option');
                    financingSchemes.innerHTML = '';
                    opt.value = '';
                    opt.innerHTML = 'No record found';
                    financingSchemes.appendChild(opt);
                }
            },
        });
    });
};

$(document).on('change keyup', '#financing_scheme_id', function () {
    changeFinancingScheme();
});

$(document).on('change keyup', '#fs_discount', function () {
    var type = $('#type').val();
    if ((type = 'financing_scheme')) {
        changeFinancingScheme();
    }
});

function changeFinancingScheme() {
    let period_id = 1;
    let id = $('#financing_scheme_id').val();
    let transaction_id = $('#transactionID').val();
    let completedTerms = [];
    if (id == '') {
        return;
    }
    $.ajax({
        url: base_url + 'transaction_payment/get_official_payments',
        type: 'POST',
        data: {
            transaction_id,
        },
        success: function (data) {
            const totalPayment = JSON.parse(data);
            $('#official_payments').val(JSON.stringify(totalPayment));
        },
    });

    // Get completed terms per period id
    while (period_id < 5) {
        $.ajax({
            url: base_url + 'transaction_payment/get_completed_terms',
            type: 'POST',
            data: {
                transaction_id,
                period_id,
            },
            success: function (data) {
                const result = JSON.parse(data);
                // completedTerms.push({'period_id': completedTerms.length + 1, 'count': result[0]});
                if (result[0]['period_id']) {
                    completedTerms.push({
                        count: result[0]['count'],
                        period_id: result[0]['period_id'],
                    });
                }
            },
        });

        // Check if partial payment exists
        if (period_id == 2) {
            $.ajax({
                url: base_url + 'transaction_payment/check_partial_payments',
                type: 'POST',
                data: {
                    transaction_id,
                    period_id,
                },
                success: function (data) {
                    const result = JSON.parse(data);
                    $('#dp_partial_payment').val(result);
                },
            });
        } else if (period_id == 3) {
            $.ajax({
                url: base_url + 'transaction_payment/check_partial_payments',
                type: 'POST',
                data: {
                    transaction_id,
                    period_id,
                },
                success: function (data) {
                    const result = JSON.parse(data);
                    $('#lo_partial_payment').val(result);
                },
            });
        }

        period_id++;
    }

    setTimeout(() => {
        $('#completed_terms').val(JSON.stringify(completedTerms));
    }, 1000);

    // Get financing scheme details
    $.ajax({
        url: base_url + 'financing_scheme/get_detail',
        type: 'POST',
        data: {
            id,
        },
        success: function (data) {
            const fScheme = JSON.parse(data);
            // Set financing scheme period rate percentage
            $('#dp_prp').val(fScheme[0]['values'][1]['period_rate_percentage']);
            $('#lo_prp').val(fScheme[0]['values'][2]['period_rate_percentage']);
            // Set financing scheme period interest percentage
            $('#dp_pip').val(
                fScheme[0]['values'][1]['period_interest_percentage']
            );
            $('#lo_pip').val(
                fScheme[0]['values'][2]['period_interest_percentage']
            );
            // Set financing scheme period rate amount
            $('#ra_amount').val(fScheme[0]['values'][0]['period_rate_amount']);
            $('#dp_amount').val(fScheme[0]['values'][1]['period_rate_amount']);
            $('#lo_amount').val(fScheme[0]['values'][2]['period_rate_amount']);
            // Set financing scheme period terms
            $('#ra_term').val(fScheme[0]['values'][0]['period_term']);
            $('#dp_term').val(fScheme[0]['values'][1]['period_term']);
            $('#lo_term').val(fScheme[0]['values'][2]['period_term']);
            // Check if equity exists
            if (fScheme[0]['values'][3]) {
                $('#eq_prp').val(
                    fScheme[0]['values'][3]['period_rate_percentage']
                );
                $('#eq_pip').val(
                    fScheme[0]['values'][3]['period_interest_percentage']
                );
                $('#eq_amount').val(
                    fScheme[0]['values'][3]['period_rate_value']
                );
                $('#eq_term').val(fScheme[0]['values'][3]['period_term']);
            }
            recalculateFinancingScheme();
        },
    });
}

// Set financing scheme modal billing information
function recalculateFinancingScheme() {
    let tcp = parseFloat($('#tcp').html()).toFixed(2);
    let financing_scheme = document.getElementById('financing_scheme_id');
    let scheme_type = financing_scheme.options[
        financing_scheme.selectedIndex
    ].getAttribute('data-scheme');
    // Get billing information table data
    const periodType = document.querySelectorAll('#period_type');
    const effectivityDate = document.querySelectorAll('#effectivity_date');
    const periodTerm = document.querySelectorAll('#period_term');
    const interestRate = document.querySelectorAll('#interest_rate');
    const billingAmount = document.querySelectorAll('#billing_amount');
    const monthlyPayment_el = document.querySelectorAll('#monthly_payment');

    // Set new financing scheme details
    let dp_interest = financing_scheme.options[
        financing_scheme.selectedIndex
    ].getAttribute('data-dp-interest');
    
    let lo_interest = financing_scheme.options[
        financing_scheme.selectedIndex
    ].getAttribute('data-loan-interest');

    if (dp_interest == null) {
        dp_interest = $('#dp_pip').val();
    }

    if (lo_interest == null) {
        lo_interest = $('#lo_pip').val();
    }


    let officialPayments = JSON.parse($('#official_payments').val());

    // Payments for each period
    let ra_date, dp_date, lo_date;
    let ra_payment = 0;
    let dp_payment = 0;
    let lo_payment = 0;
    let eq_payment = 0;
    let ra_terms = 0;
    let dp_terms = 0;
    let lo_terms = 0;
    let eq_terms = 0;
    let ra_completed_terms = 0;
    let dp_completed_terms = 0;
    let lo_completed_terms = 0;
    let eq_completed_terms = 0;
    let ra_monthly = 0;
    let dp_monthly = 0;
    let lo_monthly = 0;

    setTimeout(() => {
        let completedTerms = JSON.parse($('#completed_terms').val());
        try {
            if (
                parseFloat(
                    billingAmount[0].innerHTML.replace(/[^0-9\.-]+/g, '')
                ) != parseFloat($('#ra_amount').val())
            ) {
                swal.fire(
                    'Oops!',
                    'Reservation amount must be equal to the previous reservation amount',
                    'error'
                );
                return;
            }

            if (
                parseInt($('#dp_partial_payment').val()) != 0 ||
                parseInt($('lo_partial_payment').val() != 0)
            ) {
                swal.fire(
                    'Oops!',
                    'Please settle first the partial payment balance before changing the financing scheme!',
                    'error'
                );
                return;
            }
        } catch (err) {
            swal.fire(
                'Oops!',
                'Missing billing information in current transaction!',
                'error'
            );
            return;
        }

        for (var key in completedTerms) {
            switch (completedTerms[key]['period_id']) {
                case '1':
                    ra_completed_terms += parseInt(
                        completedTerms[key]['count']
                    );
                    break;

                case '2':
                    dp_completed_terms += parseInt(
                        completedTerms[key]['count']
                    );
                    break;

                case '3':
                    lo_completed_terms += parseInt(
                        completedTerms[key]['count']
                    );
                    break;

                case '4':
                    if (periodterm[3]) {
                        eq_completed_terms += parseInt(
                            completedTerms[key]['count']
                        );
                    }
                    break;
            }
        }

        ra_terms = $('#ra_term').val();
        dp_terms = parseInt($('#dp_term').val() - dp_completed_terms);
        lo_terms = parseInt($('#lo_term').val() - lo_completed_terms);
        if (periodTerm[3]) {
            eq_terms = parseInt($('#eq_term').val() - eq_completed_terms);
        }

        if (dp_terms <= 0) {
            dp_terms = 0;
        }

        // Get amount paid by period
        for (var key in officialPayments) {
            switch (officialPayments[key]['period_id']) {
                case '1': // Reservation
                    ra_payment += parseFloat(
                        officialPayments[key]['amount_paid']
                    );
                    break;
                case '2': // Down-payment
                    dp_payment += parseFloat(
                        officialPayments[key]['amount_paid']
                    );
                    break;
                case '3': // Loan
                    lo_payment += parseFloat(
                        officialPayments[key]['amount_paid']
                    );
                    break;
                case '4': // Equity
                    eq_payment += parseFloat(
                        officialPayments[key]['amount_paid']
                    );
                    break;
            }
        }

        // Get financing scheme period rate percentage
        let fs_dp_prp = parseFloat($('#dp_prp').val());
        let fs_lo_prp = parseFloat($('#lo_prp').val());
        let fs_eq_prp = parseFloat($('#eq_prp').val());

        // Get financing scheme period rate amount
        let fs_ra_amount = $('#ra_amount').val();
        let fs_dp_amount = $('#dp_amount').val();
        let fs_lo_amount = $('#lo_amount').val();
        let fs_eq_amount = $('#eq_amount').val();

        let ra_amount = billingAmount[0].innerHTML.replace(/[^0-9\.-]+/g, '');
        let dp_amount = 0;
        let lo_amount = 0;
        let eq_amount = 0;

        // Calculate billing amount
        switch (scheme_type) {
            case 'Zero Downpayment': // Zero down payment
                dp_amount = 0;
                tcp = parseFloat(tcp) - parseFloat(fs_ra_amount);
                lo_amount = (parseFloat(tcp) * parseFloat(fs_lo_prp)) / 100;
                // billingAmount[2].innerHTML = formatMoney(lo_amount);
                break;
            case 'Fixed Loan Amount': // Fixed loan amount
                dp_amount =
                    parseFloat(tcp) -
                    (parseFloat(fs_lo_amount) + parseFloat(fs_ra_amount));
                lo_amount = parseFloat(fs_lo_amount);
                // billingAmount[1].innerHTML = formatMoney(dp_amount);
                break;
            case 'Fixed Downpayment': // Fixed down payment
                lo_amount =
                    parseFloat(tcp) -
                    (parseFloat(fs_dp_amount) + parseFloat(fs_ra_amount));
                dp_amount = parseFloat(fs_dp_amount);
                // billingAmount[2].innerHTML = formatMoney(lo_amount);
                break;
            case 'Cash Payment': // Case Sale
                lo_amount = parseFloat(tcp) - parseFloat(fs_ra_amount);
                break;
            default:
                // standard
                if (fs_dp_prp != '0.00') {
                    fs_dp_prp /= 100;
                    dp_amount = (tcp * fs_dp_prp - fs_ra_amount).toFixed(2);
                } else {
                    dp_amount = fs_dp_amount;
                }

                if (fs_lo_prp != '0.00') {
                    fs_lo_prp /= 100;
                    lo_amount = (tcp * fs_lo_prp).toFixed(2);
                } else {
                    lo_amount = fs_lo_amount;
                }

                if (fs_eq_prp != '0.00') {
                    fs_eq_prp /= 100;
                    eq_amount = (tcp * fs_eq_prp).toFixed(2);
                } else {
                    eq_amount = fs_eq_amount;
                }
                break;
        }

        console.log(lo_amount +" "+scheme_type+" "+tcp+" "+fs_dp_amount+" "+fs_ra_amount);
        
        if (dp_amount < 0 || lo_amount < 0) {
            swal.fire(
                'Oops!',
                'Total amount may not be less than zero',
                'error'
            );
            return;
        }

        ra_date = new Date(effectivityDate[0].innerHTML);
        dp_date = new Date(effectivityDate[1].innerHTML);
        var temp_date = new Date(effectivityDate[1].innerHTML);

        if ($('#type').val() == 'financing_scheme') {
            if (dp_terms == 0) {
                lo_date = temp_date.setMonth(
                    temp_date.getMonth() + (parseInt(dp_terms) + 1)
                );
            } else {
                lo_date = temp_date.setMonth(
                    temp_date.getMonth() + parseInt(dp_terms)
                );
            }
        } else {
            lo_date = new Date(effectivityDate[2].innerHTML);
        }

        effectivityDate[2].innerHTML = moment(lo_date);

        // Get data to display in billing information table
        let billing_information = [
            {
                period_term: ra_terms,
                period_amount: billingAmount[0].innerHTML.replace(
                    /[^0-9\.-]+/g,
                    ''
                ),
                interest_rate: 0,
                period_id: 1,
                effectivity_date: formatDate(effectivityDate[0].innerHTML),
            },
            {
                period_term: parseInt($('#dp_term').val()),
                period_amount: dp_amount,
                interest_rate: dp_interest,
                period_id: 2,
                effectivity_date: formatDate(dp_date),
            },
            {
                period_term: parseInt($('#lo_term').val()),
                period_amount: lo_amount,
                interest_rate: lo_interest,
                period_id: 3,
                effectivity_date: formatDate(lo_date),
            },
        ];

        document.getElementById('tb_values').value = JSON.stringify(
            billing_information
        );

        // }

        // Deduct to value:
        // 1 = Total Selling Price
        // 2 = Total Contract Price
        // 3 = Collectible Price
        // 4 = Total Lot Price
        // 5 = Total House Price
        // 6 = Loan Amount
        let discount_type = document.querySelector('#fs_discount');

        let deduct_to = discount_type.options[
            discount_type.selectedIndex
        ].getAttribute('data-deduct-to');
        if (deduct_to != null) {
            if (deduct_to == '6') {
                lo_amount = calculateDiscount(lo_amount);
            } else if (deduct_to == '2') {
                tcp = calculateDiscount(lo_amount);
            }
        }

        // Deduct total paid amount
        dp_amount -= dp_payment;
        lo_amount -= lo_payment;
        eq_amount -= eq_payment;

        if (dp_amount <= 0) {
            dp_amount = 0;
        }

        ra_monthly = monthlyPayment(ra_amount, 0, ra_terms);
        dp_monthly = monthlyPayment(dp_amount, dp_interest, dp_terms);
        lo_monthly = monthlyPayment(lo_amount, lo_interest, lo_terms);

        setTimeout(() => {
            ra_monthly = ra_monthly.responseJSON ? ra_monthly.responseJSON : 0;
            dp_monthly = dp_monthly.responseJSON ? dp_monthly.responseJSON : 0;
            lo_monthly = lo_monthly.responseJSON ? lo_monthly.responseJSON : 0;

            // Set billing information table
            // Table terms
            periodTerm[0].innerHTML = ra_terms + ' mo(s)';
            periodTerm[1].innerHTML = dp_terms + ' mo(s)';
            periodTerm[2].innerHTML = lo_terms + ' mo(s)';

            // Table interests
            interestRate[1].innerHTML = dp_interest + '%';
            interestRate[2].innerHTML = lo_interest + '%';

            // Table total amount
            billingAmount[1].innerHTML = formatMoney(dp_amount.toFixed(2));
            billingAmount[2].innerHTML = formatMoney(lo_amount.toFixed(2));
            if (billingAmount[3]) {
                billingAmount[3].innerHTML = formatMoney(eq_amount.toFixed(2));
            }

            // Table monthly payment
            monthlyPayment_el[1].innerHTML = formatMoney(dp_monthly.toFixed(2));
            monthlyPayment_el[2].innerHTML = formatMoney(lo_monthly.toFixed(2));

            let new_billing = [
                {
                    Period: 1,
                    'Effectivity Date': moment(ra_date),
                    Terms: ra_terms,
                    'Interest Rate': 0,
                    'Total Amount': formatMoney(ra_amount),
                    'Monthly Payment': ra_monthly,
                },
                {
                    Period: 2,
                    'Effectivity Date': moment(dp_date),
                    Terms: dp_terms,
                    'Interest Rate': dp_interest,
                    'Total Amount': formatMoney(dp_amount),
                    'Monthly Payment': dp_monthly,
                },
                {
                    Period: 3,
                    'Effectivity Date': moment(lo_date),
                    Terms: lo_terms,
                    'Interest Rate': lo_interest,
                    'Total Amount': formatMoney(lo_amount),
                    'Monthly Payment': lo_monthly,
                },
            ];

            let financing_scheme_values = [
                {
                    // Reservation
                    period_term: ra_terms,
                    period_amount: billingAmount[0].innerHTML.replace(
                        /[^0-9\.-]+/g,
                        ''
                    ),
                    interest_rate: 0,
                    period_id: 1,
                    effectivity_date: formatDate(effectivityDate[0].innerHTML),
                    monthly_payment: ra_monthly,
                },
                {
                    // Downpayment Terms
                    period_term: dp_terms,
                    period_amount: billingAmount[1].innerHTML.replace(
                        /[^0-9\.-]+/g,
                        ''
                    ),
                    interest_rate: dp_interest,
                    period_id: 2,
                    effectivity_date: formatDate(effectivityDate[1].innerHTML),
                    monthly_payment: dp_monthly,
                },
                {
                    // Loan Terms
                    period_term: lo_terms,
                    period_amount: billingAmount[1].innerHTML.replace(
                        /[^0-9\.-]+/g,
                        ''
                    ),
                    interest_rate: lo_interest,
                    period_id: 3,
                    effectivity_date: formatDate(lo_date),
                    monthly_payment: lo_monthly,
                },
            ];

            document.getElementById('fs_values').value = JSON.stringify(
                financing_scheme_values
            );
            // }
        }, 1000);
    }, 1000);
}

// Calculate financing scheme discount
function calculateDiscount(loan_amount) {
    // Deduct to value:
    // 1 = Total Selling Price
    // 2 = Total Contract Price
    // 3 = Collectible Price
    // 4 = Total Lot Price
    // 5 = Total House Price
    // 6 = Loan Amount

    const fsDiscount = document.querySelector('#fs_discount');

    let discount = 0;
    let discount_price = 0;
    let deduct_to = fsDiscount.options[fsDiscount.selectedIndex].getAttribute(
        'data-deduct-to'
    );
    let value_type = fsDiscount.options[fsDiscount.selectedIndex].getAttribute(
        'data-value-type'
    );

    switch (deduct_to) {
        case '1':
            var amount = total_selling_price.innerHTML;
            break;
        case '2':
            var amount = $('#tcp').val();
            break;
        case '3':
            var amount = collectible_price.innerHTML;
            break;
        case '4':
            var amount = total_lot_price.innerHTML;
            break;
        case '5':
            var amount = total_house_price.innerHTML;
            break;
        case '6':
            var amount = loan_amount;
            break;
        default:
            var amount = 0;
            break;
    }

    if (value_type != null) {
        if (value_type == '1') {
            if ((deduct_to = '6')) {
                discount = fsDiscount.options[
                    fsDiscount.selectedIndex
                ].getAttribute('data-value');
                discount = discount / 100;
                var tcp = $('#total_contract_price').val();
                discount_price = (parseFloat(tcp) * discount).toFixed(2);
                amount -= discount_price;
                return amount;
            } else {
                discount = fsDiscount.options[
                    fsDiscount.selectedIndex
                ].getAttribute('data-value');
                discount = discount / 100;
                discount_price = (amount * discount).toFixed(2);
                return (amount -= discount_price);
            }
        } else {
            discount_price = parseFloat(
                fsDiscount.options[fsDiscount.selectedIndex].getAttribute(
                    'data-value'
                )
            );
            return (amount -= discount_price);
        }
    } else {
        return amount;
    }
}

let monthlyPayment = (full_amount, interest_rate, term) => {
    let amount = $.ajax({
        url: base_url + 'transaction/recompute_monthly_payment',
        type: 'POST',
        dataType: 'JSON',
        data: {
            full_amount: full_amount,
            interest_rate: interest_rate,
            term: term,
        },
        success: function (data) {
            const monthly_payment = JSON.parse(data);
            return monthly_payment;
        },
    });

    return amount;
};

$.fn.json_beautify = function () {
    var obj = JSON.parse(this.val());
    var pretty = JSON.stringify(obj, undefined, 4);
    this.val(pretty);
};

$(document).on('click', '.create_arnote', function () {
    let id = $(this).data('id');
    let modal = $('#create_ar_note_modal');
    let transactionIdInput = $('#transaction_id');

    // open modal
    $(modal).modal('show');
    $(transactionIdInput).val(id);
});

$('#createArNoteForm').on('submit', function (e) {
    e.preventDefault();

    let transaction_id = $('#transaction_id').val();
    let remarks = $('#remarks').val();

    $.ajax({
        url: base_url + 'ticketing/form/' + transaction_id,
        type: 'POST',
        dataType: 'JSON',
        data: {
            transaction_id: transaction_id,
            remarks: remarks,
            category: 'ar_notes',
        },
        success: function (response) {
            if (response.status) {
                swal.fire({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                }).then(function () {
                    window.location.replace(base_url + 'transaction');
                });
            } else {
                swal.fire('Oops!', response.message, 'error');
            }
        },
    });
});

// $('.update_transaction_stage').on('click', function () {
$(document).on('click', '.update_transaction_stage', function () {
    let id = $(this).data('id');
    let modal = $('#update_transaction_stage_modal');
    let transactionIdInput = $('#td_transaction_id');

    // open modal
    $(modal).modal('show');
    $(transactionIdInput).val(id);
});

$(document).on('click', '.update_transaction_loan_stage', function () {
    let id = $(this).data('id');
    let modal = $('#update_transaction_loan_stage_modal');
    let transactionIdInput = $('#td_transaction_id');

    // open modal
    $(modal).modal('show');
    $(transactionIdInput).val(id);
});

$(document).on('click', '.update_transaction_loan_documents', function () {
    let id = $(this).data('id');
    let modal = $('#update_transaction_loan_documents_modal');
    let transactionIdInput = $('#td_transaction_id');

    // open modal
    $(modal).modal('show');
    $(transactionIdInput).val(id);
});

// $('.add_personnel').on('click', function () {
$(document).on('click', '.add_personnel', function () {
    let id = $(this).data('id');
    let modal = $('#add_personnel_modal');
    let transactionIdInput = $('#ap_transaction_id');

    // open modal
    $(modal).modal('show');
    $(transactionIdInput).val(id);
});

$('#updateTransactionStageForm').on('submit', function (e) {
    e.preventDefault();

    let transaction_id = $('#td_transaction_id').val();
    let documentation_status = $('#td_documentation_status').val();

    $.ajax({
        url: base_url + 'transaction/update_document_stage/' + transaction_id,
        type: 'POST',
        dataType: 'JSON',
        data: {
            transaction_id: transaction_id,
            documentation_status: documentation_status,
        },
        success: function (response) {
            if (response.status) {
                swal.fire({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                }).then(function () {
                    window.location.replace(base_url + 'transaction');
                });
            } else {
                swal.fire('Oops!', response.message, 'error');
            }
        },
    });
});

$('#updateLoanTransactionStageForm').on('submit', function (e) {
    e.preventDefault();

    let transaction_id = $('#td_transaction_id').val();
    let stage_id = $('#td_transaction_loan_document_stage_id').val();

    $.ajax({
        url: base_url + 'transaction/update_loan_document_stage/' + transaction_id,
        type: 'POST',
        dataType: 'JSON',
        data: {
            transaction_id: transaction_id,
            transaction_loan_document_stage_id: stage_id,
        },
        success: function (response) {
            if (response.status) {
                swal.fire({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                }).then(function () {
                    window.location.replace(base_url + 'transaction');
                });
            } else {
                swal.fire('Oops!', response.message, 'error');
            }
        },
    });
});

$('#updateLoanTransactionCategoryForm').on('submit', function (e) {
    e.preventDefault();

    let transaction_id = $('#td_transaction_id').val();
    let transaction_loan_document_category_id = $('#td_transaction_loan_document_category_id').val();

    $.ajax({
        url: base_url + 'transaction/update_loan_documents/' + transaction_id,
        type: 'POST',
        dataType: 'JSON',
        data: {
            transaction_id: transaction_id,
            transaction_loan_document_category_id: transaction_loan_document_category_id,

        },
        success: function (response) {
            if (response.status) {
                swal.fire({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                }).then(function () {
                    window.location.replace(base_url + 'transaction?type=search&transaction_id=' + transaction_id);
                });
            } else {
                swal.fire('Oops!', response.message, 'error');
            }
        },
    });
});

$('#addPersonnelForm').on('submit', function (e) {
    e.preventDefault();

    let transaction_id = $('#ap_transaction_id').val();
    let assigned_at = $('#ap_assigned_at').val();
    let personnel_id = $('#ap_personnel_id').val();

    $.ajax({
        url: base_url + 'transaction/add_personnel/' + transaction_id,
        type: 'POST',
        dataType: 'JSON',
        data: {
            transaction_id: transaction_id,
            assigned_at: assigned_at,
            personnel_id: personnel_id,
        },
        success: function (response) {
            if (response.status) {
                swal.fire({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                }).then(function () {
                    window.location.replace(base_url + 'transaction');
                });
            } else {
                swal.fire('Oops!', response.message, 'error');
            }
        },
    });
});

function moment(data) {
    const d = new Date(data);
    const months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ];
    const month = months[d.getMonth()];
    const day = d.getDate();
    const year = d.getFullYear();

    return `${month} ${day}, ${year}`;
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}


// $("#view_ar_notes").on("click", function (e) {
//   let id = $(this).data("transactionid");
//   arNotesViewed(id);
// })

// Get all staff
const getStaff = () => {
    const personnelEl = document.querySelector('#ap_personnel_id');

    $.ajax({
        url: base_url + 'staff/getStaff',
        type: 'GET',
        success: function (data) {
            const personnels = JSON.parse(data);

            personnels.data.map((personnel) => {
                let opt = document.createElement('option');

                opt.value = personnel.id;
                opt.innerHTML = `${personnel.first_name} ${personnel.middle_name} ${personnel.last_name}`;
                personnelEl.appendChild(opt);
            });
        },
    });
};

getStaff();

$(document).on('change keyup', '#refund_percentage', function () {
    let id = $('#t_id').val();
    let percentage = $('#refund_percentage option:selected').val();
    $.ajax({
        url:
            base_url + 'transaction/get_refund_amount/' + id + '/' + percentage,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            $('#refund').val(data);
            $('#ra').val(data);
            getRefund();
        },
    });
});

$(document).on(
    'change keyup',
    '#processing_fee, #misc_fee, #penalty',
    function () {
        getRefund();
    }
);

function getRefund() {
    let total_payment = $('#tpm').val() ? $('#tpm').val() : 0;
    let refund_percentage = $('#refund_percentage').val()
        ? $('#refund_percentage').val()
        : 0;
    let reservation_fee = $('#reservation_fee').val()
        ? $('#reservation_fee').val()
        : 0;
    let processing_fee = $('#processing_fee').val()
        ? $('#processing_fee').val()
        : 0;
    let misc_fee = $('#misc_fee').val() ? $('#misc_fee').val() : 0;
    let penalty = $('#penalty').val() ? $('#penalty').val() : 0;
    let fees = 0;

    fees =
        (parseFloat(total_payment) - parseFloat(reservation_fee)) *
            parseFloat(refund_percentage) -
        (parseFloat(processing_fee) +
            parseFloat(misc_fee) +
            parseFloat(penalty));

    $('#refund').val(fees.toFixed(2));
}

$('#_upload_guide_btn').on('click', function () {
    var type = $('#_doc_type').val();
    $.ajax({
        url: base_url + 'transaction/update_table_fillables/' + type,

        dataType: 'JSON',
        success: function (response) {
            $('#batchuploadContent').html(response.html);
        },
    });
});

$('#_doc_type').on('change', function () {
    var type = $(this).val();

    var formEl = $('#_export_csv');

    $(formEl).attr('action', `${base_url}transaction/export_csv_${type}`);
});

function setRefundDetail() {
    var transaction_id = $('#t_id').val();
    var refund_percentage = $('#refund_percentage').val();
    var processing_fee = $('#processing_fee').val();
    var miscellaneous_fee = $('#misc_fee').val();
    var penalty = $('#penalty').val();

    var action_src =
        base_url +
        'transaction/print_refund/' +
        transaction_id +
        '/' +
        refund_percentage +
        '/' +
        processing_fee +
        '/' +
        miscellaneous_fee +
        '/' +
        penalty;
    var refund_form = document.getElementById('refund_form');
    refund_form.action = action_src;
}

$(document).on(
    'change',
    '#form_modal #promos_modal_form_repeater .promoID',
    function () {
        var total_selling_price = parseFloat(
            $('#form_modal #total_selling_price').val()
        );
        var total_contract_price = parseFloat(
            $('#form_modal #total_contract_price').val()
        );
        var collectible_price = parseFloat(
            $('#form_modal #collectible_price').val()
        );
        var loan_amount_deduction = parseFloat(
            $('#form_modal #loan_amount_deduction').val()
        );

        var promo_amount = 0;
        var tcp_discounts = [];
        var loan_deductions = [];
        var loan_sum = 0;
        var tcp = 0;

        $('.promoID')
            .find('option:selected')
            .each(function (index, element) {
                // Deduct to values
                // 1 = Deduct to Loan
                // 2 = Deduct to TCP (Total Collectible Price)
                var value = $(element).val();
                var amount = $(element)
                    .closest('[data-repeater-item]')
                    .find('.promoPrice')
                    .val();

                if (value == 1) {
                    loan_deductions.push(amount);
                } else if (value == 2) {
                    tcp_discounts.push(amount);
                }
            });

        if (loan_deductions.length != 0) {
            var loan_sum = loan_deductions.reduce(function (a, b) {
                return parseFloat(a) + parseFloat(b);
            });
        }

        if (tcp_discounts.length != 0) {
            var tcp = tcp_discounts.reduce(function (a, b) {
                return parseFloat(a) + parseFloat(b);
            });
        }

        total_contract_price =
            parseFloat(total_selling_price) - parseFloat(tcp);
        loan_amount_deduction = parseFloat(loan_sum);

        $('#form_modal #loan_amount_deduction').val(
            formatCurrency(loan_amount_deduction)
        );
        $('#form_modal #collectible_price').val(
            formatCurrency(total_contract_price)
        );
    }
);

var formRepeater = function () {
        
    $('#buyer_form_repeater').repeater({
        initEmpty: false,

        defaultValues: {},

        show: function () {
            $(this).slideDown(function(){
                $(this).find('.suggests_on_load_modal').select2({
                    minimumInputLength: 1,
                    dropdownParent: $(this),
                })
            });
        },

        hide: function (deleteElement) {
            if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        },

        ready: function (setIndexes) {
            $(this).find('.suggests_on_load_modal').select2({
                minimumInputLength: 1,
                dropdownParent: $(this),
            })
        }

    });

    $('#seller_form_repeater').repeater({
        initEmpty: false,

        defaultValues: {},

        show: function () {
            $(this).slideDown(function(){
                $(this).find('.suggests_on_load_modal').select2({
                    minimumInputLength: 1,
                    dropdownParent: $(this),
                })
            });
        },

        hide: function (deleteElement) {
            if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        },

        ready: function (setIndexes) {
            $(this).find('.suggests_on_load_modal').select2({
                minimumInputLength: 1,
                dropdownParent: $(this),
            })
        }
    });
};

var getCommValue = function () {
    var window_total_selling_price;
    var window_total_contract_price;
    var window_collectible_price;
    
    let total_selling_price = parseFloat(
        window_total_selling_price
    );
    let total_contract_price = parseFloat(
        window_total_contract_price
    );
    let collectible_price = parseFloat(
        window_collectible_price
    );
    let val = $('#commissionable_type_id').val();

    let amt = 0;
    let remarks = '';

    if (val == 1) {
        amt = total_selling_price;
        remarks = 'total selling price';
    } else if (val == 2) {
        amt = total_contract_price;
        remarks = 'total contract price';
    } else if (val == 3) {
        amt = collectible_price;
        remarks = 'collectible price';
    } else if (val == 4) {
        // fe = 60000 / 1.155;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 100000) / 1.155;
        remarks =
            'For H&L with TCP of 2,499,000 and below 5% or 4% of Expenses Exclusive TCP (Gross TCP less P100,000 fixed expenses divided by 1.155)';
    } else if (val == 5) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.225;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.225)';
    } else if (val == 6) {
        // fe = 60000 / 1.155;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.155;
        remarks =
            'For H&L with TCP of 2,499,000 and below 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.155)';
    } else if (val == 7) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.135;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.135)';
    } else if (val == 8) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 50000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.105)';
    } else if (val == 9) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 40000) / 1.155;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.155)';
    } else if (val == 10) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 40000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.105)';
    } else if (val == 11) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 35000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P35,000 fixed expenses divided by 1.105)';
    } else if (val == 12) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'House and Lot (Andrea, Cassandra. Elysa, Freya, Gabrielle)';
    } else if (val == 13) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.225;
        remarks =
            'Sitio Uno';
    } else if (val == 14) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 100000) / 1.155;
        remarks =
            'Twin Hearts Residences';
    } else if (val == 15) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'Lots Only (Except Socialized Lots)';
    } else if (val == 16) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'St. Joseph (Lots Only) **';
    }

    if (isNaN(amt)) {
        amt = 0;
    }

    $('#commissionable_amount').val(amt.toFixed(2));
    $('#commissionable_amount_remarks').val(remarks);
};

var sellerComp = function () {
    let commissionable_amount = parseFloat($('#commissionable_amount').val());

    if (isNaN(commissionable_amount)) {
        return true;
    }

    $('.sellers_rate').each(function (index, element) {
        let value = parseFloat($(element).val());

        let comm_amount = commissionable_amount * (value / 100);

        $(element)
            .closest('[data-repeater-item]')
            .find('.sellers_rate_amount')
            .val(comm_amount.toFixed(2));
    });
};

function initSellerForm() {

    getCommValue()
    sellerComp()

    $(document).on(
        'change',
        '#commissionable_type_id',
        function () {
            getCommValue();
        }
    );

    $(document).on('change', '.sellerComp', function () {
        sellerComp();
    });

    $(document).on('keyup', '.sellerComp', function () {
        sellerComp();
    });
}

var getCommValueCommission = function () {
    let total_selling_price = parseFloat(
        window_total_selling_price_commission
    );
    let total_contract_price = parseFloat(
        window_total_contract_price_commission
    );
    let collectible_price = parseFloat(
        window_collectible_price_commission
    );
    let val = $('#change_commissionable_type_id').val();
    
    console.log(window_total_selling_price_commission);
    console.log(window_total_contract_price_commission);
    console.log(window_collectible_price_commission);

    let amt = 0;
    let remarks = '';

    if (val == 1) {
        amt = total_selling_price;
        remarks = 'total selling price';
    } else if (val == 2) {
        amt = total_contract_price;
        remarks = 'total contract price';
    } else if (val == 3) {
        amt = collectible_price;
        remarks = 'collectible price';
    } else if (val == 4) {
        // fe = 60000 / 1.155;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 100000) / 1.155;
        remarks =
            'For H&L with TCP of 2,499,000 and below 5% or 4% of Expenses Exclusive TCP (Gross TCP less P100,000 fixed expenses divided by 1.155)';
    } else if (val == 5) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.225;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.225)';
    } else if (val == 6) {
        // fe = 60000 / 1.155;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.155;
        remarks =
            'For H&L with TCP of 2,499,000 and below 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.155)';
    } else if (val == 7) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.135;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.135)';
    } else if (val == 8) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 50000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.105)';
    } else if (val == 9) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 40000) / 1.155;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.155)';
    } else if (val == 10) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 40000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.105)';
    } else if (val == 11) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 35000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P35,000 fixed expenses divided by 1.105)';
    } else if (val == 12) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'House and Lot (Andrea, Cassandra. Elysa, Freya, Gabrielle)';
    } else if (val == 13) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.225;
        remarks =
            'Sitio Uno';
    } else if (val == 14) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 100000) / 1.155;
        remarks =
            'Twin Hearts Residences';
    } else if (val == 15) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'Lots Only (Except Socialized Lots)';
    } else if (val == 16) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'St. Joseph (Lots Only) **';
    }

    if (isNaN(amt)) {
        amt = 0;
    }

    $('#change_commissionable_amount').val(amt.toFixed(2));
    $('#change_commissionable_amount_remarks').val(remarks);
};

function initChangeCommissionSetup() {

    getCommValueCommission()

    $(document).on(
        'change',
        '#change_commissionable_type_id',
        function () {
            getCommValueCommission();
        }
    );
}

$(document).on('click', '.sync_aris_payment', function () {
    let project_id = $(this).data('id');
    let document_id = $(this).data('documentid');
    swal.fire({
        title: 'Are you sure?',
        text: "This will sync latest payments from ARIS!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, sync it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true,
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: base_url + 'transaction_payment/import_new_aris_payments/1/' + project_id + '/' + document_id,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    project_id: project_id,
                },
                success: function (res) {
                    if (res.status) {
                        swal.fire('Synced!', res.message, 'success');
                    } else {
                        swal.fire('Oops!', res.message, 'error');
                    }
                },
            });
        } else if (result.dismiss === 'cancel') {
            swal.fire(
                'Cancelled',
                'Cancelled Syncing',
                'error'
            );
        }
    });
});

$(document).on('click', '#_export_form_btn', function () {

    var exportColumns = $('#_export_form').serialize();
    var advanceSearch = $('#advanceSearch').serialize();
    KTApp.block('#_export_option', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Creating Excel File...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'transaction/export',
        xhrFields:{
            responseType: 'blob'
        },
        type : "POST",
        data : exportColumns+'&'+advanceSearch,
        success : function(result) {
            KTApp.unblock('#_export_option');
            var blob = result;
            var downloadUrl = URL.createObjectURL(blob);
            var a = document.createElement("a");
            a.href = downloadUrl;
            a.download = "List of Transactions.xls";
            document.body.appendChild(a);
            a.click();
        },
    })
});