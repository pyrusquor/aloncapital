"use strict";
var LetterRequest = (function () {
    var LetterRequestTable = function () {
        var table = $("#letter_request_table");

        // begin first table
        table.DataTable({
            order: [
                [1, "desc"]
            ],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,

            // ajax: base_url + "transaction_letter_request/showLetterRequests",
            ajax: {
                url:base_url + 'transaction_letter_request/showLetterRequests',
                data: function(data){
                   // Read values
                   var request_date = $('#kt_tlr_daterangepicker').val();
         
                   // Append to data
                   data.request_date = request_date;
                }
             },
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },

            columns: [
                {
                    data: null,
                },
                {
                    data: "id",
                },
                {
                    data: "transaction",
                },
                {
                    data: "project"
                },
                {
                    data: "property"
                },
                {
                    data: "buyer"
                },
                {
                    data: "request_date",
                },
                {
                    data: "subject",
                },
                {
                    data: "creator"
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: "Actions",
                    responsivePriority: -1,
                },
            ],
            columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
                            <div class="d-flex">
                            <a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon btn-icon-md approve_letter_request" title="Approve Request" data-id="` +
                            row.id +
                            `">
								<i class="la la-check"></i>
								</a>
								<a href="` +
                            base_url +
                            `transaction_letter_request/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>
                                <a target="_blank" href="` +
                            base_url +
                            `transaction_letter_request/generate/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Print">
								<i class="la la-print"></i>
								</a>
                                <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md remove_letter_request" data-id="` +
                            row.id +
                            `" title="Delete"><i class="la la-trash"></i></a>
                                </div>
                                `
                        );
                    },
                },
                {
                    targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, -1],
                    className: "dt-center",
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                {
                    targets: [6],
                    render: function (data, type, full, meta) {
                        const d = new Date(data);
                        if (Object.prototype.toString.call(d) === "[object Date]") {
                            // it is a date
                            if (isNaN(d.getTime())) { // d.valueOf() could also work
                                // date is not valid
                                return ``;
                            } else {
                                // date is valid

                                const months = [
                                    "January",
                                    "February",
                                    "March",
                                    "April",
                                    "May",
                                    "June",
                                    "July",
                                    "August",
                                    "September",
                                    "October",
                                    "November",
                                    "December",
                                ];
                                const month = months[d.getMonth()];
                                const day = d.getDate();
                                const year = d.getFullYear();

                                return `${month} ${day}, ${year}`;
                            }
                        } else {
                            // not a date
                            return ``;
                        }
                    },
                },
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            },
        });

        var oTable = table.DataTable();
        $("#generalSearch").keyup(function () {
            oTable.search($(this).val()).draw();
        });
    };

    var _add_ons = function () {
        var dTable = $("#letter_request_table").DataTable();

        $("#_search").on("keyup", function () {
            dTable.search($(this).val()).draw();
        });

        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    var confirmApprove = function () {
        $(document).on("click", ".approve_letter_request", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Approve Request!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $("#requestApproveModal").modal("toggle");
                    const form = document.querySelector("#requestApproveForm");
                    form.action = `${base_url}transaction_letter_request/approve_request/${id}`;

                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_letter_request', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'transaction_letter_request/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#letter_request_table').DataTable().ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var _selectProp = function () {
        var _table = $('#letter_request_table').DataTable();

        $('#select-all').on('click', function () {
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
                _table.button(0).enable();
            } else {
                $('.delete_check').prop('checked', false);
                _table.button(0).disable();
            }
        });

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'transaction_letter_request/bulkDelete',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#letter_request_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    var daterangepickerInit = function () {
        if ($('#kt_tlr_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_tlr_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                // range = start.format("MMM D") + " - " + end.format("MMM D");
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                },
                opens: 'left',
                // ranges: {
                // 	'Today': [moment(), moment()],
                // 	'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                // 	'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                // 	'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                // 	'This Month': [moment().startOf('month'), moment().endOf('month')],
                // 	'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                // }
            },
            cb
        );

        cb(start, end, '');

        $(picker).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(
                picker.startDate.format('MM/DD/YYYY') +
                    ' - ' +
                    picker.endDate.format('MM/DD/YYYY')
            );
            var table = $("#letter_request_table");
            var oTable = table.DataTable();
            oTable.draw();
        });

        $(picker).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            var table = $("#letter_request_table");
            var oTable = table.DataTable();
            oTable.draw();
        });
    };

    var load_filters = function(){
        var paid_date = localStorage.getItem('general_tlr_date');
        if(paid_date!==null){
            $('#kt_tlr_daterangepicker').val(paid_date);
            var table = $("#letter_request_table");
            var oTable = table.DataTable();
            oTable.draw();
        }
        localStorage.removeItem('general_tlr_date');

    }

    return {
        //main function to initiate the module
        init: function () {
            LetterRequestTable();
            _add_ons();
            status();
            confirmApprove()
            confirmDelete()
            _selectProp()
            daterangepickerInit();
            load_filters()
        },
    };
})();

jQuery(document).ready(function () {
    LetterRequest.init();
});