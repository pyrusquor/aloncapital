"use strict";
var KTDatatablesChecklist = function () {

	var _initChecklist = function () {

		var _table = $('#_checklist_table');

		_table.DataTable({
			order: [[2, 'desc']],
			pagingType: 'full_numbers',
			lengthMenu: [5, 10, 25, 50, 100],
			pageLength : 10,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			deferRender: true,
			dom:	"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
						// "<'row'<'col-sm-12 col-md-6'l>>" +
						// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
						"<'row'<'col-sm-12'tr>>" +
						"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			buttons: [
				{
					text: '<i class="fa fa-trash"></i> Delete Selected',
					className: 'btn btn-sm btn-label-primary btn-elevate btn-icon-sm _bulk_delete',
					attr: {
						id: 'bulkDelete'
					},
					enabled: false
				}
			],
			language: {
				'lengthMenu': 'Show _MENU_',
    			'infoFiltered': '(filtered from _MAX_ total records)'
			},
			ajax: base_url + 'stages/get_checklists',
			columns: [
				{
					data: null,
					orderable: false,
					searchable: false,
					render: function ( data, type, row, meta ) {

						return `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="`+row.id+`" class="m-checkable __select" data-id="`+row.id+`">
                      <span></span>
	                  </label>`;
					}
				},
				{data: 'category_id'},
				{data: 'id'},
				{data: 'name'},
				{data: 'description'},
				{data: 'number_of_documents'},
				{data: 'updated_at'},
				{data: 'Actions', responsivePriority: -1}
			],
			columnDefs: [
				{
					targets: -1,
					orderable: false,
					render: function ( data, type, row, meta ) {

						return `
                    <span class="dropdown">
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                          <i class="la la-ellipsis-h"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="`+ base_url +`stages/update/`+row.id+`" class="dropdown-item"><i class="la la-edit"></i> Update</a>
                            <a href="javascript:void(0);" class="dropdown-item _delete_list" data-id="`+row.id+`"><i class="la la-trash"></i> Delete</a>
                        </div>
                    </span>
                    <a href="`+base_url+`stages/stages/`+row.id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                      <i class="la la-eye"></i>
                    </a>
                  `;
					}
				},
				{
					targets: [0, 2, 4, 5, -1],
					className: 'dt-center',
				},
				{
					targets: 1,
					searchable: true,
					visible: false
				}
			],
			drawCallback: function ( settings ) {

				$('span#_total').text(settings.fnRecordsTotal()+' TOTAL');
			}
		});
	}

	var _add_ons = function () {

		var dTable = $('#_checklist_table').DataTable();

		$('#_search').on( 'keyup', function () {

			dTable.search($(this).val()).draw();
		});

		$('#kt_datepicker').datepicker({
    	orientation: "bottom left",
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

		$('button#_advance_search_btn').on( 'click', function () {

			$('div#_batch_upload').removeClass('show');

			// var _search = $('#_checklist_table_filter').css('display');

			// if ( _search && (_search === 'block') ) {

			// 	$('#_checklist_table_filter').css('display', 'none');
			// } else {

			// 	$('#_checklist_table_filter').css('display', 'block');
			// }
		});

		$('button#_batch_upload_btn').on( 'click', function () {

			$('div#_advance_search').removeClass('show');
		});

		$(document).on( 'click', '._delete_list', function () {

			var id = $(this).data('id');

			swal.fire({
				title: 'Are you sure?',
				text: 'You won\'t be able to revert this!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then( function ( result ) {

				if ( result.value ) {

					$.ajax({
						type: 'post',
						dataType: 'json',
						url: base_url+'stages/delete',
						data: { id: id },
						success: function ( _response ) {

							if ( _response._status ) {

								$('#_checklist_table').DataTable().ajax.reload();

								// swal.fire(
								// 	'Deleted!',
								// 	_response._msg,
								// 	'success'
								// );

								toastr.options = {
								  "closeButton": true,
								  "debug": false,
								  "newestOnTop": false,
								  "progressBar": false,
								  "positionClass": "toast-top-right",
								  "preventDuplicates": false,
								  "onclick": null,
								  "showDuration": "300",
								  "hideDuration": "1000",
								  "timeOut": "5000",
								  "extendedTimeOut": "1000",
								  "showEasing": "swing",
								  "hideEasing": "linear",
								  "showMethod": "fadeIn",
								  "hideMethod": "fadeOut"
								};

								toastr.success(_response._msg, "Deleted");
							} else {

								toastr.options = {
								  "closeButton": true,
								  "debug": false,
								  "newestOnTop": false,
								  "progressBar": false,
								  "positionClass": "toast-top-right",
								  "preventDuplicates": false,
								  "onclick": null,
								  "showDuration": "300",
								  "hideDuration": "1000",
								  "timeOut": "5000",
								  "extendedTimeOut": "1000",
								  "showEasing": "swing",
								  "hideEasing": "linear",
								  "showMethod": "fadeIn",
								  "hideMethod": "fadeOut"
								};

								toastr.error(_response._msg, "Oops!");

								// swal.fire(
								// 	'Oops!',
								// 	_response._msg,
								// 	'error'
								// );
							}
						}
					});
				} else if ( result.dismiss === 'cancel' ) {

					swal.fire(
						'Cancelled',
						'Your imaginary file is safe.',
						'error'
					);
				}
			});
		});

		$('.modal').on('hidden.bs.modal', function(){
	    $(this).find('form')[0].reset();

	    $('form').find('input, select').removeClass('is-valid').removeClass('is-invalid');

	    $(this).find('form').trigger('reset');

	    $('#form_msg').closest('div.form-group').addClass('kt-hide');
		});
	}

	var _filterChecklist = function () {

		var dTable = $('#_checklist_table').DataTable();

		function _colFilter ( n ) {

			dTable.column(n).search($('#_column_'+n).val()).draw();
		}

		$('._filter').on( 'keyup change clear', function () {

			_colFilter($(this).data('column'));
		});
	}

	var _selectProp = function () {

		var _table    = $('#_checklist_table').DataTable();
		var _buttons  = _table.buttons( ['._bulk_delete'] );
		// var _buttons  = _table.buttons( ['._consolidate', '._subdivide'] );

		$('input#select-all').on( 'click', function () {

			if ( this.checked ) {

				$('input.__select').each( function () {

					this.checked = true;
				});
			} else {

				$('input.__select').each( function () {

					this.checked = false;
				});
			}

			var _checked = $('input.__select:checked').length;

			if ( _checked === 0  ) {

				_buttons.disable();
			} else {

				_table.button(0).enable(_checked > 0);
			}
		});

		$(document).on( 'change', 'input.__select', function () {

			var _checked = $('input.__select:checked').length;

			if ( _checked === 0  ) {

				_buttons.disable();
			} else {

				_table.button(0).enable(_checked > 0);
			}

			if ( this.checked == false ) {

				$('input#select-all').prop('checked', false);
			}
		});

		$('#bulkDelete').click(function(){

			var deleteids_arr = [];
			// Read all checked checkboxes
			$('input[name="id[]"]:checked').each(function () {
			   deleteids_arr.push($(this).val());
			});
	  
			// Check checkbox checked or not
			if(deleteids_arr.length > 0){
	  
				swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Yes, delete it!',
					cancelButtonText: 'No, cancel!',
					reverseButtons: true
				}).then(function (result) {
					if (result.value) {

						$.ajax({
							url: base_url + 'stages/bulkDelete/',
							type: 'POST',
							dataType: "JSON",
							data: { deleteids_arr: deleteids_arr },
							success: function (res) {
								if (res.status) {
									$('#_checklist_table').DataTable().ajax.reload();
									swal.fire(
										'Deleted!',
										res.message,
										'success'
									);
								} else {
									swal.fire(
										'Oops!',
										res.message,
										'error'
									)
								}
							}
						});

						$('input#select-all').prop('checked', false);

					} else if (result.dismiss === 'cancel') {
						swal.fire(
							'Cancelled',
							'Your imaginary file is safe :)',
							'error'
						)
					}
				});
			}
		});
	}

	return {

		init: function () {

			_initChecklist();
			_add_ons();
			_filterChecklist();
			_selectProp();
		}
	};
}();

jQuery(document).ready(function() {

    KTDatatablesChecklist.init();
});