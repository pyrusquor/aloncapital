
var Amend = function (){

    var formEl = $('#form_amend_payment_request');

    var entryFormRepeater = function () {
        $("#entry_item_form_repeater").repeater({
            initEmpty: false,
            defaultValues: {},
    
            show: function () {
                $(this).slideDown();
                // datepicker();
                // getLedgers();
                paymentType();
                calculateDTotal();
                calculateCTotal();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width','100%');
            },
    
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
        });
    }
    var datepicker = function () {
        // minimum setup
        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on("click", function (e) {
            e.preventDefault();
            let dTotalInput = $("#dTotal").val();
            let cTotalInput = $("#cTotal").val();
            let company_id = $("#company_id").val();
            dTotalInput = parseFloat(dTotalInput.replace(/,/g, ''))
            cTotalInput = parseFloat(cTotalInput.replace(/,/g, ''))

            if (dTotalInput == cTotalInput) {
                if(company_id){
                        // See: src\js\framework\base\app.js

                        KTApp.progress(btn);
                        //KTApp.block(formEl);

                        // See: http://malsup.com/jquery/form/#ajaxSubmit
                        formEl.ajaxSubmit({
                            type: "POST",
                            dataType: "JSON",
                            success: function (response) {
                                if (response.status) {
                                    swal.fire({
                                        title: "Success!",
                                        text: response.message,
                                        type: "success",
                                    }).then(function () {
                                        window.location.replace(
                                            base_url + "payment_request"
                                        );
                                    });
                                } else {
                                    swal.fire({
                                        title: "Oops!",
                                        html: response.message,
                                        icon: "error",
                                    });
                                }
                            },
                        });
                    } else {
                        swal.fire({
                            title: "Error!",
                            html: "Please make sure that a company is selected.",
                            icon: "error",
                        });
                    }
            } else {
                swal.fire({
                    title: "Error!",
                    html: "Please make sure that Debit and credit amount is balanced.",
                    icon: "error",
                });
            }
        });
    };
    return {
        init: function (){
            initSubmit();
            entryFormRepeater();
            datepicker();
        }
    }
}();
jQuery(document).ready(function () {
    Amend.init();
});
const getLedgers = () => {
    // Get the element again
    const entryItemLedgerSelect = document.querySelectorAll(
        ".entry_item_ledger"
    );
    // Loop through the element
    entryItemLedgerSelect.forEach((select) => {
        $.ajax({
            url: base_url + "accounting_entries/get_all_ledgers",
            type: "GET",
            data:{
                company_id : $('#company_id').val()
            },
            success: function (data) {
                const ledgers = JSON.parse(data);
                console.log(ledgers);
                ledgers.data.map((ledger) => {
                    let opt = document.createElement("option");

                    opt.value = ledger.id;
                    opt.innerHTML = ledger.name;
                    select.appendChild(opt);
                });
            },
        });
    });
};
const paymentType = () => {
    const entryItemDcSelect = document.querySelectorAll("#entry_item_dc");
    const dInput = document.querySelectorAll("#dr_amount_input");
    const cInput = document.querySelectorAll("#cr_amount_input");

    entryItemDcSelect.forEach((select, index) => {
        $(select).on("change", function () {
            if (this.value !== "") {
                if (this.value == "d") {
                    cInput[index].disabled = true;
                    cInput[index].value = 0;
                    dInput[index].disabled = false;
                } else if (this.value == "c") {
                    cInput[index].disabled = false;
                    dInput[index].disabled = true;
                    dInput[index].value = 0;
                }
            }
        });
    });
};
const dTotalInput = document.querySelector("#dTotal");
const cTotalInput = document.querySelector("#cTotal");
const calculateDTotal = () => {
    const dInput = document.querySelectorAll("#dr_amount_input");
    // dTotalInput.value = 0;
    $(dInput).keyup(function () {
        var total = 0;

        $(dInput).each(function () {
            total += parseFloat($(this).val().trim() || 0);
        });
        dTotalInput.value = total.toFixed(2);
    });
};

const calculateCTotal = () => {
    const cInput = document.querySelectorAll("#cr_amount_input");
    // cTotalInput.value = 0;
    $(cInput).keyup(function () {
        var total = 0;

        $(cInput).each(function () {
            total += parseFloat($(this).val().trim() || 0);
        });
        cTotalInput.value = total.toFixed(2);
    });
};
calculateCTotal();
calculateDTotal();
paymentType();
getLedgers();