'use strict';
var PaymentRequest = (function () {

    let tableElement = $('#paymentrequest_table');

    var paymentrequestTable = function () {
        
        var dataTable = tableElement.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
             ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'payment_request/showItems',
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'payee',
                },
                {
                    data: 'payable_type_id',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'due_date',
                },
                {
                    data: 'date_requested',
                },
                {
                    data: 'gross_amount',
                },
                {
                    data: 'tax_amount',
                },
                {
                    data: 'paid_amount',
                },
                {
                    data: 'remaining_amount',
                },
                {
                    data: 'is_paid',
                },
                {
                    data: 'is_complete',
                },
                {
                    data: 'particulars',
                },
                {
                    data: 'status',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var actions = '';
                        var update = '';
                        if(row.status=='Encoded'){
                            actions = actions + `<a class="dropdown-item pr_action" href="javascript:void(0);"
                             data-id=`+ row.id+` data-action='1' data-action-name='Approve' rel="noopener noreferrer"><i class="fa fa-check"></i> Approve </a>`
                            actions = actions + `<a class="dropdown-item pr_action" href="javascript:void(0);"
                             data-id=`+ row.id+` data-action='2' data-action-name='Disapprove' rel="noopener noreferrer"><i class="fa fa-window-close"></i> Disapprove </a>`
                            actions = actions + '<div class="dropdown-divider"></div>'
                        } else if(row.status=='Approved'){
                            actions = actions + `<a class="dropdown-item pr_action" href="javascript:void(0);"
                            data-id=`+ row.id+` data-action='3' data-action-name='Post' rel="noopener noreferrer"><i class="fa fa-paperclip"></i> Post </a>`
                            actions = actions + `<a class="dropdown-item pr_action" href="javascript:void(0);"
                            data-id=`+ row.id+` data-action='2' data-action-name='Disapprove' rel="noopener noreferrer"><i class="fa fa-window-close"></i> Disapprove </a>`
                            actions = actions + `<a class="dropdown-item" href="` + base_url +  `payment_request/amend/` + row.id + `" 
                            data-id=`+ row.id+` data-action='1' data-action-name='Disapprove' rel="noopener noreferrer"><i class="fa fa-plus-circle"></i> Amend Request </a>`
                           actions = actions + '<div class="dropdown-divider"></div>'
                        } else if(row.status=='Disapproved'){
                            actions = actions + `<a class="dropdown-item pr_action" href="javascript:void(0);"
                            data-id=`+ row.id+` data-action='1' data-action-name='Approve' rel="noopener noreferrer"><i class="fa fa-check"></i> Approve </a>`
                            actions = actions + '<div class="dropdown-divider"></div>'
                        } else if(row.status=='Posted'){
                            actions = actions + `<a class="dropdown-item" href="` + base_url + `payment_voucher/form/` + row.id +
                            `" target="_blank" rel="noopener noreferrer"><i class="flaticon2-add"></i> Add Voucher </a>`
                            actions = actions + '<div class="dropdown-divider"></div>'
                            update =`<a class="dropdown-item" href="` + base_url + `payment_request/form/` + row.id + `"><i class="la la-edit"></i> Update </a>`
                        }
                        return (
                            `
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right"> `+ actions+`
                                     <a class="dropdown-item" href="` +
                                base_url +
                                `accounting_entries/view/` +
                                row.accounting_entry_id +
                                `" target="_blank" rel="noopener noreferrer"><i class="flaticon2-eye"></i> View Entry </a>
                                    <a class="dropdown-item" href="` +
                            base_url +
                            `payment_request/printable/` +
                            row.id +
                            `" target="_blank" rel="noopener noreferrer"><i class="la la-print"></i> Print </a>` + update + `
										<a href="javascript:void(0);" class="dropdown-item remove_paymentrequest" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete </a>
									</div>
								</span>
								<a href="` +
                            base_url +
                            `payment_request/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        );
                    },
                },
               
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var extra_filters = function () {
        var table = $('#paymentrequest_table');
        var oTable = table.DataTable();
        $('#pr_project_id').on('change', function(){
            oTable.draw();
        })


    }

    var confirmDelete = function () {
        $(document).on('click', '.remove_paymentrequest', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'payment_request/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#paymentrequest_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on('click', '#btn_upload_guide', function () {
            var table = $('#upload_guide_table');

            table.DataTable({
                order: [[0, 'asc']],
                pagingType: 'full_numbers',
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + 'company/get_table_schema',
                columns: [
                    // {data: 'button'},
                    {
                        data: 'no',
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'type',
                    },
                    {
                        data: 'format',
                    },
                    {
                        data: 'option',
                    },
                    {
                        data: 'required',
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear apply.daterangepicker cancel.daterangepicker', function () {
            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }

        $('#payee_type_select').on('change', () => {
            $('#payee_id_select').attr('data-module', $('#payee_type_select').val())
        })
    };

    var _selectProp = function () {
        var _table = $('#property_table').DataTable();
        var _buttons = _table.buttons(['.bulkDelete']);

        $('#select-all').on('click', function () {
            // var rows = _table.rows({ 'search': 'applied' }).nodes();

            // $('input[type="checkbox"]').prop( 'checked', this.checked );
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
            } else {
                $('.delete_check').prop('checked', false);
            }
        });

        $('#property_table tbody').on(
            'change',
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $('input#select-all').get(0);

                    if (el && el.checked && 'indeterminate' in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'property/bulkDelete/',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#property_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    var _add_ons = function () {
        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var daterangepickerInit = function () {
        if ($('.kt_pr_daterangepicker').length == 0) {
            return;
        }

        var picker = $('.kt_pr_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                // range = start.format("MMM D") + " - " + end.format("MMM D");
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                },
                opens: 'left',
            },
            cb
        );

        cb(start, end, '');

        $(picker).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(
                picker.startDate.format('YYYY-MM-DD') +
                    ' - ' +
                    picker.endDate.format('YYYY-MM-DD')
            );
        });

        $(picker).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
    }; 

    var change_status = function (){
        $(document).on('click', '.pr_action' ,function(){
            var id = $(this).data('id');
            var action = $(this).data('action');
            var action_name = $(this).data('action-name');
            console.log(action_name);
            swal.fire({
                title: action_name + ' payment request?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'payment_request/change_status/',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                            status: action,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#paymentrequest_table')
                                .DataTable()
                                .ajax.reload();
                                swal.fire('Success!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Payment Request Cancelled!',
                        'error'
                    );
                }
            });
        })
    }

    var load_filters = function(){
        var date_range = localStorage.getItem('accounting_date');
        var project_id = localStorage.getItem('accounting_project_id');
        var project_name = localStorage.getItem('accounting_project_name');
        var table = $('#paymentrequest_table');
        var oTable = table.DataTable();

        if(date_range!==null){
            $('.kt_pr_daterangepicker').val(date_range);
            $('#pr_project_id').append(new Option(project_name, project_id,false,true));
            oTable.draw();
        }

        localStorage.removeItem('accounting_date');
        localStorage.removeItem('accounting_project_id');
        localStorage.removeItem('accounting_project_name');
    }

    var datepicker = function () {
        // minimum setup
        $(".datepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            paymentrequestTable();
            filter();
            _selectProp();
            _add_ons();
            upload_guide();
            status();
            extra_filters();
            daterangepickerInit();
            load_filters();
            change_status();
            datepicker()
        },
    };
})();

jQuery(document).ready(function () {
    PaymentRequest.init();
});
