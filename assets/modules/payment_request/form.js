// Class definition
var Create = function () {
    var formRepeater = function () {
        $("#form_payment_request").repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                datepicker();
                listItemDetail();
                calculateItemTotalPrice();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
        });
    };



    var entryFormRepeater = function () {
        $("#entry_item_form_repeater").repeater({
            initEmpty: false,
            defaultValues: {},

            show: function () {
                $(this).slideDown();
                datepicker();
                getLedgers();
                paymentType();
                calculateDTotal();
                calculateCTotal();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width','100%');
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
        });
    };

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
    };

    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var _add_ons;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        wizard.on('beforePrev', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    };

    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                "request[payable_type_id]": {
                    required: true
                },
                "request[payee_type]": {
                    required: true
                },
                "request[payee_type_id]": {
                    required: true
                },
                "request[date_requested]": {
                    required: true,
                },
                "request[due_date]": {
                    required: true,
                },
                "request[approving_department_id]": {
                    required: true,
                },
                "request[approving_staff_id]": {
                    required: true,
                },
                "request[requesting_department_id]": {
                    required: true,
                },
                "request[requesting_staff_id]": {
                    required: true,
                },
                "request[prepared_by]": {
                    required: true,
                },
                "items[0][item_id]": {
                    required: true,
                },
                "items[0][unit_price]": {
                    required: true,
                },
                "items[0][name]": {
                    required: true,
                },
                "items[0][tax_amount]": {
                    required: true,
                },
                "items[0][total_amount]": {
                    required: true,
                },
                "request[gross_amount]": {
                    required: true,
                    number: true,
                },
                "entry_item[0][dc]": {
                    required: true,
                },
                "entry_item[0][ledger_id]": {
                    required: true,
                },
                "entry_item[0][amount]": {
                    required: true,
                },
            },
            // messages: {
            // 	"info[last_name]":'Last name field is required',
            // },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {

            }
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on("click", function (e) {
            e.preventDefault();
            let paidAmount = $("#total_due_amount").val();
            let dTotalInput = $("#dTotal").val();
            let cTotalInput = $("#cTotal").val();
            paidAmount = parseFloat(paidAmount.replace(/,/g, ''))
            dTotalInput = parseFloat(dTotalInput.replace(/,/g, ''))
            cTotalInput = parseFloat(cTotalInput.replace(/,/g, ''))

            if (dTotalInput == cTotalInput) {
                if (paidAmount == dTotalInput || paidAmount == cTotalInput) {
                    if (validator.form()) {
                        // See: src\js\framework\base\app.js

                        KTApp.progress(btn);
                        //KTApp.block(formEl);

                        // See: http://malsup.com/jquery/form/#ajaxSubmit
                        formEl.ajaxSubmit({
                            type: "POST",
                            dataType: "JSON",
                            success: function (response) {
                                if (response.status) {
                                    swal.fire({
                                        title: "Success!",
                                        text: response.message,
                                        type: "success",
                                    }).then(function () {
                                        window.location.replace(
                                            base_url + "payment_request"
                                        );
                                    });
                                } else {
                                    swal.fire({
                                        title: "Oops!",
                                        html: response.message,
                                        icon: "error",
                                    });
                                }
                            },
                        });
                    }
                } else {
                    swal.fire({
                        title: "Error!",
                        html: "The paid amount should be equal to the Debit or Credit total.",
                        icon: "error",
                    });
                }
            } else {
                swal.fire({
                    title: "Error!",
                    html: "Please make sure that Debit and credit amount is balanced.",
                    icon: "error",
                });
            }
        });
    };

    var _add_ons = function () {
        datepicker();
    };

    // For multiple origin processing ----->

    var check_if_multiple = function(){
        var commission_gross_sum = localStorage.getItem('commission_gross_sum');
        var commission_net_sum = localStorage.getItem('commission_net_sum');
        var commission_ids = localStorage.getItem('commission_ids');
        var seller_id = localStorage.getItem('seller_id');
        var seller_name = localStorage.getItem('seller_name');
        var commission_projects = localStorage.getItem('commission_projects');
        var commission_properties = localStorage.getItem('commission_properties');
        var basic = localStorage.getItem('basic');
        if((commission_gross_sum && commission_net_sum && commission_ids) || basic){
            process_multiple_commmission_origin(commission_ids, Number(commission_gross_sum), Number(commission_net_sum), seller_id, seller_name, commission_projects, commission_properties);
        }
        console.log(commission_gross_sum, commission_net_sum);
        localStorage.removeItem('commission_gross_sum');
        localStorage.removeItem('commission_net_sum');
        localStorage.removeItem('commission_ids');
        localStorage.removeItem('seller_id');
        localStorage.removeItem('seller_name');
        localStorage.removeItem('commission_properties');
        localStorage.removeItem('commission_projects');
        localStorage.removeItem('basic');
    }
    
    var process_multiple_commmission_origin = function ( ids, gross_sum, net_sum, seller_id, seller_name,projects,properties){
        var payable = $('#payable_type_id');
        var payable_option = new Option('Commission',1);
        payable.append(payable_option);
        payable.val('1').trigger('change');

        var payee_type = $('#payee_type');
        var land_inventory = $('#land_inventory_id');
        payee_type.val('sellers');
        land_inventory.attr('disabled',true);

        var payee_type_id = $('#payee_type_id');
        var payee_type_id_option = new Option(seller_name, seller_id);
        payee_type_id.append(payee_type_id_option);
        payee_type_id.val(seller_id).trigger('change');

        $.ajax({
            url: base_url + 'generic_api/fetch_specific',
            dataType: 'JSON',
            data: {
                table: 'departments',
                field: 'id',
                value: '2',
                limit: '1',
                select: 'name'
            },
            success: function (res){
                var approving_department = $('#approving_department_id');
                var approving_department_option = new Option(res[0]['name'], '2');
                approving_department.append(approving_department_option);
                approving_department.val('2').trigger('change');
            }
        })

        $.ajax({
            url: base_url + 'generic_api/fetch_specific',
            dataType: 'JSON',
            data: {
                table: 'departments',
                field: 'id',
                value: '1',
                limit: '1',
                select: 'name'
            },
            success: function (res){
                var requesting_department = $('#requesting_department_id');
                var requesting_department_option = new Option(res[0]['name'], '1');
                requesting_department.append(requesting_department_option);
                requesting_department.val('1').trigger('change');
            }
        })

        var wht_amount = gross_sum - net_sum;
        $('#origin_id').val(ids);
        $('#gross_amount').val(gross_sum.toFixed(2));
        $('#total_due_amount').val(net_sum.toFixed(2));
        $('#wht_amount').val(wht_amount.toFixed(2));
        $('#wht_percentage').val('5.00');
        $('#particulars').val('Commissions');
        add_options(projects,'projects','#project_id');
        add_options(properties,'properties','#property_id');
    }

    var add_options = function (data, type, identifier){
        var element = $(identifier);
        var array_data = data.split(',');
        var array_length = array_data.length;
        for (let i=0; i<array_length; i++){
            $.ajax({
                url: base_url + 'generic_api/fetch_specific',
                dataType: 'JSON',
                data: {
                    table: type,
                    field: 'id',
                    value: array_data[i],
                    limit: '1',
                    select: 'name'
                },
                async:false,
                success: function (res){
                    var element_option = new Option(res[0]['name'], array_data[i]);
                    element.append(element_option);
                }
            })
        }
        element.val(null).trigger('change');
        element.val(array_data).trigger('change');

    }

    //<---------

    // Public functions
    return {
        init: function () {
            datepicker();
            // formRepeater();
            entryFormRepeater();
            _add_ons();

            wizardEl = KTUtil.get('kt_wizard_v3');
            formEl = $('#form_payment_request');

            initWizard();
            initValidation();
            initSubmit();

            //check if this page comes from commission view_seller
            check_if_multiple();

        }
    };
}();

// Initialization
jQuery(document).ready(function () {
    Create.init();
});

const dTotalInput = document.querySelector("#dTotal");
const cTotalInput = document.querySelector("#cTotal");
const grossAmount = document.querySelector("#gross_amount");
const addEntryBtn = document.querySelector("#next_btn");

const getItems = () => {
    // Get the element again
    const entryItemLedgerSelect = document.querySelectorAll(
        "#item_id"
    );
    // Loop through the element
    entryItemLedgerSelect.forEach((select) => {
        $.ajax({
            url: base_url + "item/get_all_items",
            type: "GET",
            success: function (data) {
                const items = JSON.parse(data);
                items.data.map((item) => {
                    let opt = document.createElement("option");

                    opt.value = item.id;
                    opt.innerHTML = item.name;
                    select.appendChild(opt);
                });
            },
        });
    });
};

const getLedgers = () => {
    // Get the element again
    const entryItemLedgerSelect = document.querySelectorAll(
        "#entry_item_ledger"
    );
    // Loop through the element
    entryItemLedgerSelect.forEach((select) => {
        $.ajax({
            url: base_url + "accounting_entries/get_all_ledgers",
            type: "GET",
            success: function (data) {
                const ledgers = JSON.parse(data);
                ledgers.data.map((ledger) => {
                    let opt = document.createElement("option");

                    opt.value = ledger.id;
                    opt.innerHTML = ledger.name;
                    select.appendChild(opt);
                });
            },
        });
    });
};

const totalOfItemAmount = (el) => {

    var total = 0;

    el.forEach(item => total += parseFloat($(item).val().trim() || 0));
    if (total != 0) {
        grossAmount.value = total;
    }
}


addEntryBtn.addEventListener("click", function () {
    const amountEl = document.querySelectorAll("#item_total_amount");
    totalOfItemAmount(amountEl);
})




const paymentType = () => {
    const entryItemDcSelect = document.querySelectorAll("#entry_item_dc");
    const dInput = document.querySelectorAll("#dr_amount_input");
    const cInput = document.querySelectorAll("#cr_amount_input");

    entryItemDcSelect.forEach((select, index) => {
        $(select).on("change", function () {
            if (this.value !== "") {
                if (this.value == "d") {
                    cInput[index].disabled = true;
                    cInput[index].value = 0;
                    dInput[index].disabled = false;
                } else if (this.value == "c") {
                    cInput[index].disabled = false;
                    dInput[index].disabled = true;
                    dInput[index].value = 0;
                }
            }
        });
    });
};

const calculateDTotal = () => {
    const dInput = document.querySelectorAll("#dr_amount_input");
    // dTotalInput.value = 0;
    $(dInput).keyup(function () {
        var total = 0;

        $(dInput).each(function () {
            total += parseFloat($(this).val().trim() || 0);
        });
        dTotalInput.value = total.toFixed(2);
    });
};

const calculateCTotal = () => {
    const cInput = document.querySelectorAll("#cr_amount_input");
    // cTotalInput.value = 0;
    $(cInput).keyup(function () {
        var total = 0;

        $(cInput).each(function () {
            total += parseFloat($(this).val().trim() || 0);
        });
        cTotalInput.value = total.toFixed(2);
    });
};

const listItemDetail = () => {
    const listItemSelect = document.querySelectorAll("#item_id");
    const unitPrice = document.querySelectorAll("#item_unit_price");
    const quantity = document.querySelectorAll("#item_quantity");
    const taxID = document.querySelectorAll("#item_tax_id");
    const taxName = document.querySelectorAll("#tax_name");
    const taxAmount = document.querySelectorAll("#item_tax_amount");
    const totalAmount = document.querySelectorAll("#item_total_amount");

    listItemSelect.forEach((select, index) => {
        $(select).on("change", function () {
            const item = getSelectedItem(this.value);
            unitPrice[index].value = item['unit_price'];
            taxID[index].value = item['tax_id'];
            taxName[index].value = item['tax']['name'];
            taxAmount[index].value = item['total_price'] - item['unit_price'];
            totalAmount[index].value = item['total_price'];
        })
    })
};

const calculateItemTotalPrice = () => {
    const listItemSelect = document.querySelectorAll("#item_quantity");
    const unitPrice = document.querySelectorAll("#item_unit_price");
    const taxAmount = document.querySelectorAll("#item_tax_amount");
    const totalAmount = document.querySelectorAll("#item_total_amount");

    listItemSelect.forEach((select, index) => {
        $(select).on("change keyup blur", function () {
            const total = (parseFloat(unitPrice[index].value) + parseFloat(taxAmount[index].value)) * this.value;
            totalAmount[index].value = total.toFixed(2);
        })
    })
};

function getSelectedItem(itemID) {
    $.ajax({
        url: base_url + 'item/Item/get_item',
        type: 'post',
        data: {
            itemID,
            itemID
        },
        dataType: 'json',
        complete: function () {

        },
        success: function (json) {
            item = json;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        },
        timeout: 10000,
        async: false
    });

    return item;
}

listItemDetail();
calculateItemTotalPrice();
getItems();
getLedgers();
paymentType();
calculateDTotal();
calculateCTotal();
