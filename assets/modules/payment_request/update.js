// Class definition
var paymentRequest = function() {
	var select2 = function() {
		$('#group_id').select2({
			placeholder: "Select a group"
		});
	};

	var valStaff = function () {
		$( "#form_payment_request" ).validate({
			// define validation rules
			rules: {
				payable_type_id: {
					required: true
				},
				payee_type: {
					required: true,
				},
				payee_type_id: {
					required: true
				},
				gross_amount: {
					required: true
				},
				net_amount: {
					required: true
				},
				wht_amount: {
					required: true
				},
				wht_percentage: {
					required: true
				},
				due_date: {
					date: true
				},
			},

			//display error alert on form submit
			invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

				event.preventDefault();
			},

			submitHandler: function (form) {
				form[0].submit(); // submit the form
			}
		});
	}

	var formRepeater = function() {

		$('#academic_form_repeater, #seminar_form_repeater, #exam_form_repeater').repeater({
			initEmpty: false,

			defaultValues: {
			},

			show: function () {
				$(this).slideDown();
				datepicker();
			},

			hide: function (deleteElement) {
				$(this).slideUp(deleteElement);
			}
		});

	}

	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}

	// Private functions
	var datepicker = function () {
		// minimum setup
		$('.datePicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			locale: 'no',
			format: 'yyyy-mm-dd',
			autoclose: true
		});

		$('.yearPicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			format: "yyyy",
			viewMode: "years",
			minViewMode: "years",
			autoclose: true
		});
	}


	// Public functions
	return {
		init: function() {
			select2();
			datepicker();
			valStaff();
			formRepeater();
		}
	};
}();

// Initialization
jQuery(document).ready(function() {
	paymentRequest.init();
});