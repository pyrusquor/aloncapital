'use strict';

// Class Definition
var KTRegistrationGeneral = (function () {
    var handleSignUpFormSubmit = function () {
        $('#registration_form_btn').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $('#registration_form');

            form.validate({
                rules: {
                    agree: {
                        required: true,
                    },
                    username: {
                        required: true,
                    },
                    email: {
                        required: true,
                    },
                    password: {
                        required: true,
                    },
                    first_name: {
                        required: true,
                    },
                    middle_name: {
                        required: true,
                    },
                    last_name: {
                        required: true,
                    },
                    birthday: {
                        required: true,
                    },
                    gender: {
                        required: true,
                    },
                    contact: {
                        required: true,
                    },
                    civil_status_id: {
                        required: true,
                    },
                    nationality: {
                        required: true,
                    },
                },
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass(
                'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light'
            ).attr('disabled', true);

            form.ajaxSubmit({
                url: base_url + 'auth/register',
                type: 'POST',
                dataType: 'JSON',
                success: function (response, status, xhr, form) {
                    if (response.status) {
                        swal.fire({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                        }).then(function () {
                            window.location.replace(base_url + 'login');
                        });
                    } else {
                        btn.removeClass(
                            'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light'
                        ).attr('disabled', false);
                        swal.fire('Oops!', response.message, 'error');
                    }
                },
            });
        });
    };

    var handleChangePasswordFormSubmit = function () {
        $('#change_password_form_btn').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                    },
                },
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass(
                'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light'
            ).attr('disabled', true);

            form.ajaxSubmit({
                url: base_url + 'auth/reset_password',
                type: 'POST',
                dataType: 'JSON',
                success: function (response, status, xhr, form) {
                    if (response.status == 1) {
                        swal.fire({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                        }).then(function () {
                            window.location.replace(base_url + 'login');
                        });
                    } else {
                        btn.removeClass(
                            'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light'
                        ).attr('disabled', false);
                        swal.fire('Oops!', response.message, 'error');
                    }
                },
            });
        });
    };

    var calculate = function (){
        var loan_period = $('#loan_period').val();
        var loan_amount = $('#loan_amount').val();

        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: base_url + 'auth/calculate_loan/'+loan_period+'/'+loan_amount,
            data: {
                loan_period: loan_period,
                loan_amount: loan_amount,
            },
            success: function (response) {
                $('#processing_fee').html(response.processing_fee);
                $('#monthly_payable').html(response.monthly_payment);
            },
        });
        
    }   

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.datePicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true,
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });

       $(document).on('click', '#calculateloan', function (e) {
            calculate();
       });

        $(document).on('click', '#loan_modal_btn', function () {

            $('#loanModal').modal('toggle');
        });

    };

    // Public Functions
    return {
        // public functions
        init: function () {
            handleSignUpFormSubmit();
            // handleChangePasswordFormSubmit();
            datepicker();
        },
    };
})();

// Class Initialization
jQuery(document).ready(function () {
    KTRegistrationGeneral.init();
});

$(document).on(
     'change',
     '#type_id',
     function () {
        var val = parseInt($('#type_id').val());
        if (val == 3) {
           $('.for_salary_loan').removeClass('hide');
        } else {
           $('.for_salary_loan').addClass('hide');
        }
     }
 );