'use strict';

// Class Definition
var KTForgotPasswordGeneral = (function () {
    var handleChangePasswordFormSubmit = function () {
        $('#change_password_form_btn').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                    },
                },
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass(
                'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light'
            ).attr('disabled', true);

            form.ajaxSubmit({
                url: base_url + 'auth/reset_password',
                type: 'POST',
                dataType: 'JSON',

                success: function (response, status, xhr, form) {
                    if (response.status) {
                        swal.fire({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                        }).then(function () {
                            window.location.replace(base_url + 'login');
                        });
                    } else {
                        btn.removeClass(
                            'kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light'
                        ).attr('disabled', false);
                        swal.fire('Oops!', response.message, 'error');
                    }
                },
            });
        });
    };

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.datePicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true,
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
    };

    // Public Functions
    return {
        // public functions
        init: function () {
            handleChangePasswordFormSubmit();
            datepicker();
        },
    };
})();

// Class Initialization
jQuery(document).ready(function () {
    KTForgotPasswordGeneral.init();
});
