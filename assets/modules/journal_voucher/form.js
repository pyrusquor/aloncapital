// Class definition
var Create = (function () {
  // var formRepeater = function () {
  //   $("#form_journal_voucher").repeater({
  //     initEmpty: false,

  //     defaultValues: {},

  //     show: function () {
  //       $(this).slideDown(); 
  //     },

  //     hide: function (deleteElement) {
  //       $(this).slideUp(deleteElement);
  //     },
  //   });
  // };

  var entryFormRepeater = function () {
    $("#entry_item_form_repeater").repeater({
      initEmpty: false,

      defaultValues: {},

      show: function () {
        $(this).slideDown();
        datepicker();
        getLedgers();
        paymentType();
        calculateDTotal();
        calculateCTotal();

        $('.select2-container').remove();
        Select.init();
        $('.select2-container').css('width','100%');
      },

      hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
        Select.init();
      },
    });
  };

  var arrows;
  if (KTUtil.isRTL()) {
    arrows = {
      leftArrow: '<i class="la la-angle-right"></i>',
      rightArrow: '<i class="la la-angle-left"></i>',
    };
  } else {
    arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    };
  }

  // Private functions
  var datepicker = function () {
    // minimum setup
    $(".kt_datepicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      orientation: "bottom left",
      templates: arrows,
      locale: "no",
      format: "yyyy-mm-dd",
      autoclose: true,
    });

    $(".yearPicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true,
    });
  };

  // Base elements
  var wizardEl;
  var formEl;
  var validator;
  var _add_ons;

  // Private functions
  var initWizard = function () {
    // Initialize form wizard
    wizard = new KTWizard("kt_wizard_v3", {
      startStep: 1,
    });

    // Validation before going to next page
    wizard.on("beforeNext", function (wizardObj) {
      if (validator.form() !== true) {
        wizardObj.stop(); // don't go to the next step
      }
    });

    wizard.on("beforePrev", function (wizardObj) {
      if (validator.form() !== true) {
        wizardObj.stop(); // don't go to the next step
      }
    });

    // Change event
    wizard.on("change", function (wizard) {
      KTUtil.scrollTop();
    });
  };

  var initValidation = function () {
    validator = formEl.validate({
      // Validate only visible fields
      ignore: ":hidden",

      // Validation rules
      rules: {
        'voucher[payable_type_id]': {
              required: true
          },
          'voucher[approving_department_id]': {
            required: true
          },
          'voucher[approving_staff_id]': {
            required: true
          },
          'voucher[requesting_department_id]': {
            required: true
          },
          'voucher[requesting_staff_id]': {
            required: true
          },
          'voucher[prepared_by]': {
            required: true
          },
          'voucher[particulars]': {
            required: true
          },
          'accounting_entry[dr_total]': {
            required: true
          },
          'cr_total': {
            required: true
          },
        },
      // messages: {
      // 	"info[last_name]":'Last name field is required',
      // },
      // Display error
      invalidHandler: function (event, validator) {
        KTUtil.scrollTop();

        swal.fire({
          title: "",
          text:
            "There are some errors in your submission. Please correct them.",
          type: "error",
          confirmButtonClass: "btn btn-secondary",
        });
      },

      // Submit valid form
      // submitHandler: function (form) {},
    });
  };

  var initSubmit = function () {
    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

    btn.on("click", function (e) {
      e.preventDefault();
      let dTotalInput = $("#dTotal").val();
      let cTotalInput = $("#cTotal").val();

      // if (dTotalInput == cTotalInput) {
          if (validator.form()) {
            // See: src\js\framework\base\app.js

            KTApp.progress(btn);
            //KTApp.block(formEl);
            // See: http://malsup.com/jquery/form/#ajaxSubmit
            formEl.ajaxSubmit({
              type: "POST",
              dataType: "JSON",
              success: function (response) {
                console.log(response);
                if (response.status) {
                  swal
                    .fire({
                      title: "Success!",
                      text: response.message,
                      type: "success",
                    })
                    .then(function () {
                      window.location.replace(base_url + "journal_voucher");
                    });
                } else {
                  swal.fire({
                    title: "Oops!",
                    html: response.message,
                    icon: "error",
                  });
                }
              },
            });
          }
      // }
      // else {
      //   swal.fire({
      //     title: "Error!",
      //     html: "Please make sure that Debit and credit amount is balanced.",
      //     icon: "error",
      //   });
      // }
    });
  };

  var _add_ons = function () {
    datepicker();
  };
  // Public functions
  return {
    init: function () {
      datepicker();
      // formRepeater();
      entryFormRepeater();
      _add_ons();

      wizardEl = KTUtil.get("kt_wizard_v3");
      formEl = $("#form_journal_voucher");

      initWizard();
      initValidation();
      initSubmit();
    },
  };
})();

// Initialization
jQuery(document).ready(function () {
  Create.init();
});

const dTotalInput = document.querySelector("#dTotal");
const cTotalInput = document.querySelector("#cTotal");

const getLedgers = () => {
  // Get the element again
  const entryItemLedgerSelect = document.querySelectorAll("#entry_item_ledger");
  // Loop through the element
  entryItemLedgerSelect.forEach((select) => {
    $.ajax({
      url: base_url + "accounting_entries/get_all_ledgers",
      type: "GET",
      success: function (data) {
        const ledgers = JSON.parse(data);
        ledgers.data.map((ledger) => {
          let opt = document.createElement("option");

          opt.value = ledger.id;
          opt.innerHTML = ledger.name;
          select.appendChild(opt);
        });
      },
    });
  });
};

const paymentType = () => {
  const entryItemDcSelect = document.querySelectorAll("#entry_item_dc");
  const dInput = document.querySelectorAll("#dr_amount_input");
  const cInput = document.querySelectorAll("#cr_amount_input");

  entryItemDcSelect.forEach((select, index) => {
    $(select).on("change", function () {
      if (this.value !== "") {
        if (this.value == "d") {
          cInput[index].disabled = true;
          cInput[index].value = 0;
          dInput[index].disabled = false;
        } else if (this.value == "c") {
          cInput[index].disabled = false;
          dInput[index].disabled = true;
          dInput[index].value = 0;
        }
      }
    });
  });
};

const calculateDTotal = () => {
  const dInput = document.querySelectorAll("#dr_amount_input");
  // dTotalInput.value = 0;
  $(dInput).keyup(function () {
    var total = 0;

    $(dInput).each(function () {
      total += parseFloat($(this).val() || 0);
    });
    dTotalInput.value = total.toFixed(2);
  });
};

const calculateCTotal = () => {
  const cInput = document.querySelectorAll("#cr_amount_input");
  // cTotalInput.value = 0;
  $(cInput).keyup(function () {
    var total = 0;

    $(cInput).each(function () {
      total += parseFloat($(this).val() || 0);
    });
    cTotalInput.value = total.toFixed(2);
  });
};

getLedgers();
paymentType();
calculateDTotal();
calculateCTotal();
