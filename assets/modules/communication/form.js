// Class definition
var Create = function() {
    
	var tinyMCE = function() {

        tinymce.init({
            mode : "none",
            selector:'textarea#content',
            height: 300,
            
            // ===========================================
            // INCLUDE THE PLUGIN
            // ===========================================
            plugins: [
                "code advlist autolink accordion link image lists charmap print preview hr pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste jbimages table advtable"
            ],
            toolbar: "formatselect | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | responsivefilemanager | pastetext | fontselect | fontsizeselect | preview | accordion | code ", 
        });

        $(document).on('select2:select', '#template_id', function (e) {
            var id = $(this).val();
            $.ajax({
                url: base_url + 'communication_templates/view/'+id+'/json',
                dataType: "JSON",
                type: 'GET',
                success: function (data) {
                	tinymce.activeEditor.execCommand('mceInsertContent', false,data.content);
                }
            });
            
		});
		

		$(document).on('change', '#recipient_type', function (e) {
            loadRecipients($(this).val())
		});

		$(document).on('click', '.table_field', function (e) {
           
            var email = $(this).attr('data-email');
            var mobile = $(this).attr('data-mobile');
            var name = $(this).attr('data-name');
            var id = $(this).attr('data-id');

            /* console.log($.inArray(field_selected,existing_fields));
            if($.inArray(field_selected,existing_fields) >= 0){
                alert('ALREADY EXIST!');
            } else { */
            	row = "<option value='"+id+"' selected>"+name+' '+mobile+' '+email+"</option>";
                $('#all_recipients').append(row).select2();
            //  } 
			$(".remove_field[data-id='"+id+"']").show();
            $(this).hide();

		});

		$(document).on('click', '.remove_field', function (e) {
			id = $(this).attr('data-id');
			$(".table_field[data-id='"+id+"']").show();
			$("#all_recipients option[value='"+id+"']").remove().select2();
            $(this).hide();
		});


		$(document).on('click', '.select_all', function (e) {
	    	$('.table_field').each(function(index,element){
	             $(this).trigger('click');
			});
			$('.remove_all').show();
			$(this).hide();
		});

		$(document).on('click', '.remove_all', function (e) {
			$('.remove_field').each(function(index,element){
	             $(this).trigger('click');
			});
			$('.select_all').show();
			$(this).hide();
		});
    }


	var arrows;
	
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	// Private functions
	var datepicker = function () {
		// minimum setup
		$('.datePicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			locale: 'no',
			format: 'yyyy-mm-dd',
			autoclose: true
		});

		$('.yearPicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years",
			autoclose: true
		});

		// minimum setup
		$('.datetimePicker').datetimepicker({
			minDate : 0,
			minTime : 0,
            todayHighlight: true,
            autoclose: true,
            format: 'yyyy-mm-dd hh:ii'
        });
	}
	
	// Base elements
	var wizardEl;
	var formEl;
	var validator;
	var wizard;
	
	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		wizard = new KTWizard('kt_wizard_v3', {
			startStep: 1,
		});

		// Validation before going to next page
		wizard.on('beforeNext', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
			Select.init();
			additionalInfo();
		});

		wizard.on('beforePrev', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
				additionalInfo();
			}
		});

		// Change event
		wizard.on('change', function(wizard) {
			KTUtil.scrollTop();	
		});
	}

	var initValidation = function() {
		validator = formEl.validate({
			// Validate only visible fields
			ignore: ":hidden",

			// Validation rules
			rules: {
			 //    "info[send_schedule_date]": {
		  //           required: true 
				// },
				//  "info[medium_type]": {
		  //           required: true 
				// },
				//  "info[template_id]": {
		  //           required: true 
				// },
				//  "info[content]": {
		  //           required: true 
				// }
            },
			// Display error  
			invalidHandler: function(event, validator) {	 
				KTUtil.scrollTop();
				swal.fire({
					"title": "", 
					"text": "There are some errors in your submission. Please correct them.", 
					"type": "error",
					"confirmButtonClass": "btn btn-secondary"
				});
			},
			// Submit valid form
			submitHandler: function (form) {
				
			}
		});   
	}

	var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formEl.ajaxSubmit({
                    type: 'POSt',
                    dataType: 'JSON',
					success: function(response) {
						if (response.status) {
							swal.fire({
								title: "Success!",
								text: response.message,
								type: "success"
							}).then(function() {
								window.location.replace(base_url + "communication");
							});

						} else {
							swal.fire(
								'Oops!',
								response.message,
								'error'
							)
						}
					}
				});
			}
		});
	}

    // Public functions
    return {
        init: function() {
			datepicker(); 

			wizardEl = KTUtil.get('kt_wizard_v3');
			formEl = $('#form_communication');

			initWizard(); 
			initValidation();
			initSubmit();
			tinyMCE();

        }
    };
}();

function loadRecipients (TblName) {
	if (!!TblName.length) {

		// Clear All Recipients list
		$('#all_recipients').html("")

		// Set recipient type value
		$('#recipient_type option[value="'+TblName+'"]').attr('selected','selected');

		var tableContent = '<tr><td></td><td><button type="button" class="btn btn-primary btn-sm select_all">SELECT ALL</button><button type="button" class="btn btn-danger btn-sm remove_all" style="display:none">REMOVE ALL</button></td></tr>';

		$.ajax({
			url: base_url + TblName+'/get_all',
			dataType: "JSON",
			type: 'GET',
			success: function (data) {

				$.each(data.row, function (key, item) {

					if (window.current_recipients.includes(item.id) && TblName === window.recipient_table) {
						tableContent += '<tr>';
						tableContent += '<td data-id="' + item.id + '">' + item.first_name + ' ' + item.last_name +'</td>';
						tableContent += '<td class="kt-align-left"><button type="button" class="btn btn-primary btn-sm table_field" data-mobile="'+ item.mobile_no +'" data-id="'+item.id+'" data-email="'+item.email+'" data-name="'+item.first_name+'.'+item.last_name+'" style="display:none;">Insert</button><button type="button" class="btn btn-danger btn-sm remove_field"  data-id="'+item.id+'">Remove</button></td>';
						tableContent += '</tr>';

						row = "<option value='"+item.id+"' selected>"+item.first_name + ' ' + item.last_name+' '+item.mobile_no+' '+item.email+"</option>";
						$('#all_recipients').append(row).select2();
					} else {
						tableContent += '<tr>';
						tableContent += '<td data-id="' + item.id + '">' + item.first_name + ' ' + item.last_name +'</td>';
						tableContent += '<td class="kt-align-left"><button type="button" class="btn btn-primary btn-sm table_field" data-mobile="'+ item.mobile_no +'" data-id="'+item.id+'" data-email="'+item.email+'" data-name="'+item.first_name+'.'+item.last_name+'">Insert</button><button type="button" style="display:none;" class="btn btn-danger btn-sm remove_field"  data-id="'+item.id+'">Remove</button></td>';
						tableContent += '</tr>';
					}
				});

				$('#tbody').html(tableContent);

				// Filter name
				$('#search_name').keyup(function (e) {
					// Declare variables
					var input, filter, table, tr, td, i, txtValue;
					input = document.getElementById("search_name");
					filter = input.value.toUpperCase();
					table = document.getElementById("names_table");
					tr = table.getElementsByTagName("tr");
				
					// Loop through all table rows, and hide those who don't match the search query
					for (i = 0; i < tr.length; i++) {
						if (i > 1) {
							td = tr[i].getElementsByTagName("td")[0];
							if (td) {
								txtValue = td.textContent || td.innerText;
								if (txtValue.toUpperCase().indexOf(filter) > -1) {
									tr[i].style.display = "";
								} else {
									tr[i].style.display = "none";
								}
							}
						}
					}
				})

				let recipientIds = []

				// Filter by contacts or buyers
				if (TblName === 'contacts') {
					selectContent = ''
					$.ajax({
						url: base_url + 'contact_groups/get_all',
						dataType: "JSON",
						type: 'GET',
						success: function (contacts_data) {

							$.each(contacts_data.row, function (contacts_key, contacts_item) {
								// If first item (key===0)
								if (!contacts_key) {
									selectContent += '<option value="0">Select Contact Group</option>'
								}

								selectContent += '<option value="' + contacts_item.id + '">' + contacts_item.name + '</option>'

								// Populate recipientIds
								if (!!contacts_item.contacts) {
									recipientIds[contacts_item.id] = Object.keys(contacts_item.contacts)
								}
							})

							$('#filter_by').html(selectContent)
						}
					})
				} else if (TblName === 'buyer') {
					selectContent = ''
					$.ajax({
						url: base_url + 'project/get_all',
						dataType: "JSON",
						type: 'GET',
						success: function (project_data) {

							$.each(project_data.row, function (project_key, project_item) {
								// If first item (key===0)
								if (!project_key) {
									selectContent += '<option value="0">Select Project</option>'
								}
								
								selectContent += '<option value="' + project_item.id + '">' + project_item.name + '</option>'

								// Populate recipientIds
								if (!!project_item.buyers) {
									recipientIds[project_item.id] = Object.keys(project_item.buyers)
								}
							})

							$('#filter_by').html(selectContent)
						}
					})
				} else {
					// Reset filter by options
					let content = '<option>Select Option</option>'

					$('#filter_by').html(content)
				}

				// Filter by project or contact froup
				$('#filter_by').change(function (e) {
					// Declare variables
					let table, tr, td, i, td_data_id;
					// filter = input.value.toUpperCase();
					table = document.getElementById("names_table");
					tr = table.getElementsByTagName("tr");

					let filterBy = recipientIds[this.value]

					if (!!filterBy) {
						// Loop through all table rows, and hide those who don't match the search query
						for (i = 0; i < tr.length; i++) {
							if (i > 1) {
								td = tr[i].getElementsByTagName("td")[0];
								if (td) {
									td_data_id = td.getAttribute("data-id");
									if (filterBy.includes(td_data_id)) {
										tr[i].style.display = "";
									} else {
										tr[i].style.display = "none";
									}
								}
							}
						}
					} else {
						// Show all recipients (based on recipient type)
						for (i = 0; i < tr.length; i++) {
							if (i > 1) {
								tr[i].style.display = "";
							}
						}
					}
				})

				// loadSelectedRecipients()
			}
		});
	} else {
		let content = '<option>Select Option</option>'
		$('#filter_by').html(content)
		$('#tbody').html('')
	}

	$('#search_name').val("")
}

// Initialization
jQuery(document).ready(function() {
    Create.init();

	loadRecipients(window.recipient_table)
});