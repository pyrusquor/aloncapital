"use strict";
var Communication = (function () {
    var communicationTable = function () {
        var table = $("#communication_table");

        // begin first table
        table.DataTable({
            order: [
                [1, 'desc']
            ],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,
            // ajax: base_url + "communication/showCommunications",
            ajax: {
                url:base_url + 'communication/showCommunications',
                data: function(data){
                   // Read values
                   var communication_type = $('#medium_type').val();
                   var scheduled_date = $('#kt_communication_daterangepicker').val();
         
                   // Append to data
                   data.communication_type = communication_type;
                   data.scheduled_date = scheduled_date;
                }
             },
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                text: '<i class="la la-trash"></i> Delete Selected',
                className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm",
                attr: {
                    id: "bulkDelete",
                },
                enabled: false,
            }, ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },
            columns: [{
                    data: null,
                },
                {
                    data: "id",
                },
                {
                    data: "communication_title",
                },
                {
                    data: "medium_type",
                },
                {
                    data: "sent_type",
                },
                {
                    data: "date_sent",
                },
                {
                    data: "recipient_id",
                },
                {
                    data: "status",
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: "Actions",
                    responsivePriority: -1,
                },
            ],
            columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										        <a class="dropdown-item" href="` +
                            base_url +
                            `communication/form/` +
                            row.id +
                            `"><i class="la la-edit"></i> Update </a>

										        <a href="javascript:void(0);" class="dropdown-item delete_communication" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete </a>

                            <a href="javascript:void(0);" class="dropdown-item send_message" data-id="` +
                            row.id +
                            `"><i class="flaticon-paper-plane"></i> Send </a>
									</div>
								</span>
								<a href="` +
                            base_url +
                            `communication/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, 5, -1],
                    className: "dt-center",
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },

				/* ==================== begin: Add target fields for dropdown value ==================== */
                {
                    targets: 4,
                    render: function (data, type, row, meta) {
                        var request = {
                            0: {
                                title: 'Manual',
                            },
                            1: {
                                title: 'Automatic',
                            },
                        };

                        if (typeof request[data] === 'undefined') {
                            return ``;
                        }
                        return request[data].title;
                    },
                },

                {
                  targets: 7,
                  render: function (data, type, row, meta) {
                      var request = {
                          0: {
                              title: 'Pending',
                          },
                          1: {
                              title: 'Delivered',
                          },
                      };

                      if (typeof request[data] === 'undefined') {
                          return ``;
                      }
                      return request[data].title;
                  },
              },

                // {
                //     targets: [12, 13, 14],
                //     visible: false,
                // },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            },
        });

        var oTable = table.DataTable();
        $("#generalSearch").keyup(function () {
            oTable.search($(this).val()).draw();
        });

        $('#medium_type').on('change', function(){
            oTable.draw();
        })
    };

    var sendMessages = function () {
      $(document).on("click", ".send_message", function () {
        let btn = $(this);
        let id = btn.data("id");
  
        swal
          .fire({
            title: "Send sms/email",
            text: "Are you sure? You won't be able to revert this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, please proceed.",
            cancelButtonText: "No, cancel!",
            reverseButtons: true,
          })
          .then(function (result) {
            if (result.value) {
              $.ajax({
                url: base_url + "communication/send/" + id,
                dataType: "JSON",
                success: function (res) {
                  if (res.status) {
                    Swal.fire({
                      title: "Sent!",
                      text: res.message,
                      type: "success",
                    }).then((result) => {
                      // Reload the Page
                      location.reload();
                    });
                  } else {
                    swal.fire("Oops!", res.message, "error");
                  }
                },
              });
            } else if (result.dismiss == "cancel") {
              swal.fire("Cancelled", "Sending information cancelled", "error");
            }
          });
      });
    };

    var confirmDelete = function () {
        $(document).on("click", ".delete_communication", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "communication/delete/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            id: id
                        },
                        success: function (res) {
                            if (res.status) {
                                $("#communication_table").DataTable().ajax.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on("click", "#btn_upload_guide", function () {
            var table = $("#upload_guide_table");

            table.DataTable({
                order: [
                    [0, "asc"]
                ],
                pagingType: "full_numbers",
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + "communication/get_table_schema",
                columns: [
                    // {data: 'button'},
                    {
                        data: "no"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "type"
                    },
                    {
                        data: "format"
                    },
                    {
                        data: "option"
                    },
                    {
                        data: "required"
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _filterLandInventory = function () {
        var dTable = $("#communication_table").DataTable();

        function _colFilter(n) {

            dTable.column(n).search($('#_column_' + n).val()).draw();
        }

        $('._filter').on('keyup change clear', function () {
            if ($(this).is("input")) {
                let val = $("#_column_" + $(this).data('column')).val();

                dTable.search(val).draw()
            } else {
                _colFilter($(this).data('column'));
            }

        });
    };

    var _selectProp = function () {
        var _table = $("#communication_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            // var rows = _table.rows({ 'search': 'applied' }).nodes();

            // $('input[type="checkbox"]').prop( 'checked', this.checked );
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#property_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "communication/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#property_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    var _add_ons = function () {
        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    var daterangepickerInit = function () {
        if ($('#kt_communication_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_communication_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                // range = start.format("MMM D") + " - " + end.format("MMM D");
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                },
                opens: 'left',
                // ranges: {
                // 	'Today': [moment(), moment()],
                // 	'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                // 	'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                // 	'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                // 	'This Month': [moment().startOf('month'), moment().endOf('month')],
                // 	'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                // }
            },
            cb
        );

        cb(start, end, '');

        $(picker).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(
                picker.startDate.format('MM/DD/YYYY') +
                    ' - ' +
                    picker.endDate.format('MM/DD/YYYY')
            );
            var table = $("#communication_table");
            var oTable = table.DataTable();
            oTable.draw();
        });

        $(picker).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            var table = $("#communication_table");
            var oTable = table.DataTable();
            oTable.draw();
        });
    };

    var load_filters = function (){
        var email_date_range = localStorage.getItem('general_email_date');
        var sms_date_range = localStorage.getItem('general_sms_date');
        var table = $("#communication_table");
        var oTable = table.DataTable();
        if(email_date_range!==null){
            $('#medium_type').val('email');
            $('#kt_communication_daterangepicker').val(email_date_range);
            oTable.draw();
        }
        if(sms_date_range!==null){
            $('#medium_type').val('sms');
            $('#kt_communication_daterangepicker').val(sms_date_range);
            oTable.draw();
        }

        localStorage.removeItem('general_email_date');
        localStorage.removeItem('general_sms_date');
    }

    return {
        //main function to initiate the module
        init: function () {
            daterangepickerInit();
            confirmDelete();
            communicationTable();
            _filterLandInventory();
            _selectProp();
            _add_ons();
            upload_guide();
            status();
            sendMessages();
            load_filters();
        },
    };
})();

jQuery(document).ready(function () {
  Communication.init();
});

function TransactionPagination(page_num) {
  page_num = page_num ? page_num : 0;

  var keyword = $("#generalSearch").val();

  var name = name ? name : "";
  var transaction_type_id = transaction_type_id ? transaction_type_id : "";
  var occupation_type_id = occupation_type_id ? occupation_type_id : "";
  var occupation_location_id = occupation_location_id
    ? occupation_location_id
    : "";
  var civil_status_id = civil_status_id ? civil_status_id : "";

  $.ajax({
    url: base_url + "transaction/paginationData/" + page_num,
    method: "POST",
    data:
      "page=" +
      page_num +
      "&name=" +
      name +
      "&transaction_type_id=" +
      transaction_type_id +
      "&occupation_type_id=" +
      occupation_type_id +
      "&occupation_location_id=" +
      occupation_location_id +
      "&civil_status_id=" +
      civil_status_id,
    success: function (html) {
      KTApp.block("#kt_content", {
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Processing...",
        css: {
          padding: 0,
          margin: 0,
          width: "30%",
          top: "40%",
          left: "35%",
          textAlign: "center",
          color: "#000",
          border: "3px solid #aaa",
          backgroundColor: "#fff",
          cursor: "wait",
        },
      });

      setTimeout(function () {
        $("#transactionContent").html(html);

        KTApp.unblock("#kt_content");
      }, 500);
    },
  });
}

const getSMSTemplates = () => {
  const smsTemplateSelect = document.querySelectorAll("#sms_template_id");

  $.ajax({
    url: base_url + "communication_templates/get_all",
    type: "GET",
    success: function(data) {
      const templates = JSON.parse(data);

      smsTemplateSelect.forEach((select) => {
        templates.data.map((temp) => {
          let opt = document.createElement("option");
  
          opt.value = temp.id;
          opt.innerHTML = "SMS - " + temp.name;
          select.appendChild(opt);
        });

        if(select.dataset.template) {
          setTimeout(() => {
            select.value = select.dataset.template;
          }, 1000);
        }
      });
    }
  });
}  

const getEmailTemplates = () => {
  const emailTemplateSelect = document.querySelectorAll("#email_template_id");

  $.ajax({
    url: base_url + "communication_templates/get_all",
    type: "GET",
    success: function(data) {
      const templates = JSON.parse(data);

      emailTemplateSelect.forEach((select) => {
        templates.data.map((temp) => {
          let opt = document.createElement("option");
      
          opt.value = temp.id;
          opt.innerHTML = "SMS - " + temp.name;
          select.appendChild(opt);
        });
      
        if(select.dataset.template) {
          setTimeout(() => {
            select.value = select.dataset.template;
          }, 1000);
        }
      });      
    }
  });
}  

