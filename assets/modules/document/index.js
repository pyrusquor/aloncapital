"use strict";
var KTDatatablesDocument = function () {

	var _initDocument = function () {

		var _table = $('#_document_table');

		_table.DataTable({
			order: [
				[1, 'desc']
			],
			pagingType: 'full_numbers',
			lengthMenu: [5, 10, 25, 50, 100],
			pageLength: 10,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			deferRender: true,
			dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
				// "<'row'<'col-sm-12 col-md-6'l>>" +
				// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			buttons: [{
				text: '<i class="fa fa-trash"></i> Delete Selected',
				className: 'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
				attr: {
					id: 'bulkDelete'
				},
				enabled: false
			}],
			language: {
				'lengthMenu': 'Show _MENU_',
				'infoFiltered': '(filtered from _MAX_ total records)'
			},
			ajax: base_url + 'document/get_documents',
			columns: [{
					data: null,
					orderable: false,
					searchable: false,
					render: function (data, type, row, meta) {

						return `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` + row.id + `" class="m-checkable __select" data-id="` + row.id + `">
                      <span></span>
	                  </label>`;
					}
				},
				{
					data: 'id'
				},
				{
					data: 'name'
				},
				{
					data: 'description'
				},
				{
					data: 'owner_id'
				},
				{
					data: 'classification_id'
				},
				{
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
				{
					data: 'Actions',
					responsivePriority: -1
				}
			],
			columnDefs: [{
					targets: -1,
					orderable: false,
					render: function (data, type, row, meta) {

						return `
                    <span class="dropdown">
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                          <i class="la la-ellipsis-h"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="` + base_url + `document/update/` + row.id + `" class="dropdown-item"><i class="la la-edit"></i> Update</a>
                            <button type="button" class="dropdown-item _delete_docx" data-id="` + row.id + `"><i class="la la-trash"></i> Delete</button>
                        </div>
                    </span>
                    <a href="` + base_url + `document/view/` + row.id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                      <i class="la la-eye"></i>
                    </a>
                  `;
					}
				},
				{
					targets: [1, 4, 5, 6, -1],
					className: 'dt-center',
				},
				{
					targets: 4,
					render: function (data, type, full, meta) {

						var _owner = {
							1: {
								title: 'Legal Staff'
							},
							2: {
								title: 'Legal Head'
							},
							3: {
								title: 'Agent'
							},
							4: {
								title: 'Sales Admin'
							},
							5: {
								title: 'AR Staff'
							},
							6: {
								title: 'AR Head'
							},
							7: {
								title: 'Titling Officer'
							},
							8: {
								title: 'Tax'
							},
							9: {
								title: 'Engineering'
							},
							10: {
								title: 'Cashier'
							},
							11: {
								title: 'Sales Coordinator'
							},
							12: {
								title: 'Client'
							},
							13: {
								title: 'Credit and Collection'
							}
						};

						if (typeof _owner[data] === 'undefined') {

							return ``;
						}

						return _owner[data].title;
					}
				},
				{
					targets: 5,
					render: function (data, type, full, meta) {

						var _classification = {
							1: {
								title: 'Client Document'
							},
							2: {
								title: 'Company Document'
							},
							3: {
								title: 'Government Document'
							}
						};

						if (typeof _classification[data] === 'undefined') {

							return ``;
						}

						return _classification[data].title;
					}
				}
			],
			drawCallback: function (settings) {

				$('span#_total').text(settings.fnRecordsTotal() + ' TOTAL');
			}
		});
	}

	var _selectProp = function () {

		// var _table    = $('#_document_table').DataTable();

		// $('input#select-all').on( 'click', function () {

		// 	var rows = _table.rows({ 'search': 'applied' }).nodes();

		// 	$('input[type="checkbox"]').prop( 'checked', this.checked );
		// });

		// $('#_document_table tbody').on( 'change', 'input[type="checkbox"]', function () {

		// 	if ( !this.checked ) {

		// 		var el = $('input#select-all').get(0);

		// 		if ( el && el.checked && ('indeterminate' in el) ) {

		// 			el.indeterminate = true;
		// 		}
		// 	}
		// });

		var _table = $('#_document_table').DataTable();

		$('input#select-all').on('click', function () {

			if (this.checked) {

				$('input.__select').each(function () {

					this.checked = true;
				});
			} else {

				$('input.__select').each(function () {

					this.checked = false;
				});
			}
		});

		$('#_document_table tbody').on('change', 'input.__select', function () {

			if (this.checked == false) {

				$('input#select-all').prop('checked', false);
			}
		});

		$(document).on('change', '.m-checkable', function () {

			var _checked = $('.m-checkable:checked').length;

			if (_checked === 0) {

				_table.button(0).enable(false);
			} else {

				_table.button(0).enable(true);
			}
		});

		$('#bulkDelete').click(function () {

			var deleteids_arr = [];
			// Read all checked checkboxes
			$('input[name="id[]"]:checked').each(function () {
				deleteids_arr.push($(this).val());
			});

			// Check checkbox checked or not
			if (deleteids_arr.length > 0) {

				swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Yes, delete it!',
					cancelButtonText: 'No, cancel!',
					reverseButtons: true
				}).then(function (result) {
					if (result.value) {

						$.ajax({
							url: base_url + 'document/bulkDelete/',
							type: 'POST',
							dataType: "JSON",
							data: {
								deleteids_arr: deleteids_arr
							},
							success: function (res) {
								if (res.status) {
									$('#_document_table').DataTable().ajax.reload();
									swal.fire(
										'Deleted!',
										res.message,
										'success'
									);
								} else {
									swal.fire(
										'Oops!',
										res.message,
										'error'
									)
								}
							}
						});

						$('input#select-all').prop('checked', false);

					} else if (result.dismiss === 'cancel') {
						swal.fire(
							'Cancelled',
							'Your imaginary file is safe :)',
							'error'
						)
					}
				});
			}
		});
	}

	var _add_ons = function () {

		var dTable = $('#_document_table').DataTable();

		$('#_search').on('keyup', function () {

			dTable.search($(this).val()).draw();
		});

		$('#kt_datepicker').datepicker({
			orientation: "bottom left",
			autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

		$('button#_advance_search_btn').on('click', function () {

			$('div#_batch_upload').removeClass('show');

			// var _search = $('#_document_table_filter').css('display');

			// if ( _search && (_search === 'block') ) {

			// 	$('#_document_table_filter').css('display', 'none');
			// } else {

			// 	$('#_document_table_filter').css('display', 'block');
			// }
		});

		$('button#_batch_upload_btn').on('click', function () {

			$('div#_advance_search').removeClass('show');
		});

		$(document).on('click', 'button._delete_docx', function () {

			var id = $(this).data('id');

			swal.fire({
				title: 'Are you sure?',
				text: 'You won\'t be able to revert this!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then(function (result) {

				if (result.value) {

					$.ajax({
						type: 'post',
						dataType: 'json',
						url: base_url + 'document/delete',
						data: {
							id: id
						},
						success: function (_response) {

							if (_response._status) {

								$('#_document_table').DataTable().ajax.reload();

								swal.fire(
									'Deleted!',
									_response._msg,
									'success'
								);
							} else {

								swal.fire(
									'Oops!',
									_response._msg,
									'error'
								);
							}
						}
					});
				} else if (result.dismiss === 'cancel') {

					swal.fire(
						'Cancelled',
						'Your imaginary file is safe.',
						'error'
					);
				}
			});
		});

		$('.modal').on('hidden.bs.modal', function () {
			$(this).find('form')[0].reset();

			$('form').find('input, select').removeClass('is-valid').removeClass('is-invalid');

			$(this).find('form').trigger('reset');

			$('#form_msg').closest('div.form-group').addClass('kt-hide');
		});
	}

	var _filterDocument = function () {

		var dTable = $('#_document_table').DataTable();

		function _colFilter(n) {

			dTable.column(n).search($('#_column_' + n).val()).draw();
		}

		$('._filter').on('keyup change clear', function () {
			const nameAttr = $(this).attr("name");

			if (nameAttr == "name") {
				let val = $("#_column_" + $(this).data('column')).val();

				dTable.search(val).draw()
			} else {
				_colFilter($(this).data('column'));

			}


		});
	}

	var _exportImport = function () {

		$('#_export_select_all').on('click', function () {

			if (this.checked) {

				$('._export_column').each(function () {

					this.checked = true;
				});
			} else {

				$('._export_column').each(function () {

					this.checked = false;
				});
			}
		});

		$('._export_column').on('click', function () {

			if (this.checked == false) {

				$('#_export_select_all').prop('checked', false);
			}
		});

		$('#_export_form').validate({

			rules: {
				'_export_column[]': {
					required: true
				}
			},
			messages: {
				'_export_column[]': {
					required: ''
				}
			},
			invalidHandler: function (event, validator) {

				event.preventDefault();

				var alert = $('#form_msg');
				alert.closest('div.form-group').removeClass('kt-hide').show();
				KTUtil.scrollTop();

				toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-right",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};

				toastr.error("Please check your fields", "Something went wrong");
			},
			submitHandler: function (_frm) {

				_frm[0].submit();
			}
		});
	}

	var _exportUploadCSV = function () {

		$('#_export_csv').validate({

			rules: {
				update_existing_data: {
					required: true,
				}
			},
			messages: {
				update_existing_data: {
					required: 'File type is required'
				}
			},
			invalidHandler: function (event, validator) {

				event.preventDefault();

				// var alert   =   $('#form_msg');
				// alert.closest('div.form-group').removeClass('kt-hide').show();
				KTUtil.scrollTop();

				toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-right",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};

				toastr.error("Please check your fields", "Something went wrong");
			},
			submitHandler: function (_frm) {

				_frm[0].submit();
			}
		});

		$('#_upload_form').validate({

			rules: {
				csv_file: {
					required: true,
				}
			},
			messages: {
				csv_file: {
					required: 'CSV file is required'
				}
			},
			invalidHandler: function (event, validator) {

				event.preventDefault();

				// var alert   =   $('#form_msg');
				// alert.closest('div.form-group').removeClass('kt-hide').show();
				KTUtil.scrollTop();

				toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-right",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				};

				toastr.error("Please check your fields", "Something went wrong");
			},
			submitHandler: function (_frm) {

				_frm[0].submit();
			}
		});
	}

	var _initUploadGuideModal = function () {

		var _table = $('#_upload_guide_modal');

		_table.DataTable({
			order: [
				[0, 'asc']
			],
			pagingType: 'full_numbers',
			lengthMenu: [3, 5, 10, 25, 50, 100],
			pageLength: 3,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: false,
			deferRender: true,
			dom:
				// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
				// "<'row'<'col-sm-12 col-md-6'l>>" +
				"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			language: {
				'lengthMenu': 'Show _MENU_',
				'infoFiltered': '(filtered from _MAX_ total records)'
			},
			columnDefs: [{
					targets: [0, 2, 3, 5],
					className: 'dt-center',
				},
				{
					targets: [0, 2, 3, 4, 5],
					orderable: false,
				},
				{
					targets: [4, 5],
					searchable: false
				},
				{
					targets: 0,
					searchable: false,
					visible: false
				}
			]
		});
	}

	return {

		init: function () {

			_initDocument();
			_add_ons();
			_filterDocument();
			_exportImport();
			_selectProp();
			_exportUploadCSV();
			_initUploadGuideModal();
		}
	};
}();

jQuery(document).ready(function () {

	KTDatatablesDocument.init();
});