"use strict";
var DocumentCreate = function () {

	var _form_validate = function () {

		$('#_document_form').validate({

			rules: {
				name: {
					required: true,
					minlength: 2
				},
				description: {
					required: true,
					minlength: 2
				},
				classification_id: {},
				lead_time: {
					digits: true
				},
				owner_id: {},
				processor: {},
				retention: {},
				is_need_hard_copy: {},
				access_right_id: {},
				security_classification: {},
				issuing_party: {},
				relevant_policies: {},
				form_file: {}
			},
			messages: {
				name: {
					required: 'The Document Name field is required',
					minlength: 'The Document Name field must be at least 2 characters in length'
				},
				description: {
					required: 'The Document Description field is required',
					minlength: 'The Document Description field must be at least 2 characters in length'
				}
			},
			invalidHandler: function ( event, validator ) {

				event.preventDefault();

				var alert   =   $('#form_msg');
				alert.closest('div.form-group').removeClass('kt-hide').show();
				KTUtil.scrollTop();
				
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-right",
				  "preventDuplicates": false,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				};

				toastr.error("Please check your fields", "Something went wrong");
			},
			submitHandler: function ( _frm ) {

				_frm[0].submit();
			}
		});
	}

	var _add_ons = function () {

		$('#access_right_id').select2({
        placeholder: 'Select a Access Right',
        tags: true
    });

		$('div.invalid-feedback').each( function () {

    	var _this = $(this);

    	_this.closest('div.form-group').addClass('is-invalid').find('.form-control').addClass('is-invalid');

    	$('#form_msg').closest('div.form-group').removeClass('kt-hide');
    });
	}

	return {

		init: function () {

			_form_validate();
			_add_ons();
		}
	};
}();

jQuery(document).ready(function() {

    DocumentCreate.init();
});