"use strict";

let collectionDate = ""

let initialLoad = false

let receivable_payments_pagination = (page_num) => {

    page_num = page_num ? page_num : 0;

    KTApp.block('#datatable_content', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'calendar_of_events/pagination_receivable_payments/' + page_num,
        method: 'POST',
        data: {
            page: page_num,
            due_date: collectionDate
        },
        success: (html) => {
            $('#datatable_content').html(html);
        },
        complete: () => {
            KTApp.unblock('#kt_content');
        }
    });
}

let post_dated_checks_pagination = (page_num) => {

    page_num = page_num ? page_num : 0;

    KTApp.block('#datatable_content', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'calendar_of_events/pagination_post_dated_checks/' + page_num,
        method: 'POST',
        data: {
            page: page_num,
            due_date: collectionDate
        },
        success: (html) => {
            $('#datatable_content').html(html);
        },
        complete: () => {
            KTApp.unblock('#kt_content');
        }
    });
}


let encoded_post_dated_checks_pagination = (page_num) => {

    page_num = page_num ? page_num : 0;

    KTApp.block('#datatable_content', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'calendar_of_events/encoded_pagination_post_dated_checks/' + page_num,
        method: 'POST',
        data: {
            page: page_num,
            due_date: collectionDate
        },
        success: (html) => {
            $('#datatable_content').html(html);
        },
        complete: () => {
            KTApp.unblock('#kt_content');
        }
    });
}

let encoded_transactions_pagination = (page_num) => {

    page_num = page_num ? page_num : 0;

    KTApp.block('#datatable_content', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'calendar_of_events/pagination_encoded_transactions/' + page_num,
        method: 'POST',
        data: {
            page: page_num,
            date: collectionDate
        },
        success: (html) => {
            $('#datatable_content').html(html);
        },
        complete: () => {
            KTApp.unblock('#kt_content');
        }
    });
}

let reserved_transactions_pagination = (page_num) => {

    page_num = page_num ? page_num : 0;

    KTApp.block('#datatable_content', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'calendar_of_events/pagination_reserved_transactions/' + page_num,
        method: 'POST',
        data: {
            page: page_num,
            due_date: collectionDate
        },
        success: (html) => {
            $('#datatable_content').html(html);
        },
        complete: () => {
            KTApp.unblock('#kt_content');
        }
    });
}

let collected_payments_pagination = (page_num) => {

    page_num = page_num ? page_num : 0;

    KTApp.block('#datatable_content', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'calendar_of_events/pagination_collected_payments/' + page_num,
        method: 'POST',
        data: {
            page: page_num,
            due_date: collectionDate
        },
        success: (html) => {
            $('#datatable_content').html(html);
        },
        complete: () => {
            KTApp.unblock('#kt_content');
        }
    });
}

let encoded_collections_pagination = (page_num) => {

    page_num = page_num ? page_num : 0;

    KTApp.block('#datatable_content', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'calendar_of_events/pagination_encoded_collections/' + page_num,
        method: 'POST',
        data: {
            page: page_num,
            date: collectionDate
        },
        success: (html) => {
            $('#datatable_content').html(html);
        },
        complete: () => {
            KTApp.unblock('#kt_content');
        }
    });
}

let encoded_buyers_pagination = (page_num) => {

    page_num = page_num ? page_num : 0;

    KTApp.block('#datatable_content', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'calendar_of_events/pagination_encoded_buyers/' + page_num,
        method: 'POST',
        data: {
            page: page_num,
            date: collectionDate
        },
        success: (html) => {
            $('#datatable_content').html(html);
        },
        complete: () => {
            KTApp.unblock('#kt_content');
        }
    });
}


let CalendarOfEvents = (() => {

    let Calendar = () => {

        let date_now = moment().startOf('day');
        let date_today = date_now.format('YYYY-MM-DD');
        let calendarEl = document.getElementById('kt_calendar');

        let events = []

        let calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],

            isRTL: KTUtil.isRTL(),
            header: {
                left: 'prev',
                center: 'title',
                // right: 'dayGridMonth,timeGridWeek,timeGridDay'
                right: 'next'
            },

            height: 800,
            contentHeight: 780,
            aspectRatio: 3,  // see: https://fullcalendar.io/docs/aspectRatio

            nowIndicator: true,
            now: date_today,

            views: {
                dayGridMonth: { buttonText: 'month' },
                timeGridWeek: { buttonText: 'week' },
                timeGridDay: { buttonText: 'day' }
            },

            defaultView: 'dayGridMonth',
            defaultDate: date_today,

            editable: true,
            eventLimit: false, // allow "more" link when too many events
            navLinks: true,
            events: events,

            navLinks: false,

            eventClick: (info) => {
                setCollectionsItems(calendar, info.event.start)
            },

            dateClick: (info) => {
                setCollectionsItems(calendar, info.date)
            },

            eventRender: (info) => {
                let element = $(info.el);

                if (info.event.extendedProps && info.event.extendedProps.description) {
                    if (element.hasClass('fc-day-grid-event')) {
                        element.data('content', info.event.extendedProps.description);
                        element.data('placement', 'top');
                        KTApp.initPopover(element);
                    } else if (element.hasClass('fc-time-grid-event')) {
                        element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                    } else if (element.find('.fc-list-item-title').length !== 0) {
                        element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                    }
                }
            }
        });

        calendar.render();

        let date = calendar.getDate()
        let currentYearMonth = moment(date).format("YYYY-MM")

        getEvents(calendar, currentYearMonth)

        $('.fc-prev-button, .fc-next-button, .fc-today-button').on('click', () => {

            $('.fc-prev-button, .fc-next-button').prop('disabled', true);

            let date = calendar.getDate()
            let newYearMonth = moment(date).format("YYYY-MM")

            if (currentYearMonth != newYearMonth) {

                currentYearMonth = newYearMonth

                getEvents(calendar, newYearMonth)
            }
        })
    }

    let getEvents = (calendar, yearMonth) => {
        $.ajax({
            url: base_url + 'calendar_of_events/get_year_month_events',
            type: 'POST',
            dataType: 'JSON',
            data: {
                year_month: yearMonth,
            },
            success: (response) => {
                setEvents(calendar, response)
            },
            error: () => {
                swal.fire(
                    "An error was encounterd",
                    "",
                    "error"
                );
            },
            complete: () => {
                $('.fc-prev-button, .fc-next-button').prop('disabled', false);
            }
        })
    }

    let setEvents = (calendar, events) => {
        // Clear events
        calendar.getEvents().forEach(element => {
            element.remove()
        });


        let date_now = moment().startOf('day');
        let date_today = date_now.format('YYYY-MM-DD');

        events.forEach(element => {
            if (!!element['reserved_transactions']) {
                calendar.addEvent({
                    groupId: 'reserved_transactions',
                    title: element['reserved_transactions']['count'],
                    allDay: true,
                    start: element['date'],
                    className: "fc-event-solid-warning fc-event-light",
                })
            }
            if (!!element['encoded_transactions']) {
                calendar.addEvent({
                    groupId: 'encoded_transactions',
                    title: element['encoded_transactions']['count'],
                    allDay: true,
                    start: element['date'],
                    className: "fc-event-solid-danger fc-event-light fc-event-lime",
                })
            }
            if (!!element['collected_payments']) {
                calendar.addEvent({
                    groupId: 'collected_payments',
                    title: element['collected_payments']['count'],
                    description: '\u20B1 ' + element['collected_payments']['amount'],
                    allDay: true,
                    start: element['date'],
                    className: "fc-event-solid-success fc-event-light",
                })
            }
            if (!!element['receivable_payments']) {
                calendar.addEvent({
                    groupId: 'receivable_payments',
                    title: element['receivable_payments']['count'],
                    description: '\u20B1 ' + element['receivable_payments']['amount'],
                    allDay: true,
                    start: element['date'],
                    className: "fc-event-solid-primary fc-event-light",
                })
            }
            if (!!element['post_dated_checks']) {
                calendar.addEvent({
                    groupId: 'post_dated_checks',
                    title: element['post_dated_checks']['count'],
                    description: '\u20B1 ' + element['post_dated_checks']['amount'],
                    allDay: true,
                    start: element['date'],
                    className: "fc-event-solid-danger fc-event-light",
                })
            }
            if (!!element['encoded_post_dated_checks']) {
                calendar.addEvent({
                    groupId: 'encoded_post_dated_checks',
                    title: element['encoded_post_dated_checks']['count'],
                    description: '\u20B1 ' + element['encoded_post_dated_checks']['amount'],
                    allDay: true,
                    start: element['date'],
                    className: "fc-event-solid-danger fc-event-light fc-event-orange",
                })
            }
            if (!!element['encoded_collections']) {
                calendar.addEvent({
                    groupId: 'encoded_collections',
                    title: element['encoded_collections']['count'],
                    allDay: true,
                    start: element['date'],
                    className: "fc-event-solid-danger fc-event-light fc-event-purple",
                })
            }
            if (!!element['encoded_buyers']) {
                calendar.addEvent({
                    groupId: 'encoded_buyers',
                    title: element['encoded_buyers']['count'],
                    allDay: true,
                    start: element['date'],
                    className: "fc-event-solid-danger fc-event-light fc-event-cyan",
                })
            }
        })

        if (!initialLoad) {

            let defaultDateToLoad = new Date()
            defaultDateToLoad.setHours(0, 0, 0)

            setCollectionsItems(calendar, defaultDateToLoad)

            initialLoad = true
        }
    }

    let setCollectionsItems = (calendar, date) => {
        let items = calendar.getEvents().filter((event) => {

            return event.start == date.toString()
        })

        if (!!items.length) {

            $('#collections_date').html(moment(items[0].start).format('MMMM D, YYYY'))

            collectionDate = moment(items[0].start).format('YYYY-MM-DD')

            // Clear Collections items
            $('.collections_item').removeClass('d-flex').addClass('d-none')

            let showDefaultTable = false

            $('.collections_item').removeClass('collections_item_selected')

            items.forEach(element => {
                switch (element['groupId']) {
                    case 'reserved_transactions':
                        $('#reserved_transactions_count').html(element['title'])

                        $('#reserved_transactions_item').removeClass('d-none').addClass('d-flex')

                        if (!showDefaultTable) {

                            showDefaultTable = true

                            $('#reserved_transactions_item').addClass('collections_item_selected')

                            reserved_transactions_pagination()
                        }

                        break

                    case 'encoded_transactions':
                        $('#encoded_transactions_count').html(element['title'])

                        $('#encoded_transactions_item').removeClass('d-none').addClass('d-flex')

                        if (!showDefaultTable) {

                            showDefaultTable = true

                            $('#encoded_transactions_item').addClass('collections_item_selected')

                            encoded_transactions_pagination()
                        }

                        break

                    case 'collected_payments':
                        $('#collected_payments_amount').html(element.extendedProps.description)
                        $('#collected_payments_count').html(element['title'])

                        $('#collected_payments_item').removeClass('d-none').addClass('d-flex')

                        if (!showDefaultTable) {

                            showDefaultTable = true

                            $('#collected_payments_item').addClass('collections_item_selected')

                            collected_payments_pagination()
                        }

                        break

                    case 'receivable_payments':
                        $('#receivable_payments_amount').html(element.extendedProps.description)
                        $('#receivable_payments_count').html(element['title'])

                        $('#receivable_payments_item').removeClass('d-none').addClass('d-flex')

                        if (!showDefaultTable) {

                            showDefaultTable = true

                            $('#receivable_payments_item').addClass('collections_item_selected')

                            receivable_payments_pagination()
                        }

                        break

                    case 'post_dated_checks':
                        $('#post_dated_checks_amount').html(element.extendedProps.description)
                        $('#post_dated_checks_count').html(element['title'])

                        $('#post_dated_checks_item').removeClass('d-none').addClass('d-flex')

                        if (!showDefaultTable) {

                            showDefaultTable = true

                            $('#post_dated_checks_item').addClass('collections_item_selected')

                            post_dated_checks_pagination()
                        }

                        break

                    case 'encoded_post_dated_checks':
                        $('#encoded_post_dated_checks_amount').html(element.extendedProps.description)
                        $('#encoded_post_dated_checks_count').html(element['title'])

                        $('#encoded_post_dated_checks_item').removeClass('d-none').addClass('d-flex')

                        if (!showDefaultTable) {

                            showDefaultTable = true

                            $('#encoded_post_dated_checks_item').addClass('collections_item_selected')

                            encoded_post_dated_checks_pagination()
                        }

                        break

                    case 'encoded_collections':
                        $('#encoded_collections_count').html(element['title'])

                        $('#encoded_collections_item').removeClass('d-none').addClass('d-flex')

                        if (!showDefaultTable) {

                            showDefaultTable = true

                            $('#encoded_collections_item').addClass('collections_item_selected')

                            encoded_collections_pagination()
                        }

                        break

                    case 'encoded_buyers':
                        $('#encoded_buyers_count').html(element['title'])

                        $('#encoded_buyers_item').removeClass('d-none').addClass('d-flex')

                        if (!showDefaultTable) {

                            showDefaultTable = true

                            $('#encoded_buyers_item').addClass('collections_item_selected')

                            encoded_collections_pagination()
                        }

                        break
                }
            })
        }
    }

    let setListeners = () => {
        $('#receivable_payments_item').on('click', () => {

            $('.collections_item').removeClass('collections_item_selected')

            $('#receivable_payments_item').addClass('collections_item_selected')

            receivable_payments_pagination()
        })

        $('#post_dated_checks_item').on('click', () => {

            $('.collections_item').removeClass('collections_item_selected')

            $('#post_dated_checks_item').addClass('collections_item_selected')

            post_dated_checks_pagination()
        })

        $('#reserved_transactions_item').on('click', () => {

            $('.collections_item').removeClass('collections_item_selected')

            $('#reserved_transactions_item').addClass('collections_item_selected')

            reserved_transactions_pagination()
        })

        $('#encoded_transactions_item').on('click', () => {

            $('.collections_item').removeClass('collections_item_selected')

            $('#encoded_transactions_item').addClass('collections_item_selected')

            encoded_transactions_pagination()
        })

        $('#collected_payments_item').on('click', () => {

            $('.collections_item').removeClass('collections_item_selected')

            $('#collected_payments_item').addClass('collections_item_selected')

            collected_payments_pagination()
        })

        $('#encoded_collections_item').on('click', () => {

            $('.collections_item').removeClass('collections_item_selected')

            $('#encoded_collections_item').addClass('collections_item_selected')

            encoded_collections_pagination()
        })

        $('#encoded_buyers_item').on('click', () => {

            $('.collections_item').removeClass('collections_item_selected')

            $('#encoded_buyers_item').addClass('collections_item_selected')

            encoded_buyers_pagination()
        })
    }

    let getSalesAndCollections = () => {

        let previousWeekSales, thisWeekSales, previousWeekCollections, thisWeekCollections = 0

        $.ajax({
            url: base_url + "transaction/total_week_sales",
            type: "POST",
            dataType: "JSON",
            data: {
                filter: 1
            },
            success: function (response) {
                previousWeekSales = response.raw_amount

                $('#previous_week_sales_date').html(response.week)
                $('#previous_week_sales_amount').html(response.amount)
                $('#previous_week_sales_count').html('<span>Count: ' + response.count + '</span>')
            },
        }).then(() => {
            $.ajax({
                url: base_url + "transaction/total_week_sales",
                type: "POST",
                dataType: "JSON",
                data: {
                    filter: 0
                },
                success: function (response) {
                    thisWeekSales = response.raw_amount

                    $('#this_week_sales_date').html(response.week)
                    $('#this_week_sales_count').html('<span>Count: ' + response.count + '</span>')

                    if (previousWeekSales > thisWeekSales) {
                        $('#this_week_sales_amount').html('<div class="text-danger"><span><i class="fa fa-arrow-down"></i></span><span>' + response.amount + '</span></div>')
                    } else if (thisWeekSales > previousWeekSales) {
                        $('#this_week_sales_amount').html('<div class="text-success"><span><i class="fa fa-arrow-up"></i></span><span>' + response.amount + '</span></div>')
                    } else {
                        $('#this_week_sales_amount').html('<div><span>' + response.amount + '</span></div>')
                    }
                },
            }).then(() => {
                $("#sales_links").removeClass('d-none')
            })
        })

        $.ajax({
            url: base_url + "transaction_payment/total_week_payments",
            type: "POST",
            dataType: "JSON",
            data: {
                filter: 1
            },
            success: function (response) {
                previousWeekCollections = response.raw_amount

                $('#previous_week_collections_date').html(response.week)
                $('#previous_week_collections_amount').html(response.amount)
                $('#previous_week_collections_count').html('<span>Count: ' + response.count + '</span>')
            },
        }).then(() => {
            $.ajax({
                url: base_url + "transaction_payment/total_week_payments",
                type: "POST",
                dataType: "JSON",
                data: {
                    filter: 0
                },
                success: function (response) {
                    thisWeekCollections = response.raw_amount

                    $('#this_week_collections_date').html(response.week)
                    $('#this_week_collections_count').html('<span>Count: ' + response.count + '</span>')

                    if (previousWeekCollections > thisWeekCollections) {
                        $('#this_week_collections_amount').html('<div class="text-danger"><span><i class="fa fa-arrow-down"></i></span><span>' + response.amount + '</span></div>')
                    } else if (thisWeekCollections > previousWeekCollections) {
                        $('#this_week_collections_amount').html('<div class="text-success"><span><i class="fa fa-arrow-up"></i></span><span>' + response.amount + '</span></div>')
                    } else {
                        $('#this_week_collections_amount').html('<div><span>' + response.amount + '</span></div>')
                    }
                },
            }).then(() => {
                $("#collections_links").removeClass('d-none')
            })
        })
    }

    return {
        init: () => {
            Calendar()
            setListeners()
            getSalesAndCollections()
        }
    };
})();

jQuery(document).ready(() => {
    CalendarOfEvents.init();
});