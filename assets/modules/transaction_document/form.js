// Class definition
var TransactionCreate = function() {
    // Private functions
    var sgSelect = function() {

		$("#sales_group").on("change", function () {
			$("#transaction_type option[value]").remove();
		});

	}
	
	var formRepeater = function() {
		
		$('#fees_form_repeater,#promos_form_repeater,#buyer_form_repeater,#seller_form_repeater').repeater({
			initEmpty: false,
           
            defaultValues: {
            },
             
            show: function () {
				$(this).slideDown();
            },

            hide: function (deleteElement) {                
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }               
            }   
		});
		
	}


	
	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	// Private functions
	var datepicker = function () {
		// minimum setup
		$('.datePicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			locale: 'no',
			format: 'yyyy-mm-dd',
			autoclose: true
		});

		$('.yearPicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years",
			autoclose: true
		});
	}
	
	// Base elements
	var wizardEl;
	var formEl;
	var validator;
	var wizard;
	
	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		wizard = new KTWizard('kt_wizard_v3', {
			startStep: 1,
		});

		// Validation before going to next page
		wizard.on('beforeNext', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
			Select.init();
		});

		wizard.on('beforePrev', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
		});

		// Change event
		wizard.on('change', function(wizard) {
			KTUtil.scrollTop();	
		});
	}

	var initValidation = function() {
		validator = formEl.validate({
			// Validate only visible fields
			ignore: ":hidden",

			// Validation rules
			rules: {
			    "property[project_id]": {
		            required: true 
				},
				"property[model_id]": {
		            required: true 
				},
				"property[interior_id]": {
		            required: true 
				},
				"property[lot_area]": {
		            required: true 
				},
				"property[floor_area]": {
		            required: true 
				},
				"property[lot_price_per_sqm]": {
		            required: true 
				},
				"property[total_lot_price]": {
		            required: true 
				},
				"property[model_price]": {
		            required: true 
				},
				"property[property_id]": {
		            required: true 
				},
				"property[total_selling_price]": {
		            required: true 
				},
				"property[total_contract_price]": {
		            required: true 
				},
				"property[collectible_price]": {
		            required: true 
				},
				"property[commissionable_value]": {
		            required: true 
				},
				"property[commissionable_amount]": {
		            required: true 
				},
				"billing[reservation_date]": {
		            required: true 
				},
				"billing[expiration_date]": {
		            required: true 
				},
				"billing[financing_scheme_id]": {
		            required: true 
				},
				"client[client_id]": {
		            required: true 
				},
				"client[client_type]": {
		            required: true 
				},
				"seller[seller_id]": {
		            required: true 
				},
				"seller[seller_position_id]": {
		            required: true 
				},
				"seller[sellers_rate]": {
		            required: true 
				},
				"seller[sellers_rate_amount]": {
		            required: true 
				},
            },
			// Display error  
			invalidHandler: function(event, validator) {	 
				KTUtil.scrollTop();
				swal.fire({
					"title": "", 
					"text": "There are some errors in your submission. Please correct them.", 
					"type": "error",
					"confirmButtonClass": "btn btn-secondary"
				});
			},
			// Submit valid form
			submitHandler: function (form) {
				
			}
		});   
	}

	var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formEl.ajaxSubmit({
                    type: 'POSt',
                    dataType: 'JSON',
					success: function(response) {
						if (response.status) {
							swal.fire({
								title: "Success!",
								text: response.message,
								type: "success"
							}).then(function() {
								window.location.replace(base_url + "transaction");
							});

						} else {
							swal.fire(
								'Oops!',
								response.message,
								'error'
							)
						}
					}
				});
			}
		});
	}

    // Public functions
    return {
        init: function() {
			sgSelect();
			datepicker(); 
			formRepeater();

			wizardEl = KTUtil.get('kt_wizard_v3');
			formEl = $('#form_transaction');

			initWizard(); 
			initValidation();
			initSubmit();

        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    TransactionCreate.init();
});