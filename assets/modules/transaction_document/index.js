'use strict';
var TransactionDocument = (function () {

    var image_file_types = ['image/jpeg', 'image/png', 'image/jpg']

    var TransactionDocumentTable = function () {
        var table = $('#_transaction_document_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,

            ajax: base_url + 'transaction_document/showTransactionDocuments',
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'transaction_reference',
                },
                {
                    data: 'project'
                },
                {
                    data: 'property'
                },
                {
                    data: 'buyer'
                },
                {
                    data: 'document_name',
                },
                {
                    data: 'owner',
                },
                {
                    data: 'category_id',
                },
                {
                    data: 'checklist',
                },
                {
                    data: 'file',
                },
                {
                    data: 'timeline',
                },
                {
                    data: 'start_date',
                },
                {
                    data: 'due_date',
                },
                {
                    data: 'status',
                },
                {
                    data: 'has_file',
                },
                {
                    data: 'transaction_document_stage_id',
                },
                {
                    data: 'remarks',
                },
                {
                    data: '',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                /* ==================== end: Add model fields ==================== */
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: 18,
                    title: 'File',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {

                        if (row.has_file == 1) {
                            return (
                                `<span class="d-flex"><button class="btn btn-linkedin btn-sm btn-elevate btn-icon _upload_file mr-2" data-toggle="modal" data-target="#_upload_modal" data-document-id="`
                                + row.document_id +
                                `" data-transaction-id="`
                                + row.transaction_id +
                                `" data-id=" `
                                + row.id +
                                `" data-has-file="`
                                + row.has_file +
                                `"><i class="fa fa-pen"></i></button>`
                                +
                                `<button class="btn btn-linkedin btn-sm btn-elevate btn-icon _view_file" id="_view_file_" data-toggle="modal" data-target="#_view_file_modal" data-id="`
                                + row.uploaded_document_id +
                                `">
                                <i class="la la-eye"></i>
                                </button></span>`
                            )
                        } else {
                            return (
                                `<span><button class="btn btn-linkedin btn-sm btn-elevate btn-icon _upload_file mr-2" data-toggle="modal" data-target="#_upload_modal" data-document-id="`
                                + row.document_id +
                                `" data-transaction-id="`
                                + row.transaction_id +
                                `"><i class="fa fa-pen"></i></button></span>`
                            )
                        }
                    },
                },
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        
                        return (
                            `<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md update_status" title="Edit" data-id="` + row.id + `" data-status="` + row.transaction_status + `" data-remarks="` + row.remarks + `" data-toggle="modal" data-target="#update_status_modal"><i class="la la-edit"></i></button>`
                        )
                    },
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: 'dt-center',
                    // orderable: false,
                },
                {
                    targets: [0, 1, 3, 4, 5, 6, 7, 8, 10],
                    className: 'dt-center',
                },
                {
                    // targets: [0, 4, 6, 7, 8, 10],
                    targets: [0, 7, 9, 10, 11, 13],
                    orderable: false,
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                /* ==================== begin: Add target fields for dropdown value ==================== */

                {
                    targets: 7,
                    render: function (data, type, row, meta) {
                        // '1' => 'Legal Staff',
                        // '2' => 'Legal Head',
                        // '3' => 'Agent',
                        // '4' => 'Sales Admin',
                        // '5' => 'AR Staff',
                        // '6' => 'AR Head',
                        // '7' => 'Titling Officer',
                        // '8' => 'Tax',
                        // '9' => 'Engineering',
                        // '10' => 'Cashier',
                        // '11' => 'Sales Coordinator',
                        // '12' => 'Client',
                        // '13' => 'Credit and Collection'
                        var owner = {
                            1: {
                                title: 'Legal Staff',
                            },
                            2: {
                                title: 'Legal Head',
                            },
                            3: {
                                title: 'Agent',
                            },
                            4: {
                                title: 'Sales Admin',
                            },
                            5: {
                                title: 'AR Staff',
                            },
                            6: {
                                title: 'AR Head',
                            },
                            7: {
                                title: 'Titling Officer',
                            },
                            8: {
                                title: 'Tax',
                            },
                            9: {
                                title: 'Engineering',
                            },
                            10: {
                                title: 'Cashier',
                            },
                            11: {
                                title: 'Sales Coordinator',
                            },
                            12: {
                                title: 'Client',
                            },
                            13: {
                                title: 'Credit and Collection',
                            },
                        };

                        if (typeof owner[data] === 'undefined') {
                            return ``;
                        }
                        return owner[data].title;
                    },
                },

                {
                    targets: 8,
                    render: function (data, type, row, meta) {
                        // '1' => 'General',
                        // '2' => 'Land Inventory',
                        // '3' => 'Licensing',
                        // '4' => 'Sales',
                        // '5' => 'Construction',
                        // '6' => 'Financing'
                        var category = {
                            1: {
                                title: 'General',
                            },
                            2: {
                                title: 'Land Inventory',
                            },
                            3: {
                                title: 'Licensing',
                            },
                            4: {
                                title: 'Sales',
                            },
                            5: {
                                title: 'Construction',
                            },
                            6: {
                                title: 'Financing',
                            },
                        };

                        if (typeof category[data] === 'undefined') {
                            return ``;
                        }
                        return category[data].title;
                    },
                },
                {
                    targets: 9,
                    render: function (data, type, row, meta) {
                        return `Reservation Checklist`;
                    },
                },
                {
                    targets: [10, 15],
                    visible: false,
                },
                {
                    targets: 15,
                    render: function (data, type, row, meta) {
                        if (!data) {
                            return 'False';
                        } else {
                            return 'True';
                        }
                    },
                },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };

    // var confirmDelete = function () {
    //     $(document).on('click', '.remove_warehouse', function () {
    //         var id = $(this).data('id');

    //         swal.fire({
    //             title: 'Are you sure?',
    //             text: "You won't be able to revert this!",
    //             type: 'warning',
    //             showCancelButton: true,
    //             confirmButtonText: 'Yes, delete it!',
    //             cancelButtonText: 'No, cancel!',
    //             reverseButtons: true,
    //         }).then(function (result) {
    //             if (result.value) {
    //                 $.ajax({
    //                     url: base_url + 'warehouse/delete/' + id,
    //                     type: 'POST',
    //                     dataType: 'JSON',
    //                     data: {
    //                         id: id,
    //                     },
    //                     success: function (res) {
    //                         if (res.status) {
    //                             $('#_transaction_document_table')
    //                                 .DataTable()
    //                                 .ajax.reload();
    //                             swal.fire('Deleted!', res.message, 'success');
    //                         } else {
    //                             swal.fire('Oops!', res.message, 'error');
    //                         }
    //                     },
    //                 });
    //             } else if (result.dismiss === 'cancel') {
    //                 swal.fire(
    //                     'Cancelled',
    //                     'Your imaginary file is safe :)',
    //                     'error'
    //                 );
    //             }
    //         });
    //     });
    // };

    // var upload_guide = function () {
    //     $(document).on('click', '#btn_upload_guide', function () {
    //         var table = $('#upload_guide_table');

    //         table.DataTable({
    //             order: [[0, 'asc']],
    //             pagingType: 'full_numbers',
    //             lengthMenu: [5, 10, 25, 50, 100],
    //             pageLength: 10,
    //             responsive: true,
    //             searchDelay: 500,
    //             processing: true,
    //             serverSide: true,
    //             deferRender: true,
    //             ajax: base_url + 'warehouse/get_table_schema',
    //             columns: [
    //                 // {data: 'button'},
    //                 {
    //                     data: 'no',
    //                 },
    //                 {
    //                     data: 'name',
    //                 },
    //                 {
    //                     data: 'type',
    //                 },
    //                 {
    //                     data: 'format',
    //                 },
    //                 {
    //                     data: 'option',
    //                 },
    //                 {
    //                     data: 'required',
    //                 },
    //             ],
    //             // drawCallback: function ( settings ) {

    //             // }
    //         });
    //     });
    // };

    var _add_ons = function () {
        var dTable = $('#_transaction_document_table').DataTable();

        $('#_search').on('keyup', function () {
            dTable.search($(this).val()).draw();
        });

        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });

        $(document).on('click', '._upload_file', function () {

            let document_id = $(this).data('document-id')
            let transaction_id = $(this).data('transaction-id')

            $('form#_upload_form').attr('action', base_url + 'transaction_document/upload_file/' + transaction_id + '/' + document_id);
        })

        $('.modal').on('hidden.bs.modal', function () {

            $("#download").removeClass('d-none')

            $(this).find('form').trigger('reset');

            $('form').find('.kt-avatar__holder').css('background-image', 'url(' + base_url + 'assets/img/default/_img.png' + ')');
        });

        $(document).on('click', ('.update_status'), function () {

            $('#select_option').attr('selected', 'selected')
            $('#pending_option').removeAttr('selected')
            $('#completed_option').removeAttr('selected')
            
            if ($(this).attr('data-remarks') != 'null') {
                $('#remarks').val($(this).attr('data-remarks'))
            } else {
                $('#remarks').val(null)
            }

            if ($(this).attr('data-status') == "1") {

                $('#pending_option').attr('selected', 'selected')
            } else if ($(this).attr('data-status') == "2") {

                $('#completed_option').attr('selected', 'selected')
            }

            $('#transaction_document_id').val($(this).attr('data-id'))
        })

        $(document).on('click', '#submit_update_status', function () {
            $.ajax({
                url: base_url + 'transaction_document/update_status/' + $('#transaction_document_id').val(),
                type: 'POST',
                dataType: 'JSON',
                data: {
                    transaction_status: $('#transaction_status').val(),
                    remarks: $('#remarks').val()
                },
                success: function (response) {
                    if (response.status) {
                        $('#_transaction_document_table')
                            .DataTable()
                            .ajax.reload();
                        swal.fire('Updated!', response.message, 'success');
                    } else {
                        swal.fire('Oops!', response.message, 'error');
                    }
                },
                complete: function () {
                    $('#update_status_modal').modal('hide');
                }
            });
        })
    }

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var _filterItemBrand = function () {
        var dTable = $('#_transaction_document_table').DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($('#_column_' + n).val())
                .draw();
        }

        $('._filter').on('keyup change clear', function () {
            if ($(this).is('input')) {
                let val = $('#_column_' + $(this).data('column')).val();

                dTable.search(val).draw();
            } else {
                _colFilter($(this).data('column'));
            }
        });
    };

    var _selectProp = function () {
        var _table = $('#_transaction_document_table').DataTable();
        var _buttons = _table.buttons(['.bulkDelete']);

        $('#select-all').on('click', function () {
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
            } else {
                $('.delete_check').prop('checked', false);
            }
        });

        $('#_transaction_document_table tbody').on(
            'change',
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $('input#select-all').get(0);

                    if (el && el.checked && 'indeterminate' in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'warehouse/bulkDelete/',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#_transaction_document_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    var _response = function () {

        var _loader = '<div class="kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--success"></div>';

        $(document).on('click', '._view_file', function () {

            var id = $(this).data('id');

            $('._display_process_document_image').attr('id', '_display_process_document_image_' + id);

            $('#_display_process_document_image_' + id).html(_loader);

            if (id) {
                console.log(id)
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: base_url + 'document/view_transaction_document_file',
                    data: { id: id },
                    success: function (_respo) {
                        if (_respo._status) {

                            $('#_display_process_document_image_' + id).html(_respo._html);

                            if (image_file_types.includes(_respo.ext)) {

                                $("#download").addClass('d-none')
                                console.log('includes')
                            } else {

                                $("#download").attr('href', _respo.src).attr("download", _respo.name)
                            }
                        } else {

                            swal.fire(
                                'Oops!',
                                _respo._msg,
                                'error'
                            );
                        }
                    }
                });
            }
        });
    }

    var loadFile = function (event) {
        $('#_file').change(function () {
            var file = this.files[0];
            var reader = new FileReader();

            $('.kt-avatar__holder').removeClass('d-flex align-items-center justify-content-center')
                .html("")

            if (file) {

                if (image_file_types.includes(file.type)) {

                    reader.onloadend = function () {
                        $('.kt-avatar__holder').css('background-image', 'url("' + reader.result + '")');
                    }
                    reader.readAsDataURL(file);
                } else {
                    $('.kt-avatar__holder').addClass('d-flex align-items-center justify-content-center').html('<b>' + file.name + '</b>').css('background-image', 'none')
                }
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            // confirmDelete();
            TransactionDocumentTable();
            _add_ons();
            _filterItemBrand();
            // upload_guide();
            status();
            _selectProp();
            _response()
            loadFile()
        },
    };
})();

jQuery(document).ready(function () {
    TransactionDocument.init();
});
