// Class definition
var FormGen = function() {
    // Private functions
    var sgSelect = function() {
        
		// Sort Type
		$('#sort_by').select2({
            placeholder: "Select option",
            allowClear: true
        });

        $("#sort_by").on("select2:select", function (evt) {
          var element = evt.params.data.element;
          var $element = $(element);
          
          $element.detach();
          $(this).append($element);
          $(this).trigger("change");
        });
    }
    
    var daterangepickerInit = function () {

        var picker = $('#daterangepicker');

        picker.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        var picker = $('#reservation_daterangepicker');

        picker.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    };
    

    // Public functions
    return {
        init: function() {
            daterangepickerInit();
            sgSelect();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    FormGen.init();
    datepicker();
});