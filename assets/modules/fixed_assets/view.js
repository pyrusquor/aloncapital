var FixedAsset = (function () {
    var removeAsset = function (){
        $(document).on('click', '.remove_asset', function () {
            var delete_id = $(this).data('id');
            console.log('test');
            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'fixed_assets/delete/',
                        type: 'POST',
                        data: {
                            deleteids_arr : [delete_id]
                        },
                        dataType: 'JSON',
                        success: function (res){
                            if(res.status){
                                if (res.status) {
                                    swal.fire("Deleted!", res.message, "success");
                                    window.location = base_url + 'fixed_assets'
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            }
                        }
                    })
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            })
        })
    };
    return{
        init: function (){
            removeAsset();
        }
    }
})();

jQuery(document).ready(function () {
    FixedAsset.init();
});