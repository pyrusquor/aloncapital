"use strict";
var KTDatatablesLandInventoryDocumentChecklist = function () {

	var _initLandInventoryDocumentChecklist = function () {

		var _table = $('#_land_inventory_document_table');

		_table.DataTable({
			order: [[1, 'desc']],
			pagingType: 'full_numbers',
			lengthMenu: [5, 10, 25, 50, 100],
			pageLength : 10,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			deferRender: true,
			dom:	"<'row'<'col-sm-12 col-md-6'l>>" +
						"<'row'<'col-sm-12'tr>>" +
						"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			language: {
				'lengthMenu': 'Show _MENU_',
        'infoFiltered': '(filtered from _MAX_ total records)'
			},
			ajax: base_url + 'land/get_document_checklist/'+_li_id,
			columns: [
				{
					data: null,
					orderable: false,
					searchable: false,
					render: function ( data, type, row, meta ) {

						return `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="`+row.id+`" class="m-checkable _select" data-id="`+row.id+`">
                      <span></span>
	                  </label>`;
					}
				},
				{data: 'id'},
				{data: 'name'},
				{data: 'description'},
				{data: 'owner_id'},
				{data: 'classification_id'},
				{data: 'updated_at'},
				{data: 'Actions', responsivePriority: -1}
			],
			columnDefs: [
				{
					targets: -1,
					orderable: false,
					render: function ( data, type, row, meta ) {

						return `
                    <a href="`+base_url+`document/view/`+row.id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                      <i class="la la-eye"></i>
                    </a>
                  `;
					}
				},
				{
					targets: [0, 1, 4, 5, 6, -1],
					className: 'dt-center',
				},
				{
					targets: 4,
					render: function (data, type, full, meta) {

						var _owner = {
							1: { title: 'Legal Staff' },
							2: { title: 'Legal Head' },
							3: { title: 'Agent' },
							4: { title: 'Sales Admin' },
							5: { title: 'AR Staff' },
							6: { title: 'AR Head' },
							7: { title: 'Titling Officer' },
							8: { title: 'Tax' },
							9: { title: 'Engineering' },
							10: { title: 'Cashier' },
							11: { title: 'Sales Coordinator' },
							12: { title: 'Client' },
							13: { title: 'Credit and Collection' }
						};

						if ( typeof _owner[data] === 'undefined' ) {

							return ``;
						}

						return _owner[data].title;
					}
				},
				{
					targets: 5,
					render: function (data, type, full, meta) {

						var _classification = {
							1: { title: 'Client Document' },
							2: { title: 'Company Document' },
							3: { title: 'Government Document' }
						};

						if ( typeof _classification[data] === 'undefined' ) {

							return ``;
						}

						return _classification[data].title;
					}
				}
			],
			drawCallback: function ( settings ) {

				$('span#_total').text(settings.fnRecordsTotal()+' TOTAL');
			}
		});
	}

	var _selectProp = function () {

		var _table    = $('#_land_inventory_document_table').DataTable();

		$('input#select-all').on( 'click', function () {

			// var rows = _table.rows({ 'search': 'applied' }).nodes();

			// $('input[type="checkbox"]').prop( 'checked', this.checked );

			if ( this.checked ) {

				$('input._select').each( function () {

					this.checked = true;
				});
			} else {

				$('input._select').each( function () {

					this.checked = false;
				});
			}
		});

		$('#_land_inventory_document_table tbody').on( 'change', 'input._select', function () {

			if ( this.checked == false ) {

				$('input#select-all').prop('checked', false);
			}
		});

		// $(document).on( 'change', '.m-checkable', function () {

		// 	var _checked = $('.m-checkable:checked').length;

		// 	if ( _checked === 0  ) {

		// 		_table.button(0).enable(false);
		// 	} else {

		// 		_table.button(0).enable(true);
		// 	}
		// });

		// $('#bulkDelete').click(function(){

		// 	var deleteids_arr = [];
		// 	// Read all checked checkboxes
		// 	$('input[name="id[]"]:checked').each(function () {
		// 	   deleteids_arr.push($(this).val());
		// 	});
	  
		// 	// Check checkbox checked or not
		// 	if(deleteids_arr.length > 0){
	  
		// 		swal.fire({
		// 			title: 'Are you sure?',
		// 			text: "You won't be able to revert this!",
		// 			type: 'warning',
		// 			showCancelButton: true,
		// 			confirmButtonText: 'Yes, delete it!',
		// 			cancelButtonText: 'No, cancel!',
		// 			reverseButtons: true
		// 		}).then(function (result) {
		// 			if (result.value) {

		// 				$.ajax({
		// 					url: base_url + 'document/bulkDelete/',
		// 					type: 'POST',
		// 					dataType: "JSON",
		// 					data: { deleteids_arr: deleteids_arr },
		// 					success: function (res) {
		// 						if (res.status) {
		// 							$('#_land_inventory_document_table').DataTable().ajax.reload();
		// 							swal.fire(
		// 								'Deleted!',
		// 								res.message,
		// 								'success'
		// 							);
		// 						} else {
		// 							swal.fire(
		// 								'Oops!',
		// 								res.message,
		// 								'error'
		// 							)
		// 						}
		// 					}
		// 				});

		// 			} else if (result.dismiss === 'cancel') {
		// 				swal.fire(
		// 					'Cancelled',
		// 					'Your imaginary file is safe :)',
		// 					'error'
		// 				)
		// 			}
		// 		});
		// 	}
		// });
	}

	var _add_ons = function () {

		var dTable = $('#_land_inventory_document_table').DataTable();

		$('#_search').on( 'keyup', function () {

			dTable.search($(this).val()).draw();
		});

		$('#kt_datepicker').datepicker({
    	orientation: "bottom left",
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

		$('button#_advance_search_btn').on( 'click', function () {

			$('div#_batch_upload').removeClass('show');
		});

		$('button#_batch_upload_btn').on( 'click', function () {

			$('div#_advance_search').removeClass('show');
		});
	}

	var _filterDocumentChecklist = function () {

		var dTable = $('#_land_inventory_document_table').DataTable();

		function _colFilter ( n ) {

			dTable.column(n).search($('#_column_'+n).val()).draw();
		}

		$('._filter').on( 'keyup change clear', function () {

			_colFilter($(this).data('column'));
		});
	}

	return {

		init: function () {

			_initLandInventoryDocumentChecklist();
			_add_ons();
			_filterDocumentChecklist();
			_selectProp();
		}
	};
}();

jQuery(document).ready(function() {

    KTDatatablesLandInventoryDocumentChecklist.init();
});