"use strict";
var KTDatatablesLandInventory = (function () {

	let tableElement = $('#_land_inventory_table');

	var _initDocument = function () {
		
		var dataTable = tableElement.DataTable({
			order: [[1, "desc"]],
			pagingType: "full_numbers",
			lengthMenu: [5, 10, 25, 50, 100],
			pageLength: 10,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			serverMethod: 'post',
			deferRender: true,
			ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'land/showItems',
            },
			dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
				// "<'row'<'col-sm-12 col-md-6'l>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			buttons: [
				{
				text: '<i class="fa fa-bezier-curve"></i> Consolidate and Subdivide',
				className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm _consolidate_and_subdivide",
				action: function (e, node, config) {
					$("#_consolidate_and_subdivide_modal").modal("show");
					populateModal();
				},
				enabled: false,
				},
				{
				text: '<i class="fa fa-bezier-curve"></i> Consolidate',
				className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm _consolidate",
				action: function (e, node, config) {
					$("#_consolidate_modal").modal("show");
					populateModal();
				},
				enabled: false,
				},
				{
				text: '<i class="fa fa-project-diagram"></i> Subdivide',
				className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm _subdivide",
				action: function (e, node, config) {
					$("#_subdivide_modal").modal("show");
					populateModal();
				},
				enabled: false,
				},
				{
				text: '<i class="fa fa-trash"></i> Delete Selected',
				className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm _bulk_delete",
				attr: {
					id: "bulkDelete",
				},
				enabled: false,
				},
			],
			language: {
				lengthMenu: "Show _MENU_",
				infoFiltered: "(filtered from _MAX_ total records)",
			},
			columns: [{
				data: null,
				orderable: false,
				searchable: false,
				render: function (data, type, row, meta) {
				_checkSelectedAll();
				return (
					`
							<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<input type="checkbox" name="id[]" value="` +
					row.id +
					`" class="m-checkable __select" data-id="` +
					row.id +
					`">
							<span></span>
							</label>`
				);
				},
			},
			{
				data: "id",
			},
			{
				data: "company_id",
			},
			{
				data: "land_owner_name",
			},
			{
				data: "title",
			},
			{
				data: "lot_number",
			},
			{
				data: "location",
			},
			{
				data: "land_area",
			},
			{
				data: "land_classification_id",
			},
			{
				data: "status",
			},
			{
				data: "created_by",
			},
			{
				data: "updated_by",
			},
			{
				data: "Actions",
				responsivePriority: -1,
			},
			],
			columnDefs: [{
				targets: -1,
				orderable: false,
				render: function (data, type, row, meta) {
				return (
					`
							<span class="dropdown">
							<a href="#" class="btn btn-xs btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
								<i class="la la-ellipsis-h"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<a href="` +
					base_url +
					`land/document_checklist/` +
					row.id +
					`" class="dropdown-item"><i class="la la-book"></i> Document Checklist</a>
								<a href="` +
					base_url +
					`land/process_checklist/` +
					row.id +
					`" class="dropdown-item"><i class="la la-tasks"></i> Process Checklist</a>
					<a href="` +
					base_url +
					`payment_request/form?land_inventory_id=` + row.id + `" class="dropdown-item"><i class="fa fa-plus"></i> Create Payment Request</a>
					<a href="` +
					base_url +
					`payment_voucher/form" class="dropdown-item"><i class="fa fa-plus"></i> Create Payment Voucher</a>
								<a href="` +
					base_url +
					`land/update/` +
					row.id +
					`" class="dropdown-item"><i class="la la-edit"></i> Update</a>
								<button type="button" class="dropdown-item _delete_docx" data-id="` +
					row.id +
					`"><i class="la la-trash"></i> Delete</button>
							</div>
							</span>
							<a href="` +
					base_url +
					`land/view/` +
					row.id +
					`" class="btn btn-xs btn-clean btn-icon btn-icon-md" title="View">
							<i class="la la-eye"></i>
							</a>
						`
				);
				},
			},
			{
				targets: [1, 4, 5, 6, 7, 8, 9, -1],
				className: "dt-center",
				orderable: true,
			},
			{
				targets: 0,
				checkboxes: {
				selectRow: true,
				},
			},
				// {
				//     targets: 10,
				//     searchable: true,
				//     visible: true,
				//     render: function (data, type, full, meta) {
				//         if (data) {
				//             const {
				//                 company
				//             } = full;

				//             return company.name
				//         } else {

				//             return ``
				//         }
				//     },
				// },
			],
			// select: {
			// 	style: 'multi',
			// 	selector: 'td:first-child'
			// },
			drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
		});

		function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
	};

	var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

	const _checkSelectedAll = () => {
		var _table = $("#_land_inventory_table").DataTable();
		var _buttons = _table.buttons([
		"._consolidate",
		"._subdivide",
		"._bulk_delete",
		]);
		const selectAllElm = document.querySelector("input#select-all");
		$(document).ready(function () {
		if (selectAllElm.checked) {
			const rows = document.querySelectorAll("input.__select");
			$(rows).each(function () {
			this.checked = true;
			});
		} else {
			const rows = document.querySelectorAll("input.__select");
			$(rows).each(function () {
			this.checked = false;
			});
		}

		var _checked = $("input.__select:checked").length;

		if (_checked === 0) {
			_buttons.disable();
		} else {
			_table.button(2).enable(_checked > 0);
			_table.button(1).enable(_checked === 1);
			_table.button(0).enable(_checked > 1);
		}
		});
	};

	$(document).on("click", ".page-link", function () {
		_checkSelectedAll();
	});

	var _selectProp = function () {
		var _table = $("#_land_inventory_table").DataTable();
		var _buttons = _table.buttons([
		"._consolidate_and_subdivide",
		"._consolidate",
		"._subdivide",
		"._bulk_delete",
		]);

		// var _buttons  = _table.buttons( ['._consolidate', '._subdivide'] );

		$("input#select-all").on("click", function () {
		if (this.checked) {
			$("input.__select").each(function () {
			this.checked = true;
			});
		} else {
			$("input.__select").each(function () {
			this.checked = false;
			});
		}

		var _checked = $("input.__select:checked").length;

		if (_checked === 0) {

			_buttons.disable();
		} else {

			_table.button(0).enable(_checked > 0);  // Consolidate and Subdivide
			_table.button(1).enable(_checked > 1);  // Consolidate
			_table.button(2).enable(_checked === 1);  // Subdivide
			_table.button(3).enable(_checked > 0);  // Delete
		}
		});

		$(document).on("click", ".paginate_button.page-link", function () {
		_checkSelectedAll();
		console.log("clicked");
		});

		$(document).on("change", "input.__select", function () {
		var _checked = $("input.__select:checked").length;

		if (_checked === 0) {
			_buttons.disable();
		} else {
			// _table.button(2).enable(_checked > 0);
			// _table.button(1).enable(_checked === 1);
			// _table.button(0).enable(_checked > 1);
			_table.button(0).enable(_checked > 0);  // Consolidate and Subdivide
			_table.button(1).enable(_checked > 1);  // Consolidate
			_table.button(2).enable(_checked === 1);  // Subdivide
			_table.button(3).enable(_checked > 0);  // Delete
		}

		if (this.checked == false) {
			$("input#select-all").prop("checked", false);
		}
		});

		$("#bulkDelete").click(function () {
		var deleteids_arr = [];
		// Read all checked checkboxes
		$('input[name="id[]"]:checked').each(function () {
			deleteids_arr.push($(this).val());
		});

		// Check checkbox checked or not
		if (deleteids_arr.length > 0) {
			swal
			.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel!",
				reverseButtons: true,
			})
			.then(function (result) {
				if (result.value) {
				$.ajax({
					url: base_url + "land/bulkDelete/",
					type: "POST",
					dataType: "JSON",
					data: {
					deleteids_arr: deleteids_arr,
					},
					success: function (res) {
					if (res.status) {
						$("#_land_inventory_table").DataTable().ajax.reload();
						swal.fire("Deleted!", res.message, "success");
					} else {
						swal.fire("Oops!", res.message, "error");
					}
					},
				});

				$("input#select-all").prop("checked", false);
				} else if (result.dismiss === "cancel") {
				swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
				}
			});
		}
		});
	};

	var _processLand = function () {
		$("#_consolidate_form").validate({
		rules: {
			land_owner_name: {
			required: true,
			minlength: 2,
			},
			location: {
			required: true,
			minlength: 2,
			},
			land_area: {
			required: true,
			number: true,
			},
			land_classification_id: {
			required: true,
			},
			// lot_number: {
			// 	number: true
			// },
			status: {
			required: true,
			},
			company: {},
			remarks: {},
		},
		messages: {
			land_owner_name: {
			required: "The Land Owner Name field is required",
			minlength: "The Land Owner Name field must be at least 2 characters in length",
			},
			location: {
			required: "The Location field is required",
			minlength: "The Location field must be at least 2 characters in length",
			},
			land_classification_id: {
			required: "The Land Classification field is required",
			},
			land_area: {
			required: "The Land Area field is required",
			},
			status: {
			required: "The Status field is required",
			},
		},
		invalidHandler: function (event, validator) {
			event.preventDefault();

			var alert = $("#form_msg");
			alert.closest("div.form-group").removeClass("kt-hide").show();
			KTUtil.scrollTop();

			toastr.options = {
			closeButton: true,
			debug: false,
			newestOnTop: false,
			progressBar: false,
			positionClass: "toast-top-right",
			preventDuplicates: false,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			timeOut: "5000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			};

			toastr.error("Please check your fields", "Something went wrong");
		},
		// submitHandler: function ( _frm ) {

		// 	_frm[0].submit();
		// }
		});

		$(document).on("submit", "form#_consolidate_form", function (e) {
		e.preventDefault();

		var land_ids = [];
		var inputs = $("form#_consolidate_form").serializeArray();

		$.each($('input[name="id[]"]:checked'), function () {
			land_ids.push($(this).val());
		});

		$.ajax({
			type: "post",
			dataType: "json",
			url: base_url + "land/consolidate",
			data: {
			ids: land_ids,
			input: inputs,
			},
			beforeSend: function () {
			$("#_consolidate_modal").removeClass("show");

			KTApp.blockPage({
				overlayColor: "#000000",
				type: "v2",
				state: "primary",
				message: "Processing...",
			});
			},
			success: function (_respo) {
			$("#_consolidate_modal").modal("hide");

			$("button._consolidate").addClass("disabled", true);

			// $('#_consolidate_modal').addClass('show');

			toastr.options = {
				closeButton: true,
				debug: false,
				newestOnTop: true,
				progressBar: false,
				positionClass: "toast-top-right",
				preventDuplicates: true,
				onclick: null,
				showDuration: "300",
				hideDuration: "1000",
				timeOut: "0",
				extendedTimeOut: "0",
				showEasing: "swing",
				hideEasing: "linear",
				showMethod: "fadeIn",
				hideMethod: "fadeOut",
			};

			if (_respo._status === 1) {
				toastr.success(_respo._msg, "Success");

				$("form#_consolidate_form")
				.find("input, select")
				.addClass("is-valid");
			} else {
				toastr.error(_respo._msg, "Failed");

				$("form#_consolidate_form")
				.find("input, select")
				.addClass("is-invalid");
			}

			$("#_land_inventory_table").DataTable().ajax.reload();

			KTApp.unblockPage();
			},
		}).fail(function () {
			$("button._consolidate").addClass("disabled", true);

			$("#_consolidate_modal").modal("hide");

			KTApp.unblockPage();

			swal.fire("Oops!", "Please refresh the page and try again.", "error");
		});
		});

		$("#_subdivide_form").validate({
		rules: {
			land_owner_name: {
			required: true,
			minlength: 2,
			},
			land_classification_id: {
			required: true,
			},
			location: {
			required: true,
			minlength: 2,
			},
			company: {},
			land_area: {
			required: true,
			number: true,
			},
			status: {
			required: true,
			},
			// lot_number: {
			// 	number: true
			// },
			number_of_lot_to_subdivide: {
			required: true,
			number: true,
			},
			project: {},
			remarks: {},
		},
		messages: {
			land_owner_name: {
			required: "The Land Owner Name field is required",
			minlength: "The Land Owner Name field must be at least 2 characters in length",
			},
			location: {
			required: "The Location field is required",
			minlength: "The Location field must be at least 2 characters in length",
			},
			land_classification_id: {
			required: "The Land Classification field is required",
			},
			land_area: {
			required: "The Land Area field is required",
			},
			status: {
			required: "The Status field is required",
			},
		},
		invalidHandler: function (event, validator) {
			event.preventDefault();

			var alert = $("#form_msg");
			alert.closest("div.form-group").removeClass("kt-hide").show();
			KTUtil.scrollTop();

			toastr.options = {
			closeButton: true,
			debug: false,
			newestOnTop: false,
			progressBar: false,
			positionClass: "toast-top-right",
			preventDuplicates: false,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			timeOut: "5000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			};

			toastr.error("Please check your fields", "Something went wrong");
		},
		// submitHandler: function ( _frm ) {

		// 	_frm[0].submit();
		// }
		});

		$(document).on("submit", "form#_subdivide_form", function (e) {
		e.preventDefault();

		var land_id = $('input[name="id[]"]:checked').val();
		var inputs = $("form#_subdivide_form").serializeArray();

		$.ajax({
			type: "post",
			dataType: "json",
			url: base_url + "land/subdivide",
			data: {
			id: land_id,
			input: inputs,
			},
			beforeSend: function () {
			$("#_subdivide_modal").removeClass("show");

			KTApp.blockPage({
				overlayColor: "#000000",
				type: "v2",
				state: "primary",
				message: "Processing...",
			});
			},
			success: function (_respo) {
			$("#_subdivide_modal").modal("hide");

			$("button._subdivide").addClass("disabled", true);

			// $('#_subdivide_modal').addClass('show');

			toastr.options = {
				closeButton: true,
				debug: false,
				newestOnTop: true,
				progressBar: false,
				positionClass: "toast-top-right",
				preventDuplicates: true,
				onclick: null,
				showDuration: "300",
				hideDuration: "1000",
				timeOut: "0",
				extendedTimeOut: "0",
				showEasing: "swing",
				hideEasing: "linear",
				showMethod: "fadeIn",
				hideMethod: "fadeOut",
			};

			if (_respo._status === 1) {
				toastr.success(_respo._msg, "Success");

				$("form#_subdivide_form")
				.find("input, select")
				.addClass("is-valid");
			} else {
				toastr.error(_respo._msg, "Failed");

				$("form#_subdivide_form")
				.find("input, select")
				.addClass("is-invalid");
			}

			$("#_land_inventory_table").DataTable().ajax.reload();

			KTApp.unblockPage();
			},
		}).fail(function () {
			$("button._subdivide").addClass("disabled", true);

			$("#_subdivide_modal").modal("hide");

			KTApp.unblockPage();

			swal.fire("Oops!", "Please refresh the page and try again.", "error");
		});
		});

		$("#_consolidate_and_subdivide_form").validate({
		rules: {
			land_owner_name: {
			required: true,
			minlength: 2,
			},
			land_classification_id: {
			required: true,
			},
			location: {
			required: true,
			minlength: 2,
			},
			company: {},
			land_area: {
			required: true,
			number: true,
			},
			status: {
			required: true,
			},
			// lot_number: {
			// 	number: true
			// },
			number_of_lot_to_subdivide: {
			required: true,
			number: true,
			},
			project: {},
			remarks: {},
		},
		messages: {
			land_owner_name: {
			required: "The Land Owner Name field is required",
			minlength: "The Land Owner Name field must be at least 2 characters in length",
			},
			location: {
			required: "The Location field is required",
			minlength: "The Location field must be at least 2 characters in length",
			},
			land_classification_id: {
			required: "The Land Classification field is required",
			},
			land_area: {
			required: "The Land Area field is required",
			},
			status: {
			required: "The Status field is required",
			},
		},
		invalidHandler: function (event, validator) {
			event.preventDefault();

			var alert = $("#form_msg");
			alert.closest("div.form-group").removeClass("kt-hide").show();
			KTUtil.scrollTop();

			toastr.options = {
			closeButton: true,
			debug: false,
			newestOnTop: false,
			progressBar: false,
			positionClass: "toast-top-right",
			preventDuplicates: false,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			timeOut: "5000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			};

			toastr.error("Please check your fields", "Something went wrong");
		},
		// submitHandler: function ( _frm ) {

		// 	_frm[0].submit();
		// }
		});

		$(document).on("submit", "form#_consolidate_and_subdivide_form", function (e) {
		e.preventDefault();


		var land_ids = [];

		$.each($('input[name="id[]"]:checked'), function () {
			land_ids.push($(this).val());
		});

		var land_id = $('input[name="id[]"]:checked').val();
		var inputs = $("form#_consolidate_and_subdivide_form").serializeArray();

		$.ajax({
			type: "post",
			dataType: "json",
			url: base_url + "land/consolidate_and_subdivide",
			data: {
			ids: land_ids,
			input: inputs,
			},
			beforeSend: function () {
			$("#_consolidate_and_subdivide_modal").removeClass("show");

			KTApp.blockPage({
				overlayColor: "#000000",
				type: "v2",
				state: "primary",
				message: "Processing...",
			});
			},
			success: function (_respo) {
			$("#_consolidate_and_subdivide_modal").modal("hide");

			$("button._consolidate_and_subdivide").addClass("disabled", true);

			// $('#_consolidate_and_subdivide_modal').addClass('show');

			toastr.options = {
				closeButton: true,
				debug: false,
				newestOnTop: true,
				progressBar: false,
				positionClass: "toast-top-right",
				preventDuplicates: true,
				onclick: null,
				showDuration: "300",
				hideDuration: "1000",
				timeOut: "0",
				extendedTimeOut: "0",
				showEasing: "swing",
				hideEasing: "linear",
				showMethod: "fadeIn",
				hideMethod: "fadeOut",
			};

			if (_respo._status === 1) {
				toastr.success(_respo._msg, "Success");

				$("form#_consolidate_and_subdivide_form")
				.find("input, select")
				.addClass("is-valid");
			} else {
				toastr.error(_respo._msg, "Failed");

				$("form#_consolidate_and_subdivide_form")
				.find("input, select")
				.addClass("is-invalid");
			}

			$("#_land_inventory_table").DataTable().ajax.reload();

			KTApp.unblockPage();
			},
		}).fail(function () {
			$("button._consolidate_and_subdivide").addClass("disabled", true);

			$("#_consolidate_and_subdivide_modal").modal("hide");

			KTApp.unblockPage();

			swal.fire("Oops!", "Please refresh the page and try again.", "error");
		});
		});
	};

	var _add_ons = function () {
		var dTable = $("#_land_inventory_table").DataTable();

		$("#_search").on("keyup", function () {
		dTable.search($(this).val()).draw();
		});

		$("#kt_datepicker").datepicker({
		orientation: "bottom left",
		autoclose: true,
		todayHighlight: true,
		templates: {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>',
		},
		});

		$("button#_advance_search_btn").on("click", function () {
		$("div#_batch_upload").removeClass("show");
		});

		$("button#_batch_upload_btn").on("click", function () {
		$("div#_advance_search").removeClass("show");
		});

		$(document).on("click", "button._delete_docx", function () {
		var id = $(this).data("id");

		swal
			.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel!",
			reverseButtons: true,
			})
			.then(function (result) {
			if (result.value) {
				$.ajax({
				type: "post",
				dataType: "json",
				url: base_url + "land/delete",
				data: {
					id: id,
				},
				success: function (_response) {
					if (_response._status) {
					// $("#_land_inventory_table").load(location.href + " #_land_inventory_table>*", "");

					$("#_land_inventory_table").DataTable().ajax.reload();

					swal.fire("Deleted!", _response._msg, "success");
					} else {
					swal.fire("Oops!", _response._msg, "error");
					}
				},
				});
			} else if (result.dismiss === "cancel") {
				swal.fire("Cancelled", "Your imaginary file is safe.", "error");
			}
			});
		});

		$(".modal").on("hidden.bs.modal", function () {
		$(this).find("form")[0].reset();

		$("form")
			.find("input, select")
			.removeClass("is-valid")
			.removeClass("is-invalid");

		$(this).find("form").trigger("reset");

		$("#form_msg").closest("div.form-group").addClass("kt-hide");
		});
	};

	var _exportImport = function () {
		$("#_export_select_all").on("click", function () {
		if (this.checked) {
			$("._export_column").each(function () {
			this.checked = true;
			});
		} else {
			$("._export_column").each(function () {
			this.checked = false;
			});
		}
		});

		$("._export_column").on("click", function () {
		if (this.checked == false) {
			$("#_export_select_all").prop("checked", false);
		}
		});

		$("#_export_form").validate({
		rules: {
			"_export_column[]": {
			required: true,
			},
		},
		messages: {
			"_export_column[]": {
			required: "",
			},
		},
		invalidHandler: function (event, validator) {
			event.preventDefault();

			var alert = $("#form_msg");
			alert.closest("div.form-group").removeClass("kt-hide").show();
			KTUtil.scrollTop();

			toastr.options = {
			closeButton: true,
			debug: false,
			newestOnTop: false,
			progressBar: false,
			positionClass: "toast-top-right",
			preventDuplicates: false,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			timeOut: "5000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			};

			toastr.error("Please check your fields", "Something went wrong");
		},
		submitHandler: function (_frm) {
			_frm[0].submit();
		},
		});
	};

	var _exportUploadCSV = function () {
		$("#_export_csv").validate({
		rules: {
			update_existing_data: {
			required: true,
			},
		},
		messages: {
			update_existing_data: {
			required: "File type is required",
			},
		},
		invalidHandler: function (event, validator) {
			event.preventDefault();

			// var alert   =   $('#form_msg');
			// alert.closest('div.form-group').removeClass('kt-hide').show();
			KTUtil.scrollTop();

			toastr.options = {
			closeButton: true,
			debug: false,
			newestOnTop: false,
			progressBar: false,
			positionClass: "toast-top-right",
			preventDuplicates: false,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			timeOut: "5000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			};

			toastr.error("Please check your fields", "Something went wrong");
		},
		submitHandler: function (_frm) {
			_frm[0].submit();
		},
		});

		$("#_upload_form").validate({
		rules: {
			csv_file: {
			required: true,
			},
		},
		messages: {
			csv_file: {
			required: "CSV file is required",
			},
		},
		invalidHandler: function (event, validator) {
			event.preventDefault();

			// var alert   =   $('#form_msg');
			// alert.closest('div.form-group').removeClass('kt-hide').show();
			KTUtil.scrollTop();

			toastr.options = {
			closeButton: true,
			debug: false,
			newestOnTop: false,
			progressBar: false,
			positionClass: "toast-top-right",
			preventDuplicates: false,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			timeOut: "5000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			};

			toastr.error("Please check your fields", "Something went wrong");
		},
		submitHandler: function (_frm) {
			_frm[0].submit();
		},
		});
	};

	var _initUploadGuideModal = function () {
		var _table = $("#_upload_guide_modal");

		_table.DataTable({
		order: [
			[0, "asc"]
		],
		pagingType: "full_numbers",
		lengthMenu: [3, 5, 10, 25, 50, 100],
		pageLength: 3,
		responsive: true,
		searchDelay: 500,
		processing: true,
		serverSide: false,
		deferRender: true,
		dom:
			// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
			// "<'row'<'col-sm-12 col-md-6'l>>" +
			"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
		language: {
			lengthMenu: "Show _MENU_",
			infoFiltered: "(filtered from _MAX_ total records)",
		},
		columnDefs: [{
			targets: [0, 2, 3, 5],
			className: "dt-center",
		},
		{
			targets: [0, 2, 3, 4, 5],
			orderable: false,
		},
		{
			targets: [4, 5],
			searchable: false,
		},
		{
			targets: 0,
			searchable: false,
			visible: false,
		},
		],
		});
	};

	var populateModal = function () {

		var deleteids_arr = [];

		// Read all checked checkboxes
		$('input[name="id[]"]:checked').each(function () {
			deleteids_arr.push($(this).val());
		});

		$.ajax({
			url: base_url + "land/view/",
			type: "POST",
			dataType: "JSON",
			data: {
				ids: deleteids_arr,
				type: "json",
			},
			success: function (res) {

				$.each(res, function(i, item) {
					if (res[i] != null) {
						$('#_subdivide_form [name='+i+']').val(res[i]);
						$('#_consolidate_and_subdivide_modal [name='+i+']').val(res[i]);
						$('#_consolidate_modal [name='+i+']').val(res[i]);
					}
				});

			},
		});

	};

	return {
		init: function () {
		_initDocument();
		_processLand();
		_selectProp();
		_add_ons();
		filter();
		_exportImport();
		_exportUploadCSV();
		_initUploadGuideModal();
		_checkSelectedAll();
		},
	};
})();

jQuery(document).ready(function () {
  	KTDatatablesLandInventory.init();
});
