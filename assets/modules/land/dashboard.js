"use strict";

let LandInventoryDashboard = (() => {

    let loadLandInventoryStatus = () => {
        $.ajax({
            url: base_url + "land/get_dashboard_data",
            type: "POST",
            dataType: "JSON",
            success: (response) => {
                dashboard(response)
            },
            error: () => {
                swal.fire(
                    "An error was encounterd",
                    "",
                    "error"
                );
            }
        })

        $.ajax({
            url: base_url + "land/pie",
            type: "POST",
            dataType: "JSON",
            success: (response) => {
                pie(response);
            },
            error: () => {
                swal.fire(
                    "An error was encounterd",
                    "",
                    "error"
                );
            }
        })
    }

    let dashboard = (data) => {
        let chart = AmCharts.makeChart("dashboard_chart", {
            type: "serial",
            theme: "light",
            handDrawn: false,
            handDrawScatter: 3,
            legend: {
                useGraphSettings: true,
                markerSize: 12,
                valueWidth: 0,
                verticalGap: 0
            },
            dataProvider: data,
            valueAxes: [{
                minorGridAlpha: 0.08,
                minorGridEnabled: true,
                position: "top",
                axisAlpha: 0
            }],
            startDuration: 1,
            graphs: [
                {
                    balloonText: "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b></span>",
                    title: "Count",
                    type: "column",
                    fillAlphas: 0.8,
                    valueField: "count"
                },
            ],
            rotate: true,
            categoryField: "status",
            categoryAxis: {
                gridPosition: "start"
            },
            export: {
                enabled: true
            },
            colors: ["#5ebc44"]
        });
    }

    let pie = (data) => {
        // Build the chart
        Highcharts.chart("dashboard_pie_chart", {
          chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: "pie",
          },
          title: {
            text: "Land Classification",
          },
          subtitle: {
            text: "All Land Inventory Encoded",
          },
          tooltip: {
            pointFormat:
              "{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}",
          },
          accessibility: {
            point: {
              valueSuffix: "%",
            },
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: "pointer",
              dataLabels: {
                enabled: true,
                pointFormat: "{point.name}: <br>{point.percentage:.1f} %",
                connectorColor: "silver",
              },
            },
          },
          series: [
            {
              name: "Count",
              data: JSON.parse(data),
            },
          ],
        });
    }

    return {
        init: () => {
            loadLandInventoryStatus()
        }
    }
})()

jQuery(document).ready(() => {
    LandInventoryDashboard.init();
})