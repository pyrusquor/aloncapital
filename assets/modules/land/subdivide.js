var	KTFormControls	=	function () {

	var	trial	=	function () {

		$('#kt_form_subdivide').validate({

			rules: {

				owner_name: {
					required: true,
					minlength: 3
				},
				classification: {
					required: true
				},
				location: {
					required: true
				},
				company: {
					required: true
				},
				area: {
					required: true
				},
				status: {
					required: true
				},
				lot_number: {
					required: true
				},
				subdivide_number: {
					required: true
				},
				project: {
					required: true
				},
				remarks: {
					required: true
				},
        digits: {
            required: true,
            digits: true
        }
				
			},
			invalidHandler: function ( e, validator ) {

				var alert	=	$('#form_msg');
				alert.removeClass('kt--hide').show();
				KTUtil.scrollTop();
			},
			submitHandler: function () {
				//form[0].submit(); // submit the form
			}
		});
	};

	return {

		init: function () {

			trial();
		}
	};
}();

jQuery(document).ready( function () {

	KTFormControls.init();
});