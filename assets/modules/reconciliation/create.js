// Class definition
var KTFormControls = function () {
    // Private functions

    var valReconciliation = function () {
        $("#reconciliation_form").validate({
            // define validation rules
            rules: {
                /* ==================== begin: Add model fields ==================== */
                bank_id: {
                    required: true
                },
                month_year: {
                    required: true
                },
                ending_balance: {
                    required: true
                },
                /* ==================== end: Add model fields ==================== */
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {

                toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    };

    return {
        // public functions
        init: function () {
            valReconciliation();
        }
    };
}();


var KTBootstrapDatepicker = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd'
        });

        $('.monthYearPicker').datepicker({
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            format: "yyyy-mm-dd",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true,
            onClose: function (dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
    };

    return {
        // public functions
        init: function () {
            datepicker();
        }
    };
}();

jQuery(document).ready(function () {
    KTFormControls.init();
    KTBootstrapDatepicker.init();
});