// Class definition
var KTFormControls = (function () {
  // Private functions
  var _selectProp = function () {
    var _table = $("#entry_item_table").DataTable();

    $(document).on("change", 'input[name="id[]"]', function () {
      var _checked = $('input[name="id[]"]:checked').length;
      if (_checked < 0) {
        _table.button(0).disable();
      } else {
        _table.button(0).enable(_checked > 0);
      }
    });

    $("#reconcileItem").click(function () {
      var reconids_arr = [];
      // Read all checked checkboxes
      $('input[name="id[]"]:checked').each(function () {
        reconids_arr.push($(this).val());
      });

      // Check checkbox checked or not
      if (reconids_arr.length > 0) {
        swal
          .fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, reconcile item!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true,
          })
          .then(function (result) {
            if (result.value) {
              $.ajax({
                url: base_url + "reconciliation/reconItem",
                type: "POST",
                dataType: "JSON",
                data: { reconids_arr: reconids_arr },
                success: function (res) {
                  if (res.status) {
                    Swal.fire({
                      title: "Reconciled!",
                      text: res.message,
                      type: "success",
                    }).then((result) => {
                      // Reload the Page
                      location.reload();
                    });
                    // $("#entry_item_table")
                    //     .DataTable()
                    //     .ajax.reload();
                    // swal.fire(
                    //     "Reconciled!",
                    //     res.message,
                    //     "success"
                    // );
                  } else {
                    swal.fire("Oops!", res.message, "error");
                  }
                },
              });
            } else if (result.dismiss === "cancel") {
              swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
            }
          });
      }
    });
  };

  return {
    // public functions
    init: function () {
      _selectProp();
    },
  };
})();

jQuery(document).ready(function () {
  KTFormControls.init();
});
