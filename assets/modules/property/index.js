"use strict";

var Property = (function () {
    let tableElement = $("#property_table");

    var propertyTable = function () {

        var dataTable = tableElement.DataTable({
            order: [
                [1, 'desc']
            ],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + "property/showItems",
            },
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                text: '<i class="la la-trash"></i> Delete Selected',
                className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm",
                attr: {
                    id: "bulkDelete",
                },
                enabled: false,
            }, ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },
            columns: [{
                    data: null,
                },
                {
                    data: "id",
                },
                {
                    data: "name",
                },
                {
                    data: "project_id",
                },
                {
                    data: "project_id",
                },
                {
                    data: "phase",
                },
                {
                    data: "block",
                },
				{
                    data: "lot",
                },
                {
                    data: "lot_area",
                },
                {
                    data: "total_selling_price",
                },
                {
                    data: "model_id",
                },
                {
                    data: "interior_id",
                },
                {
                    data: "status",
                },
                {
                    data: "hlurb_classification_id",
                },
                {
                    data: "project_id",
                },
                {
                    data: "model_id",
                },
                {
                    data: "interior_id",
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: "Actions",
                    responsivePriority: -1,
                },
            ],
            columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">

            							<a class="dropdown-item" href="` + base_url + `property/form/` + row.id + `"><i class="la la-edit"></i> Update </a>

            							<a href="javascript:void(0);" class="dropdown-item remove_property" data-id="` + row.id + `"><i class="la la-trash"></i> Delete </a>

                                        <a href="javascript:void(0)" data-target="#quotationModal" class="dropdown-item process_change_quotation" data-id="` +row.id +`"> <i class="flaticon2-quotation-mark"></i>Create Quotation</a>

                                        <a href="javascript:void(0)" class="dropdown-item process_reservation" data-id="` +row.id +`"> <i class="flaticon-cart"></i>Hold for Sale</a>

									</div>
								</span>
								<a href="` +
                            base_url +
                            `property/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>

                                `


                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, 5, -1],
                    className: "dt-center",
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },

				/* ==================== begin: Add target fields for dropdown value ==================== */
                {
                    targets: 12,
                    render: function (data, type, row, meta) {
                        var property = {
                            1: {
                                title: 'Available',
                            },
                            2: {
                                title: 'Reserved',
                            },
                            3: {
                                title: 'Sold',
                            },
                            4: {
                                title: 'Hold',
                            },
                            5: {
                                title: 'Not For Sale',
                            },
                        };

                        if (typeof property[data] === 'undefined') {
                            return ``;
                        }
                        return property[data].title;
                    },
                },

                {
                    targets: 13,
                    render: function (data, type, row, meta) {
                        var property = {
                            0: {
                                title: 'None',
                            },
                            1: {
                                title: 'PD957',
                            },
                            2: {
                                title: 'BP220',
                            },
                            3: {
                                title: 'Socialize',
                            },
                        };

                        if (typeof property[data] === 'undefined') {
                            return ``;
                        }
                        return property[data].title;
                    },
                },

                {
                    targets: [4, 14, 15, 16],
                    visible: false,
                },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on("click", ".remove_property", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "property/delete/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            id: id
                        },
                        success: function (res) {
                            if (res.status) {
                                $("#property_table").DataTable().ajax.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var datepicker = function () {
        // minimum setup
        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true,
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
    };

    var processQuotationChange = function () {
        $(document).on('click', '.process_change_quotation', function () {
            var btn = $(this);
            var id = btn.data('id');

            $('#quotationModal').modal('toggle');
            const form = document.querySelector('#quotationForm');
            form.action = `${base_url}property/quotation/${id}`;
        });
    };

    var processReservation = function () {
        const propertyDisplay = document.querySelector('#property_select');
        const propertySelected = document.querySelector('#selected_property');

        $(document).on('click', '.process_reservation', function () {
            var btn = $(this);
            var id = btn.data('id');
            $(propertyDisplay).val(id).change();
            $(propertyDisplay).prop('disabled', true);
            $(propertySelected).val(id);
            $('#reservationModal').modal('toggle');
        });
    };

    const fetchSellers = () => {
        const sellerSelectElm = document.querySelector('#sellerNames');

        $.ajax({
            url: `${base_url}property/getAllSellers`,
            type: 'GET',
            success: function (res) {
                const { data: sellers } = JSON.parse(res);
                sellers.map((seller) => {
                    const { id, first_name, middle_name, last_name } = seller;

                    const optionEl = document.createElement('option');
                    optionEl.value = id;
                    optionEl.innerHTML = `${first_name} ${middle_name} ${last_name}`;

                    sellerSelectElm.appendChild(optionEl);
                });
            },
        });
    };

    const fetchProspects = () => {
        const prospectSelectElm = document.querySelector('#prospect_select');

        $.ajax({
            url: `${base_url}property/getAllProspects`,
            type: 'GET',
            success: function (res) {
                const { data: prospects } = JSON.parse(res);
                prospects.map((prospect) => {
                    const { id, first_name, middle_name, last_name } = prospect;

                    const optionEl = document.createElement('option');
                    optionEl.value = id;
                    optionEl.innerHTML = `${first_name} ${middle_name} ${last_name}`;

                    prospectSelectElm.appendChild(optionEl);
                });
            },
        });
    };

    const fetchProperties = () => {
        const propertySelectElm = document.querySelector('#property_select');

        $.ajax({
            url: `${base_url}property/getAllProperties`,
            type: 'GET',
            success: function (res) {
                const { data: properties } = JSON.parse(res);
                properties.map((property) => {
                    const { id, name } = property;

                    const optionEl = document.createElement('option');
                    optionEl.value = id;
                    optionEl.innerHTML = `${name}`;

                    propertySelectElm.appendChild(optionEl);
                });
            },
        });
    };

    const toastPropertyStatus = () => {
        const buttons = document.querySelectorAll('#notAvailable');

        buttons.forEach((btn) => {
            btn.addEventListener('click', function () {
                let status = this.dataset.status;
                switch (status) {
                    case 'Reserved':
                        // swal.fire(`This property is already ${status}. We cannot process your reservation.`, "error");
                        swal.fire({
                            title: `Already ${status}.`,
                            text: `This property is already ${status}. We cannot process your reservation.`,
                            type: 'warning',
                            showCancelButton: false,
                            cancelButtonText: 'Close',
                        });
                        break;
                    case 'Sold':
                        // swal.fire(`This property is already ${status}. We cannot process your reservation.`, "error");
                        swal.fire({
                            title: `Already ${status}.`,
                            text: `This property is already ${status}. We cannot process your reservation.`,
                            type: 'warning',
                            showCancelButton: false,
                            cancelButtonText: 'Close',
                        });
                        break;
                    case 'Hold':
                        // swal.fire(`This property is on ${status}. We cannot process your reservation.`, "error");
                        swal.fire({
                            title: `On ${status}.`,
                            text: `This property is on ${status}. We cannot process your reservation.`,
                            type: 'warning',
                            showCancelButton: false,
                            cancelButtonText: 'Close',
                        });
                        break;
                    case 'Not For Sale':
                        swal.fire(`This property is ${status}.`, 'error');
                        swal.fire({
                            title: `${status}.`,
                            text: `This property is ${status}.`,
                            type: 'warning',
                            showCancelButton: false,
                            cancelButtonText: 'Close',
                        });
                        break;
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on("click", "#btn_upload_guide", function () {
            var table = $("#upload_guide_table");

            table.DataTable({
                order: [
                    [0, "asc"]
                ],
                pagingType: "full_numbers",
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + "property/get_table_schema",
                columns: [
                    // {data: 'button'},
                    {
                        data: "no"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "type"
                    },
                    {
                        data: "format"
                    },
                    {
                        data: "option"
                    },
                    {
                        data: "required"
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    let filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('.filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $("#property_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            // var rows = _table.rows({ 'search': 'applied' }).nodes();

            // $('input[type="checkbox"]').prop( 'checked', this.checked );
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#property_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "property/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#property_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    var _add_ons = function () {
        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            propertyTable();
            filter();
            _selectProp();
            _add_ons();
            upload_guide();
            status();

            processQuotationChange();
            fetchSellers();
            fetchProspects();
            processReservation();
            fetchProperties();
            toastPropertyStatus();
            datepicker();
        },
    };
})();

jQuery(document).ready(function () {
    Property.init();
});