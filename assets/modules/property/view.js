"use strict";

// Class definition
var KTamChartsChartsDemo = function () {

    // Private functions
    // var demo1 = function () {
    //     var chart = AmCharts.makeChart("kt_amcharts_1", {
    //         "type": "serial",
    //         "categoryField": "category",
    //         "startDuration": 1,
    //         "fontSize": 13,
    //         "theme": "light",
    //         "categoryAxis": {
    //             "gridPosition": "start"
    //         },
    //         "trendLines": [],
    //         "graphs": [{
    //             "balloonText": "[[title]] of [[category]]:[[value]]",
    //             "fillAlphas": 1,
    //             "id": "AmGraph-1",
    //             "legendColor": "undefined",
    //             "title": "Sales",
    //             "type": "column",
    //             "valueField": "column-1"
    //         },
    //         {
    //             "balloonText": "[[title]] of [[category]]:[[value]]",
    //             "color": "#674E4E",
    //             "fillAlphas": 1,
    //             "id": "AmGraph-2",
    //             "title": "Acquisition",
    //             "type": "column",
    //             "valueField": "column-2"
    //         }
    //         ],
    //         "guides": [],
    //         "valueAxes": [{
    //             "id": "ValueAxis-1",
    //             "title": "Axis title"
    //         }],
    //         "allLabels": [],
    //         "balloon": {},
    //         "legend": {
    //             "enabled": true,
    //             "useGraphSettings": true
    //         },
    //         "titles": [{
    //             "id": "Title-1",
    //             "size": 15,
    //             "text": "Monthly Commissions"
    //         }],
    //         "dataProvider": [{
    //             "category": "Oct 2018",
    //             "column-1": 8,
    //             "column-2": 13
    //         },
    //         {
    //             "category": "Nov 2018",
    //             "column-1": 6,
    //             "column-2": 30
    //         },
    //         {
    //             "category": "Dec 2018",
    //             "column-1": 2,
    //             "column-2": 74
    //         },
    //         {
    //             "category": "Jan 2019",
    //             "column-1": "45",
    //             "column-2": 56
    //         },
    //         {
    //             "category": "Feb 2019",
    //             "column-1": "78",
    //             "column-2": 53
    //         },
    //         {
    //             "category": "Mar 2019",
    //             "column-1": "34",
    //             "column-2": 13
    //         },
    //         {
    //             "category": "Apr 2019",
    //             "column-1": "56",
    //             "column-2": ""
    //         },
    //         {
    //             "category": "May 2019",
    //             "column-1": null,
    //             "column-2": ""
    //         },
    //         {
    //             "category": "Jun 2019",
    //             "column-1": null,
    //             "column-2": 69
    //         },
    //         {
    //             "category": "Jul 2019",
    //             "column-1": null,
    //             "column-2": 23
    //         },
    //         {
    //             "category": "Aug 2019",
    //             "column-1": null,
    //             "column-2": 94
    //         },
    //         {
    //             "category": "Sep 2019",
    //             "column-1": null,
    //             "column-2": 82
    //         }
    //         ],
    //         "valueAxes": [{
    //             "gridColor": "#FFFFFF",
    //             "gridAlpha": 0.2,
    //             "dashLength": 0
    //         }]

    //     });
    // }

    var paymentRequestTable = function () {

        console.log(window.property_id)

        var table = $('#paymentrequest_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                url: base_url + 'payment_request/showPropertyPaymentRequests',
                type: 'POST',
                data: {
                    filter: {
                        property_id: window.property_id
                    }
                }
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'payee',
                },
                {
                    data: 'payable_type_id',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'due_date',
                },
                {
                    data: 'gross_amount',
                },
                {
                    data: 'tax_amount',
                },
                {
                    data: 'paid_amount',
                },
                {
                    data: 'remaining_amount',
                },
                {
                    data: 'is_paid',
                },
                {
                    data: 'is_complete',
                },
                {
                    data: 'particulars',
                },
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },


                {
                    targets: [0],
                    visible: false,
                },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });
    }

    var paymentVoucherTable = function () {
        var table = $('#paymentvoucher_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                url: base_url + 'payment_voucher/showPropertyPaymentVouchers',
                type: 'POST',
                data: {
                    where: {
                        property_id: window.property_id
                    }
                }
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'payment_type_id',
                },
                {
                    data: 'payee',
                },
                {
                    data: 'payee_type',
                },
                {
                    data: 'paid_date',
                },
                {
                    data: 'paid_amount',
                },
                {
                    data: 'particulars',
                },
            ],
            columnDefs: [
                {
                    targets: [0, 1, 3, 4, 5, -1],
                    className: 'dt-center',
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },

                /* ==================== begin: Add target fields for dropdown value ==================== */
                {
                    targets: 3,
                    render: function (data, type, row, meta) {
                        var voucher = {
                            1: {
                                title: 'Cash',
                            },
                            2: {
                                title: 'Regular Cheque',
                            },
                            3: {
                                title: 'Post Dated Cheque',
                            },
                            4: {
                                title: 'Online Deposits',
                            },
                            5: {
                                title: 'Wire Transfer',
                            },
                            6: {
                                title: 'Cash & Cheque',
                            },
                            7: {
                                title: 'Cash & Bank Remittance',
                            },
                            8: {
                                title: 'Journal Voucher',
                            },
                            9: {
                                title: 'Others',
                            },
                            10: {
                                title: 'Direct Deposit',
                            },
                        };

                        if (typeof voucher[data] === 'undefined') {
                            return ``;
                        }
                        return voucher[data].title;
                    },
                },

                {
                    targets: [0],
                    visible: false,
                },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });
    };

    var AccountingEntriesTable = function () {
        var table = $('#accounting_entries_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                url: base_url + 'accounting_entries/showAccountingEntries',
                type: 'POST',
                data: {
                    where: {
                        property_id: property_id
                    }
                }
            },
            dom:
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'company',
                },
                {
                    data: 'project',
                },
                {
                    data: 'payee_type',
                },
                {
                    data: 'payee_name',
                },
                {
                    data: 'or_number',
                },
                {
                    data: 'invoice_number',
                },
                {
                    data: 'journal_type',
                },
                {
                    data: 'payment_date',
                },
                {
                    data: 'cr_total',
                },
                {
                    data: 'remarks',
                },
                {
                    data: 'is_approve',
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `<a href="` +
                            base_url +
                            `accounting_entries/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        )
                    },
                },
                {
                    targets: 7,
                    render: function (data, type, full, meta) {
                        var journal_type = {
                            1: {
                                type: 'Journal Voucher',
                            },
                            2: {
                                type: 'Sales Journal',
                            },
                            3: {
                                type: 'Cash Receipt Journal',
                            },
                            4: {
                                type: 'Purchase Journal',
                            },
                            5: {
                                type: 'Cash Payment Journal',
                            },
                        };

                        if (typeof journal_type[data] === 'undefined') {
                            return ``;
                        }
                        return journal_type[data].type;
                    },
                },
                {
                    targets: 11,
                    render: function (data, type, full, meta) {
                        if (data == 0) {
                            return 'Pending';
                        } else if (data == 1) {
                            return 'Approved';
                        } else {
                            return 'Disapproved';
                        }
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };

    return {
        // public functions
        init: function () {
            // demo1();
            paymentRequestTable()
            paymentVoucherTable()
            AccountingEntriesTable()
        }
    };
}();

jQuery(document).ready(function () {
    KTamChartsChartsDemo.init();
});