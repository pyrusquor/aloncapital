// Class definition
var Create = (function () {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true,
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
    };

    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var _add_ons;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        wizard.on('beforePrev', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    };

    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ':hidden',

            // Validation rules
            rules: {
                'info[project_id]': {
                    required: true,
                },
                'info[name]': {
                    required: true,
                },
                'info[status]': {
                    required: true,
                },
                'info[model_id]': {
                    required: true,
                },
                'info[interior_id]': {
                    required: true,
                },
                'info[cluster]': {
                    number: true,
                },
                'info[lot]': {
                    required: true,
                    number: true,
                },
                'info[progress]': {
                    number: true,
                },
                'info[lot_area]': {
                    required: true,
                    number: true,
                },
                'info[floor_area]': {
                    number: true,
                },
                'info[lot_price_per_sqm]': {
                    required: true,
                    number: true,
                },
                'info[total_lot_price]': {
                    required: true,
                    number: true,
                },
                'info[total_selling_price]': {
                    required: true,
                    number: true,
                },
                'info[model_price]': {
                    number: true,
                },
            },
            // messages: {
            // 	"info[last_name]":'Last name field is required',
            // },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    title: '',
                    text:
                        'There are some errors in your submission. Please correct them.',
                    type: 'error',
                    confirmButtonClass: 'btn btn-secondary',
                });
            },

            // Submit valid form
            submitHandler: function (form) {},
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function (e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    type: 'POSt',
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: 'Success!',
                                text: response.message,
                                type: 'success',
                            }).then(function () {
                                window.location.replace(base_url + 'property');
                            });
                        } else {
                            swal.fire({
                                title: 'Oops!',
                                html: response.message,
                                icon: 'error',
                            });
                        }
                    },
                });
            }
        });
    };

    var _add_ons = function () {
        datepicker();
    };
    // Public functions
    return {
        init: function () {
            datepicker();
            _add_ons();

            wizardEl = KTUtil.get('kt_wizard_v3');
            formEl = $('#create_property');

            initWizard();
            initValidation();
            initSubmit();
        },
    };
})();

// Initialization
jQuery(document).ready(function () {
    Create.init();
});

var getHouseModel = function (property_id) {
    // Get house model base on selected project
    var project_id = $('#project_id').val();
    var houseModelEl = $('#house_model_id');

    if (property_id != 3) {
        $.ajax({
            url: base_url + 'property/get_house_model/' + property_id,
            type: 'POST',
            dataType: 'JSON',
            data: {
                project_id: project_id,
            },
            success: function (res) {
                if (res) {
                    $(houseModelEl)
                        .find('option')
                        .remove()
                        .end()
                        .append(
                            `<option value='${res.data.id}'>${res.data.name}</option>`
                        )
                        .val(res.data.id);
                    getLotTypes();
                } else {
                    $(houseModelEl)
                        .find('option')
                        .remove()
                        .end()
                        .append(`<option value='${0}'>Default</option>`)
                        .val(0);
                }
            },
        });
    } else {
        $(houseModelEl).empty();
        $.ajax({
            url: base_url + 'property/get_all_house_models',
            type: 'POST',
            data: {
                project_id,
                property_id,
            },
            success: function (data) {
                const { data: properties } = JSON.parse(data);
                if (properties) {
                    properties.map((property) => {
                        const { id, name } = property;

                        const optionEl = document.createElement('option');
                        optionEl.value = id;
                        optionEl.innerHTML = `${name}`;

                        $(houseModelEl).append(optionEl);
                    });
                }
            },
        });
    }
};

var getLotTypes = function () {
    var house_model_id = $('#house_model_id').val();
    var lotEl = $('#interior_id');

    $(lotEl).empty();
    $.ajax({
        url: base_url + 'property/get_lot_types',
        type: 'POST',
        data: {
            house_model_id,
        },
        success: function (data) {
            const { data: lots } = JSON.parse(data);

            lots.map((lot) => {
                const { id, name } = lot;

                const optionEl = document.createElement('option');
                optionEl.value = id;
                optionEl.innerHTML = `${name}`;

                $(lotEl).append(optionEl);
            });
        },
    });
};

$('#house_model_id').on('change', function () {
    var house_model_id = $(this).val();

    if (house_model_id != 0) {
        getLotTypes();
    }
});

$('#project_id').on('change', function () {
    var phaseEl = $('#phase');
    var blockEl = $('#block');
    var lotEl = $('#lot');
    $('#property_types').val(0);
    $('#house_model_parent').addClass('d-none');
    $(phaseEl).prop('disabled', true);
    $(blockEl).prop('disabled', true);
    $(lotEl).prop('disabled', true);
});

$('#property_types').on('change', function () {
    var phaseEl = $('#phase');
    var blockEl = $('#block');
    var lotEl = $('#lot');

    var prop_type = $(this).val();

    // console.log(prop_type);
    $('#house_model_parent').addClass('d-none');
    renderPropertyOptions(phaseEl, blockEl, lotEl, prop_type);
});

var renderPropertyOptions = function (phaseEl, blockEl, lotEl, prop_type) {
    if (prop_type == 1) {
        // If prop type is residential (1)
        $(phaseEl).prop('disabled', false);
        $(blockEl).prop('disabled', false);
        $(lotEl).prop('disabled', false);
        $(lotEl).prop('placeholder', 'Lot #');
        $(phaseEl).prop('readonly', false);
        $(blockEl).prop('readonly', false);

        $('#lot_no_id').text('Lot #');
        getHouseModel(prop_type);
    } else if (prop_type == 2) {
        // If prop type is commercial (2)
        $(phaseEl).prop('disabled', false);
        $(phaseEl).prop('readonly', true);
        $(phaseEl).val(0);

        $('#lot_no_id').text('Lot #');

        $(blockEl).prop('disabled', false);
        $(blockEl).prop('readonly', true);
        $(blockEl).val(0);
        $(lotEl).prop('disabled', false);
        $(lotEl).prop('placeholder', 'Lot #');
        getHouseModel(prop_type);
    } else if (prop_type == 3) {
        // If prop type is house and lot (3)
        $(phaseEl).prop('disabled', false);
        $(blockEl).prop('disabled', false);
        $(phaseEl).prop('readonly', false);
        $(blockEl).prop('readonly', false);
        $('#interior_id_label').text('Lot Type');
        $('#lot_no_id').text('Lot #');
        $(lotEl).prop('disabled', false);
        $(lotEl).prop('placeholder', 'Lot #');
        $('#house_model_parent').removeClass('d-none');
        getHouseModel(prop_type);
    } else if (prop_type == 4) {
        // If prop type is condominium (4)
        $(phaseEl).prop('disabled', false);
        $(phaseEl).prop('readonly', true);
        $(blockEl).prop('readonly', true);
        $(blockEl).prop('disabled', false);
        $('#interior_id_label').text('Orientation');
        $('#lot_no_id').text('Unit #');
        $(lotEl).prop('disabled', false);
        $(lotEl).prop('placeholder', 'Unit #');

        $('#house_model_parent').removeClass('d-none');
        getHouseModel(prop_type);
    } else {
        // default
        $(phaseEl).prop('disabled', true);
        $(blockEl).prop('disabled', true);
        $(lotEl).prop('disabled', true);
        $('#house_model_parent').addClass('d-none');
    }
};

var initializeHouseModel = function () {
    var project_id = $('#project_id').val();
    var property_id = $('#property_types').val();
    var houseModelEl = $('#house_model_id');

    var phaseEl = $('#phase');
    var blockEl = $('#block');
    var lotEl = $('#lot');

    renderPropertyOptions(phaseEl, blockEl, lotEl, property_id);
    $.ajax({
        url: base_url + 'property/get_house_model/' + property_id,
        type: 'POST',
        dataType: 'JSON',
        data: {
            project_id: project_id,
        },
        success: function (res) {
            if (res) {
                $(houseModelEl)
                    .find('option')
                    .remove()
                    .end()
                    .append(
                        `<option value='${res.data.id}'>${res.data.name}</option>`
                    )
                    .val(res.data.id);
                getLotTypes();
            } else {
                $(houseModelEl)
                    .find('option')
                    .remove()
                    .end()
                    .append(`<option value='${0}'>Default</option>`)
                    .val(0);
            }
        },
    });
};

$('#lot_area').on('blur', function () {
    var val = $(this).val();

    if (val == '') {
        $(this).val(0);
    }
});
$('#floor_area').on('blur', function () {
    var val = $(this).val();

    if (val == '') {
        $(this).val(0);
    }
});
$('#lot_price_per_sqm').on('blur', function () {
    var val = $(this).val();

    if (val == '') {
        $(this).val(0);
    }
});

initializeHouseModel();
