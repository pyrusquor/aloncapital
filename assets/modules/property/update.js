// Class definition
var BuyerCreate = function() {
    // Private functions
    var sgSelect = function() {

        // sales_group
        $('#sales_group').select2({
            placeholder: "Select a Group",
            allowClear: true
		});
		
		// Buyer Type
		$('#buyer_type').select2({
            placeholder: "Select a Buyer Type",
			allowClear: true,
			ajax: {
				url: base_url + 'buyer/fetchBuyerPositions',
				type: 'POST',
				dataType: 'json',
				delay: 250,
				data: function (term) {
					return {
						id: $("#sales_group").val(),
					}
				},
				processResults: function (data, page) {

					return {
						results: $.map(data, function (item) {
							return {
								text: item.name,
								id: item.id
							}
						})
					};
				},
				
			}
		});

		// Get current value
		var buyerTypeID = $("#buyer_type").data("id");
		var selletTypeName = $("#buyer_type").data("text");

		var setSelected = $("<option selected='selected'></option>").val(buyerTypeID).text(selletTypeName);
		$("#buyer_type").append(setSelected).trigger('change');
		// end

		// Reset Buyer type dropdown if sales group changed
		$("#sales_group").on("change", function () {

			$("#buyer_type option[value]").remove();
		});


	}
	
	var formRepeater = function() {
		
		$('#academic_form_repeater, #seminar_form_repeater, #exam_form_repeater').repeater({
			initEmpty: false,
           
            defaultValues: {
            },
             
            show: function () {
				$(this).slideDown();
				datepicker(); 
            },

            hide: function (deleteElement) {                
                $(this).slideUp(deleteElement);                 
            }   
		});
		
	}

	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	// Private functions
	var datepicker = function () {
		// minimum setup
		$('.datePicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			locale: 'no',
			format: 'yyyy-mm-dd',
			autoclose: true
		});

		$('.yearPicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years",
			autoclose: true
		});
	}
	
	// Base elements
	var wizardEl;
	var formEl;
	var validator;
	var wizard;
	
	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		wizard = new KTWizard('kt_wizard_v3', {
			startStep: 1,
		});

		// Validation before going to next page
		wizard.on('beforeNext', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
		});

		wizard.on('beforePrev', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
		});

		// Change event
		wizard.on('change', function(wizard) {
			KTUtil.scrollTop();	
		});
	}

	var initValidation = function() {
		validator = formEl.validate({
			// Validate only visible fields
			ignore: ":hidden",

			// Validation rules
			rules: {
                "info[last_name]": {
                    required: true 
				},
				"info[first_name]": {
                    required: true 
                },
                "info[birth_date]": {
                    required: true 
				},
				"info[sales_group_id]": {
					required: true
				},
				"info[buyer_position_id]": {
					required: true
				},
				"info[mobile_no]": {
					required: true
				},
                "info[email]": {
                    required: true,
					email: true,
					remote: {
						url: base_url + 'buyer/checkmail',
						type: 'POST',
						data: {
							"key": function () {
								return $("#buyer_email_key").val();
								},
							"info[email]": function () {
								return $("#buyer_email").val();
								}
						}
					}
				},
				"info[present_address]": {
					required: true
				},
				"source_info[recruiter]": {
					required: true
				},
				"source_info[supervisor]": {
					required: true
				},
				"source_info[to_report]": {
					required: true
				},
				"source_info[to_attend]": {
					required: true
				},
				"source_info[commission_based]": {
					required: true
				}
				
            },
			messages: {
				"info[last_name]":'Last name field is required',
				"info[first_name]":'First name field is required',
				"info[birth_date]":'Birth date field is required',
				"info[sales_group_id]":'Sales Group field is required',
				"info[buyer_position_id]":'Buyer Type field is required',
				"info[email]": {
					required: 'Email field is required',
					email: 'Invalid email address',
					remote:jQuery.validator.format("{0} is already taken, please enter a different email address.")
				},
				"info[mobile_no]":'Mobile Number field is required',
				"info[present_address]":'Present Address field is required',
				"source_info[recruiter]":'Recruiter field is required',
				"source_info[supervisor]":'Supervisor field is required',
			},
			// Display error  
			invalidHandler: function(event, validator) {	 
				KTUtil.scrollTop();

				swal.fire({
					"title": "", 
					"text": "There are some errors in your submission. Please correct them.", 
					"type": "error",
					"confirmButtonClass": "btn btn-secondary"
				});
			},

			// Submit valid form
			submitHandler: function (form) {
				
			}
		});   
	}

	var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formEl.ajaxSubmit({
                    type: 'POSt',
                    dataType: 'JSON',
					success: function(response) {
						if (response.status) {

							swal.fire({
								title: "Success!",
								text: response.message,
								type: "success"
							}).then(function() {
								window.location.replace(base_url + "buyer");
							});

						} else {
							swal.fire(
								'Oops!',
								response.message,
								'error'
							)
						}
					}
				});
			}
		});
	}

    // Public functions
    return {
        init: function() {
			sgSelect();
			datepicker(); 
			formRepeater();

			wizardEl = KTUtil.get('kt_wizard_v3');
			formEl = $('#update_buyer');

			initWizard(); 
			initValidation();
			initSubmit();

        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    BuyerCreate.init();
});