// Class definition

var house = (function () {
    var select2 = function () {
        $('#project_id').select2({
            placeholder: 'Select a project',
        });
    };

    var select3 = function() {
		$('#house_template_id').select2({
			placeholder: "Select a template"
		});
    };

    var valHouse = function () {
        $('#house_form').validate({
            // define validation rules
            rules: {
                project_id: {
                    required: true,
                },
                name: {
                    required: true,
                },
                code: {
                    required: true,
                },
                reservation_fee: {
                    required: true,
                    number: true,
                },
                floor_area: {
                    required: true,
                    number: true,
                },
                lot_area: {
                    required: true,
                    number: true,
                },
                no_bedroom: {
                    number: true,
                },
                no_bathroom: {
                    number: true,
                },
                no_floor: {
                    number: true,
                },
                no_ac: {
                    number: true,
                },
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                toastr.error(
                    'Please check your fields',
                    'Something went wrong'
                );

                event.preventDefault();
            },

            // submitHandler: function (form) {
            //     form[0].submit(); // submit the form
            // }
        });
    };

    var dropzoneActual = function () {
        Dropzone.autoDiscover = false;

        jQuery(document).ready(function () {
            const dropzones = [];
            var houseModID = $('input#house_model_id').val();

            $('.dropzone').each(function (i, el) {
                const name = $(el).attr('id');
                var myDropzone = new Dropzone(el, {
                    url: base_url + 'house/update',
                    paramName: name, // The name that will be used to transfer the file
                    maxFiles: 10,
                    maxFilesize: 5, // MB
                    addRemoveLinks: true,
                    thumbnailWidth: 230,
                    thumbnailHeight: 230,
                    autoProcessQueue: false,
                    acceptedFiles: '.png,.jpg,.gif,.jpeg',
                    removedfile: function (file) {
                        // x = confirm('Do you want to delete?');
                        // if(!x)  return false;
                        swal.fire({
                            title: 'Are you sure?',
                            text: "You won't be able to revert this!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: 'No, cancel!',
                            reverseButtons: true,
                        }).then(function (result) {
                            if (result.value) {
                                $.ajax({
                                    type: 'POST',
                                    url: base_url + 'house/remove_pic/' + name,
                                    data: {
                                        fileName: file.name,
                                        img_id: file.id,
                                    },
                                    dataType: 'json',
                                })
                                    .done(function (response) {
                                        if (response.status) {
                                            toastr.success(
                                                response.msg,
                                                'Removed'
                                            );

                                            window.location.reload();
                                        } else {
                                            toastr.error(response.msg, 'Oops!');
                                        }
                                    })
                                    .fail(function (resp) {
                                        console.log('xhr failed', resp);
                                    })
                                    .always(function (resp) {
                                        // do nothing for now
                                    });
                            } else if (result.dismiss === 'cancel') {
                                swal.fire(
                                    'Cancelled',
                                    'Your imaginary file is safe :)',
                                    'error'
                                );
                            }
                        });
                    },
                    init: function () {
                        var thisDropzone = this;
                        $.get(
                            base_url +
                                'house/get_image/' +
                                name +
                                '/' +
                                houseModID,
                            function (data) {
                                $.each(JSON.parse(data), function (key, value) {
                                    var mockFile = {
                                        name: value.name,
                                        size: value.size,
                                        id: value.id,
                                    };

                                    thisDropzone.options.addedfile.call(
                                        thisDropzone,
                                        mockFile
                                    );

                                    thisDropzone.options.thumbnail.call(
                                        thisDropzone,
                                        mockFile,
                                        value.path
                                    );
                                });
                            }
                        );

                        // thisDropzone.on("removedfile", function(file) {
                        //     // 'file' parameter contains the file object.
                        //     console.log('Removed File', file);

                        //     swal.fire({
                        //         title: 'Are you sure?',
                        //         text: "You won't be able to revert this!",
                        //         type: 'warning',
                        //         showCancelButton: true,
                        //         confirmButtonText: 'Yes, delete it!',
                        //         cancelButtonText: 'No, cancel!',
                        //         reverseButtons: true
                        //     }).then(function (result) {
                        //         if (result.value) {

                        //             $.ajax({
                        //                 type: 'POST',
                        //                 url: base_url + 'house/remove_pic/' + name,
                        //                 data: {
                        //                     fileName: file.name,
                        //                 },
                        //                 dataType: 'json'
                        //             }).done(function (response) {
                        //                 // check repsonse, notify user
                        //             }).fail(function(resp) {
                        //                 console.log('xhr failed', resp);
                        //             }).always(function(resp) {
                        //                 // do nothing for now
                        //             });

                        //         } else if (result.dismiss === 'cancel') {
                        //             swal.fire(
                        //                 'Cancelled',
                        //                 'Your imaginary file is safe :)',
                        //                 'error'
                        //             )
                        //         }
                        //     })
                        //     // Delete file from server

                        // });
                    },
                });

                dropzones.push(myDropzone);
            });

            document
                .querySelector('#btn_update')
                .addEventListener('click', function (e) {
                    // Make sure that the form isn't actually being sent.
                    e.preventDefault();
                    e.stopPropagation();
                    let form = new FormData($('#house_form')[0]);

                    dropzones.forEach((dropzone) => {
                        let { paramName } = dropzone.options;
                        dropzone.files.forEach((file, i) => {
                            form.append(paramName + '[' + i + ']', file);
                        });
                    });

                    if ($('#house_form').valid()) {
                        $.ajax({
                            method: 'POST',
                            data: form,
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                            success: function (response) {
                                if (response.status) {
                                    var redirectUrl = base_url + 'house/index';

                                    toastr.options.onShown = function () {
                                        window.location.href = redirectUrl;
                                    };
                                    toastr.options.onHidden = function () {
                                        window.location.href = redirectUrl;
                                    };
                                    toastr.options.onclick = function () {
                                        window.location.href = redirectUrl;
                                    };
                                    toastr.options.onCloseClick = function () {
                                        window.location.href = redirectUrl;
                                    };

                                    toastr.success(response.message, 'Success');
                                } else {
                                    toastr.error(response.message, 'Oops!');
                                }
                            },
                        });
                    }
                });
        });
    };

    // Public functions
    return {
        init: function () {
            select2();
            select3();
            valHouse();
            dropzoneActual();
        },
    };
})();

house.init();
