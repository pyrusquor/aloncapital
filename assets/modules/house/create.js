// Class definition
var house = function() {
	var select2 = function() {
		$('#project_id').select2({
			placeholder: "Select a project"
		});
    };

    var select3 = function() {
		$('#house_template_id').select2({
			placeholder: "Select a template"
		});
    };
    
    var valHouse = function () {
        $( "#house_form" ).validate({
            // define validation rules
            rules: {
                project_id: {
                    required: true 
                },
                name: {
                    required: true 
                },
                code: {
                    required: true 
                },
                reservation_fee: {
                    required: true,
                    number: true
                },
                floor_area: {
                    required: true,
                    number: true
                },
				lot_area: {
                    required: true,
                    number: true
                },
                no_bedroom: {
                    number: true
                },
                no_bathroom: {
                    number: true
                },
                no_floor: {
                    number: true
                },
                no_ac: {
                    number: true
                },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            // submitHandler: function (form) {
            //     form[0].ajaxSubmit(); // submit the form
            // }
        });       
    }
    var dropzoneActual = function () {
        // file type validation
        // Dropzone.options.dropzoneActual = {
        //     url: base_url + 'house/create',
        //     paramName: "file", // The name that will be used to transfer the file
        //     maxFiles: 10,
        //     maxFilesize: 10, // MB
        //     addRemoveLinks: true,
        //     autoProcessQueue: false,
        //     acceptedFiles:".png,.jpg,.gif,.bmp,.jpeg",
        //     init: function(){
        //         var actualDropzone = this;
        //         $("#btn_create").click(function (e) {
        //             e.preventDefault();
        //             actualDropzone.processQueue();
        //         });

        //         actualDropzone.on('sending', function(file, xhr, formData) {
        //             // Append all form inputs to the formData Dropzone will POST
        //             var data = $('#house_form').serializeArray();

        //             $.each(data, function(key, el) {
        //                 formData.append(el.name, el.value);
        //             });
        //         });
               
        //     },
        // };

        Dropzone.autoDiscover = false;

        jQuery(document).ready(function(){

            const dropzones = [];

            $('.dropzone').each(function(i, el){
                const name = $(el).attr('id')
                var myDropzone = new Dropzone(el, {
                    url: base_url + 'house/create',
                    autoProcessQueue: false,
                    uploadMultiple: true,
                    parallelUploads: 100,
                    maxFiles: 100,
                    paramName: name,
                    addRemoveLinks: true,
                    acceptedFiles:".png,.jpg,.gif,.jpeg",
                })

                dropzones.push(myDropzone)
            })

            document.querySelector("#btn_create").addEventListener("click", function(e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();

                let form = new FormData($('#house_form')[0])
            
                dropzones.forEach(dropzone => {

                    let  { paramName }  = dropzone.options
                    dropzone.files.forEach((file, i) => {
                        form.append(paramName + '[' + i + ']', file)
                    })
                })

                if($("#house_form").valid()){

                    dropzones.forEach(dropzone => {
                        dropzone.processQueue();
                    })

                    $.ajax({
                        method: 'POST',
                        data: form,
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        success: function(response) {

                            if ( response.status ) {
                                
					
                                var redirectUrl = base_url + 'house/index';

                                toastr.options.onShown      = function() { window.location.href = redirectUrl; }
                                toastr.options.onHidden     = function() { window.location.href = redirectUrl; }
                                toastr.options.onclick      = function() { window.location.href = redirectUrl; }
                                toastr.options.onCloseClick = function() { window.location.href = redirectUrl; }

                                toastr.success(response.message, "Success");
                                
							} else {

								toastr.error(response.message, "Oops!");
							}
                        }
                    });
                }
                
            });
        });
    }

	// Public functions
    return {
        init: function() {
            select2();
            select3();
            valHouse();
            dropzoneActual();
        }
    };
}();

house.init();




    


