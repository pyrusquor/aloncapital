// Class definition
var house = function() {
	var select2 = function() {
		$('#project_id').select2({
			placeholder: "Select a project"
		});
    };
    
    var valHouse = function () {
        $( "#house_form" ).validate({
            // define validation rules
            rules: {
                project_id: {
                    required: true 
                },
                name: {
                    required: true 
                },
                reservation_fee: {
                    required: true
                },
				furnishing_fee: {
                    required: true 
                },
                floor_area: {
                    required: true
                },
				lot_area: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }

    var dropzoneActual = function () {
        // file type validation
        Dropzone.options.dropzoneActual = {
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 10,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            autoProcessQueue: false,
            acceptedFiles:".png,.jpg,.gif,.bmp,.jpeg",
            init: function(){
                var submitButton = document.querySelector('#btn_create');
                actualDropzone = this;
                submitButton.addEventListener("click", function(e){
                    // var actual_action = $("#dropzone-actual").attr('action');
                    // $("#dropzone-actual").attr('action', actual_action+$('#code').val());
                    
                    actualDropzone.processQueue();
                });
                this.on("complete", function(){
                    if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
                    {
                        var _this = this;
                        _this.removeAllFiles();
                    }
                });
            },
        };

        Dropzone.options.dropzoneFloor = {
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 10,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            autoProcessQueue: false,
            acceptedFiles:".png,.jpg,.gif,.bmp,.jpeg",
            init: function(){
                var submitButton = document.querySelector('#btn_create');
                floorDropzone = this;
                submitButton.addEventListener("click", function(e){
                    // var floor_action = $("#dropzone-floor").attr('action');
                    // $("#dropzone-floor").attr('action', floor_action+$('#code').val());
                    
                    floorDropzone.processQueue();
                });
                this.on("complete", function(){
                    if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
                    {
                        var _this = this;
                        _this.removeAllFiles();
                    }
                });
            },
        };
    }

    // var code_change = function () {
    //     $('#code').change(function() {
    //         var actual_action = $("#dropzone-actual").attr('action');
    //         actual_action = actual_action.substring(0,actual_action.lastIndexOf('/') +1);
    //             actualDropzone.options.url = actual_action+$('#code').val();
    //         var floor_action = $("#dropzone-floor").attr('action');
    //         floor_action = floor_action.substring(0,floor_action.lastIndexOf('/') +1);
    //             floorDropzone.options.url = floor_action+$('#code').val();
    //     });
    // }
	
	// Public functions
    return {
        init: function() {
            select2();
            valHouse();
            dropzoneActual();
            // code_change();
        }
    };
}();

house.init();