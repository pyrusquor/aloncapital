$(document).ready(function () {
    var datepicker = function () {
        // minimum setup
        $('#request_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function (e) {
            app.changeRequestDate($('#request_date').val());
            app.checkInfoForm();
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true
        });
    };

    datepicker();

    $("#company_id").on("change", function (e) {
        app.changeCompanyId($(this).val());
        app.checkInfoForm();
    });
    $("#approving_staff_id").on("change", function (e) {
        app.changeApprovingStaffId($(this).val());
        app.checkInfoForm();
    });
    $("#requesting_staff_id").on("change", function (e) {
        app.changeRequestingStaffId($(this).val());
        app.checkInfoForm();
    });

});

const __info_form_defaults = {
    id: null,
    project_id: null,
    company_id: null,
    approving_staff_id: null,
    requesting_staff_id: null,
    sub_project_id: null,
    amenity_id: null,
    sub_amenity_id: null,
    property_id: null,
    department_id: null,
    vehicle_equipment_id: null,
    reference: null,
    days_lapsed: null,
    particulars: null,
    request_date: null,
    request_amount: null,
    request_reason: null,
    customer_type: null,
    request_type: null,
    accounting_ledger_id: null,
}

const __request_items_data_defaults = {
    item_group_id: null,
    item_type_id: null,
    item_brand_id: null,
    item_class_id: null,
    item_id: null,
    unit_of_measurement_id: null,
    quantity: 0,
    unit_cost: 0.0,
    total_cost: 0.0,
    id: null,
};

let __request_data_fields = {};

const __info_form_required_defaults = [
    'approving_staff_id',
    'requesting_staff_id',
    'request_date',
    'customer_type',
    'request_type',
    // 'accounting_ledger_id'
];

const __request_items_required_defaults = [
    'item_group_id',
    'item_type_id',
    // 'item_brand_id',
    // 'item_class_id',
    'item_id',
    'unit_of_measurement_id',
    'quantity',
    'unit_cost',
    'total_cost',
];

const __store_key_assoc = {
    'item_group_id': {
        store: 'item_groups',
        obj_name: 'item_group'
    },
    'item_type_id': {
        store: 'item_types',
        obj_name: 'item_type'
    },
    'item_brand_id': {
        store: 'item_brands',
        obj_name: 'item_brand'
    },
    'item_class_id': {
        store: 'item_classes',
        obj_name: 'item_class'
    },
    'item_id': {
        store: 'items',
        obj_name: 'item'
    },
    'unit_of_measurement_id': {
        store: 'units_of_measurement',
        obj_name: 'unit_of_measurement'
    },
}

var app = new Vue({
    el: '#material_request_app',
    data: {
        is_mounted: false,
        object_request_id: null,
        wizard: {
            current: 1,
        },
        info: {
            form: {},
            data: {},
            required_fields: [],
            form_is_valid: false,
            ui: {
                show_project: false,
                show_sub_project: false,
                show_amenity: false,
                show_sub_amenity: false,
                show_property: false,
                show_department: false,
                show_vehicle_equipment: false,
            },
            total_cost: 0.0,
        },
        request_items: {
            cart: {},
            data: {},
            form: [],
            deleted_items: [],
            cart_is_valid: false,
            form_is_valid: false,
            active: null
        },

    },
    watch: {
        "request_items.active": function (newVal, oldVal) {
            if (newVal === null) {
                this.request_items.cart = JSON.parse(JSON.stringify(__request_items_data_defaults));
            }
        },
        "request_items.cart": {
            handler: function (newVal, oldVal) {
                this.checkItemsCart();
            }, deep: true
        },
        "request_items.form": {
            handler: function (newVal, oldVal) {
                this.calculateTotalCost();
                this.checkItemsForm();
            }, deep: true
        },
        "request_items.cart.item_group_id": function (newVal, oldVal) {
            if (newVal) {
                this.fetchItemObjects("item_type", "item_types", newVal, "group_id");
            } else {
                this.request_items.data.item_types = [];
            }
        },
        "request_items.cart.item_type_id": function (newVal, oldVal) {
            if(newVal){
                this.fetchItems();
                this.fetchItemObjects("item_class", "item_classes", newVal, "item_type_id");
            } else {
                this.request_items.data.item_classes = [];
            }
        },
        "request_items.cart.item_brand_id": function (newVal, oldVal) {
            if(newVal){
                this.fetchItems();
            }
        },
        "request_items.cart.item_id": function (newVal, oldVal) {
            if (newVal) {
                let item = this.searchItem(newVal);
                this.request_items.cart.unit_cost = item.unit_price;
            }else{
                this.request_items.cart.unit_cost = 0.0;
                this.request_items.cart.quantity = 0;
            }
        },
        "request_items.cart.quantity": function (newVal, oldVal) {
            if (newVal && newVal > 0) {
                if (this.request_items.cart.unit_cost > 0) {
                    this.request_items.cart.total_cost = this.request_items.cart.unit_cost * this.request_items.cart.quantity;
                }
            } else {
                this.request_items.cart.total_cost = 0.0;
            }
        },
        "info.form.customer_type": function (newCustomerType, oldCustomerType) {
            this.resetCustomerTypeDependencies();
            switch (newCustomerType) {
                case "project":
                    this.info.ui.show_project = true;
                    this.info.required_fields.push('project_id');
                    break;
                case "sub_project":
                    this.info.ui.show_project = true;
                    this.info.ui.show_sub_project = true;
                    this.info.required_fields.push('project_id');
                    this.info.required_fields.push('sub_project_id');
                    break;
                case "amenity":
                    this.info.ui.show_project = true;
                    this.info.ui.show_amenity = true;
                    this.info.required_fields.push('project_id');
                    this.info.required_fields.push('amenity_id');
                    break;
                case "sub_amenity":
                    this.info.ui.show_project = true;
                    this.info.ui.show_amenity = true;
                    this.info.ui.show_sub_amenity = true;
                    this.info.required_fields.push('project_id');
                    this.info.required_fields.push('amenity_id');
                    this.info.required_fields.push('sub_amenity_id');
                    break;
                case "block_and_lot":
                    this.info.ui.show_project = true;
                    this.info.ui.show_block_and_lot = true;
                    this.info.required_fields.push('project_id');
                    this.info.required_fields.push('property_id');
                    break;
                case "department":
                    this.info.ui.show_department = true;
                    this.info.required_fields.push('department_id');
                    break;
                case "vehicle_equipment":
                    this.info.ui.show_vehicle_equipment = true;
                    this.info.required_fields.push('vehicle_equipment_id');
                    break;
            }
            this.checkInfoForm();
        }
    },
    methods: {
        wizardSwitchPane(n) {
            this.wizard.current = n;
        },
        wizardLabelState(n) {
            if (n === this.wizard.current) {
                return "current";
            } else {
                return "";
            }
        },
        wizardStepStateClass(n) {
            if (n === this.wizard.current) {
                return "kt-wizard-v3__content active";
            } else {
                return "kt-wizard-v3__content";
            }
        },

        // init
        initObjectRequest() {
            this.object_request_id = window.object_request_id;
            this.fetchObjectRequest();
        },
        resetCustomerTypeDependencies() {
            this.info.ui.show_project = false;
            this.info.ui.show_sub_project = false;
            this.info.ui.show_amenity = false;
            this.info.ui.show_sub_amenity = false;
            this.info.ui.show_block_and_lot = false;
            this.info.ui.show_department = false;
            this.info.ui.show_vehicle_equipment = false;
            // this.info_form = this.setInfoFormDefaults(); // JSON.parse(JSON.stringify(__info_form_defaults));
            this.info.required_fields = JSON.parse(JSON.stringify(__info_form_required_defaults));
        },

        // cart
        addItemToRequest(source) {
            let form = {};
            let data = {};
            switch (source) {
                case 'cart':
                    data = this.request_items.cart;
                    break;
            }
            if (this.request_items.active !== null) {
                form = data;
                this.request_items.form[this.request_items.active] = form;
            } else {
                for (let i in data) {
                    let key = this.searchItemKey(i);
                    let obj = null;
                    if (key) {
                        if (source === 'cart') {
                            form[key.obj_name] = this.searchItemRef(data[i], key.store);
                            form[i] = data[i];
                        } else {
                            // traverse object
                        }
                    } else {
                        form[i] = data[i];
                    }
                }
                if (form.item_brand == null) {
                    form.item_brand = {
                        id: null,
                        name: ""
                    }
                }
                if (form.item_class == null) {
                    form.item_class = {
                        id: null,
                        name: ""
                    }
                }
                this.request_items.form.push(form);
            }
            this.request_items.active = null;
            this.request_items.cart = JSON.parse(JSON.stringify(__request_items_data_defaults));
            this.$forceUpdate();
        },
        calculateTotalCost() {
            let total = 0.0;
            for (i in this.request_items.form) {
                total += (this.request_items.form[i].total_cost * 1.0);
            }
            this.info.form.request_amount = total;
        },
        loadItem(key) {
            let data = this.request_items.form[key]; //JSON.parse(JSON.stringify(this.request_items.form[key]));
            this.$set(this.request_items, "cart", data);
            this.request_items.active = key;
            this.$forceUpdate();
        },
        removeItem(key) {
            let item = this.request_items.form[key];
            if (item.id) {
                this.request_items.deleted_items.push(item.id);
            }
            this.request_items.form.splice(key, 1);
        },

        // parsers
        parseObjectRequest(data) {
            if (data.length > 0) {
                this.info.form = data[0];
                this.fetchObjectRequestChildren();
            }
        },
        parseObjectRequestItems(data) {
            if (data.length > 0) {
                this.request_items.form = data;
            }
        },

        // search
        searchItemRef(id, store) {
            for (let i = 0; i < this.request_items.data[store].length; i++) {
                if (this.request_items.data[store][i].id === id) {
                    return this.request_items.data[store][i]
                }
            }
            return null;
        },
        searchItemKey(key) {
            for (i in __store_key_assoc) {
                if (key === i) {
                    return __store_key_assoc[i];
                }
            }
            return false;
        },
        searchItem(id) {
            for (let i = 0; i < this.request_items.data.items.length; i++) {
                if (this.request_items.data.items[i].id === id) {
                    return this.request_items.data.items[i]
                }
            }
            return null;
        },
        searchItemBrands(id) {
            for (let i = 0; i < this.request_items.data.item_brands.length; i++) {
                if (this.request_items.data.item_brands[i].id === id) {
                    return this.request_items.data.item_brands[i]
                }
            }
            return null;
        },
        searchItemClasses(id) {
            for (let i = 0; i < this.request_items.data.item_classes.length; i++) {
                if (this.request_items.data.item_classes[i].id === id) {
                    return this.request_items.data.item_classes[i]
                }
            }
            return null;
        },
        searchItemGroups(id) {
            for (let i = 0; i < this.request_items.data.item_groups.length; i++) {
                if (this.request_items.data.item_groups[i].id === id) {
                    return this.request_items.data.item_groups[i]
                }
            }
            return null;
        },
        searchItemTypes(id) {
            for (let i = 0; i < this.request_items.data.item_types.length; i++) {
                if (this.request_items.data.item_types[i].id === id) {
                    return this.request_items.data.item_types[i]
                }
            }
            return null;
        },
        searchUnits(id) {
            for (let i = 0; i < this.request_items.data.units_of_measurement.length; i++) {
                if (this.request_items.data.units_of_measurement[i].id === id) {
                    return this.request_items.data.units_of_measurement[i]
                }
            }
            return null;
        },

        // watchers
        projectChanged() {
            this.fetchInfoObjects("sub_projects", "project_id", this.info.form.project_id, 'sub_projects', null);
            this.fetchInfoObjects("properties", "project_id", this.info.form.project_id, 'properties', null);
        },
        amenityChanged() {
            this.fetchInfoObjects("sub_amenities", "amenity_id", this.info.form.project_id, 'sub_amenities', null);
        },
        checkInfoForm() {
            let fields_complete = true;
            for (let i = 0; i < this.info.required_fields.length; i++) {
                let req_field = this.info.form[this.info.required_fields[i]];
                if (!req_field) {
                    fields_complete = false;
                }
            }
            this.info.form_is_valid = fields_complete;
        },
        checkItemsForm() {
            this.request_items.form_is_valid = this.request_items.form.length > 0;
        },
        checkItemsCart() {
            let valid = true;
            for (i in __request_items_required_defaults) {
                let key = __request_items_required_defaults[i];
                if(!this.request_items.cart[key]){
                    valid = false;
                }
            }
            this.request_items.cart_is_valid = valid;
        },
        changeRequestDate(date) {
            this.info.form.request_date = date; //date.toISOString().split("T")[0];
        },
        changeCompanyId(id) {
            this.info.form.company_id = id;
        },
        changeApprovingStaffId(id) {
            this.info.form.approving_staff_id = id;
        },
        changeRequestingStaffId(id) {
            this.info.form.requesting_staff_id = id;
        },

        // fetchers
        fetchObjectRequest() {
            if (this.object_request_id) {

                let url = base_url + "material_request/search?id=" + this.object_request_id + "&with_relations=no";
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectRequest(response.data);
                            this.fetchObjectRequestChildren();
                        }
                    });
            } else {
                this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
            }
            this.request_items.cart = JSON.parse(JSON.stringify(__request_items_data_defaults));
        },
        fetchObjectRequestChildren() {
            let url = base_url + "material_request_item/get_all_by_request/" + this.object_request_id;
            axios.get(url)
                .then(response => {
                    if (response.data) {

                        response.data.forEach(item => {

                            if (!('item_class' in item)) {
                                item.item_class = {
                                    id: null,
                                    name: ""
                                }
                            } else if (item.item_class == null) {
                                item.item_class = {
                                    id: null,
                                    name: ""
                                }
                            }

                            if (!('item_brand' in item)) {
                                item.item_brand = {
                                    id: null,
                                    name: ""
                                }
                            } else if (item.item_brand == null) {
                                item.item_brand = {
                                    id: null,
                                    name: ""
                                }
                            }
                        })

                        this.parseObjectRequestItems(response.data);
                    }
                });
        },
        async fetchInfoObjects(table, field, value, store, order) {
            if (!order) {
                order = "name ASC";
            }
            let url = base_url + "generic_api/fetch_all?table=" + table + "&order=" + order;
            if (field && value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + value + "&order=" + order;
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        //this.info.data[store] = response.data;
                        this.$set(this.info.data, store, response.data);
                    } else {
                        this.$set(this.info.data, store, []);
                    }
                });
        },
        async fetchItemObjects(table, store, value, fk) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            if (value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + fk + "&value=" + value + "&order=name ASC";
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.$set(this.request_items.data, store, response.data);
                        this.$forceUpdate();
                    } else {
                        this.$set(this.request_items.data, store, []);
                    }
                });
        },
        async fetchItems() {
            var params = {};
            let store = "items";
            if (this.request_items.cart.item_brand_id) {
                params["brand_id"] = this.request_items.cart.item_brand_id;
            }
            if (this.request_items.cart.item_type_id) {
                params["type_id"] = this.request_items.cart.item_type_id;
            }

            let url = base_url + "item/get_all_items";
            if (Object.keys(params).length > 0) {
                url = url + "?" + new URLSearchParams(params).toString();
            }

            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.$set(this.request_items.data, store, response.data.data);
                        if(! response.data.data){
                            this.request_items.cart.item_id = null;
                        }
                    } else {
                        this.$set(this.request_items.data, store, []);
                    }
                    this.$forceUpdate();
                })
        },

        // form
        submitRequest() {
            let info_data = this.prepInfoData();
            let items_data = this.prepObjectRequestItems();
            let data = {
                request_amount: this.info.form.request_amount,
                items: items_data,
                deleted_items: this.request_items.deleted_items
            }

            let url = base_url + "material_request/amend";

            if (this.material_request_id) {
                url = base_url + "material_request/amend/" + this.material_request_id;
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                success: function (response) {
                    if(response.status){
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                base_url + "material_request"
                            );
                        });
                    }
                }
            })

        },

        prepInfoData() {
            let form = {};
            for (key in this.info.form) {
                if (this.searchField(__request_data_fields, key)) {
                    if (this.info.form[key]) {
                        form[key] = this.info.form[key];
                    } else {
                        form[key] = null;
                    }
                }
            }
            return form;
        },

        prepObjectRequestItems() {
            let items_data = [];
            for (i in this.request_items.form) {
                let item_data = {};
                for (key in this.request_items.form[i]) {
                    if (this.searchField(__request_items_data_defaults, key)) {
                        if (this.request_items.form[i][key]) {
                            item_data[key] = this.request_items.form[i][key];
                        } else {
                            item_data[key] = null;
                        }
                    }
                }
                items_data.push(item_data);
            }
            return items_data;
        },

        searchField(fields, field) {
            for (key in fields) {
                if (field === key) {
                    return true;
                }
            }
            return false;
        }
    },
    mounted() {
        this.fetchInfoObjects("projects", "", "", "projects", null);
        this.fetchInfoObjects("amenities", "", "", "amenities", null);
        this.fetchInfoObjects("departments", "", "", "departments", null);
        this.fetchInfoObjects("companies", "", "", "companies", null);
        this.fetchInfoObjects("staff", "", "", "staff", "id DESC");

        this.fetchItemObjects("item_group", "item_groups", "");
        this.fetchItemObjects("item_brand", "item_brands", "");
        this.fetchItemObjects("item_class", "item_classes", "");
        this.fetchItemObjects("inventory_settings_unit_of_measurements", "units_of_measurement", "");
        this.initObjectRequest();

        this.is_mounted = true;
        this.material_request_id = window.object_request_id;
        __request_data_fields = window.object_request_fillables;

        this.checkItemsForm();
    }
});