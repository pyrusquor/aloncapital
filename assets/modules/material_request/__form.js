// Class definition
var Create = function () {
    var formRepeater = function () {
        $("#form_material_request").repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                datepicker();
                // listItemDetail();
                // calculateItemTotalPrice();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
        });
    };

    var entryFormRepeater = function () {
        $("#entry_item_form_repeater").repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                datepicker();
                getLedgers();
                materialType();
                calculateDTotal();
                calculateCTotal();

            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
        });
    };

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true
        });
    };

    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    // var _add_ons;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        wizard.on('beforePrev', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });


    };

    var initValidation = function () {

        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                "request[request_date]": {
                    required: true,
                },

                "request[approving_staff_id]": {
                    required: true,
                },

                "request[requesting_staff_id]": {
                    required: true,
                },
                "request[company_id]": {
                    required: true,
                },
                // "request[project_id]": {
                //     required: true,
                // },
                "request[request_amount]": {
                    required: true,
                },
            },
            // messages: {
            // 	"info[last_name]":'Last name field is required',
            // },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {
                initSubmit();
            }
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on("click", function (e) {
            e.preventDefault();
            if (validator.form()) {
                // See: src\js\framework\base\app.js

                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    type: "POST",
                    dataType: "JSON",
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success",
                            }).then(function () {
                                window.location.replace(
                                    base_url + "material_request"
                                );
                            });
                        } else {
                            swal.fire({
                                title: "Oops!",
                                html: response.message,
                                icon: "error",
                            });
                        }
                    },
                });
            }

        });
    };

    var _add_ons = function () {
        datepicker();
    };
    // Public functions
    return {
        init: function () {
            datepicker();
            // formRepeater();
            // entryFormRepeater();
            // _add_ons();

            wizardEl = KTUtil.get('kt_wizard_v3');
            formEl = $('#form_material_request');

            initWizard();
            initValidation();
            initSubmit();

        }
    };
}();

// Initialization
jQuery(document).ready(function () {
    Create.init();

    var brandSelector = $('.brand-select');
    var classSelector = $('.class-select');


});

const dTotalInput = document.querySelector("#dTotal");
const cTotalInput = document.querySelector("#cTotal");
const grossAmount = document.querySelector("#gross_amount");
const addEntryBtn = document.querySelector("#next_btn");

const getItemBrands = () => {
    const objectSelect = document.querySelectorAll(".brand-select");

    objectSelect.forEach((select) => {
        $.ajax({
            url: base_url + "item_brand/get_all_brands",
            type: "GET",
            success: function (data) {
                const brands = JSON.parse(data);
                brands.data.map((brand) => {
                    let opt = document.createElement("option");
                    opt.value = brand.id;
                    opt.innerHTML = brand.name;
                    select.appendChild(opt);
                })
            }
        })
    })
}

const getItemClasses = () => {
    const objectSelect = document.querySelectorAll(".class-select");

    objectSelect.forEach((select) => {
        $.ajax({
            url: base_url + "item_class/get_all_classes",
            type: "GET",
            success: function (data) {
                const item_classes = JSON.parse(data);
                item_classes.data.map((item_class) => {
                    let opt = document.createElement("option");
                    opt.value = item_class.id;
                    opt.innerHTML = item_class.name;
                    select.appendChild(opt);
                })
            }
        })
    })
}


const getItems = () => {
    // Get the element again
    const entryItemLedgerSelect = document.querySelectorAll(
        "#item_id"
    );
    // Loop through the element
    entryItemLedgerSelect.forEach((select) => {
        $.ajax({
            url: base_url + "item/get_all_items",
            type: "GET",
            success: function (data) {
                const items = JSON.parse(data);
                items.data.map((item) => {
                    let opt = document.createElement("option");

                    opt.value = item.id;
                    opt.innerHTML = item.name;
                    select.appendChild(opt);
                });
            },
        });
    });
};

const totalOfItemAmount = (el) => {

    var total = 0;

    el.forEach(item => total += parseFloat($(item).val().trim() || 0));
    if (total != 0) {
        grossAmount.value = total;
    }
}


addEntryBtn.addEventListener("click", function () {
    const amountEl = document.querySelectorAll("#item_total_amount");
    totalOfItemAmount(amountEl);
})


const materialType = () => {
    const entryItemDcSelect = document.querySelectorAll("#entry_item_dc");
    const dInput = document.querySelectorAll("#dr_amount_input");
    const cInput = document.querySelectorAll("#cr_amount_input");

    entryItemDcSelect.forEach((select, index) => {
        $(select).on("change", function () {
            if (this.value !== "") {
                if (this.value == "d") {
                    cInput[index].disabled = true;
                    cInput[index].value = 0;
                    dInput[index].disabled = false;
                } else if (this.value == "c") {
                    cInput[index].disabled = false;
                    dInput[index].disabled = true;
                    dInput[index].value = 0;
                }
            }
        });
    });
};

const calculateDTotal = () => {
    const dInput = document.querySelectorAll("#dr_amount_input");
    // dTotalInput.value = 0;
    $(dInput).keyup(function () {
        var total = 0;

        $(dInput).each(function () {
            total += parseFloat($(this).val().trim() || 0);
        });
        dTotalInput.value = total.toFixed(2);
    });
};

const calculateCTotal = () => {
    const cInput = document.querySelectorAll("#cr_amount_input");
    // cTotalInput.value = 0;
    $(cInput).keyup(function () {
        var total = 0;

        $(cInput).each(function () {
            total += parseFloat($(this).val().trim() || 0);
        });
        cTotalInput.value = total.toFixed(2);
    });
};

const listItemDetail = () => {
    const listItemSelect = document.querySelectorAll("#item_id");
    const unitPrice = document.querySelectorAll("#item_unit_price");
    const quantity = document.querySelectorAll("#item_quantity");
    const taxID = document.querySelectorAll("#item_tax_id");
    const taxName = document.querySelectorAll("#tax_name");
    const taxAmount = document.querySelectorAll("#item_tax_amount");
    const totalAmount = document.querySelectorAll("#item_total_amount");

    listItemSelect.forEach((select, index) => {
        $(select).on("change", function () {
            const item = getSelectedItem(this.value);
            unitPrice[index].value = item['unit_price'];
            taxID[index].value = item['tax_id'];
            taxName[index].value = item['tax']['name'];
            taxAmount[index].value = item['total_price'] - item['unit_price'];
            totalAmount[index].value = item['total_price'];
        })
    })
};

const calculateItemTotalPrice = () => {
    const listItemSelect = document.querySelectorAll("#item_quantity");
    const unitPrice = document.querySelectorAll("#item_unit_price");
    const taxAmount = document.querySelectorAll("#item_tax_amount");
    const totalAmount = document.querySelectorAll("#item_total_amount");

    listItemSelect.forEach((select, index) => {
        $(select).on("change keyup blur", function () {
            const total = (parseFloat(unitPrice[index].value) + parseFloat(taxAmount[index].value)) * this.value;
            totalAmount[index].value = total.toFixed(2);
        })
    })
};

function getSelectedItem(itemID) {
    $.ajax({
        url: base_url + 'item/Item/get_item',
        type: 'post',
        data: {
            itemID,
            itemID
        },
        dataType: 'json',
        complete: function () {

        },
        success: function (json) {
            item = json;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        },
        timeout: 10000,
        async: false
    });

    return item;
}

listItemDetail();
calculateItemTotalPrice();
getItemBrands();
getItemClasses();
getItems();
// getLedgers();
materialType();
calculateDTotal();
calculateCTotal();

const form_actions = {
    "add": "Add Entry Item",
    "edit": "Edit Entry Item"
}

const __form_defaults = {
    item_class: null,
    item_brand: null,
    item_group: null,
    item_type: null,
    item: null,
    units_of_measurement: null,
    unit_cost: 0,
    total_cost: 0,
    quantity: 0
}

// const __info_form_defaults = {
//     project: null,
//     sub_project: null,
//     amenity: null,
//     sub_amenity: null,
//     property: null,
//     department: null,
//     vehicle_equipment: null
// }
const __info_form_defaults = window.info_form_data;

var app = new Vue({
    el: '#form_app',
    data: {
        material_request_id: null,
        form_action: {
            label: "Add Entry Item",
            action: "add",
            id: null
        },
        selected_item: null,
        form_data: __form_defaults,
        form_defaults: __form_defaults,
        form_is_valid: false,
        form_errors: [],
        item_data: [],
        item_brands: [],
        item_groups: [],
        item_types: [],
        item_classes: [],
        units_of_measurement: [],
        items: [],
        deleted_items: [],

        // info form
        customer_type: null,
        show_project: false,
        show_sub_project: false,
        show_amenity: false,
        show_sub_amenity: false,
        show_property: false,
        show_department: false,
        show_vehicle_equipment: false,

        info_required_fields: [],
        info_form_is_valid: false,

        info_data: {
            projects: [],
            sub_projects: [],
            amenities: [],
            sub_amenities: [],
            properties: [],
            departments: [],
            vehicle_equipments: []
        },

        info_form: [], // JSON.parse(JSON.stringify(__info_form_defaults))
    },
    watch: {
        customer_type: function (newCustomerType, oldCustomerType) {
            this.resetCustomerTypeDependencies();
            switch(newCustomerType){
                case "project":
                    this.show_project = true;
                    this.info_required_fields = ['project'];
                    break;
                case "sub_project":
                    this.show_project = true;
                    this.show_sub_project = true;
                    this.info_required_fields = ['project', 'sub_project'];
                    break;
                case "amenity":
                    this.show_project = true;
                    this.show_amenity = true;
                    this.info_required_fields = ['project', 'amenity'];
                    break;
                case "sub_amenity":
                    this.show_project = true;
                    this.show_amenity = true;
                    this.show_sub_amenity = true;
                    this.info_required_fields = ['project', 'amenity', 'sub_amenity'];
                    break;
                case "block_and_lot":
                    this.show_project = true;
                    this.show_property = true;
                    this.info_required_fields = ['project', 'property'];
                    break;
                case "department":
                    this.show_department = true;
                    this.info_required_fields = ['department'];
                    break;
                case "vehicle_equipment":
                    this.show_vehicle_equipment = true;
                    this.info_required_fields = ['vehicle_equipment']
                    break;
            }
            this.checkInfoForm();
        },
    },
    methods: {
        // info form start
        checkInfoForm() {
            // if(this.customer_type !== ''){
            //     let req_fields_len = this.info_required_fields.length;
            //     let fields_complete = true;
            //     if(req_fields_len > 0 && this.info_form > 0){
            //         for(let i = 0; i < req_fields_len; i++){
            //             let req_field = this.info_form[this.info_required_fields[i]];
            //             if(! req_field){
            //                 fields_complete = false;
            //             }
            //         }
            //         this.info_form_is_valid = fields_complete;
            //         $("#next_btn").removeClass("hide");
            //     }
            // }else{
            //     this.info_form_is_valid = false;
            //     $("#next_btn").addClass("hide");
            // }
        },
        projectChanged() {
            this.fetchObjects("sub_projects", "project_id", this.info_form.project, 'sub_projects');
            this.fetchObjects("properties", "project_id", this.info_form.project, 'properties');
        },
        amenityChanged() {
            this.fetchObjects("sub_amenities", "amenity_id", this.info_form.project, 'sub_amenities');
        },
        fetchObjects(table, field, value, store){
            const url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + value + "&order=name ASC";
            axios.get(url)
                .then(response => {
                    if(response.data){
                        this.info_data[store] = response.data;
                    }
                });
        },
        resetCustomerTypeDependencies() {
            this.show_project = false;
            this.show_sub_project = false;
            this.show_amenity = false;
            this.show_sub_amenity = false;
            this.show_block_and_lot = false;
            this.show_department = false;
            this.show_vehicle_equipment = false;
            this.info_form = this.setInfoFormDefaults(); // JSON.parse(JSON.stringify(__info_form_defaults));
            this.info_required_fields = [];
        },
        setInfoFormDefaults(){
            this.info_form = window.info_form_data;
            this.customer_type = window.customer_type;
        },
        // info form end
        formHasErrors() {
            let has_errors = false;
            this.form_errors = [];
            for (let f in this.form_data) {
                if (!this.formItemCheck(f)) {
                    has_errors = true;
                    this.form_errors.push(f);
                }
            }
            return has_errors;
        },
        formItemCheck(field) {
            return this.form_data[field] !== this.form_defaults[field];
        },
        parseForm(source) {
            return source;
        },
        addItem() {
            let form_has_errors = this.formHasErrors();
            if (form_has_errors === false) {
                if (this.form_action.action === "add") {
                    // fucking javascript copy by reference bullshit workaround
                    // this.item_data.push(JSON.parse(JSON.stringify(this.form_data)));
                    this.item_data.push(this.parseForm(this.form_data))
                } else {
                    this.item_data[this.form_action.id] = this.parseForm(this.form_data);
                }
                this.resetForm();
                this.form_is_valid = true;
            } else {
                console.log(form_has_errors);
            }
            this.selected_item = null;

        },
        removeItem(idx) {
            let row = this.item_data[idx];
            if (row.id) {
                this.deleted_items.push(row.id);
            }
            this.item_data.splice(idx, 1);
            if (this.item_data.length <= 0) {
                this.form_is_valid = false;
            }
        },
        editItem(idx) {
            this.form_data = {
                item_class: this.item_data[idx].item_class,
                item_brand: this.item_data[idx].item_brand,
                item: this.item_data[idx].item,
                units_of_measurement: this.item_data[idx].units_of_measurement,
                quantity: this.item_data[idx].quantity,
                unit_cost: this.item_data[idx].unit_cost,
                id: this.item_data[idx].id
            }
            this.selected_item = idx;
            this.calculateSubTotal();

            this.form_action = {
                label: "Edit Entry Item",
                action: "edit",
                id: idx
            }
        },
        resetForm() {
            this.form_action = {
                label: "Add Entry Item",
                action: "add",
                id: null
            }
            this.form_data = JSON.parse(JSON.stringify(this.form_defaults))
        },
        calculateSubTotal() {
            console.log(this.form_data.unit_cost, this.form_data.quantity);
            this.form_data.total_cost = this.form_data.unit_cost * this.form_data.quantity;
        },
        populateItemData() {
            if (this.selected_item) {
                this.form_data.unit_cost = this.item_data[this.selected_item].item.unit_cost;
            } else {
                let item = this.searchItem(this.form_data.item);
                this.form_data.unit_cost = parseFloat(item.unit_price);
            }


            // this.form_data.total_cost = this.item_data[this.selected_item].item.unit_cost * this.item_data[this.selected_item].quantity;
            this.calculateSubTotal();
        },
        getUnitsOfMeasure() {
            const url = base_url + "inventory_settings_unit_of_measurement/get_all_units";
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.units_of_measurement = response.data.data;
                    }
                });
        },
        getItemGroups() {
            const url = base_url + "generic_api/fetch_all?table=item_group&order=name ASC";
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.item_groups = response.data;
                    }
                });
        },
        getItemTypes() {
            var table = "item_type";
            var field = "group_id";
            var value = this.form_data.item_group;
            const url = base_url + "generic_api/fetch_specific?order=name ASC&table=" + table + "&field=" + field + "&value=" + value;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.item_types = response.data;
                    }
                });
        },
        getBrands() {
            const url = base_url + "item_brand/get_all_brands";
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.item_brands = response.data.data;
                    }
                });
        },
        getClasses() {
            const url = base_url + "item_class/get_all_classes";
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.item_classes = response.data.data;
                    }
                });
        },
        getItems() {
            // this.form_data.item = {name: "", value: ""};
            var params = {};
            if (this.form_data.item_brand) {
                params["brand_id"] = this.form_data.item_brand;
            }
            if (this.form_data.item_type) {
                params["type_id"] = this.form_data.item_type;
            }

            let url = base_url + "item/get_all_items";
            if (Object.keys(params).length > 0) {
                url = url + "?" + new URLSearchParams(params).toString();
            }

            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.items = response.data.data;
                    } else {
                        this.items = [];
                    }
                })
        },
        getRequestItems() {
            let url = base_url + "material_request_item/get_all_by_request/" + this.material_request_id;

            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.parseRequestItems(response.data);
                    }
                })
        },
        parseRequestItems(data) {
            for (let i in data) {
                let d = data[i];
                let parsed_data = {
                    item_class: d.item_class_id.id,
                    item_brand: d.item_brand_id.id,
                    item_group: d.item_group_id.id,
                    item_type: d.item_type_id.id,
                    item: d.item_id.id,
                    units_of_measurement: d.unit_of_measurement_id.id,
                    unit_cost: d.unit_cost,
                    total_cost: d.total_cost,
                    quantity: d.quantity,
                    id: d.id
                }
                this.item_data.push(this.parseForm(parsed_data))
            }

        },
        searchItemBrands(id) {
            for (let i = 0; i < this.item_brands.length; i++) {
                if (this.item_brands[i].id === id) {
                    return this.item_brands[i]
                }
            }
            return null;
        },
        searchItemClasses(id) {
            for (let i = 0; i < this.item_classes.length; i++) {
                if (this.item_classes[i].id === id) {
                    return this.item_classes[i]
                }
            }
            return null;
        },
        searchItemGroups(id) {
            for (let i = 0; i < this.item_groups.length; i++) {
                if (this.item_groups[i].id === id) {
                    return this.item_groups[i]
                }
            }
            return null;
        },
        searchItemTypes(id) {
            for (let i = 0; i < this.item_types.length; i++) {
                if (this.item_types[i].id === id) {
                    return this.item_types[i]
                }
            }
            return null;
        },
        searchUnits(id) {
            for (let i = 0; i < this.units_of_measurement.length; i++) {
                if (this.units_of_measurement[i].id === id) {
                    return this.units_of_measurement[i]
                }
            }
            return null;
        },
        searchItem(id) {
            for (let i = 0; i < this.items.length; i++) {
                if (this.items[i].id === id) {
                    return this.items[i]
                }
            }
            return null;
        },
        getFormData(col, key) {
            let row = this.item_data[key];
            switch (col) {
                case 'item_brand':
                    return this.searchItemBrands(row.item_brand);
                case 'item_class':
                    return this.searchItemClasses(row.item_class);
                case 'units_of_measurement':
                    return this.searchUnits(row.units_of_measurement);
                case 'item':
                    return this.searchItem(row.item);
                case 'item_group':
                    return this.searchItemGroups(row.item_group);
                case 'item_type':
                    return this.searchItemTypes(row.item_type);
            }
        },
        getItemDataKey(col, key, suffix) {
            return "item_data[" + key + "][" + col + suffix + "]";
        }
    },

    mounted() {
        this.material_request_id = $("#material_request_id").val();
        this.setInfoFormDefaults();
        this.resetForm();
        this.getBrands();
        this.getItemGroups();
        this.getClasses();
        this.getItems();
        this.getUnitsOfMeasure();

        if (this.material_request_id) {
            this.getRequestItems();
            this.form_is_valid = true;
        }
    }
});
