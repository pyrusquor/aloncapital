"use strict";
function getQueryParams (query = window.location.search) {
    return query.replace(/^\?/, '').split('&').reduce((json, item) => {
        if (item) {
            item = item.split('=').map((value) => decodeURIComponent(value))
            json[item[0]] = item[1]
        }
        return json
    }, {})
}
var MaterialRequest = function () {

    var confirmDelete = function () {
        $(document).on('click', '.remove_material_request', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var btn = $(this);

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {

                    $.ajax({
                        url: base_url + 'material_request/delete/' + id,
                        type: 'POST',
                        dataType: "JSON",
                        data: {id: id},
                        success: function (res) {
                            if (res.status) {
                                btn.closest('div.col-md-4').remove();
                                swal.fire(
                                    'Deleted!',
                                    res.message,
                                    'success'
                                );
                                location.reload();
                            } else {
                                swal.fire(
                                    'Oops!',
                                    res.message,
                                    'error'
                                )
                            }
                        }
                    });

                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            });
        });
    };

    var _add_ons = function () {

        $('#_export_select_all').on('click', function () {

            if (this.checked) {

                $('._export_column').each(function () {

                    this.checked = true;
                });
            } else {

                $('._export_column').each(function () {

                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {

            if (this.checked == false) {

                $('#_export_select_all').prop('checked', false);
            }
        });

    };

    var generalSearch = function () {

        // General Search
        function load_data(keyword) {
            var page_num = page_num ? page_num : 0;

            var keyword = keyword ? keyword : '';
            $.ajax({
                url: base_url + 'material_request/paginationData/' + page_num,
                method: "POST",
                data: 'page=' + page_num + '&keyword=' + keyword,
                success: function (html) {

                    KTApp.block('#kt_content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: 'Processing...',
                        css: {
                            padding: 0,
                            margin: 0,
                            width: '30%',
                            top: '40%',
                            left: '35%',
                            textAlign: 'center',
                            color: '#000',
                            border: '3px solid #aaa',
                            backgroundColor: '#fff',
                            cursor: 'wait'
                        },

                    });

                    setTimeout(function () {

                        $('#material_request_content').html(html);

                        KTApp.unblock('#kt_content');

                    }, 2000);

                }
            })
        }

        $('#generalSearch').blur(function () {

            var keyword = $(this).val();

            if (keyword !== '') {

                load_data(keyword);
            } else {

                load_data();
            }

        });
    };

    var advanceFilter = function () {

        function filter(values) {

            var page_num = page_num ? page_num : 0;
            var filter_data = values ? getQueryParams(values) : '';
            filter_data.page = page_num;

            $.ajax({
                url: base_url + 'material_request/paginationData/' + page_num,
                method: "POST",
                data: filter_data,
                success: function (html) {

                    $('#filterModal').modal('hide');

                    KTApp.block('#kt_content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: 'Processing...',
                        css: {
                            padding: 0,
                            margin: 0,
                            width: '30%',
                            top: '40%',
                            left: '35%',
                            textAlign: 'center',
                            color: '#000',
                            border: '3px solid #aaa',
                            backgroundColor: '#fff',
                            cursor: 'wait'
                        },

                    });

                    setTimeout(function () {

                        $('#material_request_content').html(html);

                        KTApp.unblock('#kt_content');

                    }, 500);


                }
            })
        }

        $('#advanceSearch').submit(function (e) {
            e.preventDefault();
            var form_values = $('#advanceSearch').serialize();
            console.log(form_values);

            filter(form_values);

        });
    };

    var _exportUploadCSV = function () {

        $('#_export_csv').validate({

            rules: {
                update_existing_data: {
                    required: true,
                }
            },
            messages: {
                update_existing_data: {
                    required: 'File type is required'
                }
            },
            invalidHandler: function (event, validator) {

                event.preventDefault();

                // var alert   =   $('#form_msg');
                // alert.closest('div.form-group').removeClass('kt-hide').show();
                KTUtil.scrollTop();

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                toastr.error("Please check your fields", "Something went wrong");
            },
            submitHandler: function (_frm) {

                _frm[0].submit();
            }
        });

        $('#_upload_form').validate({

            rules: {
                csv_file: {
                    required: true,
                }
            },
            messages: {
                csv_file: {
                    required: 'CSV file is required'
                }
            },
            invalidHandler: function (event, validator) {

                event.preventDefault();

                // var alert   =   $('#form_msg');
                // alert.closest('div.form-group').removeClass('kt-hide').show();
                KTUtil.scrollTop();

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                toastr.error("Please check your fields", "Something went wrong");
            },
            submitHandler: function (_frm) {

                _frm[0].submit();
            }
        });
    }

    var _initUploadGuideModal = function () {

        var _table = $('#_upload_guide_modal');

        _table.DataTable({
            order: [[0, 'asc']],
            pagingType: 'full_numbers',
            lengthMenu: [3, 5, 10, 25, 50, 100],
            pageLength: 5,
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: false,
            deferRender: true,
            dom:
            // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
            // "<'row'<'col-sm-12 col-md-6'l>>" +
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            language: {
                'lengthMenu': 'Show _MENU_',
                'infoFiltered': '(filtered from _MAX_ total records)'
            },
            columnDefs: [
                {
                    targets: [0, 2, 3, 5],
                    className: 'dt-center',
                },
                {
                    targets: [0, 2, 3, 4, 5],
                    orderable: false,
                },
                {
                    targets: [4, 5],
                    searchable: false
                },
                {
                    targets: 0,
                    searchable: false,
                    visible: false
                }
            ]
        });
    };

    var bulkDelete = function () {

        $(document.body).on('change', '.delete_check', function () {

            if ($('.delete_check:checked').length) {

                $('#bulkDelete').removeAttr('disabled');

            } else {
                $('#bulkDelete').attr('disabled', 'disabled');
            }

        });

        $('#bulkDelete').click(function () {

            var deleteids_arr = [];

            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {

                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {

                        $.ajax({
                            url: base_url + 'material_request/bulkDelete',
                            type: 'POST',
                            dataType: "JSON",
                            data: {deleteids_arr: deleteids_arr},
                            success: function (res) {
                                if (res.status) {

                                    Swal.fire({
                                        title: "Deleted!",
                                        text: res.message,
                                        type: "success"
                                    }).then((result) => {
                                        // Reload the Page
                                        location.reload();
                                    });

                                } else {
                                    swal.fire(
                                        'Oops!',
                                        res.message,
                                        'error'
                                    )
                                }
                            }
                        });

                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                });
            }
        });
    }

    var cancelRequest = function () {
        $(document).on('click', '.cancel_request', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var btn = $(this);

            swal.fire({
                title: 'Are you sure?',
                text: "This will cancel the request",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, cancel this request',
                cancelButtonText: 'No',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {

                    $.ajax({
                        url: base_url + 'material_request/cancel/' + id,
                        type: 'POST',
                        dataType: "JSON",
                        data: {id: id},
                        success: function (res) {
                            if (res.status) {
                                btn.closest('div.col-md-4').remove();
                                swal.fire(
                                    'Cancelled!',
                                    res.message,
                                    'success'
                                );
                            } else {
                                swal.fire(
                                    'Oops!',
                                    res.message,
                                    'error'
                                )
                            }
                        }
                    });

                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            });
        });
    }

    var processStatusAction = function () {
        $(document).on('click', '.process-action', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var action = $(this).data('action');
            var table = $(this).data('table');
            var field = $(this).data('field');
            var value = $(this).data('value');
            var actor = $(this).data('actor');

            var action_label = '';
            var data = {
                table: table,
                id: id,
                field: field,
                value: value,
                actor: actor
            }

            switch (action) {
                case "0":
                    action_label = 'Change status to New Request';
                    break;
                case "1":
                    action_label = 'Change status to for RPO';
                    break;
                case "2":
                    action_label = 'Change status to For Issuance';
                    break;
                case "3":
                    action_label = 'Change status to Issued';
                    break;
                case "4":
                    action_label = 'Change status to Cancelled';
                    break;
            }

            swal.fire({
                title: action_label,
                text: "This will change the status of this request",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes, change the status',
                cancelButtonText: 'No',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "generic_api/authorized_update",
                        type: "POST",
                        dataType: "JSON",
                        data: data,
                        success: function (res) {
                            if (res.status) {
                                swal.fire(
                                    {
                                        title: 'Status changed!',
                                        text: res.message,
                                        type: 'success'
                                    }
                                ).then((result) => {
                                    location.reload();
                                });
                            } else {
                                swal.fire(
                                    'Oops!',
                                    res.message,
                                    'error'
                                )
                            }
                        }
                    })
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })
        })
    }

    var processStatusActionWithSupervision = function () {
        $(document).on('click', '.process-action-with-supervision', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var action = $(this).data('action');
            var table = $(this).data('table');
            var field = $(this).data('field');
            var value = $(this).data('value');
            var actor = $(this).data('actor');
            var validator = $(this).data('validator');
            var modal_confirm = $("#password_confirm_modal");
            var data = {
                table: table,
                id: id,
                field: field,
                value: value,
                actor: actor
            }

            swal.fire({
                title: "Cancel own Request",
                text: "This will change the status of this request",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes, change the status',
                cancelButtonText: 'No',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    modal_confirm.modal('show');
                    filter_app.confirm_form.user_id = validator;
                    status_data = data;
                }
            })
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            confirmDelete();
            _add_ons();
            generalSearch();
            advanceFilter();
            _exportUploadCSV();
            _initUploadGuideModal();
            bulkDelete();
            cancelRequest();
            processStatusAction();
            processStatusActionWithSupervision();
        },
    };

}();

jQuery(document).ready(function () {
    MaterialRequest.init();
});

function MaterialRequestPagination(page_num) {

    page_num = page_num ? page_num : 0;

    var keyword = $('#generalSearch').val();

    var name = name ? name : '';


    $.ajax({
        url: base_url + 'material_request/paginationData/' + page_num,
        method: "POST",
        data: 'page=' + page_num + '&name=' + name,
        success: function (html) {

            KTApp.block('#kt_content', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Processing...',
                css: {
                    padding: 0,
                    margin: 0,
                    width: '30%',
                    top: '40%',
                    left: '35%',
                    textAlign: 'center',
                    color: '#000',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait'
                },

            });

            setTimeout(function () {

                $('#material_request_content').html(html);

                KTApp.unblock('#kt_content');

            }, 500);
        }
    })
}

var status_data = {};

var filter_app = new Vue({
    el: '#material_request_content',
    data: {
        confirm_form: {
            identity: "",
            password: "",
            user_id: null
        },
    },
    watch: {},
    methods: {
        confirmIdentity() {
            var url = base_url + "generic_api/check_credentials";
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: this.confirm_form,
                success: function (res) {
                    if (res.status) {
                        var d = status_data;
                        $.ajax({
                            url: base_url + "generic_api/authorized_update",
                            type: "POST",
                            dataType: "JSON",
                            data: d,
                            success: function (r) {
                                if (r.status) {
                                    swal.fire(
                                        {
                                            title: 'Status changed!',
                                            text: r.message,
                                            type: 'success'
                                        }
                                    ).then((result) => {
                                        location.reload();
                                    });
                                } else {
                                    swal.fire(
                                        'Oops!',
                                        r.message,
                                        'error'
                                    )
                                }
                            }
                        })
                    } else {
                        swal.fire(
                            {
                                title: 'Ooops!',
                                text: res.message,
                                type: 'danger'
                            }
                        )
                    }
                }
            })
        }
    },
    mounted() {
    }
});
