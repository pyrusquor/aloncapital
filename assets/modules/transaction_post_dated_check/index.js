"use strict";
var Transaction_post_dated_check = (function () {
    let tableElement = $("#transaction_post_dated_check_table");

    var transaction_post_dated_checkTable = function () {
        var dataTable = tableElement.DataTable({
            order: [[1, "desc"]],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'transaction_post_dated_check/showItems',
            },
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                text: '<i class="la la-trash"></i> Delete Selected',
                className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm",
                attr: {
                    id: "bulkDelete",
                },
                enabled: false,
            }, ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },
            columns: [ {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'transaction',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'buyer_id',
                },
                {
                    data: "unique_number",
                },
                {
                    data: "bank",
                },
                {
                    data: "branch",
                },
                {
                    data: "amount",
                },
                {
                    data: "due_date",
                },
                {
                    data: "pdc_status",
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },

            ],
            columnDefs: [{
                    targets:  -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="` +
                            base_url +
                            `transaction_post_dated_check/form/` +
                            row.transaction_id +
                            `/` +
                            row.id +
                            `"><i class="la la-edit"></i> Update </a>
										<a href="javascript:void(0);" class="dropdown-item remove_transaction_post_dated_check" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete </a>
									</div>
								</span>
								<a href="` +
                            base_url +
                            `transaction_post_dated_check/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        );
                    },
                },
                {
                	targets: [2, 3, 4, 5, 7],
                	orderable: false,
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                {
                    targets: 11,
                    render: function (data, type, row, meta) {
                        var pdc_status = {
                            1: {
                                title: 'Pending',
                            },
                            2: {
                                title: 'Processing',
                            },
                            3: {
                                title: 'Cleared',
                            },
                            4: {
                                title: 'Bounced',
                            },
                        };
                        if (typeof pdc_status[data] === 'undefined') {
                            return ``;
                        }
                        return pdc_status[data].title;
                    },
                },

            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize() + '&' + $('#advance_search_date_range').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })

        $('#advance_search_date_range').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on(
            "click",
            ".remove_transaction_post_dated_check",
            function () {
                var id = $(this).data("id");

                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url +
                                "transaction_post_dated_check/delete/" +
                                id,
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                id: id
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#transaction_post_dated_check_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        );
    };

    var upload_guide = function () {

        var _table = $("#upload_guide_table");

        _table.DataTable({
            order: [
                [0, "asc"]
            ],
            pagingType: "full_numbers",
            lengthMenu: [3, 5, 10, 25, 50, 100],
            pageLength: 5,
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: false,
            deferRender: true,
            dom:
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l>>" +
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },
            columnDefs: [{
                    targets: [0, 2, 3, 5],
                    className: "dt-center",
                },
                {
                    targets: [0, 2, 3, 4, 5],
                    orderable: false,
                },
                {
                    targets: [4, 5],
                    searchable: false,
                },
                {
                    targets: 0,
                    searchable: false,
                    visible: false,
                },
            ],
        });

    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        $('._filter_date_range').on('keyup change clear', function () {

            processChangeDateRange()
        })

        const processChange = debounce(() => submitInput());
        const processChangeDateRange = debounce(() => submitInputDateRange());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }

        function submitInputDateRange(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $("#transaction_post_dated_check_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            // var rows = _table.rows({ 'search': 'applied' }).nodes();

            // $('input[type="checkbox"]').prop( 'checked', this.checked );
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#transaction_post_dated_check_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url +
                                "transaction_post_dated_check/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#transaction_post_dated_check_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    var _add_ons = function () {
        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
        $('.kt_datepicker').datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: 'yyyy-mm-dd',
            clearBtn: true
        });
    };

    var filterDateChange = function () {
        const filter_start = $('#filter_start_date');
        const filter_end = $('#filter_end_date');

        filter_start.on("change", function () {
            if (!filter_end.val()==""){
                if (new Date(filter_end.val())<=new Date(filter_start.val())){
                    filter_end.val(filter_start.val());
                    get_check_information(filter_start.val(),filter_start.val());
                } else{
                    get_check_information(filter_start.val(),filter_end.val());
                }
            }
        });

        filter_end.on("change", function () {   
            if (!filter_end.val()==""){
                if (new Date(filter_end.val())<=new Date(filter_start.val())){
                    filter_start.val(filter_end.val());
                    get_check_information(filter_start.val(),filter_start.val());
                } else{
                    get_check_information(filter_start.val(),filter_end.val());
                }
            }
        });

    };

    var get_check_information = function (d,e) {
        

        if (typeof e ==="undefined" && typeof d ==="undefined"){
            e='';
            d='';
        };
        
        $.ajax({
            url: base_url + "transaction_post_dated_check/get_pdc_summary",
            type: "POST",
            dataType: "JSON",
            data : {start:d,end:e},
            success: function (res) {
                $("#pending_count").html(res.pending_count);
                $("#pending_amount").html(res.pending_amount);
                $("#processing_count").html(res.processing_count);
                $("#processing_amount").html(res.processing_amount);
                $("#cleared_count").html(res.cleared_count);
                $("#cleared_amount").html(res.cleared_amount);
                $("#bounced_count").html(res.bounced_count);
                $("#bounced_amount").html(res.bounced_amount);
            },
        });

    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            transaction_post_dated_checkTable();
            filter();
            _selectProp();
            _add_ons();
            upload_guide();
            status();
            filterDateChange();
            get_check_information();
        },
    };

    var advanceFilter = function () {
        $("#advanceSearch").submit(function (e) {
            e.preventDefault();
            var form_values = $("#advanceSearch").serialize();

            filter(form_values);
        });

        function filter(values) {
            var page_num = page_num ? page_num : 0;
            var values = values ? values : "";

            $.ajax({
                url: base_url + "transaction/paginationData/" + page_num,
                method: "GET",
                data: "page=" + page_num + "&" + values,
                success: function (html) {
                    $("#filterModal").modal("hide");
                    KTApp.block("#kt_content", {
                        overlayColor: "#000000",
                        type: "v2",
                        state: "primary",
                        message: "Processing...",
                        css: {
                            padding: 0,
                            margin: 0,
                            width: "30%",
                            top: "40%",
                            left: "35%",
                            textAlign: "center",
                            color: "#000",
                            border: "3px solid #aaa",
                            backgroundColor: "#fff",
                            cursor: "wait",
                        },
                    });

                    setTimeout(function () {
                        $("#transactionContent").html(html);

                        KTApp.unblock("#kt_content");
                    }, 500);
                },
            });
        }
    };
})();

jQuery(document).ready(function () {
    Transaction_post_dated_check.init();
});

$(document).on('click', '#_export_form_btn', function () {

    var exportColumns = $('#_export_form').serialize();
    var advanceSearch = $('#advance_search').serialize();
    var today = new Date();
    var dd = String(today.getDate()).padStart(2,'0');
    var mm = String(today.getMonth() + 1).padStart(2,'0');
    var yyyy = today.getFullYear();
    var today = mm + '/' + dd + '/' + yyyy;
    var dt = $('#transaction_post_dated_check_table').DataTable();
    var raw_order = dt.order();
    var order = [dt.column(raw_order[0][0]).dataSrc(),raw_order[0][1]];
    KTApp.block('#_export_option', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Creating Excel File...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'transaction_post_dated_check/export',
        xhrFields:{
            responseType: 'blob'
        },
        type : "POST",
        data : exportColumns+'&'+advanceSearch + '&order=' + order ,
        success : function(result) {
            KTApp.unblock('#_export_option');
            var blob = result;
            var downloadUrl = URL.createObjectURL(blob);
            var a = document.createElement("a");
            a.href = downloadUrl;
            a.download = "List of Sellers (" + today + ").xls" ;
            document.body.appendChild(a);
            a.click();
        },
    })
});