"use strict";
var Transaction_post_dated_checkView = function() {

	var initTransaction_post_dated_checkProjects = function() {
		var table = $('#transaction_post_dated_check_projects');

		// begin first table
		table.DataTable({
			scrollY: '50vh',
			scrollX: true,
            scrollCollapse: true,
            bPaginate: false,
            bLengthChange: false,
            bFilter: true,
            bInfo: false,
            bAutoWidth: false,
            dom:	"<'row'<'col-sm-12 col-md-12'l><'col-sm-12 col-md-12'f>>",
			columnDefs: [
				{
					targets: -1,
					title: 'Actions',
					orderable: false
				},
			],
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTransaction_post_dated_checkProjects();
		},

	};

}();

jQuery(document).ready(function() {
	Transaction_post_dated_checkView.init();
});