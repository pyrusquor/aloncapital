'use strict';
var SalesRecognize = (function () {
    let tableElement = $('#sales_recognize_table');

    var SalesRecognizeTable = function () {

        var dataTable = tableElement.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'sales_recognize/showItems',
            },
            dom:
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-money"></i> Recognize Sale Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'recognizeSale',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'buyer_id',
                },
                {
                    data: 'total_collectible_price',
                },
                {
                    data: 'total_payments_made',
                },
                {
                    data: 'date_of_reaching_twenty_five_percent',
                },
                {
                    data: 'is_recognized',
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        if (row.is_recognized !== '1') {
                            return (
                                `
                                    <span class="dropdown">
                                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                                        <i class="la la-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="javascript:void(0);" class="dropdown-item recognize_sale" data-id="` +
                                row.id +
                                `"><i class="la la-money"></i> Recognize Sale</a>
                                        </div>
                                    </span>
                                    <a href="` +
                                base_url +
                                `sales_recognize/view/` +
                                row.id +
                                `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                    <i class="la la-edit"></i>
                                    </a>`
                            );
                        } else {
                            return (
                                `
                                    <span class="dropdown">
                                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                                        <i class="la la-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="javascript:void(0);" class="dropdown-item derecognize_sale" data-id="` +
                                row.id +
                                `"><i class="la la-money"></i> Revert recognition</a>
                                        </div>
                                    </span>
                                    <a href="` +
                                base_url +
                                `sales_recognize/view/` +
                                row.id +
                                `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                    <i class="la la-edit"></i>
                                    </a>`
                            );
                        }
                    },
                },
                {
                    targets: [0, 1, 3, -1],
                    className: 'dt-center',
                    // orderable: false,
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                {
                    targets: 9,
                    render: function (data, type, full, meta) {
                        var status = {
                            1: {
                                name: 'Recognized',
                            },
                        };

                        if (typeof status[data] === 'undefined') {
                            return 'For Recognition';
                        }
                        return status[data].name;
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_sales_recognize', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'sales_recognize/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#sales_recognize_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var confirmRecognize = function () {
        $(document).on('click', '.recognize_sale', function () {
            var id = $(this).data('id');
            $('#sr_transaction_id').val(id);

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, I want to recognize this transaction!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    
                    $('#receipt_type').val('0'),
                    $('#sr_or_number').val(''),
                    $('#sr_cost_of_sales').val(0),
                    $('#sr_development_cost').val(''),
                    $('#sr_rawland').val(''),
                    $('#sr_titling').val(''),
                    $('#is_vatable').val(1),


                    $('#salesRecognitionModal').modal('toggle');
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };
    var confirmDerecognize = function () {
        $(document).on('click', '.derecognize_sale', function () {
            var id = $(this).data('id');
            $('#sr_transaction_id').val(id);

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, I want to revert recognition of this transaction!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'sales_recognize/derecognize_sale',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            transaction_id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#sales_recognize_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire(
                                    'Reverted!',
                                    res.message,
                                    'success'
                                );
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Revert cancelled!',
                        'error'
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on('click', '#btn_upload_guide', function () {
            var table = $('#upload_guide_table');

            table.DataTable({
                order: [[0, 'asc']],
                pagingType: 'full_numbers',
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + 'sales_recognize/get_table_schema',
                columns: [
                    // {data: 'button'},
                    {
                        data: 'no',
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'type',
                    },
                    {
                        data: 'format',
                    },
                    {
                        data: 'option',
                    },
                    {
                        data: 'required',
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $('#sales_recognize_table').DataTable();

        $('#_search').on('keyup', function () {
            console.log('yow');
            dTable.search($(this).val()).draw();
        });

        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $(document).on('change', '#receipt_type', function () {
            receiptType();
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $('#sales_recognize_table').DataTable();
        var _buttons = _table.buttons([
            '.bulkDelete',
            '.bulkBounce',
            'bulkClear',
        ]);

        $('#select-all').on('click', function () {
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
                _buttons.buttons().enable();
            } else {
                $('.delete_check').prop('checked', false);
                _buttons.buttons().disable();
            }
        });

        $('#sales_recognize_table tbody').on(
            'change',
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $('input#select-all').get(0);

                    if (el && el.checked && 'indeterminate' in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var ids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                ids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (ids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'sales_recognize/bulkActions',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                ids_arr: ids_arr,
                                type: 'delete',
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#sales_recognize_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });

        $('#recognizeSale').click(function () {
            var ids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                ids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (ids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, recognize selected rows!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'sales_recognize/bulkActions',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                ids_arr: ids_arr,
                                type: 'recognize',
                            },
                            success: function (res) {
                                if (res.status) {

                                    get_sales_recognize_summary();

                                    $('#sales_recognize_table')
                                        .DataTable()
                                        .ajax.reload();

                                    swal.fire(
                                        'Recognized!',
                                        res.message,
                                        'success'
                                    );
                                } else {

                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        // swal.fire(
                        //     'Cancelled',
                        //     'Your imaginary file is safe :)',
                        //     'error'
                        // );
                    }
                });
            }
        });

        $('#recognize_button').click(function () {
            $.ajax({
                url: base_url + 'sales_recognize/recognize_sale',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    receipt_type: $('#receipt_type').val(),
                    sr_or_number: $('#sr_or_number').val(),
                    sr_cost_of_sales: $('#sr_cost_of_sales').val(),
                    sr_development_cost: $('#sr_development_cost').val(),
                    sr_transaction_id: $('#sr_transaction_id').val(),
                    sr_rawland: $('#sr_rawland').val(),
                    sr_titling: $('#sr_titling').val(),
                    is_vatable: $('#is_vatable').val(),
                },
                success: function (res) {
                    if (res.status == 1) {

                        $('#salesRecognitionModal').modal('toggle');

                        $('#sales_recognize_table')
                            .DataTable()
                            .ajax.reload();

                        swal.fire(
                            'Recognized!',
                            res.message,
                            'success'
                        );
                    } else {

                        swal.fire('Oops!', res.message, 'error');
                    }
                },
            });
        })
    };

    var get_sales_recognize_summary = function () {
        
        $.ajax({
            url: base_url + "sales_recognize/get_sales_recognize_summary",
            type: "POST",
            dataType: "JSON",
            data : {e:'please'},
            success: function (res) {
                $("#recognized_count").html(res.recognized_count);
                $("#recognized_amount").html(res.recognized_amount);
                $("#for_recognition_count").html(res.for_recognition_count);
                $("#for_recognition_amount").html(res.for_recognition_amount);
            },
        });

    };

    var sum_cost_of_sales = () => {
        $('#sr_development_cost, #sr_rawland, #sr_titling').on('change, keyup', () => {
            let developmentCost = 0 + $('#sr_development_cost').val()
            let rawland = 0 + $('#sr_rawland').val()
            let titling = 0 + $('#sr_titling').val()

            $('#sr_cost_of_sales').val(parseFloat(developmentCost) + parseFloat(rawland) + parseFloat(titling))
        })
    }
    return {
        //main function to initiate the module
        init: function () {
            confirmRecognize();
            confirmDerecognize();
            confirmDelete();
            SalesRecognizeTable();
            _add_ons();
            filter();
            upload_guide();
            status();
            _selectProp();
            get_sales_recognize_summary();
            sum_cost_of_sales();
        },
    };
})();

jQuery(document).ready(function () {
    SalesRecognize.init();
});

var receiptType = function () {
    // <option value="0">Select Type</option>
    // <option value="1">Collection Receipt</option>
    // <option value="2">Acknowledgement Receipt</option>
    // <option value="3">Provisionary Receipt</option>
    // <option value="4">Official Receipt</option>

    var receipt_type = parseInt($('#receipt_type').val());

    var ornumber = ' ';

    if (receipt_type == 1) {
        ornumber = 'CR-';
    } else if (receipt_type == 2) {
        ornumber = 'AR-';
    } else if (receipt_type == 3) {
        ornumber = 'PR-';
    } else if (receipt_type == 4) {
        ornumber = 'OR-';
    } else if (receipt_type == 6) {
        ornumber = 'SI-';
    } else if (receipt_type == 5) {
        ornumber = ' ';
    }

    $('#sr_or_number').val(ornumber);
};
