var dynamicColors = function() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
};
function handleHover(evt, item, legend) {
    legend.chart.data.datasets[0].backgroundColor.forEach((color, index, colors) => {
      colors[index] = index === item.index || color.length === 9 ? color : color + '4D';
    });
    legend.chart.update();
  };
  function handleLeave(evt, item, legend) {
    legend.chart.data.datasets[0].backgroundColor.forEach((color, index, colors) => {
      colors[index] = color.length === 9 ? color.slice(0, -2) : color;
    });
    legend.chart.update();
  };
var DashboardWidgets = (function () {

    var general_notices_info_box = function () {

            var options = {
                title: {
                    display: true,
                },
                plugins: {
                    legend: false,
                    title: false,
                    tooltip: {
                        mode: 'nearest',
                        intersect: false,
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10,
                        callbacks: {
                            label: function(tooltipItem) {
                                return tooltipItem.parsed.y + ' item(s)';
                            }
                        }
                    },
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        display: false,
                    },
                    x: {
                        display: false,
                    }
                },
                elements: {
                    line: {
                        tension: 0.0000001
                    },
                    point: {
                        radius: 4,
                        borderWidth: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 10,
                        bottom: -10
                    }
                }
            
        }

        KTApp.block('#recent_communication_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });

        var gsc_canvas = document.getElementById('general_stats_chart').getContext("2d");
        var general_stats_chart = new Chart(gsc_canvas, {type: 'line',options});

        var load_info_and_chart = function (date){
            $.ajax({
                url: base_url + "dashboard/get_count_for_week",
                type: "POST",
                dataType: "JSON",
                data : {
                    date:date,
                },
                success: function (res) {
                    KTApp.unblock('#recent_communication_portlet');
                    $("#general_emails").html(res.total_email);
                    $("#general_sms").html(res.total_sms);
                    $("#general_letter_requests").html(res.total_letter_request);
                    $("#general_past_due_notices").html(res.total_past_due_notices);
    
                    var gradient = gsc_canvas.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#FF9DB6').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#FF9DB6').alpha(0.3).rgbString());
    
    
    
                    var newDataSet = {
                        backgroundColor: '#E14C86',
                        fill:true,
                        borderColor: '#E13A58',
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.totals
                    }
                    general_stats_chart.data.datasets.push(newDataSet);
                    general_stats_chart.data.labels=res.labels;
                    general_stats_chart.update();
    
    
                },
            });
        }

        var recent_activities = function(){
            KTApp.block('#recent_activities_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + "dashboard/get_recent_activities",
                type: "POST",
                dataType: "JSON",
                data : {
                    date:' ',
                },
                success: function (res) {
                    KTApp.unblock('#recent_activities_portlet');
                    $("#recent_activity_timeline").html(res);
                },
            });
        }
        var recent_tickets = function(){

            KTApp.block('#tickets_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });

            $.ajax({
                url: base_url + "dashboard/get_recent_tickets",
                type: "POST",
                dataType: "JSON",
                data : {
                    date:' ',
                },
                success: function (res) {
                    KTApp.unblock('#tickets_portlet');
                    $("#recent_tickets").html(res);
                },
            });
        }
        var top_three_sellers = function(){
            KTApp.block('#best_sellers_general_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/dashboard_seller_top_sellers',
                dataType: 'json',
                type: "POST",
                data : {
                    time_frame:'year',
                    limit: 1
                },
                success: function (response) {
                    KTApp.unblock('#best_sellers_general_portlet');
                    $('#top_three_sellers_table').html(response.html);
                },
            });
        }
        var get_latest_buyers = function(){
            KTApp.block('#three_latest_buyers_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/get_latest_buyers',
                dataType: 'json',
                type: "POST",
                data : {
                    time_frame:'year',
                    limit: 1
                },
                success: function (response) {
                    KTApp.unblock('#three_latest_buyers_portlet');
                    $('#three_latest_buyers').html(response);
                },
            });
        }

        load_info_and_chart('now');
        recent_activities();
        recent_tickets();
        top_three_sellers();
        get_latest_buyers();
    }

    var chart_builder_bar_graph = function() {

            var options = {
                    plugins: {
                        legend: false,
                        title: false,
                        tooltip: {
                            mode: 'nearest',
                            intersect: false,
                            position: 'nearest',
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10,
                            callbacks: {
                                label: function(context) {
                                    var label = context.dataset.label || '';
            
                                    if (label) {
                                        label += ': ';
                                    }
                                    if (context.parsed.y !== null) {
                                        label += context.parsed.y;
                                    }
                                    label += " unit(s)";
                                    return label;
                                }
                            }
                        },
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        y: {
                            display: true,
                            suggestedMax: 5
                        },
                        x: {
                            display: true,
                            
                        }
                    },
                    elements: {
                        line: {
                            tension: 0.0000001
                        },
                        point: {
                            radius: 4,
                            borderWidth: 12
                        }
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 10,
                            bottom: 0
                        }
                    }
            };
        
        var psc = document.getElementById('projects_bar_chart').getContext("2d");
        var properties_chart = new Chart(psc, {type: 'bar',options});

        KTApp.block('#projects_bar_chart_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });

        var load_projects = function (down_id,time_frame,start_date,end_date){
            $.ajax({
                url: base_url + 'dashboard/per_project_summary',
                dataType: 'json',
                type: "post",
                data : {
                    data_sets:down_id,
                    time_frame: time_frame,
                    start_date: start_date,
                    end_date: end_date
                },
                success: function (res) {
                    KTApp.unblock('#projects_bar_chart_portlet');

                    $('#project_analysis_range').html(res.date_range);
                    var label_colors = [];
                    for (i in res.labels){
                        label_colors.push(dynamicColors());
                    }; 
                    if (down_id==0){
                        var newDataSet = {
                            backgroundColor: label_colors,
                            fill:true,
                            borderColor: '#FFFFFF',
                            pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                            pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                            pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                            pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                            data: res.values
                        }
                        properties_chart.options.plugins.legend = false;
                        properties_chart.data.datasets.push(newDataSet);
                        properties_chart.data.labels=res.labels;
                        properties_chart.update();
                        
                    } else {
                        result_values = res.values;
                        $.each(result_values, function(i) {
                                var newDataSet = {
                                    label: i,
                                    backgroundColor: dynamicColors(),
                                    fill:true,
                                    borderColor: '#FFFFFF',
                                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                                    pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                                    data: result_values[i],
                                }
                                properties_chart.data.datasets.push(newDataSet);
                    });
                    properties_chart.options.plugins.legend = {
                            labels:{
                                font: {
                                    size:15
                                },
                                padding: 30
                            }
                        }
                        properties_chart.data.labels=res.labels;
                        properties_chart.update();
                    }

                },
            })
        }
        var id_selected = $("#project_id_selected_hidden");
        load_projects(id_selected.val(), ' -1 year');
        var start_month = $('#properties_start_month');
        var end_month = $('#properties_end_month');

        $("#project_selector").on('click', '.dropdown-item', function (){
            $('#selected_text1').html($(this).data('project'));
            $('#selected_text2').html($(this).data('project'));
            $('#properties_selected_project_id').val($(this).data('id'));
            $('#properties_selected_project_name').val($(this).data('project'));
            id_selected.val($(this).data('id'))
            var time_frame = 'specific'
            var down_id = $(this).data('id');
            if(end_month.val()=="" || start_month.val()==""){
                time_frame = ' -1 year'
            }
            properties_chart.data.datasets = [];
            properties_chart.data.labels = [];
            properties_chart.update();
            load_projects(down_id,time_frame, start_month.val(),end_month.val());
        
        })

        $('#properties_start_month, #properties_end_month').on("change", function () {
            var project_id = id_selected.val();
            if(this.id=='properties_start_month'){
                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        end_month.val(start_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            } else if(this.id=='properties_end_month'){

                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        start_month.val(end_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            }
            if(start_month.val()=='' || end_month.val()==''){
                time_frame='year';
            }
            properties_chart.data.datasets = [];
            properties_chart.data.labels = [];
            properties_chart.update();
            load_projects(project_id,time_frame,start_month.val(),end_month.val());
            if(time_frame=='specific'){
                var start_date = moment(start_month.val()).startOf('month').format('YYYY/MM/DD');
                var end_date = moment(end_month.val()).endOf('month').format('YYYY/MM/DD');
                $('#properties_selected_date_range').val(start_date + '-' + end_date);
            }

        })

        $("#properties_reset_date").on("click", function (){
            var project_id = id_selected.val();
            start_month.val('');
            end_month.val('');
            properties_chart.data.datasets = [];
            properties_chart.data.labels = [];
            properties_chart.update();
            $('#properties_selected_date_range').val('');
            load_projects(project_id,' -1 year');
        })
    };

// <-----------------------------Sales Dashboard

    var sales_money_charts = function (e) {

        var options = {
                title: {
                    display: false,
                },
                plugins: {
                    legend: false,
                    title: false,
                    tooltip: {
                        mode: 'nearest',
                        intersect: false,
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10,
                        callbacks: {
                            label: function(tooltipItem) {
                                var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(tooltipItem.parsed.y);
                                
                                return number.replace("$", "₱ ");
                            }
                        }
                    },
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        display: false,
                    },
                    x: {
                        display: false,
                    }
                },
                elements: {
                    line: {
                        tension: 0.0000001
                    },
                    point: {
                        radius: 4,
                        borderWidth: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 10,
                        bottom: 0
                    }
                }
            
        }
        

        var tsmc_canvas = document.getElementById('total_sales_month_chart').getContext("2d");
        var total_sales_month_chart = new Chart(tsmc_canvas, {type: 'line',options});

        var tcmc_canvas = document.getElementById('total_collections_month_chart').getContext("2d");
        var total_collections_month_chart = new Chart(tcmc_canvas, {type: 'line',options});

        var ysc_canvas = document.getElementById('yearly_sales_chart').getContext("2d");
        var yearly_sales_chart = new Chart(ysc_canvas, {type: 'line',options});

        KTApp.block('#sales_month_total_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });
        
        var month_sales = function (e) {

            $.ajax({
                url: base_url + "dashboard/month_sales",
                type: "POST",
                dataType: "JSON",
                data : {
                    date_range:'-5 month',
                    select: 'sum',
                    project_id: e
                },
                success: function (res) {
                    KTApp.unblock('#sales_month_total_portlet');
                    $("#sales_month_total").html(res.amount);

                    var gradient = tsmc_canvas.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#BCEAE1').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#BCEAE1').alpha(0.3).rgbString());


    
                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('success'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
                    total_sales_month_chart.data.datasets.push(newDataSet);
                    total_sales_month_chart.data.labels=res.labels;
                    total_sales_month_chart.update();
                },
            });

        };

        KTApp.block('#total_collections_month_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });

        var month_collections = function (e){
            $.ajax({
                url: base_url + "dashboard/month_collections",
                type: "POST",
                dataType: "JSON",
                data : {
                    date_range:'-5 month',
                    project_id: e
                },
                success: function (res) {
                    KTApp.unblock('#total_collections_month_portlet');
                    $('#collections_month').html(res.month);
                    $("#total_collections_month").html(res.amount);
    
                    var gradient = tcmc_canvas.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#A1C5EE').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#A1C5EE').alpha(0.3).rgbString());


    
                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('brand'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
                    total_collections_month_chart.data.datasets.push(newDataSet);
                    total_collections_month_chart.data.labels=res.labels;
                    total_collections_month_chart.update();
                    
                },
            });
        }

        KTApp.block('#yearly_sales_chart_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });
        var year_chart = function (e){

            $.ajax({
                url: base_url + "dashboard/month_sales",
                type: "POST",
                dataType: "JSON",
                data : {
                    date_range:'-1 year',
                    select: 'sum',
                    project_id: e
                },
                success: function (res) {
                    KTApp.unblock('#yearly_sales_chart_portlet');

                    $("#total_yearly_sales_amount").html(res.total);
                    $("#yearly_date_range").html(res.date_range);
                    // chart_builder_sales_yearly('yearly_sales_chart',res.labels,res.values,'#BCEAE1');

                    var gradient = ysc_canvas.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#BCEAE1').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#BCEAE1').alpha(0.3).rgbString());



                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: '#FFFFFF',
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
                    yearly_sales_chart.data.datasets.push(newDataSet);
                    yearly_sales_chart.data.labels=res.labels;
                    yearly_sales_chart.update();


                },
            });

        };
        
        year_chart(e);
        month_collections(e);
        month_sales(e);
        // properties_chart(e);
        $("#sales_project_id").on("change", function (){
            project_id = this.value
            total_sales_month_chart.data.datasets = [];
            total_sales_month_chart.data.labels = [];
            total_sales_month_chart.update();
            total_collections_month_chart.data.datasets = [];
            total_collections_month_chart.data.labels = [];
            total_collections_month_chart.update();
            yearly_sales_chart.data.datasets = [];
            yearly_sales_chart.data.labels = [];
            yearly_sales_chart.update();
            year_chart(project_id);
            month_collections(project_id);
            month_sales(project_id);

        })

    };

    var sales_text_charts = function (e) {

        var options = {
            title: {
                display: false,
            },
            plugins: {
                legend: false,
                title: false,
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    display: false,
                },
                x: {
                    display: false,
                }
            },
            elements: {
                line: {
                    tension: 0.0000001
                },
                point: {
                    radius: 4,
                    borderWidth: 12
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 10,
                    bottom: 0
                }
            }
        
        };

        var pmc_canvas = document.getElementById('properties_month_chart').getContext("2d");
        var properties_month_chart = new Chart(pmc_canvas, {type: 'line',options});

        var cmc_canvas = document.getElementById('clients_month_chart').getContext("2d");
        var clients_month_chart = new Chart(cmc_canvas, {type: 'line',options});

        KTApp.block('#property_sales_month_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });

        var properties_chart = function (e){
            $.ajax({
                url: base_url + "dashboard/month_property_count",
                type: "POST",
                dataType: "JSON",
                data : {
                    date_range:'-5 month',
                    select: 'count',
                    project_id: e
                },
                success: function (res) {
                    KTApp.unblock('#property_sales_month_portlet');

                    $("#total_properties_month").html(res.amount);

                    var gradient = pmc_canvas.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#FF9DB6').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#FF9DB6').alpha(0.3).rgbString());

                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('danger'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
                    properties_month_chart.data.datasets.push(newDataSet);
                    properties_month_chart.data.labels=res.labels;
                    properties_month_chart.options.plugins.tooltip ={
                        mode: 'nearest',
                        intersect: false,
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10,
                        callbacks: {
                            label: function(tooltipItem) {                            
                                return tooltipItem.parsed.y + " property(s)";
                            }
                        }
                    };
                    properties_month_chart.update();


                },
            });
        };

        KTApp.block('#clients_month_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });

        $.ajax({
            url: base_url + "dashboard/month_buyer_count",
            type: "POST",
            dataType: "JSON",
            data : {
                date_range:'-5 month'
            },
            success: function (res) {
                KTApp.unblock('#clients_month_portlet');

                $("#total_clients_month").html(res.amount);
                var gradient = pmc_canvas.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#ffefce').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#ffefce').alpha(0.3).rgbString());

                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('warning'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
                    clients_month_chart.data.datasets.push(newDataSet);
                    clients_month_chart.data.labels=res.labels;
                    clients_month_chart.options.plugins.tooltip ={
                        mode: 'nearest',
                        intersect: false,
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10,
                        callbacks: {
                            label: function(tooltipItem) {                            
                                return tooltipItem.parsed.y + " client(s)";
                            }
                        }
                    };
                    clients_month_chart.update();


            },
        });



        properties_chart(e);

        $("#sales_project_id").on("change", function (){
            project_id = this.value
            properties_month_chart.data.datasets = [];
            properties_month_chart.data.labels = [];
            properties_month_chart.update();
            properties_chart(project_id);

        })


    };
// ------------------------------>End of sales dashboard

// <-----------------------------Financials Dashboard


    var financials_cards = function (e) {

        var for_collection_today = function (e){
            KTApp.block('#collect_today_amount_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + "dashboard/for_collection",
                type: "POST",
                dataType: "JSON",
                data : {
                    type:'today',
                    project_id: e
                },
                success: function (res) {
                    KTApp.unblock('#collect_today_amount_portlet');

                    $("#collect_today_amount").html(res.amount);
                    $("#collect_today_date").html(res.date);
                    $("#collect_today_count").html(res.count);
                },
            });
        }

        var for_collection_past_due = function (e) {
            KTApp.block('#collect_past_due_amount_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + "dashboard/for_collection",
                type: "POST",
                dataType: "JSON",
                data : {
                    type:'past_due',
                    project_id: e
                },
                success: function (res) {
                    KTApp.unblock('#collect_past_due_amount_portlet');

                    $("#collect_past_due_amount").html(res.amount);
                    $("#collect_past_due_date").html(res.date);
                    $("#collect_past_due_count").html(res.count);
                },
            });
        }


        var for_pdc_today = function (e) {
            KTApp.block('#pdc_today_amount_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + "dashboard/for_pdc",
                type: "POST",
                dataType: "JSON",
                data : {
                    type:'today',
                    project_id: e
                },
                success: function (res) {
                    KTApp.unblock('#pdc_today_amount_portlet');

                    $("#pdc_today_amount").html(res.amount);
                    $("#pdc_today_date").html(res.date);
                    $("#pdc_today_count").html(res.count);
                },
            });
        }      


        var for_pdc_past_due = function (e){
            KTApp.block('#pdc_past_due_amount_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + "dashboard/for_pdc",
                type: "POST",
                dataType: "JSON",
                data : {
                    type:'past_due',
                    project_id: e
                },
                success: function (res) {
                    KTApp.unblock('#pdc_past_due_amount_portlet');

                    $("#pdc_past_due_amount").html(res.amount);
                    $("#pdc_past_due_count").html(res.count);
                },
            });
         }

        var for_collection_today_side = function (e){
            KTApp.block('#dashboard_for_collection_today_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/for_today_collections_list',
                dataType: 'json',
                type: "POST",
                data : {
                    type:'today',
                    project_id: e
                },
                success: function (response) {
                    KTApp.unblock('#dashboard_for_collection_today_portlet');

                    $('#dashboard_for_collection_today').html(response.html);
                },
            });
        }


        var for_pdc_today_side = function (e) {
            KTApp.block('#for_pdc_clearing_today_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/for_today_pdc_list',
                dataType: 'json',
                type: "POST",
                data : {
                    type:'today',
                    project_id: e
                },
                success: function (response) {
                    KTApp.unblock('#for_pdc_clearing_today_portlet');

                    $('#for_pdc_clearing_today').html(response.html);
                },
            });
        };
        for_collection_today(e);
        for_collection_past_due(e);
        for_pdc_today(e);
        for_pdc_past_due(e);
        for_collection_today_side(e);
        for_pdc_today_side(e);

        $("#financials_project_id").on("change", function (){
            project_id = this.value
            for_collection_today(project_id);
            for_collection_past_due(project_id);
            for_pdc_today(project_id);
            for_pdc_past_due(project_id);
            for_collection_today_side(project_id);
            for_pdc_today_side(project_id);

        })

    };

    var collection_receivable_yearly = function (e) {

        var options = {
                interaction: {
                    axis : 'xy',
                    mode : 'index',
                    intersect: false
                  },
                plugins: {
                    legend: {
                        display: true,
                        position:'top',
                        maxHeight: 500,
                        labels: {
                            color: "white",
                            fontSize: 100
                        }
                    },
                    title: false,
                    tooltip: {
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10,
                        callbacks: {
                            label: function(context) {
                                var label = context.dataset.label || '';
        
                                if (label) {
                                    label += ': ';
                                }
                                if (context.parsed.y !== null) {
                                    label += new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(context.parsed.y);
                                }
                                return label.replace("$", "₱ ");
                            }
                        }
                    },
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        display: false,
                    },
                    x: {
                        display: false,
                    }
                },
                elements: {
                    line: {
                        tension: 0.0000001
                    },
                    point: {
                        radius: 4,
                        borderWidth: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            
        };

        
        var ycrc_canvas = document.getElementById('yearly_c_r_chart').getContext("2d");

        var gradient1 = ycrc_canvas.createLinearGradient(0, 0, 0, 240);
        gradient1.addColorStop(0, Chart.helpers.color('#E8DCB9').alpha(1).rgbString());
        gradient1.addColorStop(1, Chart.helpers.color('#E8DCB9').alpha(0.3).rgbString());

        
        var gradient2 = ycrc_canvas.createLinearGradient(0, 0, 0, 240);
        gradient2.addColorStop(0, Chart.helpers.color('#EFBCD5').alpha(1).rgbString());
        gradient2.addColorStop(1, Chart.helpers.color('#EFBCD5').alpha(0.3).rgbString());

        var yearly_c_r_chart = new Chart(ycrc_canvas, {type: 'line',options});

        KTApp.block('#yearly_c_r_chart_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });

        var load_rc_chart = function (e){
            $.ajax({
                url: base_url + "dashboard/yearly_collections_receivables",
                type: "POST",
                dataType: "JSON",
                data : {project_id:e},
                success: function (res) {
                    KTApp.unblock('#yearly_c_r_chart_portlet');

                    $("#total_yearly_sales_amount").html(res.amount);
                    $("#yearly_c_r_range").html(res.range);
                    var newDataSet = {
                        label: "Collectibles",
                        fill: true,
                        backgroundColor: gradient1,
                        borderColor: '#E8DCB9',
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('light'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values_c
                    }
                    var newDataSet1 = {
                        label: "Receivables",
                        backgroundColor: gradient2,
                        fill: true,
                        borderColor: '#EFBCD5',
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('light'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values_r
                    }
    
                    yearly_c_r_chart.data.datasets.push(newDataSet);
                    yearly_c_r_chart.data.datasets.push(newDataSet1);
                    yearly_c_r_chart.data.labels=res.labels;
                    yearly_c_r_chart.update();
    
                },
            });
        }

        load_rc_chart(e);

        $("#financials_project_id").on("change", function (){
            project_id = this.value
            yearly_c_r_chart.data.datasets = [];
            yearly_c_r_chart.data.labels = [];
            yearly_c_r_chart.update();
            load_rc_chart(project_id);

        })
        
    };
    var sales_click = function () {
        var month = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'];
        
        
        var d = new Date(),

        n = d.getMonth(),

        y = d.getFullYear();
        var month_name = month[n];
        $('#sales_month_total,#property_sales_month').on('click', function (){
            localStorage.setItem('sales_dashboard_id',$('#sales_project_id').val());
            localStorage.setItem('sales_dashboard_name',$("#sales_project_id option:selected").text());
            localStorage.setItem('current_month', n+1);
            localStorage.setItem('current_year', y);
            $(location).attr("href", base_url + 'transaction');
        })
        $('#total_collections_month').on('click', function (){
            localStorage.setItem('t_payment_filter','^'+ month_name +' (0?[1-9]|[12][0-9]|3[01]), '+ y +'$');
            $(location).attr("href", base_url + 'transaction_payment');
        })
        $('#total_yearly_sales_amount').on('click', function (){
            var start_date = moment().subtract(1, 'years').startOf('month').format('YYYY/MM/DD');
            var end_date = moment().format('YYYY/MM/DD');
            localStorage.setItem('sales_dashboard_id',$('#sales_project_id').val());
            localStorage.setItem('sales_dashboard_name',$("#sales_project_id option:selected").text());
            localStorage.setItem('sales_reservation_date',start_date + '-' + end_date);
            $(location).attr("href", base_url + 'transaction');
        })
    }
    var general_click = function () {
        $('#top_three_sellers_table').on('click', '.top-three-seller-item' ,function (){
            localStorage.setItem('general_buyer_id', $(this).data('id'));
            localStorage.setItem('general_buyer_name', $(this).data('name'));
            $(location).attr("href", base_url + 'transaction');
        })
        $('#email_comms_click').on('click', function (){
            var start_date = moment().startOf('week').format('YYYY/MM/DD');
            var end_date = moment().endOf('week').format('YYYY/MM/DD');
            localStorage.setItem('general_email_date',start_date+'-'+end_date);
            $(location).attr("href", base_url + 'communication');
        })
        $('#sms_comms_click').on('click', function (){
            var start_date = moment().startOf('week').format('YYYY/MM/DD');
            var end_date = moment().endOf('week').format('YYYY/MM/DD');
            localStorage.setItem('general_sms_date',start_date+'-'+end_date);
            $(location).attr("href", base_url + 'communication');
        })
        $('#past_due_notice_click').on('click', function (){
            var start_date = moment().startOf('week').format('YYYY/MM/DD');
            var end_date = moment().endOf('week').format('YYYY/MM/DD');
            localStorage.setItem('general_pdn_date',start_date+'-'+end_date);
            $(location).attr("href", base_url + 'transaction_past_due_notices');
        })
        $('#tlr_click').on('click', function (){
            var start_date = moment().startOf('week').format('YYYY/MM/DD');
            var end_date = moment().endOf('week').format('YYYY/MM/DD');
            localStorage.setItem('general_tlr_date',start_date+'-'+end_date);
            $(location).attr("href", base_url + 'transaction_letter_request');
        })
    }

    var financials_click = function () {
        $('#financials_collections_today_click').on('click', function (){
            localStorage.setItem('financials_project_id',$('#financials_project_id').val());
            localStorage.setItem('financials_project_name',$('#financials_project_id option:selected').text());
            $(location).attr("href", base_url + 'alerts/payments');
        })
        $('#financials_pdc_today_click').on('click', function (){
            localStorage.setItem('financials_project_id',$('#financials_project_id').val());
            localStorage.setItem('financials_project_name',$('#financials_project_id option:selected').text());
            $(location).attr("href", base_url + 'alerts/post_dated_checks');
        })
    }
    var properties_click = function () {
        $('#properties_click').on('click', function (){
            var start_date = moment().subtract(1, 'years').startOf('month').format('YYYY/MM/DD');
            var end_date = moment().format('YYYY/MM/DD');
            date_range = $('#properties_selected_date_range').val();
            console.log(date_range);
            localStorage.setItem('properties_dashboard_id',$('#properties_selected_project_id').val());
            localStorage.setItem('properties_dashboard_name',$("#properties_selected_project_name").val());
            if(date_range==''){
                localStorage.setItem('properties_reservation_date',start_date + '-' + end_date);
            }else{
                localStorage.setItem('properties_reservation_date',date_range);
            }
            $(location).attr("href", base_url + 'transaction');
        })
    }
    var sellers_click = function () {
        $('#top_sellers_table').on('click', '.top-seller-item' ,function (){
            var start_date = moment().subtract(1, 'years').startOf('month').format('YYYY/MM/DD');
            var end_date = moment().format('YYYY/MM/DD');
            date_range = $('#sellers_selected_date_range').val();
            localStorage.setItem('sellers_project_id',$('#sellers_project_id').val());
            localStorage.setItem('sellers_project_name',$('#sellers_project_id option:selected').text());
            localStorage.setItem('sellers_id', $(this).data('id'));
            localStorage.setItem('sellers_name', $(this).data('name'));
            if(date_range==''){
                localStorage.setItem('sellers_reservation_date',start_date + '-' + end_date);
            }else{
                localStorage.setItem('sellers_reservation_date',date_range);
            }
            $(location).attr("href", base_url + 'transaction');
        })
    }
    var purchases_click = function () {
            var start_date = moment().subtract(1, 'years').startOf('month').format('YYYY/MM/DD');
            var end_date = moment().format('YYYY/MM/DD');
            var start_m_date = moment().startOf('month').format('YYYY/MM/DD');
            var end_m_date = moment().endOf('month').format('YYYY/MM/DD');
        $('.purchases-click').click(function (){
            localStorage.setItem('purchases_status',$(this).data('status'));
            if($(this).data('type')=='specific'){
                date_range = $('#purchases_selected_date_range').val();

                if(date_range==''){
                    localStorage.setItem('purchases_date',start_date + '-' + end_date);
                }else{
                    localStorage.setItem('purchases_date',date_range);
                }
            } else if($(this).data('type')=='month'){
                localStorage.setItem('purchases_date',start_m_date + '-' + end_m_date);
            }

            $(location).attr("href", base_url + $(this).data('target'));
        })

        $('#top_suppliers_table_count,#top_suppliers_table_sum').on('click', '.top-supplier-item' ,function (){
            date_range = $('#purchases_selected_date_range').val();
            localStorage.setItem('purchases_status',$(this).data('status'));

            if(date_range==''){
                localStorage.setItem('purchases_date',start_date + '-' + end_date);
            }else{
                localStorage.setItem('purchases_date',date_range);
            }
            localStorage.setItem('suppliers_name',$(this).data('name'))
            localStorage.setItem('suppliers_id',$(this).data('id'))
            $(location).attr("href", base_url + $(this).data('target'));

        })
    }
    var accounting_click = function () {
            var start_date = moment().subtract(1, 'years').startOf('month').format('YYYY/MM/DD');
            var end_date = moment().format('YYYY/MM/DD');
            $('#accounting_pr_click,#accounting_pv_click').on('click', function (){
                if($(this).data('target')=='payment_request'){
                    localStorage.setItem('accounting_project_id',$('#accounting_project_id').val());
                    localStorage.setItem('accounting_project_name',$('#accounting_project_id option:selected').text());
                }
                date_range = $('#accounting_selected_date_range').val();

                if(date_range==''){
                    localStorage.setItem('accounting_date',start_date + '-' + end_date);
                }else{
                    localStorage.setItem('accounting_date',date_range);
                }
                $(location).attr("href", base_url + $(this).data('target'));

            })
    }

    // ------------------------------>End of Financials dashboard

    // <------------------------------Start of Inventory dashboard

    var inventory_dashboard = function () {
        KTApp.block('#inventory_report_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });
        $.ajax({
            url: base_url + 'property/dashboard_inventory_report',
            dataType: 'json',
            type: "POST",
            data : {hi:"hello"},
            success: function (response) {
                KTApp.unblock('#inventory_report_portlet');

                $('#inventory_report').html(response.html);
            },
        });
    };

    var projects_list = function () {
        $.ajax({
            url: base_url + 'dashboard/sold_projects_list',
            dataType: 'json',
            type: "POST",
            data : {hi:"hello"},
            success: function (response) {
                $('#project_dropdown_container').html(response.html);
            },
        });
    };
    // ------------------------------>End of Inventory dashboard    
    // <------------------------------Start of Sellers dashboard
    

    
    var total_sellers_graph = function () {

        const options={
            plugins: {
                legend: false,
                title: false,
                tooltip: {
                    mode: 'nearest',
                    intersect: false,
                    position: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem) {
                            var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(tooltipItem.parsed.y);
                           
                            return number.replace("$", "₱ ");
                        }
                    }
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    display: true,
                    suggestedMax: 5,
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(label, index, labels) {
                            var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(label);
                        
                            return number.replace("$", "₱ ");
                        }
                    }
                },
                x: {
                    display: true,
                    
                }
            },
            elements: {
                line: {
                    tension: 0.0000001
                },
                point: {
                    radius: 4,
                    borderWidth: 12
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 10,
                    bottom: 0
                }
            }
        };

        var cbc = document.getElementById('commissions_bar_chart').getContext("2d")
        yearly_chart = new Chart(cbc, {type: 'bar',options});
        
        KTApp.block('#commissions_bar_chart_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });

        var monthly_commission_chart = function (project_id,time_frame,start_date,end_date){

            $.ajax({
                url: base_url + 'dashboard/dashboard_seller_graph',
                dataType: 'json',
                type: "post",
                data : {
                    time_frame:time_frame,
                    commission_status:0,
                    project_id: project_id,
                    start_date: start_date,
                    end_date: end_date    
                },
    
                success: function (res) {
                    KTApp.unblock('#commissions_bar_chart_portlet');

                    $('#commissions_range').html(res.date_range);
                    var newDataSet = {
                        backgroundColor: dynamicColors(),
                        fill:true,
                        borderColor: '#FFFFFF',
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
    
                    yearly_chart.data.datasets.push(newDataSet);
                    yearly_chart.options.scales = {
                        x: {
                            display:true
                        },
                        y: {
                            display:true,
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(label, index, labels) {
                                    var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(label);
                                
                                    return number.replace("$", "₱ ");
                                }
                            }
                        }
                    },
                    yearly_chart.data.labels=res.labels;
                    yearly_chart.update();
                },
            });

        }
        var top_seller_graph = function (project_id,time_frame,start_date,end_date){

            KTApp.block('#top_sellers_table_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/dashboard_seller_top_sellers',
                dataType: 'json',
                type: "POST",
                data : {
                    time_frame:time_frame,
                    project_id: project_id,
                    start_date: start_date,
                    end_date: end_date   
                },
                success: function (response) {
                    KTApp.unblock('#top_sellers_table_portlet');

                    $('#top_sellers_table').html(response.html);
                },
            });
        }

        top_seller_graph(0,'year');
        monthly_commission_chart(0,'year');

        var start_month = $('#sellers_start_month');
        var end_month = $('#sellers_end_month');

        $('#sellers_project_id, #sellers_start_month, #sellers_end_month').on("change", function () {
            var project_id = $('#sellers_project_id').val();


            if(this.id=='sellers_start_month'){
                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        end_month.val(start_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            } else if(this.id=='sellers_end_month'){

                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        start_month.val(end_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            }
            if(start_month.val()=='' || end_month.val()==''){
                time_frame='year';
            }
            yearly_chart.data.datasets = [];
            yearly_chart.data.labels = [];
            yearly_chart.update();
            monthly_commission_chart(project_id,time_frame,start_month.val(),end_month.val());
            top_seller_graph(project_id,time_frame,start_month.val(),end_month.val());
            if(time_frame=='specific'){
                var start_date = moment(start_month.val()).startOf('month').format('YYYY/MM/DD');
                var end_date = moment(end_month.val()).endOf('month').format('YYYY/MM/DD');
                $('#sellers_selected_date_range').val(start_date + '-' + end_date);
            }

        })

        $("#sellers_reset_date").on("click", function (){
            var project_id = $('#sellers_project_id').val();
            start_month.val('');
            end_month.val('');
            yearly_chart.data.datasets = [];
            yearly_chart.data.labels = [];
            yearly_chart.update();
            top_seller_graph(project_id,'year');
            monthly_commission_chart(project_id,'year');
            $('#sellers_selected_date_range').val('')
        })

    }

    var sellers_graph = function() {

//      Build the different charts in the sellers dashboard using the options template above
//      Use ajax to get the different values and then add the custom options required for each graph.
//      Total Pending Commissions for the Year

//      Total Commissions for the prev 6 months
        const options={
            plugins: {
                legend: false,
                title: false,
                tooltip: {
                    mode: 'nearest',
                    intersect: false,
                    position: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem) {
                            var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(tooltipItem.parsed.y);
                        
                            return number.replace("$", "₱ ");
                        }
                    }
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    display: true,
                    suggestedMax: 5,
                },
                x: {
                    display: true,
                    
                }
            },
            elements: {
                line: {
                    tension: 0.0000001
                },
                point: {
                    radius: 4,
                    borderWidth: 12
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 10,
                    bottom: 0
                }
            }
        };
       

        KTApp.block('#total_commissions_month_chart_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });
            $.ajax({
                url: base_url + 'dashboard/dashboard_seller_graph',
                dataType: 'json',
                type: "post",
                data : {time_frame:'6months',commission_status:0},
    
                success: function (res) {
                    KTApp.unblock('#total_commissions_month_chart_portlet');

                    $('#total_commissions_month').html(res.current_month);
                    var tcmc = document.getElementById('total_commissions_month_chart').getContext("2d")
    
                    var gradient = tcmc.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#BCEAE1').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#BCEAE1').alpha(0.3).rgbString());
                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('success'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
                    var total_c_chart = new Chart(tcmc, {type: 'line',options});   
                    total_c_chart.options.plugins.legend = false;
                    total_c_chart.options.scales.y.display = false;
                    total_c_chart.options.scales.x.display = false;
                    total_c_chart.data.datasets.push(newDataSet);
                    total_c_chart.data.labels=res.labels;
                    total_c_chart.update();
                },
            });
            
            KTApp.block('#pending_commissions_month_chart_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
    //      Pending Commissions for the prev 6 months
            $.ajax({
                url: base_url + 'dashboard/dashboard_seller_graph',
                dataType: 'json',
                type: "post",
                data : {time_frame:'6months',commission_status:2},
    
                success: function (res) {
                    KTApp.unblock('#pending_commissions_month_chart_portlet');

                    $('#pending_commissions_month').html(res.current_month);
                    var pcmc = document.getElementById('pending_commissions_month_chart').getContext("2d")
    
                    var gradient = pcmc.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#A1C5EE').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#A1C5EE').alpha(0.3).rgbString());
                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('brand'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
    
                    var pending_c_chart = new Chart(pcmc, {type: 'line',options});   
                    pending_c_chart.options.plugins.legend = false;
                    pending_c_chart.options.scales.y.display = false;
                    pending_c_chart.options.scales.x.display = false;
                    pending_c_chart.data.datasets.push(newDataSet);
                    pending_c_chart.data.labels=res.labels;
                    pending_c_chart.update();
                },
            });

            KTApp.block('#processing_commissions_month_chart_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
    
    //      Processing Commissions for the prev 6 months
            $.ajax({
                url: base_url + 'dashboard/dashboard_seller_graph',
                dataType: 'json',
                type: "post",
                data : {time_frame:'6months',commission_status:3},
    
                success: function (res) {
                    KTApp.unblock('#processing_commissions_month_chart_portlet');

                    $('#processing_commissions_month').html(res.current_month);
                    var pccmc = document.getElementById('processing_commissions_month_chart').getContext("2d")
    
                    var gradient = pccmc.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#FF9DB6').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#FF9DB6').alpha(0.3).rgbString());
                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('danger'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
    
                    var processing_c_chart = new Chart(pccmc, {type: 'line',options});   
                    processing_c_chart.options.plugins.legend = false;
                    processing_c_chart.options.scales.y.display = false;
                    processing_c_chart.options.scales.x.display = false;
                    processing_c_chart.data.datasets.push(newDataSet);
                    processing_c_chart.data.labels=res.labels;
                    processing_c_chart.update();
                },
            });
            
            KTApp.block('#released_commissions_month_chart_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
    //      Released Commissions for the prev 6 months
            $.ajax({
                url: base_url + 'dashboard/dashboard_seller_graph',
                dataType: 'json',
                type: "post",
                data : {time_frame:'6months',commission_status:4},
    
                success: function (res) {
                    KTApp.unblock('#released_commissions_month_chart_portlet');

                    $('#released_commissions_month').html(res.current_month);
                    var rcmc = document.getElementById('released_commissions_month_chart').getContext("2d")
    
                    var gradient = rcmc.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#ffefce').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#ffefce').alpha(0.3).rgbString());
                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('warning'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
    
                    var released_c_chart = new Chart(rcmc, {type: 'line',options});   
                    released_c_chart.options.plugins.legend = false;
                    released_c_chart.options.scales.y.display = false;
                    released_c_chart.options.scales.x.display = false;
                    released_c_chart.data.datasets.push(newDataSet);
                    released_c_chart.data.labels=res.labels;
                    released_c_chart.update();
                },
            });
    };

    var top_seller_table = function () {

    };

// ------------------------------>End of Sellers dashboard    
// <------------------------------Start of Purchase dashboard

    var purchase_order_chart = function () {

        const options={
            plugins: {
                legend: false,
                title: false,
                tooltip: {
                    mode: 'nearest',
                    intersect: false,
                    position: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem) {
                            return tooltipItem.parsed.y + ' order(s)' ;
                        }
                    }
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    display: true,
                    suggestedMax: 5,
                    ticks: {
                        callback: function(label, index, labels) {
                            
                        
                            return label + ' order(s)';
                        }
                    }
                },
                x: {
                    display: true,
                    
                }
            },
            elements: {
                line: {
                    tension: 0.0000001
                },
                point: {
                    radius: 4,
                    borderWidth: 12
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 10,
                    bottom: 0
                }
            }
        };


        var poc = document.getElementById('po_bar_chart').getContext("2d")
        po_chart = new Chart(poc, {type: 'bar',options});
        

        var purchase_charts = function(project_id,time_frame,start_date,end_date){

            KTApp.block('#po_bar_chart_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });

            $.ajax({
                url: base_url + 'dashboard/purchase_charts',
                dataType: 'json',
                type: "post",
                data : {
                    time_frame:time_frame,
                    status:2,
                    get_stat:'count',
                    // project_id: project_id,
                    start_date: start_date,
                    end_date: end_date,
                    },
    
                success: function (res) {
                    KTApp.unblock('#po_bar_chart_portlet');

                    $('#po_range').html(res.date_range);
                    var newDataSet = {
                        backgroundColor: dynamicColors(),
                        fill:true,
                        borderColor: '#FFFFFF',
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
    
                    po_chart.data.datasets.push(newDataSet);
                    po_chart.options.scales = {
                        x: {
                            display:true
                        },
                        y: {
                            display:true,
                            ticks: {
                            stepSize: 1
                            }
                        }
                    },
                    po_chart.data.labels=res.labels;
                    po_chart.update();
                },
            });

        }

        purchase_charts(0,' -1 year');
        var start_month = $('#purchases_start_month');
        var end_month = $('#purchases_end_month');

        $('#purchases_project_id, #purchases_start_month, #purchases_end_month').on("change", function () {
            var project_id = $('#purchases_project_id').val();


            if(this.id=='purchases_start_month'){
                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        end_month.val(start_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            } else if(this.id=='purchases_end_month'){

                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        start_month.val(end_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            }
            if(start_month.val()=='' || end_month.val()==''){
                time_frame=' -1 year';
            }
            po_chart.data.datasets = [];
            po_chart.data.labels = [];
            po_chart.update();
            purchase_charts(project_id,time_frame,start_month.val(),end_month.val());
            if(time_frame=='specific'){
                var start_date = moment(start_month.val()).startOf('month').format('YYYY/MM/DD');
                var end_date = moment(end_month.val()).endOf('month').format('YYYY/MM/DD');
                $('#purchases_selected_date_range').val(start_date + '-' + end_date);
            }

        })

        $("#purchases_reset_date").on("click", function (){
            var project_id = $('#purchases_project_id').val();
            start_month.val('');
            end_month.val('');
            po_chart.data.datasets = [];
            po_chart.data.labels = [];
            po_chart.update();
            purchase_charts(project_id,' -1 year','','');
            $('#purchases_selected_date_range').val('');
        })

    }
    var purchases_tiles = function () {
        KTApp.block('#po_this_month_amount_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });
        $.ajax({
            url: base_url + 'dashboard/purchase_charts',
            dataType: 'json',
            type: "post",
            data : {
                time_frame:'',
                status:100,
                get_stat:'both' 
                },
            success: function (response) {
                KTApp.unblock('#po_this_month_amount_portlet');

                var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(response.values[0]['sum']);
                $('#po_this_month').html(response.labels);
                $('#po_this_month_amount').html(number.replace("$", "₱ "));
                $('#po_this_month_count').html(response.values[0]['count']);
            },
        });
        KTApp.block('#new_po_this_month_amount_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });
        $.ajax({
            url: base_url + 'dashboard/purchase_charts',
            dataType: 'json',
            type: "post",
            data : {
                time_frame:'',
                status:1,
                get_stat:'both' 
                },
            success: function (response) {
                KTApp.unblock('#new_po_this_month_amount_portlet');

                var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(response.values[0]['sum']);
                $('#new_po_this_month').html(response.labels);
                $('#new_po_this_month_amount').html(number.replace("$", "₱ "));
                $('#new_po_this_month_count').html(response.values[0]['count']);
            },
        });
        KTApp.block('#app_po_this_month_amount_portlet',{
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Loading data...'
        });
        $.ajax({
            url: base_url + 'dashboard/purchase_charts',
            dataType: 'json',
            type: "post",
            data : {
                time_frame:'',
                status:2,
                get_stat:'both' 
                },
            success: function (response) {
                KTApp.unblock('#app_po_this_month_amount_portlet');

                var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(response.values[0]['sum']);
                $('#app_po_this_month').html(response.labels);
                $('#app_po_this_month_amount').html(number.replace("$", "₱ "));
                $('#app_po_this_month_count').html(response.values[0]['count']);
            },
        });
    };

    var suppliers_table = function () {

        var suppliers_table_rank = function (project_id,time_frame,start_date,end_date){
            KTApp.block('#top_suppliers_table_count_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/top_po_suppliers',
                dataType: 'json',
                type: "post",
                data : {
                    status:2,
                    get_stat:'count',
                    project_id: project_id,
                    time_frame: time_frame,
                    start_date: start_date,
                    end_date: end_date
                    },
                success: function (response) {
                    KTApp.unblock('#top_suppliers_table_count_portlet');

                    $('#top_po_count_date_range').html(response.date_range);
                    $('#top_suppliers_table_count').html(response.html);
                },
            });
            KTApp.block('#top_suppliers_table_sum_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/top_po_suppliers',
                dataType: 'json',
                type: "post",
                data : {
                    status:2,
                    get_stat:'sum',
                    project_id: project_id,
                    time_frame: time_frame,
                    start_date: start_date,
                    end_date: end_date
                    },
                success: function (response) {
                    KTApp.unblock('#top_suppliers_table_sum_portlet');

                    $('#top_po_sum_date_range').html(response.date_range);
                    $('#top_suppliers_table_sum').html(response.html);
                },
            });
        }

        suppliers_table_rank(0,' -1 year');

        var start_month = $('#purchases_start_month');
        var end_month = $('#purchases_end_month');

        $('#purchases_project_id, #purchases_start_month, #purchases_end_month').on("change", function () {
            var project_id = $('#purchases_project_id').val();


            if(this.id=='purchases_start_month'){
                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        end_month.val(start_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            } else if(this.id=='purchases_end_month'){

                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        start_month.val(end_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            }
            if(start_month.val()=='' || end_month.val()==''){
                time_frame=' -1 year';
            }
            suppliers_table_rank(project_id,time_frame,start_month.val(),end_month.val());

        })

        $("#purchases_reset_date").on("click", function (){
            var project_id = $('#purchases_project_id').val();
            start_month.val('');
            end_month.val('');
            suppliers_table_rank(project_id,' -1 year','','');
        })

    }

    // <------------------------------Start of Accounting dashboard

    var payment_request_chart = function () {

        const options={
            plugins: {
                legend: false,
                title: false,
                tooltip: {
                    mode: 'nearest',
                    intersect: false,
                    position: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem) {
                            var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(tooltipItem.parsed.y);
                        
                            return number.replace("$", "₱ ");
                        }
                    }
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    display: true,
                    suggestedMax: 5,
                },
                x: {
                    display: true,
                    
                }
            },
            elements: {
                line: {
                    tension: 0.0000001
                },
                point: {
                    radius: 4,
                    borderWidth: 12
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 10,
                    bottom: 0
                }
            }
        };

        var prr = document.getElementById('payment_requests_line_chart').getContext("2d")
        prr_chart = new Chart(prr, {type: 'line',options});

        var prv = document.getElementById('payment_vouchers_line_chart').getContext("2d")
        prv_chart = new Chart(prv, {type: 'line',options});


        var accounting_payment_requests = function (project_id,time_frame,start_date,end_date){
            KTApp.block('#payment_requests_line_chart_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/accounting_payment_rv',
                dataType: 'json',
                type: "post",
                data : {
                    time_frame:time_frame,
                    type: 0,
                    project_id: project_id,
                    start_date: start_date,
                    end_date: end_date
                    },
    
                success: function (res) {
                    KTApp.unblock('#payment_requests_line_chart_portlet');

                    $('#payment_requests_range').html(res.date_range);
    
    
                    var gradient = prr.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#A1C5EE').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#A1C5EE').alpha(0.3).rgbString());
                    var newDataSet = {
                        backgroundColor: gradient,
                        fill:true,
                        borderColor: KTApp.getStateColor('brand'),
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values
                    }
                    prr_chart.data.datasets.push(newDataSet);
                    prr_chart.options.scales = {
                        x: {
                            display:true,
                            grid: {
                                display: false
                            }
                        },
                        y: {
                            display: true,
                            suggestedMax: 5,
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(label, index, labels) {
                                    var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(label);
                                
                                    return number.replace("$", "₱ ");
                                }
                            }
                        }
                    },
                    prr_chart.data.labels=res.labels;
                    prr_chart.update();
                },
            });
        }

        var accounting_payment_vouchers = function (project_id,time_frame,start_date,end_date){
            KTApp.block('#payment_vouchers_line_chart_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
        $.ajax({
            url: base_url + 'dashboard/accounting_payment_rv',
            dataType: 'json',
            type: "post",
            data : {
                time_frame:time_frame,
                type: 1,
                project_id: project_id,
                start_date: start_date,
                end_date: end_date
                },

            success: function (res) {
                KTApp.unblock('#payment_vouchers_line_chart_portlet');

                $('#payment_vouchers_range').html(res.date_range);


                var gradient = prv.createLinearGradient(0, 0, 0, 240);
                gradient.addColorStop(0, Chart.helpers.color('#FF9DB6').alpha(1).rgbString());
                gradient.addColorStop(1, Chart.helpers.color('#FF9DB6').alpha(0.3).rgbString());
                var newDataSet = {
                    backgroundColor: gradient,
                    fill:true,
                    borderColor: KTApp.getStateColor('danger'),
                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                    data: res.values
                }
                prv_chart.data.datasets.push(newDataSet);
                prv_chart.options.scales = {
                    x: {
                        display:true,
                        grid: {
                            display: false
                        }
                    },
                    y: {
                        display: true,
                        suggestedMax: 5,
                        ticks: {
                            // Include a dollar sign in the ticks
                            callback: function(label, index, labels) {
                                var number = new Intl.NumberFormat('en-US',{ style: 'currency', currency: 'USD' }).format(label);
                            
                                return number.replace("$", "₱ ");
                            }
                        }
                    }
                },
                prv_chart.data.labels=res.labels;
                prv_chart.update();
            },
        });
    };
        accounting_payment_requests(0,' -1 year','','');
        accounting_payment_vouchers(0,' -1 year','','');

        var start_month = $('#accounting_start_month');
        var end_month = $('#accounting_end_month');

        $('#accounting_project_id, #accounting_start_month, #accounting_end_month').on("change", function () {
            var project_id = $('#accounting_project_id').val();


            if(this.id=='accounting_start_month'){
                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        end_month.val(start_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            } else if(this.id=='accounting_end_month'){

                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        start_month.val(end_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
        }
            if(start_month.val()=='' || end_month.val()==''){
                time_frame=' -1 year';
            }
            prr_chart.data.datasets = [];
            prr_chart.data.labels = [];
            prr_chart.update();
            prv_chart.data.datasets = [];
            prv_chart.data.labels = [];
            prv_chart.update();
            accounting_payment_requests(project_id,time_frame,start_month.val(),end_month.val());
            accounting_payment_vouchers(project_id,time_frame,start_month.val(),end_month.val());

            if(time_frame=='specific'){
                var start_date = moment(start_month.val()).startOf('month').format('YYYY/MM/DD');
                var end_date = moment(end_month.val()).endOf('month').format('YYYY/MM/DD');
                $('#accounting_selected_date_range').val(start_date + '-' + end_date);
            }

        })

        $("#accounting_reset_date").on("click", function (){
            var project_id = $('#accounting_project_id').val();
            start_month.val('');
            end_month.val('');
            prr_chart.data.datasets = [];
            prr_chart.data.labels = [];
            prr_chart.update();
            prv_chart.data.datasets = [];
            prv_chart.data.labels = [];
            prv_chart.update();
            accounting_payment_requests(project_id,' -1 year','','');
            accounting_payment_vouchers(project_id,' -1 year','','');
            $('#accounting_selected_date_range').val('');

        })

    }
    
    var payment_request_doughnut_chart = function () {

        const options={
            plugins: {
                legend: {
                    display: true,
                    position: 'right',
                    labels: {
                        font: {
                            size: 14
                        },
                        padding: 15
                    }

                },
                title: false,
                tooltip: {
                    mode: 'nearest',
                    intersect: false,
                    position: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem, data) {             
                            return  tooltipItem.label + ': ' + tooltipItem.parsed + '%';
                        }
                    }
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            elements: {
                line: {
                    tension: 0.0000001
                },
                point: {
                    radius: 4,
                    borderWidth: 12
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 10,
                    bottom: 0
                }
            }
        };

        var prd = document.getElementById('payment_requests_doughnut_chart').getContext("2d")
        prd_chart = new Chart(prd, {type: 'doughnut',options});

        var accounting_request_pie = function (project_id,time_frame,start_date,end_date){
            KTApp.block('#payment_requests_doughnut_chart_portlet',{
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Loading data...'
            });
            $.ajax({
                url: base_url + 'dashboard/accounting_payment_requests_pie',
                dataType: 'json',
                type: "post",
                data : {
                    time_frame:time_frame,
                    project_id: project_id,
                    start_date: start_date,
                    end_date: end_date
                    },
    
                success: function (res) {
                    KTApp.unblock('#payment_requests_doughnut_chart_portlet');

                    $('#payment_rpie_range').html(res.date_range);

    
                    var gradient = prd.createLinearGradient(0, 0, 0, 240);
                    gradient.addColorStop(0, Chart.helpers.color('#A1C5EE').alpha(1).rgbString());
                    gradient.addColorStop(1, Chart.helpers.color('#A1C5EE').alpha(0.3).rgbString());
                    var c = res.values.length;
                    colors = [];
                    for (var i= 0; i<c;i++){
                        colors.push(dynamicColors());
                    }
                    var newDataSet = {
                        backgroundColor: colors,
                        fill:true,
                        borderColor: '#FFFFFF',
                        pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: KTApp.getStateColor('danger'),
                        pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
                        data: res.values,
                    }
                    prd_chart.data.datasets.push(newDataSet);
                    prd_chart.data.labels=res.labels;
                    prd_chart.update();
                },
            });

        }
        accounting_request_pie(0,' -1 year');

        var start_month = $('#accounting_start_month');
        var end_month = $('#accounting_end_month');

        $('#accounting_project_id, #accounting_start_month, #accounting_end_month').on("change", function () {
            var project_id = $('#accounting_project_id').val();


            if(this.id=='accounting_start_month'){
                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        end_month.val(start_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            } else if(this.id=='accounting_end_month'){

                if (!end_month.val()==""){
                    if (new Date(end_month.val())<=new Date(start_month.val())){
                        start_month.val(end_month.val());
                        time_frame = 'specific';
                    }else{
                        time_frame='specific';
                    }
                }
            }
            if(start_month.val()=='' || end_month.val()==''){
                time_frame=' -1 year';
            }
            prd_chart.data.datasets = [];
            prd_chart.data.labels = [];
            prd_chart.update();
            accounting_request_pie(project_id,time_frame,start_month.val(),end_month.val());

        })

        $("#accounting_reset_date").on("click", function (){
            var project_id = $('#accounting_project_id').val();
            start_month.val('');
            end_month.val('');
            prd_chart.data.datasets = [];
            prd_chart.data.labels = [];
            prd_chart.update();
            accounting_request_pie(project_id,' -1 year','','');
        })
        
    
    }

    var datepickers = function () {

        $('#sellers_start_month').val('');
        $('#sellers_end_month').val('');
        $('#purchases_start_month').val('');
        $('#purchases_end_month').val('');
        $('#accounting_start_month').val('');
        $('#accounting_end_month').val('');
        $('#properties_start_month').val('');
        $('#properties_end_month').val('');

        $('#sellers_start_month').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            container:'#sellers_start_month_modal',
            viewMode: 'months',
            minViewMode: 'months',
            format: 'yyyy-mm',
            autoclose: true,
            templates: arrows
        });
        $('#sellers_end_month').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            container:'#sellers_end_month_modal',
            viewMode: 'months',
            minViewMode: 'months',
            format: 'yyyy-mm',
            autoclose: true,
            templates: arrows
        });
        $('#purchases_start_month').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            container:'#purchases_start_month_modal',
            viewMode: 'months',
            minViewMode: 'months',
            format: 'yyyy-mm',
            autoclose: true,
            templates: arrows
        });
        $('#purchases_end_month').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            container:'#purchases_end_month_modal',
            viewMode: 'months',
            minViewMode: 'months',
            format: 'yyyy-mm',
            autoclose: true,
            templates: arrows
        });
        $('#accounting_start_month').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            container:'#accounting_start_month_modal',
            viewMode: 'months',
            minViewMode: 'months',
            format: 'yyyy-mm',
            autoclose: true,
            templates: arrows
        });
        $('#accounting_end_month').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            container:'#accounting_end_month_modal',
            viewMode: 'months',
            minViewMode: 'months',
            format: 'yyyy-mm',
            autoclose: true,
            templates: arrows
        });
        $('#properties_start_month').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            container:'#properties_start_month_modal',
            viewMode: 'months',
            minViewMode: 'months',
            format: 'yyyy-mm',
            autoclose: true,
            templates: arrows
        });
        $('#properties_end_month').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            container:'#properties_end_month_modal',
            viewMode: 'months',
            minViewMode: 'months',
            format: 'yyyy-mm',
            autoclose: true,
            templates: arrows
        });
    
      }

    return {
        //main function to initiate the module
        general: function (){
            general_notices_info_box();
            general_click();
        },
        sales: function () {
            sales_money_charts(0);
            sales_text_charts(0);
            sales_click();
            // auditTrailSide();
            // loginHistorySide();
        },
        financials: function () {
            financials_cards(0);
            collection_receivable_yearly(0);
            financials_click();
        },
        inventory: function () {
            chart_builder_bar_graph();
            inventory_dashboard();
            // projects_chart_load();
            projects_list();
            properties_click();
        },
        sellers: function () {
            total_sellers_graph();
            sellers_graph();
            top_seller_table();
            sellers_click();
        },
        purchase: function () {
            purchase_order_chart();
            purchases_tiles();
            suppliers_table();
            purchases_click();
        },
        accounting: function () {
            payment_request_chart();
            payment_request_doughnut_chart();
            accounting_click();
        },
        initiate: function () {
            datepickers();
        }
    };
})();

jQuery(document).ready(function () {
    DashboardWidgets.initiate();
    DashboardWidgets.general();
    $('li a[data-target="#sales"]').one( "click", function() {
        // KTApp.block('#kt_content', {
        //     overlayColor: '#000000',
        //     type: 'v2',
        //     state: 'primary',
        //     message: 'Loading data...',
        //     css: {
        //         padding: 0,
        //         margin: 0,
        //         width: '30%',
        //         top: '40%',
        //         left: '35%',
        //         textAlign: 'center',
        //         color: '#000',
        //         border: '3px solid #aaa',
        //         backgroundColor: '#fff',
        //         cursor: 'wait',
        //     },
        // });

            DashboardWidgets.sales();
            // KTApp.unblock('#kt_content');

    });
    $('li a[data-target="#financials"]').one( "click", function() {
        // KTApp.block('#kt_content', {
        //     overlayColor: '#000000',
        //     type: 'v2',
        //     state: 'primary',
        //     message: 'Loading data...',
        //     css: {
        //         padding: 0,
        //         margin: 0,
        //         width: '30%',
        //         top: '40%',
        //         left: '35%',
        //         textAlign: 'center',
        //         color: '#000',
        //         border: '3px solid #aaa',
        //         backgroundColor: '#fff',
        //         cursor: 'wait',
        //     },
        // });

            DashboardWidgets.financials();
            // KTApp.unblock('#kt_content');

    });
    $('li a[data-target="#properties"]').one( "click", function() {
        // KTApp.block('#kt_content', {
        //     overlayColor: '#000000',
        //     type: 'v2',
        //     state: 'primary',
        //     message: 'Loading data...',
        //     css: {
        //         padding: 0,
        //         margin: 0,
        //         width: '30%',
        //         top: '40%',
        //         left: '35%',
        //         textAlign: 'center',
        //         color: '#000',
        //         border: '3px solid #aaa',
        //         backgroundColor: '#fff',
        //         cursor: 'wait',
        //     },
        // });


            DashboardWidgets.inventory();

            // KTApp.unblock('#kt_content');

    });
    $('li a[data-target="#sellers"]').one( "click", function() {
        // KTApp.block('#kt_content', {
        //     overlayColor: '#000000',
        //     type: 'v2',
        //     state: 'primary',
        //     message: 'Loading data...',
        //     css: {
        //         padding: 0,
        //         margin: 0,
        //         width: '30%',
        //         top: '40%',
        //         left: '35%',
        //         textAlign: 'center',
        //         color: '#000',
        //         border: '3px solid #aaa',
        //         backgroundColor: '#fff',
        //         cursor: 'wait',
        //     },
        // });


            DashboardWidgets.sellers();
            // KTApp.unblock('#kt_content');

    });

    $('li a[data-target="#purchases"]').one( "click", function() {
        // KTApp.block('#kt_content', {
        //     overlayColor: '#000000',
        //     type: 'v2',
        //     state: 'primary',
        //     message: 'Loading data...',
        //     css: {
        //         padding: 0,
        //         margin: 0,
        //         width: '30%',
        //         top: '40%',
        //         left: '35%',
        //         textAlign: 'center',
        //         color: '#000',
        //         border: '3px solid #aaa',
        //         backgroundColor: '#fff',
        //         cursor: 'wait',
        //     },
        // });


            DashboardWidgets.purchase();
            // KTApp.unblock('#kt_content');

    });

    $('li a[data-target="#accounting"]').one( "click", function() {
        // KTApp.block('#kt_content', {
        //     overlayColor: '#000000',
        //     type: 'v2',
        //     state: 'primary',
        //     message: 'Loading data...',
        //     css: {
        //         padding: 0,
        //         margin: 0,
        //         width: '30%',
        //         top: '40%',
        //         left: '35%',
        //         textAlign: 'center',
        //         color: '#000',
        //         border: '3px solid #aaa',
        //         backgroundColor: '#fff',
        //         cursor: 'wait',
        //     },
        // });


            DashboardWidgets.accounting();
            // KTApp.unblock('#kt_content');

    });

    KTQuickPanel.init();
  });


