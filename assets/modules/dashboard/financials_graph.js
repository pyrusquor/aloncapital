'use strict';

// Class definition
var KTDashboard = function () {

    var highCharts = function (info) {

        Highcharts.chart('container', {
            chart: {
                type: 'spline'
            },
            title: {
                text: info.title
            },

            subtitle: {
                text: info.subtitle
            },

            yAxis: {
                title: {
                    text: info.yAxisTitle
                }
            },

            xAxis: {
                categories:  JSON.parse(info.dates)
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                spline: {
                    dataLabels: {
                        enabled:false
                    },
                    enableMouseTracking:true
                }
            },
            series: JSON.parse(info.series),

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });

    }
    $('#advanceSearch').submit(function(e){
        e.preventDefault();
        var form_values = $('#advanceSearch').serialize();
        filter(form_values);
    });
    
    function filter(values){
        var values = values ? values: 'ajax:1';
        $.ajax({
            url: base_url + 'dashboard/financials_graph/',
            method:"GET",
            data:values,
            dataType: 'json',
            success:function(data){
                $('#filterModal').modal('hide');
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding:    0,
                        margin:     0,
                        width:      '30%',
                        top:        '40%',
                        left:       '35%',
                        textAlign:  'center',
                        color:      '#000',
                        border:     '3px solid #aaa',
                        backgroundColor:'#fff',
                        cursor:     'wait'
                    },
                });
                setTimeout(function() {
                    $('#g_container').html(data.html);
                    // console.log(JSON.parse(data.data.series));
                    // console.log(data.data.series);

                    highCharts(data.data);
                    $('#daterangepicker').val(data.daterange);


                    KTApp.unblock('#kt_content');
                }, 500);
            }
        })
    }
    return {
        // Init demos
        init: function () {
            filter();

            // demo loading
            var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
            loading.show();

            setTimeout(function () {
                loading.hide();
            }, 3000);
        }
    };
}();

// Class initialization on page load
jQuery(document).ready(function () {
    KTDashboard.init();
    datepicker()
});