'use strict';

// Class definition
var KTDashboard = function () {

    var highCharts = function (info) {

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: info.title
            },
            subtitle: {
                text: info.subtitle
            },
            xAxis: {
                categories: JSON.parse(info.dates),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: info.yAxisTitle
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} php</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: JSON.parse(info.series),
        });

         Highcharts.chart('container_2', {
            chart: {
                type: 'column'
            },
            title: {
                text: info.title_2
            },
            subtitle: {
                text: info.subtitle
            },
            xAxis: {
                categories: JSON.parse(info.dates),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: info.yAxisTitle
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} php</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: JSON.parse(info.series_2),
        });

    }
    $('#advanceSearch').submit(function(e){
        e.preventDefault();
        var form_values = $('#advanceSearch').serialize();
        filter(form_values);
    });
    
    function filter(values){
        var values = values ? values: 'ajax:1';
        $.ajax({
            url: base_url + 'dashboard/sellers_graph/',
            method:"GET",
            data:values,
            dataType: 'json',
            success:function(data){
                $('#filterModal').modal('hide');
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding:    0,
                        margin:     0,
                        width:      '30%',
                        top:        '40%',
                        left:       '35%',
                        textAlign:  'center',
                        color:      '#000',
                        border:     '3px solid #aaa',
                        backgroundColor:'#fff',
                        cursor:     'wait'
                    },
                });
                setTimeout(function() {
                    $('#g_container').html(data.html);
                    // console.log(JSON.parse(data.data.series));
                    // console.log(data.data.series);

                    highCharts(data.data);
                    $('#daterangepicker').val(data.daterange);


                    KTApp.unblock('#kt_content');
                }, 500);
            }
        })
    }
    return {
        // Init demos
        init: function () {
            filter();

            // demo loading
            var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
            loading.show();

            setTimeout(function () {
                loading.hide();
            }, 3000);
        }
    };
}();

// Class initialization on page load
jQuery(document).ready(function () {
    KTDashboard.init();
    datepicker()
    
});