'use strict';

// Class definition
var KTDashboard = function () {

        // Radialize the colors
        Highcharts.setOptions({
            colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                return {
                    radialGradient: {
                        cx: 0.5,
                        cy: 0.3,
                        r: 0.7
                    },
                    stops: [
                        [0, color],
                        [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                    ]
                };
            })
        });

    var highCharts = function (info) {

        // Build the chart
        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_1
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br> {point.y} / {point.total}'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: [
                    { name: 'Male', y: info.male, color: 'skyblue' },
                    { name: 'Female', y: info.female, color: 'pink' }
                ]
            }]
        });

        // Build the chart
        Highcharts.chart('container2', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_2
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_2)

            }]
        });

        // Build the chart
        Highcharts.chart('container3', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_3
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_3)

            }]
        });

        // Build the chart
        Highcharts.chart('container4', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_4
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_4)

            }]
        });

         // Build the chart
        Highcharts.chart('container5', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_4
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_s1)

            }]
        });

        // Build the chart
        Highcharts.chart('container6', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_4
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_s2)

            }]
        });

        // Build the chart
        Highcharts.chart('container7', {
            chart: {
                type: 'spline'
            },
            title: {
                text: info.title_7
            },

            subtitle: {
                text: info.subtitle
            },

            yAxis: {
                title: {
                    text: 'Number of Sellers'
                }
            },

            xAxis: {
                categories: ["18 to 23", "24 to 29", "30 to 35", "36 to 41", "42 to 47", "48 to 53", "54 to 59", "60 to 65","66 and above",]
            },

            plotOptions: {
                spline: {
                    dataLabels: {
                        enabled:false
                    },
                    enableMouseTracking:true
                }
            },

            series: [{
                name: 'Sellers Age Range',
                data: JSON.parse(info.series_7),
                color: "silver"
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });

        // Build the chart
        Highcharts.chart('container8', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_8
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_8)

            }]
        });

        // Build the chart
        Highcharts.chart('container9', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_9
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_9)

            }]
        });

        // Build the chart
        Highcharts.chart('container10', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_10
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_10)

            }]
        });

        // Build the chart
        Highcharts.chart('container11', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: info.title_11
            },
            subtitle: {
                text: info.subtitle
            },
            tooltip: {
                pointFormat: '{series.name}: <br>{point.percentage:.1f} %<br>{point.y} / {point.total}',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.name}: <br>{point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Count',
                data: JSON.parse(info.series_11)

            }]
        });


    }
    
    $('#advanceSearch').submit(function(e){
        e.preventDefault();
        var form_values = $('#advanceSearch').serialize();
        filter(form_values);
    });
    
    function filter(values){
        var values = values ? values: 'ajax:1';
        $.ajax({
            url: base_url + 'dashboard/sellers_demography',
            method:"GET",
            data:values,
            dataType: 'json',
            success:function(data){
                $('#filterModal').modal('hide');
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding:    0,
                        margin:     0,
                        width:      '30%',
                        top:        '40%',
                        left:       '35%',
                        textAlign:  'center',
                        color:      '#000',
                        border:     '3px solid #aaa',
                        backgroundColor:'#fff',
                        cursor:     'wait'
                    },
                });
                setTimeout(function() {
                    $('#g_container').html(data.html);
                    // console.log(JSON.parse(data.data.series));
                    // console.log(data.data.series);

                    highCharts(data.data);

                    $('#daterangepicker').val(data.daterange);


                    KTApp.unblock('#kt_content');
                }, 500);
            }
        })
    }
    return {
        // Init demos
        init: function () {
            filter();

            // demo loading
            var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
            loading.show();

            setTimeout(function () {
                loading.hide();
            }, 3000);
        }
    };
}();

// Class initialization on page load
jQuery(document).ready(function () {
    KTDashboard.init();
    datepicker()
    
});