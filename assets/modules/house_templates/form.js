"use strict";
var HouseTemplate = (function () {
    
    let formRepeater = function () {
        $('#form_repeater').repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width', '100%');
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {

                    $(this).slideUp(deleteElement);
                    $('.select2-container').remove();
                    Select.init();
                    $('.select2-container').css('width', '100%');
                }
            },
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            formRepeater();
        },
    };
})()

jQuery(document).ready(function () {
    HouseTemplate.init();
});
