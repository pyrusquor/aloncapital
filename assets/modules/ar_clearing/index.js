'use strict';
var ArClearing = (function () {

    let tableElement = $('#ar_clearing_table');

    var ArClearingTable = function () {

        var dataTable = tableElement.DataTable({
            order: [[0, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'ar_clearing/showItems',
            },
            dom:
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-check"></i> Clear Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkClear',
                    },
                    enabled: false,
                },
                {
                    text: '<i class="la la-circle"></i> Bounced Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkBounce',
                    },
                    enabled: false,
                },
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'transaction_id',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'buyer_id',
                },
                {
                    data: 'payment_type_id',
                },
                {
                    data: 'amount_paid',
                },
                {
                    data: 'cheque_number',
                },
                {
                    data: 'is_active',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<a href="javascript:void(0);" class="dropdown-item clear_transaction" data-id="` +
                            row.transaction +
                            `"><i class="la la-check"></i> Clear Transaction</a>
                            <a href="javascript:void(0);" class="dropdown-item bounce_transaction" data-id="` +
                            row.id +
                            `"><i class="la la-circle"></i> Bounced Transaction</a>
									</div>
								</span>
								<a href="` +
                            base_url +
                            `ar_clearing/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: 'dt-center',
                    // orderable: false,
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },

                {
                    targets: 6,
                    render: function (data, type, full, meta) {
                        // '1' => 'Cash',
                        // '2' => 'Regular Cheque',
                        // '3' => 'Post Dated Cheque',
                        // '4' => 'Online Deposits',
                        // '5' => 'Wire Transfer',
                        // '6' => 'Cash & Cheque',
                        // '7' => 'Cash & Bank Remittance',
                        // '8' => 'Journal Voucher',
                        // '9' => 'Others',
                        var payment_type = {
                            1: {
                                name: 'Cash',
                            },
                            2: {
                                name: 'Regular Cheque',
                            },
                            3: {
                                name: 'Post Dated Cheque',
                            },
                            4: {
                                name: 'Online Deposits',
                            },
                            5: {
                                name: 'Wire Transfer',
                            },
                            6: {
                                name: 'Cash & Cheque',
                            },
                            7: {
                                name: 'Cash & Bank Remittance',
                            },
                            8: {
                                name: 'Journal Voucher',
                            },
                            9: {
                                name: 'Others',
                            },
                        };

                        if (typeof payment_type[data] === 'undefined') {
                            return ``;
                        }
                        return payment_type[data].name;
                    },
                },
                {
                    targets: 9,
                    render: function (data, type, full, meta) {
                        var status = {
                            1: {
                                name: 'For Clearing',
                            },
                            2: {
                                name: 'Bounced',
                            },
                            3: {
                                name: 'Cleared',
                            },
                        };

                        if (typeof status[data] === 'undefined') {
                            return ``;
                        }
                        return status[data].name;
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_ar_clearing', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'ar_clearing/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#ar_clearing_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var confirmClear = function () {
        $(document).on('click', '.clear_transaction', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, I want to clear this transaction!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'ar_clearing/clear_transaction/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#ar_clearing_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Cleared!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var confirmBounce = function () {
        $(document).on('click', '.bounce_transaction', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, I want to bounce this transaction!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'ar_clearing/bounce_transaction/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#ar_clearing_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Bounced!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on('click', '#btn_upload_guide', function () {
            var table = $('#upload_guide_table');

            table.DataTable({
                order: [[0, 'asc']],
                pagingType: 'full_numbers',
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + 'ar_clearing/get_table_schema',
                columns: [
                    // {data: 'button'},
                    {
                        data: 'no',
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'type',
                    },
                    {
                        data: 'format',
                    },
                    {
                        data: 'option',
                    },
                    {
                        data: 'required',
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $('#ar_clearing_table').DataTable();

        $('#_search').on('keyup', function () {
            dTable.search($(this).val()).draw();
        });

        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $('#ar_clearing_table').DataTable();
        var _buttons = _table.buttons([
            '.bulkDelete',
            '.bulkBounce',
            'bulkClear',
        ]);

        $('#select-all').on('click', function () {
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
                _buttons.buttons().enable();
            } else {
                $('.delete_check').prop('checked', false);
                _buttons.buttons().disable();
            }
        });

        $('#ar_clearing_table tbody').on(
            'change',
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $('input#select-all').get(0);

                    if (el && el.checked && 'indeterminate' in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var ids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                ids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (ids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'ar_clearing/bulkActions',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                ids_arr: ids_arr,
                                type: 'delete',
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#ar_clearing_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });

        $('#bulkClear').click(function () {
            var ids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                ids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (ids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, clear all!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'ar_clearing/bulkActions',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                ids_arr: ids_arr,
                                type: 'clear',
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#ar_clearing_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Cleared!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
        $('#bulkBounce').click(function () {
            var ids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                ids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (ids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, bounce all!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'ar_clearing/bulkActions',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                ids_arr: ids_arr,
                                type: 'bounced',
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#ar_clearing_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Cleared!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    var get_ar_clearing_summary = function () {
        
        $.ajax({
            url: base_url + "ar_clearing/get_ar_clearing_summary",
            type: "POST",
            dataType: "JSON",
            data : {e:'please'},
            success: function (res) {
                $("#for_clearing_count").html(res.for_clearing_count);
                $("#for_clearing_amount").html(res.for_clearing_amount);
                $("#cleared_count").html(res.cleared_count);
                $("#cleared_amount").html(res.cleared_amount);
                $("#bounced_count").html(res.bounced_count);
                $("#bounced_amount").html(res.bounced_amount);
            },
        });

    };

    return {
        //main function to initiate the module
        init: function () {
            confirmBounce();
            confirmDelete();
            ArClearingTable();
            _add_ons();
            filter();
            upload_guide();
            status();
            _selectProp();
            confirmClear();
            get_ar_clearing_summary();
        },
    };
})();

jQuery(document).ready(function () {
    ArClearing.init();
});
