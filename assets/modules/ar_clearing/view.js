var confirmClear = function () {
    $(document).on('click', '.clear_transaction', function () {
        var id = $(this).data('id');

        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, I want to clear this transaction!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'ar_clearing/clear_transaction/' + id,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: id,
                    },
                    success: function (res) {
                        if (res.status) {
                            $('#ar_clearing_table').DataTable().ajax.reload();
                            swal.fire('Cleared!', res.message, 'success');
                        } else {
                            swal.fire('Oops!', res.message, 'error');
                        }
                    },
                });
            } else if (result.dismiss === 'cancel') {
                swal.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                );
            }
        });
    });
};

var confirmBounce = function () {
    $(document).on('click', '.bounce_transaction', function () {
        var id = $(this).data('id');

        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, I want to bounce this transaction!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'ar_clearing/bounce_transaction/' + id,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: id,
                    },
                    success: function (res) {
                        if (res.status) {
                            $('#ar_clearing_table').DataTable().ajax.reload();
                            swal.fire('Bounced!', res.message, 'success');
                        } else {
                            swal.fire('Oops!', res.message, 'error');
                        }
                    },
                });
            } else if (result.dismiss === 'cancel') {
                swal.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                );
            }
        });
    });
};

confirmClear();
confirmBounce();
