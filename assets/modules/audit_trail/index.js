"use strict";
var AuditTrail = (function () {

    let tableElement = $('#audit_trail_table');

    var AuditTrailTable = function () {

        var dataTable = tableElement.DataTable({
            order: [
                [1, "desc"]
            ],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'audit_trail/showItems',
            },
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                text: '<i class="la la-trash"></i> Delete Selected',
                className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm",
                attr: {
                    id: "bulkDelete",
                },
                enabled: false,
            }, ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },

            columns: [{
                    data: null,
                },
                {
                    data: "id",
                },
                /* ==================== begin: Add model fields ==================== */
                {
                    data: "affected_id",
                },
                {
                    data: "affected_table",
                },
                {
                    data: "action",
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                /* ==================== end: Add model fields ==================== */
                {
                    data: "Actions",
                    responsivePriority: -1,
                },
            ],
            columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `<a href="` +
                            base_url +
                            `audit_trail/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: "dt-center",
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                // {
                //     targets: 5,
                //     render: function (data, type, full, meta) {
                //         const f_name = full.user.first_name;
                //         const l_name = full.user.last_name;

                //         const full_name = `${f_name} ${l_name}`;

                //         return full_name;
                //     },
                // },
                // {
                //     targets: 6,
                //     render: function (data, type, full, meta) {
                //         const d = new Date(data);
                //         const months = [
                //             "January",
                //             "February",
                //             "March",
                //             "April",
                //             "May",
                //             "June",
                //             "July",
                //             "August",
                //             "September",
                //             "October",
                //             "November",
                //             "December",
                //         ];
                //         const month = months[d.getMonth()];
                //         const day = d.getDate();
                //         const year = d.getFullYear();

                //         return `${month} ${day}, ${year}`;
                //     },
                // },
                // {
                //     targets: 5,
                //     visible: false,
                // },
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on("click", ".remove_audit_trail", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "audit_trail/delete/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $("#audit_trail_table")
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on("click", "#btn_upload_guide", function () {
            var table = $("#upload_guide_table");

            table.DataTable({
                order: [
                    [0, "asc"]
                ],
                pagingType: "full_numbers",
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + "audit_trail/get_table_schema",
                columns: [
                    // {data: 'button'},
                    {
                        data: "no",
                    },
                    {
                        data: "name",
                    },
                    {
                        data: "type",
                    },
                    {
                        data: "format",
                    },
                    {
                        data: "option",
                    },
                    {
                        data: "required",
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $("#audit_trail_table").DataTable();

        $("#_search").on("keyup", function () {
            dTable.search($(this).val()).draw();
        });

        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $("#audit_trail_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#audit_trail_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "audit_trail/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#audit_trail_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    // Private functions
    var datepicker = function () {
        // minimum setup
        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
        $(".kt_datepicker").datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: "yyyy-mm-dd",
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            AuditTrailTable();
            _add_ons();
            filter();
            upload_guide();
            status();
            _selectProp();
            datepicker();
        },
    };
})();

jQuery(document).ready(function () {
    AuditTrail.init();
});