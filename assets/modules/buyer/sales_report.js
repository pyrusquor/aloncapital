'use strict';
var SalesReport = (function (){
    var salesreportTable = function () {
        var table = $('#buyer_sales_report_table');
        // begin first table
        table.DataTable({
            order: [
                [1, 'desc']
            ],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,
            ajax: {
                url:base_url + "buyer/sales_report",
                data: function(data){
                   // Read values
                   var company = $('#company_id').val();
                   var project = $('#project_id').val();
                   var house_model = $('#house_model_id').val();
         
                   // Append to data
                   data.company = company;
                   data.project = project;
                   data.house_model = house_model;
                }
             },
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                buttons: [
                    {
                        extend: 'print',
                        title: 'Buyers List',
                        customize: function ( win ) {
                            var project = $('#project_id').val();
                            var company = $('#company_id').val();
                            var house_model = $('#house_model_id').val();
                            var text = '<h5>';

                            if(company.length>0){
                                text = text + '<strong>Company:</strong> ' + $('#select2-company_id-container').attr('title') + '</small>' + '&nbsp; &nbsp; &nbsp;';
                            }
                            if(project.length>0){
                                text = text + '<strong>Project:</strong> ' + $('#select2-project_id-container').attr('title') + '&nbsp; &nbsp; &nbsp;';
                            }
                            if(house_model.length>0){
                                text = text + '<strong>House Model:</strong> ' + $('#select2-house_model_id-container').attr('title') + '</small>' + '&nbsp; &nbsp; &nbsp;';
                            }
                            text = text + '</h5>'
         
                            $(win.document.body).find( 'h1' )
                                .html( 'BUYERS LIST' + text );
                            $(win.document.body).css( '<style>@media print{html, body { height: auto; }#page-wrapper{ margin: 0 0 0 0; border-left:#FFF; padding: 0 10px; min-height:auto;}.navbar-static-side { margin: 0 0 0 0; margin-top: 10px;}}</style>');
                        }
                    }
                ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },
            columns: [
                {
                    data: "buyer",
                },
                {
                    data: "reference",
                },
                {
                    data: "property",
                },
                {
                    data: "house_model",
                },
                {
                    data: "reservation_date",
                },
				{
                    data: "total_collectible_price",
                }
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            },
        });
    };

    var filters = function (){
        $('#company_id, #project_id, #house_model_id').on('change', function(){
            var table = $('#buyer_sales_report_table');
            var dtable = table.DataTable();
            dtable.draw();
        })
    }

    return {
        //main function to initiate the module
        init: function () {
            salesreportTable();
            filters();
        },
    };
})();

$(document).ready( function (){
    SalesReport.init();
})