// Class definition
var KTFormControls = (function () {
    // Private functions

    var valLoginHistory = function () {
        $("#login_history_form").validate({
            // define validation rules
            rules: {},

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                toastr.error(
                    "Please check your fields",
                    "Something went wrong"
                );

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });
    };

    return {
        // public functions
        init: function () {
            valLoginHistory();
        },
    };
})();

var KTBootstrapDatepicker = (function () {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
    };

    return {
        // public functions
        init: function () {
            datepicker();
        },
    };
})();

jQuery(document).ready(function () {
    KTFormControls.init();
    KTBootstrapDatepicker.init();
});
