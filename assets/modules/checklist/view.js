'use strict';
var KTChecklist = (function () {
    var _viewChecklist = function () {
        $('#_documents, #_document_checklists').sortable({
            connectWith: '.connectedSortable',
            opacity: 0.8,
        });
    };

    var _updateDocumentChecklist = function () {
        $('#_update_document_checklist').on('click', function () {
            var checklist_id = $('#_document_checklists').data('id'),
                _docx_ids = $('#_document_checklists > ._document_checklist')
                    .map(function () {
                        return this.id;
                    })
                    .get();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: base_url + 'checklist/update_document_checklist',
                data: {
                    ids: _docx_ids,
                    id: checklist_id,
                },
                beforeSend: function () {
                    $('#_update_document_checklist').attr('disabled', true);

                    KTApp.blockPage({
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: 'Processing...',
                    });
                },
                success: function (_response) {
                    $('#_update_document_checklist').removeAttr('disabled');

                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        newestOnTop: true,
                        progressBar: false,
                        positionClass: 'toast-top-right',
                        preventDuplicates: true,
                        onclick: null,
                        showDuration: '300',
                        hideDuration: '1000',
                        timeOut: '5000',
                        extendedTimeOut: '1000',
                        showEasing: 'swing',
                        hideEasing: 'linear',
                        showMethod: 'fadeIn',
                        hideMethod: 'fadeOut',
                    };

                    if (_response._status === 1) {
                        toastr.success(_response._msg, 'Success');
                    } else if (_response._status === 2) {
                        toastr.warning(_response._msg, 'Success');
                    } else {
                        toastr.error(_response._msg, 'Failed');
                    }

                    KTApp.unblockPage();
                },
            });
        });
    };

    return {
        init: function () {
            _viewChecklist();
            _updateDocumentChecklist();
        },
    };
})();

jQuery(document).ready(function () {
    KTChecklist.init();
});
