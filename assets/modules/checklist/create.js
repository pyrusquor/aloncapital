"use strict";
var ChecklistCreate = function () {

	var _form_validate = function () {

		$('#_checklist_template').validate({

			rules: {
				name: {
					required: true,
					minlength: 2
				},
				category_id: {
					required: true
				},
				description: {
					minlength: 2
				}
			},
			messages: {
				name: {
					required: 'The Checklist Name field is required',
					minlength: 'The Checklist Name field must be at least 2 characters in length'
				},
				category_id: 'The Category Name field is required',
				description: 'The Checklist Description field must be at least 2 characters in length'
			},
			invalidHandler: function ( event, validator ) {

				event.preventDefault();

				var alert   =   $('#form_msg');
				alert.closest('div.form-group').removeClass('kt-hide').show();
				KTUtil.scrollTop();
				
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-right",
				  "preventDuplicates": false,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				};

				toastr.error("Please check your fields", "Something went wrong");
			},
			submitHandler: function ( _frm ) {

				_frm[0].submit();
			}
		});
	}

	var _add_ons = function () {

		$('div.invalid-feedback').each( function () {

    	var _this = $(this);

    	_this.closest('div.form-group').addClass('is-invalid').find('.form-control').addClass('is-invalid');

    	$('#form_msg').closest('div.form-group').removeClass('kt-hide');
    });
	}

	return {

		init: function () {

			_form_validate();
			_add_ons();
		}
	};
}();

jQuery(document).ready(function() {

    ChecklistCreate.init();
});