"use strict";

var KTDatatablesLandInventoryProcessChecklist = function () {

	var _doc_id = '';

	var _initLandInventoryProcessChecklist = function () {

		var _table = $('#_land_inventory_document_table');

		_table.DataTable({
			order: [[0, 'desc']],
			pagingType: 'full_numbers',
			lengthMenu: [5, 10, 25, 50, 100],
			pageLength : 10,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			deferRender: true,
			dom:	"<'row'<'col-sm-12 col-md-6'l>>" +
						"<'row'<'col-sm-12'tr>>" +
						"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			language: {
				'lengthMenu': 'Show _MENU_',
        'infoFiltered': '(filtered from _MAX_ total records)'
			},
			ajax: base_url + 'checklist/get_process',
			columns: [
				// {data: 'button'},
				{data: 'id'},
				{data: 'name'},
				{data: 'owner_id'},
				{
					data: null,
					orderable: false,
					searchable: false,
					render: function ( data, type, row, meta ) {

						return `
	                  <div class="kt-subheader__wrapper">
											<button class="btn btn-linkedin btn-sm btn-elevate btn-icon _upload_file" data-toggle="modal" data-target="#_upload_modal" data-id="`+row.id+`">
												<i class="fa fa-pen"></i>
											</button>
											<button class="btn btn-linkedin btn-sm btn-elevate btn-icon _view_file" id="_view_file_`+row.id+`" data-toggle="modal" data-target="#_view_file_modal" data-id="`+row.id+`">
												<i class="la la-eye"></i>
											</button>
										</div>`;
					}
				},
				{
					data: null,
					orderable: false,
					searchable: false,
					render: function ( data, type, row, meta ) {

						return ``;
					}
				},
				{
					data: null,
					orderable: false,
					searchable: false,
					render: function ( data, type, row, meta ) {

						return `N/A`;
					}
				},
				{
					data: null,
					orderable: false,
					searchable: false,
					render: function ( data, type, row, meta ) {

						return `N/A`;
					}
				},
				{data: 'created_at'},
				{data: 'Actions', responsivePriority: -1}
			],
			columnDefs: [
				{
					targets: -1,
					orderable: false,
					render: function ( data, type, row, meta ) {

						return `
                    <a href="`+base_url+`document/view/`+row.id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                      <i class="la la-eye"></i>
                    </a>
                  `;
					}
				},
				{
					targets: [0, 2, 3, 4, 5, 6, 7, -1],
					className: 'dt-center',
				},
				{
					targets: 2,
					render: function (data, type, full, meta) {

						var _owner = {
							1: { title: 'Legal Staff' },
							2: { title: 'Legal Head' },
							3: { title: 'Agent' },
							4: { title: 'Sales Admin' },
							5: { title: 'AR Staff' },
							6: { title: 'AR Head' },
							7: { title: 'Titling Officer' },
							8: { title: 'Tax' },
							9: { title: 'Engineering' },
							10: { title: 'Cashier' },
							11: { title: 'Sales Coordinator' },
							12: { title: 'Client' },
							13: { title: 'Credit and Collection' }
						};

						if ( typeof _owner[data] === 'undefined' ) {

							return ``;
						}

						return _owner[data].title;
					}
				},
				{
					targets: 4,
					render: function (data, type, full, meta) {

						var _stats = {
							1: { title: 'For Research' },
							2: { title: 'For Negotiation' },
							3: { title: 'For Purchase' },
							4: { title: 'Purchased - For Transfer of Title' },
							5: { title: 'Purchased - Under BOD and Other Name' },
							6: { title: 'Purchased - Title Transferred' },
							7: { title: 'Sold' },
							8: { title: 'Transferred' },
							9: { title: 'Subdivided' },
							10: { title: 'Consolidated' },
							11: { title: 'Joint Venture' },
							12: { title: 'New' },
							13: { title: 'Converted to Project' },
							14: { title: 'Cancelled' },
							15: { title: 'Consolidated and Subdivided' },
						};

						if ( typeof _stats[data] === 'undefined' ) {

							return ``;
						}

						return _stats[data].title;
					}
				}
			],
			drawCallback: function ( settings ) {

				$('span#_total').text(settings.fnRecordsTotal()+' TOTAL');
			}
		});
	}

	var _add_ons = function () {

		$('#_search').on( 'keyup', function () {

			$('#_land_inventory_document_table').DataTable().search($(this).val()).draw();
		});

		$('#kt_datepicker').datepicker({
    	orientation: "bottom left",
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

		$('button#_advance_search_btn').on( 'click', function () {

			$('div#_batch_upload').removeClass('show');
		});

		$('button#_batch_upload_btn').on( 'click', function () {

			$('div#_advance_search').removeClass('show');
		});

		$(document).on( 'click', 'button._upload_file', function () {

			var _id = $(this).data('id');

			_doc_id = _id;
		});

		$('.modal').on('hidden.bs.modal', function(){

	    $(this).find('form')[0].reset();

	    $('form').find('.kt-avatar__holder').css('background-image', 'url('+base_url+'assets/img/default/_img.png'+')');
		});
	}

	var _uploadProcess = function () {

		$(document).on( 'submit', 'form#_upload_form', function ( e ) {

			e.preventDefault();

			var docx_id = _doc_id;
			var file  = $('#process_checklist_file')[0].files[0];

			var _formData = new FormData();
			_formData.append('_id', docx_id);
			_formData.append('_file', file);

			$.ajax({
				type: 'post',
				dataType: 'json',
				url: base_url+'checklist/process_upload',
				data: _formData,
				cache:false,
				contentType: false,
				processData: false,
				mimeType: 'multipart/form-data',
				beforeSend: function () {

					$('#_upload_modal').removeClass('show');

					KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Processing...'
	        });
				},
				success: function ( _respo ) {

					$('#_upload_modal').addClass('show');

					toastr.options = {
					  "closeButton": true,
					  "debug": false,
					  "newestOnTop": true,
					  "progressBar": false,
					  "positionClass": "toast-top-right",
					  "preventDuplicates": true,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					}

					if ( _respo._status === 1 ) {

						// $('button#_view_file_'+docx_id).removeAttr('disabled');

						toastr.success(_respo._msg, "Success");
					} else {

						toastr.error(_respo._msg, "Failed");
					}

					KTApp.unblockPage();
				}
			}).fail ( function () {

				KTApp.unblockPage();

				swal.fire(
					'Oops!',
					'Please refresh the page and try again.',
					'error'
				);
			});
		});
	}

	var _response = function () {

		var _loader = '<div class="kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--success"></div>';

		$(document).on( 'click', '._view_file', function () {

			var id = $(this).data('id');

			if ( id ) {

				$('._display_process_document_image').attr('id', '_display_process_document_image_'+id);

				$('#_display_process_document_image_'+id).html(_loader);

				$.ajax({
					type: 'post',
					dataType: 'json',
					url: base_url+'document/view_document_file',
					data: { id: id },
					success: function ( _respo ) {

						if ( _respo._status ) {

							$('#_display_process_document_image_'+id).html(_respo._html);
						} else {

							swal.fire(
								'Oops!',
								_respo._msg,
								'error'
							);
						}
					}
				});
				// .fail ( function () {

				// 	$('#_display_process_document_image_'+id).html(_loader);
				// });
			}
		});
	}

	var _filterProcessChecklist = function () {

		var dTable = $('#_land_inventory_document_table').DataTable();

		function _colFilter ( n ) {

			dTable.column(n).search($('#_column_'+n).val()).draw();
		}

		$('._filter').on( 'keyup change clear', function () {

			_colFilter($(this).data('column'));
		});
	}

	return {

		init: function () {

			_initLandInventoryProcessChecklist();
			_add_ons();
			_uploadProcess();
			_response();
			_filterProcessChecklist();

			new KTAvatar('kt_apps_user_add_avatar');
		}
	};
}();

jQuery(document).ready(function() {

    KTDatatablesLandInventoryProcessChecklist.init();
});