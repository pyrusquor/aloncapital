var app = new Vue({
    el: '#purchase_order_app',
    data: {
        object_id: null,
        notes: {
            loading: false,
            data: [],
            form: {
                content: ""
            },
            selected: {}
        }
    },
    watch: {},
    methods: {
        fetchNotes() {
            this.notes.loading = true;
            let url = base_url + "note/get_by_parent?parent_type=purchase_orders&parent_type_id=" + this.object_id;
            axios.get(url)
                .then(response => {
                    if(response.data){
                        this.notes.data = response.data;
                    }
                    this.notes.loading = false;
                });
        },
        addNote() {
            $("#note_modal").modal('show');
        },
        saveNote() {
            this.notes.loading = true;
            $("#note_modal").modal('hide');
            let data = {
                request: this.notes.form
            };
            data.request.object_type = "purchase_orders";
            data.request.object_type_id = this.object_id;

            let url = base_url + "note/process_create";
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.status) {
                        app.fetchNotes();
                        swal.fire({
                            title: "Success!",
                            text: "Note saved!",
                            type: "success",
                        });
                    }
                }
            })
        }
    },
    mounted() {
        this.object_id = window.object_id;
        this.fetchNotes();
    }
});