$(document).ready(function () {
    var datepicker = function () {
        // minimum setup
        $('#filter_request_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function (e) {
            app.changeFilter('request_date', $('#filter_request_date').val());
        });

    };

    datepicker();

    $("#company_id").on("change", function (e) {
        app.changeInfo('company_id', $(this).val());
    });

    $("#accounting_ledger_id").on("change", function (e) {
        app.changeInfo('accounting_ledger_id', $(this).val());
    });

    $("#warehouse_id").on("change", function (e) {
        app.changeInfo('warehouse_id', $(this).val());
    });

    $("#requesting_staff_id").on("change", function (e) {
        app.changeInfo('requesting_staff_id', $(this).val());
    });

    $("#approving_staff_id").on("change", function (e) {
        app.changeInfo('approving_staff_id', $(this).val());
    });

    $("#filter_company_id").on("change", function (e) {
        app.changeFilter('company_id', $(this).val());
    });

    $("#filter_accounting_ledger_id").on("change", function (e) {
        app.changeFilter('accounting_ledger_id', $(this).val());
    });


});

const __rpo_filter_defaults = {
    reference: null,
    request_date: null,
    company_id: null,
    // accounting_ledger_id: null
}

const __info_form_defaults = {
    id: null,
    company_id: null,
    warehouse_id: null,
    // accounting_ledger_id: null,
    status: 1,
    purchase_order_request_id: null
}

var app = new Vue({
    el: '#purchase_order_app',
    data: {
        object_request_id: null,
        object_status: 1,
        object_is_editable: 1,
        canvass_id: null,
        rpo: {
            filter: {},
            data: [],
            items: [],
            canvass_items: [],
            canvass_items_filter: {
                with_canvass: "yes",
                purchase_order_request_id: null,
                supplier_id: null
            },
            selected: null,
        },
        info: {
            form: {},
            required: {
                'company_id': 'Company',
                'approving_staff_id': 'Approving Staff',
                'requesting_staff_id': 'Requesting Staff',
                'warehouse_id': 'Warehouse',
                // 'accounting_ledger_id': 'Expense Account',
                'purchase_order_request_id': 'Purchase Request',
                'supplier_id': "Supplier"
            },
            check: {
                is_valid: false,
                missing_fields: []
            }
        },
        items: {
            form: [],
            data: [],
            selected: null,
            required: {},
            check: {
                is_valid: false,
            },
            deleted_items: []
        },
        sources: {},
        has_exceeded: false
    },
    watch: {},
    methods: {
        isEditable() {
            return this.object_is_editable == 1;
        },
        // region init
        initObjectRequest() {
            this.object_request_id = window.object_request_id;
            this.object_status = window.object_status;
            this.object_is_editable = window.is_editable;

            if (window.rpo) {
                this.fetchRPO(window.rpo);
            }
            if (window.canvass_id) {
                this.canvass_id = window.canvass_id;
            }

            this.fetchObjectRequest();
        },
        // endregion

        // region items form
        addItemToPurchaseOrder(key) {
            let item = this.rpo.items[key];

            if (item.quantity) {

                this.addItemToCart(true, item);
            } else {

                swal.fire({
                    title: "Can't Add",
                    text: "Quantity: " + item.quantity,
                    type: "error",
                })
            }
        },

        removeItemFromPurchaseOrder(key) {
            let item = this.items.data[key];
            if (item.id) {
                this.items.deleted_items.push(item.id);
            }
            this.items.data.splice(key, 1);
            this.calculateTotal();
        },

        addItemToCart(isNew, data) {
            let item = {};

            if (isNew) {
                for (key in data) {
                    item[key] = data[key];
                }
                item.supplier_id = this.info.form.supplier_id;
                let unit_cost = item.item.unit_price;
                if (item.supplier_id) {
                    unit_cost = this.searchSupplierUnitCost(item.id, item.supplier_id);
                    if (unit_cost) {
                        item.unit_cost = unit_cost;
                    }
                }
                item.total_cost = item.unit_cost * item.quantity;
                item.purchase_order_request_item_id = item.id;
                delete item['id'];
                delete item['created_by'];
                delete item['created_at'];
                delete item['updated_at'];
                delete item['updated_by'];
                delete item['deleted_by'];
                delete item['deleted_at'];

            } else {
                for (key in data) {
                    item[key] = data[key];
                }
            }

            if (item.id) {
                let idx = this.searchItemInStore(item.id);
                if (idx !== null) {
                    this.items.data[idx] = item;
                } else {
                    this.items.data.push(item);
                }
            } else {
                this.items.data.push(item);
            }
            this.$forceUpdate();

            this.calculateTotal();

            this.resetItemForm();
        },

        updateSubTotal(key) {
            this.items.data[key].total_cost = this.items.data[key].unit_cost * this.items.data[key].quantity;
            this.$forceUpdate();

        },

        calculateTotal() {
            let total = 0;
            for (let i in this.items.data) {
                // let subtotal = this.items.data[i].unit_cost * this.items.data[i].quantity;
                total += this.items.data[i].total_cost * 1.0;
            }
            this.info.form.total = total;
            this.$forceUpdate();
        },

        searchItemInStore(id) {
            for (let i = 0; i < this.items.data.length; i++) {
                if (id == this.items.data[i].id) {
                    return i;
                }
            }
            return null;
        },

        resetItemForm() {
            this.items.selected = null;
            this.items.loaded = null;
            this.items.form = {};
        },

        checkItemsForm() {
            this.items.check.is_valid = true;
            if (this.items.data.length <= 0) {
                this.items.check.is_valid = false;
            }
        },

        prepItemsFormData() {
            return this.items.data;
        },
        // endregion

        // region rpo filters
        selectItemForCanvass(idx) {
            this.items.selected = this.rpo.items[idx];
        },

        resetRPOFilter() {
            this.rpo.data = [];
            this.rpo.items = [];
            this.rpo.selected = null;
            this.info.form.purchase_order_request_id = null;
            this.items.data = [];
            this.canvass_id = null;
            $('#filter_request_date').val(null);
            $("#filter_company_id").val(null);
            // $("#filter_accounting_ledger_id").val(null);
            for (var d in __rpo_filter_defaults) {
                this.rpo.filter[d] = __rpo_filter_defaults[d];
            }
        },

        changeFilter(key, val) {
            this.rpo.filter[key] = val;
        },

        filterRPO() {
            let params = $.param(this.rpo.filter);
            let url = base_url + "purchase_order_request/search?with_relations=yes&" + params;
            axios.get(url)
                .then(response => {
                    this.rpo.data = response.data;
                })
        },

        fetchRPO(id) {
            let url = base_url + "purchase_order_request/search?with_relations=yes&id=" + id;
            axios.get(url)
                .then(response => {
                    this.rpo.data = response.data;
                    this.selectRPO(0);
                    this.fetchItemsWithCanvass();
                })
        },

        selectRPO(key) {
            this.rpo.selected = this.rpo.data[key];
            this.info.form.purchase_order_request_id = this.rpo.selected.id;
            this.info.form.warehouse_id = this.rpo.selected.warehouse_id;
            // this.info.form.accounting_ledger_id = this.rpo.selected.accounting_ledger_id;
            this.info.form.company_id = this.rpo.selected.company_id;
            this.rpo.canvass_items_filter.purchase_order_request_id = this.rpo.selected.id;
        },
        // endregion

        // region parsers
        searchSupplierUnitCost(item_id, supplier_id) {
            let item = null;
            for (let i = 0; i < this.rpo.items.length; i++) {
                if (this.rpo.items[i].id === item_id) {
                    item = this.rpo.items[i];
                    break;
                }
            }
            if (item) {
                let canvass = item.canvass;
                for (let i = 0; i < canvass.length; i++) {
                    if (canvass[i].supplier_id === supplier_id) {
                        return canvass[i].unit_cost;
                    }
                }
            }
            return false;
        },

        parseObjectRequest(data) {
            if (data.length > 0) {
                this.info.form = data[0];
                this.fetchObjectRequestChildren();
                this.fetchRPO(this.info.form.purchase_order_request_id);

                // if (this.object_request_id) {
                //     this.rpo.filter = window.purchase_order_request;
                //     this.filterRPO();
                // }
            }
        },

        parseObjectRequestItems(data) {
            for (row in data) {
                this.addItemToCart(false, data[row]);
            }
        },

        // endregion

        // region fetchers
        lookupInject(table, field, val, ret_field) {
            let url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + val;
            let retval = null;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        if (ret_field) {
                            retval = response.data[0][ret_field];
                        } else {
                            retval = response.data;
                        }
                    }
                    return retval;
                })
        },

        fetchObjectRequest() {
            if (this.object_request_id) {
                let url = base_url + "purchase_order/search?id=" + this.object_request_id + "&with_relations=no";
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectRequest(response.data);


                        }
                    });
            } else {
                this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
            }
            // this.request_items.cart = JSON.parse(JSON.stringify(__request_items_data_defaults));
        },
        fetchObjectRequestChildren() {
            // let url = base_url + "generic_api/fetch_specific?table=purchase_order_items&field=purchase_order_id&value=" + this.object_request_id;
            let url = base_url + "purchase_order_item/get_all_by_po/" + this.object_request_id;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.parseObjectRequestItems(response.data);
                    }
                });
        },
        async fetchInfoObjects(table, field, value, store, order) {
            if (!order) {
                order = "name ASC";
            }
            let url = base_url + "generic_api/fetch_all?table=" + table + "&order=" + order;
            if (field && value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + value + "&order=" + order;
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        //this.info.data[store] = response.data;
                        this.$set(this.info.data, store, response.data);
                    } else {
                        this.$set(this.info.data, store, []);
                    }
                });
        },
        fetchCanvassItems(rpo_id) {
            let params = $.param(this.rpo.canvass_items_filter);
            let url = base_url + "canvassing_item/search?" + params;
            axios.get(url)
                .then(response => {
                    this.$set(this.rpo, "canvass_items", response.data);
                    this.$forceUpdate();
                });
        },

        updateCanvassFilter() {
            this.rpo.canvass_items_filter.purchase_order_request_id = this.rpo.selected.id;
            this.rpo.canvass_items_filter.supplier_id = this.info.form.supplier_id;
        },

        fetchItemsWithCanvass() {
            let rpo_id = this.rpo.selected.id;
            let params = $.param(this.rpo.canvass_items_filter);
            let url = base_url + "purchase_order_request/get_items?" + params + "&with_current_quantity=true";

            axios.get(url)
                .then(response => {
                    this.$set(this.rpo, "items", response.data);
                    this.$forceUpdate();
                })
        },

        async fetchItemObjects(table, cat, store, value, fk, order) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            let order_by = null;
            if (order) {
                order_by = "&order=" + order;
            }
            if (value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + fk + "&value=" + value + order_by;
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        if (cat === 'rpo') {
                            this.$set(this.rpo, store, response.data);
                        }
                    } else {
                        if (cat === 'rpo') {
                            this.$set(this.rpo.data, store, []);
                        }
                    }
                    this.$forceUpdate();
                });
        },
        fetchObjects(table, lookup_field) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        let data = response.data
                        this.sources[table] = data.filter(function(item) {
                            return item.is_blacklisted == "0"
                        });
                        console.log(this.sources[table])
                    } else {
                        this.sources[table] = [];
                    }
                })
        },
        // endregion

        // region info form
        changeInfo(key, val) {
            this.info.form[key] = val;
        },

        prepInfoFormData() {
            return this.info.form;
        },

        checkInfoForm() {
            this.info.check.missing_fields = [];
            this.info.check.is_valid = true;
            for (var key in this.info.required) {
                if (!this.info.form[key]) {
                    this.info.check.missing_fields.push(this.info.required[key]);
                    this.info.check.is_valid = false;
                }
            }
        },

        confirmSubmitRequest() {

            let items = this.prepItemsFormData()
            
            let request = items.reduce((promiseChain, item) => {

                return promiseChain.then(() => new Promise((resolve) => {

                    this._requestPurchaseOrderRequestItem(item, resolve)
                }))
            }, Promise.resolve())

            request.then(() => {
                
                if (!this.has_exceeded) {

                    this.submitRequest()
                }
            })
        },

        async _requestPurchaseOrderRequestItem(item, resolve) {

            return await axios.post(base_url + "purchase_order_request_item/get_item/" + item.purchase_order_request_item_id)
            .then(response => {
                
                if (response.data) {
                    
                    if (parseInt(item.quantity) > parseInt(response.data.quantity)) {

                        this.has_exceeded = true

                        swal.fire({
                            title: "Oooops!",
                            text: item.item_name + " has exceeded the allowed quantity.",
                            type: "error",
                        }).then(() => {

                            // console.log("asd")
                        });   
                    } else {

                        this.has_exceeded = false
                        resolve()
                    }
                } else {

                    this.has_exceeded = true
                }
            })
        },

        submitRequest() {
            this.checkInfoForm();
            let canvass_id = this.canvass_id;
            let message = '';
            if (!this.info.check.is_valid) {
                message = 'Missing fields: ' + this.info.check.missing_fields.join(', ');
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            this.checkItemsForm();
            if (!this.items.check.is_valid) {
                message = 'Please add Canvass items';
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            let url = base_url + "purchase_order/process_create";
            if (this.object_request_id) {
                url = base_url + "purchase_order/process_update/" + this.object_request_id;
            }

            let data = {
                request: this.prepInfoFormData(),
                items: this.prepItemsFormData(),
                deleted_items: this.items.deleted_items
            }

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.status) {
                        if(canvass_id){
                            var status_data = {
                                table: "canvassings",
                                id: canvass_id,
                                field: "status",
                                value: "2",
                                actor: null
                            }
                            console.log(status_data);
                            $.ajax({
                                url: base_url + "generic_api/authorized_update",
                                type: "POST",
                                dataType: "JSON",
                                data: status_data
                            });
                        }
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                base_url + "purchase_order"
                            );
                        });
                    }
                }
            })
        },

        prepInfoData() {

        },

        prepObjectRequestItems() {

        },

        searchField(fields, field) {
            for (key in fields) {
                if (field === key) {
                    return true;
                }
            }
            return false;
        },
        // endregion

        changePrice(key) {

            swal.fire({
                title: 'Enter new price',
                input: 'number',
                inputAttributes: {
                  autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (input) => {

                    if (!input) {

                        swal.fire({
                            title: "Error!",
                            text: "Invalid value",
                            type: "error",
                        })

                        return false
                    }

                    this.rpo.items[key].unit_cost = input

                    this.rpo.items[key].total_cost = this.rpo.items[key].unit_cost * this.rpo.items[key].quantity
                },
                allowOutsideClick: () => !swal.isLoading()
            })
        },
    },
    mounted() {

        this.initObjectRequest();

        this.is_mounted = true;
        this.object_request_id = window.object_request_id;

        // this.resetRPOFilter();
        this.fetchObjects('suppliers');

        this.info.form.requesting_staff_id = window.requesting_staff_id
    }
});