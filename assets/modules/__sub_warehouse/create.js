// Class definition
var KTFormControls = (function () {
    // Private functions

    var valSubWarehouse = function () {
        $("#sub_warehouse_form").validate({
            // define validation rules
            rules: {
                name: {
                    required: true,
                },
                sub_warehouse_code: {
                    required: true,
                },
                warehouse: {
                    required: true,
                },
                address: {
                    required: true,
                },
                telephone_number: {
                    required: true,
                },
                mobile_number: {
                    required: true,
                },
                email_address: {
                    required: true,
                },
                contact_person: {
                    required: true,
                },
                is_active: {
                    required: true,
                },
                /* ==================== begin: Add model fields ==================== */

                /* ==================== end: Add model fields ==================== */
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                toastr.error(
                    "Please check your fields",
                    "Something went wrong"
                );

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });
    };

    return {
        // public functions
        init: function () {
            valSubWarehouse();
        },
    };
})();

var KTBootstrapDatepicker = (function () {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
    };

    return {
        // public functions
        init: function () {
            datepicker();
        },
    };
})();

jQuery(document).ready(function () {
    KTFormControls.init();
    KTBootstrapDatepicker.init();
});