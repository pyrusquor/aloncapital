const __info_form_defaults = {
    id: null,
    name: null,
    description: null
}

const __construction_template_items_data_defaults = {
    id: null,
    item_group_id: null,
    item_type_id: null,
    item_brand_id: null,
    item_class_id: null,
    item_id: null,
    unit_of_measurement_id: null,
    quantity: 0,
    unit_cost: 0.0,
    total_cost: 0.0,
    stage_id: null
};

let __construction_template_data_fields = {};

const __info_form_required_defaults = [
    'name'
];

const __construction_template_items_required_defaults = [
    'item_group_id',
    'item_type_id',
    'item_brand_id',
    'item_class_id',
    'item_id',
    'unit_of_measurement_id',
    'quantity',
    'stage_id',
    'unit_cost',
    'total_cost',
];

const __store_key_assoc = {
    'item_group_id': {
        store: 'item_groups',
        obj_name: 'item_group'
    },
    'item_type_id': {
        store: 'item_types',
        obj_name: 'item_type'
    },
    'item_brand_id': {
        store: 'item_brands',
        obj_name: 'item_brand'
    },
    'item_class_id': {
        store: 'item_classes',
        obj_name: 'item_class'
    },
    'item_id': {
        store: 'items',
        obj_name: 'item'
    },
    'unit_of_measurement_id': {
        store: 'units_of_measurement',
        obj_name: 'unit_of_measurement'
    },
    'stage_id': {
        store: 'stages',
        obj_name: 'stage'
    },
}
Vue.config.devtools = true
var app = new Vue({
    el: '#construction_template_app',
    data: {
        is_mounted: false,
        object_construction_template_id: null,
        wizard: {
            current: 1,
        },
        info: {
            form: {},
            data: {},
            required_fields: [],
            form_is_valid: false,
            total_cost: 0.0,
        },
        construction_template_items: {
            cart: {},
            data: {
                items: [],
            },
            form: [],
            deleted_items: [],
            cart_is_valid: false,
            form_is_valid: false,
            active: null
        },
    },
    watch: {
        "construction_template_items.active": function (newVal, oldVal) {
            if (newVal === null) {
                this.construction_template_items.cart = JSON.parse(JSON.stringify(__construction_template_items_data_defaults));
            }
        },
        "construction_template_items.cart": {
            handler: function (newVal, oldVal) {
                this.checkItemsCart();
            }, deep: true
        },
        "construction_template_items.form": {
            handler: function (newVal, oldVal) {
                this.calculateTotalCost();
                this.checkItemsForm();
            }, deep: true
        },
        "construction_template_items.cart.item_group_id": function (newVal, oldVal) {
            if (newVal) {
                this.fetchItemObjects("item_type", "item_types", newVal, "group_id");
                this.construction_template_items.data.item_classes = [];
                this.construction_template_items.data.items = [];
            } else {
                this.construction_template_items.data.item_types = [];
            }
            this.resetCart('item_group_id');
        },
        "construction_template_items.cart.item_type_id": function (newVal, oldVal) {
            if (newVal) {
                this.fetchItemObjects("item_class", "item_classes", newVal, "item_type_id");
                this.construction_template_items.data.items = [];
            } else {
                this.construction_template_items.data.item_classes = [];
            }
            this.resetCart();
        },
        "construction_template_items.cart.item_class_id": function (newVal, oldVal) {
            if (newVal) {
                this.fetchItems();
            }
        },
        "construction_template_items.cart.item_brand_id": function (newVal, oldVal) {
            if(newVal){
                this.fetchItems();
            }
        },
        "construction_template_items.cart.item_id": function (newVal, oldVal) {
            if (newVal) {
                let item = this.searchItem(newVal);
                if (item) {
                    this.construction_template_items.cart.unit_cost = item.unit_price;
                }
            }else{
                this.construction_template_items.cart.unit_cost = 0.0;
                this.construction_template_items.cart.quantity = 0;
            }
        },
        "construction_template_items.cart.quantity": function (newVal, oldVal) {
            if (newVal && newVal > 0) {
                if (this.construction_template_items.cart.unit_cost > 0) {
                    this.construction_template_items.cart.total_cost = this.construction_template_items.cart.unit_cost * this.construction_template_items.cart.quantity;
                }
            } else {
                this.construction_template_items.cart.total_cost = 0.0;
            }
        },
        "info.form.name": function (newVal, oldVal) {
            this.checkInfoForm();
        }
    },
    methods: {
        wizardSwitchPane(n) {
            if (!this.info.form_is_valid) {
                swal.fire({
					"title": "", 
					"text": "There are some errors in your submission. Please correct them.", 
					"type": "error",
					"confirmButtonClass": "btn btn-secondary"
				});
            } else {
                this.wizard.current = n;
            }
        },
        wizardLabelState(n) {
            if (n === this.wizard.current) {
                return "current";
            } else {
                return "";
            }
        },
        wizardStepStateClass(n) {
            if (n === this.wizard.current) {
                return "kt-wizard-v3__content active";
            } else {
                return "kt-wizard-v3__content";
            }
        },

        // init
        initObjectConstructionTemplate() {
            this.object_construction_template_id = window.object_construction_template_id;
            this.fetchObjectConstructionTemplate();
        },

        // cart
        addItemToConstructionTemplate(source) {
            let form = {};
            let data = {};
            switch (source) {
                case 'cart':
                    data = this.construction_template_items.cart;
                    break;
            }
            if (this.construction_template_items.active !== null) {
                form = data;
                this.construction_template_items.form[this.construction_template_items.active] = form;
            } else {
                for (let i in data) {
                    let key = this.searchItemKey(i);
                    let obj = null;
                    if (key) {
                        if (source === 'cart') {
                            form[key.obj_name] = this.searchItemRef(data[i], key.store);
                            form[i] = data[i];
                        } else {
                            // traverse object
                        }
                    } else {
                        form[i] = data[i];
                    }
                }
                this.construction_template_items.form.push(form);
            }
            this.construction_template_items.active = null;
            this.construction_template_items.cart = JSON.parse(JSON.stringify(__construction_template_items_data_defaults));
            this.$forceUpdate();

            console.log(this.construction_template_items.form.length)
        },
        calculateTotalCost() {
            let total = 0.0;
            for (i in this.construction_template_items.form) {
                total += (this.construction_template_items.form[i].total_cost * 1.0);
            }
            this.info.form.construction_template_amount = total;
        },
        loadItem(key) {
            let data = this.construction_template_items.form[key];
            this.$set(this.construction_template_items, "cart", data);
            this.construction_template_items.active = key;
            this.$forceUpdate();
        },
        removeItem(key) {
            let item = this.construction_template_items.form[key];
            if (item.id) {
                this.construction_template_items.deleted_items.push(item.id);
            }
            this.construction_template_items.form.splice(key, 1);
        },
        // parsers
        parseObjectConstructionTemplate(data) {
            if (data.length > 0) {
                this.info.form = data[0];
                this.fetchObjectConstructionTemplateChildren();
            }
        },
        parseObjectConstructionTemplateItems(data) {
            if (data.length > 0) {
                this.construction_template_items.form = data;
            }
        },
        // search
        searchItemRef(id, store) {
            for (let i = 0; i < this.construction_template_items.data[store].length; i++) {
                if (this.construction_template_items.data[store][i].id === id) {
                    return this.construction_template_items.data[store][i]
                }
            }
            return null;
        },
        searchItemKey(key) {
            for (i in __store_key_assoc) {
                if (key === i) {
                    return __store_key_assoc[i];
                }
            }
            return false;
        },
        searchItem(id) {
            for (let i = 0; i < this.construction_template_items.data.items.length; i++) {
                if (this.construction_template_items.data.items[i].id === id) {
                    return this.construction_template_items.data.items[i]
                }
            }
            return null;
        },
        checkInfoForm() {
            let fields_complete = true;
            for (let i = 0; i < this.info.required_fields.length; i++) {
                let req_field = this.info.form[this.info.required_fields[i]];
                if (!req_field) {
                    fields_complete = false;
                }
            }
            this.info.form_is_valid = fields_complete;
        },
        checkItemsForm() {
            this.construction_template_items.form_is_valid = this.construction_template_items.form.length > 0;
        },
        checkItemsCart() {
            let valid = true;
            for (i in __construction_template_items_required_defaults) {
                let key = __construction_template_items_required_defaults[i];
                if(!this.construction_template_items.cart[key]){
                    valid = false;
                }
            }
            this.construction_template_items.cart_is_valid = valid;
        },
        // fetchers
        fetchObjectConstructionTemplate() {
            if (this.object_construction_template_id) {
                let url = base_url + "construction_template/search?id=" + this.object_construction_template_id;
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectConstructionTemplate(response.data);
                            this.fetchObjectConstructionTemplateChildren();
                        }
                    });
            } else {
                this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
            }
            this.construction_template_items.cart = JSON.parse(JSON.stringify(__construction_template_items_data_defaults));
        },
        fetchObjectConstructionTemplateChildren() {
            let url = base_url + "construction_template_items/get_all_by_construction_template/" + this.object_construction_template_id;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.parseObjectConstructionTemplateItems(response.data);
                    }
                });
        },
        async fetchInfoObjects(table, field, value, store, order) {
            if (!order) {
                order = "name ASC";
            }
            let url = base_url + "generic_api/fetch_all?table=" + table + "&order=" + order;
            if (field && value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + value + "&order=" + order;
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        //this.info.data[store] = response.data;
                        this.$set(this.info.data, store, response.data);
                    } else {
                        this.$set(this.info.data, store, []);
                    }
                });
        },
        async fetchItemObjects(table, store, value, fk, dd=0) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            if (value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + fk + "&value=" + value + "&order=name ASC";
            }
            if(dd){
                this.$set(this.construction_template_items.data, store, stages);
                this.$forceUpdate();
            }else{
                await axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.$set(this.construction_template_items.data, store, response.data);
                        this.$forceUpdate();
                    } else {
                        this.$set(this.construction_template_items.data, store, []);
                    }
                });
            }

        },
        async fetchItems() {
            var params = {};
            let store = "items";
            if (this.construction_template_items.cart.item_brand_id) {
                params["brand_id"] = this.construction_template_items.cart.item_brand_id;
            }
            if (this.construction_template_items.cart.item_type_id) {
                params["type_id"] = this.construction_template_items.cart.item_type_id;
            }

            let url = base_url + "item/get_all_items";
            if (Object.keys(params).length > 0) {
                url = url + "?" + new URLSearchParams(params).toString();
            }

            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.$set(this.construction_template_items.data, store, response.data.data);
                        if(! response.data.data){
                            this.construction_template_items.cart.item_id = null;
                        }
                    } else {
                        this.$set(this.construction_template_items.data, store, []);
                    }
                    this.$forceUpdate();
                })
        },

        // form
        submitConstructionTemplate() {
            let info_data = this.prepInfoData();
            let items_data = this.prepObjectConstructionTemplateItems();
            let data = {
                construction_template: info_data,
                items: items_data,
                deleted_items: this.construction_template_items.deleted_items
            }

            let url = base_url + "construction_template/form";

            if (this.construsction_template_id) {
                url = base_url + "construction_template/form/" + this.construsction_template_id;
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                success: function (response) {
                    if(response.status){
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                base_url + "construction_template"
                            );
                        });
                    }
                }
            })

        },

        prepInfoData() {
            let form = {};
            for (key in this.info.form) {
                if (this.searchField(__construction_template_data_fields, key)) {
                    if (this.info.form[key]) {
                        form[key] = this.info.form[key];
                    } else {
                        form[key] = null;
                    }
                }
            }
            return form;
        },

        prepObjectConstructionTemplateItems() {
            let items_data = [];
            for (i in this.construction_template_items.form) {
                let item_data = {};
                for (key in this.construction_template_items.form[i]) {
                    if (this.searchField(__construction_template_items_data_defaults, key)) {
                        if (this.construction_template_items.form[i][key]) {
                            item_data[key] = this.construction_template_items.form[i][key];
                        } else {
                            item_data[key] = null;
                        }
                    }
                }
                items_data.push(item_data);
            }
            return items_data;
        },

        searchField(fields, field) {
            for (key in fields) {
                if (field === key) {
                    return true;
                }
            }
            return false;
        },

        resetCart(from) {

            if (from == 'item_group_id') {

                this.construction_template_items.cart.item_type_id = null;
            }

            this.construction_template_items.cart.id = null;
            this.construction_template_items.cart.item_brand_id = null;
            this.construction_template_items.cart.item_class_id = null;
            this.construction_template_items.cart.item_id = null;
            this.construction_template_items.cart.unit_of_measurement_id = null;
            this.construction_template_items.cart.quantity = 0;
            this.construction_template_items.cart.unit_cost = 0.0;
            this.construction_template_items.cart.total_cost = 0.0;
            this.construction_template_items.cart.stage_id = null;
        },
    },
    mounted() {

        this.fetchItemObjects("item_group", "item_groups", "");
        this.fetchItemObjects("item_brand", "item_brands", "");
        // this.fetchItemObjects("item_class", "item_classes", "");
        this.fetchItemObjects("inventory_settings_unit_of_measurements", "units_of_measurement", "");
        this.fetchItemObjects("stage", "stages", "", "", "1");
        this.initObjectConstructionTemplate();

        this.is_mounted = true;

        this.construsction_template_id = window.object_construction_template_id;
        
        __construction_template_data_fields = window.object_construction_template_fillables;

        this.info.required_fields = JSON.parse(JSON.stringify(__info_form_required_defaults));

        this.checkItemsForm();
    }
});