// Class definition
var Create = (function () {
    var formRepeater = function () {
      $("#form_construction_template").repeater({
        initEmpty: true,
  
        defaultValues: {},
  
        show: function () {
          $(this).slideDown();
          datepicker();
        },
  
        hide: function (deleteElement) {
          $(this).slideUp(deleteElement);
        },
      });
    };
  
    var construction_templateFormRepeater = function () {
      $("#construction_template_item_form_repeater").repeater({
        initEmpty: false,
  
        defaultValues: {},
  
        show: function () {
          $(this).slideDown();
          datepicker();
        },
  
        hide: function (deleteElement) {
          $(this).slideUp(deleteElement);
        },
      });
    };
  
    var arrows;
    if (KTUtil.isRTL()) {
      arrows = {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>',
      };
    } else {
      arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      };
    }
  
    // Private functions
    var datepicker = function () {
      // minimum setup
      $(".kt_datepicker").datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows,
        locale: "no",
        format: "yyyy-mm-dd",
        autoclose: true,
      });
  
      $(".yearPicker").datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
      });
    };
  
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var _add_ons;
  
    // Private functions
    var initWizard = function () {
      // Initialize form wizard
      wizard = new KTWizard("kt_wizard_v3", {
        startStep: 1,
      });
  
      // Validation before going to next page
      wizard.on("beforeNext", function (wizardObj) {
        if (validator.form() !== true) {
          wizardObj.stop(); // don't go to the next step
        }
      });
  
      wizard.on("beforePrev", function (wizardObj) {
        if (validator.form() !== true) {
          wizardObj.stop(); // don't go to the next step
        }
      });
  
      // Change event
      wizard.on("change", function (wizard) {
        KTUtil.scrollTop();
      });
    };
  
    var initValidation = function () {
      validator = formEl.validate({
        // Validate only visible fields
        ignore: ":hidden",
  
        // Validation rules
        rules: {
            "name": {
                required: true
            },
        },
        // messages: {
        // 	"info[last_name]":'Last name field is required',
        // },
        // Display error
        invalidHandler: function (event, validator) {
          KTUtil.scrollTop();
  
          swal.fire({
            title: "",
            text:
              "There are some errors in your submission. Please correct them.",
            type: "error",
            confirmButtonClass: "btn btn-secondary",
          });
        },
  
        // Submit valid form
        submitHandler: function (form) {},
      });
    };
  
    var initSubmit = function () {
      var btn = formEl.find('[data-ktwizard-type="action-submit"]');
  
      btn.on("click", function (e) {
        e.preventDefault();
        let paidAmount = $("#total_due_amount").val();
        let dTotalInput = $("#dTotal").val();
        let cTotalInput = $("#cTotal").val();
  
        if (dTotalInput == cTotalInput) {
          if (paidAmount == dTotalInput || paidAmount == cTotalInput) {
            if (validator.form()) {
              // See: src\js\framework\base\app.js
  
              KTApp.progress(btn);
              //KTApp.block(formEl);
  
              // See: http://malsup.com/jquery/form/#ajaxSubmit
              formEl.ajaxSubmit({
                type: "POST",
                dataType: "JSON",
                success: function (response) {
                  if (response.status) {
                    swal
                      .fire({
                        title: "Success!",
                        text: response.message,
                        type: "success",
                      })
                      .then(function () {
                        window.location.replace(base_url + "construction_template");
                      });
                  } else {
                    swal.fire({
                      title: "Oops!",
                      html: response.message,
                      icon: "error",
                    });
                  }
                },
              });
            }
          } else {
            swal.fire({
              title: "Error!",
              html:
                "The paid amount should be equal to the Debit or Credit total.",
              icon: "error",
            });
          }
        } else {
          swal.fire({
            title: "Error!",
            html: "Please make sure that Debit and credit amount is balanced.",
            icon: "error",
          });
        }
      });
    };
  
    var _add_ons = function () {
      datepicker();
    };
    // Public functions
    return {
      init: function () {
        datepicker();
        formRepeater();
        construction_templateFormRepeater();
        _add_ons();
  
        wizardEl = KTUtil.get("kt_wizard_v3");
        formEl = $("#form_construction_template");
  
        initWizard();
        initValidation();
        initSubmit();
      },
    };
  })();
  
  // Initialization
  jQuery(document).ready(function () {
    Create.init();
  });  

  const item_select = document.querySelector('#item');

  $(document).on('change', '#item_group_id, #item_type_id, #item_brand_id, #item_class_id', function() {
    let item_group_id = $('#item_group_id').val();
    let item_type_id = $('#item_type_id').val();
    let item_brand_id = $('#item_brand_id').val();
    let item_class_id = $('#item_class_id').val();
    $.ajax({
        url: base_url + 'item/item_query',
        type: 'POST',
        data: {
            item_group_id   :   item_group_id,
            item_type_id    :   item_type_id,
            item_brand_id   :   item_brand_id,
            item_class_id   :   item_class_id,
        },
        success: function(data) {
            item_select.innerHTML = '';
            const items = JSON.parse(data);

            try{
                items.map((item) => {
                    let opt = document.createElement('option');

                    opt.value = item.id;
                    opt.innerHTML = item.name;
                    opt.setAttribute('data-code', item.code);
                    opt.setAttribute('data-total_price', item.total_price);
                    opt.setAttribute('data-name', item.name);
                    item_select.appendChild(opt);
                    $('#amenity').val(item.total_price);
                })
            } catch (error) {
                let opt = document.createElement('option');
                    item_select.innerHTML = '';
                    opt.value = '';
                    opt.innerHTML = 'No record found';
                    item_select.appendChild(opt);
            }
        }
    })
  });

  $(document).on('click', '#add_entry_btn', function () {
    var item = $('#item');
    var item_name = $(item).find(':selected').attr('data-name');
    var item_code = $(item).find(':selected').attr('data-code');
    var item_price = $(item).find(':selected').attr('data-total_price');

    var items = $('.item_name');
    var code = $('.item_code');
    var quantity = $('.quantity');
    var amenity = $('.amenity');
    var sub_amenity = $('.sub_amenity');
    var quantity_val = $('#quantity').val();
    var sub_amenity_val = $('#sub_amenity').val();

    items.each(function(i, v) {
      var is_last_item = (i == (items.length - 1));
      if(is_last_item) {
        $(items[i]).val(item_name);
        $(code[i]).val(item_code);
        $(quantity[i]).val(quantity_val);
        $(amenity[i]).val(item_price);
        $(sub_amenity[i]).val(sub_amenity_val);
      }
    })
  })

  $(document).on('keyup', '#quantity', function() {
    computeTotalCost();
  })

  $(document).on('click', '#item', function() {
    var item = $('#item');
    $('#amenity').val($(item).find(':selected').attr('data-total_price'));
    computeTotalCost();
  })

  function computeTotalCost() {
    var quantity = $('#quantity').val() !== '' ? $('#quantity').val() : 1;
    var amount = 0;
    var amenity = $('#amenity').val();
    var sub_amenity = $('#sub_amenity');
    
    amount = parseFloat(amenity) * parseFloat(quantity);

    $(sub_amenity).val(amount);
  }