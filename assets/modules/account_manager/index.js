function AccountManagerPagination(page_num) {
    page_num = page_num ? page_num : 0;

    var keyword = $('#generalSearch').val();

    const project_id = $('#project_id').val();
    const personnel_id = $('#personnel_id').val();
    const collection_date = $('#collection_date').val();

    $.ajax({
        url: base_url + 'account_manager/paginationData/' + page_num,
        method: 'POST',
        data:
            'page=' +
            page_num +
            '&keyword=' +
            keyword +
            '&project_id=' +
            project_id +
            '&personnel_id=' +
            personnel_id +
            '&collection_date=' +
            collection_date,
        success: function (html) {
            KTApp.block('#kt_content', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Processing...',
                css: {
                    padding: 0,
                    margin: 0,
                    width: '30%',
                    top: '40%',
                    left: '35%',
                    textAlign: 'center',
                    color: '#000',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait',
                },
            });

            setTimeout(function () {
                $('#accountManagerContent').html(html);
                initializeLinks();
                var title = $('#collection_title');
                if (collection_date == 'due_today') {
                    $(title).html('Due Today');
                } else if (collection_date == 'overdue_collections') {
                    $(title).html('Overdue Collections');
                } else if (collection_date == 'upcoming_collections') {
                    $(title).html('Upcoming Collections');
                } else {
                    $(title).html('Overdue Collections');
                }

                KTApp.unblock('#kt_content');
            }, 500);
        },
    });
}

function PDCPagination(page_num) {
    console.log('clicked')
    page_num = page_num ? page_num : 0;

    var keyword = $('#generalSearch').val();

    const project_id = $('#project_id').val();
    const personnel_id = $('#personnel_id').val();
    const collection_date = $('#collection_date').val();

    $.ajax({
        url: base_url + 'account_manager/paginationDataPDC/' + page_num,
        method: 'POST',
        data:
            'page=' +
            page_num +
            '&keyword=' +
            keyword +
            '&project_id=' +
            project_id +
            '&personnel_id=' +
            personnel_id +
            '&collection_date=' +
            collection_date,
        success: function (html) {
            KTApp.block('#kt_content', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Processing...',
                css: {
                    padding: 0,
                    margin: 0,
                    width: '30%',
                    top: '40%',
                    left: '35%',
                    textAlign: 'center',
                    color: '#000',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait',
                },
            });

            setTimeout(function () {
                $('#accountManagerContent').html(html);
                initializeLinks();
                var title = $('#collection_title');
                if (collection_date == 'due_today') {
                    $(title).html('Due Today');
                } else if (collection_date == 'overdue_collections') {
                    $(title).html('Overdue Collections');
                } else if (collection_date == 'upcoming_collections') {
                    $(title).html('Upcoming Collections');
                } else if (collection_date == 'for_clearing_today') {
                    $(title).html('For Clearing Today');
                } else if (collection_date == 'overdue_checks') {
                    $(title).html('Overdue Checks');
                } else if (collection_date == 'upcoming_clearing') {
                    $(title).html('Upcoming Clearing');
                } else {
                    $(title).html('Overdue Collections');
                }

                KTApp.unblock('#kt_content');
            }, 500);
        },
    });
}

const getAM = () => {
    // Get the element again
    const amSelect = document.querySelectorAll('#personnel_id');
    // Loop through the element
    amSelect.forEach((select) => {
        $.ajax({
            url: base_url + 'account_manager/get_all_am',
            type: 'GET',
            success: function (data) {
                const am_res = JSON.parse(data);
                am_res.data.map((am) => {
                    let opt = document.createElement('option');

                    opt.value = am.id;

                    var fullName = `${am.first_name} ${am.middle_name} ${am.last_name}`;
                    opt.innerHTML = fullName;
                    select.appendChild(opt);
                });
            },
        });
    });
};

const getProjects = () => {
    // Get the element again
    const projectSelect = document.querySelectorAll('#project_id');
    // Loop through the element
    projectSelect.forEach((select) => {
        $.ajax({
            url: base_url + 'account_manager/get_all_projects',
            type: 'GET',
            success: function (data) {
                const projects = JSON.parse(data);
                projects.data.map((project) => {
                    let opt = document.createElement('option');

                    opt.value = project.id;
                    opt.innerHTML = project.name;
                    select.appendChild(opt);
                });
            },
        });
    });
};

$('#project_id, #personnel_id').on('change', function () {
    AccountManagerPagination();
});

const initializeLinks = () => {
    $('#due_today').on('click', function () {
        var collectDateEl = $('#collection_date');

        $(collectDateEl).val(''); // Reset Value

        $(collectDateEl).val('due_today');
        AccountManagerPagination();
    });

    $('#overdue_collections').on('click', function () {
        var collectDateEl = $('#collection_date');

        $(collectDateEl).val(''); // Reset Value

        $(collectDateEl).val('overdue_collections');
        AccountManagerPagination();
    });

    $('#upcoming_collections').on('click', function () {
        var collectDateEl = $('#collection_date');

        $(collectDateEl).val(''); // Reset Value

        $(collectDateEl).val('upcoming_collections');
        AccountManagerPagination();
    });

    // PDC
    $('#for_clearing_today').on('click', function () {
        var collectDateEl = $('#collection_date');

        $(collectDateEl).val(''); // Reset Value

        $(collectDateEl).val('for_clearing_today');
        PDCPagination();
    });

    $('#overdue_checks').on('click', function () {
        var collectDateEl = $('#collection_date');

        $(collectDateEl).val(''); // Reset Value

        $(collectDateEl).val('overdue_checks');
        PDCPagination();
    });

    $('#upcoming_clearing').on('click', function () {
        var collectDateEl = $('#collection_date');

        $(collectDateEl).val(''); // Reset Value

        $(collectDateEl).val('upcoming_clearing');
        PDCPagination();
    });
};

initializeLinks();
getAM();
getProjects();
