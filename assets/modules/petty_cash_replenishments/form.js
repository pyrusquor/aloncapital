// Class definition
let PettyCashReplenishmentForm = function () {
    // Base elements
    let formEl, validator, arrows

    // Entry Items
    let dTotalInput, cTotalInput, grossAmount
    var addEntryBtn;

    let datepicker = function () {

        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    };

    let initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        wizard.on('beforePrev', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    };

    let initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                "amount": {
                    required: true
                },
                "entry_item[0][dc]": {
                    required: true,
                },
                "entry_item[0][ledger_id]": {
                    required: true,
                },
                "entry_item[0][amount]": {
                    required: true,
                },
            },
            // messages: {
            // 	"info[last_name]":'Last name field is required',
            // },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {

            }
        });
    };

    let initSubmit = function () {
        let btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on("click", function (e) {
            e.preventDefault();
            let amount = parseFloat($("#amount").val());
            let dTotalInput = parseFloat($("#dTotal").val());
            let cTotalInput = parseFloat($("#cTotal").val());

            if (dTotalInput == cTotalInput || window.is_update) {
                if (amount == dTotalInput || amount == cTotalInput || window.is_update) {

                    if (validator.form()) {
                        // See: src\js\framework\base\app.js

                        KTApp.progress(btn);
                        //KTApp.block(formEl);

                        // See: http://malsup.com/jquery/form/#ajaxSubmit
                        formEl.ajaxSubmit({
                            type: "POST",
                            dataType: "JSON",
                            success: function (response) {
                                if (response.status) {
                                    swal.fire({
                                        title: "Success!",
                                        text: response.message,
                                        type: "success",
                                    }).then(function () {
                                        window.location.replace(
                                            base_url + "petty_cash_replenishments"
                                        );
                                    });
                                } else {
                                    swal.fire({
                                        title: "Oops!",
                                        html: response.message,
                                        icon: "error",
                                    });
                                }
                            },
                        });
                    }
                } else {
                    swal.fire({
                        title: "Error!",
                        html: "The paid amount should be equal to the Debit or Credit total.",
                        icon: "error",
                    });
                }
            } else {
                swal.fire({
                    title: "Error!",
                    html: "Please make sure that Debit and credit amount is balanced.",
                    icon: "error",
                });
            }
        });
    };

    let add_ons = function () {

        formEl = $('#petty_cash_replenishment_form');

        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            }
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }

        dTotalInput = document.querySelector("#dTotal");
        cTotalInput = document.querySelector("#cTotal");
        grossAmount = document.querySelector("#gross_amount");
        addEntryBtn = document.querySelector("#next_btn");

        addEntryBtn.addEventListener("click", function () {
            const amountEl = document.querySelectorAll("#item_total_amount");
            totalOfItemAmount(amountEl);
        })
    };

    let payeeTypeListener = () => {

        $("#payee_type_id").on('change', () => {

            let value = $("#payee_type_id").val() != '0' ? $("#payee_type_id").val() : '5'

            if (value == '1' || value == '4') {
                $("#payee_id").attr('data-type', 'payee')
            } else {
                $("#payee_id").attr('data-type', 'person')
            }

            $("#payee_id").html('<option value="">Select Payee</option>')

            $("#payee_id").attr('data-module', window.payee_types[value].toLowerCase())
        })
    }

    // Entry Items
    let entryFormRepeater = function () {
        $("#entry_item_form_repeater").repeater({

            initEmpty: false,
            defaultValues: {},

            show: function () {
                $(this).slideDown();
                getLedgers();
                paymentType();
                calculateDTotal();
                calculateCTotal();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width', '100%');
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
        });
    };

    const getItems = () => {
        // Get the element again
        const entryItemLedgerSelect = document.querySelectorAll(
            "#item_id"
        );
        // Loop through the element
        entryItemLedgerSelect.forEach((select) => {
            $.ajax({
                url: base_url + "item/get_all_items",
                type: "GET",
                success: function (data) {
                    const items = JSON.parse(data);
                    items.data.map((item) => {
                        let opt = document.createElement("option");

                        opt.value = item.id;
                        opt.innerHTML = item.name;
                        select.appendChild(opt);
                    });
                },
            });
        });
    };

    const getLedgers = () => {
        // Get the element again
        const entryItemLedgerSelect = document.querySelectorAll(
            "#entry_item_ledger"
        );
        // Loop through the element
        entryItemLedgerSelect.forEach((select) => {
            $.ajax({
                url: base_url + "accounting_entries/get_all_ledgers",
                type: "GET",
                success: function (data) {
                    const ledgers = JSON.parse(data);
                    if (Array.isArray(ledgers)) {
                        ledgers.data.map((ledger) => {
                            let opt = document.createElement("option");

                            opt.value = ledger.id;
                            opt.innerHTML = ledger.name;
                            select.appendChild(opt);
                        });
                    }
                },
            });
        });
    };

    const totalOfItemAmount = (el) => {

        let total = 0;

        el.forEach(item => total += parseFloat($(item).val().trim() || 0));
        if (total != 0) {
            grossAmount.value = total;
        }
    }

    const paymentType = () => {
        const entryItemDcSelect = document.querySelectorAll("#entry_item_dc");
        const dInput = document.querySelectorAll("#dr_amount_input");
        const cInput = document.querySelectorAll("#cr_amount_input");

        entryItemDcSelect.forEach((select, index) => {
            $(select).on("change", function () {
                if (this.value !== "") {
                    if (this.value == "d") {
                        cInput[index].disabled = true;
                        cInput[index].value = 0;
                        dInput[index].disabled = false;
                    } else if (this.value == "c") {
                        cInput[index].disabled = false;
                        dInput[index].disabled = true;
                        dInput[index].value = 0;
                    }
                }
            });
        });
    };

    const calculateDTotal = () => {
        const dInput = document.querySelectorAll("#dr_amount_input");
        // dTotalInput.value = 0;
        $(dInput).keyup(function () {
            let total = 0;

            $(dInput).each(function () {
                total += parseFloat($(this).val().trim() || 0);
            });
            dTotalInput.value = total.toFixed(2);
        });
    };

    const calculateCTotal = () => {
        const cInput = document.querySelectorAll("#cr_amount_input");
        // cTotalInput.value = 0;
        $(cInput).keyup(function () {
            let total = 0;

            $(cInput).each(function () {
                total += parseFloat($(this).val().trim() || 0);
            });
            cTotalInput.value = total.toFixed(2);
        });
    };

    const listItemDetail = () => {
        const listItemSelect = document.querySelectorAll("#item_id");
        const unitPrice = document.querySelectorAll("#item_unit_price");
        const quantity = document.querySelectorAll("#item_quantity");
        const taxID = document.querySelectorAll("#item_tax_id");
        const taxName = document.querySelectorAll("#tax_name");
        const taxAmount = document.querySelectorAll("#item_tax_amount");
        const totalAmount = document.querySelectorAll("#item_total_amount");

        listItemSelect.forEach((select, index) => {
            $(select).on("change", function () {
                const item = getSelectedItem(this.value);
                unitPrice[index].value = item['unit_price'];
                taxID[index].value = item['tax_id'];
                taxName[index].value = item['tax']['name'];
                taxAmount[index].value = item['total_price'] - item['unit_price'];
                totalAmount[index].value = item['total_price'];
            })
        })
    };

    const calculateItemTotalPrice = () => {
        const listItemSelect = document.querySelectorAll("#item_quantity");
        const unitPrice = document.querySelectorAll("#item_unit_price");
        const taxAmount = document.querySelectorAll("#item_tax_amount");
        const totalAmount = document.querySelectorAll("#item_total_amount");

        listItemSelect.forEach((select, index) => {
            $(select).on("change keyup blur", function () {
                const total = (parseFloat(unitPrice[index].value) + parseFloat(taxAmount[index].value)) * this.value;
                totalAmount[index].value = total.toFixed(2);
            })
        })
    };

    function getSelectedItem(itemID) {
        $.ajax({
            url: base_url + 'item/Item/get_item',
            type: 'post',
            data: {
                itemID,
                itemID
            },
            dataType: 'json',
            complete: function () {

            },
            success: function (json) {
                item = json;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            },
            timeout: 10000,
            async: false
        });

        return item;
    }

    return {
        init: function () {
            add_ons();
            datepicker();
            entryFormRepeater();
            initWizard();
            initValidation();
            initSubmit();
            payeeTypeListener()

            // Entry Items
            listItemDetail();
            calculateItemTotalPrice();
            getItems();
            getLedgers();
            paymentType();
            calculateDTotal();
            calculateCTotal();
        }
    };
}();

// Initialization
jQuery(document).ready(function () {
    PettyCashReplenishmentForm.init();
});