var Form = function () {
    var validator;
    var formEl;
    var initValidation = function () {
        validator = formEl.validate({
            rules: {
                'first_name': {
                      required: true
                  },
                'last_name': {
                      required: true
                  },
                'laborer_type': {
                      required: true
                  },
                },
            ignore: ":hidden",
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                });
            },

            submitHandler: function (form) {

            }
        });
    };
    var initSubmit = function (){
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');
        var id = $('#id').val();
        btn.on("click", function (e) {
            e.preventDefault();
            if (validator.form()) {
                KTApp.progress(btn);
                formEl.ajaxSubmit({
                    type: "POST",
                    dataType: "JSON",
                    url: base_url + 'laborer/form/'+id,
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success",
                            }).then(function () {
                                window.location.replace(
                                    base_url + "laborer"
                                );
                            });
                        } else {
                            swal.fire({
                                title: "Oops!",
                                html: response.message,
                                icon: "error",
                            });
                        }
                    },
                });
            }
        })
    }
    return{
        init: function () {
            formEl = $('#form_laborers');
            initValidation();
            initSubmit();
        }
    }
}();
jQuery(document).ready(function () {
    Form.init();
});