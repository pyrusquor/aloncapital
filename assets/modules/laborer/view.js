var Laborer = (function () {
    var removeLaborer = function (){
        $(document).on('click', '.remove_laborer', function () {
            var delete_id = $(this).data('id');
            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'laborer/delete/',
                        type: 'POST',
                        data: {
                            deleteids_arr : [delete_id]
                        },
                        dataType: 'JSON',
                        success: function (res){
                            if(res.status){
                                if (res.status) {
                                    swal.fire("Deleted!", res.message, "success");
                                    window.location = base_url + 'laborer'
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            }
                        }
                    })
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            })
        })
    };
    return{
        init: function (){
            removeLaborer();
        }
    }
})();

jQuery(document).ready(function () {
    Laborer.init();
});