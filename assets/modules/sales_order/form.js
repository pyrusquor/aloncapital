// Class definition
var SalesOrder = (function () {
    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true,
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
    };

    var initializeCustomerType = function () {
        var customerTypeEl = $('#customer_type');

        $(customerTypeEl).on('change', function () {
            var customerTypeValue = $('#customer_type option:selected').val();

            if (
                customerTypeValue == 'sellers' ||
                customerTypeValue == 'buyers' ||
                customerTypeValue == 'staff'
            ) {
                document
                    .getElementById('customer_id')
                    .setAttribute('data-type', 'person');
            } else {
                document
                    .getElementById('customer_id')
                    .setAttribute('data-type', 'customer');
            }
            document
                .getElementById('customer_id')
                .setAttribute('data-module', customerTypeValue);
        });
    };

    var formRepeater = function () {
        $('#sales_order_items_form_repeater').repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                datepicker();
                populateItemInfo();
                populateItemOptions();
                calculateGrandTotal();
                initializeDeleteBtn();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },

            isFirstItemUndeletable: true,
        });
    };

    var populateItemInfo = function () {
        let items_el = $('.populateItems');
        let item_el_index = items_el.length - 1;

        let itemCodeEl = $(
            `input[name="sales_order_items[${item_el_index}][item_code]"]`
        );
        let unitPriceEl = $(
            `input[name="sales_order_items[${item_el_index}][unit_price]"]`
        );
        let quantityEl = $(
            `input[name="sales_order_items[${item_el_index}][quantity]"]`
        );
        let subtotalEl = $(
            `input[name="sales_order_items[${item_el_index}][subtotal]"]`
        );

        calculateItemQuantityByUnitPrice(quantityEl, unitPriceEl, subtotalEl);

        $(items_el).each((idx, el) => {
            $(el).on('change', async function () {
                const item_id = $(this).val();

                if (item_id === '') {
                    $(itemCodeEl).val('');
                    $(unitPriceEl).val('');
                    $(quantityEl).val('');
                    $(subtotalEl).val('');
                    return;
                }

                var { data } = await getItemData(item_id);

                if (idx == item_el_index) {
                    $(itemCodeEl).val(data.code);
                    $(unitPriceEl).val(data.unit_price);
                    $(quantityEl).val(1);
                    $(subtotalEl).val('');
                }
            });
        });
    };

    var getItemData = async function (id) {
        let res = await $.ajax({
            url: base_url + 'sales_order/get_item/' + id,
            type: 'POST',
            data: {
                id: id,
            },
            success: function (res) {
                return res.data;
            },
        });

        return JSON.parse(res);
    };

    var populateItemOptions = async function () {
        let items_el = $('.populateItems');
        let item_el_index = items_el.length - 1;

        // Default option
        var option1 = $(`<option value="">Select Item</option>`);
        $(items_el[item_el_index]).append(option1);

        var { data } = await getAllItems();

        if (data.length > 0) {
            data.map((item, idx) => {
                var option = $(
                    `<option value="${item.id}">${item.name}</option>`
                );

                $(items_el[item_el_index]).append(option);
            });

            calculateGrandTotal();
        }
    };

    var getAllItems = async function () {
        let res = await $.ajax({
            url: base_url + 'sales_order/get_all_item',
            type: 'GET',
            success: function (res) {
                return res.data;
            },
        });

        return JSON.parse(res);
    };

    var calculateItemQuantityByUnitPrice = function (
        quantityEl,
        unitPriceEl,
        subtotalEl
    ) {
        $(quantityEl).on('change', function () {
            let quantityVal = $(this).val();
            let unitPriceCurrentVal = $(unitPriceEl).val();

            let total =
                parseFloat(unitPriceCurrentVal) * parseFloat(quantityVal);

            $(subtotalEl).val(parseFloat(total));
            calculateGrandTotal();
        });
    };

    var calculateGrandTotal = function () {
        let items_el = $('.populateItems');

        let grandtotalEl = $(`#grandtotal`);

        let grandtotal = 0;

        for (let i = 0; i < items_el.length; i++) {
            let val = parseFloat(
                $(`input[name="sales_order_items[${i}][subtotal]"]`).val()
            );

            if (isNaN(val)) {
                val = 0;
            }

            grandtotal += val;
        }

        grandtotalEl.val(grandtotal);
    };

    var initializeDeleteBtn = function () {
        $('.remove_item').each((idx, el) => {
            $(el).on('click', function () {
                setTimeout(() => {
                    calculateGrandTotal();
                }, 500);
            });
        });
    };

    // Public functions
    return {
        init: function () {
            datepicker();
            initializeCustomerType();
            formRepeater();
            populateItemInfo();
            getAllItems();
            populateItemOptions();
        },
    };
})();

// Initialization
jQuery(document).ready(function () {
    SalesOrder.init();
});
