let CashDisbursements = (() => {

    let AccountingEntriesTable = () => {
        var table = $('#accounting_entry_items_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            // ajax: base_url + 'cash_disbursements/showCashDisbursements',
            ajax: {
                url: base_url + 'accounting_entries/showAccountingEntryItems',
                type: 'POST',
                data: {
                    where: {
                        accounting_entry_id: window.accounting_entry_id
                    }
                }
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'dc',
                },
                {
                    data: 'ledger_id',
                },
                {
                    data: 'amount',
                },
                {
                    data: 'description',
                },
                // {
                //     data: 'Actions',
                //     responsivePriority: -1,
                // },
            ],
            columnDefs: [
                // {
                //     targets: -1,
                //     title: 'Actions',
                //     searchable: false,
                //     orderable: false,
                //     render: function (data, type, row, meta) {
                //         return (
                //             `
                // 				<a href="` +
                //             base_url +
                //             `cash_disbursements/view/` +
                //             row.id +
                //             `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" target="_blank">
                // 				<i class="la la-eye"></i>
                // 				</a>
                //             `
                //         );
                //     },
                // },
                {
                    targets: [0, 1, 2, 3, 4],
                    className: 'dt-center',
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };

    return {
        init: () => {
            AccountingEntriesTable()
        }
    }
})()

jQuery(document).ready(() => {
    CashDisbursements.init();
});
