'use strict';
$(document).ready(function () {
    var getMap = function () {
        $.ajax({
            url: base_url + 'map/view_map',
            type: 'GET',

            success: function (data) {
                setTimeout(() => {
                    initializeMap();
                }, 3000);
            },
        });
    };

    getMap();

    var sourceURL = base_url + 'assets/coordinates/json/1.json';

    var initializeMap = function () {
        // Uncomment "source: data," to enable inline JSON
        $('#mapplic').mapplic({
            source: sourceURL,
            // source: mapSource,
            height: 600,
            sidebar: false,
            minimap: false,
            zoombuttons: false,
            fullscreen: false,
            hovertip: true,
            maxscale: 2,
        });
    };
});
