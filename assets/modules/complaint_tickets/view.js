"use strict";

let ComplaintTicket = (() => {

    var image_file_types = ['image/jpeg', 'image/png', 'image/jpg']

    let addOns = () => {
        var loader = '<div class="kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--success"></div>';

        $('.view_file').click(function () {
            let documentId = $(this).data('id')

            console.log(documentId)

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: base_url + 'complaint_tickets/preview_file',
                data: { document_id: documentId },
                success: (response) => {
                    if (response.status) {

                        $('#document_display_container').html(response.content);

                        if (image_file_types.includes(response.ext)) {

                            $("#download").addClass('d-none')
                        } else {

                            $("#download").attr('href', response.src).attr("download", response.name)
                        }
                    } else {

                        swal.fire(
                            'Oops!',
                            response._msg,
                            'error'
                        );
                    }
                }
            });
        })
    }

    return {
        init: () => {
            addOns()
        }
    }
})()

jQuery(document).ready(() => {
    ComplaintTicket.init();
});