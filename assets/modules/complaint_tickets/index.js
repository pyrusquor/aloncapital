"use strict";
var ComplaintTicket = (function () {

    let tableElement = $('#complaint_tickets_table');

    var complaintTable = function () {

        var dataTable = tableElement.DataTable({
            order: [
                [1, 'desc']
            ],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,
			      ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'complaint_tickets/showItems',
            },
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                text: '<i class="la la-trash"></i> Delete Selected',
                className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm",
                attr: {
                    id: "bulkDelete",
                },
                enabled: false,
            }, ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },
            columns: [{
                    data: null,
                },
                {
                    data: "id",
                },
                {
                    data: "reference",
                },
                {
                    data: "project_id",
                },
				{
                    data: "property_id",
                },
                {
                    data: "house_model_id",
                },
                {
                    data: "buyer_id",
                },
                {
                    data: "turn_over_date",
                },
                {
                  data: "warranty_date",
              },
                {
                    data: "inquiry_category_id",
                },
                {
                    data: "department_id",
                },
                {
                    data: "warranty",
                },
                {
                    data: "created_at",
                },
                {
                    data: "updated_at",
                },
                {
                    data: "status",
                },
                {
                  data: "completion_turn_around_time"
                },
                {
                  data: "closing_turn_around_time"
                },
                {
                  data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: "Actions",
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
				{
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                render: function (data, type, row, meta) {
                  var action = (row.status_id == "5") ? `<a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="closed"  data-id="` + row.id + `" data-status="6" onclick="setComplaintTicketID(` + row.id + `,  '` + row.ref + `', 6 )" class="dropdown-item">Closed</a>` : ''
                  return (
                    `
                            <span class="dropdown">

                              <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right">

                              <a href="javascript:void(0);" class="dropdown-item"  data-toggle="modal" data-target="#forwardStatusModal" onclick="setComplaintTicketID(` + row.id + `,  '` + row.ref + `', 2 )">Forward</a>

                              <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="work_in_progress"  data-id="` + row.id + `" data-status="3" onclick="setComplaintTicketID(` + row.id + `,  '` + row.ref + `', 3 )" class="dropdown-item">Work in progress</a>

                              <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="on_hold"  data-id="` + row.id + `" data-status="4" onclick="setComplaintTicketID(` + row.id + `,  '` + row.ref + `', 4 )" class="dropdown-item">On hold</a>

                              <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="completed"  data-id="` + row.id + `" data-status="5" onclick="setComplaintTicketID(` + row.id + `,  '` + row.ref + `', 5 )" class="dropdown-item">Completed</a> ` +

                    action

                    + ` <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="cancel_ticket"  data-id="` + row.id + `" data-status="7" onclick="setComplaintTicketID(` + row.id + `,  '` + row.ref + `', 7 )" class="dropdown-item">Cancel Ticket</a>

                              <a class="dropdown-item" href="` + base_url + `complaint_tickets/form?ticket_id=` + row.id +
                    `"><i class="la la-plus"></i> Add Related Complaint </a>
                                                
                              <a class="dropdown-item" href="` + base_url + `complaint_tickets/form/` + row.id +
                    `"><i class="la la-edit"></i> Update </a>

                              <a href="javascript:void(0);" class="dropdown-item remove_complaint_ticket" data-id="` + row.id + `"><i class="la la-trash"></i> Delete </a>

                              </div>
                            </span>
                            
                            <a href="` + base_url + `complaint_tickets/view/` + row.id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                              <i class="la la-eye"></i>
                            </a>`
                  );
                },
                },
                {
                    targets: [0, 1, 3, 4, 5, -1],
                    className: "dt-center",
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },

				/* ==================== begin: Add target fields for dropdown value ==================== */
                {
                    targets: 10,
                    render: function (data, type, row, meta) {
                        var inquirtyTicket = {
                            0: {
                                title: 'No',
                            },
                            1: {
                                title: 'Yes',
                            },
                        };

                        if (typeof inquirtyTicket[data] === 'undefined') {
                            return ``;
                        }
                        return inquirtyTicket[data].title;
                    },
                },

                // {
                //   targets: 17,
                //     render: function (data, type, row, meta) {
                //       var inquirtyTicket = {
                //           1: {
                //               title: 'New Request',
                //           },
                //           2: {
                //               title: 'Forwarded',
                //           },
                //           3: {
                //               title: 'Work in progress',
                //           },
                //           4: {
                //               title: 'On Hold',
                //           },
                //           5: {
                //               title: 'Completed',
                //           },
                //           6: {
                //               title: 'Closed',
                //           },
                //           7: {
                //               title: 'Cancelled',
                //           },
                //       };

                //       if (typeof inquirtyTicket[data] === 'undefined') {
                //           return ``;
                //       }
                //       return inquirtyTicket[data].title;
                //     },
                // },


            //   {
            //     targets: [4, 6, 8, 13],
            //     visible: false,
			// },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on("click", ".remove_complaint_ticket", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "complaint_tickets/delete/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            id: id
                        },
                        success: function (res) {
                            if (res.status) {
                                $("#inquiry_tickets_table").DataTable().ajax.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on("click", "#btn_upload_guide", function () {
            var table = $("#upload_guide_table");

            table.DataTable({
                order: [
                    [0, "asc"]
                ],
                pagingType: "full_numbers",
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + "complaint_tickets/get_table_schema",
                columns: [
                    // {data: 'button'},
                    {
                        data: "no"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "type"
                    },
                    {
                        data: "format"
                    },
                    {
                        data: "option"
                    },
                    {
                        data: "required"
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $("#complaint_tickets_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            // var rows = _table.rows({ 'search': 'applied' }).nodes();

            // $('input[type="checkbox"]').prop( 'checked', this.checked );
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#complaint_tickets_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "property/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#inquiry_tickets_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    var _add_ons = function () {

        $("#forwardStatusModal").on('shown.bs.modal', function () {
          // 
        });

        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });

        $(".custom-file-input").on("change", function() {
          var fileName = $(this).val().split("\\").pop();
          $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        // $(".modal").on('hidden.bs.modal', function(){
          // $('#forwardStatus').trigger("reset");
          // $("#forwardStatus")[0].reset()
          // document.getElementById('forwardStatus').reset();
        // });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    var advanceFilter = function () {
    function filter(
      project_id,
      property_id,
      house_model_id,
      buyer_id,
      turn_over_date,
      complaint_category_id,
      complaint_sub_category_id
    ) {
      var page_num = page_num ? page_num : 0;
      var project_id = project_id ? project_id : "";
      var property_id = property_id ? property_id : "";
      var buyer_id = buyer_id ? buyer_id : "";
      var house_model_id = house_model_id ? house_model_id : "";
      var turn_over_date = turn_over_date ? turn_over_date : "";
      var complaint_category_id = complaint_category_id
        ? complaint_category_id
        : "";
      var complaint_sub_category_id = complaint_sub_category_id
        ? complaint_sub_category_id
        : "";

      $.ajax({
        url: base_url + "complaint_tickets/paginationData/" + page_num,
        method: "POST",
        data:
          "page=" +
          page_num +
          "&project_id=" +
          project_id +
          "&property_id=" +
          property_id +
          "&buyer_id=" +
          buyer_id +
          "&house_model_id=" +
          house_model_id +
          "&turn_over_date=" +
          turn_over_date +
          "&complaint_category_id=" +
          complaint_category_id +
          "&complaint_sub_category_id=" +
          complaint_sub_category_id,
        success: function (html) {
          $("#filterModal").modal("hide");

          KTApp.block("#kt_content", {
            overlayColor: "#000000",
            type: "v2",
            state: "primary",
            message: "Processing...",
            css: {
              padding: 0,
              margin: 0,
              width: "30%",
              top: "40%",
              left: "35%",
              textAlign: "center",
              color: "#000",
              border: "3px solid #aaa",
              backgroundColor: "#fff",
              cursor: "wait",
            },
          });

          setTimeout(function () {
            $("#complaint_ticketsContent").html(html);

            KTApp.unblock("#kt_content");
          }, 500);
        },
      });
    }

    $("#advanceSearch").submit(function (e) {
      e.preventDefault();
      // var form_values = $('#advanceSearch').serialize();
      var project_id = $("#project_id").val();
      var property_id = $("#property_id").val();
      var house_model_id = $("#house_model_id").val();
      var buyer_id = $("#filterBuyer").val();
      var turn_over_date = $("#filterTurnOverDate").val();
      var complaint_category_id = $("#complaint_category_id").val();
      var complaint_sub_category_id = $("#filterComplaintSubCategory").val();

      if (
        project_id !== "" ||
        property_id !== "" ||
        house_model_id !== "" ||
        buyer_id !== "" ||
        turn_over_date !== "" ||
        complaint_category_id !== "" ||
        complaint_sub_category_id !== ""
      ) {
        filter(
          project_id,
          property_id,
          house_model_id,
          buyer_id,
          turn_over_date,
          complaint_category_id,
          complaint_sub_category_id
        );
      } else {
        filter();
      }
    });
  };

  var forwardStatus = function () {

    function forward(formData) {

      $.ajax({
        url: base_url + "complaint_tickets/forward_status",
        method: "POST",
        dataType: "JSON",
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (res) {

          if (res.status) {

            $("#complaint_tickets_table").DataTable().ajax.reload();
            swal.fire("Logged!", res.message, "success")
            
          } else {

            swal.fire("Oops!", res.message, "error")
              .then(function () {
                window.location.reload();
              });
          }
        },
        complete: function () {
          $("#forwardStatusModal").modal("hide");
          $("#forward_status_id").val('')
          $("#department_in_charge").html('<option>Select option</option>')
          $(".custom-file-input").val('')
          $(".custom-file-label").removeClass('selected').html('Choose file')
        }
      });
    }

    $("#forwardStatus").submit(function (e) {
      e.preventDefault();

      var formData = new FormData(this);

      var complaint_ticket_id = $("#forward_status_id").val();
      var department_id = $("#department_in_charge").val();

      if (complaint_ticket_id !== "" || department_id !== "") {

        formData.append('complaint_ticket_id', complaint_ticket_id)
        formData.append('department_id', department_id)

        forward(formData);
      }
    });
  };

  var updateStatus = function () {
    function update(formData) {

      $.ajax({
        url: base_url + "complaint_tickets/update_status",
        method: "POST",
        dataType: "JSON",
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (res) {
          $("#forwardStatusModal").modal("hide");

          if (res.status) {

            $("#complaint_tickets_table").DataTable().ajax.reload();
            swal.fire("Logged!", res.message, "success")
            
          } else {

            swal.fire("Oops!", res.message, "error")
              .then(function () {
                window.location.reload();
              });
          }
        },
        complete: function () {
          $("#updateStatusModal").modal("hide");
          $(".complaint_ticket_id").val('')
          $(".status_id").val()
          $("#remarks").val('')
          $(".custom-file-input").val('')
          $(".custom-file-label").removeClass('selected').html('Choose file')
        }
      });
    }

    $("#updateStatus").submit(function (e) {
      e.preventDefault();

      var formData = new FormData(this);

      var complaint_ticket_id = $("#update_complaint_ticket_id_single").val();
      var status_id = $("#update_status_id").val();
      var remarks = $("#remarks").val();

      if (complaint_ticket_id !== "" || remarks !== "") {

        formData.append('complaint_ticket_id', complaint_ticket_id)
        formData.append('status_id', status_id)
        formData.append('remarks', remarks)

        update(formData);
      }
    });
  };

  var closeStatus = function () {
    function close_ticket(complaint_ticket_id, status_id, remarks, image) {
      var complaint_ticket_id = complaint_ticket_id ? complaint_ticket_id : "";
      var status_id = status_id ? status_id : "";
      var remarks = remarks ? remarks : "";
      var image = image ? image : "";

      $.ajax({
        url: base_url + "complaint_tickets/update_status",
        method: "POST",
        dataType: "JSON",
        data:
          "complaint_ticket_id=" +
          complaint_ticket_id +
          "&status_id=" +
          status_id +
          "&remarks=" +
          remarks +
          "&image=" +
          image,
        success: function (res) {
          $("#closeStatusModal").modal("hide");

          if (res.status) {
            swal.fire("Logged!", res.message, "success");
          } else {
            swal.fire("Oops!", res.message, "error");
          }

          setTimeout(function () {
            window.location.reload();
          }, 500);
        },
      });
    }

    $("#closeStatus").submit(function (e) {
      e.preventDefault();
      var complaint_ticket_id = $("#complaint_ticket_id").val();
      var status_id = $("#status_id").val();
      var remarks = $("#remarks").val();
      var image = $("#image").val();

      if ((complaint_ticket_id !== "" && remarks !== "") || image !== "") {
        close_ticket(complaint_ticket_id, status_id, remarks, image);
      }
    });
  };

  // Daterangepicker Init
  var daterangepickerInit = function () {
    if ($(".kt_transaction_daterangepicker").length == 0) {
      return;
    }

    var picker = $(".kt_transaction_daterangepicker");
    var start = moment();
    var end = moment();

    function cb(start, end, label) {
      var title = "";
      var range = "";

      if (end - start < 100 || label == "Today") {
        title = "Today:";
        range = start.format("MMM D");
      } else if (label == "Yesterday") {
        title = "Yesterday:";
        range = start.format("MMM D");
      } else {
        range = start.format("MMM D") + " - " + end.format("MMM D");
      }

      $("#kt_dashboard_daterangepicker_date").html(range);
      $("#kt_dashboard_daterangepicker_title").html(title);
    }

    picker.daterangepicker(
      {
        direction: KTUtil.isRTL(),
        autoUpdateInput: false,
        locale: {
          cancelLabel: "Clear",
        },
        opens: "left",
        // ranges: {
        //  'Today': [moment(), moment()],
        //  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //  'This Month': [moment().startOf('month'), moment().endOf('month')],
        //  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        // }
      },
      cb
    );

    cb(start, end, "");

    $(picker).on("apply.daterangepicker", function (ev, picker) {
      $(this).val(
        picker.startDate.format("MM/DD/YYYY") +
          " - " +
          picker.endDate.format("MM/DD/YYYY")
      );
    });

    $(picker).on("cancel.daterangepicker", function (ev, picker) {
      $(this).val("");
    });
  };

  var batchChangeStatus = function () {
    var ids = [];
    $('.batch_update_status').click(function () {
      // $('#update_status').attr('class', 'btn btn-primary batch_update_apply');
      var selectedids_arr = [];
      // Read all checked checkboxes
      $('input[name="id[]"]:checked').each(function () {
        selectedids_arr.push($(this).val());
      });

      if (selectedids_arr.length > 0) {
        ids = selectedids_arr;
      }
    })

    $("#updateStatusBatch").submit(function (e) {
      e.preventDefault();

      var formData = new FormData(this);

      var complaint_ticket_id = $("#update_complaint_ticket_id").val();
      var status_id = $("#update_status_id").val();
      var remarks = $("#remarks").val();

      formData.append('complaint_ticket_id', complaint_ticket_id)
      formData.append('status_id', status_id)
      formData.append('remarks', remarks)

      if (complaint_ticket_id !== "" || remarks !== "") {

        for (let i=0; i < ids.length; i++){
          formData.set('complaint_ticket_id',ids[i]);
          $.ajax({
            url: base_url + "complaint_tickets/update_status",
            method: "POST",
            dataType: "JSON",
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (res) {
            },
          });
        }
        swal.fire("Logged!", 'Successful', "success");
        $("#complaint_tickets_table").DataTable().ajax.reload();
      }
    });

    $("#forwardStatusBatch").submit(function (e) {
      e.preventDefault();

      var formData = new FormData(this);

      var department_id = $("#department_in_charge_batch").val();

      formData.append('department_id', department_id)

      if (department_id !== "") {

        for (let i=0; i < ids.length; i++){
          formData.set('complaint_ticket_id',ids[i]);
          $.ajax({
            url: base_url + "complaint_tickets/forward_status",
            method: "POST",
            dataType: "JSON",
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (res) {
            },
          });
        }
        swal.fire("Logged!", 'Successful', "success");
        $("#complaint_tickets_table").DataTable().ajax.reload();
      }
    });
  };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            complaintTable();
            filter();
            _selectProp();
            _add_ons();
            upload_guide();
            status();

            forwardStatus();
            updateStatus();
            closeStatus();
            daterangepickerInit();

            batchChangeStatus();
        },
    };
})();

jQuery(document).ready(function () {
  ComplaintTicket.init();
});
$('#updateStatusModal').on('hidden.bs.modal', function () {
  $('#update_status').attr('class', 'btn btn-primary');
});
// Function for setting up modal information: complaint ticket id, status
function setComplaintTicketID(id, ref_no, status, for_multiple=0) {

    window.scrollTo({ top: 0, behavior: 'smooth' });

  var header = document.getElementsByClassName("statusHeaderNo");
  var label;

  switch (status) {
    case 2:
      label = "Forward to Department-in-charge";
      break;
    case 3:
      label = "Change General Status to Work in Progress";
      break;
    case 4:
      label = "Change General Status to On Hold";
      break;
    case 5:
      label = "Change General Status to Completed";
      break;
    case 6:
      label = "Do you want to close the ticket?";
      break;
    case 7:
      label = "Do you want to cancel the ticket?";
      break;
  }

  $("#forward_status_id").val(id)
  $(".complaint_ticket_id").val(id)
  $(".status_id").val(status)
  $("#updateStatusModalLabel").html(label)

  for (var i = 0; i < header.length; i++) {
    header[i].innerHTML = `Ticket No.: ${ref_no.toString()}`;
  }
}

function setComplaintTicketIDBatch(id, ref_no, status, for_multiple=0) {

  window.scrollTo({ top: 0, behavior: 'smooth' });

var header = document.getElementsByClassName("statusHeaderNoBatch");
var label;

switch (status) {
  case 2:
    label = "Forward to Department-in-charge";
    break;
  case 3:
    label = "Change General Status to Work in Progress";
    break;
  case 4:
    label = "Change General Status to On Hold";
    break;
  case 5:
    label = "Change General Status to Completed";
    break;
  case 6:
    label = "Do you want to close the ticket?";
    break;
  case 7:
    label = "Do you want to cancel the ticket?";
    break;
}

// $("#forward_status_id").val(id)
$(".complaint_ticket_id").val(id)
$(".status_id").val(status)
$("#updateStatusBatchModalLabel").html(label)

for (var i = 0; i < header.length; i++) {
  header[i].innerHTML = `Ticket No.: ${ref_no.toString()}`;
}
}

// Check if complaint ticket is already resolve
document.getElementById("closeStatus").onsubmit = function () {
  var confirmation = this.elements["confirmation"];

  if (!confirmation.checked) {
    console.log("Please confirm first");
    return false;
  }
  console.log("Processing...");
  return true;
};