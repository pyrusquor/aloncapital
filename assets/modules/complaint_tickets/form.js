// Class definition
var Form = (function () {
  var arrows;

  if (KTUtil.isRTL()) {
    arrows = {
      leftArrow: '<i class="la la-angle-right"></i>',
      rightArrow: '<i class="la la-angle-left"></i>',
    };
  } else {
    arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    };
  }

  // Private functions
  var datepicker = function () {
    // minimum setup
    $(".kt_datepicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      orientation: "bottom left",
      templates: arrows,
      locale: "no",
      format: "yyyy-mm-dd",
      autoclose: true,
    });

    $(".yearPicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true,
    });
  };

  var formEl;
  var validator;

  var initValidation = function () {
    validator = formEl.validate({
      // Validate only visible fields
      ignore: ":hidden",

      // Validation rules
      rules: {
        name: {
          required: true,
        },
      },
      // messages: {
      // 	"info[last_name]":'Last name field is required',
      // },
      // Display error
      invalidHandler: function (event, validator) {
        KTUtil.scrollTop();

        swal.fire({
          title: "",
          text:
            "There are some errors in your submission. Please correct them.",
          type: "error",
          confirmButtonClass: "btn btn-secondary",
        });
      },

      // Submit valid form
      submitHandler: function (form) { },
    });
  };

  var initSubmit = function () {
    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

    btn.on("click", function (e) {
      e.preventDefault();

      if (validator.form()) {
        // See: src\js\framework\base\app.js
        KTApp.progress(btn);
        //KTApp.block(formEl);

        // See: http://malsup.com/jquery/form/#ajaxSubmit
        formEl.ajaxSubmit({
          type: "POSt",
          dataType: "JSON",
          success: function (response) {
            if (response.status) {
              swal
                .fire({
                  title: "Success!",
                  text: response.message,
                  type: "success",
                })
                .then(function () {
                  window.location.replace(base_url + "complaint_tickets");
                });
            } else {
              swal.fire({
                title: "Oops!",
                html: response.message,
                icon: "error",
              });
            }
          },
        });
      }
    });
  };

  var _add_ons = function () {
    datepicker();

    $('.suggests-property').on('change', function () {
      $.ajax({
        url: base_url + "generic_api/fetch_specific?table=properties&field=id&value=" + $(this).val() + "&select=turn_over_date",
        type: "POST",
        success: function (data) {
          console.log(data[0])

          let turn_over_date = data[0]['turn_over_date']

          if (turn_over_date) {
            $('#turn_over_date').val(turn_over_date.split(" ")[0])
          } else {
            $('#turn_over_date').val("")
          }
        },
      });
    })

    $("#turn_over_date").on('change', function () {

      turn_over_date = new Date($(this).val() + "T00:00:00");

      let warranty_date = new Date(turn_over_date.setDate(turn_over_date.getDate() + 180));

      $("#warranty_date").val(warranty_date.toISOString().split('T')[0])
    })
  };

  // Public functions
  return {
    init: function () {
      datepicker();
      _add_ons();

      formEl = $("#form_complaint_ticket");

      initValidation();
      initSubmit();
    },
  };
})();

// Initialization
jQuery(document).ready(function () {
  Form.init();
});

const complaintCategorySelect = document.querySelector(
  "#complaint_category_id"
);
const complaintSubCategory = document.querySelector(
  "#complaint_sub_category_id"
);

const getComplaintCategories = () => {
  $.ajax({
    url: base_url + "complaint_categories/get_all",
    type: "GET",
    success: function (data) {
      const categories = JSON.parse(data);
      categories.data.map((category) => {
        let opt = document.createElement("option");

        opt.value = category.id;
        opt.innerHTML = category.name;
        complaintCategorySelect.appendChild(opt);
      });
    },
  });
};

const setComplaintSubCategory = () => {
  $("#complaint_category_id").on("change", function () {
    
    complaintCategoryID = $(this).val();
    if (complaintCategoryID != '') {
      $.ajax({
        url:
          base_url +
          "complaint_sub_categories/complaint_sub_categories_by_category",
        type: "POST",
        data: {
          complaintCategoryID,
          complaintCategoryID,
        },
        success: function (data) {
          const sub_categories = JSON.parse(data);
          complaintSubCategory.innerHTML = "";
          try {
            sub_categories.map((sub_category) => {
              let opt = document.createElement("option");
  
              opt.value = sub_category.id;
              opt.innerHTML = sub_category.name;
              complaintSubCategory.appendChild(opt);
            });
          } catch (error) {
            let opt = document.createElement("option");
  
            complaintSubCategory.innerHTML = "";
            opt.value = "";
            opt.innerHTML = "No record found";
            complaintSubCategory.appendChild(opt);
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.log(
            thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText
          );
          complaintSubCategory.removeChild();
        },
      });
    } else {
      $('#complaint_sub_category_id').html('<option>Select option</option>')
    }
  });
};

// getComplaintCategories();
setComplaintSubCategory();
