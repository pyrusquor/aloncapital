"use strict";
var TransactionCommission = (function () {

    let tableElement = $('#transaction_commission_table');
    
    var TransactionCommissionTable = function () {

        var dataTable = tableElement.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'transaction_commission/showItemssellers',
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            columns: [
                {
                    data: null,
                },
                {
                    data: 'seller_id',
                },
                {
                    data: 'seller_name',
                },
                {
                    data: 'amount_sum',
                },
                {
                    data: 'commission_count',
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `<a href="` + base_url + `transaction_commission/view_seller/`  + row.seller_id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                <i class="la la-eye"></i>
                            </a>`
                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: 'dt-center',
                },
                {
                    targets: 3,
                    className: 'dt-center',
                    orderable: false,
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
                      <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
                      </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                // {
                //     targets: 4,
                //     visible: false,
                // },
                // {
                //     targets: 4,
                //     render: function (data, type, row, meta) {
                //         if (data == 0) {
                //             return `No entries found`;
                //         }
    
                //         return `<a class="btn btn-primary" href="${base_url}/accounting_entries/view/${data}" target="_blank" rel="noopener noreferrer">View Entry</a>`;
                //     },
                // },
                // {
                //     targets: 11,
                //     render: function (data, type, row, meta) {
                //         // '1' => 'Reservation', '2' => 'Downpayment', '3' => 'Loan', '4' => 'Equity', '5' => 'Others'
                //         var transaction_commission_status = {
                //             1: {
                //                 title: 'Pending',
                //             },
                //             2: {
                //                 title: 'For RFP',
                //             },
                //             3: {
                //                 title: 'Processing',
                //             },
                //             4: {
                //                 title: 'Released',
                //             },
                //         };
    
                //         if (
                //             typeof transaction_commission_status[data] ===
                //             'undefined'
                //         ) {
                //             return ``;
                //         }
    
                //         return transaction_commission_status[data].title;
                //     },
                // },
    
                // {
                //     targets: 5,
                //     render: function (data, type, row, meta) {
                //         // '1' => 'Reservation', '2' => 'Downpayment', '3' => 'Loan', '4' => 'Equity', '5' => 'Others'
                //         var transaction_commission_periods = {
                //             1: {
                //                 title: 'Reservation',
                //             },
                //             2: {
                //                 title: 'Downpayment',
                //             },
                //             3: {
                //                 title: 'Loan',
                //             },
                //             4: {
                //                 title: 'Equity',
                //             },
                //             5: {
                //                 title: 'Others',
                //             },
                //         };
    
                //         if (
                //             typeof transaction_commission_periods[data] ===
                //             'undefined'
                //         ) {
                //             return ``;
                //         }
    
                //         return transaction_commission_periods[data].title;
                //     },
                // },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });
    
        function getFilter(data) {

            data.filter = $('#generalSearch').serialize();
        }

        $('#generalSearch').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on("click", ".remove_project", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "transaction_commission/delete/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            id: id
                        },
                        success: function (res) {
                            if (res.status) {
                                $("#transaction_commission_table").DataTable().ajax.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on("click", "#btn_upload_guide", function () {
            var table = $("#upload_guide_table");

            table.DataTable({
                order: [
                    [0, "asc"]
                ],
                pagingType: "full_numbers",
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + "transaction_commission/get_table_schema",
                columns: [
                    // {data: 'button'},
                    {
                        data: "no"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "type"
                    },
                    {
                        data: "format"
                    },
                    {
                        data: "option"
                    },
                    {
                        data: "required"
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $("#transaction_commission_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            // var rows = _table.rows({ 'search': 'applied' }).nodes();

            // $('input[type="checkbox"]').prop( 'checked', this.checked );
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#transaction_commission_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "transaction_commission/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#transaction_commission_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    var _add_ons = function () {
        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    var get_commission_summary = function () {
        
        $.ajax({
            url: base_url + "transaction_commission/get_commission_summary",
            type: "POST",
            dataType: "JSON",
            data : {e:'please'},
            success: function (res) {
                $("#total_commission_count").html(res.total_commission_count);
                $("#total_commission_amount").html(res.total_commission_amount);
                $("#for_approval_count").html(res.for_approval_count);
                $("#for_approval_amount").html(res.for_approval_amount);
                $("#approved_commissions_count").html(res.approved_commissions_count);
                $("#approved_commissions_amount").html(res.approved_commissions_amount);
                $("#paid_commissions_count").html(res.paid_commissions_count);
                $("#paid_commissions_amount").html(res.paid_commissions_amount);
            },
        });

    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            TransactionCommissionTable();
            filter();
            _selectProp();
            _add_ons();
            upload_guide();
            status();
            get_commission_summary();
        },
    };
})();

jQuery(document).ready(function () {
    TransactionCommission.init();
});