"use strict";
var ViewSeller = (function () {

    var selectAllcommissions = function (){
        $(".check_all").click(function(){
            $('.commission_box').not(this).prop('checked', this.checked);
        });
    }

    var processCommissions = function (){
        $(document).on('click', '#create_payment_requests', function (){
            var commission_checkboxes = $('.commission_box:checkbox:checked');
            if(commission_checkboxes.length){

                var array_commisions = commission_checkboxes.map(function (){
                    return $(this).data('commission-id');
                }).get();
                var array_commisions_sum_net = commission_checkboxes.map(function (){
                    return $(this).data('net');
                }).get();
                var array_commisions_sum_gross = commission_checkboxes.map(function (){
                    return $(this).data('gross');
                }).get();
                var net_commision_amount = array_commisions_sum_net.reduce(function(a,b){
                    return parseFloat(a)+parseFloat(b);
                },0);
                var gross_commision_amount = array_commisions_sum_gross.reduce(function(a,b){
                    return parseFloat(a)+parseFloat(b);
                },0);
                var array_commisions_projects = commission_checkboxes.map(function (){
                    return $(this).data('project');
                }).get();
                var array_commisions_properties = commission_checkboxes.map(function (){
                    return $(this).data('property');
                }).get();
                var array_commisions_companies = commission_checkboxes.map(function (){
                    return $(this).data('company');
                }).get();

                var unique_projects = array_commisions_projects.filter(function(item, i, array_commisions_projects){
                    return i == array_commisions_projects.indexOf(item);
                })
                var unique_properties = array_commisions_properties.filter(function(item, i, array_commisions_properties){
                    return i == array_commisions_properties.indexOf(item);
                })

                var seller_id = $('#seller_info').data('id');
                var seller_name = $('#seller_info').data('name')

                localStorage.setItem('commission_net_sum', net_commision_amount);
                localStorage.setItem('commission_gross_sum', gross_commision_amount);
                localStorage.setItem('commission_ids', array_commisions);
                localStorage.setItem('seller_id', seller_id);
                localStorage.setItem('seller_name', seller_name);
                localStorage.setItem('commission_properties', unique_properties);
                localStorage.setItem('commission_projects', unique_projects);
                var array_commisions_companies_set = new Set(array_commisions_companies)
                window.location.replace(base_url + 'payment_request/form/0/0/0/' + net_commision_amount +'/' + [...array_commisions_companies_set][0] + '/' + (gross_commision_amount-net_commision_amount));
            }
        })
    }

    return {
        //main function to initiate the module
        init: function () {
            selectAllcommissions();
            processCommissions();
        },
    };
})();

jQuery(document).ready(function () {
    localStorage.removeItem('commission_net_sum');
    localStorage.removeItem('commission_gross_sum');
    localStorage.removeItem('commission_ids');
    localStorage.removeItem('seller_id');
    localStorage.removeItem('seller_name');
    localStorage.removeItem('commission_properties');
    localStorage.removeItem('commission_projects');
    ViewSeller.init();
});