// Class definition
var KTFormControls = (function () {
    // Private functions

    var valCommissions = function () {
        $('#commissions_form').validate({
            // define validation rules
            rules: {
                name: {
                    required: true,
                },
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                toastr.error(
                    'Please check your fields',
                    'Something went wrong'
                );

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });
    };

    return {
        // public functions
        init: function () {
            valCommissions();
        },
    };
})();

var KTBootstrapDatepicker = (function () {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
    };

    const calculateCommission = (wht, atr) => {
        // console.log(wht, atr);
    };

    $('#amount_to_release').on('change blur', function () {
        let wht = $('#commission_vat').val();
        let atr = $(this).val();

        if (wht !== undefined && atr !== undefined) {
            calculateCommission(wht, atr);
        }
    });

    $('#commission_vat').on('change blur', function () {
        let wht = $(this).val();
        let atr = $('amount_to_release').val();

        if (wht !== undefined && atr !== undefined) {
            calculateCommission(wht, atr);
        }
    });

    return {
        // public functions
        init: function () {
            datepicker();
        },
    };
})();

jQuery(document).ready(function () {
    KTFormControls.init();
    KTBootstrapDatepicker.init();
});

const amountType = () => {
    const commissionCurrency = document.querySelectorAll(
        '#commission_rate_amount'
    );
    const commissionPercentage = document.querySelectorAll(
        '#commission_rate_percentage'
    );

    commissionCurrency.forEach((select, index) => {
        $(select).on('change keyup', function () {
            if (this.value !== '') {
                commissionPercentage[index].disabled = true;
                commissionPercentage[index].value = '';
            } else {
                commissionPercentage[index].disabled = false;
            }
        });
    });

    commissionPercentage.forEach((select, index) => {
        $(select).on('change keyup', function () {
            if (this.value !== '') {
                commissionCurrency[index].disabled = true;
                commissionCurrency[index].value = '';
            } else {
                commissionCurrency[index].disabled = false;
            }
        });
    });
};

amountType();
