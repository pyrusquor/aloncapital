'use strict';
var VehicleEquipment = (function () {
    var VehicleEquipmentTable = function () {
        var table = $('#vehicle_equipment_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,

            ajax: base_url + 'vehicle_equipment/showVehicleEquipments',
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'company_id',
                },
                {
                    data: 'vehicle_equipment_group_id',
                },
                {
                    data: 'code',
                },
                {
                    data: 'name',
                },
                {
                    data: 'model',
                },
                {
                    data: 'chassis_number',
                },
                {
                    data: 'date_acquired',
                },
                {
                    data: 'denomination',
                },
                {
                    data: 'fuel_type',
                },
                {
                    data: 'engine_number',
                },
                {
                    data: 'make',
                },
                {
                    data: 'gross_weight',
                },
                {
                    data: 'net_weight',
                },
                {
                    data: 'body_number',
                },
                {
                    data: 'body_type_id',
                },
                {
                    data: 'year_model',
                },
                {
                    data: 'plate_number',
                },
                {
                    data: 'color',
                },
                {
                    data: 'is_active',
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
                            <span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="` +
                            base_url +
                            `vehicle_equipment/form/` +
                            row.id +
                            `"><i class="la la-edit"></i> Update </a>
										<a href="javascript:void(0);" class="dropdown-item remove_vehicle_equipment" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete </a>
									</div>
								</span>
								<a href="` +
                            base_url +
                            `vehicle_equipment/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>
                            `
                        );
                    },
                },
                {
                    targets: [0, 1, 2, 3, -1],
                    className: 'dt-center',
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                // {
                //     targets: 6,
                //     render: function (data, type, row, meta) {
                //         var status_type = {
                //             0: {
                //                 status: 'Inactive',
                //             },
                //             1: {
                //                 status: 'Active',
                //             },
                //         };

                //         if (typeof status_type[data] === 'undefined') {
                //             return ``;
                //         }

                //         return status_type[data].status;
                //     },
                // },
                {
                    targets: 3,
                    render: function (data, type, row, meta) {
                        var vehicle_equipment_group = {
                            1: {
                                vehicle_equipment: 'Delivery Equipment'
                            },
                            2: {
                                vehicle_equipment: 'Transportation Equipment'
                            },
                            3: {
                                vehicle_equipment: 'Heavy Equipment'
                            },
                            4: {
                                vehicle_equipment: 'Construction Equipment'
                            },
                            5: {
                                vehicle_equipment: 'Small Tools Machinery'
                            },
                            6: {
                                vehicle_equipment: 'Machinery'
                            },
                            7: {
                                vehicle_equipment: 'Others'
                            },
                        }

                        if (typeof vehicle_equipment_group[data] === 'undefined') {
                            return ``
                        }

                        return vehicle_equipment_group[data].vehicle_equipment
                    }
                },
                {
                    targets: 10,
                    render: function (data, type, row, meta) {
                        var fuel_type = {
                            1: {
                                fuel: 'Gas'
                            },
                            2: {
                                fuel: 'Diesel'
                            }
                        }

                        if (typeof fuel_type[data] === 'undefined') {
                            return ``
                        }

                        return fuel_type[data].fuel
                    }
                },
                {
                    targets: 16,
                    render: function (data, type, row, meta) {
                        var body_type = {
                            1: {
                                body: 'Motorcycle'
                            },
                            2: {
                                body: 'Sedan'
                            },
                            3: {
                                body: 'Sport-Utility Vehicle (SUV)'
                            },
                            4: {
                                body: 'Van/Minivan'
                            },
                            5: {
                                body: 'Pickup Truck'
                            },
                            6: {
                                body: 'Jeep'
                            },
                            7: {
                                body: 'Flat Bed Body'
                            },
                            8: {
                                body: 'Van Body (Truck)'
                            },
                            9: {
                                body: 'Contractor Body'
                            },
                            10: {
                                body: 'Dump Bodies'
                            },
                            11: {
                                body: 'Temperature-Controlled Body'
                            },
                            12: {
                                body: 'Tankers'
                            },
                        }
                        
                        if (typeof body_type[data] === 'undefined') {
                            return ``
                        }

                        return body_type[data].body
                    }
                },
                {
                    targets: 20,
                    render: function (data, type, row, meta) {
                        var status_type = {
                            0: {
                                status: 'Inactive',
                            },
                            1: {
                                status: 'Active',
                            },
                        };

                        if (typeof status_type[data] === 'undefined') {
                            return ``;
                        }

                        return status_type[data].status;
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };

    var upload_guide = function () {
        $(document).on('click', '#btn_upload_guide', function () {
            var table = $('#upload_guide_table');

            table.DataTable({
                order: [[0, 'asc']],
                pagingType: 'full_numbers',
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + 'vehicle_equipment/get_table_schema',
                columns: [
                    // {data: 'button'},
                    {
                        data: 'no',
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'type',
                    },
                    {
                        data: 'format',
                    },
                    {
                        data: 'option',
                    },
                    {
                        data: 'required',
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $('#vehicle_equipment_table').DataTable();

        $('#_search').on('keyup', function () {
            dTable.search($(this).val()).draw();
        });

        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var _filterVehicleEquipment = function () {
        var dTable = $('#vehicle_equipment_table').DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($('#_column_' + n).val())
                .draw();
        }

        $('._filter').on('keyup change clear', function () {
            if ($(this).is('input')) {
                let val = $('#_column_' + $(this).data('column')).val();

                dTable.search(val).draw();
            } else {
                _colFilter($(this).data('column'));
            }
        });
    };

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
        $('.kt_datepicker').datepicker({
            orientation: 'bottom left',
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: 'yyyy-mm-dd',
        });
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_vehicle_equipment', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'vehicle_equipment/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#vehicle_equipment_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var _selectProp = function () {
        var _table = $('#vehicle_equipment_table').DataTable();

        $('#select-all').on('click', function () {
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
                _table.button(0).enable();
            } else {
                $('.delete_check').prop('checked', false);
                _table.button(0).disable();
            }
        });

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'vehicle_equipment/bulkDelete',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#vehicle_equipment_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            VehicleEquipmentTable();
            _add_ons();
            _filterVehicleEquipment();
            upload_guide();
            status();
            datepicker();
            confirmDelete();
            _selectProp();
        },
    };
})();

jQuery(document).ready(function () {
    VehicleEquipment.init();
});
