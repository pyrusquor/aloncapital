'use strict';
var JobRequest = (function () {
    var JobRequestTable = function () {
        var table = $('#job_request_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,

            ajax: base_url + 'job_request/showJobRequests',
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'number',
                },
                {
                    data: 'company_id',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'description',
                },
                {
                    data: 'staff_id',
                },
                {
                    data: 'status',
                },
                {
                    data: 'created_at',
                },
                {
                    data: 'request_type',
                },
                {
                    data: 'company',
                },
                {
                    data: 'project',
                },
                {
                    data: 'staff',
                },
                {
                    data: 'status_id',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
                            <span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
                                    <a href="` +
                            base_url +
                            `job_request/printable/` +
                            row.id +
                            `" target="_blank" rel="noopener noreferrer"  class="dropdown-item"><i class="la la-document"></i> Print </a>
										<a class="dropdown-item" href="` +
                            base_url +
                            `job_request/add_job_order/` +
                            row.id +
                            `"><i class="la la-edit"></i> Add Job Order </a>
										<a class="dropdown-item" href="` +
                            base_url +
                            `job_request/form/` +
                            row.id +
                            `"><i class="la la-edit"></i> Update </a>
										<a href="javascript:void(0);" class="dropdown-item remove_job_request" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete </a>
                            <a href="javascript:void(0);" class="dropdown-item update_job_request_status" data-status="2" data-id="` +
                            row.id +
                            `"><i class="la la-check"></i> Approve Request </a>
                            <a href="javascript:void(0);" class="dropdown-item update_job_request_status" data-status="3" data-id="` +
                            row.id +
                            `"><i class="flaticon2-cross"></i> Disapprove Request </a>
                            <a href="javascript:void(0);" class="dropdown-item update_job_request_status" data-status="4" data-id="` +
                            row.id +
                            `"><i class="flaticon2-refresh"></i> Cancel Request </a>
									</div>
								</span>
								<a href="` +
                            base_url +
                            `job_request/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>
                            `
                        );
                    },
                },
                {
                    targets: [0, 1, 2, 3, -1],
                    className: 'dt-center',
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                {
                    targets: 7,
                    render: function (data, type, row, meta) {
                        // '1' => 'Request Filed', '2' => 'Approved - RFP', '3' => 'Approved - Job Request'
                        var job_requests_status = {
                            1: {
                                title: 'Request Filed',
                            },
                            2: {
                                title: 'Approved',
                            },
                            3: {
                                title: 'Disapproved',
                            },
                            4: {
                                title: 'Cancelled',
                            },
                        };

                        if (typeof job_requests_status[data] === 'undefined') {
                            return ``;
                        }

                        return job_requests_status[data].title;
                    },
                },
                {
                    targets: 9,
                    render: function (data, type, row, meta) {
                        // '1' => 'Request Filed', '2' => 'Approved - RFP', '3' => 'Approved - Job Request'
                        var job_request_type = {
                            1: {
                                title: 'Job Request',
                            },
                            2: {
                                title: 'Job Order',
                            },
                        };

                        if (typeof job_request_type[data] === 'undefined') {
                            return ``;
                        }

                        return job_request_type[data].title;
                    },
                },
                {
                    targets: [10, 11, 12, 13],
                    visible: false,
                },
                // {
                //     targets: 5,
                //     render: function (data, type, full, meta) {
                //         const d = new Date(data);
                //         const months = [
                //             'January',
                //             'February',
                //             'March',
                //             'April',
                //             'May',
                //             'June',
                //             'July',
                //             'August',
                //             'September',
                //             'October',
                //             'November',
                //             'December',
                //         ];
                //         const month = months[d.getMonth()];
                //         const day = d.getDate();
                //         const year = d.getFullYear();

                //         return `${month} ${day}, ${year}`;
                //     },
                // },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };

    var upload_guide = function () {
        $(document).on('click', '#btn_upload_guide', function () {
            var table = $('#upload_guide_table');

            table.DataTable({
                order: [[0, 'asc']],
                pagingType: 'full_numbers',
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + 'job_request/get_table_schema',
                columns: [
                    // {data: 'button'},
                    {
                        data: 'no',
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'type',
                    },
                    {
                        data: 'format',
                    },
                    {
                        data: 'option',
                    },
                    {
                        data: 'required',
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $('#job_request_table').DataTable();

        $('#_search').on('keyup', function () {
            dTable.search($(this).val()).draw();
        });

        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var filterJobRequest = function () {
        var dTable = $('#job_request_table').DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($('#_column_' + n).val())
                .draw();
        }

        $('._filter').on('keyup change clear', function () {
            if ($(this).is('input')) {
                let val = $('#_column_' + $(this).data('column')).val();

                dTable.search(val).draw();
            } else {
                _colFilter($(this).data('column'));
            }
        });
    };

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
        $('.kt_datepicker').datepicker({
            orientation: 'bottom left',
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: 'yyyy-mm-dd',
        });
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_job_request', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'job_request/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#job_request_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var confirmApprove = function () {
        $(document).on('click', '.update_job_request_status', function () {
            var status = $(this).data('status');
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'job_request/update_status/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            status: status,
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#job_request_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Success!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var _selectProp = function () {
        var _table = $('#job_request_table').DataTable();

        $('#select-all').on('click', function () {
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
                _table.button(0).enable();
            } else {
                $('.delete_check').prop('checked', false);
                _table.button(0).disable();
            }
        });

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'job_request/bulkDelete',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#job_request_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            JobRequestTable();
            _add_ons();
            filterJobRequest();
            upload_guide();
            status();
            datepicker();
            confirmDelete();
            confirmApprove();
            _selectProp();
        },
    };
})();

jQuery(document).ready(function () {
    JobRequest.init();
});
