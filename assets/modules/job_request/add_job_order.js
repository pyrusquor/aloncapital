var datepicker = function () {
    // minimum setup
    $('.compDatepicker').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: 'bottom left',
        templates: arrows,
        locale: 'no',
        format: 'yyyy-mm-dd',
    });

    $('.yearPicker').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        format: 'yyyy',
        viewMode: 'years',
        minViewMode: 'years',
        autoclose: true,
    });
    $('.kt_datepicker').datepicker({
        orientation: 'bottom left',
        autoclose: true,
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        },
        format: 'yyyy-mm-dd',
    });
};

datepicker();
