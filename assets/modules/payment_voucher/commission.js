// Class definition
var KTFormControls = function () {
    let default_link = $('#create_pr').attr("href");
// Private functions
    var _selectProp = function() {
        var _table = $("#commission_table").DataTable();

        $(document).on("change", 'input[name="id[]"]', function() {
            var _checked = $('input[name="id[]"]:checked').length;
            var commissionAmount = 0;
            var commissions = [];
            var arr = [];
            var link;
            var createButton = document.getElementById("create_pr");
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }

            $('input[name="id[]"]:checked').each(function () {
                arr.push($(this).attr('data-seller'));
                commissions.push($(this).attr('data-id'));
            });

            if (checkSeller(arr)) {
                $('input[name="id[]"]:checked').each(function () {
                    commissionAmount += parseFloat($(this).attr('data-amount'));
                });

                $('#commission_amount').val(commissionAmount);
                var commisions_url = ArrayToURL(commissions);
                // url
                // id = 0
                // seller_id
                // gross_amount
                // commission_id
                link = default_link.concat(arr[0], '/', commissionAmount.toString(), '/', commisions_url);

                $('#create_pr').attr("href", link);
                createButton.classList.remove("disabled");
                $('#total_amount').text(commissionAmount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            } else {
                createButton.classList.add("disabled");
                swal.fire({
                    title: "Oops!",
                    text: "You've selected multiple seller! You can only select one seller per transaction.",
                    type: "warning",
                });
            }
        });
    };

    return {
        // public functions
        init: function () {
            _selectProp();
        }
    };
}();

jQuery(document).ready(function () {
    KTFormControls.init();
});

function checkSeller(arr){
    var x= arr[0];
    return arr.every(function(item){
        return item === x;
    });
}

function ArrayToURL(array) {
    var pairs = [];
    for (var key in array) {
        if(array.hasOwnProperty(key)) {
            pairs.push(encodeURIComponent(array[key]));
        }
    }
    return pairs.join('-');
}