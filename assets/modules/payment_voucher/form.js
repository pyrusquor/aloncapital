// Class definition
var Create = (function () {
  var formRepeater = function () {
    $("#form_payment_voucher").repeater({
      initEmpty: false,

      defaultValues: {},

      show: function () {
        $(this).slideDown(); 
        datepicker();
        listItemDetail();
        calculateItemTotalPrice();
      },

      hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
      },
    });
  };

  var entryFormRepeater = function () {
    $("#entry_item_form_repeater").repeater({
      initEmpty: false,

      defaultValues: {},

      show: function () {
        $(this).slideDown();
        datepicker();
        getLedgers();
        paymentType();
        calculateDTotal();
        calculateCTotal();

        $('.select2-container').remove();
        Select.init();
        $('.select2-container').css('width','100%');
      },

      hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
        Select.init();
      },
    });
  };

  var arrows;
  if (KTUtil.isRTL()) {
    arrows = {
      leftArrow: '<i class="la la-angle-right"></i>',
      rightArrow: '<i class="la la-angle-left"></i>',
    };
  } else {
    arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    };
  }

  // Private functions
  var datepicker = function () {
    // minimum setup
    $(".kt_datepicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      orientation: "bottom left",
      templates: arrows,
      locale: "no",
      format: "yyyy-mm-dd",
      autoclose: true,
    });

    $(".yearPicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true,
    });
  };

  // Base elements
  var wizardEl;
  var formEl;
  var validator;
  var _add_ons;

  // Private functions
  var initWizard = function () {
    // Initialize form wizard
    wizard = new KTWizard("kt_wizard_v3", {
      startStep: 1,
    });

    // Validation before going to next page
    wizard.on("beforeNext", function (wizardObj) {
      if (validator.form() !== true) {
        wizardObj.stop(); // don't go to the next step
      }
    });

    wizard.on("beforePrev", function (wizardObj) {
      if (validator.form() !== true) {
        wizardObj.stop(); // don't go to the next step
      }
    });

    // Change event
    wizard.on("change", function (wizard) {
      KTUtil.scrollTop();
    });
  };

  var initValidation = function () {
    validator = formEl.validate({
      // Validate only visible fields
      ignore: ":hidden",

      // Validation rules
      // rules: {
      //     "voucher[payable_type_id]": {
      //         required: true
      //     },
      //     "voucher[payee_type]": {
      //         required: true
      //     },
      //     "voucher[payee_type_id]": {
      //         required: true
      //     },
      //     "voucher[date_requested]": {
      //         required: true,
      //     },
      //     "voucher[due_date]": {
      //         required: true,
      //     },
      //     "voucher[approving_department_id]": {
      //         required: true,
      //     },
      //     "voucher[approving_staff_id]": {
      //         required: true,
      //     },
      //     "voucher[requesting_department_id]": {
      //         required: true,
      //     },
      //     "voucher[requesting_staff_id]": {
      //         required: true,
      //     },
      //     "voucher[prepared_by]": {
      //       required: true,
      //   },
      //     "items[0][item_id]": {
      //         required: true,
      //     },
      //     "items[0][unit_price]": {
      //         required: true,
      //     },
      //     "items[0][name]": {
      //         required: true,
      //     },
      //     "items[0][tax_amount]": {
      //         required: true,
      //     },
      //     "items[0][total_amount]": {
      //         required: true,
      //     },
      //     "voucher[gross_amount]": {
      //         required: true,
      //         number: true,
      //     },
      //     "voucher[payment_type_id]": {
      //         required: true,
      //     },
      //     "entry_item[0][dc]": {
      //         required: true,
      //     },
      //     "entry_item[0][ledger_id]": {
      //         required: true,
      //     },
      //     "entry_item[0][amount]": {
      //         required: true,
      //     },
      // },
      // messages: {
      // 	"info[last_name]":'Last name field is required',
      // },
      // Display error
      invalidHandler: function (event, validator) {
        KTUtil.scrollTop();

        swal.fire({
          title: "",
          text:
            "There are some errors in your submission. Please correct them.",
          type: "error",
          confirmButtonClass: "btn btn-secondary",
        });
      },

      // Submit valid form
      submitHandler: function (form) {},
    });
  };

  var initSubmit = function () {
    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

    btn.on("click", function (e) {
      e.preventDefault();
      let paidAmount = $("#total_due_amount").val();
      let dTotalInput = $("#dTotal").val();
      let cTotalInput = $("#cTotal").val();

      if (dTotalInput == cTotalInput) {
        if (paidAmount == dTotalInput || paidAmount == cTotalInput) {
          if (validator.form()) {
            // See: src\js\framework\base\app.js
            KTApp.progress(btn);
            //KTApp.block(formEl);

            // See: http://malsup.com/jquery/form/#ajaxSubmit
            formEl.ajaxSubmit({
              type: "POST",
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  swal
                    .fire({
                      title: "Success!",
                      text: response.message,
                      type: "success",
                    })
                    .then(function () {
                      window.location.replace(base_url + "payment_voucher");
                    });
                } else {
                  swal.fire({
                    title: "Oops!",
                    html: response.message,
                    icon: "error",
                  });
                }
              },
            });
          }
        } else {
          swal.fire({
            title: "Error!",
            html:
              "The paid amount should be equal to the Debit or Credit total.",
            icon: "error",
          });
        }
      } else {
        swal.fire({
          title: "Error!",
          html: "Please make sure that Debit and credit amount is balanced.",
          icon: "error",
        });
      }
    });
  };

  var _add_ons = function () {
    datepicker();
  };

  var load_multiple_data = function(){
    var properties = $('#properties').val();
    var projects = $('#projects').val();

    add_options(properties,'properties','#property_id');
    add_options(projects,'projects','#project_id');
  }
  var add_options = function (data, type, identifier){
    var element = $(identifier);
    var array_data = data.split(',');
    var array_length = array_data.length;
    for (let i=0; i<array_length; i++){
        $.ajax({
            url: base_url + 'generic_api/fetch_specific',
            dataType: 'JSON',
            data: {
                table: type,
                field: 'id',
                value: array_data[i],
                limit: '1',
                select: 'name'
            },
            async:false,
            success: function (res){
                var element_option = new Option(res[0]['name'], array_data[i]);
                element.append(element_option);
            }
        })
    }
    element.val(array_data).trigger('change');
}
  // Public functions
  return {
    init: function () {
      datepicker();
      // formRepeater();
      entryFormRepeater();
      _add_ons();

      wizardEl = KTUtil.get("kt_wizard_v3");
      formEl = $("#form_payment_voucher");

      initWizard();
      initValidation();
      initSubmit();

      load_multiple_data();
    },
  };
})();

// Initialization
jQuery(document).ready(function () {
  Create.init();
});

const dTotalInput = document.querySelector("#dTotal");
const cTotalInput = document.querySelector("#cTotal");
const grossAmount = document.querySelector("#gross_amount");
const addEntryBtn = document.querySelector("#next_btn");

const totalOfItemAmount = (el) => {
  var total = 0;

  el.forEach((item) => (total += parseFloat($(item).val().trim() || 0)));
  if (total != 0) {
    grossAmount.value = total;
  }
};

addEntryBtn.addEventListener("click", function () {
  const amountEl = document.querySelectorAll("#item_total_amount");
  totalOfItemAmount(amountEl);
});

const getItems = () => {
  // Get the element again
  const entryItemLedgerSelect = document.querySelectorAll("#item_id");
  // Loop through the element
  entryItemLedgerSelect.forEach((select) => {
    $.ajax({
      url: base_url + "item/get_all_items",
      type: "GET",
      success: function (data) {
        const items = JSON.parse(data);
        items.data.map((item) => {
          let opt = document.createElement("option");

          opt.value = item.id;
          opt.innerHTML = item.name;
          select.appendChild(opt);
        });
      },
    });
  });
};

const getLedgers = () => {
  // Get the element again
  const entryItemLedgerSelect = document.querySelectorAll("#entry_item_ledger");
  // Loop through the element
  entryItemLedgerSelect.forEach((select) => {
    $.ajax({
      url: base_url + "accounting_entries/get_all_ledgers",
      type: "GET",
      success: function (data) {
        const ledgers = JSON.parse(data);
        ledgers.data.map((ledger) => {
          let opt = document.createElement("option");

          opt.value = ledger.id;
          opt.innerHTML = ledger.name;
          select.appendChild(opt);
        });
      },
    });
  });
};

const paymentType = () => {
  const entryItemDcSelect = document.querySelectorAll("#entry_item_dc");
  const dInput = document.querySelectorAll("#dr_amount_input");
  const cInput = document.querySelectorAll("#cr_amount_input");

  entryItemDcSelect.forEach((select, index) => {
    $(select).on("change", function () {
      if (this.value !== "") {
        if (this.value == "d") {
          cInput[index].disabled = true;
          cInput[index].value = 0;
          dInput[index].disabled = false;
        } else if (this.value == "c") {
          cInput[index].disabled = false;
          dInput[index].disabled = true;
          dInput[index].value = 0;
        }
      }
    });
  });
};

const calculateDTotal = () => {
  const dInput = document.querySelectorAll("#dr_amount_input");
  // dTotalInput.value = 0;
  $(dInput).keyup(function () {
    var total = 0;

    $(dInput).each(function () {
      total += parseFloat($(this).val() || 0);
    });
    dTotalInput.value = total.toFixed(2);
  });
};

const calculateCTotal = () => {
  const cInput = document.querySelectorAll("#cr_amount_input");
  // cTotalInput.value = 0;
  $(cInput).keyup(function () {
    var total = 0;

    $(cInput).each(function () {
      total += parseFloat($(this).val() || 0);
    });
    cTotalInput.value = total.toFixed(2);
  });
};

const listItemDetail = () => {
  const listItemSelect = document.querySelectorAll("#item_id");
  const unitPrice = document.querySelectorAll("#item_unit_price");
  const quantity = document.querySelectorAll("#item_quantity");
  const taxID = document.querySelectorAll("#item_tax_id");
  const taxName = document.querySelectorAll("#tax_name");
  const taxAmount = document.querySelectorAll("#item_tax_amount");
  const totalAmount = document.querySelectorAll("#item_total_amount");

  listItemSelect.forEach((select, index) => {
    $(select).on("change", function () {
      const item = getSelectedItem(this.value);
      unitPrice[index].value = item["unit_price"];
      taxID[index].value = item["tax_id"];
      taxName[index].value = item["tax"]["name"];
      taxAmount[index].value = item["total_price"] - item["unit_price"];
      totalAmount[index].value = item["total_price"];
    });
  });
};

const calculateItemTotalPrice = () => {
  const listItemSelect = document.querySelectorAll("#item_quantity");
  const unitPrice = document.querySelectorAll("#item_unit_price");
  const taxAmount = document.querySelectorAll("#item_tax_amount");
  const totalAmount = document.querySelectorAll("#item_total_amount");

  listItemSelect.forEach((select, index) => {
    $(select).on("change keyup blur", function () {
      const total =
        (parseFloat(unitPrice[index].value) +
          parseFloat(taxAmount[index].value)) *
        this.value;
      totalAmount[index].value = total.toFixed(2);
    });
  });
};

function getSelectedItem(itemID) {
  $.ajax({
    url: base_url + "item/Item/get_item",
    type: "post",
    data: {
      itemID,
      itemID,
    },
    dataType: "json",
    complete: function () {},
    success: function (json) {
      item = json;
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(
        thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText
      );
    },
    timeout: 10000,
    async: false,
  });

  return item;
}

listItemDetail();
calculateItemTotalPrice();
getItems();
getLedgers();
paymentType();
calculateDTotal();
calculateCTotal();
