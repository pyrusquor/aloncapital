'use strict';
var PaymentVoucher = (function () {
    var paymentvoucherTable = function () {
        var table = $('#paymentvoucher_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            // ajax: base_url + 'payment_voucher/showPaymentVouchers',
            ajax: {
                url:base_url + 'payment_voucher/showPaymentVouchers',
                data: function(data){
                   // Read values
                   var paid_date = $('#kt_pv_daterangepicker').val();
         
                   // Append to data
                   data.paid_date = paid_date;
                }
             },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'payment_type_id',
                },
                {
                    data: 'payee',
                },
                {
                    data: 'payee_type',
                },
                {
                    data: 'paid_date',
                },
                {
                    data: 'paid_amount',
                },
                {
                    data: 'particulars',
                },
                {
                    data: 'check_id'
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var additional_options = '';
                        if(row.cheque_voucher_number!==null){
                            additional_options=
                            `<a class="dropdown-item" href="` +
                            base_url +
                            `payment_voucher/print_cv/` +
                            row.id +
                            `" target="_blank" rel="noopener noreferrer"><i class="la la-edit"></i> Print Cheque Voucher </a>
                            <a class="dropdown-item" href="` +
                            base_url +
                            `payment_voucher/print_cheque/` +
                            row.id +
                            `" target="_blank" rel="noopener noreferrer"><i class="la la-edit"></i> Print Cheque </a>`
                        }

                        return (
                            `
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="` +
                            base_url +
                            `payment_voucher/printable/` +
                            row.id +
                            `" target="_blank" rel="noopener noreferrer"><i class="la la-edit"></i> Print </a>
							` + 
                            additional_options + 
                            `<a href="javascript:void(0);" class="dropdown-item remove_paymentvoucher" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete </a>
									</div>
								</span>
								<a href="` +
                            base_url +
                            `payment_voucher/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, 5, -1],
                    className: 'dt-center',
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },

                /* ==================== begin: Add target fields for dropdown value ==================== */
                {
                    targets: 3,
                    render: function (data, type, row, meta) {
                        var voucher = {
                            1: {
                                title: 'Cash',
                            },
                            2: {
                                title: 'Regular Cheque',
                            },
                            3: {
                                title: 'Post Dated Cheque',
                            },
                            4: {
                                title: 'Online Deposits',
                            },
                            5: {
                                title: 'Wire Transfer',
                            },
                            6: {
                                title: 'Cash & Cheque',
                            },
                            7: {
                                title: 'Cash & Bank Remittance',
                            },
                            8: {
                                title: 'Journal Voucher',
                            },
                            9: {
                                title: 'Others',
                            },
                            10: {
                                title: 'Direct Deposit',
                            },
                        };

                        if (typeof voucher[data] === 'undefined') {
                            return ``;
                        }
                        return voucher[data].title;
                    },
                },

                {
                    targets: [9],
                    visible: false,
                },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_paymentvoucher', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'payment_voucher/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#paymentvoucher_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on('click', '#btn_upload_guide', function () {
            var table = $('#upload_guide_table');

            table.DataTable({
                order: [[0, 'asc']],
                pagingType: 'full_numbers',
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + 'payment_voucher/get_table_schema',
                columns: [
                    // {data: 'button'},
                    {
                        data: 'no',
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'type',
                    },
                    {
                        data: 'format',
                    },
                    {
                        data: 'option',
                    },
                    {
                        data: 'required',
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _filterLandInventory = function () {
        var dTable = $('#paymentvoucher_table').DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($('#_column_' + n).val())
                .draw();
        }

        $('._filter').on('keyup change clear', function () {
            if ($(this).is('input')) {
                let val = $('#_column_' + $(this).data('column')).val();

                dTable.search(val).draw();
            } else {
                _colFilter($(this).data('column'));
            }
        });
    };

    var _selectProp = function () {
        var _table = $('#paymentvoucher_table').DataTable();
        var _buttons = _table.buttons(['.bulkDelete']);

        $('#select-all').on('click', function () {
            // var rows = _table.rows({ 'search': 'applied' }).nodes();

            // $('input[type="checkbox"]').prop( 'checked', this.checked );
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
            } else {
                $('.delete_check').prop('checked', false);
            }
        });

        $('#paymentvoucher_table tbody').on(
            'change',
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $('input#select-all').get(0);

                    if (el && el.checked && 'indeterminate' in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'payment_voucher/bulkDelete/',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#paymentvoucher_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    var _add_ons = function () {
        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var daterangepickerInit = function () {
        if ($('#kt_pv_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_pv_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                // range = start.format("MMM D") + " - " + end.format("MMM D");
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                },
                opens: 'left',
                // ranges: {
                // 	'Today': [moment(), moment()],
                // 	'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                // 	'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                // 	'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                // 	'This Month': [moment().startOf('month'), moment().endOf('month')],
                // 	'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                // }
            },
            cb
        );

        cb(start, end, '');

        $(picker).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(
                picker.startDate.format('MM/DD/YYYY') +
                    ' - ' +
                    picker.endDate.format('MM/DD/YYYY')
            );
            var table = $('#paymentvoucher_table');
            var oTable = table.DataTable();
            oTable.draw();
        });

        $(picker).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            var table = $('#paymentvoucher_table');
            var oTable = table.DataTable();
            oTable.draw();
        });
    };

    var load_filters = function(){
        var paid_date = localStorage.getItem('accounting_date');
        if(paid_date!==null){
            $('#kt_pv_daterangepicker').val(paid_date);
            var table = $('#paymentvoucher_table');
            var oTable = table.DataTable();
            oTable.draw();
        }
        localStorage.removeItem('accounting_date');

    }

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            paymentvoucherTable();
            _filterLandInventory();
            _selectProp();
            _add_ons();
            upload_guide();
            status();
            daterangepickerInit();
            load_filters();
        },
    };
})();

jQuery(document).ready(function () {
    PaymentVoucher.init();
});
