// Class definition
var FormGen = function() {
    // Private functions
    var sgSelect = function() {
        
		// Seller Type
		$('#table_list').select2({
            placeholder: "Select a table",
            allowClear: true
        });

         $(document).on('select2:select', '#table_list', function (e) {
            var TblName = $(this).val();
            var tableContent = '';
            
            $.ajax({
                url: base_url + 'communication_templates/get_table_fields/' + TblName,
                dataType: "JSON",
                type: 'GET',
                success: function (data) {

                    $.each(data, function (key, item) {
                        tableContent += '<tr>';
                        tableContent += '<td>' + item + '</td>';
                        tableContent += '<td class="kt-align-center"><button type="button" class="btn btn-primary btn-xs table_field" data-field="'+ key +'">Insert</button></td>';
                        tableContent += '</tr>';
                    });

                    $('#tbody').html(tableContent);

                }
            });
            
		});

    }
    
    var tinyMCE = function() {

        tinymce.init({
            mode : "none",
            selector:'textarea#contentEditor',
            height: 500,
            
            // ===========================================
            // INCLUDE THE PLUGIN
            // ===========================================
            style_formats: [
                {
                    title: 'Custom Line Height',
                    items: [
                      {
                        title: `Default`,
                        inline: 'span',
                        styles: { 'line-height': `normal`, display: 'inline-block' }
                      },
                      {
                        title: `0px`,
                        inline: 'span',
                        styles: { 'line-height': `0px`, display: 'inline-block' }
                      },
                      {
                        title: `5px`,
                        inline: 'span',
                        styles: { 'line-height': `5px`, display: 'inline-block' }
                      },
                      {
                        title: `10px`,
                        inline: 'span',
                        styles: { 'line-height': `10px`, display: 'inline-block' }
                      },
                      {
                        title: `15px`,
                        inline: 'span',
                        styles: { 'line-height': `15px`, display: 'inline-block' }
                      }
                    ]
                }
            ],  
            plugins: [
                "code advlist autolink accordion link image lists charmap print preview hr pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste jbimages table advtable"
            ],
            toolbar: "formatselect | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | responsivefilemanager | pastetext | fontselect | fontsizeselect | preview | accordion | code ", 
            
            /* enable title field in the Image dialog*/
              image_title: true,
              /* enable automatic uploads of images represented by blob or data URIs*/
              automatic_uploads: true,
              /*
                URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
                images_upload_url: 'postAcceptor.php',
                here we add custom filepicker only to Image dialog
              */
              file_picker_types: 'image',
              /* and here's our custom image picker*/
            file_picker_callback: function (cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                /*
                  Note: In modern browsers input[type="file"] is functional without
                  even adding it to the DOM, but that might not be the case in some older
                  or quirky browsers like IE, so you might want to add it to the DOM
                  just in case, and visually hide it. And do not forget do remove it
                  once you do not need it anymore.
                */

                input.onchange = function () {
                  var file = this.files[0];

                  var reader = new FileReader();
                  reader.onload = function () {
                    /*
                      Note: Now we need to register the blob in TinyMCEs image blob
                      registry. In the next release this part hopefully won't be
                      necessary, as we are looking to handle it internally.
                    */
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    /* call the callback and populate the Title field with the file name */
                    cb(blobInfo.blobUri(), { title: file.name });
                  };
                  reader.readAsDataURL(file);
                };

                input.click();
            },
        });

        $(document).on('click', '.table_field', function (e) {
           
            var field_selected = $(this).attr('data-field');

            if(field_selected == null ){
		
                modal_trigger('Warning','Select a Field!');
                
            } else {
                
                /* console.log($.inArray(field_selected,existing_fields));
                if($.inArray(field_selected,existing_fields) >= 0){
                    
                    alert('ALREADY EXIST!');
                    
                } else { */
                
                    tinymce.activeEditor.execCommand('mceInsertContent', false, "{{"+field_selected+"}}");
                    existing_fields.push(field_selected);
                
                //  } 
            }
		});

    }

    var valForm = function () {
        $( "#form_generator" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true 
                },
                description: {
                    required: true 
                },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }

    // Public functions
    return {
        init: function() {
            valForm();
            sgSelect();
            tinyMCE();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    FormGen.init();
});