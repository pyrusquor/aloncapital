"use strict";
var Communication_templates = function () {

	var commTempTable = function () {
		var table = $('#commtemp_table');

		// begin first table
		table.DataTable({
			responsive: true,
			pageLength: 10,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			deferRender: true,
			dom:	"<'row'<'col-sm-12 col-md-6'l>>" +
						// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
						"<'row'<'col-sm-12'tr>>" +
						"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			language: {
				'lengthMenu': 'Show _MENU_',
        'infoFiltered': '(filtered from _MAX_ total records)'
			},
			ajax: base_url + 'communication_templates/showData',
			columns: [{
					data: 'id'
				},
				{
					data: 'name'
				},
				{
					data: 'description'
				},
				{
					data: 'type'
				},
				{
					data: 'created_at'
				},
				{
					data: 'Actions',
					responsivePriority: -1
				},
			],
			columnDefs: [{
				targets: -1,
				title: 'Actions',
				orderable: false,
				render: function (data, type, row, meta) {
					return `
							<span class="dropdown">
								<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
								<i class="la la-ellipsis-h"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a href="` + base_url + `communication_templates/update/` + row.id + `"  class="dropdown-item"><i class="fa flaticon-edit"></i> Update </a>
									<a href="javascript:void(0);" class="dropdown-item remove_template" data-id="` + row.id + `"><i class="fa flaticon-delete"></i> Delete </a>
								</div>
							</span>
							<a href="` + base_url + `communication_templates/view/` + row.id + `" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
							<i class="fa flaticon-eye"></i>
							</a>`;
				},
			}, ],
			drawCallback: function(settings) {
				$('#total').text( settings.fnRecordsTotal() + ' TOTAL' );
			}
		});

		var oTable = table.DataTable();
		$('#generalSearch').keyup(function(){
			oTable.search($(this).val()).draw() ;
		});
	
	};

	var confirmDelete = function () {
		$(document).on('click', '.remove_template', function () {
			var id = $(this).data('id');

			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then(function (result) {
				if (result.value) {

					$.ajax({
						url: base_url + 'communication_templates/delete/' + id,
						type: 'POST',
						dataType: "JSON",
						data: { id: id },
						success: function (res) {
							if (res.status) {
								$('#commtemp_table').DataTable().ajax.reload();
								swal.fire(
									'Deleted!',
									res.message,
									'success'
								);
							} else {
								swal.fire(
									'Oops!',
									res.message,
									'error'
								)
							}
						}
					});

				} else if (result.dismiss === 'cancel') {
					swal.fire(
						'Cancelled',
						'Your imaginary file is safe :)',
						'error'
					)
				}
			});
		});
	};

	return {

		//main function to initiate the module
		init: function () {
			confirmDelete();
			commTempTable();
		},

	};

}();

jQuery(document).ready(function () {
	Communication_templates.init();
});