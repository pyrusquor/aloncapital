"use strict";
var Document = (function () {

    let tableElement = $('#alert_documents_table');

    var DocumentTable = function () {

        var dataTable = tableElement.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'alerts/showDocumentItems',
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
    
            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'file_name',
                },
                {
                    data: 'number_of_days',
                },
                {
                    data: 'period_id',
                },
                {
                    data: 'due_date',
                },
                {
                    data: 'created_at',
                },
            ],
            columnDefs: [
                // {
                //     targets: -1,
                //     title: 'Actions',
                //     searchable: false,
                //     orderable: false,
                //     render: function (data, type, row, meta) {
                //         return (
                //             `
                //                 <span class="dropdown"><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
                //                     <div class="dropdown-menu dropdown-menu-right">
                //                         <a class="dropdown-item" target="_BLANK" href="` + base_url + `transaction_payment/form/` + row.id + `"><i class="la la-edit"></i> Collect </a>
                //                     </div>
                //                 </span>
                //                 <a target="_BLANK" href="` + base_url + `transaction/view/` + row.transaction_id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-eye"> </i> </a>`
                //         );
                //     },
                // },
                // {
                //     targets: [0, 6, 7, 8],
                //     className: 'dt-center',
                //     // orderable: false,
                // },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });
    
        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    return {
        //main function to initiate the module
        init: function () {
            DocumentTable();
            filter()
        },
    };
})();

jQuery(document).ready(function () {
    Document.init();
});