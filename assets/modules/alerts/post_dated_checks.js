"use strict";
var TransactionPayment = (function () {
    var TransactionPaymentTable = function () {
        var table = $('#transaction_payment_table');
    
        // begin first table
        table.DataTable({
            order: [[6, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
    
            // ajax: base_url + 'alerts/showPostDatedChecks',
            ajax: {
                url:base_url + 'alerts/showPostDatedChecks',
                data: function(data){
                   // Read values
                   var project_id = $('#alerts_project_id').val();
         
                   // Append to data
                   data.project_id = project_id;
                }
             },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
    
            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'transaction',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'buyer_id',
                },
                {
                    data: 'bank',
                },
                {
                    data: 'number',
                },
                {
                    data: 'amount',
                },
                {
                    data: 'status',
                },
                {
                    data: 'period',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
                                <span class="dropdown"><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" target="_BLANK" href="` + base_url + `transaction_payment/form/` + row.transaction_id + `"><i class="la la-edit"></i> Collect </a>
                                    </div>
                                </span>
                                <a target="_BLANK" href="` + base_url + `transaction/view/` + row.transaction_id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-eye"> </i> </a>`
                        );
                    },
                },
                {
                    targets: [0, 6, 7, 8],
                    className: 'dt-center',
                    // orderable: false,
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });
    
        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };
    var load_filters = function (){
        var table = $('#transaction_payment_table');
    
        var oTable = table.DataTable();
        var project_id = localStorage.getItem('financials_project_id');
        var project_name = localStorage.getItem('financials_project_name');
        
        if(project_id!==null){
            $('#alerts_project_id').append(new Option(project_name, project_id,false,true));
            oTable.draw();
    
        }
        localStorage.removeItem('financials_project_id');
        localStorage.removeItem('financials_project_name');
    }

    return {
        //main function to initiate the module
        init: function () {
            TransactionPaymentTable();
            load_filters();
        },
    };
})();

jQuery(document).ready(function () {
    TransactionPayment.init();
});