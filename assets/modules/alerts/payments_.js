"use strict";
var Property = (function () {
    

    var generalSearch = function () {
        // General Search
        function load_data(keyword) {
            var page_num = page_num ? page_num : 0;

            var keyword = keyword ? keyword : "";
            $.ajax({
                url: base_url + "property/paginationData/" + page_num,
                method: "POST",
                data: "page=" + page_num + "&keyword=" + keyword,
                success: function (html) {
                    KTApp.block("#kt_content", {
                        overlayColor: "#000000",
                        type: "v2",
                        state: "primary",
                        message: "Processing...",
                        css: {
                            padding: 0,
                            margin: 0,
                            width: "30%",
                            top: "40%",
                            left: "35%",
                            textAlign: "center",
                            color: "#000",
                            border: "3px solid #aaa",
                            backgroundColor: "#fff",
                            cursor: "wait",
                        },
                    });

                    setTimeout(function () {
                        $("#propertyContent").html(html);

                        KTApp.unblock("#kt_content");
                    }, 2000);
                },
            });
        }

        $("#generalSearch").keyup(function () {
            var keyword = $(this).val();

            if (keyword !== "") {
                load_data(keyword);
            } else {
                load_data();
            }
        });
    };

    var advanceFilter = function () {
        function filter(project_id, status_id, phase, cluster, block, lot) {
            filters = "";

            var page_num = page_num ? page_num : 0;
            var project_id = project_id ? project_id : "";
            var status_id = status_id ? status_id : "";
            var phase = phase ? phase : "";
            var cluster = cluster ? cluster : "";
            var block = block ? block : "";
            var lot = lot ? lot : "";

            $.ajax({
                url: base_url + "property/paginationData/" + page_num,
                method: "POST",
                data:
                    "page=" +
                    page_num +
                    "&project_id=" +
                    project_id +
                    "&status_id=" +
                    status_id +
                    "&phase=" +
                    phase +
                    "&cluster=" +
                    cluster +
                    "&block=" +
                    block +
                    "&lot=" +
                    lot,
                success: function (html) {
                    $("#filterModal").modal("hide");

                    KTApp.block("#kt_content", {
                        overlayColor: "#000000",
                        type: "v2",
                        state: "primary",
                        message: "Processing...",
                        css: {
                            padding: 0,
                            margin: 0,
                            width: "30%",
                            top: "40%",
                            left: "35%",
                            textAlign: "center",
                            color: "#000",
                            border: "3px solid #aaa",
                            backgroundColor: "#fff",
                            cursor: "wait",
                        },
                    });

                    setTimeout(function () {
                        $("#propertyContent").html(html);

                        KTApp.unblock("#kt_content");
                    }, 500);
                },
            });

            var filters =
                "project_id:" +
                project_id +
                ";status_id:" +
                status_id +
                ";phase:" +
                phase +
                ";cluster:" +
                cluster +
                ";block:" +
                block +
                ";cluster:" +
                lot +
                ";";

            $("#_export_csv #filters").val(filters);
            $("#_export_form #filters").val(filters);
        }

        $("#advanceSearch").submit(function (e) {
            e.preventDefault();

            var project_id = $("#filterProjectName").val();
            var status_id = $("#filterPropertyStatus").val();
            var phase = $("#filterPhase").val();
            var cluster = $("#filterCluster").val();
            var block = $("#filterBlock").val();
            var lot = $("#filterLot").val();

            if (
                project_id !== "" ||
                status_id !== "" ||
                phase !== "" ||
                cluster !== "" ||
                block !== "" ||
                lot !== ""
            ) {
                filter(project_id, status_id, phase, cluster, block, lot);
            } else {
                filter();
            }
        });
    };

  

return {
        //main function to initiate the module
        init: function () {
            generalSearch();
            advanceFilter();
        },
    };
})();

jQuery(document).ready(function () {
    Property.init();
});

function Pagination(page_num) {
    page_num = page_num ? page_num : 0;

    var keyword = $("#generalSearch").val();

    var project_id = $("#filterProjectName").val();
    var status_id = $("#filterPropertyStatus").val();
    var phase = $("#filterPhase").val();
    var cluster = $("#filterCluster").val();
    var block = $("#filterBlock").val();
    var lot = $("#filterLot").val();

    $.ajax({
        url: base_url + "alerts/paginationData/" + page_num,
        method: "POST",
        data:
            "page=" +
            page_num +
            "&project_id=" +
            project_id +
            "&status_id=" +
            status_id +
            "&phase=" +
            phase +
            "&cluster=" +
            cluster +
            "&block=" +
            block +
            "&lot=" +
            lot,
        success: function (html) {
            KTApp.block("#kt_content", {
                overlayColor: "#000000",
                type: "v2",
                state: "primary",
                message: "Processing...",
                css: {
                    padding: 0,
                    margin: 0,
                    width: "30%",
                    top: "40%",
                    left: "35%",
                    textAlign: "center",
                    color: "#000",
                    border: "3px solid #aaa",
                    backgroundColor: "#fff",
                    cursor: "wait",
                },
            });

            setTimeout(function () {
                $("#propertyContent").html(html);
                KTApp.unblock("#kt_content");
            }, 500);
        },
    });
}
