"use strict";
const mrr_status = [
    'Select Option',
    'PO Amended',
    'Item Received',
    'Wrong Delivery',
    'Returned to Supplier',
    'Partial Delivery'
];

function getQueryParams(query = window.location.search) {
    return query.replace(/^\?/, '').split('&').reduce((json, item) => {
        if (item) {
            item = item.split('=').map((value) => decodeURIComponent(value))
            json[item[0]] = item[1]
        }
        return json
    }, {})
}

function filter() {
    var values = $('#advanceSearch').serialize();
    var page_num = page_num ? page_num : 0;
    var filter_data = values ? getQueryParams(values) : '';
    filter_data.page = page_num;
    var url = base_url + "material_receiving/showMaterialReceivings?filter=true&with_relations=yes&" + values;
    return url;
}

var MaterialReceiving = (function () {
    var MaterialReceivingTable = function () {
        var table = $("#material_receiving_table");
        var url = base_url + "material_receiving/showMaterialReceivings?filter=true&with_relations=yes";

        // begin first table
        table.DataTable({
            order: [[0, "desc"]],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,

            ajax: url,
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        "btn btn-sm btn-label-primary btn-elevate btn-icon-sm",
                    attr: {
                        id: "bulkDelete"
                    },
                    enabled: false
                }
            ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)"
            },

            columns: [
                {
                    data: null
                },
                {
                    data: "id"
                },
                {
                    data: "reference"
                },
                /* ==================== begin: Add model fields ==================== */
                {
                    data: "purchase_order.reference"
                },
                {
                    data: "created_at"
                },
                {
                    data: "supplier.name"
                },
                {
                    data: "warehouse.name"
                },
                {
                    data: "total_cost"
                },
                {
                    data: "status"
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },

                /* ==================== end: Add model fields ==================== */
                {
                    data: "Actions",
                    responsivePriority: -1
                }
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            // `
                            // 	<span class="dropdown">
                            // 		<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                            // 		<i class="la la-ellipsis-h"></i>
                            // 		</a>
                            // 		<div class="dropdown-menu dropdown-menu-right">
                            // 			<a class="dropdown-item" href="` +
                            // base_url +
                            // `material_receiving/form/` +
                            // row.id +
                            // `"><i class="la la-edit"></i> Update </a>
                            // 			<a href="javascript:void(0);" class="dropdown-item remove_material_receiving" data-id="` +
                            // row.id +
                            // `"><i class="la la-trash"></i> Delete </a>
                            // 		</div>
                            // 	</span>
                            // 	<a href="` +
                            // base_url +
                            // `material_receiving/view/` +
                            // row.id +
                            // `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                            // 	<i class="la la-eye"></i>
                            // 	</a>`
                            `
                            <span class="dropdown">
                                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                                    <i class="la la-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="` + base_url + `material_receiving/form?po=` + row.purchase_order_id + `">
                                        New MRR based on this PO
                                    </a>
                                    <a class="dropdown-item" href="` + base_url + `material_receiving/view/` + row.id + `">View</a>
                                </div>` +
                            `</span>`
                        );
                    }
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: "dt-center"
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false
                },
                /* ==================== begin: Add target fields for dropdown value ==================== */
                {
                    targets: 4,
                    render: function (data, type, row, meta) {
                        return row.created_at;
                    }
                },
                {
                    targets: 8,
                    render: function (data, type, row, meta) {
                        return mrr_status[row.status];
                    }
                },
                {
                    targets: 3,
                    render: function(data, type, row, meta) {
                        if(row.purchase_order){
                            return row.purchase_order.reference;
                        }else{
                            if(row.receiving_type == "1"){
                                return "N/A; Direct Input";
                            }else if(row.receiving_type == "3"){
                                return "N/A; Transfer";
                            }else{
                                return "No PO";
                            }
                        }
                    }
                },
                {
                    targets: 5,
                    render: function(data, type, row, meta){
                        if(row.supplier){
                            return row.supplier.name;
                        }else{
                            return "No supplier specified";
                        }
                    }
                }
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            }
        });

        var oTable = table.DataTable();
        $("#generalSearch").keyup(function () {
            let s = $(this).val();
            var url = base_url + "material_receiving/showMaterialReceivings?filter=true&with_relations=yes&reference=" + s;
            if (s.length > 1) {
                var dTable = $("#material_receiving_table").DataTable();
                dTable.ajax.url(url).load();
            }
        });

        $('#advanceSearch').submit(function (e) {
            e.preventDefault();
            var dTable = $("#material_receiving_table").DataTable();
            dTable.ajax.url(filter()).load();
            $('#filterModal').modal('hide');

        });

        $("#resetFilters").click(function (e) {
            e.preventDefault();
            var dTable = $("#material_receiving_table").DataTable();
            var url = base_url + "material_receiving/showMaterialReceivings?filter=true&with_relations=yes";
            dTable.ajax.url(url).load();
        });
    };

    var confirmDelete = function () {
        $(document).on("click", ".remove_material_receiving", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "material_receiving/delete/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {id: id},
                        success: function (res) {
                            if (res.status) {
                                $("#material_receiving_table")
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        }
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on("click", "#btn_upload_guide", function () {
            var table = $("#upload_guide_table");

            table.DataTable({
                order: [[0, "asc"]],
                pagingType: "full_numbers",
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + "material_receiving/get_table_schema",
                columns: [
                    // {data: 'button'},
                    {data: "no"},
                    {data: "name"},
                    {data: "type"},
                    {data: "format"},
                    {data: "option"},
                    {data: "required"}
                ]
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $("#material_receiving_table").DataTable();

        $("#_search").on("keyup", function () {
            console.log("yow");
            dTable.search($(this).val()).draw();
        });

        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait"
                    }
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait"
                    }
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    var _filterMaterialReceiving = function () {
        var dTable = $("#material_receiving_table").DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($("#_column_" + n).val())
                .draw();
        }

        $("._filter").on("keyup change clear", function () {
            console.log("yow");
            _colFilter($(this).data("column"));
        });
    };

    var _selectProp = function () {
        var _table = $("#material_receiving_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#material_receiving_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "material_receiving/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {deleteids_arr: deleteids_arr},
                            success: function (res) {
                                if (res.status) {
                                    $("#material_receiving_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            }
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            MaterialReceivingTable();
            _add_ons();
            _filterMaterialReceiving();
            upload_guide();
            status();
            _selectProp();
        }
    };
})();

jQuery(document).ready(function () {
    MaterialReceiving.init();
});
