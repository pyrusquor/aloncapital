$(document).ready(function () {
    $("#filter_purchase_order_id").on("change", function (e) {
        app.changeFilter('purchase_order_id', $(this).val());
        // app.fetchPO(null);
    });

    $("#warehouse_id").on("change", function (e) {
        app.changeInfo('warehouse_id', $(this).val());
    });

    $("#filter_company_id").on("change", function (e) {
        app.changeFilter('company_id', $(this).val());
    });

    var daterangepickerInit = function () {
        if ($('#filter_request_date').length == 0) {
            return;
        }

        var picker = $('#filter_request_date');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                // range = start.format("MMM D") + " - " + end.format("MMM D");
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                },
                opens: 'left',
            },
            cb
        );

        cb(start, end, '');

        $(picker).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(
                picker.startDate.format('YYYY/MM/DD') +
                    '-' +
                    picker.endDate.format('YYYY/MM/DD')
            );

            app.changeFilter('created_at', picker.startDate.format('YYYY/MM/DD') + '-' + picker.endDate.format('YYYY/MM/DD'));
        });

        $(picker).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            app.changeFilter('created_at', picker.startDate.format('YYYY/MM/DD') + '-' + picker.endDate.format('YYYY/MM/DD'));
        });
    }; 

    daterangepickerInit()
});

const __po_filter_defaults = {
    purchase_order_id: null,
    company_id: null,
    created_at: null,
}

const __info_form_defaults = {
    id: null,
    warehouse_id: null,
    purchase_order_id: null,
    // accounting_ledger_id: null,
    supplier_id: null,
    landed_cost: 0.0,
    freight_amount: 0.0,
    total_cost: 0.0,
    input_tax: 0.0,
    status: 2,
    expiration_date: null
}

var app = new Vue({
    el: '#material_receiving_app',
    data: {
        object_request_id: null,
        object_status: 1,
        object_is_editable: 1,
        canvass_id: null,
        po: {
            filter: {},
            data: [],
            items: [],
            selected: null,
            loading: false,
        },
        info: {
            form: {},
            required: {
                'warehouse_id': 'Warehouse',
                // 'accounting_ledger_id': 'Expense Account',
                'purchase_order_id': 'Purchase Request',
                'supplier_id': "Supplier",
                "landed_cost": "Landed Cost",
                "freight_amount": "Freight Amount",
                "total_cost": "Total",
                "input_tax": "Input Tax",
                "status": "Status"
            },
            check: {
                is_valid: false,
                missing_fields: []
            },
            is_active: false,
        },
        items: {
            form: [],
            data: [],
            selected: null,
            selected_idx: null,
            required: {},
            check: {
                is_valid: false,
            },
            deleted_items: [],
            loading: false
        },
        sources: {}
    },
    watch: {
        "info.form.landed_cost": function (newValue, oldValue) {
            this.calculateTotalCost();
        },
        "info.form.freight_amount": function (newValue, oldValue) {
            if (newValue !== oldValue) {
                this.calculateTotalCost();
            }
        },
        "info.form.total_cost": function (newValue, oldValue) {
            this.calculateTax();
        },
        "info.form.warehouse_id": function (newValue, oldValue) {
            for(let i = 0; i < this.items.data.length; i++){
                this.items.data[i].warehouse_id = newValue;
            }
        },
        "info.form.status": function (newValue, oldValue) {
            this.$forceUpdate();
            if(newValue == 4 || newValue == 3){
                for(let i = 0; i < this.items.data.length; i++){
                    this.rejectItem(i);
                }
            }
            this.info.form.status = newValue;
        }
    },
    methods: {
        generateURL(func, id){
            return base_url + func + "/" + id;
        },

        // region init
        initObjectRequest() {
            this.object_request_id = window.object_request_id;
            this.object_status = window.object_status;
            this.object_is_editable = window.is_editable;

            if (window.po) {
                this.fetchPO(window.po);
            }

            this.fetchObjectRequest();
        },
        // endregion

        // regions modals
        modalAmendPOItem(idx) {
            let modal = $("#amend_item_modal");
            this.items.selected_idx = idx;
            this.items.selected = this.items.data[idx];
            modal.modal('show');
        },

        updateAmendedItem(id) {
            let url = base_url + "purchase_order_item/get/" + id;
            axios.get(url)
                .then(response => {
                    this.info.form.status = 1; // PO amended
                    response.data.purchase_order_item_id = id;
                    this.items.selected = response.data;
                    this.items.selected.status = 2;
                    this.items.data[this.items.selected_idx] = this.items.selected;

                    this.selectPO(0, false);
                    this.calculateLandingCost();

                    this.$forceUpdate();
                    this.resetItemForm();
                })
        },

        rejectItem(idx) {
            this.items.data[idx].previous_quantity = this.items.data[idx].quantity;
            this.items.data[idx].quantity = 0;
            this.items.data[idx].status = 3;

            if(this.info.form.status != 4 || this.info.form.status != 3){
                this.info.form.status = 5; // partial delivery
            }

            this.info.form.landed_cost = Number(this.info.form.landed_cost) - Number(this.items.data[idx].total_cost);
            this.items.data[idx].total_cost = 0.0;

            this.$forceUpdate();

        },

        amendItem() {
            let modal = $("#amend_item_modal");
            modal.modal('hide');
            let data = {
                'quantity': this.items.selected.quantity
            }
            let id = this.items.selected.purchase_order_item_id;
            let url = base_url + "purchase_order_item/amend/" + this.items.selected.purchase_order_item_id;

            let poid = this.items.selected.purchase_order_id;

            let app = this;

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.status) {
                        app.fetchPO(poid);
                        app.updateAmendedItem(id);

                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        });
                    } else {
                        swal.fire({
                            title: "Oooops!",
                            text: response.message,
                            type: "error",
                        });
                    }
                }
            })
        },

        // endregion

        // region items form
        addItemsToMaterialReceiving() {
            for (let i = 0; i < this.po.items.length; i++) {
                let item = this.po.items[i];
                item.status = 1;
                item.freight_cost = 0
                this.addItemToCart(true, item);
            }
            this.info.is_active = true;
        },

        removeItemFromMaterialReceiving(key) {
            let item = this.items.data[key];
            if (item.id) {
                this.items.deleted_items.push(item.id);
            }
            this.items.data.splice(key, 1);
            this.calculateLandingCost();
        },

        addItemToCart(isNew, data) {
            let item = {};

            if (isNew) {
                for (key in data) {
                    item[key] = data[key];
                }
                item.purchase_order_item_id = item.id;
                delete item['id'];
                delete item['created_by'];
                delete item['created_at'];
                delete item['updated_at'];
                delete item['updated_by'];
                delete item['deleted_by'];
                delete item['deleted_at'];

            } else {
                for (key in data) {
                    item[key] = data[key];
                }
            }

            if (item.id) {
                let idx = this.searchItemInStore(item.id);
                if (idx !== null) {
                    this.items.data[idx] = item;
                } else {
                    this.items.data.push(item);
                }
            } else {
                this.items.data.push(item);
            }
            this.$forceUpdate();

            this.calculateLandingCost();
            this.calculateTotalCost();

            this.resetItemForm();
        },

        updateSubTotal(key) {
            this.items.data[key].total_cost = this.items.data[key].unit_cost * this.items.data[key].quantity;
            this.$forceUpdate();

        },

        calculateLandingCost() {
            // this.info.form.landed_cost = this.po.selected.total;
            let total = 0.0;
            for(let i = 0; i < this.items.data.length; i++){
                let item = this.items.data[i];
                total += Number(item.total_cost);
            }
            this.info.form.landed_cost = total;
            this.$forceUpdate();
        },

        calculateTotalCost() {
            this.info.form.total_cost = (this.info.form.landed_cost * 1.0) + (this.info.form.freight_amount * 1.0);
            this.$forceUpdate();
        },

        calculateTax() {
            this.info.form.input_tax = Number((this.info.form.total_cost * 1.0) * .12).toFixed(2);
            this.$forceUpdate();
        },

        searchItemInStore(id) {
            for (let i = 0; i < this.items.data.length; i++) {
                if (id == this.items.data[i].id) {
                    return i;
                }
            }
            return null;
        },

        resetItemForm() {
            this.items.selected = null;
            this.items.selected_idx = null;
            this.items.loaded = null;
            this.items.form = {};
        },

        checkItemsForm() {
            this.items.check.is_valid = true;
            if (this.items.data.length <= 0) {
                this.items.check.is_valid = false;
            }
        },

        prepItemsFormData() {
            return this.items.data;
        },
        // endregion

        // region po filters
        changeFilter(key, val) {
            this.po.filter[key] = val;
        },

        resetPOFilter() {
            this.po.data = [];
            this.po.items = [];
            this.po.selected = null;
            this.items.data = [];
            for (var d in __po_filter_defaults) {
                this.po.filter[d] = __po_filter_defaults[d];
            }
            for (var d in __info_form_defaults) {
                this.info.form[d] = __info_form_defaults[d];
            }

            $("#select2-filter_purchase_order_id-container").attr("title", "");
            $("#select2-filter_purchase_order_id-container").html("Select");
        },

        fetchPO(poId) {
            
            if (!this.po.filter.purchase_order_id && !this.po.filter.company_id && !this.po.filter.created_at && !poId) {

                return false;
            }

            this.po.loading = true;

            let po = poId;
            if (!po) {
                po = this.po.filter.purchase_order_id;
            }

            let url = base_url + "purchase_order/search?with_relations=yes&status_list=2,5&id=" + (po ?? '') + "&company_id=" + (this.po.filter.company_id ?? '') + "&created_at=" + (this.po.filter.created_at ?? '');

            axios.get(url)
                .then(response => {
                    this.po.loading = false;
                    this.po.data = response.data;
                    if(window.po){
                        this.selectPO(0, true);
                    }
                })
        },

        selectPO(key, reload_items) {
            this.po.selected = this.po.data[key];
            this.info.form.warehouse_id = this.po.selected.warehouse_id;
            // this.info.form.accounting_ledger_id = this.po.selected.accounting_ledger_id;
            this.info.form.supplier_id = this.po.selected.supplier_id;
            this.info.form.purchase_order_id = this.po.selected.id;
            this.info.form.landed_cost = this.po.selected.total;
            this.info.form.freight_amount = 0.0;
            this.info.form.input_tax = 0.0;
            if (reload_items) {
                this.fetchItems();
            }
        },
        // endregion

        // region parsers
        parseObjectRequest(data) {
            if (data.length > 0) {
                this.info.form = data[0];
                this.fetchObjectRequestChildren();
                this.fetchPO(this.info.form.material_receiving_request_id);

                // if (this.object_request_id) {
                //     this.po.filter = window.material_receiving_request;
                //     this.filterPO();
                // }
            }
        },

        parseObjectRequestItems(data) {
            for (row in data) {
                this.addItemToCart(false, data[row]);
            }
        },

        // endregion

        // region fetchers
        lookupInject(table, field, val, ret_field) {
            let url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + val;
            let retval = null;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        if (ret_field) {
                            retval = response.data[0][ret_field];
                        } else {
                            retval = response.data;
                        }
                    }
                    return retval;
                })
        },

        fetchObjectRequest() {
            if (this.object_request_id) {
                let url = base_url + "material_receiving/search?id=" + this.object_request_id + "&with_relations=no";
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectRequest(response.data);


                        }
                    });
            } else {
                this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
            }
            // this.request_items.cart = JSON.parse(JSON.stringify(__request_items_data_defaults));
        },
        fetchObjectRequestChildren() {
            // let url = base_url + "generic_api/fetch_specific?table=material_receiving_items&field=material_receiving_id&value=" + this.object_request_id;
            let url = base_url + "material_receiving_item/get_all_by_po/" + this.object_request_id;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.parseObjectRequestItems(response.data);
                    }
                });
        },
        async fetchInfoObjects(table, field, value, store, order) {
            if (!order) {
                order = "name ASC";
            }
            let url = base_url + "generic_api/fetch_all?table=" + table + "&order=" + order;
            if (field && value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + value + "&order=" + order;
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        //this.info.data[store] = response.data;
                        this.$set(this.info.data, store, response.data);
                    } else {
                        this.$set(this.info.data, store, []);
                    }
                });
        },
        fetchItems() {
            this.items.loading = true;
            let po_id = this.po.selected.id;
            let url = base_url + "purchase_order_item/get_all_by_po/" + po_id + "?with_relations=yes&receiving_status=1,3";

            axios.get(url)
                .then(response => {
                    this.items.loading = false;
                    this.$set(this.po, "items", response.data);
                    this.$forceUpdate();
                })
        },

        fetchObjects(table, lookup_field) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.sources[table] = response.data;
                    } else {
                        this.sources[table] = [];
                    }
                })
        },

        // endregion

        // region info form
        changeInfo(key, val) {
            this.info.form[key] = val;
        },

        prepInfoFormData() {
            return this.info.form;
        },

        checkInfoForm() {
            this.info.check.missing_fields = [];
            this.info.check.is_valid = true;
            for (var key in this.info.required) {
                if (this.info.form[key] === null) {
                    this.info.check.missing_fields.push(this.info.required[key]);
                    this.info.check.is_valid = false;
                }
            }
        },
        submitRequest() {
            this.checkInfoForm();
            let message = '';
            if (!this.info.check.is_valid) {``
                message = 'Missing fields: ' + this.info.check.missing_fields.join(', ');
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            this.checkItemsForm();
            if (!this.items.check.is_valid) {
                message = 'Please add Canvass items';
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            let url = base_url + "material_receiving/process_create";
            if (this.object_request_id) {
                url = base_url + "material_receiving/process_update/" + this.object_request_id;
            }

            let data = {
                request: this.prepInfoFormData(),
                items: this.prepItemsFormData(),
                deleted_items: this.items.deleted_items
            }

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.status) {
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                base_url + "material_receiving"
                            );
                        });
                    }
                }
            })

        },

        prepInfoData() {

        },

        prepObjectRequestItems() {

        },

        searchField(fields, field) {
            for (key in fields) {
                if (field === key) {
                    return true;
                }
            }
            return false;
        },
        // endregion
        addFreightCost(idx) {

            swal.fire({
                title: 'Enter Freight Cost',
                input: 'number',
                inputAttributes: {
                  autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (input) => {

                    if (!input) {

                        swal.fire({
                            title: "Error!",
                            text: "Invalid value",
                            type: "error",
                        })

                        return false
                    }

                    this.items.data[idx].freight_cost = input
                },
                allowOutsideClick: () => !swal.isLoading()
            })
        },

        addExpirationDate(idx) {

            swal.fire({
                title: 'Enter Expiration Date',
                input: 'text',
                inputAttributes: {
                  autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                onOpen: () => {

                    $('.swal2-input').addClass('datepicker').blur()

                    $('.datepicker').datepicker({
                        rtl: KTUtil.isRTL(),
                        todayHighlight: true,
                        orientation: "bottom left",
                        templates: arrows,
                        locale: 'no',
                        format: 'yyyy-mm-dd',
                        autoclose: true,
                    });
                },
                preConfirm: (input) => {

                    if (!input) {

                        swal.fire({
                            title: "Error!",
                            text: "Invalid value",
                            type: "error",
                        })

                        return false
                    }

                    this.items.data[idx].expiration_date = input
                    this.$forceUpdate();
                },
                allowOutsideClick: () => !swal.isLoading()
            })
        },
    },
    mounted() {

        this.initObjectRequest();

        this.is_mounted = true;
        this.object_request_id = window.object_request_id;

        // this.resetPOFilter();
        this.fetchObjects('warehouses');

    }
});