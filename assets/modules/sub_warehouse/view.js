function getQueryParams(query = window.location.search) {
    return query.replace(/^\?/, '').split('&').reduce((json, item) => {
        if (item) {
            item = item.split('=').map((value) => decodeURIComponent(value))
            json[item[0]] = item[1]
        }
        return json
    }, {})
}

function filter() {
    var values = $('#advanceSearch').serialize();
    var page_num = page_num ? page_num : 0;
    var filter_data = values ? getQueryParams(values) : '';
    filter_data.page = page_num;

    return values;
}

jQuery(document).ready(function () {
    $("#transfer_warehouse_id").on('change', function (e) {
        view_app.setValue("warehouse_transfer", "target_id", $(this).val());
    });


});


const __defaults = {
    warehouse_transfer: {
        id: null,
        destination_type: "warehouse",
        destination_id: window.parent_id,
        source_type: null,
        source_id: null,
        quantity: 0,
        item_id: null
    },
    sub_warehouse_transfer: {
        id: null,
        destination_type: "sub_warehouse",
        destination_id: null,
        source_type: null,
        source_id: null,
        quantity: 0,
        item_id: null,
        warehouse_id: window.object_id
    }
}

const __checks = {
    warehouse_transfer: {
        id: ["ID", null],
        destination_id: ["Warehouse", null],
        quantity: ["Quantity", 0],
        item_id: ["Item", null]
    },
    sub_warehouse_transfer: {
        id: ["ID", null],
        warehouse_id: ["Warehouse", null],
        destination_id: ["Sub Warehouse", null],
        quantity: ["Quantity", 0],
        item_id: ["Item", null]
    }
}

var view_app = new Vue({
    el: '#sub_warehouse_inventory_content',
    data: {
        warehouse_items: {
            data: [],
        },
        store_groups: ["warehouse_transfer", "sub_warehouse_transfer"],
        data_store: {
            modal: null,
            warehouse_transfer: {
                form: {},
                is_valid: false,
                bad_fields: [],
                pk: "warehouse_id",
                check_url: base_url + "warehouse_inventory/get_item",
                transfer_url: base_url + "warehouse_inventory/transfer"
            },
            sub_warehouse_transfer: {
                form: {},
                is_valid: false,
                bad_fields: [],
                pk: "sub_warehouse_id",
                check_url: base_url + "sub_warehouse_inventory/get_item",
                transfer_url: base_url + "warehouse_inventory/transfer"
            }
        },
        sources: {}
    },
    watch: {},
    methods: {
        // region generic
        setValue(store_group, field, value) {
            this.data_store[store_group]["form"][field] = value;
            this.$forceUpdate();
        },

        setDefault(store_group) {
            let defaults = __defaults[store_group];
            for (f in defaults) {
                this.data_store[store_group]["form"][f] = defaults[f];
            }
            this.data_store[store_group]["is_valid"] = false;
            this.data_store[store_group]["bad_fields"] = [];
            this.$forceUpdate();
        },

        checkForm(store_group) {
            let checks = __checks[store_group];
            this.data_store[store_group]["is_valid"] = true;
            for (f in checks) {
                if (this.data_store[store_group]["form"][f] === checks[f][1]) {
                    this.data_store[store_group]["is_valid"] = false;
                    this.data_store[store_group]["bad_fields"].push(checks[f][0]);
                }
            }
            return this.data_store[store_group]["is_valid"];
        },

        checkItem(store_group, item_id) {
            let pk = this.data_store[store_group]["pk"];
            let url = this.data_store[store_group]["url"] + "?item_id=" + item_id + "id=" + this.data_store[store_group]["form"][pk];
            axios.get(url)
                .then(result => function () {
                    console.log(result);
                });
        },

        submitRequest(store_group) {

        },

        fetchDependents(table, field, store_group)
        {
            let value = this.data_store[store_group]["form"][field];
            this.fetchObjects(table, field, value, null);
        },

        fetchObjects(table, field, value, filter_out) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            if(field && value){
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + value;
            }
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        if (filter_out) {
                            let data = [];
                            for (let i in response.data) {
                                if (response.data[i].id != filter_out) {
                                    data.push(response.data[i]);
                                }
                            }
                            this.sources[table] = data;
                        } else {
                            this.sources[table] = response.data;
                        }

                    } else {
                        this.sources[table] = [];
                    }
                    this.$forceUpdate();
                })
        },
        // endregion

        // region warehouse items
        loadWarehouseItems(id) {
            $('#filterModal').modal('hide');
            this.warehouse_items.loading = true;
            var url = base_url + "sub_warehouse_inventory/showSubWarehouseInventories?filter=true&with_relations=yes";
            if (id) {
                url += "&sub_warehouse_id=" + window.object_id;
            } else {
                url += "&" + filter();
            }
            axios.get(url)
                .then(response => {
                    this.warehouse_items.loading = false;
                    this.warehouse_items.data = response.data.data;
                    this.warehouse_items.total = response.data.iTotalRecords;
                    this.warehouse_items.display = response.data.iTotalDisplayRecords;
                });
        },

        // endregion

        // region transfer
        prepTransfer(id, item_id, store_group) {
            this.setValue(store_group, "item_id", item_id);
            this.setValue(store_group, "id", id);
            this.setValue(store_group, "source_type", "sub_warehouse");
            this.setValue(store_group, "source_id", window.object_id);
            switch(store_group){
                case 'warehouse_transfer':
                    this.data_store.modal = $("#modalTransferToOtherWarehouse");
                    break;
                case 'sub_warehouse_transfer':
                    this.fetchDependents('sub_warehouses', 'warehouse_id', 'sub_warehouse_transfer');
                    this.data_store.modal = $("#modalTransferToSubWarehouse");
                    break;
            }
            this.data_store.modal.modal("show");
        },
        transfer(store_group) {
            let message = "";
            let data = this.data_store[store_group]["form"];
            this.data_store.modal.modal("hide");
            if (this.checkForm(store_group)) {
                let check_url = this.data_store[store_group]["check_url"] + "?item_id=" + this.data_store[store_group]["form"]["item_id"] + "&warehouse_id=" + window.object_id;
                let process_url = "";
                axios.get(check_url)
                    .then(response => {
                        let transfer_url = this.data_store[store_group]["transfer_url"];

                        $.ajax({
                            url: transfer_url,
                            type: 'POST',
                            dataType: 'JSON',
                            data: data,
                            success: function (response) {
                                if (response.status) {
                                    swal.fire({
                                        title: "Success!",
                                        text: response.message,
                                        type: "success",
                                    });
                                    this.setDefault(store_group);
                                    this.loadWarehouseItems(window.object_id);
                                    this.$forceUpdate();
                                } else {
                                    swal.fire({
                                        title: "Oooops!",
                                        text: response.message,
                                        type: "error"
                                    });
                                }
                            }
                        })

                    });
            } else {
                message = 'Missing fields: ' + this.data_store[store_group]["bad_fields"].join(', ');
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });

                return false;
            }
        }
        // endregion
    },
    mounted() {
        this.fetchObjects("warehouses", null, null, null);
        // this.fetchObjects("sub_warehouses", null, null, null);
        for (sg in this.store_groups) {
            this.setDefault(this.store_groups[sg]);
        }
        this.parent_id = window.parent_id;
        this.loadWarehouseItems(window.object_id);
    }
});