"use strict";
var CommunicationSetting = function () {
	const getCommunicationSettings = () => {
		$.ajax({
		  url: base_url + "communication_settings/get_all",
		  type: "GET",
		  success: function (data) {
			const categories = JSON.parse(data);
		
			$("#communication_settings_data").val(JSON.stringify(categories));


			$('input[type=radio][name=overdue_payment_period]').change();
			$('input[type=radio][name=upcoming_payment]').change();
		  },
		});
	  };
	return {
		//main function to initiate the module
		init: function () 
		{
			getCommunicationSettings();
    	},
	};

}();

jQuery(document).ready(function () {
	
	$("[class='btn-switch']").bootstrapSwitch({
		size: 'small',
		offColor: 'danger',
		disabled: 'true'
	});

	$('input[type=radio][name=overdue_payment_period]').change(function() {
		
		let settings = JSON.parse($("#communication_settings_data").val());
		let result = settings.data.filter(d=> d.period_id == this.value && d.setting_slug === "overdue_payments");
		var cols = [];
		var header = ["Date to Send", "SMS Communication", "Email Communication"];

		$("#overdue_payment_update").attr("href", base_url + "communication_settings/form/" + this.value + "/overdue_payments");

		for(var i = 0; i < result.length; i++) {
			for(var k in result[i]) {
				if(cols.indexOf(k) === -1) {
					cols.push(k);
				}
			}

			// Create a table element
			var table = document.createElement("table");
			table.classList.add("table");

			// Create table row tr element of a table 
			var tr = table.insertRow(-1); 

			// Adding table header
			for(var i = 0; i < 3; i++ ) {
				var theader = document.createElement("th");
				theader.classList.add("text-center", "kt-widget13__text", "kt-widget13__text--bold");
				theader.setAttribute("colspan", "3");
				theader.innerHTML = header[i];
				tr.appendChild(theader); 
			}

			// Adding the data to the table 
            for (var i = 0; i < result.length; i++) { 

                // Create a new row 
                var trow = table.insertRow(-1); 
                for (var j = 0; j < cols.length; j++) { 
                	var cell = trow.insertCell(-1); 
					
					if(cols[j] == "days_to_send") {
						cell.style.width = "15%";
						cell.innerHTML = result[i][cols[j]] + " Day(s) " + result[i][cols[j+1]] + " Due Date";
					} else if(cols[j] == "sms_template" || cols[j] == "email_template") {
						cell.style.width = "25%";
						let content = result[i][cols[j]]
						cell.innerHTML = content.replace(/(.{50})..+/, "$1&hellip;");
					} else if(cols[j] == "sms" || cols[j] == "email") {
						cell.style.width = "10%";
						cell.innerHTML = `<input type="checkbox" name="sms_switch" class="btn-switch" ${result[i][cols[j]] == '1' ? 'checked' : ''}  >`;
					} else if (cols[j] == "days_to_send_type" || cols[j] == "period_id" || cols[j] == "setting_slug" || cols[j] == "sms_template_id" || cols[j] == "email_template_id") {
						continue
					} else {
						// Inserting the cell at particular place 
						cell.innerHTML = result[i][cols[j]]; 
					}
					
                } 
            } 
            // Add the newely created table containing json data 
            var el = document.getElementById("overdue_payment_table"); 
            el.innerHTML = ""; 
            el.appendChild(table); 
		}

		$("[class='btn-switch']").bootstrapSwitch({
			size: 'small',
			offColor: 'danger',
			disabled: 'true'
		});
	});


	$('input[type=radio][name=upcoming_payment]').change(function() {
		
		let settings = JSON.parse($("#communication_settings_data").val());
		let result = settings.data.filter(d=> d.period_id == this.value && d.setting_slug === "upcoming_payments");
		var cols = [];
		var header = ["Date to Send", "SMS Communication", "Email Communication"];

		$("#upcoming_payment_update").attr("href", base_url + "communication_settings/form/" + this.value + "/upcoming_payments");

		for(var i = 0; i < result.length; i++) {
			for(var k in result[i]) {
				if(cols.indexOf(k) === -1) {
					cols.push(k);
				}
			}

			// Create a table element
			var table = document.createElement("table");
			table.classList.add("table");

			// Create table row tr element of a table 
			var tr = table.insertRow(-1); 

			// Adding table header
			for(var i = 0; i < 3; i++ ) {
				var theader = document.createElement("th");
				theader.classList.add("text-center", "kt-widget13__text", "kt-widget13__text--bold");
				theader.setAttribute("colspan", "3");
				theader.innerHTML = header[i];
				tr.appendChild(theader); 
			}

			// Adding the data to the table 
            for (var i = 0; i < result.length; i++) { 

                // Create a new row 
                var trow = table.insertRow(-1); 
                for (var j = 0; j < cols.length; j++) { 
                	var cell = trow.insertCell(-1); 
					
					if(cols[j] == "days_to_send") {
						cell.style.width = "15%";
						cell.innerHTML = result[i][cols[j]] + " Day(s) " + result[i][cols[j+1]] + " Due Date";
					} else if(cols[j] == "sms_template" || cols[j] == "email_template") {
						cell.style.width = "25%";
						let content = result[i][cols[j]]
						cell.innerHTML = content.replace(/(.{50})..+/, "$1&hellip;");
					} else if(cols[j] == "sms" || cols[j] == "email") {
						cell.style.width = "10%";
						cell.innerHTML = `<input type="checkbox" name="sms_switch" class="btn-switch" ${result[i][cols[j]] == '1' ? 'checked' : ''}  >`;
					} else if (cols[j] == "days_to_send_type" || cols[j] == "period_id" || cols[j] == "setting_slug" || cols[j] == "sms_template_id" || cols[j] == "email_template_id") {
						continue
					} else {
						// Inserting the cell at particular place 
						cell.innerHTML = result[i][cols[j]]; 
					}
					
                } 
            } 
            // Add the newely created table containing json data 
            var el = document.getElementById("upcoming_payment_table"); 
            el.innerHTML = ""; 
            el.appendChild(table); 
		}

		$("[class='btn-switch']").bootstrapSwitch({
			size: 'small',
			offColor: 'danger',
			disabled: 'true'
		});
	});

	
	
	CommunicationSetting.init();
});

