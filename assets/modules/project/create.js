// Class definition
var project = function() {

    var valProject = function () {
        $( "#project_form" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true 
                },
                type: {
                    required: true 
                },
                company_id: {
                    required: true
                },
                location: {
                    required: true
                },
				min_price: {
                    number: true
                },
                max_price: {
                    number: true
                },
                status: {
                    required: true 
                },
                project_area: {
                    number: true
                },
                saleable_area: {
                    number: true
                },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }

    var _add_ons = function () {

        $('.kt_datepicker').datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
			format: 'yyyy-mm-dd',

        });

    }
	
	// Public functions
    return {
        init: function() {
            valProject();
            _add_ons();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    project.init();
});