"use strict";
var Blocks = (function () {
    var percentage_slider = document.getElementById('percentage');
    var price_range_slider = document.getElementById('price_range');
    var price_floor = document.getElementById('price_floor');
    var price_ceiling = document.getElementById('price_ceiling');
    var progress_floor = document.getElementById('progress_floor');
    var progress_ceiling = document.getElementById('progress_ceiling');
    noUiSlider.create(percentage_slider, {
        range: {
            'min': 0,
            'max': 100
        },
    
        step: 10,
    
        // Handles start at ...
        start: [0, 100],
    
        // ... must be at least 300 apart
        // margin: 300,
    
        // ... but no more than 600
        // limit: 600,
    
        // Display colored bars between handles
        connect: true,
    
        // Put '0' at the bottom of the slider
        // direction: 'rtl',
        orientation: 'horizontal',
    
        // Move handle on tap, bars are draggable
        behaviour: 'tap-drag',
        tooltips: [
            true,
            wNumb({
                suffix: '%'
            }),
        ],
        format: wNumb({
            decimals: 0
        }),
    
        // Show a scale with the slider
        pips: {
            mode: 'steps',
            // stepped: true,
            density: 10,
        }
    });

    // Price Range Slider setup

    //get max_price for project and set that as the max of slider
    var max_price = parseInt($('#max_price').val());

    noUiSlider.create(price_range_slider, {
        range: {
            'min': 0,
            'max': max_price
        },
    
        // step: 10,
    
        // Handles start at ...
        start: [0, max_price],
    
        // ... must be at least 300 apart
        // margin: 300,
    
        // ... but no more than 600
        // limit: 600,
    
        // Display colored bars between handles
        connect: true,
    
        // Put '0' at the bottom of the slider
        // direction: 'rtl',
        orientation: 'horizontal',
    
        // Move handle on tap, bars are draggable
        behaviour: 'tap-drag',
        tooltips: true,
        format: wNumb({
            decimals: 2,
            thousand: ',',
            prefix: '₱ '
        }),
    });

    var initiateSliders = function (){
        // Perrcentage Slider Setup
        price_range_slider.noUiSlider.on('update', function (values, handle) {
            price_floor.value = values[0];
            price_ceiling.value = values[1];
        })
        percentage_slider.noUiSlider.on('update', function (values, handle) {
            progress_floor.value = values[0];
            progress_ceiling.value = values[1];
        })

        $('#price_floor, #price_ceiling').on('change', function(){
            price_range_slider.noUiSlider.set([$('#price_floor').val(), $('#price_ceiling').val()]);
        })
    }

    var blocksTable = function () {
        var price_floor_value = price_range_slider.noUiSlider.get()[0].replace(/[^\d.-]/g,'');
        var price_ceiling_value = price_range_slider.noUiSlider.get()[1].replace(/[^\d.-]/g,'');
        var id = $('#project_id').val();
        var status = $('#status').val();
        var model = $('#model_id').val();
        var progress_f = progress_floor.value;
        var progress_c= progress_ceiling.value;
        var price_f = price_floor_value;
        var price_c = price_ceiling_value;
        var is_default = document.getElementById('is_default').value;
        $.ajax({
            url: base_url + "project/load_blocks/",
            type: "POST",
            dataType: "JSON",
            data: {
                id: id,
                status: status,
                model: model,
                progress_f: progress_f,
                progress_c: progress_c,
                price_f: price_f,
                price_c: price_c,
                is_default: is_default,
            },
            success: function (res) {
                $('#blocks_table').html(res.html);
            },
        });
    }

    var propertyModal = function(){
        $('#blocks_table').on( 'click', '.specific_property', function () {
            var id= $(this).data('property-id');
            $.ajax({
                url: base_url + "property/view/" + id + "/modal",
                type: "GET",
                dataType: "JSON",
                success: function (res) {
                    $('#property_modal_info').html(res);
                    $('#propertyModal').modal('show');
                },
            });
        })
    }

    var applyFilter = function(){
        $('#apply_filter').on( 'click',function () {
            document.getElementById('is_default').value = 'not_default';
            $('#filterModal').modal('hide');
            blocksTable();
        })
    }
    var resetFilter = function(){
        $('#reset_filter').on( 'click',function () {
            document.getElementById('is_default').value = 'default';
            $('#filterModal').modal('hide');
            blocksTable();
        })
    }

    return {
        //main function to initiate the module
        init: function () {
            blocksTable();
            initiateSliders();
            propertyModal();
            applyFilter();
            resetFilter();
        },
    };
})();

jQuery(document).ready(function () {
    Blocks.init();
});
