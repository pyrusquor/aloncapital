// Class definition
var project = function() {
	var confirmDelete = function () {
		$(document).on('click', '.remove_project', function () {
			var id = $(this).data('id');
			var btn = $(this);
			swal.fire({
				title: 'Delete Project',
				text: "Are you sure you want to delete this project?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then(function (result) {
				if (result.value) {

					$.ajax({
						url: base_url + 'project/delete/' + id,
						type: 'POST',
						dataType: "JSON",
						data: {
							id: id
						},
						success: function (res) {
							if (res.status) {
								console.log(btn);
								btn.closest('div.col-md-4').remove();
								swal.fire(
									'Deleted!',
									res.message,
									'success'
								);
							} else {
								swal.fire(
									'Oops!',
									res.message,
									'error'
								)
							}
						}
					});

				}
			});
		});
	};

	var filter = function() {
		$(document).on('click', '#apply_filter', function () {
			$( "#filter_form" ).submit();
		});
	}
	
	var fn_export = function() {
		$(document).on('click', '#export', function () {
			window.open(base_url + 'project/export?filter_name='+$("#filter_name").val()+'&filter_type='+$("#filter_type").val()+'&filter_company='+$("#filter_company").val()+'&filter_status='+$("#filter_status").val(),'_blank' );
		});
	}
	
	var upload_guide = function() {
		$(document).on('click', '#btn_upload_guide', function () {
			
			var table = $('#upload_guide_table');
			
			table.DataTable({
				order: [[0, 'asc']],
				pagingType: 'full_numbers',
				lengthMenu: [5, 10, 25, 50, 100],
				pageLength : 10,
				responsive: true,
				searchDelay: 500,
				processing: true,
				serverSide: true,
				deferRender: true,
				ajax: base_url + 'project/get_table_schema',
				columns: [
					// {data: 'button'},
					{data: 'no'},
					{data: 'name'},
					{data: 'type'},
					{data: 'format'},
					{data: 'option'},
					{data: 'required'}
				],
				// drawCallback: function ( settings ) {
					
				// }
			});
		});
	}
	
	var status = function () {
		$(document).on('change', '#import_status', function () {
			$('#export_csv_status').val($(this).val());
		});
	}

	var search = function () {
		$(document).on('change', '#generalSearch', function () {
			$('#filter_name').val($(this).val());
			$('#apply_filter').click();
		});
	}
	
	return {
		init: function () {
			confirmDelete();
			filter();
			fn_export();
			upload_guide();
			status();
			search();
		},
	};
}();


// Initialization
jQuery(document).ready(function() {
    project.init();
});