"use strict";
var projectChecklist = function() {
    var checklistDT = function() {
        var table = $('#checklist');

        // begin first table
		table.DataTable({
			responsive: true,
			columnDefs: [
				{
					targets: -1,
                    title: 'Actions',
                    className: 'dt-center',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-upload"></i> Upload</a>
                                <a class="dropdown-item" href="#"><i class="la la-search"></i> View</a>
                            </div>
                        </span>`;
					},
                },
                {
					targets: 0,
					className: 'dt-center',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="" class="m-checkable">
                            <span></span>
                        </label>`;
					},
                }
            ],
		});
    }

    return {

		//main function to initiate the module
		init: function() {
			checklistDT();
		},

	};
}();

jQuery(document).ready(function() {
	projectChecklist.init();
});