"use strict";
var KTDatatablesProjectChecklist = function () {

	var _initProjectChecklist = function () {

		var _table = $('#_project_document_table');

		_table.DataTable({
			order: [[ 1, 'desc']],
			pagingType: 'full_numbers',
			lengthMenu: [5, 10, 25, 50, 100],
			pageLength : 5,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: false,
			deferRender: true,
			dom:	
						"<'row'<'col-sm-12 col-md-6'l>>" +
						"<'row'<'col-sm-12'tr>>" +
						"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			language: {
				'lengthMenu': 'Show _MENU_',
        'infoFiltered': '(filtered from _MAX_ total records)'
			},
			columnDefs: [
				{
					targets: [0, 1, 3, 4, 5, 6, 7, 8, 10],
					className: 'dt-center',
				},
				{
					targets: [0, 4, 6, 7, 8, 10],
					orderable: false,
				},
				{
					targets: [9],
					searchable: true,
					visible: false
				}
			],
			drawCallback: function ( settings ) {

				$('span#_total').text(settings.fnRecordsTotal()+' TOTAL');
			}
		});
	}

	var _selectProp = function () {

		var _table    = $('#_project_document_table').DataTable();

		$('input#select-all').on( 'click', function () {

			if ( this.checked ) {

				$('input._select').each( function () {

					this.checked = true;
				});
			} else {

				$('input._select').each( function () {

					this.checked = false;
				});
			}
		});

		$('#_project_document_table tbody').on( 'change', 'input._select', function () {

			if ( this.checked == false ) {

				$('input#select-all').prop('checked', false);
			}
		});
	}

	var _add_ons = function () {

		var dTable = $('#_project_document_table').DataTable();

		$('#_search').on( 'keyup', function () {

			dTable.search($(this).val()).draw();
		});

		$('#kt_datepicker').datepicker({
    	orientation: "bottom left",
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

		$('button#_advance_search_btn').on( 'click', function () {

			$('div#_batch_upload').removeClass('show');
		});

		$('button#_batch_upload_btn').on( 'click', function () {

			$('div#_advance_search').removeClass('show');
		});
	}

	var _filterDocumentChecklist = function () {

		var dTable = $('#_project_document_table').DataTable();

		function _colFilter ( n ) {

			dTable.column(n).search($('#_column_'+n).val()).draw();
		}

		$('._filter').on( 'keyup change clear', function () {

			_colFilter($(this).data('column'));
		});
	}

	var _uploadProcess = function () {

		$(document).on( 'click', 'button._upload_file', function () {

			var _id = $(this).data('id');

			$('form#_upload_form').attr('action', base_url+'project/process_checklist_upload/'+_proj_id+'/'+_id);
		});

		$('.modal').on('hidden.bs.modal', function(){

	    // $(this).find('form')[0].reset();
	    
	    $(this).find('form').trigger('reset');

	    $('form').find('.kt-avatar__holder').css('background-image', 'url('+base_url+'assets/img/default/_img.png'+')');
		});
	}

	var _response = function () {

		var _loader = '<div class="kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--success"></div>';

		$(document).on( 'click', '._view_file', function () {

			var id = $(this).data('id');

			if ( id ) {

				$('._display_process_document_image').attr('id', '_display_process_document_image_'+id);

				$('#_display_process_document_image_'+id).html(_loader);

				$.ajax({
					type: 'post',
					dataType: 'json',
					url: base_url+'document/view_project_document_file',
					data: { id: id },
					success: function ( _respo ) {

						if ( _respo._status ) {

							$('#_display_process_document_image_'+id).html(_respo._html);
						} else {

							swal.fire(
								'Oops!',
								_respo._msg,
								'error'
							);
						}
					}
				});
				// .fail ( function () {

				// 	$('#_display_process_document_image_'+id).html(_loader);
				// });
			}
		});
	}

	return {

		init: function () {

			_initProjectChecklist();
			_add_ons();
			_filterDocumentChecklist();
			_selectProp();
			_uploadProcess();
			_response();

			new KTAvatar('kt_apps_user_add_avatar');
		}
	};
}();

jQuery(document).ready(function() {

    KTDatatablesProjectChecklist.init();
});