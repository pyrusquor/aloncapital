'use strict';
var sellerPosition = (function () {
    var sellerPositionTable = function () {
        var table = $('#seller_position_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,

            ajax: base_url + 'position/showSellerPositions',
            dom:
                "<'row'<'col-sm-12 col-md-6'l>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'name',
                },
                {
                    data: 'position_type_id',
                },
                {
                    data: 'sales_group_id',
                },
                {
                    data: 'level',
                },
                {
                    data: 'rate',
                },
                {
                    data: 'is_bypassable',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
							<span class="dropdown">
								<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
								<i class="la la-ellipsis-h"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="` +
                            base_url +
                            `position/update/` +
                            row.id +
                            `"><i class="fa flaticon-edit"></i> Update </a>
									<a href="javascript:void(0);" class="dropdown-item remove_position" data-id="` +
                            row.id +
                            `"><i class="fa flaticon-delete"></i> Delete </a>
								</div>
							</span>`
                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: 'dt-center',
                    // orderable: false,
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                  <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                  <span></span>
                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                {
                    targets: 2,
                    render: function (data, type, full, meta) {
                        var position_type = {
                            1: {
                                title: 'Agent',
                            },
                            2: {
                                title: 'Broker',
                            },
                        };

                        if (typeof position_type[data] === 'undefined') {
                            return ``;
                        }

                        return position_type[data].title;
                    },
                },
                {
                    targets: 6,
                    render: function (data, type, full, meta) {
                        var sales_group = {
                            0: {
                                title: 'No',
                            },
                            1: {
                                title: 'Yes',
                            },
                        };

                        if (typeof sales_group[data] === 'undefined') {
                            return ``;
                        }

                        return sales_group[data].title;
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_position', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'position/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#seller_position_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var _filterLandInventory = function () {
        var dTable = $('#seller_position_table').DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($('#_column_' + n).val())
                .draw();
        }

        $('._filter').on('keyup change clear', function () {
            if ($(this).is('input')) {
                let val = $('#_column_' + $(this).data('column')).val();

                dTable.search(val).draw();
            } else {
                _colFilter($(this).data('column'));
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            sellerPositionTable();
            _filterLandInventory();
        },
    };
})();

jQuery(document).ready(function () {
    sellerPosition.init();
});
