// Class definition
var KTFormControls = function () {
    // Private functions

    var valSellerPosition = function () {
        $( "#seller_position_form" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true 
                },
                level: {
                    required: true 
                },
                sales_group_id: {
                    required: true 
                },
                position_type_id: {
                    required: true 
                },
                rate: {
                    required: true 
                },
                is_bypassable: {
                    required: true 
                },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }

    return {
        // public functions
        init: function() {
            valSellerPosition();
        }
    };
}();

jQuery(document).ready(function() {    
    KTFormControls.init();
});