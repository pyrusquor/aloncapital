var KTFormControls = function () {
    // Private functions

    var valCashier = function () {
        validator = formEl.validate({
            // define validation rules
            rules: {
                company_id: {
                    required: true 
                },
                payee_type: {
                    required: true 
                },
                payee_type_id: {
                    required: true
                },
                payment_date: {
                    required: true
                },
                payment_type_id: {
                    required: true
                },
                payment_amount: {
                    required: true
                },
                bank: {
                    required: false
                },
                cheque_number: {
                    required: false
                },
                receipt_type: {
                    required: true
                },
                receipt_number: {
                    required: true
                },
                remarks: {
                    required: false
                },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

                swal.fire(
                    "Oops",
                    "There are some errors in your submission. Please correct them.",
                    "error"
                );
            },

            submitHandler: function (form) {},
        });       
    }

    var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formEl.ajaxSubmit({
                    type: 'POSt',
                    dataType: 'JSON',
					success: function(response) {
						if (response.status) {
							swal.fire({
								title: "Success!",
								text: response.message,
								type: "success"
							}).then(function() {
								window.location.replace(base_url + "cashier");
							});

						} else {
							swal.fire(
								'Oops!',
								response.message,
								'error'
							)
						}
					}
				});
			}
		});
	}

    return {
        // public functions
        init: function() {
            formEl = $('#form_cashier');
            valCashier();
            initSubmit();
        }
    };
}();

var KTBootstrapDatepicker = (function () {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }


    var datepicker = function () {
        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
        $(".kt_datepicker").datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: "yyyy-mm-dd",
        });
    };

    return {
        init: function () {
            datepicker();
        },
    };
})();

const payment_type = $('#payment_type_id');

jQuery(document).ready(function () {
    KTBootstrapDatepicker.init();
    KTFormControls.init();
    Checkpaymenttype(payment_type.val());
});


var Checkpaymenttype = (function (e) {

    const bank = document.querySelector("#bank");
    const cheque_number = document.querySelector("#cheque_number");
    if (e=="0"){
        cheque_number.disabled = true;
        bank.disabled = true;
    }
    else{
        cheque_number.disabled = false;
        bank.disabled = false
    }

})

payment_type.on("change", function () {
    Checkpaymenttype(this.value);
})