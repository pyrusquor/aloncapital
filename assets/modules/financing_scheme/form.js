"use strict";
var Create = (function () {
  var _form_validate = function () {
    $("#_financing_scheme_form").validate({
      rules: {
        name: {
          required: true,
          minlength: 2,
        },
      },
      invalidHandler: function (event, validator) {
        event.preventDefault();

        var alert = $("#form_msg");
        alert.closest("div.form-group").removeClass("kt-hide").show();
        KTUtil.scrollTop();

        toastr.options = {
          closeButton: true,
          debug: false,
          newestOnTop: false,
          progressBar: false,
          positionClass: "toast-top-right",
          preventDuplicates: false,
          onclick: null,
          showDuration: "300",
          hideDuration: "1000",
          timeOut: "5000",
          extendedTimeOut: "1000",
          showEasing: "swing",
          hideEasing: "linear",
          showMethod: "fadeIn",
          hideMethod: "fadeOut",
        };

        toastr.error("Please check your fields", "Something went wrong");
      },
      submitHandler: function (_frm) {
        _frm[0].submit();
      },
    });
  };

  var _add_ons = function () {
    // $('.kt_inputmask_price').inputmask('999,999,999.99', {
    // 	numericInput: true
    // });

    $(".kt_datepicker").datepicker({
      orientation: "bottom left",
      autoclose: true,
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });

    $("div.invalid-feedback").each(function () {
      var _this = $(this);

      _this
        .closest("div.form-group")
        .addClass("is-invalid")
        .find(".form-control")
        .addClass("is-invalid");

      $("#form_msg").closest("div.form-group").removeClass("kt-hide");
    });
  };

  var formRepeater = function () {
    $("#fees_form_repeater").repeater({
      initEmpty: false,

      defaultValues: {},

      show: function () {
        $(this).slideDown();
      },

      hide: function (deleteElement) {
        if (confirm("Are you sure you want to delete this element?")) {
          $(this).slideUp(deleteElement);
        }
      },
    });
  };
  return {
    init: function () {
      _form_validate();
      _add_ons();
    },
  };
})();

jQuery(document).ready(function () {
  Create.init();
});
