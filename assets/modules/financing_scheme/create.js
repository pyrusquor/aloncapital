"use strict";
var Create = (function () {
  var _form_validate = function () {
    $("#_financing_scheme_form").validate({
      rules: {
        name: {
          required: true,
          minlength: 2,
        },
      },
      invalidHandler: function (event, validator) {
        event.preventDefault();

        var alert = $("#form_msg");
        alert.closest("div.form-group").removeClass("kt-hide").show();
        KTUtil.scrollTop();

        toastr.options = {
          closeButton: true,
          debug: false,
          newestOnTop: false,
          progressBar: false,
          positionClass: "toast-top-right",
          preventDuplicates: false,
          onclick: null,
          showDuration: "300",
          hideDuration: "1000",
          timeOut: "5000",
          extendedTimeOut: "1000",
          showEasing: "swing",
          hideEasing: "linear",
          showMethod: "fadeIn",
          hideMethod: "fadeOut",
        };

        toastr.error("Please check your fields", "Something went wrong");
      },
      submitHandler: function (_frm) {
        _frm[0].submit();
      },
    });
  };

  var _add_ons = function () {
    // $('.kt_inputmask_price').inputmask('999,999,999.99', {
    // 	numericInput: true
    // });

    $(".kt_datepicker").datepicker({
      orientation: "bottom left",
      autoclose: true,
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });

    $("div.invalid-feedback").each(function () {
      var _this = $(this);

      _this
        .closest("div.form-group")
        .addClass("is-invalid")
        .find(".form-control")
        .addClass("is-invalid");

      $("#form_msg").closest("div.form-group").removeClass("kt-hide");
    });
  };

  let repeater = () => {
      $('#billing_form_repeater').repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            },
      });

      $(".period").on("change", function () {

        if ($(this).val() == 2) {
            $(this).closest('.item-repeater')
                .find('.period_rate_amount')
                .addClass('dp_input')
        } else {
            $(this).closest('.item-repeater')
            .find('.period_rate_amount')
            .removeClass('dp_input');
        }
    })

    $(".repeater-create").on('click', () => {
        $(".period").on("change", function () {

            if ($(this).val() == 2) {
                $(this).closest('.item-repeater')
                    .find('.period_rate_amount')
                    .addClass('dp_input')
            } else {
                $(this).closest('.item-repeater')
                .find('.period_rate_amount')
                .removeClass('dp_input');
            }
        })
    })
  }

  return {
    init: function () {
      _form_validate();
      _add_ons();
      repeater()
    },
  };
})();

jQuery(document).ready(function () {
  Create.init();
});

const periodAmount = document.querySelectorAll("#period_rate_amount");
const reservationAmount = periodAmount[0];
const loanAmount = periodAmount[1];

const setFinancingScheme = () => {
  $("#scheme_type").on("change", function () {
      
    const periodAmountDp = document.querySelectorAll(".dp_input");
    const downpaymentAmount = periodAmountDp;

    let schemeId = $(this).val();

    switch (schemeId) {
      case "1":
        // Standard
        reservationAmount.value = "";
        reservationAmount.disabled = false;

        downpaymentAmount.forEach((item, index) => {
            item.value = 0
            item.disabled = false
        })

        loanAmount.value = "";
        loanAmount.disabled = false;
        break;
      case "2":
        // Zero Downpayment
        reservationAmount.value = "";
        reservationAmount.disabled = false;

        downpaymentAmount.forEach((item, index) => {
            item.value = 0
            item.disabled = true
        })

        loanAmount.value = "";
        loanAmount.disabled = true;
        break;
      case "3":
        // Fixed Loan Amount
        reservationAmount.value = "";
        reservationAmount.disabled = false;

        downpaymentAmount.forEach((item, index) => {
            item.value = ""
            item.disabled = true
        })

        loanAmount.value = "1800";
        loanAmount.disabled = false;
        break;
      case "4":
        // Fixed Downpayment
        reservationAmount.value = "";
        reservationAmount.disabled = false;

        downpaymentAmount.forEach((item, index) => {
            item.value = "80000"
            item.disabled = false
        })

        loanAmount.value = "";
        loanAmount.disabled = true;
        break;
    }
  });
};

const getSchemeCategories = () => {
  // Get the element again
  const schemeCategory = document.querySelector(
      "#category_id"
  );  
  
  $.ajax({
      url: base_url + "financing_scheme_categories/get_all",
      type: "GET",
      success: function (data) {
          const categories = JSON.parse(data);

          categories.data.map((category) => {
              let opt = document.createElement("option");

              opt.value = category.id;
              opt.innerHTML = category.name;
              schemeCategory.appendChild(opt);
          });
      },
  });
};

setFinancingScheme();
getSchemeCategories();

