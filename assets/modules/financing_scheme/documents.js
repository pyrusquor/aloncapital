"use strict";
var KTDatatablesLandInventoryDocumentChecklist = function () {

	var _add_ons = function () {

		$('#checklist').select2({
			placeholder: "Select Checklist"
		});

		// $('._start_date').datepicker({
  //   	orientation: "bottom left",
  //   	todayBtn: "linked",
  //   	clearBtn: true,
  //   	autoclose: true,
		// 	todayHighlight: true,
		// 	templates: {
		// 		leftArrow: '<i class="la la-angle-left"></i>',
		// 		rightArrow: '<i class="la la-angle-right"></i>',
		// 	},
		// });
	}

	var _get_document_checklist = function () {

		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": true,
		  "progressBar": false,
		  "positionClass": "toast-top-right",
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "0",
		  "extendedTimeOut": "0",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		$('#checklist').on( 'change', function () {

			var _value = $(this).val();
			var _li_id = $(this).data('li_id');

			$.ajax({
				type: 'post',
				dataType: 'json',
				url: base_url+'land/get_filtered_document_checklist',
				data: {
					value: _value,
					li_id: _li_id
				},
				beforeSend: function () {

					KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'Processing...'
	        });
				},
				success: function ( _respo ) {

					if ( _respo._status === 1 ) {

						$('div#_filtered_document_checklist').html(_respo._html);

						$('#_document_checklist').DataTable({
							order: [[ 1, 'desc']],
							pagingType: 'full_numbers',
							lengthMenu: [5, 10, 25, 50, 100],
							pageLength : 5,
							responsive: true,
							searchDelay: 500,
							processing: true,
							serverSide: false,
							deferRender: true,
							dom:	
										// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
										"<'row'<'col-sm-12 col-md-6'l>>" +
										"<'row'<'col-sm-12'tr>>" +
										"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
							language: {
								'lengthMenu': 'Show _MENU_',
				        'infoFiltered': '(filtered from _MAX_ total records)'
							},
							columnDefs: [
								{
									targets: [0, 1, 4, 5, 6, 7, 8],
									className: 'dt-center',
								},
								{
									targets: [0],
									orderable: false,
								}
							]
						});
					} else {

						toastr.error(_respo._msg, "Failed");
					}

					KTApp.unblockPage();
				}
			}).fail ( function () {

				KTApp.unblockPage();

				toastr.error('Please refresh the page and try again.', 'Oops!');
			});
		});
	}

	var _filterDocumentChecklist = function () {

		var dTable = $('#_financing_scheme_document_table').DataTable();

		function _colFilter ( n ) {

			dTable.column(n).search($('#_column_'+n).val()).draw();
		}

		$('._filter').on( 'keyup change clear', function () {

			_colFilter($(this).data('column'));
		});
	}

	return {

		init: function () {

			_add_ons();
			_get_document_checklist();
			_filterDocumentChecklist();
		}
	};
}();

jQuery(document).ready(function() {

    KTDatatablesLandInventoryDocumentChecklist.init();
});