"use strict";
var LandInventoryUpdate = (function () {
  var _form_validate = function () {
    $("#_financing_scheme_form").validate({
      rules: {
        land_owner_name: {
          required: true,
          minlength: 2,
        },
        location: {
          required: true,
          minlength: 2,
        },
        owner_contact_info: {},
        land_area: {
          required: true,
          number: true,
        },
        estimated_price: {
          number: true,
        },
        negotiated_price: {
          number: true,
        },
        final_price: {
          number: true,
        },
        date_offered: {},
        decision_date: {},
        ep_code: {},
        assessed_value: {
          number: true,
        },
        mother_lot_tax_declaration: {},
        land_classification_id: {
          required: true,
        },
        location_of_registration: {},
        title: {},
        title_details: {},
        lot_number: {},
        encumbrance: {},
        date_transfer_of_title: {},
        ownership_classification_id: {
          required: true,
        },
        status: {
          required: true,
        },
        tax_declaration: {},
        company: {},
        remarks: {},
      },
      messages: {
        land_owner_name: {
          required: "The Land Owner Name field is required",
          minlength:
            "The Land Owner Name field must be at least 2 characters in length",
        },
        location: {
          required: "The Location field is required",
          minlength:
            "The Location field must be at least 2 characters in length",
        },
        land_classification_id: {
          required: "The Land Classification field is required",
        },
        land_area: {
          required: "The Land Area field is required",
        },
        ownership_classification_id: {
          required: "The Ownership Classification field is required",
        },
        status: {
          required: "The Status field is required",
        },
      },
      invalidHandler: function (event, validator) {
        event.preventDefault();

        var alert = $("#form_msg");
        alert.closest("div.form-group").removeClass("kt-hide").show();
        KTUtil.scrollTop();

        toastr.options = {
          closeButton: true,
          debug: false,
          newestOnTop: false,
          progressBar: false,
          positionClass: "toast-top-right",
          preventDuplicates: false,
          onclick: null,
          showDuration: "300",
          hideDuration: "1000",
          timeOut: "5000",
          extendedTimeOut: "1000",
          showEasing: "swing",
          hideEasing: "linear",
          showMethod: "fadeIn",
          hideMethod: "fadeOut",
        };

        toastr.error("Please check your fields", "Something went wrong");
      },
      submitHandler: function (_frm) {
        _frm[0].submit();
      },
    });
  };

  var _add_ons = function () {
    // $('.kt_inputmask_price').inputmask('999,999,999.99', {
    // 	numericInput: true
    // });

    $(".kt_datepicker").datepicker({
      orientation: "bottom left",
      autoclose: true,
      todayHighlight: true,
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
      },
    });

    $("div.invalid-feedback").each(function () {
      var _this = $(this);

      _this
        .closest("div.form-group")
        .addClass("is-invalid")
        .find(".form-control")
        .addClass("is-invalid");

      $("#form_msg").closest("div.form-group").removeClass("kt-hide");
    });
  };

  let repeater = () => {
    $('#billing_form_repeater').repeater({
          initEmpty: false,

          defaultValues: {},

          show: function () {
              $(this).slideDown();
          },

          hide: function (deleteElement) {
              if (confirm('Are you sure you want to delete this element?')) {
                  $(this).slideUp(deleteElement);
              }
          },
    });

    $(".period").on("change", function () {

      if ($(this).val() == 2) {
          $(this).closest('.item-repeater')
              .find('.period_rate_amount')
              .addClass('dp_input')
      } else {
          $(this).closest('.item-repeater')
          .find('.period_rate_amount')
          .removeClass('dp_input');
      }
  })

    $(".repeater-create").on('click', () => {
      $(".period").on("change", function () {

          if ($(this).val() == 2) {
              $(this).closest('.item-repeater')
                  .find('.period_rate_amount')
                  .addClass('dp_input')
          } else {
              $(this).closest('.item-repeater')
              .find('.period_rate_amount')
              .removeClass('dp_input');
          }
      })
  })
  }

  return {
    init: function () {
      _form_validate();
      _add_ons();
      repeater();
    },
  };
})();

jQuery(document).ready(function () {
  LandInventoryUpdate.init();
});

const periodAmount = document.querySelectorAll("#period_rate_amount");
const reservationAmount = periodAmount[0];
const downpaymentAmount = periodAmount[1];
const loanAmount = periodAmount[2];

const setFinancingScheme = () => {
  $("#scheme_type").on("change", function () {

    const periodAmountDp = document.querySelectorAll(".dp_input");
    const downpaymentAmount = periodAmountDp;

    let schemeId = $(this).val();

    switch (schemeId) {
      case "1":
        // Standard
        reservationAmount.disabled = false;

        downpaymentAmount.forEach((item, index) => {
          item.disabled = false
        })

        loanAmount.disabled = false;
        break;
      case "2":
        // Zero Downpayment
        reservationAmount.disabled = false;

        downpaymentAmount.forEach((item, index) => {
          item.disabled = true
        })

        loanAmount.disabled = true;
        break;
      case "3":
        // Fixed Loan Amount
        reservationAmount.disabled = false;

        downpaymentAmount.forEach((item, index) => {
          item.disabled = true
        })

        loanAmount.disabled = false;
        break;
      case "4":
        // Fixed Downpayment
        reservationAmount.disabled = false;

        downpaymentAmount.forEach((item, index) => {
          item.disabled = false
        })

        loanAmount.disabled = true;
        break;
    }
  });
};

const getSchemeCategories = () => {
  // Get the element again
  const schemeCategory = document.querySelector(
      "#category_id"
  );  
  
  $.ajax({
      url: base_url + "financing_scheme_categories/get_all",
      type: "GET",
      success: function (data) {
          const categories = JSON.parse(data);

          categories.data.map((category) => {
              let opt = document.createElement("option");

              opt.value = category.id;
              opt.innerHTML = category.name;
              schemeCategory.appendChild(opt);
          });
      },
  });
  if (schemeCategory.dataset.category) {
    setTimeout(() => {
      schemeCategory.value = schemeCategory.dataset.category;
    }, 1000);
  };
};

setFinancingScheme();
getSchemeCategories();
