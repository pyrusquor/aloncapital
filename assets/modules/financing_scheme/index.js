"use strict";
var KTDatatablesFinancing = (function () {
    var _initDocument = function () {
        var _table = $("#_financing_scheme_table");

        _table.DataTable({
            order: [
                [1, "asc"]
            ],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 10,
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                text: '<i class="fa fa-trash"></i> Delete Selected',
                className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm _bulk_delete",
                attr: {
                    id: "bulkDelete",
                },
                enabled: false,
            }, ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },
            ajax: base_url + "financing_scheme/get_financing_schemes",
            columns: [
                {
                    data: null,
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable __select" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                },
                {
                    data: "id"
                },
                {
                    data: "name"
                },
                {
                    data: "description"
                },
                {
                    data: "scheme_type"
                },
                {
                    data: "category"
                },
                {
                    data: "category_id"
                },
                // {
                //     data: "created_at"
                // },
                // {
                //     data: "updated_at"
                // },
                {
                    data: "reservation_amount"
                },
                {
                    data: "downpayment_percentage"
                },
                {
                    data: "downpayment_term"
                },
                {
                    data: "downpayment_interest_percentage"
                },
                {
                    data: "loan_percentage"
                },
                {
                    data: "loan_term"
                },
                {
                    data: "loan_interest_percentage"
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: "",
                    responsivePriority: -1
                },
            ],
            columnDefs: [{
                    targets: -1,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
                    <span class="dropdown">
                      <a href="#" class="btn btn-xs btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                        <i class="la la-ellipsis-h"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="` +
                            base_url +
                            `financing_scheme/document_checklist/` +
                            row.id +
                            `" class="dropdown-item"><i class="la la-book"></i> Document Checklist</a>
                        <a href="` +
                            base_url +
                            `financing_scheme/update/` +
                            row.id +
                            `" class="dropdown-item"><i class="la la-edit"></i> Update</a>
                        <button type="button" class="dropdown-item _delete_docx" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete</button>
                      </div>
                    </span>
                    <a href="` +
                            base_url +
                            `financing_scheme/view/` +
                            row.id +
                            `" class="btn btn-xs btn-clean btn-icon btn-icon-md" title="View">
                      <i class="la la-eye"></i>
                    </a>
                  `
                        );
                    },
                },
                {
                    targets: 0,
                    checkboxes: {
                        selectRow: true,
                    },
                },
                {
                    targets: 4,
                    // render: function (data, type, row, meta) {
                    //     var financingScheme = {
                    //         1: {
                    //             title: 'Standard',
                    //         },
                    //         2: {
                    //             title: 'Zero Downpayment',
                    //         },
                    //         3: {
                    //             title: 'Fixed Loan Amount',
                    //         },
                    //         4: {
                    //             title: 'Fixed Downpayment',
                    //         },
                    //         5: {
                    //             title: 'Cash Payment',
                    //         },
                    //     };

                    //     if (typeof financingScheme[data] === 'undefined') {
                    //         return ``;
                    //     }
                    //     return financingScheme[data].title;
                    // },
                },

                {
                    targets: [6, 7, 8, 9, 10, 11, 12, 13],
                    visible: false,
                },
            ],
            drawCallback: function (settings) {
                $("span#_total").text(settings.fnRecordsTotal() + " TOTAL");
            },
        });
    };

    var _filterLandInventory = function () {
        var dTable = $("#_financing_scheme_table").DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($("#_column_" + n).val())
                .draw();
        }

        $("._filter").on("keyup change clear", function () {
            if ($(this).is("input")) {
                let val = $("#_column_" + $(this).data('column')).val();

                dTable.search(val).draw()
            } else {
                _colFilter($(this).data('column'));
            }
        });
    };

    var _selectProp = function () {
        $("input#select-all").on("click", function () {
            if (this.checked) {
                $("input.__select").each(function () {
                    this.checked = true;
                });
            } else {
                $("input.__select").each(function () {
                    this.checked = false;
                });
            }
        });

        $(document).on("change", "input.__select", function () {

            if (this.checked == false) {
                $("input#select-all").prop("checked", false);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "financing_scheme/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#_financing_scheme_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });

                        $("input#select-all").prop("checked", false);
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    var _processLand = function () {
        $("#_consolidate_form").validate({
            rules: {
                land_owner_name: {
                    required: true,
                    minlength: 2,
                },
                location: {
                    required: true,
                    minlength: 2,
                },
                land_area: {
                    required: true,
                    number: true,
                },
                land_classification_id: {
                    required: true,
                },
                lot_number: {
                    number: true,
                },
                status: {
                    required: true,
                },
                company: {},
                remarks: {},
            },
            invalidHandler: function (event, validator) {
                event.preventDefault();

                var alert = $("#form_msg");
                alert.closest("div.form-group").removeClass("kt-hide").show();
                KTUtil.scrollTop();

                toastr.options = {
                    closeButton: true,
                    debug: false,
                    newestOnTop: false,
                    progressBar: false,
                    positionClass: "toast-top-right",
                    preventDuplicates: false,
                    onclick: null,
                    showDuration: "300",
                    hideDuration: "1000",
                    timeOut: "5000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                };

                toastr.error(
                    "Please check your fields",
                    "Something went wrong"
                );
            },
            // submitHandler: function ( _frm ) {

            // 	_frm[0].submit();
            // }
        });

        $(document).on("submit", "form#_consolidate_form", function (e) {
            e.preventDefault();

            var land_ids = [];
            var inputs = $("form#_consolidate_form").serializeArray();

            $.each($('input[name="id[]"]:checked'), function () {
                land_ids.push($(this).val());
            });

            $.ajax({
                type: "post",
                dataType: "json",
                url: base_url + "land/consolidate",
                data: {
                    ids: land_ids,
                    input: inputs,
                },
                beforeSend: function () {
                    $("#_consolidate_modal").removeClass("show");

                    KTApp.blockPage({
                        overlayColor: "#000000",
                        type: "v2",
                        state: "primary",
                        message: "Processing...",
                    });
                },
                success: function (_respo) {
                    $("#_consolidate_modal").modal("hide");

                    $("button._consolidate").addClass("disabled", true);

                    // $('#_consolidate_modal').addClass('show');

                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        newestOnTop: true,
                        progressBar: false,
                        positionClass: "toast-top-right",
                        preventDuplicates: true,
                        onclick: null,
                        showDuration: "300",
                        hideDuration: "1000",
                        timeOut: "0",
                        extendedTimeOut: "0",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                    };

                    if (_respo._status === 1) {
                        toastr.success(_respo._msg, "Success");

                        $("form#_consolidate_form")
                            .find("input, select")
                            .addClass("is-valid");
                    } else {
                        toastr.error(_respo._msg, "Failed");

                        $("form#_consolidate_form")
                            .find("input, select")
                            .addClass("is-invalid");
                    }

                    $("#_financing_scheme_table").DataTable().ajax.reload();

                    KTApp.unblockPage();
                },
            }).fail(function () {
                $("button._consolidate").addClass("disabled", true);

                $("#_consolidate_modal").modal("hide");

                KTApp.unblockPage();

                swal.fire(
                    "Oops!",
                    "Please refresh the page and try again.",
                    "error"
                );
            });
        });

        $("#_subdivide_form").validate({
            rules: {
                land_owner_name: {
                    required: true,
                    minlength: 2,
                },
                land_classification_id: {
                    required: true,
                },
                location: {
                    required: true,
                    minlength: 2,
                },
                company: {},
                land_area: {
                    required: true,
                    number: true,
                },
                status: {
                    required: true,
                },
                lot_number: {
                    number: true,
                },
                number_of_lot_to_subdivide: {
                    required: true,
                    number: true,
                },
                project: {},
                remarks: {},
            },
            messages: {
                land_owner_name: {
                    required: "The Land Owner Name field is required",
                    minlength: "The Land Owner Name field must be at least 2 characters in length",
                },
                location: {
                    required: "The Location field is required",
                    minlength: "The Location field must be at least 2 characters in length",
                },
                land_classification_id: {
                    required: "The Land Classification field is required",
                },
                land_area: {
                    required: "The Land Area field is required",
                },
                status: {
                    required: "The Status field is required",
                },
            },
            invalidHandler: function (event, validator) {
                event.preventDefault();

                var alert = $("#form_msg");
                alert.closest("div.form-group").removeClass("kt-hide").show();
                KTUtil.scrollTop();

                toastr.options = {
                    closeButton: true,
                    debug: false,
                    newestOnTop: false,
                    progressBar: false,
                    positionClass: "toast-top-right",
                    preventDuplicates: false,
                    onclick: null,
                    showDuration: "300",
                    hideDuration: "1000",
                    timeOut: "5000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                };

                toastr.error(
                    "Please check your fields",
                    "Something went wrong"
                );
            },
            // submitHandler: function ( _frm ) {

            // 	_frm[0].submit();
            // }
        });

        $(document).on("submit", "form#_subdivide_form", function (e) {
            e.preventDefault();

            var land_id = $('input[name="id[]"]:checked').val();
            var inputs = $("form#_subdivide_form").serializeArray();

            $.ajax({
                type: "post",
                dataType: "json",
                url: base_url + "land/subdivide",
                data: {
                    id: land_id,
                    input: inputs,
                },
                beforeSend: function () {
                    $("#_subdivide_modal").removeClass("show");

                    KTApp.blockPage({
                        overlayColor: "#000000",
                        type: "v2",
                        state: "primary",
                        message: "Processing...",
                    });
                },
                success: function (_respo) {
                    $("#_subdivide_modal").modal("hide");

                    $("button._subdivide").addClass("disabled", true);

                    // $('#_subdivide_modal').addClass('show');

                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        newestOnTop: true,
                        progressBar: false,
                        positionClass: "toast-top-right",
                        preventDuplicates: true,
                        onclick: null,
                        showDuration: "300",
                        hideDuration: "1000",
                        timeOut: "0",
                        extendedTimeOut: "0",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                    };

                    if (_respo._status === 1) {
                        toastr.success(_respo._msg, "Success");

                        $("form#_subdivide_form")
                            .find("input, select")
                            .addClass("is-valid");
                    } else {
                        toastr.error(_respo._msg, "Failed");

                        $("form#_subdivide_form")
                            .find("input, select")
                            .addClass("is-invalid");
                    }

                    $("#_financing_scheme_table").DataTable().ajax.reload();

                    KTApp.unblockPage();
                },
            }).fail(function () {
                $("button._subdivide").addClass("disabled", true);

                $("#_subdivide_modal").modal("hide");

                KTApp.unblockPage();

                swal.fire(
                    "Oops!",
                    "Please refresh the page and try again.",
                    "error"
                );
            });
        });
    };

    var _add_ons = function () {
        var dTable = $("#_financing_scheme_table").DataTable();

        $("#_search").on("keyup", function () {
            dTable.search($(this).val()).draw();
        });

        $("#kt_datepicker").datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
        });

        $("button#_advance_search_btn").on("click", function () {
            $("div#_batch_upload").removeClass("show");
        });

        $("button#_batch_upload_btn").on("click", function () {
            $("div#_advance_search").removeClass("show");
        });

        $(document).on("click", "button._delete_docx", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "post",
                        dataType: "json",
                        url: base_url + "financing_scheme/delete",
                        data: {
                            id: id
                        },
                        success: function (_response) {
                            if (_response._status) {
                                // $("#_financing_scheme_table").load(location.href + " #_financing_scheme_table>*", "");

                                $("#_financing_scheme_table")
                                    .DataTable()
                                    .ajax.reload();

                                swal.fire(
                                    "Deleted!",
                                    _response._msg,
                                    "success"
                                );
                            } else {
                                swal.fire("Oops!", _response._msg, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe.",
                        "error"
                    );
                }
            });
        });

        $(".modal").on("hidden.bs.modal", function () {
            $(this).find("form")[0].reset();

            $("form")
                .find("input, select")
                .removeClass("is-valid")
                .removeClass("is-invalid");

            $(this).find("form").trigger("reset");

            $("#form_msg").closest("div.form-group").addClass("kt-hide");
        });
    };

    return {
        init: function () {
            _initDocument();
            _processLand();
            _selectProp();
            _add_ons();
            _filterLandInventory();
        },
    };
})();

jQuery(document).ready(function () {
    KTDatatablesFinancing.init();
});