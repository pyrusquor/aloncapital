// Class definition
var KTFormControls = (function () {
    // Private functions

    var valContractors = function () {
        $("#contractors_form").validate({
            // define validation rules
            rules: {
                name: {
                    required: true,
                },
                code: {
                    required: true,
                },
                type: {
                    required: true,
                },
                payment_terms: {
                    required: true,
                },
                payment_type: {
                    required: true,
                },
                delivery_terms: {
                    required: true,
                },
                address: {
                    required: true,
                },
                mobile_number: {
                    required: true,
                },

                email_address: {
                    required: true,
                },
                tin_number: {
                    required: true,
                },
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                toastr.error(
                    "Please check your fields",
                    "Something went wrong"
                );

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });
    };

    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var _add_ons;
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        wizard.on('beforePrev', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    };

    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                // "name": {
                //     required: true
                // },
                // "code": {
                //     required: true
                // },
                // "type": {
                //     required: true,
                // },
                // "payment_terms": {
                //     required: true,
                // },
                // "delivery_terms": {
                //     required: true,
                // },
                // "address": {
                //     required: true,
                // },
                // "mobile_number": {
                //     required: true,
                //     number: true
                // },
                // "email_address": {
                //     required: true,
                // },
                // "tin_number": {
                //     required: true,
                //     number: true
                // },
            },
            // messages: {
            // 	"info[last_name]":'Last name field is required',
            // },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {

            }
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function (e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success"
                            }).then(function () {
                                window.location.replace(base_url + "contractors");
                            });
                        } else {
                            swal.fire({
                                title: 'Oops!',
                                html: response.message,
                                icon: 'error'
                            })
                        }
                    }
                });
            }
        });
    };

    return {
        // public functions
        init: function () {
            valContractors();

            wizardEl = KTUtil.get('kt_wizard_v3');
            formEl = $('#contractors_form');

            initWizard()
            initValidation()
            initSubmit()
        },
    };
})();

jQuery(document).ready(function () {
    KTFormControls.init();
});