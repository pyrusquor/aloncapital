"use strict";

const mrr_status = [
    'Select Option',
    'PO Amended',
    'Item Received',
    'Wrong Delivery',
    'Returned to Supplier',
    'Partial Delivery'
];

// Class definition
var Suppliers = function () {

    var paymentRequestTable = function () {

        var table = $('#paymentrequest_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                url: base_url + 'payment_request/showPaymentRequests',
                type: 'POST',
                data: {
                    where: {
                        payee_type: 'suppliers',
                        payee_type_id: window.payee_type_id
                    }
                }
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'payee',
                },
                {
                    data: 'payable_type_id',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'due_date',
                },
                {
                    data: 'gross_amount',
                },
                {
                    data: 'tax_amount',
                },
                {
                    data: 'paid_amount',
                },
                {
                    data: 'remaining_amount',
                },
                {
                    data: 'is_paid',
                },
                {
                    data: 'is_complete',
                },
                {
                    data: 'particulars',
                },
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },


                {
                    targets: [0],
                    visible: false,
                },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });
    }

    var paymentVoucherTable = function () {
        var table = $('#paymentvoucher_table');

        // begin first table
        table.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                url: base_url + 'payment_voucher/showPaymentVouchers',
                type: 'POST',
                data: {
                    where: {
                        payee_type: 'suppliers',
                        payee_type_id: window.payee_type_id
                    }
                }
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'payment_type_id',
                },
                {
                    data: 'payee',
                },
                {
                    data: 'payee_type',
                },
                {
                    data: 'paid_date',
                },
                {
                    data: 'paid_amount',
                },
                {
                    data: 'particulars',
                },
            ],
            columnDefs: [
                {
                    targets: [0, 1, 3, 4, 5, -1],
                    className: 'dt-center',
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },

                /* ==================== begin: Add target fields for dropdown value ==================== */
                {
                    targets: 3,
                    render: function (data, type, row, meta) {
                        var voucher = {
                            1: {
                                title: 'Cash',
                            },
                            2: {
                                title: 'Regular Cheque',
                            },
                            3: {
                                title: 'Post Dated Cheque',
                            },
                            4: {
                                title: 'Online Deposits',
                            },
                            5: {
                                title: 'Wire Transfer',
                            },
                            6: {
                                title: 'Cash & Cheque',
                            },
                            7: {
                                title: 'Cash & Bank Remittance',
                            },
                            8: {
                                title: 'Journal Voucher',
                            },
                            9: {
                                title: 'Others',
                            },
                            10: {
                                title: 'Direct Deposit',
                            },
                        };

                        if (typeof voucher[data] === 'undefined') {
                            return ``;
                        }
                        return voucher[data].title;
                    },
                },

                {
                    targets: [0],
                    visible: false,
                },
                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });
    };

    var purchaseOrderTable = function () {
        var table = $("#purchase_order_table");
        var url = base_url + "purchase_order/showPurchaseOrders?supplier_id=" + window.payee_type_id;

        // begin first table
        table.DataTable({
            // order: [[2, "asc"]],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,

            ajax: url,
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)"
            },

            columns: [
                {
                    data: "id"
                },
                {
                    data: "reference"
                },
                /* ==================== begin: Add model fields ==================== */
                {
                    data: "created_at"
                },
                {
                    data: "company.name"
                },
                {
                    data: "supplier.name"
                },
                {
                    data: "total"
                },
                {
                    data: "status_label"
                },
                /* ==================== end: Add model fields ==================== */
                {
                    data: "Actions",
                    responsivePriority: -1
                }
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `<a href="` + base_url + `purchase_order/view/` + row.id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md tooltip_hover" id="tooltip_` + row.id + `"
                            data-toggle="tooltip" data-html="true">
                                <i class="la la-eye"></i>
                            </a>`
                        )
                    }
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: "dt-center"
                },
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            }
        });

        var oTable = table.DataTable();
        $("#generalSearch").keyup(function () {
            oTable.search($(this).val()).draw();
        });
    };

    var purchaseOrderItemTable = function () {
        var table = $("#purchase_order_item_table");
        var url = base_url + "purchase_order_item/showPurchaseOrderItemsBySupplier/" + window.payee_type_id;

        // begin first table
        table.DataTable({
            order: [[0, "desc"]],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,

            ajax: url,
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)"
            },

            columns: [
                {
                    data: "id"
                },
                {
                    data: "reference"
                },
                /* ==================== begin: Add model fields ==================== */
                {
                    data: "item_name"
                },
                {
                    data: "quantity"
                },
                {
                    data: "unit_cost"
                },
                {
                    data: "total_cost"
                },
            ],
            columnDefs: [
                // {
                //     targets: -1,
                //     title: "Actions",
                //     searchable: false,
                //     orderable: false,
                //     render: function (data, type, row, meta) {
                //         return (
                //             `<a href="` + base_url + `purchase_order/view/` + row.id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md tooltip_hover" id="tooltip_` + row.id + `"
                //             data-toggle="tooltip" data-html="true">
                //                 <i class="la la-eye"></i>
                //             </a>`
                //         )
                //     }
                // },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: "dt-center"
                },
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            }
        });

        var oTable = table.DataTable();
        $("#generalSearch").keyup(function () {
            oTable.search($(this).val()).draw();
        });
    };

    var materialReceivingTable = function () {
        var table = $("#material_receiving_table");
        var url = base_url + "material_receiving/showMaterialReceivings?filter=true&with_relations=yes&supplier_id=" + window.payee_type_id;

        // begin first table
        table.DataTable({
            order: [[0, "desc"]],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,

            ajax: url,
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)"
            },

            columns: [
                {
                    data: "id"
                },
                {
                    data: "reference"
                },
                /* ==================== begin: Add model fields ==================== */
                {
                    data: "purchase_order.reference"
                },
                {
                    data: "created_at"
                },
                {
                    data: "supplier.name"
                },
                {
                    data: "warehouse.name"
                },
                {
                    data: "total_cost"
                },
                {
                    data: "status"
                },
                {
                    data: "Actions",
                    responsivePriority: -1
                }
                /* ==================== end: Add model fields ==================== */
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `<a href="` + base_url + `material_receiving/view/` + row.id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md tooltip_hover" id="tooltip_` + row.id + `"
                            data-toggle="tooltip" data-html="true" target="_blank">
                                <i class="la la-eye"></i>
                            </a>`
                        )
                    }
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: "dt-center"
                },
                {
                    targets: 7,
                    render: function (data, type, row, meta) {
                        return mrr_status[row.status];
                    }
                },
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            }
        });
    };

    return {
        // public functions
        init: function () {
            paymentRequestTable()
            paymentVoucherTable()
            purchaseOrderTable()
            purchaseOrderItemTable()
            materialReceivingTable()
        }
    };
}();

jQuery(document).ready(function () {
    Suppliers.init();
});