// Class definition
var KTFormControls = (function () {

    var wizardEl;
    var formEl;
    var validator;

    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        wizard.on('beforePrev', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    };

    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                name: {
                    required: true,
                },
                code: {
                    required: true,
                },
                type: {
                    required: true,
                },
                payment_type: {
                    required: true,
                },
                vat_type: {
                    required: true,
                },
                payment_terms: {
                    required: true,
                },
                // delivery_terms: {
                //     required: true,
                // },
                address: {
                    required: true,
                },
                mobile_number: {
                    required: true,
                },

                email_address: {
                    required: false,
                },
                // tin_number: {
                //     required: true,
                // },
                // bank_id: {
                //     required: true,
                // },

            },
            // messages: {
            // 	"info[last_name]":'Last name field is required',
            // },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {

            }
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');
        btn.on('click', function (e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success"
                            }).then(function () {
                                window.location.replace(base_url + "suppliers");
                            });
                        } else {
                            swal.fire({
                                title: 'Oops!',
                                html: response.message,
                                icon: 'error'
                            })
                        }
                    }
                });
            }
        });
    };

    var add_ons = function () {

        var arrows;

        wizardEl = KTUtil.get('kt_wizard_v3');
        formEl = $('#suppliers_form');

        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>',
            };
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            };
        }

        $("#payment_terms_select_input").on('change', function () {
            console.log($(this).val())

            if ($(this).val() == 'Others Please Specify') {

                $('#payment_terms_select').removeClass('col-sm-12').addClass('col-sm-6')
                $('#payment_terms_select_input').attr('name', '')

                $('#payment_terms_others').removeClass('d-none')
                $('#payment_terms_others_input').attr('name', 'payment_terms')

            } else {

                $('#payment_terms_select').addClass('col-sm-12').removeClass('col-sm-6')
                $('#payment_terms_select_input').attr('name', 'payment_terms')

                $('#payment_terms_others').addClass('d-none')
                $('#payment_terms_others_input').val("").attr('name', '')
            }
        })
    }

    var datepicker = function () {

        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
    }

    return {
        init: function () {
            add_ons();
            initValidation();
            initWizard();
            initSubmit();
            datepicker()
        },
    };
})();

jQuery(document).ready(function () {
    KTFormControls.init();
});