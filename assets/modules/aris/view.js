"use strict";

// Class definition
var ArisView = function () {
	// Base elements
	var wizardEl;
	var formEl;
	var validator;
	var wizard;
	
	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		wizard = new KTWizard('aris_view', {
			startStep: 1,
			// clickableSteps: false
		});

		// Change event
		wizard.on('change', function(wizard) {
			KTUtil.scrollTop();	
		});
	}

	var initValidation = function() {
		validator = formEl.validate({
			// Validate only visible fields
			ignore: ":hidden",

			// Validation rules
			rules: {
				//= Step 1
				address1: {
					required: true 
				}
			},
			
			// Display error  
			invalidHandler: function(event, validator) {	 
				KTUtil.scrollTop();

				swal.fire({
					"title": "", 
					"text": "There are some errors in your submission. Please correct them.", 
					"type": "error",
					"confirmButtonClass": "btn btn-secondary"
				});
			},

			// Submit valid form
			submitHandler: function (form) {
				
			}
		});   
	}

	var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formEl.ajaxSubmit({
					success: function() {
						KTApp.unprogress(btn);
						//KTApp.unblock(formEl);

						swal.fire({
							"title": "", 
							"text": "The application has been successfully submitted!", 
							"type": "success",
							"confirmButtonClass": "btn btn-secondary"
						});
					}
				});
			}
		});
	}

	var syncTransaction = function() {
		$(document).on('click', "#document_table .sync_transaction", function() {
			var info = $(this);
			var project_id = info.data('project');
			var document_id = info.data('document');

			KTApp.block('#kt_content', {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Syncing Transaction...',
				css: {
					padding: 0,
					margin: 0,
					width: '30%',
					top: '40%',
					left: '35%',
					textAlign: 'center',
					color: '#000',
					border: '3px solid #aaa',
					backgroundColor: '#fff',
					cursor: 'wait',
				},
			});

			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, please proceed',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true,
			}).then(function (result) {
				if(result.value) {
					$.ajax({
						url: base_url + 'aris/sync_transaction/' + project_id + '/' + document_id,
						dataType: 'JSON',
						success: function (response) {
							console.log(response);
							KTApp.unblock('#kt_content');
							if(response.status) {
								if(response.status == 1) {
									$.ajax({
										url: base_url + 'aris/load_document_information/' + project_id,
										method: 'POST',
										success: function (response) {
											$('#document_information').html(response);
										},
									})
									swal.fire({
										title: 'Success!',
										text: response.message,
										type: 'success',
									})
								} else {
									swal.fire({
										title: 'Success!',
										text: response.message,
										type: 'success',
									})
								}
								
							} else {
								swal.fire('Oops!', response.message, 'error');
							}
						},
					})
				} else{
					KTApp.unblock('#kt_content');
				} 
			})
		});
	}

	var syncCollections = function() {
		$(document).on('click', "#document_table .sync_collections", function() {
			var info = $(this);
			var project_id = info.data('project');
			var document_id = info.data('document');

			KTApp.block('#kt_content', {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Processing...',
				css: {
					padding: 0,
					margin: 0,
					width: '30%',
					top: '40%',
					left: '35%',
					textAlign: 'center',
					color: '#000',
					border: '3px solid #aaa',
					backgroundColor: '#fff',
					cursor: 'wait',
				},
			});

			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to rever this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, please proceed',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true,
			}).then(function (result) {
				if(result.value) {
					$.ajax({
						url: base_url + 'transaction_payment/sync_aris_collections/',
						dataType: 'JSON',
						method: 'POST',
						data: {
							project_id : project_id,
							document_id : document_id,
						},
						success: function (response) {
							KTApp.unblock('#kt_content');
							if(response.status) {
								swal.fire({
									title: 'Success!',
									text: response.message,
									type: 'success',
								})
							} else {
								swal.fire('Oops!', response.message, 'error');
							}

						},
					})
				} else{
					KTApp.unblock('#kt_content');
				}

			})

		});
	}
	var resyncTransaction = function() {
		$(document).on('click', "#document_table .resync_collections", function() {
			var info = $(this);
			var project_id = info.data('project');
			var document_id = info.data('document');
			var transaction_id = info.data('transaction-id');

			KTApp.block('#kt_content', {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Processing...',
				css: {
					padding: 0,
					margin: 0,
					width: '30%',
					top: '40%',
					left: '35%',
					textAlign: 'center',
					color: '#000',
					border: '3px solid #aaa',
					backgroundColor: '#fff',
					cursor: 'wait',
				},
			});

			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, please proceed',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true,
			}).then(function (result) {
				if(result.value) {
					$.ajax({
						url: base_url + 'aris/resync_transaction/' + project_id + '/' + document_id,
						dataType: 'JSON',
						method: 'POST',
						data: {
							project_id : project_id,
							document_id : document_id,
						},
						success: function (response) {
							KTApp.unblock('#kt_content');
							if(response.status) {
								$.ajax({
									url: base_url + 'aris/load_document_information/' + project_id,
									method: 'POST',
									success: function (response) {
										$('#document_information').html(response);
									},
								})
								swal.fire({
									title: 'Success!',
									text: response.message,
									type: 'success',
								})
							} else {
								swal.fire('Oops!', response.message, 'error');
							}

						},
					})
				} else{
					KTApp.unblock('#kt_content');
				}

			})

		});
	}

	var document_load = function(){
		$(document).on('click', "#document_information_click", function() {

			KTApp.block('#kt_content', {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Processing...',
				css: {
					padding: 0,
					margin: 0,
					width: '30%',
					top: '40%',
					left: '35%',
					textAlign: 'center',
					color: '#000',
					border: '3px solid #aaa',
					backgroundColor: '#fff',
					cursor: 'wait',
				},
			});
			var info = $(this);
			var project_id = info.data('project');
			$.ajax({
				url: base_url + 'aris/load_document_information/' + project_id,
				method: 'POST',
				success: function (response) {
					KTApp.unblock('#kt_content');
					$('#document_information').html(response);
				},
			})
		})
	}

	return {
		// public functions
		init: function() {
			wizardEl = KTUtil.get('aris_view');
			formEl = $('#kt_form');

			initWizard(); 
			initValidation();
			initSubmit();
			syncTransaction();
			syncCollections();
			resyncTransaction();
			document_load();
		}
	};
}();

jQuery(document).ready(function() {	
	ArisView.init();
});