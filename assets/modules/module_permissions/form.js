// Class definition
"user strict";
var Form = (function () {
  var arrows;

  if (KTUtil.isRTL()) {
    arrows = {
      leftArrow: '<i class="la la-angle-right"></i>',
      rightArrow: '<i class="la la-angle-left"></i>',
    };
  } else {
    arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    };
  }

  // Private functions
  var datepicker = function () {
    // minimum setup
    $(".kt_datepicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      orientation: "bottom left",
      templates: arrows,
      locale: "no",
      format: "yyyy-mm-dd",
      autoclose: true,
    });

    $(".yearPicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true,
    });
  };

  var formEl;
  var validator;

  var initValidation = function () {
    validator = formEl.validate({
      // Validate only visible fields
      ignore: ":hidden",

      // Validation rules
      rules: {
        name: {
          required: true,
        },
      },
      // messages: {
      // 	"info[last_name]":'Last name field is required',
      // },
      // Display error
      invalidHandler: function (event, validator) {
        KTUtil.scrollTop();

        swal.fire({
          title: "",
          text:
            "There are some errors in your submission. Please correct them.",
          type: "error",
          confirmButtonClass: "btn btn-secondary",
        });
      },

      // Submit valid form
      submitHandler: function (form) {},
    });
  };

  var initSubmit = function () {
    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

    btn.on("click", function (e) {
      e.preventDefault();

      if (validator.form()) {
        // See: src\js\framework\base\app.js
        KTApp.progress(btn);
        //KTApp.block(formEl);

        // See: http://malsup.com/jquery/form/#ajaxSubmit
        formEl.ajaxSubmit({
          type: "POSt",
          dataType: "JSON",
          success: function (response) {
            if (response.status) {
              swal
                .fire({
                  title: "Success!",
                  text: response.message,
                  type: "success",
                })
                .then(function () {
                  window.location.replace(base_url + "permission");
                });
            } else {
              swal.fire({
                title: "Oops!",
                html: response.message,
                icon: "error",
              });
            }
          },
        });
      }
    });
  };

  var _selectProp = function () {
    var _table = $("#form_module_permission").DataTable();

    $(document).on("change", 'input[name="id[]"]', function () {
      var _checked = $('input[name="id[]"]:checked').length;
      console.log(_checked);
      if (_checked < 0) {
        _table.button(0).disable();
      } else {
        _table.button(0).enable(_checked > 0);
      }
    });
  };

  var _add_ons = function () {
    datepicker();
  };

  // Public functions
  return {
    init: function () {
      datepicker();
      _add_ons();

      formEl = $("#form_module_permission");

      initValidation();
      initSubmit();
    },
  };
})();

// Initialization
jQuery(document).ready(function () {
  Form.init();
});

// function checkAll(name, bx) {
//   console.log(name);
//   for (i = name.length; i--; ) {
//     name[i].checked = bx.checked;
//   }
// }

function checkAll(bx) {
  for (
    var tbls = document.getElementsByTagName("table"), i = tbls.length;
    i--;

  )
    for (var bxs = tbls[i].getElementsByTagName("input"), j = bxs.length; j--; )
      if (bxs[j].type == "checkbox") bxs[j].checked = bx.checked;
}

function checkPermissions(name) {
  var _checkboxes = document
    .getElementById(name.dataset.rowid)
    .querySelectorAll('[type="checkbox"]');
  [].forEach.call(_checkboxes, function (x) {
    x.checked = name.checked;
  });
}

document
  .getElementById("form_module_permission")
  .addEventListener("click", function (e) {
    if (e.target.tagName === "INPUT") {
      var td = e.target.parentNode,
        tr = td.parentNode,
        row = tr.parentNode.id;

      var ch = document.getElementsByClassName(row);
      var checkAll = document.getElementById("checkall-" + row);
      var dot = ".";
      var _class = dot.concat(row);
      $(_class).click(function () {
        if ($(_class).length == $(_class.concat(":checked")).length) {
          $("#checkall-" + row).prop("checked", true);
        } else {
          $("#checkall-" + row).prop("checked", false);
        }
      });
    }
  });

$("#mp_submit").click(function () {
  let module_permissions_dict = {};
  var permissionID = $("#permissionID").html();

  $('input[name="is_create[]"]').each(function () {
    if (this.checked) {
      module_permissions_dict[$(this).attr("data-id")] = { is_create: 1 };
    } else {
      module_permissions_dict[$(this).attr("data-id")] = { is_create: 0 };
    }
  });

  $('input[name="is_read[]"]:checked').each(function () {
    if (this.checked) {
      try {
        Object.assign(module_permissions_dict[$(this).attr("data-id")], {
          is_read: 1,
        });
      } catch (err) {
        module_permissions_dict[$(this).attr("data-id")] = { is_read: 1 };
      }
    } else {
      try {
        Object.assign(module_permissions_dict[$(this).attr("data-id")], {
          is_read: 0,
        });
      } catch (err) {
        module_permissions_dict[$(this).attr("data-id")] = { is_read: 0 };
      }
    }
  });

  $('input[name="is_update[]"]:checked').each(function () {
    if (this.checked) {
      try {
        Object.assign(module_permissions_dict[$(this).attr("data-id")], {
          is_update: 1,
        });
      } catch (err) {
        module_permissions_dict[$(this).attr("data-id")] = { is_update: 1 };
      }
    } else {
      try {
        Object.assign(module_permissions_dict[$(this).attr("data-id")], {
          is_update: 0,
        });
      } catch (err) {
        module_permissions_dict[$(this).attr("data-id")] = { is_update: 0 };
      }
    }
  });

  $('input[name="is_delete[]"]:checked').each(function () {
    if (this.checked) {
      try {
        Object.assign(module_permissions_dict[$(this).attr("data-id")], {
          is_delete: 1,
        });
      } catch (err) {
        module_permissions_dict[$(this).attr("data-id")] = { is_delete: 1 };
      }
    } else {
      try {
        Object.assign(module_permissions_dict[$(this).attr("data-id")], {
          is_delete: 1,
        });
      } catch (err) {
        module_permissions_dict[$(this).attr("data-id")] = { is_delete: 1 };
      }
    }
  });

  if (Object.keys(module_permissions_dict).length > 0) {
    swal
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, add module permission(s)!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then(function (result) {
        if (result.value) {
          $.ajax({
            url: window.location,
            type: "POST",
            dataType: "JSON",
            data: { module_permissions_dict: module_permissions_dict },
            success: function (res) {
              if (res.status) {
                Swal.fire({
                  title: "Module Permissions!",
                  text: res.message,
                  type: "success",
                }).then((result) => {
                  // Reload the Page
                  window.location.replace(
                    base_url + "permission/view/" + permissionID
                  );
                });
                // $("#entry_item_table")
                //     .DataTable()
                //     .ajax.reload();
                // swal.fire(
                //     "Reconciled!",
                //     res.message,
                //     "success"
                // );
              } else {
                swal.fire("Oops!", res.message, "error");
              }
            },
          });
        } else if (result.dismiss === "cancel") {
          swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
  }
});
