"use strict";
var TransactionPayment = (function () {
    let tableElement = $("#transaction_payment_table");

    var TransactionPaymentTable = function () {
        var dataTable = tableElement.DataTable({
            order: [[9, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'transaction_payment/showItems',
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
    
            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'transaction_id',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'property_id',
                },
                {
                    data: 'buyer_id',
                },
                {
                    data: 'or_number',
                },
                {
                    data: 'or_date',
                },
                {
                    data: 'period_id',
                },
                {
                    data: 'payment_date',
                },
                {
                    data: 'amount_paid',
                },
                {
                    data: 'remarks',
                },
                {
                    data: 'entry',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
                                <span class="dropdown"><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="` + base_url + `transaction_payment/prnt/` + row.id + `"><i class="la la-edit"></i> Print </a>
                                        <a class="dropdown-item" href="` + base_url + `transaction_payment/entries_form/` + row.id + /0/ + row.principal_amount + '/' + row.interest_amount + '/' + 0 +`"><i class="la la-edit"></i> Add Entries </a>
                                        <a class="dropdown-item" href="` + base_url + `transaction_payment/re_generate_entries/` + row.id +`"><i class="la la-edit"></i> Regenerate Entries </a>
                                        <a class="dropdown-item" href="` + base_url + `accounting_entries/view/` + row.entry_id + `"><i class="la la-eye"></i> View Entry </a>
                                    </div>
                                </span>
                                <a href="` + base_url + `transaction/view/` + row.transaction_id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"> <i class="la la-eye"> </i> </a>`
                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: 'dt-center',
                    // orderable: false,
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
                      <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
                      </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                // {
                //     targets: 7,
                //     render: function (data, type, row, meta) {
                //         if (data == 0) {
                //             return `No entries found`;
                //         }
    
                //         return `<a class="btn btn-primary" href="${base_url}/accounting_entries/view/${data}" target="_blank" rel="noopener noreferrer">View Entry</a>`;
                //     },
                // },
                {
                    targets: 8,
                    render: function (data, type, row, meta) {
                        // '1' => 'Reservation', '2' => 'Downpayment', '3' => 'Loan', '4' => 'Equity', '5' => 'Others'
                        var transaction_payment_periods = {
                            1: {
                                title: 'Reservation',
                            },
                            2: {
                                title: 'Downpayment',
                            },
                            3: {
                                title: 'Loan',
                            },
                            4: {
                                title: 'Equity',
                            },
                            5: {
                                title: 'Others',
                            },
                        };
    
                        if (
                            typeof transaction_payment_periods[data] ===
                            'undefined'
                        ) {
                            return ``;
                        }
    
                        return transaction_payment_periods[data].title;
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });
    
        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on("click", ".remove_project", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "project/delete/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            id: id
                        },
                        success: function (res) {
                            if (res.status) {
                                $("#project_table").DataTable().ajax.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on("click", "#btn_upload_guide", function () {
            var table = $("#upload_guide_table");

            table.DataTable({
                order: [
                    [0, "asc"]
                ],
                pagingType: "full_numbers",
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + "transaction_payment/get_table_schema",
                columns: [
                    // {data: 'button'},
                    {
                        data: "no"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "type"
                    },
                    {
                        data: "format"
                    },
                    {
                        data: "option"
                    },
                    {
                        data: "required"
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear apply.daterangepicker cancel.daterangepicker', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $("#transaction_payment_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            // var rows = _table.rows({ 'search': 'applied' }).nodes();

            // $('input[type="checkbox"]').prop( 'checked', this.checked );
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#project_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "transaction_payment/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#project_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    var _add_ons = function () {
        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };
    
    var clickPrevMonth = function (){
        $("#prev_month").on("click", function () {
            summary_collections('monthly',1);
        });

    };

    var clickThisMonth = function (){
        $("#this_month").on("click", function () {
            summary_collections('monthly',0);
        });

    };

    var clickThisWeek = function (){
        $("#this_week").on("click", function () {
            summary_collections('weekly',0);
        });

    };
    var clickPrevWeek = function (){
        $("#prev_week").on("click", function () {
            summary_collections('weekly',1);
        });

    };

    var summary_collections = function (d,e) {
        if (typeof e ==="undefined"){
            e=0;
        };
        console.log(d,e);
        if(d=='daily'){

            $.ajax({
                url: base_url + "transaction_payment/total_day_payments",
                type: "POST",
                dataType: "JSON",
                data : {filter:e},
                success: function (res) {
                    $("#daily_collection").html(res.amount);
                },
            });

        }else if(d=='weekly'){

            $.ajax({
                url: base_url + "transaction_payment/total_week_payments",
                type: "POST",
                dataType: "JSON",
                data : {filter:e},
                success: function (res) {
                    $("#weekly_collection").html(res.amount);
                    $("#daterange_weekly").html(res.week);
                },
            });

        }else if(d=='monthly'){
            
            $.ajax({
                url: base_url + "transaction_payment/total_month_payments",
                type: "POST",
                dataType: "JSON",
                data : {filter:e},
                success: function (res) {
                    $("#monthly_collection").html(res.amount);
                    $("#daterange_monthly").html(res.month);
                },
            });

        }
    };

    // Daterangepicker Init
    var daterangepickerInit = function () {
        if ($('.kt_daterangepicker').length == 0) {
            return;
        }

        var picker = $('.kt_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            // $('#kt_dashboard_daterangepicker_date').html(range);
            // $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                opens: 'left',
                autoUpdateInput: false,
                locale: {
                    format: 'Y-MM-DD',
                    cancelLabel: 'Clear'
                  }
            },
            cb
        );

        picker.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
        });

        picker.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        cb(start, end, '');
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            TransactionPaymentTable();
            filter();
            _selectProp();
            _add_ons();
            upload_guide();
            status();
            clickPrevMonth();
            clickThisMonth();
            clickThisWeek();
            clickPrevWeek();
            summary_collections();
            summary_collections('daily',0);
            summary_collections('monthly',0);
            summary_collections('weekly',0);
            daterangepickerInit();
        },
    };
})();
var load_filters = function (){
    var date_filter = localStorage.getItem('t_payment_filter');
    if(date_filter!==null){
        $('#_column_9').val(date_filter);
        $('#_column_9').trigger('keyup');
        localStorage.removeItem('t_payment_filter');
    }
}
jQuery(document).ready(function () {
    TransactionPayment.init();
    load_filters();
});

$(document).on('click', '#_export_form_btn', function () {

    var exportColumns = $('#_export_form').serialize();
    var advanceSearch = $('#advance_search').serialize();
    var today = new Date();
    var dd = String(today.getDate()).padStart(2,'0');
    var mm = String(today.getMonth() + 1).padStart(2,'0');
    var yyyy = today.getFullYear();
    var today = mm + '/' + dd + '/' + yyyy;
    var dt = $('#transaction_payment_table').DataTable();
    var raw_order = dt.order();
    var order = [dt.column(raw_order[0][0]).dataSrc(),raw_order[0][1]];
    KTApp.block('#_export_option', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Creating Excel File...',
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait',
        },
    });

    $.ajax({
        url: base_url + 'transaction_payment/export',
        xhrFields:{
            responseType: 'blob'
        },
        type : "POST",
        data : exportColumns+'&'+advanceSearch + '&order=' + order ,
        success : function(result) {
            KTApp.unblock('#_export_option');
            var blob = result;
            var downloadUrl = URL.createObjectURL(blob);
            var a = document.createElement("a");
            a.href = downloadUrl;
            a.download = "List of Transactions Official Payments (" + today + ").xls" ;
            document.body.appendChild(a);
            a.click();
        },
    })
});