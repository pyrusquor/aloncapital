// Class definition
var Create = (function () {
    // Private functions
    var privFunctions = function () {
        $(document).on('change', '#payment_date', function () {
            computePenalty();
            computeTblPenalty();
            $('#is_waived').val('1');

            setTimeout(function () {
                id = $('.due_detailed_check').attr('data-id');
                checkDues(id);
                calculateGrandTotal();
            }, 1000);
        });

        $(document).on('change', '.due_detailed_check', function () {
            id = $(this).attr('data-id');
            checkDues(id);
            waivePenalty();
            paymentType();
            calculateGrandTotal();
        });

        $(document).on('change', '#is_waived', function () {
            waivePenalty();
            paymentType();
        });

        $(document).on('change', '#payment_type_id', function () {
            paymentType();
        });

        $(document).on('change', '#receipt_type', function () {
            receiptType();
        });
        $(document).on('change', '#amount_paid', function () {
            paymentType();
        });
    };

    var showSchedule = function () {
        id = $('#transaction_id').val();
        $.ajax({
            url: base_url + 'transaction/view_mortgage/' + id,
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id,
            },
            success: function (res) {
                $('#schedule').html(res.html);
            },
        });
    };

    var showCollections = function () {
        id = $('#transaction_id').val();
        $.ajax({
            url: base_url + 'transaction_payment/view_collections/' + id,
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id,
            },
            success: function (res) {
                $('#collection').html(res.html);
            },
        });
    };

    var formRepeater = function () {
        $(
            '#fees_form_repeater,#promos_form_repeater,#buyer_form_repeater,#seller_form_repeater'
        ).repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            },
        });
    };

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }
    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.datePicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true,
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
    };

    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
            Select.init();
        });

        wizard.on('beforePrev', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop(); // don't go to the next step
            }
        });

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    };

    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ':hidden',

            // Validation rules
            rules: {
                'info[period_id]': {
                    required: true,
                },
                'info[payment_type_id]': {
                    required: true,
                },
                'info[payment_date]': {
                    required: true,
                },
                'info[remarks]': {
                    required: false,
                },
                'info[amount_paid]': {
                    notEqual: '0',
                    required: true,
                },
                'info[receipt_type]': {
                    notEqual: '0',
                    required: true,
                },
                'info[is_waived]': {
                    required: {
                        depends: function (element) {
                            if ($('#penalty_amount').val() == 0) {
                                return false;
                            } else {
                                return true;
                            }
                        },
                    },
                },
                // "info[amount_paid]": {
                //     required: {
                //         depends: function (element) {
                //             if ($("#payment_application").val() !== 2 && $("#amount_paid").val() <= 3000) {
                //                 return false;
                //             } else {
                //                 return true;
                //             }
                //         },
                //     }
                // }

            },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
                swal.fire({
                    title: '',
                    text:
                        'There are some errors in your submission. Please correct them.',
                    type: 'error',
                    confirmButtonClass: 'btn btn-secondary',
                });
            },
            // Submit valid form
            submitHandler: function (form) {},
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function (e) {
            let pass = baloonCheck();
            let transaction_id = $('#transaction_id').val();

            e.preventDefault();

            if (validator.form() && pass) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);
                let payment_check = checkPayment();
                let payment_application_check = checkPaymentApplication();
                console.log(payment_application_check)

                if (!payment_check) {
                    swal.fire(
                        'Oops!',
                        'Your payment amount exceeds to the current Downpayment amount. Please change the financing scheme if you wish to continue.',
                        'error'
                    );
                    return;
                }

                if (!payment_application_check) {
                    swal.fire(
                        'Oops!',
                        'Your payment application is not valid, please choose the correct payment application for this payment.',
                        'error'
                    );
                    return;
                }

                var moneyFields = $(
                    '#penalty_amount, #amount_paid, #cash_amount, #check_deposit_amount, #adhoc_payment, #rebate_amount'
                );

                $.each(moneyFields, function () {
                    var parsedVal = parseFloat($(this).val().replace(/,/g, ''));

                    $(this).val(parsedVal);
                });

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });
                formEl.ajaxSubmit({
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
						KTApp.unblock('#kt_content');
                        if (response.status) {
                            swal.fire({
                                title: 'Success!',
                                text: response.message,
                                type: 'success',
                            }).then(function () {
                                window.location.replace(
                                    base_url + 'transaction/view/' + transaction_id
                                );
                            });
                        } else {
                            swal.fire('Oops!', response.message, 'error');
                        }
                    },
                });
            } else {
                swal.fire(
                    'Oops!',
                    'Balloon Payment should not be lower than ₱ 3,000.',
                    'error'
                );
            }
        });

        var arbtn = formEl.find('[data-ktwizard-type="action-submit-arnote"]');
        arbtn.on('click', function (e) {
            e.preventDefault();
            transaction_id = $('#transaction_id').val();
            remarks = $.trim($('#remarks').val());

            if (remarks) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, please proceed.',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'ticketing/form/' + transaction_id,
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                transaction_id: transaction_id,
                                remarks: remarks,
                                category: 'ar_notes',
                            },
                            success: function (response) {
                                if (response.status) {
                                    swal.fire({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                    }).then(function () {
                                        $('.view_ar_note').trigger('click');
                                    });
                                } else {
                                    swal.fire(
                                        'Oops!',
                                        response.message,
                                        'error'
                                    );
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            } else {
                swal.fire(
                    'Error',
                    'Remarks field should not be empty!',
                    'error'
                );
            }
        });

        var saveClearingBtn = formEl.find(
            '[data-ktwizard-type="action-submit-for-clearing"]'
        );
        saveClearingBtn.on('click', function (e) {
            e.preventDefault();
            transaction_id = $('#transaction_id').val();


            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(saveClearingBtn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: 'Success!',
                                text: response.message,
                                type: 'success',
                            }).then(function () {
                                window.location.replace(
                                    base_url + 'transaction/view/' + transaction_id
                                );
                            });
                        } else {
                            KTApp.unprogress(saveClearingBtn);
                            swal.fire('Oops!', response.message, 'error');
                        }
                    },
                });
            }
        });
    };

    var paymentCheck = function () {
        var paymentType = $('#payment_type_id');

        // data-ktwizard-type="action-submit-arnote"
        // data-ktwizard-type="action-submit"

        $(paymentType).on('change', function () {
            var btn = $('#submit_btn');
            var arbtn = $('#saveAr_btn');

            var payment_type_id = parseInt($('#payment_type_id').val());
            if (
                payment_type_id == 2 ||
                payment_type_id == 3 ||
                payment_type_id == 4 ||
                payment_type_id == 6
            ) {
                // Add disabled class on submit btn
                $(btn).attr('disabled', true);
                // Remove data-ktwizard-type
                $(arbtn).attr('disabled', false);
                // Add data-ktwizard-type
            } else {
                // Add disabled class on submit btn
                $(arbtn).attr('disabled', true);
                // Remove data-ktwizard-type
                // Remove disabled class on submit btn
                $(btn).attr('disabled', false);
                // Add data-ktwizard-type
            }
        });
    };

    // Public functions
    return {
        init: function () {
            privFunctions();
            datepicker();
            formRepeater();
            showSchedule();
            showCollections();
            paymentCheck();

            wizardEl = KTUtil.get('kt_wizard_v3');
            formEl = $('#form');

            initWizard();
            initValidation();
            initSubmit();

            const period_type = parseInt($('#period_id').val());

            setTimeout(function () {
                id = $('.due_detailed_check').attr('data-id');
                if (period_type !== 1) {
                    checkDues(id);
                } else {
                    return;
                }
            }, 1000);
        },
    };
})();

// Initialization
jQuery(document).ready(function () {
    Create.init();
});

var computePenalty = function () {
    payment_date = $('#payment_date').val();
    transaction_id = $('#transaction_id').val();
    amount_paid = parseFloat($('#amount_paid').val());

    $.ajax({
        url:
            base_url +
            'transaction_payment/computePenalty/' +
            transaction_id +
            '/' +
            payment_date +
            '/' +
            amount_paid,
        dataType: 'json',
        success: function (response) {
            p_amount = parseFloat(response.amount);
            new_amount = parseFloat(response.new_amount);

            $('#penalty_amount').val(parseFloat(p_amount));
            $('#amount_paid').val(parseFloat(new_amount));
        },
    });
};

var computeTblPenalty = function () {
    payment_date = $('#payment_date').val();
    transaction_id = $('#transaction_id').val();

    $.ajax({
        url:
            base_url +
            'transaction_payment/tablePenalty/' +
            transaction_id +
            '/' +
            payment_date,
        dataType: 'json',
        success: function (response) {
            $('#period_count').val(response.period_count);
            $('#detailed_due').html(response.html);
        },
    });
};

var checkDues = function (id = 0) {
    var amount = 0;
    var total_amount = 0;
    var total_penalty = 0;
    var dis = 1;
    var period_type = $('#period_id').val();
    var period_count = 0;
    var penalty_type = $('#penalty_type').val();

    $('.due_detailed_check').each(function () {
        var my_id = parseInt($(this).attr('data-id'));

        if (id > my_id) {
            $(this).prop('disabled', true);
            $(this).prop('checked', true);
        } else {
            $(this).prop('disabled', false);
        }
        i = $(this).is(':checked');

        var amount = parseFloat($(this).attr('data-amount'));
        var penalty = parseFloat($(this).attr('data-penalty'));

        if (i) {
            total_amount = total_amount + amount;
            if (penalty_type == '2') {
                total_penalty = $('#total_penalty').attr('data-total-penalty')
                    ? parseFloat($('#total_penalty').attr('data-total-penalty'))
                    : 0;
            } else {
                total_penalty = total_penalty + penalty;
            }
            $('#transaction_payment_id').val(
                $(this).attr('data-transaction-payment-id')
            );
        } else {
            dis = 1;
        }

        if (my_id == id) {
            prev_id = id - 1;
            if (!i) {
                $('.due_detailed_check[data-id=' + prev_id + ']').prop(
                    'disabled',
                    false
                );
            }
        }
    });

    period_count = $('.due_detailed_check:checked').length;
    $('#amount_paid').val(parseFloat(total_amount).toFixed(2));
    $('#penalty_amount').val(parseFloat(total_penalty).toFixed(2));
    $('#period_count').val(period_count);
    var t_amount = document.getElementById('total_amount');
    var p_amount = document.getElementById('total_penalty');

    if (t_amount) {
        t_amount.setAttribute('data-total-amount', total_amount);
        t_amount.innerHTML = formatMoney(parseFloat(total_amount).toFixed(2));
        p_amount.setAttribute('data-total-penalty', total_penalty);
        p_amount.innerHTML = formatMoney(parseFloat(total_penalty).toFixed(2));
    }

    if (dis) {
        $('.due_detailed_check_all').prop('checked', false);
    } else {
        $('.due_detailed_check_all').prop('checked', true);
    }
};

var waivePenalty = function () {
    val = parseInt($('#is_waived').val());

    var penalty_amount;
    var total_amount = parseFloat($('#total_amount').attr('data-total-amount'));
    penalty_amount = parseFloat($('#penalty_amount').val());

    if (val == 1) {
        remaining = total_amount;
    } else {
        remaining = total_amount + penalty_amount;
    }

    $('#amount_paid').val(parseFloat(remaining).toFixed(2));
};

var paymentType = function () {
    // <option value="1">Cash</option>
    // <option value="2">Regular Cheque</option>
    // <option value="3">Post Dated Cheque</option>
    // <option value="4">Online Deposits</option>
    // <option value="5">Wire Transfer</option>
    // <option value="6">Cash &amp; Cheque</option>
    // <option value="7">Cash &amp; Bank Remittance</option>
    // <option value="8">Journal Voucher</option>
    // <option value="9">Others</option>

    payment_type_id = parseInt($('#payment_type_id').val());
    amount_paid = parseFloat($('#amount_paid').val().replace(/,/g, ''));

    $('#cash_amount').val(0);
    $('[data-field-type=regular_cheque]').addClass('hide');


    if (payment_type_id == 1) {
        $('#cash_amount').attr('readonly', false);
        $('#check_deposit_amount').attr('readonly', true);

        $('#cash_amount').val(
            parseFloat(amount_paid)
                .toFixed(2)
                .replace(/\d(?=(\d{3})+\.)/g, '$&,')
        );
    } else if (
        payment_type_id == 2 ||
        payment_type_id == 3 ||
        payment_type_id == 4
    ) {
        $('#cash_amount').attr('readonly', true);
        $('#check_deposit_amount').attr('readonly', false);

        $('#check_deposit_amount').val(
            parseFloat(amount_paid)
                .toFixed(2)
                .replace(/\d(?=(\d{3})+\.)/g, '$&,')
        );

        if (payment_type_id == 2) {
            $('[data-field-type=regular_cheque]').removeClass('hide');
        }

    } else if (
        payment_type_id == 6 ||
        payment_type_id == 7 ||
        payment_type_id == 9
    ) {
        $('#cash_amount').attr('readonly', false);
        $('#check_deposit_amount').attr('readonly', false);
    } else if (payment_type_id == 8) {
        $('#cash_amount').attr('readonly', true);
        $('#check_deposit_amount').attr('readonly', true);
    }
};

var receiptType = function () {
    // <option value="0">Select Type</option>
    // <option value="1">Collection Receipt</option>
    // <option value="2">Acknowledgement Receipt</option>
    // <option value="3">Provisionary Receipt</option>
    // <option value="4">Official Receipt</option>

    receipt_type = parseInt($('#receipt_type').val());

    var ornumber = ' ';

    if (receipt_type == 1) {
        ornumber = 'CR-';
    } else if (receipt_type == 2) {
        ornumber = 'AR-';
    } else if (receipt_type == 3) {
        ornumber = 'PR-';
    } else if (receipt_type == 4) {
        ornumber = 'OR-';
    } else if (receipt_type == 5) {
        ornumber = ' ';
    }

    $('#OR_number').val(ornumber);
};

jQuery.validator.addMethod(
    'notEqual',
    function (value, element, param) {
        return this.optional(element) || value != param;
    },
    'Please specify a different (non-default) value'
);

const pastDueCheck = () => {
    let billings = JSON.parse($('#billings').html());
    let reservationDate = new Date($('#reservation_date').html());

    let billingTermDate;

    Object.keys(billings).forEach(function (key) {
        if (billings[key].period_id === '2') {
            billingTermDate = new Date(billings[key].effectivity_date);
        }
    });

    if (reservationDate > billingTermDate) {
        notifyCashier();
    }
};

let ticketExist = (transaction_id, remarks) => {
    let response = $.ajax({
        url: base_url + 'transaction_payment/check_ticket/' + transaction_id,
        type: 'POST',
        dataType: 'JSON',
        data: {
            transaction_id: transaction_id,
            remarks: remarks,
        },
        success: function (response) {
            return response;
        },
    });

    return response;
};

const notifyCashier = () => {
    transaction_id = $('#transaction_id').val();
    remarks = 'Past due for reservation exceeded the first downpayment.';

    let ticket = ticketExist(transaction_id, remarks);

    setTimeout(() => {
        if (ticket.responseJSON.status === 1) {
            $('.view_ar_note').trigger('click');
        } else {
            $.ajax({
                url: base_url + 'ticketing/form/' + transaction_id,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    transaction_id: transaction_id,
                    remarks: remarks,
                    category: 'ar_notes',
                },
                success: function (response) {
                    if (response.status) {
                        $('.view_ar_note').trigger('click');
                    } else {
                        swal.fire('Oops!', response.message, 'error');
                    }
                },
            });
        }
    }, 3000);
};

const baloonCheck = () => {
    let paymentApp = $('#payment_application').val();
    let amountPaid = $('#amount_paid').val();

    let pass = true;

    if (paymentApp == 2 && amountPaid <= 3000) {
        pass = false;
    } else {
        pass = true;
    }
    return pass;
};

// Check if amount paid is greater than the total downpayment balance
const checkPayment = () => {
    let period_id = $('#period_id').val();
    let amount_paid = parseFloat($('#amount_paid').val());
    let dp_amount = parseFloat($('#dp_amount').val());
    let total_penalty = $('#detailed_information #total_penalty');

    if (period_id == '2') {
        total =
            parseFloat(dp_amount) +
            parseFloat($(total_penalty).data('total-penalty'));
        if (amount_paid > total) {
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
};

// const checkPaymentApplication = () => {

//     let amount_paid = parseFloat($('#amount_paid').val().replace(/,/g, ''));
//     let penalty_amount = parseFloat($('#penalty_amount').val().replace(/,/g, ''));
//     let total_amount_due = parseFloat($('#total_amount').attr('data-total-amount'));
//     let is_waived = parseInt($('#is_waived').val());
//     payment_application = $('#payment_application').val();

//     let i = 0;

//     $('.due_detailed_check').each(function () {
//         if($(this).is(':checked')){
//             i++;
//         };
//     });

//     if (payment_application == 0) {

//         console.log(amount_paid+" "+total_amount_due);

//         if (i == 1) {

//             if (is_waived) {
//                  // if (amount_paid == total_amount_due) {
//                     return true;
//                 // } else {
//                     // return false;
//                 // }
//             } else {
//                 return true;
//             }

//         } else {
//             return false;
//         }

//     } else if (payment_application == 4) {

//         console.log(amount_paid+" "+total_amount_due);

//         if (i == 1) {

//             if (is_waived) {
//                 if (amount_paid < total_amount_due) {
//                     return true;
//                 } else {
//                     return false;
//                 }
//             } else {
//                 return true;
//             }
            
//         }

//     }  else {
//         return true;
//     }
   
   
// };

const checkPaymentApplication = () => {

    let is_waived = parseInt($('#is_waived').val());
    amount_paid = parseFloat($('#amount_paid').val().replace(/,/g, ''));
    grand_total = parseFloat($('#grand_total').val().replace(/,/g, ''));

    period_id = $('#period_id').val();
    payment_application = $('#payment_application').val();
    monthly_ra = parseFloat($('#monthly_ra').val());
    monthly_dp = parseFloat($('#monthly_dp').val());
    monthly_loan = parseFloat($('#monthly_loan').val());
    total_amount_due = parseFloat($('#total_amount').attr('data-total-amount'));

    // grand_total =  parseFloat($('#grand_total').val());

    if (period_id > 3) {
        return true;
    }

    if (!is_waived) {
        return true;
    }

    if (period_id == 1) {
        amount_due = monthly_ra;
    }else if (period_id == 2) {
        amount_due = monthly_dp;
    }else if (period_id == 3) {
        amount_due = monthly_loan;
    }

    if(isNaN(total_amount_due)){
        total_amount_due = 0;
    }

    console.log(amount_due + ' ' + grand_total + ' ' + total_amount_due);

    if (payment_application == 0) {
        if (grand_total == amount_due) {
            return true;
        } else {
            if (grand_total == total_amount_due) {
                return true;
            } else {
                return false;
            }
        }
    } else if (payment_application == 4) {
        if (grand_total < amount_due) {
            return true;
        } else {
            return false;
        }
    }  else if ((payment_application == 2)||(payment_application == 1)||(payment_application == 3)) {
        if (grand_total > amount_due) {
            return true;
        } else {
            return false;
        }
    }  else {
        return true;
    }
   
   
};



pastDueCheck();

$('#payment_application').on('change', function () {
    $('#remarks').val('');
    switch ($(this).val()) {
        case '1':
            $('#remarks').val('Advance payment');
            break;
        case '2':
            $('#remarks').val('Balloon payment');
            break;
        case '3':
            $('#remarks').val('Continuous Payment');
            break;
        case '4':
            $('#remarks').val('Partial Payment');
            break;
        default:
            $('#remarks').val('Regular payment');
            break;
    }
});

$('#amount_paid, #is_waived, #adhoc_payment, #rebate_amount').on(
    'change keyup blur paste',
    function () {
        calculateGrandTotal();
    }
);

const calculateGrandTotal = () => {
    setTimeout(() => {
        let amount_paid = $('#amount_paid').val() ? $('#amount_paid').val() : 0;
        let adhoc_payment = $('#adhoc_payment').val()
            ? $('#adhoc_payment').val()
            : 0;
        let rebate_amount = $('#rebate_amount').val()
            ? $('#rebate_amount').val()
            : 0;
        console.log(amount_paid,adhoc_payment)
        
        let grand_total =
            parseFloat(amount_paid.replace(/[, ]+/g, "").trim()) +
            parseFloat(rebate_amount.replace(/[, ]+/g, "").trim()) +
            parseFloat(adhoc_payment.replace(/[, ]+/g, "").trim());

        $('#grand_total').val(grand_total.toFixed(2));
    }, 1000);
};
