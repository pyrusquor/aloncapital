// Class definition
var KTFormControls = function () {
    // Private functions

    var valWarehouse = function () {
        $( "#warehouse_form" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true
                },
                /* ==================== begin: Add model fields ==================== */
                warehouse_code: {
                    required: true
                },
                sbu_id: {
                    required: true
                },
                address: {
                    required: false
                },
                telephone_number: {
                    required: false
                },
                fax_number: {
                    required: false
                },
                mobile_number: {
                    required: false
                },
                email: {
                    required: false
                },
                contact_person: {
                    required: true
                },
                is_active: {
                    required: true
                }
                /* ==================== end: Add model fields ==================== */
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {

                toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    };

    return {
        // public functions
        init: function() {
            valWarehouse();
        }
    };
}();


var KTBootstrapDatepicker = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd'
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
    };

    return {
        // public functions
        init: function() {
            datepicker();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormControls.init();
    KTBootstrapDatepicker.init();
});