$(document).ready(function () {
    var datepicker = function () {
        // minimum setup
        $('#filter_request_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function (e) {
            app.changeFilter('request_date', $('#filter_request_date').val());
        });

    };

    datepicker();

    $("#company_id").on("change", function (e) {
        app.changeInfo('company_id', $(this).val());
    });

    $("#accounting_ledger_id").on("change", function (e) {
        app.changeInfo('accounting_ledger_id', $(this).val());
    });

    $("#warehouse_id").on("change", function (e) {
        app.changeInfo('warehouse_id', $(this).val());
    });

    $("#requesting_staff_id").on("change", function (e) {
        app.changeInfo('requesting_staff_id', $(this).val());
    });

    $("#approving_staff_id").on("change", function (e) {
        app.changeInfo('approving_staff_id', $(this).val());
    });

    $("#filter_company_id").on("change", function (e) {
        app.changeFilter('company_id', $(this).val());
    });

    $("#filter_accounting_ledger_id").on("change", function (e) {
        app.changeFilter('accounting_ledger_id', $(this).val());
    });


});

const __rpo_filter_defaults = {
    reference: null,
    request_date: null,
    company_id: null,
    accounting_ledger_id: null
}

const __info_form_defaults = {
    id: null,
    company_id: null,
    warehouse_id: null,
    accounting_ledger_id: null,
    status: 1,
    purchase_order_request_id: null
}

var app = new Vue({
    el: '#canvassing_app',
    data: {
        object_request_id: null,
        rpo: {
            filter: {},
            data: [],
            items: [],
            selected: null,
        },
        info: {
            form: {},
            required: {
                'company_id': 'Company',
                'approving_staff_id': 'Approving Staff',
                'requesting_staff_id': 'Requesting Staff',
                // 'accounting_ledger_id': 'Expense Account',
                'purchase_order_request_id': 'Purchase Request',
            },
            check: {
                is_valid: false,
                missing_fields: []
            }
        },
        items: {
            form: [],
            data: [],
            selected: null,
            required: {
                'supplier_id': 'Supplier',
                'unit_price': 'Unit Price'
            },
            check: {
                is_valid: false,
            },
            deleted_items: []
        },
        sources: {}
    },
    watch: {},
    methods: {
        // region init
        initObjectRequest() {
            this.object_request_id = window.object_request_id;
            this.fetchObjectRequest();
        },
        // endregion

        // region items form
        loadItem(idx) {
            this.items.loaded = this.items.data[idx].id;
            this.items.selected = this.items.data[idx];
            this.items.form.unit_cost = this.items.selected.unit_cost;
            this.items.form.supplier_id = this.items.selected.supplier_id;
        },

        removeItem(idx) {
            if (this.items.data[idx].id) {
                this.items.deleted_items.push(this.items.data[idx].id);
            }
            this.items.data.splice(idx, 1);
        },

        async addItemToCart(isNew, data) {
            let item = {};

            if (! data) {
                for (key in this.items.selected) {
                    item[key] = this.items.selected[key];
                }

                if(! this.items.loaded){
                    delete item['id'];
                    delete item['total_cost'];
                    delete item['created_by'];
                    delete item['created_at'];
                    delete item['updated_at'];
                    delete item['updated_by'];
                    delete item['deleted_by'];
                    delete item['deleted_at'];
                }
                item.supplier_id = this.items.form.supplier_id;
                item.company_id = this.info.form.company_id;
                item.unit_cost = this.items.form.unit_cost;
            } else {
                for (key in data) {
                    item[key] = data[key];
                    if(key === 'company_id' && (data[key] === null || data[key] == 0) ){
                        item.company_id = this.info.form.company_id;
                    }
                }
            }

            await axios.get(base_url + "generic_api/fetch_specific?table=suppliers&field=id&value=" + item.supplier_id).then(
                response => {
                    if (response.data.length > 0) {
                        item['supplier'] = response.data[0].name;
                        if(item.id){
                            let idx = this.searchItemInStore(item.id);
                            if(idx !== null){
                                this.items.data[idx] = item;
                            }else{
                                this.items.data.push(item);
                            }
                        }else{
                            this.items.data.push(item);
                        }
                        this.resetItemForm();
                    }
                }
            )
        },

        searchItemInStore(id){
            for(let i = 0; i < this.items.data.length; i++){
                if(id == this.items.data[i].id){
                    return  i;
                }
            }
            return null;
        },

        resetItemForm() {
            this.items.selected = null;
            this.items.loaded = null;
            this.items.form = {};
        },

        checkItemsForm() {
            this.items.check.is_valid = true;
            if (this.items.data.length <= 0) {
                this.items.check.is_valid = false;
            }
        },

        prepItemsFormData() {
            return this.items.data;
        },
        // endregion

        // region rpo filters
        selectItemForCanvass(idx) {
            this.items.selected = this.rpo.items[idx];
            this.items.selected.purchase_order_request_item_id = this.rpo.items[idx].id;
        },

        resetRPOFilter() {
            this.rpo.data = [];
            this.rpo.items = [];
            this.rpo.selected = null;
            this.info.form.purchase_order_request_id = null;
            $('#filter_request_date').val(null);
            $("#filter_company_id").val(null);
            $("#filter_accounting_ledger_id").val(null);
            for (var d in __rpo_filter_defaults) {
                this.rpo.filter[d] = __rpo_filter_defaults[d];
            }
        },

        changeFilter(key, val) {
            this.rpo.filter[key] = val;
        },

        filterRPO() {
            let params = $.param(this.rpo.filter);
            let url = base_url + "purchase_order_request/search?with_relations=yes&" + params;
            axios.get(url)
                .then(response => {
                    this.rpo.data = response.data;
                })
        },

        selectRPO(key) {
            this.rpo.selected = this.rpo.data[key];
            this.info.form.purchase_order_request_id = this.rpo.selected.id;
        },
        // endregion

        // region parsers
        parseObjectRequest(data) {
            if (data.length > 0) {
                this.info.form = data[0];
                this.fetchObjectRequestChildren();

                if(this.object_request_id){
                    this.rpo.filter = window.purchase_order_request;
                    this.filterRPO();
                }
            }
        },

        parseObjectRequestItems(data) {
            for (row in data) {
                this.addItemToCart(false, data[row]);
            }
        },

        // endregion

        // region fetchers
        lookupInject(table, field, val, ret_field) {
            let url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + val;
            let retval = null;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        if (ret_field) {
                            retval = response.data[0][ret_field];
                        } else {
                            retval = response.data;
                        }
                    }
                    return retval;
                })
        },

        fetchObjectRequest() {
            if (this.object_request_id) {
                let url = base_url + "canvassing/search?id=" + this.object_request_id + "&with_relations=no";
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectRequest(response.data);

                        }
                    });
            } else {
                this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
            }
            // this.request_items.cart = JSON.parse(JSON.stringify(__request_items_data_defaults));
        },
        fetchObjectRequestChildren() {
            let url = base_url + "generic_api/fetch_specific?table=canvassing_items&field=canvassing_id&value=" + this.object_request_id;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.parseObjectRequestItems(response.data);
                    }
                });
        },
        async fetchInfoObjects(table, field, value, store, order) {
            if (!order) {
                order = "name ASC";
            }
            let url = base_url + "generic_api/fetch_all?table=" + table + "&order=" + order;
            if (field && value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + value + "&order=" + order;
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        //this.info.data[store] = response.data;
                        this.$set(this.info.data, store, response.data);
                    } else {
                        this.$set(this.info.data, store, []);
                    }
                });
        },
        async fetchItemObjects(table, cat, store, value, fk, order) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            let order_by = null;
            if (order) {
                order_by = "&order=" + order;
            }
            if (value) {
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + fk + "&value=" + value + order_by;
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        if (cat === 'rpo') {
                            this.$set(this.rpo, store, response.data);
                        }
                    } else {
                        if (cat === 'rpo') {
                            this.$set(this.rpo.data, store, []);
                        }
                    }
                    this.$forceUpdate();
                });
        },
        fetchObjects(table, lookup_field) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.sources[table] = response.data;
                    } else {
                        this.sources[table] = [];
                    }
                })
        },
        // endregion

        // region info form
        changeInfo(key, val) {
            this.info.form[key] = val;
        },

        prepInfoFormData() {
            return this.info.form;
        },

        checkInfoForm() {
            this.info.check.missing_fields = [];
            this.info.check.is_valid = true;
            for (var key in this.info.required) {
                if (!this.info.form[key]) {
                    this.info.check.missing_fields.push(this.info.required[key]);
                    this.info.check.is_valid = false;
                }
            }
        },
        submitRequest() {
            this.checkInfoForm();
            let message = '';
            if (!this.info.check.is_valid) {
                message = 'Missing fields: ' + this.info.check.missing_fields.join(', ');
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            this.checkItemsForm();
            if (!this.items.check.is_valid) {
                message = 'Please add Canvass items';
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            let url = base_url + "canvassing/process_create";
            if (this.object_request_id) {
                url = base_url + "canvassing/process_update/" + this.object_request_id;
            }

            let data = {
                request: this.prepInfoFormData(),
                items: this.prepItemsFormData(),
                deleted_items: this.items.deleted_items
            }

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.status) {
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                base_url + "canvassing"
                            );
                        });
                    }
                }
            })

        },

        prepInfoData() {

        },

        prepObjectRequestItems() {

        },

        searchField(fields, field) {
            for (key in fields) {
                if (field === key) {
                    return true;
                }
            }
            return false;
        },
        // endregion

    },
    mounted() {

        this.initObjectRequest();

        this.is_mounted = true;
        this.object_request_id = window.object_request_id;

        // this.resetRPOFilter();
        this.fetchObjects('suppliers');

    }
});