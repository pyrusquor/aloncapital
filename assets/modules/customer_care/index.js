"use strict";
var CustomerCare = (function () {
  var yearFilter = function () {
    function filter(year) {
      var year = year ? year : "";

      $.ajax({
        url: base_url + "customer_care/complaint_annual_report/" + year,
        method: "POST",
        data: "year=" + year,
        success: function (html) {
          setTimeout(function () {
            window.location.reload();
          }, 500);
        },
      });
    }

    $("#filter_report").submit(function (e) {
      var year = $("#year").val();

      if (year !== "") {
        filter(year);
      } else {
        filter();
      }
    });
  };

  var daterangepickerInit = function () {
    if ($(".kt_transaction_daterangepicker").length == 0) {
      return;
    }

    var picker = $(".kt_transaction_daterangepicker");
    var start = moment();
    var end = moment();

    function cb(start, end, label) {
      var title = "";
      var range = "";

      if (end - start < 100 || label == "Today") {
        title = "Today:";
        range = start.format("MMM D");
      } else if (label == "Yesterday") {
        title = "Yesterday:";
        range = start.format("MMM D");
      } else {
        range = start.format("MMM D") + " - " + end.format("MMM D");
      }

      $("#kt_dashboard_daterangepicker_date").html(range);
      $("#kt_dashboard_daterangepicker_title").html(title);
    }

    picker.daterangepicker(
      {
        direction: KTUtil.isRTL(),
        startDate: start,
        endDate: end,
        opens: "left",
      },
      cb
    );

    cb(start, end, "");
  };

  return {
    init: function () {
      yearFilter();
      datepicker();
      daterangepickerInit();
    },
  };
})();

jQuery(document).ready(function () {
  CustomerCare.init();
});
