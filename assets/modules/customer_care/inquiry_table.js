'use strict';
var CustomerCare = (function () {
    var ComplaintTable = function () {
        var table = $('#inquiry_table');

        // begin first table
        table.DataTable({
            order: [[0, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,

            ajax: base_url + 'customer_care/showInquiries',
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: 'id',
                },
                /* ==================== begin: Add model fields ==================== */
                {
                    data: 'buyer_id',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'house_model_id',
                },
                {
                    data: 'inquiry_category_id',
                },
                {
                    data: 'inquiry_sub_category_id',
                },
                {
                    data: 'year',
                },
                {
                    data: 'month',
                },
                {
                    data: 'reference',
                },
                {
                    data: 'buyer_name',
                },
                {
                    data: 'created_at',
                },
                {
                    data: 'project_name',
                },
                {
                    data: 'block_lot',
                },
                {
                    data: 'turn_over_date',
                },
                {
                    data: 'warranty',
                },
                {
                    data: 'house_model_name',
                },
                {
                    data: 'description',
                },
                {
                    data: 'forwarded_date',
                },
                {
                    data: 'closed_date',
                },
                /* ==================== end: Add model fields ==================== */
            ],
            columnDefs: [
                {
                    targets: [0],
                    className: 'dt-center',
                },
                {
                    targets: [1, 2, 3, 4, 5],
                    visible: false,
                },
                /* ==================== begin: Add target fields for dropdown value ==================== */

                /* ==================== end: Add target fields for dropdown value ==================== */
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        var oTable = table.DataTable();
        $('#generalSearch').blur(function () {
            oTable.search($(this).val()).draw();
        });
    };

    var _add_ons = function () {
        var dTable = $('#inquiry_table').DataTable();

        $('#_search').on('keyup', function () {
            dTable.search($(this).val()).draw();
        });

        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var _filterComplaints = function () {
        var dTable = $('#inquiry_table').DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($('#_column_' + n).val())
                .draw();
        }

        $('._filter').on('keyup change clear', function () {
            _colFilter($(this).data('column'));
        });
    };

    return {
        init: function () {
            ComplaintTable();
            _add_ons();
            status();
            _filterComplaints();
        },
    };
})();

jQuery(document).ready(function () {
    CustomerCare.init();
});

const houseModel = () => {
    $('#_column_2').on('change', function () {
        var e = document.getElementById('_column_2');
        var projectId = e.options[e.selectedIndex].value;

        document.getElementById('project_id').setAttribute('value', projectId);
    });
};

const inquiryCategory = () => {
    $('#_column_4').on('change', function () {
        var e = document.getElementById('_column_4');
        var categoryId = e.options[e.selectedIndex].value;

        document
            .getElementById('inquiry_category_id')
            .setAttribute('value', categoryId);
    });
};

houseModel();
inquiryCategory();
