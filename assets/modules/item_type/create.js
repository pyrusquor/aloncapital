// Class definition
var KTFormControls = function () {
    // Private functions

    var valItemGroup = function () {
        $( "#item_type_form" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true
                },
                type_code: {
                    required: true
                },
                group_id: {
                    required: true,
                    number: true
                },
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {

                toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    };

    return {
        // public functions
        init: function() {
            valItemGroup();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormControls.init();
});