"use strict";
var KTDatatablesLandInventoryDocumentChecklist = function () {

	var _initLandInventoryDocumentChecklist = function () {

		var _table = $('#_land_inventory_document_table');

		_table.DataTable({
			order: [[0, 'desc']],
			pagingType: 'full_numbers',
			lengthMenu: [5, 10, 25, 50, 100],
			pageLength : 10,
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			deferRender: true,
			dom:	"<'row'<'col-sm-12 col-md-6'l>>" +
						"<'row'<'col-sm-12'tr>>" +
						"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			language: {
				'lengthMenu': 'Show _MENU_',
        'infoFiltered': '(filtered from _MAX_ total records)'
			},
			ajax: base_url + 'checklist/get_document_checklist/'+_li_id,
			columns: [
				// {data: 'button'},
				{data: 'id'},
				{data: 'name'},
				{data: 'description'},
				{data: 'owner_id'},
				{data: 'classification_id'},
				{data: 'updated_at'},
				{data: 'Actions', responsivePriority: -1}
			],
			columnDefs: [
				{
					targets: -1,
					orderable: false,
					render: function ( data, type, row, meta ) {

						return `
                    <a href="`+base_url+`document/view/`+row.id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                      <i class="la la-eye"></i>
                    </a>
                  `;
					}
				},
				{
					targets: [0, 3, 4, 5, -1],
					className: 'dt-center',
				},
				{
					targets: 3,
					render: function (data, type, full, meta) {

						var _owner = {
							1: { title: 'Legal Staff' },
							2: { title: 'Legal Head' },
							3: { title: 'Agent' },
							4: { title: 'Sales Admin' },
							5: { title: 'AR Staff' },
							6: { title: 'AR Head' },
							7: { title: 'Titling Officer' },
							8: { title: 'Tax' },
							9: { title: 'Engineering' },
							10: { title: 'Cashier' },
							11: { title: 'Sales Coordinator' },
							12: { title: 'Client' },
							13: { title: 'Credit and Collection' }
						};

						if ( typeof _owner[data] === 'undefined' ) {

							return ``;
						}

						return _owner[data].title;
					}
				},
				{
					targets: 4,
					render: function (data, type, full, meta) {

						var _classification = {
							1: { title: 'Client Document' },
							2: { title: 'Company Document' },
							3: { title: 'Government Document' }
						};

						if ( typeof _classification[data] === 'undefined' ) {

							return ``;
						}

						return _classification[data].title;
					}
				}
			],
			drawCallback: function ( settings ) {

				$('span#_total').text(settings.fnRecordsTotal()+' TOTAL');
			}
		});
	}

	var _add_ons = function () {

		var dTable = $('#_land_inventory_document_table').DataTable();

		$('#_search').on( 'keyup', function () {

			dTable.search($(this).val()).draw();
		});

		$('#kt_datepicker').datepicker({
    	orientation: "bottom left",
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

		$('button#_advance_search_btn').on( 'click', function () {

			$('div#_batch_upload').removeClass('show');
		});

		$('button#_batch_upload_btn').on( 'click', function () {

			$('div#_advance_search').removeClass('show');
		});
	}

	var _filterDocumentChecklist = function () {

		var dTable = $('#_land_inventory_document_table').DataTable();

		function _colFilter ( n ) {

			dTable.column(n).search($('#_column_'+n).val()).draw();
		}

		$('._filter').on( 'keyup change clear', function () {

			_colFilter($(this).data('column'));
		});
	}

	return {

		init: function () {

			_initLandInventoryDocumentChecklist();
			_add_ons();
			_filterDocumentChecklist();
		}
	};
}();

jQuery(document).ready(function() {

    KTDatatablesLandInventoryDocumentChecklist.init();
});