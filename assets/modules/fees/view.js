"use strict";
var CompanyView = function() {

	var initCompanyProjects = function() {
		var table = $('#company_projects');

		// begin first table
		table.DataTable({
			scrollY: '50vh',
			scrollX: true,
            scrollCollapse: true,
            bPaginate: false,
            bLengthChange: false,
            bFilter: true,
            bInfo: false,
            bAutoWidth: false,
            dom:	"<'row'<'col-sm-12 col-md-12'l><'col-sm-12 col-md-12'f>>",
			columnDefs: [
				{
					targets: -1,
					title: 'Actions',
					orderable: false
				},
			],
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initCompanyProjects();
		},

	};

}();

jQuery(document).ready(function() {
	CompanyView.init();
});