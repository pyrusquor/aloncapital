// Class definition
var Form = (function () {
  var arrows;

  if (KTUtil.isRTL()) {
    arrows = {
      leftArrow: '<i class="la la-angle-right"></i>',
      rightArrow: '<i class="la la-angle-left"></i>',
    };
  } else {
    arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    };
  }

  // Private functions
  var datepicker = function () {
    // minimum setup
    $(".kt_datepicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      orientation: "bottom left",
      templates: arrows,
      locale: "no",
      format: "yyyy-mm-dd",
      autoclose: true,
    });

    $(".yearPicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true,
    });
  };

  var formEl;
  var validator;

  var initValidation = function () {
    validator = formEl.validate({
      // Validate only visible fields
      ignore: ":hidden",

      // Validation rules
      rules: {
        name: {
          required: true,
        },
      },
      // messages: {
      // 	"info[last_name]":'Last name field is required',
      // },
      // Display error
      invalidHandler: function (event, validator) {
        KTUtil.scrollTop();

        swal.fire({
          title: "",
          text:
            "There are some errors in your submission. Please correct them.",
          type: "error",
          confirmButtonClass: "btn btn-secondary",
        });
      },

      // Submit valid form
      submitHandler: function (form) {},
    });
  };

  var initSubmit = function () {
    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

    btn.on("click", function (e) {
      e.preventDefault();

      if (validator.form()) {
        // See: src\js\framework\base\app.js
        KTApp.progress(btn);
        //KTApp.block(formEl);

        // See: http://malsup.com/jquery/form/#ajaxSubmit
        formEl.ajaxSubmit({
          type: "POSt",
          dataType: "JSON",
          success: function (response) {
            if (response.status) {
              swal
                .fire({
                  title: "Success!",
                  text: response.message,
                  type: "success",
                })
                .then(function () {
                  window.location.replace(base_url + "inquiry_tickets");
                });
            } else {
              swal.fire({
                title: "Oops!",
                html: response.message,
                icon: "error",
              });
            }
          },
        });
      }
    });
  };

  var _add_ons = function () {
    datepicker();

    $("#turn_over_date").on('change', function () {

      turn_over_date = new Date($(this).val() + "T00:00:00");

      let warranty_date = new Date(turn_over_date.setDate(turn_over_date.getDate() + 180));

      $("#warranty_date").val(warranty_date.toISOString().split('T')[0])
    })
  };

  // Public functions
  return {
    init: function () {
      datepicker();
      _add_ons();

      formEl = $("#form_inquiry_ticket");

      initValidation();
      initSubmit();
    },
  };
})();

// Initialization
jQuery(document).ready(function () {
  Form.init();
});

const inquiryCategorySelect = document.querySelector("#inquiry_category_id");
const inquirySubCategory = document.querySelector("#inquiry_sub_category_id");

const getInquiryCategories = () => {
  $.ajax({
    url: base_url + "inquiry_categories/get_all",
    type: "GET",
    success: function (data) {
      const categories = JSON.parse(data);
      categories.data.map((category) => {
        let opt = document.createElement("option");

        opt.value = category.id;
        opt.innerHTML = category.name;
        inquiryCategorySelect.appendChild(opt);
      });
    },
  });
};

const setInquirySubCategory = () => {
  $("#inquiry_category_id").on("change", function () {
    inquiryCategoryID = $(this).val();
    $.ajax({
      url:
        base_url + "inquiry_sub_categories/inquiry_sub_categories_by_category",
      type: "POST",
      data: {
        inquiryCategoryID,
        inquiryCategoryID,
      },
      success: function (data) {
        const sub_categories = JSON.parse(data);
        inquirySubCategory.innerHTML = "";
        try {
          sub_categories.map((sub_category) => {
            let opt = document.createElement("option");

            opt.value = sub_category.id;
            opt.innerHTML = sub_category.name;
            inquirySubCategory.appendChild(opt);
          });
        } catch (error) {
          let opt = document.createElement("option");

          inquirySubCategory.innerHTML = "";
          opt.value = "";
          opt.innerHTML = "No record found";
          inquirySubCategory.appendChild(opt);
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log(
          thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText
        );
        inquirySubCategory.removeChild();
      },
    });
  });
};

getInquiryCategories();
setInquirySubCategory();
