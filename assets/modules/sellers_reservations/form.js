var KTBootstrapDatepicker = (function () {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }


    var datepicker = function () {
        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
        $(".kt_datepicker").datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: "yyyy-mm-dd",
        });
    };

    return {
        init: function () {
            datepicker();
        },
    };
})();

var KTFormControls = function () {
    // Private functions

    var valReservation = function () {
        validator = formEl.validate({
            // define validation rules
            rules: {
                property_id: {
                    required: true
                },
                seller_id: {
                    required: true
                },
                buyer_id: {
                    required: true
                },
                expected_reservation_date: {
                    required: true
                }, 
                reservation_fee: {
                    required: true
                }, 
                remarks: {
                    required: false
                }, 
                seller_group: {
                    required: true
                }, 
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

                swal.fire(
                    "Oops",
                    "There are some errors in your submission. Please correct them.",
                    "error"
                );
            },

            submitHandler: function (form) {},
        });       
    }

    var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                        type: 'POSt',
                        dataType: 'JSON',
                        success: function(response) {
                            if (response.status) {
                                swal.fire({
                                    title: "Success!",
                                    text: response.message,
                                    type: "success"
                                }).then(function() {
                                    window.location.replace(base_url + "sellers_reservations");
                                });
    
                            } else {
                                swal.fire(
                                    'Oops!',
                                    response.message,
                                    'error'
                                )
                            }
                        }
                    });
			}
		});
    }

    return {
        // public functions
        init: function() {
            formEl = $('#form_sellers_reservation');
            valReservation();
            initSubmit();
        }
    };
}();

$(document).ready(function () {
	KTFormControls.init();
    KTBootstrapDatepicker.init();
})