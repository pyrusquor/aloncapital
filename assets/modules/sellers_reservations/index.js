"use strict";
var SellersReservations = (function () {
    var table = $("#sellers_reservations_table");

    var SellersReservationsTable = function () {

        const toastReservation = (elLists, status) => {
            if (status !== "reserved") {
                // console.log("not reserve");
                elLists.forEach(function (btn) {
                    btn.href = "javascript:void(0)";

                    btn.addEventListener("click", function () {
                        swal.fire(
                            "Error",
                            `This reservation is already ${status}. Cannot do any action.`,
                            "error"
                        );
                    })
                })

            } else {
                elLists.forEach(function (btn) {
                    // console.log("reserve");
                    var id = $(btn).data("id");
                    btn.href = "javascript:void(0)";

                    btn.addEventListener("click", function () {
                        swal.fire({
                            title: "Are you sure?",
                            text: "You won't be able to revert this!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Yes, cancel this reservation!",
                            cancelButtonText: "No!",
                            reverseButtons: true,
                        }).then(function (result) {
                            if (result.value) {
                                $.ajax({
                                    url: base_url + "sellers_reservations/cancel_reservation/" + id,
                                    type: "POST",
                                    dataType: "JSON",
                                    data: {
                                        id: id
                                    },
                                    success: function (res) {
                                        // $("#sellers_reservations_table")
                                        //     .DataTable()
                                        //     .ajax.reload();
                                        setTimeout(() => {
                                            location.reload();
                                        }, 500);
                                        swal.fire("Reservation cancelled!", res.message, "success");
                                    },
                                });
                            } else if (result.dismiss === "cancel") {
                                swal.fire(
                                    "Cancelled",
                                    "Your imaginary file is safe :)",
                                    "error"
                                );
                            }
                        });
                    })
                })

            }
        }

        // begin first table
        var dataTable = table.DataTable({
            order: [
                [1, "asc"]
            ],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,

            ajax: {
                data: function (data) {
                    getFilter(data);
                },
                url: base_url + 'sellers_reservations/showItems',
            },
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [{
                text: '<i class="la la-trash"></i> Delete Selected',
                className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm",
                attr: {
                    id: "bulkDelete",
                },
                enabled: false,
            }, ],
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)",
            },

            columns: [{
                    data: null,
                },
                {
                    data: "id",
                },
                {
                    data: "buyer_id",
                },
                {
                    data: "property_id",
                },
                {
                    data: "seller_id",
                },
                {
                    data: "expected_reservation_date",
                },
                {
                    data: "expiration_date",
                },
                {
                    data: "payment_status",
                },
                {
                    data: "reservation_status",
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: "Actions",
                    responsivePriority: -1,
                },
            ],
            columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var additional = ''
                        if(row.reservation_status == '1'){
                            additional = '<a class="dropdown-item btn-link" href='+ base_url + '/sellers_reservations/payment/' + row.id +'><i class="la la-money"></i> Add Payment </a>'
                        }
                        return (
                            `
								<span class="dropdown">
									<a id="dropdownAction" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">`+ additional +`
										<a id="process_reservation_btn_${row.reservation_status}" class="dropdown-item" href="` +
                            base_url +
                            `transaction/form` +
                            `"><i class="la la-edit"></i> Process Reservation </a>
										<a id="cancel_reservation_btn_${row.reservation_status}" data-id="${row.id}" class="dropdown-item btn-link"><i class="la la-trash"></i> Cancel Reservation </a>
									</div>
								</span>
							`
                        );
                    },
                },
                {
                    targets: [0, 1, 3, 4, -1],
                    className: "dt-center",
                    // orderable: false,
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                // {
                //     targets: 7,
                //     render: function (data, type, full, meta) {
                //         var property_status = {
                //             1: {
                //                 title: "Available"
                //             },
                //             2: {
                //                 title: "Reserved"
                //             },
                //             3: {
                //                 title: "Sold"
                //             },
                //             4: {
                //                 title: "Hold"
                //             },
                //             5: {
                //                 title: "Not For Sale"
                //             },
                //         };

                //         if (typeof property_status[data] === `undefined`) {
                //             return ``;
                //         }

                //         return property_status[data].title;
                //     },
                // },
                {
                    targets: 8,
                    render: function (data, type, full, meta) {
                        const cancelBtn3 = document.querySelectorAll(`#cancel_reservation_btn_3`);
                        const processBtn3 = document.querySelectorAll(`#process_reservation_btn_3`);
                        const cancelBtn2 = document.querySelectorAll(`#cancel_reservation_btn_2`);
                        const processBtn2 = document.querySelectorAll(`#process_reservation_btn_2`);
                        const cancelBtn1 = document.querySelectorAll(`#cancel_reservation_btn_1`);


                        var reservation_status = {
                            1: {
                                title: "Reserved"
                            },
                            2: {
                                title: "Completed"
                            },
                            3: {
                                title: "Cancelled"
                            },
                            4: {
                                title: "Expired"
                            },
                        };

                        toastReservation(cancelBtn3, "cancelled");
                        toastReservation(processBtn3, "cancelled");
                        toastReservation(cancelBtn2, "completed");
                        toastReservation(processBtn2, "completed");
                        toastReservation(cancelBtn1, "reserved");

                        if (typeof reservation_status[data] === `undefined`) {
                            return ``;
                        }

                        return reservation_status[data].title;
                    },
                },
                // {
                //     targets: [7],
                //     visible: false
                // },
                {
                    targets: 5,
                    render: function (data, type, full, meta) {
                        const d = new Date(data);
                        const months = [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December",
                        ];
                        const month = months[d.getMonth()];
                        const day = d.getDate();
                        const year = d.getFullYear();

                        return `${month} ${day}, ${year}`;
                    },
                },
                {
                    targets: 6,
                    render: function (data, type, full, meta) {
                        const d = new Date(data);
                        const months = [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December",
                        ];
                        const month = months[d.getMonth()];
                        const day = d.getDate();
                        const year = d.getFullYear();

                        return `${month} ${day}, ${year}`;
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if (code === "Enter") {
                table
                    .DataTable()
                    .search($(this)
                        .val())
                    .draw();
                console.log($(this).val())
            }
        });

        $('._filter').on('keyup change clear apply.daterangepicker cancel.daterangepicker', function () {
            console.log($(this))
            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500) {
            let timer;
            return (...args) => {
                clearTimeout(timer);
                timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput() {
            $('#advance_search').submit()
        }
    };

    var confirmDelete = function () {
        $(document).on("click", ".remove_sellers_reservations", function () {
            var id = $(this).data("id");

            swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + "sellers_reservations/delete/" + id,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            id: id
                        },
                        success: function (res) {
                            if (res.status) {
                                $("#sellers_reservations_table")
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire("Deleted!", res.message, "success");
                            } else {
                                swal.fire("Oops!", res.message, "error");
                            }
                        },
                    });
                } else if (result.dismiss === "cancel") {
                    swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on("click", "#btn_upload_guide", function () {
            var table = $("#upload_guide_table");

            table.DataTable({
                order: [
                    [0, "asc"]
                ],
                pagingType: "full_numbers",
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + "sellers_reservations/get_table_schema",
                columns: [
                    // {data: 'button'},
                    {
                        data: "no"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "type"
                    },
                    {
                        data: "format"
                    },
                    {
                        data: "option"
                    },
                    {
                        data: "required"
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $("#sellers_reservations_table").DataTable();

        $("#_search").on("keyup", function () {
            dTable.search($(this).val()).draw();
        });

        $("#_export_select_all").on("click", function () {
            if (this.checked) {
                $("._export_column").each(function () {
                    this.checked = true;
                });
            } else {
                $("._export_column").each(function () {
                    this.checked = false;
                });
            }
        });

        $("._export_column").on("click", function () {
            if (this.checked == false) {
                $("#_export_select_all").prop("checked", false);
            }
        });

        $("#import_status").on("change", function (e) {
            var doc_type = $(this).val();

            if (doc_type != "") {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").removeAttr("disabled");
                    $("#_batch_upload button").removeClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            } else {
                KTApp.block("#kt_content", {
                    overlayColor: "#000000",
                    type: "v2",
                    state: "primary",
                    message: "Processing...",
                    css: {
                        padding: 0,
                        margin: 0,
                        width: "30%",
                        top: "40%",
                        left: "35%",
                        textAlign: "center",
                        color: "#000",
                        border: "3px solid #aaa",
                        backgroundColor: "#fff",
                        cursor: "wait",
                    },
                });

                setTimeout(function () {
                    $("#_batch_upload button").attr("disabled", "disabled");
                    $("#_batch_upload button").addClass("disabled");

                    KTApp.unblock("#kt_content");
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on("change", "#import_status", function () {
            $("#export_csv_status").val($(this).val());
        });
    };

    var _filterSellersReservations = function () {
        var dTable = $("#sellers_reservations_table").DataTable();

        function _colFilter(n) {
            dTable
                .column(n)
                .search($("#_column_" + n).val())
                .draw();
        }
    };

    var _selectProp = function () {
        var _table = $("#sellers_reservations_table").DataTable();
        var _buttons = _table.buttons([".bulkDelete"]);

        $("#select-all").on("click", function () {
            if ($(this).is(":checked")) {
                $(".delete_check").prop("checked", true);
            } else {
                $(".delete_check").prop("checked", false);
            }
        });

        $("#sellers_reservations_table tbody").on(
            "change",
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $("input#select-all").get(0);

                    if (el && el.checked && "indeterminate" in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on("change", 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $("#bulkDelete").click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + "sellers_reservations/bulkDelete/",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                deleteids_arr: deleteids_arr
                            },
                            success: function (res) {
                                if (res.status) {
                                    $("#sellers_reservations_table")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        "Deleted!",
                                        res.message,
                                        "success"
                                    );
                                } else {
                                    swal.fire("Oops!", res.message, "error");
                                }
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        );
                    }
                });
            }
        });
    };

    // Private functions
    var datepicker = function () {
        // minimum setup
        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
        $(".kt_datepicker").datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: "yyyy-mm-dd",
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            SellersReservationsTable();
            _add_ons();
            _filterSellersReservations();
            upload_guide();
            status();
            datepicker();
            _selectProp();
            filter();
        },
    };
})();

jQuery(document).ready(function () {
    SellersReservations.init();
});