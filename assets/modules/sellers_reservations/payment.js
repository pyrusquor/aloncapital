var KTFormControls = function () {
    // Private functions

    var valReservation = function () {
        validator = formEl.validate({
            // define validation rules
            rules: {
                project_id: {
                    required: true
                },
                company_id: {
                    required: true
                },
                date_paid: {
                    required: true
                },
                receipt_type: {
                    required: true
                },
                or_number: {
                    required: true
                },
                amount: {
                    required: true
                },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

                swal.fire(
                    "Oops",
                    "There are some errors in your submission. Please correct them.",
                    "error"
                );
            },

            submitHandler: function (form) {},
        });       
    }

    var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
            console.log(formEl);
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                        type: 'POSt',
                        dataType: 'JSON',
                        success: function(response) {
                            if (response.status) {
                                swal.fire({
                                    title: "Success!",
                                    text: response.message,
                                    type: "success"
                                }).then(function() {
                                    window.location.replace(base_url + "sellers_reservations");
                                });
    
                            } else {
                                swal.fire(
                                    'Oops!',
                                    response.message,
                                    'error'
                                )
                            }
                        }
                    });
			}
		});
    }

    return {
        // public functions
        init: function() {
            formEl = $('#form_reservation_payment');
            valReservation();
            initSubmit();
        }
    };
}();

var KTBootstrapDatepicker = (function () {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }


    var datepicker = function () {
        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
        $(".kt_datepicker").datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: "yyyy-mm-dd",
        });
    };

    return {
        init: function () {
            datepicker();
        },
    };
})();

$(document).ready(function () {
    KTBootstrapDatepicker.init();
    KTFormControls.init();
})

$(document).on('change','#receipt_type',function(){
    receiptType();
})

var receiptType = function () {

    receipt_type = parseInt($('#receipt_type').val());

    var ornumber = ' ';

    if (receipt_type == 1) {
        ornumber = 'CR-';
    } else if (receipt_type == 2) {
        ornumber = 'AR-';
    } else if (receipt_type == 3) {
        ornumber = 'PR-';
    } else if (receipt_type == 4) {
        ornumber = 'OR-';
    } else if (receipt_type == 5) {
        ornumber = ' ';
    }

    $('#OR_number').val(ornumber);
};