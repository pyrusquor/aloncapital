// Class definition
var interior = function() {
	var select2 = function() {
		$('#house_model_id').select2({
			placeholder: "Select a house model"
		});
    };
    
    var valInterior = function () {
        $( "#interior_form" ).validate({
            // define validation rules
            rules: {
                house_model_id: {
                    required: true 
                },
                name: {
                    required: true 
                },
                price: {
                    required: true
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }
	
	// Public functions
    return {
        init: function() {
            select2();
            valInterior();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    interior.init();
});