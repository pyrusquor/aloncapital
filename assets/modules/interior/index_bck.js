// Class definition
var interior = function() {
	var confirmDelete = function () {
		$(document).on('click', '.remove_interior', function () {
			var id = $(this).data('id');
			var btn = $(this);
			swal.fire({
				title: 'Delete House Model Interior',
				text: "Are you sure you want to delete this interior?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then(function (result) {
				if (result.value) {

					$.ajax({
						url: base_url + 'interior/delete/' + id,
						type: 'POST',
						dataType: "JSON",
						data: {
							id: id
						},
						success: function (res) {
							if (res.status) {
								console.log(btn);
								btn.closest('div.col-md-4').remove();
								swal.fire(
									'Deleted!',
									res.message,
									'success'
								);
							} else {
								swal.fire(
									'Oops!',
									res.message,
									'error'
								)
							}
						}
					});

				}
			});
		});
	};

	var filter = function() {
		$(document).on('click', '#apply_filter', function () {
			$( "#filter_form" ).submit();
		});
	}
	
	return {
		init: function () {
			confirmDelete();
			filter();
		},
	};
}();


// Initialization
jQuery(document).ready(function() {
    interior.init();
});