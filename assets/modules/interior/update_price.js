// Class definition
var interior = function() {
	var select2 = function() {
		$('#house_model_id').select2({
			placeholder: "Select a house model"
		});
    };
    
    var valInterior = function () {
        $( "#__form" ).validate({
            // define validation rules
            rules: {
                house_model_id: {
                    required: true
                },
                name: {
                    required: true 
                },
                price: {
                    required: true,
                    number: true
                }
            },
            invalidHandler: function ( event, validator ) {

                event.preventDefault();

                var alert   =   $('#form_msg');
                alert.closest('div.form-group').removeClass('kt-hide').show();
                KTUtil.scrollTop();
                
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };

                toastr.error("Please check your fields", "Something went wrong");
            },
            submitHandler: function ( _frm ) {

                _frm[0].submit();
            }
        });       
    }

    var _add_ons = function () {

        $('.kt_datepicker').datepicker({
        orientation: "bottom left",
        autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
        });
    }
	
	// Public functions
    return {
        init: function() {
            // select2();
            valInterior();
            _add_ons();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    interior.init();
});