// Class definition
var project = function() {

    var valProject = function () {
        $( "#ticketing_form" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true 
                },
                date: {
                    required: true 
                },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }

    var _add_ons = function () {

        $('.kt_datepicker').datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
			format: 'yyyy-mm-dd',

        });

        $(document).on('change','#ticketing_form #category,#ticketing_form #past_due_status, #ticketing_form #letter_request',function(){
           operateForm();
        });
    }
	
	// Public functions
    return {
        init: function() {
            valProject();
            _add_ons();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    project.init();
});



var operateForm = function() {
    category = $('#ticketing_form #category').val();
    past_due_status = $('#ticketing_form #past_due_status').val();
    letter_request_status = $('#ticketing_form #letter_request_status').val();

    if (category == "past_due_notices") {
        $('#past_due_form').show();
    } else if (category == "letter_of_request") {
        $('#letter_request_form').show();
    } else {
        $('#past_due_form').hide();
        $('#letter_request_form').hide();
    }
    
    $('.pds').hide();
    if (past_due_status) {
        $('#pd_'+past_due_notices).show();
    }

    $('.ars').hide();
    if (letter_request_status) {
        $('#ar_'+letter_request_status).show();
    }
    
}