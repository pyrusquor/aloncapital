// Class definition
var project = function() {

    var valProject = function () {
        $( "#ticketing_form" ).validate({
            // define validation rules
            rules: {
                remarks: {
                    required: true 
                },
                category: {
                    required: true 
                },
                // past_due: {
                //     required: true 
                // },
                // past_due_status: {
                //     required: true 
                // },
                // received_date: {
                //     required: true 
                // },
                // letter_request: {
                //     required: true 
                // },
                // letter_request_name: {
                //     required: true 
                // },
                // letter_request_status: {
                //     required: true 
                // },
                // date_requested: {
                //     required: true 
                // },
                // date_approved: {
                //     required: true 
                // },
                // date_requested: {
                //     required: true 
                // },
                // date_disapproved: {
                //     required: true 
                // },
                // date_cancelled: {
                //     required: true 
                // },
                // assignatory: {
                //     required: true 
                // },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }

    var _add_ons = function () {

        $('.kt_datepicker').datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
			format: 'yyyy-mm-dd',

        });

        $(document).on('change','#ticketing_form #category, #ticketing_form #past_due_status, #ticketing_form #letter_request_status',function(){
           operateForm();
        });
    }
	
	// Public functions
    return {
        init: function() {
            valProject();
            _add_ons();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    project.init();
});



var operateForm = function() {
    category = $('#ticketing_form #category').val();
    past_due_status = $('#ticketing_form #past_due_status').val();
    letter_request_status = $('#ticketing_form #letter_request_status').val();

    if (category == "past_due_notices") {
        $('#past_due_form').show();
        $('#letter_request_form').hide();

        $('#letter_request_form select').val(0);
        $('#letter_request_form input').val('');
    } else if (category == "letter_of_request") {
        $('#letter_request_form').show();
        $('#past_due_form').hide();

        $('#past_due_form select').val(0);
        $('#past_due_form input').val('');
    } else {
        $('#past_due_form').hide();
        $('#letter_request_form').hide();

        $('#letter_request_form select').val(0);
        $('#letter_request_form input').val('');

        $('#past_due_form select').val(0);
        $('#past_due_form input').val('');
    }
    
    $('.pds').hide();
    if (past_due_status) {
        // $('.pds input').val('');
        $('#pd_'+past_due_status).show();
    }

    $('.ars').hide();

    if (letter_request_status) {
        // $('.ars input').val('');
        $('#ar_'+letter_request_status).show();
    }
    
}