// Class definition
var project = function() {
	var confirmDelete = function () {
		$(document).on('click', '.remove_ticketing', function () {
			var id = $(this).data('id');
			var btn = $(this);
			swal.fire({
				title: 'Delete Ticketing',
				text: "Are you sure you want to delete this transaction?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then(function (result) {
				if (result.value) {

					$.ajax({
						url: base_url + 'ticketing/delete/' + id,
						type: 'POST',
						dataType: "JSON",
						data: {
							id: id
						},
						success: function (res) {
							if (res.status) {
								console.log(btn);
								btn.closest('div.col-md-12').remove();
								swal.fire(
									'Deleted!',
									res.message,
									'success'
								);
							} else {
								swal.fire(
									'Oops!',
									res.message,
									'error'
								)
							}
						}
					});

				}
			});
		});
	};

	var filter = function() {
		$(document).on('click', '#apply_filter', function () {
			$( "#filter_form" ).submit();
		});
	}
	
	return {
		init: function () {
			confirmDelete();
			filter();
		},
	};
}();


// Initialization
jQuery(document).ready(function() {
    project.init();
});