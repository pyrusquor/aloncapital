function getQueryParams(query = window.location.search) {
    return query.replace(/^\?/, '').split('&').reduce((json, item) => {
        if (item) {
            item = item.split('=').map((value) => decodeURIComponent(value))
            json[item[0]] = item[1]
        }
        return json
    }, {})
}

function filter() {
    var values = $('#advanceSearch').serialize();
    var page_num = page_num ? page_num : 0;
    var filter_data = values ? getQueryParams(values) : '';
    filter_data.page = page_num;

    return values;
}

jQuery(document).ready(function () {
    $("#transfer_warehouse_id").on('change', function (e) {
        view_app.setValue("warehouse_transfer", "destination_id", $(this).val());
    });


});

const __defaults = {
    warehouse_transfer: {
        id: null,
        destination_id: null,
        warehouse_id: null,
        child_warehouse_id: null,
        source_id: null,
        quantity: 0,
        item_id: null
    },
}

const __checks = {
    warehouse_transfer: {
        id: ["ID", null],
        destination_id: ["Destination Warehouse", null],
        quantity: ["Quantity", 0],
        item_id: ["Item", null]
    },
}

var view_app = new Vue({
    el: '#warehouse_inventory_content',
    data: {
        warehouse_items: {
            data: [],
        },
        store_groups: ["warehouse_transfer"],
        transfer_state: {
            warehouse_type: 1,
            material_items: [],
            form: [],
            data: []
        },
        data_store: {
            modal: null,
            warehouse_transfer: {
                form: {},
                is_valid: false,
                bad_fields: [],
                pk: "warehouse_id",
                check_url: base_url + "warehouse_inventory/get_item",
                transfer_url: base_url + "warehouse_inventory/transfer"
            }
        },
        sources: {
            warehouses: [],
            parent_warehouses: [],
            child_warehouses: []
        }
    },
    watch: {},
    methods: {
        // region generic
        setValue(store_group, field, value) {
            this.data_store[store_group]["form"][field] = value;
            this.$forceUpdate();
        },

        setDefault() {
            let defaults = __defaults.warehouse_transfer;
            for (f in defaults) {
                this.data_store.warehouse_transfer.form[f] = defaults[f];
            }
            this.data_store.warehouse_transfer.is_valid = false;
            this.transfer_state.form = [];
            this.transfer_state.data = [];
            this.data_store.warehouse_transfer.bad_fields = [];
            this.$forceUpdate();
        },

        checkForm(store_group) {
            let checks = __checks[store_group];
            this.data_store[store_group]["is_valid"] = true;
            for (f in checks) {
                if (this.data_store[store_group]["form"][f] === checks[f][1]) {
                    this.data_store[store_group]["is_valid"] = false;
                    this.data_store[store_group]["bad_fields"].push(checks[f][0]);
                }
            }
            return this.data_store[store_group]["is_valid"];
        },

        checkItem(store_group, item_id) {
            let pk = this.data_store[store_group]["pk"];
            let url = this.data_store[store_group]["url"] + "?item_id=" + item_id + "id=" + this.data_store[store_group]["form"][pk];
            axios.get(url)
                .then(result => function () {
                    console.log(result);
                });
        },

        submitRequest(store_group) {

        },

        fetchDependents(table, field, store_group)
        {
            let value = this.data_store[store_group]["form"][field];
            this.fetchObjects(table, field, value, null);
        },

        fetchObjects(table, field, value, filter_out) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            if(field && value){
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + field + "&value=" + value;
            }
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        if (filter_out) {
                            let data = [];
                            for (let i in response.data) {
                                if (response.data[i].id != filter_out) {
                                    data.push(response.data[i]);
                                }
                            }
                            this.sources[table] = data;
                        } else {
                            this.sources[table] = response.data;
                        }

                    } else {
                        this.sources[table] = [];
                    }
                    this.$forceUpdate();
                })
        },
        // endregion

        // region warehouse items
        filterWarehouses() {
            let type = this.transfer_state.warehouse_type;
            let parent_id = this.data_store.warehouse_transfer.form.warehouse_id;
            let filtered = [];
            for(let i = 0; i < this.sources.warehouses.length; i++){
                if(this.sources.warehouses[i].warehouse_type == type && this.sources.warehouses[i].parent_id == parent_id){
                    filtered.push(this.sources.warehouses[i]);
                }
            }
            this.sources.child_warehouses = filtered;
            this.$forceUpdate();
        },
        filterParentWarehouses() {
            let filtered = [];
            for(let i = 0; i < this.sources.warehouses.length; i++){
                if(this.sources.warehouses[i].warehouse_type == 1){
                    filtered.push(this.sources.warehouses[i]);
                }
            }
            this.sources.parent_warehouses = filtered;
            this.$forceUpdate();
        },
        warehouseSelectionDisplay(){
            this.data_store.warehouse_transfer.form.warehouse_id = null;
            this.data_store.warehouse_transfer.form.child_warehouse_id = null;
        },
        loadWarehouseItems(id) {
            $('#filterModal').modal('hide');
            this.warehouse_items.loading = true;
            var url = base_url + "warehouse_inventory/showWarehouseInventories?filter=true&with_relations=yes";
            if (id) {
                url += "&warehouse_id=" + window.object_id;
            } else {
                url += "&" + filter();
            }
            axios.get(url)
                .then(response => {
                    this.warehouse_items.loading = false;
                    this.warehouse_items.data = response.data.data;
                    this.warehouse_items.total = response.data.iTotalRecords;
                    this.warehouse_items.display = response.data.iTotalDisplayRecords;
                });
        },
        editWarehouseItem(id) {
            window.location = base_url + "warehouse_inventory/update/" + id;
        },

        // endregion

        // region material inventory
        loadMaterialInventory(){
            this.transfer_state.form = [];
            let url = base_url + "material_receiving/get_items_by_item_id/" + this.data_store.warehouse_transfer.form.item_id + "?warehouse_id=" + this.data_store.warehouse_transfer.form.source_id + "&empty=no";

            axios.get(url)
                .then(response => {
                    if(response.data.length > 0){
                        this.transfer_state.material_items = response.data;
                        for(let i = 0; i < this.transfer_state.material_items.length; i++){
                            let mi = this.transfer_state.material_items[i];
                            let form_item = {
                                material_receiving_item_id: mi.id,
                                quantity: 0,
                            }
                            this.transfer_state.form.push(form_item);
                        }
                    }else{
                        this.transfer_state.material_items = [];
                        this.transfer_state.form = [];
                        this.transfer_state.data = [];
                    }
                });
        },
        // endregion

        // region transfer
        prepTransfer(id, item_id) {
            this.setValue('warehouse_transfer', "item_id", item_id);
            this.setValue('warehouse_transfer', "id", id);
            this.setValue('warehouse_transfer', "source_id", window.object_id);

            this.filterParentWarehouses();

            this.data_store.modal = $("#modalTransfer");

            this.data_store.modal.modal("show");

            this.loadMaterialInventory();
        },
        setDestination() {
            if(this.transfer_state.warehouse_type == 1){
                this.data_store.warehouse_transfer.form.destination_id = this.data_store.warehouse_transfer.form.warehouse_id;
            }else{
                this.data_store.warehouse_transfer.form.destination_id = this.data_store.warehouse_transfer.form.child_warehouse_id;
            }
            console.log(this.transfer_state.warehouse_type, this.data_store.warehouse_transfer.form.destination_id);
        },
        preTransferChecks() {
            if(this.transfer_state.data.length <= 0){
                return {
                    status: false,
                    message: "No items"
                }
            }

            for(let i = 0; i < this.transfer_state.data.length; i++){
                if(this.transfer_state.data[i].quantity <= 0){
                    return {
                        status: false,
                        message: "Invalid quantity"
                    }
                }
            }

            return {
                status: true,
                message: ""
            }
        },
        setTransferItems(){
            for(let i = 0; i < this.transfer_state.form.length; i++){
                if(this.transfer_state.form[i].quantity > 0){
                    this.transfer_state.data.push(this.transfer_state.form[i]);
                }
            }
        },
        transfer() {
            this.setDestination();
            this.setTransferItems();
            $("#modalTransfer").modal('hide');
            let pre_transfer_checks = this.preTransferChecks();
            if(! pre_transfer_checks.status ){
                swal.fire({
                    title: "Oooops!",
                    text: pre_transfer_checks.message,
                    type: "error"
                });
                return false;
            }
            let url = base_url + "warehouse_inventory/transfer";
            let data = {
                source_id: this.data_store.warehouse_transfer.form.source_id,
                destination_id: this.data_store.warehouse_transfer.form.destination_id,
                items: this.transfer_state.data
            };

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function(response){
                    if (response.status) {
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        });
                        window.location = base_url + "warehouse/view/" + window.object_id;
                    } else {
                        swal.fire({
                            title: "Oooops!",
                            text: response.message,
                            type: "error"
                        });
                    }
                }
            })
        },

        __transfer(store_group) {
            let message = "";
            let data = this.data_store[store_group]["form"];
            this.data_store.modal.modal("hide");
            if (this.checkForm(store_group)) {
                let check_url = this.data_store[store_group]["check_url"] + "?item_id=" + this.data_store[store_group]["form"]["item_id"] + "&warehouse_id=" + window.object_id;
                let process_url = "";
                axios.get(check_url)
                    .then(response => {
                        let transfer_url = this.data_store[store_group]["transfer_url"];

                        $.ajax({
                            url: transfer_url,
                            type: 'POST',
                            dataType: 'JSON',
                            data: data,
                            success: function (response) {
                                if (response.status) {
                                    swal.fire({
                                        title: "Success!",
                                        text: response.message,
                                        type: "success",
                                    });
                                    this.setDefault(store_group);
                                    this.loadWarehouseItems(window.object_id);
                                    this.$forceUpdate();
                                } else {
                                    swal.fire({
                                        title: "Oooops!",
                                        text: response.message,
                                        type: "error"
                                    });
                                }
                            }
                        })

                    });
            } else {
                message = 'Missing fields: ' + this.data_store[store_group]["bad_fields"].join(', ');
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });

                return false;
            }
        }
        // endregion
    },
    mounted() {
        this.fetchObjects("warehouses", null, null, null);
        this.setDefault();
        this.loadWarehouseItems(window.object_id);
    }
});