var InventoryLogs = (function () {
    var InventoryLogsTable = function () {
        var table = $("#inventory_logs_table");
        var warehouse_id = window.warehouse_id;
        var url = base_url + "warehouse_inventory_log/showWarehouseInventoryLogs?warehouse_id=" + warehouse_id;
        table.DataTable({
            order: [0, "desc"],
            pagingType: "full_numbers",
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: "post",
            deferRender: true,
            ajax: url,
            dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            language: {
                lengthMenu: "Show _MENU_",
                infoFiltered: "(filtered from _MAX_ total records)"
            },
            columns: [
                {
                    data: "created_at"
                },
                {
                    data: "actor"
                },
                {
                    data: "transaction_label"
                },
                {
                    data: "item.name"
                },
                {
                    data: "unit_of_measurement.name"
                },
                {
                    data: null
                },
                {
                    data: null
                },
                {
                    data: null
                },
                {
                    data: null
                },
                {
                    data: null
                }
            ],
            columnDefs: [
                {
                    targets: 8,
                    render: function(data, type, row, meta){
                        if(row.source){
                            return row.source.name
                        }
                        return ""
                    }
                },
                {
                    targets: 9,
                    render: function(data, type, row, meta){
                        if(row.destination){
                            return row.destination.name
                        }
                        return ""
                    }
                },
                {
                    targets: 5,
                    render: function(data, type, row, meta){
                        return  row.available_quantity_in + "(" + row.actual_quantity_in + ")";
                    }
                },
                {
                    targets: 6,
                    render: function(data, type, row, meta){
                        return  row.available_quantity_out + "(" + row.actual_quantity_out + ")";
                    }
                },
                {
                    targets: 7,
                    render: function(data, type, row, meta){
                        return  row.current_available_quantity + "(" + row.current_actual_quantity + ")";
                    }
                }
            ],
            drawCallback: function (settings) {
                $("#total").text(settings.fnRecordsTotal() + " TOTAL");
            }
        })
    };

    return {
        init: function () {
            InventoryLogsTable();
        }
    }
})();

jQuery(document).ready(function () {
    InventoryLogs.init();
});