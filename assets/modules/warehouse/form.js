$(document).ready(function () {
    $("#sbu_id").on("change", function (e) {
        app.changeInfo('sbu_id', $(this).val());
    });

    $("#parent_id").on("change", function (e) {
        console.log($(this).val());
        app.changeInfo('parent_id', $(this).val());
    });

});

let warehouse_type = window.warehouse_type;

if (warehouse_type == 1) {
    let app_container = "#warehouse_app";
    const __info_form_defaults = {
        name: null,
        address: null,
        warehouse_code: null,
        is_active: 1,
        sbu_id: null,
        telephone_number: null,
        fax_number: null,
        mobile_number: null,
        contact_person: null,
        email: null,
        warehouse_type: 1,
        file_path: null,
    }

    var app = new Vue({
        el: app_container,
        data: {
            object_request_id: null,
            object_status: 1,

            info: {
                form: {},
                required: {
                    name: "Name",
                    address: "Address",
                    warehouse_code: "Warehouse Code",
                    sbu_id: "SBU",
                    telephone_number: "Telephone Number",
                    fax_number: "Fax Number",
                    mobile_number: "Mobile Number",
                    contact_person: "Contact Person",
                    email: "Email",
                },
                check: {
                    is_valid: false,
                    missing_fields: []
                },
                is_active: false,
            },
            items: {
                form: [],
                data: [],
                selected: null,
                selected_idx: null,
                required: {},
                check: {
                    is_valid: false,
                },
                deleted_items: [],
                loading: false
            },
            sources: {}
        },
        watch: {},
        methods: {

            initObjectRequest() {
                this.object_request_id = window.object_request_id;
                this.object_status = window.object_status;

                this.fetchObjectRequest();
            },

            resetItemForm() {
                this.items.selected = null;
                this.items.selected_idx = null;
                this.items.loaded = null;
                this.items.form = {};
            },

            checkItemsForm() {
                this.items.check.is_valid = true;
                if (this.items.data.length <= 0) {
                    this.items.check.is_valid = false;
                }
            },

            prepItemsFormData() {
                return this.items.data;
            },

            changeInfo(key, val) {
                this.info.form[key] = val;
            },

            prepInfoFormData() {
                return this.info.form;
            },

            checkInfoForm() {
                this.info.check.missing_fields = [];
                this.info.check.is_valid = true;
                for (var key in this.info.required) {
                    if (this.info.form[key] === null) {
                        this.info.check.missing_fields.push(this.info.required[key]);
                        this.info.check.is_valid = false;
                    }
                }
            },

            submitRequest() {
                this.checkInfoForm();
                let message = '';
                if (!this.info.check.is_valid) {
                    ``
                    message = 'Missing fields: ' + this.info.check.missing_fields.join(', ');
                    swal.fire({
                        title: "Oooops!",
                        text: message,
                        type: "error",
                    });
                    return false;
                }

                let url = base_url + "warehouse/process_create";
                if (this.object_request_id) {
                    url = base_url + "warehouse/process_update/" + this.object_request_id;
                }

                let data = {
                    request: this.prepInfoFormData(),
                }

                let formData = new FormData();
                formData.append('file', this.info.form.file_path)
                
                Object.keys(data.request).forEach(function(key) {

                    formData.append(key, data.request[key])
                })

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    enctype: 'multipart/form-data',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success",
                            }).then(function () {
                                window.location.replace(
                                    base_url + "warehouse"
                                );
                            });
                        }
                    }
                })
            },

            fetchObjects(table, lookup_field) {
                let url = base_url + "generic_api/fetch_all?table=" + table;
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.sources[table] = response.data;
                        } else {
                            this.sources[table] = [];
                        }
                    })
            },

            fetchObjectRequest() {
                if (this.object_request_id) {
                    let url = base_url + "warehouse/search?id=" + this.object_request_id + "&with_relations=no";
                    axios.get(url)
                        .then(response => {
                            if (response.data) {
                                this.parseObjectRequest(response.data);
                            }
                        });
                } else {
                    this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
                }

            },
            fetchObjectRequestChildren() {
                let url = base_url + "warehouse_item/get_all_by_parent/" + this.object_request_id;
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectRequestItems(response.data);
                        }
                    });
            },
            parseObjectRequest(data) {
                if (data.length > 0) {
                    this.info.form = data[0];
                    // this.fetchObjectRequestChildren();
                }
            },

            parseObjectRequestItems(data) {
                for (row in data) {
                    this.addItemToCart(false, data[row]);
                }
            },

            addItemToCart(isNew, data) {
                let item = {};

                if (isNew) {
                    for (key in data) {
                        item[key] = data[key];
                    }
                    // do something with id
                    // delete item['id'];
                    delete item['created_by'];
                    delete item['created_at'];
                    delete item['updated_at'];
                    delete item['updated_by'];
                    delete item['deleted_by'];
                    delete item['deleted_at'];

                } else {
                    for (key in data) {
                        item[key] = data[key];
                    }
                }

                if (item.id) {
                    let idx = this.searchItemInStore(item.id);
                    if (idx !== null) {
                        this.items.data[idx] = item;
                    } else {
                        this.items.data.push(item);
                    }
                } else {
                    this.items.data.push(item);
                }
                this.$forceUpdate();

                this.resetItemForm();
            },

            searchItemInStore(id) {
                for (let i = 0; i < this.items.data.length; i++) {
                    if (id == this.items.data[i].id) {
                        return i;
                    }
                }
                return null;
            },

            updateFile(event) {

                this.info.form.file_path = event.target.files[0];
             }

        },
        mounted() {

            this.initObjectRequest();

            this.is_mounted = true;
            this.object_request_id = window.object_request_id;


        }
    });
}

if (warehouse_type == 2) {
    let app_container = "#sub_warehouse_app";
    const __info_form_defaults = {
        name: null,
        address: null,
        warehouse_code: null,
        is_active: 1,
        telephone_number: null,
        fax_number: null,
        mobile_number: null,
        contact_person: null,
        email: null,
        parent_id: null,
        warehouse_type: 2,
        file_path: null,
    }

    var app = new Vue({
        el: app_container,
        data: {
            object_request_id: null,
            object_status: 1,

            info: {
                form: {},
                required: {
                    name: "Name",
                    address: "Address",
                    warehouse_code: "Warehouse Code",
                    parent_id: "Parent Warehouse",
                    telephone_number: "Telephone Number",
                    fax_number: "Fax Number",
                    mobile_number: "Mobile Number",
                    contact_person: "Contact Person",
                    email: "Email",
                },
                check: {
                    is_valid: false,
                    missing_fields: []
                },
                is_active: false,
            },
            items: {
                form: [],
                data: [],
                selected: null,
                selected_idx: null,
                required: {},
                check: {
                    is_valid: false,
                },
                deleted_items: [],
                loading: false
            },
            sources: {}
        },
        watch: {},
        methods: {

            initObjectRequest() {
                this.object_request_id = window.object_request_id;
                this.object_status = window.object_status;

                this.fetchObjectRequest();
            },

            changeInfo(key, val) {
                this.info.form[key] = val;
            },

            prepInfoFormData() {
                return this.info.form;
            },

            checkInfoForm() {
                this.info.check.missing_fields = [];
                this.info.check.is_valid = true;
                for (var key in this.info.required) {
                    if (this.info.form[key] === null) {
                        this.info.check.missing_fields.push(this.info.required[key]);
                        this.info.check.is_valid = false;
                    }
                }
            },

            submitRequest() {
                this.checkInfoForm();
                let message = '';
                if (!this.info.check.is_valid) {
                    ``
                    message = 'Missing fields: ' + this.info.check.missing_fields.join(', ');
                    swal.fire({
                        title: "Oooops!",
                        text: message,
                        type: "error",
                    });
                    return false;
                }

                let url = base_url + "warehouse/process_create";
                if (this.object_request_id) {
                    url = base_url + "warehouse/process_update/" + this.object_request_id;
                }

                let data = {
                    request: this.prepInfoFormData(),
                }

                let formData = new FormData();
                formData.append('file', this.info.form.file_path)
                
                Object.keys(data.request).forEach(function(key) {

                    formData.append(key, data.request[key])
                })

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    data: formData,
                    enctype: 'multipart/form-data',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success",
                            }).then(function () {
                                window.location.replace(
                                    base_url + "warehouse"
                                );
                            });
                        }
                    }
                })

            },

            fetchObjects(table, lookup_field) {
                let url = base_url + "generic_api/fetch_all?table=" + table;
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.sources[table] = response.data;
                        } else {
                            this.sources[table] = [];
                        }
                    })
            },

            fetchObjectRequest() {
                if (this.object_request_id) {
                    let url = base_url + "warehouse/search?id=" + this.object_request_id + "&with_relations=no";
                    axios.get(url)
                        .then(response => {
                            if (response.data) {
                                this.parseObjectRequest(response.data);
                            }
                        });
                } else {
                    this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
                }

            },
            parseObjectRequest(data) {
                if (data.length > 0) {
                    this.info.form = data[0];
                }
            },
            updateFile(event) {

                this.info.form.file_path = event.target.files[0];
            }
        },
        mounted() {

            this.initObjectRequest();

            this.is_mounted = true;
            this.object_request_id = window.object_request_id;


        }
    });
}

if (warehouse_type == 3) {
    let app_container = "#virtual_warehouse_app";
    const __info_form_defaults = {
        name: null,
        warehouse_code: null,
        is_active: 1,
        parent_id: null,
        warehouse_type: 3,
        file_path: null,
    }

    var app = new Vue({
        el: app_container,
        data: {
            object_request_id: null,
            object_status: 1,

            info: {
                form: {},
                required: {
                    name: "Name",
                    warehouse_code: "Warehouse Code",
                    parent_id: "Parent Warehouse",
                },
                check: {
                    is_valid: false,
                    missing_fields: []
                },
                is_active: false,
            },
            items: {
                form: [],
                data: [],
                selected: null,
                selected_idx: null,
                required: {},
                check: {
                    is_valid: false,
                },
                deleted_items: [],
                loading: false
            },
            sources: {}
        },
        watch: {},
        methods: {

            initObjectRequest() {
                this.object_request_id = window.object_request_id;
                this.object_status = window.object_status;

                this.fetchObjectRequest();
            },

            changeInfo(key, val) {
                this.info.form[key] = val;
            },

            prepInfoFormData() {
                return this.info.form;
            },

            checkInfoForm() {
                this.info.check.missing_fields = [];
                this.info.check.is_valid = true;
                for (var key in this.info.required) {
                    if (this.info.form[key] === null) {
                        this.info.check.missing_fields.push(this.info.required[key]);
                        this.info.check.is_valid = false;
                    }
                }
            },

            submitRequest() {
                this.checkInfoForm();
                let message = '';
                if (!this.info.check.is_valid) {
                    ``
                    message = 'Missing fields: ' + this.info.check.missing_fields.join(', ');
                    swal.fire({
                        title: "Oooops!",
                        text: message,
                        type: "error",
                    });
                    return false;
                }

                let url = base_url + "warehouse/process_create";
                if (this.object_request_id) {
                    url = base_url + "warehouse/process_update/" + this.object_request_id;
                }

                let data = {
                    request: this.prepInfoFormData(),
                }

                let formData = new FormData();
                formData.append('file', this.info.form.file_path)
                
                Object.keys(data.request).forEach(function(key) {

                    formData.append(key, data.request[key])
                })

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    data: formData,
                    enctype: 'multipart/form-data',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success",
                            }).then(function () {
                                window.location.replace(
                                    base_url + "warehouse"
                                );
                            });
                        }
                    }
                })

            },

            fetchObjects(table, lookup_field) {
                let url = base_url + "generic_api/fetch_all?table=" + table;
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.sources[table] = response.data;
                        } else {
                            this.sources[table] = [];
                        }
                    })
            },

            fetchObjectRequest() {
                if (this.object_request_id) {
                    let url = base_url + "warehouse/search?id=" + this.object_request_id + "&with_relations=no";
                    axios.get(url)
                        .then(response => {
                            if (response.data) {
                                this.parseObjectRequest(response.data);
                            }
                        });
                } else {
                    this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
                }

            },
            parseObjectRequest(data) {
                if (data.length > 0) {
                    this.info.form = data[0];
                }
            },
            updateFile(event) {

                this.info.form.file_path = event.target.files[0];
            }
        },
        mounted() {

            this.initObjectRequest();

            this.is_mounted = true;
            this.object_request_id = window.object_request_id;


        }
    });
}