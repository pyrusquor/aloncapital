// Class definition
var KTFormControls = function () {
    // Private functions

    var valMenu = function () {
        $( "#menu_form" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true
                },
                /* ==================== begin: Add model fields ==================== */
                description: {
                    required: true
                },
                parent_id: {
                    required: false
                },
                link: {
                    required: false
                },
                icon: {
                    required: false
                },
                ctr: {
                    required: false
                },
                /* ==================== end: Add model fields ==================== */
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {

                toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    };

    return {
        // public functions
        init: function() {
            valMenu();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormControls.init();
});