// Class definition
var KTFormControls = function () {
    // Private functions

    var valLoan = function () {
        $( "#loan_form" ).validate({
            // define validation rules
            rules: {
                /* ==================== begin: Add model fields ==================== */
                loan_id: {
                    required: true
                },

                transaction_fee_amount: {
                    required: true
                },

                net_disbursement_amount: {
                    required: true
                },

                transaction_fee: {
                    required: true
                },

                disbursement_type: {
                    required: true
                },
                /* ==================== end: Add model fields ==================== */
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {

                toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    };

    return {
        // public functions
        init: function() {
            valLoan();
        }
    };
}();


var KTBootstrapDatepicker = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd'
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });

        $('.datePicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    };

    return {
        // public functions
        init: function() {
            datepicker();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormControls.init();
    KTBootstrapDatepicker.init();
});


$(document).on(
    'blur',
    '#loan_form #transaction_fee',
    function () {
        loan_amount = parseFloat($('#loan_form #loan_amount').val());
        transaction_fee = parseFloat($('#loan_form #transaction_fee').val());

        transaction_fee_amount = loan_amount * (transaction_fee / 100);
        net_disbursement_amount = loan_amount - transaction_fee_amount;

        $('#loan_form #transaction_fee_amount').val(transaction_fee_amount);
        $('#loan_form #net_disbursement_amount').val(net_disbursement_amount);
    }
);