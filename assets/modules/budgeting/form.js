var KTFormControls = function () {
    // Private functions

    var valCashier = function () {
        validator = formEl.validate({
            // define validation rules
            rules: {
                ledger_id: {
                    required: true
                },
                amount: {
                    required: true
                },
                month: {
                    required: false
                },
                year: {
                    required: false
                }, 
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

                swal.fire(
                    "Oops",
                    "There are some errors in your submission. Please correct them.",
                    "error"
                );
            },

            submitHandler: function (form) {},
        });       
    }

    var initSubmit = function() {
		var btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
                if($("#property_id").val() || $("#land_inventory_id").val() || $("#department_id").val() || $('#project_id').val()){
                    formEl.ajaxSubmit({
                        type: 'POSt',
                        dataType: 'JSON',
                        success: function(response) {
                            if (response.status) {
                                swal.fire({
                                    title: "Success!",
                                    text: response.message,
                                    type: "success"
                                }).then(function() {
                                    window.location.replace(base_url + "budgeting");
                                });
    
                            } else {
                                swal.fire(
                                    'Oops!',
                                    response.message,
                                    'error'
                                )
                            }
                        }
                    });
                }else{
                    swal.fire(
                        'Oops!',
                        'No selected recipient for this budget',
                        'error'
                    )
                }

			}
		});
    }
    var selectDisabler = function(){

        $("#project_id, #property_id, #land_inventory_id, #department_id").on('change', function (){
            if(this.id=='project_id'){
                if($(this).val()!==''){
                    $("#property_id").prop('disabled', false);
                    $("#land_inventory_id").prop('disabled', true);
                    $("#department_id").prop('disabled', true);
                    $("#land_inventory_id option:selected").removeAttr("selected");
                    $("#department_id option:selected").removeAttr("selected");
                }else{
                    $("#property_id").prop('disabled', true);
                    $("#land_inventory_id").prop('disabled', false);
                    $("#department_id").prop('disabled', false);
                }
            }
            if(this.id=='land_inventory_id'){
                if($(this).val()!==''){
                    $("#project_id").prop('disabled', true);
                    $("#department_id").prop('disabled', true);
                    $("#department_id option:selected").removeAttr("selected");
                    $("#project_id option:selected").removeAttr("selected");

                }else{
                    $("#project_id").prop('disabled', false);
                    $("#department_id").prop('disabled', false);
                }
            }
            if(this.id=='department_id'){
                if($(this).val()!==''){
                    $("#project_id").prop('disabled', true);
                    $("#land_inventory_id").prop('disabled', true);
                    $("#project_id option:selected").removeAttr("selected");
                    $("#land_inventory_id option:selected").removeAttr("selected");

                }else{
                    $("#project_id").prop('disabled', false);
                    $("#land_inventory_id").prop('disabled', false);
                }
            }
        })
        
        $("#project_id, #property_id, #land_inventory_id, #department_id").each(function (){
            if ($(this).val()!==''){
                $(this).prop('disabled', false);
            }
        })
    }

    return {
        // public functions
        init: function() {
            // formEl = $('#form_cashier');
            // valCashier();
            // initSubmit();
            selectDisabler();
            formEl = $('#form_budgeting');
            valCashier();
            initSubmit();
        }
    };
}();

var KTBootstrapDatepicker = (function () {
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }


    var datepicker = function () {
        $(".compDatepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
        $(".kt_datepicker").datepicker({
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
            format: "yyyy-mm-dd",
        });
    };

    return {
        init: function () {
            datepicker();
        },
    };
})();

$(document).ready(function () {
	KTFormControls.init();
    KTBootstrapDatepicker.init();
})
$('#ledger_id').select2({
    allowClear: true,
    placeholder: ' Select...',
})