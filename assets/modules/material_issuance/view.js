var app = new Vue({
    el: '#material_issuance_app',
    data: {
        object_id: null,
        notes: {
            loading: false,
            data: [],
            form: {
                content: ""
            },
            selected: {}
        }
    },
    watch: {},
    methods: {
        fetchNotes() {
            this.notes.loading = true;
            let url = base_url + "note/get_by_parent?parent_type=material_issuances&parent_type_id=" + this.object_id;
            axios.get(url)
                .then(response => {
                    if(response.data){
                        this.notes.data = response.data;
                    }
                    this.notes.loading = false;
                });
        },
        addNote() {
            $("#note_modal").modal('show');
        },
        saveNote() {
            this.notes.loading = true;
            $("#note_modal").modal('hide');
            let data = {
                request: this.notes.form
            };
            data.request.object_type = "material_issuances";
            data.request.object_type_id = this.object_id;

            let url = base_url + "note/process_create";
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.status) {
                        app.fetchNotes();
                        swal.fire({
                            title: "Success!",
                            text: "Note saved!",
                            type: "success",
                        });
                    }
                }
            })
        },
        cancelItem(id, quantity) {

            console.log(id)

            swal.fire({
                title: 'Enter quantity to cancel',
                input: 'number',
                inputAttributes: {
                  autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (input) => {

                    if (input > quantity) {

                        swal.fire({
                            title: "Error!",
                            text: "Quantity exceeded",
                            type: "error",
                        })

                        return false
                    }

                    let url = base_url + "material_issuance/cancel_item/" + id;
                    
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            quantity: input
                        },
                        success: function (response) {
                            if (response.status) {
                                
                                swal.fire({
                                    title: response.title,
                                    text: response.message,
                                    type: response.type,
                                }).then(() => {

                                    location.reload()
                                })
                            }
                        }
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
              }).then((result) => {
                if (result.isConfirmed) {
                  swal.fire({
                    title: "Success",
                    text: "Item(s) cancelled",
                    type: "success",
                  })
                }
              })
        }
    },
    mounted() {
        this.object_id = window.object_id;
        this.fetchNotes();
    }
});