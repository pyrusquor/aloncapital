$(document).ready(function () {
    var datepicker = function () {
        // minimum setup
        $('#request_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function (e) {
            app.changeRequestDate($("#request_date").val());
            app.checkInfoForm();
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true
        });

        $("#filter_mrr").on('change', function (e) {
            app.changeFilter($(this).data('key'), $(this).val());
        });
        $("#filter_project").on('change', function (e) {
            app.changeFilter($(this).data('key'), $(this).val());
        });
        $("#filter_amenity").on('change', function (e) {
            app.changeFilter($(this).data('key'), $(this).val());
        });
        $("#filter_department").on('change', function (e) {
            app.changeFilter($(this).data('key'), $(this).val());
        });
        $("#filter_vehicle_equipment").on('change', function (e) {
            app.changeFilter($(this).data('key'), $(this).val());
        });

        $("#warehouse_id").on('change', function (e) {
            app.changeInfo('warehouse_id', $(this).val());
        });

        $("#issued_to_others").on('change', function (e) {
            app.info.form.issued_to_others = $(this).val()
        });
    };

    datepicker();
});


const __info_form_defaults = {
    warehouse_id: null,
    material_request_id: null,
    issued_to: null,
    remarks: null,
    issued_to_others: null,
}

var app = new Vue({
    el: '#material_issuance_app',
    data: {
        object_request_id: null,
        object_status: 1,

        info: {
            form: {},
            required: {

                material_request_id: "Material Request",

            },
            check: {
                is_valid: false,
                missing_fields: []
            },
            is_active: false,
        },
        items: {
            form: [],
            selected: null,
            selected_idx: null,
            required: {},
            total: 0.0,
            quantity: 0.0,
            check: {
                is_valid: false,
            },
            deleted_items: [],
            loading: false
        },
        material_requests: {
            show_filter: true,
            loading: false,
            selected: null,
            receiving_item: {},
            filter: {
                request_status: null,
                project_id: null,
                sub_project_id: null,
                property_id: null,
                amenity_id: null,
                sub_amenity_id: null,
                department_id: null,
                vehicle_equipment_id: null,
                reference: null,
                with_relations: "yes"
            },
            results: [],
            items: [],
            items_ref: [],
            show_items_ref: false,
            items_loading: false,
            items_meta: {
                per_page: null,
                max_pages: null,
                num_entries: null,
                current_page: null,
                current_page_display: null,
                show_detail: null,
                start_count: null,
                end_count: null,
                num_pages: null
            },
            items_form: []
        },
        sources: {
            accounting_ledgers: [],
            projects: [],
            sub_projects: [],
            properties: [],
            amenities: [],
            sub_amenities: [],
            departments: [],
            vehicle_equipment: [],
        }
    },
    watch: {
        "material_requests.items_meta.current_page": function (newValue, oldValue) {
            this.material_requests.items_meta.current_page_display = newValue + 1;
            this.material_requests.items_meta.start_count = newValue * this.material_requests.items_meta.per_page;
            this.material_requests.items_meta.end_count = newValue * this.material_requests.items_meta.per_page + this.material_requests.items_meta.per_page - 1;
            if (this.material_requests.items_meta.end_count >= this.material_requests.items_meta.num_entries) {
                this.material_requests.items_meta.end_count = this.material_requests.items_meta.num_entries - 1;
            }

        }
    },
    methods: {

        initObjectRequest() {
            this.object_request_id = window.object_request_id;

            this.fetchObjectRequest();
        },
        resetItemForm() {
            this.items.selected = null;
            this.items.selected_idx = null;
            this.items.loaded = null;
            this.items.form = {};
        },
        checkItemsForm() {
            this.items.check.is_valid = true;
            if (this.items.form.length <= 0) {
                this.items.check.is_valid = false;
            }
        },
        prepItemsFormData() {
            return this.items.form;
        },
        changeInfo(key, val) {
            this.info.form[key] = val;
        },
        prepInfoFormData() {
            return this.info.form;
        },
        checkInfoForm() {
            this.info.check.missing_fields = [];
            this.info.check.is_valid = true;
            for (var key in this.info.required) {
                if (this.info.form[key] === null) {
                    this.info.check.missing_fields.push(this.info.required[key]);
                    this.info.check.is_valid = false;
                }
            }
        },
        submitRequest() {
            this.checkInfoForm();
            let message = '';
            if (!this.info.check.is_valid) {
                ``
                message = 'Missing fields: ' + this.info.check.missing_fields.join(', ');
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            this.checkItemsForm();
            if (!this.items.check.is_valid) {
                message = 'Please add Items';
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            let url = base_url + "material_issuance/process_create";
            if (this.object_request_id) {
                url = base_url + "material_issuance/process_update/" + this.object_request_id;
            }

            let data = {
                request: this.prepInfoFormData(),
                items: this.prepItemsFormData(),
                deleted_items: this.items.deleted_items
            }

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.status) {
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                base_url + "material_issuance"
                            );
                        });
                    }else{
                        this.fetchMaterialRequestItemsSelection(this.material_requests.selected.id, this.material_requests.selected_key);
                    }
                }
            })

        },
        fetchObjects(table, lookup_field) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.sources[table] = response.data;
                    } else {
                        this.sources[table] = [];
                    }
                })
        },
        fetchObjectRequest() {
            if (this.object_request_id) {
                let url = base_url + "material_issuance/search?id=" + this.object_request_id + "&with_relations=no";
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectRequest(response.data);
                        }
                    });
            } else {
                this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
            }

        },
        fetchObjectRequestChildren() {
            let url = base_url + "material_issuance_item/get_all_by_parent/" + this.object_request_id;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.parseObjectRequestItems(response.data);
                    }
                });
        },
        parseObjectRequest(data) {
            if (data.length > 0) {
                this.info.form = data[0];
                this.fetchObjectRequestChildren();
            }
        },
        parseObjectRequestItems(data) {
            for (row in data) {
                this.addItemToCart(false, data[row]);
            }
        },
        addItemToCart(isNew, data) {
            let item = {};

            if (isNew) {
                for (key in data) {
                    item[key] = data[key];
                }
                // do something with id
                // delete item['id'];
                delete item['created_by'];
                delete item['created_at'];
                delete item['updated_at'];
                delete item['updated_by'];
                delete item['deleted_by'];
                delete item['deleted_at'];

            } else {
                for (key in data) {
                    item[key] = data[key];
                }
            }

            if (item.id) {
                let idx = this.searchItemInStore(item.id);
                if (idx !== null) {
                    this.items.data[idx] = item;
                } else {
                    this.items.data.push(item);
                }
            } else {
                this.items.data.push(item);
            }
            this.$forceUpdate();

            this.resetItemForm();
        },
        searchItemInStore(id) {
            for (let i = 0; i < this.items.data.length; i++) {
                if (id == this.items.data[i].id) {
                    return i;
                }
            }
            return null;
        },

        // region material issuance item
        inspectRequestItem(key) {
            $("#selectItemModal").modal("show");

            console.log('load cross referenced data');
        },

        // endregion

        // region material requests
        changeFilter(field, value) {
            this.material_requests.filter[field] = value;
        },
        resetMaterialRequestItems() {
            this.material_requests.selected = null;
            this.material_requests.show_filter = true;
            this.material_requests.items = [];
            this.material_requests.items_form = [];
            this.items.form = [];
            this.material_requests.items_ref = [];
        },
        fetchMaterialRequests() {
            this.material_requests.loading = true;
            let params = $.param(this.material_requests.filter);
            let url = base_url + "material_request/search?" + params;

            axios.get(url)
                .then(response => {
                    this.material_requests.loading = false;
                    if (response.data) {
                        this.processMaterialRequestResponse(response.data);
                    } else {
                        this.material_requests.results = [];
                    }
                })
        },
        processMaterialRequestResponse(data) {
            let d = data;
            for(let i in d){
                d[i].approving_staff_display_name = "";
                d[i].requesting_staff_display_name = "";
                if( d[i].approving_staff){
                    d[i].approving_staff_display_name = d[i].approving_staff.first_name + " " + d[i].approving_staff.last_name;
                }
                if( d[i].requesting_staff){
                    d[i].requesting_staff_display_name = d[i].requesting_staff.first_name + " " + d[i].requesting_staff.last_name;
                }

            }
            this.material_requests.results =  d;
        },
        fetchMaterialRequestItems(mr) {
            let url = base_url + "material_request_item/get_all_by_request/" + mr + "?with_relations=yes&with_quantity_issued=true";
            axios.get(url)
                .then(response => {
                    this.material_requests.items_ref = response.data;
                });
        },
        fetchMaterialRequestItemsSelection(id, key) {
            this.material_requests.items_loading = true;
            let url = base_url + "material_receiving/get_items_by_material_request/" + id + "?warehouse_id=" + this.info.form.warehouse_id;
            this.material_requests.selected_key = key;
            this.material_requests.show_filter = false;
            axios.get(url)
                .then(response => {
                    this.material_requests.items_loading = false;
                    if (response.data) {
                        if (response.data.length > 0) {
                            this.material_requests.items = response.data;
                            this.populateItemsForm();
                            this.material_requests.selected = this.material_requests.results[key];
                            this.info.form.material_request_id = this.material_requests.selected.id;
                            this.material_requests.selected_url = base_url + "material_request/view/" + this.material_requests.selected.id;

                            let select_issued_to_others = document.getElementById("issued_to_others")

                            let option_issued_to_others = document.createElement('option')

                            select_issued_to_others.options.length = 0

                            if (!!this.material_requests.selected.requesting_staff_display_name) {
                                option_issued_to_others.value = this.material_requests.selected.requesting_staff_id ?? "";
                                option_issued_to_others.innerHTML = this.material_requests.selected.requesting_staff_display_name ?? "";
                                select_issued_to_others.appendChild(option_issued_to_others)
                            }
                        } else {
                            swal.fire({
                                title: "Warning!",
                                text: "No items to release for this Material Request Item",
                                type: "warning",
                            })
                            this.material_requests.items = [];
                        }
                    } else {
                        this.material_requests.items = [];
                    }
                    this.initializeItemsMeta();
                })
        },
        hasOverflow(item_id, quantity) {
            quantity = quantity * 1.0;
            let items_ref = this.material_requests.items_ref;
            if(! items_ref ){
                swal.fire({
                    title: "Error!",
                    text: "No items in Material Request",
                    type: "error",
                })
                return false;
            }

            let request_item_counts = {};
            for(let i = 0; i < items_ref.length; i++){
                if(request_item_counts[items_ref[i].item_id]){
                    request_item_counts[items_ref[i].item_id] += items_ref[i].quantity;
                }else{
                    request_item_counts[items_ref[i].item_id] = items_ref[i].quantity;
                }
            }

            let count = this.countItemsInCart(item_id) + quantity;
            if(count > request_item_counts[item_id]){
                return true;
            }
            return false;

        },
        countItemsInCart(item_id) {
            for(let i = 0; i < this.items.form.length; i++){
                if(this.items.form[i].item_id == item_id){
                    return this.items.form[i].quantity * 1.0;
                }
            }
            return 0;
        },
        addToItemsCart(idx) {
            let ref = this.material_requests.items[idx];
            let qty = this.material_requests.items_form[idx].quantity;

            let form_item_quantity = this.getTotalQuantityFromFormByItemId(ref.item_id)

            let available_quantity = this.getTotalQuantityFromMRSItemRef(ref.item_id)

            if (parseInt(available_quantity) - (parseInt(qty) + parseInt(form_item_quantity)) < 0) {

                swal.fire({
                    title: "Error!",
                    text: "This will exceed requested quantity",
                    type: "error",
                })
                return false;
            }

            if (ref.stock_data.remaining < qty) {
                swal.fire({
                    title: "Error!",
                    text: "Not enough stocks of this item from that Material Receiving entity",
                    type: "error",
                })
                return false;
            }

            if (qty <= 0) {
                swal.fire({
                    title: "Error!",
                    text: "Invalid quantity: " + qty,
                    type: "error",
                })
                return false;
            }

            if( this.hasOverflow(ref.item_id, qty) ){
                swal.fire({
                    title: "Error!",
                    text: "This quantity will exceed the requested amount!",
                    type: "error",
                })
                return false;
            }

            let item = {
                ref: ref,
                quantity: qty,
                material_receiving_ref: ref.material_receiving.reference,
                material_receiving_id: ref.material_receiving.id,
                material_receiving_item_id: ref.id,
                item_id: ref.item_id
            }
            this.items.form.push(item);
            this.recomputeCart();
        },
        getTotalQuantityFromFormByItemId(item_id) {
            if (this.items.form.length > 0) {

                let items = this.items.form.filter((item) => {

                    return item.item_id == item_id
                })

                let quantity = 0

                if (items.length > 0) {

                    items.forEach((item) => {

                        quantity += parseInt(item.quantity)
                    })
                }

                return quantity
            } else {

                return 0
            }
        },
        getTotalQuantityFromMRSItemRef(item_id) {
            
            let items = this.material_requests.items_ref.filter((item) => {
                
                return item.item_id == item_id
            })

            let quantity = 0

            if (items.length > 0) {

                let item = items[0]

                quantity = item.quantity - item.quantity_issued >= 0
                    ? item.quantity - item.quantity_issued
                    : 0
            }

            return quantity
        },
        removeItemFromCart(idx) {
            this.items.form.splice(idx, 1);
            this.recomputeCart();
        },
        recomputeCart() {
            let total = 0.0;
            let quantity = 0.0;
            for (let i = 0; i < this.items.form.length; i++) {
                total += (this.items.form[i].ref.unit_cost * 1.0) * (this.items.form[i].quantity * 1.0);
                quantity += this.items.form[i].quantity * 1.0;
            }
            this.items.total = total;
            this.items.quantity = quantity;
            this.$forceUpdate();
        },
        populateItemsForm() {
            for (let i = 0; i < this.material_requests.items.length; i++) {
                this.material_requests.items_form.push({
                    key: i,
                    quantity: 0
                })
            }
        },
        initializeItemsMeta() {
            const per_page = 3;
            this.material_requests.items_meta = {
                per_page: per_page,
                num_entries: this.material_requests.items.length,
                current_page: 0,
                current_page_display: 1,
                show_detail: null,
                start_count: 0,
                end_count: per_page - 1,
                num_pages: Math.ceil(this.material_requests.items.length / per_page)
            }
        },
        toggleDetail(key) {
            if (!this.material_requests.items_meta.show_detail) {
                this.material_requests.items_meta.show_detail = key;
            } else {
                this.material_requests.items_meta.show_detail = null;
            }
            this.$forceUpdate();
        },
        lookupMRRStatus(key) {
            let s = [
                'Invalid Status',
                'New Request',
                'For RPO',
                'For Issuance',
                'Issued',
                'Cancelled',
            ];
            return s[key];
        }
        // endregion

    },
    mounted() {

        this.initObjectRequest();

        this.is_mounted = true;
        this.object_request_id = window.object_request_id;


    }
});