var app = new Vue({
    el: '#warehouse_inventory_app',
    data: {
        inventory_item_id: null,
        warehouse_id: null,
        item_id: null,
        material_items: {
            data: [],
            form: [],
            loading: false
        },
    },
    watch: {},
    mounted() {
        this.inventory_item_id = window.object_request_id;
        this.warehouse_id = window.warehouse_id;
        this.item_id = window.item_id;

        this.loadMaterialInventory();
    },
    methods: {
        loadMaterialInventory(){
            this.material_items.form = [];
            this.material_items.loading = true;
            let url = base_url + "material_receiving/get_items_by_item_id/" + this.item_id + "?warehouse_id=" + this.warehouse_id;

            axios.get(url)
                .then(response => {
                    if(response.data.length > 0){
                        this.material_items.data = response.data;
                        for(let i = 0; i < this.material_items.data.length; i++){
                            let mi = this.material_items.data[i];
                            let form_item = {
                                material_receiving_item_id: mi.id,
                                quantity: mi.quantity,
                            }
                            this.material_items.form.push(form_item);
                        }
                    }else{
                        this.material_items.data = [];
                        this.material_items.form = [];
                    }
                    this.material_items.loading = false;
                });
        },
        // endregion

        submitRequest() {
            let url = base_url + "warehouse_inventory/process_update/" + this.inventory_item_id;
            let data = {
                items: this.material_items.form
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function(response){
                    if(response.status){
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                url
                            );
                        });
                    }
                }
            })
        }
    }
});