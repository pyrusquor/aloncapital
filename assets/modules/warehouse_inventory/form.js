$(document).ready(function () {
    $("#warehouse_id").on('change', function (e) {
        app.changeInfo("warehouse_id", $(this).val());
    });

    $("#item_id").on('change', function (e) {
        let val = $(this).val();
        app.changeInfo("item_id", val);
        app.fetchCanvassDetails(val);
    });

    $("#unit_of_measurement_id").on('change', function (e) {
        app.changeInfo("unit_of_measurement_id", $(this).val());
    });

});

const __info_form_defaults = {
    warehouse_id: null,
    item_id: null,
    unit_of_measurement_id: null,
    actual_quantity: null,
    available_quantity: null,
    unit_price: null,
}

var app = new Vue({
    el: '#warehouse_inventory_app',
    data: {
        object_request_id: null,
        object_status: 1,

        info: {
            form: {},
            required: {
                warehouse_id: "Warehouse",
                item_id: "Item",
                unit_of_measurement_id: "Unit of Measurement",
                actual_quantity: "Actual Quantity",
                unit_price: "Unit price",
            },
            check: {
                is_valid: false,
                missing_fields: []
            },
            is_active: false,
        },
        items: {
            form: [],
            data: [],
            selected: null,
            selected_idx: null,
            required: {},
            check: {
                is_valid: false,
            },
            deleted_items: [],
            loading: false
        },
        canvass: {
            form: [],
            data: [],
            selected: null,
            selected_idx: null,
            required: {},
            check: {
                is_valid: false,
            },
            deleted_items: [],
            loading: false
        },
        sources: {
            canvass: null
        }
    },
    watch: {
        "info.form.warehouse_id": function(newValue, oldValue) {
            this.checkItemExistsInWarehouse();
        },
        "info.form.item_id": function(oldValue, newValue) {
            this.checkItemExistsInWarehouse();
        },
    },
    methods: {

        initObjectRequest() {
            this.object_request_id = window.object_request_id;

            this.fetchObjectRequest();
        },

        resetItemForm() {
            this.items.selected = null;
            this.items.selected_idx = null;
            this.items.loaded = null;
            this.items.form = {};
        },

        checkItemsForm() {
            this.items.check.is_valid = true;
            if (this.items.data.length <= 0) {
                this.items.check.is_valid = false;
            }
        },

        prepItemsFormData() {
            return this.items.data;
        },

        changeInfo(key, val) {
            this.info.form[key] = val;
            this.$forceUpdate();
        },

        prepInfoFormData() {
            this.info.form.available_quantity = this.info.form.actual_quantity;
            return this.info.form;
        },

        checkInfoForm() {
            this.info.check.missing_fields = [];
            this.info.check.is_valid = true;
            for (var key in this.info.required) {
                if (this.info.form[key] === null) {
                    this.info.check.missing_fields.push(this.info.required[key]);
                    this.info.check.is_valid = false;
                }
            }
        },

        checkItemExistsInWarehouse()
        {
            if(this.object_request_id) {
                return false;
            }
            let item_id = this.info.form.item_id;
            let warehouse_id = this.info.form.warehouse_id;

            if(! item_id || ! warehouse_id ) {
                return false;
            }

            let url = base_url + "warehouse_inventory/get_item?item_id=" + item_id + "&warehouse_id=" + warehouse_id;

            axios.get(url)
                .then(response => {
                    if(response.data && response.data.length > 0){
                        let obj_id = response.data[0].id;
                        swal.fire({
                            title: "Warning!",
                            text: "This item already exists in this warehouse.",
                            type: "warning",
                        }).then(function () {

                        });
                    }
                })
        },

        submitRequest() {
            this.checkInfoForm();
            let message = '';
            if (!this.info.check.is_valid) {
                message = 'Missing fields: ' + this.info.check.missing_fields.join(', ');
                swal.fire({
                    title: "Oooops!",
                    text: message,
                    type: "error",
                });
                return false;
            }

            let url = base_url + "warehouse_inventory/process_create";
            if (this.object_request_id) {
                url = base_url + "warehouse_inventory/process_update/" + this.object_request_id;
            }

            let data = {
                request: this.prepInfoFormData(),
                items: this.prepItemsFormData(),
                deleted_items: this.items.deleted_items
            }

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.status) {
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                base_url + "warehouse/view/" + window.warehouse_id
                            );
                        });
                    }
                }
            })

        },

        fetchCanvassDetails(id) {
            this.canvass.loading = true;
            let url = base_url + "generic_api/helper_lookup?helper=item&func=get_item_unit_prices&value=" + id;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.sources.canvass = response.data;
                    } else {
                        this.sources.canvass = [];
                    }
                    this.canvass.loading = false;
                })
        },

        fetchObjects(table, lookup_field) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.sources[table] = response.data;
                    } else {
                        this.sources[table] = [];
                    }
                })
        },

        fetchObjectRequest() {
            if (this.object_request_id) {
                let url = base_url + "warehouse_inventory/search?id=" + this.object_request_id + "&with_relations=no";
                axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectRequest(response.data);
                        }
                    });
            } else {
                this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
            }

        },
        fetchObjectRequestChildren() {
            let url = base_url + "warehouse_inventory_item/get_all_by_parent/" + this.object_request_id;
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.parseObjectRequestItems(response.data);
                    }
                });
        },
        parseObjectRequest(data) {
            if (data.length > 0) {
                this.info.form = data[0];
            }
            this.fetchCanvassDetails(this.info.form.item_id);
        },

        parseObjectRequestItems(data) {
            for (row in data) {
                this.addItemToCart(false, data[row]);
            }
        },

        addItemToCart(isNew, data) {
            let item = {};

            if (isNew) {
                for (key in data) {
                    item[key] = data[key];
                }
                // do something with id
                // delete item['id'];
                delete item['created_by'];
                delete item['created_at'];
                delete item['updated_at'];
                delete item['updated_by'];
                delete item['deleted_by'];
                delete item['deleted_at'];

            } else {
                for (key in data) {
                    item[key] = data[key];
                }
            }

            if (item.id) {
                let idx = this.searchItemInStore(item.id);
                if (idx !== null) {
                    this.items.data[idx] = item;
                } else {
                    this.items.data.push(item);
                }
            } else {
                this.items.data.push(item);
            }
            this.$forceUpdate();

            this.resetItemForm();
        },

        searchItemInStore(id) {
            for (let i = 0; i < this.items.data.length; i++) {
                if (id == this.items.data[i].id) {
                    return i;
                }
            }
            return null;
        },

    },
    mounted() {

        this.initObjectRequest();

        this.is_mounted = true;
        this.object_request_id = window.object_request_id;
        if(window.warehouse_id){
            this.changeInfo("warehouse_id", window.warehouse_id);
        }


    }
});