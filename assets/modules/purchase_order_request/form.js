$(document).ready(function () {
    var datepicker = function () {
        // minimum setup
        $('#request_date').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        }).on('changeDate', function (e) {
            app.changeRequestDate($("#request_date").val());
            app.checkInfoForm();
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true
        });
    };

    datepicker();

    $("#company_id").on("change", function (e) {
        app.changeCompanyId($(this).val());
        app.checkInfoForm();
    });
    $("#warehouse_id").on("change", function (e) {
        app.changeWarehouseId($(this).val());
        app.checkInfoForm();
    });
    $("#approving_staff_id").on("change", function (e) {
        app.changeApprovingStaffId($(this).val());
        app.checkInfoForm();
    });
    $("#requesting_staff_id").on("change", function (e) {
        app.changeRequestingStaffId($(this).val());
        app.checkInfoForm();
    });

});

let __request_data_fields = {};
const __request_items_data_defaults = {
    id: null,
    item_group_id: null,
    item_type_id: null,
    item_brand_id: null,
    item_class_id: null,
    item_id: null,
    unit_of_measurement_id: null,
    material_request_id: null,
    purchase_order_request_id: null,
    item_brand_name: null,
    item_class_name: null,
    item_name: null,
    unit_of_measurement_name: null,
    item_group_name: null,
    item_type_name: null,
    quantity: null,
    unit_cost: null,
    total_cost: null,
}

var app = new Vue({
    el: '#purchase_order_request_app',
    data: {
        is_ready: false,
        purchase_request_id: null,
        wizard: {
            current: 1,
        },
        info: {
            form: {},
            required_fields: [
                'company_id',
                'approving_staff_id',
                'requesting_staff_id',
                'request_date',
                // 'accounting_ledger_id',
            ],
            form_is_valid: false
        },
        request_items: {
            cart: [],
            form: [],
            total_cost: 0.0,
            hasItems: false,
            deleted_items: []
        },
        material_requests: {
            showFilter: true,
            selected: null,
            filter: {
                request_status: 2,
                project_id: null,
                sub_project_id: null,
                property_id: null,
                amenity_id: null,
                sub_amenity_id: null,
                department_id: null,
                vehicle_equipment_id: null,
                reference: null
            },
            results: [],
            items: []
        },
        sources: {
            accounting_ledgers: [],
            projects: [],
            sub_projects: [],
            properties: [],
            amenities: [],
            sub_amenities: [],
            departments: [],
            vehicle_equipment: [],
        }
    },
    watch: {
        'info.form': {
            handler: function (oldValue, newValue) {
                this.checkInfoForm();
            }, deep: true
        },
        'request_items.cart': {
            handler: function(oldValue, newValue) {
                this.checkItemsForm();
                this.calculateTotalCost();
            }, deep: true
        }
    },
    methods: {
        wizardSwitchPane(n) {
            this.wizard.current = n;
        },
        wizardLabelState(n) {
            if (n === this.wizard.current) {
                return "current";
            } else {
                return "";
            }
        },
        wizardStepStateClass(n) {
            if (n === this.wizard.current) {
                return "kt-wizard-v3__content active";
            } else {
                return "kt-wizard-v3__content";
            }
        },

        confirmSubmitRequest() {

            let items = this.prepObjectRequestItems()

            console.log(items)
            
            let request = items.reduce((promiseChain, item) => {

                return promiseChain.then(() => new Promise((resolve) => {

                    this._requestPurchaseOrderRequestItem(item, resolve)
                }))
            }, Promise.resolve())

            request.then(() => {
                
                if (!this.has_exceeded) {

                    this.submitPurchaseOrderRequest()
                }
            })
        },

        async _requestPurchaseOrderRequestItem(item, resolve) {

            return await axios.post(base_url + "material_request_item/get_item/" + item.material_request_id + "/" + item.item_id)
            .then(response => {
                
                if (response.data) {
                    
                    if (parseInt(item.quantity) > parseInt(response.data.quantity)) {

                        this.has_exceeded = true

                        this.wizardSwitchPane(1)

                        swal.fire({
                            title: "Oooops!",
                            text: item.item_name + " has exceeded the allowed quantity: " + response.data.quantity,
                            type: "error",
                        }).then(() => {

                            // console.log("asd")
                        });   
                    } else {

                        this.has_exceeded = false
                        resolve()
                    }
                } else {

                    this.has_exceeded = true
                }
            })
        },

        // form
        submitPurchaseOrderRequest() {
            let url = base_url + "purchase_order_request/process_create";
            if (this.object_request_id) {
                url = base_url + "purchase_order_request/process_update/" + this.object_request_id;
            }
            let info_data = this.prepInfoData();
            let items_data = this.prepObjectRequestItems();

            let data = {
                request: info_data,
                items: items_data,
                deleted_items: this.request_items.deleted_items
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if(response.status){
                        swal.fire({
                            title: "Success!",
                            text: response.message,
                            type: "success",
                        }).then(function () {
                            window.location.replace(
                                base_url + "purchase_order_request"
                            );
                        });
                    }
                }
            })
        },

        prepInfoData() {
            let form = {};
            for (key in this.info.form) {
                if (this.searchField(__request_data_fields, key)) {
                    if (this.info.form[key]) {
                        form[key] = this.info.form[key];
                    } else {
                        form[key] = null;
                    }
                }
            }
            if(form.request_status == 0 || form.request_status == null){
                form.request_status = 1;
            }
            form.total_cost = this.request_items.total_cost;
            return form;
        },

        prepObjectRequestItems() {
            let items_data = [];
            for (i in this.request_items.cart) {
                let item_data = {};
                for (key in this.request_items.cart[i]) {
                    if (this.searchField(__request_items_data_defaults, key)) {
                        if (this.request_items.cart[i][key]) {
                            item_data[key] = this.request_items.cart[i][key];
                        } else {
                            item_data[key] = null;
                        }
                    }
                }
                items_data.push(item_data);
            }
            return items_data;
        },

        searchField(fields, field) {
            for (key in fields) {
                if (field === key) {
                    return true;
                }
            }
            return false;
        },

        // init
        initializeForm(form, defaults) {
            for (var k in defaults) {
                this[form].form[k] = defaults[k];
            }

        },
        resetMaterialRequestItems() {
            this.material_requests.selected = null;
            this.material_requests.showFilter = true;
            this.material_requests.items = [];
        },

        // lookups
        lookupHelper(helper, func, value) {

            let url = base_url + "generic_api/helper_lookup?helper=" + helper + "&func=" + func + "&value=" + value;
            let val = '';
            axios.get(url)
                .then(response => {
                    return new Promise((resolve) => {
                        resolve(response.data);
                    })
                });
        },

        // RPO items
        addItemToPurchaseRequest(key) {
            let item = this.material_requests.items[key];
            let mr_id = this.material_requests.selected.id;

            let cart_item = {
                id: null,
                item_group_id: item.item_group_id,
                item_type_id: item.item_type_id,
                item_brand_id: item.item_brand_id,
                item_class_id: item.item_class_id,
                item_id: item.item_id,
                unit_of_measurement_id: item.unit_of_measurement_id,
                material_request_id: mr_id,
                purchase_order_request_id: item.purchase_order_request_id,
                item_brand_name: item.item_brand.name,
                item_class_name: item.item_class.name,
                item_name: item.item.name,
                unit_of_measurement_name: item.unit_of_measurement.name,
                item_group_name: item.item_group.name,
                item_type_name: item.item_type.name,
                quantity: item.quantity,
                unit_cost: item.unit_cost,
                total_cost: item.total_cost,
            };

            if (item.quantity) {

                this.request_items.cart.push(cart_item);
                this.calculateTotalCost();
            } else {

                swal.fire({
                    title: "Can't Add",
                    text: "Quantity: " + item.quantity,
                    type: "error",
                })
            }
        },
        removeItemFromPurchaseRequest(key) {
            if(this.object_request_id){
                let row = this.request_items.cart[key];
                if(row.id){
                    this.request_items.deleted_items.push(row.id);
                }
            }
            this.request_items.cart.splice(key, 1);
            if (this.request_items.cart.length <= 0) {
                this.resetMaterialRequestItems();
            }
        },
        updateCartItemTotalCost(key) {
            let qty = this.request_items.cart[key].quantity;
            let unit_cost = this.request_items.cart[key].unit_cost;
            let total_cost = qty * unit_cost;
            this.request_items.cart[key].total_cost = total_cost;
            this.calculateTotalCost();
        },
        calculateTotalCost() {
            let total = 0.0;
            if (this.request_items.cart.length > 0) {
                for (i in this.request_items.cart) {
                    total += (this.request_items.cart[i].total_cost * 1.0);
                }
            }
            this.request_items.total_cost = total;
        },

        // parsers
        parseObjectRequest(data) {
            if (data.length > 0) {
                this.info.form = data[0];
                this.fetchObjectRequestChildren();
            }
        },
        parseObjectRequestItems(data) {
            // change this depending on source
            if (data.length > 0) {
                for (i in data) {
                    this.addItemToPurchaseRequest(null, 'api', data[i]);
                }
            }
        },

        async initObjectRequest() {
            await this.fetchObjectRequest();
        },

        // fetchers
        async fetchObjectRequest() {
            if (this.object_request_id) {
                let url = base_url + "purchase_order_request/search?id=" + this.object_request_id + "&with_relations=no";
                await axios.get(url)
                    .then(response => {
                        if (response.data) {
                            this.parseObjectRequest(response.data);
                            this.fetchObjectRequestChildren();
                        }
                    });
            } else {
                // this.info.form = JSON.parse(JSON.stringify(__info_form_defaults));
            }
            // this.request_items.cart = JSON.parse(JSON.stringify(__request_items_data_defaults));
        },
        fetchObjectRequestChildren() {
            let url = base_url + "purchase_order_request_item/get_all_by_request/" + this.object_request_id;
            this.request_items.cart = [];
            axios.get(url)
                .then(response => {
                    if (response.data) {
                        // this.parseObjectRequestItems(response.data);
                        this.request_items.cart = response.data;
                    }
                });
        },
        async fetchAccountingLedgers() {
            let url = base_url + "generic_api/expense_ledgers";
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        // response.data.unshift({id: '', name: ''});
                        this.sources.accounting_ledgers = response.data;
                    }
                })
        },
        async fetchObjects(table, lookup_field) {
            let url = base_url + "generic_api/fetch_all?table=" + table;
            if (lookup_field) {
                let value = this.material_requests.filter[lookup_field];
                url = base_url + "generic_api/fetch_specific?table=" + table + "&field=" + lookup_field + "&value=" + value;
            }
            await axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.sources[table] = response.data;
                    } else {
                        this.sources[table] = [];
                    }
                })
        },
        fetchMaterialRequests() {
            let params = $.param(this.material_requests.filter);
            let url = base_url + "material_request/search?" + params;

            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.processMaterialRequestResponse(response.data);
                    } else {
                        this.material_requests.results = [];
                    }
                })
        },
        processMaterialRequestResponse(data) {
            let d = data;
            for(let i in d){
                d[i].approving_staff_display_name = "";
                d[i].requesting_staff_display_name = "";
                if( d[i].approving_staff){
                    d[i].approving_staff_display_name = d[i].approving_staff.first_name + " " + d[i].approving_staff.last_name;
                }
                if( d[i].requesting_staff){
                    d[i].requesting_staff_display_name = d[i].requesting_staff.first_name + " " + d[i].requesting_staff.last_name;
                }

            }
            this.material_requests.results =  d;
        },
        fetchMaterialRequestItems(mr, key) {
            let url = base_url + "material_request_item/get_all_by_request/" + mr + "?with_current_quantity=true";
            this.material_requests.selected = this.material_requests.results[key];
            this.material_requests.showFilter = false;

            let variable_keys_with_id = [
                "company_id",
                "approving_staff_id",
                "requesting_staff_id",
            ]

            Object.keys(this.info.form).forEach((key) => {

                if (variable_keys_with_id.includes(key)) {

                    this.info.form[key] = this.material_requests.selected[key.replace("_id", "")]?.id ?? ""
                } else {

                    this.info.form[key] = this.material_requests.selected[key] ?? ""
                }
            });

            let select_company = document.getElementById("company_id")
            let select_approving_staff = document.getElementById("approving_staff_id")
            let select_requesting_staff = document.getElementById("requesting_staff_id")

            let option_company = document.createElement('option')
            let option_approving_staff = document.createElement('option')
            let option_requesting_staff = document.createElement('option')

            select_company.options.length = 0
            select_approving_staff.options.length = 0
            select_requesting_staff.options.length = 0

            if (!!this.material_requests.selected.company) {

                option_company.value = this.material_requests.selected.company.id ?? "";
                option_company.innerHTML = this.material_requests.selected.company.name ?? "";
                select_company.appendChild(option_company)
            }

            if (!!this.material_requests.selected.approving_staff) {

                option_approving_staff.value = this.material_requests.selected.approving_staff.id ?? "";
                option_approving_staff.innerHTML = (this.material_requests.selected.approving_staff.last_name ?? "") + (this.material_requests.selected.approving_staff.first_name ?? "");
                select_approving_staff.appendChild(option_approving_staff)
            }

            if (!!this.material_requests.selected.requesting_staff) {

                option_requesting_staff.value = (this.material_requests.selected.requesting_staff.id ?? "");
                option_requesting_staff.innerHTML = (this.material_requests.selected.requesting_staff.last_name ?? "") + (this.material_requests.selected.requesting_staff.first_name ?? "");
                select_requesting_staff.appendChild(option_requesting_staff)
            }

            this.checkInfoForm()

            axios.get(url)
                .then(response => {
                    if (response.data) {
                        this.material_requests.items = response.data;
                    } else {
                        this.material_requests.items = [];
                    }
                })
        },

        // change triggers
        checkInfoForm() {
            let fields_complete = true;
            for (let i = 0; i < this.info.required_fields.length; i++) {
                let req_field = this.info.form[this.info.required_fields[i]];
                if (!req_field) {
                    fields_complete = false;
                }
            }
            this.info.form_is_valid = fields_complete;
        },
        checkItemsForm() {
            this.request_items.form_is_valid = this.request_items.cart.length > 0;
        },
        changeRequestDate(date) {
            this.info.form.request_date = date; //date.toISOString().split("T")[0];
        },
        changeCompanyId(id) {
            this.info.form.company_id = id;
        },
        changeWarehouseId(id) {
            this.info.form.warehouse_id = id;
        },
        changeApprovingStaffId(id) {
            this.info.form.approving_staff_id = id;
        },
        changeRequestingStaffId(id) {
            this.info.form.requesting_staff_id = id;
        },
    },
    async mounted() {
        this.is_ready = false;
        this.object_request_id = window.object_request_id;
        this.initializeForm("info", window.info_form_data);
        // await this.fetchAccountingLedgers();
        this.is_ready = true;

        await this.fetchObjects("projects", null);
        await this.fetchObjects("sub_projects", null);
        await this.fetchObjects("amenities", null);
        await this.fetchObjects("sub_amenities", null);
        // await this.fetchObjects("properties", null);
        await this.fetchObjects("departments", null);
        // this.fetchObjects("vehicle_equipment", null, null);

        __request_data_fields = window.object_request_fillables;
        if (this.purchase_request_id) {
            this.checkInfoForm();
        }
        await this.initObjectRequest();
    }
});