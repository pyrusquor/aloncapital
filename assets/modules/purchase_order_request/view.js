var app = new Vue({
    el: '#document_attachment_modal',
    data: {
        documents: [],
        loading: false,
        object_type: "purchase_order_requests",
        object_type_id: window.object_id
    },
    methods: {
        fetchDocuments(){
            this.loading = true;
            let url = base_url + "document_attachment/search?object_type=" + this.object_type + "&object_type_id=" + this.object_type_id;
            axios.get(url).then( response => {
                this.documents = response.data;
                this.loading = false;
            })
        }
    },
    mounted() {
        this.fetchDocuments();
    }
});