// Class definition
var FormGen = function() {
    // Private functions
    var sgSelect = function() {
        
		// Seller Type
		$('#table_list').select2({
            placeholder: "Select a table",
            allowClear: true
        });

         

    }
    
    

    // Public functions
    return {
        init: function() {
            
            sgSelect();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    FormGen.init();
    datepicker();
});