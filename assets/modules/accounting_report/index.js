'use strict';
var Form_Generator = (function () {

    let tableElement = $('#accounting_report_table');

    var formGenTable = function () {

        var dataTable = tableElement.DataTable({
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            dom:
                "<'row'<'col-sm-12 col-md-6'l>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'accounting_report/showItems',
            },
            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'name',
                },
                {
                    data: 'description',
                },
                {
                    data: 'company_id',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
							<span class="dropdown">
								<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
								<i class="la la-ellipsis-h"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a href="` +
                            base_url +
                            `accounting_report/update/` +
                            row.id +
                            `"  class="dropdown-item"><i class="fa flaticon-edit"></i> Generate </a>
								</div>
							</span>
							<a href="` +
                            base_url +
                            `accounting_report/view/` +
                            row.id +
                            `" target="_blank" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
							<i class="fa flaticon-eye"></i>
							</a>`
                        );
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_template', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'accounting_report/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: { id: id },
                        success: function (res) {
                            if (res.status) {
                                $('#accounting_report_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            formGenTable();
            filter();
        },
    };
})();

jQuery(document).ready(function () {
    Form_Generator.init();
});
