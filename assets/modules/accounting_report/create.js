// Class definition
var FormGen = function() {
    // Private functions
    var sgSelect = function() {
        
		// Seller Type
		$('#table_list').select2({
            placeholder: "Select a table",
            allowClear: true
        });

         $(document).on('select2:select', '#table_list', function (e) {
            var TblName = $(this).val();
            var tableContent = '';
            
            $.ajax({
                url: base_url + 'form_generator/get_table_fields/' + TblName,
                dataType: "JSON",
                type: 'GET',
                success: function (data) {

                    $.each(data, function (key, item) {
                        tableContent += '<tr>';
                        tableContent += '<td>' + item + '</td>';
                        tableContent += '<td class="kt-align-center"><button type="button" class="btn btn-primary btn-xs table_field" data-field="'+ TblName +'.'+ item +'">Insert</button></td>';
                        tableContent += '</tr>';
                    });

                    $('#tbody').html(tableContent);

                }
            });
            
		});

    }
    
    var tinyMCE = function() {

        tinymce.init({
            mode : "none",
            selector:'textarea#contentEditor',
            height: 500,
            
            // ===========================================
            // INCLUDE THE PLUGIN
            // ===========================================
            
            plugins: [
                "code advlist autolink accordion link image lists charmap print preview hr pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste jbimages table advtable"
            ],
            toolbar: "formatselect | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | responsivefilemanager | pastetext | fontselect | fontsizeselect | preview | accordion | code ", 
            
        });

        $(document).on('click', '.table_field', function (e) {
           
            var field_selected = $(this).attr('data-field');

            if(field_selected == null ){
		
                modal_trigger('Warning','Select a Field!');
                
            } else {
                
                /* console.log($.inArray(field_selected,existing_fields));
                if($.inArray(field_selected,existing_fields) >= 0){
                    
                    alert('ALREADY EXIST!');
                    
                } else { */
                
                    tinymce.activeEditor.execCommand('mceInsertContent', false, "{{"+field_selected+"}}");
                    existing_fields.push(field_selected);
                
                //  } 
            }
		});

    }

    var valForm = function () {
        $( "#form_generator" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true 
                },
                description: {
                    required: true 
                },
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {

				toastr.error("Please check your fields", "Something went wrong");

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }

    // Public functions
    return {
        init: function() {
            valForm();
            sgSelect();
            tinyMCE();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    FormGen.init();
});