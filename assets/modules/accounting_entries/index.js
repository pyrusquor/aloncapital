'use strict';
var AccountingEntries = (function () {
    let tableElement = $("#accounting_entries_table");

    var AccountingEntriesTable = function () {

        var dataTable = tableElement.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,

            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'accounting_entries/showItems',
            },
            dom:
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-check"></i> Approve Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm text-nowrap mr-2',
                    attr: {
                        id: 'bulkApprove',
                    },
                    enabled: false,
                },
                {
                    text: '<i class="la la-times"></i> Disapprove Selected',
                    className:
                        'btn btn-sm btn-label-warning btn-elevate btn-icon-sm text-nowrap mr-2',
                    attr: {
                        id: 'bulkDisapprove',
                    },
                    enabled: false,
                },
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-danger btn-elevate btn-icon-sm text-nowrap',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                {
                    data: 'company_id',
                },
                {
                    data: 'project',
                },
                {
                    data: 'payee_type',
                },
                {
                    data: 'payee_type_id',
                },
                {
                    data: 'or_number',
                },
                {
                    data: 'invoice_number',
                },
                {
                    data: 'journal_type',
                },
                {
                    data: 'payment_date',
                },
                {
                    data: 'cr_total',
                },
                {
                    data: 'remarks',
                },
                {
                    data: 'is_approve',
                },
                {
                    data: "created_by",
                },
                {
                    data: "updated_by",
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var item = '';
                        if (row.is_approve == 0) {
                            item = `<a href="javascript:void(0);" class="dropdown-item approve_accounting_entries" data-id="` + row.id + `"><i class="la la-check"></i> Approve </a>` + `<a href="javascript:void(0);" class="dropdown-item disapprove_accounting_entries" data-id="` + row.id + `"><i class="la la-check"></i> Disapprove </a>`;
                        } else if (row.is_approve == 1) {
                            item = `<a href="javascript:void(0);" class="dropdown-item disapprove_accounting_entries" data-id="` + row.id + `"><i class="la la-check"></i> Disapprove </a>`
                        };
                        return (
                            `
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="` +
                            base_url +
                            `accounting_entries/update/` +
                            row.id +
                            `"><i class="la la-edit"></i> Update </a>
										<a href="javascript:void(0);" class="dropdown-item remove_accounting_entries" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete </a>` + item + `

                            
									</div>
								</span>
								<a href="` +
                            base_url +
                            `accounting_entries/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        );
                    },
                },
                // {
                //     targets: [0, 1, 3, 4, -1],
                //     className: 'dt-center',
                // },
                // {
                //     targets: [2, 3, 4, 5, 6, 7, 8, 9],
                //     orderable: false,
                //     searchable: false,
                // },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                {
                    targets: 8,
                    render: function (data, type, full, meta) {
                        var journal_type = {
                            1: {
                                type: 'Journal Voucher',
                            },
                            2: {
                                type: 'Sales Journal',
                            },
                            3: {
                                type: 'Cash Receipt Journal',
                            },
                            4: {
                                type: 'Purchase Journal',
                            },
                            5: {
                                type: 'Cash Payment Journal',
                            },
                        };

                        if (typeof journal_type[data] === 'undefined') {
                            return ``;
                        }
                        return journal_type[data].type;
                    },
                },
                {
                    targets: 12,
                    render: function (data, type, full, meta) {
                        if (data == 0) {
                            return 'Pending';
                        } else if (data == 1) {
                            return 'Approved';
                        } else {
                            return 'Disapproved';
                        }
                    },
                },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_accounting_entries', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'accounting_entries/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#accounting_entries_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on('click', '#btn_upload_guide', function () {
            var table = $('#upload_guide_table');

            table.DataTable({
                order: [[0, 'asc']],
                pagingType: 'full_numbers',
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + 'accounting_entries/get_table_schema',
                columns: [
                    // {data: 'button'},
                    {
                        data: 'no',
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'type',
                    },
                    {
                        data: 'format',
                    },
                    {
                        data: 'option',
                    },
                    {
                        data: 'required',
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $('#accounting_entries_table').DataTable();

        $('#_search').on('keyup', function () {
            dTable.search($(this).val()).draw();
        });

        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $('#accounting_entries_table').DataTable();
        var bulk_approve_button = _table.buttons(['#bulkApprove']);
        var bulk_disapprove_button = _table.buttons(['#bulkDisapprove']);
        var bulk_delete_button = _table.buttons(['#bulkDelete']);

        $('#select-all').on('click', function () {
            if ($(this).is(':checked')) {

                $('.delete_check').prop('checked', true);

                bulk_approve_button.enable()
                bulk_disapprove_button.enable()
                bulk_delete_button.enable()
            } else {

                $('.delete_check').prop('checked', false);

                bulk_approve_button.disable()
                bulk_disapprove_button.disable()
                bulk_delete_button.disable()
            }
        });

        $('#accounting_entries_table tbody').on(
            'change',
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $('input#select-all').get(0);

                    if (el && el.checked && 'indeterminate' in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                // _table.button(0).disable();
                bulk_approve_button.disable()
                bulk_disapprove_button.disable()
                bulk_delete_button.disable()
            } else {
                // _table.button(0).enable(_checked > 0);
                bulk_approve_button.enable(_checked > 0)
                bulk_disapprove_button.enable(_checked > 0)
                bulk_delete_button.enable(_checked > 0)
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'accounting_entries/bulkDelete/',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#accounting_entries_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });

        $('#bulkApprove').click(function () {

            var approve_array = [];

            $('input[name="id[]"]:checked').each(function () {
                approve_array.push($(this).val());
            });

            if (approve_array.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, approve it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            url: base_url + 'accounting_entries/bulk_approve_entry',
                            data: {
                                approve_array: approve_array,
                            },
                            success: function (response) {
                                if (response.status) {
                                    $('#accounting_entries_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Approved!',
                                        response.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', response.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Accounting Entries not approved.',
                            'error'
                        );
                    }
                });
            }
        });

        $('#bulkDisapprove').click(function () {

            var disapprove_array = [];

            $('input[name="id[]"]:checked').each(function () {
                disapprove_array.push($(this).val());
            });

            if (disapprove_array.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, disapprove it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            url: base_url + 'accounting_entries/bulk_disapprove_entry',
                            data: {
                                disapprove_array: disapprove_array,
                            },
                            success: function (response) {
                                if (response.status) {
                                    $('#accounting_entries_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Disapproved!',
                                        response.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', response.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Accounting Entries not disapproved.',
                            'error'
                        );
                    }
                });
            }
        });
    };

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            format: 'yyyy-mm-dd',
            autoclose: true,
            templates: arrows,
        });
    };

    // Daterangepicker Init
    var daterangepickerInit = function () {
        if ($('.kt_transaction_daterangepicker').length == 0) {
            return;
        }

        var picker = $('.kt_transaction_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                // startDate: start,
                // endDate: end,
                opens: 'left',
                autoUpdateInput: false,
                locale: {
                    format: 'Y-MM-DD',
                    cancelLabel: 'Clear'
                  }
                // ranges: {
                // 	'Today': [moment(), moment()],
                // 	'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                // 	'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                // 	'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                // 	'This Month': [moment().startOf('month'), moment().endOf('month')],
                // 	'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                // }
            },
            cb
        );

        picker.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
        });

        picker.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        cb(start, end, '');
    };
    var confirmApprove = function () {
        $(document).on('click', '.approve_accounting_entries', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "Approve entry " + id + "?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Approve entry!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'accounting_entries/approve_entry/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                get_accounting_entry_information($('#filter_start_date').val(), $('#filter_end_date').val());
                                $('#accounting_entries_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Approved!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Accounting Entry ' + id + ' not approved',
                        'error'
                    );
                }
            });
        });
    };

    var confirmDisapprove = function () {
        $(document).on('click', '.disapprove_accounting_entries', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "Disapprove entry " + id + "?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Disapprove entry!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'accounting_entries/disapprove_entry/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                get_accounting_entry_information($('#filter_start_date').val(), $('#filter_end_date').val());
                                $('#accounting_entries_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Disapproved!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Accounting Entry ' + id + ' not disapproved',
                        'error'
                    );
                }
            });
        });
    };

    var filterDateChange = function () {
        const filter_start = $('#filter_start_date');
        const filter_end = $('#filter_end_date');

        filter_start.on("change", function () {
            if (!filter_end.val() == "") {
                if (new Date(filter_end.val()) <= new Date(filter_start.val())) {
                    filter_end.val(filter_start.val());
                    get_accounting_entry_information(filter_start.val(), filter_start.val());
                } else {
                    get_accounting_entry_information(filter_start.val(), filter_end.val());
                }
            }
        });

        filter_end.on("change", function () {
            if (!filter_end.val() == "") {
                if (new Date(filter_end.val()) <= new Date(filter_start.val())) {
                    filter_start.val(filter_end.val());
                    get_accounting_entry_information(filter_start.val(), filter_start.val());
                } else {
                    get_accounting_entry_information(filter_start.val(), filter_end.val());
                }
            }
        });

    };

    var get_accounting_entry_information = function (d, e) {


        if (typeof e === "undefined" && typeof d === "undefined") {
            e = '';
            d = '';
        };

        $.ajax({
            url: base_url + "accounting_entries/get_accounting_entry_information",
            type: "POST",
            dataType: "JSON",
            data: { start: d, end: e },
            success: function (res) {
                $("#pending_count").html(res.pending_count);
                $("#pending_amount").html(res.pending_amount);
                $("#approved_count").html(res.approved_count);
                $("#approved_amount").html(res.approved_amount);
                $("#disapproved_count").html(res.disapproved_count);
                $("#disapproved_amount").html(res.disapproved_amount);
            },
        });

    };

    return {
        //main function to initiate the module
        init: function () {
            confirmApprove();
            confirmDelete();
            AccountingEntriesTable();
            _add_ons();
            datepicker();
            daterangepickerInit();
            filter();
            upload_guide();
            status();
            _selectProp();
            filterDateChange();
            get_accounting_entry_information();
            confirmDisapprove();
        },
    };
})();

jQuery(document).ready(function () {
    AccountingEntries.init();
});

// jQuery(document).on('change', 'select#payee_type', function(e) {
//     e.preventDefault();
//     var payeeTypeValue = $("#payee_type option:selected").val();
//
//     if (payeeTypeValue == "sellers" || payeeTypeValue == "buyers" || payeeTypeValue == "staff") {
//         document.getElementById("payee_type_id").setAttribute("data-type", "person");
//     } else {
//         document.getElementById("payee_type_id").setAttribute("data-type", "");
//     }
//
//     document.getElementById("payee_type_id").setAttribute("data-module", payeeTypeValue);
//
// });

const payeeType = () => {
    $('#_column_7').on('change', function () {
        var e = document.getElementById('_column_7');
        var payeeTypeValue = e.options[e.selectedIndex].value;

        if (
            payeeTypeValue === 'sellers' ||
            payeeTypeValue === 'buyers' ||
            payeeTypeValue === 'staff'
        ) {
            document
                .getElementById('_column_8')
                .setAttribute('data-type', 'person');
        } else {
            document.getElementById('_column_8').setAttribute('data-type', '');
        }

        document
            .getElementById('_column_8')
            .setAttribute('data-module', payeeTypeValue);
    });
};

payeeType();
