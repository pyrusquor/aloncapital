var KTFormControls = (function () {
    // Private functions

    var valAccountingEntries = function () {
        $("#accounting_entries_form").validate({
            // define validation rules
            rules: {
                or_number: {
                    required: true,
                },
                invoice_number: {
                    required: true,
                },
                journal_type: {
                    required: true,
                },
                payment_date: {
                    required: true,
                },
                payee_type: {
                    required: true,
                },
                payee_type_id: {
                    required: true,
                },
                remarks: {
                    required: true,
                },
                cr_total: {
                    required: true,
                    isEqual: "#dr_total",
                },
                dr_total: {
                    required: true,
                },
                entry_item: { required: true },
                ledger_id: { required: true },
                amount: { required: true },
                dc: { required: true },
                is_reconciled: { required: true },
                description: { required: true },
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                toastr.error(
                    "Please check your fields",
                    "Something went wrong"
                );

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });

        //add a custom validator to check if the debit and credit values are equal.
        jQuery.validator.addMethod("isEqual", function (value, element, param) {
            return this.optional(element) || value == $(param).val();
        }, "");
    };

    var formRepeater = function () {
        $("#entry_item_form_repeater").repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                datepicker();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
        });
    };

    return {
        // public functions
        init: function () {
            formRepeater();
            valAccountingEntries();
        },
    };
})();

$(".kt_datepicker").datepicker({
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true,
    templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>',
    },
    format: "yyyy-mm-dd",
});

const payeeTypeSelect = document.querySelector("#payee_value");
const payeeTypeLabel = document.querySelector("#payee_type_id_label");
const payeeTypeIdSelect = document.querySelector("#payee_type_id_select");
const addPayeeBtn = document.querySelector("#add_payee_btn");
const dTotalInput = document.querySelector("#dTotal");
const cTotalInput = document.querySelector("#cTotal");

const getPayeeSuggests = (id) => {
    if (id == 1) {
        $.ajax({
            url: base_url + "accounting_entries/get_all_buyers",
            type: "GET",
            success: function (data) {
                const payee = JSON.parse(data);
                payee.data.map((buyer) => {
                    let opt = document.createElement("option");

                    const full_name = `${buyer.first_name} ${buyer.last_name}`;
                    opt.value = buyer.id;
                    opt.innerHTML = full_name;
                    payeeTypeIdSelect.appendChild(opt);
                });
            },
        });
    }
    if (id == 2) {
        $.ajax({
            url: base_url + "accounting_entries/get_all_sellers",
            type: "GET",
            success: function (data) {
                const payee = JSON.parse(data);
                payee.data.map((seller) => {
                    let opt = document.createElement("option");

                    const full_name = `${seller.first_name} ${seller.last_name}`;
                    opt.value = seller.id;
                    opt.innerHTML = full_name;
                    payeeTypeIdSelect.appendChild(opt);
                });
            },
        });
    }
};

// Watch for payee type change
$(payeeTypeSelect).on("change", function () {
    if (this.value != 0) {
        getPayeeSuggests(this.value);
        payeeTypeLabel.innerHTML = this.options[this.selectedIndex].text;
        payeeTypeIdSelect.disabled = false;
        payeeTypeIdSelect.innerHTML = "";
        if (this.value == 1) {
            addPayeeBtn.href = base_url + "buyer/create";
        } else {
            addPayeeBtn.href = base_url + "seller/create";
        }
    } else {
        payeeTypeIdSelect.disabled = true;
        addPayeeBtn.href = "javascript:void(0)";
    }
});

const paymentType = () => {
    const entryItemDcSelect = document.querySelectorAll("#entry_item_dc");
    const dInput = document.querySelectorAll("#dr_amount_input");
    const cInput = document.querySelectorAll("#cr_amount_input");

    entryItemDcSelect.forEach((select, index) => {
        if (select.dataset.type) {
            select.value = select.dataset.type;
        }
        if (dInput[index].value == "") {
            dInput[index].disabled = true;
        }
        if (cInput[index].value == "") {
            cInput[index].disabled = true;
        }
        $(select).on("change", function () {
            if (this.value !== "") {
                if (this.value == "d") {
                    cInput[index].disabled = true;
                    cInput[index].value = "";
                    dInput[index].disabled = false;
                } else if (this.value == "c") {
                    cInput[index].disabled = false;
                    dInput[index].disabled = true;
                    dInput[index].value = "";
                }
            }
        });
    });
};

const calculateDTotal = () => {
    const dInput = document.querySelectorAll("#dr_amount_input");
    // dTotalInput.value = 0;
    $(dInput).change(function () {
        var total = 0;

        $(dInput).each(function () {
            total += Number($(this).val().trim() || 0);
        });
        dTotalInput.value = total.toFixed(2);
    });
};
const calculateCTotal = () => {
    const cInput = document.querySelectorAll("#cr_amount_input");
    // cTotalInput.value = 0;
    $(cInput).change(function () {
        var total = 0;

        $(cInput).each(function () {
            total += Number($(this).val().trim() || 0);
        });
        cTotalInput.value = total.toFixed(2);
    });
};


const getLedgers = () => {
    // Get the element again
    const entryItemLedgerSelect = document.querySelectorAll(
        "#entry_item_ledger"
    );
    // Loop through the element
    entryItemLedgerSelect.forEach((select) => {
        $.ajax({
            url: base_url + "accounting_entries/get_all_ledgers",
            type: "GET",
            success: function (data) {
                const ledgers = JSON.parse(data);
                ledgers.data.map((ledger) => {
                    let opt = document.createElement("option");

                    opt.value = ledger.id;
                    opt.innerHTML = ledger.name;
                    select.appendChild(opt);
                });
            },
        });
        if (select.dataset.ledger) {
            setTimeout(() => {
                select.value = select.dataset.ledger;
            }, 1000);
        }
    });
};

var formRepeater = function () {
    $("#entry_item_form_repeater").repeater({
        initEmpty: false,

        defaultValues: {},

        show: function () {
            $(this).slideDown();
            datepicker();
            getLedgers();
            paymentType();
            calculateDTotal();
            calculateCTotal();
        },

        hide: function (deleteElement) {
            $(this).slideUp(deleteElement);
            getLedgers();
        },
    });
};

getLedgers();
paymentType();
calculateDTotal();
calculateCTotal();
