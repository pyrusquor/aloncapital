// Class definition
var Form = (function () {
  var arrows;

  if (KTUtil.isRTL()) {
    arrows = {
      leftArrow: '<i class="la la-angle-right"></i>',
      rightArrow: '<i class="la la-angle-left"></i>',
    };
  } else {
    arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>',
    };
  }

  // Private functions
  var datepicker = function () {
    // minimum setup
    $(".kt_datepicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      orientation: "bottom left",
      templates: arrows,
      locale: "no",
      format: "yyyy-mm-dd",
      autoclose: true,
    });

    $(".yearPicker").datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true,
    });
  };

  var formEl;
  var validator;

  var initValidation = function () {
    validator = formEl.validate({
      // Validate only visible fields
      ignore: ":hidden",

      // Validation rules
      rules: {
        name: {
          required: true,
        },
      },
      // messages: {
      // 	"info[last_name]":'Last name field is required',
      // },
      // Display error
      invalidHandler: function (event, validator) {
        KTUtil.scrollTop();

        swal.fire({
          title: "",
          text:
            "There are some errors in your submission. Please correct them.",
          type: "error",
          confirmButtonClass: "btn btn-secondary",
        });
      },

      // Submit valid form
      submitHandler: function (form) {},
    });
  };

  var initSubmit = function () {
    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

    btn.on("click", function (e) {
      e.preventDefault();

      if (validator.form()) {
        // See: src\js\framework\base\app.js
        KTApp.progress(btn);
        //KTApp.block(formEl);

        // See: http://malsup.com/jquery/form/#ajaxSubmit
        formEl.ajaxSubmit({
          type: "POSt",
          dataType: "JSON",
          success: function (response) {
            if (response.status) {
              swal
                .fire({
                  title: "Success!",
                  text: response.message,
                  type: "success",
                })
                .then(function () {
                  window.location.replace(base_url + "customer_service");
                });
            } else {
              swal.fire({
                title: "Oops!",
                html: response.message,
                icon: "error",
              });
            }
          },
        });
      }
    });
  };

  var _add_ons = function () {
    datepicker();

    $("#turn_over_date").on('change', function () {

      turn_over_date = new Date($(this).val() + "T00:00:00");

      let warranty_date = new Date(turn_over_date.setDate(turn_over_date.getDate() + 180));

      $("#warranty_date").val(warranty_date.toISOString().split('T')[0])
    })

    $("#property_id").on('change', function() {
      
      let property_id = $(this).val()

      $.ajax({
        url: base_url + "generic_api/fetch_specific?table=properties&field=id&value=" + property_id + "&select[]=id&select[]=phase&select[]=block&select[]=lot&select[]=turn_over_date",
        type: "POST",
        success: function (response) {
          if(response){
            try{
              $('#phase').val(response[0]['phase'])
              $('#block').val(response[0]['block'])
              $('#lot').val(response[0]['lot'])
              $('#turn_over_date').val(response[0]['turn_over_date']?.substring(0,10))
            }
            catch{

            }
          }
        },
      });
    })
  };

  var buyerName = function (){
    var firstNameElement = $('#first_name')
    var lastNameElement = $('#last_name')
    var emailElement = $('#email')
    var projectElement = '#project_id'
    var propertyElement = '#property_id'
    var houseIdElement = '#house_model_id'
    $('.select_buyer').on('select2:select', function(e){
      var id = e.params.data['id'];
      $.ajax({
        url: base_url + 'generic_api/fetch_specific',
        type: 'GET',
        dataType: 'JSON',
        data: {
          select: 'first_name, last_name, user_id',
          table: 'buyers',
          field: 'id',
          value: id
        },
        success: function(res){
          if(res){
            firstNameElement.val(res[0].first_name)
            lastNameElement.val(res[0].last_name)
            $.ajax({
              url: base_url + 'generic_api/fetch_specific',
              type: 'GET',
              dataType: 'JSON',
              data: {
                select: 'email',
                table: 'users',
                field: 'id',
                value: res[0].user_id
              },
              success: function(res){
                if(res){
                  emailElement.val(res[0].email)
                }
              }
            })
            var transactions = checkTransactions(id)
            if(transactions.length == 1){
              addOptions(transactions[0].project_id, 'projects', projectElement)
              addOptions(transactions[0].property_id, 'properties', propertyElement)

              $.ajax({
                url: base_url + 'generic_api/fetch_specific',
                type: 'GET',
                dataType: 'JSON',
                data: {
                  select: 'model_id',
                  table: 'properties',
                  field: 'id',
                  value: transactions[0].property_id
                },
                success: function(res){
                  if(res){
                    addOptions(res[0].model_id,'house_models', houseIdElement)
                  }
                }
              })
            }
          }
        }
      })
      firstNameElement.prop('readonly', true);
      lastNameElement.prop('readonly', true);
    })
    $('.select_buyer').on('select2:clear', function(e){
      firstNameElement.val('');
      lastNameElement.val('');
      emailElement.val('');
      if($(propertyElement).val() || $(projectElement).val()){
        location.reload()
        $(projectElement).select2().val(null).trigger('change')
        $(propertyElement).select2().val(null).trigger('change')
        $(houseIdElement).select2().val(null).trigger('change')
        $('#block').val('')
        $('#lot').val('')
      }
      firstNameElement.prop('readonly', false);
      lastNameElement.prop('readonly', false);
    })
  }

  var checkTransactions = function(userId) {
    var result = 0;
    $.ajax({
      url: base_url + 'generic_api/fetch_specific',
      type: 'GET',
      dataType: 'JSON',
      async: false,
      data: {
        select: 'project_id, property_id',
        table: 'transactions',
        field: 'buyer_id',
        value: userId
      },
      success: function(res){
        if(res){
          result = res
        }
      }
    })
    return result
  }

  var addOptions = function (data, type, identifier){
    var element = $(identifier);
    $.ajax({
        url: base_url + 'generic_api/fetch_specific',
        dataType: 'JSON',
        data: {
            table: type,
            field: 'id',
            value: data,
            limit: '1',
            select: 'name'
        },
        async:false,
        success: function (res){
          var element_option = new Option(res[0]['name'], data);
          element.append(element_option);
        }
    })
    element.val(null).trigger('change');
    element.val(data).trigger('change');

}

  // Public functions
  return {
    init: function () {
      datepicker();
      _add_ons();

      formEl = $("#form_customer_service");

      initValidation();
      initSubmit();
      buyerName();
      // checkTransactions();
    },
  };
})();

// Initialization
jQuery(document).ready(function () {
  Form.init();
});

const inquiryCategorySelect = document.querySelector("#inquiry_category_id");
const inquirySubCategory = document.querySelector("#inquiry_sub_category_id");

// const getInquiryCategories = () => {
//   $.ajax({
//     url: base_url + "inquiry_categories/get_all",
//     type: "GET",
//     success: function (data) {
//       const categories = JSON.parse(data);
//       categories.data.map((category) => {
//         let opt = document.createElement("option");

//         opt.value = category.id;
//         opt.innerHTML = category.name;
//         inquiryCategorySelect.appendChild(opt);
//       });
//     },
//   });
// };

// const setInquirySubCategory = () => {
//   $("#inquiry_category_id").on("change", function () {
//     inquiryCategoryID = $(this).val();
//     $.ajax({
//       url:
//         base_url + "inquiry_sub_categories/inquiry_sub_categories_by_category",
//       type: "POST",
//       data: {
//         inquiryCategoryID,
//         inquiryCategoryID,
//       },
//       success: function (data) {
//         const sub_categories = JSON.parse(data);
//         inquirySubCategory.innerHTML = "";
//         try {
//           sub_categories.map((sub_category) => {
//             let opt = document.createElement("option");

//             opt.value = sub_category.id;
//             opt.innerHTML = sub_category.name;
//             inquirySubCategory.appendChild(opt);
//           });
//         } catch (error) {
//           let opt = document.createElement("option");

//           inquirySubCategory.innerHTML = "";
//           opt.value = "";
//           opt.innerHTML = "No record found";
//           inquirySubCategory.appendChild(opt);
//         }
//       },
//       error: function (xhr, ajaxOptions, thrownError) {
//         console.log(
//           thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText
//         );
//         inquirySubCategory.removeChild();
//       },
//     });
//   });
// };

// getInquiryCategories();
// setInquirySubCategory();
