function setInquiryTicketID(id, ref_no, status = false) {

    $("#ticket_number").html(ref_no);
    $("#ticket_number_status").html(ref_no);

    document.getElementById("forward_status_id").value = id;
    document.getElementById("customer_service_id").value = id;
    document.getElementById("status_id").value = status;
    // document.getElementById("updateStatusModalLabel").innerHTML = label;
}

function setForwardStatus(id,statusId, status = false) {

    swal.fire({
        title: "Confirmation",
        text: "Set forward status to " + status + "?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, change it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      }).then(function (result) {
        if (result.value) {
          $.ajax({
            url: base_url + "customer_service/change_forward_status/",
            type: "POST",
            dataType: "JSON",
            data: {
              id: id,
              forwarded_status: statusId
            },
            success: function (res) {
              if (res.status) {
                swal.fire("Ticket set to " + status + "!", res.message, "success");
                location.reload()
              } else {
                swal.fire("Oops!", res.message, "error");
              }
            },
          });
        }
      });
}

var customerServiceView = (function (){
    var forwardStatus = function () {
        function forward(customer_service_id, category_id) {
          var customer_service_id = customer_service_id ? customer_service_id : "";
          var status_id = 2;
          var category_id = category_id ? category_id : "";
    
          $.ajax({
            url: base_url + "customer_service/forward_status",
            method: "POST",
            dataType: "JSON",
            data:
              "customer_service_id=" +
              customer_service_id +
              "&status_id=" +
              status_id +
              "&category_id=" +
              category_id,
            success: function (res) {
              $("#forwardStatusModal").modal("hide");
    
              if (res.status) {
                swal.fire("Logged!", res.message, "success");
                location.reload()
              } else {
                swal.fire("Oops!", res.message, "error");
              }
            },
          });
        }
    
        $("#forwardStatus").submit(function (e) {
          e.preventDefault();
          var customer_service_id = $("#forward_status_id").val();
          var category_id = $("#category_id").val();
    
          if (customer_service_id !== "" || category_id !== "") {
            forward(customer_service_id, category_id);
          }
        });
      };
    
      var updateStatus = function () {
        function update(customer_service_id, status_id, remarks) {
          var customer_service_id = customer_service_id ? customer_service_id : "";
          var status_id = status_id ? status_id : "";
          var remarks = remarks ? remarks : "";
    
          $.ajax({
            url: base_url + "customer_service/update_status",
            method: "POST",
            dataType: "JSON",
            data:
              "customer_service_id=" +
              customer_service_id +
              "&status_id=" +
              status_id +
              "&remarks=" +
              remarks,
            success: function (res) {
              $("#updateStatusModal").modal("hide");
    
              if (res.status) {
                swal.fire("Logged!", res.message, "success")
                location.reload()
              } else {
                swal.fire("Oops!", res.message, "error")
              }
            },
          });
        }
    
        $("#updateStatus").submit(function (e) {
          e.preventDefault();
          var customer_service_id = $("#customer_service_id").val();
          var status_id = $("#status_id").val();
          var remarks = $("#remarks").val();
    
          if (customer_service_id !== "" || remarks !== "") {
            update(customer_service_id, status_id, remarks);
          }
        });
      };
    return{
        init: function(){
            forwardStatus()
            updateStatus()
        }
    }
})()
jQuery(document).ready(function () {
    customerServiceView.init();
  });