"use strict";
var InquiryTicket = (function () {

  let tableElement = $('#customer_service_table');

  var customerServiceTable = function () {

    var dataTable = tableElement.DataTable({
      order: [
        [1, 'desc']
      ],
      pagingType: "full_numbers",
      lengthMenu: [5, 10, 25, 50, 100],
      responsive: true,
      pageLength: 10,
      searchDelay: 500,
      processing: true,
      serverSide: true,
      serverMethod: "post",
      deferRender: true,
      ajax: {
        data: function (data) {
          getFilter(data);
        },
        url: base_url + 'customer_service/showItems',
      },
      dom: "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
        // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      buttons: [{
        text: '<i class="la la-trash"></i> Delete Selected',
        className: "btn btn-sm btn-label-primary btn-elevate btn-icon-sm",
        attr: {
          id: "bulkDelete",
        },
        enabled: false,
      },],
      language: {
        lengthMenu: "Show _MENU_",
        infoFiltered: "(filtered from _MAX_ total records)",
      },
      columns: [{
        data: null,
      },
      {
        data: "id",
      },
      {
        data: "reference",
      },
      {
        data: "project_id",
      },
      {
        data: "property_id",
      },
      {
        data: "house_model_id",
      },
      {
        data: "buyer_id",
      },
      {
        data: "category_id",
      },
      {
        data: "department_id",
      },
      {
        data: "created_at",
      },
      {
        data: "updated_at",
      },
      {
        data: "notes",
      },
      {
        data: "status",
      },
      {
        data: "sub_status",
      },
      {
        data: "created_by",
      },
      {
        data: "updated_by",
      },
      {
        data: "Actions",
        responsivePriority: -1,
      },
      ],
      columnDefs: [
        {
        targets: -1,
        title: "Actions",
        searchable: false,
        orderable: false,
        render: function (data, type, row, meta) {
          let close_action = row.status_id == 1 && (row.sub_status_id == 3 || row.forwarded_status == 2) ? `<a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="closed"  data-id="` + row.id + `" data-status="6" onclick="setInquiryTicketID(` + row.id + `,  '` + row.ref + `', 2, false )" class="dropdown-item">Closed</a>` : ''
          return (
            `
                                <span class="dropdown">
    
                                  <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                                  <i class="la la-ellipsis-h"></i>
                                  </a>
                                  <div class="dropdown-menu dropdown-menu-right">
    
                                  <a href="javascript:void(0);" class="dropdown-item"  data-toggle="modal" data-target="#updateStatusModal" onclick="setInquiryTicketID(` + row.id + `, '` + row.ref + `', 1 )">Ongoing</a>
    
                                  <a href="javascript:void(0);" data-toggle="modal" data-target="#forwardStatusModal" id="work_in_progress"  data-id="` + row.id + `" data-status="3" onclick="setInquiryTicketID(` + row.id + `,  '` + row.ref + `', 2 )" class="dropdown-item">Forwarded</a>
    
                                  <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="on_hold"  data-id="` + row.id + `" data-status="4" onclick="setInquiryTicketID(` + row.id + `,  '` + row.ref + `', 3 )" class="dropdown-item">Completed</a>` +

            close_action

            + `<a href="javascript:void(0);" data-toggle="modal" data-target="#updateNotesModal" id="on_hold"  data-id="` + row.id + `" data-status="4" onclick="setNotes(` + row.id + `)" class="dropdown-item"><i class="fas fa-sticky-note"></i>Update Notes</a>`

            + `<a class="dropdown-item" href="` + base_url + `customer_service/form?ticket_id=` + row.id +
            `"><i class="la la-plus"></i> Add Related Complaint </a>
                                                    
                                  <a class="dropdown-item" href="` + base_url + `customer_service/form/` + row.id +
            `"><i class="la la-edit"></i> Update </a>
    
                                  <a href="javascript:void(0);" class="dropdown-item remove_ticket" data-id="` + row.id + `"><i class="la la-trash"></i> Delete </a>
    
                                  </div>
                                </span>
                                
                                <a href="` + base_url + `customer_service/view/` + row.id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                  <i class="la la-eye"></i>
                                </a>`
          );
        },
      },
      {
        targets: [0, 1, 3, 4, 5, -1],
        className: "dt-center",
      },
      {
        targets: 0,
        render: function (data, type, row, meta) {
          return (
            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
            row.id +
            `" class="m-checkable delete_check" data-id="` +
            row.id +
            `">
                      <span></span>
	                  </label>`
          );
        },
        orderable: false,
        searchable: false,
      },

      /* ==================== begin: Add target fields for dropdown value ==================== */
      // {
      //   targets: 11,
      //   render: function (data, type, row, meta) {
      //     var inquirtyTicket = {
      //       0: {
      //         title: 'No',
      //       },
      //       1: {
      //         title: 'Yes',
      //       },
      //     };

      //     if (typeof inquirtyTicket[data] === 'undefined') {
      //       return ``;
      //     }
      //     return inquirtyTicket[data].title;
      //   },
      // },

      //   {
      //     targets: 17,
      //     render: function (data, type, row, meta) {
      //         var inquirtyTicket = {
      //             1: {
      //                 title: 'New Request',
      //             },
      //             2: {
      //                 title: 'Forwarded',
      //             },
      //             3: {
      //                 title: 'Work in progress',
      //             },
      //             4: {
      //                 title: 'On Hold',
      //             },
      //             5: {
      //                 title: 'Completed',
      //             },
      //             6: {
      //                 title: 'Closed',
      //             },
      //             7: {
      //                 title: 'Cancelled',
      //             },
      //         };

      //         if (typeof inquirtyTicket[data] === 'undefined') {
      //             return ``;
      //         }
      //         return inquirtyTicket[data].title;
      //     },
      // },


      // {
      //   targets: [8, 12, 13],
      //   visible: false,
      // },
        /* ==================== end: Add target fields for dropdown value ==================== */
      ],
      drawCallback: function (settings) {
        $("#total").text(settings.fnRecordsTotal() + " TOTAL");
      },
    });

    function getFilter(data) {

      data.filter = $('#advance_search').serialize();
    }

    $('#advance_search').submit(function (e) {

      e.preventDefault();
      dataTable.ajax.reload()
    })
  };

  var confirmDelete = function () {
    $(document).on("click", ".remove_ticket", function () {
      var id = $(this).data("id");

      swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      }).then(function (result) {
        if (result.value) {
          $.ajax({
            url: base_url + "customer_service/delete/" + id,
            type: "POST",
            dataType: "JSON",
            data: {
              id: id
            },
            success: function (res) {
              if (res.status) {
                $("#customer_service_table").DataTable().ajax.reload();
                swal.fire("Deleted!", res.message, "success");
              } else {
                swal.fire("Oops!", res.message, "error");
              }
            },
          });
        }
      });
    });
  };

  var upload_guide = function () {
    $(document).on("click", "#btn_upload_guide", function () {
      var table = $("#upload_guide_table");

      table.DataTable({
        order: [
          [0, "asc"]
        ],
        pagingType: "full_numbers",
        lengthMenu: [5, 10, 25, 50, 100],
        pageLength: 10,
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        deferRender: true,
        ajax: base_url + "customer_service/get_table_schema",
        columns: [
          // {data: 'button'},
          {
            data: "no"
          },
          {
            data: "name"
          },
          {
            data: "type"
          },
          {
            data: "format"
          },
          {
            data: "option"
          },
          {
            data: "required"
          },
        ],
        // drawCallback: function ( settings ) {

        // }
      });
    });
  };

  var filter = function () {
    $("#generalSearch").keyup(function (e) {
      e.preventDefault();
      let code = e.key; // recommended to use e.key, it's normalized across devices and languages
      if (code === "Enter") {
        tableElement
          .DataTable()
          .search($(this)
            .val())
          .draw();
      }
    });

    $('._filter').on('keyup change clear', function () {

      processChange()
    })

    const processChange = debounce(() => submitInput());

    function debounce(func, timeout = 500) {
      let timer;
      return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
      };
    }

    function submitInput() {
      $('#advance_search').submit()
    }
  };

  var _selectProp = function () {
    var _table = $("#customer_service_table").DataTable();
    var _buttons = _table.buttons([".bulkDelete"]);

    $("#select-all").on("click", function () {
      // var rows = _table.rows({ 'search': 'applied' }).nodes();

      // $('input[type="checkbox"]').prop( 'checked', this.checked );
      if ($(this).is(":checked")) {
        $(".delete_check").prop("checked", true);
      } else {
        $(".delete_check").prop("checked", false);
      }
    });

    $("#customer_service_table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          var el = $("input#select-all").get(0);

          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
      }
    );

    $(document).on("change", 'input[name="id[]"]', function () {
      var _checked = $('input[name="id[]"]:checked').length;

      if (_checked < 0) {
        _table.button(0).disable();
      } else {
        _table.button(0).enable(_checked > 0);
      }
    });

    $("#bulkDelete").click(function () {
      var deleteids_arr = [];
      // Read all checked checkboxes
      $('input[name="id[]"]:checked').each(function () {
        deleteids_arr.push($(this).val());
      });

      // Check checkbox checked or not
      if (deleteids_arr.length > 0) {
        swal.fire({
          title: "Are you sure?",
          text: "You won't be able to revert this!",
          type: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel!",
          reverseButtons: true,
        }).then(function (result) {
          if (result.value) {
            $.ajax({
              url: base_url + "customer_service/bulkDelete",
              type: "POST",
              dataType: "JSON",
              data: {
                deleteids_arr: deleteids_arr
              },
              success: function (res) {
                if (res.status) {
                  $("#customer_service_table")
                    .DataTable()
                    .ajax.reload();
                  swal.fire(
                    "Deleted!",
                    res.message,
                    "success"
                  );
                } else {
                  swal.fire("Oops!", res.message, "error");
                }
              },
            });
          }
        });
      }
    });
  };

  var _add_ons = function () {
    $("#_export_select_all").on("click", function () {
      if (this.checked) {
        $("._export_column").each(function () {
          this.checked = true;
        });
      } else {
        $("._export_column").each(function () {
          this.checked = false;
        });
      }
    });

    $("._export_column").on("click", function () {
      if (this.checked == false) {
        $("#_export_select_all").prop("checked", false);
      }
    });

    $("#import_status").on("change", function (e) {
      var doc_type = $(this).val();

      if (doc_type != "") {
        KTApp.block("#kt_content", {
          overlayColor: "#000000",
          type: "v2",
          state: "primary",
          message: "Processing...",
          css: {
            padding: 0,
            margin: 0,
            width: "30%",
            top: "40%",
            left: "35%",
            textAlign: "center",
            color: "#000",
            border: "3px solid #aaa",
            backgroundColor: "#fff",
            cursor: "wait",
          },
        });

        setTimeout(function () {
          $("#_batch_upload button").removeAttr("disabled");
          $("#_batch_upload button").removeClass("disabled");

          KTApp.unblock("#kt_content");
        }, 500);
      } else {
        KTApp.block("#kt_content", {
          overlayColor: "#000000",
          type: "v2",
          state: "primary",
          message: "Processing...",
          css: {
            padding: 0,
            margin: 0,
            width: "30%",
            top: "40%",
            left: "35%",
            textAlign: "center",
            color: "#000",
            border: "3px solid #aaa",
            backgroundColor: "#fff",
            cursor: "wait",
          },
        });

        setTimeout(function () {
          $("#_batch_upload button").attr("disabled", "disabled");
          $("#_batch_upload button").addClass("disabled");

          KTApp.unblock("#kt_content");
        }, 500);
      }
    });
  };

  var advanceFilter = function () {
    function filter(
      project_id,
      property_id,
      house_model_id,
      buyer_id,
      turn_over_date,
      complaint_category_id,
      complaint_sub_category_id
    ) {
      var page_num = page_num ? page_num : 0;
      var project_id = project_id ? project_id : "";
      var property_id = property_id ? property_id : "";
      var buyer_id = buyer_id ? buyer_id : "";
      var house_model_id = house_model_id ? house_model_id : "";
      var turn_over_date = turn_over_date ? turn_over_date : "";
      var complaint_category_id = complaint_category_id
        ? complaint_category_id
        : "";
      var complaint_sub_category_id = complaint_sub_category_id
        ? complaint_sub_category_id
        : "";

      $.ajax({
        url: base_url + "customer_service/paginationData/" + page_num,
        method: "POST",
        data:
          "page=" +
          page_num +
          "&project_id=" +
          project_id +
          "&property_id=" +
          property_id +
          "&buyer_id=" +
          buyer_id +
          "&house_model_id=" +
          house_model_id +
          "&turn_over_date=" +
          turn_over_date +
          "&complaint_category_id=" +
          complaint_category_id +
          "&complaint_sub_category_id=" +
          complaint_sub_category_id,
        success: function (html) {
          $("#filterModal").modal("hide");

          KTApp.block("#kt_content", {
            overlayColor: "#000000",
            type: "v2",
            state: "primary",
            message: "Processing...",
            css: {
              padding: 0,
              margin: 0,
              width: "30%",
              top: "40%",
              left: "35%",
              textAlign: "center",
              color: "#000",
              border: "3px solid #aaa",
              backgroundColor: "#fff",
              cursor: "wait",
            },
          });

          setTimeout(function () {
            $("#inquiry_ticketsContent").html(html);

            KTApp.unblock("#kt_content");
          }, 500);
        },
      });
    }

    $("#advanceSearch").submit(function (e) {
      e.preventDefault();
      // var form_values = $('#advanceSearch').serialize();
      var project_id = $("#project_id").val();
      var property_id = $("#property_id").val();
      var house_model_id = $("#house_model_id").val();
      var buyer_id = $("#filterBuyer").val();
      var turn_over_date = $("#filterTurnOverDate").val();
      var complaint_category_id = $("#complaint_category_id").val();
      var complaint_sub_category_id = $("#filterComplaintSubCategory").val();

      if (
        project_id !== "" ||
        property_id !== "" ||
        house_model_id !== "" ||
        buyer_id !== "" ||
        turn_over_date !== "" ||
        complaint_category_id !== "" ||
        complaint_sub_category_id !== ""
      ) {
        filter(
          project_id,
          property_id,
          house_model_id,
          buyer_id,
          turn_over_date,
          complaint_category_id,
          complaint_sub_category_id
        );
      } else {
        filter();
      }
    });
  };

  var forwardStatus = function () {
    function forward(customer_service_id, category_id) {
      var customer_service_id = customer_service_id ? customer_service_id : "";
      var status_id = 2;
      var category_id = category_id ? category_id : "";

      $.ajax({
        url: base_url + "customer_service/forward_status",
        method: "POST",
        dataType: "JSON",
        data:
          "customer_service_id=" +
          customer_service_id +
          "&status_id=" +
          status_id +
          "&category_id=" +
          category_id,
        success: function (res) {
          $("#forwardStatusModal").modal("hide");

          if (res.status) {
            swal.fire("Logged!", res.message, "success");
          } else {
            swal.fire("Oops!", res.message, "error");
          }

          $("#customer_service_table").DataTable().ajax.reload();
        },
      });
    }

    $("#forwardStatus").submit(function (e) {
      e.preventDefault();
      var customer_service_id = $("#forward_status_id").val();
      var category_id = $("#category_id").val();

      if (customer_service_id !== "" || category_id !== "") {
        forward(customer_service_id, category_id);
      }
    });
  };

  var updateStatus = function () {
    function update(customer_service_id, status_id, remarks) {
      var customer_service_id = customer_service_id ? customer_service_id : "";
      var status_id = status_id ? status_id : "";
      var remarks = remarks ? remarks : "";

      $.ajax({
        url: base_url + "customer_service/update_status",
        method: "POST",
        dataType: "JSON",
        data:
          "customer_service_id=" +
          customer_service_id +
          "&status_id=" +
          status_id +
          "&remarks=" +
          remarks,
        success: function (res) {
          $("#updateStatusModal").modal("hide");

          if (res.status) {
            swal.fire("Logged!", res.message, "success")
          } else {
            swal.fire("Oops!", res.message, "error")
          }

          $("#customer_service_table").DataTable().ajax.reload();
        },
      });
    }

    $("#updateStatus").submit(function (e) {
      e.preventDefault();
      var customer_service_id = $("#customer_service_id").val();
      var status_id = $("#status_id").val();
      var remarks = $("#remarks").val();

      if (customer_service_id !== "" || remarks !== "") {
        update(customer_service_id, status_id, remarks);
      }
    });
  };

  var updateNotes = function () {

    $("#updateNotes").submit(function (e) {
      e.preventDefault();
      var customer_service_id = $("#customer_service_id").val();
      var notes = $("#notes").val();
      console.log(customer_service_id, notes)
      if (customer_service_id !== "") {
        $.ajax({
          url: base_url + "customer_service/update_notes",
          method: "POST",
          dataType: "JSON",
          data:{
            customer_service_id: customer_service_id,
            notes: notes
          },
          success: function (res) {
            $("#updateNotesModal").modal("hide");

            if (res.status) {
              swal.fire("Updated!", res.message, "success")
            } else {
              swal.fire("Oops!", res.message, "error")
            }

            $("#customer_service_table").DataTable().ajax.reload();
          },
        });
      }
    });
  };

  var closeStatus = function () {
    function close_ticket(customer_service_id, status_id, remarks, image) {
      var customer_service_id = customer_service_id ? customer_service_id : "";
      var status_id = status_id ? status_id : "";
      var remarks = remarks ? remarks : "";
      var image = image ? image : "";

      $.ajax({
        url: base_url + "customer_service/update_status",
        method: "POST",
        dataType: "JSON",
        data:
          "customer_service_id=" +
          customer_service_id +
          "&status_id=" +
          status_id +
          "&remarks=" +
          remarks +
          "&image=" +
          image,
        success: function (res) {
          $("#closeStatusModal").modal("hide");

          if (res.status) {
            swal.fire("Logged!", res.message, "success");
          } else {
            swal.fire("Oops!", res.message, "error");
          }

          $("#customer_service_table").DataTable().ajax.reload();
        },
      });
    }

    $("#closeStatus").submit(function (e) {
      e.preventDefault();
      var customer_service_id = $("#customer_service_id").val();
      var status_id = $("#status_id").val();
      var remarks = $("#remarks").val();
      var image = $("#image").val();

      if ((customer_service_id !== "" && remarks !== "") || image !== "") {
        close_ticket(customer_service_id, status_id, remarks, image);
      }
    });
  };

  // Daterangepicker Init
  var daterangepickerInit = function () {
    if ($(".kt_transaction_daterangepicker").length == 0) {
      return;
    }

    var picker = $(".kt_transaction_daterangepicker");
    var start = moment();
    var end = moment();

    function cb(start, end, label) {
      var title = "";
      var range = "";

      if (end - start < 100 || label == "Today") {
        title = "Today:";
        range = start.format("MMM D");
      } else if (label == "Yesterday") {
        title = "Yesterday:";
        range = start.format("MMM D");
      } else {
        range = start.format("MMM D") + " - " + end.format("MMM D");
      }

      $("#kt_dashboard_daterangepicker_date").html(range);
      $("#kt_dashboard_daterangepicker_title").html(title);
    }

    picker.daterangepicker(
      {
        direction: KTUtil.isRTL(),
        autoUpdateInput: false,
        locale: {
          cancelLabel: "Clear",
        },
        opens: "left",
        // ranges: {
        //  'Today': [moment(), moment()],
        //  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //  'This Month': [moment().startOf('month'), moment().endOf('month')],
        //  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        // }
      },
      cb
    );

    cb(start, end, "");

    $(picker).on("apply.daterangepicker", function (ev, picker) {
      $(this).val(
        picker.startDate.format("MM/DD/YYYY") +
        " - " +
        picker.endDate.format("MM/DD/YYYY")
      );
    });

    $(picker).on("cancel.daterangepicker", function (ev, picker) {
      $(this).val("");
    });
  };

  var status = function () {
    $(document).on("change", "#import_status", function () {
      $("#export_csv_status").val($(this).val());
    });
  };

  return {
    //main function to initiate the module
    init: function () {
      confirmDelete();
      customerServiceTable();
      filter();
      _selectProp();
      _add_ons();
      upload_guide();
      status();
      updateNotes();
      forwardStatus();
      updateStatus();
      closeStatus();
      daterangepickerInit();
    },
  };
})();

jQuery(document).ready(function () {
  InquiryTicket.init();
});

function setNotes(id) {
  $('#customer_service_id').val(id)
  $.ajax({
    url: base_url + "generic_api/fetch_specific",
    method: "GET",
    dataType: "JSON",
    data: {
      table: 'customer_services',
      field: 'id',
      value: id,
      select: 'notes'
    },
    success: function (res) {
    },
  })
}

// Function for setting up modal information: inquiry ticket id, status
function setInquiryTicketID(id, ref_no, status = false) {

  $("#ticket_number").html(ref_no);
  $("#ticket_number_status").html(ref_no);

  document.getElementById("forward_status_id").value = id;
  document.getElementById("customer_service_id").value = id;
  document.getElementById("status_id").value = status;
  // document.getElementById("updateStatusModalLabel").innerHTML = label;
}

$(document).on("click", ".ellipsis", function () {
  var id = $(this).data('id')
  var more_element = $('.more[data-id="' + id + '"]')

  if(more_element[0].style.display === 'none'){
    $(this)[0].text = ' Show Less'
    more_element[0].style.display = 'inline'
  }else if(more_element[0].style.display === 'inline'){
    more_element[0].style.display = 'none'
    $(this)[0].text = ' Show More'
  }else{
    $(this)[0].text = ' Show Less'
    more_element[0].style.display = 'inline'
  }
})