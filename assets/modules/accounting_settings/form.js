// Class definition
var Form = (function () {
    var arrows;

    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>',
        };
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>',
        };
    }

    // Private functions
    var datepicker = function () {
        // minimum setup
        $(".kt_datepicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: "no",
            format: "yyyy-mm-dd",
            autoclose: true,
        });

        $(".yearPicker").datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
        });
    };

    var formEl;
    var validator;

    var initValidation = function () {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                company_id: {
                    required: true,
                },
            },
            // messages: {
            // 	"info[last_name]":'Last name field is required',
            // },
            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    title: "",
                    text:
                        "There are some errors in your submission. Please correct them.",
                    type: "error",
                    confirmButtonClass: "btn btn-secondary",
                });
            },

            // Submit valid form
            submitHandler: function (form) {},
        });
    };

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on("click", function (e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    type: "POSt",
                    dataType: "JSON",
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success",
                            }).then(function () {
                                window.location.replace(
                                    base_url + "accounting_settings"
                                );
                            });
                        } else {
                            swal.fire({
                                title: "Oops!",
                                html: response.message,
                                icon: "error",
                            });
                        }
                    },
                });
            }
        });
    };

    const getLedgers = () => {
        company_id = $('#company_id').val();
        
        // Get the element again
        const entryItemLedgerSelect = document.querySelectorAll(
            "#accounting_entry"
        );
        // Loop through the element
        entryItemLedgerSelect.forEach((select) => {
            $.ajax({
                url: base_url + "accounting_entries/get_all_ledgers",
                type: "GET",
                data: {
                    company_id: company_id,
                },
                success: function (data) {
                    let ledgers = JSON.parse(data);

                    // Sort Array alphabetically
                    ledgers.data.sort(function (a, b) {
                        var textA = a.name.toUpperCase();
                        var textB = b.name.toUpperCase();
                        return textA < textB ? -1 : textA > textB ? 1 : 0;
                    });

                    ledgers.data.map((ledger) => {
                        let opt = document.createElement("option");

                        opt.value = ledger.id;
                        opt.innerHTML = ledger.name;
                        select.appendChild(opt);
                    });
                },
            });
        });
    };

    var formRepeater = function () {
        $("#accounting_setting_items_form_repeater").repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                getLedgers();
                datepicker();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
                getLedgers();
            },
        });
    };

    var _add_ons = function () {
        datepicker();
        formRepeater();
        getLedgers();
    };

    $('#form_accounting_settings #company_id').on('change', function () {
        getLedgers();
    });

    // Public functions
    return {
        init: function () {
            datepicker();
            _add_ons();

            formEl = $("#form_accounting_settings");

            initValidation();
            initSubmit();
        },
    };
})();

// Initialization
jQuery(document).ready(function () {
    Form.init();


    
});
