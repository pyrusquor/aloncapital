'use strict';
var AccountingSettings = (function () {

    let tableElement = $('#accounting_settings_table');

    var AccountingSettingsTable = function () {
        
        var dataTable = tableElement.DataTable({
            order: [[1, 'desc']],
            pagingType: 'full_numbers',
            lengthMenu: [5, 10, 25, 50, 100],
            responsive: true,
            pageLength: 10,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            deferRender: true,
            ajax: {
                data: function(data) {
                    getFilter(data);
                },
                url: base_url + 'accounting_settings/showItems',
            },
            dom:
                "<'row'<'col-sm-12 col-md-10'l><'col-sm-12 col-md-2 text-right'<'btn-block'B>>>" +
                // "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    text: '<i class="la la-trash"></i> Delete Selected',
                    className:
                        'btn btn-sm btn-label-primary btn-elevate btn-icon-sm',
                    attr: {
                        id: 'bulkDelete',
                    },
                    enabled: false,
                },
            ],
            language: {
                lengthMenu: 'Show _MENU_',
                infoFiltered: '(filtered from _MAX_ total records)',
            },

            columns: [
                {
                    data: null,
                },
                {
                    data: 'id',
                },
                /* ==================== begin: Add model fields ==================== */
                {
                    data: 'category_type',
                },
                {
                    data: 'period_id',
                },
                {
                    data: 'module_types_id',
                },
                {
                    data: 'entry_types_id',
                },
                {
                    data: 'company_id',
                },
                {
                    data: 'project_id',
                },
                {
                    data: 'Actions',
                    responsivePriority: -1,
                },
            ],
            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return (
                            `
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="` +
                            base_url +
                            `accounting_settings/form/` +
                            row.id +
                            `"><i class="la la-edit"></i> Update </a>
										<a href="javascript:void(0);" class="dropdown-item remove_accounting_setting" data-id="` +
                            row.id +
                            `"><i class="la la-trash"></i> Delete </a>
									</div>
								</span>
								<a href="` +
                            base_url +
                            `accounting_settings/view/` +
                            row.id +
                            `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
								<i class="la la-eye"></i>
								</a>`
                        );
                    },
                },
                {
                    targets: [0, 1, 2, 3, 4, -1],
                    className: 'dt-center',
                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return (
                            `
	                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                      <input type="checkbox" name="id[]" value="` +
                            row.id +
                            `" class="m-checkable delete_check" data-id="` +
                            row.id +
                            `">
                      <span></span>
	                  </label>`
                        );
                    },
                    orderable: false,
                    searchable: false,
                },
                // {
                //     targets: 2,
                //     render: function (data, type, full, meta) {
                //         var category_type = {
                //             1: {
                //                 type: 'Collection',
                //             },
                //             2: {
                //                 type: 'Reservation',
                //             },
                //         };

                //         if (typeof category_type[data] === 'undefined') {
                //             return ``;
                //         }
                //         return category_type[data].type;
                //     },
                // },

                // {
                //     targets: 3,
                //     render: function (data, type, full, meta) {
                //         var period_type = {
                //             1: {
                //                 type: 'Reservation',
                //             },
                //             2: {
                //                 type: 'Downpayment',
                //             },
                //             3: {
                //                 type: 'Loan',
                //             },
                //             4: {
                //                 type: 'Equity',
                //             },
                //             5: {
                //                 type: 'Others',
                //             },
                //         };

                //         if (typeof period_type[data] === 'undefined') {
                //             return ``;
                //         }
                //         return period_type[data].type;
                //     },
                // },
                // {
                //     targets: 4,
                //     render: function (data, type, full, meta) {
                //         // '1' => 'Commissions', '2' => 'Collections', '3' => 'Purchase Request', '4' => 'Purchase Invoice', '5' => 'Waive'
                //         var module_type = {
                //             1: {
                //                 type: 'Commissions',
                //             },
                //             2: {
                //                 type: 'Collections',
                //             },
                //             3: {
                //                 type: 'Purchase Request',
                //             },
                //             4: {
                //                 type: 'Purchase Invoice',
                //             },
                //             5: {
                //                 type: 'Waive',
                //             },
                //         };

                //         if (typeof module_type[data] === 'undefined') {
                //             return ``;
                //         }
                //         return module_type[data].type;
                //     },
                // },
                // {
                //     targets: 5,
                //     render: function (data, type, full, meta) {
                //         // '1' => 'Receipt', '2' => 'Journal', '3' => 'Payment', '4' => 'Contra'
                //         var entry_type = {
                //             1: {
                //                 type: 'Receipt',
                //             },
                //             2: {
                //                 type: 'Journal',
                //             },
                //             3: {
                //                 type: 'Payment',
                //             },
                //             4: {
                //                 type: 'Contra',
                //             },
                //         };

                //         if (typeof entry_type[data] === 'undefined') {
                //             return ``;
                //         }
                //         return entry_type[data].type;
                //     },
                // },
            ],
            drawCallback: function (settings) {
                $('#total').text(settings.fnRecordsTotal() + ' TOTAL');
            },
        });

        function getFilter(data) {

            data.filter = $('#advance_search').serialize();
        }

        $('#advance_search').submit(function (e) {

            e.preventDefault();
            dataTable.ajax.reload()
        })
    };

    // Private functions
    var datepicker = function () {
        // minimum setup
        $('.compDatepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
        });

        $('.monthYearPicker').datepicker({
            todayHighlight: true,
            orientation: 'bottom left',
            templates: arrows,
            format: 'yyyy-mm',
            viewMode: 'months',
            minViewMode: 'months',
            autoclose: true,
            onClose: function (dateText, inst) {
                $(this).datepicker(
                    'setDate',
                    new Date(inst.selectedYear, inst.selectedMonth)
                );
            },
        });

        $('.yearPicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy',
            viewMode: 'years',
            minViewMode: 'years',
            autoclose: true,
        });
    };

    var daterangepickerInit = function () {
        if ($('.kt_transaction_daterangepicker').length == 0) {
            return;
        }

        var picker = $('.kt_transaction_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if (end - start < 100 || label == 'Today') {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday') {
                title = 'Yesterday:';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D');
            }

            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker(
            {
                direction: KTUtil.isRTL(),
                startDate: start,
                endDate: end,
                opens: 'left',
                // ranges: {
                // 	'Today': [moment(), moment()],
                // 	'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                // 	'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                // 	'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                // 	'This Month': [moment().startOf('month'), moment().endOf('month')],
                // 	'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                // }
            },
            cb
        );

        cb(start, end, '');
    };

    var confirmDelete = function () {
        $(document).on('click', '.remove_accounting_setting', function () {
            var id = $(this).data('id');

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'accounting_settings/delete/' + id,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id: id,
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#accounting_settings_table')
                                    .DataTable()
                                    .ajax.reload();
                                swal.fire('Deleted!', res.message, 'success');
                            } else {
                                swal.fire('Oops!', res.message, 'error');
                            }
                        },
                    });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    );
                }
            });
        });
    };

    var upload_guide = function () {
        $(document).on('click', '#btn_upload_guide', function () {
            var table = $('#upload_guide_table');

            table.DataTable({
                order: [[0, 'asc']],
                pagingType: 'full_numbers',
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 10,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: base_url + 'accounting_settings/get_table_schema',
                columns: [
                    // {data: 'button'},
                    {
                        data: 'no',
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'type',
                    },
                    {
                        data: 'format',
                    },
                    {
                        data: 'option',
                    },
                    {
                        data: 'required',
                    },
                ],
                // drawCallback: function ( settings ) {

                // }
            });
        });
    };

    var _add_ons = function () {
        var dTable = $('#accounting_settings_table').DataTable();

        $('#_search').on('keyup', function () {
            dTable.search($(this).val()).draw();
        });

        $('#_export_select_all').on('click', function () {
            if (this.checked) {
                $('._export_column').each(function () {
                    this.checked = true;
                });
            } else {
                $('._export_column').each(function () {
                    this.checked = false;
                });
            }
        });

        $('._export_column').on('click', function () {
            if (this.checked == false) {
                $('#_export_select_all').prop('checked', false);
            }
        });

        $('#import_status').on('change', function (e) {
            var doc_type = $(this).val();

            if (doc_type != '') {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').removeAttr('disabled');
                    $('#_batch_upload button').removeClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            } else {
                KTApp.block('#kt_content', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...',
                    css: {
                        padding: 0,
                        margin: 0,
                        width: '30%',
                        top: '40%',
                        left: '35%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #aaa',
                        backgroundColor: '#fff',
                        cursor: 'wait',
                    },
                });

                setTimeout(function () {
                    $('#_batch_upload button').attr('disabled', 'disabled');
                    $('#_batch_upload button').addClass('disabled');

                    KTApp.unblock('#kt_content');
                }, 500);
            }
        });
    };

    var status = function () {
        $(document).on('change', '#import_status', function () {
            $('#export_csv_status').val($(this).val());
        });
    };

    var filter = function () {
        $("#generalSearch").keyup(function (e) {
            e.preventDefault();
            let code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter"){
                tableElement
                    .DataTable()
                    .search($(this)
                    .val())
                    .draw();
            }
        });

        $('._filter').on('keyup change clear', function () {

            processChange()
        })

        const processChange = debounce(() => submitInput());

        function debounce(func, timeout = 500){
            let timer;
            return (...args) => {
              clearTimeout(timer);
              timer = setTimeout(() => { func.apply(this, args); }, timeout);
            };
        }

        function submitInput(){
            $('#advance_search').submit()
        }
    };

    var _selectProp = function () {
        var _table = $('#accounting_settings_table').DataTable();
        var _buttons = _table.buttons(['.bulkDelete']);

        $('#select-all').on('click', function () {
            if ($(this).is(':checked')) {
                $('.delete_check').prop('checked', true);
            } else {
                $('.delete_check').prop('checked', false);
            }
        });

        $('#accounting_settings_table tbody').on(
            'change',
            'input[type="checkbox"]',
            function () {
                if (!this.checked) {
                    var el = $('input#select-all').get(0);

                    if (el && el.checked && 'indeterminate' in el) {
                        el.indeterminate = true;
                    }
                }
            }
        );

        $(document).on('change', 'input[name="id[]"]', function () {
            var _checked = $('input[name="id[]"]:checked').length;
            if (_checked < 0) {
                _table.button(0).disable();
            } else {
                _table.button(0).enable(_checked > 0);
            }
        });

        $('#bulkDelete').click(function () {
            var deleteids_arr = [];
            // Read all checked checkboxes
            $('input[name="id[]"]:checked').each(function () {
                deleteids_arr.push($(this).val());
            });

            // Check checkbox checked or not
            if (deleteids_arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url + 'accounting_settings/bulkDelete',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                deleteids_arr: deleteids_arr,
                            },
                            success: function (res) {
                                if (res.status) {
                                    $('#accounting_settings_table')
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire(
                                        'Deleted!',
                                        res.message,
                                        'success'
                                    );
                                } else {
                                    swal.fire('Oops!', res.message, 'error');
                                }
                            },
                        });
                    } else if (result.dismiss === 'cancel') {
                        swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        );
                    }
                });
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            confirmDelete();
            datepicker();
            daterangepickerInit();
            AccountingSettingsTable();
            _add_ons();
            filter();
            upload_guide();
            status();
            _selectProp();
        },
    };
})();

jQuery(document).ready(function () {
    AccountingSettings.init();
});
