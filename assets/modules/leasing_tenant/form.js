"use strict";
let LeasingProperty = (() => {

    let arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}

    // Base elements
	let wizardEl;
	let formEl;
	let validator;
	let wizard;
	
	// Private functions
	let initWizard = () => {
		// Initialize form wizard
		wizard = new KTWizard('kt_wizard_v3', {
			startStep: 1,
		});

		// Validation before going to next page
		wizard.on('beforeNext', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
		});

		wizard.on('beforePrev', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
		});

		// Change event
		wizard.on('change', function(wizard) {
			KTUtil.scrollTop();	
		});
	}

	let initValidation = () => {
		validator = formEl.validate({
			// Validate only visible fields
			ignore: ":hidden",
			// Validation rules
			rules: {
				// "info[type_id]": {
                //     required: true 
				// },
            },
			// messages: {
			// 	"info[email]": {
			// 		required: 'Email field is required',
			// 		email: 'Invalid email address',
			// 		remote:jQuery.validator.format("{0} is already taken, please enter a different email address.")
			// 	},
			// },
			// Display error  
			invalidHandler: function(event, validator) {	 
				KTUtil.scrollTop();

				swal.fire({
					"title": "", 
					"text": "There are some errors in your submission. Please correct them.", 
					"type": "error",
					"confirmButtonClass": "btn btn-secondary"
				});
			},

			// Submit valid form
			submitHandler: function (form) {
				
			}
		});   
	}

	let initSubmit = () => {
		let btn = formEl.find('[data-ktwizard-type="action-submit"]');

		btn.on('click', function(e) {
			e.preventDefault();

			if (validator.form()) {
				// See: src\js\framework\base\app.js
				KTApp.progress(btn);
				//KTApp.block(formEl);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formEl.ajaxSubmit({
                    type: 'POSt',
                    dataType: 'JSON',
					success: function(response) {
						if (response.status) {

							swal.fire({
								title: "Success!",
								text: response.message,
								type: "success"
							}).then(function() {
								window.location.replace(base_url + "leasing_tenant");
							});

						} else {
							swal.fire(
								'Oops!',
								response.message,
								'error'
							)
						}
					}
				});
			}
		});
	}

	let datepicker = () => {
		// minimum setup
		$('.datePicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			locale: 'no',
			format: 'yyyy-mm-dd',
			autoclose: true
		});

		$('.yearPicker').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years",
			autoclose: true
		});
	}

	let checkTenantTypeId = () => {
		let value = $('#tenant_type_id').val()

		if (value == 2) {

			$('.company-tenant-info').removeClass('d-none')
		} else {

			$('.company-tenant-info input').val('')
			$('.company-tenant-info').addClass('d-none')
		}
	}

	let watcher = () => {

		checkTenantTypeId()
		
		$('#tenant_type_id').on('change', () => {
			
			checkTenantTypeId()
		})
	}

    return {
        init: function () {
            wizardEl = KTUtil.get('kt_wizard_v3');
			formEl = $('#leasing_tenants_form');

			initWizard(); 
			initValidation();
			initSubmit();
			datepicker();
			watcher()
        },
    };
})();

jQuery(document).ready(function () {
    LeasingProperty.init();
});