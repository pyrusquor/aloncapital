var laborInfo = function (){
    var formEl;
    var validator;
    var initValidation = function () {
        validator = formEl.validate({
            ignore: ":hidden",
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                });
            },

            submitHandler: function (form) {

            }
        });
    };
    var laborRepater = function() {
        $('.repeater').repeater({
            initEmpty: false,
            defaultValues: {},
            show: function () {
                $(this).slideDown();
                datePicker();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width','100%');
            },
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
        })
    }
    var datePicker = function () {
        // minimum setup
        $('.kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows,
            locale: 'no',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('.timepicker').timepicker({
            showMeridian: false
        });
    };
    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');
        btn.on("click", function (e) {
            e.preventDefault();
            if (validator.form()) {
                KTApp.progress(btn);
                formEl.ajaxSubmit({
                    type: "POST",
                    dataType: "JSON",
                    url: base_url + 'construction_order/overhead_cost_form/',
                    success: function (response) {
                        if (response.status) {
                            swal.fire({
                                title: "Success!",
                                text: response.message,
                                type: "success",
                            }).then(function () {
                                window.location.replace(
                                    base_url + "construction_order"
                                );
                            });
                        } else {
                            swal.fire({
                                title: "Oops!",
                                html: response.message,
                                icon: "error",
                            });
                        }
                    },
                });
            }
        })
    }
    return{
        init: function(){
            laborRepater();
            datePicker();
            formEl = $('#overhead_cost_form')
            initSubmit();
            initValidation();
        }
    }
}();
$(document).ready(function(){
    laborInfo.init();
})

function goBack() {
    if(window.history.length > 3 ){
        window.history.back()
    }else{
        window.location.replace(base_url + '/construction_order')
    }
 }