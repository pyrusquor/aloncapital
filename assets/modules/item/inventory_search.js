$(document).ready(function () {
    $("#item_id").on("change", function (e) {
        app.item_id = $("#item_id").val();
        app.search();
    });

});

var app = new Vue({
    el: '#item_search_app',
    data: {
        item_id: null,
        inventory: {
            data: [],
            loading: false
        }
    },
    methods: {
        warehouseURL(id) {
            return base_url + "warehouse/view/" + id;
        },
        search() {
            this.inventory.loading = true;
            if (this.item_id) {
                let url = base_url + "warehouse_inventory/search?item_id=" + this.item_id;

                axios.get(url)
                    .then(response => {
                        if (response.data.length > 0) {
                            this.inventory.data = response.data;
                        } else {
                            swal.fire({
                                title: "Oooops!",
                                text: "Not in inventory!",
                                type: "error",
                            });
                        }
                        this.inventory.loading = false;
                    });
            } else {
                swal.fire({
                    title: "Oooops!",
                    text: "Nothing to search!",
                    type: "error",
                });
            }
            this.first_load = false;
        }
    },
    mounted() {
        this.item_id = window.item_id;
        if(this.item_id){
            this.search();
        }
    }
});