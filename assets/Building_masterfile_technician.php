<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building_masterfile_technician extends MX_Controller 
{
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Manila");

        //set table name
        $this->crud->tbl_name = 'asa_md_technician_tbl';
    }

    public function load()
    {
        $result = $this->crud->fetch_all();
        if($result)
        {
            echo json_encode($result->result());
        }
    }

    public function create()
    {
        $data = [
            'name' => cleaner(urldecode($this->input->post('name'))),
            'employee_id' => cleaner(urldecode($this->input->post('employee_id'))),
            'created_by' => urldecode($this->input->post('created_by')),
            'created_date' => urldecode($this->input->post('created_date')),
            'mod_by' => urldecode($this->input->post('mod_by'))
        ];

        //check if the required fields have values
        if(empty($data['name']))
        {
            echo json_encode(['error'=> TRUE,'message'=> 'Fields with * are required']);
            exit;
        }

        //check name if exist
        if($this->has_duplicate($data['name'],$data['employee_id']))
        {
            echo json_encode(['error'=> TRUE,'message'=> 'Name already existing']);
            exit;
        }

        //process the insert to database
        $this->crud->data = $data;
        if($this->crud->save())
        {
            echo json_encode(['error'=> FALSE,'message'=> 'Technician successfully added']);
        }
    }

    public function update()
    {
        $id = urldecode($this->input->post('sys_id'));
        $data = [
            'name' => cleaner(urldecode($this->input->post('name'))),
            'employee_id' => cleaner(urldecode($this->input->post('employee_id'))),
            'mod_by' => urldecode($this->input->post('mod_by')),
            'mod_date' => urldecode($this->input->post('mod_date'))
        ];

        //check if id is not empty
        if(empty($id) || $id === "")
        {
            echo json_encode(['error'=> TRUE,'message'=> 'Update failed. Please contact system administrator!']);
            exit;
        }

        //check if the required fields have values
        if(empty($data['name']))
        {
            echo json_encode(['error'=> TRUE,'message'=> 'Fields with * are required']);
            exit;
        }

        //check name if exist
        if($this->has_duplicate($data['name'],$data['employee_id'],$id))
        {
            echo json_encode(['error'=> TRUE,'message'=> 'Name already existing']);
            exit;
        }

        //process the update to database
        $this->crud->id = $id;
        $this->crud->data = $data;
        if($this->crud->save())
        {
            echo json_encode(['error'=> FALSE,'message'=> 'Technician successfully updated']);
        }
    }

    public function update_status()
    {
        $id = urldecode($this->input->post('sys_id'));

        $data = [
            'status'=> urldecode($this->input->post('status')),
            'mod_by'=> urldecode($this->input->post('mod_by')),
            'mod_date'=> urldecode($this->input->post('mod_date'))
        ];

        //check if id is not empty
        if(empty($id) || $id === "")
        {
            echo json_encode(['error'=> TRUE,'message'=> 'Update failed. Please contact system administrator!']);
            exit;
        }

        //process the update to database
        $this->crud->id = $id;
        $this->crud->data = $data;
        if($this->crud->save())
        {
            echo json_encode(['error'=> FALSE,'message'=> 'Technician status updated']);
        }
    }

    private function has_duplicate($nameVal,$parentId,$id = NULL)
    {
        $this->crud->where = [];//declare variable "where" as array

        //use for create
        $this->crud->where[0] = ['name',$nameVal];
        $this->crud->where[1] = ['employee_id',$parentId];

        //use for update
        if($id != NULL)
        {
            $this->crud->where[2] = ['sys_id !=',$id];
        }

        if($this->crud->find_row() > 0)
        {
            return TRUE;
        }
    }

    public function seeder()
    {
        require 'vendor/autoload.php';
        $faker = Faker\Factory::create(); 

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->name,
                'employee_id'=> $faker->randomDigitNotNull,
                'created_by' => 1,
                'created_date' => date('Y-m-d h:i:s'),
                'mod_by' => 1
            ));
        }

        $this->crud->data = $data;
        if($this->crud->batch_create())
        {
            echo json_encode(['error'=> FALSE,'message'=> 'Data seeder successful']);
        }
    }
}