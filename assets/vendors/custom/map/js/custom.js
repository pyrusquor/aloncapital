
$(document).ready(function() {
    // Inline JSON example
    var data = {
        "mapwidth":"640",
        "mapheight":"600",
        "categories":[
        ],
        "levels":[
            {
                "id":"canada",
                "title":"Canada",
                "map":"maps/canada.svg",
                "locations":[
                    {
                        "id": "ca-qc",
                        "title": "Quebec",
                        "description": "Quebec.",
                        "pin": "hidden",
                        "fill":"#AFC144",
                        "x": "0.6874",
                        "y": "0.7325",
                        "zoom": "2.5"
                    },
                    {
                        "id": "calgary",
                        "title": "Calgary",
                        "description": "Calgary.",
                        "pin": "pin-marker",
                        "fill": "#B47ACF",
                        "x": "0.2273",
                        "y": "0.7425"
                    }
                ]
            }
        ]
    };

    // Uncomment "source: data," to enable inline JSON
    $('#mapplic').mapplic({
        source: 'canada.json',
        //source: data,
        height: 600,
        sidebar: false,
        minimap: false,
        zoombuttons: false,
        fullscreen: false,
        hovertip: true,
        maxscale: 2
    });
});
