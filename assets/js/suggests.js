// Class definition
var Select = (function () {
    // Private functions
    var sgSelect = function () {
        $('.suggests').select2({
            allowClear: true,
            placeholder: ' Select...',
            minimumInputLength: 1,
            ajax: {
                url: base_url + 'suggests/auth',
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    mod = $(this).attr('data-module');
                    param = $(this).attr('data-param');
                    param2 = $(this).attr('data-param-2');
                    param_type = $(this).attr('data-type');
                    select = $(this).attr('data-select');
                    param_val = $('#' + param).val();
                    param_val2 = $('#' + param2).val();
                    return {
                        term: params.term,
                        page: params.page,
                        param: param,
                        param2: param2,
                        param_val: param_val,
                        param_val2: param_val2,
                        param_type: param_type,
                        mod: mod,
                        select: select,
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: data.incomplete_results,
                        },
                    };
                },
                templateSelection: function (data) {
                    return data.text;
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                cache: true,
            },
        });
        $('.suggests-show-options').select2({
            allowClear: true,
            placeholder: ' Select...',
            minimumInputLength: 0,
            ajax: {
                url: base_url + 'suggests/auth',
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    mod = $(this).attr('data-module');
                    param = $(this).attr('data-param');
                    param2 = $(this).attr('data-param-2');
                    param_type = $(this).attr('data-type');
                    select = $(this).attr('data-select');
                    param_val = $('#' + param).val();
                    param_val2 = $('#' + param2).val();
                    return {
                        term: params.term,
                        page: params.page,
                        param: param,
                        param2: param2,
                        param_val: param_val,
                        param_val2: param_val2,
                        param_type: param_type,
                        mod: mod,
                        select: select,
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: data.incomplete_results,
                        },
                    };
                },
                templateSelection: function (data) {
                    return data.text;
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                cache: true,
            },
        });
        $('.suggests-multiple').select2({
            allowClear: false,
            multiple:true,
            placeholder: ' Select...',
            minimumInputLength: 1,
            ajax: {
                url: base_url + 'suggests/auth',
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    mod = $(this).attr('data-module');
                    param = $(this).attr('data-param');
                    param_type = $(this).attr('data-type');
                    select = $(this).attr('data-select');
                    param_val = $('#' + param).val();
                    return {
                        term: params.term,
                        page: params.page,
                        param: param,
                        param_val: param_val,
                        param_type: param_type,
                        mod: mod,
                        select: select,
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: data.incomplete_results,
                        },
                    };
                },
                templateSelection: function (data) {
                    return data.text;
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                cache: true,
            },
        });
        $('.suggests_modal').select2({
            allowClear: true,
            placeholder: ' Select...',
            dropdownParent: $('#filterModal, #forwardStatusBatchModal, #forwardStatusModal'),
            minimumInputLength: 1,
            ajax: {
                url: base_url + 'suggests/auth',
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    mod = $(this).attr('data-module');
                    param = $(this).attr('data-param');
                    param_type = $(this).attr('data-type');
                    select = $(this).attr('data-select');
                    param_val = $('#' + param).val();
                    return {
                        term: params.term,
                        page: params.page,
                        param: param,
                        param_val: param_val,
                        param_type: param_type,
                        mod: mod,
                        select: select,
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: data.incomplete_results,
                        },
                    };
                },
                templateSelection: function (data) {
                    return data.text;
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                cache: true,
            },
        });

        $('.complaint_tickets_suggests_modal').select2({
            allowClear: true,
            placeholder: ' Select...',
            dropdownParent: $('#forwardStatusModal'),
            minimumInputLength: 1,
            ajax: {
                url: base_url + 'suggests/auth',
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    mod = $(this).attr('data-module');
                    param = $(this).attr('data-param');
                    param_type = $(this).attr('data-type');
                    select = $(this).attr('data-select');
                    param_val = $('#' + param).val();
                    return {
                        term: params.term,
                        page: params.page,
                        param: param,
                        param_val: param_val,
                        param_type: param_type,
                        mod: mod,
                        select: select,
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: data.incomplete_results,
                        },
                    };
                },
                templateSelection: function (data) {
                    return data.text;
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                cache: true,
            },
        });

        $('.quotation_modal').select2({
            allowClear: true,
            dropdownParent: $('#quotationModal'),
            minimumInputLength: 1,
            ajax: {
                url: base_url + 'suggests/auth',
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    mod = $(this).attr('data-module');
                    param = $(this).attr('data-param');
                    param_type = $(this).attr('data-type');
                    select = $(this).attr('data-select');
                    param_val = $('#' + param).val();
                    return {
                        term: params.term,
                        page: params.page,
                        param: param,
                        param_val: param_val,
                        param_type: param_type,
                        mod: mod,
                        select: select,
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: data.incomplete_results,
                        },
                    };
                },
                templateSelection: function (data) {
                    return data.text;
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                cache: true,
            },
        });
    };

    // Public functions
    return {
        init: function () {
            sgSelect();
        },
    };
})();

// Initialization
jQuery(document).ready(function () {
    Select.init();
});
// temporary fix for broken css for suggests-multiple
$(document).ready(function(){
    $('.suggests-multiple').trigger('change');
});
