$(document).ready(function(){

    // Legal
    $(".legal").click(function(){
      $(".dashboard-main").addClass('hidden');
      $(".dashboard-legal-submain").removeClass('hidden');
    });

    // Hide Designated Cards after Clicking Back Button
    $(".back-button").click(function(){
        $(".dashboard-main").removeClass('hidden');
        $(".dashboard-legal-submain").addClass('hidden');
        $(".dashboard-marketing-submain").addClass('hidden');
        $(".dashboard-sales_admin-submain").addClass('hidden');
        $(".dashboard-documentation-submain").addClass('hidden');
        $(".dashboard-credit_collection-submain").addClass('hidden');
        $(".dashboard-property_management-submain").addClass('hidden');
        $(".dashboard-accounting-submain").addClass('hidden');
        $(".dashboard-taxation-submain").addClass('hidden');
        $(".dashboard-warehouse-submain").addClass('hidden');
        $(".dashboard-construction-submain").addClass('hidden');
        $(".dashboard-purchasing-submain").addClass('hidden');
    });

    // Marketing
    $(".marketing").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-marketing-submain").removeClass('hidden');
    });

    // Sales Admin
    $(".sales-admin").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-sales_admin-submain").removeClass('hidden');
    });

    // Documentation
    $(".documentation").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-documentation-submain").removeClass('hidden');
    });

    // Credit & Collection
    $(".credit_collection").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-credit_collection-submain").removeClass('hidden');
    });

    // Property Management
    $(".property_management").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-property_management-submain").removeClass('hidden');
    });

    // Accounting
    $(".accounting").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-accounting-submain").removeClass('hidden');
    });

    // Taxation
    $(".taxation").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-taxation-submain").removeClass('hidden');
    });

    // Warehouse
    $(".warehouse").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-warehouse-submain").removeClass('hidden');
    });

    // Construction
    $(".construction").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-construction-submain").removeClass('hidden');
    });

    // Purchasing
    $(".purchasing").click(function(){
        $(".dashboard-main").addClass('hidden');
        $(".dashboard-purchasing-submain").removeClass('hidden');
    });


});