// Class definition
var Computation = (function () {
    // Private functions
    var onProp = function () {
        $(document).on('change', '#create_property  .compute', function () {
            populateName();
            computePropPromos();
            populateComputation();
        });

        $(document).on('keyup', '#create_property .populateCode', function () {
            populateName();
        });

        $(document).on('change', '#create_property  #interior_id', function () {
            id = $(this).val();
            populateDataInterior(id);
            setTimeout(function () {
                computePropPromos();
                populateComputation();
            }, 2000);
        });

        $(document).on('change', '#create_property #project_id', function () {
            id = $(this).val();
            populateDataProject(id);
            computePropPromos();
            populateComputation();
            populateName();
        });

        $(document).on('change', '#create_property .promoID', function () {
            computePropPromos();
            populateComputation();
        });

        $(document).on(
            'change',
            '#create_property #package_type_id',
            function () {
                id = $('#interior_id').val();
                populateDataInterior(id);
                setTimeout(function () {
                    computePropPromos();
                    populateComputation();
                }, 2000);
            }
        );
    };

    var populateComputation = function () {
        is_multiply_floor_area = parseInt($('#is_multiply_floor_area').val());
        is_multiply_lot_area = parseInt($('#is_multiply_lot_area').val());

        lot_area = parseFloat($('#lot_area').val());
        floor_area = parseFloat($('#floor_area').val());
        lot_price_per_sqm = parseFloat($('#lot_price_per_sqm').val());
        promo_amount = parseFloat($('#promo_amount').val());
        adjusted_price = parseFloat($('#adjusted_price').val());

        if (lot_area == '') {
            lot_area = 0;
        }

        if (floor_area == '') {
            floor_area = 0;
        }

        if (lot_price_per_sqm == '') {
            lot_price_per_sqm = 0;
        }

        total_lot_price = (lot_area * lot_price_per_sqm).toFixed(2);
        $('#total_lot_price').val(total_lot_price);

        model_price = parseFloat($('#model_price').val());

        if (is_multiply_floor_area) {
            model_price = (model_price * floor_area).toFixed(2);
            // $('.popField#model_price').val(model_price);
        } else if (is_multiply_lot_area) {
            model_price = (model_price * lot_area).toFixed(2);
            // $('.popField#model_price').val(model_price);
        }

        total_selling_price =
            parseFloat(total_lot_price) +
            parseFloat(model_price) +
            parseFloat(adjusted_price);

        $('#total_selling_price').val(total_selling_price);

        d_total_selling_price = total_selling_price - promo_amount;

        $('#discounted_total_selling_price').val(d_total_selling_price);
    };

    var populateName = function () {
        lot = $('#lot').val();
        block = $('#block').val();
        phase = $('#phase').val();
        cluster = $('#cluster').val();
        project_id = $('#project_id').val();

        name =
            lpad(project_id) +
            '-' +
            lpad(phase) +
            '-' +
            lpad(block) +
            '-' +
            lpad(lot);

        $('#name').val(name);
    };

    var populateDataInterior = function (val = 0) {
        package_type_id = $('#package_type_id').val();
        if (package_type_id == 0) {
            package_type_id = 1;
        }
        if (package_type_id == 1) {
            clm = 'price';
        } else if (package_type_id == 2) {
            clm = 'improved_price';
        } else if (package_type_id == 3) {
            clm = 'fully_furnished_price';
        }

        // If 0 set values to default
        // if value is already set return

        var lot_area = $('#lot_area').val();
        var floor_area = $('floor_area').val();

        // if (lot_area == 0 || floor_area == 0) {
        $.ajax({
            url: base_url + 'interior/view/' + val + '/json',
            dataType: 'json',
            success: function (response) {
                $.each(response, function (index, value) {
                    if (index == 'interior') {
                        $.each(value, function (int_key, int_val) {
                            if (int_key == clm) {
                                $('.popField#model_price').val(int_val);
                            }

                            if (int_key == 'house') {
                                $.each(
                                    int_val,
                                    function (house_key, house_val) {
                                        $('.popField#' + house_key).val(
                                            house_val
                                        );
                                    }
                                );
                            }

                            $('.popField#' + int_key).val(int_val);
                        });
                    }

                    if (index == 'price_logs') {
                        $.each(value, function (ind, val) {
                            console.log(ind + ' ' + val);
                        });
                    }
                });
            },
        });
        // }
    };

    var populateDataProject = function (val = 0) {
        $.ajax({
            url: base_url + 'project/view/' + val + '/json',
            dataType: 'json',
            success: function (response) {
                $.each(response.project, function (index, value) {
                    // if (index == 'lot_price_per_sqm') {
                    $('.popField#' + index).val(value);
                    // }
                });
            },
        });
    };

    var computePropPromos = function () {
        promo_amount = 0;
        promos = [];

        $('.promoID')
            .find('option:selected')
            .each(function (index, element) {
                var value = parseFloat($(element).attr('data-value'));
                var value_type = $(element).attr('data-value-type'); // 1 = percentage, 2 = currency
                var promo_type = $(element).attr('data-promo-type'); // 1 = individual, 2 = compounded
                var deduct_to = $(element).attr('data-deduct-to'); // 1 = tsp, 2 = tcp, 3 = collectible, 4 = Total Lot Price, 5 = Total House Price,

                full_amt = getPromoType(deduct_to);

                promo_amount = getComputedAmount(value, value_type, full_amt);

                formula = getFormula($(element), deduct_to);

                if (isNaN(promo_amount)) {
                    promo_amount = 0;
                }
                $('#promo_amount').val(promo_amount);
                $('.formula').html(formula);

                promos.push(promo_amount);
            });
    };

    var getPromoType = function (type = 0) {
        total_selling_price = parseFloat($('#total_selling_price').val());
        model_price = parseFloat($('#model_price').val());
        total_lot_price = parseFloat($('#total_lot_price').val());

        amount = 0;

        if (type == 1) {
            amount = total_selling_price;
        } else if (type == 4) {
            amount = total_lot_price;
        } else if (type == 5) {
            amount = model_price;
        }

        return amount;
    };

    function lpad(value, padding = 3) {
        var zeroes = new Array(padding + 1).join('0');
        return (zeroes + value).slice(-padding);
    }
    // Public functions
    return {
        init: function () {
            onProp();
        },
    };
})();

// Initialization
jQuery(document).ready(function () {
    Computation.init();
});
