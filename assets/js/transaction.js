// Class definition
var TransactionFormComputation = (function () {
    // Private functions
    var onProp = function () {
        $(document).on(
            'change',
            '#form_transaction #interior_id, #form_transaction #package_type_id',
            function () {
                id = $('#form_transaction #package_type_id').val();
                populateDataInterior(id);
                setTimeout(function () {
                    populateComputation();
                }, 2000);
            }
        );

        $(document).on(
            'keyup',
            '#form_transaction #lot_area,#form_transaction #floor_area',
            function () {
                populateComputation();
                getTCP();
                getCP();
            }
        );

        $(document).on('change', '#form_transaction #project_id', function () {
            // id = $(this).val();
            setTimeout(() => {
                id = $('#property_id').val();
                computeAll(id);
            }, 2000);
        });

        $(document).on(
            'change',
            '#form_transaction #is_inclusive',
            function () {
                var collectible_price = parseFloat(
                    $('#collectible_price').val()
                );
                var vat_rate = 0.12;

                if (!collectible_price) {
                    collectible_price = 0;
                }

                $('#vat_amount').val(vat_rate * collectible_price);

                getTCP();
                setTimeout(function () {
                    getCP();
                }, 1000);
            }
        );

        $(document).on('change', '#form_transaction #property_id', function () {
            setTimeout(() => {
                id = $(this).val();
                computeAll(id);
            }, 2000);
        });

        $(document).on(
            'change',
            '#form_transaction .feesID,#fees_amount_rate',
            function () {
                if (!$(this).hasClass('feesPrice')) {
                    setTimeout(function () {
                        computeFees();
                        setTimeout(() => {
                            getTCP();
                            computePromos();
                            getCP();
                        }, 1000);
                    }, 1000);
                }
            }
        );

        $(document).on('change', '#form_modal .feesID', function () {
            computeAdhocFees();
            getAdhocSchedule();
        });

        $(document).on(
            'change',
            '#form_transaction .promoPrice, .promoID',
            function () {
                if ($(this).hasClass('promoPrice') || $(this).hasClass('promoID')) {
                    console.log('Add Discount Form');

                    setTimeout(function () {
                        computeFees();
                        setTimeout(() => {
                            computePromos();
                        }, 1000);
                        setTimeout(function () {
                            getTCP();
                            getCP();
                        }, 1000);
                    }, 1000);
                }
            }
        );
    };

    $(document).on(
        'change',
        '#form_transaction #commissionable_type_id',
        function () {
            getCommValue();
        }
    );

    $(document).on('change', '#form_transaction #wht_rate', function () {
        getTaxRate();
    });

    $(document).on(
        'change',
        '#form_transaction #financing_scheme_id,#form_transaction #reservation_date',
        
        function () {
            loadScheme();
        }
    );

    $(document).on('change', '#form_transaction .sellerComp', function () {
        sellerComp();
    });

    $(document).on('keyup', '#form_transaction .sellerComp', function () {
        sellerComp();
    });

    $(document).on('change', '#form_transaction .terms', function () {
        updateDateEffectivity();
    });

    $(document).on(
        'change',
        '#form_transaction .effectivity_dates',
        function () {
            period_id = $(this).attr('data-period-id');
            updateDateEffectivity_2(period_id);
        }
    );

    $(document).on(
        'change',
        '#form_transaction .computeSchemeAmount',
        function () {
            updateSchemeAmount();
        }
    );

    var formRepeater = function () {
        $(
            '#fees_form_repeater,#promos_form_repeater,#buyer_form_repeater,#seller_form_repeater'
        ).repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                $('.select2-container').remove();
                Select.init();
                $('.select2-container').css('width', '100%');
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    setTimeout(function () {
                        computeFees();
                        setTimeout(function () {
                            getTCP();
                            setTimeout(function () {
                                computePromos();
                                setTimeout(function () {
                                    getCP();
                                    getLA();
                                }, 1000);
                            }, 1000);
                        }, 1000);
                    }, 1000);

                    // setTimeout(function(){
                    //     computePromos();
                    //     getCP();
                    // },1000);

                    $(this).slideUp(deleteElement);
                    $('.select2-container').remove();
                    Select.init();
                    $('.select2-container').css('width', '100%');
                }
            },
        });
    };

    function lpad(value, padding = 3) {
        var zeroes = new Array(padding + 1).join('0');
        return (zeroes + value).slice(-padding);
    }

    // Public functions
    return {
        init: function () {
            onProp();
            formRepeater();
        },
    };
})();

// Initialization
jQuery(document).ready(function () {
    TransactionFormComputation.init();
});

var computeAll = function (id) {
    populateDataProperty(id);

    setTimeout(function () {
        vatSetting();
        computeFees();
        setTimeout(function () {
            getTCP();
            setTimeout(function () {
                getCP();
            }, 750);
        }, 750);
    }, 750);
};

var getComputedAmount = function (
    val = 0,
    type = 1,
    full_amount = 0,
    deduct = 0
) {
    amount = val;

    if (type == 1) {
        // percentage
        amount = (val / 100) * full_amount;
    }

    if (deduct) {
        amount = amount - deduct;
    }

    return formatCurrency(amount);
};

var getLoanDeduction = function (amt = []) {
    amount = 0;
    for (var i = 0; i < amt.length; i++) {
        amount = parseFloat(amount) + parseFloat(amt[i]);
    }

    return formatCurrency(amount);
};

var getAdhocFee = function (amt = []) {
    amount = 0;
    for (var i = 0; i < amt.length; i++) {
        amount = parseFloat(amount) + parseFloat(amt[i]);
    }

    return formatCurrency(amount);
};

var getFormula = function (element, type = 1) {
    str = '';

    var value = $(element).attr('data-value');
    var value_type = $(element).attr('data-value-type'); // 1 = percentage, 2 = currency
    var fees_type = $(element).attr('data-fees-type'); // 1 = individual, 2 = compounded
    var add_to = $(element).attr('data-add-to'); // 1 = tsp, 2 = tcp, 3 = collectible

    if (type == 1) {
        str = 'Total Selling Price ';
    } else if (type == 2) {
        str = 'Total Contract Price ';
    } else if (type == 3) {
        str = 'Collectible Price ';
    } else if (type == 4) {
        str = 'Total Lot Price ';
    } else if (type == 5) {
        str = 'Model Price ';
    }

    if (value_type == 1) {
        str += ' * ' + value + '%';
    } else if (value_type == 2) {
        str += ' + ' + 'Php ' + value;
    }

    if (fees_type == 1) {
        str += ' Individual';
    } else {
        str += ' Compounded';
    }

    return str;
};

var populateComputation = function () {
    lot_area = parseFloat($('#form_transaction #lot_area').val());
    floor_area = parseFloat($('#form_transaction #floor_area').val());
    lot_price_per_sqm = parseFloat(
        $('#form_transaction #lot_price_per_sqm').val()
    );

    total_lot_price = lot_area * lot_price_per_sqm;

    $('#form_transaction #total_lot_price').val(total_lot_price);

    model_price = parseFloat($('#form_transaction #total_house_price').val());

    total_selling_price = parseFloat(total_lot_price) + parseFloat(model_price);
    $('#form_transaction #total_selling_price').val(total_selling_price);
};

var computeTCP = function () {
    total_selling_price = parseFloat(
        $('#form_transaction #total_selling_price').val()
    );

    $('#form_transaction #total_contract_price').val(total_selling_price);
};

var computeFees = function () {
    total_selling_price = parseFloat(
        $('#form_transaction #total_selling_price').val()
    );
    total_contract_price = parseFloat(
        $('#form_transaction #total_contract_price').val()
    );
    collectible_price = parseFloat(
        $('#form_transaction #collectible_price').val()
    );

    fee_amount = 0;
    fees = [];

    $('#form_transaction .feesID')
        .find('option:selected')
        .each(function (index, element) {
            var value = parseFloat($(element).attr('data-value'));
            var value_type = $(element).attr('data-value-type'); // 1 = percentage, 2 = currency
            var fees_type = $(element).attr('data-fees-type'); // 1 = individual, 2 = compounded
            var add_to = $(element).attr('data-add-to'); // 1 = tsp, 2 = tcp, 3 = collectible

            new_total_selling_price = getNewAmount(
                fees,
                fees_type,
                total_selling_price
            );

            fee_amount = getComputedAmount(
                value,
                value_type,
                new_total_selling_price
            );

            formula = getFormula($(element), 1);

            $(element)
                .closest('[data-repeater-item]')
                .find('.feesPrice')
                .val(fee_amount);
            $(element)
                .closest('[data-repeater-item]')
                .find('.formula')
                .html(formula);

            fees.push(fee_amount);
        });
};

var computePromos = function () {
    total_selling_price = parseFloat(
        $('#form_transaction #total_selling_price').val()
    );
    total_contract_price = parseFloat(
        $('#form_transaction #total_contract_price').val()
    );
    collectible_price = parseFloat(
        $('#form_transaction #collectible_price').val()
    );
    loan_amount_deduction = parseFloat(
        $('#form_transaction #loan_amount_deduction').val()
    );

    promo_amount = 0;
    promos = [];
    tcp_deductions = [];
    loan_deductions = [];
    loan_deduction_amount = 0;
    loans = [];

    $('.promoID')
        .find('option:selected')
        .each(function (index, element) {
            // Deduct to values
            // 1 = Deduct to Loan
            // 2 = Deduct to TCP (Total Collectible Price)
            var value = $(element).val();
            var amount = $(element)
                .closest('[data-repeater-item]')
                .find('.promoPrice')
                .val();

            if (value == 1) {
                loan_deductions.push(amount);
            } else if (value == 2) {
                tcp_deductions.push(amount);
            }
            console.log('Selected');
            // Function related to promos table
            // var value = parseFloat($(element).attr('data-value'));
            // var value_type = $(element).attr('data-value-type'); // 1 = percentage, 2 = currency
            // var promo_type = $(element).attr('data-promo-type'); // 1 = individual, 2 = compounded
            // var deduct_to = $(element).attr('data-deduct-to'); // 1 = tsp, 2 = tcp, 3 = collectible, 4 = tlp, 5 = thp, 6 = loan_amount

            // new_total_contract_price = getNewAmount(
            //     promos,
            //     promo_type,
            //     total_contract_price
            // );

            // promo_amount = getComputedAmount(
            //     value,
            //     value_type,
            //     new_total_contract_price
            // );

            // formula = getFormula($(element), 2);

            // if (deduct_to == 6) {
            //     loan_deduction_amount = getComputedAmount(
            //         value,
            //         promo_type,
            //         total_contract_price
            //     );

            //     loans.push(loan_deduction_amount);

            //     new_loan_deduction = getLoanDeduction(loans);
            //     formula +=
            //         ' (Amount to be deducted in Billing Information Loan Amount Value)';
            //     $(element)
            //         .closest('[data-repeater-item]')
            //         .find('.promoPrice')
            //         .val(loan_deduction_amount);
            //     $(element)
            //         .closest('[data-repeater-item]')
            //         .find('.formula')
            //         .html(formula);
            //     $('#form_transaction #loan_amount_deduction').va l(
            //         new_loan_deduction
            //     );
            // } else {
            //     new_loan_deduction = getLoanDeduction(loans);
            //     $('#form_transaction #loan_amount_deduction').val(
            //         new_loan_deduction
            //     );
            //     $(element)
            //         .closest('[data-repeater-item]')
            //         .find('.promoPrice')
            //         .val(promo_amount);
            //     $(element)
            //         .closest('[data-repeater-item]')
            //         .find('.formula')
            //         .html(formula);

            //     promos.push(promo_amount);
            // }
        });
};

var computeAdhocFees = function () {
    total_adhoc_fee = $('#total_fee').val();
    fees = [];
    $('#form_modal .feesID')
        .find('option:selected')
        .each(function (index, element) {
            var tcp = $('#tcp').val();
            var value_type = $(element).attr('data-value-type');
            var value = $(element).attr('data-value');
            var amount = 0;

            switch (value_type) {
                case '1':
                    amount = (parseFloat(tcp) / 100) * parseFloat(value);
                    break;
                default:
                    amount = parseFloat(value);
                    break;
            }

            fees.push(formatCurrency(amount));
            adhoc_fee_amount = getAdhocFee(fees);

            $(element)
                .closest('[data-repeater-item]')
                .find('.feesAmount')
                .val(formatCurrency(parseFloat(amount)));
            $('#total_fee').val(adhoc_fee_amount);
        });
};

var getNewAmount = function (amt = [], type = 1, full_amount = 0) {
    if (type == 1) {
        amount = full_amount;
    } else if (type == 2) {
        amount = 0;

        for (var i = 0; i < amt.length; i++) {
            amount += amt[i] << 0;
        }

        amount += full_amount;
    }

    return formatCurrency(amount);
};

var getTCP = function (val = 0) {
    total_selling_price = parseFloat(
        $('#form_transaction #total_selling_price').val()
    );
    vat_amount = parseFloat($('#form_transaction #vat_amount').val());
    adjusted_price = parseFloat($('#form_transaction #adjusted_price').val());

    fee_amount = 0;
    total_fee_amount = 0;

    $('.feesPrice').each(function (index, element) {
        fee_amount = parseFloat($(element).val());
        total_fee_amount += fee_amount;
    });

    if (isNaN(total_fee_amount)) {
        total_fee_amount = 0;
    }
    if (isNaN(vat_amount)) {
        vat_amount = 0;
    }

    if (isNaN(adjusted_price)) {
        adjusted_price = 0;
    }

    total_contract_price =
        total_selling_price + total_fee_amount + adjusted_price;

    // if (check_inclusive() == false) {
    //     total_contract_price = total_contract_price + vat_amount;
    // }

    $('#total_contract_price').val(formatCurrency(total_contract_price));

    additionalInfo();
};

var getAdhocSchedule = function () {
    terms = $('#terms').val();
    effectivity_date = $('#effectivity_date').val();
    amount = $('#total_fee').val();
    tcp = $('#tcp').val();

    $.ajax({
        url:
            base_url +
            'transaction/process_adhoc_schedule/' +
            terms +
            '/' +
            effectivity_date +
            '/' +
            amount +
            '/' +
            tcp,
        dataType: 'json',
        success: function (data) {
            $('#billing_table tbody').empty();
            $.each(data, function (i, item) {
                var row = '<tr>';

                $.each(item, function (x, value) {
                    if (x != 'month') {
                        if (
                            x == 'beginning_balance' ||
                            x == 'ending_balance' ||
                            x == 'amount'
                        ) {
                            row +=
                                '<td>' + parseFloat(value).toFixed(2) + '</td>';
                        } else {
                            row += '<td>' + value + '</td>';
                        }
                    }
                });
                row += '</tr>';
                $('#billing_table').append(row);
            });
        },
    });
};

var additionalInfo = function () {
    total_contract_price = parseFloat(
        $('#form_transaction #total_contract_price').val()
    );

    total_paid_principal = parseFloat(
        $('#form_transaction #total_paid_principal').attr('data-value')
    );

    remaining_balance_to_pay =
        parseFloat(total_contract_price) - parseFloat(total_paid_principal);

    $('#form_transaction #new_contact_price').html(
        moneyFormat(total_contract_price)
    );
    $('#form_transaction #remaining_balance_to_pay').html(
        moneyFormat(remaining_balance_to_pay)
    );
    $('#form_transaction #remaining_balance_to_pay').attr(
        'data-value',
        remaining_balance_to_pay
    );

    // Calculate remaining amount per period
    // balance = (remaining_amount * period_rate_percentage) / 100
    if ($('#type').val() == 'property') {
        $('#form_transaction .period_amount').each(function (idx, el) {
            balance_per_period =
                (total_contract_price *
                    parseFloat(
                        $('input[id="period_rate_p"]:eq(' + idx + ')').val()
                    )) /
                100;
            switch (idx) {
                case 0:
                    re_paid_amount = parseFloat(
                        $('#form_transaction #re_paid_amount').val()
                    );
                    dp_paid_amount = parseFloat(
                        $('#form_transaction #dp_paid_amount').val()
                    );
                    dp_deduction = re_paid_amount + dp_paid_amount;
                    balance_per_period -= dp_deduction;
                    break;
                case 1:
                    lo_paid_amount = parseFloat(
                        $('#form_transaction #lo_paid_amount').val()
                    );
                    balance_per_period -= lo_paid_amount;
            }

            $(this).val(balance_per_period);
        });
    }
};

var getCP = function (val = 0) {
    total_contract_price = parseFloat(
        $('#form_transaction #total_contract_price').val()
    );

    if (isNaN(total_contract_price)) {
        total_contract_price = parseFloat(
            $('#total_selling_price').val()
        );
    }
    

    // promo_amount = 0;
    // total_promo_amount = 0;
    loan_amount = 0;
    tcp_amount = 0;

    $('.promoPrice').each(function (index, element) {
        var deduct_to = $(element)
            .closest('[data-repeater-item]')
            .find('.promoID')
            .val();
        var value = $(element).val();

        if (deduct_to == 1) {
            loan_amount += parseFloat(value);
        } else if (deduct_to == 2) {
            tcp_amount += parseFloat(value);
        }

    //     // Function related to promos table
    //     if ($('.promoID').eq(index).find(':selected').data('deduct-to') == 6) {
    //         promo_amount = 0;
    //     } else {
    //         promo_amount = parseFloat($(element).val());
    //     }
    //     total_promo_amount += promo_amount;
    });

    // if (isNaN(total_promo_amount)) {
    //     total_promo_amount = 0;
    // }

    if (isNaN(promo_amount)) {
        promo_amount = 0;
    }
    if (isNaN(tcp_amount)) {
        tcp_amount = 0;
    }

    // promos = promo_amount + total_promo_amount;

    // initial_cp = promos + tcp_amount;

    // collectible_price = total_contract_price - initial_cp;
    collectible_price = total_contract_price - tcp_amount;
    console.log(total_contract_price);
    console.log(loan_amount);
    console.log(collectible_price);
    // $('#loan_amount_deduction').val(formatCurrency(promos));
    $('#loan_amount_deduction').val(formatCurrency(loan_amount));
    $('#collectible_price').val(formatCurrency(collectible_price));
};

var getLA = function (val = 0) {
    amount = 0;
    $('.promoID').each(function (index, element) {
        var deduct_to = $(element).val();
        var value = $(element)
            .closest('[data-repeater-item')
            .find('.promoPrice')
            .val();
        if (deduct_to == 2) {
            amount += parseFloat(value);
        }
        // Function related to promos table
        // Function related to promos table
        // if ($(element).find(':selected').data('deduct-to') == 6) {
        //     amount =
        //         parseFloat(amount) +
        //         parseFloat($('.promoPrice').eq(index).val());
        // }
    });
    loan_amount = formatCurrency(amount);

    $('#form_transaction #loan_amount_deduction').val(loan_amount);
};

// Private functions
var datepicker = function () {
    // minimum setup
    $('.datePicker').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: 'bottom left',
        templates: arrows,
        locale: 'no',
        format: 'yyyy-mm-dd',
        autoclose: true,
    });

    $('.yearPicker').datepicker({
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        format: 'yyyy',
        viewMode: 'years',
        minViewMode: 'years',
        autoclose: true,
    });
};

var loadScheme = function () {
    val = $('#financing_scheme_id').val();
    cp = $('#collectible_price').val();
    rd = $('#reservation_date').val();
    $.ajax({
        url:
            base_url +
            'financing_scheme/view/' +
            val +
            '/transaction/' +
            cp +
            '/' +
            rd,
        dataType: 'json',
        success: function (response) {
            $('.financing_scheme_steps').html(response.html);
            $('#expiration_date').val(response.expiration_date);
            datepicker();
            setTimeout(() => {
                loan_amount_deduction = $(
                    '#form_transaction #loan_amount_deduction'
                ).val();
                let loan_period_amount = $(
                    '#financing_scheme_form #period_amount_3'
                ).val();

                loan_period_amount -= loan_amount_deduction;
                $('#financing_scheme_form #period_amount_3').val(
                    formatCurrency(loan_period_amount)
                );
            }, 1000);
        },
    });
};

var vatSetting = function () {
    is_lot = $('#form_transaction #is_lot').val()
        ? $('#form_transaction #is_lot').val()
        : 0;
    sp = parseFloat($('#form_transaction #total_selling_price').val());

    $.ajax({
        url: base_url + 'transaction/get_tax_amount/' + sp + '/' + is_lot,
        dataType: 'json',
        success: function (response) {
            va = formatCurrency(parseFloat(response.amount));

            $('#vat_amount').val(va);
        },
    });
};

var check_inclusive = function () {
    i = $('#is_inclusive').val();

    if (i == 1) {
        return true;
    } else {
        return false;
    }
};

var getTaxRate = function () {
    let tax_rate = parseFloat($('#wht_rate').val());
    let tax_description = '';

    if (tax_rate == 0) {
        tax_description = 'Transaction is less than 150 000.00';
    } else if (tax_rate == 1.5) {
        tax_description = 'Transaction is less than 500 000.00';
    } else if (tax_rate == 3) {
        tax_description =
            'Transaction is more than 500000.00 but Less than 2 000 000.00';
    } else if (tax_rate == 5) {
        tax_description = 'Transaction is more than 2 000 000.00';
    } else if (tax_rate == 7.5) {
        tax_description = 'Seller is not habitually engaged in real estate';
    }

    $('#tax_description').val(tax_description);
};

var getCommValue = function () {
    total_selling_price = parseFloat(
        $('#form_transaction #total_selling_price').val()
    );
    total_contract_price = parseFloat(
        $('#form_transaction #total_contract_price').val()
    );
    collectible_price = parseFloat(
        $('#form_transaction #collectible_price').val()
    );
    val = $('#form_transaction #commissionable_type_id').val();

    amt = 0;
    remarks = '';

    if (val == 1) {
        amt = total_selling_price;
        remarks = 'total selling price';
    } else if (val == 2) {
        amt = total_contract_price;
        remarks = 'total contract price';
    } else if (val == 3) {
        amt = collectible_price;
        remarks = 'collectible price';
    } else if (val == 4) {
        // fe = 60000 / 1.155;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 100000) / 1.155;
        remarks =
            'For H&L with TCP of 2,499,000 and below 5% or 4% of Expenses Exclusive TCP (Gross TCP less P100,000 fixed expenses divided by 1.155)';
    } else if (val == 5) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.225;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.225)';
    } else if (val == 6) {
        // fe = 60000 / 1.155;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.155;
        remarks =
            'For H&L with TCP of 2,499,000 and below 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.155)';
    } else if (val == 7) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.135;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P60,000 fixed expenses divided by 1.135)';
    } else if (val == 8) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 50000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.105)';
    } else if (val == 9) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 40000) / 1.155;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.155)';
    } else if (val == 10) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 40000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P40,000 fixed expenses divided by 1.105)';
    } else if (val == 11) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 35000) / 1.105;
        remarks =
            'For H&L with TCP of 2,500,000 and above 5% or 4% of Expenses Exclusive TCP (Gross TCP less P35,000 fixed expenses divided by 1.105)';
    } else if (val == 12) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'House and Lot (Andrea, Cassandra. Elysa, Freya, Gabrielle) (total_contract_price / 1.12) / 1.155';
    } else if (val == 13) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 60000) / 1.225;
        remarks =
            'Sitio Uno (total_contract_price - 60000) / 1.225';
    } else if (val == 14) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price - 100000) / 1.155;
        remarks =
            'Twin Hearts Residences';
    } else if (val == 15) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'Lots Only (Except Socialized Lots) (total_contract_price / 1.12) / 1.155';
    } else if (val == 16) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'St. Joseph (Lots Only) ** (total_contract_price / 1.12) / 1.155';
    } else if (val == 17) {
        // fe = 60000 / 1.235;
        // amt = total_contract_price - fe;
        amt = (total_contract_price / 1.12) / 1.155;
        remarks =
            'Computation #9 (total_contract_price / 1.12) / 1.155';
    }

    if (isNaN(amt)) {
        amt = 0;
    }

    $('#commissionable_amount').val(amt.toFixed(2));
    $('#commissionable_amount_remarks').val(remarks);
};

var sellerComp = function () {
    commissionable_amount = parseFloat($('#commissionable_amount').val());

    if (isNaN(commissionable_amount)) {
        return true;
    }

    $('.sellers_rate').each(function (index, element) {
        var value = parseFloat($(element).val());

        comm_amount = commissionable_amount * (value / 100);

        $(element)
            .closest('[data-repeater-item]')
            .find('.sellers_rate_amount')
            .val(comm_amount.toFixed(2));
    });
};

var updateDateEffectivity = function () {
    new_date = '';

    $('#form_transaction .terms').each(function (index, element) {
        if (new_date) {
            $(element)
                .closest('[data-repeater-item]')
                .find('.effectivity_dates')
                .val(new_date);
        }

        var term = parseFloat($(element).val());
        var date = $(element)
            .closest('[data-repeater-item]')
            .find('.effectivity_dates')
            .val();

        var second_date = new Date(date);
        s_date = second_date.setMonth(second_date.getMonth() + term);

        s_date = new Date(s_date);
        s_month =
            s_date.getMonth() + 1 <= 9
                ? '0' + (s_date.getMonth() + 1)
                : s_date.getMonth() + 1;
        s = s_date.getFullYear() + '-' + s_month + '-' + s_date.getDate();

        new_date = s;
    });
};

var updateDateEffectivity_2 = function (pid) {
    new_date = '';

    $('#form_transaction .effectivity_dates').each(function (index, element) {
        var period_id = $(this).attr('data-period-id');

        if (period_id >= pid) {
            if (new_date) {
                $(element)
                    .closest('[data-repeater-item]')
                    .find('.effectivity_dates')
                    .val(new_date);
            }

            var date = $(element).val();
            var term = parseInt(
                $(element).closest('[data-repeater-item]').find('.terms').val()
            );

            var second_date = new Date(date);
            s_date = second_date.setMonth(second_date.getMonth() + term);

            s_date = new Date(s_date);
            s_month =
                s_date.getMonth() + 1 <= 9
                    ? '0' + (s_date.getMonth() + 1)
                    : s_date.getMonth() + 1;
            s = s_date.getFullYear() + '-' + s_month + '-' + s_date.getDate();

            new_date = s;
        }
    });
};

var updateSchemeAmount = function () {
    cp = $('#collectible_price').val();
    tsp = $('#total_selling_price').val();
    tcp = $('#total_contract_price').val();

    tsp = parseFloat($('#remaining_balance_to_pay').attr('data-value'));
    var total = 0;
    var ra_amount = 0;

    $('#form_transaction .percentage_rate').each(function (index, element) {
        var percentage_rate = parseFloat($(element).val());
        total = percentage_rate + total;
    });

    if (total > 100) {
        swal.fire('Oops!', 'Total Percentage is Greater than 100', 'error');
        return false;
    }

    $('#form_transaction .percentage_rate').each(function (index, element) {
        var period_id = $(this).attr('data-period-id');
        var percentage_rate = parseFloat($(element).val());
        var period_amount = parseFloat(
            $(element)
                .closest('[data-repeater-item]')
                .find('.period_amount')
                .val()
        );
        var new_amount = 0;

        if (period_id == 1) {
            var ra_amount = parseFloat(
                $(element)
                    .closest('[data-repeater-item]')
                    .find('.period_amount')
                    .val()
            );
        } else {
            if (period_id == 2) {
                new_amount = getComputedAmount(
                    percentage_rate,
                    1,
                    tsp,
                    ra_amount
                );
            } else {
                new_amount = getComputedAmount(percentage_rate, 1, tsp);
            }
            // alert(new_amount)
            $(element)
                .closest('[data-repeater-item]')
                .find('.period_amount')
                .val(new_amount);
        }
    });
};

var populateDataInterior = function (val = 0) {
    $.ajax({
        url: base_url + 'interior/view/' + val + '/json',
        dataType: 'json',
        success: function (response) {
            console.log(response);
            $.each(response, function (index, value) {
                if (index == 'interior') {
                    // console.log(value);

                    // alert('index: '+index+' value: '+value);

                    $.each(value, function (int_key, int_val) {
                        // alert('int_key: '+int_key+' int_val: '+int_val);

                        if (int_key == 'price') {
                            $('.popField#model_price').val(int_val);
                        }

                        if (int_key == 'house') {
                            $.each(int_val, function (house_key, house_val) {
                                // alert(
                                //     'house_key: ' +
                                //         house_key +
                                //         ' house_val: ' +
                                //         house_val
                                // );

                                if (
                                    house_key == 'lot_area' ||
                                    house_key == 'floor_area'
                                ) {
                                    orig_loa = parseFloat(
                                        $('#form_transaction #lot_area').attr(
                                            'data-original'
                                        )
                                    );
                                    orig_floa = parseFloat(
                                        $('#form_transaction #floor_area').attr(
                                            'data-original'
                                        )
                                    );

                                    if (
                                        orig_loa == 0 ||
                                        orig_loa == 0.0 ||
                                        orig_loa == '' ||
                                        orig_loa == '0.00' ||
                                        (isNaN(orig_loa) &&
                                            house_key == 'lot_area')
                                    ) {
                                        $('.popField#' + house_key).val(
                                            house_val
                                        );
                                        // alert("success floa");
                                    } else {
                                        // alert(orig_loa);
                                    }

                                    if (
                                        orig_loa == 0 ||
                                        orig_loa == 0.0 ||
                                        orig_loa == '' ||
                                        orig_loa == '0.00' ||
                                        (isNaN(orig_loa) &&
                                            house_key == 'floor_area')
                                    ) {
                                        $('.popField#' + house_key).val(
                                            house_val
                                        );
                                    } else {
                                        // alert("success floa");
                                        // alert(orig_floa);
                                    }
                                } else {
                                    $('.popField#' + house_key).val(house_val);
                                }
                            });
                        }
                    });
                }

                if (index == 'price_logs') {
                    $.each(value, function (ind, val) {
                        console.log(ind + ' ' + val);
                    });
                }
            });
        },
    });
};

var populateDataProperty = function (val = 0) {
    $.ajax({
        url: base_url + 'property/view/' + val + '/json',
        dataType: 'json',
        success: function (response) {
            $('#adjusted_price').val(response.info.adjusted_price);
            $.each(response.info, function (index, value) {
                if (index == 'model') {
                    // alert(value.id+' '+value.name);
                    // $('.popField#house_model_id').select2('data', {id: value.id, text: value.name});
                    if (value) {
                        $('.popField#house_model_id')
                            .append(
                                "<option value='" +
                                    value.id +
                                    "' selected>" +
                                    value.name +
                                    '</option>'
                            )
                            .select2();
                        $('.popField#is_lot').val(value.is_lot);
                    }
                } else if (index == 'interior') {
                    if (value) {
                        // alert(value.id+' '+value.name);
                        // $('.popField#interior_id').select2('data', {id: value.id, text: value.name});
                        $('.popField#interior_id')
                            .append(
                                "<option value='" +
                                    value.id +
                                    "' selected>" +
                                    value.name +
                                    '</option>'
                            )
                            .select2();
                    }
                } else if (index == 'model_price') {
                    $('.popField#total_house_price').val(value);
                } else {
                    if (isNaN(value)) {
                        value = 0.0;
                    }

                    $('.popField#' + index).val(value);

                    if (index == 'lot_area' || index == 'floor_area') {
                        $('.popField#' + index).attr('data-original', value);
                    }
                }
            });
        },
    });
};

var auditTrailSide = function () {
    $.ajax({
        url: base_url + 'audit_trail/side',
        dataType: 'json',
        success: function (response) {
            $('#side_notification').html(response.html);
            $('#dashboard_audit_trail').html(response.html);
        },
    });
};

var loginHistorySide = function () {
    $.ajax({
        url: base_url + 'login_history/side',
        dataType: 'json',
        success: function (response) {
            $('#side_login_history').html(response.html);
            $('#dashboard_login_history').html(response.html);
        },
    });
};

var loanCalculator = function () {
    calc_amount = parseFloat($('#calc_amount').val());
    calc_interest = parseFloat($('#calc_interest').val());
    calc_term = parseFloat($('#calc_term').val());

    if (isNaN(calc_amount) || isNaN(calc_interest) || isNaN(calc_term)) {
        swal.fire({
            title: 'Error',
            text:
                'There are some errors in your submission. Please correct them.',
            type: 'error',
            confirmButtonClass: 'btn btn-secondary',
        });
        return false;
    }

    $.ajax({
        url:
            base_url +
            'suggests/auth/calculator/' +
            calc_amount +
            '/' +
            calc_interest +
            '/' +
            calc_term,
        dataType: 'json',
        success: function (response) {
            $('#calc_result').html(response.html);
        },
    });
};

var pdCalculator = function () {
    buyer_id = parseFloat($('#pd_buyer_id').val());
    property_id = parseFloat($('#pd_property_id').val());

    if (isNaN(buyer_id) || isNaN(property_id)) {
        swal.fire({
            title: 'Error',
            text:
                'There are some errors in your submission. Please correct them.',
            type: 'error',
            confirmButtonClass: 'btn btn-secondary',
        });
        return false;
    }

    $.ajax({
        url:
            base_url +
            'suggests/auth/pdcalculator/' +
            buyer_id +
            '/' +
            property_id,
        dataType: 'json',
        success: function (response) {
            $('#pd_result').html(response.html);
        },
    });
};

function populateAddress() {
    var region = $('#region');
    var province = $('#province');
    var city = $('#city');
    var barangay = $('#brgy');

    if (region.data('val')) {
        $(region).val(region.data('val'));
        getProvinceList(region.data('val'));
        setTimeout(() => {
            $(province).val(province.data('val'));
            getCityList(province.data('val'));
        }, 500);
        setTimeout(() => {
            $(city).val(city.data('val'));
            getBarangayList(city.data('val'));
        }, 500);
        setTimeout(() => {
            $(barangay).val(barangay.data('val'));
        }, 500);
    }
}

$(document).ready(function () {
    // turn off functionality
    // auditTrailSide();
    // loginHistorySide();

    populateAddress();
});

$(document).on('click', '#btn_loan_calculator', function (e) {
    e.preventDefault();
    loanCalculator();
});

$(document).on('click', '#btn_pd_calculator', function (e) {
    e.preventDefault();
    pdCalculator();
});

jQuery(document).on('change', 'select#country', function (e) {
    e.preventDefault();
    var countryID = jQuery(this).val();
    if (countryID === '169') {
        getRegionList(countryID);
    }
});

function getRegionList(countryID) {
    $.ajax({
        url: base_url + 'suggests/auth/regions',
        type: 'post',
        data: {
            countryID,
            countryID,
        },
        dataType: 'json',
        beforeSend: function () {
            jQuery('select#country').find('option:eq(0)').html('Please wait..');
            $('#region').prop('disabled', false);
            $('#province').prop('disabled', false);
            $('#city').prop('disabled', true);
            $('#brgy').prop('disabled', true);
        },
        complete: function () {},
        success: function (json) {
            var options = '';
            options += '<option value="">Select Region</option>';
            for (var i = 0; i < json.length; i++) {
                options +=
                    '<option value="' +
                    json[i].regCode +
                    '">' +
                    json[i].regDesc +
                    '</option>';
            }

            jQuery('select#region').html(options);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(
                thrownError +
                    '\r\n' +
                    xhr.statusText +
                    '\r\n' +
                    xhr.responseText
            );
        },
        timeout: 10000,
        async: false,
    });
}

jQuery(document).on('change', 'select#region', function (e) {
    e.preventDefault();
    var regionID = jQuery(this).val();
    getProvinceList(regionID);
});

function getProvinceList(regionID) {
    $.ajax({
        url: base_url + 'suggests/auth/provinces',
        type: 'post',
        data: {
            regionID,
            regionID,
        },
        dataType: 'json',
        beforeSend: function () {
            jQuery('select#province')
                .find('option:eq(0)')
                .html('Please wait..');
            $('#province').prop('disabled', false);
            $('#city').prop('disabled', true);
            $('#brgy').prop('disabled', true);
        },
        complete: function () {},
        success: function (json) {
            var options = '';
            options += '<option value="">Select Province</option>';
            for (var i = 0; i < json.length; i++) {
                options +=
                    '<option value="' +
                    json[i].provCode +
                    '">' +
                    json[i].provDesc +
                    '</option>';
            }

            jQuery('select#province').html(options);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(
                thrownError +
                    '\r\n' +
                    xhr.statusText +
                    '\r\n' +
                    xhr.responseText
            );
        },
        timeout: 10000,
        async: false,
    });
}

jQuery(document).on('change', 'select#province', function (e) {
    e.preventDefault();
    var provinceID = jQuery(this).val();
    getCityList(provinceID);
});

function getCityList(provinceID) {
    $.ajax({
        url: base_url + 'suggests/auth/cities',
        type: 'post',
        data: {
            provinceID,
            provinceID,
        },
        dataType: 'json',
        beforeSend: function () {
            jQuery('select#city').find('option:eq(0)').html('Please wait..');
            $('#city').prop('disabled', false);
            $('#brgy').prop('disabled', true);
        },
        complete: function () {},
        success: function (json) {
            var options = '';
            options += '<option value="">Select City</option>';
            for (var i = 0; i < json.length; i++) {
                options +=
                    '<option value="' +
                    json[i].citymunCode +
                    '">' +
                    json[i].citymunDesc +
                    '</option>';
            }

            jQuery('select#city').html(options);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(
                thrownError +
                    '\r\n' +
                    xhr.statusText +
                    '\r\n' +
                    xhr.responseText
            );
        },
        timeout: 10000,
        async: false,
    });
}

jQuery(document).on('change', 'select#city', function (e) {
    e.preventDefault();
    var cityID = jQuery(this).val();
    getBarangayList(cityID);
});

function getBarangayList(cityID) {
    $.ajax({
        url: base_url + 'suggests/auth/barangays',
        type: 'post',
        data: {
            cityID,
            cityID,
        },
        dataType: 'json',
        beforeSend: function () {
            jQuery('select#brgy').find('option:eq(0)').html('Please wait..');
            $('#brgy').prop('disabled', false);
        },
        complete: function () {},
        success: function (json) {
            var options = '';
            options += '<option value="">Select Barangay</option>';
            for (var i = 0; i < json.length; i++) {
                options +=
                    '<option value="' +
                    json[i].brgyCode +
                    '">' +
                    json[i].brgyDesc +
                    '</option>';
            }

            jQuery('select#brgy').html(options);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(
                thrownError +
                    '\r\n' +
                    xhr.statusText +
                    '\r\n' +
                    xhr.responseText
            );
        },
        timeout: 10000,
        async: false,
    });
}

// begin:Item module get tax
jQuery(document).on('change', 'select#tax_id', function (e) {
    e.preventDefault();
    var taxID = $('#tax_id option:selected').val();

    getTax(taxID);
    getTotalPrice();
});

jQuery(document).on('change keyup blur', '#unit_price', function (e) {
    getTotalPrice();
});

function getTax(taxID) {
    $.ajax({
        url: base_url + 'suggests/auth/tax',
        type: 'post',
        data: {
            taxID,
            taxID,
        },
        dataType: 'json',
        complete: function () {},
        success: function (json) {
            let rate = json.filter((tax) => tax.id === taxID);
            $('#tax_rate').val(rate[0]['rate']);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(
                thrownError +
                    '\r\n' +
                    xhr.statusText +
                    '\r\n' +
                    xhr.responseText
            );
        },
        timeout: 10000,
        async: false,
    });
}

function getTotalPrice() {
    var tax_rate = $('#tax_rate').val();
    var unit_price = $('#unit_price').val();
    var total;
    if (tax_rate) {
        var tax = (unit_price / 100) * tax_rate;
        total = parseFloat(unit_price) + parseFloat(tax);
    } else {
        total = parseFloat(unit_price);
    }
    $('#total_price').val(total.toFixed(2));
}
// end:Item module get tax

// begin:Payment request

jQuery(document).on('change', 'select#payee_type', function (e) {
    e.preventDefault();
    var payeeTypeValue = $('#payee_type option:selected').val();

    if (
        payeeTypeValue == 'sellers' ||
        payeeTypeValue == 'buyers' ||
        payeeTypeValue == 'staff'
    ) {
        if (document.getElementById('payee_type_id')!=null){
            document
            .getElementById('payee_type_id')
            .setAttribute('data-type', 'person');
        }
        else{
            document
            .getElementById('payee_id')
            .setAttribute('data-type', 'person');
        }

    } else {
        if (document.getElementById('payee_type_id')!=null){
            document
            .getElementById('payee_type_id')
            .setAttribute('data-type', 'payee');
        }
        else{
            document
            .getElementById('payee_id')
            .setAttribute('data-type', 'payee');
        }
    }
    if (document.getElementById('payee_type_id')!=null){
        document
        .getElementById('payee_type_id')
        .setAttribute('data-module', payeeTypeValue);
    }
    else{
        document
        .getElementById('payee_id')
        .setAttribute('data-module', payeeTypeValue);
    }
        
});

jQuery(document).on('change', 'select#entry_payee_type', function (e) {
    e.preventDefault();
    var payeeTypeValue = $('#entry_payee_type option:selected').val();

    document
        .getElementById('entry_payee_type_id')
        .setAttribute('data-module', payeeTypeValue);
});

jQuery(document).on(
    'change',
    'select#payment_voucher_payee_type',
    function (e) {
        e.preventDefault();
        var payeeTypeValue = $(
            '#payment_voucher_payee_type option:selected'
        ).val();

        document
            .getElementById('payment_voucher_payee_type_id')
            .setAttribute('data-module', payeeTypeValue);
    }
);

jQuery(document).on('change keyup blur', '#gross_amount', function (e) {
    calculatePayable();
});

jQuery(document).on('change keyup blur', '#tax_percentage', function (e) {
    calculatePayable();
});

jQuery(document).on('change keyup blur', '#wht_percentage', function (e) {
    calculatePayable();
});




function calculatePayable() {
    var gross = $('#gross_amount').val();
    var tax = $('#tax_percentage').val();
    var wht = $('#wht_percentage').val();

    var taxDec = (tax / 100).toFixed(2);
    var vatAmount = parseFloat(gross) * parseFloat(taxDec);

    var whtDec = (wht / 100).toFixed(2);
    var whtAmount = parseFloat(gross) * parseFloat(whtDec);

    var netAmount = parseFloat(gross) - parseFloat(vatAmount);
    var dueAmount =
        parseFloat(gross) + parseFloat(vatAmount) - parseFloat(whtAmount);

    $('#vat_tax_amount').val(vatAmount.toFixed(2));
    $('#wht_amount').val(whtAmount.toFixed(2));
    $('#net_amount').val(netAmount.toFixed(2));
    $('#total_due_amount').val(dueAmount.toFixed(2));
}

// end:Payment request

function formatMoney(number) {
    var sign = '₱ ';
    number = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return sign.concat(number);
}

$('#penalty_amount, #amount_paid, #check_deposit_amount, #adhoc_payment').on(
    'focus',
    function () {
        var parsedVal = parseFloat($(this).val().replace(/,/g, ''));

        $(this).val(parsedVal);
    }
);

$('#penalty_amount, #amount_paid, #check_deposit_amount, #adhoc_payment').on(
    'change',
    function () {
        var val = $(this).val();
        var formatMoney = parseFloat(val)
            .toFixed(2)
            .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

        $(this).val(formatMoney);
    }
);


jQuery(document).on('click', '.view_properties_transaction_dashboard', function (e) {

    let month = $(this).attr('data-month');
    let project_name = $(this).attr('data-project-id');
    let mod = $(this).attr('data-mod');

    $.ajax({
        url: base_url + 'transaction/get_properties_dashboard',
        type: 'POST',
        data: {
            month: month,
            project_name: project_name,
            mod: mod,
        },
        dataType: 'json',
        success: function (data) {
            swal.fire('List of Properties', data.view);
        },
    });

});