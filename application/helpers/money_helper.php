<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('vp') ) {

	function vp ( $d = '' ) {

		echo '<pre>';
			var_dump($d);
		echo '</pre>';
	}
}

if ( !function_exists('money_php') ) {

	function money_php ( $_string = FALSE, $_sign = FALSE  ) {

		$_return	=	'';

		// if ( $_string != '' ) {

			$_string	=	number_format($_string, 2);

			if ( $_sign ) {

				$_return	=	$_sign.' '.$_string;
			} else {

				$_return	=	'&#8369;  '.$_string;
			}
		// }

		return $_return;
	}
}


if ( !function_exists('get_percentage_amount') ) {

	function get_percentage_amount ( $_amount = 0, $_percentage = 0  ) {

		$_return	=	'';

		if ( $_amount  ) {

			$_return = $_amount * ( $_percentage / 100 );
			
		}

		return $_return;
	}
}

if ( !function_exists('get_percentage') ) {

	function get_percentage ( $target = 0, $actual = 0  ) {

		$_return	=	0;
		
		if(($target != 0)&&($actual != 0)){

			$_return = ( $actual / $target );

		}


		return $_return;
	}
}


if ( !function_exists('get_percentage_reverse') ) {

	function get_percentage_reverse ( $target = 0, $actual = 0  ) {

		$_return	=	0;


		$_return = ( $actual / $target );
			

		return $_return - 100;
	}
}

if ( !function_exists('get_net') ) {

	function get_net ( $_amount = 0, $_percentage = 0  ) {

		$_return	=	'';

		if ( $_amount  ) {

			$_return = $_amount * ( $_percentage / 100 );

			$_return = $_amount - $_return;
		}

		return $_return;
	}
}

if ( !function_exists('get_net_amount') ) {
	function get_net_amount ( $_amount = 0, $_percentage = 0, $first_release_amount, $period_id, $key  ) {

		$_return	=	'';

		if($period_id == 1) {
			$wht_amount = $first_release_amount * ( $_percentage / 100 );
			return $first_release_amount;
		}

		if( ($period_id == 2) && ($key == 1) ) {

			$_amount = $_amount - $first_release_amount;

			$wht_amount = $_amount * ( $_percentage / 100 );

			return $_amount - $wht_amount;
		} else {
			$wht_amount = $_amount * ( $_percentage / 100 );
			
			return $_amount - $wht_amount;
		}

		// if ( $_amount  ) {
		// 	$wht_amount = $_amount * ( $_percentage / 100 );
		// 	$net_commission_amount = $_amount - $wht_amount;
			
		// 	$_return = ( $net_commission_amount - $first_release_amount ) / 2;
		// }

		return $_return;
	}
}


