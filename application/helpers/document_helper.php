<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_document_img'))
{

	function get_document_img($folder = false, $file_name = FALSE, $title = '')
	{
        if($folder && $file_name){
            $file = './assets/uploads/form_generator/'.$folder.'/'.$file_name;

            if(file_exists($file)){
                // returm image
                $data = '<img src="'.$file.'" width="100%" height="100%"/>';
            }else{
                // return title
                $data = '<h1 align="center">'.$title.'</h1>';
            }

            return $data;
        }
        
                
	}
}
