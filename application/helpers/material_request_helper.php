<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    if (!function_exists('mr_status_array')) {
        function mr_status_array()
        {
            /*
            Returns text value of numeric status
               0 - New Request
               1 - For RPO
               2 - For Issuance
               3 - Issued
               4 - Cancelled
            */
            $status_list = array(
                0 => 'Select Option',
                1 => 'New Request',
                2 => 'For RPO',
                3 => 'For Issuance',
                4 => 'Issued',
                5 => 'Cancelled',
            );
            return $status_list;
        }
    }

    if (!function_exists('mr_status_lookup')) {
        function mr_status_lookup($key)
        {
            $status_list = mr_status_array();
            return $status_list[$key];
        }
    }

    if ((!function_exists('mr_type_array'))) {
        function mr_type_array()
        {
            $status_list = array(
                "",
                "For Purchase",
                "For Issuance",
                "For Warehouse Transfer"
            );
            return $status_list;
        }
    }

    if (!function_exists('mr_type_lookup')) {
        function mr_type_lookup($key)
        {
            $status_list = mr_type_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('mr_customer_type_lookup')) {
        function mr_customer_type_lookup($key)
        {
            $array = array(
                '' => 'Select Option',
                'project' => 'Project',
                'sub_project' => 'Sub-Project',
                'amenity' => 'Amenity',
                'sub_amenity' => 'Sub-Amenity',
                'block_and_lot' => 'Block & Lot',
                'department' => 'Department',
                'vehicle_equipment' => 'Vehicle / Equipment',
            );

            if(array_key_exists($key, $array)){
                return $array[$key];
            }
            return null;
        }
    }

    if (!function_exists('mr_customer_type_ref')) {
        function mr_customer_type_ref($key)
        {
            $array = array(
                '' => null,
                'project' => array(
                    'table' => 'projects',
                    'fk' => 'project_id',
                    'controller' => $key
                ),
                'sub_project' => array(
                    'table' => 'sub_projects',
                    'fk' => 'sub_project_id',
                    'controller' => $key
                ),
                'amenity' => array(
                    'table' => 'amenities',
                    'fk' => 'amenity_id',
                    'controller' => $key
                ),
                'sub_amenity' => array(
                    'table' => 'sub_amenities',
                    'fk' => 'sub_amenity_id',
                    'controller' => $key
                ),
                'block_and_lot' => array(
                    'table' => 'properties',
                    'fk' => 'property_id',
                    'controller' => 'property'
                ),
                'department' => array(
                    'table' => 'departments',
                    'fk' => 'department_id',
                    'controller' => $key
                ),
                'vehicle_equipment' => array(
                    'table' => 'vehicle_equipments',
                    'fk' => 'vehicle_equipment_id',
                    'controller' => $key
                ),
            );
            if(array_key_exists($key, $array)){
                return $array[$key];
            }
            return null;
        }
    }

    if (!function_exists('mr_customer_type_data_display')) {
        function mr_customer_type_data_display($request_id)
        {
            // key => (fk => table)
            $dependencies = array(
                'project' => array('project_id' => array('Project', 'projects', 'project')),
                'sub_project' => array('project_id' => array('Project', 'projects', 'project'), 'sub_project_id' => array('Sub Project', 'sub_projects', 'sub_project')),
                'amenity' => array('project_id' => array('Project', 'projects', 'project'), 'amenity_id' => array('Amenity', 'amenities', 'amenity')),
                'sub_amenity' => array('project_id' => array('Project', 'projects', 'project'), 'amenity_id' => array('Amenity', 'amenities', 'amenity'), 'sub_amenity_id', array('Sub Amenity', 'sub_amenities', 'sub_amenity')),
                'block_and_lot' => array('project_id' => array('Project', 'projects', 'project'), 'property_id' => array('Property', 'properties', 'property')),
                'department' => array('department_id' => array('Department', 'departments', 'department')),
                'vehicle_equipment' => array()
            );

            $request = get_object_from_table($request_id, 'material_requests', false);
            $key = $request['customer_type'];
            if ($key) {
                $dependency = $dependencies[$key];

                $customer_type_data = [];
                foreach ($dependency as $column => $table) {
                    if (sizeof($table) > 0) {
                        $fk = $request[$column];
                        $__dep = get_object_from_table($fk, $table[1]);
                        array_push($customer_type_data, array(
                            "name" => $table[0],
                            "data" => $__dep,
                            "url" => $table[2]
                        ));
                    }
                }

                return $customer_type_data;
            } else {
                return null;
            }
        }
    }

    if (!function_exists('mr_status_reverse_lookup')) {
        function mr_status_reverse_lookup($status)
        {
            $arr = mr_status_array();
            foreach ($arr as $key => $value) {
                if ($status == $value) {
                    return true;
                }
            }
            return false;
        }
    }