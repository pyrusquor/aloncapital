<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('monthly_payment'))
{

	function monthly_payment( $full_amount = 0, $interest_rate = 0, $term = 0 ) {
        $CI = &get_instance();
        if (($interest_rate != "0.00")||($interest_rate != "0.000000")||($interest_rate != "0")) {
            return  ( ( $interest_rate / 100 ) / 12 * $full_amount ) / ( 1 - pow( ( 1 + ( $interest_rate / 100 ) / 12 ) , - ( $term ) ) );
        } else {
            if ($full_amount && $term)
                return  ( $full_amount ) / ( $term  );
            else
                return 0;
        }
    }



}

if ( ! function_exists('payment_stats'))
{

    function payment_stats( $days = 0 ) {

        if ($days == 0 ) {
            return "<div class='btn btn-warning'>Due Today</div><br>";
        } else if ($days < 0) {
            return "<div class='btn btn-success'>".abs($days)." day(s) until next collection</div><br>";
        } else if ($days > 0) {
            return "<div class='btn btn-danger' style='color: #fff !important;'>".abs($days).' day(s) overdue</div><br>';
        }
    }

}

if ( ! function_exists('is_due'))
{
    function is_due( $due_date = '' ) {
        $return = false;

        $now = time(); // or your date as well
        $your_date = strtotime($due_date);
        $datediff = $now - $your_date;
        $over_due = floor($datediff/(60*60*24));
        $months = floor($over_due / 30);
        
        if ($over_due > 0) {
           $return = true;
        }

        return $return;
    }
}


if ( ! function_exists('date_diffs'))
{
    function date_diffs( $given_date = '', $due_date = '' ,$type = 'days') {
        $return = 0;

        $now = strtotime($given_date); // or your date as well
        $your_date = strtotime($due_date);
        $datediff = $now - $your_date;
        $over_due = floor($datediff/(60*60*24));
        $months = floor($over_due / 30);

        if ($over_due > 0) { // comment for now, return the negative also
            $return = $over_due;
            if ($type == "months") {
                $return = round($over_due / 30);
            }
        } else if($over_due < 0) {
            $return = $over_due;
        }
        
        return $return;
    }
}

if ( ! function_exists('get_color'))
{
    function get_color($stat = 0,$status = "",$name = "",$period = ""){

        $class = "";
        if ($status == "general") {
            if ($stat == 1) {
                $class = "warning";
            } else if ($stat == 2) {
                $class = "info";
            }else if ($stat == 3) {
                $class = "success";
            }else if ($stat == 4) {
                $class = "danger";
            }else if ($stat == 5) {
                $class = "danger";
            }
        }
        $str = "<div class='btn btn-".$class."'>".$name."<br>".$period."</div>";

        return $str;        
    }
}

 function get_version(){
    $CI = &get_instance();
    $ret = $CI->db->get('migrations')->row();
    return $ret->version;

}

if( ! function_exists('cc_date_diff')) {
    function cc_date_diff($complaint_ticket) {
        $date_now = new DateTime(date('Y-m-d H:i:s'));

        if (!empty($complaint_ticket['updated_at'])) {
            $saved_date = new DateTime($complaint_ticket['updated_at']);
        } else {
            $saved_date = new DateTime($complaint_ticket['created_at']);
        }

        $date_diff = $saved_date->diff($date_now);
        
        if($complaint_ticket['status'] == 5 || $complaint_ticket['status'] == 6 || $complaint_ticket['status'] == 7) {
            $date_value = '--'; 
            $status = 0;
        } else {

            $date_value = $date_diff->format('%r%a day(s) overdue');

            if ($date_diff->d - 3 > 0) {
                $status = 0;
            } else {
                $status = 1;
            }
        }

        $result = array(
            'date_diff' => $date_value,
            'status' => $status,
        );
        return $result;
       
    }
}

if( ! function_exists('cc_status')) {
    function cc_status($customer_care_status, $overdue_status = false) {
        if($customer_care_status == 5) {
            $status_bg_color = 'bg-success';
        } else if($customer_care_status == 6 || $customer_care_status == 7) {
            $status_bg_color = 'bg-dark';
        } else {
            if($overdue_status) {
                $status_bg_color = 'bg-warning';
            } else {
                $status_bg_color = 'bg-danger';
            }
        }
        return $status_bg_color;
    }
}



if( ! function_exists('get_sum')) {
    function get_sum($payments = array(), $type = 0)
    {   
        $zero = 0;
        
        if ($payments) {
            $principal = array_sum(array_column($payments, 'principal_amount'));

            $interest = array_sum(array_column($payments, 'interest_amount'));

            $total = array_sum(array_column($payments, 'total_amount'));

            if ($type == 1) {
                return $principal;
            } else if ($type == 2) {
                return $interest;
            } else {
                return $principal + $interest;
                // return $total;
            }
        } else {
            return $zero;
        }
      
    }
}


if( ! function_exists('get_documentation_category')) {
    function get_documentation_category($property = array())
    {

        $property_id = $property['property_id'];
        $model_id = get_value_field($property_id,'properties','model_id');
        $sub_type_id = get_value_field($model_id,'house_models','sub_type_id');

        $general_category_id = 2;
        if (($sub_type_id == 1) || ($sub_type_id == 2)) {
            $general_category_id = 1;
        }

        return $general_category_id;
    }
}





