<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('generate'))
{

	function generate( $transaction_id = 0, $value = ""  ) {
        $CI = &get_instance();

        $CI->transaction_library->initiate( $transaction_id );

        switch ($value) {
            case 'present_date':
                $result = view_date(NOW);
                break;

            case 'present_month':
                $result = view_date(NOW,'month');
                break;

            case 'present_day':
                $result = view_date(NOW,'day');
                break;
 
            case 'present_year':
                $result = view_date(NOW,'year');
                break;

            case 'cancellation_date':
                $result = view_date(get_value_field($transaction_id,'transactions','cancellation_date'));
                break;

            case 'expiration_date':
                $result = view_date(get_value_field($transaction_id,'transactions','expiration_date'));
                break;

            case 'reservation_date':
                $result = view_date($CI->transaction_library->effectivity_dates(1));
                break;

            case 'downpayment_date':
                $result = view_date($CI->transaction_library->effectivity_dates(2));
                break;

            case 'loan_date':
                $result = view_date($CI->transaction_library->effectivity_dates(3));
                break;

            case 'equity_date':
                $result = view_date($CI->transaction_library->effectivity_dates(4));
                break;

            case 'reservation_total_rate':
                $result = money_php($CI->transaction_library->get_amount(1));
                break;

            case 'downpayment_total_rate':
                $result = money_php($CI->transaction_library->get_amount(2));
                break;

            case 'loan_total_rate':
                $result = money_php($CI->transaction_library->get_amount(3));
                break;

            case 'equity_total_rate':
                $result = money_php($CI->transaction_library->get_amount(4));
                break;

            case 'reservation_monthly_rate':
                $result = money_php($CI->transaction_library->monthly_ammort(1));
                break;

            case 'downpayment_monthly_rate':
                $result = money_php($CI->transaction_library->monthly_ammort(2));
                break;

            case 'loan_monthly_rate':
                $result = money_php($CI->transaction_library->monthly_ammort(3));
                break;

            case 'equity_monthly_rate':
                $result = money_php($CI->transaction_library->monthly_ammort(4));
                break;

            case 'reservation_terms':
                $result = $CI->transaction_library->get_remaining_terms(1,1);
                break;

            case 'downpayment_terms':
                $result = $CI->transaction_library->get_remaining_terms(2,1);
                break;

            case 'loan_terms':
                $result = $CI->transaction_library->get_remaining_terms(3,1);
                break;

            case 'equity_terms':
                $result = $CI->transaction_library->get_remaining_terms(4,1);
                break;

            case 'reservation_interest':
                $result = $CI->transaction_library->interests(1);
                break;

            case 'downpayment_interest':
                $result = $CI->transaction_library->interests(2);
                break;

            case 'loan_interest':
                $result = $CI->transaction_library->interests(3);
                break;

            case 'equity_interest':
                $result = $CI->transaction_library->interests(4);
                break;

            case 'total_amount_due':
                $result = money_php($CI->transaction_library->total_amount_due());
                break;

            case 'total_contract_price':
                $result = money_php($CI->transaction_library->get_total_contract_price());
                break;

            case 'reservation_fee':
                $result = money_php($CI->transaction_library->get_reservation_fee());
                break;

            case 'due_date':
                $result = view_date($CI->transaction_library->due_date());
                break;

            case 'days_delayed':
                $result = $value;
                break;

            case 'months_delayed':
                $result = $value;
                break;

            case 'cancellation_date':
                $result = $CI->transaction_library->cancellation_date();
                break;
            
            case 'cancellation_reason':
                $result = $CI->transaction_library->cancellation_reason();
                break;
            
            case 'number_to_words':
                $result = $value;
                break;
            
            default:
                $result = $value;
                break;
        }


        return $result;

    }


}
