<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/third_party/dompdf/autoload.inc.php'; 

use Dompdf\Dompdf;

function pdf_create($html, $filename='', $stream = TRUE, $paper=array('a4', 'portrait')) 
{    
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    // ini_set('memory_limit', '0');
    ini_set('max_execution_time', 0);
    set_time_limit(0);
    
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
	$dompdf->set_paper($paper[0], $paper[1]);
    $dompdf->render();

    if ($stream) {
        $dompdf->stream($filename.".pdf", array("Attachment" => 0));
    } else {
        return $dompdf->output();
    }
    // if ($stream === 1 || $stream === TRUE) {
        
    //     $dompdf->stream(array(
    //         'compress' => true,
    //         'Attachment' => true
    //     ));
    // } elseif ($stream === 2) {
    //     //save the pdf file on the server
    //     unlink(FCPATH . $filename);
    //     file_put_contents(FCPATH . $filename, $dompdf->output());

    //     $absoluteurl = base_url(str_replace('\\', '/', $filename));

    //     echo '<script>window.location = "' . $absoluteurl . '"';
    // } elseif ($stream === 3) {
    //     return $dompdf->output();
    // } elseif ($stream === 4) {
    //     $dompdf->stream($filename . ".pdf");
    // } elseif ($stream === 5) {
    //     $dompdf->stream($filename,array('Attachment'=>0));
    // } elseif ($stream === 6) {
    //     // FOR STANDARD REPORTS
    //     $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'generated_standard_reports';
    //     $download_path = $path . DIRECTORY_SEPARATOR . $filename . ".pdf";

    //     unlink($download_path);
    //     file_put_contents($download_path, $dompdf->output());

    //     $results['fileName'] = $filename . ".pdf";
    //     $results['path'] = $download_path;
    //     return $results;
    // }
}
?>