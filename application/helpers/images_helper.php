<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_document_img'))
{

	function get_document_img($folder = false, $file_name = FALSE, $title = '')
	{
        if($folder && $file_name){
            $file = './assets/uploads/form_generator/'.$folder.'/'.$file_name;

            if(file_exists($file)){
                // returm image
                $data = '<img src="'.$file.'" width="100%" height="100%"/>';
            }else{
                // return title
                $data = '<h1 align="center">'.$title.'</h1>';
            }

            return $data;
        }
	}
}

if ( ! function_exists('get_house_img'))
{

	function get_house_img($folder = false, $file_name = FALSE)
	{
        if($folder){
            $file = 'assets/uploads/house_models/'.$folder.'/'.$file_name;

            if(file_exists($file) && !empty($file_name)){
                // returm image
                $data = base_url($file);
            }else{
                // return title
                $data = 'http://via.placeholder.com/640x360?text=Not+Available';
            }

            return $data;
        }
	}
}

if ( ! function_exists('get_image'))
{

	function get_image($directory = false, $path = false, $file_name = FALSE)
	{
        if($file_name && $directory && $path){
            $file = 'assets/uploads/'.$directory.'/'.$path.'/'.$file_name;

            if(file_exists($file)){
                // returm image path
                $data = $file;
            }else{
                // return title
                $data = FALSE;
            }

            return $data;
        }
	}
}