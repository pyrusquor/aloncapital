<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists('dropdown_array'))
{
	function dropdown_array($array, $value='id', $name='name', $is_array=false, $test = false)
	{
		$tmp = [''=>''];

		if($is_array)
		{
			foreach($array as $data)
			{
				$tmp[$data[$value]] = $data[$name];
			}
		}
		else
		{

			foreach($array as $data)
			{
				if(! empty($data->children))
				{
					foreach($data->children as $row)
					{
						$tmp[$data->name][$row->{$value}] = $row->{$name};
					}
				}
				else
				{
					$tmp[$data->{$value}] = $data->{$name};
				}
			}
		}

		return $tmp;
	}
}


if( ! function_exists('get_percentage'))
{
	function get_percentage($numerator, $denominator, $symbol='')
	{
		if($denominator)
		{
			return floor(($numerator / $denominator) * 100) . $symbol;
		}
		else
		{
			return '0'.$symbol;
		}
	}
}


/**
 * set array key name from value
 * Usage :: array_key_name(&$array, 'foo1');
 *
 * $array = array(
 * 			array( 'foo'=>'bar', 'foo1'=>'bar1'),
 * 			array( 'foo'=>'hehe', 'foo1'=>'rawr')
 * 		);
 *
 * Result
 * -------------------------
 *	array(
 * 		'bar1' => array( 'foo'=>'bar', 'foo1'=>'bar1'),
 * 		'rawr' => array( 'foo'=>'bar', 'foo1'=>'rawr')
 * );
 *
 */
 
if( ! function_exists('array_key_name'))
{
	function array_key_name(&$array, $key, $is_array = false)
	{
		$new_array = array();

		if($is_array)
		{
			foreach($array as $arr)
			{
				$new_array[$arr[$key]] = $arr;
			}
		}
		else
		{
			foreach($array as $arr)
			{
				$new_array[$arr->$key] = $arr;
			}
		}

		$array = $new_array;

		return $array;
	}
}


if( ! function_exists('seconds_format'))
{
	function seconds_format($time, $display=0)
	{
		$days = floor($time / (60 * 60 * 24));
		$time -= $days * (60 * 60 * 24);

		$hours = floor($time / (60 * 60));
		$time -= $hours * (60 * 60);

		$minutes = floor($time / 60);
		$time -= $minutes * 60;

		$seconds = floor($time);
		$time -= $seconds;

		$str = [];
		$arr = [];

		if($days) $str[] = $days.($days > 1 ? ' days' : ' day');
		if($hours) $str[] = $hours.($hours > 1 ? ' hrs' : ' hr');
		if($minutes) $str[] = $minutes.($minutes > 1 ? ' mins' : ' min');
		if($seconds) $str[] = $seconds.($seconds > 1 ? ' secs' : ' sec');

		if( ! empty($display))
		{
			for($a=0; $a<$display; $a++)
			{
				if( ! empty($str[$a])) $arr[] = $str[$a];
			}
		}
		else
		{
			$arr = $str;
		}

		if( ! empty($arr))
		{
			return implode(', ', $arr);
		}
		return 0;
	}

}


if( ! function_exists('format_currency'))
{
	function format_currency($amount, $currency = false)
	{
		if( ! $currency)
		{
			$currency = "<span class='peso_currency'>&#8369;</span>";
		}
		
		
		return $currency .' '. substr(number_format($amount, 4, '.', ','), 0, -2);
	}
}


if( ! function_exists('toFixed'))
{
	function toFixed($amount)
	{
		return substr(number_format($amount, 4, '.', ''), 0, -2);
		// return number_format($amount, 2, '.', '');
	}
}


/**
 * Slugify a string
 * @param  [type] $text
 * @return [type]
 */
function slugify ($text)
{
	// replace å, ä with a
	$text = str_replace(array('å', 'ä'), 'a', $text);

	// replace ö with o
	$text = str_replace('ö', 'o', $text);

	// replace non letter or digits by -
	$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	// trim
	$text = trim($text, '-');

	// transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	// lowercase
	$text = strtolower($text);

	// remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);


	return $text;
}



if( ! function_exists('object_to_array'))
{
	/*
	 * Recursive conversion of object to array
	 */
	function object_to_array($obj)
	{
		if(is_object($obj)) $obj = (array) $obj;

		if(is_array($obj))
		{
			$new = array();
			foreach($obj as $key => $val)
			{
				$new[$key] = object_to_array($val);
			}
		}
		else
		{
			$new = $obj;
		}

		return $new;
	}
}


if( ! function_exists('array_diff_recursive'))
{
	function array_diff_recursive($aArray1, $aArray2, $exclude=[])
	{
		$aReturn = array();

		if( ! is_array($aArray2))
		{
			return $aArray2;
		}

		foreach ($aArray1 as $mKey => $mValue)
		{
			if(! is_numeric($mKey) && in_array($mKey, $exclude))
			{
				continue;
			}

			if (array_key_exists($mKey, $aArray2))
			{
				if (is_array($mValue))
				{
					
					$aRecursiveDiff = array_diff_recursive($mValue, $aArray2[$mKey], $exclude);
					
					if (count($aRecursiveDiff)) { $aReturn[$mKey] = $aRecursiveDiff; }
				}
				else
				{
					if ($mValue != $aArray2[$mKey])
					{
						$aReturn[$mKey] = $mValue;
					}
				}
			}
			else
			{
				
				$aReturn[$mKey] = $mValue;
			}
		}
		return $aReturn;
	}
}

function array_filter_recursive(&$array)
{
	foreach ( $array as $key => $item )
	{
		is_array ( $item ) && $array [$key] = array_filter_recursive ( $item );
		if (empty ( $array [$key] ))
			unset ( $array [$key] );
	}
	return $array;
}


function displaying_records($pagination, $total_rows)
{
	if( ! $total_rows) return '';

	$tmp_from = $pagination['offset'] + 1;
	$tmp_to = $pagination['offset'] + $pagination['limit'];
	$tmp_to = ($tmp_to > $total_rows ? $total_rows : $tmp_to);

	return 'Displaying <strong>'.$tmp_from.' - '. $tmp_to.' of '.$total_rows.'</strong> Results. &nbsp;';
}


if( ! function_exists('ajaxRequest'))
{
	function ajaxRequest($url, $post=[])
	{
		$ch = curl_init($url);

		if(empty($post))
		{
			curl_setopt($ch, CURLOPT_FAILONERROR, true); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		}
		else
		{
			// API KEY HAHA
			/*$post['api'] = [
				'user' => env('BLIINKHR_API_USER'),
				'password' => env('BLIINKHR_API_PASSWORD')
			];*/
	
			$post = http_build_query($post);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		
		$output = curl_exec($ch);
	
		if ($output === FALSE) 
		{
			printf("cUrl error (#%d): %s<br>\n", curl_errno($ch),  htmlspecialchars(curl_error($ch)));
			die();
		}

		curl_close($ch); 
	
		$response =  json_decode($output);
		
		if( ! $response)
		{
			echo "x"; print($output); echo "x";
			die('x');
		}
		
		return $response;
	}
}


if(! function_exists('render_account_tree'))
{
	function render_account_tree($data = [], $ctr = 0, $type = '')
	{

		$options = '';

		$indent = str_repeat("-", $ctr);

		foreach($data as $row)
		{

			if($type == 'data')
			{
				$options .= '<option type="'. $type .'" value="'. $row['id'] .'">'.$indent.' '.$row['name'] .'</option>';
			}
			else
			{
				$options .= '<option type="'. $type .'" value="-1" disabled="disabled">'.$indent.' '.$row['name'] .'</option>';
			}


			if(! empty($row['children']))
			{
				$options .= render_account_tree($row['children'], $ctr + 1);
			}

			if(! empty($row['subgroups']))
			{
				$options .= render_account_tree($row['subgroups'], $ctr + 2);
			}

			if(! empty($row['ledgers']))
			{
				$options .= render_account_tree($row['ledgers'], $ctr + 3, 'data');
			}
			



		}

		return $options;
	}
}


if(! function_exists('xls_column_name'))
{
	// for downloading template
	function xls_column_name($count)
	{
		$str = '';

		$set = floor($count / 26);

		$primary = $count;

		if($set)
		{
			$str .= chr( $set + 64);


			for($a=1;$a<=$set; $a++)
			{
				$primary -= 26;
			}
		}

		return $str . chr( $primary + 65);
	}
}

if(! function_exists('lang1'))
{
	function lang1($text)
	{
		if($lang = lang($text))
		{
			return $lang;
		}

		return $text;
	}
}

if(! function_exists('number_metric'))
{
	function number_metric($n, $precision = 3) 
	{
		$negative = ($n < 0);
		$n = abs($n);
		
		
		if ($n < 1000) 
		{
			// Anything less than a thousand
			$n_format = number_format($n, $precision);
		} 
		else if ($n < 100000) 
		{
			// Anything less than a hundred thousand
			$n_format = number_format($n/1000, $precision).'K';
		} 
		else if ($n < 1000000) 
		{
			// Anything less than a million
			$n_format = number_format($n/100000, $precision).'K';
		} 
		else if ($n < 1000000000) 
		{
			// Anything less than a billion
			$n_format = number_format($n / 1000000, $precision) . 'M';
		} 
		else 
		{
			// At least a billion
			$n_format = number_format($n / 1000000000, $precision) . 'B';
		}

		if($negative)
		{
			$n_format = '-'.$n_format;
		}
		
		return $n_format;
	}
}


if(! function_exists('logs'))
{
	function logs($data = [])
	{
		$CI = &get_instance();

        //dd($this->ci->session->userdata('current_user'));


        $user = $CI->session->userdata('account');

        $data['user_id'] = @$user['id'];
		$data['company_id'] = @$user['company_id'];
        $data['created'] = date('Y-m-d H:i:s');


        if(isset($data['action']) && $data['action'] == 'edit')
        {
            $old = object_to_array($data['old_data']);
            $new = object_to_array($data['new_data']);

            // exclude these keys from comparison, array_diff_recursive()
            $exclude = ['created', 'created_on', 'updated', 'updated_on', 'ref_uid'];

            if($old == $new)
            {
                return false;
            }
            else
            {
                $_old = array_diff_recursive($old, $new, $exclude);
                $_new = array_diff_recursive($new, $old, $exclude);
              	
            	//$_old = $old;
            	//$_new = $new;

                if($_old == $_new)
                {
                    return false;
                }
                else
                {
                    $data['old_data'] = json_encode($_old);
                    $data['new_data'] = json_encode($_new);
                }

            }
        }
        else
        {
            foreach(['new_data', 'old_data'] as $tmp_data)
            {
                if(! empty($data[$tmp_data]))
                    $data[$tmp_data] = json_encode($data[$tmp_data]);
                else
                    $data[$tmp_data] = "";
            }
        }

		// unset audit trail (watch out sa iba pang possible na variable)
		unset($data['audit_trail']);

        return $CI->db->insert('logs', $data);
	}
}