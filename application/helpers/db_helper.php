<?php

defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('add_foreign_key'))
{

	function add_foreign_key($table, $foreign_key, $references, $on_delete = 'RESTRICT', $on_update = 'RESTRICT')
	{
		$constraint = "{$table}_{$foreign_key}_fk";
		$sql = "ALTER TABLE {$table} ADD CONSTRAINT {$constraint} FOREIGN KEY ({$foreign_key}) REFERENCES {$references} ON DELETE {$on_delete} ON UPDATE {$on_update}";
		return $sql;
	}
}
if ( ! function_exists('drop_foreign_key'))
{
	/**
	 * @param string $table
	 * @param string $foreign_key
	 *
	 * @return string SQL command
	 */
	function drop_foreign_key($table, $foreign_key)
	{
		$constraint = "{$table}_{$foreign_key}_fk";
		$sql = "ALTER TABLE {$table} DROP FOREIGN KEY {$constraint}";
		return $sql;
	}
}

function get_value_field($id,$table,$field,$extra_id = 0){
	$CI = &get_instance();
	ini_set('display_errors', 0);
	 

	 if (!$table) {
	 	return $id;
	}

	// $result = mysql_query("SHOW COLUMNS FROM `".$table."` LIKE 'is_deleted'");
	// $exists = (mysql_num_rows($result))?TRUE:FALSE;

	$result = $CI->db->query("SHOW COLUMNS FROM `".$table."` LIKE 'is_deleted'")->result_array();
	$exists = !empty($result)? TRUE : FALSE;
	
	if ($extra_id) {
		$id_f = $extra_id;
	} else {
		$id_f = 'id';
	}

	$query = $CI->db->get_where($table, array($id_f => $id));

	if($exists) {
		$query = $CI->db->get_where($table, array($id_f => $id,'is_deleted' => 0));
	} else {
		$query = $CI->db->get_where($table, array($id_f => $id));
	}

	$results = $query->row_array();
	if ($results) {
		if (is_bool($field)) {
			return $results;
		} else {
			if (isset($results[$field])) {
				return $results[$field];
			} else{
				return false;
			}

		}
	} else {
		return false;
	}
}
function get_value_field_like($id,$table,$field,$extra_id = 0){
	$CI = &get_instance();

	// $result = mysql_query("SHOW COLUMNS FROM `".$table."` LIKE 'is_deleted'");
	// $exists = (mysql_num_rows($result))?TRUE:FALSE;

	if ($extra_id) {
		$id_f = $extra_id;
	} else {
		$id_f = 'id';
	}

	$CI->db->like(array($id_f => $id));

	// if($exists) {
	//    $CI->db->where('is_deleted' => 0);
	// }

	$query = $CI->db->from($table)->get();
	$results = $query->row_array();
	if ($results) {
		return $results[$field];
	} else {
		return "0";
	}
}
function get_value_field_multiple($where = array(),$table,$field){
	$CI = &get_instance();
	$query = $CI->db->get_where($table,$where);
	$results = $query->row_array();

	if ($results) {
		return $results[$field];
	} else {
		return "0";
	}
}

function get_person($id = 0,$table = ""){
	$CI = &get_instance();
	$results = [];

	if(is_numeric($table)){
		$table = strtolower(Dropdown::get_static('accounting_entries_payee_type',$table,'view'));
	}

	if( ($table) && ($id) && ($table != "N/A")){
		$results = $CI->db->get_where(strtolower($table),array('id'=>$id))->row_array();
	}
	
	return $results;
}
    if(!function_exists('get_object_from_table')) {
        function get_object_from_table($id, $table, $as_object = true, $order = 'id ASC', $start = null, $limit = null, $select = null)
        {
            if ($as_object) {
                return get_objects_from_table_by_field($table, 'id', $id, $order, $start, $limit, $select)->row();
            }
            return get_objects_from_table_by_field($table, 'id', $id, $order, $start, $limit, $select)->row_array();
        }
    }

    if(! function_exists('default_table_fields')){
        function default_table_fields($table)
        {
            $fields = array(
                'properties' => 'id, name',
                'projects' => 'id, name',
                'sub_projects' => 'id, name',
                'sbu' => 'id, name',
                'amenities' => 'id, name',
                'sub_amenities' => 'id, name',
                'vehicle_equipments' => 'id, name',
                'departments' => 'id, name',
                'item' => 'id, name',
                'item_abc' => 'id, name',
                'item_brand' => 'id, name',
                'item_class' => 'id, name',
                'item_group' => 'id, name',
                'item_type' => 'id, name',
                'companies' => 'id, name',
                'accounting_ledgers' => 'id, name',
                'material_requests' => 'id, reference, approving_staff_id, requesting_staff_id',
                'material_issuances' => 'id, reference',
                'canvassings' => 'id, reference',
                'purchase_order_requests' => 'id, reference, approving_staff_id, requesting_staff_id',
                'purchase_orders' => 'id, reference, approving_staff_id, requesting_staff_id',
                'inventory_settings_conversions' => 'id, name',
                'inventory_settings_delivery_status' => 'id, name',
                'inventory_settings_delivery_types' => 'id, name',
                'inventory_settings_unit_of_measurements' => 'id, name',
            );
            if(array_key_exists($table, $fields)){
                return $fields[$table];
            }
            return null;
        }
    }

    if(!function_exists('get_staff_id')) {
        function get_staff_id($user_id)
        {
            $CI = &get_instance();
            $CI->db->select('id');
            $CI->db->where('user_id', $user_id);
            $staff = $CI->db->get('staff');
            if($staff){
                return $staff->row()->id;
            }
            return false;
        }
    }

    if(!function_exists('get_objects_from_table_by_field')) {
        function get_objects_from_table_by_field($table, $field = null, $val = null, $order = 'id ASC', $start = null, $limit = null, $select = null)
        {
            $CI = &get_instance();

            if(! $select){
                $default_fields = default_table_fields($table);
                if($default_fields){
                    $CI->db->select($default_fields);
                }
            }else{
                $CI->db->select($select);
            }

            if ($limit) {
                if ($start) {
                    $CI->db->limit($start, $limit);
                } else {
                    $CI->db->limit($limit);
                }
            }
            $CI->db->where('deleted_at is NULL');

            $CI->db->order_by($order);
            if ($field) {
                $CI->db->where($field, $val);
            }
            $result = $CI->db->get($table);
            return $result;
        }
    }
    if(!function_exists('set_field')) {
        function set_field($table, $field, $value, $id, $user_id)
        {
            $CI = &get_instance();
            $CI->db->set($field, $value);
            $CI->db->set('updated_by', $user_id);
            $CI->db->set('updated_at', NOW);
            $CI->db->where('id', $id);
            return $CI->db->update($table);
        }
    }

if(!function_exists('concat_ids')){
    function concat_ids($results, $as_string=false){
        $ids = [];
        foreach($results as $row){
            $id = null;
            if(gettype($row) === 'array'){
                $id = $row['id'];
            }elseif (gettype($row === 'object')){
                $id = $row->id;
            }
            if($id){
                array_push($ids, $id);
            }
        }
        if($as_string){
            return implode(",", $ids);
        }
        return $ids;
    }
}

function fill_form_data($fillable, $data=null) {
    $fields = array();
    if($data){
        foreach($data as $key => $value){
            $fields[$key] = $value;
        }
    }else{
        foreach($fillable as $field){
            $fields[$field] = null;
        }
    }
    return $fields;
}

function get_person_name($id = 0,$table = ""){

	if( ($table) && ($id) ){
		$row = get_person($id,$table);
	}

	return @$row['first_name']." ".@$row['last_name'];
}

function count_paid_amount($id = 0, $table = "", $primary_field = "", $amount_field = "") {
    $CI = &get_instance();
    $CI->db->select_sum($amount_field);
    $CI->db->where($primary_field, $id);
    $query = $CI->db->get($table);
    $paid_amount = $query->result_array();

    return $paid_amount[0][$amount_field];
}

function is_authorized($class = false, $userdata = false, $method = false) {
	$CI = &get_instance();
	if($class) 
	{
		if($class === "auth") {
			return true;
		} else {
			$CI->db->select('id');
			$CI->db->from('modules');
			$CI->db->where('link', $class);

			$query = $CI->db->get()->result_array();
			if($query) {
				$module_id = $query[0]['id'];
			} else {
				return false;
			}
		}
	} else {
		return false;
	}
	
	if($userdata) {
		if($method) {
			$permission_id = $userdata['permission_id'];
			
			// Check is user permission has administrator access 
			$CI->db->select('is_admin');
			$CI->db->from('permissions');
			$CI->db->where('id', $permission_id);
			$query = $CI->db->get()->result_array();

			$is_admin = @$query[0]['is_admin'];
			if($is_admin) {
				return true;
			}

			$CI->db->select($method);
			$CI->db->from('module_permissions');
			$CI->db->where('module_id', $module_id);
			$CI->db->where('permission_id', $permission_id);
			$CI->db->where($method, 1);

			$query = $CI->db->get()->result_array();

			if ($query) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
}


function get_origin_source($payable_type_id = 0,$origin_id = 0){
	if ($payable_type_id == 1) {
		//commission
		$transaction_id = get_value_field($origin_id,'transaction_commissions','transaction_id');
		$seller_id = get_value_field($origin_id,'transaction_commissions','seller_id');
		$link = base_url('transaction_commission/view/'.$transaction_id.'/'.$seller_id);
		return "<a href='".$link."' target='_BLANK'>Source</a><br>";
	}
}