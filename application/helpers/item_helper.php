<?php
    /*  Requires:
     *  - db_helper
     */

    if(! function_exists('get_item_unit_prices')):
        function get_item_unit_prices($id)
        {
            $item = get_object_from_table($id, 'item');
            if( ! $item ) return false;
            $canvass_items = get_objects_from_table_by_field('canvassing_items', 'item_id', $id, 'canvassing_id DESC');

            $sizeof_canvass = $canvass_items->num_rows();

            $canvasses = [];
            $canvass_avg = 0.0;
            $canvass_total = 0.0;

            if($sizeof_canvass > 0) {
                foreach ($canvass_items->result() as $canvass_item) {
                    $canvass = get_object_from_table($canvass_item->canvassing_id, 'canvassings');
                    $canvass_total += $canvass_item->unit_cost;
                    $canvass_data = array(
                        'canvass' => $canvass,
                        'item' => $canvass_item
                    );
                    array_push($canvasses, $canvass_data);
                }
                $canvass_avg = $canvass_total / $sizeof_canvass;
            }

            return array(
                "item" => $item,
                "canvasses" => $canvasses,
                "canvass_avg" => $canvass_avg
            );
        }
    endif;

