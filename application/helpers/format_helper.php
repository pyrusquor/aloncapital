<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('get_fname')) {

    function get_fname($data = array())
    {
        $fullname = "";

        if ($data) {
            $fullname = @$data['company_name'] . ' ' .@$data['name'] . ' ' . @$data['first_name'] . ' ' . @$data['last_name'];
        }
        return $fullname;
    }
}

if (!function_exists('get_email')) {

    function get_email($data = array())
    {

        $email = $data['email'];

        return $email;
    }
}

if (!function_exists('get_name')) {

    function get_name($data = array())
    {
        $name = "";

        if ($data) {
            $name = $data['name'];
        }

        return $name;
    }
}

if (!function_exists('password_format')) {

    function password_format($last_name, $birth_date, $type=0)
    {
        // Seller Password Format
        // Password : Lastname+Birthdate = Hernandez10131993

        $formatLname = ucfirst(cleaner(trim(str_replace(' ', '', $last_name)), 'username')); //Lastname
        if($type){
            $formatBdate = ucfirst(cleaner($birth_date, 'username')); //Lastname
        } else{
            $formatBdate = date("mdY", strtotime($birth_date)); //Birthdate
        }
        $sellerPwd = $formatLname . $formatBdate;

        return $sellerPwd;
    }

    function lpad($n)
    {
        // Block, Lot, Phase, Cluster
        // example : 0001

        return sprintf('%03u', $n);
    }
}

if (!function_exists('uniqidReal')) {

    function uniqidReal($length = 13)
    {

        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $length);
    }
}

if (!function_exists('uniqidPR')) {

    function uniqidPR($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('payment_requests');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();

        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], "%d-%d");
            $reference_int = $id + 1;
            $new_reference = 'PR-' . $year . '-' . sprintf('%04s', $reference_int);

            return $new_reference;
        } else {
            return 'PR-' . $year . '-' . '0001';
        }
    }
}

if (!function_exists('uniqidMR')) {

    function uniqidMR($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('material_requests');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();

        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], "%d-%d");
            $reference_int = $id + 1;
            $new_reference = 'MR-' . $year . '-' . sprintf('%04s', $reference_int);

            return $new_reference;
        } else {
            return 'MR-' . $year . '-' . '0001';
        }
    }
}

if (!function_exists('uniqidCNV')) {

    function uniqidCNV($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('canvassings');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();

        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], "%d-%d");
            $reference_int = $id + 1;
            $new_reference = 'CNV-' . $year . '-' . sprintf('%04s', $reference_int);

            return $new_reference;
        } else {
            return 'CNV-' . $year . '-' . '0001';
        }
    }
}

if (!function_exists('uniqidPO')) {

    function uniqidPO($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('purchase_orders');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();

        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], "%d-%d");
            $reference_int = $id + 1;
            $new_reference = 'PO-' . $year . '-' . sprintf('%04s', $reference_int);

            return $new_reference;
        } else {
            return 'PO-' . $year . '-' . '0001';
        }
    }
}

if (!function_exists('uniqidMRR')) {

    function uniqidMRR($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('material_receivings');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();

        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], "%d-%d");
            $reference_int = $id + 1;
            $new_reference = 'MRR-' . $year . '-' . sprintf('%04s', $reference_int);

            return $new_reference;
        } else {
            return 'MRR-' . $year . '-' . '001';
        }
    }
}

if (!function_exists('uniqidMI')) {

    function uniqidMI($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('material_issuances');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();

        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], "%d-%d");
            $reference_int = $id + 1;
            $new_reference = 'MI-' . $year . '-' . sprintf('%04s', $reference_int);

            return $new_reference;
        } else {
            return 'MI-' . $year . '-' . '0001';
        }
    }
}

if (!function_exists('uniqidRPO')) {

    function uniqidRPO($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('purchase_order_requests');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();

        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], "%d-%d");
            $reference_int = $id + 1;
            $new_reference = 'RPO-' . $year . '-' . sprintf('%04s', $reference_int);

            return $new_reference;
        } else {
            return 'RPO-' . $year . '-' . '0001';
        }
    }
}

if (!function_exists('uniqidPV')) {

    function uniqidPV($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('payment_vouchers');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'PV-' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'PV-' . $year . '-' . '001';
        }
    }
}
if (!function_exists('uniqidCV')) {

    function uniqidCV($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('cheque_vouchers');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'CV' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'CV' . $year . '-' . '001';
        }
    }
}

if (!function_exists('uniqidJV')) {

    function uniqidJV($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('journal_vouchers');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'JV-' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'JV-' . $year . '-' . '001';
        }
    }
}

if (!function_exists('uniqidJR')) {

    function uniqidJR($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, number');
        $CI->db->from('job_requests');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $number = sscanf($result[0]['number'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'JR-' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'JR-' . $year . '-' . '001';
        }
    }
}

if (!function_exists('uniqidJO')) {

    function uniqidJO($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, number');
        $CI->db->from('job_orders');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $number = sscanf($result[0]['number'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'JO-' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'JO-' . $year . '-' . '001';
        }
    }
}

if (!function_exists('uniqidCT')) {

    function uniqidCT($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('complaint_tickets');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return $year . '-' . '0001';
        }
    }
}

if (!function_exists('uniqidSO')) {

    function uniqidSO($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, number');
        $CI->db->from('sales_orders');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $number = sscanf($result[0]['number'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'SO-' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'SO-' . $year . '-' . '001';
        }
    }
}

if (!function_exists('uniqidPCR')) {

    function uniqidPCR($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('petty_cash_replenishments');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'PCR-' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'PCR-' . $year . '-' . '001';
        }
    }
}

if (!function_exists('uniqidCD')) {

    function uniqidCD($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('cash_disbursements');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'CD-' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'CD-' . $year . '-' . '001';
        }
    }
}

if (!function_exists('ordinal')) {

    function ordinal($number)
    {
        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
        if ((($number % 100) >= 11) && (($number % 100) <= 13)) {
            return $number . 'th';
        } else {
            return $number . $ends[$number % 10];
        }
    }
}

if (!function_exists('convertPercentage')) {

    function convertPercentage($number)
    {
        return round($number * 100, 2);
    }
}

if (!function_exists('number_to_words')) {
    function number_to_words($num)
    {
        $num = str_replace(array(',', ' '), '', trim($num));
        $num_arr = explode(".", $num);
        $decnum = $num_arr[1];

        if (!$num) {
            return false;
        }
        $num = (int) $num;
        $words = array();
        $list1 = array(
            '', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen',
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array(
            '', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion',
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        $cur_levels = str_split($decnum);

        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int) ($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
            $tens = (int) ($num_levels[$i] % 100);
            $singles = '';
            if ($tens < 20) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
            } else {
                $tens = (int) ($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . (($levels && (int) ($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
        } //end for loop

        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }

        if ($decnum) {
            if ($decnum < 20 && $decnum > 9) {
                $singles = strtoupper($list1[$decnum]);
                $cents = $singles;
            } elseif ($decnum < 10) {
                if ($cur_levels[0] < 1) {
                    $cents = strtoupper($list1[$cur_levels[1]]);
                } elseif ($cur_levels[0] <= 9) {
                    $tens = strtoupper($list2[$cur_levels[0]]);
                    $singles = strtoupper($list1[$cur_levels[1]]);
                    $cents = $tens . " " . $singles;
                }
            } else {
                if ($cur_levels[0] > 1) {
                    $tens = strtoupper($list2[$cur_levels[0]]);
                    $singles = strtoupper($list1[$cur_levels[1]]);
                    $cents = $tens . " " . $singles;
                }
            }
        }

        $words = strtoupper(implode(' ', $words));

        if (!empty($cents)) {
            $words .= " AND " . $cents . " CENTS";
        }

        return $words;
    }
}

if (!function_exists('format_aris_property')) {
    function format_aris_property($str = "")
    {
        $chunks = str_split($str, 3);
        $result = implode('-', $chunks);

        return $result;
    }
}

if (!function_exists('get_value_field_aris')) {
    function get_value_field_aris($id, $table, $field, $extra_id = 0)
    {
        $CI = &get_instance();
        $CI->load->database();
        $CI->db_aris = $CI->load->database('aris', TRUE);

        ini_set('display_errors', 0);

        // if (strpos($table, 'cornerstone') !== false){
        //  $table = $table;
        // } else{
        //  $table = "cornerstone_".$table;
        // }

        // $result = mysql_query("SHOW COLUMNS FROM `".$table."` LIKE 'is_deleted'");
        // $exists = (mysql_num_rows($result))?TRUE:FALSE;

        $result = $CI->db_aris->query("SHOW COLUMNS FROM `" . $table . "` LIKE 'is_deleted'")->result_array();
        $exists = !empty($result) ? TRUE : FALSE;

        if ($extra_id) {
            $id_f = $extra_id;
        } else {
            $id_f = 'id';
        }

        $query = $CI->db_aris->get_where($table, array($id_f => $id));

        if ($exists) {
            $query = $CI->db_aris->get_where($table, array($id_f => $id, 'is_deleted' => 0));
        } else {
            $query = $CI->db_aris->get_where($table, array($id_f => $id));
        }

        $results = $query->row_array();
        if ($results) {
            if (is_bool($field)) {
                return $results;
            } else {
                if (isset($results[$field])) {
                    return $results[$field];
                } else {
                    return 0;
                }
            }
        } else {
            return "0";
        }
    }
}

if (!function_exists('isLeapDay')) {
    function isLeapDay($date)
    {
        if (date('L', strtotime($date))) {
            if (date('m', strtotime($date)) == '02' && date('d', strtotime($date)) == '29') {
                $date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
            }
        }
        return $date;
    }
}

if (!function_exists('uniqidIT')) {

    function uniqidIT($ticket_date)
    {   
        $ticket_date = date_format(date_create($ticket_date), 'Y-m-d');
        $ref_date = date_format(date_create($ticket_date), 'm-d-Y');
        $now_time = $ticket_date . ' 00:00:00';

        $CI = &get_instance();
        $CI->db->where('ticket_date', $now_time);
        $CI->db->select('count(id) as count');
        $CI->db->from('customer_services');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $id = $result[0]['count'];
            $reference_int = $id + 1;
            $new_reference = 'CS-' . $ref_date . '-' . sprintf('%04d', $reference_int) ;
        } else {
            $new_reference = 'CS-' . $ref_date . '-0001';
        }
        return $new_reference;
    }
}
if (!function_exists('get_date_range')) {

    function get_date_range($type, $filter = 0)
    {

        $CI = &get_instance();

        if ($type == 'week') {

            $weekly_collections = $CI->db->query('SELECT * FROM  transaction_official_payments WHERE  YEARWEEK(payment_date, 0) = YEARWEEK(CURDATE() - INTERVAL ' . $filter . ' WEEK, 0)');
            $first_day_query = $CI->db->query('SELECT SUBDATE(CURDATE()-INTERVAL ' . $filter . ' WEEK , dayofweek(CURDATE()-INTERVAL ' . $filter . ' WEEK) -1) AS first_day');
            $last_day_query = $CI->db->query('SELECT ADDDATE(CURDATE()-INTERVAL ' . $filter . ' WEEK , 7 - dayofweek(CURDATE()-INTERVAL ' . $filter . ' WEEK )) AS last_day');
            $first_day = view_date($first_day_query->result_array()[0]['first_day']);
            $last_day = view_date($last_day_query->result_array()[0]['last_day']);

            return $first_day . ' - ' . $last_day;
        } elseif ($type == "month") {

            $monthly_collections = $CI->db->query('SELECT * FROM transaction_official_payments WHERE YEAR(payment_date) = YEAR(CURDATE()) AND MONTH(payment_date) = MONTH(CURDATE() - INTERVAL ' . $filter . ' MONTH)');
            $monthname_query = $CI->db->query('SELECT MONTHNAME(CURDATE()- INTERVAL ' . $filter . ' MONTH) AS month');
            $year_query = $CI->db->query('SELECT YEAR(CURDATE()- INTERVAL ' . $filter . ' MONTH) AS year');
            $monthname = $monthname_query->result_array();
            $year = $year_query->result_array();

            return $monthname[0]['month'] . ' ' . $year[0]['year'];
        }
    }
}

if (!function_exists('uniqidDLR')) {

    function uniqidDLR($length = 13)
    {
        $reference_pad = '000';
        $year = date('Y');

        $CI = &get_instance();
        $CI->db->select('id, reference');
        $CI->db->from('transaction_letter_request');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $category['rows'] = $query->num_rows();
            $result = $query->result_array();

            $id = $result[0]['id'];
            $reference = sscanf($result[0]['reference'], '%d-%d');
            $reference_int = $id + 1;
            $new_reference = 'DLR-' . $year . '-' . sprintf('%04s', $reference_int);
            return $new_reference;
        } else {
            return 'DLR-' . $year . '-' . '001';
        }
    }
}

if (!function_exists('uniqidSL')) {

    function uniqidSL()
    {
        $CI = &get_instance();
        $CI->db->select('id, code');
        $CI->db->from('suppliers');
        $CI->db->order_by('id', 'DESC');
        $CI->db->limit(1);
        $query = $CI->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
            $id = $result[0]['id'];
            $reference_int = $id + 1;
            $new_reference = 'SL' . sprintf('%03s', $reference_int);

            return $new_reference;
        } else {

            return 'SL001';
        }
    }
}
