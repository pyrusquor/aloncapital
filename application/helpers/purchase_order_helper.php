<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    if (!function_exists('po_status_array')) {
        function po_status_array()
        {
            $status_list = array(
                0 => 'Select Option',
                1 => 'New Request',
                2 => 'Approved',
                3 => 'Denied',
                4 => 'Cancelled',
                5 => 'Amended'
            );
            return $status_list;
        }
    }

    if (!function_exists('po_status_lookup')) {
        function po_status_lookup($key)
        {
            $status_list = po_status_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('po_status_reverse_lookup')) {
        function po_status_reverse_lookup($status)
        {
            $arr = po_status_array();
            foreach ($arr as $key => $value) {
                if ($status == $value) {
                    return true;
                }
            }
            return false;
        }
    }

    if (!function_exists('po_item_status_array')) {
        function po_item_status_array()
        {
            $status_list = array(
                0 => 'Select Option',
                1 => 'Pending',
                2 => 'Received',
                3 => 'Rejected',
                4 => 'Amended'
            );
            return $status_list;
        }
    }

    if (!function_exists('po_item_status_lookup')) {
        function po_item_status_lookup($key)
        {
            $status_list = po_item_status_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('po_item_status_reverse_lookup')) {
        function po_item_status_reverse_lookup($status)
        {
            $arr = po_item_status_array();
            foreach ($arr as $key => $value) {
                if ($status == $value) {
                    return true;
                }
            }
            return false;
        }
    }

    if (!function_exists('generate_payment_request_for_purchase_order')) {
        function generate_payment_request_for_purchase_order($obj)
        {
            $view_data = [];
            $view_data['info']['payable_type_id'] = 2;
            $view_data['info']['payable_type']['name'] = "PO";

            $view_data['info']['origin_id'] = $obj['id'];

            $payee_data = [
                'payee_type' => 'suppliers',
                'payee_type_id' => 1,
                'payee_name' => $obj['supplier']['name']
            ];

            $supplier_is_vat = strtolower($obj['supplier']['vat_type']) == 'vat';

            $payee = get_payee($payee_data);

            $view_data['info']['payee_type'] = $payee_data['payee_type'];
            $view_data['info']['payee_name'] = $payee['name'];
            $view_data['info']['payee_type_id'] = $payee_data['payee_type_id'];

            $gross_amount = $obj['total'];
            $view_data['info']['gross_amount'] = $gross_amount;
            $view_data['info']['commission_amount'] = 0.0;
            $view_data['info']['wht_percentage'] = 0.01;
            $view_data['info']['tax_percentage'] = 0.0;
            if($supplier_is_vat){
                $view_data['info']['tax_percentage'] = 0.12;
                $view_data['info']['wht_amount'] = $obj['total'] * $view_data['info']['tax_percentage'] * $view_data['info']['wht_percentage'];
            }else{
                $view_data['info']['wht_amount'] = $obj['total'] * $view_data['info']['wht_percentage'];
            }
            $view_data['info']['tax_amount'] = $obj['total'] * $view_data['info']['tax_percentage'];
            $view_data['info']['total_due_amount'] = $obj['total'] - $view_data['info']['wht_amount'];

            $project_id = null;
            $project_name = null;
            if (array_key_exists('project_id', $obj)) {
                $project_id = $obj['project_id'];
                $project_name = get_value_field($project_id, 'projects', 'name');
            }
            $property_id = null;
            $property_name = null;
            if (array_key_exists('property_id', $obj)) {
                $property_id = $obj['property_id'];
                get_value_field($property_id, 'properties', 'name');
            }
            $sub_project_id = null;
            $sub_project_name = null;
            if (array_key_exists('sub_project_id', $obj)) {
                $sub_project_id = $obj['sub_project_id'];
            }

            $view_data['info']['project_id'] = $project_id;
            $view_data['info']['sub_project_id'] = $sub_project_id;
            $view_data['info']['property_id'] = $property_id;

            $view_data['info']['project']['name'] = $project_name;
            $view_data['info']['property']['name'] = $property_name;

            $view_data['info']['particulars'] = null; // $obj['remarks'];
//            $view_data['info']['wht_percentage'] = 0.0; //$wht_percentage;
//            $view_data['info']['wht_amount'] = 0.0; //$commision_amount - $net_amount;
//
//            $view_data['info']['total_due_amount'] = $gross_amount;

            // tbd
            $view_data['info']['approving_department_id'] = 2;
            $view_data['info']['requesting_department_id'] = 1;

            $view_data['info']['approving_department']['name'] = get_value_field(2, 'departments', 'name');
            $view_data['info']['requesting_department']['name'] = get_value_field(1, 'departments', 'name');
            // /tbd

//            $view_data['info']['tax_percentage'] = 0.12;
//            $view_data['info']['tax_amount'] = 0.0;
            if (array_key_exists('items', $obj)) {
                $view_data['items'] = generate_purchase_order_items_for_payment_request($obj['items']);
//                $tax_amount = 0.0;
//                foreach ($view_data['items'] as $item) {
//                    $tax_amount += $item['tax_amount'];
//                }
//                $view_data['info']['tax_amount'] = $tax_amount;
            } else {
                $view_data['items'] = [];
            }

            // $view_data += generate_payment_request_accounting_entries_for_material_issuance($obj, $gross_amount);
            $view_data['accounting_entries'] = $obj['accounting_entries'];
            $view_data['entry_items'] = $obj['accounting_entry_items'];

            return $view_data;
        }
    }

    if (!function_exists('generate_purchase_order_items_for_payment_request')) {
        function generate_purchase_order_items_for_payment_request($items)
        {
            if (!is_array($items)) {
                return [];
            }
            if (sizeof($items) <= 0 || !$items) {
                return [];
            }
            $items_data = [];
            foreach ($items as $item) {
                $i = array(
                    'item_id' => $item['item']['id'],
                    'name' => $item['item']['name'],
                    'unit_price' => $item['unit_cost'] * 1.0,
                    'quantity' => $item['quantity'] * 1.0,
                    'tax_id' => null,
                    'rate' => 0.12,
                    'tax_amount' => 0.0,
                    'total_amount' => $item['total_cost'] * 1.0
                );
                array_push($items_data, $i);
            }
            return $items_data;
        }
    }

