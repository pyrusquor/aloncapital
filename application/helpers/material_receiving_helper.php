<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    if (!function_exists('mrr_status_array')) {
        function mrr_status_array()
        {
            $status_list = array(
                0 => 'Select Option',
                1 => 'PO Amended',
                2 => 'Item Received',
                3 => 'Wrong Delivery',
                4 => 'Returned to Supplier',
            );
            return $status_list;
        }
    }

    if (!function_exists('mrr_status_lookup')) {
        function mrr_status_lookup($key)
        {
            $status_list = mrr_status_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('mrr_status_reverse_lookup')) {
        function mrr_status_reverse_lookup($status)
        {
            $arr = mrr_status_array();
            foreach($arr as $key => $value){
                if($status == $value){
                    return true;
                }
            }
            return false;
        }
    }

    if (!function_exists('mrr_type_array')) {
        function mrr_type_array()
        {
            $status_list = array(
                0 => 'Select Option',
                1 => 'Direct In',
                2 => 'Standard Receiving Process',
                3 => 'Transfer',
            );
            return $status_list;
        }
    }

    if (!function_exists('mrr_type_lookup')) {
        function mrr_type_lookup($key)
        {
            $status_list = mrr_type_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('mrr_type_reverse_lookup')) {
        function mrr_type_reverse_lookup($status)
        {
            $arr = mrr_type_array();
            foreach($arr as $key => $value){
                if($status == $value){
                    return true;
                }
            }
            return false;
        }
    }

    if (!function_exists('mrr_item_status_array')) {
        function mrr_item_status_array()
        {
            $status_list = array(
                0 => 'Select Option',
                1 => 'Item Received',
                2 => 'Amended',
                3 => 'Item Rejected',
            );
            return $status_list;
        }
    }

    if (!function_exists('mrr_item_status_lookup')) {
        function mrr_item_status_lookup($key)
        {
            $status_list = mrr_item_status_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('mrr_item_status_reverse_lookup')) {
        function mrr_item_status_reverse_lookup($status)
        {
            $arr = mrr_item_status_array();
            foreach($arr as $key => $value){
                if($status == $value){
                    return true;
                }
            }
            return false;
        }
    }