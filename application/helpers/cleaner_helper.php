<?php

/* 
 * File Description : This is a helper to clean and sanitize user input for invalid characters
 * Author           : Bradley B. Dalina
 * Date             : June 23, 2018
 */

defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('cleaner'))
{
    function cleaner($input, $option=null, $allow_char='')
    {
        if(empty(trim($input)))
        {
            return null; //Nothing to do; stop it here!
        }

        $notags = trim(strip_tags($input)); //Remove any form of tags <hack></hack>

        switch ($option)
        {
            case 'alphanum':
                $cleaned = preg_replace("/[^a-zA-Z0-9".$allow_char."]+/", "", $notags);
                break;
            case 'username':
                $notags = filter_var($notags, FILTER_SANITIZE_EMAIL);

                // Validate e-mail
                if (filter_var($notags, FILTER_VALIDATE_EMAIL)) 
                {
                    $cleaned = $notags;
                } 
                else 
                {
                    $cleaned = preg_replace("/[^a-zA-Z0-9".$allow_char."]+/", "", $notags);
                }
                break;
            case 'alpha':
                $cleaned = preg_replace("/[^a-zA-Z".$allow_char."]+/", "", $notags);
                break;
            case 'semi-alpha':
                $cleaned = preg_replace("/[0-9".$allow_char."]+/", "", $notags);
                break;
            case 'numeric':
                $cleaned = preg_replace("/[^0-9".$allow_char."]+/", "", $notags);
                break;
            case 'semi-numeric':
                $cleaned = preg_replace("/[a-zA-Z".$allow_char."]+/", "", $notags);
                break;
            default:
                $cleaned = trim(htmlentities($input, ENT_NOQUOTES));
                break;
        }
        return $cleaned; //return the cleaned input
    }
}

if( !function_exists('get_initials') )
{
    function get_initials ($str = '') {

        $acronym = "N/A";
        if ($str) {
            // Delimit by multiple spaces, hyphen, underscore, comma
            $words = preg_split("/[\s,_-]+/", ucwords($str));
            $acronym = "";

            foreach ($words as $value) {
                $acronym .= @$value[0];
            }
        }
        

        return $acronym;
    } 
 }

 if( !function_exists('sanitize') )
{
    function sanitize ($data = array()) {
        if ($data) {
            return $data;
        } else {
            return $data;
        }
    } 
 }

  if( !function_exists('cln') )
{
    function cln ($data = '') {
        if ($data) {
            return $data;
        } else {
            $data = "";
            return $data;
        }
    } 
 }


if( !function_exists('remove_commas') )
{
    function remove_commas ($string = '') {
        if ($string) {
            return str_replace( ',', '', $string );
        } {
            return $string;
        }
    } 
 }

 function cleanString($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
        '/[“”«»„]/u'    =>   ' ', // Double quote
        '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}
