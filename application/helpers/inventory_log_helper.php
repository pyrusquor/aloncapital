<?php
    if (!function_exists('inventory_log_transaction_type_array')) {
        function inventory_log_transaction_type_array()
        {
            $status_list = array(
                0 => 'Select Option',
                1 => 'Normal Transaction',
                2 => 'Transfer: Add',
                3 => 'Transfer: Subtract',
                4 => 'Insert',
                5 => 'Update',
                6 => 'Material Receiving',
                7 => 'Material Releasing',
                8 => 'Virtual Receive',
                9 => 'Virtual Release'
            );
            return $status_list;
        }
    }

    if (!function_exists('inventory_log_transaction_type_lookup')) {
        function inventory_log_transaction_type_lookup($key)
        {
            $status_list = inventory_log_transaction_type_array();
            return $status_list[$key];
        }
    }

    if(!function_exists('inventory_log_transaction_keyval')) {
        function inventory_log_transaction_keyval($movement){
            switch($movement){
                case 'receive':
                    return array(
                        'key' => 6,
                        'label' => inventory_log_transaction_type_array()[6]
                    );
                case 'direct_in':
                    return array(
                        'key' => 4,
                        'label' => inventory_log_transaction_type_array()[4]
                    );
                case 'release':
                    return array(
                        'key' => 7,
                        'label' => inventory_log_transaction_type_array()[7]
                    );
                case 'receive_virtual':
                    return array(
                        'key' => 8,
                        'label' => inventory_log_transaction_type_array()[8]
                    );
                case 'release_virtual':
                    return array(
                        'key' => 9,
                        'label' => inventory_log_transaction_type_array()[9]
                    );
                case 'transfer_add':
                    return array(
                        'key' => 2,
                        'label' => inventory_log_transaction_type_array()[2]
                    );
                case 'transfer_subtract':
                    return array(
                        'key' => 3,
                        'label' => inventory_log_transaction_type_array()[3]
                    );
            }
        }
    }

    if (!function_exists('inventory_log_transaction_type_reverse_lookup')) {
        function inventory_log_transaction_type_reverse_lookup($status)
        {
            $arr = inventory_log_transaction_type_array();
            foreach ($arr as $key => $value) {
                if ($status == $value) {
                    return true;
                }
            }
            return false;
        }
    }

    if (!function_exists('get_last_log_for_item_in_warehouse')) {
        function get_last_log_for_item_in_warehouse($item_id, $warehouse_id, $warehouse_type = 'warehouse')
        {
            $CI = &get_instance();

            $warehouse_key = $warehouse_type . '_id';

            $log = $CI->db->get_where('warehouse_inventory_logs', array(
                    $warehouse_key => $warehouse_id,
                    'item_id' => $item_id
                ))->order_by('created', 'DESC')->row();

            return $log;
        }
    }

    if (!function_exists('update_log_type')) {
        function update_log_type($log_id, $type)
        {
            $CI = &get_instance();

            $CI->db->set('transaction_type', $type);
            $CI->db->where('id', $log_id);
            $CI->db->update('warehouse_inventory_logs');
        }
    }