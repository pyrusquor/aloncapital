<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_aris_ids'))
{

	function get_aris_ids( $transaction = [] ) {
        $CI = &get_instance();

        $aris_ids['buyer_id'] = 0;
        $aris_ids['project_id'] = 0;
        $aris_ids['lot_id'] = 0;
        $aris_ids['document_id'] = 0;

        $middle_name = $transaction['buyer']['middle_name'];
        $first_name = $transaction['buyer']['first_name'];
        $last_name = $transaction['buyer']['last_name'];


        $CI->db->like('LASTNAME', $last_name, 'both');
        $CI->db->like('FIRSTNAME', $first_name, 'both');
        $aris_customer = $CI->db->get('aris_customers')->row();

        if ($aris_customer) {
            $aris_ids['buyer_id'] = $aris_customer->CUSTOMERID;
            $aris_ids['project_id'] = $aris_customer->project_id;

            $CI->db->where('CUSTOMERID', $aris_ids['buyer_id']);
            $CI->db->where('project_id', $aris_ids['project_id']);

            $aris_document = $CI->db->get('aris_documents')->row()
            ;
            if ($aris_document) {
                $aris_ids['document_id'] = $aris_document->DocumentID;
                $aris_ids['lot_id'] = $aris_document->LOTID;
            }

        }
        // vdebug($aris_customer);
        // vdebug($aris_document);
        return $aris_ids;
    }


}