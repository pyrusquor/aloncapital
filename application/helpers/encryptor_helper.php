<?php

/* 
 * File Description : Helper function for encryption / decryption of password 
 * Author           : Bradley B. Dalina
 * Date             : June 23, 2018
 */

defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('generateRandomString'))
{
    function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
}

if ( ! function_exists('enCrypt'))
{
    function enCrypt($pass)
    {
        $e1 = str_split(strrev($pass), round(count(str_split(strrev($pass)))/2));
        $e2 = generateRandomString(24);
        $e3=".".$e2.".";
        $e4='$'.substr(str_shuffle('$abcdefghijklmnopqrstuvwxyz/0123456789$ABCEFGHIJKLMNOPQRSTUVWXYZ/'),0, 6).".";
        $e5 = $e4.base64_encode($e1[1].$e3.$e1[0]);
        return $e5;
    }
}

if ( ! function_exists('deCrypt'))
{
    function deCrypt($pass)
    {
        $e1 = preg_replace('/\$(.*?)\./', "", $pass);
        $e2 = base64_decode($e1);
        $e3 = preg_replace("/\.(.*?)\./", "", $e2);
        $e4 = str_split(strrev($e3), round(count(str_split(strrev($e3)))/2));
        $e5=$e4[1].$e4[0];
        return $e5;
    }
}

if ( ! function_exists('passGen'))
{
    function passGen()
    {
        return substr(str_shuffle('abcdefghijkmnpqrstuvwxyz0123456789ABCEFGHJKLMNPQRSTUVWXYZ'),0, 10);
    }
}