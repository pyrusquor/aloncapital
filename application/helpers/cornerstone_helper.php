<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_remaining_days($date1,$date2){
	$datetime1 = strtotime($date1);
	$datetime2 = strtotime($date2);

	$secs = $datetime2 - $datetime1;// == <seconds between the two times>
	return $days = $secs / 86400;
}

if ( ! function_exists('character_limiter'))
{
	function character_limiter($str, $n = 500, $end_char = '&#8230;')
	{
		if (strlen($str) < $n)
		{
			return $str;
		}
		
		$str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

		if (strlen($str) <= $n)
		{
			return $str;
		}

		$out = "";
		foreach (explode(' ', trim($str)) as $val)
		{
			$out .= $val.' ';
			
			if (strlen($out) >= $n)
			{
				$out = trim($out);
				return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
			}		
		}
	}
}


if ( ! function_exists('get_info'))
{
	function get_info($table,$id){
		$CI = &get_instance();
		$query = $CI->db->get_where($table, array('id' => $id));
		$result = $query->row_array();

		if (!$id) {
			return 'N/A';
		} else {
			return @$result['firstname'].' '.@$result['lastname'];
		}
	}
}

function get_monthly_avg($ledger_id = 0, $date_from = null, $date_to = null, $term = 1) {
	$CI = &get_instance();
	$CI->load->model('Ledger_model');

	if ($ledger_id) {
		$clbalance = $CI->Ledger_model->get_ledger_balance($ledger_id,0,@$date_from,@$date_to);
		return $clbalance / $term;
	} else {
		return 0;
	}
}

function get_url_admin_logo() {
	$CI = &get_instance();

	$url = base_url();
  	$urls = explode('/', $url);

  	$company = $CI->config->item('company');
  	
  	if ($_SERVER['SERVER_ADDR'] == "172.31.29.235") {
  		$base_company = explode('.', $urls[2]);
  		$base_company = $base_company[0];

  		if ($base_company == 'ptr') {
  			$img_url = 'lhoopapro.com/cornerstone-ptr/superadmin/assets/img/logo/'.get_value_field(1,'cornerstone_developers','logo');
  		} else {
  			$img_url = $_SERVER['SERVER_NAME'].'/superadmin/assets/img/logo/'.get_value_field(1,'cornerstone_developers','logo');
  		}
  	} else {
	    $img_url = $_SERVER['SERVER_NAME'].'/'.$company['module'].'/superadmin/assets/img/logo/'.get_value_field(1,'cornerstone_developers','logo');
  	}

  	return $img_url;
}

function get_project_image($project_id){
	$CI = &get_instance();
	$result = $CI->db->get_where('cornerstone_projects',array('id'=>$project_id))->row_array();
	return $result['image'];
}


if ( ! function_exists('entry_type_info'))
{
	function entry_type_info($entry_type_id)
	{
		$CI =& get_instance();	

		$CI->db->where('id', $entry_type_id);		
		$query = $CI->db->get('cornerstone_entry_types');
		$result = $query->row_array();

		if ($result)
		{
			return array(
				'id' => $result['id'],
				'label' => $result['label'],
				'name' => $result['name'],
				'numbering' => $result['numbering'],
				'prefix' => $result['prefix'],
				'suffix' =>$result['suffix'],
				'zero_padding' => $result['zero_padding'],
				'bank_cash_ledger_restriction' => $result['bank_cash_ledger_restriction'],
			);
		} else {
			return array(
				'id' => $entry_type_id,
				'label' => '',
				'name' => '(Unkonwn)',
				'numbering' => 1,
				'prefix' => '',
				'suffix' => '',
				'zero_padding' => 0,
				'bank_cash_ledger_restriction' => 5,
			);
		}
	}
}


if ( ! function_exists('in_array_r'))
{
	function in_array_r($item , $array){
		if ($item && $array) {
	  	 	 return preg_match('!"'.$item.'"!i' , json_encode($array));
		} else {
			return 0;
		}
	}
}

if ( ! function_exists('searchForId'))
{
	function searchForId($name, $array) {
		foreach ($array as $key => $val) {
	       	if ($val['name'] === $name) {
	        	return $key;
	       	}
	   	}
	   	return null;
	}
}



if ( ! function_exists('date_mysql_to_php_display'))
{
	function date_mysql_to_php_display($dt)
	{
		$ts = human_to_unix($dt);
		$CI =& get_instance();
		$current_date_format = $CI->config->item('account_date_format');
		switch ($current_date_format)
		{
		case 'dd/mm/yyyy':
			return date('d M Y', $ts);
			break;
		case 'mm/dd/yyyy':
			return date('M d Y', $ts);
			break;
		case 'yyyy/mm/dd':
			return date('Y M d', $ts);
			break;
		default:
			return "";
		}
		return;
	}
}

if ( ! function_exists('convert_dc'))
{
	function convert_dc($label)
	{
		if ($label == "D")
			return "Dr";
		else if ($label == "C")
			return "Cr";
		else
			return "Error";
	}
}



if ( ! function_exists('human_to_unix'))
{
	function human_to_unix($datestr = '')
	{
		if ($datestr == '')
		{
			return FALSE;
		}
	
		$datestr = trim($datestr);
		$datestr = preg_replace("/\040+/", "\040", $datestr);

		if ( ! preg_match('/^[0-9]{2,4}\-[0-9]{1,2}\-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}(?::[0-9]{1,2})?(?:\s[AP]M)?$/i', $datestr))
		{
			return FALSE;
		}

		$split = preg_split("/\040/", $datestr);

		$ex = explode("-", $split['0']);
	
		$year  = (strlen($ex['0']) == 2) ? '20'.$ex['0'] : $ex['0'];
		$month = (strlen($ex['1']) == 1) ? '0'.$ex['1']  : $ex['1'];
		$day   = (strlen($ex['2']) == 1) ? '0'.$ex['2']  : $ex['2'];

		$ex = explode(":", $split['1']);
	
		$hour = (strlen($ex['0']) == 1) ? '0'.$ex['0'] : $ex['0'];
		$min  = (strlen($ex['1']) == 1) ? '0'.$ex['1'] : $ex['1'];

		if (isset($ex['2']) && preg_match('/[0-9]{1,2}/', $ex['2']))
		{
			$sec  = (strlen($ex['2']) == 1) ? '0'.$ex['2'] : $ex['2'];
		}
		else
		{
			// Unless specified, seconds get set to zero.
			$sec = '00';
		}
	
		if (isset($split['2']))
		{
			$ampm = strtolower($split['2']);
		
			if (substr($ampm, 0, 1) == 'p' AND $hour < 12)
				$hour = $hour + 12;
			
			if (substr($ampm, 0, 1) == 'a' AND $hour == 12)
				$hour =  '00';
			
			if (strlen($hour) == 1)
				$hour = '0'.$hour;
		}
			
		return mktime($hour, $min, $sec, $month, $day, $year);
	}
}

function array_to_id($array = array(), $type ='')
{
	$ledgers = array();
	foreach ($array as $arr)
	{
		if ($type == 'assets')
		{
			if ($arr['type'] == 0)
			{
				array_push($ledgers, $arr['id']);
			}
		}
		else
		{
			array_push($ledgers, $arr['id']);
		}
	}

	return $ledgers;
}

function extract_group_ledgers($groups)
{
	$ledgers = array();
	foreach ($groups as $group)
	{
		extract_ledgers($group, $ledgers);
	}

	return $ledgers;
}
	

function get_entry_id($invoice_id, $type)
{
	$CI = &get_instance();
	
	$CI->db->select('id');
	$CI->db->where('invoice_id', $invoice_id);
	$CI->db->where('invoice_type', $type);
	$query = $CI->db->get('cornerstone_entries');

	$id = 0;
	$entry_id = 0;
	if ($query->num_rows() > 0)
	{
		$id = $query->row_array();
		$entry_id = $id['id'];
	}

	return $entry_id;

}

function extract_ledgers($grp = array(), &$ledger)
{
	if (count( $grp['ledgers']) > 0)
	{
		foreach ($grp['ledgers'] as $led)
		{
			array_push($ledger, $led);
		}
	}

	if (count( $grp['subgroups']) > 0)
	{
		foreach ($grp['subgroups'] as $grp)
		{
			extract_ledgers($grp, $ledger);
		}
	}
}




function get_ledgers($field)
{
	$CI = &get_instance();
	
	$CI->db->where($field, 1);
	$query = $CI->db->get('cornerstone_ledgers');

	return $query->result_array();

}

function check_exists($email,$field,$table){
	$CI = &get_instance();
	$query = $CI->db->get_where($table, array($field => $email));
	$result = $query->row_array();
	// return $CI->db->count_all_results();
	return count($result);
}

function get_last_id($table='')
{
	$CI = &get_instance();
	$CI->db->select('max(id) as max');
	$query = $CI->db->get($table);

	$result = $query->row_array();
	return $result['max'] + 1;

}
function user_permission($personnel_group_id,$module_id,$action){
	$CI = &get_instance();
	$query = $CI->db->get_where('cornerstone_personnel_permissions', array('personnel_group_id' => $personnel_group_id,'module_id' => $module_id));
	$row = $query->row_array();
	if ($personnel_group_id) {
		if ($row) {
			return $row[$action];
		} else {
			return 1;
		}
	} else {
		return 1;
	}
}


function get_value_field_raw($id,$table,$field,$extra_id = 0){
	$CI = &get_instance();

	if ($extra_id) {
		$id_f = $extra_id;
	} else {
		$id_f = 'id';
	}

	$query = $CI->db->query("SELECT * FROM `".$table."` WHERE `".$id_f."` = ".$id." ");
	$results = $query->row_array();

	if ($results) {
		return $results[$field];
	} else {
		return "N/A";
	}
}

function array_unique_multidimensional($input)
{
    $serialized = array_map('serialize', $input);
    $unique = array_unique($serialized);
    return array_intersect_key($input, $unique);
}

if ( ! function_exists('convert_amount_dc'))
{
	function convert_amount_dc($amount,$type = 0)
	{
		// if ($amount == "D")
		// 	return "0";
		// else if ($amount < 0)
		// 	return "Cr " . "&nbsp&nbsp" . money_php(convert_cur(-$amount), 2, '.', '');
		// else
		// 	return "Dr " . "&nbsp&nbsp" . money_php(convert_cur($amount), 2, '.', '');

		if ($type == "abs") {
			return money_php(abs($amount));
		}

		if($amount == 0)
		{
			return money_php(0);
		}
		else if($amount < 0)
		{
			return '(' . money_php(abs($amount)).')';
		}
		else
		{
			return money_php($amount);
		}
	}

}

if ( ! function_exists('convert_amount_dc_pdf'))
{
	function convert_amount_dc_pdf($amount)
	{
		if ($amount == "D")
			return "0";
		else if ($amount < 0)
			return "Cr " . "  " . number_format(convert_cur(-$amount), 2, '.', '');
		else
			return "Dr " . "  " . number_format(convert_cur($amount), 2, '.', '');
	}

}



 
if ( ! function_exists('convert_opening'))
{
	function convert_opening($amount, $dc)
	{
		if ($amount == 0)
			return "0";
		else if ($dc == 'D')
			return "Dr " . convert_cur($amount);
		else
			return "Cr " . convert_cur($amount);
	}
}

if ( ! function_exists('convert_cur'))
{
	function convert_cur($amount)
	{
		return number_format($amount, 2, '.', '');
	}
}

function get_value_field_orig($id,$table,$field){
	$CI = &get_instance();
	$query = $CI->db->get_where($table, array('id' => $id));
	$results = $query->row_array();
	if ($results) {
		return $results[$field];
	} else {
		return false;
	}
}


if ( ! function_exists('convert_amount_dc_2'))
{
	function convert_amount_dc_2($amount)
	{
		if ($amount < 0)
			return "(" . money_php(number_format(convert_cur(abs($amount)), 2, '.', '')) . ")";
		else
			return money_php(number_format(convert_cur($amount), 2, '.', ''));
	}
}




	function float_ops($param1 = 0, $param2 = 0, $op = '')
	{
		$result = 0;
		$param1 = $param1 * 100;
		$param2 = $param2 * 100;
		$param1 = (int)round($param1, 0);
		$param2 = (int)round($param2, 0);
		switch ($op)
		{
		case '+':
			$result = $param1 + $param2;
			break;
		case '-':
			$result = $param1 - $param2;
			break;
		case '==':
			if ($param1 == $param2)
				return TRUE;
			else
				return FALSE;
			break;
		case '!=':
			if ($param1 != $param2)
				return TRUE;
			else
				return FALSE;
			break;
		case '<':
			if ($param1 < $param2)
				return TRUE;
			else
				return FALSE;
			break;
		case '>':
			if ($param1 > $param2)
				return TRUE;
			else
				return FALSE;
			break;

		}
		$result = $result/100;
		return $result;
	}


function save_transaction_items($transaction_id, $type, $items, $items_quantity, $prices, $tax_rates, $items_tax)
{
	$table = define_table_transactions($type);

	$CI = &get_instance();
	//First, delete all items on that transaction
	$CI->db->delete($table['table'], array($table['column'] => $transaction_id));

	foreach ($items as $key => $item)
	{
		if (!empty($item))
		{
			$params['data'] = array(
				$table['column']	=> $transaction_id,
				'item_id'			=> $items[$key],
				'quantity' 			=> $items_quantity[$key],
				'item_price'		=> $prices[$key]
			);

			if (isset($tax_rates[$key]))
			{
				$params['data']['tax_rate'] = $tax_rates[$key]; 
			}
			else
			{
				$params['data']['tax_rate'] = 0;
			}

			if (isset($items_tax[$key]))
			{
				$params['data']['tax_id'] = $items_tax[$key]; 
			}
			else
			{
				$params['data']['tax_id'] = 0;
			}

			$params['table'] = $table['table'];
			$CI->db->insert($params['table'],$params['data']);	
		}	
	}
}

function save_transaction_items_payable($transaction_id, $type, $items, $items_quantity, $prices, $tax_rates, $items_tax)
{
	$table = define_table_transactions($type);

	$CI = &get_instance();
	//First, delete all items on that transaction
	$CI->db->delete($table['table'], array($table['column'] => $transaction_id));

	foreach ($items as $key => $item)
	{
		if (!empty($item))
		{
			$params['data'] = array(
				$table['column']			=> $transaction_id,
				'item_payable_id'			=> $items[$key],
				'item_payable_quantity' 	=> $items_quantity[$key],
				'item_payable_price'		=> $prices[$key]
			);

			if (isset($tax_rates[$key]))
			{
				$params['data']['tax_rate'] = $tax_rates[$key]; 
			}
			else
			{
				$params['data']['tax_rate'] = 0;
			}

			if (isset($items_tax[$key]))
			{
				$params['data']['tax_id'] = $items_tax[$key]; 
			}
			else
			{
				$params['data']['tax_id'] = 0;
			}

			$params['table'] = $table['table'];
			$CI->db->insert($params['table'],$params['data']);	
		}	
	}
}

function save_transaction_fees($transaction_id, $type, $fees, $amounts)
{
	$table = define_table_transactions($type);
	$CI = &get_instance();
	//First, delete all fees on that transaction
	$CI->db->delete($table['table_fees'], array($table['column'] => $transaction_id));

	foreach ($fees as $key => $fee) 
	{
		if (!empty($fee))
		{
			$params['data'] = array(
				$table['column'] 	=> $transaction_id,
				'fee_id'			=> $fees[$key],
				'fee_amount'		=> $amounts[$key]
			);

			$params['table'] = $table['table_fees'];
			$CI->db->insert($params['table'],$params['data']);	
		}	
	}	


}

function define_table_transactions($type)
{
	$table = array();

	if ($type == 'collection')
	{
		$table['column'] = 'collection_id';
		$table['table'] = 'cornerstone_' . $type . '_items';
		$table['table_fees'] = 'cornerstone_' . $type . '_fees';
	}
	else if ($type == 'requests')
	{
		$table['column'] = 'payment_request_id';
		$table['table'] = 'cornerstone_accounting_payment_requests_items';
		$table['table_fees'] = 'cornerstone_accounting_payment_requests_fees';
	}
	else if ($type == 'vendor')
	{
		$table['column'] = 'vendor_invoice_id';
		$table['table'] = 'cornerstone_vendor_items';
		$table['table_fees'] = 'cornerstone_vendor_fees';
	}
	else if ($type == 'voucher')
	{

		$table['column'] = 'voucher_id';
		$table['table'] = 'cornerstone_payment_voucher_items';
		$table['table_fees'] = 'cornerstone_payment_voucher_fees';
		
	}
	else if ($type == 'invoice')
	{
		$table['column'] = 'invoice_id';
		$table['table'] = 'cornerstone_' . $type . '_items';
		$table['table_fees'] = 'cornerstone_' . $type . '_fees';
	}
	else
	{

	}
	
	return $table;
}


function get_all_accounting_modules_v2($id = "")
{
	$CI = &get_instance();
		$CI->db->select('*');
		$CI->db->order_by('cornerstone_accounting_modules_v2.parent', 'asc');
		$CI->db->order_by('cornerstone_accounting_modules_v2.key', "asc");

		$query = $CI->db->get_where('cornerstone_accounting_modules_v2');

		$results = $query->result_array();
		return $results;

}

function string_limit($string, $limit) {
	if (strlen($string) > $limit) {
		return substr($string,0,$limit).'...';
	} else {
		return $string? $string : 'N/A';
	}
}

function getDeveloperURL() {
	$CI = &get_instance();
	$company = $CI->config->item('company');

	if ($_SERVER['SERVER_ADDR'] == "172.31.29.235") {
		$url = "http://".$_SERVER['SERVER_NAME']."/developers";
	} else {
		$url = "http://".$_SERVER['SERVER_NAME']."/".$company['module']."/"."developers";
	}

	return $url;
}

if ( ! function_exists('form_input_ledger'))
{
	function form_input_ledger($name, $selected = NULL, $extra = '', $type = 'all')
	{
		$CI =& get_instance();
		$CI->load->model('Ledger_model');
		error_reporting(E_ALL);
		
		if ($type == 'bankcash')
			$options = $CI->Ledger_model->get_all_ledgers_bankcash();
		else if ($type == 'nobankcash')
			$options = $CI->Ledger_model->get_all_ledgers_nobankcash();
		else if ($type == 'bank')
			$options = $CI->Ledger_model->get_all_ledgers_bank();
		else if ($type == 'reconciliation')
			$options = $CI->Ledger_model->get_all_ledgers_reconciliation();
		else if ($type == 'projects')
			$options = $CI->Ledger_model->get_all_projects();
		else if ($type == 'departments')
			$options = $CI->Ledger_model->get_all_departments();
		else if ($type == 'properties')
			$options = $CI->Ledger_model->get_all_properties();
		else if ($type == 'agents')
			$options = $CI->Ledger_model->get_all_agents();
		else if ($type == 'brokers')
			$options = $CI->Ledger_model->get_all_brokers();
		else if ($type == 'clients')
			$options = $CI->Ledger_model->get_all_clients();
		else if ($type == 'employees')
			$options = $CI->Ledger_model->get_all_employees();
		else if ($type == 'suppliers')
			$options = $CI->Ledger_model->get_all_suppliers();
		else if ($type == 'modules')
			$options = $CI->Ledger_model->get_all_entities();
		else if ($type == 'module_proj_prop')
			$options = $CI->Ledger_model->get_project_and_property_entity();
		else
			$options = $CI->Ledger_model->get_all_ledgers();

		// If no selected state was submitted we will attempt to set it automatically
		if ( ! ($selected))
		{
			// If the form name appears in the $_POST array we have a winner!
			if (isset($_POST[$name]))
			{
				$selected = $_POST[$name];
			}
		}

		if ($extra != '') $extra = ' '.$extra;

		$form = '<select name="'.$name.'"'.$extra.' class="ledger-dropdown form-control">';
		
		foreach ($options as $key => $val)
		{
			$key = (string) $key;
			$sel = ($key == (string) $selected) ? ' selected="selected"' : '';
			$form .= '<option value="'.$key.'"'.$sel.'>'.(string) ucwords($val)."</option>\n";
		}

		$form .= '</select>';
		return $form;
	}
}



if ( ! function_exists('get_payee'))
{
	function get_payee($data)
	{
		$CI =& get_instance();
		$id = $data['payee_type_id'];

        $CI->load->model(array('buyer/Buyer_model' => 'M_buyer','seller/Seller_model' => 'M_seller','staff/Staff_model' => 'M_staff','contractors/Contractors_model' => 'M_contractor','suppliers/Suppliers_model' => 'M_supplier',));

        $p_type = strtolower(Dropdown::get_static('accounting_entries_payee_type',$data['payee_type'],'view'));

		if($data['payee_type'] == '1' || $data['payee_type'] == 'suppliers') { // suppliers
			$data = $CI->M_supplier->get($id);
		} else if($data['payee_type'] == '2' || $data['payee_type'] == 'sellers') { // sellers
			$data = $CI->M_seller->get($id);
		} else if($data['payee_type'] == '3' || $data['payee_type'] == 'staff') { // staff
			$data = $CI->M_staff->get($id);
		} else if($data['payee_type'] == '4' || $data['payee_type'] == 'contractors') { // contractors
			$data = $CI->M_contractor->get($id);
		} else if($data['payee_type'] == '5' || $data['payee_type'] == 'buyers') { // buyers
			$data = $CI->M_buyer->get($id);
		}

		return $data;
	}
}


function count_payments() {
	$CI = &get_instance();
	$CI->load->model('Entry_items_model');

	$params['where'] = array( 'reconciliation' => '1', 'cornerstone_entry_items.is_reconciled' => '1', 'dc' => 'C' );
	$row = $CI->Entry_items_model->get_all_entry_items( $params, true );
	
	return $row;
}


function count_deposits() {
	$CI = &get_instance();
	$CI->load->model('Entry_items_model');

	$params['where'] = array( 'reconciliation' => '1',  'cornerstone_entry_items.is_reconciled' => '1', 'dc' => 'D' );
	$row = $CI->Entry_items_model->get_all_entry_items( $params, true );
	
	return $row;
}

function count_payments_sum() {
	$CI = &get_instance();
	$CI->load->model('Entry_items_model');

	$params['where'] = array( 'reconciliation' => '1',  'cornerstone_entry_items.is_reconciled' => '1', 'dc' => 'C' );
	$row = $CI->Entry_items_model->get_all_entry_items( $params, false, true );
	return $row[0]['amount'];
}


function count_deposits_sum() {
	$CI = &get_instance();
	$CI->load->model('Entry_items_model');

	$params['where'] = array( 'reconciliation' => '1',  'cornerstone_entry_items.is_reconciled' => '1', 'dc' => 'D' );
	$row = $CI->Entry_items_model->get_all_entry_items( $params, false, true );
	// echo "<pre>";echo $CI->db->last_query();print_r($row);die();
	return $row[0]['amount'];
}

function cleared_balance() {
	$CI = &get_instance();
	$CI->load->model('Entry_items_model');

	$params['where'] = array( 'reconciliation' => '1', 'cornerstone_entry_items.is_reconciled' => '1', 'dc' => 'D' );
	$row = $CI->Entry_items_model->get_all_entry_items( $params, false, true );
	$debit = $row[0]['amount'];

	$params['where'] = array( 'reconciliation' => '1', 'cornerstone_entry_items.is_reconciled' => '1', 'dc' => 'C' );
	$row = $CI->Entry_items_model->get_all_entry_items( $params, false, true );
	$credit = $row[0]['amount'];

	$balance = $credit - $debit;
	return number_format($balance);
}

function file_exists2($file) {
	$file_headers = @get_headers($file);

	if (empty($file_headers)) {
		$exists = false;
	} else if($file_headers[0] == 'HTTP/1.0 404 Not Found') {
	    $exists = false;
	} else {
	    $exists = true;
	}

	return $exists;
}

/** END **/