<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    if (!function_exists('get_asset_groups')) {

        function get_asset_groups($order_by = null, $as_array = false, $recursive = 0, $parent_ids = array(),$groups = array())
        {
            $CI = &get_instance();

            if($parent_ids){
                $CI->db->where_in('parent_id', $parent_ids); 
                $parent_ids = [];
            } else {
                $CI->db->where('parent_id', 1); 
                $groups = [];
            }
         
            $CI->db->where('deleted_at', NULL);
            $CI->db->from('accounting_groups');

            if($order_by){
                $CI->db->order_by($order_by);
            }

            $array_groups = $CI->db->get();

            if ($recursive) {

                $results =  $array_groups->result_array();

                $groups = array_merge($groups, $results);

                if ($results) {

                    foreach ($results as $key => $result) {
                        $parent_id = $result['id'];
                        $parent_ids[] = $parent_id;
                    }

                    if ($parent_ids) {
                        
                        $results = get_asset_groups($order_by, $as_array, $recursive, $parent_ids,$groups);

                        if (!$results) {

                            return $groups;

                        } else {

                            return $groups = array_merge($groups, $results);

                        }
                    }
                   
                } else {

                    return $groups;
                }
            }

            if ($as_array) {
                return $array_groups->result_array();
            }

            return $array_groups->result();
        }

    }

    if (!function_exists('get_income_groups')) {

        function get_income_groups($order_by = null, $as_array = false, $recursive = 0, $parent_ids = array(),$groups = array())
        {
            $CI = &get_instance();

            if($parent_ids){
                $CI->db->where_in('parent_id', $parent_ids); 
                $parent_ids = [];
            } else {
                $CI->db->where('parent_id', 3); 
                $groups = [];
            }
         
            $CI->db->where('deleted_at', NULL);
            $CI->db->from('accounting_groups');

            if($order_by){
                $CI->db->order_by($order_by);
            }

            $array_groups = $CI->db->get();

            if ($recursive) {

                $results =  $array_groups->result_array();

                $groups = array_merge($groups, $results);

                if ($results) {

                    foreach ($results as $key => $result) {
                        $parent_id = $result['id'];
                        $parent_ids[] = $parent_id;
                    }

                    if ($parent_ids) {
                        
                        $results = get_income_groups($order_by, $as_array, $recursive, $parent_ids,$groups);

                        if (!$results) {

                            return $groups;

                        } else {

                            return $groups = array_merge($groups, $results);

                        }
                    }
                   
                } else {

                    return $groups;
                }
            }

            if ($as_array) {
                return $array_groups->result_array();
            }

            return $array_groups->result();
        }

    }
    if (!function_exists('get_all_by_slug')) {

        function get_all_by_slug($order_by = null, $as_array = false, $account = "")
        {
            $CI = &get_instance();


            $CI->db->where('slug', $account);

            $CI->db->where('deleted_at', NULL);
            $CI->db->from('accounting_groups');

            if($order_by){
                $CI->db->order_by($order_by);
            }

            $array_groups = $CI->db->get();

            if ($as_array) {
                return $array_groups->result_array();
            }

            return $array_groups->result();
        }

    }

    // if (!function_exists('get_expense_groups')) {
    //     function get_expense_groups($order_by = null, $as_array = false)
    //     {
    //         $CI = &get_instance();

    //         $CI->db->where('parent_id', 4); // expense id is 4
    //         $CI->db->where('deleted_at', NULL);
    //         $CI->db->from('accounting_groups');

    //         if($order_by){
    //             $CI->db->order_by($order_by);
    //         }
    //         $expense_groups = $CI->db->get();
    //         if ($as_array) {
    //             return $expense_groups->result_array();
    //         }
    //         return $expense_groups->result();
    //     }
    // }

    if (!function_exists('get_expense_groups')) {

        function get_expense_groups($order_by = null, $as_array = false, $recursive = 0, $parent_ids = array(),$groups = array())
        {
            $CI = &get_instance();

            if($parent_ids){
                $CI->db->where_in('parent_id', $parent_ids); 
                $parent_ids = [];
            } else {
                $CI->db->where('parent_id', 4); 
                $groups = [];
            }
         
            $CI->db->where('deleted_at', NULL);
            $CI->db->from('accounting_groups');

            if($order_by){
                $CI->db->order_by($order_by);
            }

            $array_groups = $CI->db->get();

            if ($recursive) {

                $results =  $array_groups->result_array();

                $groups = array_merge($groups, $results);

                if ($results) {

                    foreach ($results as $key => $result) {
                        $parent_id = $result['id'];
                        $parent_ids[] = $parent_id;
                    }

                    if ($parent_ids) {
                        
                        $results = get_expense_groups($order_by, $as_array, $recursive, $parent_ids,$groups);

                        if (!$results) {

                            return $groups;

                        } else {

                            return $groups = array_merge($groups, $results);

                        }
                    }
                   
                } else {

                    return $groups;
                }
            }

            if ($as_array) {
                return $array_groups->result_array();
            }

            return $array_groups->result();
        }

    }


    if (!function_exists('get_expense_ledgers')) {
        function get_expense_ledgers($order_by = null, $as_array = false)
        {
            $CI = &get_instance();
            $expense_group_ids = concat_ids(get_expense_groups());

            $CI->db->from('accounting_ledgers');
            $CI->db->where('deleted_at', NULL);
            $CI->db->where_in('group_id', $expense_group_ids);

            if($order_by){
                $CI->db->order_by($order_by);
            }else{
                $CI->db->order_by('name ASC');
            }

            $expense_ledgers = $CI->db->get();

            if ($as_array) {
                return $expense_ledgers->result_array();
            }
            return $expense_ledgers->result();

        }
    }