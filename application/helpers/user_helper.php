<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    if(!function_exists('current_user_id')) {
        function current_user_id()
        {
            $CI = &get_instance();
            return $CI->ion_auth->user()->row()->id;
        }
    }