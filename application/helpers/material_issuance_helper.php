<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    if (!function_exists('issuance_type_array')) {
        function issuance_type_array()
        {
            $status_list = array(
                0 => 'Select Option',
                1 => 'Standard Issuance',
                2 => 'Direct Out',
                3 => 'Transfer'
            );
            return $status_list;
        }
    }

    if (!function_exists('issuance_type_lookup')) {
        function issuance_type_lookup($key)
        {
            $status_list = issuance_type_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('issuance_type_reverse_lookup')) {
        function issuance_type_reverse_lookup($status)
        {
            $arr = issuance_type_array();
            foreach ($arr as $key => $value) {
                if ($status == $value) {
                    return true;
                }
            }
            return false;
        }
    }

    if (!function_exists('calculate_issuance_amount')) {
        /**
         * @throws ErrorException
         */
        function calculate_issuance_amount($obj)
        {
            if (array_key_exists('issuance_items', $obj)) {
                $total = 0.0;
                foreach ($obj['issuance_items'] as $mii) {
                    $total += $mii['subtotal'] * 1.0;
                }
                return $total;
            } else {
                throw new ErrorException("Object has no issuance_items attribute");
            }
        }
    }

    if (!function_exists('generate_material_issuance_items_for_payment_request')) {
        function generate_material_issuance_items_for_payment_request($items)
        {
            if (!is_array($items)) {
                return [];
            }
            if (sizeof($items) <= 0 || !$items) {
                return [];
            }
            $CI = &get_instance();
            $items_data = [];
            foreach ($items as $item) {
                $CI->load->library('MaterialReceivingLibrary');
                $mr_lib = new MaterialReceivingLibrary();
                $mr = $mr_lib->get_obj($item['material_receiving_item']['material_receiving_id']);
                $i = array(
                    'item_id' => $item['material_receiving_item']['item_id'],
                    'name' => $item['material_receiving_item']['item_name'],
                    'unit_price' => $item['unit_cost'] * 1.0,
                    'quantity' => $item['quantity'] * 1.0,
                    'tax_id' => null,
                    'rate' => 0.12,
                    'tax_amount' => $mr['input_tax'],
                    'total_amount' => $item['subtotal'] * 1.0
                );
                array_push($items_data, $i);
            }
            return $items_data;
        }
    }

    if (!function_exists('generate_payment_request_accounting_entries_for_material_issuance')) {
        function generate_payment_request_accounting_entries_for_material_issuance($obj)
        {
            $view_data = array(
                'accounting_entries' => array(
                    'id' => null,
                    'company_id' => $obj['material_request']['company_id'],
                    'cr_total' => $obj['total'],
                    'dr_total' => $obj['total'],
                ),
                'entry_item' => array(
                    'id' => null,
                    'ledger_id' => $obj['expense_account']['id'],
                    'dc' => 'c',
                    'description' => null,
                    'amount' => $obj['total'],
                )
            );

            return $view_data;

        }
    }

    if (!function_exists('generate_payment_request_data_for_material_issuance')) {
        /**
         * @throws ErrorException
         */
        function generate_payment_request_data_for_material_issuance($obj)
        {
            $view_data = [];
            $view_data['info']['payable_type_id'] = 2;
            $view_data['info']['payable_type']['name'] = "Issuance";

            $view_data['info']['origin_id'] = $obj['id'];

            $payee_data = [
                'payee_type' => 'staff',
                'payee_type_id' => $obj['material_request']['requesting_staff_id']
            ];

            $payee = get_payee($payee_data);

            $view_data['info']['payee_type'] = $payee_data['payee_type'];
            $view_data['info']['payee_name'] = $payee['last_name'] . ' ' . $payee['first_name'];
            $view_data['info']['payee_type_id'] = $payee_data['payee_type_id'];


            $gross_amount = calculate_issuance_amount($obj);
            $view_data['info']['gross_amount'] = $gross_amount;
            $view_data['info']['commission_amount'] = 0.0;

            $project_id = null;
            $project_name = null;
            if (array_key_exists('project_id', $obj)) {
                $project_id = $obj['project_id'];
                $project_name = get_value_field($project_id, 'projects', 'name');
            }
            $property_id = null;
            $property_name = null;
            if (array_key_exists('property_id', $obj)) {
                $property_id = $obj['property_id'];
                get_value_field($property_id, 'properties', 'name');
            }
            $sub_project_id = null;
            $sub_project_name = null;
            if (array_key_exists('sub_project_id', $obj)) {
                $sub_project_id = $obj['sub_project_id'];
            }

            $view_data['info']['project_id'] = $project_id;
            $view_data['info']['sub_project_id'] = $sub_project_id;
            $view_data['info']['property_id'] = $property_id;

            $view_data['info']['project']['name'] = $project_name;
            $view_data['info']['property']['name'] = $property_name;

            $view_data['info']['particulars'] = $obj['remarks'];
            $view_data['info']['wht_percentage'] = 0.0; //$wht_percentage;
            $view_data['info']['wht_amount'] = 0.0; //$commision_amount - $net_amount;

            $view_data['info']['total_due_amount'] = $gross_amount;

            // tbd
            $view_data['info']['approving_department_id'] = 2;
            $view_data['info']['requesting_department_id'] = 1;

            $view_data['info']['approving_department']['name'] = get_value_field(2, 'departments', 'name');
            $view_data['info']['requesting_department']['name'] = get_value_field(1, 'departments', 'name');
            // /tbd

            $view_data['info']['tax_percentage'] = 0.12;
            $view_data['info']['tax_amount'] = 0.0;
            if (array_key_exists('issuance_items', $obj)) {
                $view_data['items'] = generate_material_issuance_items_for_payment_request($obj['issuance_items']);
                $tax_amount = 0.0;
                foreach ($view_data['items'] as $item) {
                    $tax_amount += $item['tax_amount'];
                }
                $view_data['info']['tax_amount'] = $tax_amount;
            } else {
                $view_data['items'] = [];
            }

            $view_data += generate_payment_request_accounting_entries_for_material_issuance($obj, $gross_amount);

            return $view_data;

        }
    }
