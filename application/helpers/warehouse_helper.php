<?php
    if (!function_exists('warehouse_type_array')) {
        function warehouse_type_array()
        {
            $status_list = array(
                0 => 'Select Option',
                1 => 'Warehouse',
                2 => 'Sub Warehouse',
                3 => 'Virtual Warehouse'
            );
            return $status_list;
        }
    }

    if (!function_exists('warehouse_type_lookup')) {
        function warehouse_type_lookup($key)
        {
            $status_list = warehouse_type_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('inventory_log_transaction_keyval')) {
        function inventory_log_transaction_keyval($movement)
        {
            switch ($movement) {
                case 'receive':
                    return array(
                        'key' => 6,
                        'label' => warehouse_type_array()[6]
                    );
                case 'direct_in':
                    return array(
                        'key' => 4,
                        'label' => warehouse_type_array()[4]
                    );
                case 'release':
                    return array(
                        'key' => 7,
                        'label' => warehouse_type_array()[7]
                    );
            }
        }
    }

    if (!function_exists('warehouse_type_reverse_lookup')) {
        function warehouse_type_reverse_lookup($status)
        {
            $arr = warehouse_type_array();
            foreach ($arr as $key => $value) {
                if ($status == $value) {
                    return true;
                }
            }
            return false;
        }
    }
