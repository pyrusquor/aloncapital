<?php defined('BASEPATH') OR exit('No direct script access allowed.');

if ( ! function_exists('is_column_exists')) { 

	function is_column_exists($field = "", $table = "") {
		$CI = &get_instance();
		if ($CI->db->field_exists($field, $table)) {
       	 	return true;
		} else {
       	 	return false;
		}

	}

}

if ( ! function_exists('refactor_rows')) { 

	function refactor_rows($rows = "", $k = "", $v = "") {

		$results = array();
		if ($rows) {
			foreach ($rows as $key => $row) {
				$results[$row[$k]] = $row[$v];
			}
		}

		return $results;
	}

}

if ( ! function_exists('dbdate')) { 

	function dbdate($dbdate = "") {

		if ($dbdate) {
			return date('Y-m-d H:i:s',strtotime($dbdate));
		}

	}

}