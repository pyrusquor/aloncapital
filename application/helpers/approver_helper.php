<?php defined('BASEPATH') OR exit('No direct script access allowed.');

if ( ! function_exists('approver_test')) { 

	function get_approver($module_name) {
        $approver_setting = [];
        $CI = &get_instance();
        $CI->load->model('approver/Approver_model', 'M_approver');
        $CI->db->where("`link` = '$module_name'");
        $module = $CI->db->get('modules')->row_array();
        if($module){
            $CI->db->where('`module_id` = '. $module['id']);
            $approver_setting = $CI->M_approver->with_staff()->with_department()->get();
        }
        return $approver_setting;
	}
}