<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    if (!function_exists('rpo_status_array')) {
        function rpo_status_array()
        {
            $status_list = array(
                'Select Option',
                'New Request',
                'Approved',
                'Denied',
                'Cancelled'
            );
            return $status_list;
        }
    }

    if (!function_exists('rpo_status_lookup')) {
        function rpo_status_lookup($key)
        {
            $status_list = rpo_status_array();
            return $status_list[$key];
        }
    }

    if (!function_exists('rpo_status_reverse_lookup')) {
        function rpo_status_reverse_lookup($status)
        {
            $arr = rpo_status_array();
            foreach($arr as $key => $value){
                if($status == $value){
                    return true;
                }
            }
            return false;
        }
    }