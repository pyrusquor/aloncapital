<?php defined('BASEPATH') or exit('No direct script access allowed.');

if (!function_exists('get_dates')) {
    function get_dates($params = [])
    {
        $dates = explode('-', $params['daterange']);
        $date_from = db_date2($dates[0]);
        $date_to  = db_date2($dates[1]);

        if ($params['date_option_range'] == "daily") {

            $month_opt = "date";

            $months = get_day_periods($date_from, $date_to);
        } else if ($params['date_option_range'] == "weekly") {

            $month_opt = "daterange";

            $months = get_week_periods($date_from, $date_to);
        } else if ($params['date_option_range'] == "weekdays") {

            $month_opt = "daterange";

            $months = get_week_days($date_from);
        } else if ($params['date_option_range'] == "monthly") {

            $month_opt = "year_month";

            $date_option_range = "Y-m";

            $period = get_periods($date_from, $date_to);

            $months = array();

            foreach ($period as $key => $dt) {

                $months[] = $dt->format("Y-m");
            }
        } else if ($params['date_option_range'] == "yearly") {

            $month_opt = "year";

            $date_option_range = "Y";

            $period = get_year_periods($date_from, $date_to);

            $months = array();

            foreach ($period as $key => $dt) {
                $months[] = $dt->format("Y");
            }
        }

        $data['months'] = $months;
        $data['month_opt'] = $month_opt;
        $data['date_from'] = $date_from;
        $data['date_to'] = $date_to;

        return $data;
    }
}

if (!function_exists('get_day_periods')) {
	function get_day_periods($start, $end)
	{

		$dstart = date("Y-m-d", strtotime($start));
		$dend = date("Y-m-d", strtotime($end));

		$diff = date_converter($dstart, $dend);
		$count = 0;
		$rem = 0;

		if ($diff->format("%a") > 0) {
			$count =  $diff->format("%a");
		}

		$count = (int) $count;

		for ($i = 0; $i <= $count; $i++) {

			$date_initial = date('Y-m-d', strtotime($dstart . ' +' . $i . ' day'));
			$months[] = $date_initial;
		}

		return $months;
	}
}

if (!function_exists('get_week_periods')) {
	function get_week_periods($start, $end)
	{
		$start_day = date('Y-m-d');
		$dstart = date("Y-m-d", strtotime($start));
		$dend = date("Y-m-d", strtotime($end));

		$diff = date_converter($dstart, $dend);
		$count = 0;
		$rem = 0;

		if ($diff->format("%a") > 0) {
			$count =  $diff->format("%a") / 7;
		}

		$count = (int) $count;
		$start_day = $dstart;

		for ($i = 0; $i <= $count; $i++) {
			$days1 = 7 * $i;
			$days2 = (7 * $i) + 7;

			$date_initial = date('Y/m/d', strtotime($start_day . "+" . $days1 . " days"));
			$date_til =  date('Y/m/d', strtotime($start_day . "+" . $days2 . " days"));

			$months[] = $date_initial . "-" . $date_til;
		}

		return $months;
	}
}

if (!function_exists('get_periods')) {
	function get_periods($start, $end)
	{
		$start 		= new DateTime($start);
		$start->modify('first day of this month');
		$end 		= new DateTime($end);
		$end->modify('first day of next month');
		$interval 	= DateInterval::createFromDateString('1 month');
		$period 	= new DatePeriod($start, $interval, $end);

		return $period;
	}
}

if (!function_exists('get_year_periods')) {
	function get_year_periods($start, $end)
	{
		$start 		= new DateTime($start);
		$start->modify('first day of this year');
		$end 		= new DateTime($end);
		$end->modify('first day of next year');
		$interval 	= DateInterval::createFromDateString('1 year');
		$period 	= new DatePeriod($start, $interval, $end);

		return $period;
	}
}
if (!function_exists('get_week_days')) {
	function get_week_days($date)
	{
		$day_number = $first_day = date('w', strtotime($date));
		if($day_number==7){
			$first_day=0;
		}
		// $last_day = 6 - $day_number;

		$start = date('Y/m/d', strtotime("$date - $first_day day"));

		for($i=0; $i<=6; $i++){
			$period[] = date('Y/m/d', strtotime("$start + $i day"));
		}

		return $period;
	}
}
if (!function_exists('get_month_name')) {
	function get_month_name($month_num)
	{
		$months = array('','January','February','March','April','May','June','July','August','September','October','November','December');

		return $months[$month_num];
	}
}