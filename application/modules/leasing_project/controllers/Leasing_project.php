<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Leasing_project extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $models = [
            'Leasing_project_model' => 'M_leasing_project'
        ];

        // Load models
        $this->load->model($models);
    }

    public function index()
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index');
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'project_name' => true,
            'project_type' => true,
            'company' => true,
            'created_by' => true,
            'updated_by' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_leasing_project
                ->with_company('fields:name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("project_name like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'project_name' => [
                    'data' => $filters['project_name'] ?? null,
                    'operator' => 'like',
                ],
                'project_type_id' => [
                    'data' => $filters['project_type_id'] ?? null,
                    'operator' => '=',
                ],
                'company_id' => [
                    'data' => $filters['company_id'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['project_type'] = Dropdown::get_static('project_type', $value['project_type_id']) ?? '';

                        $items[$key]['company'] = $value['company']['name'] ?? '';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_leasing_project->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {
        $method = !!$id ? "Update" : "Create";

        if ($this->input->post()) {

            $this->db->trans_begin();

            $info = $this->input->post('info');

            $photo = $_FILES['file'];

            if ($id) {

                $updated_by = ['updated_by' => $this->user->id ?? NULL];

                $this->M_leasing_project->update($info + $updated_by, $id);
            } else {

                $id = $this->M_leasing_project->insert($info);
            }

            if (!!$photo['name']) {

                $this->uploadImage($id);
            }

            if ($this->db->trans_status() === FALSE) {

                $this->db->trans_rollback();
                $this->notify->error('Oops something went wrong.', 'leasing_project');
            } else {

                $this->db->trans_commit();
                $this->notify->success('Leasing Project successfully' . " " . $method, 'leasing_project');
            }
        } else {

            if ($id) {

                $this->view_data['info'] = $this->M_leasing_project->with_company()->get($id);
            }

            $this->template->build('form', $this->view_data);
        }
    }

    public function view($id = null)
    {
        if ($id) {

            $this->view_data['info'] = $this->M_leasing_project->with_company()->get($id);

            if ($this->view_data['info']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    private function uploadImage($leasing_project_id)
    {
        $_location = 'assets/img/leasing/project';

        // Create dir if does not exist
        if (!file_exists($_location)) {

            mkdir($_location, 0777, true);
        }

        $leasing_project = $this->M_leasing_project->get($leasing_project_id);

        if (file_exists($leasing_project['full_path'])) {

            unlink($leasing_project['full_path']);
        }

        $_config['upload_path'] = $_location;
        $_config['allowed_types'] = '*';
        $_config['overwrite'] = TRUE;
        $_config['max_size'] = '1000000';
        $_config['file_name'] = $leasing_project_id;

        $this->load->library('upload', $_config);

        if (!$this->upload->do_upload('file')) {

            $this->notify->error($this->upload->display_errors(), 'leasing/projects');
        } else {

            $photo_info['file_name'] = $this->upload->data('file_name');
            $photo_info['full_path'] = strstr($this->upload->data('full_path'), 'assets');

            $this->M_leasing_project->update($photo_info, $leasing_project_id);
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_leasing_project->get($id);
            if ($list) {

                $deleted = $this->M_leasing_project->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Leasing Project Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_leasing_project->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Leasing Project/s Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
}
