<?php

$project_name = $info['project_name'] ?? '';
$project_type_id = $info['project_type_id'] ?? '';
$company_id = $info['company']['id'] ?? '';
$company_name = $info['company']['name'] ?? '';
$location = $info['location'] ?? '';
$real_property_tax = $info['real_property_tax'] ?? '';
$price_minimum = $info['price_minimum'] ?? '';
$price_maximum = $info['price_maximum'] ?? '';
$status = $info['status'] ?? '';
$total_lot_area = $info['total_lot_area'] ?? '';
$total_leasable_area = $info['total_leasable_area'] ?? '';
$leasable_units = $info['leasable_units'] ?? '';
$total_gross_area = $info['total_gross_area'] ?? '';
$number_of_floors = $info['number_of_floors'] ?? '';
$leasable_units_per_floor = $info['leasable_units_per_floor'] ?? '';
$leasable_office = $info['leasable_office'] ?? '';
$total_beds = $info['total_beds'] ?? '';
$beds_per_floor = $info['beds_per_floor'] ?? '';
$embedded_map = $info['embedded_map'] ?? '';
$image_full_path = $info['full_path'] ?? '';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Project Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[project_name]" value="<?= $project_name ?>" placeholder="Project Name" required>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Project Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[project_type_id]', Dropdown::get_static("project_type"), $project_type_id, 'class="form-control"') ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <select type="text" class="form-control suggests" name="info[company_id]" data-module="companies" required>
                <option value="<?= $company_id ?>"><?= $company_name ?></option>
            </select>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Location (Full Address) <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[location]" value="<?= $location ?>" placeholder="Location" required>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Real Property Tax</label>
            <input type="number" class="form-control" name="info[real_property_tax]" value="<?= $real_property_tax ?>" placeholder="Real Property Tax">
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Price Minimum</label>
            <input type="number" class="form-control" name="info[price_minimum]" value="<?= $price_minimum ?>" placeholder="Price Minimum">
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Price Maximum</label>
            <input type="number" class="form-control" name="info[price_maximum]" value="<?= $price_maximum ?>" placeholder="Price Maximum">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Status <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[project_status_id]', Dropdown::get_static("project_status"), $project_status_id, 'class="form-control"') ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Total Lot Area</label>
            <div class="input-group">
                <input type="number" class="form-control" name="info[total_lot_area]" value="<?= $total_lot_area ?>" placeholder="Total Lot Area">
                <div class="input-group-append">
                    <span class="input-group-text">SQM</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Total Leasable Area</label>
            <div class="input-group">
                <input type="number" class="form-control" name="info[total_leasable_area]" value="<?= $total_leasable_area ?>" placeholder="Total Leasable Area">
                <div class="input-group-append">
                    <span class="input-group-text">SQM</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Leasable Units / Rooms</label>
            <input type="number" class="form-control" name="info[leasable_units]" value="<?= $leasable_units ?>" placeholder="Leasable Units / Rooms">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Total Gross Area</label>
            <div class="input-group">
                <input type="number" class="form-control" name="info[total_gross_area]" value="<?= $total_gross_area ?>" placeholder="Total Gross Area">
                <div class="input-group-append">
                    <span class="input-group-text">SQM</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Number of Floors</label>
            <input type="number" class="form-control" name="info[number_of_floors]" value="<?= $number_of_floors ?>" placeholder="Number of Floors">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Leasable Units per Floor</label>
            <input type="number" class="form-control" name="info[leasable_units_per_floor]" value="<?= $leasable_units_per_floor ?>" placeholder="Leasable Units per Floor">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Leasable Office / Commercial</label>
            <input type="number" class="form-control" name="info[leasable_office]" value="<?= $leasable_office ?>" placeholder="Leasable Office / Commercial">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Total Beds</label>
            <input type="number" class="form-control" name="info[total_beds]" value="<?= $total_beds ?>" placeholder="Total Beds">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Beds per Floor</label>
            <input type="number" class="form-control" name="info[beds_per_floor]" value="<?= $beds_per_floor ?>" placeholder="Beds per Floor">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Embedded Map</label>
            <input type="text" class="form-control" name="info[embedded_map]" value="<?= $embedded_map ?>" placeholder="Embedded Map">
        </div>
    </div>

    <div class="col-sm-6">
        <!--  -->
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Project Photo</label>
            <br>
            <input type="file" name="file" accept="image/png, image/jpeg, image/jpg">
        </div>
    </div>

    <div class="col-sm-6">
        <!--  -->
    </div>

    <?php if ($image_full_path) : ?>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Current Project Photo</label>
                <br>
                <img src="<?= $image_full_path ?>" alt="photo" width="200" height="200" class="img-fluid">
            </div>
        </div>
    <?php endif; ?>
</div>