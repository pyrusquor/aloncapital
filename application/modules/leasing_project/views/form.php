<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Create Leasing Project</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="leasing_projects_form">
                    <i class="fa fa-plus-circle"></i> Submit
                </button>
                <a href="<?php echo site_url('leasing_project'); ?>" class="btn btn-label-instagram btn-elevate btn-sm">
                    <i class="fa fa-reply"></i> Cancel
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <!--begin::Form-->
            <form method="post" class="kt-form kt-form--label-right" id="leasing_projects_form" enctype="multipart/form-data">
                <div class="kt-portlet__body">

                    <?php $this->load->view('_form'); ?>

                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
</div>
<!-- begin:: Footer -->