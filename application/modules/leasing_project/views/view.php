<?php

$id = $info['id'] ?? '';
$project_name = $info['project_name'] ?? '';
$project_type_id = $info['project_type_id'] ?? '';
$company_id = $info['company']['id'] ?? '';
$company_name = $info['company']['name'] ?? '';
$location = $info['location'] ?? '';
$real_property_tax = $info['real_property_tax'] ?? '';
$price_minimum = $info['price_minimum'] ?? '';
$price_maximum = $info['price_maximum'] ?? '';
$status = $info['status'] ?? '';
$total_lot_area = $info['total_lot_area'] ?? '';
$total_leasable_area = $info['total_leasable_area'] ?? '';
$leasable_units = $info['leasable_units'] ?? '';
$total_gross_area = $info['total_gross_area'] ?? '';
$number_of_floors = $info['number_of_floors'] ?? '';
$leasable_units_per_floor = $info['leasable_units_per_floor'] ?? '';
$leasable_office = $info['leasable_office'] ?? '';
$total_beds = $info['total_beds'] ?? '';
$beds_per_floor = $info['beds_per_floor'] ?? '';
$embedded_map = $info['embedded_map'] ?? '';
$image_full_path = $info['full_path'] ?? '';

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Leasing Project</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('leasing_project/form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('leasing_project'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Leasing Project
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Project Name:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $project_name ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Project Type:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $project_type_id ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Company:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $company_name ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Location (Full Address):
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $location ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Real Property Tax:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $real_property_tax ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Price Minimum:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $price_minimum ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Price Maximum:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $price_maximum ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Status:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $status ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Total Lot Area:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $total_lot_area ?></p>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Total Leasable Area:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $total_leasable_area ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Leasable Units / Rooms:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $leasable_units ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Total Gross Area:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $total_gross_area ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Number of Floors:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $number_of_floors ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Leasable Units per Floor:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $leasable_units_per_floor ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Leasable Office / Commercial:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $leasable_office ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Total Beds:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $total_beds ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Beds per Floor:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $beds_per_floor ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Embedded Map:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $embedded_map ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Project Photo:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">
                                        <?php if ($image_full_path) : ?>
                                            <img src="<?= $image_full_path ?>" alt="photo" width="200" height="200" class="img-fluid">
                                        <?php else : ?>
                                    <p class="kt-widget13__text kt-widget13__text--bold">No Photo</p>
                                <?php endif; ?>
                                </p>
                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->