<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Leasing_project_model extends My_Model
{
    public $table = 'leasing_projects';
    public $primary_key = 'id';
    public $fillable = [
        'project_name',
        'project_type_id',
        'company_id',
        'location',
        'real_property_tax',
        'price_minimum',
        'price_maximum',
        'project_status_id',
        'total_lot_area',
        'total_leasable_area',
        'leasable_units',
        'total_gross_area',
        'number_of_floors',
        'leasable_units_per_floor',
        'leasable_office',
        'total_beds',
        'beds_per_floor',
        'embedded_map',
        'file_name',
        'file_type',
        'file_src',
        'file_path',
        'full_path',
        'raw_name',
        'orig_name',
        'client_name',
        'file_ext',
        'file_size',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id'];
    public $rules = [];
    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['company'] = array('foreign_model' => 'company/Company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
    }
}
