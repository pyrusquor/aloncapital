<?php
$id = isset($warehouse_inventory_logs['id']) && $warehouse_inventory_logs['id'] ? $warehouse_inventory_logs['id'] : '';
$company = isset($warehouse_inventory_logs['company']) && $warehouse_inventory_logs['company'] ? $warehouse_inventory_logs['company'] : '';
$sbu = isset($warehouse_inventory_logs['sbu']) && $warehouse_inventory_logs['sbu'] ? $warehouse_inventory_logs['sbu'] : '';
$warehouse = isset($warehouse_inventory_logs['warehouse']) && $warehouse_inventory_logs['warehouse'] ? $warehouse_inventory_logs['warehouse'] : '';
$item_code = isset($warehouse_inventory_logs['item_code']) && $warehouse_inventory_logs['item_code'] ? $warehouse_inventory_logs['item_code'] : '';
$item_name = isset($warehouse_inventory_logs['item_name']) && $warehouse_inventory_logs['item_name'] ? $warehouse_inventory_logs['item_name'] : '';
$in = isset($warehouse_inventory_logs['in']) && $warehouse_inventory_logs['in'] ? $warehouse_inventory_logs['in'] : '';
$out = isset($warehouse_inventory_logs['out']) && $warehouse_inventory_logs['out'] ? $warehouse_inventory_logs['out'] : '';
$balance = isset($warehouse_inventory_logs['balance']) && $warehouse_inventory_logs['balance'] ? $warehouse_inventory_logs['balance'] : '';
$remarks = isset($warehouse_inventory_logs['remarks']) && $warehouse_inventory_logs['remarks'] ? $warehouse_inventory_logs['remarks'] : '';
$is_active = isset($warehouse_inventory_logs['is_active']) && $warehouse_inventory_logs['is_active'] ? $warehouse_inventory_logs['is_active'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="company" value="<?php echo set_value('company', $company); ?>" placeholder="Company" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building"></i></span>
            </div>
            <?php echo form_error('company'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>SBU <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="sbu" value="<?php echo set_value('sbu', $sbu); ?>" placeholder="SBU" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('sbu'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="warehouse" value="<?php echo set_value('warehouse', $warehouse); ?>" placeholder="Warehouse" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-industry"></i></span>
            </div>
            <?php echo form_error('warehouse'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_code" value="<?php echo set_value('item_code', $item_code); ?>" placeholder="Item Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('item_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_name" value="<?php echo set_value('item_name', $item_name); ?>" placeholder="Item name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-archive"></i></span>
            </div>
            <?php echo form_error('item_name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>In <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="in" value="<?php echo set_value('in', $in); ?>" placeholder="In" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sign-in"></i></span>
            </div>
            <?php echo form_error('in'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Out <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="out" value="<?php echo set_value('out', $out); ?>" placeholder="Out" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sign-out"></i></span>
            </div>
            <?php echo form_error('out'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Balance <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="balance" value="<?php echo set_value('balance', $balance); ?>" placeholder="Balance" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-exchange"></i></span>
            </div>
            <?php echo form_error('balance'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Remarks <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="remarks" value="<?php echo set_value('remarks', $remarks); ?>" placeholder="Remarks" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-align-left"></i></span>
            </div>
            <?php echo form_error('remarks'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Is Active <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), set_value('is_active', @$is_active), 'class="form-control"'); ?>
                <?php echo form_error('is_active'); ?>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-check-square"></i></span>
            </div>
        </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->

    <!-- ==================== end: Add form model fields ==================== -->
</div>