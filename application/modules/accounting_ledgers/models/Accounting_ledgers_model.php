<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_ledgers_model extends MY_Model
{
    public $table = 'accounting_ledgers'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'group_id',
        'account_id',
        'name',
        'color',
        'op_balance',
        'op_balance_dc',
        'type',
        'reconciliation',
        'accounts_receivable',
        'accounts_payable',
        'non_operating',
        'asset_type',
        'slug',
        'expense_type',
        'company_id',
        'created_at',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [

    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();

    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}
