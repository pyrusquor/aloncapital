<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Seller_position_model extends MY_Model {

	public $table = 'seller_positions'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = ['position_type_id', 'sales_group_id', 'name', 'rate', 'level', 'is_bypassable', 'created_by', 'created_at', 'updated_by', 'updated_at', 'deleted_by', 'deleted_at']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [
		'name' => array(
			'field'=>'name',
			'label'=>'Name',
			'rules'=>'trim|required'
        ),
        'rate' => array(
			'field'=>'rate',
			'label'=>'Rate',
			'rules'=>'trim|required'
		),
		'type' => array(
			'field'=>'type',
			'label'=>'Position type',
			'rules'=>'trim|required'
        ),
        'level' => array(
			'field'=>'level',
			'label'=>'Position Level',
			'rules'=>'trim|required'
        ),
        'is_bypassable' => array(
			'field'=>'is_bypassable',
			'label'=>'Is Bypassable',
			'rules'=>'trim|required'
		),
	];
	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		$this->has_one['seller_group'] = array('foreign_model' => 'seller_group/Seller_group_model', 'foreign_table' => 'seller_group', 'foreign_key' => 'id', 'local_key' => 'sales_group_id');
	}

	public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}