<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Position extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('position/Seller_position_model', 'M_seller_position');
		$this->_table_fillables = $this->M_seller_position->fillable;
		$this->_table_columns = $this->M_seller_position->__get_columns();
	}
	public function index()
	{
		$_fills = $this->_table_fillables;
		$_colms = $this->_table_columns;

		$this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns'] = $this->__get_columns($_fills);

		$db_columns = $this->M_seller_position->get_columns();
		if ($db_columns) {
			$column = [];
			foreach ($db_columns as $key => $dbclm) {
				$name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

				if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
					if (strpos($name, '_id') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('_id', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} elseif (strpos($name, 'is_') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('is_', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} else {
						$column[$key]['value'] = $name;
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					}
				} else {
					continue;
				}
			}

			$column_count = count($column);
			$cceil = ceil(($column_count / 2));

			$this->view_data['columns'] = array_chunk($column, $cceil);
			$column_group = $this->M_seller_position->count_rows();
			if ($column_group) {
				$this->view_data['total'] = $column_group;
			}
		}

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function showSellerPositions()
	{
		$output = ['data' => ''];

		$columnsDefault = [
			'id'     => true,
			'name'      => true,
			'position_type_id'      => true,
			'sales_group_id'      => true,
			'level'      => true,
			'rate'     => true,
			'is_bypassable'  => true,
			'created_by' => true,
			'updated_by' => true
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}

		// get all raw data
		$alldata = $this->M_seller_position->with_seller_group()->as_array()->get_all();


		$data = [];
		if ($alldata) {

			foreach ($alldata as $key => $value) {
				$alldata[$key]['sales_group_id'] = @$value['seller_group']['name'];

				$alldata[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

				$alldata[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
			}

			// internal use; filter selected columns only from raw data
			foreach ($alldata as $d) {
				$data[] = $this->filterArray($d, $columnsDefault);
			}

			// count data
			$totalRecords = $totalDisplay = count($data);

			// filter by general search keyword
			if (isset($_REQUEST['search'])) {
				$data         = $this->filterKeyword($data, $_REQUEST['search']);
				$totalDisplay = count($data);
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
				foreach ($_REQUEST['columns'] as $column) {
					if (isset($column['search'])) {
						$data         = $this->filterKeyword($data, $column['search'], $column['data']);
						$totalDisplay = count($data);
					}
				}
			}

			// sort
			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

				$_column = $_REQUEST['order'][0]['column'] - 1;
				$_dir = $_REQUEST['order'][0]['dir'];

				usort($data, function ($x, $y) use ($_column, $_dir) {

					// echo "<pre>";
					// print_r($x) ;echo "<br>";
					// echo $_column;echo "<br>";

					$x = array_slice($x, $_column, 1);

					// vdebug($x);

					$x = array_pop($x);

					$y = array_slice($y, $_column, 1);
					$y = array_pop($y);

					if ($_dir === 'asc') {

						return $x > $y ? true : false;
					} else {

						return $x < $y ? true : false;
					}
				});
			}

			// pagination length
			if (isset($_REQUEST['length'])) {
				$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
			}

			// return array values only without the keys
			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
				$tmp  = $data;
				$data = [];
				foreach ($tmp as $d) {
					$data[] = array_values($d);
				}
			}
			$secho = 0;
			if (isset($_REQUEST['sEcho'])) {
				$secho = intval($_REQUEST['sEcho']);
			}

			$output = array(
				'sEcho' => $secho,
				'sColumns' => '',
				'iTotalRecords' => $totalRecords,
				'iTotalDisplayRecords' => $totalDisplay,
				'data' => $data
			);
		}


		echo json_encode($output);
		exit();
	}

	public function create()
	{

		if ($this->input->post()) {

			$record = $this->input->post();

			$record['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
			$record['created_at']	=	NOW;

			$result = $this->M_seller_position->insert($record);

			if ($result === FALSE) {
				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Seller Position successfully created.', 'position');
			}
		}

		$this->template->build('create');
	}

	public function update($id = FALSE)
	{
		if ($id) {

			$this->view_data['position'] = $data =  $this->M_seller_position->get($id);

			if ($data) {

				if ($this->input->post()) {

					$record = $this->input->post();

					$record['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
					$record['created_at']	=	NOW;

					$result = $this->M_seller_position->update($record, $data['id']);

					if ($result === FALSE) {

						// Validation
						$this->notify->error('Oops something went wrong.');
					} else {

						// Success
						$this->notify->success('Seller Position successfully updated.', 'position');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function view($id = FALSE)
	{
		if ($id) {

			$this->view_data['position'] = $this->M_seller_position->get($id);

			if ($this->view_data['position']) {

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function delete($id = FALSE)
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_seller_position->get($id);
			if ($list) {

				$deleted = $this->M_seller_position->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Seller Position successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}
}
