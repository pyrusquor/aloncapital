<?php
    $name            =    isset($position['name']) && $position['name'] ? $position['name'] : '';
    $rate        =    isset($position['rate']) && $position['rate'] ? $position['rate'] : '';
    $position_type_id        =    isset($position['position_type_id']) && $position['position_type_id'] ? $position['position_type_id'] : '';
    $sales_group_id        =    isset($position['sales_group_id']) && $position['sales_group_id'] ? $position['sales_group_id'] : '';
    $level            =    isset($position['level']) && $position['level'] ? $position['level'] : '';
    $is_bypassable    =    isset($position['is_bypassable']) && $position['is_bypassable'] ? $position['is_bypassable'] : '';
?>


<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Position Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Position Name">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Position Level <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="level" value="<?php echo set_value('level', $level); ?>" placeholder="Position Level">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('level'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Position Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('position_type_id', Dropdown::get_static('position_type'), set_value('position_type_id', @$position_type_id), 'class="form-control"'); ?>
            <?php echo form_error('position_type_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Seller Rate <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="rate" value="<?php echo set_value('rate', $rate); ?>" placeholder="Seller Rate">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('rate'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Is Bypassable <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('is_bypassable', Dropdown::get_static('bool'), set_value('is_bypassable', @$is_bypassable), 'class="form-control"'); ?>
            <?php echo form_error('is_bypassable'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
        <label>Sales Group <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('sales_group_id', Dropdown::get_dynamic('seller_group'), set_value('sales_group_id', @$sales_group_id), 'class="form-control kt-select2" id="sales_group"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>