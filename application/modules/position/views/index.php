<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Seller Positions</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="total"></span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search Seller Position..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <!-- <div class="kt-subheader__wrapper">
                <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
					<i class="fa fa-filter"></i> Filter
				</button>
                <a href="<?php echo site_url('position/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
					<i class="fa fa-plus"></i> Create
				</a>
            </div> -->
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
            <a href="<?php echo site_url('position/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Create Seller Position
            </a>
        </div>

        <div class="module__filter">
            <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true">
                <i class="fa fa-filter"></i> Filter
            </button>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">

        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label class="form-control-label">Position Name</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" class="form-control form-control-sm _filter" placeholder="Position Name" id="_column_1" data-column="1">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-edit"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form-control-label">Position Type</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('_column_2', Dropdown::get_static('position_type'), set_value('_column_2'), 'class="form-control form-control-sm _filter" id="_column_2" data-column="2"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-edit"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form-control-label">Sales Group</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('_column_3', Dropdown::get_static('group_type'), set_value('_column_3'), 'class="form-control form-control-sm _filter" id="_column_3" data-column="3"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-group"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form-control-label">Sellers Rate</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="number" class="form-control form-control-sm _filter" placeholder="Sellers Rate" id="_column_5" data-column="5">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-dollar"></i></span></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="seller_position_table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Position Name</th>
                    <th>Position Type</th>
                    <th>Sales Group</th>
                    <th>Position Level</th>
                    <th>Seller's Rate</th>
                    <th>Is Bypassable</th>
                    <th>Created By</th>
                    <th>Last Update By</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->
    </div>
</div>