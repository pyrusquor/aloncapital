<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Construction_template extends MY_Controller
{
    private $fields = [
        array(
            'field' => 'construction_template[name]',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'construction_template[description]',
            'label' => 'Description',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Construction_template_model', 'M_Construction_template');
        $this->load->model('construction_template_items/Construction_template_items_model', 'M_Construction_template_item');

        $this->load->helper('form');

        $this->_table_fillables = $this->M_Construction_template->fillable;
        $this->_table_columns = $this->M_Construction_template->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Construction_template->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Construction_template->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showConstructionTemplates()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'name' => true,
            'description' => true,
            'created_by' => true,
            'updated_by' => true
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $construction_templates = $this->M_Construction_template->order_by('id', 'DESC')->as_array()->get_all();
        $data = [];

        if ($construction_templates) {

            foreach ($construction_templates as $key => $value) {

                $construction_templates[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                $construction_templates[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
            }

            foreach ($construction_templates as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    private function process_create_master($data, $additional)
    {
        $construction_template = $this->db->insert('construction_templates', array_merge($data, $additional));
        if ($construction_template) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    private function process_create_slave($data, $additional)
    {
        $construction_template_item = $this->M_Construction_template_item->insert($data, $additional);
        if ($construction_template_item) {
            return $this->db->insert_id();
        }
        return false;
    }

    private function process_update_master($data, $additional, $id)
    {
        return $this->M_Construction_template->update($data + $additional, $id);
    }

    private function process_update_slave($data, $additional, $construction_template_id)
    {
        /*
            * 1. Get all children of master
            * 2. If current item is not listed, create new
            * 3. Else, update
        */
        $id = isset($data['id']) ? $data['id'] : null;

        if ($id) {
            return $this->M_Construction_template_item->update($data + $additional, $id);
        } else {
            $data['construction_template_id'] = $construction_template_id;
            $additional = [
                'created_by' => $additional['updated_by'],
                'created_at' => $additional['updated_at']
            ];
            return $this->process_create_slave($data, $additional);
        }
    }

    private function process_purge_deleted($items, $additional)
    {
        if ($items) {
            foreach ($items as $item) {
                if ($item) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('construction_template_items', $data, array('id' => $item));
                    $this->M_Construction_template_item->delete($item);
                }
            }
        }
    }

    private function __search($params)
    {
        $id = isset($params['id']) ? $params['id'] : null;

        $this->M_Construction_template->where('id', $id)->order_by('id', 'DESC');

        $result = $this->M_Construction_template->get_all();

        return $result;
    }

    public function search()
    {
        $result = $this->__search($_GET);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function form($id = null)
    {
        $this->js_loader->queue([
            'js/vue2.js',
            // 'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js',
            'js/axios.min.js'
        ]);

        $this->view_data['id'] = $id;
        $this->view_data['fillables'] = $this->M_Construction_template->form_fillables;

        if ($id) {
            $method = "Update";
            $form_data = $this->M_Construction_template->get($id);
        } else {
            $method = "Create";
            $form_data = $this->M_Construction_template->fillable;
        }

        $this->view_data['form_data'] = $form_data;

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            if ($this->form_validation->run() === true) {
                $construction_template = $this->input->post('construction_template');
                $items = $this->input->post('items');
                $deleted_items = $this->input->post('deleted_items');

                if ($id) {

                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    $result = $this->process_update_master($construction_template, $additional, $id);
                    if ($result) {
                        foreach ($items as $item) {
                            $this->process_update_slave($item, $additional, $id);
                        }
                    }
                    $this->db->trans_complete(); # Completing construction_template
                    if ($this->db->trans_status() === false) {
                        # Something went wrong.
                        $this->db->trans_rollback();
                        $response['status'] = 0;
                        $response['message'] = 'Error!';
                    } else {
                        # Everything is Perfect.
                        # Committing data to the database.
                        $this->db->trans_commit();
                        $this->process_purge_deleted($deleted_items, $additional);

                        $response['status'] = 1;
                        $response['message'] = 'Construction Template Successfully ' . $method . 'd!';
                    }
                } else {

                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];
                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
                    $result = $this->process_create_master($construction_template, $additional);
                    if ($result) {
                        foreach ($items as $item) {
                            $item['construction_template_id'] = $result;
                            $this->process_create_slave($item, $additional);
                        }
                    }
                    $this->db->trans_complete(); # Completing construction_template
                    if ($this->db->trans_status() === false) {
                        # Something went wrong.
                        $this->db->trans_rollback();
                        $response['status'] = 0;
                        $response['message'] = 'Error!';
                    } else {
                        # Everything is Perfect.
                        # Committing data to the database.
                        $this->db->trans_commit();

                        $response['status'] = 1;
                        $response['message'] = 'Construction Template Successfully ' . $method . 'd!';
                    }
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }
            echo json_encode($response);
            exit();
        } else {

            $this->template->build('vueform', $this->view_data);
        }
    }

    public function _form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Construction_template->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                $data['name'] = $info['name'];
                $data['description'] = $info['description'];
                $item = $info['item'];

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $construction_template_status = $this->M_Construction_template->update($data + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $construction_template_status = $this->M_Construction_template->insert($data + $additional);

                    if ($item) {
                        foreach ($item as $key => $value) {
                            $items['construction_template_id'] = $construction_template_status;
                            $items['item_name'] = $value['item_name'];
                            $items['item'] = $value['item_name'];
                            $items['item_code'] = $value['item_code'];
                            $items['unit_of_measurement'] = $value['unit_of_measurement'];
                            $items['quantity'] = $value['quantity'];
                            $items['unit_price'] = $value['amenity'];
                            $items['subtotal'] = $value['sub_amenity'];

                            $construction_template_item = $this->M_Construction_template_item->insert($items + $additional);
                        }
                    }
                }

                if ($construction_template_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Construction Template Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_Construction_template->get($id);
        }

        $this->view_data['item_abc'] = $this->M_item_abc->get_all();
        $this->view_data['item_group'] = $this->M_item_group->get_all();
        $this->view_data['item_type'] = $this->M_item_type->get_all();

        $this->view_data['method'] = $method;
        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Construction_template->get($id);
            if ($list) {
                $data = [
                    'deleted_by' => $this->session->userdata['user_id']
                ];
                $this->db->update('construction_templates', $data, array('id' => $list['id']));
                $deleted = $this->M_Construction_template->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Construction Template Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('construction_templates', $data, array('id' => $value));
                    $deleted = $this->M_Construction_template->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Construction Template Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = FALSE)
    {
        if ($id) {

            $this->view_data['info'] = $this->M_Construction_template->get($id);

            // Get unique stages from the items
            $stages = $this->M_Construction_template_item->fields('stage_id')->group_by('stage_id')->where('construction_template_id', $id)->get_all();
            $grouped_items = [];
            foreach($stages as $key => $stage){
                if($stage){
                    $stage_id = $stage['stage_id'];
                    $stage_name = $stage['stage_id'] ? Dropdown::get_static('construction_template_stages', $stage_id, 'view') : 'None';
                    $items = $this->M_Construction_template_item->where(['construction_template_id' => $id, 'stage_id' => $stage_id])->get_all();
                    $grouped_items[] = [
                        'stage_id' => $stage_id,
                        'stage_name' => $stage_name,
                        'items' => $items
                    ];
                }

            }
            $this->view_data['grouped_items'] = $grouped_items;

            if ($this->view_data['info']) {
                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        /* ==================== begin: Add model fields ==================== */
                        'name' => $upload_data[0],
                        'description' => $upload_data[1],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $construction_template_id = $upload_data[0];
                    $construction_template = $this->M_Construction_template->get($construction_template_id);

                    if ($construction_template) {
                        $result = $this->M_Construction_template->update($fields, $construction_template_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[1],
                            'description' => $upload_data[2],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Construction_template->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[0],
                            'description' => $upload_data[1],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Construction_template->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'construction_template');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'construction_template');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued    =    $this->input->post('update_existing_data');

            $_is_update    =    $_ued === '1' ? TRUE : FALSE;

            $_alphas        =    [];
            $_datas            =    [];

            $_titles[]    =    'id';

            $_start    =    3;
            $_row        =    2;

            $_filename    =    'Construction Template CSV Template.csv';

            $_fillables    =    $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'construction_template');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[]    =    $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group    =    $this->M_Construction_template->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title]    =    isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas            =    $this->__get_excel_columns(count($_titles));
            $_xls_columns    =    array_combine($_alphas, $_titles);
            $_firstAlpha    =    reset($_alphas);
            $_lastAlpha        =    end($_alphas);

            $_objSheet    =    $this->excel->getActiveSheet();
            $_objSheet->setTitle('Construction Template');
            $_objSheet->setCellValue('A1', 'CONSTRUCTION TEMPLATE');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style    =    array(
                'font'  => array(
                    'bold'    =>    TRUE,
                    'size'    =>    10,
                    'name'    =>    'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $construction_templates    =    $this->M_Construction_template->as_array()->get_all();
        if ($construction_templates) {

            foreach ($construction_templates as $_lkey => $construction_template) {

                $_datas[$construction_template['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_construction_templates_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'construction_template');
                }
            } else {

                $_filename    =    'list_of_construction_templates_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'construction_template');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($construction_templates as $_lkey => $construction_template) {

                                $_datas[$construction_template['id']][$_title]    =    isset($construction_template[$_column]) && ($construction_template[$_column] !== '') ? Dropdown::get_static('bool', $construction_template[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($construction_templates as $_lkey => $construction_template) {

                                if ($_name === 'status') {

                                    $_datas[$construction_template['id']][$_title]    =    isset($construction_template[$_name]) && $construction_template[$_name] ? Dropdown::get_static('inventory_status', $construction_template[$_name], 'view') : '';
                                } else {

                                    $_datas[$construction_template['id']][$_title]    =    isset($construction_template[$_name]) && $construction_template[$_name] ? $construction_template[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Construction Template');
                $_objSheet->setCellValue('A1', 'LIST OF CONSTRUCTION TEMPLATE');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'construction_template');
            }
        } else {

            $this->notify->error('No Record Found', 'construction_template');
        }
    }
}
