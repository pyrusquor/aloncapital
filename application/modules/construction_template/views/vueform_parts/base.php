<!--begin: Basic Transaction Info-->
<div :class="wizardStepStateClass(1)" :data-ktwizard-state="wizardLabelState(1)">
    <?php $this->load->view('info'); ?>
</div>
<!--end: Basic Transaction Info-->

<!--begin: Item Form -->
<div :class="wizardStepStateClass(2)" :data-ktwizard-state="wizardLabelState(2)">
    <?php $this->load->view('item'); ?>
</div>
<!--end: Item Form -->

<!--begin: Form Actions -->
<div class="kt-form__actions">
    <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" v-if="info.form_is_valid && wizard.current == 2" @click.prevent="wizardSwitchPane(1)">
        Previous
    </div>
    <button class="btn btn-success btn-md btn-tall btn-wide" type="submit" @click.prevent="submitConstructionTemplate()" v-if="info.form_is_valid && wizard.current == 2 && construction_template_items.form_is_valid">
        Submit
    </button>
    <div id="next_btn" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
         v-if="info.form_is_valid && wizard.current == 1" @click.prevent="wizardSwitchPane(2)">
        Next Step
    </div>
</div>
<!--end: Form Actions -->