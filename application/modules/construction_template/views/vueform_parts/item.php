<?php $stages = Dropdown::get_static('construction_template_stages') ?>
<script type="text/javascript">
    var stages = [
        <?php foreach ($stages as $key => $stage) : ?> {
                id: '<?= $key ?>',
                name: '<?= $stage ?>'
            },
        <?php endforeach ?>
    ]
</script>

<div class="row">
    <div class="container">
        <h4>Filter Items</h4>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Group <span class="kt-font-danger">*</span></label>
                    <select class="form-control" v-model="construction_template_items.cart.item_group_id">
                        <option selected value="null">Select Group</option>
                        <option v-for="group in construction_template_items.data.item_groups" :value="group.id">
                            {{ group.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control" v-model="construction_template_items.cart.item_type_id" autocomplete="off" id="type_selection">
                        <option selected value="null">Select Type</option>
                        <option v-for="type in construction_template_items.data.item_types" :value="type.id">
                            {{ type.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Class Name</label>
                    <select class="form-control" name="class_selection" v-model="construction_template_items.cart.item_class_id" placeholder="Select Class" autocomplete="off" id="class_selection">
                        <option selected value="null">Select Class</option>
                        <option v-for="item_class in construction_template_items.data.item_classes" :value="item_class.id">
                            {{ item_class.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Brand Name</label>
                    <select class="form-control" name="brand_selection" v-model="construction_template_items.cart.item_brand_id" placeholder="Select brand" autocomplete="off" id="brand_selection">
                        <option selected value="null">Select Brand</option>
                        <option v-for="brand in construction_template_items.data.item_brands" :value="brand.id">
                            {{ brand.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Item Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="item_selection" v-model="construction_template_items.cart.item_id" placeholder="Select Item" autocomplete="off" id="item_selection">
                        <option selected value="null">Select Name</option>
                        <option v-for="item in construction_template_items.data.items" :value="item.id">
                            {{ item.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="">Unit of Measurement <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="unit_selection" v-model="construction_template_items.cart.unit_of_measurement_id" autocomplete="off" id="unit_selection">
                        <option selected value="null">Select Unit of Measurement</option>
                        <option v-for="unit in construction_template_items.data.units_of_measurement" :value="unit.id">
                            {{ unit.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="form-group">
                    <label class="">Quantity</label>
                    <input type="number" step="1" min="0" class="form-control" v-model="construction_template_items.cart.quantity">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Unit Cost</label>
                    <span class="form-control">
                        {{ construction_template_items.cart.unit_cost }}
                    </span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Sub Total Cost</label>
                    <span class="form-control">
                        {{ construction_template_items.cart.total_cost }}
                    </span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Stage <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="stage_selection" v-model="construction_template_items.cart.stage_id" autocomplete="off" id="stage_selection">
                        <option selected value="null">Select Stage</option>
                        <option v-for="stage in construction_template_items.data.stages" :value="stage.id">
                            {{ stage.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <a href="#!" class="btn btn-sm btn-primary" @click.prevent="addItemToConstructionTemplate('cart')" v-if="construction_template_items.cart_is_valid">
                Add to Items
            </a>
        </div>

    </div>
    <hr>
    <div class="container">
        <h4>Items</h4>
        <h5>Total: {{ info.form.construction_template_amount }}</h5>

        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Stage</th>
                        <th>Item</th>
                        <th>Group</th>
                        <th>Type</th>
                        <th>Brand</th>
                        <th>Class</th>
                        <th>Unit</th>
                        <th>Qty</th>
                        <th>SubTotal</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody v-if="construction_template_items.form">
                    <tr v-for="(ri, key) in construction_template_items.form">
                        <td>{{ ri.stage.name }}</td>
                        <td>{{ ri.item.name }}</td>
                        <td>{{ ri.item_group.name }}</td>
                        <td>{{ ri.item_type.name }}</td>
                        <td>{{ ri.item_brand.name }}</td>
                        <td>{{ ri.item_class.name }}</td>
                        <td>{{ ri.unit_of_measurement.name }}</td>
                        <td>{{ ri.quantity }}</td>
                        <td>{{ ri.total_cost }}</td>
                        <td>
                            <a href="#!" class="text-warning" @click.prevent="loadItem(key)">Edit</a>
                            <a href="#!" class="text-danger" @click.prevent="removeItem(key)">Delete</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>