<?php
// ==================== begin: Add model fields ====================
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$description = isset($info['description']) && $info['description'] ? $info['description'] : '';
// ==================== end: Add model fields ==================
?>

<div class="row">
    <div class="col-md-12">
        <!-- ==================== begin: Add form model fields ==================== -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Name <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
                    </div>
                    <?php echo form_error('name'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                        <label>Description <span class="kt-font-danger"></span></label>
                        <textarea class="form-control" id="description"
                            name="description"><?php echo set_value('description"', $description); ?></textarea>
                <?php echo form_error('description'); ?>
                <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
        <!-- ==================== end: Add form model fields ==================== -->
    </div>
</div>