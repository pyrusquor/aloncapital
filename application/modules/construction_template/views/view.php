<?php

$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$description = isset($info['description']) && $info['description'] ? $info['description'] : '';

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Construction Template</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('construction_template/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('construction_template');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
<div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab"
                           aria-selected="true">
                            <i class="flaticon2-user-outline-symbol"></i> Basic Information
                        </a>
                    </li>
                    <?php foreach($grouped_items as $gig_key => $g_grouped_item): ?>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#<?= $g_grouped_item['stage_name'] ?>" role="tab"
                           aria-selected="false">
                            <i class="fa fa-box-open"></i> <?= $g_grouped_item['stage_name'] ?>
                        </a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content kt-margin-t-20">
                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="basic_information" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row ">
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder">
                                                    <?= $info['name']; ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Description:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder">
                                                    <?= $info['description']; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <?php $i='2'; 
                foreach($grouped_items as $gi_key => $grouped_item): ?>
                <div class="tab-pane" id="<?= $grouped_item['stage_name'] ?>" role="tabpanel-<?= $i ?>">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col">
                                        <table class="table table-borderless">
                                            <thead>
                                            <tr>
                                                <th>Group</th>
                                                <th>Type</th>
                                                <th>Brand</th>
                                                <th>Class</th>
                                                <th>Item</th>
                                                <th>Unit</th>
                                                <th>Quantity</th>
                                                <th>Cost</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                $this->load->model('item/item_model');
                                                $this->load->model('item_brand/item_brand_model');
                                                $this->load->model('item_class/item_class_model');
                                                $this->load->model('item_type/item_type_model');
                                                $this->load->model('item_group/item_group_model');
                                                $this->load->model('inventory_settings_unit_of_measurement/inventory_settings_unit_of_measurement_model', 'unit_of_measurement_model');
                                                foreach ($grouped_item['items'] as $item):
                                                    $item_group = $this->item_group_model->get($item['item_group_id']);
                                                    $item_type = $this->item_type_model->get($item['item_type_id']);
                                                    $item_brand = $this->item_brand_model->get($item['item_brand_id']);
                                                    $item_class = $this->item_class_model->get($item['item_class_id']);
                                                    $__item = $this->item_model->get($item['item_id']);
                                                    $unit_of_measurement = $this->unit_of_measurement_model->get($item['unit_of_measurement_id']);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?= $item_group['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $item_type['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $item_brand['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $item_class['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $__item['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $unit_of_measurement['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $item['quantity']; ?>
                                                        </td>
                                                        <td>
                                                            <?= money_php($item['unit_cost']) ?> /
                                                            <?= money_php($item['total_cost']) ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
                <!--End:: Tab Content-->
            </div>
        </div>
    </div>
</div>
<!-- begin:: Footer -->
