<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaction_document_model extends MY_Model
{

	public $table = 'transaction_documents'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
		'document_id',
		'uploaded_document_id',
		'name',
		'owner',
		'category_id',
		'checklist',
		'file',
		'timeline',
		'start_date',
		'due_date', // start date - timeline 
		'status', // due date - start date 
		'has_file',
		'remarks',
		'transaction_status',
		'transaction_document_stage_id',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = TRUE;
		$this->return_as = 'array';

		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['transaction'] = array('foreign_model' => 'transaction/transaction_model', 'foreign_table' => 'transactions', 'foreign_key' => 'id', 'local_key' => 'transaction_id');

		$this->has_one['document'] = array('foreign_model' => 'document/Document_model', 'foreign_table' => 'documents', 'foreign_key' => 'id', 'local_key' => 'document_id');

		$this->has_one['checklist'] = array('foreign_model' => 'checklist/Checklist_model', 'foreign_table' => 'checklists', 'foreign_key' => 'id', 'local_key' => 'checklist');

		$this->has_one['transaction_document_stage'] = array('foreign_model' => 'transaction_document_stages/Transaction_document_stages_model', 'foreign_table' => 'transaction_document_stages', 'foreign_key' => 'id', 'local_key' => 'transaction_document_stage_id');
	}

	function _get_document_checklist($_li_id = FALSE)
	{

		$_return	=	FALSE;

		if ($_li_id) {

			$_sql	=	"
								SELECT d.`id`,
									d.`name`,
									d.`description`,
									d.`owner_id`,
									d.`classification_id`,
									d.`lead_time`,
									lid.`document_id`,
									lid.`uploaded_document_id`,
									lid.`transaction_id`,
									lid.`category_id`,
									lid.`checklist`,
									lid.`start_date`,
									lid.`owner`,
									lid.`status`,
									lid.`transaction_status`,
									lid.`transaction_document_stage_id`,
									'' AS `timeline`,
									'' AS `due_date`,
									lid.`created_at` AS `date_created`,
									d.`updated_at`
								FROM transaction_documents AS lid
								LEFT JOIN transactions AS li ON li.`id` = lid.`transaction_id`
								LEFT JOIN documents AS d ON d.`id` = lid.`document_id`
								WHERE lid.`deleted_at` IS NULL
									AND li.`deleted_at` IS NULL
									AND d.`deleted_at` IS NULL
									AND li.`id` = ?
							";

			$_query	=	$this->db->query($_sql, [$_li_id]);

			$_return	=	$_query->num_rows() > 0 ? $_query->result_array() : FALSE;
		}

		return $_return;
	}

	function find_all($_where = FALSE, $_columns = FALSE, $_row = FALSE)
	{

		$_return	=	FALSE;

		if ($_where) {

			if (is_array($_where)) {

				foreach ($_where as $key => $_wang) {

					$this->db->where($key, $_wang);
				}
			} else {

				$this->db->where('id', $_where);
			}
		}
		// else {

		// 	$this->db->where('deleted_at IS NULL');
		// }

		if ($_columns) {

			if (is_array($_columns)) {

				foreach ($_columns as $key => $_col) {

					$this->db->select($_col);
				}
			} else {

				$this->db->select($_columns);
			}
		} else {

			$this->db->select('*');
		}

		$this->db->where('deleted_at IS NULL');

		// if ( $_limit ) {

		// 	if ( is_numeric($_limit) ) {

		// 		$this->db->limit($_limit);
		// 	}
		// }

		$_query	=	$this->db->get($this->table);

		if ($_row) {

			$_return	=	$_query->num_rows() > 0 ? $_query->row() :  FALSE;
		} else {

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}

	function get_columns()
	{
		$_return = FALSE;

		if ($this->fillable) {
			$_return = $this->fillable;
		}

		return $_return;
	}
}
