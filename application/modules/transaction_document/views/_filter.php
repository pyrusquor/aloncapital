<?php if (!empty($records))  : //vdebug($documents); ?>
<?php foreach ($records as $r) : if(!isset($r['seller'])){ continue ;} ?>
<?php
        $id = $r['id'];
        $seller = @$r['seller'];
        $transaction = $r['transaction'];
        $transaction_id = $transaction['id'];
        $reference = $transaction['reference'];

        $project_name = get_value_field($transaction['project_id'],'projects','name');
        $property_name = get_value_field($transaction['property_id'],'properties','name');

        $seller_id = $seller['id'];
        $seller_name = get_fname($seller);

        $project_id = $transaction['project_id'];
        $property_id = $transaction['property_id'];
        
        $documents = $this->M_Transaction_document->as_array()->get_all(['transaction_id' => $transaction_id]);

        $total_document = count($documents);
    ?>
<!--begin:: Portlet-->
<div class="kt-portlet ">
    <div class="kt-portlet__body">
        <div class="kt-widget kt-widget--user-profile-3">
            <div class="kt-widget__top">
                <div class="kt-widget__media kt-hidden">
                    <img src="./assets/media/project-logos/3.png" alt="image">
                </div>
                <div
                    class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                    <?php echo get_initials($r['seller']['first_name'] . ' ' . $r['seller']['last_name']); ?>
                </div>
                <div class="kt-widget__content">
                    <div class="kt-widget__head">
                        <a href="<?php echo base_url(); ?>transaction/process_checklist/<?php echo $transaction_id; ?>"
                            class="kt-widget__username">
                            Reference : <?php echo $reference; ?>
                            <i class="flaticon2-correct"></i>
                        </a>


                        <div class="kt-widget__action">

                            <div class="kt-portlet__head kt-portlet__head--noborder" style="min-height: 0px;">
                                <div class="kt-portlet__head-toolbar">
                                    <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                        <i class="flaticon-more-1 kt-font-brand"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a href="<?php echo base_url('transaction/process_checklist/' . $transaction_id); ?>"
                                                    class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                    <span class="kt-nav__link-text">View</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="kt-widget__info">
                        <div class="kt-widget__desc">
                            <a href="<?php echo base_url(); ?>project/view/<?php echo $project_id; ?>" target="_BLANK"
                                class="kt-widget__username">
                                Project : <?php echo $project_name; ?>
                            </a>
                            <br>
                            <a href="<?php echo base_url(); ?>property/view/<?php echo $property_id; ?>" target="_BLANK"
                                class="kt-widget__username">
                                Property : <?php echo $property_name; ?>
                            </a>
                            <br>
                            <a href="<?php echo base_url(); ?>seller/view/<?php echo $seller_id; ?>" target="_BLANK"
                                class="kt-widget__username">
                                Seller Name : <?php echo $seller_name; ?>
                            </a>

                        </div>
                        <div class="kt-widget__desc">
                            <span class="kt-widget__username">
                                Document Total :
                                <u><b><?php echo $total_document; ?></u></b>
                            </span>
                            <br>
                            <span class="kt-widget__username">
                                Documents :
                                <?php if($documents): ?>
                                <?php foreach($documents as $key => $doc):?>
                                <?php
                                    $documment_id = $doc['document_id'];
                                    $document = get_person($documment_id,'documents');
                                    $document_name = get_name($document);
                                ?>
                                <u><b><?php echo $document_name; ?></u></b>
                            </span>
                            <br>
                            <?php endforeach; ?>
                                <?php else: ?>
                                <b><p class="font-italic">No documents found</p></b>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endforeach; ?>

<div class="row">
    <div class="col-xl-12">

        <!--begin:: Components/Pagination/Default-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">

                <!--begin: Pagination-->
                <div class="kt-pagination kt-pagination--brand">
                    <?php echo $this->ajax_pagination->create_links(); ?>

                    <div class="kt-pagination__toolbar">
                        <span class="pagination__desc">
                            <?php echo $this->ajax_pagination->show_count(); ?>
                        </span>
                    </div>
                </div>

                <!--end: Pagination-->
            </div>
        </div>

        <!--end:: Components/Pagination/Default-->
    </div>
</div>
<?php else : ?>
<div class="row">
    <div class="col-lg-12">
        <div class="kt-portlet kt-callout">
            <div class="kt-portlet__body">
                <div class="kt-callout__body">
                    <div class="kt-callout__content">
                        <h3 class="kt-callout__title">No Records Found</h3>
                        <p class="kt-callout__desc">
                            Sorry no record were found.
                        </p>
                    </div>
                    <div class="kt-callout__action">
                        <a href="<?php echo base_url('transaction/form'); ?>"
                            class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>