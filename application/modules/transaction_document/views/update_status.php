<?php

$transaction_status = isset($info['transaction_status']) && $info['transaction_status'] ? $info['transaction_status'] : 'unknown';

$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';

?>
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Update Transaction Document</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="update_form">
                    <i class="fa fa-plus-circle"></i> Submit
                </button>
                <a href="<?php echo site_url('transaction_document'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <!--begin::Form-->
                <form method="POST" id="update_form">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="">Status <span class="kt-font-danger">*</span></label>
                                <select class="form-control" id="transaction_status" name="transaction_status" required>
                                    <option value="">Select Status</option>
                                    <option value="1" <?php if ($transaction_status == 1) : ?> selected="selected" <?php endif; ?>>Pending</option>
                                    <option value="2" <?php if ($transaction_status == 2) : ?> selected="selected" <?php endif; ?>>Completed</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="">Remarks</label>
                                <textarea class="form-control" name="remarks" id="remarks" cols="30" rows="5"><?php echo $remarks; ?></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>