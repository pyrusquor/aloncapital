<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_document extends MY_Controller
{

	private $fields = [
		array(
			'field' => 'property[property_id]',
			'label' => 'Property Name',
			'rules' => 'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();

		$transaction_models = array(
			'transaction/Transaction_model' => 'M_transaction',
			'Transaction_document_model' => 'M_Transaction_document',
			'Transaction/transaction_seller_model' => 'M_Transaction_seller'
		);

		// Load models
		$this->load->model('auth/Ion_auth_model', 'M_auth');
		$this->load->model('user/User_model', 'M_user');
		$this->load->model($transaction_models);


		// Load pagination library 
		$this->load->library('ajax_pagination');

		// Format Helper
		$this->load->helper(['format', 'images']); // Load Helper

		$this->load->library('mortgage_computation');

		// Per page limit 
		$this->perPage = 12;

		$this->_table_fillables		=	$this->M_Transaction_document->fillable;
		$this->_table_columns		=	$this->M_Transaction_document->__get_columns();
	}

	public function template($page = "")
	{

		$this->template->build($page);
	}

	public function index()
	{
		$_fills = $this->_table_fillables;
		$_colms = $this->_table_columns;

		$this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns'] = $this->__get_columns($_fills);

		$db_columns = $this->M_Transaction_document->get_columns();
		if ($db_columns) {
			$column = [];
			foreach ($db_columns as $key => $dbclm) {
				$name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

				if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
					if (strpos($name, '_id') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('_id', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} elseif (strpos($name, 'is_') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('is_', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} else {
						$column[$key]['value'] = $name;
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					}
				} else {
					continue;
				}
			}

			$column_count = count($column);
			$cceil = ceil(($column_count / 2));

			$this->view_data['columns'] = array_chunk($column, $cceil);
			$column_group = $this->M_Transaction_document->count_rows();
			if ($column_group) {
				$this->view_data['total'] = $column_group;
			}
		}

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function showTransactionDocuments()
	{

		$where = $this->input->post('where') ? $this->input->post('where') : [];

		$output = ['data' => ''];

		$columnsDefault = [
			'id' => true,
			'transaction_id' => true,
			'transaction_reference' => true,
			'project' => true,
			'property' => true,
			'buyer' => true,
			'document_id' => true,
			'document_name' => true,
			'owner' => true,
			'category_id' => true,
			'checklist' => true,
			'file' => true,
			'timeline' => true,
			'start_date' => true,
			'due_date' => true,
			'status' => true,
			'has_file' => true,
			'doc_id' => true,
			'transaction_document_stage_id' => true,
			'remarks' => true,
			'uploaded_document_id' => true,
			'transaction_status' => true,
			'created_by' => true,
			'updated_by' => true
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}

		// get all raw data
		$document = $this->M_Transaction_document->where($where)->with_document()->with_transaction_document_stage()->as_array()->get_all();

		$data = [];

		if ($document) {
			foreach ($document as $key => $value) {

				$transaction = $this->M_transaction->with_buyer('fields:id,first_name,last_name')->with_project('fields:id,name')->with_property('fields:id,name')->as_array()->get($value['transaction_id']);

				$document[$key]['transaction_reference'] = isset($transaction['reference']) ? '<a href="' . site_url('transaction/view/' . $transaction['id']) . '">' . $transaction['reference'] . '</a>' : '';
				$document[$key]['project'] = isset($transaction['project']['name']) ? '<a href="' . site_url('project/view/' . $transaction['project']['id']) . '">' . $transaction['project']['name'] . '</a>' : '';
				$document[$key]['property'] = isset($transaction['property']['name']) ? '<a href="' . site_url('property/view/' . $transaction['property']['id']) . '">' . $transaction['property']['name'] . '</a>' : '';
				$document[$key]['buyer'] = isset($transaction['buyer']) ? '<a href="' . site_url('buyer/view/' . $transaction['buyer']['id']) . '">' . get_fname($transaction['buyer']) . '</a>' : '';

				$document[$key]['doc_id'] = @$value['document']['id'];
				$document[$key]['document_name'] = @$value['document']['name'];
				$document[$key]['transaction_document_stage_id'] = @$value['transaction_document_stage']['name'];
				$document[$key]['timeline'] = @$value['document']['lead_time'] . " day(s)";
				$due_date =  date('Y-m-d H:i:s', strtotime(@$value['start_date'] . ' +' . @$value['document']['lead_time'] . 'day'));
				$document[$key]['due_date'] = date_format(date_create($due_date), 'F j, Y');
				$document[$key]['start_date'] = $start_date =  date_format(date_create($value['start_date']), 'F j, Y');

				$now = time(); // or your date as well

				if ($start_date) {
					$now = strtotime($start_date);
				}

				$your_date = strtotime($due_date);
				$datediff = $now - $your_date;
				$over_due = floor($datediff / (60 * 60 * 24));
				$months = floor($over_due / 30);
				$days = $over_due;

				$transaction_status = $document[$key]['transaction_status'] == 2 ? "Completed" : "Pending";

				if ($due_date) {
					if (($over_due < 0 && $over_due == -1)) {
						$document[$key]['status'] = abs($over_due) . " day(s) until overdue - <br> " . $transaction_status;
					} else if ($over_due == 0) {
						$document[$key]['status'] = "Due Today - <br> " . $transaction_status;
					} else if ($over_due < 0) {
						$document[$key]['status'] = abs($days) . " day(s) until overdue - <br> " . $transaction_status;
					} else if ($months == 0 && $days > 0) {
						$document[$key]['status'] = abs($days) . ' day(s) overdue - <br> ' . $transaction_status;
					} else if (($over_due > 0) && ($over_due < 15)) {
						$document[$key]['status'] = abs($days) . ' day(s) overdue - <br> ' . $transaction_status;
					} else if ($over_due > 14) {
						$document[$key]['status'] = abs($days) . ' day(s) overdue - <br> ' . $transaction_status;
					}
				}

				$document[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

				$document[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
			}

			foreach ($document as $d) {
				$data[] = $this->filterArray($d, $columnsDefault);
			}

			// count data
			$totalRecords = $totalDisplay = count($data);

			// filter by general search keyword
			if (isset($_REQUEST['search'])) {
				$data = $this->filterKeyword($data, $_REQUEST['search']);
				$totalDisplay = count($data);
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
				foreach ($_REQUEST['columns'] as $column) {
					if (isset($column['search'])) {
						$data = $this->filterKeyword($data, $column['search'], $column['data']);
						$totalDisplay = count($data);
					}
				}
			}

			// sort
			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

				$_column = $_REQUEST['order'][0]['column'] - 1;
				$_dir = $_REQUEST['order'][0]['dir'];

				usort($data, function ($x, $y) use ($_column, $_dir) {

					// echo "<pre>";
					// print_r($x) ;echo "<br>";
					// echo $_column;echo "<br>";

					$x = array_slice($x, $_column, 1);

					// vdebug($x);

					$x = array_pop($x);

					$y = array_slice($y, $_column, 1);
					$y = array_pop($y);

					if ($_dir === 'asc') {

						return $x > $y ? true : false;
					} else {

						return $x < $y ? true : false;
					}
				});
			}

			// pagination length
			if (isset($_REQUEST['length'])) {
				$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
			}

			// return array values only without the keys
			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
				$tmp = $data;
				$data = [];
				foreach ($tmp as $d) {
					$data[] = array_values($d);
				}
			}
			$secho = 0;
			if (isset($_REQUEST['sEcho'])) {
				$secho = intval($_REQUEST['sEcho']);
			}

			$output = array(
				'sEcho' => $secho,
				'sColumns' => '',
				'iTotalRecords' => $totalRecords,
				'iTotalDisplayRecords' => $totalDisplay,
				'data' => $data,
			);
		}

		echo json_encode($output);
		exit();
	}

	public function view($id = FALSE, $type = '')
	{
		$this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

		$this->js_loader->queue([
			'//www.amcharts.com/lib/3/amcharts.js',
			'//www.amcharts.com/lib/3/serial.js',
			'//www.amcharts.com/lib/3/radar.js',
			'//www.amcharts.com/lib/3/pie.js',
			'//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
			'//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
			'//www.amcharts.com/lib/3/plugins/export/export.min.js',
			'//www.amcharts.com/lib/3/themes/light.js'
		]);

		if ($id) {

			$this->view_data['info'] =  $this->M_transaction
				->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_payments()->with_payments()
				->get($id);

			$this->view_data['seller'] = $this->M_Transaction_seller->order_by('id', 'DESC')
				->with_documents()->with_seller()
				->get(array('transaction_id' => $id));

			$this->view_data['transaction_id'] = $id;

			if ($this->view_data['info']) {

				if ($type == "debug") {

					vdebug($this->view_data);
				}

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];
		$_extra_datas		=	[];
		$_adatas			=	[];

		$_titles[]	=	'#';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		// $transactions	=	$this->M_Transaction_document->as_array()->get_all();
		$transactions = $this->M_transaction
			->with_source('fields:recruiter, supervisor, net_worth, to_report, to_attend, document_based, is_member, group')
			->with_reference('fields: ref_name, ref_address, ref_contact_no')
			->with_work_experience('fields: employer, designation, emp_address, salary, emp_contact_no, supervisor, start_date, end_date, reason, to_contact')
			->with_academic('fields: level, course, school, year, rating')
			->with_exam()
			->with_training()
			->with_position('fields:name')
			->get_all();

		if ($transactions) {

			foreach ($transactions as $skey => $transaction) {

				$_datas[$transaction['id']]['#']	=	$_no;

				$_no++;
			}


			$_filename	=	'list_of_transactions' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				$_additional_column	=	$this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

						if ((strpos($_name, 'source') !== FALSE) or (strpos($_name, 'reference') !== FALSE) or (strpos($_name, 'academic') !== FALSE)) {

							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($transactions as $skey => $transaction) {

								$_extra_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
							}
						} elseif ((strpos($_name, 'work_experience') !== FALSE)) {
							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? $_name : '';

							foreach ($transactions as $skey => $transaction) {

								$_extra_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
							}
						} else {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($transactions as $skey => $transaction) {

								if ($_column === 'sales_group_id') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_static('group_type', $transaction[$_column], 'view') : '';
								} elseif ($_column === 'transaction_position_id') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('transaction_positions', $transaction[$_column], 'name', 'id', 'view') : '';
								} elseif ($_column === 'birth_date') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] && (strtotime($transaction[$_column]) > 0) ? date_format(date_create($transaction[$_column]), 'm/d/Y') : '';
								} elseif ($_column === 'gender') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_static('sex', $transaction[$_column], 'view') : '';
								} elseif ($_column === 'is_active') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_static('bool', $transaction[$_column], 'view') : '';
								} else {
									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
								}
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title	=	ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('List of Transactions');
				$_objSheet->setCellValue('A1', 'LIST OF SELLERS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($transactions as $key => $transaction) {

					$transaction_id	=	isset($transaction['id']) && $transaction['id'] ? $transaction['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title	=	ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$transaction_id][$_xls_columns[$_alpha]]) && $_datas[$transaction_id][$_xls_columns[$_alpha]] ? $_datas[$transaction_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns	=	$_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							if ($_a_column === 'contact') {

								$ad_title	=	'Contact Informations';
							} elseif ($_a_column === 'reference') {

								$ad_title	=	'References';
							} elseif ($_a_column === 'source') {

								$ad_title	=	'Source of Informations';
							} elseif ($_a_column === 'academic') {

								$ad_title	=	'Academic History';
							} else {

								$ad_title = $_a_column;
							}

							$a_title	=	ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if ($key === 'to_report' or $key === 'to_attend' or $key === 'document_based' or $key === 'is_member') {
												$xa_value	=	isset($_extra_datas[$transaction_id][$_a_column][$key]) && ($_extra_datas[$transaction_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$transaction_id][$_a_column][$key], 'view') : '';
											} else {
												$xa_value	=	$_extra_datas[$transaction_id][$_a_column][$key];
											}

											$xa_titles =	isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$transaction_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$transaction_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					}

					$_start += 1;
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}
		} else {

			$this->notify->error('No Record Found', 'transaction');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Transaction CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_Transaction_document->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Transactions');
			$_objSheet->setCellValue('A1', 'SELLERS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_failed_reasons = [];
				$_inserted	=	0;

				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {
							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							$_data['birth_date']	=	isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
							$transactionGroupID = ['3'];

							if ($_id) {
								$data	=	$this->M_Transaction_document->get($_id);
								if ($data) {

									unset($_data['id']);

									$oldPwd = password_format($data['last_name'], $data['birth_date']);
									$newPwd = password_format($_data['last_name'], $_data['birth_date']);

									$oldEmail = $data['email'];
									$newEmail = $_data['email'];

									if ($this->M_auth->email_check($oldEmail)) {

										if ($oldPwd !== $newPwd) {
											// Update User Password
											$this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
										}

										if ($oldEmail !== $newEmail) {
											// Update User Email
											$_user_data = [
												'email' => $newEmail,
												'username' => $newEmail,
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											];
											$this->M_user->update($_user_data, array('id' => $data['user_id']));
										}

										// Update Transaction Info
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_update	=	$this->M_Transaction_document->update($_data, $_id);
										if ($_update !== FALSE) {

											$user_data = array(
												'first_name' => $_data['first_name'],
												'last_name' => $_data['last_name'],
												'active' => $_data['is_active'],
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											);

											$this->M_user->update($user_data, $data['user_id']);

											$_updated++;
										} else {

											$_failed_reasons[$_data['id']][] = 'update not working.';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email already used';
										$_failed++;

										break;
									}
								} else {

									// Generate user password
									$transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
									$transactionEmail = $_data['email'];

									if (!$this->M_auth->email_check($_data['email'])) {

										$user_data = array(
											'first_name' => $_data['first_name'],
											'last_name' => $_data['last_name'],
											'active' => $_data['is_active'],
											'created_by' => $this->user->id,
											'created_at' => NOW
										);

										$userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

										if ($userID !== FALSE) {

											$_data['user_id']	= $userID;
											$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['created_at']	=	NOW;
											$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['updated_at']	=	NOW;

											$_insert	=	$this->M_Transaction_document->insert($_data);
											if ($_insert !== FALSE) {

												$_inserted++;
											} else {

												$_failed_reasons[$_data['id']][] = 'insert transaction not working';
												$_failed++;

												break;
											}
										} else {

											$_failed_reasons[$_data['id']][] = 'insert ion auth not working';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email check already existing';
										$_failed++;

										break;
									}
								}
							} else {

								// Generate user password
								$transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
								$transactionEmail = $_data['email'];

								if (!$this->M_auth->email_check($_data['email'])) {

									$user_data = array(
										'first_name' => $_data['first_name'],
										'last_name' => $_data['last_name'],
										'active' => $_data['is_active'],
										'created_by' => $this->user->id,
										'created_at' => NOW
									);

									$userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

									if ($userID !== FALSE) {

										$_data['user_id']	= $userID;
										$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['created_at']	=	NOW;
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_insert	=	$this->M_Transaction_document->insert($_data);
										if ($_insert !== FALSE) {

											$_inserted++;
										} else {

											$_failed_reasons[$_dkey][] = 'insert transaction not working';

											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_dkey][] = 'insert ion auth not working';

										$_failed++;

										break;
									}
								} else {

									$_failed_reasons[$_dkey][] = 'email check already existing';

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {
							$this->notify->error('Upload Failed! Please follow upload guide. ', 'transaction');
						} else {

							$this->notify->success($_msg . '.', 'transaction');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'transaction');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'transaction');
			}
		} else {

			$this->notify->error('Something went wrong!', 'transaction');
		}
	}

	public function upload_file($_li_id = FALSE, $_doc_id = FALSE)
	{

		$redirect = $this->input->get('redirect') ? $this->input->get('redirect') . $_li_id : 'transaction_document';

		if ($_li_id && $_doc_id && isset($_FILES['_file']['name']) && ($_FILES['_file']['name'] !== '')) {

			$this->load->model('transaction_document/Transaction_document_model', 'M_transaction_document');

			unset($_where);
			$_where['transaction_id'] = $_li_id;
			$_where['document_id'] = $_doc_id;

			$_li_document = $this->M_transaction_document->find_all($_where, ['id'], TRUE);

			if ($_li_document && isset($_li_document->id) && $_li_document->id) {

				$_ext = ['.jpg', '.jpeg', '.png'];

				set_time_limit(0);

				$_location = 'assets/img/_transaction/_documents/_process';

				if (!file_exists($_location)) {
					mkdir($_location, 0777);
				}

				unset($_config);
				$_config['upload_path']	= $_location;
				$_config['allowed_types'] = '*';
				$_config['overwrite'] = TRUE;
				$_config['max_size'] = '1000000';
				$_config['file_name'] = $_filename = $_doc_id . '_thumb';

				if (isset($_ext) && $_ext) {

					$_image	=	$_location . '/' . $_filename;

					foreach ($_ext as $key => $x) {

						if (file_exists($_image . $x)) {

							unlink($_image . $x);
						}
					}
				}

				$this->load->library('upload', $_config);

				if (!$this->upload->do_upload('_file')) {

					$this->notify->error($this->upload->displaY_errors(), 'transaction_document');
				} else {

					$_uploaded_document_id	=	FALSE;

					$this->load->model('document/Transaction_document_upload_model', 'M_transaction_uploaded_document');

					$_udatas	=	$this->upload->data();

					if ($_udatas) {

						unset($_where);
						$_where['transaction_id']	=	$_li_id;
						$_where['document_id']				=	$_doc_id;

						$_uploaded_document	=	$this->M_transaction_uploaded_document->find_all($_where, ['id'], TRUE); #lqq(); ud($_uploaded_document);

						if ($_uploaded_document && isset($_uploaded_document->id) && $_uploaded_document->id) {

							$_uploaded_document_id	=	$_uploaded_document->id;

							unset($_update);
							$_update['transaction_id']	=	$_li_id;
							$_update['document_id']				=	$_doc_id;
							$_update['file_name']					=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_update['file_type']					=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_update['file_src']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_update['file_path']					=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_update['full_path']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_update['raw_name']					=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_update['orig_name']					=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_update['client_name']				=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_update['file_ext']					=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_update['file_size']					=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_update['updated_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']				=	NOW;

							$_updated	=	$this->M_transaction_uploaded_document->update($_update, $_uploaded_document_id);
							if (!$_updated) {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'transaction_document');
							}
						} else {

							unset($_insert);
							$_insert['transaction_id']	=	$_li_id;
							$_insert['document_id']				=	$_doc_id;
							$_insert['file_name']					=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_insert['file_type']					=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_insert['file_src']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_insert['file_path']					=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_insert['full_path']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_insert['raw_name']					=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_insert['orig_name']					=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_insert['client_name']				=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_insert['file_ext']					=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_insert['file_size']					=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_insert['created_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['created_at']				=	NOW;
							$_insert['updated_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['updated_at']				=	NOW;

							$_inserted	=	$this->M_transaction_uploaded_document->insert($_insert);
							if ($_inserted) {

								// $_uploaded_document_id	=	$this->db->insert_id();
								$_uploaded_document_id	=	$_inserted;
							} else {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'transaction_document');
							}
						}

						if ($_uploaded_document_id) {

							unset($_update);
							$_update['uploaded_document_id'] = $_uploaded_document_id;
							$_update['has_file'] = 1;
							$_update['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at'] = NOW;

							$_updated = $this->M_transaction_document->update($_update, $_li_document->id);
							if ($_updated) {
								$this->transaction_library->initiate($_li_id);
								$this->transaction_library->check_commission_status($_li_id, $this->user->id);
								$this->notify->success('Success! Image uploaded.', $redirect);
							} else {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'transaction_document');
							}
						} else {

							$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction_document');
						}
					}
				}
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function update_status($id = false)
	{
		if ($this->input->post()) {

			$response['status'] = 0;
			$response['message'] = 'Oops! Please refresh the page and try again.';

			$info = $this->input->post();

			$result = $this->M_Transaction_document->update($info, $id);

			if ($result) {

				$response['status'] = true;
				$response['message'] = 'Transaction Document successfully Updated.';
			}

			echo json_encode($response);
		}
	}
}
