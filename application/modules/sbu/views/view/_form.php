<?php
$id = isset($sbu['id']) && $sbu['id'] ? $sbu['id'] : '';
$name = isset($sbu['name']) && $sbu['name'] ? $sbu['name'] : '';
$company = isset($sbu['company']) && $sbu['company'] ? $sbu['company'] : '';
$code = isset($sbu['code']) && $sbu['code'] ? $sbu['code'] : '';
$is_active = isset($sbu['is_active']) && $sbu['is_active'] ? $sbu['is_active'] : '';

$company_name = isset($sbu['company']['name']) && $sbu['company']['name'] ? $sbu['company']['name'] : 'N/A';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="companies"  id="company_id" name="company">
                    <option value="">Select Company</option>
                    <?php if ($company_name): ?>
                        <option value="<?php echo @$company['id']; ?>" selected><?php echo $company_name; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('company'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="code" value="<?php echo set_value('code', $code); ?>" placeholder="Code" autocomplete="off">
            </div>
            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Is Active <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icont">
            <?php echo form_dropdown('is_active', Dropdown::get_static('company_status'), set_value('is_active', @$is_active), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->

    <!-- ==================== end: Add form model fields ==================== -->
</div>