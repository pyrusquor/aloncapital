<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sbu_model extends MY_Model
{
    public $table = 'sbu'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name',
        'company',
        'code',
        'is_active',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'company' => array(
            'field' => 'company',
            'label' => 'Company',
            'rules' => 'trim|required',
        ),
        'code' => array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim|required',
        ),
        'is_active' => array(
            'field' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim|required',
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['company'] = array('foreign_model' => 'company/Company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
                'company' => $faker->numberBetween(1, 2),
                'code' => $faker->word,
                'is_active' => $faker->numberBetween(0, 1),
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}
