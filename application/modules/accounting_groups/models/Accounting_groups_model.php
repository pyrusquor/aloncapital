<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_groups_model extends MY_Model
{
    public $table = 'accounting_groups'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'parent_id',
        'name',
        'slug',
        'affects_gross',
        'account_title',
        'company_id',
        'created_at',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [

    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();

    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function get_tabs_children($parent_id = 0)
    {
        $this->db->select("*");
        $this->db->from("accounting_groups");
        $this->db->where('parent_id', $parent_id);
        $query = $this->db->get();
        return $query->result();

    }

    public function tree($company_id = 0)
    {
        $parents = $this->call_children(0,0,$company_id);

        foreach ($parents as &$parent) {
            $children = $this->call_children($parent['id'],0,$company_id);
            $parent['children'] = $children;
        }

        return $parents;
    }

    public function call_children($parent_id = 0, $affects = -1,$company_id = 0)
    {
        if ($parent_id == 0) {
            return $this->get_children(0,0,$company_id);
        } else {
            $children = $this->get_children($parent_id, $affects,$company_id);
            foreach ($children as &$child) {

                $child['subgroups'] = $this->call_children($child['id'], $affects,$company_id);
                $child['ledgers'] = $this->get_ledgers($child['id'],$company_id);

                if ($child['id'] == 7) {
                    # code..
                    // vdebug($child['ledgers']);
                }
            }

            return $children;
        }
    }

    public function get_super_parent_id($group_id)
    {
        $parent = $this->get_parent_id($group_id);

        if ($parent['parent_id'] == 0) {
            return $parent['id'];
        } else {
            return $this->get_super_parent_id($parent['parent_id']);
        }

    }

    public function get_parent_id($group_id)
    {
        $this->db->select('id, parent_id');
        $this->db->where('id', $group_id);

        $this->db->where_in('company_id', [0, 1]);

        $query = $this->db->get('accounting_groups');
        $result = $query->row_array();

        return $result;
    }

    public function get_ledgers($group_id,$company_id = 0)
    {
        $this->select('accounting_ledgers.*, options.op_balance, options.op_balance_dc, options.reconciliation, options.non_operating');

        if ($company_id) {
            $this->db->where_in('accounting_ledgers.company_id', [0, $company_id]);
        } else {
            $this->db->where_in('accounting_ledgers.company_id', [0, 1]);
        }

        $this->db->where('accounting_ledgers.deleted_at', NULL);
        $this->db->where('accounting_ledgers.group_id', $group_id);
        $this->db->join('ledger_options as options', 'options.ledger_id = accounting_ledgers.id', 'left');
        $query = $this->db->get('accounting_ledgers');

        return $query->result_array();
    }

    public function get_ledger($ledger_id,$company_id = 0)
    {
        $this->select('accounting_ledgers.*, options.op_balance, options.op_balance_dc, options.reconciliation, options.non_operating');

        if ($company_id) {
            $this->db->where_in('accounting_ledgers.company_id', [0, $company_id]);
        } else {
            $this->db->where_in('accounting_ledgers.company_id', [0, 1]);
        }

        $this->db->where('accounting_ledgers.id', $ledger_id);
        $this->db->where('accounting_ledgers.deleted_at', NULL);

        $this->db->join('ledger_options as options', 'options.ledger_id = accounting_ledgers.id', 'left');
        $query = $this->db->get('accounting_ledgers', 1);

        return $query->row_array();
    }
    public function has_content($group_id, $type,$company_id = 0)
    {
        $column = '';
        $table = '';

        if ($type == 'ledger') {
            $table = 'accounting_ledgers';
            $column = 'group_id';
        } else {
            $table = 'groups';
            $column = 'parent_id';
        }

        if ($company_id) {
            $this->db->where_in('company_id', [0, $company_id]);
        } else {
            $this->db->where_in('company_id', [0, 1]);
        }

        $this->db->where($column, $group_id);
        $query = $this->db->get($table, 1);

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_children($parent_id = 0, $affects = -1,$company_id = 0)
    {
        $this->db->select('id, parent_id, name, slug, company_id');

        if ($parent_id > 0) {
            $this->db->where('parent_id', $parent_id);

            if ($company_id) {
                $this->db->where_in('company_id', [0, $company_id]);
            } else {
                $this->db->where_in('company_id', [0, 1]);
            }

            if ($affects != -1) {
                $this->db->where('affects_gross', $affects);
            }

        } else {
            $this->db->where('company_id', 0); //4 Main Groups ;  Universal
            $this->db->where('parent_id', 0);
        }

        $this->db->where('deleted_at', NULL);
        $query = $this->db->get('accounting_groups');
        return $query->result_array();
    }

    public function get_group($group_id,$company_id = 0)
    {
        $result = array();
        $this->db->where('id', $group_id);
        $this->db->where('deleted_at', NULL);

        $query = $this->db->get('accounting_groups', 1);

        $result = $query->row_array();
        $result['has_ledgers'] = $this->has_content($group_id, 'ledger',$company_id);
        $result['has_subgroups'] = $this->has_content($group_id, 'group',$company_id);

        return $result;

    }

}
