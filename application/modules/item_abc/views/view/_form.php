<?php
$name = isset($item_abc['name']) && $item_abc['name'] ? $item_abc['name'] : '';
$abc_code = isset($item_abc['abc_code']) && $item_abc['abc_code'] ? $item_abc['abc_code'] : '';
$description = isset($item_abc['description']) && $item_abc['description'] ? $item_abc['description'] : '';
$is_active = isset($item_abc['is_active']) && $item_abc['is_active'] ? $item_abc['is_active'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>ABC Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="abc_code" value="<?php echo set_value('abc_code', $abc_code); ?>" placeholder="ABC Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('abc_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Description <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-align-left"></i></span>
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Status <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">

            <?php echo form_dropdown('is_active', Dropdown::get_static('inventory_status'), set_value('is_active', @$is_active), 'class="form-control"'); ?>
            <?php echo form_error('is_active'); ?>
            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-check-square"></i>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->

    <!-- ==================== end: Add form model fields ==================== -->
</div>