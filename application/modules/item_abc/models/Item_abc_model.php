<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item_abc_model extends MY_Model
{
    public $table = 'item_abc'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = ['name', 'abc_code', 'description', 'is_active', 'created_by', 'updated_by', 'deleted_by'];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'abc_code' => array(
            'field' => 'abc_code',
            'label' => 'ABC Code',
            'rules' => 'trim|required',
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required',
        ),
        'is_active' => array(
            'field' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim|required',
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
                'abc_code' => $faker->word,
                'description' => $faker->paragraph,
                'is_active' => $faker->numberBetween(0, 1),
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}
