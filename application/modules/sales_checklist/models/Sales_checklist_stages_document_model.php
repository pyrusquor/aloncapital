<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Sales_checklist_stages_document_model extends MY_Model {

	public $primary_key	=	'id'; // you MUST mention the primary key
	public $protected		=	['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $fillable		=	[
		'sales_checklist_stage_id',
		'document_id',
		'required_copies',
		'days_to_finish',
		'days_to_expire',
		'is_required',
		'uploaded_document_id',
		'category_id',
		'start_date',
		'owner_id',
		'group_id',
		'dependency_id',
		'is_active',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
		'deleted_by',
		'deleted_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	
	public $table				=	'sales_checklist_stages_documents'; // you MUST mention the table name
	public $rules				=	[];
	public $_fields			=	[];

	function __construct () {

		parent::__construct();

		$this->soft_deletes = TRUE;

		$this->return_as = 'array';

		$this->rules['insert']	=	$this->_fields;
		$this->rules['update']	=	$this->_fields;
	}

	function find_all ( $_where = FALSE, $_columns = FALSE, $_row = FALSE ) {

		$_return	=	FALSE;

		if ( $_where ) {

			if ( is_array($_where) ) {

				foreach ( $_where as $key => $_wang ) {

					$this->db->where( $key, $_wang );
				}
			} else {

				$this->db->where( 'id', $_where );
			}
		}
		// else {

		// 	$this->db->where('deleted_at IS NULL');
		// }

		if ( $_columns ) {

			if ( is_array($_columns) ) {

				foreach ( $_columns as $key => $_col ) {

					$this->db->select($_col);
				}
			} else {

				$this->db->select($_columns);
			}
		} else {

			$this->db->select('*');
		}

		$this->db->where('deleted_at IS NULL');

		// if ( $_limit ) {

		// 	if ( is_numeric($_limit) ) {

		// 		$this->db->limit($_limit);
		// 	}
		// }

		$_query	=	$this->db->get($this->table);
		if ( $_row ) {

			$_return	=	$_query->num_rows() > 0 ? $_query->row() :  FALSE;
		} else {

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}

	function _get_document_checklist ( $_proj_id = FALSE ) {

		$_return	=	FALSE;

		if ( $_proj_id ) {

			$_sql	=	"
						SELECT d.`id`,
							d.`name`,
							d.`description`,
							d.`owner_id`,
							d.`classification_id`,
							pd.`uploaded_document_id`,
							IF( pd.`uploaded_document_id` <> '', 'uploaded', '' ) AS `status`,
							'' AS `timeline`,
							'' AS `due_date`,
							pd.`created_at` AS `date_created`,
							d.`updated_at`
						FROM sales_checklist_stages_documents AS pd
						LEFT JOIN sales_checklist_stages AS p ON p.`id` = pd.`sales_checklist_stage_id`
						LEFT JOIN documents AS d ON d.`id` = pd.`document_id`
						WHERE pd.`deleted_at` IS NULL
							AND p.`deleted_at` IS NULL
							AND d.`deleted_at` IS NULL
							AND p.`id` = ?
					";
			$_query	=	$this->db->query($_sql, [$_proj_id]);
			$_return	=	$_query->num_rows() > 0 ? $_query->result_array() : FALSE;
		}

		return $_return;
	}
}