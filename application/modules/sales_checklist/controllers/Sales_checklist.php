<?php defined('BASEPATH') or exit('No direct script access allowed');

class Sales_checklist extends MY_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->model('sales_checklist/Sales_checklist_model', 'M_checklist');
		$this->load->model('stages/Stage_model', 'M_stages');

		// $this->view_data['_largescreen']	=	TRUE;
	}

	function index()
	{

		$_checklists =	$this->M_checklist->get_all();
		if ($_checklists) {

			$this->view_data['_total']	=	count($_checklists);
		}

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	function stages($id = 0)
	{

		$_checklists =	$this->M_stages->get_all(array('sales_checklist_id' => $id));

		if ($_checklists) {
			$this->view_data['_total']	=	count($_checklists);
		}

		$this->view_data['_proj_id'] =	count($id);

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('stages', $this->view_data);
	}

	function create()
	{

		if ($this->input->post()) {

			$_input	=	$this->input->post();

			$_insert	=	$this->M_checklist->insert($_input); #lqq(); ud($_insert);
			if ($_insert !== FALSE) {

				// $_cid	=	$this->db->insert_id();

				// $this->notify->success('Checklist Template successfully created.', 'sales_checklist/update/'.$_cid);

				$this->notify->success('Sales Checklist Template successfully created.', 'checklist');
			} else {

				$this->notify->error('Oh snap! Please refresh the page and try again.');
			}
		}

		$this->template->build('create', $this->view_data);
	}

	function view($_id = FALSE)
	{

		$this->js_loader->queue(['vendors/custom/jquery-ui/jquery-ui.bundle.js']);
		$this->load->model('document/Document_model', 'M_document');
		$this->load->model('sales_checklist/Document_checklist_model', 'M_document_checklist');

		if ($_id) {

			$this->view_data['_list']	=	$this->M_checklist->get($_id);
			if ($this->view_data['_list']) {

				$_document_checklists	=	$this->M_document_checklist->where('checklist_id', $_id)->get_all(); #up($this->db->last_query()); ud($_document_checklists);
				if ($_document_checklists) {

					$_selected_ids	=	[];
					foreach ($_document_checklists as $key => $_lts) {

						$_selected_ids[$_lts['id']]	=	$_lts['document_id'];
					}

					if ($_selected_ids) {

						$this->view_data['_selected_ids']	=	$_selected_ids;
					}
				}

				unset($_fields);
				$_fields	=	['id', 'name'];

				$_documents	=	$this->M_document->fields($_fields)->order_by('name')->get_all(); //($data, $field, $order_by, $sort_by = 'DESC')
				#up($this->db->last_query()); ud($_documents);

				if ($_documents) {

					// krsort($_documents);

					$this->view_data['_documents']	=	$_documents;
				}

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function update_document_checklist()
	{

		$_response['_status']	=	0;
		$_response['_msg']		=	'Oops! Please refresh the page and try again.';

		$_checklist_id	=	$this->input->post('id');
		$_docx_ids			=	$this->input->post('ids');
		if ($_checklist_id) {

			$this->load->model('sales_checklist/Document_checklist_model', 'M_document_checklist');

			$_selected_ids	=	[];

			if ($_docx_ids) {

				$_inserted	=	0;
				$_removed		=	0;

				$_document_checklists	=	$this->M_document_checklist->where('checklist_id', $_checklist_id)->get_all();
				if ($_document_checklists) {

					foreach ($_document_checklists as $key => $_lts) {

						$_selected_ids[$_lts['id']]	=	$_lts['document_id'];
					}
				}

				if (count($_docx_ids) >= count($_selected_ids)) {

					foreach ($_docx_ids as $key => $_id) {

						if (!in_array($_id, $_selected_ids)) {

							unset($_data);
							$_data['checklist_id']	=	$_checklist_id;
							$_data['document_id']		=	$_id;

							$_insert	=	$this->M_document_checklist->insert($_data);
							if ($_insert) {

								$_inserted++;
							}
						} else {
							continue;
						}
					}
				} else {

					$_must_delete	=	array_diff($_selected_ids, $_docx_ids);
					if ($_must_delete) {

						foreach ($_must_delete as $_key => $_del_id) {

							unset($_where);
							$_where['checklist_id']	=	$_checklist_id;
							$_where['document_id']	=	$_del_id;

							$_deleted	=	$this->M_document_checklist->where($_where)->delete();
							if ($_deleted) {

								$_removed++;
							}
						}
					}
				}

				if (count($_docx_ids) == count($_selected_ids)) {

					$_response['_status']	=	2;
					$_response['_msg']		=	'Oops! Nothings change.';
				} else {

					if ($_inserted) {

						$_response['_status']	=	1;
						$_response['_msg']		=	$_inserted . ' document/s was added.';
					} elseif ($_removed && ($_removed <=  count($_selected_ids))) {

						$_response['_status']	=	1;
						$_response['_msg']		=	$_removed . ' document/s was removed.';
					}
				}
			} else {

				$_deleted	=	$this->M_document_checklist->where('checklist_id', $_checklist_id)->delete();
				if ($_deleted) {

					$_response['_status']	=	1;
					$_response['_msg']		=	'Checklist was updated.';
				} else {

					$_response['_status']	=	2;
					$_response['_msg']		=	'Oops! Nothings change.';
				}
			}
		}

		echo json_encode($_response);

		exit();
	}

	function update($_id = FALSE)
	{

		if ($_id) {

			$_list	=	$this->M_checklist->get($_id);
			if ($_list) {

				$this->view_data['_list']	=	$_list;

				if ($this->input->post()) {

					$_input	=	$this->input->post();

					// $_updated	=	$this->M_checklist->from_form()->update(NULL, $_list['id']);
					$_updated	=	$this->M_checklist->update($_input, $_list['id']);
					if ($_updated !== FALSE) {

						// $this->notify->success('Checklist Template successfully updated.');

						$this->notify->success('Checklist Template successfully updated.', 'checklist');
					} else {

						$this->notify->error('Oh snap! Please refresh the page and try again.');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function delete()
	{

		$_response['_status']	=	0;
		$_response['_msg']		=	'Oops! Please refresh the page and try again.';

		$_id	=	$this->input->post('id');
		if ($_id) {

			$_list	=	$this->M_checklist->get($_id);
			if ($_list) {

				$_deleted	=	$this->M_checklist->delete($_list['id']);
				if ($_deleted !== FALSE) {

					$_response['_status']	=	1;
					$_response['_msg']		=	'Checklist successfully deleted';
				}
			}
		}

		echo json_encode($_response);

		exit();
	}

	function bulkDelete()
	{

		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_checklist->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Record/s successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	function get_checklists()
	{

		$_response	=	[
			'iTotalDisplayRecords'	=>	'',
			'iTotalRecords'					=>	'',
			'sColumns'							=>	'',
			'sEcho'									=>	'',
			'data'									=>	'',
		];

		$_total['_displays']	=	0;
		$_total['_records']		=	0;

		$_columns	=	[
			'id'					=>	TRUE,
			'name'				=>	TRUE,
			'description'	=>	TRUE,
			'number_of_documents'		=>	TRUE,
			'created_at'	=>	TRUE,
			'updated_at'	=>	TRUE,
			'category_id'	=>	TRUE
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

			$_columns	=	[];

			foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

				$_columns[$_def]	=	TRUE;
			}
		}

		// $_checklists	=	$this->M_checklist->as_array()->get_all(); vp($this->db->last_query()); vd($_checklists);
		$_checklists	=	$this->M_checklist->_get_checklist_info(); #vp($this->db->last_query()); vd($_checklists);
		if ($_checklists) {

			foreach ($_checklists as $key => $value) {

				$_checklists[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

				$_checklists[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
			}

			$_datas	=	[];

			foreach ($_checklists as $_ckkey => $_ck) {

				$_datas[]	=	$this->filterArray($_ck, $_columns);
			}

			$_total['_displays']	=	$_total['_records']	=	count($_datas);

			if (isset($_REQUEST['search'])) {

				$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

				$_total['_displays']	=	$_datas ? count($_datas) : 0;
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

				foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

					if ($_clm['search']) {

						$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

						$_total['_displays']	=	$_datas ? count($_datas) : 0;
					}
				}
			}

			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

				$_column	=	$_REQUEST['order'][0]['column'];
				$_dir			=	$_REQUEST['order'][0]['dir'];

				usort($_datas, function ($x, $y) use ($_column, $_dir) {

					$x	=	array_slice($x, $_column, 1);
					$x	=	array_pop($x);

					$y	=	array_slice($y, $_column, 1);
					$y	=	array_pop($y);

					if ($_dir === 'asc') {

						return $x > $y ? TRUE : FALSE;
					} else {

						return $x < $y ? TRUE : FALSE;
					}
				});
			}

			if (isset($_REQUEST['length'])) {

				$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
			}

			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

				$_temp	=	$_datas;
				$_datas	=	[];

				foreach ($_temp as $key => $_tmp) {

					$_datas[]	=	array_values($_tmp);
				}
			}

			$_secho	=	0;
			if (isset($_REQUEST['sEcho'])) {

				$_secho	=	intval($_REQUEST['sEcho']);
			}

			$_response	=	[
				'iTotalDisplayRecords'	=>	$_total['_displays'],
				'iTotalRecords'					=>	$_total['_records'],
				'sColumns'							=>	'',
				'sEcho'									=>	$_secho,
				'data'									=>	$_datas
			];
		}

		// ud($_response);

		echo json_encode($_response);

		exit();
	}
}
