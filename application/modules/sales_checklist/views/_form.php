<?php
	$_description	=	isset($_list['description']) && $_list['description'] ? $_list['description'] : '';
	$_category_id	=	isset($_list['category_id']) && $_list['category_id'] ? $_list['category_id'] : '';
	$_name				=	isset($_list['name']) && $_list['name'] ? $_list['name'] : '';
?>

<div class="kt-section kt-section--first">
	<div class="form-group row">
		<div class="col-sm-6 offset-sm-3">
			<div class="form-group">
				<label class="form-control-label">Sales Checklist Name <code>*</code></label>
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="name" class="form-control" id="name" placeholder="Sales Checklist Name" value="<?php echo set_value('name', @$_name);?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-book"></i></span></span>
				</div>
				<?php echo form_error('name'); ?>
				<span class="form-text text-muted"></span>
			</div>
			<div class="form-group">
				<label class="form-control-label">Category Name</label>
				<div class="kt-input-icon  kt-input-icon--left">
					<?php echo form_dropdown('category_id', Dropdown::get_static('document_checklist_category'), set_value('category_id', @$_category_id), 'class="form-control" id="category_id"'); ?>
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
				</div>
				<?php echo form_error('category_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
			<div class="form-group">
				<label class="form-control-label">Description</label>
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="description" class="form-control" id="description" placeholder="Description" value="<?php echo set_value('description', @$_description);?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-comments"></i></span></span>
				</div>
				<?php echo form_error('description'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
</div>