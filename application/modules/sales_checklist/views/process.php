<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Land Inventory Process</h3>
			<?php if ( isset($_total) && $_total ): ?>

				<span class="kt-subheader__separator kt-subheader__separator--v"></span>
				<span class="kt-subheader__desc" id="_total"><?php echo $_total;?> TOTAL</span>
			<?php endif; ?>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
				<input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
				<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<?php if ( FALSE ): ?>

					<a href="javascript:void(0);" class="btn btn-label-primary btn-elevate btn-icon-sm">
					<!-- <a href="<?php echo site_url('checklist/create_land_inventory_document');?>" class="btn btn-label-primary btn-elevate btn-icon-sm"> -->
						<i class="fa fa-plus-circle"></i> Process
					</a>
					<button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
						<i class="fa fa-upload"></i> Import
					</button>
				<?php endif; ?>
				<button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
					<i class="fa fa-filter"></i> Filter
				</button>
			</div>
		</div>
	</div>
</div>


<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__body">
		<div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
			<div class="row">
				<div class="col-lg-12">
					<form class="kt-form">
						<div class="form-group row">
							<div class="col-sm-3">
								<label class="form-control-label">Document Name</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="land_owner_name" class="form-control form-control-sm _filter" placeholder="Document Name"  id="_column_1" data-column="1">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-book"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3">
								<label class="form-control-label">Owner</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php echo form_dropdown('owner_id', Dropdown::get_static('document_owner'), set_value('owner_id', @$_owner_id), 'class="form-control form-control-sm _filter" id="_column_2" data-column="2"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-certificate"></i></span></span>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
			<form class="kt-form">
				<div class="form-group row">
					<div class="col-lg-3">
						<label class="form-control-label">Document Type</label>
						<div class="kt-input-icon  kt-input-icon--left">
							<select class="form-control form-control-sm" name="status">
								<option value=""> -- Update Existing Data -- </option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-lg-3">
						<label class="form-control-label">Upload CSV file:</label>
						<label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
						<input type="file" name="" class="">
					</div>
				</div>
				<div class="form-group form-group-last row">
					<div class="col-lg-3">
						<div class="row">
							<div class="col-lg-6">
								<button type="button" class="btn btn-brand btn-success btn-elevate btn-sm">
									<i class="fa fa-upload"></i> Upload
								</button>
							</div>
							<div class="col-lg-6 kt-align-right">
								<button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#_export_option">
									<i class="fa fa-file-csv"></i>
								</button>
								<button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
									<i class="fa fa-info-circle"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_land_inventory_document_table">
			<thead>
				<tr class="text-center">
					<th>ID</th>
					<th>Name</th>
					<th>Owner</th>
					<th>File</th>
					<th>Status</th>
					<th>Timeline</th>
					<th>Due Date</th>
					<th>Date Created</th>
					<th width="1%">Actions</th>
				</tr>
			</thead>
		</table>
		<!--end: Datatable -->
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_export_option_label">Export Options</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-4 offset-lg-1">
						<div class="kt-checkbox-list">
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Field
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Name
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Description
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Owner
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Classification
								<span></span>
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm">
					<i class="fa fa-file-export"></i> Export
				</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="upload_guide_label">Document</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<h2>UPLOAD GUIDE</h2>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!-- Start Upload Modal-->
<div class="modal fade" id="_upload_modal" tabindex="-1" role="dialog" aria-labelledby="_upload_modal_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_upload_modal_label">Upload</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="_upload_form" method="POST" enctype="multipart/form-data">
					<div class="form-group row text-center">
						<div class="col-lg-12 col-xl-12">
							<div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar">
								<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/img/default/_img.png');?>);"></div>
								<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
									<i class="fa fa-pen"></i>
									<!-- <input type="file" id="process_checklist_file" name="process_image" accept=".png, .jpg, .jpeg"> -->
									<input type="file" class="process_checklist_file" id="process_checklist_file" name="process_checklist_file" accept=".png, .jpg, .jpeg">
								</label>
								<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
									<i class="fa fa-times"></i>
								</span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_upload_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Upload Modal-->

<!-- Start Upload Modal-->
<div class="modal fade" id="_view_file_modal" tabindex="-1" role="dialog" aria-labelledby="_view_file_modal_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_view_file_modal_label">View</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="_display_process_document_image"></div>
				<!-- <div class="form-group row text-center">
					<div class="col-lg-12 col-xl-12">
						<div class="kt-avatar kt-avatar--outline kt-avatar--circle-">
							<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/img/default/_img.png');?>);"></div>
						</div>
					</div>
				</div> -->
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<!-- <button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_upload_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button> -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Upload Modal-->