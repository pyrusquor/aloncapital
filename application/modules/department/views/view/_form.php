<?php
$name = isset($department['name']) && $department['name'] ? $department['name'] : '';
$description = isset($department['description']) && $department['description'] ? $department['description'] : '';
$signatory_level_1 = isset($department['signatory_level_1']) && $department['signatory_level_1'] ? $department['signatory_level_1'] : '';
$signatory_level_2 = isset($department['signatory_level_2']) && $department['signatory_level_2'] ? $department['signatory_level_2'] : '';
$signatory_level_3 = isset($department['signatory_level_3']) && $department['signatory_level_3'] ? $department['signatory_level_3'] : '';

$signatory_level_1_name = isset($department['signatory_1']) && $department['signatory_1'] ? $department['signatory_1']['first_name'] . ' ' . $department['signatory_1']['last_name'] : '';

$signatory_level_2_name = isset($department['signatory_2']) && $department['signatory_2'] ? $department['signatory_2']['first_name'] . ' ' . $department['signatory_2']['last_name'] : '';

$signatory_level_3_name = isset($department['signatory_3']) && $department['signatory_3'] ? $department['signatory_3']['first_name'] . ' ' . $department['signatory_3']['last_name'] : '';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Description <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Signatory Level 1 <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="staff"  data-type="person" id="signatory_level_1" name="signatory_level_1" required>
                <option value="">Select Staff</option>
                <?php if ($signatory_level_1_name): ?>
                    <option value="<?php echo $signatory_level_1; ?>" selected><?php echo $signatory_level_1_name; ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Signatory Level 2 <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="staff" data-type="person"  id="signatory_level_2" name="signatory_level_2" required>
                <option value="">Select Staff</option>
                <?php if ($signatory_level_2_name): ?>
                    <option value="<?php echo $signatory_level_2; ?>" selected><?php echo $signatory_level_2_name; ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Signatory Level 3 <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="staff" data-type="person"  id="signatory_level_3" name="signatory_level_3" required>
                <option value="">Select Staff</option>
                <?php if ($signatory_level_3_name): ?>
                    <option value="<?php echo $signatory_level_3; ?>" selected><?php echo $signatory_level_3_name; ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>