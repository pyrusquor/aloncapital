<?php
$id = isset($department['id']) && $department['id'] ? $department['id'] : '';
$name = isset($department['name']) && $department['name'] ? $department['name'] : '';
$description = isset($department['description']) && $department['description'] ? $department['description'] : '';
$signatory_level_1 = isset($department['signatory_level_1']) && $department['signatory_level_1'] ? $department['signatory_level_1'] : '';
$signatory_level_2 = isset($department['signatory_level_2']) && $department['signatory_level_2'] ? $department['signatory_level_2'] : '';
$signatory_level_3 = isset($department['signatory_level_3']) && $department['signatory_level_3'] ? $department['signatory_level_3'] : '';

$signatory_level_1_name = isset($department['signatory_1']) && $department['signatory_1'] ? $department['signatory_1']['first_name'] . ' ' . $department['signatory_1']['last_name'] : '';

$signatory_level_2_name = isset($department['signatory_2']) && $department['signatory_2'] ? $department['signatory_2']['first_name'] . ' ' . $department['signatory_2']['last_name'] : '';

$signatory_level_3_name = isset($department['signatory_3']) && $department['signatory_3'] ? $department['signatory_3']['first_name'] . ' ' . $department['signatory_3']['last_name'] : '';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Department</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('department/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('department'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Description
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $description; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Signatory Level 1
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $signatory_level_1_name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                    Signatory Level 2
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $signatory_level_2_name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                    Signatory Level 3
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $signatory_level_3_name; ?></span>
                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
