<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Department_model extends MY_Model
{
    public $table = 'departments'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name',
        'description',
        'signatory_level_1',
        'signatory_level_2',
        'signatory_level_3',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required',
        ),
        'signatory_level_1' => array(
            'field' => 'signatory_level_1',
            'label' => 'Signatory Level 1',
            'rules' => 'trim|required',
        ),
        'signatory_level_2' => array(
            'field' => 'signatory_level_2',
            'label' => 'Signatory Level 2',
            'rules' => 'trim|required',
        ),
        'signatory_level_3' => array(
            'field' => 'signatory_level_3',
            'label' => 'Signatory Level 3',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['signatory_1'] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'signatory_level_1');
        $this->has_one['signatory_2'] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'signatory_level_2');
        $this->has_one['signatory_3'] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'signatory_level_3');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    // public function insert_dummy()
    // {
    //     require APPPATH.'/third_party/faker/autoload.php';
    //     $faker = Faker\Factory::create();

    //     $data = [];

    //     for($x = 0; $x < 10; $x++)
    //     {
    //         array_push($data,array(
    //             'name'=> $faker->word,
    //         ));
    //     }
    //     $this->db->insert_batch($this->table, $data);

    // }
}
