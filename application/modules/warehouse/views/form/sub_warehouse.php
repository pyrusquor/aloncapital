<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Parent Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control" id="parent_id" name="parent_id" v-model="info.form.parent_id">
                    <option>Select Parent Warehouse</option>
                    <?php foreach($warehouses as $warehouse):?>
                        <option value="<?php echo $warehouse['id'];?>" <?php if($obj['parent_id'] == $warehouse['id']):?>selected<?php endif;?>>
                            <?php echo $warehouse['name'];?>
                        </option>
                    <?php endforeach;?>
                </select>
                <span class="kt-input-icon__icon">

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">

                <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                       v-model="info.form.name">
                <span class="kt-input-icon__icon"></span>

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Warehouse Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">

                <input type="text" class="form-control" id="warehouse_code" name="warehouse_code"
                       placeholder="Warehouse Code" v-model="info.form.warehouse_code">
                <span class="kt-input-icon__icon"></span>

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Telephone Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">


                <input type="text" class="form-control" id="telephone_number"
                       name="telephone_number" placeholder="Telephone Number" v-model="info.form.telephone_number">
                <span class="kt-input-icon__icon"></span>


            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Fax Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="fax_number" name="fax_number"
                       placeholder="Fax Number" v-model="info.form.fax_number">
                <span class="kt-input-icon__icon"></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Mobile Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="mobile_number" name="mobile_number"
                       placeholder="Mobile Number" v-model="info.form.mobile_number">
                <span class="kt-input-icon__icon"></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Address <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">

                <input type="text" class="form-control" id="address" name="address" placeholder="Address"
                       v-model="info.form.address">
                <span class="kt-input-icon__icon"></span>

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Contact Person <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">

                <input type="text" class="form-control" id="contact_person" name="contact_person"
                       placeholder="Contact Person" v-model="info.form.contact_person">
                <span class="kt-input-icon__icon"></span>

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Email <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                       v-model="info.form.email">
                <span class="kt-input-icon__icon"></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <?php if($id):?>
        <div class="col-sm-12 col-md-6">
            <div class="form-group my-3">
                <label>Status <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon">
                    <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), null, 'class="form-control" v-model="info.form.is_active"'); ?>
                </div>
                <span class="form-text text-muted"></span>
            </div>
        </div>
    <?php endif;?>

</div>