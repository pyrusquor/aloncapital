<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Parent Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control" id="parent_id" name="parent_id" v-model="info.form.parent_id">
                    <option>Select Parent Warehouse</option>
                    <?php foreach($warehouses as $warehouse):?>
                        <option value="<?php echo $warehouse['id'];?>" <?php if($obj['parent_id'] == $warehouse['id']):?>selected<?php endif;?>>
                            <?php echo $warehouse['name'];?>
                        </option>
                    <?php endforeach;?>
                </select>
                <span class="kt-input-icon__icon"></span>

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">

                <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                       v-model="info.form.name">
                <span class="kt-input-icon__icon"></span>

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Warehouse Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">

                <input type="text" class="form-control" id="warehouse_code" name="warehouse_code"
                       placeholder="Warehouse Code" v-model="info.form.warehouse_code">
                <span class="kt-input-icon__icon"></span>

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <?php if($id):?>
        <div class="col-sm-12 col-md-6">
            <div class="form-group my-3">
                <label>Status <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon">
                    <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), null, 'class="form-control" v-model="info.form.is_active"'); ?>
                </div>
                <span class="form-text text-muted"></span>
            </div>
        </div>
    <?php endif;?>

</div>