<?php
if (!$obj) {
    $obj = array(
        "id" => null,
        "address" => null,
        "contact_person" => null,
        "email" => null,
        "fax_number" => null,
        "is_active" => null,
        "mobile_number" => null,
        "name" => null,
        "sbu_id" => null,
        "telephone_number" => null,
        "warehouse_code" => null,
        "warehouse_type" => null,
        "parent_id" => null,
    );
}
?>
<script type="text/javascript">
    window.object_request_id = "<?php echo $id; ?>";
    window.object_request_fillables = {};
    window.current_user_id = "<?= current_user_id(); ?>";
    window.object_status = "<?php echo $form_data['status']; ?>";
    <?php foreach ($fillables as $fillable) : ?>
        window.object_request_fillables["<?php echo $fillable; ?>"] = null;
    <?php endforeach; ?>
    window.warehouse_type = "<?php echo $warehouse_type; ?>";
</script>
<?php if ($warehouse_type == 1) : ?>
    <div id="warehouse_app">
    <?php endif; ?>
    <?php if ($warehouse_type == 2) : ?>
        <div id="sub_warehouse_app">
        <?php endif; ?>
        <?php if ($warehouse_type == 3) : ?>
            <div id="virtual_warehouse_app">
            <?php endif; ?>
            <!--begin::Form-->
            <form method="POST" class="kt-form kt-form--label-right" id="form_warehouse" enctype="multipart/form-data" action="<?php form_open('warehouse/form/' . @$obj['id']); ?>">
                <!-- CONTENT HEADER -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title"><?= $method ?></h3>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">
                                <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="form_warehouse" data-ktwizard-type="action-submit" @click.prevent="submitRequest()">
                                    <i class="fa fa-plus-circle"></i> Submit
                                </button>
                                <a href="<?php echo site_url('warehouse'); ?>" class="btn btn-label-instagram btn-elevate btn-sm">
                                    <i class="fa fa-reply"></i> Cancel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- CONTENT -->
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class=" kt-portlet__body">
                                <?php if ($warehouse_type == 1) : ?>
                                    <?php $this->load->view('form/warehouse'); ?>
                                <?php endif; ?>
                                <?php if ($warehouse_type == 2) : ?>
                                    <?php $this->load->view('form/sub_warehouse'); ?>
                                <?php endif; ?>
                                <?php if ($warehouse_type == 3) : ?>
                                    <?php $this->load->view('form/virtual_warehouse'); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
                <!-- begin:: Footer -->
            </form>
            <!--end::Form-->
            </div>