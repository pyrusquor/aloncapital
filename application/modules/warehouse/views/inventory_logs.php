<script type="text/javascript">
    window.current_user_id = "<?php echo current_user_id();?>";
    window.warehouse_id = "<?php echo $warehouse_id;?>";
</script>
<div id="warehouse_content">


    <!-- CONTENT HEADER -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Warehouse</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc" id="total"></span>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                    <input type="text" class="form-control" placeholder="Search Warehouse..."
                           id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm"
                            data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true"
                            aria-controls="_batch_upload">
                        <i class="fa fa-upload"></i> Import
                    </button>
                    <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal"
                            data-target="#_export_option">
                        <i class="fa fa-download"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="module__cta">
        <div class="kt-container  kt-container--fluid ">

            <div class="module__filter">
                <button class="btn btn-secondary btn-elevate btn-sm" id="resetFilters">
                    <i class="fa fa-refresh"></i> Reset
                </button>
                <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                    <i class="fa fa-filter"></i> Filter
                </button>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">

<table class="table table-striped- table-bordered table-hover" id="inventory_logs_table">
    <thead>
    <tr>
        <!-- ==================== begin: Add header fields ==================== -->
        <th>Created</th>
        <th>By</th>
        <th>Transaction</th>
        <th>Item</th>
        <th>Unit</th>
        <th>In</th>
        <th>Out</th>
        <th>Current</th>
        <th>Source</th>
        <th>Destination</th>
        <!-- ==================== end: Add header fields ==================== -->
    </tr>
    </thead>
</table>
<!--end: Datatable -->