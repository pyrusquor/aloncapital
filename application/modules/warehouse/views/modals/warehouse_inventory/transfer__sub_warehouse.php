<div class="modal fade" id="modalTransferToSubWarehouse" tabindex="-2" role="dialog" aria-labelledby="modalTransferToSubWarehouseLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTransferToSubWarehouseLabel">Transfer to Sub Warehouse</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Warehouse:</label>
                            <select class="form-control" v-model="data_store.sub_warehouse_transfer.form.warehouse_id" @change="fetchDependents('sub_warehouses', 'warehouse_id', 'sub_warehouse_transfer')" disabled>
                                <option v-for="warehouse in sources.warehouses" :value="warehouse.id">
                                    {{ warehouse.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Sub Warehouse:</label>
                            <select class="form-control" v-model="data_store.sub_warehouse_transfer.form.destination_id">
                                <option v-for="warehouse in sources.sub_warehouses" :value="warehouse.id">
                                    {{ warehouse.name }}
                                </option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Quantity:</label>
                            <input class="form-control" v-model="data_store.sub_warehouse_transfer.form.quantity" type="number" min="0" step="0.01">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" @click.prevent="transfer('sub_warehouse_transfer')">Transfer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>