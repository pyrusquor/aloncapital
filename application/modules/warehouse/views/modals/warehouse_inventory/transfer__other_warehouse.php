<div class="modal fade" id="modalTransferToOtherWarehouse" tabindex="-2" role="dialog" aria-labelledby="modalTransferToOtherWarehouseLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTransferToOtherWarehouseLabel">Transfer to Other Warehouse</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Warehouse:</label>
                            <select class="form-control" v-model="data_store.warehouse_transfer.form.destination_id">
                                <option v-for="warehouse in sources.warehouses" :value="warehouse.id">
                                    {{ warehouse.name }}
                                </option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Quantity:</label>
                            <input class="form-control" v-model="data_store.warehouse_transfer.form.quantity" type="number" min="0" step="0.01">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" @click.prevent="transfer('warehouse_transfer')">Transfer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>