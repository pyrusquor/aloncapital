<div class="modal fade modal-wide" id="modalTransfer" tabindex="-2" role="dialog" aria-labelledby="modalTransferLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-wide modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTransferLabel">Transfer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Warehouse Type:</label>
                            <select class="form-control" v-model="transfer_state.warehouse_type" @change="warehouseSelectionDisplay()">
                                <option value="1">Warehouse</option>
                                <option value="2">Sub Warehouse</option>
                                <option value="3">Virtual Warehouse (Reservations)</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Warehouse:</label>
                            <select class="form-control" v-model="data_store.warehouse_transfer.form.warehouse_id" @change="filterWarehouses()">
                                <option v-for="warehouse in sources.parent_warehouses" :value="warehouse.id" v-if="warehouse.warehouse_type = 1">
                                    {{ warehouse.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4" v-if="transfer_state.warehouse_type != 1">
                            <label class="form-control-label">Child Warehouse</label>
                            <select class="form-control" v-if="sources.child_warehouses.length > 0" v-model="data_store.warehouse_transfer.form.child_warehouse_id">
                                <option v-for="warehouse in sources.child_warehouses" :value="warehouse.id">
                                    {{ warehouse.name }}
                                </option>
                            </select>
                        </div>


                    </div>
                    <div class="table-responsive my-2">
                        <div v-if="transfer_state.form.length <= 0">
                            <div class="spinner-border text-success" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <table class="table table-condensed table-striped" v-else>
                            <thead>
                            <tr>
                                <th>Receiving</th>
                                <th>Unit Price</th>
                                <th>Unit</th>
                                <th>Available Qty</th>
                                <th>Transfer Qty</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(mi, idx) in transfer_state.material_items">
                                <td>{{ mi.material_receiving.reference }}</td>
                                <td>{{ mi.unit_cost }}</td>
                                <td>{{ mi.unit_of_measurement.name }}</td>
                                <td>{{ mi.stock_data.remaining }}</td>
                                <td>
                                    <input type="number" step="0.01" min="0" :max="mi.stock_data.remaining" v-model="transfer_state.form[idx].quantity" class="form-control">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" @click.prevent="transfer()">Transfer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>