<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Warehouse:</label>


                            <select class="form-control suggests_modal" style="width:100%;"
                                    data-module="warehouses" id="warehouse_id"
                                    name="warehouse_id" disabled>
                                <option value="<?php echo $data['id'];?>" selected><?php echo $data['name'];?></option>
                            </select>


                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Item:</label>


                            <select class="form-control suggests_modal" style="width:100%;"
                                    data-module="item" id="item_id"
                                    name="item_id">
                                <option value="">Select Item</option>
                            </select>


                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Unit of Measurement:</label>


                            <select class="form-control suggests_modal" style="width:100%;"
                                    data-module="inventory_settings_unit_of_measurements"
                                    id="unit_of_measurement_id"
                                    name="unit_of_measurement_id">
                                <option value="">Select Unit of Measurement</option>
                            </select>


                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Actual Quantity:</label>


                            <input type="number" min="0" step="0.01" class="form-control"
                                   id="actual_quantity" name="actual_quantity"
                                   placeholder="Actual Quantity">


                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Available Quantity:</label>


                            <input type="number" min="0" step="0.01" class="form-control"
                                   id="available_quantity" name="available_quantity"
                                   placeholder="Available Quantity">


                        </div>

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Unit Cost:</label>


                            <input type="number" min="0" step="0.01" class="form-control" id="unit_cost"
                                   name="unit_cost" placeholder="Unit Cost">


                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" @click.prevent="loadWarehouseItems(null)">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>