<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Warehouse extends MY_Controller
{

    private $__errors = array(
        'create_master' => null,
        'create_slave' => null,
        'update_master' => null,
        'update_slave' => null,
    );

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Warehouse_model', 'M_Warehouse');

        $this->load->helper('form');

        $this->_table_fillables = $this->M_Warehouse->fillable;
        $this->_form_fillables = $this->M_Warehouse->form_fillables;
        $this->_table_columns = $this->M_Warehouse->__get_columns();
    }

    #region private methods

    public function search()
    {
        $result = $this->__search($_GET);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function __search($params)
    {
        $id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
        $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

        if ($status_list) {
            $status_list_arr = explode(",", $status_list);
            $this->M_Warehouse->where('status', $status_list_arr);
        }
        if (!$id) {

            $address = isset($params['address']) && !empty($params['address']) ? $params['address'] : null;
            if ($address) {
                $this->M_Warehouse->where('address', $address);
            }

            $contact_person = isset($params['contact_person']) && !empty($params['contact_person']) ? $params['contact_person'] : null;
            if ($contact_person) {
                $this->M_Warehouse->where('contact_person', $contact_person);
            }

            $created_at = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;
            if ($created_at) {
                $this->M_Warehouse->where('created_at', $created_at);
            }

            $created_by = isset($params['created_by']) && !empty($params['created_by']) ? $params['created_by'] : null;
            if ($created_by) {
                $this->M_Warehouse->where('created_by', $created_by);
            }

            $deleted_at = isset($params['deleted_at']) && !empty($params['deleted_at']) ? $params['deleted_at'] : null;
            if ($deleted_at) {
                $this->M_Warehouse->where('deleted_at', $deleted_at);
            }

            $deleted_by = isset($params['deleted_by']) && !empty($params['deleted_by']) ? $params['deleted_by'] : null;
            if ($deleted_by) {
                $this->M_Warehouse->where('deleted_by', $deleted_by);
            }

            $email = isset($params['email']) && !empty($params['email']) ? $params['email'] : null;
            if ($email) {
                $this->M_Warehouse->where('email', $email);
            }

            $fax_number = isset($params['fax_number']) && !empty($params['fax_number']) ? $params['fax_number'] : null;
            if ($fax_number) {
                $this->M_Warehouse->where('fax_number', $fax_number);
            }

            $id = isset($params['id']) && !empty($params['id']) ? $params['id'] : null;
            if ($id) {
                $this->M_Warehouse->where('id', $id);
            }

            $is_active = isset($params['is_active']) && !empty($params['is_active']) ? $params['is_active'] : null;
            if ($is_active) {
                $this->M_Warehouse->where('is_active', $is_active);
            }

            $mobile_number = isset($params['mobile_number']) && !empty($params['mobile_number']) ? $params['mobile_number'] : null;
            if ($mobile_number) {
                $this->M_Warehouse->where('mobile_number', $mobile_number);
            }

            $name = isset($params['name']) && !empty($params['name']) ? $params['name'] : null;
            if ($name) {
                $this->M_Warehouse->where('name', $name);
            }

            $sbu_id = isset($params['sbu_id']) && !empty($params['sbu_id']) ? $params['sbu_id'] : null;
            if ($sbu_id) {
                $this->M_Warehouse->where('sbu_id', $sbu_id);
            }

            $telephone_number = isset($params['telephone_number']) && !empty($params['telephone_number']) ? $params['telephone_number'] : null;
            if ($telephone_number) {
                $this->M_Warehouse->where('telephone_number', $telephone_number);
            }

            $updated_at = isset($params['updated_at']) && !empty($params['updated_at']) ? $params['updated_at'] : null;
            if ($updated_at) {
                $this->M_Warehouse->where('updated_at', $updated_at);
            }

            $updated_by = isset($params['updated_by']) && !empty($params['updated_by']) ? $params['updated_by'] : null;
            if ($updated_by) {
                $this->M_Warehouse->where('updated_by', $updated_by);
            }

            $warehouse_code = isset($params['warehouse_code']) && !empty($params['warehouse_code']) ? $params['warehouse_code'] : null;
            if ($warehouse_code) {
                $this->M_Warehouse->where('warehouse_code', $warehouse_code);
            }

            $warehouse_type = isset($params['warehouse_type']) && !empty($params['warehouse_type']) ? $params['warehouse_type'] : null;
            if ($warehouse_type) {
                $this->M_Warehouse->where('warehouse_type', $warehouse_type);
            }

            $parent_id = isset($params['parent_id']) && !empty($params['parent_id']) ? $params['parent_id'] : null;
            if ($parent_id) {
                $this->M_Warehouse->where('parent_id', $parent_id);
            }
        } else {
            $this->M_Warehouse->where('id', $id);
        }

        $this->M_Warehouse->order_by('name', 'ASC');

        if ($with_relations === 'yes') {
            $result = $this->M_Warehouse
                ->with_sbu()
                ->get_all();
        } else {
            $result = $this->M_Warehouse->get_all();
        }

        return $result;
    }

    public function showWarehouses()
    {
        $this->load->helper('warehouse');
        header('Content-Type: application/json');
        $output = ['data' => ''];

        if (property_exists($this->M_Warehouse, 'form_fillables')) {
            $fillables = $this->M_Warehouse->form_fillables;
        } else {
            $fillables = $this->M_Warehouse->fillable;
        }

        $columnsDefault = [];
        foreach ($fillables as $field) {
            $columnsDefault[$field] = true;
        }
        $columnsDefault['sbu'] = true;
        $columnsDefault['type_label'] = true;
        $columnsDefault['parent_name'] = true;
        $columnsDefault['parent_id'] = true;
        $columnsDefault['created_by'] = true;
        $columnsDefault['updated_by'] = true;
        // optionally add relations

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }
        // get all raw data
        $objects = $this->__search($_GET);
        $data = [];

        // vdebug($objects);

        if ($objects) {

            foreach ($objects as $key => $value) {

                $objects[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $objects[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($objects as $d) {
                $__data = $this->filterArray($d, $columnsDefault);
                $__data['type_label'] = warehouse_type_lookup($__data['warehouse_type']);
                if ($__data['parent_id']) {
                    $__data['parent_name'] = $this->M_Warehouse->get($__data['parent_id'])['name'];
                } else {
                    $__data['parent_name'] = "";
                }
                array_push($data, $__data);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search'], 'name');
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function process_create()
    {
        header('Content-Type: application/json');
        if ($this->input->post()) {

            $__request = $this->input->post();
            $request = [];

            if (property_exists($this->M_Warehouse, 'form_fillables')) {
                $fillables = $this->M_Warehouse->form_fillables;
            } else {
                $fillables = $this->M_Warehouse->fillable;
            }

            foreach ($fillables as $field) {
                if (array_key_exists($field, $__request)) {
                    $request[$field] = $__request[$field];
                }
            }

            $this->db->trans_start();
            $this->db->trans_strict(false);

            $additional = array(
                'created_at' => NOW,
                'created_by' => $this->user->id
            );

            $request_object = $this->process_create_master($request, $additional);
            if ($request_object) {
                $references = array(
                    'warehouse_id' => $request_object,
                );
            }

            $this->db->trans_complete(); # Completing request
            if ($this->db->trans_status() === false || !$request_object) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
                $response['message'] = $this->__errors['create_master'] + $this->__errors['create_slave'];
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();

                $response['status'] = 1;
                $response['message'] = 'Request Successfully saved!';
            }

            echo json_encode($response);
            exit();
        }
    }

    private function process_create_master($data, $additional)
    {
        $form_data = $data + $additional;
        $this->form_validation->set_data($form_data);
        $this->form_validation->set_rules($this->M_Warehouse->fields);

        if ($this->form_validation->run() === true) {
            if ($data['warehouse_type'] != 1) {
                $parent = $this->M_Warehouse->get($data['parent_id']);
                $data['sbu_id'] = $parent['sbu_id'];
            }

            $request = $this->db->insert('warehouses', $data + $additional);

            $insert_id = $this->db->insert_id();

            if (!!$_FILES['file']['name']) {

                $this->uploadImage($insert_id);
            }

            if ($request) {
                return $insert_id;
            } else {
                return false;
            }
        } else {
            $this->_form_errors['create_master'] = validation_errors();
            return false;
        }
    }

    private function process_create_slave($data, $references, $additional)
    {
        $this->load->model('warehouse_item/Warehouse_item_model', 'M_item');
        if (property_exists($this->M_item, 'form_fillables')) {
            $fillables = $this->M_item->form_fillables;
        } else {
            $fillables = $this->M_item->fillable;
        }

        $item = [];
        foreach ($fillables as $field) {
            if (key_exists($field, $data)) {
                $item[$field] = $data[$field];
            } else {
                if (key_exists($field, $references)) {
                    $item[$field] = $references[$field];
                }
            }
        }

        return $this->M_item->insert($item);
    }

    public function process_update($id)
    {
        header('Content-Type: application/json');
        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        if ($this->input->post()) {
            $__request = $this->input->post();
            $request = [];

            if (property_exists($this->M_Warehouse, 'form_fillables')) {
                $fillables = $this->M_Warehouse->form_fillables;
            } else {
                $fillables = $this->M_Warehouse->fillable;
            }

            foreach ($fillables as $key) {
                if (array_key_exists($key, $__request)) {
                    $request[$key] = $__request[$key];
                }
            }

            $this->db->trans_start();
            $this->db->trans_strict(false);
            $request_object = $this->process_update_master($id, $request, $additional);
            if ($request_object) {
                $references = [
                    'warehouse_id' => $id,
                ];
            }

            if (!!$_FILES['file']['name']) {

                $this->uploadImage($id);
            }

            $this->db->trans_complete(); # Completing request
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
                $response['message'] = 'Error!';
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                // $this->process_purge_deleted($deleted_items, $additional);

                $response['status'] = 1;
                $response['message'] = 'Successfully saved!';
            }

            echo json_encode($response);
            exit();
        }
    }
    # endregion


    // API

    private function process_update_master($id, $data, $additional)
    {
        //            $old_object = $this->M_Warehouse->get($id);
        if ($data['warehouse_type'] != 1) {
            $parent = $this->M_Warehouse->get($data['parent_id']);
            $data['sbu_id'] = $parent['sbu_id'];
        }
        return $this->M_Warehouse->update($data + $additional, $id);
    }

    function process_update_slave($data, $additional, $references)
    {
        $id = isset($data['id']) ? $data['id'] : null;
        $this->load->model('warehouse_item/Warehouse_item_model', 'M_item');

        $temp_data = $data + $additional;
        $update_data = [];
        if (property_exists($this->M_item, 'form_fillables')) {
            $fillables = $this->M_item->form_fillables;
        } else {
            $fillables = $this->M_item->fillable;
        }
        foreach ($fillables as $key) {
            if (array_key_exists($key, $temp_data)) {
                $update_data[$key] = $temp_data[$key];
            }
        }

        if ($id) {
            // return $this->db->update('material_receiving_items');
            return $this->M_item->update($update_data, $id);
        } else {
            $additional = [
                'created_by' => $additional['updated_by'],
                'created_at' => $additional['updated_at']
            ];

            return $this->process_create_slave($data, $references, $additional);
        }
    }

    private function process_purge_deleted($items, $additional)
    {
        $this->load->model('warehouse_item/Warehouse_item_model', 'M_item');
        if ($items) {
            foreach ($items as $item) {
                if ($item) {
                    $this->M_item->delete($item);
                }
            }
        }
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->_table_columns;
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Warehouse->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue([
            'vendors/custom/datatables/datatables.bundle.js',
            'js/vue2.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);

        $this->template->build('index', $this->view_data);
    }

    // http

    public function form($id = false)
    {
        $this->load->helper('warehouse');
        $type = isset($_GET['type']) ? (int)$_GET['type'] : null;
        if (property_exists($this->M_Warehouse, 'form_fillables')) {
            $fillables = $this->M_Warehouse->form_fillables;
        } else {
            $fillables = $this->M_Warehouse->fillable;
        }
        if ($id) {

            $obj = $this->__get_obj($id);

            if (!$type) {
                $type = $obj['warehouse_type'];
            }

            $method = "Update " . warehouse_type_lookup($type);

            if (in_array('reference', $fillables)) {
                $reference = $obj['reference'];
            } else {
                $reference = null;
            }

            $form_data = fill_form_data($fillables, $obj);

            $this->view_data['sbu'] = $obj['sbu'];
        } else {
            if (!$type) {
                $type = 1;
            }
            $method = "Create " . warehouse_type_lookup($type);
            $obj = null;
            $reference = null;
            $form_data = fill_form_data($fillables);
            $this->view_data['sbu'] = null;
        }
        $this->view_data['warehouse_type'] = $type;
        $this->view_data['warehouses'] = $this->M_Warehouse->where('warehouse_type', 1)->order_by('name')->get_all();
        $this->view_data['fillables'] = $fillables;
        $this->view_data['current_user'] = $this->user->id;
        $this->view_data['method'] = $method;
        $this->view_data['obj'] = $obj;
        $this->view_data['id'] = $id;
        $this->view_data['form_data'] = $form_data;
        $this->view_data['reference'] = $reference;

        $this->js_loader->queue([
            'js/vue2.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);

        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Warehouse->get($id);
            if ($list) {

                $deleted = $this->M_Warehouse->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Warehouse Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_Warehouse->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Warehouse Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = FALSE)
    {
        if ($id) {

            $this->view_data['data'] = $this->__get_obj($id);
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue([
                'vendors/custom/datatables/datatables.bundle.js',
                'js/vue2.js',
                'js/axios.min.js',
                'js/utils.js'
            ]);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function inventory_logs($id)
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue([
            'vendors/custom/datatables/datatables.bundle.js',
            'js/vue2.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);
        $this->view_data['warehouse_id'] = $id;
        $this->template->build('inventory_logs', $this->view_data);
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'name' => $upload_data[0],
                        /* ==================== begin: Add model fields ==================== */

                        /* ==================== end: Add model fields ==================== */
                    );

                    $warehouse_id = $upload_data[0];
                    $warehouse = $this->M_Warehouse->get($warehouse_id);

                    if ($warehouse) {
                        $result = $this->M_Warehouse->update($fields, $warehouse_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'name' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Warehouse->insert($fields);
                    } else {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Warehouse->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'warehouse');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'warehouse');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? TRUE : FALSE;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Warehouse CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Warehouse->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Warehouse');
            $_objSheet->setCellValue('A1', 'WAREHOUSE');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $warehouses = $this->M_Warehouse->as_array()->get_all();
        if ($warehouses) {

            foreach ($warehouses as $_lkey => $warehouse) {

                $_datas[$warehouse['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_warehouses_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse');
                }
            } else {

                $_filename = 'list_of_warehouses_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($warehouses as $_lkey => $warehouse) {

                                $_datas[$warehouse['id']][$_title] = isset($warehouse[$_column]) && ($warehouse[$_column] !== '') ? Dropdown::get_static('bool', $warehouse[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($warehouses as $_lkey => $warehouse) {

                                if ($_name === 'status') {

                                    $_datas[$warehouse['id']][$_title] = isset($warehouse[$_name]) && $warehouse[$_name] ? Dropdown::get_static('inventory_status', $warehouse[$_name], 'view') : '';
                                } else {

                                    $_datas[$warehouse['id']][$_title] = isset($warehouse[$_name]) && $warehouse[$_name] ? $warehouse[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Warehouse');
                $_objSheet->setCellValue('A1', 'LIST OF WAREHOUSE');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse');
            }
        } else {

            $this->notify->error('No Record Found', 'warehouse');
        }
    }

    private function __get_obj($id, $with_relations = TRUE)
    {
        if ($with_relations) {
            $obj = $this->M_Warehouse
                ->with_sbu()
                ->get($id);
        } else {
            $obj = $this->M_Warehouse->get($id);
        }

        return $obj;
    }

    private function uploadImage($model_id)
    {
        $_location = 'assets/img/warehouses';

        // Create dir if does not exist
        if (!file_exists($_location)) {

            mkdir($_location, 0777, true);
        }

        $model = $this->M_Warehouse->get($model_id);

        if (file_exists($model['file_path'])) {

            unlink($model['file_path']);
        }

        $_config['upload_path'] = $_location;
        $_config['allowed_types'] = '*';
        $_config['overwrite'] = TRUE;
        $_config['max_size'] = '1000000';
        $_config['file_name'] = $model_id;

        $this->load->library('upload', $_config);

        if (!$this->upload->do_upload('file')) {

            $this->notify->error($this->upload->display_errors(), 'leasing/projects');
        } else {

            $info['file_path'] = strstr($this->upload->data('full_path'), 'assets');

            // $this->M_Warehouse->update($info, $model_id);

            $this->db->where('id', $model_id);
            $this->db->update('warehouses', $info);
        }
    }
}
