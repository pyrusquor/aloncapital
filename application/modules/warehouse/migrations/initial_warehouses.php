<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Initial_warehouses extends CI_Migration {
    protected $tbl = "warehouses";
    protected $fields = array(
        
            "contact_person" =>  array(
                
                    "constraint" => "255",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "VARCHAR",
                    
                
            ),
        
            "created_at" =>  array(
                
                    "NOT NULL" => False,
                    
                
                    "type" => "DATETIME",
                    
                
            ),
        
            "created_by" =>  array(
                
                    "NOT NULL" => False,
                    
                
                    "type" => "INT",
                    
                
                    "unsigned" => True,
                    
                
            ),
        
            "deleted_at" =>  array(
                
                    "NOT NULL" => False,
                    
                
                    "type" => "DATETIME",
                    
                
            ),
        
            "deleted_by" =>  array(
                
                    "NOT NULL" => False,
                    
                
                    "type" => "INT",
                    
                
                    "unsigned" => True,
                    
                
            ),
        
            "email" =>  array(
                
                    "constraint" => "512",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "VARCHAR",
                    
                
            ),
        
            "fax_number" =>  array(
                
                    "constraint" => "15",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "VARCHAR",
                    
                
            ),
        
            "id" =>  array(
                
                    "auto_increment" => True,
                    
                
                    "constraint" => "11",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "INT",
                    
                
                    "unsigned" => True,
                    
                
            ),
        
            "is_active" =>  array(
                
                    "default" => True,
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "BOOL",
                    
                
            ),
        
            "mobile_number" =>  array(
                
                    "constraint" => "15",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "VARCHAR",
                    
                
            ),
        
            "name" =>  array(
                
                    "constraint" => "255",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "VARCHAR",
                    
                
            ),
        
            "sbu_id" =>  array(
                
                    "constraint" => "11",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "INT",
                    
                
            ),
        
            "telephone_number" =>  array(
                
                    "constraint" => "15",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "VARCHAR",
                    
                
            ),
        
            "updated_at" =>  array(
                
                    "NOT NULL" => False,
                    
                
                    "type" => "DATETIME",
                    
                
            ),
        
            "updated_by" =>  array(
                
                    "NOT NULL" => False,
                    
                
                    "type" => "INT",
                    
                
                    "unsigned" => True,
                    
                
            ),
        
            "warehouse_code" =>  array(
                
                    "constraint" => "32",
                    
                
                    "NOT NULL" => True,
                    
                
                    "type" => "VARCHAR",
                    
                
            ),
        
    );

    public function up() {     
        if (! $this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
        
    }

    public function down() {
        if (! $this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
