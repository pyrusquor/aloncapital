<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_request extends MY_Controller
{
    private $fields = [
        array(
            'field' => 'request[reference]',
            'label' => 'Reference',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[payable_type_id]',
            'label' => 'Payable Type ID',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[origin_id]',
            'label' => 'Origin',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[payee_type]',
            'label' => 'Payee Type',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[payee_type_id]',
            'label' => 'Payee Type ID',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[date_requested]',
            'label' => 'Date Requested',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[project_id]',
            'label' => 'Project ID',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[property_id]',
            'label' => 'Property ID',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[approving_department_id]',
            'label' => 'Approving Department ID',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[approving_staff_id]',
            'label' => 'Approving Staff ID',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[prepared_by]',
            'label' => 'Prepared By',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[gross_amount]',
            'label' => 'Gross Amount',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[total_due_amount]',
            'label' => 'Total Due Amount',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[tax_amount]',
            'label' => 'Tax Amount',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[tax_percentage]',
            'label' => 'Tax Percentage',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[wht_amount]',
            'label' => 'Withholding Tax Amount',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[wht_percentage]',
            'label' => 'Withholding Tax Percentage',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[payment_voucher_id]',
            'label' => 'Payment Voucher ID',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[is_paid]',
            'label' => 'Is Paid',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[is_complete]',
            'label' => 'Is Complete',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[due_date]',
            'label' => 'Due Date',
            'rules' => 'trim',
        ),
        array(
            'field' => 'request[particulars]',
            'label' => 'Particulars',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $payment_request_models = array(
            'payment_request/Payment_request_model' => 'M_payment_request',
            'payment_request/Payment_request_origin_model' => 'M_payment_request_origins',
            'payment_request/Payment_request_property_model' => 'M_payment_request_properties',
            'payment_request/Payment_request_project_model' => 'M_payment_request_projects'
        );

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model($payment_request_models);
        $this->load->model('payable_type/Payable_type_model', 'M_payable_type');
        $this->load->model('item/Item_model', 'M_item');
        $this->load->model('payable_items/Payable_items_model', 'M_payable_item');
        $this->load->model('accounting_entries/Accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_settings/Accounting_settings_model', 'M_accounting_settings');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');
        $this->load->model('transaction_commission/Transaction_commission_model', 'M_Transaction_commission');
        $this->load->model('transaction/Transaction_model', 'M_Transaction');
        $this->load->model('company/Company_model', 'M_company');
        $this->load->model('land/land_inventory_model', 'M_land_inventory');
        $this->load->model('staff/Staff_model', 'M_staff');

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper
        $this->load->helper('form');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_payment_request->fillable;
        $this->_table_columns = $this->M_payment_request->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'is_active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
    }

    public function template($page = "")
    {

        $this->template->build($page);
    }

    public function index()
    {
        // $_fills = $this->_table_fillables;
        // $_colms = $this->_table_columns;

        // $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        // $this->view_data['_columns'] = $this->__get_columns($_fills);

        // // Get record count
        // // $conditions['returnType'] = 'count';
        // $this->view_data['totalRec'] = $totalRec = $this->M_payment_request->count_rows();

        // // Pagination configuration
        // $config['target'] = '#payment_requestContent';
        // $config['base_url'] = base_url('payment_request/paginationData');
        // $config['total_rows'] = $totalRec;
        // $config['per_page'] = $this->perPage;
        // $config['link_func'] = 'PaymentRequestPagination';

        // // Initialize pagination library
        // $this->ajax_pagination->initialize($config);

        // // Get records
        // $this->view_data['records'] = $this->M_payment_request->with_payable_type()->as_array()->order_by('id', 'DESC')
        //     ->limit($this->perPage, 0)
        //     ->get_all();

        // // Get payable types
        // $this->view_data['payable_type'] = $this->M_payable_type->get_all();

        // $this->template->build('index', $this->view_data);

        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;
        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);
        $db_columns = $this->M_payment_request->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';
                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }
            $column_count = count($column);
            $cceil = ceil(($column_count / 2));
            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_payment_request->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);
        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'payee' => true,
            'payee_type_id' => true,
            'payee_type' => true,
            'payable_type_id' => true,
            'property_id' => true,
            'due_date' => true,
            'date_requested' => true,
            'gross_amount' => true,
            'tax_amount' => true,
            'wht_amount' => true,
            'total_due_amount' => true,
            'is_paid' => true,
            'is_complete' => true,
            'status' => true,
            'particulars' => true,
            'accounting_entry_id' => true,
            'paid_amount' => true,
            'total_due_amount' => true,
            'remaining_amount' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];

        if ($_POST['filter']) {

            if (!is_array($_POST['filter'])) {

                parse_str($_POST['filter'], $filters);
            }
        }

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];
        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_payment_request
                ->with_payable_type('fields:name')
                ->with_project('fields:name')
                ->with_property('fields:name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("reference like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'reference' => [
                    'data' => $filters['reference'] ?? null,
                    'operator' => 'like',
                ],
                'payee_type_id' => [
                    'data' => $filters['payee_type_id'] ?? null,
                    'operator' => '=',
                ],
                'payee_type' => [
                    'data' => $filters['payee_type'] ?? null,
                    'operator' => '=',
                ],
                'is_complete' => [
                    'data' => $filters['is_complete'] ?? null,
                    'operator' => '=',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'status' => [
                    'data' => $filters['status'] ?? null,
                    'operator' => '=',
                ],
                'is_paid' => [
                    'data' => $filters['is_paid'] ?? null,
                    'operator' => '=',
                ],
                'due_date' => [
                    'type' => 'daterange',
                    'data' => $filters['due_date'] ?? null,
                    'column' => 'due_date',
                ],
                'date_requested' => [
                    'type' => 'daterange',
                    'data' => $filters['date_requested'] ?? null,
                    'column' => 'date_requested',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data'] || ($key == 'is_complete' && is_numeric($value['data'])) || ($key == 'status' && is_numeric($value['data'])) || ($key == 'is_paid' && is_numeric($value['data']))) {

                    if ($value['type'] == 'daterange') {
                        $query->where($value['column'], '>=', explode(' - ', $value['data'])[0]);
                        $query->where($value['column'], '<=', explode(' - ', $value['data'])[1]);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $payee = get_person($value['payee_type_id'], $value['payee_type']);
                        $payee_name = get_fname($payee);

                        $id = $value['id'];

                        $items[$key]['due_date'] = view_date($value['due_date']);
                        $items[$key]['date_requested'] = view_date($value['date_requested']);
                        $items[$key]['gross_amount'] = money_php($value['gross_amount']);
                        $items[$key]['total_due_amount'] = money_php($value['total_due_amount']);
                        $items[$key]['wht_amount'] = money_php($value['wht_amount']);
                        $items[$key]['tax_amount'] = money_php($value['tax_amount']) . " - " . money_php($value['wht_amount']);;

                        $items[$key]['payee'] = ucwords($value['payee_type']) . "<br>" . $payee_name;

                        $payable_type_id = isset($value['payable_type_name']) && $value['payable_type_name'] ? $value['payable_type_name'] : 'Manual';
                        $items[$key]['property_id'] = isset($value['property_name']) && $value['property_name'] ? $value['property_name'] : 'N/A';

                        $paid_amount = count_paid_amount($id, 'payment_vouchers', 'payment_request_id', 'paid_amount');

                        $source = get_origin_source($value['payable_type_id'], $value['origin_id']);
                        $items[$key]['payable_type_id'] = $source . $payable_type_id;

                        $payment_status = ($value['is_paid'] ? "Paid" : "Unpaid");
                        $status = ($value['is_complete'] ? "Complete" : "Incomplete");

                        $items[$key]['is_paid'] = $payment_status;
                        $items[$key]['is_complete'] = $status;
                        $items[$key]['status'] = Dropdown::get_static('request_status', $value['status'], 'view');
                        $items[$key]['paid_amount'] = money_php($paid_amount);
                        $remaining_amount = $value['total_due_amount'] - $paid_amount;
                        $items[$key]['remaining_amount'] = money_php($remaining_amount);

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_payment_request->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function showPropertyPaymentRequests()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'payee' => true,
            'payee_type_id' => true,
            'payee_type' => true,
            'payable_type_id' => true,
            'property_id' => true,
            'due_date' => true,
            'date_requested' => true,
            'gross_amount' => true,
            'tax_amount' => true,
            'wht_amount' => true,
            'total_due_amount' => true,
            'is_paid' => true,
            'is_complete' => true,
            'status' => true,
            'particulars' => true,
            'accounting_entry_id' => true,
            'paid_amount' => true,
            'total_due_amount' => true,
            'remaining_amount' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];

        if ($_POST['filter']) {

            if (!is_array($_POST['filter'])) {

                parse_str($_POST['filter'], $filters);
            }
        }

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];
        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $payment_request_properties = $this->M_payment_request_properties->where('property_id', $_POST['filter'])->fields('id')->with_payment_request('fields:id')->get_all();

            $request_ids = [];

            foreach ($payment_request_properties as $value) {

                array_push($request_ids, $value['payment_request']['id']);
            }

            $query = $this->M_payment_request
                ->with_payable_type('fields:name')
                ->with_project('fields:name')
                ->with_property('fields:name')
                ->where('id', !empty($request_ids) ? $request_ids : 0)
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            // if ($searchValue) {

            //     $query->or_where("(id like '%$searchValue%'");
            //     $query->or_where("reference like '%$searchValue%')");
            // }

            // $advanceSearchValues = [
            //     'reference' => [
            //         'data' => $filters['reference'] ?? null,
            //         'operator' => 'like',
            //     ],
            //     'payee_type_id' => [
            //         'data' => $filters['payee_type_id'] ?? null,
            //         'operator' => '=',
            //     ],
            //     'payee_type' => [
            //         'data' => $filters['payee_type'] ?? null,
            //         'operator' => '=',
            //     ],
            //     'is_complete' => [
            //         'data' => $filters['is_complete'] ?? null,
            //         'operator' => '=',
            //     ],
            //     'project_id' => [
            //         'data' => $filters['project_id'] ?? null,
            //         'operator' => '=',
            //     ],
            //     'status' => [
            //         'data' => $filters['status'] ?? null,
            //         'operator' => '=',
            //     ],
            //     'is_paid' => [
            //         'data' => $filters['is_paid'] ?? null,
            //         'operator' => '=',
            //     ],
            //     'due_date' => [
            //         'type' => 'daterange',
            //         'data' => $filters['due_date'] ?? null,
            //         'column' => 'due_date',
            //     ],
            //     'date_requested' => [
            //         'type' => 'daterange',
            //         'data' => $filters['date_requested'] ?? null,
            //         'column' => 'date_requested',
            //     ],
            //     'property_id' => [
            //         'data' => $filters['property_id'] ?? null,
            //         'operator' => '=',
            //     ],
            // ];

            // Advance Search
            // foreach ($advanceSearchValues as $key => $value) {

            //     if ($value['data'] || ($key == 'is_complete' && is_numeric($value['data'])) || ($key == 'status' && is_numeric($value['data'])) || ($key == 'is_paid' && is_numeric($value['data']))) {

            //         if ($value['type'] == 'daterange') {
            //             $query->where($value['column'], '>=', explode(' - ', $value['data'])[0]);
            //             $query->where($value['column'], '<=', explode(' - ', $value['data'])[1]);
            //         } else {

            //             $query->where($key, $value['operator'], $value['data']);
            //         }
            //     }
            // }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $payee = get_person($value['payee_type_id'], $value['payee_type']);
                        $payee_name = get_fname($payee);

                        $id = $value['id'];

                        $items[$key]['due_date'] = view_date($value['due_date']);
                        $items[$key]['date_requested'] = view_date($value['date_requested']);
                        $items[$key]['gross_amount'] = money_php($value['gross_amount']);
                        $items[$key]['total_due_amount'] = money_php($value['total_due_amount']);
                        $items[$key]['wht_amount'] = money_php($value['wht_amount']);
                        $items[$key]['tax_amount'] = money_php($value['tax_amount']) . " - " . money_php($value['wht_amount']);;

                        $items[$key]['payee'] = ucwords($value['payee_type']) . "<br>" . $payee_name;

                        $payable_type_id = isset($value['payable_type_name']) && $value['payable_type_name'] ? $value['payable_type_name'] : 'Manual';
                        $items[$key]['property_id'] = isset($value['property_name']) && $value['property_name'] ? $value['property_name'] : 'N/A';

                        $paid_amount = count_paid_amount($id, 'payment_vouchers', 'payment_request_id', 'paid_amount');

                        $source = get_origin_source($value['payable_type_id'], $value['origin_id']);
                        $items[$key]['payable_type_id'] = $source . $payable_type_id;

                        $payment_status = ($value['is_paid'] ? "Paid" : "Unpaid");
                        $status = ($value['is_complete'] ? "Complete" : "Incomplete");

                        $items[$key]['is_paid'] = $payment_status;
                        $items[$key]['is_complete'] = $status;
                        $items[$key]['status'] = Dropdown::get_static('request_status', $value['status'], 'view');
                        $items[$key]['paid_amount'] = money_php($paid_amount);
                        $remaining_amount = $value['total_due_amount'] - $paid_amount;
                        $items[$key]['remaining_amount'] = money_php($remaining_amount);

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_payment_request->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            $payable_type_id = $this->input->post('payable_type_id');
            $project_id = $this->input->post('project_id');
            $property_id = $this->input->post('property_id');
            $payee_type_id = $this->input->post('payee_type_id');
            $payee_type = $this->input->post('payee_type');
            $is_paid = $this->input->post('is_paid');
            $due_date = $this->input->post('due_date');
            $page = $this->input->post('page');

            if (!empty($due_date)) {
                $date_range = explode('-', $due_date);

                $from_str = strtotime($date_range[0]);
                $from = date('Y-m-d H:i:s', $from_str);

                $to_str = strtotime($date_range[1] . '23:59:59');
                $to = date('Y-m-d H:i:s', $to_str);
            }

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_payment_request->count_rows();
            //            $where = array();

            // Pagination configuration
            $config['target'] = '#payment_requestContent';
            $config['base_url'] = base_url('payment_request/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'PaymentrequestPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                //                $this->db->or_like('payable_type_id', $keyword, 'both');
                //                $this->db->or_like('property_id', $keyword, 'both');
                //                $this->db->or_like('project_id', $keyword, 'both');
                //                $this->db->or_like('is_paid', $keyword, 'both');
                //                $this->db->or_like('due_date', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payable_type_id)) :
                $this->db->group_start();
                $this->db->where('payable_type_id', $payable_type_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payee_type)) :
                $this->db->group_start();
                $this->db->where('payee_type', $payee_type, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payee_type_id)) :
                $this->db->group_start();
                $this->db->where('payee_type_id', $payee_type_id, 'both');
                $this->db->group_end();
            endif;

            if ($is_paid !== 0 && $is_paid !== null) :
                $this->db->group_start();
                $this->db->where('is_paid', $is_paid, 'both');
                $this->db->group_end();
            endif;

            if (!empty($due_date)) :
                $this->db->group_start();
                $this->db->where('due_date BETWEEN "' . $from . '" AND "' . $to . '"');
                $this->db->group_end();
            endif;

            $totalRec = $this->M_payment_request->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                //                $this->db->or_like('payable_type_id', $keyword, 'both');
                //                $this->db->or_like('property_id', $keyword, 'both');
                //                $this->db->or_like('project_id', $keyword, 'both');
                //                $this->db->or_like('is_paid', $keyword, 'both');
                //                $this->db->or_like('due_date', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payable_type_id)) :
                $this->db->group_start();
                $this->db->where('payable_type_id', $payable_type_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payee_type)) :
                $this->db->group_start();
                $this->db->where('payee_type', $payee_type, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payee_type_id)) :
                $this->db->group_start();
                $this->db->where('payee_type_id', $payee_type_id, 'both');
                $this->db->group_end();
            endif;

            if ($is_paid !== 0 && $is_paid !== null) :
                $this->db->group_start();
                $this->db->where('is_paid', $is_paid, 'both');
                $this->db->group_end();
            endif;

            if (!empty($due_date)) :
                $this->db->group_start();
                $this->db->where('due_date BETWEEN "' . $from . '" AND "' . $to . '"');
                $this->db->group_end();
            endif;

            $this->view_data['records'] = $records = $this->M_payment_request
                ->limit($this->perPage, $offset)
                ->order_by('id', 'DESC')
                ->get_all();

            $this->load->view('payment_request/_filter', $this->view_data, false);
        }
    }

    public function view($id = false, $type = '')
    {

        if ($id) {

            $this->view_data['info'] = $this->M_payment_request->with_entry_items()->with_list_items()->with_payable_type()->with_vouchers()->get($id);

            if ($this->view_data['info']) {

                if ($type == "debug") {

                    vdebug($this->view_data);
                }

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function form($id = false, $payable_type_id = 0, $origin_id = false, $net_amount = 0, $company_id = 0, $com_wht_amount = 0)
    {
        $land_inventory_id = $this->input->get('land_inventory_id') != null && $this->input->get('land_inventory_id') ? $this->input->get('land_inventory_id') : 0;

        $method = "Create";
        if ($id) {
            $method = "Update";
        } else {
            $this->db->where('user_id = ' . $this->user->id);
            $staff = $this->M_staff->get();
            $this->view_data['staff'] = $staff;
        }

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            if ($this->form_validation->run() === true) {

                $oof = $this->input->post();
                $request = $oof['request'];
                $entry_items = $oof['entry_item'];
                $accounting_entry = $oof['accounting_entry'];
                $accounting_entry['cr_total'] = str_replace(',', '', $accounting_entry['cr_total']);
                $accounting_entry['dr_total'] = str_replace(',', '', $accounting_entry['dr_total']);
                $request['gross_amount'] = remove_commas($request['gross_amount']);
                $request['total_due_amount'] = remove_commas($request['total_due_amount']);

                if (isset($oof['items'])) {
                    $list_items = $oof['items'];
                }
                if (isset($oof['commissions'])) {
                    $commission_ids = $oof['commissions'];
                }
                $project_ids = $oof['project_id'];
                $property_ids = $oof['property_id'];
                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $accounting_entry['or_number'] = 0;
                    $accounting_entry['invoice_number'] = $oof['reference'];
                    $accounting_entry['journal_type'] = 4;
                    $accounting_entry['payment_date'] = $request['date_requested'];
                    $accounting_entry['payee_type'] = $request['payee_type'];
                    $accounting_entry['payee_type_id'] = $request['payee_type_id'];
                    $accounting_entry['remarks'] = $request['particulars'];

                    $accounting_entry_info = $this->M_Accounting_entries->update($accounting_entry + $additional, $oof['acc_id']);

                    $paymentRequestID = $this->M_payment_request->update($request + $additional, $id);

                    $this->M_Accounting_entry_items->delete(['accounting_entry_id' => $oof['acc_id']]);

                    $this->M_payment_request_projects->delete(['payment_request_id' => $id]);

                    $this->M_payment_request_properties->delete(['payment_request_id' => $id]);

                    $this->M_payment_request_origins->delete(['payment_request_id' => $id]);

                    foreach ($entry_items as $key => $entry) {
                        $entry_item['accounting_entry_id'] = $oof['acc_id'];
                        $entry_item['ledger_id'] = $entry['ledger_id'];
                        $entry_item['amount'] = $entry['amount'];
                        $entry_item['dc'] = $entry['dc'];
                        $entry_item['payee_type'] = $accounting_entry['payee_type'];
                        $entry_item['payee_type_id'] = $accounting_entry['payee_type_id'];
                        $entry_item['is_reconciled'] = 0;
                        $entry_item['description'] = $entry['description'];

                        $this->M_Accounting_entry_items->insert($entry_item + $additional);
                    }

                    foreach ($project_ids as $pj_id) {
                        $pj_info['payment_request_id'] = $id;
                        $pj_info['project_id'] = $pj_id;
                        $this->M_payment_request_projects->insert($pj_info + $additional);
                    }
                    foreach ($property_ids as $pr_id) {
                        $pr_info['payment_request_id'] = $id;
                        $pr_info['property_id'] = $pr_id;
                        $this->M_payment_request_properties->insert($pr_info + $additional);
                    }


                    if ($request['payable_type_id'] == '1') {
                        $origin_ids = explode(',', $request['origin_id']);
                        foreach ($origin_ids as $origin_id) {

                            $request_item = ['payment_request_id' => $id, 'transaction_commission_id' => $origin_id];
                            $this->M_payment_request_origins->insert($request_item + $additional);

                            $commission_data = [
                                'status' => 3,
                                'updated_by' => $this->user->id,
                                'updated_at' => NOW,
                                'payment_request_id' => $id
                            ];

                            $this->M_Transaction_commission->update($commission_data, $origin_id);
                        }
                    }
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];
                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
                    $request['reference'] = uniqidPR(); // change to pr id helper
                    $request['is_paid'] = 0;
                    $request['is_complete'] = 0;


                    // begin:Save accounting entries
                    $accounting_entry['or_number'] = 0;
                    $accounting_entry['invoice_number'] = $request['reference'];
                    $accounting_entry['journal_type'] = 4;
                    $accounting_entry['payment_date'] = $request['date_requested'];
                    $accounting_entry['payee_type'] = $request['payee_type'];
                    $accounting_entry['payee_type_id'] = $request['payee_type_id'];
                    $accounting_entry['remarks'] = $request['particulars'];

                    $accounting_entry_info = $this->M_Accounting_entries->insert($accounting_entry + $additional);
                    $accounting_entry_id = $this->db->insert_id();
                    // end:Save accounting entries

                    // begin:Save payment request
                    $request['accounting_entry_id'] = $accounting_entry_info;
                    $payment_request_info = $this->M_payment_request->insert($request + $additional);
                    $payment_request_id = $this->db->insert_id();


                    // end:Save payment request

                    // for payment_request multiple origin_id saves

                    if (isset($list_items)) {
                        foreach ($list_items as $key => $item) {
                            if (isset($item['item_id'])) {
                                $item_id = $item['item_id'];
                                $item_info = $this->M_item->with_tax()->get($item_id);
                                $list_item['payment_request_id'] = $payment_request_id;
                                $list_item['item_id'] = $item_id;
                                $list_item['payable_type'] = 'PR';
                                $list_item['name'] = $item_info['name'];
                                $list_item['unit_price'] = $item_info['unit_price'];
                                $list_item['quantity'] = $item['quantity'];
                                $list_item['tax_id'] = $item['tax_id'];
                                $list_item['rate'] = $item_info['tax']['rate'];
                                $list_item['tax_amount'] = $item['tax_amount'];
                                $list_item['total_amount'] = $item['total_amount'];

                                $this->M_payable_item->insert($list_item + $additional);
                            }
                        }
                    }

                    foreach ($entry_items as $key => $entry) {
                        $entry_item['accounting_entry_id'] = $accounting_entry_info;
                        $entry_item['ledger_id'] = $entry['ledger_id'];
                        $entry_item['amount'] = $entry['amount'];
                        $entry_item['dc'] = $entry['dc'];
                        $entry_item['payee_type'] = $accounting_entry['payee_type'];
                        $entry_item['payee_type_id'] = $accounting_entry['payee_type_id'];
                        $entry_item['is_reconciled'] = 0;
                        $entry_item['description'] = $entry['description'];

                        $this->M_Accounting_entry_items->insert($entry_item + $additional);
                    }

                    foreach ($project_ids as $pj_id) {
                        $pj_info['payment_request_id'] = $payment_request_id;
                        $pj_info['project_id'] = $pj_id;
                        $this->M_payment_request_projects->insert($pj_info + $additional);
                    }
                    foreach ($property_ids as $pr_id) {
                        $pr_info['payment_request_id'] = $payment_request_id;
                        $pr_info['property_id'] = $pr_id;
                        $this->M_payment_request_properties->insert($pr_info + $additional);
                    }


                    if ($request['payable_type_id'] == '1') {
                        $origin_ids = explode(',', $request['origin_id']);
                        foreach ($origin_ids as $id) {

                            $request_item = ['payment_request_id' => $payment_request_id, 'transaction_commission_id' => $id];
                            $this->M_payment_request_origins->insert($request_item + $additional);

                            $commission_data = [
                                'status' => 3,
                                'updated_by' => $this->user->id,
                                'updated_at' => NOW,
                                'payment_request_id' => $payment_request_id
                            ];

                            $this->M_Transaction_commission->update($commission_data, $id);
                        }
                    }
                }

                // M_payment_request_payment
                $this->db->trans_complete(); # Completing payment_request

                /*Optional*/
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $response['status'] = 1;
                    $response['message'] = 'Payment Request Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;

        $id = @$id && $id != "false" ? $id : false;

        if ($id) {
            $this->view_data['info'] = $data = $this->M_payment_request
                ->with_payable_type(['fields' => 'name'])
                ->with_entry_items(['with' => ['relation' => 'accounting_ledger', 'fields' => 'name']])
                ->with_entry_item(['with' => ['relation' => 'company', 'fields' => 'name']])
                ->with_origins(['fields' => 'transaction_commission_id'])
                ->with_projects(['with' => ['relation' => 'project', 'fields' => 'name'], 'fields' => 'project_id'])
                ->with_properties(['with' => ['relation' => 'property', 'fields' => 'name'], 'fields' => 'property_id'])
                ->with_approving_department(['fields' => 'name'])
                ->with_requesting_department(['fields' => 'name'])
                ->with_land_inventory(['fields' => 'title'])
                ->with_approving_staff(['fields' => 'last_name,first_name'])
                ->with_requesting_staff(['fields' => 'last_name,first_name'])
                ->with_prepared_by_staff(['fields' => 'last_name,first_name'])
                ->get($id);

            $payable_type_id_ref = $data['payable_type_id'];

            $this->view_data['payable_type_id'] = $this->M_payable_type->get($payable_type_id_ref);

            $payee_type = $data['payee_type'];
            $payee_type_id = $data['payee_type_id'];
            $payee_info = get_person($payee_type_id, $payee_type);
            $payee_name = get_fname($payee_info);

            $this->view_data['payee_info'] = ['id' => $payee_type_id, 'name' => $payee_name];
        }

        if (($payable_type_id) && ($origin_id)) {
            # code...
            if ($payable_type_id == 1) { // commission
                # code...
                $commission = $this->M_Transaction_commission->get($origin_id);

                if ($commission) {
                    # code...

                    $this->view_data['info']['approving_department_id'] = 2;
                    $this->view_data['info']['requesting_department_id'] = 1;

                    $this->view_data['info']['approving_department']['name'] = get_value_field(2, 'departments', 'name');
                    $this->view_data['info']['requesting_department']['name'] = get_value_field(1, 'departments', 'name');
                }
            } else if ($payable_type_id == 2) { // po

                # code...
                $this->load->library('PurchaseOrderLibrary');
                $this->load->helper('purchase_order_helper');
                $po_lib = new PurchaseOrderLibrary();
                $purchase_order = $po_lib->get_obj($origin_id);

                if ($purchase_order) {
                    try {
                        $purchase_order_data = generate_payment_request_for_purchase_order($purchase_order);
                    } catch (ErrorException $e) {
                        $purchase_order_data = [];
                    }

                    $this->view_data['po_obj'] = $purchase_order;
                    $this->view_data = $this->view_data + $purchase_order_data;
                }
            }
        }
        if ($payable_type_id == '2') {
            $this->view_data['type'] = 'po';
        }
        // vdebug($this->view_data);

        if ($land_inventory_id) {

            $data = [
                'land_inventory' => $this->M_land_inventory->get($land_inventory_id)
            ];

            $this->view_data['info'] = $data;
        }

        if ($company_id && $net_amount) {
            $entry_items = [];
            $amounts = [$net_amount + $com_wht_amount, $net_amount, $com_wht_amount];
            $accounting_setting_filter = [
                'category_type' => '3',
                'period_id' => '5',
                'module_types_id' => '1',
                'entry_types_id' => '2',
                'for_recognize' => '0',
                'company_id' => $company_id
            ];
            $accounting_setting = $this->M_accounting_settings->with_accounting_setting_item()->get($accounting_setting_filter);
            if ($accounting_setting) {
                foreach ($accounting_setting['accounting_setting_item'] as $setting_key => $setting_item) {
                    $entry_items[] = [
                        'dc' => $setting_item['accounting_entry_type'],
                        'accounting_ledger' => [
                            'id' => $setting_item['accounting_entry'],
                            'name' => Dropdown::get_dynamic('accounting_ledgers', $setting_item['accounting_entry'], 'name', 'id', 'view')
                        ],
                        'amount' => $amounts[$setting_key] ? $amounts[$setting_key] : '0',
                        'description' => 'Commission'
                    ];
                }
                $this->view_data['type'] = 'commission';
                $this->view_data['entry_items'] = $entry_items;
                $this->view_data['accounting_total'] = number_format($net_amount + $com_wht_amount, 2);
                $this->view_data['accounting_total'] = $net_amount + $com_wht_amount;
            }
            $this->view_data['company'] = [
                'id' => $company_id,
                'name' => Dropdown::get_dynamic('companies', $company_id, 'name', 'id', 'view'),
            ];
        }

        $this->template->build('form', $this->view_data);
    }

    public function commission()
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->view_data['data'] = $this->M_Transaction_commission->show_rfp();

        $this->template->build('commission', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_payment_request->get($id);
            if ($list) {

                $deleted = $this->M_payment_request->delete($list['id']);
                $this->M_payment_request_origins->where(['payment_request_id' => $list['id']])->delete();
                $this->M_payment_request_properties->where(['payment_request_id' => $list['id']])->delete();
                $this->M_payment_request_projects->where(['payment_request_id' => $list['id']])->delete();

                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Payment Request successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_payment_request->delete($value);
                    $this->M_payment_request_origins->where(['payment_request_id' => $value])->delete();
                    $this->M_payment_request_properties->where(['payment_request_id' => $value])->delete();
                    $this->M_payment_request_projects->where(['payment_request_id' => $value])->delete();
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Payment request successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            $this->view_data['info'] = $info = $this->M_payment_request->with_vouchers()->with_project()
                ->with_payable_types()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_items()
                ->with_entry_item()
                ->get($id);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->view_data['company'] = $this->M_company->get(1);

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }

    public function change_status($id = false, $status = 0)
    {

        // this method will change the status of the payment request: 0 - no status, 1 - approved, 2 - disapproved, 3 - posted

        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        $response['status'] = 0;
        $response['message'] = 'Payment Request not Found';

        if ($this->input->post()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status');
            if ($id) {
                $payment_request = $this->M_payment_request->get($id);
                if ($payment_request) {

                    $data = array(
                        'status' => $status
                    );

                    $payment_request_updated = $this->M_payment_request->update($data + $additional, $id);
                    if ($payment_request_updated) {
                        $response['status'] = 1;
                        switch ($status) {
                            case 1:
                                $response['message'] = "Payment Request $id approved!";
                                break;
                            case 2:
                                $response['message'] = "Payment Request $id disapproved!";
                                break;
                            case 3:
                                $response['message'] = "Payment Request $id posted!";
                                $posted_data = ['posted_by' => $this->user->id];
                                $this->M_payment_request->update($posted_data + $additional, $id);
                                break;
                        }
                    }
                }
            }
            echo json_encode($response);
            exit();
        }
    }

    public function amend($id = '')
    {
        $additional = [
            'updated_at' => NOW,
            'updated_by' => $this->user->id
        ];
        $response['status'] = '0';
        $response['message'] = 'Failed to Amend Request';
        $id ?: redirect(base_url('payment_request'));
        $this->db->where('status = 1');
        $payment_request = $this->M_payment_request->with_entry_item(['with' => ['relation' => 'company', 'fields' => 'name']])->with_entry_items(['with' => ['relation' => 'accounting_ledger', 'fields' => 'name']])->get($id);
        $payment_request ?: redirect(base_url('payment_request'));

        if ($this->input->post()) {
            $post = $this->input->post();
            $entry_id = $post['acc_id'];
            $entry = $payment_request['entry_item'];
            $post_accounting_entry = $post['accounting_entry'];
            $post_entry_items = $post['entry_item'];

            $compare_entry = [
                'company_id' => $entry['company_id'],
                'dr_total' => $entry['dr_total'],
                'cr_total' => $entry['cr_total']
            ];

            if ($payment_request['date_requested'] != $post['request']['date_requested']) {
                $request_data = [
                    'date_requested' => $post['request']['date_requested']
                ];
                $this->M_payment_request->update($request_data + $this->u_additional, $id);
            }

            if ($compare_entry != $post_accounting_entry) {
                $this->M_Accounting_entries->update($post_accounting_entry + $this->u_additional, $entry_id);
            }

            $this->M_Accounting_entry_items->delete(['accounting_entry_id' => $entry_id]);
            foreach ($post_entry_items as $key => $p_entry) {
                $entry_item['accounting_entry_id'] = $entry_id;
                $entry_item['ledger_id'] = $p_entry['ledger_id'];
                $entry_item['amount'] = $p_entry['amount'];
                $entry_item['dc'] = $p_entry['dc'];
                $entry_item['payee_type'] = $entry['payee_type'];
                $entry_item['payee_type_id'] = $entry['payee_type_id'];
                $entry_item['is_reconciled'] = 0;
                $entry_item['description'] = $p_entry['description'];

                $this->M_Accounting_entry_items->insert($entry_item + $additional);
            }

            $response['status'] = '1';
            $response['message'] = 'Amended Request ' . $payment_request['reference'];

            echo json_encode($response);
            exit();
        }

        $this->view_data['payment_request'] = $payment_request;
        $this->template->build('amend', $this->view_data);
    }
}
