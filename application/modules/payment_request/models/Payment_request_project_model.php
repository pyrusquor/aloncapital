<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Payment_request_project_model extends MY_Model
{

    public $table = 'payment_request_projects'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'id',
        'payment_request_id',
        'project_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');

        $this->soft_deletes = true;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;
    }
}