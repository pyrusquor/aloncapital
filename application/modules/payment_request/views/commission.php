<?php
//vdebug($data);
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Request for Payment Commissions</h3>
        </div>
        <div class="kt-subheader__toolbar">

            <div class="kt-subheader__wrapper">
                <a id="create_pr" href="<?php echo site_url('payment_request/form/0/'); ?>"
                        class="btn btn-label-success btn-elevate btn-sm disabled">
                    <i class="fa fa-check"></i> Submit
                </a>
                <a href="<?php echo base_url('payment_request'); ?>" class="btn btn-label-instagram"><i
                            class="la la-times"></i>
                    Back
                </a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-sm-4">
                    Amount: <span id="total_amount"></span>
                </div>
                <div class="input-group">
                    <input type="number" class="form-control" id="commission_amount" placeholder=""
                           name="commission_amount" readonly hidden>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="commission_table">
                <thead>
                <tr>
                    <th>ID</th>
                    <!-- ==================== begin: Add header fields ==================== -->
                    <th>Period</th>
                    <th>Seller</th>
                    <th>Project</th>
                    <th>Percentage Rate</th>
                    <th>Amount</th>
                    <th>Commission Rate</th>
                    <th>Commission</th>
                    <th>With Holding Tax Rate</th>
                    <th>Status</th>
                    <th>Remarks</th>
                    <!-- ==================== end: Add header fields ==================== -->
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if (isset($data)): ?>
                    <?php foreach ($data as $key => $value): ?>
                        <tr>
                            <td><?= $value['id'] ?></td>
                            <td><?= $value['period_type_id'] ?></td>
                            <td><?= $value['last_name']. ' ' . $value['first_name'] ?></td>
                            <td><?= $value['name'] ?></td>
                            <td><?= $value['percentage_rate_to_collect'] ?></td>
                            <td><?= money_php($value['amount_rate_to_collect']) ?></td>
                            <td><?= $value['commission_percentage_rate'] ?></td>
                            <td><?= $value['commission_amount_rate'] ?></td>
                            <td><?= $value['wht_rate'] ?>%</td>
                            <td><?= $value['status'] ?></td>
                            <td><?= $value['remarks'] ?></td>
                            <td>
                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input type="checkbox" name="id[]" value="<?= $value['id'] ?>"
                                           class="m-checkable recon_check"
                                           data-amount="<?=$value['commission_amount_rate']?>"
                                           data-id="<?=$value['id'] ?>"
                                           data-seller="<?=$value['seller_id']?>" >
                                    <span></span>
                                </label>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="10" class="text-center">No record found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
        <!-- begin:: Footer -->
    </div>
</div>