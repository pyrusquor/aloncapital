<?php
$entry_item = $info['entry_item'];
if(!isset($entry_items)){
    $entry_items = $info['entry_items'];
}
if(!isset($company)){
    $company = $entry_item['company'];
}
if(!isset($accounting_total)){
    $accounting_total = $entry_item['cr_total'];
}
?>
<input type='hidden' name='acc_id' value='<?= $info['accounting_entry_id'] ?>'>
<input type='hidden' name='reference' value='<?= $info['reference'] ?>'>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="">Companies<span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select name="accounting_entry[company_id]" id="company_id" class="suggests form-control" data-module="companies">
                    <?php if ($company) : ?>
                        <option value="<?= $company['id']; ?>" selected><?= $company['name']; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<!-- Accounting Entry Items -->
<?php if (!empty($entry_items)) : ?>
    <div id="entry_item_form_repeater">
    <div data-repeater-list="entry_item">

    <?php foreach ($entry_items as $key => $entry_item) : ?>
        <?php
        $item_id = $key;
        $ledger_id = $entry_item['accounting_ledger'];
        $dc = $entry_item['dc'];
        $amount = $entry_item['amount'];
        $description = $entry_item['description'];
        ?>


                <div data-repeater-item="entry_item" class="row">
                    <div class="col-sm-2 d-none">
                        <div class="form-group">
                            <label>ID <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <input class="form-control" name="entry_item[<?php echo $item_id ?>][id]" value="<?php echo set_value('item_id', $item_id); ?>" placeholder="Type" autocomplete="off" />

                            </div>
                            <?php echo form_error('dc'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Type <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <select class="form-control" name="entry_item[<?php echo $item_id ?>][dc]" placeholder="Type" autocomplete="off" id="entry_item_dc">
                                    <option value="">Select Option</option>
                                    <option value="d" <?= $dc == 'd' ? 'selected' : '' ?>>D</option>
                                    <option value="c" <?= $dc == 'c' ? 'selected' : '' ?>>C</option>
                                </select>

                            </div>
                            <?php echo form_error('dc'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Ledger <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <select class="form-control suggests" name="entry_item[<?php echo $item_id ?>][ledger_id]" value="<?php echo set_value('ledger_id', $ledger_id['id']); ?>" id="entry_item_ledger" data-ledger="<?php echo $ledger_id['id']; ?>" data-param="company_id" data-module="accounting_ledgers">
                                    <?php if ($ledger_id) : ?>
                                        <option value="<?= $ledger_id['id']; ?>" selected><?= $ledger_id['name']; ?></option>
                                    <?php endif ?>
                                </select>

                            </div>
                            <?php echo form_error('ledger_id'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Dr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="number" class="form-control" name="entry_item[<?php echo $item_id ?>][amount]" value="<?= $dc == 'd' ? $amount : '0' ?>" <?= $dc == 'c' ? 'disabled' : '' ?> placeholder="DR Amount" autocomplete="off" id="dr_amount_input">

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Cr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="number" class="form-control" name="entry_item[<?php echo $item_id ?>][amount]" value="<?= $dc == 'c' ? $amount : '0' ?>" <?= $dc == 'd' ? 'disabled' : '' ?> placeholder="CR Amount" autocomplete="off" id="cr_amount_input">

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Description</label>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" name="entry_item[<?php echo $item_id ?>][description]" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">

                                    </div>
                                    <?php echo form_error('description'); ?>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="col-sm-3">

                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                        <i class="la la-trash-o"></i>
                                    </a>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
    <?php endforeach; ?>
    </div>
    <div class="form-group form-group-last row">
            <div class="offset-10"></div>
            <div class="col-lg-2">
                <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                    <i class="la la-plus"></i> Add Entry Item
                </a>
            </div>
        </div>
    </div>

<?php else : ?>
    <div id="entry_item_form_repeater">
        <div data-repeater-list="entry_item">
            <div data-repeater-item="entry_item" class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Type <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon">
                            <select class="form-control" name="entry_item[dc]" value="<?php echo set_value('dc', $dc); ?>" placeholder="Type" autocomplete="off" id="entry_item_dc">
                                <option value="">Select option</option>
                                <option value="d">D</option>
                                <option value="c">C</option>
                            </select>

                        </div>
                        <?php echo form_error('dc'); ?>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Ledger <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon">
                            <select class="form-control suggests" name="entry_item[ledger_id]" value="<?php echo set_value('ledger_id', $ledger_id); ?>" placeholder="Type" autocomplete="off" id="entry_item_ledger" data-param="company_id" data-module="accounting_ledgers">
                                <option>Select option</option>
                            </select>
                        </div>
                        <?php echo form_error('ledger_id'); ?>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Dr Amount <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon">
                            <input type="number" class="form-control" name="entry_item[amount]" value="<?php echo set_value('amount', $amount); ?>" placeholder="DR Amount" autocomplete="off" id="dr_amount_input">

                        </div>
                        <?php echo form_error('amount'); ?>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Cr Amount <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon">
                            <input type="number" class="form-control" name="entry_item[amount]" value="<?php echo set_value('amount', $amount); ?>" placeholder="CR Amount" autocomplete="off" id="cr_amount_input">

                        </div>
                        <?php echo form_error('amount'); ?>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Description</label>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="kt-input-icon">
                                    <input type="text" class="form-control" name="entry_item[description]" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">

                                </div>
                                <?php echo form_error('description'); ?>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-sm-3">

                                <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                    <i class="la la-trash-o"></i>
                                </a>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div class="form-group form-group-last row">
            <div class="offset-10"></div>
            <div class="col-lg-2">
                <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                    <i class="la la-plus"></i> Add Entry Item
                </a>
            </div>
        </div>
    </div>

<?php endif ?>


<div class="row">
    <div class="offset-3"></div>
    <div class="col-sm-3">
        <div class="form-group row">
            <label for="dTotal" class="col-sm-4 col-form-label">Dr Total</label>
            <div class="col-sm-8">
                <input class="form-control" name="accounting_entry[dr_total]" type="text" placeholder="D Total" id="dTotal" value="<?= $accounting_total ?>" readonly>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group row">
            <label for="cTotal" class="col-sm-4 col-form-label">Cr Total</label>
            <div class="col-sm-8">
                <input class="form-control" name="accounting_entry[cr_total]" type="text" placeholder="C Total" id="cTotal" value="<?= $accounting_total ?>" s readonly>
            </div>
        </div>
    </div>

    <div class="offset-3"></div>
</div>
<!-- /Accounting Entry Items -->