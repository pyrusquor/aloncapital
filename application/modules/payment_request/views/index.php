<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Payment Request
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc"><?php echo (!empty($totalRec)) ? $totalRec : 0; ?> TOTAL</span>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                    <input type="text" class="form-control" placeholder="Search Payment Request..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"></path>
                                </g>
                            </svg>

                            <!--<i class="flaticon2-search-1"></i>-->
                        </span>
                    </span>
                </div>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <!-- <a href="<?php echo site_url('payment_request/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Add Payment Request
            </a>
            <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                <i class="fa fa-filter"></i> Filter
            </button>

            <button type="button" class="btn btn-label-primary btn-elevate btn-sm" id="bulkDelete" disabled>
                <i class="fa fa-trash"></i> Delete Selected
            </button> -->
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
            <a href="<?php echo site_url('payment_request/form/0/2'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Add Payment Request (PO)
            </a>
            <a href="<?php echo site_url('payment_request/form/'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Add Payment Request (Non PO)
            </a>
        </div>

        <div class="module__create">

        </div>

        <div class="module__filter">
            <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                <i class="fa fa-filter"></i> Filter
            </button>
        </div>
    </div>
</div>


<!-- CONTENT -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">

        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form" id="advance_search">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">Reference:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="reference" class="form-control form-control-sm _filter" placeholder="Reference" id="_column_2" data-column="2">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building-o"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Payee type:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('payee_type', Dropdown::get_static('payee_type'), '', 'class="form-control form-control-sm _filter" id="payee_type_select" name="payee_type"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Payee:</label>
                                <select name="payee_type_id" class="form-control suggests _filter" id="payee_id_select" data-type="person"></select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">Completed:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('is_complete', Dropdown::get_static('payment_request_status'), '', 'class="form-control form-control-sm _filter" name="is_complete"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Status:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('status', Dropdown::get_static('request_status'), '', 'class="form-control form-control-sm _filter" name="status"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <label class="form-control-label">Payment Status:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('is_paid', Dropdown::get_static('payment_request_payment_status'), '', 'class="form-control form-control-sm _filter" name="is_paid"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">Due Date:</label>
                                <input type="text" class="form-control form-control-sm kt_pr_daterangepicker _filter" id="due_date" name="due_date">
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Date of Request:</label>
                                <input type="text" class="form-control form-control-sm kt_pr_daterangepicker _filter" id="date_requested" name="date_requested">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
            <form class="kt-form" id="export_csv" action="<?php echo site_url('company/export_csv'); ?>" method="POST">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Document Type</label>
                            <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control form-control-sm" name="status" id="import_status">
                                    <option value=""> -- Update Existing Data -- </option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
                            </div>
                            <?php echo form_error('status'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <form class="kt-form" id="upload_form" action="<?php echo site_url('company/import'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Upload CSV file:</label>
                            <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
                            <input type="file" name="file" class="" size="1000" accept="*.csv">
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group form-group-last row custom_import_style">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm disabled" form="upload_form">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                        <div class="col-lg-6 kt-align-right">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm disabled" form="export_csv">
                                <i class="fa fa-file-csv"></i>
                            </button>
                            <button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm disabled" disabled id="btn_upload_guide" data-toggle="modal" data-target="#upload_guide">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentrequest_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Reference</th>
                    <th>Payee</th>
                    <th>Payable Type</th>
                    <th>Property</th>
                    <th>Due Date</th>
                    <th>Date Requested</th>
                    <th>Gross Amount</th>
                    <th>Tax Rate</th>
                    <th>Paid Amount</th>
                    <th>Remaining Amount</th>
                    <th>Payment Status</th>
                    <th>Completed</th>
                    <th>Particulars</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Last Update By</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Payable Type:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="payable_types" id="filterPayableType" name="filterPayableType">
                                <option value="">Select Payable Type</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Project:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="projects" id="filterProject" name="filterProject">
                                <option value="">Select Project</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Property:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="properties" id="filterProperty" name="filterProperty">
                                <option value="">Select Property</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Payee Type:</label>
                            <?php echo form_dropdown('filterPayeeType', Dropdown::get_static('payee_type'), set_value('filterPayeeType', ''), 'class="form-control" id="filterPayeeTypeID"'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Payee:</label>
                            <select class="form-control suggests_modal" id="filterPayee" name="payee_id">
                                <option value="">Select Payee</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Is Paid:</label>
                            <select class="form-control" id="filterIsPaid" name="filterIsPaid">
                                <option value="">Select All</option>
                                <option value="0">Not Paid</option>
                                <option value="1">Paid</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Due Date:</label>
                            <input type="text" class="form-control kt_transaction_daterangepicker _filter" name="filterDueDate" id="filterDueDate" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>