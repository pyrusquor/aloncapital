<?php
    $gross_amount = isset($info['gross_amount']) && $info['gross_amount'] ? $info['gross_amount'] : 0;
    $commission_amount = isset($info['commission_amount']) && $info['commission_amount'] ? $info['commission_amount'] : 0;
    $tax_amount = isset($info['tax_amount']) && $info['tax_amount'] ? $info['tax_amount'] : 0;
    $tax_percentage = isset($info['tax_percentage']) && $info['tax_percentage'] ? $info['tax_percentage'] : 0;
    $wht_amount = isset($info['wht_amount']) && $info['wht_amount'] ? $info['wht_amount'] : 0;
    $wht_percentage = isset($info['wht_percentage']) && $info['wht_percentage'] ? $info['wht_percentage'] : 0;
    $payment_voucher_id = isset($info['payment_voucher_id']) && $info['payment_voucher_id'] ? $info['payment_voucher_id'] : 0;
    $is_paid = isset($info['is_paid']) && $info['is_paid'] ? $info['is_paid'] : 0;
    $is_complete = isset($info['is_complete']) && $info['is_complete'] ? $info['is_complete'] : 0;
    $total_due_amount = isset($info['total_due_amount']) && $info['total_due_amount'] ? $info['total_due_amount'] : 0;
    $particulars = isset($info['particulars']) && $info['particulars'] ? $info['particulars'] : '';
?>
<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Gross Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="gross_amount" placeholder=""
                                name="request[gross_amount]"
                                value="<?php echo set_value('gross_amount"', @$gross_amount); ?>">
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="">VAT Tax Percentage</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">%</span></div>
                            <input type="number" class="form-control" id="tax_percentage" placeholder=""
                                name="request[tax_percentage]"
                                value="<?php echo set_value('tax_percentage"', @$tax_percentage); ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="">Tax Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="vat_tax_amount" placeholder=""
                                name="request[tax_amount]" value="<?php echo set_value('tax_amount"', @$tax_amount); ?>"
                                readonly>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Withholding Tax Percentage</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">%</span></div>
                            <input type="number" class="form-control" id="wht_percentage" placeholder=""
                                name="request[wht_percentage]"
                                value="<?php echo set_value('$wht_percentage"', @$wht_percentage); ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="">Withholding Tax Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="wht_amount" placeholder=""
                                name="request[wht_amount]" value="<?php echo set_value('wht_amount"', @$wht_amount); ?>"
                                readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="">Total Due Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="total_due_amount" placeholder=""
                                name="request[total_due_amount]"
                                value="<?php echo set_value('total_due_amount"', @$total_due_amount); ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="message">Particulars</label>
                    <textarea type="text" class="form-control" id="particulars" name="request[particulars]"
                        placeholder="Particulars" rows="7"><?=$particulars;?></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php if (isset($commissions)): ?>
                <?php foreach ($commissions as $key => $value): ?>
                <div class="form-group">
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="" name="commissions[<?= $key ?>]"
                                value="<?php echo set_value('commissions[' + $key + ']', @$value); ?>" readonly hidden>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

