<?php
    $id = isset($item['id']) && $item['id'] ? $item['id'] : '';
    $payment_request_id = isset($item['payment_request_id']) && $item['payment_request_id'] ? $item['payment_request_id'] : '';
    $item_id = isset($item['item_id']) && $item['item_id'] ? $item['item_id'] : '';
    $name = isset($item['name']) && $item['name'] ? $item['name'] : '';
    $unit_price = isset($item['name']) && $item['name'] ? $item['name'] : '';
    $quantity = isset($item['name']) && $item['name'] ? $item['name'] : '';
    $tax_id = isset($item['name']) && $item['name'] ? $item['name'] : '';
    $rate = isset($item['name']) && $item['name'] ? $item['name'] : '';
    $tax_amount = isset($item['name']) && $item['name'] ? $item['name'] : '';
    $total_amount = isset($item['name']) && $item['name'] ? $item['name'] : '';

    $tax_name = isset($item['tax']['name']) && $item['tax']['name'] ? $item['tax']['name'] : 'N/A';
    $payable_type_id = isset($info['payable_type_id']) && $info['payable_type_id'] ? $info['payable_type_id'] : '';
?>
<div class="row">
    <div class="container">

        <?php if ($payable_type_id != 1): ?>
            
            <!-- Payment Request Items -->
            <?php if (!empty($items)): ?>
                <?php foreach ($items as $key => $item): ?>
                    <?php
                    $payment_request_id = isset($item['payment_request_id']) && $item['payment_request_id'] ? $item['payment_request_id'] : '';
                    $item_id = isset($item['item_id']) && $item['item_id'] ? $item['item_id'] : '';
                    $name = isset($item['name']) && $item['name'] ? $item['name'] : '';
                    $unit_price = isset($item['name']) && $item['name'] ? $item['name'] : '';
                    $quantity = isset($item['name']) && $item['name'] ? $item['name'] : '';
                    $tax_id = isset($item['name']) && $item['name'] ? $item['name'] : '';
                    $rate = isset($item['name']) && $item['name'] ? $item['name'] : '';
                    $tax_amount = isset($item['name']) && $item['name'] ? $item['name'] : '';
                    $total_amount = isset($item['name']) && $item['name'] ? $item['name'] : '';

                    ?>
                    <div id="request_item_form_repeater">
                        <div data-repeater-list="item[<?php echo $item_id; ?>]">
                            <div data-repeater-item="item[<?php echo $item_id; ?>]" class="row">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <!-- Suggest field
                                    <div class="form-group">
                                        <label class="">Item Name <span class="kt-font-danger">*</span></label>
                                        <select class="form-control suggests" data-module="item"  id="item_id" name="item[<?php echo $item_id ?>[item_id]" required>
                                            <option value="">Select Item</option>
                                            <?php if ($name): ?>
                                                <option value="<?php echo $item_id; ?>" selected><?php echo $name; ?></option>
                                            <?php endif ?>
                                        </select>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                    -->
                                        <div class="form-group">
                                            <label class="">Item Name <span class="kt-font-danger">*</span></label>
                                            <select class="form-control suggests" data-module="item"
                                                    name="item[<?php echo $item_id ?>][name]" required>
                                                <option value="">Select Item</option>
                                                <?php if ($name): ?>
                                                    <option value="<?php echo $item_id; ?>"
                                                            selected><?php echo $name; ?></option>
                                                <?php endif ?>
                                            </select>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Unit Price <span class="kt-font-danger">*</span></label>
                                            <div class="kt-input-icon kt-input-icon--left">
                                                <input type="number" class="form-control item_detail"
                                                       name="item[<?php echo $item_id ?>][unit_price]"
                                                       value="<?php echo set_value('unit_price', $unit_price); ?>"
                                                       placeholder="Unit Price"
                                                       autocomplete="off" id="unit_price">

                                            </div>
                                            <?php echo form_error('unit_price'); ?>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Quantity</label>
                                            <div class="kt-input-icon kt-input-icon--left">
                                                <input type="text" class="form-control"
                                                       id="item_quantity"
                                                       name="item[<?php echo $item_id ?>][quantity]"
                                                       value="<?php echo set_value('quantity', $quantity); ?>"
                                                       placeholder="Quantity" autocomplete="off">
                                            </div>
                                            <?php echo form_error('quantity'); ?>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group d-none">
                                            <label class="">Tax <span class="kt-font-danger">*</span></label>
                                            <div class="kt-input-icon kt-input-icon--left">
                                                <input type="number" class="form-control item_detail"
                                                       name="item[<?php echo $item_id ?>][tax_id]"
                                                       value="<?php echo set_value('tax_id', $tax_id); ?>"
                                                       placeholder="Tax"
                                                       autocomplete="off" id="tax_id" readonly>
                                            </div>
                                            <?php echo form_error('tax_id'); ?>
                                            <span class="form-text text-muted"></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Tax <span class="kt-font-danger">*</span></label>
                                            <div class="kt-input-icon kt-input-icon--left">
                                                <input type="number" class="form-control item_detail"
                                                       name="item[<?php echo $item_id ?>][tax][name]"
                                                       id="tax_name"
                                                       value="<?php echo set_value('tax_name', $tax_name); ?>"
                                                       placeholder="Tax"
                                                       autocomplete="off" readonly>
                                            </div>
                                            <?php echo form_error('tax_name'); ?>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Tax Amount <span class="kt-font-danger">*</span></label>
                                            <div class="kt-input-icon kt-input-icon--left">
                                                <input type="number" class="form-control item_detail"
                                                       name="item[<?php echo $item_id ?>][tax_amount]"
                                                       value="<?php echo set_value('tax_amount', $tax_amount); ?>"
                                                       placeholder="Tax Amount"
                                                       autocomplete="off" id="tax_amount">
                                            </div>
                                            <?php echo form_error('tax_amount'); ?>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <div class="form-group">
                                                    <label>Total Amount <span class="kt-font-danger">*</span></label>
                                                    <div class="kt-input-icon kt-input-icon--left">
                                                        <input type="number" class="form-control item_detail"
                                                               name="item[<?php echo $item_id ?>][total_amount]"
                                                               value="<?php echo set_value('total_amount', $total_amount); ?>"
                                                               placeholder="Total Amount"
                                                               autocomplete="off" id="item[<?php echo $item_id ?>][total_amount]">
                                                    </div>
                                                    <?php echo form_error('total_amount'); ?>
                                                    <span class="form-text text-muted"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div id="request_item_form_repeater">
                    <div data-repeater-list="items">
                        <div data-repeater-item="items">
                            <div class="row">
                                <div class="col-sm-2">
                                    <!-- Suggest field
                                <div class="form-group">
                                    <label class="">Item Name <span class="kt-font-danger">*</span></label>
                                    <select class="form-control suggests" data-module="item"  id="item_id" name="item[item_id]" required>
                                        <option value="">Select Item</option>
                                        <?php if ($name): ?>
                                            <option value="<?php echo $item_id; ?>" selected><?php echo $name; ?></option>
                                        <?php endif ?>
                                    </select>
                                    <span class="form-text text-muted"></span>
                                </div>
                                -->
                                    <div class="form-group">
                                        <label class="">Item Name <span class="kt-font-danger">*</span></label>
                                        <select class="form-control" name="item[item_id]"
                                                value="<?php echo set_value('item_id', $item_id); ?>"
                                                placeholder="Select Item"
                                                autocomplete="off" id="item_id">
                                            <option>Select option</option>
                                        </select>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Unit Price <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="number" class="form-control item_detail"
                                                   name="item[unit_price]"
                                                   id="item_unit_price"
                                                   value="<?php echo set_value('unit_price', $unit_price); ?>"
                                                   placeholder="Unit Price"
                                                   autocomplete="off" id="unit_price" readonly>
                                        </div>
                                        <?php echo form_error('unit_price'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control"
                                                   name="item[quantity]"
                                                   id="item_quantity"
                                                   value="<?php echo set_value('quantity', $quantity); ?>"
                                                   placeholder="Quantity" autocomplete="off">
                                        </div>
                                        <?php echo form_error('quantity'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group d-none">
                                        <label>Tax ID <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="number" class="form-control item_detail"
                                                   name="item[tax_id]"
                                                   id="item_tax_id"
                                                   value="<?php echo set_value('tax_id', $tax_id); ?>"
                                                   placeholder="Tax"
                                                   autocomplete="off" readonly>
                                        </div>
                                        <?php echo form_error('tax_amount'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Tax <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control item_detail"
                                                   name="item[tax][name]"
                                                   id="tax_name"
                                                   value="<?php echo set_value('tax_name', $tax_name); ?>"
                                                   placeholder="Tax"
                                                   autocomplete="off" readonly>
                                        </div>
                                        <?php echo form_error('tax_amount'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Tax Amount <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="number" class="form-control item_detail"
                                                   name="item[tax_amount]"
                                                   value="<?php echo set_value('tax_amount', $tax_amount); ?>"
                                                   placeholder="Tax Amount"
                                                   autocomplete="off" id="item_tax_amount">
                                        </div>
                                        <?php echo form_error('tax_amount'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label>Total Amount <span class="kt-font-danger">*</span></label>
                                                <div class="kt-input-icon kt-input-icon--left">
                                                    <input type="number" class="form-control item_detail"
                                                           name="item[total_amount]"
                                                           id="item_total_amount"
                                                           value="<?php echo set_value('total_amount', $total_amount); ?>"
                                                           placeholder="Total Amount"
                                                           autocomplete="off" id="item_total_amount" readonly>
                                                </div>
                                                <?php echo form_error('total_amount'); ?>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <a href="javascript:;" data-repeater-delete=""
                                                   class="btn-sm btn btn-label-danger btn-bold">
                                                    <i class="la la-trash-o"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (empty($items)): ?>
                <div class="form-group form-group-last row">
                    <div class="offset-10"></div>
                    <div class="col-lg-2">
                        <a id="add_entry_btn" href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                            <i class="la la-plus"></i> Add Entry Item
                        </a>
                    </div>
                </div>

            <?php endif ?>

            <!-- /Payment Request  Items -->

        <?php endif ?>

    </div>
</div>