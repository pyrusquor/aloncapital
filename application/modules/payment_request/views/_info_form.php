<?php
$origin_id = implode(',',array_column($info['origins'],'transaction_commission_id'));

// Info for payable type field
$payable_name = $info['payable_type']['name'];
$payable_type_id = $info['payable_type']['id'];

// Store info array items to variables for readability
$project_array = $info['projects'];
$property_array = $info['properties'];
$payee_type = $info['payee_type'];
$land_inventory = $info['land_inventory'];
$date_requested = $info['date_requested'];
$approving_department = $info['approving_department'];
$due_date = $info['due_date'];
$requesting_department = $info['requesting_department'];
$approving_staff = $info['approving_staff'];
$prepared_by = $info['prepared_by_staff'];
$requesting_staff = $info['requesting_staff'];

if(isset($staff)){
    $staff_id = $staff['id'];
    $prepared_by['first_name'] = $staff['first_name'];
    $prepared_by['last_name'] = $staff['last_name'];
    $prepared_by['id'] = $staff_id;
    $requesting_staff['first_name'] = $staff['first_name'];
    $requesting_staff['last_name'] = $staff['last_name'];
    $requesting_staff['id'] = $staff_id;
}

// vdebug($staff);

?>
<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Origin</label>
                    <div class="kt-input-icon">
                        <input type="text" readonly class="form-control" placeholder="Origin" id="origin_id" name="request[origin_id]" value="<?php echo set_value('origin_id"', @$origin_id); ?>" autocomplete="off">
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Payable Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="payable_types" id="payable_type_id" name="request[payable_type_id]">
                        <option value="">Select Payable Type</option>
                        <?php if ($payable_name) : ?>
                            <option value="<?= $payable_type_id; ?>" selected><?= $payable_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Project Name <span class="kt-font-danger"></span></label>
                    <select class="form-control suggests-multiple" data-module="projects" id="project_id" name="project_id[]" multiple>
                        <?php if ($project_array) : ?>
                            <?php foreach($project_array as $pa_key => $project): ?>
                                <option value="<?= $project['project']['id']; ?>" selected><?= $project['project']['name']; ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Property Name <span class="kt-font-danger"></span></label>
                    <select class="form-control suggests-multiple" data-module="properties" id="property_id" name="property_id[]" multiple>
                    <?php if ($property_array) : ?>
                            <?php foreach($property_array as $pra_key => $property): ?>
                                <option value="<?= $property['property']['id']; ?>" selected><?= $property['property']['name']; ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Payee Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('request[payee_type]', Dropdown::get_static('payee_type'), set_value('$request[payee_type]', @$payee_type), 'class="form-control" id="payee_type"'); ?>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Land Inventory </label>
                    <div class="kt-input-icon">
                        <select class="form-control suggests" data-module="land_inventories" data-type='land_inventory' id="land_inventory_id" name="request[land_inventory_id]">
                            <option value="">Select Property</option>
                            <?php if ($land_inventory) : ?>
                                <option value="<?= $land_inventory['id']; ?>" selected><?= $land_inventory['title']; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date of Request <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Date of Request" name="request[date_requested]" value="<?php echo set_value('date_requested"', @$date_requested); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Payee <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-type="person" id="payee_type_id" name="request[payee_type_id]">
                        <option value="">Select Payee</option>
                        <?php if ($payee_info) : ?>
                            <option value="<?= $payee_info['id']; ?>" selected><?= $payee_info['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Approving Department <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="departments" id="approving_department_id" name="request[approving_department_id]">
                        <option value="">Select Property</option>
                        <?php if ($approving_department) : ?>
                            <option value="<?= $approving_department['id']; ?>" selected><?= $approving_department['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Request Due Date <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Request Due Date" name="request[due_date]" value="<?php echo set_value('due_date"', @$due_date); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Requesting Department <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="departments" id="requesting_department_id" name="request[requesting_department_id]">
                        <option value="">Select Department</option>
                        <?php if ($requesting_department) : ?>
                            <option value="<?= $requesting_department['id']; ?>" selected><?= $requesting_department['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id" name="request[approving_staff_id]">
                        <option value="">Select Property</option>
                        <?php if ($approving_staff) : ?>
                            <option value="<?= $approving_staff['id']; ?>" selected><?= $approving_staff['last_name'] . ' ' . $approving_staff['first_name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Prepared by <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person" id="prepared_by" name="request[prepared_by]">
                        <option value="">Select Staff</option>
                        <?php if ($prepared_by) : ?>
                            <option value="<?= $prepared_by['id']; ?>" selected><?= $prepared_by['last_name'] . ' ' . $prepared_by['first_name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person" id="requesting_staff_id" name="request[requesting_staff_id]">
                        <option value="">Select Requesting Staff</option>
                        <?php if ($requesting_staff) : ?>
                            <option value="<?= $requesting_staff['id']; ?>" selected><?= $requesting_staff['last_name'] . ' ' . $requesting_staff['first_name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    /* .select2-selection__clear{
        all:unset
    } */
</style>