<?php
    $id = isset($accounting_entries['id']) && $accounting_entries['id'] ? $accounting_entries['id'] : '';
    $company_id = isset($accounting_entries['company_id']) && $accounting_entries['company_id'] ? $accounting_entries['company_id'] : '';
    $cr_total = isset($accounting_entries['cr_total']) && $accounting_entries['cr_total'] ? $accounting_entries['cr_total'] : '';
    $dr_total = isset($accounting_entries['dr_total']) && $accounting_entries['dr_total'] ? $accounting_entries['dr_total'] : '';
    $item_id = isset($entry_item['id']) && $entry_item['id'] ? $entry_item['id'] : '';
    $ledger_id = isset($entry_item['ledger_id']) && $entry_item['ledger_id'] ? $entry_item['ledger_id'] : '';
    $dc = isset($entry_item['dc']) && $entry_item['dc'] ? $entry_item['dc'] : '';
    $description = isset($entry_item['description']) && $entry_item['description'] ? $entry_item['description'] : '';
    $amount = isset($entry_item['amount']) && $entry_item['amount'] ? $entry_item['amount'] : '';

    $company_name = $accounting_entries['company_id'] ? get_object_from_table($accounting_entries['company_id'], 'companies', true, 'id ASC', 0, 1, 'name')->name : '';
    $ledger_name = $entry_item['ledger_id'] ? get_object_from_table($entry_item['ledger_id'], 'accounting_ledgers', true, 'id ASC', 0, 1, 'name')->name : '';
?>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="">Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select name="accounting_entry[company_id]" id="company_id" class="suggests form-control"
                        data-module="companies">
                    <option value="0">Select Company
                    <option value="<?= $company_id; ?>" selected>
                        <?= $company_name; ?>
                    </option>
                </select>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-2">
        <div class="form-group">
            <label>Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control" name="entry_item[dc]"
                        id="entry_item_dc">
                    <option value="">Select option</option>
                    <option value="d" selected>D</option>
                    <option value="c">C</option>
                </select>

            </div>
            <?php echo form_error('dc'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Ledger <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control suggests" name="entry_item[ledger_id]"
                        id="entry_item_ledger" data-param="company_id"
                        data-module="accounting_ledgers">
                    <option>Select option</option>
                    <option selected value="<?=$ledger_id;?>"><?=$ledger_name?></option>
                </select>
            </div>
            <?php echo form_error('ledger_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label>Dr Amount <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="number" class="form-control" name="entry_item[amount]"
                       value="<?php echo set_value('amount', $amount); ?>" placeholder="DR Amount"
                       autocomplete="off" id="dr_amount_input">

            </div>
            <?php echo form_error('amount'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label>Cr Amount <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="number" class="form-control" name="entry_item[amount]"
                       value="<?php echo set_value('amount', $amount); ?>" placeholder="CR Amount"
                       autocomplete="off" id="cr_amount_input">

            </div>
            <?php echo form_error('amount'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Description</label>
            <div class="row">
                <div class="col-sm-9">
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="entry_item[description]"
                               value="<?php echo set_value('description', $description); ?>"
                               placeholder="Description" autocomplete="off">

                    </div>
                    <?php echo form_error('description'); ?>
                    <span class="form-text text-muted"></span>
                </div>
                <div class="col-sm-3">

                    <a href="javascript:;" data-repeater-delete=""
                       class="btn-sm btn btn-label-danger btn-bold">
                        <i class="la la-trash-o"></i>
                    </a>

                </div>
            </div>

        </div>
    </div>

</div>

<div class="row">
    <div class="offset-3"></div>
    <div class="col-sm-3">
        <div class="form-group row">
            <label for="dTotal" class="col-sm-4 col-form-label">Dr Total</label>
            <div class="col-sm-8">
                <input class="form-control" name="accounting_entry[dr_total]" type="text" placeholder="D Total"
                       id="dTotal"
                       value="<?php echo set_value('dr_total', $dr_total); ?>" readonly>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group row">
            <label for="cTotal" class="col-sm-4 col-form-label">Cr Total</label>
            <div class="col-sm-8">
                <input class="form-control" name="accounting_entry[cr_total]" type="text" placeholder="C Total"
                       id="cTotal"
                       value="<?php echo set_value('cr_total', $cr_total); ?>" readonly>
            </div>
        </div>
    </div>

    <div class="offset-3"></div>
</div>