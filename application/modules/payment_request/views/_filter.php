<?php if (!empty($records)) : ?>
    <div class="row" id="kt_content">
        <?php foreach ($records as $r) : ?>
            <?php
                $id = ( $r['id'] ? $r['id'] : "" );
                $reference = ( $r['reference'] ? $r['reference'] : "N/A" );
                $payable_type_id = ( $r['payable_type_id'] ? Dropdown::get_dynamic('payable_types',$r['payable_type_id'],'name','id','view') : "" );
                $origin_id = ( $r['origin_id'] ? $r['origin_id'] : "" );
                $payee_type = ( $r['payee_type'] ? $r['payee_type'] : "" );
                $payee_type_id = ( $r['payee_type_id'] ? $r['payee_type_id'] : "" );

                $person = get_person($payee_type_id,$payee_type);
                $person_name = get_fname($person);
                $paid_amount = count_paid_amount($id, 'payment_vouchers', 'payment_request_id', 'paid_amount');

                $gross_amount = ( $r['gross_amount'] ? $r['gross_amount'] : "" );
                $tax_amount = ( $r['tax_amount'] ? $r['tax_amount'] : "" );
                $tax_rate = ( $r['tax_percentage'] ? $r['tax_percentage'] : "" );
                $tax_percentage = ( $r['tax_percentage'] ? $r['tax_percentage'] : "" );
                $payment_voucher_id = ( $r['payment_voucher_id'] ? $r['payment_voucher_id'] : "" );
                $is_paid = ( $r['is_paid'] != 0 ? "Paid" : "Unpaid" );
                $is_complete = ($r['is_complete'] ? "Completed" : "Incomplete");
                $due_date = ( $r['due_date'] ? view_date($r['due_date']) : "" );
                $particulars = ( $r['particulars'] ? $r['particulars'] : "" );

                $payment_status = ( $r['is_paid'] ? "kt-badge--success" : "kt-badge--danger" );
                $status = ( $r['is_complete'] ? "kt-badge--success" : "kt-badge--danger" );

                $transaction_id = get_value_field($origin_id,'transaction_commissions','transaction_id','id');
                $this->load->model('transaction/transaction_model');
                $transaction = $this->transaction_model->with_property()->get($transaction_id);
                $property = $transaction['property']['name'];
                $property_id = $transaction['property']['id'];
            ?>
        <!--begin:: Portlet-->
        <div class="kt-portlet ">
            <div class="kt-portlet__body custom-transaction_payments">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__media kt-hidden">
                            <img src="./assets/media/project-logos/3.png" alt="image">
                        </div>
                        <div
                            class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                            <?php echo get_initials($payable_type_id); ?>
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="<?php echo base_url(); ?>payment_request/view/<?php echo $id?>" class="kt-widget__username">
                                    Reference : <?php echo $reference; ?>
                                    <i class="flaticon2-correct"></i>
                                </a>


                                <div class="kt-widget__action custom_portlet_header">

                                    <div class="kt-portlet__head kt-portlet__head--noborder" style="min-height: 0px;">
                                        <div class="kt-portlet__head-label">
                                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>" class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="kt-portlet__head-toolbar">
                                            <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                <i class="flaticon-more-1 kt-font-brand"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <ul class="kt-nav">
                                                    <?php if($is_complete === "Incomplete"): ?>
                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('payment_voucher/form/' . $id); ?>" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-add"></i>
                                                                <span class="kt-nav__link-text">Add Voucher</span>
                                                            </a>
                                                        </li>
                                                    <?php endif;?>
                                                    <li class="kt-nav__item">
                                                        <a href="<?php echo base_url('payment_request/view/' . $id); ?>" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon-eye"></i>
                                                            <span class="kt-nav__link-text">View</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="<?php echo base_url('payment_request/form/' . $id); ?>" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-pen"></i>
                                                            <span class="kt-nav__link-text">Update</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    <a href="#" class="kt-widget__username">
                                        Payable Type  : <?php echo ucwords($payable_type_id); ?>
                                    </a>
                                    <br>
                                    <a href="<?php echo base_url(); ?>property/view/<?php echo $property_id; ?>" target="_BLANK" class="kt-widget__username">
                                        Property : <?=$property;?>
                                    </a>
                                </div>
                                <div class="kt-widget__desc">
                                    <a href="#" class="kt-widget__username">
                                        Payee  : <?php echo ucwords($person_name); ?>
                                    </a>
                                    <br>
                                    <a href="<?php echo base_url(); ?><?php echo $payee_type; ?>/view/<?php echo $payee_type_id; ?>" target="_BLANK" class="kt-widget__username">
                                        Payee Type : <?php echo ucwords($payee_type); ?>
                                    </a>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__bottom">


                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Due Date</span>
                                <span class="kt-widget__value"><?php echo view_date($due_date); ?></span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Gross Amount</span>
                                <span class="kt-widget__value"><?php echo money_php($gross_amount); ?></span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Tax Rate</span>
                                <span class="kt-widget__value"><?php echo money_php($tax_amount); ?> (<?php echo $tax_rate."%"; ?>)</span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Paid Amount</span>
                                <span class="kt-widget__value"><?php echo money_php($paid_amount); ?></span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Remaining Amount</span>
                                <span class="kt-widget__value"><?php echo money_php($gross_amount - $paid_amount); ?></span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Payment Status</span>
                                <span class="kt-widget__value">
                                    <span class="kt-badge <?=$payment_status;?> kt-badge--inline kt-badge--pill kt-badge--rounded" style="color:white">
                                        <?=$is_paid;?>
                                        </span>
                                </span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Status</span>
                                <span class="kt-widget__value">
                                    <span class="kt-badge <?=$status;?> kt-badge--inline kt-badge--pill kt-badge--rounded" style="color:white">
                                        <?=$is_complete;?>
                                        </span>
                                </span>
                            </div>
                        </div>
    
                         <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Particulars</span>
                                <span class="kt-widget__value"><?php echo $particulars; ?></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php endforeach; ?>
    </div>

    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="<?php echo base_url('payment_request/form'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>