<?php

// $rows = $result['rows'];
$date = $result['date'];
$period_label = $result['period_label'];

?>
<!DOCTYPE html>
<html>

<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 15px;
		}

		.table>thead>tr>th,
		.table>tbody>tr>td,
		.table>tbody>tr>th,
		.table>tfoot>tr>td,
		.table>tfoot>tr>th {
			padding: 5px 8px
		}

		.table>thead>tr>th {
			border-bottom: 2px solid #00529C;
		}

		.table>tfoot>tr>td {
			border-top: 2px solid #00529C;
		}

		.table>tfoot {
			border-top: 2px solid #DDD;
		}

		.table>tbody {
			border-bottom: 1px solid #fff;
		}

		.table.table-striped>thead>tr>th {
			background: #00529C;
			color: #FFF;
			border: 0;
			padding: 12px 8px;
			text-transform: uppercase;
		}

		.table.table-striped>thead>tr>th a {
			color: #FFF;
			font-weight: 400
		}

		.table.table-striped>thead>tr:nth-child(2)>th {
			background: #0075de;
		}

		.table.table-striped td {
			border: 0;
			vertical-align: middle;
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			background: #FFF
		}

		.table-striped>tbody>tr:nth-of-type(even) {
			background: #F1F1F1;
		}

		.color-bluegreen {
			color: #169F98;
		}

		.color-white {
			color: #fff;
		}

		.peso_currency {
			font-family: DejaVu Sans;
		}

		.bg-bluegreen {
			background-color: #169F98;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.margin0 {
			margin: 0;
		}

		.padding10 {
			padding: 10px;
		}

		p {
			margin: 0 0 15px;
		}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>

<body>

	<div class="row">
		<div class="col-md-12">

			<div class="text-center">
				<h1 class="color-bluegreen margin0">
					<?php
					$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
					echo $company_name ? $company_name : '';
					?>
				</h1>
				<p class="margin0">Balance Sheet</p>
				<p>
					<?php echo $period_label; ?>
				</p>
			</div>
			<br />

			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<td colspan="3" align="left">Beginning Balance</td>
						<td colspan="6">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($starting_balance = $tree['checks']['starting_balance']); ?> </td>
					</tr>
					<tr>
						<td colspan="3" align="center">Cleared Transactions</td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3" align="right">Checks and Payments - <?= count($tree['checks']['result']); ?> items</td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>Type</th>
						<th>
							<div style="padding-left:30px">Date</div>
						</th>
						<th>Num</th>
						<th>Name</th>
						<th>Clr</th>
						<th class="text-right">Amount</th>
						<th class="text-right">Balance</th>
					</tr>
				</thead>

				<tbody>
					<?php $balance = 0;
					$credit = 0; ?>
					<?php foreach ($tree['checks']['result'] as $key => $entry) : ?>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>Checks</td>
							<td>
								<div style="padding-left:30px"><?= nice_date($entry->date, 'F j, Y'); ?></div>
							</td>
							<td>
								<?php
								$tmp = [];

								$tmp[] = 'Ref #: ' . $entry->ref_no;

								if (!empty($entry->or_number))
									$tmp[] = 'O.R. Number: ' . $entry->or_number;
								if (!empty($entry->invoice_number))
									$tmp[] = 'Invoice Number: ' . $entry->invoice_number;
								if (!empty($entry->po_number))
									$tmp[] = ' P.O. Number: ' . $entry->po_number;

								echo implode(' | ', $tmp);

								if (!empty($entry->narration))
									echo '<div>' . $entry->narration . '</div>';
								?>
							</td>
							<td><?php echo get_payee((array)$entry); ?></td>
							<td>&nbsp;</td>
							<!-- <td class="text-right"><?php if ($entry->debit > 0) {
															echo convert_amount_dc($entry->debit);
															$debit += $entry->debit;
														} ?></td> -->
							<td class="text-right"><?php if ($entry->credit > 0) {
														echo convert_amount_dc($entry->credit);
														$credit += $entry->credit;
													} ?></td>
							<?php $balance += ($entry->credit); ?>
							<td class="text-right"><?= convert_amount_dc($balance); ?></td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="3" align="right">Checks and Payments - <?= count($tree['checks']['result']); ?> items</td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($credit); ?></td>
						<td class="text-right"><?= convert_amount_dc($balance); ?></td>
					</tr>
				</tbody>

			</table>

			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<td colspan="3" align="center">Cleared Transactions</td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3" align="right">Deposits - <?= count($tree['deposits']['result']); ?> items</td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>Type</th>
						<th>
							<div style="padding-left:30px">Date</div>
						</th>
						<th>Num</th>
						<th>Name</th>
						<th>Clr</th>
						<th class="text-right">Amount</th>
						<th class="text-right">Balance</th>
					</tr>
				</thead>

				<tbody>
					<?php $balance = 0;
					$debit = 0; ?>
					<?php foreach ($tree['deposits']['result'] as $key => $entry) : ?>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>Deposit</td>
							<td>
								<div style="padding-left:30px"><?= nice_date($entry->date, 'F j, Y'); ?></div>
							</td>
							<td>
								<?php
								$tmp = [];

								$tmp[] = 'Ref #: ' . $entry->ref_no;

								if (!empty($entry->or_number))
									$tmp[] = 'O.R. Number: ' . $entry->or_number;
								if (!empty($entry->invoice_number))
									$tmp[] = 'Invoice Number: ' . $entry->invoice_number;
								if (!empty($entry->po_number))
									$tmp[] = ' P.O. Number: ' . $entry->po_number;

								echo implode(' | ', $tmp);

								if (!empty($entry->narration))
									echo '<div>' . $entry->narration . '</div>';
								?>
							</td>
							<td><?php echo get_payee((array)$entry); ?></td>
							<td>&nbsp;</td>
							<td class="text-right"><?php if ($entry->debit > 0) {
														echo convert_amount_dc($entry->debit);
														$debit += $entry->debit;
													} ?></td>
							<!-- <td class="text-right"><?php //if($entry->credit > 0){ echo convert_amount_dc($entry->credit); $credit += $entry->credit; } 
														?></td> -->
							<?php $balance += ($entry->debit); ?>
							<td class="text-right"><?= convert_amount_dc($balance); ?></td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="3" align="right">Total Deposits and Credits - <?= count($tree['deposits']['result']); ?> items</td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($debit); ?></td>
						<td class="text-right"><?= convert_amount_dc($balance); ?></td>
					</tr>

					<tr>
						<td colspan="3" align="center">Total Cleared Transactions</td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($cleared_balance = $debit - $credit); ?></td>
						<td class="text-right"><?= convert_amount_dc($cleared_balance = $debit - $credit); ?></td>
					</tr>
					<tr>
						<td colspan="3" align="right">Cleared Balance</td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($cleared_balance = $debit - $credit); ?></td>
						<td class="text-right"><?= convert_amount_dc($cleared_balance = $debit - $credit); ?></td>
					</tr>
				</tbody>

			</table>

			<!-- UNCLEARED -->

			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						&nbsp;
					</tr>
					<tr>
						<td colspan="3" align="center">Uncleared Transactions</td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3" align="right">Checks and Payments - <?= count($tree_2['checks']['result']); ?> items</td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>Type</th>
						<th>
							<div style="padding-left:30px">Date</div>
						</th>
						<th>Num</th>
						<th>Name</th>
						<th>Clr</th>
						<th class="text-right">Amount</th>
						<th class="text-right">Balance</th>
					</tr>
				</thead>

				<tbody>
					<?php $balance = 0;
					$credit = 0; ?>
					<?php foreach ($tree_2['checks']['result'] as $key => $entry) : ?>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>Checks</td>
							<td>
								<div style="padding-left:30px"><?= nice_date($entry->date, 'F j, Y'); ?></div>
							</td>
							<td>
								<?php
								$tmp = [];

								$tmp[] = 'Ref #: ' . $entry->ref_no;

								if (!empty($entry->or_number))
									$tmp[] = 'O.R. Number: ' . $entry->or_number;
								if (!empty($entry->invoice_number))
									$tmp[] = 'Invoice Number: ' . $entry->invoice_number;
								if (!empty($entry->po_number))
									$tmp[] = ' P.O. Number: ' . $entry->po_number;

								echo implode(' | ', $tmp);

								if (!empty($entry->narration))
									echo '<div>' . $entry->narration . '</div>';
								?>
							</td>
							<td><?php echo get_payee((array)$entry); ?></td>
							<td>&nbsp;</td>
							<!-- <td class="text-right"><?php if ($entry->debit > 0) {
															echo convert_amount_dc($entry->debit);
															$debit += $entry->debit;
														} ?></td> -->
							<td class="text-right"><?php if ($entry->credit > 0) {
														echo convert_amount_dc($entry->credit);
														$credit += $entry->credit;
													} ?></td>
							<?php $balance += ($entry->credit); ?>
							<td class="text-right"><?= convert_amount_dc($balance); ?></td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="3" align="right">Checks and Payments - <?= count($tree_2['checks']['result']); ?> items</td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($credit); ?></td>
						<td class="text-right"><?= convert_amount_dc($balance); ?></td>
					</tr>
				</tbody>

			</table>

			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<td colspan="3" align="center">Uncleared Transactions</td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3" align="right">Deposits - <?= count($tree_2['deposits']['result']); ?> items</td>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>Type</th>
						<th>
							<div style="padding-left:30px">Date</div>
						</th>
						<th>Num</th>
						<th>Name</th>
						<th>Clr</th>
						<th class="text-right">Amount</th>
						<th class="text-right">Balance</th>
					</tr>
				</thead>

				<tbody>
					<?php $balance = 0;
					$debit = 0; ?>
					<?php foreach ($tree_2['deposits']['result'] as $key => $entry) : ?>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>Deposit</td>
							<td>
								<div style="padding-left:30px"><?= nice_date($entry->date, 'F j, Y'); ?></div>
							</td>
							<td>
								<?php
								$tmp = [];

								$tmp[] = 'Ref #: ' . $entry->ref_no;

								if (!empty($entry->or_number))
									$tmp[] = 'O.R. Number: ' . $entry->or_number;
								if (!empty($entry->invoice_number))
									$tmp[] = 'Invoice Number: ' . $entry->invoice_number;
								if (!empty($entry->po_number))
									$tmp[] = ' P.O. Number: ' . $entry->po_number;

								echo implode(' | ', $tmp);

								if (!empty($entry->narration))
									echo '<div>' . $entry->narration . '</div>';
								?>
							</td>
							<td><?php echo get_payee((array)$entry); ?></td>
							<td>&nbsp;</td>
							<td class="text-right"><?php if ($entry->debit > 0) {
														echo convert_amount_dc($entry->debit);
														$debit += $entry->debit;
													} ?></td>
							<!-- <td class="text-right"><?php //if($entry->credit > 0){ echo convert_amount_dc($entry->credit); $credit += $entry->credit; } 
														?></td> -->
							<?php $balance += ($entry->debit); ?>
							<td class="text-right"><?= convert_amount_dc($balance); ?></td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="3" align="right">Total Deposits and Credits - <?= count($tree_2['deposits']['result']); ?> items</td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($debit); ?></td>
						<td class="text-right"><?= convert_amount_dc($balance); ?></td>
					</tr>

					<tr>
						<td colspan="3" align="center">Total Uncleared Transactions</td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($debit - $credit); ?></td>
						<td class="text-right"><?= convert_amount_dc($debit - $credit); ?></td>
					</tr>
					<tr>
						<td colspan="3" align="right">Uncleared Balance</td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($uncleared_balance = $debit - $credit); ?></td>
						<td class="text-right"><?= convert_amount_dc($uncleared_balance = $debit - $credit); ?></td>
					</tr>
					<tr>
						<td colspan="3" align="right">Register Balance as of <?= $date; ?></td>
						<td colspan="5">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($cleared_balance - $uncleared_balance); ?></td>
						<td class="text-right"><?= convert_amount_dc($ending_balance = $cleared_balance - $uncleared_balance); ?></td>
					</tr>
					<tr>
						<td colspan="3" align="left">Ending Balance</td>
						<td colspan="6">&nbsp;</td>
						<td class="text-right"><?= convert_amount_dc($starting_balance - $ending_balance); ?> </td>
					</tr>
				</tbody>

			</table>


			<br />
			<br />

			<table class="table">
				<tbody>
					<tr>
						<td><strong>Date Printed:</strong></td>
						<td><?= date('F j, Y'); ?></td>

						<?php if (isset($result['signatory']) && ($result['signatory'])) : ?>
							<?php foreach ($result['signatory'] as $key => $signatory) { ?>
								<td><strong><?= ucwords(str_replace('_id', '', $key)); ?> By: </strong></td>
								<td> <?= get_person_name($signatory, 'staff'); ?> </td>
							<?php } ?>
						<?php endif ?>

					</tr>
				</tbody>
			</table>


</body>

</html>