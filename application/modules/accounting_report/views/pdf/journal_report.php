<?php

$signatory = $result['signatory'];
$date = $result['date'];
$period_label = $result['period_label'];
$title = $result['title'];
$total = $result['total'];
$rows = $result['rows'];

?>


<!DOCTYPE html>
<html>

<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 15px;
		}

		.table>thead>tr>th,
		.table>tbody>tr>td,
		.table>tbody>tr>th,
		.table>tfoot>tr>td,
		.table>tfoot>tr>th {
			padding: 5px 8px
		}

		.table>thead>tr>th {
			border-bottom: 2px solid #00529C;
		}

		.table>tfoot>tr>td {
			border-top: 2px solid #00529C;
		}

		.table>tfoot {
			border-top: 2px solid #DDD;
		}

		.table>tbody {
			border-bottom: 1px solid #fff;
		}

		.table.table-striped>thead>tr>th {
			background: #00529C;
			color: #FFF;
			border: 0;
			padding: 12px 8px;
			text-transform: uppercase;
		}

		.table.table-striped>thead>tr>th a {
			color: #FFF;
			font-weight: 400
		}

		.table.table-striped>thead>tr:nth-child(2)>th {
			background: #0075de;
		}

		.table.table-striped td {
			border: 0;
			vertical-align: middle;
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			background: #FFF
		}

		.table-striped>tbody>tr:nth-of-type(even) {
			background: #F1F1F1;
		}

		.color-bluegreen {
			color: #169F98;
		}

		.color-white {
			color: #fff;
		}

		.peso_currency {
			font-family: DejaVu Sans;
		}

		.bg-bluegreen {
			background-color: #169F98;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.margin0 {
			margin: 0;
		}

		.padding10 {
			padding: 10px;
		}

		p {
			margin: 0 0 15px;
		}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>

<body>
	<div class="row">
		<div class="col-md-12">

			<div class="text-center">
				<h1 class="color-bluegreen margin0">
					<?php
					$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
					echo $company_name ? $company_name : '';
					?>
				</h1>
				<p class="margin0"><?= $title; ?></p>
				<p>
					<?php echo $period_label; ?>
				</p>
			</div>
			<br />
			<?php if (!empty($rows)) : ?>
				<table class="table table-condensed table-striped">
					<thead>
						<tr>
							<th>Date</th>
							<th>Particulars</th>
							<th>Ref#</th>
							<th class="text-right">Debit</th>
							<th class="text-right">Credit</th>
						</tr>
					</thead>
					<?php $tmp_date = ''; ?>
					<tbody>
						<?php foreach ($rows as $row) : ?>

							<?php
							$date = nice_date($row->payment_date, 'F j, Y');
							$first_row = [
								'date' => ($tmp_date != $date ? $date : ''),
								'ref_no' => str_pad($row->or_number, 6, "0", STR_PAD_LEFT)
							];

							$tmp_date = $date;
							?>
							<?php foreach ($row->items as $item) : ?>
								<tr>
									<td><?= @$first_row['date']; ?></td>
									<td>
										<div <?= ($item->dc == 'c' ? 'style="padding-left:30px"' : ''); ?>><?= $item->ledger_name;  ?></div>
									</td>
									<td><?= @$first_row['ref_no']; ?></td>
									<td class="text-right">
										<?= ($item->dc == 'd' ? money_php($item->debit) : ''); ?>
									</td>
									<td class="text-right">
										<?= ($item->dc == 'c' ? money_php($item->credit) : ''); ?>
									</td>
								</tr>
							<?php unset($first_row);
							endforeach; ?>

							<?php if (!empty($row->narration)) : ?>
								<tr>
									<td></td>
									<td>
										<div style="padding-left:60px"><?= $row->narration; ?></div>
									</td>
									<td class="text-right"></td>
									<td class="text-right"></td>
								</tr>
							<?php endif; ?>

							<?php if (!empty($row->or_number) || !empty($row->invoice_number)) : ?>
								<tr>
									<td></td>
									<td>
										<div style="padding-left:60px">
											<?php
											if (!empty($row->or_number))
												echo 'O.R. Number: ' . $row->or_number;
											else if (!empty($row->invoice_number))
												echo 'Invoice Number: ' . $row->invoice_number;
											?>
										</div>
									</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							<?php endif; ?>


							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						<?php endforeach; ?>
					</tbody>

					<tfoot>
						<tr>
							<th colspan="5"></th>
						</tr>
						<tr>
							<th colspan="3"><strong>Total</strong></th>
							<th class="text-right"><strong><?= money_php($total->debit); ?></strong></th>
							<th class="text-right"><strong><?= money_php($total->credit); ?></strong></th>
						</tr>
					</tfoot>

				</table>

			<?php else : ?>
				<div class="alert alert-warning">No Data.</div>
			<?php endif; ?>
			<br />

			<div class="row">
				<div class="col-md-8">
					<? //=$pagination['links'];
					?>
				</div>

			</div>


		</div>
	</div>



	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td><strong>Date Printed:</strong></td>
				<td><?= date('F j, Y'); ?></td>

				<?php if (isset($result['signatory']) && ($result['signatory'])) : ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?= ucwords(str_replace('_id', '', $key)); ?> By: </strong></td>
						<td> <?= get_person_name($signatory, 'staff'); ?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>