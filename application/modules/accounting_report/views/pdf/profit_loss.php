<?php
$rows = $result['rows'];
$income_ledgers = $result['income_ledgers'];
$expense_ledgers = $result['expense_ledgers'];
$other_incomes = $result['other_incomes'];
$other_expenses = $result['other_expenses'];
$months = $result['months'];
$company = $result['company'];
$period_label = $result['period_label'];
?>
<!DOCTYPE html>
<html>

<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 15px;
		}

		.table>thead>tr>th,
		.table>tbody>tr>td,
		.table>tbody>tr>th,
		.table>tfoot>tr>td,
		.table>tfoot>tr>th {
			padding: 5px 8px
		}

		.table>thead>tr>th {
			border-bottom: 2px solid #00529C;
		}

		.table>tfoot>tr>td {
			border-top: 2px solid #00529C;
		}

		.table>tfoot {
			border-top: 2px solid #DDD;
		}

		.table>tbody {
			border-bottom: 1px solid #fff;
		}

		.table.table-striped>thead>tr>th {
			background: #00529C;
			color: #FFF;
			border: 0;
			padding: 12px 8px;
			text-transform: uppercase;
		}

		.table.table-striped>thead>tr>th a {
			color: #FFF;
			font-weight: 400
		}

		.table.table-striped>thead>tr:nth-child(2)>th {
			background: #0075de;
		}

		.table.table-striped td {
			border: 0;
			vertical-align: middle;
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			background: #FFF
		}

		.table-striped>tbody>tr:nth-of-type(even) {
			background: #F1F1F1;
		}

		.color-bluegreen {
			color: #169F98;
		}

		.color-white {
			color: #fff;
		}

		.peso_currency {
			font-family: DejaVu Sans;
		}

		.bg-bluegreen {
			background-color: #169F98;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.margin0 {
			margin: 0;
		}

		.padding10 {
			padding: 10px;
		}

		p {
			margin: 0 0 15px;
		}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>

<body>

	<div class="row">
		<div class="col-md-12">

			<div class="text-center">
				<h1 class="color-bluegreen margin0">
					<?php
					$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
						$project_name = get_value_field($result['filters']['project_id'], 'projects', 'name');
						echo $company_name ? $company_name : '';
						echo $project_name ? " - ".$project_name : '';
					?>
				</h1>
				<p class="margin0">Profit and Loss</p>
				<p>
					<?php echo $period_label; ?>
				</p>
			</div>
			<br />

			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<?php if (!empty($months)) : ?>
							<?php foreach ($months as $key => $month) : ?>
								<th><?php echo date('F', strtotime($month . '-01')); ?></th>
							<?php endforeach; ?>
						<?php endif; ?>
						<th>Total</td>
					</tr>
				</thead>
				<tbody>
					<?php $colspan = count($months) + 1; ?>
					<tr>
						<td colspan="<?php echo $colspan; ?>"><strong>INCOME</strong></td>
						<td>&nbsp;</td>
					</tr>

					<?php foreach ($income_ledgers as $row) : ?>
						<tr><?php $total_ii = 0; ?>
							<td>
								<div class="ledger_name"><?php echo $row['name']; ?></div>
							</td>
							<?php foreach ($months as $month) : $total_ii = @$row['months'][$month]->amount + $total_ii; ?>
								<td><?php echo money_php((!isset($row['months'][$month])) ? 0 : $row['months'][$month]->amount); ?></td>
							<?php endforeach; ?>
							<td><?= money_php($total_ii); ?></td>
						</tr>
					<?php endforeach; ?>

					<tr><?php $total_i = 0; ?>
						<td><strong>TOTAL INCOME</strong></td>
						<?php foreach ($months as $month) : $total_lcs = @$row['months'][$month]->amount + $total_i; ?>
							<td><strong><?php echo money_php($rows[$month]['gross_income']); ?></strong></td>
						<?php endforeach; ?>
						<td><?= money_php($total_i); ?></td>
					</tr>

					<tr>
						<td colspan="<?php echo $colspan; ?>"><strong>Less Cost of Sales<strong></td>
						<td>&nbsp;</td>
					</tr>

					<?php foreach ($expense_ledgers as $row) : ?>
						<tr><?php $total_lcs = 0; ?>
							<td>
								<div class="ledger_name"><?php echo $row['name']; ?></div>
							</td>
							<?php foreach ($months as $month) : $total_lcs = @$row['months'][$month]->amount + $total_lcs; ?>
								<td><?php echo money_php((!isset($row['months'][$month])) ? 0 : $row['months'][$month]->amount); ?></td>
							<?php endforeach; ?>
							<td><?= money_php($total_lcs); ?></td>
						</tr>
					<?php endforeach; ?>

					<tr><?php $total_cos = 0; ?>
						<td><strong>TOTAL COST OF SALES</strong></td>
						<?php foreach ($months as $month) : $total_cos = @$rows[$month]['gross_expense'] + $total_cos; ?>
							<td><strong><?php echo money_php($rows[$month]['gross_expense']); ?></strong></td>
						<?php endforeach; ?>
						<td><?= money_php($total_cos); ?></td>
					</tr>

					<tr><?php $total_gp = 0; ?>
						<td><strong>Gross Profit</strong></td>
						<?php foreach ($months as $month) : $total_gp = @$rows[$month]['gross_profit'] + $total_gp; ?>
							<td><strong><?php echo money_php($rows[$month]['gross_profit']); ?></strong></td>
						<?php endforeach; ?>
						<td><?= money_php($total_gp); ?></td>
					</tr>

					<tr>
						<td colspan="<?php echo $colspan; ?>"></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="<?php echo $colspan; ?>">
							<strong>Less Operating Expense<strong>
						</td>
						<td>&nbsp;</td>
					</tr>

					<?php foreach ($other_expenses as $row) : ?>
						<tr><?php $total_loe = 0; ?>
							<td>
								<div class="ledger_name"><?php echo $row['name']; ?></div>
							</td>
							<?php foreach ($months as $month) :
								// $d_amt = (isset($row['months'][$month]->debit) ? $row['months'][$month]->debit: 0);
								// $c_amt = (isset($row['months'][$month]->credit) ? $row['months'][$month]->credit : 0);
								// $total_loe = ($d_amt - $c_amt) + $total_loe; 
								$total_loe = @$row['months'][$month]->amount + $total_loe;
							?>
								<td>
									<?php echo money_php((!isset($row['months'][$month])) ? 0 : $row['months'][$month]->amount); ?>
								</td>
							<?php endforeach; ?>
							<td><?= money_php($total_loe); ?></td>
						</tr>
					<?php endforeach; ?>

					<tr>
						<td colspan="<?php echo $colspan; ?>">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr><?php $total_np = 0; ?>
						<td><strong>Net Profit</strong></td>
						<?php foreach ($months as $month) : $total_np = $rows[$month]['net_profit'] + $total_np; ?>
							<td><strong><?php echo money_php($rows[$month]['net_profit']); ?></strong></td>
						<?php endforeach; ?>
						<td><?= money_php($total_np); ?></td>
					</tr>

					<tr>
						<td colspan="<?php echo $colspan; ?>">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td colspan="<?php echo $colspan; ?>"><strong>Plus Other Income<strong></td>
						<td>&nbsp;</td>
					</tr>

					<?php $total_poi = 0; ?>
					<?php foreach ($other_incomes as $row) : ?>
						<tr>
							<td>
								<div class="ledger_name"><?php echo $row['name']; ?></div>
							</td>
							<?php foreach ($months as $month) : $total_poi = @$row['months'][$month]->amount + $total_poi; ?>
								<td><?php echo money_php((!isset($row['months'][$month])) ? 0 : $row['months'][$month]->amount); ?></td>
							<?php endforeach; ?>
							<td> <?= money_php($total_poi); ?></td>

						</tr>
					<?php endforeach; ?>

					<tr><?php $total_toi = 0; ?>
						<td><strong>TOTAL OTHER INCOME</strong></td>
						<?php foreach ($months as $month) : $total_toi = $rows[$month]['other_income'] + $total_toi;  ?>
							<td><strong><?php echo money_php($rows[$month]['other_income']); ?></strong></td>
						<?php endforeach; ?>
						<td> <?= money_php($total_toi); ?></td>

					</tr>
					<tr>
						<td colspan="<?php echo $colspan; ?>">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr><?php $total_tt = 0; ?>
						<td><strong>Income By Tax</strong></td>
						<?php foreach ($months as $month) : $total_tt = $rows[$month]['income_by_tax'] + $total_tt; ?>
							<td><strong><?php echo money_php($rows[$month]['income_by_tax']); ?></strong></td>
						<?php endforeach; ?>
						<td> <?= money_php($total_tt); ?></td>
					</tr>
				</tbody>
			</table>



		</div>
	</div>

	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td><strong>Date Printed:</strong></td>
				<td><?= date('F j, Y'); ?></td>

				<?php if (isset($result['signatory']) && ($result['signatory'])) : ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?= ucwords(str_replace('_id', '', $key)); ?> By: </strong></td>
						<td> <?= get_person_name($signatory, 'staff'); ?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>

	<style type="text/css">
		.ledger_name {
			padding-left: 15px;
			font-weight: 600;
		}
	</style>

</body>

</html>