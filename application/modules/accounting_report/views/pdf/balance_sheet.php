<?php

$net_income = $result['net_income'];
$assets = $result['assets'];
$liabilities = $result['liabilities'];
$equities = $result['equities'];
$retained = $result['retained'];
$date_to = $result['date_to'];
$period_label = 'As of ' . date('t F Y', strtotime($date_to));

$total = ['cash_banks' => 0, 'current_fix' => 0, 'liabilities' => 0, 'retained_earnings' => 0, 'other_equity' => 0, 'equity' => 0, 'ni_cash_banks' => 0, 'ni_current_fix' => 0, 'ni_liabilities' => 0, 'ni_other_equity' => 0, 'net_income' => 0, 'pre_cash_banks' => 0, 'pre_current_fix' => 0, 'pre_liabilities' => 0, 'pre_other_equity' => 0, 'previous_retained_earning' => 0];

?>


<?php foreach ($net_income['assets']['cash_banks'] as $row) : $total['ni_cash_banks'] += $row->total;
endforeach; ?>
<?php foreach ($net_income['assets']['current_fix'] as $row) : $total['ni_current_fix'] += $row->total;
endforeach; ?>
<?php foreach ($net_income['liabilities'] as $row) : $total['ni_liabilities'] += $row->total;
endforeach; ?>
<?php foreach ($net_income['equities'] as $row) : $total['ni_other_equity'] += $row->total;
endforeach; ?>

<?php foreach ($retained['assets']['cash_banks'] as $row) : $total['pre_cash_banks'] += $row->total;
endforeach; ?>
<?php foreach ($retained['assets']['current_fix'] as $row) : $total['pre_current_fix'] += $row->total;
endforeach; ?>
<?php foreach ($retained['liabilities'] as $row) : $total['pre_liabilities'] += $row->total;
endforeach; ?>
<?php foreach ($retained['equities'] as $row) : $total['pre_other_equity'] += $row->total;
endforeach; ?>

<!DOCTYPE html>
<html>

<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 15px;
		}

		.table>thead>tr>th,
		.table>tbody>tr>td,
		.table>tbody>tr>th,
		.table>tfoot>tr>td,
		.table>tfoot>tr>th {
			padding: 5px 8px
		}

		.table>thead>tr>th {
			border-bottom: 2px solid #00529C;
		}

		.table>tfoot>tr>td {
			border-top: 2px solid #00529C;
		}

		.table>tfoot {
			border-top: 2px solid #DDD;
		}

		.table>tbody {
			border-bottom: 1px solid #fff;
		}

		.table.table-striped>thead>tr>th {
			background: #00529C;
			color: #FFF;
			border: 0;
			padding: 12px 8px;
			text-transform: uppercase;
		}

		.table.table-striped>thead>tr>th a {
			color: #FFF;
			font-weight: 400
		}

		.table.table-striped>thead>tr:nth-child(2)>th {
			background: #0075de;
		}

		.table.table-striped td {
			border: 0;
			vertical-align: middle;
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			background: #FFF
		}

		.table-striped>tbody>tr:nth-of-type(even) {
			background: #F1F1F1;
		}

		.color-bluegreen {
			color: #169F98;
		}

		.color-white {
			color: #fff;
		}

		.peso_currency {
			font-family: DejaVu Sans;
		}

		.bg-bluegreen {
			background-color: #169F98;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.margin0 {
			margin: 0;
		}

		.padding10 {
			padding: 10px;
		}

		p {
			margin: 0 0 15px;
		}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>

<body>

	<div class="row">
		<div class="col-md-12">

			<div class="text-center">
				<h1 class="color-bluegreen margin0">
					<?php
						$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
						$project_name = get_value_field($result['filters']['project_id'], 'projects', 'name');
						echo $company_name ? $company_name : '';
						echo $project_name ? " - ".$project_name : '';
					?>
				</h1>
				<p class="margin0">Balance Sheet</p>
				<p>
					<?php echo $period_label; ?>
				</p>
			</div>
			<br />
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-left">Accounts</th>
						<th class="text-right"><?php echo date('F j, Y', strtotime($date_to)); ?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th style="background:#5EBC44;color:#FFF;border-color:transparent" colspan="2"><strong>ASSETS</strong></th>
					</tr>
					<tr>
						<td colspan="2"><strong>Cash and Bank</strong></td>
					</tr>

					<?php foreach ($assets['cash_banks'] as $row) : $total['cash_banks'] += $row->total; ?>
						<tr>
							<td style="padding-left: 20px"><?php echo $row->ledger_name ?></td>
							<td class="text-right"><?php echo convert_amount_dc($row->total) ?></td>
						</tr>
					<?php endforeach; ?>

					<tr>
						<td><strong>Total Cash and Bank</strong></td>
						<td class="text-right"><?php echo convert_amount_dc($total['cash_banks']); ?></td>
					</tr>

					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>

					<tr>
						<td colspan="2"><strong>Current and Fixed Assets</strong></td>
					</tr>

					<?php foreach ($assets['current_fix'] as $row) : $total['current_fix'] += $row->total; ?>
						<tr>
							<td style="padding-left: 20px"><?php echo $row->ledger_name ?></td>
							<td class="text-right"><?php echo convert_amount_dc($row->total) ?></td>
						</tr>
					<?php endforeach; ?>

					<tr>
						<td><strong>Total Current and Fixed Assets</strong></td>
						<td class="text-right"><strong><?php echo convert_amount_dc($total['current_fix']); ?></strong></td>
					</tr>


					<tr>
						<td><strong>Total Assets</strong></td>
						<td class="text-right"><strong><?php echo convert_amount_dc($total['cash_banks'] + $total['current_fix']); ?></strong></td>
					</tr>


					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" style="background:#5EBC44;color:#FFF;border-color:transparent">
							<strong>LIABILITIES</strong>
						</td>
					</tr>
					<tr>
						<td colspan="2"><strong>Current Liabilities</strong></td>
					</tr>
					<?php foreach ($liabilities as $row) : $total['liabilities'] += $row->total; ?>
						<tr>
							<td style="padding-left: 20px;">
								<?php echo $row->ledger_name ?>
							</td>
							<td class="text-right"><?php echo convert_amount_dc($row->total); ?></td>
						</tr>
					<?php endforeach; ?>


					<tr>
						<td><strong>Total Liabilities</strong></td>
						<td class="text-right"><strong><?php echo convert_amount_dc($total['liabilities']); ?></strong></td>
					</tr>

					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" style="background:#5EBC44;color:#FFF;border-color:transparent">
							<strong>Equities</strong>
						</td>
					</tr>
					<!-- <tr><td colspan="2"><strong>Earnings</strong></td></tr> -->
					<tr>
						<td colspan="2">
							<strong>Other Equity</strong>
						</td>
					</tr>
					<?php foreach ($equities as $row) : $total['other_equity'] += $row->total; ?>
						<tr>
							<td><?php echo $row->ledger_name ?></td>
							<td class="text-right"><?php echo convert_amount_dc($row->total); ?></td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td>
							<strong>Total Other Equity</strong>
						</td>
						<td class="text-right">
							<?php echo convert_amount_dc($total['other_equity']) ?>
						</td>
					</tr>

					<tr>
						<td>
							<strong>Share Capital</strong>
						</td>
						<td class="text-right">
							&nbsp;
						</td>
					</tr>

					<tr>
						<td>
							<strong>Net Income</strong>
						</td>
						<td class="text-right" data-cb="<?= $total['ni_cash_banks']; ?>" data-cf="<?= $total['ni_current_fix']; ?>" data-l="<?= $total['ni_liabilities']; ?>" data-oe="<?= $total['ni_other_equity']; ?>">
							<?php
							$total['net_income'] = ($total['ni_cash_banks'] + $total['ni_current_fix']) - ($total['ni_liabilities'] + $total['ni_other_equity']);
							echo convert_amount_dc($total['net_income']);
							?>
						</td>
					</tr>


					<tr>
						<td>
							<strong>Retained Earnings</strong>
						</td>
						<td class="text-right" data-pre-cash-banks="<?=$total['pre_cash_banks'];?>" data-pre-current-fix="<?=$total['pre_current_fix'];?>" data-pre-liabilities="<?=$total['pre_liabilities'];?>" data-pre-other-equity="<?=$total['pre_other_equity'];?>">
							<?php
							// $total['retained_earnings'] = ($total['cash_banks'] + $total['current_fix']) - abs($total['liabilities']) - abs($total['other_equity']);

							$total['previous_retained_earning'] = ($total['pre_cash_banks'] + $total['pre_current_fix']) - abs($total['pre_liabilities']) - abs($total['pre_other_equity']);

							// $total['retained_earnings'] = "-3905667.82";
							// $total['previous_retained_earning'] = "0";

							echo convert_amount_dc($total['previous_retained_earning']);
							?>
						</td>
					</tr>


					<tr>
						<td><strong>Total Equity</strong></td>

						<?php $total['equity'] = $total['other_equity'] + $total['previous_retained_earning'] + $total['net_income']; ?>

						<td class="text-right" data-other-equity="<?= $total['other_equity']; ?>" data-retained="<?= $total['previous_retained_earning']; ?>" data-net="<?= $total['net_income']; ?>">

							<?php echo convert_amount_dc($total['equity']); ?>
						</td>
					</tr>

					<tr>
						<td><strong>Total Liabilities & Equities</strong></td>
						<td class="text-right" data-liability="<?= $total['liabilities']; ?>" data-equity="<?= $total['equity']; ?>">
							<?php echo convert_amount_dc($total['liabilities'] + $total['equity']); ?>
						</td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>

	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td><strong>Date Printed:</strong></td>
				<td><?= date('F j, Y'); ?></td>

				<?php if (isset($result['signatory']) && ($result['signatory'])) : ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?= ucwords(str_replace('_id', '', $key)); ?> By: </strong></td>
						<td> <?= get_person_name($signatory, 'staff'); ?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>

</html>