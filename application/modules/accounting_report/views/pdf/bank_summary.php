<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

	</style>
</head>
<body>
<body>

	<div class="text-center">
			<h1 class="color-bluegreen margin0">
				<?php 
					$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
					echo $company_name ? $company_name : '';
				?>
			</h1>
			<p class="margin0"><?=$title;?></p>
			<p>
				<?php echo $period_label; ?>
			</p>
		</div>

	<?php $total = ['op_balance' => 0, 'credit' => 0, 'debit' => 0, 'diff' => 0]; ?>
	<table class="table table-striped">
		<thead>
			<tr>
				<th class="text-left">Bank Accounts</th>
				<th class="text-right">Opening Balance</th>
				<th class="text-right">Cash Received</th>
				<th class="text-right">Cash Spent</th>
				<th class="text-right">Closing Balance</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($rows as $row): ?>
				<?php
					$row->diff =  $row->debit - $row->credit;
					foreach($total as $key => $trow)
					{

						$total[$key] += $row->{$key};
					}
				?>
				<tr>
					<td><?=$row->ledger_name; ?></td>
					<td class="text-right"><?=convert_amount_dc_2($row->op_balance, 'PHP'); ?></td>
					<td class="text-right"><?=convert_amount_dc_2($row->debit, 'PHP'); ?></td>
					<td class="text-right"><?=convert_amount_dc_2($row->credit, 'PHP'); ?></td>
					<td class="text-right"><?=convert_amount_dc_2($row->diff, 'PHP'); ?></td>
				</tr>
			<?php endforeach; ?>

			<?php if(empty($rows)): ?>
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
			<?php endif; ?>
			
		</tbody>
		<tfoot>
			<tr>
				<td><strong>Total</strong></td>
				<td class="text-right"><strong><?=convert_amount_dc_2($total['op_balance'], 'PHP'); ?></strong></td>
				<td class="text-right"><strong><?=convert_amount_dc_2($total['debit'], 'PHP'); ?></strong></td>
				<td class="text-right"><strong><?=convert_amount_dc_2($total['credit'], 'PHP'); ?></strong></td>
				<td class="text-right"><strong><?=convert_amount_dc_2($total['diff'], 'PHP'); ?></strong></td>
			</tr>
		</tfoot>
	</table>
	<br />
	<br />
	<table class="table">
		<tbody>
			<tr>
				<td style="width: 100px;"><strong>Date Printed:</strong></td>
				<td><?=date('F j, Y'); ?></td>
			</tr>
			<tr>
				<td style="width: 100px;"><strong>Printed By:</strong></td>
				<td><?=$this->account['name']; ?></td>
			</tr>
		</tbody>
	</table>
</body>
</html>