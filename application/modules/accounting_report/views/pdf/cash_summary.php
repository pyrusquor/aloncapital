<?php

$signatory = $result['signatory'];
$date = $result['date'];
$period_label = $result['period_label'];
$tree = @$result['tree'];
$title = $result['title'];
$year = $result['year'];
$month = $result['month'];
$period_label = $result['period_label'];
$incomes = $result['incomes'];

?>


<!DOCTYPE html>
<html>

<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 15px;
		}

		.table>thead>tr>th,
		.table>tbody>tr>td,
		.table>tbody>tr>th,
		.table>tfoot>tr>td,
		.table>tfoot>tr>th {
			padding: 5px 8px
		}

		.table>thead>tr>th {
			border-bottom: 2px solid #00529C;
		}

		.table>tfoot>tr>td {
			border-top: 2px solid #00529C;
		}

		.table>tfoot {
			border-top: 2px solid #DDD;
		}

		.table>tbody {
			border-bottom: 1px solid #fff;
		}

		.table.table-striped>thead>tr>th {
			background: #00529C;
			color: #FFF;
			border: 0;
			padding: 12px 8px;
			text-transform: uppercase;
		}

		.table.table-striped>thead>tr>th a {
			color: #FFF;
			font-weight: 400
		}

		.table.table-striped>thead>tr:nth-child(2)>th {
			background: #0075de;
		}

		.table.table-striped td {
			border: 0;
			vertical-align: middle;
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			background: #FFF
		}

		.table-striped>tbody>tr:nth-of-type(even) {
			background: #F1F1F1;
		}

		.color-bluegreen {
			color: #169F98;
		}

		.color-white {
			color: #fff;
		}

		.peso_currency {
			font-family: DejaVu Sans;
		}

		.bg-bluegreen {
			background-color: #169F98;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.margin0 {
			margin: 0;
		}

		.padding10 {
			padding: 10px;
		}

		p {
			margin: 0 0 15px;
		}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>

<body>
	<div class="row">
		<div class="col-md-12">

			<div class="text-center">
				<h1 class="color-bluegreen margin0">
					<?php
					$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
					echo $company_name ? $company_name : '';
					?>
				</h1>
				<p class="margin0"><?= $title; ?></p>
				<p>
					<?php echo $period_label; ?>
				</p>
			</div>
			<br />

			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th class="text-right"><?= nice_date($year . '-' . $month . '-01', 'F Y'); ?></th>
						<th class="text-right">Monthly Avg <i class="glyphicon glyphicon-info-sign" data-toggle="tooltip" title="Average for the past 4 months"></i></th>
					</tr>
				</thead>
				<tbody>
					<tr style="background:#0075de;color:#FFF;border-color:transparent">
						<td colspan="3"><strong>INCOME</strong></td>
					</tr>
					<?php
					$total_income = 0;
					$total_expenses = 0;
					$total_operating_surplus = 0;
					$total_non_operating_movements = 0;
					$total_net_cash_movement = 0;

					$income_monthly_avg = 0;
					$operating_expenses_monthly_avg = 0;
					$operating_movement_monthly_avg = 0;
					$total_operating_surplus_monthly_avg = 0;
					?>

					<?php foreach ($incomes['monthly_avg'] as $ledger_id => $row) : ?>
						<?php

						$total_income += $incomes['month'][$ledger_id]->total;
						$income_monthly_avg += $monthly_avg = ($row->total / 4);

						if ($incomes['month'][$ledger_id]->total == 0 && $monthly_avg == 0)
							continue; // skip 0 0
						?>
						<tr>
							<td><?= $row->name; ?></td>
							<td class="text-right"><?= convert_amount_dc_2($incomes['month'][$ledger_id]->total, false); ?></td>
							<td class="text-right"><?= convert_amount_dc_2($monthly_avg, false); ?></td>
						</tr>
					<?php endforeach ?>

					<tr>
						<th><strong>TOTAL INCOME</strong></th>
						<th class="text-right"><strong><?= convert_amount_dc_2($total_income, false); ?></strong></th>
						<th class="text-right"><strong><?= convert_amount_dc_2($income_monthly_avg, false); ?></strong></th>
					</tr>

					<tr>
						<td colspan="10">&nbsp;</td>
					</tr>
					<tr style="background:#0075de;color:#FFF;border-color:transparent;">
						<td colspan="10"><strong>LESS OF OPERATING EXPENSES</strong></td>
					</tr>


					<?php foreach ($operating_expenses['monthly_avg'] as $ledger_id => $row) : ?>
						<?php
						$total_expenses += $operating_expenses['month'][$ledger_id]->total;
						$operating_expenses_monthly_avg += $monthly_avg = ($row->total / 4);

						if ($operating_expenses['month'][$ledger_id]->total == 0 && $monthly_avg == 0)
							continue;
						?>
						<tr>
							<td><?= $row->name; ?></td>
							<td class="text-right"><?= convert_amount_dc_2($operating_expenses['month'][$ledger_id]->total, false); ?></td>
							<td class="text-right"><?= convert_amount_dc_2($monthly_avg, false); ?></td>
						</tr>
					<?php endforeach ?>


					<tr>
						<th><strong>Total Less Operating Expenses</strong></th>
						<th class="text-right"><strong><?= convert_amount_dc_2($total_expenses, false); ?></strong></th>
						<th class="text-right"><strong><?= convert_amount_dc_2($operating_expenses_monthly_avg, false); ?></strong></th>
					</tr>

					<?php
					$total_operating_surplus = ($total_income - $total_expenses);
					$total_operating_surplus_monthly_avg = ($income_monthly_avg - $operating_expenses_monthly_avg);
					?>

					<tr>
						<td><strong>Operating Surplus (Deficit)</strong></td>
						<td class="text-right"><strong><?= convert_amount_dc_2($total_operating_surplus, false) ?></strong></td>
						<td class="text-right"><strong><?= convert_amount_dc_2($total_operating_surplus_monthly_avg, false) ?></strong></td>
					</tr>
					<tr>
						<td colspan="10">&nbsp;</td>
					</tr>
					<tr style="background:#0075de;color:#FFF;border-color:transparent;">
						<td colspan="10"><strong>Plus Non Operating Movements</strong></td>
					</tr>


					<?php foreach ($operating_movements['monthly_avg'] as $ledger_id => $row) : ?>
						<?php
						$total_non_operating_movements += $operating_movements['month'][$ledger_id]->total;
						$operating_movement_monthly_avg += $monthly_avg = ($row->total / 4);

						if ($operating_movements['month'][$ledger_id]->total == 0 && $monthly_avg == 0)
							continue;
						?>
						<tr>
							<td><?= $row->name; ?></td>
							<td class="text-right"><?= convert_amount_dc_2($operating_movements['month'][$ledger_id]->total, false); ?></td>
							<td class="text-right"><?= convert_amount_dc_2($monthly_avg, false); ?></td>
						</tr>
					<?php endforeach ?>


					<tr>
						<th><strong>Total Plus Non Operating Movements</strong></th>
						<th class="text-right"><strong><?= convert_amount_dc_2($total_non_operating_movements, false); ?></strong></th>
						<th class="text-right"><strong><?= convert_amount_dc_2($operating_movement_monthly_avg, false); ?></strong></th>
					</tr>

					<tr>
						<td colspan="10">&nbsp;</td>
					</tr>

					<?php
					$total_net_cash_movement = ($total_operating_surplus + $total_non_operating_movements);
					$total_net_cash_movement_monthly_avg = ($total_operating_surplus_monthly_avg + $operating_movement_monthly_avg);
					?>

					<tr>
						<th><strong>Net Cash Movement</strong></th>
						<th class="text-right"><strong><?= convert_amount_dc_2($total_net_cash_movement, false) ?></strong></th>
						<th class="text-right"><strong><?= convert_amount_dc_2($total_net_cash_movement_monthly_avg, false) ?></strong></th>
					</tr>

				</tbody>
			</table>

			<br />

		</div>
	</div>



	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td><strong>Date Printed:</strong></td>
				<td><?= date('F j, Y'); ?></td>

				<?php if (isset($result['signatory']) && ($result['signatory'])) : ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?= ucwords(str_replace('_id', '', $key)); ?> By: </strong></td>
						<td> <?= get_person_name($signatory, 'staff'); ?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>