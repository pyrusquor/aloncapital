<?php

$signatory = $result['signatory'];
$date = $result['date'];
$period_label = $result['period_label'];
$tree = $result['tree'];
$title = $result['title'];

?>


<!DOCTYPE html>
<html>

<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 15px;
		}

		.table>thead>tr>th,
		.table>tbody>tr>td,
		.table>tbody>tr>th,
		.table>tfoot>tr>td,
		.table>tfoot>tr>th {
			padding: 5px 8px
		}

		.table>thead>tr>th {
			border-bottom: 2px solid #00529C;
		}

		.table>tfoot>tr>td {
			border-top: 2px solid #00529C;
		}

		.table>tfoot {
			border-top: 2px solid #DDD;
		}

		.table>tbody {
			border-bottom: 1px solid #fff;
		}

		.table.table-striped>thead>tr>th {
			background: #00529C;
			color: #FFF;
			border: 0;
			padding: 12px 8px;
			text-transform: uppercase;
		}

		.table.table-striped>thead>tr>th a {
			color: #FFF;
			font-weight: 400
		}

		.table.table-striped>thead>tr:nth-child(2)>th {
			background: #0075de;
		}

		.table.table-striped td {
			border: 0;
			vertical-align: middle;
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			background: #FFF
		}

		.table-striped>tbody>tr:nth-of-type(even) {
			background: #F1F1F1;
		}

		.color-bluegreen {
			color: #169F98;
		}

		.color-white {
			color: #fff;
		}

		.peso_currency {
			font-family: DejaVu Sans;
		}

		.bg-bluegreen {
			background-color: #169F98;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.margin0 {
			margin: 0;
		}

		.padding10 {
			padding: 10px;
		}

		p {
			margin: 0 0 15px;
		}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>

<body>
	<div class="row">
		<div class="col-md-12">

			<div class="text-center">
				<h1 class="color-bluegreen margin0">
					<?php
					$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
					echo $company_name ? $company_name : '';
					?>
				</h1>
				<p class="margin0"><?= $title; ?></p>
				<p>
					<?php echo $period_label; ?>
				</p>
			</div>
			<br />

			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>
							<div style="padding-left:30px">Date</div>
						</th>
						<th>Particulars</th>
						<th>Payee</th>
						<th class="text-right">Debit</th>
						<th class="text-right">Credit</th>
						<th class="text-right">Balance</th>
					</tr>
				</thead>
				<?php foreach ($tree as $accounts) : ?>
					<tbody>
						<tr style="background:#0075de;color:#FFF">
							<td colspan="6"><strong><?= $accounts['name']; ?></strong></td>
						</tr>

						<?php foreach ($accounts['groups'] as $groups) : ?>
							<tr>
								<td colspan="6"><strong><?= $groups['name']; ?></strong></td>
							</tr>
							<?php $debit = $credit = $balance = 0; ?>
							<?php if (!empty($groups['starting'])) : ?>
								<tr>
									<td>
										<div style="padding-left:30px">Starting Balance</div>
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="text-right"><?= convert_amount_dc($groups['starting']);
															$balance += $groups['starting']; ?></td>
								</tr>
							<?php endif; ?>

							<?php foreach ($groups['entries'] as $entry) :
								$project_id = get_value_field($entry->accounting_entry_id, 'projects', 'name');
								$property_id = get_value_field($entry->accounting_entry_id, 'properties', 'name');
							?>
								<tr>
									<td>
										<div style="padding-left:30px"><?= view_date($entry->payment_date); ?></div>
									</td>
									<td>
										<?php
										$tmp = [];

										$tmp[] = 'Ref #: ' . $entry->ref_no;

										if (!empty($entry->or_number))
											$tmp[] = 'O.R. Number: ' . $entry->or_number;
										if (!empty($entry->invoice_number))
											$tmp[] = 'Invoice Number: ' . $entry->invoice_number;
										if (!empty($entry->po_number))
											$tmp[] = ' P.O. Number: ' . $entry->po_number;
										if (!empty($project_id))
											$tmp[] = ' Project Name: ' . get_value_field($project_id, 'projects', 'name');
										if (!empty($property_id))
											$tmp[] = ' Property Name: ' . get_value_field($property_id, 'properties', 'name');

										$tmp[] = ' Ledger Name: ' . get_value_field($entry->ledger_id, 'accounting_ledgers', 'name');

										echo implode(' | ', $tmp);

										if (!empty($entry->narration))
											echo '<div>' . $entry->narration . '</div>';
										?>
									</td>
									<td>
										<?php
										$r = get_payee((array)$entry);
										echo ($entry->payee_type_id ? @$r['name'] . " " . @$r['first_name'] . " " . @$r['last_name'] : 'N/A')
										?>
									</td>
									<td class="text-right"><?php if ($entry->debit > 0) {
																echo convert_amount_dc($entry->debit);
																$debit += $entry->debit;
															} ?></td>
									<td class="text-right"><?php if ($entry->credit > 0) {
																echo convert_amount_dc($entry->credit);
																$credit += $entry->credit;
															} ?></td>
									<?php $balance += ($entry->debit - $entry->credit); ?>
									<td class="text-right"><?= convert_amount_dc($balance); ?></td>
								</tr>
							<?php endforeach; ?>

							<tr>
								<td colspan="3">
									<div style="padding-left:30px"><strong>Totals and Ending Balance</strong></div>
								</td>

								<td class="text-right"><strong><?= convert_amount_dc($debit); ?></strong></td>
								<td class="text-right"><strong><?= convert_amount_dc($credit); ?></strong></td>
								<td class="text-right"><strong><?= convert_amount_dc($balance); ?></strong></td>
							</tr>

							<tr>
								<td colspan="5">
									<div style="padding-left:30px">
										<strong>Balance Change</strong>
										<div class="color-gray"><small>Difference between starting and ending balances</small></div>
									</div>
								</td>
								<td class="text-right"><strong><?= convert_amount_dc($balance); ?></strong></td>
							</tr>

							<tr>
								<td colspan="6">&nbsp;</td>
							</tr>

						<?php endforeach; ?>

					</tbody>
				<?php endforeach; ?>
				<?php if (empty($tree)) : ?>
					<tbody>
						<tr>
							<td colspan="6" class="text-center">
								No Data
							</td>
						</tr>
					</tbody>
				<?php endif; ?>

			</table>


		</div>
	</div>


	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td><strong>Date Printed:</strong></td>
				<td><?= date('F j, Y'); ?></td>

				<?php if (isset($result['signatory']) && ($result['signatory'])) : ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?= ucwords(str_replace('_id', '', $key)); ?> By: </strong></td>
						<td> <?= get_person_name($signatory, 'staff'); ?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>