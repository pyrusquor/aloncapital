<?php

$signatory = $result['signatory'];
$date = $result['date'];
$period_label = $result['period_label'];
$title = $result['title'];
$rows = $result['rows'];
$ledger = $result['ledger'];

?>


<!DOCTYPE html>
<html>

<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 15px;
		}

		.table>thead>tr>th,
		.table>tbody>tr>td,
		.table>tbody>tr>th,
		.table>tfoot>tr>td,
		.table>tfoot>tr>th {
			padding: 5px 8px
		}

		.table>thead>tr>th {
			border-bottom: 2px solid #00529C;
		}

		.table>tfoot>tr>td {
			border-top: 2px solid #00529C;
		}

		.table>tfoot {
			border-top: 2px solid #DDD;
		}

		.table>tbody {
			border-bottom: 1px solid #fff;
		}

		.table.table-striped>thead>tr>th {
			background: #00529C;
			color: #FFF;
			border: 0;
			padding: 12px 8px;
			text-transform: uppercase;
		}

		.table.table-striped>thead>tr>th a {
			color: #FFF;
			font-weight: 400
		}

		.table.table-striped>thead>tr:nth-child(2)>th {
			background: #0075de;
		}

		.table.table-striped td {
			border: 0;
			vertical-align: middle;
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			background: #FFF
		}

		.table-striped>tbody>tr:nth-of-type(even) {
			background: #F1F1F1;
		}

		.color-bluegreen {
			color: #169F98;
		}

		.color-white {
			color: #fff;
		}

		.peso_currency {
			font-family: DejaVu Sans;
		}

		.bg-bluegreen {
			background-color: #169F98;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.margin0 {
			margin: 0;
		}

		.padding10 {
			padding: 10px;
		}

		p {
			margin: 0 0 15px;
		}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>

<body>
	<div class="row">
		<div class="col-md-12">

			<div class="text-center">
				<h1 class="color-bluegreen margin0">
					<?php
					$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
					echo $company_name ? $company_name : '';
					?>
				</h1>
				<p class="margin0"><?= $title; ?></p>
				<p>
					<?php echo $period_label; ?>
				</p>
			</div>
			<br />

			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th class="text-left">Project Name</th>
						<th class="text-right">Debit</th>
						<th class="text-right">Credit</th>
						<th class="text-right">Balance</th>
					</tr>
				</thead>
				<tbody>
					<tr style="background:#0075de;color:#FFF">
						<td colspan="4"><strong><?= $ledger; ?></strong></td>
					</tr>

					<?php foreach ($rows as $row) : $entry = @$row['ledger'][0]; //vdebug($rows); 
					?>
						<tr>
							<td class="text-left"><?php echo $row['project_name']; ?></td>
							<td class="text-right"><?php if ($entry->debit > 0) {
														echo convert_amount_dc($entry->debit);
														$debit += $entry->debit;
													} else {
														echo convert_amount_dc(0);
													}; ?></td>
							<td class="text-right"><?php if ($entry->credit > 0) {
														echo convert_amount_dc($entry->credit);
														$credit += $entry->credit;
													} else {
														echo convert_amount_dc(0);
													}; ?></td>
							<?php $balance += ($entry->debit - $entry->credit); ?>
							<td class="text-right"><?= convert_amount_dc($balance); ?></td>
						</tr>
					<?php endforeach; ?>

				</tbody>
			</table>


		</div>
	</div>


	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td><strong>Date Printed:</strong></td>
				<td><?= date('F j, Y'); ?></td>

				<?php if (isset($result['signatory']) && ($result['signatory'])) : ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?= ucwords(str_replace('_id', '', $key)); ?> By: </strong></td>
						<td> <?= get_person_name($signatory, 'staff'); ?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>