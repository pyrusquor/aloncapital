<?php

$ledgers_tree = $result['ledgers_tree'];
$date = $result['date'];
$rows = $result['rows'];
$period_label = $result['period_label'];

?>

<!DOCTYPE html>
<html>

<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff
		}

		.table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 15px;
		}

		.table>thead>tr>th,
		.table>tbody>tr>td,
		.table>tbody>tr>th,
		.table>tfoot>tr>td,
		.table>tfoot>tr>th {
			padding: 5px 8px
		}

		.table>thead>tr>th {
			border-bottom: 2px solid #00529C;
		}

		.table>tfoot>tr>td {
			border-top: 2px solid #00529C;
		}

		.table>tfoot {
			border-top: 2px solid #DDD;
		}

		.table>tbody {
			border-bottom: 1px solid #fff;
		}

		.table.table-striped>thead>tr>th {
			background: #00529C;
			color: #FFF;
			border: 0;
			padding: 12px 8px;
			text-transform: uppercase;
		}

		.table.table-striped>thead>tr>th a {
			color: #FFF;
			font-weight: 400
		}

		.table.table-striped>thead>tr:nth-child(2)>th {
			background: #0075de;
		}

		.table.table-striped td {
			border: 0;
			vertical-align: middle;
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			background: #FFF
		}

		.table-striped>tbody>tr:nth-of-type(even) {
			background: #F1F1F1;
		}

		.color-bluegreen {
			color: #169F98;
		}

		.color-white {
			color: #fff;
		}

		.peso_currency {
			font-family: DejaVu Sans;
		}

		.bg-bluegreen {
			background-color: #169F98;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.margin0 {
			margin: 0;
		}

		.padding10 {
			padding: 10px;
		}

		p {
			margin: 0 0 15px;
		}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>

<body>




	<div class="row">

		<div class="col-md-12">
			<div class="text-center">
				<h1 class="color-bluegreen margin0">
					<?php
					$company_name = get_value_field($result['filters']['company_id'], 'companies', 'name');
					echo $company_name ? $company_name : '';
					?>
				</h1>
				<p class="margin0">General Ledger Summary</p>
				<p>
					<?php echo $period_label; ?>
				</p>
			</div>

			<table class="table table-condensed table-striped">
				<tbody>

					<?php $total = ['debit' => 0, 'credit' => 0, 'net_movement' => 0]; ?>
					<?php
					if (!empty($ledgers_tree)) {
						error_reporting(E_ALL);

						foreach ($ledgers_tree as $account) {
							$str_accounts = '
						<tr style="background:#00529C;color:#FFF">
							<td style="background:#00529C;color:#FFF">' . $account['name'] . '</td>
							<td style="background:#00529C;color:#FFF" class="text-right">Debit</td>
							<td style="background:#00529C;color:#FFF" class="text-right">Credit</td>
							<td style="background:#00529C;color:#FFF" class="text-right">Net Movement</td>
						</tr>
						';

							$str_groups = '';
							$total_account = ['debit' => 0, 'credit' => 0, 'net_movement' => 0];

							if ($account['children']) {

								foreach ($account['children'] as $group) {
									$init_ledgers = [];

									$tmp_str = '
								<tr style="background:#0075DE;color:#FFF">
									<td colspan="4" style="padding-left:30px">' . $group['name'] . '</td>
								</tr>
								';

									$str_ledgers = '';

									$total_group = ['debit' => 0, 'credit' => 0, 'net_movement' => 0];


									if (isset($group['subgroups']) && !empty($group['subgroups'])) {
										# code...

										foreach ($group['subgroups'] as $key => $g_subgroup) {
											# code...

											if (isset($g_subgroup['subgroups']) && !empty($g_subgroup['subgroups'])) {

												foreach ($g_subgroup['subgroups'] as $key => $gg_subgroup) {

													$init_ledgers = array_merge($init_ledgers, $gg_subgroup['ledgers']);
												}
											}

											$init_ledgers = array_merge($init_ledgers, $g_subgroup['ledgers']);
										}
									}


									$group['ledgers'] = array_merge($init_ledgers, $group['ledgers']);

									// if ($group['ledgers']) {
									// 	// echo "<pre>";print_r($rows);echo "</pre>";
									// 	vdebug($group['ledgers']);
									// }

									if ($group['ledgers']) {

										foreach ($group['ledgers'] as $ledger) {

											if (!isset($rows[$ledger['id']])) {
												continue;
											}

											$row = $rows[$ledger['id']];
											$row->net_movement = $row->debit - $row->credit;

											// get total
											foreach ($total as $key => $val) {
												$total[$key] += $row->{$key};
												$total_group[$key] += $row->{$key};
												$total_account[$key] += $row->{$key};
											}
											// Ledgers
											$str_ledgers .= '
										<tr>
											<td style="padding-left:50px">' . $row->ledger_name . '</td>
											<td class="text-right">' . convert_amount_dc($row->debit) . '</td>
											<td class="text-right">' . convert_amount_dc($row->credit) . '</td>
											<td class="text-right">' . convert_amount_dc($row->net_movement) . '</td>
										</tr>
										';
										}
									}


									if (!empty($str_ledgers)) {
										// GROUP TOTAL
										$str_ledgers .= '
									<tr>
										<th style="padding-left:30px"><strong>Total ' . $group['name'] . '</strong></th>
										<th class="text-right"><strong>' . convert_amount_dc($total_group['debit']) . '</strong></th>
										<th class="text-right"><strong>' . convert_amount_dc($total_group['credit']) . '</strong></th>
										<th class="text-right"><strong>' . convert_amount_dc($total_group['net_movement']) . '</strong></th>
									</tr>
									';

										$str_groups .= $tmp_str . $str_ledgers;
									}
								}
							}

							if ($str_groups) {
								// ASSET TOTAL
								$str = '
							<tr style="background: #dedede;border-bottom: 3px solid #FFF;">
								<th><strong>Total ' . $account['name'] . '</strong></th>
								<th class="text-right"><strong>' . convert_amount_dc($total_account['debit']) . '</strong></th>
								<th class="text-right"><strong>' . convert_amount_dc($total_account['credit']) . '</strong></th>
								<th class="text-right"><strong>' . convert_amount_dc($total_account['net_movement']) . '</strong></th>
							</tr>
							';

								echo $str_accounts . $str_groups . $str;
							}
						}
					} else { ?>
						<tr>
							<td colspan="4" class="text-center">No Data.</td>
						</tr>
					<?php } ?>

				</tbody>

				<?php if (!empty($total['debit']) || !empty($total['credit']) || !empty($total['net_movement'])) : ?>
					<tfoot>
						<tr>
							<th colspan="4"></th>
						</tr>
						<tr>
							<th><strong>Total for all accounts</strong></th>
							<th class="text-right"><strong><?= convert_amount_dc_2($total['debit'], false) ?></strong></th>
							<th class="text-right"><strong><?= convert_amount_dc_2($total['credit'], false) ?></strong></th>
							<th class="text-right"><strong><?= convert_amount_dc_2($total['net_movement'], false) ?></strong></th>
						</tr>
					</tfoot>
				<?php endif; ?>
			</table>

		</div>

	</div>



	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td><strong>Date Printed:</strong></td>
				<td><?= date('F j, Y'); ?></td>

				<?php if (isset($result['signatory']) && ($result['signatory'])) : ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?= ucwords(str_replace('_id', '', $key)); ?> By: </strong></td>
						<td> <?= get_person_name($signatory, 'staff'); ?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>

</body>

</html>