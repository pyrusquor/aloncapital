<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">View Logs</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				
			</div>
		</div>
	</div>
</div>

<!-- begin:: Content -->
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-12">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							<?=$accounting_report['name'];?> Logs
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<table class="table table-striped- table-bordered table-hover">
						<thead>
							<tr>
								<td>
									No.
								</td>
								<td>
									Parameters
								</td>
								<td>
									Signatories
								</td>
								<td>
									Date Generated
								</td>
								<td>
									Action
								</td>
							</tr>
						</thead>
						<tbody>
							<?php if ($accounting_report['logs']): krsort($accounting_report['logs'])?>
								<?php foreach ($accounting_report['logs'] as $key => $log) { ?>
									<tr>
										<td>
											<?=$log['id'];?>
										</td>
										<td>
											<?php $f = json_decode($log['filters']);?>
											<?php foreach ($f as $key => $v) { $kk = str_replace("_id", "", $key); $tbl = table(plural($kk)); ?>
													<?=ucwords($kk)." : ";?> <?php echo ( isset($kk) && !empty($kk) ) ? get_value_field($v,$tbl,'name') : $v ;?><br>
											<?php } ?>
										</td>
										<td>
											<?php $s = json_decode($log['signatory']);?>
											<?php foreach ($s as $key => $v) { ?>
													<?=$key." : ".get_person_name($v,'staff');?><br>
											<?php } ?>
										</td>
										<td>
											<?=view_date($log['created_at']);?>
										</td>
										<td>
											<a href="<?=base_url()?>accounting_report/generate/<?=$log['id'];?>" target="_blank" class="btn btn-success">Generate</a>
										</td>
									</tr>
								<?php } ?>
							<?php endif ?>
						</tbody>
					</table>

					<!--end::Form-->
				</div>
			</div>
			<!--end::Portlet-->

		</div>
		
	</div>
</div>
<!-- begin:: Footer -->