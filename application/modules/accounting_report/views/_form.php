<?php

$filters  =    isset($form['filters']) && $form['filters'] ? (array) json_decode($form['filters']) : '0';
$signatory  =    isset($form['signatory']) && $form['signatory'] ? (array) json_decode($form['signatory']) : '';

// vdebug($form);
?>

<input type="hidden" name="slug" id="slug" value="<?= $form['slug']; ?>">
<input type="hidden" name="accounting_report_id" id="accounting_report_id" value="<?= $form['id']; ?>">

<div class="row">
    <div class="col-md-6">

        <?php if (in_array('daterange', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Date Range <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control" name="filters[daterange]" id="daterangepicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>


        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="">Company <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="filters[company_id]" id="company_id" class="suggests form-control" data-module="companies">
                            <option value="0">Select Company</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <?php if (in_array('transaction_type', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Transaction Type</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <?php echo form_dropdown('filters[transaction_type]', Dropdown::get_static('accounting_entries_journal_type'), '', 'class="form-control" id="transaction_type"'); ?>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('ledger_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Ledger Name</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[ledger_id]" id="ledger_id" class="suggests form-control" data-param="company_id" data-module="accounting_ledgers">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>


         <?php if (in_array('project_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Projects</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[project_id]" id="project_id" class="suggests form-control" data-module="projects">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('property_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Property </label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[property_id]" id="property_id" class="suggests form-control" data-module="properties" data-param="project_id">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('payee_type', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Payee Type</label>
                        <div class="kt-input-icon  kt-input-icon--left">
                            <?php echo form_dropdown('filters[payee_type]', Dropdown::get_static('payee_type'), '', 'class="form-control" id="payee_type"'); ?>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('payee_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Payee</label>
                        <select class="form-control suggests" data-type="person" data-module="buyers" id="payee_type_id" name="filters[payee_type_id]">
                            <option value="">Select Payee</option>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('buyer_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Buyers <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[buyer_id]" id="buyer" class="suggests form-control" data-type="person" data-module="buyers">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('seller_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Sellers <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[seller_id]" id="seller" class="suggests form-control" data-type="person" data-module="sellers">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('date', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Date <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control datePicker" value="<?= date('Y-m-d'); ?>" readonly name="filters[date]" id="datePicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('year', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Year <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control yearPicker" value="<?= date('Y'); ?>" readonly name="filters[year]" id="yearPicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('month_year', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Month Year <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control yearmonthPicker" value="<?= date('Y-m'); ?>" readonly name="filters[month_year]" id="yearmonthPicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('collectible_type', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Collectible Type <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[collectible_type]" id="collectible_type" class="form-control">
                                <option value="due_date">Due Date</option>
                                <option value="monthly">Montly</option>
                                <option value="yearly">Yearly</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

    </div>


    <div class="col-md-6">





        <?php if (in_array('prepared_id', $signatory)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Prepared By</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="signatory[prepared_id]" id="prepared" class="suggests form-control" data-module="staff" data-type="person">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('checked_id', $signatory)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Checked By</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="signatory[checked_id]" id="checked" class="suggests form-control" data-module="staff" data-type="person">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('approved_id', $signatory)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Approved By</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="signatory[approved_id]" id="approved" class="suggests form-control" data-module="staff" data-type="person">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>


    </div>
</div>