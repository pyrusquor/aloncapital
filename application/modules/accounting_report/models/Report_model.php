<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->account = [];
		$company_id = 2;
	}

	public function submenu()
	{
		return [
			'all' => ['url'=>base_url('v2/reports'), 'name'=>'All'],
			'financial' => ['url'=>base_url('v2/reports/financial'), 'name'=>'Financial'],
			'accounting' => ['url'=>base_url('v2/reports/accounting'), 'name'=>'Accounting'],
			'sales' => ['url'=>base_url('v2/reports/sales'), 'name'=>'Sales'],
			'purchases' => ['url'=>base_url('v2/reports/purchases'), 'name'=>'Purchases'],
			'bir' => ['url' => base_url('v2/reports/bir'), 'name' => 'BIR'],
		];
	}

	public function generate($params = [])
	{
		// $dates = '05/01/2018 - 05/31/2018';

		$data = ['account' => $this->account];

		$type = 'daily';
		if( ! empty($params['type']))
		{
			$type = $params['type'];
		}

		if( ! empty($params['date_from']) && ! empty($params['date_to']))
		{
			$end = DateTime::createFromFormat('Y-m-d', $params['date_to']);
			$begin = DateTime::createFromFormat('Y-m-d', $params['date_from']);
			$data['label'] = $begin->format('M j, Y') . " to " . $end->format('M j, Y');
		}
		/*else if( ! empty($dates))
		{
			$dates = explode(' - ',$dates);
			$end = DateTime::createFromFormat('m/d/Y', $dates[1]);
			$begin = DateTime::createFromFormat('m/d/Y', $dates[0]);
			$data['label'] = $begin->format('M j, Y') . " to " . $end->format('M j, Y');
		}*/
		else
		{
			$end = new DateTime();
			$begin = (new DateTime())->modify('-6 day');
			$data['label'] = "Today, " . date('M j, Y');
		}

			$data['dates'] = $this->get_start_date($end, $begin);
			$data['reports'] = $this->generate_report($end, $begin, $type);

			$data['collection']['total'] = $this->get_total_records('collection_receipts', true);
			$data['collection']['count_today'] = $this->get_count_today('collection_receipts', $begin->format('Y-m-d'), $end->format('Y-m-d'));
			$data['collection']['amount_today'] = $this->get_amt_today('collection_receipts', $begin->format('Y-m-d'), $end->format('Y-m-d'));

			$data['accounting'] = $this->define_accounting($begin->format('Y-m-d'), $end->format('Y-m-d'));
			$data['invoice'] = $this->define_invoices($date_from='',$date_to='');
			$data['voucher'] = $this->define_vouchers($begin->format('Y-m-d'), $end->format('Y-m-d'));
			$data['payment_request'] = $this->get_payment_requests();

		// dd($data);

		return $data;
	}

	public function get_total_records($table, $count = false)
	{
		// $this->db->select('count(id) as total');
		$this->db->where($table.'.company_id', $company_id);
		$this->db->where('is_deleted', 0);

		return ($count)?
			$this->db->count_all_results($table) : $this->db->get($table)->row_array();

	}

	public function get_start_date($end, $begin)
	{

		$arr = array();
		$arr['current'] = date('F d, Y');

		$month = $begin->format('m');
		$year = $begin->format('Y');
		$day = $begin->format('d');

		$arr['end'] = $year . ", " . ($month-1). ", " . $day;
		$arr['end_day'] = $begin->format('F d, Y');
		$arr['current'] = $end->format('F d, Y');

		return $arr;
	}

	public function generate_report($end, $begin, $type='daily')
	{
		$reports = array();

		$params = array();
		$params['where']['date(accounting_entries.payment_date)'] = date_format($begin, "Y-m-d");

		/*if( 'yearly' == $type )
		{
			$params['where']['year(accounting_entries.payment_date)'] = date('Y', strtotime($begin));
		}
		else if( 'monthly' == $type )
		{
			$params['where']['month(accounting_entries.payment_date)'] = date('m', strtotime($begin));
			$params['where']['year(accounting_entries.payment_date)'] = date('Y', strtotime($begin));
		}
		else if( 'weekly' == $type )
		{
			$params['where']['date(accounting_entries.payment_date)'] = ' => date_format($date, "Y-m-d")';
		}*/

		for($i = $begin; $i <= $end; $i->modify('+1 day'))
		{
			// $collection = $this->_get_total($i->format('Y-m-d'), 1);
			// $payout = $this->_get_total($i->format('Y-m-d'), 2);
			$collection = $this->get_total_dues($params, 1);
			$payout = $this->get_total_dues($params, 2);

			$arr = array();
			$arr['date'] = $i->format('Y-m-d');
			$arr['collection'] = $collection;
			$arr['payout'] = $payout;
			array_push($reports, $arr);
 	 	}

 		return $reports;
	}

	/*private function _get_total($date, $type, $date_from="", $date_to="")
	{
		$this->db->select("sum(dr_total) as total_due");
		$this->db->where('entry_type', $type);
		$this->db->where('is_approve', 1);

		if ($date_from == '' && $date_to == '')
		{
			$this->db->where('date(date)', $date);
		}
		else
		{
			$where = "date(date) >= '" . $date_from . "' and date(date) <= '" . $date_to . "'";
			$this->db->where($where);
		}

		$this->db->where('accounting_entries.company_id', $company_id);

		$query = $this->db->get('accounting_entries');
		$value = '0.00';
		$data = $query->row_array();
		if ( !$data['total_due'] == '' )
		{
			$value = $data['total_due'];
		}

		return $value;
	}*/

	public function get_total_dues($params = array(), $type)
	{
		$this->db->select("sum(dr_total) as total_due");
		$this->db->where('entry_type', $type);
		$this->db->where('is_approve', 1);
		$this->db->where('accounting_entries.company_id', $company_id);

		if ( ! empty($params['where']))
		{
			$this->db->where( $params['where'] );
		}

		$data = $this->db->get('accounting_entries')->row_array();

		$value = '0.00';
		if ( !$data['total_due'] == '' )
		{
			$value = $data['total_due'];
		}

		return $value;
	}

	public function get_count_today($table, $date_from = NULL, $date_to = NULL)
	{
		$this->db->select("count(id) as count");

		$this->db->where('is_deleted', 0);


		$where = "";
		if ( $date_from == NULL || $date_to == NULL )
		{
			$where = "date_created = '" . date('Y-m-d'). "'";
		}
		else
		{
			$where = "date_created >= '" . $date_from . "' and date_created <= '" . $date_to . "'";

		}

		$this->db->where($table.'.company_id', $company_id);

		$this->db->where($where);
		$query = $this->db->get($table);
		$data = $query->row_array();


		$value = '0';

		if ( !$data['count'] == '' )
		{
			$value = $data['count'];
		}

		return $value;
	}

	public function get_amt_today($table, $date_from = NULL, $date_to = NULL)
	{
		$this->db->select("sum(paid_amount) as count");

		$this->db->where('is_deleted', 0);


		$where = "";
		if ( $date_from == NULL || $date_to == NULL )
		{
			$where = "date_created = '" . date('Y-m-d'). "'";
		}
		else
		{
			$where = "date_created >= '" . $date_from . "' and date_created <= '" . $date_to . "'";

		}

		$this->db->where($table.'.company_id', $company_id);

		$this->db->where($where);
		$query = $this->db->get($table);
		$data = $query->row_array();
		// echo $this->db->last_query();

		$value = '0';

		if ( !$data['count'] == '' )
		{
			$value = $data['count'];
		}
		return $value;
	}

	// Financial Model Start
	
	// cash summary income
	public function get_cs_ledgers($param=[])
	{
		
		switch($param['type'])
		{
			case 'incomes' :
				$this->db->where('accounting_ledgers.account_id', 3);
			break;
			case 'operating_expenses' :
				//operating expense ledger
				$this->db->where('accounting_ledgers.account_id', 4);
				$this->db->where('accounting_ledgers.group_id !=', 34);
			break;
			case 'non_operating_expenses' :
				// all expense except operating expense
				$this->db->where('accounting_ledgers.group_id', 34);
			break;
		}
		
		$date_range = "MONTH(accounting_entries.payment_date) = ".$param['month']." AND YEAR(accounting_entries.payment_date) = ".$param['year'];
	
		if(isset($param['monthly_avg']))
		{
			$past_months = $param['year'].'-'.$param['month'].'-01 - 3 months';
			$date_range = "DATE_FORMAT(accounting_entries.payment_date, '%Y%m') BETWEEN ".nice_date($past_months, 'Ym')." AND ".$param['year'].$param['month'];
			
		}

		$this->db->select('accounting_ledgers.id, accounting_ledgers.name');
		/*$this->db->select("(
			SELECT ABS(SUM(IF(dc = 'D', amount, 0)) - SUM(IF(dc = 'C', amount, 0))) as amount
			FROM `accounting_entry_items` JOIN `accounting_entries` ON `accounting_entries`.`id` = `accounting_entry_items`.`accounting_entry_id` AND `accounting_entries`.`is_approve` = 1 
			WHERE ".$date_range." 
				AND accounting_entry_items.ledger_id = accounting_ledgers.id
				AND accounting_entries.company_id = ".$company_id."
		) as total", false);*/
		
		// [1 = ASSET, 4 = EXPENSE]
		$this->db->select("(
			SELECT 
				CASE `accounting_ledgers`.`account_id`
					WHEN 1 OR 4
						THEN 
							ABS(SUM(IF(dc = 'D', amount, 0)) - SUM(IF(dc = 'C', amount, 0)))
					ELSE 
						ABS(SUM(IF(dc = 'C', amount, 0)) - SUM(IF(dc = 'D', amount, 0)))
				END as `amount`
			FROM `accounting_entry_items` JOIN `accounting_entries` ON `accounting_entries`.`id` = `accounting_entry_items`.`accounting_entry_id` AND `accounting_entries`.`is_approve` = 1 
			WHERE ".$date_range." 
				AND accounting_entry_items.ledger_id = accounting_ledgers.id
				AND accounting_entries.deleted_at = NULL
				AND accounting_entry_items.deleted_at = NULL
		) as total", false);
		
		$this->db->where_in('accounting_ledgers.company_id', [$param['company_id'], 0]);
		$this->db->where('accounting_ledgers.deleted_at', NULL);
		
		$result = $this->db->get('accounting_ledgers')->result();

		$result = array_key_name($result, 'id');

		return $result;
	}
	

	// Financial Model End
	
	// Get Accounting Data Start
	public function define_accounting($date_from="", $date_to="")
	{
		return $acc = array(
			'income_account' => $this->get_transaction_amount('income', $date_from, $date_to),
			'expense_account' => $this->get_transaction_amount('expense', $date_from, $date_to),
		);
	}

	public function get_transaction_amount($type, $date_from = '', $date_to = '')
	{
		$amount = 0;
		$ledgers = array();
		if ($type == 'income')
		{
			$ledgers = $this->get_ledgers(3);
		}
		else
		{
			$ledgers = $this->get_ledgers(4);
		}

		foreach ($ledgers as $led)
		{
			$amount += $this->get_ledger_sum($led['id'], $date_from, $date_to);
		}

		return $amount;
	}

	public function get_ledgers($parent_id )
	{
		$groups = [];
		$groups = $this->_call_children( $parent_id );

		$ledgers = array();
		foreach ($groups as $grp)
		{
			foreach ($grp['accounting_ledgers'] as $led)
			{
				array_push($ledgers, $led);
			}
		}

		return $ledgers;
	}

	public function get_ledger_sum($ledger_id, $date_from='', $date_to='')
	{
		$this->db->select('sum(accounting_entry_items.amount) as amount');
		$this->db->where('accounting_entry_items.ledger_id', $ledger_id);
		$this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id', 'left');
		$this->db->where('accounting_entries.company_id', $company_id);


		if ($date_from == '' && $date_to == '')
		{
			$this->db->where('date(accounting_entries.payment_date) = curdate()');
		}
		else
		{
			$where = "date(accounting_entries.payment_date) >= '" . $date_from . "' and date(accounting_entries.payment_date) <= '" . $date_to ."'";
			$this->db->where($where);
		}

		$query = $this->db->get('accounting_entry_items');
		$data = $query->row_array();

		$value = '0';

		if ( !$data['amount'] == '' )
		{
			$value = $data['amount'];
		}

		return $value;
	}

	private function _call_children( $parent_id = 0, $affects=-1)
	{
		if ( 0 == $parent_id)
		{
			return $this->_get_children();
		}
		else
		{
			$children = $this->_get_children($parent_id, $affects);
			foreach ($children as &$child)
			{

				$child['subgroups'] = $this->_call_children($child['id'], $affects);
				$child['accounting_ledgers'] = $this->_get_ledgers($child['id']);
			}

			return $children;
		}
	}

	private function _get_children( $parent_id = 0 ,  $affects=-1)
	{
		$this->db->select('id, parent_id, name');

		if ($parent_id > 0)
		{
			$this->db->where('parent_id', $parent_id);
			$this->db->where('company_id', $company_id);

			if ($affects != -1)
			{
				$this->db->where('affects_gross', $affects);
			}

		}
		else
		{
			$this->db->where('company_id', 0); //4 Main Groups ;  Universal
			$this->db->where('parent_id', 0);
		}

		$result = $this->db->get('accounting_groups')->result_array();
	
		return $result;
	}

	private function _get_ledgers($group_id)
	{
		return $this->db->get_where('accounting_ledgers', [ 'group_id'=>$group_id ])->result_array();
	}

	public function get_cash_receipts($params, $count = false)
	{
		$this->db->where_in('slug', ['fees-and-charges', 'creditable-wht', 'deferred-vat', 'output-vat', 'vat-payable']);
		$ledgers = $this->db->get('accounting_ledgers')->result();
		$ledgers = dropdown_array($ledgers, 'slug', 'id');
		//===============
	
		$this->db->select('items.id, items.accounting_entry_id, accounting_entries.payment_date, accounting_entries.or_number, accounting_entries.invoice_number, clients.name as client_name, items.amount');
		$this->db->select('items.ledger_id, accounting_ledgers.name as ledger_name');
	
		if( ! empty($params['from']) && ! empty($params['to']))
		{
			$this->db->where('DATE(accounting_entries.payment_date) BETWEEN "'.$params['from'].'" AND "'.$params['to'].'"');
		}

		if( ! empty($params['bank_ledger']))
		{
			$this->db->where('items.ledger_id', $params['bank_ledger']);
		}
		
		if( ! empty($params['where']))
		{
			$this->db->where($params['where']);
		}
	
		$this->db->where('accounting_entries.entry_type', 1);
		$this->db->where('accounting_ledgers.group_id', 6); // cash and bank
		$this->db->where('accounting_entries.is_approve', 1);
		
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = items.ledger_id');
		$this->db->join('accounting_entries', 'accounting_entries.id = items.accounting_entry_id AND accounting_entries.company_id = '.$company_id);
		$this->db->join('clients', 'clients.id = accounting_entries.client');
		
		$this->db->order_by('accounting_entries.payment_date', 'asc');
		$this->db->order_by('items.id', 'asc');

		if( ! $count)
		{
			if( ! empty($params['limit']))
			{
				$offset = isset($params['offset']) ? $params['offset'] : 0;
				$this->db->limit($params['limit'], $offset);
			}

			$result = $this->db->get('entry_items as items')->result();

			foreach($result as $row)
			{
				// get other accounts
				$this->db->select('items.id, items.ledger_id, accounting_ledgers.name as ledger_name, items.amount');
				$this->db->select('
					IF(items.dc = "c", items.amount, 0) as `credit`,
					IF(items.dc = "d", items.amount, 0) as `debit`,
				');
				$this->db->where('accounting_ledgers.group_id !=', 6);
				// $this->db->where('accounting_ledgers.id !=', 4); //cwt
				$this->db->where('accounting_entry_id', $row->accounting_entry_id);
				$this->db->join('accounting_ledgers', 'accounting_ledgers.id = items.ledger_id');
				$result_ledgers = $this->db->order_by('items.id', 'asc')->get('entry_items as items')->result();
			
				$row->output_vat = 0;
				$row->deffered_vat = 0;
				$row->creditable_wht = 0;

				$tmp_ledgers = [];

				foreach($result_ledgers as $tmp)
				{
					// get output vat
					if($tmp->ledger_id == $ledgers['output-vat'])
					{
						$row->output_vat = $tmp->amount;
						continue;
					}
					
					// get deffered vat
					if($tmp->ledger_id == $ledgers['deferred-vat'])
					{
						$row->deffered_vat = $tmp->amount;
						continue;
					}
					
					// get creditable wht
					if($tmp->ledger_id == $ledgers['creditable-wht'])
					{
						$row->creditable_wht = $tmp->amount;
						continue;
					}
					
					$tmp_ledgers[] = $tmp;
				}
				
				$row->ledgers = $tmp_ledgers;
			}
		
			return $result;
		}
		else
		{
			return $this->db->count_all_results('entry_items as items');
		}
	}

	//==== Get Accounting Data End===


	// Get Invoices Start
	public function define_invoices($date_from='', $date_to='')
	{
		$inv = array();
		$inv['overdue_amount'] = $this->get_invoices_amount('invoice', 'overdue');
		$inv['underdue_amount'] = $this->get_invoices_amount('invoice', 'underdue');
		$inv['due_amount'] = $this->get_invoices_amount('invoice', 'due');
		$inv['overdue_count'] =$this->payment_requests_status_count('invoice', 'overdue');
		$inv['underdue_count'] = $this->payment_requests_status_count('invoice', 'underdue');
		$inv['due_count'] = $this->payment_requests_status_count('invoice', 'due', $date_from, $date_to);
		$inv['due_invoices'] = $this->get_invoices('due');
		$inv['overdue_invoices'] = $this->get_invoices('overdue');

		$inv['total_records'] = $this->get_total_records('invoice', true);

		return $inv;
	}

	public function get_invoices($type)
	{
		$where = "";
		if ($type=='underdue')
		{
			$where = "date_due > curdate()";
		}
		else if ($type =='overdue')
		{
			$where = "date_due < curdate()";
		}
		else
		{
			$where = "date_due = curdate()";
		}

		$this->db->select('count(invoice.id) as count, invoice.id, sum(invoice.remaining_balance) as remaining_balance, clients.first_name, clients.last_name');
		$this->db->where('invoice.is_deleted', 0);
		$this->db->where('invoice.is_settled', 0);
		$this->db->where('invoice.company_id', $company_id);
		$this->db->where($where);
		$this->db->join('clients', 'invoice.client_id = clients.id', 'left');
		$this->db->order_by('invoice.date_due', 'desc');
		$this->db->group_by('invoice.client_id');

		$query = $this->db->get('invoice', 4);
		return $query->result_array();
	}

	public function get_invoices_amount($table, $type = '', $date_from='', $date_to='', $field='date_due')
	{
		$where = "";
		if ($type=='underdue')
		{
			$where = $field . " > curdate()";
		}
		else if ($type =='overdue')
		{
			$where = $field . " < curdate()";
		}
		else
		{
			$where = $field . " = curdate()";

			if ($date_from != '' && $date_to != '')
			{
				$where = $field . " >= '". $date_from . "' and " . $field .  " <= '" . $date_to . "'";
			}
		}


		if ($table == 'invoice')
		{
			$this->db->select('sum(remaining_balance) as amount');
		}
		else
		{
			$this->db->select('sum(total_due) as amount');
		}


		$this->db->where('is_deleted', 0);
		$this->db->where('company_id', $company_id);
		if ($table == 'invoice')
		{
			$this->db->where('is_settled', 0);
		}
		$this->db->where($where);

		$query = $this->db->get($table);
		$data = $query->row_array();

		$value = '0';

		if ( !$data['amount'] == '' )
		{
			$value = $data['amount'];
		}

		return $value;
	}

	// Get Invoices End

	public function define_vouchers($date_from='',$date_to='')
	{
		$voucher = array();
		$voucher['count'] = $this->payment_requests_status_count('payment_voucher', 'due', $date_from, $date_to, 'date_created', 'date_created');
		$voucher['amount'] = $this->get_invoices_amount('payment_voucher', 'due', $date_from, $date_to, 'date_created');

		return $voucher;
	}

	public function get_payment_requests()
	{
		$pr = array();
		$pr['overdue'] = $this->payment_requests_status_count('payment_requests','overdue');
		$pr['underdue'] = $this->payment_requests_status_count('payment_requests','underdue');
		$pr['due'] = $this->payment_requests_status_count('payment_requests','due');
		$pr['approved'] = $this->count_request( 'approved' );
		$pr['disapproved'] = $this->count_request( 'disapproved' );

		return $pr;
	}

	public function payment_requests_status_count($table, $type = '', $date_from='', $date_to='', $field='date_due')
	{
		$where = "";
		if ($type=='underdue')
		{
			$where = $field . " > curdate()";
		}
		else if ($type =='overdue')
		{
			$where = $field . " < curdate()";
		}
		else
		{
			$where = $field . " = curdate()";

			if ($date_from != '' && $date_to != '')
			{
				$where = $field . " >= '". $date_from . "' and " . $field .  " <= '" . $date_to . "'";
			}
		}


		$this->db->select('count(id) as count');
		$this->db->where('is_deleted', 0);
		$this->db->where($table.'.company_id', $company_id);

		if ($table == 'invoice')
		{
			$this->db->where('is_settled', 0);
		}

		$this->db->where($where);

		$query = $this->db->get($table);
		$data = $query->row_array();

		$value = '0';

		if ( !$data['count'] == '' )
		{
			$value = $data['count'];
		}

		return $value;
	}

	public function count_request( $status = '')
	{
		$where = "";

		if ($status == 'approved' )
		{
			$where = "is_approved = 1";
		}
		else
		{
			$where = "is_approved = 0";
		}

		$this->db->select('count(id) as count');
		$this->db->where('is_deleted', 0);
		$this->db->where($where);
		$this->db->where('payment_requests.company_id', $company_id);

		$query = $this->db->get('payment_requests');
		$data = $query->row_array();

		$value = '0';

		if ( !$data['count'] == '' )
		{
			$value = $data['count'];
		}

		return $value;
	}


	// query para sa lumang chart
	/*public function regenerate_graph_data()
	{
		$daterange = $this->input->post('daterange');

		// /$daterange = '12/13/2017 - 02/13/2018';
		$type = $this->input->post('filter_type');
		$daterange = explode(' - ',$daterange);
		$loader = $this->input->post('loader');
		$reports = array();
		$dates = array();

		if ($type == 'daily')
		{
	 	 	$dstart = date("Y-m-d", strtotime($daterange[0]));
			$dend = date("Y-m-d", strtotime($daterange[1]));

			$diff = $this->date_converter($dstart, $dend);
			$count =  $diff->format("%a");

			$begin = date('Y-m-d', strtotime($daterange[0]));

			for ($i = 0; $i <= $count; $i++)
			{

				$date = date_create($begin);

				$condition = array(
				'date(accounting_entries.payment_date) = ' => date_format($date, "Y-m-d")
				);

				$collection = $this->get_total_v2(1, $params = array('where' => $condition));
				$payout = $this->get_total_v2(2, $params = array('where'=> $condition));

				$arr['date'] = date("F d, Y", strtotime( $begin ));
				$arr['collection'] = $collection;
				$arr['payout'] = $payout;
				array_push($reports, $arr);
				$begin = date('Y-m-d', strtotime($begin . ' +1 day'));
			}

			$dates['end_day'] = date("F d, Y", strtotime($dstart));
			$dates['current'] =  date("F d, Y", strtotime($dend));


		}

		else if ($type == 'monthly')
		{ //12-13-2017 to 02-13-2018

			$begin = date("Y-m-01", strtotime($daterange[0]));
			$end = date("Y-m-28", strtotime($daterange[1]));

			$diff = $this->date_converter($begin, $end);
			$count =  $diff->format("%m");
			$years_count = $diff->format("%Y");
			$count = $count + ($years_count * 12);

			$start = date('Y-m', strtotime($daterange[0]));


			for ($i = 0; $i <= $count; $i++)
			{
				$arr = array();

				$month =  date("m", strtotime( $start ." +$i months"));
    			$year =  date("Y", strtotime( $start ." +$i months"));

				$condition = array(
				'month(accounting_entries.payment_date) = ' => $month,
				'year(accounting_entries.payment_date) = ' => $year
				);

				$collection = $this->get_total_v2(1, $params = array('where' => $condition));
				$payout = $this->get_total_v2(2, $params = array('where'=> $condition));

				$arr['date'] = date("M Y", strtotime( $start ." +$i months" ));
				$arr['collection'] = $collection;
				$arr['payout'] = $payout;
				array_push($reports, $arr);

			}

			$dates['end_day'] = date('M Y', strtotime($begin));
			$dates['current'] =date('M Y', strtotime($end));
		}

		else if ($type == 'yearly')
		{

			$dstart = date("Y-m-d", strtotime($daterange[0]));
			$dend = date("Y-m-d", strtotime($daterange[1]));

			$diff = $this->date_converter($dstart, $dend);
			$count =  $diff->format("%Y");
			$count = (int) $count;

			$date_start = date('Y', strtotime($daterange[0]));
			$date_end = date('Y', strtotime($daterange[1]));

			$min = $date_start;
			$max = $date_end;


			for ($i = $min ; $i <= $max; $i++)
			{
				$arr = array();
				$condition = array(
				'year(accounting_entries.payment_date) = ' => $i
				);


				$collection = $this->get_total_v2(1, $params = array('where' => $condition));
				$payout = $this->get_total_v2(2, $params = array('where'=> $condition));

				$arr['date'] = $i;
				$arr['collection'] = $collection;
				$arr['payout'] = $payout;
				array_push($reports, $arr);
			}


			$dates['end_day'] = date('Y', strtotime($date_start));
			$dates['current'] =date('Y', strtotime($date_end));

		}

		else if ($type == 'weekly')
		{
			$dstart = date("Y-m-d", strtotime($daterange[0]));
			$dend = date("Y-m-d", strtotime($daterange[1]));
			$start_day = $dend;

			$diff = $this->date_converter($dstart, $dend);

			$count = 0;
			$rem = 0;

			if ($diff->format("%a") > 0)
			{
				$count =  $diff->format("%a") / 7;
			}

			$count = (int) $count;
			$start_day = $dstart;
			$min = date("M j, Y", strtotime($dstart));


			for ($i = 0; $i <= $count; $i++)
			{

				$days1 = 7 * $i;
				$days2 = (7 * $i) + 7;

				$date_initial = date('Y-m-d', strtotime($start_day . "+" . $days1 . " days"));
				$date_til =  date('Y-m-d', strtotime($start_day . "+" . $days2 . " days"));

				$condition = array(
					'date(accounting_entries.payment_date) >= ' => $date_initial,
					'date(accounting_entries.payment_date) <= ' => $date_til,

				);
				$collection = $this->get_total_v2(1, $params = array('where' => $condition));
				$payout = $this->get_total_v2(2, $params = array('where'=> $condition));

				$arr['date'] = 'Week ' . ($i+1) . ": " . date('m/d/Y', strtotime($date_initial)) . " - " . date('m/d/Y', strtotime($date_til));
				$arr['collection'] = $collection;
				$arr['payout'] = $payout;
				array_push($reports, $arr);
			}


			$dates['end_day'] = $min;
			$dates['current'] = date('M j, Y', strtotime($date_til));

		}



		if ($loader =='ajax')
		{
			return $reports;
		}
		else
		{
			$data['dates'] = $dates;
			$data['reports'] = $reports;

			$view = $this->load->view( 'reports/report_graph', $data, true );
			echo json_encode($view);
		}

	}*/



	// ===== accounting

	// use for bank summary, general ledger
	public function ledger_summary($params = [], $count = false)
	{
		$company_id = $params['company_id'];

		$from = date('Y-m-d', strtotime('-1 year'));
		$to   = date('Y-m-d');

		if( ! empty($params['from']) || ! empty($params['to']))
		{
			$from = $params['from'];
			$to   = $params['to'];
		}
		
		if( ! empty($params['where']))
		{
			$this->db->where($params['where']);
		}
		
		
		$this->db->select('accounting_ledgers.name as ledger_name, accounting_ledgers.id as ledger_id, accounting_ledgers.op_balance');
		$this->db->select('
			SUM(accounting_entry_items.amount) as `total_amount`,
			SUM(IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0)) as `credit`,
			SUM(IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0)) as `debit`,
			accounting_entries.payment_date
		',false);

		// get starting balance
		$this->db->select("(SELECT SUM(IF(dc = 'D', amount, 0)) - SUM(IF(dc = 'C', amount, 0)) as amount
				FROM `accounting_entry_items` JOIN `accounting_entries` ON `accounting_entries`.`id` = `accounting_entry_items`.`accounting_entry_id` AND `accounting_entries`.`is_approve` = 1 
				WHERE DATE(accounting_entries.payment_date) < '".$from."'
					AND accounting_entry_items.ledger_id = accounting_ledgers.id
					AND accounting_entries.company_id = ".$company_id."
				) as starting_balance",false);
				
		// company -d
		$this->db->where('DATE(accounting_entries.payment_date) BETWEEN "'. $from .'" AND "'. $to .'"');
		$this->db->where(['accounting_entries.is_approve' => 1, 'accounting_entries.company_id' => $company_id]);
 	
		$this->db->where('accounting_entries.deleted_at', NULL);
		$this->db->where('accounting_ledgers.deleted_at', NULL);
		$this->db->where('accounting_entry_items.deleted_at', NULL);

		$this->db->join('accounting_groups', 'accounting_groups.id = accounting_ledgers.group_id', 'left');
		$this->db->join('accounting_entry_items', 'accounting_entry_items.ledger_id = accounting_ledgers.id', 'left');
		$this->db->join('accounting_entries', 'accounting_entry_items.accounting_entry_id = accounting_entries.id', 'left');

		$this->db->order_by('accounting_ledgers.name', 'ASC');
		$this->db->group_by('accounting_ledgers.id');
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
 
		if( ! $count)
		{
			$result = $this->db->get('accounting_ledgers')->result();

			return $result; 
		}
		else
		{
			return $this->db->count_all_results('accounting_ledgers');
		}
	}

	public function get_transactions($params=[],$debug = 0)
	{
		$company_id = $params['company_id'];

        $this->load->model('accounting_groups/Accounting_groups_model', 'Group_m');

		if(empty($params['ledger_id']))
		{
			// get ledgers
			$ledger_tree = $ledgers_tree = $this->Group_m->tree($company_id);

			// vdebug($ledger_tree);
		}
		else
		{
			// get single ledger
			$ledger = $this->db->get_where('accounting_ledgers', ['id' => $params['ledger_id']])->row_array();
			
			// get top parent
			$group = $this->db->get_where('accounting_groups', ['id'=>$ledger['group_id']])->row_array();
			// vdebug($group);
			$group['children'][0] = $this->db->get_where('accounting_groups', ['id'=>$ledger['group_id']])->row_array();
			$group['children'][0]['subgroups'][0]['ledgers'][0] = $ledger;
			
			$ledger_tree[0] = $group;
		}
		$tmp = [];


		if ($debug) {
			echo "<pre>";
			print_r($params);
			print_r($ledger_tree);
			echo "</pre>";
		}

		foreach($ledger_tree as $key => $accounts)
		{		
			foreach($accounts['children'] as $key1 => $groups)
			{
				foreach($groups['subgroups'] as $key2 => $subgroup)
				{
					if ($subgroup) {

						if ( isset($subgroup['ledgers']) ) {

							# code...
							foreach ($subgroup['ledgers'] as $key => $ledger) {

								$this->db->select("SUM(IF(dc = 'D', amount, 0)) - SUM(IF(dc = 'C', amount, 0)) as amount", false);
								$this->db->where('DATE(accounting_entries.payment_date) <', $params['from']);

								$this->db->join('accounting_entries', 'accounting_entries.id = items.accounting_entry_id AND accounting_entries.company_id = '.$company_id);

								if ( isset( $params['payee_type'] ) &&  !empty($params['payee_type']) && isset( $params['payee_type_id'] ) &&  !empty($params['payee_type_id']) )  {
									$this->db->where('accounting_entries.payee_type', $params['payee_type']);
									$this->db->where('accounting_entries.payee_type_id', $params['payee_type_id']);
								}
								
								if ( isset( $params['payee_type'] ) && ($params['project_id']) ) {
									$this->db->where('accounting_entries.project_id', $params['project_id']);
								}

								if ( isset( $params['property_id'] ) && ($params['property_id']) ) {
									$this->db->where('accounting_entries.property_id', $params['property_id']);
								}

								$this->db->where('accounting_entries.deleted_at', NULL);
								$starting_balance = $this->db->get_where('accounting_entry_items as items', ['items.ledger_id'=>$ledger['id'],'items.deleted_at'=>NULL])->row();

								if ($debug) {
									if ($starting_balance->amount) {
										echo "<pre>";
										print_r($starting_balance);
										echo "<br>";
										echo $this->db->last_query();
										echo "</pre>";
									}
								}
								
								// get transactions
								$this->db->select('items.id, items.ledger_id, items.accounting_entry_id, accounting_entries.invoice_number, accounting_entries.remarks, accounting_entries.or_number, accounting_entries.invoice_number, accounting_entries.id, accounting_entries.payment_date, accounting_entries.payee_type, accounting_entries.payee_type_id');
								$this->db->select('
									IF(items.dc = "d", items.amount, 0) as `debit`,
									IF(items.dc = "c", items.amount, 0) as `credit`
								',false);
								
								
								$this->db->join('accounting_entries', 'accounting_entries.id = items.accounting_entry_id AND accounting_entries.company_id = '.$company_id.' AND accounting_entries.is_approve = 1');

								$this->db->where('DATE(payment_date) BETWEEN "'.$params['from'].'" AND "'.$params['to'].'"');

								if ( isset( $params['payee_type'] ) &&  !empty($params['payee_type']) && isset( $params['payee_type_id'] ) &&  !empty($params['payee_type_id']) )  {

									$this->db->where('accounting_entries.payee_type', $params['payee_type']);
									$this->db->where('accounting_entries.payee_type_id', $params['payee_type_id']);

								}

								if ( isset( $params['project_id'] ) &&  !empty($params['project_id'])  )  {
									$this->db->where('accounting_entries.project_id', $params['project_id']);
								}

								if ( isset( $params['property_id'] ) &&  !empty($params['property_id'])  )  {
									$this->db->where('accounting_entries.property_id', $params['property_id']);
								}

								$this->db->where('accounting_entries.deleted_at', NULL);
								$this->db->where('items.deleted_at', NULL);

								$this->db->where('(IF(items.dc = "d", items.amount, 0) != 0 OR IF(items.dc = "c", items.amount, 0) != 0)');//don't display 0

								$result = $this->db->order_by('payment_date', 'asc')->get_where('accounting_entry_items as items', ['items.ledger_id'=>$ledger['id']])->result();

								if( ! empty($result))
								{
									$tmp[$accounts['id']]['name'] = $accounts['name'];
									$tmp[$accounts['id']]['groups'][$groups['id']]['name']		=  $groups['name'];
									$tmp[$accounts['id']]['groups'][$groups['id']]['starting']	=  $starting_balance->amount;
									
									if( ! empty($tmp[$accounts['id']]['groups'][$groups['id']]['entries']))
									{
										$tmp[$accounts['id']]['groups'][$groups['id']]['entries']	 = array_merge($tmp[$accounts['id']]['groups'][$groups['id']]['entries'], $result);
									}
									else
									{
										$tmp[$accounts['id']]['groups'][$groups['id']]['entries']		=  $result;
									}
								}

							}
						}
							
							
						
					}
					
				}
			}
		}

		// vdebug($tmp);/

		return $tmp;
	}

	public function get_reconciliation($params=[])
	{
		$company_id = $params['company_id'];

		$this->db->select("SUM(IF(dc = 'D', amount, 0)) as debit", false);
		$this->db->where('DATE(accounting_entries.payment_date) <', $params['from']);
		$this->db->where('accounting_ledgers.reconciliation', 1 );
		$this->db->where('items.is_reconciled', 1 );
		$this->db->where('items.dc = "d"');
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = items.ledger_id');

		$this->db->join('accounting_entries', 'accounting_entries.id = items.accounting_entry_id AND accounting_entries.company_id = 1');
		$starting_debit_balance = $this->db->get_where('accounting_entry_items as items')->row();

		$this->db->select("SUM(IF(dc = 'C', amount, 0)) as credit", false);
		$this->db->where('DATE(accounting_entries.payment_date) <', $params['from']);
		$this->db->where('accounting_ledgers.reconciliation', 1 );
		$this->db->where('items.is_reconciled', 1 );
		$this->db->where('items.dc = "c"');
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = items.ledger_id');
		$this->db->join('accounting_entries', 'accounting_entries.id = items.accounting_entry_id AND accounting_entries.company_id = 1');
		$starting_credit_balance = $this->db->get_where('accounting_entry_items as items')->row();
		
		// get transactions
		$this->db->select('items.id, items.ledger_id, items.accounting_entry_id, accounting_entries.invoice_number, accounting_entries.remarks, accounting_entries.or_number, accounting_entries.invoice_number, accounting_entries.id, accounting_entries.payment_date, accounting_entries.payee_type, accounting_entries.payee_type_id');
		$this->db->select('
			IF(items.dc = "d", items.amount, 0) as `debit`,
			IF(items.dc = "c", items.amount, 0) as `credit`
		',false);
		
		
		$this->db->join('accounting_entries', 'accounting_entries.id = items.accounting_entry_id AND accounting_entries.company_id = '.$company_id.' AND accounting_entries.is_approve = 1');

		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = items.ledger_id');

		$this->db->where('DATE(payment_date) BETWEEN "'.$params['from'].'" AND "'.$params['to'].'"');
		$this->db->where('items.dc = "'.$params['dc'].'"');
		$this->db->where( 'accounting_ledgers.reconciliation', 1 );
		$this->db->where( 'items.is_reconciled', $params['is_reconciled'] );

		// $this->db->where('(IF(items.dc = "d", items.amount, 0) != 0 OR IF(items.dc = "c", items.amount, 0) != 0)');//don't display 0

		$result = $this->db->order_by('payment_date', 'asc')->get_where('accounting_entry_items as items')->result();
		

		$tmp['starting_debit_balance'] = $starting_debit_balance->debit;
		$tmp['starting_credit_balance'] = $starting_credit_balance->credit;
		$tmp['starting_balance'] = $starting_debit_balance->debit  - $starting_credit_balance->credit;
		$tmp['result'] = $result;
		
		// if( ! empty($result))
		// {
		// 	$tmp[$accounts['id']]['name'] = $accounts['name'];
			
		// 	$tmp[$accounts['id']]['accounting_groups'][$groups['id']]['name']		=  $groups['name'];
		// 	$tmp[$accounts['id']]['accounting_groups'][$groups['id']]['starting']	=  $starting_balance->amount;
			
		// 	if( ! empty($tmp[$accounts['id']]['accounting_groups'][$groups['id']]['accounting_entries']))
		// 	{
		// 		$tmp[$accounts['id']]['accounting_groups'][$groups['id']]['accounting_entries']	 = array_merge($tmp[$accounts['id']]['accounting_groups'][$groups['id']]['accounting_entries'], $result);
		// 	}
		// 	else
		// 	{
		// 		$tmp[$accounts['id']]['accounting_groups'][$groups['id']]['accounting_entries']		=  $result;
		// 	}
		// }
				


		return $tmp;
	}
	
	public function get_item_transactions($params = [], $count = false)
	{

		$from = date('Y-m-d', strtotime('-1 year'));
		$to   = date('Y-m-d');

		if( ! empty($params['from']) || ! empty($params['to']))
		{
			$from = $params['from'];
			$to   = $params['to'];
		}
		
		$this->db->where('DATE(accounting_entries.payment_date) BETWEEN "'. $from .'" AND "'. $to .'"');

		if( ! empty($params['where']))
		{
			$this->db->where($params['where']);
		}
		

		$this->db->where(['accounting_entries.is_approve' => 1, 'accounting_entries.company_id' => $company_id]);


		$this->db->select('
			accounting_entries.*,
			accounting_ledgers.id as ledger_id,
			accounting_ledgers.name as ledger_name,
			accounting_ledgers.group_id as ledger_group_id,
			accounting_ledgers.account_id,
			accounting_entry_items.id as entry_item_id,
			accounting_entry_items.amount,
			accounting_entry_items.dc,
			accounting_entry_items.description,
			entry_types.name as entry_type_name,
		');

		$this->db->select('
			CASE
				WHEN accounting_entries.payee_type = "supplier" THEN suppliers.name
				ELSE clients.name
			END AS `payee_name`
		', false);
		$this->db->select('
			IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0) as `debit`,
			IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0) as `credit`,
		',false);

		// remove zeros
		$this->db->group_start()
			->where('IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0) !=', 0)
			->or_where('IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0) !=', 0)
			->group_end();			

		$this->db->join('entry_types', 'entry_types.id = accounting_entries.entry_type', 'left');
		$this->db->join('accounting_entry_items', 'accounting_entry_items.accounting_entry_id = accounting_entries.id', 'left');
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
		$this->db->join('accounting_groups', 'accounting_groups.id = accounting_ledgers.group_id', 'left');

		$this->db->join('suppliers', 'suppliers.id = accounting_entries.supplier', 'left');
		$this->db->join('clients', 'clients.id = accounting_entries.client', 'left');
		
		
		if( ! $count)
		{
			if( ! empty($params['limit']))
			{
				$offset = isset($params['offset']) ? $params['offset'] : 0;
				$this->db->limit($params['limit'], $offset);
			}
			
			$this->db->order_by('accounting_entries.payment_date', 'DESC');
			
			$result =  $this->db->get('accounting_entries')->result();
			return $result;
		}
		else
		{
			return $this->db->count_all_results('accounting_entries');
		}
	}

	public function get_journal($params = [], $count = false)
	{
		/***
		* DONT FORGET TO UPDATE ALSO
		* get_journal_total();
		*/
		
		$from = date('Y-m-d', strtotime('-1 year'));
		$to   = date('Y-m-d');

		if( ! empty($params['from']) || ! empty($params['to']))
		{
			$from = $params['from'];
			$to   = $params['to'];
		}
		
		if( ! empty($params['entry_types']))
		{
			// $this->db->where_in('accounting_entries.entry_type', [1,2,3,4]);
			$this->db->where_in('accounting_entries.journal_type', $params['entry_types']);
		}
		
		if( ! empty($params['where']))
		{
			$this->db->where($params['where']);
		}
		
		if( ! empty($params['where_in']))
		{
			$this->db->where_in($params['where_in']);
		}
		

		$this->db->where([
			'accounting_entries.is_approve' => 1,
			'accounting_entries.company_id' => $params['company_id']
		]);

		$this->db->where('DATE(accounting_entries.payment_date) BETWEEN "'. $from .'" AND "'. $to .'"');
		$this->db->where('accounting_entries.deleted_at', NULL);
		$this->db->order_by('accounting_entries.payment_date', 'ASC');

		if($count)
		{
			return $this->db->count_all_results('accounting_entries');
		}
		else
		{
			if( ! empty($params['limit']))
			{
				$offset = isset($params['offset']) ? $params['offset'] : 0;
				$this->db->limit($params['limit'], $params['offset']);
			}
	
			$entries = $this->db->get('accounting_entries')->result();

			foreach ($entries as $row)
			{
				/* $row->debit  = 0;
				$row->credit = 0; */

				$this->db->select('accounting_entry_items.*, accounting_ledgers.name as ledger_name',false);
				$this->db->select('
					IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0) as `debit`,
					IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0) as `credit`
				',false);

				$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
				$this->db->order_by('(CASE accounting_entry_items.dc 
					WHEN "d" THEN 0 
					WHEN "c" THEN 1 
					ELSE 2
					END), dc asc', false);

				$this->db->order_by('debit', 'desc');
				$this->db->order_by('credit', 'desc');
				$this->db->where('accounting_entry_items.deleted_at', NULL);
				$this->db->where('accounting_ledgers.deleted_at', NULL);

				$items = $this->db->get_where('accounting_entry_items', ['accounting_entry_id' => $row->id])->result();
				$row->items = $items;

				/* foreach($items as $item)
				{
					$row->debit += $item->debit;
					$row->credit += $item->credit;
				} */
			}

			return $entries;
		}
	}
	
	public function get_journal_total($params=[])
	{
		$from = date('Y-m-d', strtotime('-1 year'));
		$to   = date('Y-m-d');

		if( ! empty($params['from']) || ! empty($params['to']))
		{
			$from = $params['from'];
			$to   = $params['to'];
		}
		
		if( ! empty($params['entry_types']))
		{
			// $this->db->where_in('accounting_entries.entry_type', [1,2,3,4]);
			$this->db->where_in('accounting_entries.entry_type', $params['entry_types']);
		}
	
		if( ! empty($params['where']))
		{
			$this->db->where($params['where']);
		}
		
		if( ! empty($params['where_in']))
		{
			$this->db->where_in($params['where_in']);
		}
		
		$this->db->where('DATE(accounting_entries.payment_date) BETWEEN "'. $from .'" AND "'. $to .'"');
		
	
		$this->db->select('
			SUM(IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0)) as `debit`,
			SUM(IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0)) as `credit`
		',false);

		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
		$this->db->order_by('(CASE accounting_entry_items.dc 
			WHEN "D" THEN 0 
			WHEN "C" THEN 1 
			ELSE 2
			END), dc asc', false);

		$this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id AND accounting_entries.is_approve = 1');
	
		$total = $this->db->get_where('accounting_entry_items', ['accounting_entries.company_id' => $params['company_id']])->row();
		
		return $total;
		
	}

	public function get_trial_balance_ledgers($params = [], $count = false)
	{
		$from = date('Y-m-d', strtotime('-1 year'));
		$to   = date('Y-m-d');

		if( ! empty($params['from']) || ! empty($params['to']))
		{
			$from = $params['from'];
			$to   = $params['to'];
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('accounting_entries.project', $params['project_id']);
		}


		if( ! empty($params['ledger_id']))
		{
			$this->db->where('accounting_ledgers.id', $params['ledger_id']);
		}

		$this->db->where(['accounting_entries.is_approve' => 1, 'accounting_entries.company_id' => $company_id]);

		$this->db->select('accounting_entry_items.ledger_id, accounting_ledgers.name as ledger_name, accounting_ledgers.op_balance, accounting_ledgers.op_balance_dc');
		$this->db->select('
			SUM(IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0)) as `dr_total`,
			SUM(IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0)) as `cr_total`,
		',false);
		$this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id', 'left');
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
		$this->db->join('accounting_groups', 'accounting_groups.id = accounting_ledgers.group_id', 'left');
		$this->db->group_by('accounting_entry_items.ledger_id');
		$ledgers = $this->db->get('accounting_entry_items')->result();

		return ($count)? count($ledgers) : $ledgers;
	}

	public function get_bir_logs($params = [], $count = false)
	{

		if( ! empty($params['keywords']))
		{
			$this->db->group_start()
				->like('users.first_name', $params['keywords'])
				->or_like('users.last_name', $params['keywords'])
				->group_end();
		}



		$this->db->select('bir_logs.*, CONCAT(users.last_name, ", ", users.first_name) as name, bir_reports.name as report_name, bir_reports.code as report_code');

		$this->db->join('users', 'users.id = bir_logs.user_id');
		$this->db->join('bir_reports', 'bir_reports.id = bir_logs.report_id');


		$this->db->where('bir_logs.is_deleted', 0);

		return ($count)?
			$this->db->count_all_results('bir_logs') : $this->db->get('bir_logs')->result();
	}

    public function financial_summary($params = [], $count = false)
    {
        $from = date('Y-m-d', strtotime('-1 year'));
        $to   = date('Y-m-d');

        if( ! empty($params['from']) || ! empty($params['to']))
        {
            $from = $params['from'];
            $to   = $params['to'];
        }

        if( ! empty($params['ledger_type']))
        {
            $this->db->where('accounting_ledgers.type', $params['ledger_type']);
        }

        if( ! empty($params['project_id']))
        {
            $this->db->where('accounting_entries.project', $params['project_id']);
        }

        if( ! empty($params['accounting_ledgers']) && is_array($params['accounting_ledgers']))
        {
            $this->db->where_in('accounting_ledgers.id', $params['accounting_ledgers']);
        }

        // $this->db->select("accounting_entries.id as accounting_entry_id, accounting_ledgers.id, accounting_ledgers.name, accounting_entry_items.amount as total_amount, accounting_entry_items.dc");
        $this->db->select("accounting_ledgers.id as ledger_id, accounting_ledgers.name as ledger_name");
        $this->db->select("accounting_entries.id as accounting_entry_id, accounting_entry_items.amount as total_amount, accounting_entry_items.dc");
        $this->db->select('date_format(accounting_entries.payment_date, "%Y-%m") as date_created', false);


        $this->db->where('DATE(accounting_entries.payment_date) BETWEEN "'. $from .'" AND "'. $to .'"');
        $this->db->where(['accounting_entries.company_id' => $company_id, 'accounting_entries.is_approve' => 1]);


        $this->db->join('accounting_entry_items', 'accounting_entry_items.accounting_entry_id = accounting_entries.id', 'left');
        $this->db->join('accounting_ledgers', 'accounting_entry_items.ledger_id = accounting_ledgers.id', 'left');

        $this->db->order_by('accounting_entries.payment_date', 'desc');

        return ($count)?
            $this->db->count_all_results('accounting_entries') : $this->db->get('accounting_entries')->result();
    }

    public function get_bank_ledgers( $params = [], $count = false)
    {
        $this->db->select('accounting_ledgers.*');

        if( ! empty($params['ledger_id']))
        {
            $this->db->where('accounting_ledgers.id', $params['ledger_id']);
        }

        if( ! empty($params['where']))
        {
            $this->db->where($params['where']);
        }

        // $this->db->where(['accounting_entries.is_approve' => 1, 'accounting_entries.company_id' => $company_id]);

        $this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id', 'left');
        $this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
        $this->db->join('accounting_groups', 'accounting_groups.id = accounting_ledgers.group_id', 'left');
        $this->db->group_by('accounting_ledgers.id');
        $ledgers = $this->db->get('accounting_entry_items')->result();

        return ($count)? count($ledgers) : $ledgers;
    }

    public function vat_output($params = [], $count = false)
    {
    	$from = date('Y-m-d');
        $to   = date('Y-m-t', strtotime($from));

        if( ! empty($params['from']) || ! empty($params['to']))
        {
            $from = $params['from'];
            $to   = $params['to'];
        }

       	$this->db->where('DATE(accounting_entries.payment_date) BETWEEN "'. $from .'" AND "'. $to .'"');
        $this->db->where(['accounting_entries.company_id' => $company_id, 'accounting_entries.is_approve' => 1]);

        $this->db->join('accounting_ledgers', 'accounting_ledgers.accounting_entry_id = accounting_entries.id');

        return ($count)?
        	$this->db->count_all_results('accounting_entries') : $this->db->get('accounting_entries')->result();
    }

    public function vat_input($params = [])
    {
    	$from = date('Y-m-d');
        $to   = date('Y-m-t', strtotime($from));

        if( ! empty($params['from']) || ! empty($params['to']))
        {
            $from = $params['from'];
            $to   = $params['to'];
        }

       	$this->db->where('DATE(accounting_entries.payment_date) BETWEEN "'. $from .'" AND "'. $to .'"');
        $this->db->where(['accounting_entries.company_id' => $company_id, 'accounting_entries.is_approve' => 1]);

        $this->db->join('accounting_ledgers', 'accounting_ledgers.accounting_entry_id = accounting_entries.id');

        return ($count)?
        	$this->db->count_all_results('accounting_entries') : $this->db->get('accounting_entries')->result();
    }

    public function get_income_expense($params = [], $count = false)
	{	
		// vdebug($params);

		if( ! empty($params['year']))
		{
			$this->db->where('YEAR(accounting_entries.payment_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']))
		{
			$this->db->where('DATE_FORMAT(accounting_entries.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['from']) && ! empty($params['to']))
		{
			$this->db->where('accounting_entries.payment_date BETWEEN "'. $params['from'] .'" AND "'. $params['to'] .'"');
		}

		if( ! empty($params['entry_type']))
		{
			$this->db->where('accounting_entries.entry_type', $params['entry_type']);
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('accounting_entries.project_id', $params['project_id']);
		}

		if(isset($params['account']))
		{
			if(is_array($params['account']))
			{
				$this->db->where_in('account.slug', $params['account']);
			}
			else
			{
				$this->db->where('account.slug', $params['account']);
			}
		}

		if(isset($params['affects_gross']))
		{
			$this->db->where('accounting_groups.affects_gross', $params['affects_gross']);
		}

		if( ! empty($params['except_ledgers']))
		{
			$this->db->where_not_in('accounting_entry_items.ledger_id', $params['except_ledgers']);
		}

		if( ! empty($params['ledger_slugs']))
		{
			$this->db->where_in('accounting_ledgers.slug', $params['ledger_slugs']);
		}

		// if(! empty($params['group_id']))
		// {
		// 	$this->db->where('accounting_groups.id', $params['group_id']);
		// }

		if(! empty($params['group_id']))
		{

			$this->db->where_in('accounting_groups.id', $params['group_id']);
		}


		if(! empty($params['exclude_group_id']))
		{
			$this->db->where_not_in('accounting_groups.id', $params['exclude_group_id']);
		}
		
		// [1 = ASSETS , 4 = EXPENSE]
		$this->db->select('
			accounting_entry_items.ledger_id,
			accounting_ledgers.name as ledger_name,
			accounting_ledgers.color as ledger_color,

			IFNULL(SUM(IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0)), 0) as `credit`,
   			IFNULL(SUM(IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0)), 0) as `debit`,
   			CASE `accounting_ledgers`.`account_id`
				WHEN 4
					THEN 
						(SUM(IF(dc = "d", amount, 0)) - SUM(IF(dc = "c", amount, 0)))
				ELSE 
					ABS(SUM(IF(dc = "c", amount, 0)) - SUM(IF(dc = "d", amount, 0)))
			END as `amount`
		',false);

   		//IFNULL(SUM(accounting_entry_items.amount), 0) as `amount`

		$this->db->where([
			'accounting_entries.is_approve' => 1, 
			'accounting_entries.company_id' => @$params['company_id']
		]);

		$this->db->where('accounting_entries.deleted_at', NULL);
		$this->db->where('accounting_ledgers.deleted_at', NULL);
		$this->db->where('accounting_groups.deleted_at', NULL);
		$this->db->where('accounting_entry_items.deleted_at', NULL);


		$this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id', 'left');
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
		$this->db->join('accounting_groups as account', 'account.id = accounting_ledgers.account_id', 'left');
		$this->db->join('accounting_groups', 'accounting_groups.id = accounting_ledgers.group_id', 'left');

		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		if( ! empty($params['type']))
		{
			$this->db->group_by('accounting_ledgers.id');

			return $this->db->get('accounting_entry_items')->result();
		}

		return $this->db->get('accounting_entry_items')->row();
	}

	public function get_cash_flow($params = [], $count = false)
	{

		if( ! empty($params['year_month']))
		{
			$this->db->where('DATE_FORMAT(accounting_entries.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		$this->db->where([
			'accounting_entries.is_approve' => 1, 
			'accounting_entries.company_id' => $company_id
		]);

		$this->db->select('IFNULL(SUM(dr_total), 0) as `amount`');
		//$this->db->select('accounting_entries.*');
		$this->db->where('account.slug', $params['account']);

		$this->db->join('accounting_entry_items', 'accounting_entry_items.accounting_entry_id = accounting_entries.id', 'left');
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
		$this->db->join('groups as account', 'account.id = accounting_ledgers.account_id', 'left');
		$this->db->group_by('accounting_entries.id');
		$row = $this->db->get('accounting_entries')->row();


		return (empty($row->amount))? 0 : $row->amount;
	}

	// SALES REPORT
	
	public function get_invoice_report($param=[], $count=false)
	{
		$paid_amount = '(SELECT SUM(paid_amount) FROM collection_receipts as cr WHERE cr.invoice_id = invoice.id AND cr.is_deleted = 0 AND cr.is_active=1)';
		
		if(empty($param['total']))
		{
			$this->db->select('invoice.id, invoice_number, clients.name, invoice.date_created, date_due, total_due, '.$paid_amount.' as paid_amount', false);
			$this->db->select('(SELECT payment_date FROM collection_receipts as cr WHERE cr.remaining_balance = 0 AND cr.invoice_id = invoice.id AND cr.is_deleted = 0 AND cr.is_active=1 LIMIT 1) as paid_date', false);
		}
		// GET TOTAL
		else
		{
			$this->db->select('SUM(total_due) as total_due, SUM('.$paid_amount.') as paid_amount', false);
			unset($param['limit']);
		}
		
		if( ! empty($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if( ! empty($param['date']))
		{
			$this->db->where('invoice.date_created BETWEEN "'.$param['date']['from'].'" AND "'.$param['date']['to'].'"');			
		}
		
		$this->db->where(['invoice.is_deleted'=>0,'invoice.is_active'=>1]);
		$this->db->where('invoice.company_id', $company_id);
		
		$this->db->join('clients', 'clients.id = invoice.client_id', 'left');
		
		if( ! $count)
		{
			if( ! empty($param['limit']))
			{
				$offset = isset($param['offset']) ? $param['offset'] : 0;
				$this->db->limit($param['limit'], $offset);
			}
			
			if( ! empty($param['order_by']))
			{
				$sort_by = isset($param['sort_by']) ? $param['sort_by'] : 'asc';
				$this->db->order_by($param['order_by'], $sort_by);
			}
			
			$result = $this->db->get('invoice')->result();


			$this->db->where_in('slug', ['debitable-ewt', 'output-vat', 'deferred-vat', 'creditable-wht']);
			$ledgers = $this->db->get('accounting_ledgers')->result();
			$ledgers = dropdown_array($ledgers, 'slug', 'id');

			foreach($result as $row)
			{

				// get entry
				$entry = $this->db->get_where('accounting_entries', ['invoice_id' => @$row->id, 'invoice_type' => 'invoice'])->row();
				$row->accounting_entry_id = @$entry->id;


				// get other accounts
				$this->db->select('items.id, items.ledger_id, accounting_ledgers.name as ledger_name, items.amount');
				$this->db->select('
					IF(items.dc = "c", items.amount, 0) as `credit`,
					IF(items.dc = "d", items.amount, 0) as `debit`,
				');
				$this->db->where('accounting_ledgers.group_id !=', 6);
				// $this->db->where('accounting_ledgers.id !=', 4); //cwt
				$this->db->where('accounting_entry_id', $row->accounting_entry_id);
				$this->db->join('accounting_ledgers', 'accounting_ledgers.id = items.ledger_id');
				$result_ledgers = $this->db->order_by('items.id', 'asc')->get('entry_items as items')->result();
				


				$row->output_vat = 0;
				$row->deffered_vat = 0;
				$row->creditable_wht = 0;

				$tmp_ledgers = [];

				foreach($result_ledgers as $tmp)
				{
					// get output vat
					if($tmp->ledger_id == $ledgers['output-vat'])
					{
						$row->output_vat = $tmp->amount;
						continue;
					}
					
					// get deffered vat
					if($tmp->ledger_id == $ledgers['deferred-vat'])
					{
						$row->deffered_vat = $tmp->amount;
						continue;
					}
					
					// get creditable wht
					if($tmp->ledger_id == $ledgers['creditable-wht'])
					{
						$row->creditable_wht = $tmp->amount;
						continue;
					}
					
					$tmp_ledgers[] = $tmp;
				}
				
				$row->ledgers = $tmp_ledgers;

			}
			


			return $result;
		}
		else
		{
			return $this->db->count_all_results('invoice');
		}
		
	}
	
	// ==SALES REPORT END==
	
	// PURCHASES REPORT
	
	public function get_supplier_invoice($param=[], $count=false)
	{
		$paid_amount = '(
			CASE 
					WHEN payment_requests.id IS NOT NULL 
					THEN (
						SELECT SUM(paid_amount) FROM payment_voucher as pv WHERE pv.payment_request_id = payment_requests.id AND pv.is_deleted = 0 AND pv.is_active=1
					)
				    ELSE (
				    	SELECT SUM(paid_amount) FROM payment_voucher as pv WHERE pv.vendor_invoice_id = vendor_invoice.id AND pv.is_deleted = 0 AND pv.is_active=1
				    )
			END
		)';


		$paid_date = '(
			CASE 
				WHEN payment_requests.id IS NOT NULL 
				THEN (
					SELECT payment_date FROM payment_voucher as pv WHERE pv.remaining_balance = 0 AND pv.payment_request_id = payment_requests.id AND 	pv.is_deleted = 0 AND pv.is_active=1 LIMIT 1
				)
			    ELSE (
			    	SELECT payment_date FROM payment_voucher as pv WHERE pv.remaining_balance = 0 AND pv.vendor_invoice_id = vendor_invoice.id AND 	pv.is_deleted = 0 AND pv.is_active=1 LIMIT 1
			    )
			END
		)';



		
		if(empty($param['total']))
		{
			$this->db->select('vendor_invoice.id, vendor_invoice.invoice_number, suppliers.name, vendor_invoice.date_created, vendor_invoice.date_due, vendor_invoice.total_due, '.$paid_amount.' as paid_amount', false);
			$this->db->select('('.$paid_date.') as paid_date', false);
		}
		// GET TOTAL
		else
		{
			$this->db->select('SUM(vendor_invoice.total_due) as total_due, SUM('.$paid_amount.') as paid_amount', false);
			unset($param['limit']);
		}
		
		if( ! empty($param['where']))
		{
			$this->db->where($param['where']);
		}
		
		if( ! empty($param['date']))
		{
			$this->db->where('vendor_invoice.date_created BETWEEN "'.$param['date']['from'].'" AND "'.$param['date']['to'].'"');			
		}
		
		$this->db->where(['vendor_invoice.is_deleted'=>0,'vendor_invoice.is_active'=>1]);
		$this->db->where('vendor_invoice.company_id', $company_id);
	
		$this->db->join('suppliers', 'suppliers.id = vendor_invoice.supplier_id', 'left');
		$this->db->join('payment_requests', 'payment_requests.vendor_invoice_id = vendor_invoice.id AND payment_requests.is_deleted = 0', 'left');
		
		if( ! $count)
		{
			if( ! empty($param['limit']))
			{
				$offset = isset($param['offset']) ? $param['offset'] : 0;
				$this->db->limit($param['limit'], $offset);
			}
			
			if( ! empty($param['order_by']))
			{
				$sort_by = isset($param['sort_by']) ? $param['sort_by'] : 'asc';
				$this->db->order_by($param['order_by'], $sort_by);
			}
			
			$result = $this->db->get('vendor_invoice')->result();

			$this->db->where_in('slug', ['debitable-ewt', 'output-vat', 'deferred-vat', 'creditable-wht', 'input-vat']);
			$ledgers = $this->db->get('accounting_ledgers')->result();
			$ledgers = dropdown_array($ledgers, 'slug', 'id');

			foreach($result as $row)
			{

				// get entry
				$entry = $this->db->get_where('accounting_entries', ['invoice_id' => @$row->id, 'invoice_type' => 'vendor_invoice'])->row();
				$row->accounting_entry_id = @$entry->id;


				// get other accounts
				$this->db->select('items.id, items.ledger_id, accounting_ledgers.name as ledger_name, items.amount');
				$this->db->select('
					IF(items.dc = "c", items.amount, 0) as `credit`,
					IF(items.dc = "d", items.amount, 0) as `debit`,
				');
				$this->db->where('accounting_ledgers.group_id !=', 6);
				// $this->db->where('accounting_ledgers.id !=', 4); //cwt
				$this->db->where('accounting_entry_id', $row->accounting_entry_id);
				$this->db->join('accounting_ledgers', 'accounting_ledgers.id = items.ledger_id');
				$this->db->order_by('items.dc', 'asc');
				$result_ledgers = $this->db->order_by('items.id', 'asc')->get('entry_items as items')->result();
				


				$row->output_vat = 0;
				$row->deffered_vat = 0;
				$row->creditable_wht = 0;
				$row->input_vat = 0;

				$tmp_ledgers = [];

				foreach($result_ledgers as $tmp)
				{
					if($tmp->ledger_id == $ledgers['input-vat'])
					{
						$row->input_vat = $tmp->amount;
						continue;
					}
					
					$tmp_ledgers[] = $tmp;
				}
				
				$row->ledgers = $tmp_ledgers;

			}


		
			return $result;
		}
		else
		{
			return $this->db->count_all_results('vendor_invoice');
		}
		
	}
	
	// ==PURCHASES REPORT==
	
	public function get_disbursement($params = [], $count = 0)
	{
		$cash_bank_group_id = 6;
		$cash_payment_type = 2;


		$this->db->where_in('slug', ['debitable-ewt', 'input-vat']);
		$ledgers = $this->db->get('accounting_ledgers')->result();
		$ledgers = dropdown_array($ledgers, 'slug', 'id');


		if(! empty($params['bank_ledger_id']))
		{
			$this->db->where('payment_voucher.bank_ledger_id', $params['bank_ledger_id']);
		}

		if(! empty($params['payment_type']))
		{
			$this->db->where('payment_voucher.payment_type', $params['payment_type']);
		}


		if(! empty($params['from']) && ! empty($params['to']))
		{
			$this->db->where('accounting_entries.payment_date BETWEEN "'.$params['from'].'" AND "'. $params['to'] .'"');
		}

		$this->db->where('accounting_entries.entry_type', $cash_payment_type);
		$this->db->where('accounting_entry_items.ledger_id NOT IN (SELECT id FROM ledgers WHERE group_id = '. $cash_bank_group_id .')');
		$this->db->where_not_in('accounting_entry_items.ledger_id', $ledgers);
		$this->db->where('accounting_entries.company_id', $company_id);

		$tax_sql = "SELECT 
				SUM(item.amount) as total_tax
			FROM entry_items as item 
			WHERE 
				item.accounting_entry_id = accounting_entries.id AND 
				item.ledger_id = ". $ledgers['input-vat'];

		$ewt_sql = "SELECT 
				SUM(item.amount) as total_tax
			FROM entry_items as item 
			WHERE 
				item.accounting_entry_id = accounting_entries.id AND 
				item.ledger_id = ". $ledgers['debitable-ewt'];


		$this->db->select('
			accounting_entries.id as accounting_entry_id,
			accounting_entries.payment_date,
			accounting_ledgers.name as ledger_name, payment_voucher.payment_bank, payment_voucher.payment_number, payment_voucher.payment_date,
			suppliers.name as supplier_name,
			IFNULL(('.$tax_sql.'), 0) as input_tax,
			IFNULL(('.$ewt_sql.'), 0) as ewt,
			IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0) as `debit`,
			IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0) as `credit`,
			bank.amount as bank_amount,
			bank.ledger_name as bank_ledger_name
		');


		$this->db->join('accounting_entry_items', 'accounting_entry_items.accounting_entry_id = accounting_entries.id', 'left');
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
		$this->db->join('payment_voucher', 'payment_voucher.id = accounting_entries.invoice_id', 'left');
		$this->db->join('suppliers', 'suppliers.id = payment_voucher.supplier_id', 'left');

		$this->db->join('(
			SELECT item.accounting_entry_id, item.amount, accounting_ledgers.name as ledger_name 
			FROM entry_items as item
			LEFT JOIN ledgers ON accounting_ledgers.id = item.ledger_id
			WHERE item.ledger_id IN (SELECT id FROM ledgers WHERE group_id = '. $cash_bank_group_id .')
		) as bank', 'bank.accounting_entry_id = accounting_entries.id', 'left');

		if(! empty($params['limit']) && ! $count)
		{
			$this->db->limit($params['limit'], $params['offset']);
		}
		return ($count)? 
			$this->db->count_all_results('accounting_entries') : $this->db->get('accounting_entries')->result();
	}

	public function get_balance_sheet($params = [], $count = 0)
	{
		if( ! empty($params['year']))
		{
			$this->db->where('YEAR(accounting_entries.payment_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']))
		{
			$this->db->where('DATE_FORMAT(accounting_entries.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['from']) && ! empty($params['to']))
		{
			$this->db->where('accounting_entries.payment_date BETWEEN "'. $params['from'] .'" AND "'. $params['to'] .'"');
		}

		if( ! empty($params['entry_type']))
		{
			$this->db->where('accounting_entries.entry_type', $params['entry_type']);
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('accounting_entries.project_id', $params['project_id']);
		}


		if(isset($params['account']))
		{
			if(is_array($params['account']))
			{
				$this->db->where_in('account.slug', $params['account']);
			}
			else
			{
				$this->db->where('account.slug', $params['account']);
			}
		}

		if(! empty($params['group_id']))
		{

			$this->db->where_in('accounting_groups.id', $params['group_id']);
		}


		if(! empty($params['exclude_group_id']))
		{
			$this->db->where_not_in('accounting_groups.id', $params['exclude_group_id']);
		}



		if( isset($params['not_group']) && !empty($params['not_group']) ){
			$this->db->select('accounting_entry_items.accounting_entry_id,accounting_entry_items.id, accounting_ledgers.name, accounting_entry_items.amount, accounting_entry_items.dc');
		}  else {
			$this->db->select('
					accounting_ledgers.name as ledger_name,
					SUM(IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0)) AS `debit_total`, 
					SUM(IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0)) AS `credit_total`,
			', false);


			if(($params['account'] == 'liabilities-credit-cards')||($params['account'] == 'equity'))
			{
				$this->db->select('(SUM(IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0)) - SUM(IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0))) AS `total`',false);
			}
			else
			{
				$this->db->select('(SUM(IF(accounting_entry_items.dc = "d", accounting_entry_items.amount, 0)) - SUM(IF(accounting_entry_items.dc = "c", accounting_entry_items.amount, 0))) AS `total`',false);
			}
		}	

		$this->db->where([
			'accounting_entries.is_approve' => 1, 
			'accounting_entries.company_id' => $params['company_id']
		]);

		$this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id', 'left');
		$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id', 'left');
		$this->db->join('accounting_groups as account', 'account.id = accounting_ledgers.account_id', 'left');
		$this->db->join('accounting_groups', 'accounting_groups.id = accounting_ledgers.group_id', 'left');

		$this->db->where('accounting_entry_items.deleted_at', NULL);
		$this->db->where('accounting_entries.deleted_at', NULL);
		$this->db->where('accounting_ledgers.deleted_at', NULL);
		$this->db->where('accounting_groups.deleted_at', NULL);

		if( !isset($params['not_group']) || empty($params['not_group']) ){
			$this->db->group_by('accounting_entry_items.ledger_id');
		}

		return $this->db->get('accounting_entry_items')->result();
	}
	
}