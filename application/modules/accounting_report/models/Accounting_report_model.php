<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Accounting_report_model extends MY_Model {

	public $table = 'accounting_reports'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'name', 
		'description', 
		'content', 
		'header', 
		'footer',
		'company_id',
		'filters',
		'signatory',
		'slug',
		'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];
	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_many['logs']  = array('foreign_model'=>'accounting_report_log_model','foreign_table'=>'accounting_report_logs','foreign_key'=>'accounting_report_id','local_key'=>'id');

		$this->has_one['company']  = array('foreign_model'=>'company/company_model','foreign_table'=>'companies','foreign_key'=>'id','local_key'=>'company_id');
	}

	public function insert_default(){

		// -- Adminer 4.2.2 MySQL dump

		// SET NAMES utf8;
		// SET time_zone = '+00:00';
		// SET foreign_key_checks = 0;
		// SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

		// INSERT INTO `accounting_reports` (`id`, `name`, `description`, `filters`, `signatory`, `slug`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
		// (1,	'Collection Report',	'Collection Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'collection_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (2,	'Accounts Receivable Report',	'Accounts Receivable Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'accounts_receivable_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (3,	'Past Due Accounts',	'Past Due Accounts',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'past_due_accounts',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (4,	'Fully Paid Accounts',	'Fully Paid Accounts',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'fully_paid_accounts',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (5,	'Collectible Report',	'Collectible Report',	'{\"0\":\"project_id\",\"1\":\"daterange\",\"2\":\"collectible_type\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'collectible_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (6,	'Aging of Accounts',	'Aging of Accounts',	'{\"0\":\"project_id\",\"1\":\"date\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'aging_of_accounts',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (7,	'Lot Inventory',	'Lot Inventory',	'{\"0\":\"project_id\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'lot_inventory',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (8,	'AR Principal Monitoring',	'AR Principal Monitoring',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'ar_principal_monitoring',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (9,	'Downpayment Monitoring',	'Downpayment Monitoring',	'{\"0\":\"project_id\",\"1\":\"date\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'downpayment_monitoring',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (10,	'Paid Up Sales',	'Paid Up Sales',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'paid_up_sales',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (11,	'Monthly Unit Inventory Report',	'monthly_unit_inventory',	'{\"0\":\"project_id\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'monthly_unit_inventory',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (12,	'Transferred Title Report',	'Transferred Title Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'transferred_title_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (13,	'Net Reservation Sales Monitoring',	'Net Reservation Sales Monitoring',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'net_reservation_sales',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (14,	'Collection Monitoring',	'Collection Monitoring',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'collection_monitoring',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (15,	'Projects by TCP',	'Projects by TCP',	'{\"0\":\"project_id\",\"1\":\"sub_type\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'projects_by_tcp',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (16,	'Actual Collection Report',	'Actual Collection Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'actual_collection_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (17,	'Sales Report',	'Sales Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'sales_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (18,	'Summary of Current and Non Current Accounts Receivable',	'Summary of Current and Non Current Accounts Receivable',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'current_nc_ar',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL);

		// -- 2020-05-10 11:44:50

	}

	public function get_day_periods($start,$end){

        $dstart = date("Y-m-d",strtotime($start));
        $dend = date("Y-m-d", strtotime($end));

        $diff = date_converter($dstart, $dend);
        $count = 0;
        $rem = 0;

        if ($diff->format("%a") > 0)
        {
            $count =  $diff->format("%a");
        }

        $count = (int) $count;

        for ($i = 0; $i <= $count; $i++)
        {

			$date_initial = date('Y-m-d', strtotime($dstart . ' +'.$i.' day'));
            $months[] = $date_initial;

        }

        return $months;
	}

	public function get_week_periods($start,$end){
		$start_day = date('Y-m-d');
        $dstart = date("Y-m-d",strtotime($start));
        $dend = date("Y-m-d", strtotime($end));

        $diff = date_converter($dstart, $dend);
        $count = 0;
        $rem = 0;

        if ($diff->format("%a") > 0)
        {
            $count =  $diff->format("%a") / 7;
        }

        $count = (int) $count;
        $start_day = $dstart;

        for ($i = 0; $i <= $count; $i++)
        {
            $days1 = 7 * $i;
            $days2 = (7 * $i) + 7;

            $date_initial = date('Y/m/d', strtotime($start_day . "+" . $days1 . " days"));
            $date_til =  date('Y/m/d', strtotime($start_day . "+" . $days2 . " days"));

            $months[] = $date_initial."-".$date_til;
        }

        return $months;
	}

	public function get_periods($start,$end)
	{
		$start 		= new DateTime($start);
		$start->modify('first day of this month');
		$end 		= new DateTime($end);
		$end->modify('first day of next month');
		$interval 	= DateInterval::createFromDateString('1 month');
		$period 	= new DatePeriod($start, $interval, $end);

		return $period;
	}

	public function get_year_periods($start,$end)
	{
		$start 		= new DateTime($start);
		$start->modify('first day of this year');
		$end 		= new DateTime($end);
		$end->modify('first day of next year');
		$interval 	= DateInterval::createFromDateString('1 year');
		$period 	= new DatePeriod($start, $interval, $end);

		return $period;
	}

	public function collection_report($params = [], $count = false)
	{
		$this->db->select('
			transaction_official_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
		');
		

		if( ! empty($params['group_by']) )
		{
			$this->db->select('
				SUM(transaction_official_payments.principal_amount) as total_principal,
				SUM(transaction_official_payments.interest_amount) as total_interest,
				SUM(transaction_official_payments.amount_paid) as t_amount,
				COUNT(transaction_official_payments.id) as t_count,
			',false);

			$this->db->group_by('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m")');
		}

		$this->db->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('transaction_payments', 'transaction_payments.id = transaction_official_payments.transaction_payment_id', 'left');
		

		if( ! empty($params['sub_type_id']))
		{
			// $this->db->join('properties', 'projects.id = properties.project_id', 'left');
			$this->db->join('house_models', 'house_models.id = transaction_properties.house_model_id', 'left');
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);

		}

		if( ! empty($params['period_id']) )
		{
			$this->db->where('transaction_official_payments.period_id', $params['period_id']);
		}

		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transaction_official_payments.payment_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_official_payments.payment_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_official_payments.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transaction_official_payments')->result_array();
	}

	public function ar_report($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transactions.seller_id,
			transactions.financing_scheme_id,
			transactions.reference,
			transactions.reservation_date,
		');

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		
		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['transaction_id']) )
		{
			$this->db->where('transaction_payments.transaction_id', $params['transaction_id']);

		}

		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_payments.due_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		$this->db->where('transaction_payments.deleted_at IS NULL');

		$this->db->where('transactions.general_status !=', 4);

		return $this->db->get('transaction_payments')->result_array();
	}


	public function past_due_accounts($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transactions.buyer_id,
		');

		$this->db->select('
			SUM(transaction_payments.principal_amount) as total_principal,
			SUM(transaction_payments.interest_amount) as total_interest,
			SUM(transaction_payments.total_amount) as t_amount,
			COUNT(transaction_payments.id) as t_count,
		',false);

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		
		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['date']) )
		{
			
			$this->db->where('transaction_payments.due_date < ', $params['date']);
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		$this->db->where('transaction_payments.is_paid',0);

		$this->db->group_by('transaction_payments.transaction_id');

		return $this->db->get('transaction_payments')->result_array();
	}


	public function fully_paid_accounts($params = [], $count = false)
	{
		$this->db->select('
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transactions.reference,
			properties.cts_number,
			properties.doas_notarized_date,
			properties.lot_area,
		');

		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('properties', 'properties.id = transaction_properties.property_id', 'left');
		
		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			// $this->db->where('transaction_payments.due_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		// $this->db->where('transactions.collection_status', 3);

		return $this->db->get('transactions')->result_array();
	}

	public function aging_of_accounts($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transactions.buyer_id,
		');

		$this->db->select('
			SUM(transaction_payments.principal_amount) as total_principal,
			SUM(transaction_payments.interest_amount) as total_interest,
			SUM(transaction_payments.total_amount) as t_amount,
			COUNT(transaction_payments.id) as t_count,

		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 1 AND 30, SUM(transaction_payments.principal_amount), 0) as total_principala,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 31 AND 60, SUM(transaction_payments.principal_amount), 0) as total_principalb,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 61 AND 90, SUM(transaction_payments.principal_amount), 0) as total_principalc,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) > 90, SUM(transaction_payments.principal_amount), 0) as total_principald,

		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 1 AND 30, SUM(transaction_payments.interest_amount), 0) as total_interesta,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 31 AND 60, SUM(transaction_payments.interest_amount), 0) as total_interestb,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 61 AND 90, SUM(transaction_payments.interest_amount), 0) as total_interestc,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) > 90, SUM(transaction_payments.interest_amount), 0) as total_interestd,


		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 1 AND 30, SUM(transaction_payments.total_amount), 0) as total_amounta,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 31 AND 60, SUM(transaction_payments.total_amount), 0) as total_amountb,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 61 AND 90, SUM(transaction_payments.total_amount), 0) as total_amountc,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) > 90, SUM(transaction_payments.total_amount), 0) as total_amountd
		',false);

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		

		if( ! empty($params['date']) )
		{
			
			$this->db->where('transaction_payments.due_date < ', $params['date']);
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		$this->db->where('transaction_payments.is_paid',0);

		$this->db->group_by('transaction_payments.transaction_id');

		return $this->db->get('transaction_payments')->result_array();
	}

	public function collectible_report($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transactions.seller_id,
			transactions.financing_scheme_id,
			transactions.reference,
			transactions.reservation_date,
		');

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		
		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
			$this->db->select('
				SUM(transaction_payments.principal_amount) as total_principal,
				SUM(transaction_payments.interest_amount) as total_interest,
				SUM(transaction_payments.total_amount) as t_amount,
				COUNT(transaction_payments.id) as t_count,
			',false);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
			$this->db->select('
				SUM(transaction_payments.principal_amount) as total_principal,
				SUM(transaction_payments.interest_amount) as total_interest,
				SUM(transaction_payments.total_amount) as t_amount,
				COUNT(transaction_payments.id) as t_count,
			',false);
		}

		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_payments.due_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transaction_payments')->result_array();
	}


	public function lot_inventory($params = [], $count = false)
	{
		$this->db->select('
			properties.*,
			transactions.buyer_id,
		');

		$this->db->join('transaction_properties', 'transaction_properties.property_id = properties.id', 'left');
		$this->db->join('transactions', 'transaction_properties.transaction_id = transactions.id', 'left');
		

		if( ! empty($params['project_id']))
		{
			$this->db->where('properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');

		return $this->db->get('properties')->result_array();
	}


	public function ar_principal_monitoring($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transaction_payments.beginning_balance,
		');

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		
		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_payments.due_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transaction_payments')->result_array();
	}

	public function downpayment_monitoring($params = [], $count = false)
	{
		$this->db->select('
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transactions.seller_id,
			transactions.id as transaction_id,  
			transactions.financing_scheme_id,
			transactions.reservation_date,
			properties.lot_area,
			properties.floor_area,
			properties.model_id,
		');

		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('properties', 'properties.id = transaction_properties.property_id', 'left');
		// $this->db->join('transaction_payments', 'transaction_payments.id = transaction_official_payments.transaction_payment_id', 'left');

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		// $this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transactions')->result_array();
	}

	public function paid_up_sales($params = [], $count = false)
	{
		$this->db->select(
			'transaction_official_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transactions.buyer_id'
		);

		$this->db->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('transaction_payments', 'transaction_payments.id = transaction_official_payments.transaction_payment_id', 'left');
		
		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transaction_official_payments.payment_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_official_payments.payment_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_official_payments.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transaction_official_payments')->result_array();
	}

	public function monthly_unit_inventory($params = [], $count = false)
	{
		$this->db->select('
			projects.name as project_name,
			COUNT(properties.id) as total_count,
			SUM(properties.lot_area) as total_lot,
			SUM(properties.total_selling_price) as total_pesos,
		', FALSE);

		$this->db->join('properties', 'projects.id = properties.project_id', 'left');

		if( ! empty($params['sub_type_id']))
		{
			$this->db->join('house_models', 'house_models.id = properties.model_id', 'left');
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);

		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('projects.id', $params['project_id']);
		}

		if( ! empty($params['status']))
		{
			$this->db->where('properties.status', $params['status']);
		}

		$this->db->where('properties.deleted_at IS NULL');
		$this->db->where('projects.deleted_at IS NULL');

		$this->db->group_by('properties.project_id');

		return $this->db->get('projects')->row_array();
	}

	public function transferred_title_report($params = [], $count = false)
	{
		$this->db->select('
			projects.name as project_name,
			COUNT(properties.id) as total_count,
		', FALSE);

		$this->db->join('properties', 'projects.id = properties.project_id', 'left');

		if( ! empty($params['project_id']))
		{
			$this->db->where('projects.id', $params['project_id']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(properties.date_registered, "%Y-%m") = ', $params['year_month']);
		}

		$this->db->where('properties.deleted_at IS NULL');
		$this->db->where('projects.deleted_at IS NULL');

		$this->db->group_by('properties.project_id');

		return $this->db->get('projects')->row_array();
	}

	public function net_reservation_sales($params = [], $count = false)
	{
		$this->db->select('
			projects.name as project_name,
			COUNT(properties.id) as total_count,
		', FALSE);

		$this->db->join('properties', 'projects.id = properties.project_id', 'left');

		if( ! empty($params['project_id']))
		{
			$this->db->where('projects.id', $params['project_id']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(properties.date_registered, "%Y-%m") = ', $params['year_month']);
		}

		$this->db->where('properties.deleted_at IS NULL');
		$this->db->where('projects.deleted_at IS NULL');

		$this->db->group_by('properties.project_id');

		return $this->db->get('projects')->row_array();
	}

	public function sales_report($params = [], $count = false)
	{
		$this->db->select('
			SUM(transaction_properties.total_selling_price) as total_selling_price,
			COUNT(transactions.id) as t_count,
		',false);

		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		
		if( ! empty($params['group']) )
		{
			$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			$this->db->where('sellers.sales_group_id', $params['group']);
		}

		if( ! empty($params['position']) )
		{
			if( empty($params['group']) ){
				$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			}
			$this->db->where('sellers.seller_position_id', $params['position']);
		}

		if( ! empty($params['tops']) )
		{
			$this->db->select('
				sellers.first_name, sellers.last_name, sellers.sales_group_id, seller_positions.name as sp_name,
			',false);
			$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			$this->db->join('seller_positions', 'seller_positions.id = sellers.seller_position_id', 'left');
			$this->db->group_by('transactions.seller_id');
			$this->db->order_by('total_selling_price', 'DESC');
			$this->db->limit(10);
		}
		
		if( ! empty($params['sub_type_id']))
		{
			// $this->db->join('properties', 'projects.id = properties.project_id', 'left');
			$this->db->join('house_models', 'house_models.id = transaction_properties.house_model_id', 'left');
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);

		}

		if( ! empty($params['date']) )
		{
			$this->db->where('DATE(transactions.reservation_date) = ', $params['date']);
		}

		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transactions.reservation_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transactions.reservation_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['transaction_id']) )
		{
			$this->db->where('transactions.transaction_id', $params['transaction_id']);
		}


		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			$this->db->where('transactions.reservation_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');

		return $this->db->get('transactions')->result_array();
	}

	public function receivable($params = [], $count = false)
	{
		$this->db->select('
			SUM(transaction_payments.principal_amount) as total_principal,
			SUM(transaction_payments.interest_amount) as total_interest,
			SUM(transaction_payments.total_amount) as t_amount,
			COUNT(transaction_payments.id) as t_count,
		',false);

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		
		if( ! empty($params['date']) )
		{
			$this->db->where('DATE(transaction_payments.due_date) = ', $params['date']);
		}
		
		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_payments.due_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');
		// $this->db->group_by('transaction_payments.transaction_id');

		return $this->db->get('transaction_payments')->row_array();
	}
	public function collection($params = [], $count = false)
	{
		$this->db->select('
			SUM(transaction_official_payments.principal_amount) as total_principal,
			SUM(transaction_official_payments.interest_amount) as total_interest,
			SUM(transaction_official_payments.amount_paid) as t_amount,
			COUNT(transaction_official_payments.id) as t_count,
		',false);

		$this->db->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
			
		if( ! empty($params['date']) )
		{
			$this->db->where('DATE(transaction_official_payments.payment_date) = ', $params['date']);
		}

		if( ! empty($params['year']) )
		{
			$this->db->where('YEAR(transaction_official_payments.payment_date) = ', $params['year']);
		}

		if( ! empty($params['year_month']) )
		{
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if( ! empty($params['daterange']) )
		{
			$dates = explode('-', $params['daterange']);
	        $f_date_from = db_date($dates[0]);
	        $f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_official_payments.payment_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if( ! empty($params['project_id']))
		{
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_official_payments.deleted_at IS NULL');
		// $this->db->group_by('transaction_official_payments.transaction_id');

		return $this->db->get('transaction_official_payments')->row_array();
	}


	public function get_projects($params = [], $count = false)
	{
		$this->db->select('projects.id,projects.name',false);

		$this->db->join('house_models', 'house_models.project_id = projects.id', 'left');
		
		if( ! empty($params['sub_type_id']) )
		{
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);
		}

		if( ! empty($params['project_id']) )
		{
			$this->db->where('house_models.project_id', $params['project_id']);
		}

		$this->db->where('house_models.deleted_at IS NULL');
		$this->db->where('projects.deleted_at IS NULL');

		$this->db->group_by('house_models.project_id');

		return $this->db->get('projects')->result_array();
	}
	
}