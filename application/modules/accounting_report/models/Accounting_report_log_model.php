<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Accounting_report_log_model extends MY_Model {

	public $table = 'accounting_report_logs'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = ['accounting_report_id', 'slug', 'filters', 'signatory', 'created_at', 'created_by', 'deleted_at', 'deleted_by', 'updated_at', 'updated_by']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];
	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['report']  = array('foreign_model'=>'accounting_report_model','foreign_table'=>'accounting_reports','foreign_key'=>'id','local_key'=>'accounting_report_id');

	}

}