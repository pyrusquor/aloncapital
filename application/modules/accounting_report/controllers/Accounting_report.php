<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_report extends MY_Controller
{

	public $fields = [
		'slug' => array(
			'field' => 'slug',
			'label' => 'Template Name',
			'rules' => 'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Accounting_report_model', 'M_report');
		$this->load->model('standard_report/Standard_report_model', 'S_report');
		$this->load->model('Accounting_report_log_model', 'M_report_logs');
		$this->load->model('chart_of_accounts/Chart_of_accounts_model', 'Ledger_m');
		$this->load->model('accounting_groups/Accounting_groups_model', 'Group_m');

		$this->load->model('Report_model', 'Report_m');
		$this->load->model('project/Project_model', 'M_project');

		$transaction_models = array('transaction/Transaction_billing_model' => 'M_transaction_billing', 'transaction/Transaction_client_model' => 'M_transaction_client', 'transaction/Transaction_discount_model' => 'M_transaction_discount', 'transaction/Transaction_fee_model' => 'M_transaction_fee', 'transaction/Transaction_payment_model' => 'M_transaction_payment', 'transaction/Transaction_property_model' => 'M_transaction_property', 'transaction/Transaction_seller_model' => 'M_transaction_seller', 'transaction_commission/Transaction_commission_model' => 'M_Transaction_commission', 'ticketing/Ticketing_model' => 'M_Transaction_ticketing', 'transaction_post_dated_check/Transaction_post_dated_check_model' => 'M_transaction_post_dated_check', 'transaction_payment/Transaction_official_payment_model' => 'M_Transaction_official_payment', 'seller/Seller_model' => 'M_seller', 'buyer/Buyer_model' => 'M_buyer', 'transaction/Transaction_billing_logs_model' => 'M_Tbilling_logs', 'transaction_payment/Transaction_penalty_logs_model' => 'M_tpenalty_logs', 'transaction/Transaction_refund_logs_model' => 'M_refund_logs', 'project/Project_model' => 'M_Project');

		// Load models
		$this->load->model('transaction/Transaction_model', 'M_transaction');
		$this->load->model($transaction_models);

		// $this->account['company_id'] = 1;
	}

	public function index()
	{
		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index');
	}

	public function update($id = FALSE)
	{
		if ($id) {

			$this->view_data['form']  = $this->M_report->get($id);
			if ($post = $this->input->post()) {
				$this->form_validation->set_rules($this->fields);

				if ($this->form_validation->run() === TRUE) {
					$signatory = $post['signatory'];

					$data['signatory'] = json_encode($post['signatory']);
					$data['filters'] =  json_encode($post['filters']);
					$data['slug'] =  $post['slug'];
					$data['accounting_report_id'] =  $post['accounting_report_id'];
					$data['created_at'] =  NOW;
					$data['created_by'] =  $this->user->id;

					// $_ar_data['company_id'] =  $post['company_id'];

					// if($post['company_id']) {

					// 	$this->M_report->update($_ar_data, $id);

					// }

					$result = $this->M_report_logs->insert($data);

					if ($result !== FALSE) {

						redirect('accounting_report/generate/' . $result, 'refresh');
					}
				} else {

					// Error
					$this->notify->error('Oops something went wrong.');
				}
			}

			$this->template->build('update', $this->view_data);
		} else {

			show_404();
		}
	}

	public function view($id = FALSE, $debug = FALSE)
	{
		if ($id) {

			$data['accounting_report'] = $this->M_report->with_logs()->get($id);

			$this->template->build('view', $data);

			// vdebug($data);

		}
	}

	public function generate($id = FALSE, $debug = 0)
	{
		if ($id) {

			$this->view_data['data'] = $data =  $this->M_report_logs->with_report()->get($id);

			if ($data) {

				switch ($data['slug']) {
					case 'balance_sheet':
						$this->view_data['result'] = $this->balance_sheet($data, $debug);
						break;

					case 'trial_balance':
						$this->view_data['result'] = $this->trial_balance($data, $debug);
						break;

					case 'profit_loss':
						$this->view_data['result'] = $this->profit_loss($data);
						break;

					case 'general_ledger':
						$this->view_data['result'] = $this->general_ledger($data);
						break;

					case 'ledger_statement':
						$this->view_data['result'] = $this->ledger_statement($data);
						break;

					case 'project_ledger_statement':
						$this->view_data['result'] = $this->project_ledger_statement($data);
						break;

					case 'subsidiary_ledger_statement':
						$this->view_data['result'] = $this->subsidiary_ledger_statement($data, $debug);
						break;

					case 'journal_report':
						$this->view_data['result'] = $this->journal_report($data);
						break;

					case 'cash_summary':
						$this->view_data['result'] = $this->cash_summary($data);
						break;

					case 'journal_report':
						$this->view_data['result'] = $this->journal_report($data);
						break;

					case 'bank_summary':
						$this->view_data['result'] = $this->bank_summary($data);
						break;
					case 'reconciliation':
						$this->view_data['result'] = $this->reconciliation($data);
						break;

					default:
						# code...
						break;
				}

				if ($debug == 1) {
					vdebug($this->view_data);
				}

				$generateHTML = $this->load->view('pdf/' . $data['slug'], $this->view_data, true);

				echo $generateHTML;
				die();

				pdf_create($generateHTML, 'generated_form');
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function balance_sheet($data = array(), $debug = 0)
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = $dates[0];
		$post['f_date_to']  = $dates[1];

		$params = [];

		$company_id = $params['company_id'] = $data['filters']['company_id'];

		$project_id = $params['project_id'] = $data['filters']['project_id'];

		$date_from  = date('Y-m-01', strtotime("-1 years"));
		$date_to    = date('Y-m-t');

		if (!empty($post['f_date_from']) && !empty($post['f_date_to'])) {
			$date_from  = date('Y-m-01', strtotime($post['f_date_from']));
			$date_to    = date('Y-m-t', strtotime($post['f_date_to']));
		}

		$params['from']	 = date('Y-01-01', strtotime("-25 years"));
		// $params['from']	 = date('Y-01-01');
		$params['to']	 = $date_to;

		$asset_groups = get_asset_groups('id', true, true);
		$asset_groups = get_only_in_array(get_asset_groups('id', true, true), 'id');
		$cash_banks_group = get_only_in_array(get_all_by_slug(true, true, 'cash-on-hand-and-in-banks'), 'id');

		// 6 = cash and banks and 7 = money transit groups only
		$data['assets']['cash_banks'] = $this->Report_m->get_balance_sheet($params + ['group_id' => $asset_groups, 'account' => 'assets']);


		// if ($debug == "cash_banks") {
		// 	echo "<pre>";
		// 	echo $debug;
		// 	print_r($params + ['group_id' => $asset_groups, 'account' => 'assets']);echo "<br>";
		// 	vdebug($data['assets']['cash_banks']);
		// }


		$data['assets']['current_fix'] = $this->Report_m->get_balance_sheet($params + ['exclude_group_id' => $cash_banks_group, 'account' => 'assets']);

		// if ($debug == "current_fix") {
		// 	echo "<pre>";
		// 	echo $debug;
		// 	print_r($params + ['exclude_group_id' => $cash_banks_group, 'account' => 'assets']);echo "<br>";
		// 	vdebug($data['assets']['current_fix']);
		// }

		$data['liabilities'] = $this->Report_m->get_balance_sheet($params + ['account' => 'liabilities-credit-cards']);
		$data['equities'] = $this->Report_m->get_balance_sheet($params + ['account' => 'equity']);

		// NET INCOME
		$params['from']	 = date('Y-01-01');
		$params['to']	 = $date_to;
		// $params['not_group'] = 1;
		// echo "<pre>";print_r($params);
		$data['net_income']['assets']['cash_banks'] = $this->Report_m->get_balance_sheet($params + ['group_id' => $cash_banks_group, 'account' => 'assets']);

		// if ($debug == "net_cash_banks") {
		// 	vdebug($data['net_income']['assets']['cash_banks']);
		// }

		// vdebug(  $data['net_income']['assets']['cash_banks'] );
		$data['net_income']['assets']['current_fix'] = $this->Report_m->get_balance_sheet($params + ['exclude_group_id' => $cash_banks_group, 'account' => 'assets']);
		$data['net_income']['liabilities'] = $this->Report_m->get_balance_sheet($params + ['account' => 'liabilities-credit-cards']);

		// if ($debug == "net_liabilities") {
		// 	vdebug($data['net_income']['liabilities']);
		// }
		

		$data['net_income']['equities'] = $this->Report_m->get_balance_sheet($params + ['account' => 'equity']);

		// RETAINED
		// $params['from']	 = date('Y-01-01', strtotime($post['f_date_from']." -10 years"));
		// $params['to']	 = date('Y-12-31', strtotime($post['f_date_to']." -1 year"));
		// $params['from']	 = date('Y-01-01', strtotime($post['f_date_from'] . " -25 years"));
		// $params['to']	 = date('Y-m-t', strtotime($post['f_date_from']. " -1 day"));


		$params['to']	 = date('Y-m-d', strtotime($params['from'] . "-1 days"));
		$params['from']	 = date('Y-01-01', strtotime($params['to'] . " -25 years"));
		

		$data['retained']['assets']['cash_banks'] = $this->Report_m->get_balance_sheet($params + ['group_id' => $cash_banks_group, 'account' => 'assets']);

		// if ($debug == "retained_cash_banks") {
		// 	echo "<pre>";
		// 	print_r($post);
		// 	echo "<br>";
		// 	print_r($params);
		// 	vdebug($data['retained']['assets']['cash_banks']);
		// }

		$data['retained']['assets']['current_fix'] = $this->Report_m->get_balance_sheet($params + ['exclude_group_id' => $cash_banks_group, 'account' => 'assets']);

		// if ($debug == "retained_current_fix") {
		// 	vdebug($data['retained']['assets']['current_fix']);
		// }

		$data['retained']['liabilities'] = $this->Report_m->get_balance_sheet($params + ['account' => 'liabilities-credit-cards']);

		// if ($debug == "retained_liabilities") {
		// 	vdebug($data['retained']['liabilities']);
		// }

		$data['retained']['equities'] = $this->Report_m->get_balance_sheet($params + ['account' => 'equity']);

		// if ($debug == "retained_equities") {
		// 	vdebug($data['retained']['equities']);
		// }

		//$data = $this->accounting_report_m->get_balance_sheet_data($project_id, $date_from, $date_to);
		//$data['company'] = $this->user_bm->get_company($this->account['company_id']);

		$data['date_to'] = $date_to;


		return $data;
	}

	public function trial_balance($data = array(), $debug = 0)
	{
		$company_id = 1;

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$post = $data['filters'];

		$company_id = $params['company_id'] = $data['filters']['company_id'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = db_date($dates[0]);
		$post['f_date_to']  = db_date($dates[1]);

		$params = [];

		$data['date'] = ['from' =>  date('Y-01-01'), 'to' => date('Y-m-d')];

		if (!empty($post['f_date_from']) || !empty($post['f_date_to'])) {
			$data['date']['from'] = $params['from'] = $post['f_date_from'];
			$data['date']['to'] = $params['to']	= $post['f_date_to'];
		}

		// $company = $this->user_bm->get_company($this->account['company_id']);
		$company = array();
		$data['company'] 	= $company;

		// get account
		$this->db->where('accounting_groups.deleted_at', NULL);
		$this->db->where_in('company_id', [$company_id, 0]);
		$accounts = $this->db->get_where('accounting_groups', ['parent_id' => 0])->result();

		// vdebug($accounts);

		$rows = [];


		foreach ($accounts as $account) {
			$s_acct = [];
			$s_acct[] = $account->id;

			$this->db->where('accounting_groups.deleted_at', NULL);
			$this->db->where_in('company_id', [$company_id]);
			$sub_accts = $this->db->get_where('accounting_groups', ['parent_id' => $account->id])->result_array();

			if ($sub_accts) {


				foreach ($sub_accts as $key => $sub_acct) {

					$s_acct[] = $sub_acct['id'];

					$this->db->where_in('company_id', [$company_id]);
					$this->db->where('accounting_groups.deleted_at', NULL);
					$ssub_accts = $this->db->get_where('accounting_groups', ['parent_id' => $sub_acct['id']])->result_array();

					if ($ssub_accts) {

						foreach ($ssub_accts as $key => $ssub_acct) {

							$s_acct[] = $ssub_acct['id'];

							$this->db->where_in('company_id', [$company_id]);
							$this->db->where('accounting_groups.deleted_at', NULL);

							$sssub_accts = $this->db->get_where('accounting_groups', ['parent_id' => $ssub_acct['id']])->result_array();


							if ($sssub_accts) {

								foreach ($sssub_accts as $key => $sssub_acct) {

									$s_acct[] = $sssub_acct['id'];
								}
							}
						}
					}
				}
			}

			if ($s_acct) {
				# code...

				// get ledgers
				$this->db->select('accounting_entry_items.ledger_id, accounting_ledgers.name as ledger_name, accounting_ledgers.op_balance, accounting_ledgers.op_balance_dc');

				$this->db->select('
					SUM(IF(accounting_entry_items.dc = "D", accounting_entry_items.amount, 0)) as `dr_total`,
					SUM(IF(accounting_entry_items.dc = "C", accounting_entry_items.amount, 0)) as `cr_total`
				', false);

				// get starting balance
				$this->db->select("(SELECT SUM(IF(dc = 'D', amount, 0)) - SUM(IF(dc = 'C', amount, 0)) as amount
					FROM `accounting_entry_items` JOIN `accounting_entries` ON `accounting_entries`.`id` = `accounting_entry_items`.`accounting_entry_id` AND `accounting_entries`.`is_approve` = 1 
					WHERE DATE(accounting_entries.payment_date) < '" . $data['date']['from'] . "'
						AND accounting_entry_items.ledger_id = accounting_ledgers.id
						AND accounting_entries.company_id = " . $company_id . "
					) as starting_balance", false);

				$this->db->where_in('accounting_ledgers.group_id', $s_acct);
				$this->db->where('accounting_entries.is_approve', 1);
				$this->db->where('accounting_entries.company_id', $company_id);

				$this->db->where('DATE(accounting_entries.payment_date) >=', $data['date']['from']);
				$this->db->where('DATE(accounting_entries.payment_date) <=', $data['date']['to']);

				$this->db->where('accounting_entries.deleted_at', NULL);
				$this->db->where('accounting_entry_items.deleted_at', NULL);
				$this->db->where('accounting_ledgers.deleted_at', NULL);

				$this->db->join('accounting_ledgers', 'accounting_ledgers.id = accounting_entry_items.ledger_id');
				$this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id');

				$account->ledgers = $this->db->group_by('ledger_id')->get('accounting_entry_items')->result();
			}

			if ($debug == 2) {
				vdebug($account);
			}

			if (isset($account->ledgers) && !empty($account->ledgers)) {
				$rows[] = $account;
			}
		}
		// vdebug($rows);

		$data['rows'] = $rows;
		$data['period_label'] = 'As of ' . date('d F Y', strtotime($data['date']['from'])) . ' to ' . date('d F Y', strtotime($data['date']['to']));


		return $data;
	}

	public function profit_loss($data = array())
	{
		$data['filters'] = $filters = (array) json_decode($data['filters']);
		$data['signatory'] = $signatory = json_decode($data['signatory']);


		$post = $data['filters'];

		$company_id = $params['company_id'] = $data['filters']['company_id'];
		$project_id = $params['project_id'] = $data['filters']['project_id'];

		$dates = explode('-', $post['daterange']);

		$date_from = date('Y-m-01', strtotime($dates[0]));
		$date_to  = date('Y-m-t', strtotime($dates[1]));


		$period = $this->S_report->get_periods($date_from, $date_to);

		$months = array();

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}


		$rows = [];

		// 5260 = 102 opex
		// 5263 = 4 opex

		$income_groups = get_income_groups('id', true, true);
		$income_groups = get_only_in_array(get_income_groups('id', true, true), 'id');

		$operating_income = get_only_in_array(get_all_by_slug(true, true, 'operating-income'), 'id');
		$other_incomes = get_only_in_array(get_all_by_slug(true, true, 'other-income'), 'id');


		$expense_groups = get_expense_groups('id', true, true);
		$expense_groups = get_only_in_array(get_expense_groups('id', true, true), 'id');

		$operating_expenses = get_only_in_array(get_all_by_slug(true, true, 'operating-expenses'), 'id');
		$other_expenses = get_only_in_array(get_all_by_slug(true, true, 'other-expense'), 'id');

		// vdebug($income_groups);

		foreach ($months as $date) {
			$income = $this->Report_m->get_income_expense(['project_id' => $project_id, 'company_id' => $company_id, 'year_month' => $date, 'account' => 'income', 'group_id' => $income_groups, 'exclude_group_id' => $other_incomes]);

			$income_arr = $this->Report_m->get_income_expense(['project_id' => $project_id, 'company_id' => $company_id, 'year_month' => $date, 'account' => 'income', 'group_id' => $income_groups, 'exclude_group_id' => $other_incomes, 'type' => 'breakdown']);


			$other_income = $this->Report_m->get_income_expense(['project_id' => $project_id, 'company_id' => $company_id, 'year_month' => $date, 'account' => 'income', 'group_id' => $other_incomes]);

			$other_income_arr = $this->Report_m->get_income_expense(['project_id' => $project_id, 'company_id' => $company_id, 'year_month' => $date, 'account' => 'income', 'group_id' => $other_incomes, 'type' => 'breakdown']);


			// cost of good sold group only 
			$expense = $this->Report_m->get_income_expense([
				'year_month' => $date,
				'account' => 'expense',
				'group_id' => $expense_groups,
				'exclude_group_id' => $other_expenses,
			]);

			$expense_arr = $this->Report_m->get_income_expense([
				'year_month' => $date,
				'account' => 'expense',
				'group_id' => $expense_groups,
				'exclude_group_id' => $other_expenses,
				'type' => 'breakdown'
			]);

			// all expense except the cost of good sold group
			$other_expense = $this->Report_m->get_income_expense(['project_id' => $project_id, 'company_id' => $company_id, 'year_month' => $date, 'account' => 'expense', 'group_id' => $other_expenses]);

			// plus other income
			$other_expense_arr = $this->Report_m->get_income_expense(['project_id' => $project_id, 'company_id' => $company_id, 'year_month' => $date, 'account' => 'expense', 'group_id' => $other_expenses, 'type' => 'breakdown']);

			$rows[$date]['gross_income']  = $income->amount;
			$rows[$date]['gross_expense'] = $expense->amount;
			$rows[$date]['gross_profit'] = $income->amount - $expense->amount;

			$rows[$date]['other_expense'] = $other_expense->amount;
			$rows[$date]['net_profit']     =  $rows[$date]['gross_profit'] - $other_expense->amount;

			$rows[$date]['other_income']  = $other_income->amount;

			$rows[$date]['income_by_tax'] = $rows[$date]['other_income'] + $rows[$date]['net_profit'];

			//$rows[$date]['total_expense'] = $gross_expense->amount + $other_expense->amount;

			$rows[$date]['income_ledgers'] = $income_arr;
			$rows[$date]['other_income_ledgers'] = $other_income_arr;
			$rows[$date]['expense_ledgers'] = $expense_arr;
			$rows[$date]['other_expense_ledgers'] = $other_expense_arr;

			$rows[$date]['date'] = date('Y-m', strtotime($date . '-01'));
		}

		$income_ledgers = $expense_ledgers = $other_incomes = $other_expenses = [];

		foreach ($rows as $key_date => $row) {
			foreach ($row['income_ledgers'] as $val) {
				if (empty($val->ledger_id))
					continue;

				$income_ledgers[$val->ledger_id]['name'] = $val->ledger_name;
				$income_ledgers[$val->ledger_id]['months'][$key_date] = $val;
			}

			foreach ($row['expense_ledgers'] as $val) {
				if (empty($val->ledger_id))
					continue;

				$expense_ledgers[$val->ledger_id]['name'] = $val->ledger_name;
				$expense_ledgers[$val->ledger_id]['months'][$key_date] = $val;
			}

			foreach ($row['other_income_ledgers'] as $val) {
				if (empty($val->ledger_id))
					continue;

				$other_incomes[$val->ledger_id]['name'] = $val->ledger_name;
				$other_incomes[$val->ledger_id]['months'][$key_date] = $val;
			}

			foreach ($row['other_expense_ledgers'] as $val) {
				if (empty($val->ledger_id))
					continue;

				$other_expenses[$val->ledger_id]['name'] = $val->ledger_name;
				$other_expenses[$val->ledger_id]['months'][$key_date] = $val;
			}
		}


		// $company = $this->user_bm->get_company($this->account['company_id']);
		$company = array();
		$current_month = end($months);
		$period_label = 'For the month ended ' . date('t F Y', strtotime($current_month . '-01'));
		$data = compact(
			'rows',
			'income_ledgers',
			'expense_ledgers',
			'other_incomes',
			'other_expenses',
			'months',
			'company',
			'period_label',
			'code'
		);

		$data['filters'] = $filters;
		$data['signatory'] = $signatory;


		return $data;
	}

	public function general_ledger($data = array(), $debug = 0)
	{
		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$company_id = $params['company_id'] = $data['filters']['company_id'];

		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = db_date($dates[0]);
		$post['f_date_to']  = db_date($dates[1]);

		$data['date'] = ['from' =>  date('Y-01-01'), 'to' => date('Y-m-d')];
		$data['project'] = [];

		if (!empty($post['f_date_from']) || !empty($post['f_date_to'])) {
			$data['date']['from'] = $params['from'] = $post['f_date_from'];
			$data['date']['to'] = $params['to']	= $post['f_date_to'];
		}

		// $company = $this->user_bm->get_company($this->account['company_id']);
		$company = array();

		// get ledgers tree
		$data['ledgers_tree'] = $ledgers_tree = $this->Group_m->tree($company_id);

		$ledgers = $this->Report_m->ledger_summary($params);

		$data['rows'] = $rows = array_key_name($ledgers, 'ledger_id');


		$data['company'] = $company;

		$data['period_label'] = 'From ' . date('d F Y', strtotime($data['date']['from'])) . ' to ' . date('d F Y', strtotime($data['date']['to']));


		if ($debug) {
			vdebug($data);
		}

		return $data;
	}

	public function ledger_statement($data = array())
	{


		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$company_id = $data['filters']['company_id'];


		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = db_date($dates[0]);
		$post['f_date_to']  = db_date($dates[1]);

		$data['date'] = ['from' =>  date('Y-01-01'), 'to' => date('Y-m-d')];
		$data['project'] = [];

		$params = [];

		$title = 'Ledger Statement';
		$date = ['from' => $post['f_date_from'], 'to' => $post['f_date_to']];

		if (!empty($post['f_date_from']) || !empty($post['f_date_to'])) {
			$date['from'] = $post['f_date_from'];
			$date['to']	= $post['f_date_to'];
		}

		if (!empty($post['ledger_id'])) {
			$params['ledger_id'] = $post['ledger_id'];

			// get ledger name
			$ledger = $this->db->get_where('accounting_ledgers', ['id' => $post['ledger_id']])->row();
			$title = 'Ledger Statement - ' . $ledger->name;
		}


		$data['date']		= $date;
		$params['from']		= $date['from'];
		$params['to']		= $date['to'];
		$params['company_id']		= $company_id;


		$data['tree'] = $this->Report_m->get_transactions($params);


		$data['company'] 	= 1;

		$data['period_label'] = 'From ' . date('d F Y', strtotime($data['date']['from'])) . ' to ' . date('d F Y', strtotime($data['date']['to']));

		$data['title'] = $title;



		return $data;
	}

	public function project_ledger_statement($data = array())
	{
		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$company_id = $data['filters']['company_id'];
		$this->view_data['projects'] = $projects = $this->M_project->order_by('name', 'ASC')->get_all();


		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = db_date($dates[0]);
		$post['f_date_to']  = db_date($dates[1]);

		$data['date'] = ['from' =>  date('Y-01-01'), 'to' => date('Y-m-d')];
		$data['project'] = [];

		$params = [];

		$title = 'Ledger Statement';
		$date = ['from' => $post['f_date_from'], 'to' => $post['f_date_to']];

		if (!empty($post['f_date_from']) || !empty($post['f_date_to'])) {
			$date['from'] = $post['f_date_from'];
			$date['to']	= $post['f_date_to'];
		}

		if (!empty($post['ledger_id'])) {
			$params['ledger_id'] = $post['ledger_id'];

			// get ledger name
			$ledger = $this->db->get_where('accounting_ledgers', ['id' => $post['ledger_id']])->row();
			$title = 'Ledger Statement - ' . $ledger->name;
		}


		$data['date']		= $date;
		$params['from']		= $date['from'];
		$params['to']		= $date['to'];
		$params['company_id']		= $company_id;

		if ($projects) {
			foreach ($projects as $key => $project) {
				$params['where']['project_id'] = $project_id = $project['id'];
				$params['where']['ledger_id'] = $post['ledger_id'];

				$rows[$project_id]['ledger'] = $this->Report_m->ledger_summary($params);
				$rows[$project_id]['project_name'] = $project['name'];
			}
		}


		$data['period_label'] = 'From ' . date('d F Y', strtotime($data['date']['from'])) . ' to ' . date('d F Y', strtotime($data['date']['to']));

		$data['title'] = $title;

		$data['ledger'] = $ledger->name;
		$data['rows'] = $rows;

		return $data;
	}

	public function subsidiary_ledger_statement($data = array(), $debug = 0)
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);
		$company_id = $data['filters']['company_id'];

		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = db_date($dates[0]);
		$post['f_date_to']  = db_date($dates[1]);

		$data['date'] = ['from' =>  date('Y-01-01'), 'to' => date('Y-m-d')];

		$params = [];

		$title = 'Subsidiary Ledger Statement';
		$date = ['from' => date('Y-m-01'), 'to' => date('Y-m-d')];

		if (!empty($post['f_date_from']) || !empty($post['f_date_to'])) {
			$date['from'] = $post['f_date_from'];
			$date['to']	= $post['f_date_to'];
		}

		if (!empty($post['ledger_id'])) {
			$params['ledger_id'] = $post['ledger_id'];

			// get ledger name
			$this->db->where('deleted_at', NULL);
			$ledger = $this->db->get_where('accounting_ledgers', ['id' => $post['ledger_id']])->row();
			$title = 'Subsidiary Ledger Statement - ' . $ledger->name;
		}

		if (isset($post['project_id']) && (!empty($post['project_id']))) {
			$title .= " - " . get_value_field($post['project_id'], 'projects', 'name');
		}

		$data['date']		= $date;
		$params['from']	= $date['from'];
		$params['to']		= $date['to'];
		$params['company_id']  = $company_id;
		$params['payee_type'] = @$post['payee_type'];
		$params['payee_type_id'] = @$post['payee_type_id'];
		$params['project_id'] = $post['project_id'];
		$params['property_id'] = @$post['property_id'];

		$data['tree'] = $this->Report_m->get_transactions($params, $debug);

		$data['period_label'] = 'From ' . date('d F Y', strtotime($data['date']['from'])) . ' to ' . date('d F Y', strtotime($data['date']['to']));

		$data['title'] = $title;


		return $data;
	}

	public function journal_report($data = array(), $debug = 0)
	{
		$company_id = 1;

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);


		$post['f_date_from'] = db_date($dates[0]);
		$post['f_date_to']  = db_date($dates[1]);

		$data['date'] = ['from' =>  date('Y-01-01'), 'to' => date('Y-m-d')];
		$data['project'] = [];

		$params = [];

		$params['company_id'] = $data['filters']['company_id'];

		if (!empty($post['f_date_from']) || !empty($post['f_date_to'])) {
			$data['date']['from'] = $params['from'] = $post['f_date_from'];
			$data['date']['to'] = $params['to'] = $post['f_date_to'];
		} else {
			$params = [];

			$data['date'] = ['from' =>  date('Y-m-01'), 'to' => date('Y-m-d')];

			$params['from'] = $data['date']['from'];
			$params['to'] = $data['date']['to'];
		}

		$params['where']['journal_type'] = $post['transaction_type'];

		// $params = [];
		// $params['limit'] = ( ! @$download ? 100000000 : 0);
		// $data['total_rows'] = $this->Report_m->get_journal($params, true); 
		// $data['pagination'] = create_pagination('servequest/accounting_reports/journal/'.$code.'/'.$download, $data['total_rows'], $params['limit'], '#journal_list', 6);
		// $params['offset'] = $data['pagination']['offset'];

		$data['rows'] = $this->Report_m->get_journal($params);
		$data['total'] = $this->Report_m->get_journal_total($params);
		$title = 'Journal Report';

		// $data['company'] = $company;
		$data['period_label'] = 'From ' . date('d F Y', strtotime($data['date']['from'])) . ' to ' . date('d F Y', strtotime($data['date']['to']));

		$data['title'] = $title;

		if ($debug) {
			vdebug($data);
		}


		return $data;
	}

	public function cash_summary($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = db_date($dates[0]);
		$post['f_date_to']  = db_date($dates[1]);

		$data['date'] = ['from' =>  date('Y-01-01'), 'to' => date('Y-m-d')];
		$data['project'] = [];
		$param = [];

		$param['company_id'] = $company_id = $data['filters']['company_id'];



		// $params = [];

		// $post = $this->input->post(null, false);

		//    if($post)
		//    {
		//        $term   = json_encode($post);
		//        $code   = $this->Search_m->record_term($term);

		//        // monthrange
		//     $dates = explode('-', $post['monthrange']);

		//        $post['f_month'] = $dates[0];
		//        $post['f_year']  = $dates[1];

		//    }
		//    else if ($code)
		//    {
		//        $term   = $this->Search_m->get_term($code);
		//        $post   = json_decode($term, true);
		//    }

		$year = date('Y');
		$month = date('m');


		if (!empty($post['f_month']) && !empty($post['f_year'])) {
			$year = $post['f_year'];
			$month = sprintf('%02d', $post['f_month']);
		}

		// $company = $this->user_bm->get_company($this->account['company_id']);
		$company =  array();
		$data['company']    = $company;
		$this->view_data['chart'] = $chart = $this->Group_m->tree($company_id);


		$param['year'] = $data['year'] = $year;
		$param['month'] =  $data['month'] = $month;

		$data['period_label'] = 'For ' . date('F Y', strtotime($year . '-' . $month . '-01'));

		$this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'incomes']));

		// get income
		$data['incomes'] = [
			'month' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'incomes'])),
			'monthly_avg' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'incomes', 'monthly_avg' => true])),
		];

		// get operating expense
		$data['operating_expenses'] = [
			'month' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'operating_expenses'])),
			'monthly_avg' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'operating_expenses', 'monthly_avg' => true])),
		];

		// get non operating expense
		$data['operating_movements'] = [
			'month' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'non_operating_expenses'])),
			'monthly_avg' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'non_operating_expenses', 'monthly_avg' => true])),
		];


		// vdebug($data);
		$title = 'Cash Summary';


		$data['title'] = $title;

		$data['period_label'] = 'From ' . date('d F Y', strtotime($data['date']['from'])) . ' to ' . date('d F Y', strtotime($data['date']['to']));


		return $data;
	}

	public function bank_summary($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = db_date($dates[0]);
		$post['f_date_to']  = db_date($dates[1]);

		$data['date'] = ['from' =>  date('Y-01-01'), 'to' => date('Y-m-d')];
		$data['project'] = [];
		$param = [];

		$param['company_id'] = $company_id = $data['filters']['company_id'];

		$year = date('Y');
		$month = date('m');


		if (!empty($post['f_month']) && !empty($post['f_year'])) {
			$year = $post['f_year'];
			$month = sprintf('%02d', $post['f_month']);
		}

		// $company = $this->user_bm->get_company($this->account['company_id']);
		$company =  array();
		$data['company']    = $company;
		$this->view_data['chart'] = $chart = $this->Group_m->tree($company_id);


		$param['year'] = $data['year'] = $year;
		$param['month'] =  $data['month'] = $month;

		$data['period_label'] = 'For ' . date('F Y', strtotime($year . '-' . $month . '-01'));

		$this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'incomes']));

		// get income
		$data['incomes'] = [
			'month' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'incomes'])),
			'monthly_avg' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'incomes', 'monthly_avg' => true])),
		];

		// get operating expense
		$data['operating_expenses'] = [
			'month' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'operating_expenses'])),
			'monthly_avg' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'operating_expenses', 'monthly_avg' => true])),
		];

		// get non operating expense
		$data['operating_movements'] = [
			'month' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'non_operating_expenses'])),
			'monthly_avg' => $this->Report_m->get_cs_ledgers(array_merge($param, ['type' => 'non_operating_expenses', 'monthly_avg' => true])),
		];


		// vdebug($data);
		$title = 'Cash Summary';

		$data['title'] = $title;

		$data['period_label'] = 'From ' . date('d F Y', strtotime($data['date']['from'])) . ' to ' . date('d F Y', strtotime($data['date']['to']));


		return $data;
	}

	public function reconciliation($data = array())
	{
		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$post = $data['filters'];

		$dates = explode('-', $post['daterange']);

		$post['f_date_from'] = $dates[0];
		$post['f_date_to']  = $dates[1];


		$params = [];

		$params['company_id'] = $company_id = $data['filters']['company_id'];

		$date_from  = date('Y-m-01', strtotime("-1 years"));
		$date_to    = date('Y-m-t');

		if (!empty($post['f_date_from']) && !empty($post['f_date_to'])) {
			$date_from  = date('Y-m-01', strtotime($post['f_date_from']));
			$date_to    = date('Y-m-t', strtotime($post['f_date_to']));
		}


		$params['from']	 = date('Y-01-01', strtotime("-10 years"));
		$params['to']	 = $date_to;


		// $this->load->model('account_m');

		// $post = $this->input->post(null, false);
		$date =  $post['daterange'];

		// monthrange: 05-2020

		// if($post)
		// {
		// 	$term	= json_encode($post);
		// 	$code	= $this->Search_m->record_term($term);

		// 	// $post['monthrange'] = "03-2020";
		// 	// $post['from'] = "12-2016";
		// 	// $post['to'] = "12-2020";

		// 	$date =  $post['monthrange'];
		// 	$params['from'] = date('Y-m-01', strtotime(str_replace('-','/01/',$date)));
		// 	$params['to'] = date('Y-m-31', strtotime(str_replace('-','/01/',$date)));

		// }
		// else if ($code)
		// {
		// 	$term	= $this->Search_m->get_term($code);
		// 	$post	= json_decode($term, true);

		// 	$date =  $post['monthrange'];
		// 	$params['from'] = date('Y-m-01', strtotime(str_replace('-','/01/',$date)));
		// 	$params['to'] = date('Y-m-31', strtotime(str_replace('-','/01/',$date)));
		// }

		error_reporting(E_ALL);
		$title = 'Reconciliation Report';
		$data['date']	= $date;

		$params['dc'] = "C";
		$params['is_reconciled'] = "1";
		$data['tree']['checks'] = $this->Report_m->get_reconciliation($params);
		$params['dc'] = "D";
		$data['tree']['deposits'] = $this->Report_m->get_reconciliation($params);


		$params['dc'] = "C";
		$params['from'] = date('Y-m-01', strtotime(str_replace('-', '/01/', $date)));
		$params['to'] = date('Y-m-31', strtotime(str_replace('-', '/01/', $date)));
		$params['is_reconciled'] = "0";
		$data['tree_2']['checks'] = $this->Report_m->get_reconciliation($params);
		$params['dc'] = "D";
		$data['tree_2']['deposits'] = $this->Report_m->get_reconciliation($params);

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// die();

		$data['company'] 	= $company_id;

		$data['period_label'] = $date . " Bank Reconciliation";

		$data['title'] = $title;

		$data['company_id'] = $company_id;
		return $data;

		// $data['save_links'] = [
		// 	['link'=> 'servequest/accounting_reports/transactions/'.$code.'/pdf', 'name'=>'PDF Format'],
		// 	['link'=> 'servequest/accounting_reports/transactions/'.$code.'/excel', 'name'=>'Excel Format'],
		// ];

		// if($download == 'pdf')
		// {
		// 	// @TODO download pdf
		// 	$html = $this->load->view('reports/pdf/transactions', $data, true);
		// 	echo $html;die();
		//    	pdf_create($html, 'Ledger Statement', 5, array('letter','portrait'));
		// }
		// else if($download == 'excel')
		// {
		// 	$this->load->library('excel');
		// 	$excel = $this->excel;

		// 	$sheet = $excel->getActiveSheet();

		// 	$filename = 'Ledger Statement.xlsx';


		// 	$header_style = [
		// 		'alignment' => ['horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
		// 		'font' => ['bold' => TRUE]
		// 	];


		// 	$column_count = 4;

		// 	$sheet->SetCellValue('A1', "vanderbuilt internationale properties, inc.");
		// 	$sheet->mergeCells('A1:'. xls_column_name($column_count) .'1');


		// 	$sheet->SetCellValue('A2', $title);

		// 	$sheet->mergeCells('A2:'. xls_column_name($column_count) .'2');

		// 	$sheet->SetCellValue('A3', $data['period_label']);
		// 	$sheet->mergeCells('A3:'. xls_column_name($column_count) .'3');
		// 	$sheet->mergeCells('A4:'. xls_column_name($column_count) .'4');

		// 	$sheet->getStyle('A1:'. xls_column_name($column_count) .'3')->applyFromArray($header_style);


		// 	$sheet->getColumnDimension('A')->setAutoSize(true);
		// 	$sheet->getColumnDimension('B')->setAutoSize(true);
		// 	$sheet->getColumnDimension('C')->setWidth(20);
		// 	$sheet->getColumnDimension('D')->setWidth(20);
		// 	$sheet->getColumnDimension('E')->setWidth(20);

		// 	$row_ctr = 5;

		// 	$sheet->SetCellValue('A'.$row_ctr, 'Date');
		// 	$sheet->SetCellValue('B'.$row_ctr, 'Particulars');
		// 	$sheet->SetCellValue('C'.$row_ctr, 'Debit');
		// 	$sheet->SetCellValue('D'.$row_ctr, 'Credit');
		// 	$sheet->SetCellValue('E'.$row_ctr, 'Balance');

		// 	$sheet->getStyle("A".$row_ctr.":".xls_column_name($column_count).$row_ctr)->applyFromArray([
		// 		'font' => ['bold' => TRUE]
		// 	]);

		// 	$row_ctr++;
		// 	$padding = "     ";
		// 	foreach($data['tree'] as $accounts)
		// 	{
		// 		$sheet->SetCellValue('A'.$row_ctr, trim($accounts['name']));
		// 		$sheet->mergeCells('A'.$row_ctr.':'. xls_column_name($column_count).$row_ctr);
		// 		$sheet->getStyle("A".$row_ctr.":".xls_column_name($column_count).$row_ctr)->applyFromArray([
		// 			'font' => ['bold' => TRUE]
		// 		]);
		// 		$row_ctr++;

		// 		foreach($accounts['groups'] as $groups)
		// 		{
		// 			$sheet->SetCellValue('A'.$row_ctr, $padding . trim($groups['name']));
		// 			$sheet->mergeCells('A'.$row_ctr.':'. xls_column_name($column_count).$row_ctr);
		// 			$sheet->getStyle("A".$row_ctr.":".xls_column_name($column_count).$row_ctr)->applyFromArray([
		// 				'font' => ['bold' => TRUE]
		// 			]);
		// 			$row_ctr++;
		// 			$debit = $credit = $balance = 0;

		// 			if( ! empty($groups['starting']))
		// 			{
		// 				$sheet->SetCellValue('A'.$row_ctr, $padding.'Starting Balance');
		// 				$sheet->SetCellValue('E'.$row_ctr, $groups['starting']);
		// 				$balance += $groups['starting'];
		// 				$row_ctr++;
		// 			}

		// 			foreach($groups['entries'] as $entry)
		// 			{
		// 				$sheet->SetCellValue('A'.$row_ctr, $padding.nice_date($entry->date, 'F j, Y'));

		// 				$particular = ['Ref#: '.$entry->ref_no];
		// 				if( ! empty($entry->narration)) $particular[] = $entry->narration;
		// 				if( ! empty($entry->or_number)) $particular[] = 'O.R. Number: '. $entry->or_number;
		// 				if( ! empty($entry->invoice_number)) $particular[] = 'Invoice Number: '.$entry->invoice_number;
		// 				if( ! empty($entry->po_number)) $particular[] = 'P.O. Number: '.$entry->po_number;

		// 				$sheet->SetCellValue('B'.$row_ctr, implode(' | ', $particular));

		// 				if($entry->debit > 0)
		// 				{ 
		// 					$sheet->SetCellValue('C'.$row_ctr, $entry->debit); 
		// 					$debit += $entry->debit; 
		// 				} 

		// 				if($entry->credit > 0)
		// 				{ 
		// 					$sheet->SetCellValue('D'.$row_ctr, $entry->credit); 
		// 					$credit += $entry->credit; 
		// 				} 

		// 				$balance += ($entry->debit - $entry->credit);

		// 				$sheet->SetCellValue('E'.$row_ctr, $balance); 
		// 				$row_ctr++;
		// 			}


		// 			$sheet->SetCellValue('A'.$row_ctr, $padding.'Totals and Ending Balance'); 
		// 			$sheet->mergeCells('A'.$row_ctr.':B'.$row_ctr);
		// 			$sheet->getStyle("A".$row_ctr.":".xls_column_name($column_count).$row_ctr)->applyFromArray([
		// 				'font' => ['bold' => TRUE]
		// 			]);

		// 			$sheet->SetCellValue('C'.$row_ctr, $debit); 
		// 			$sheet->SetCellValue('D'.$row_ctr, $credit); 
		// 			$sheet->SetCellValue('E'.$row_ctr, $balance); 
		// 			$row_ctr++;

		// 			$sheet->SetCellValue('A'.$row_ctr, $padding.'Balance Change (Difference between starting and ending balances)'); 
		// 			$sheet->mergeCells('A'.$row_ctr.':B'.$row_ctr);
		// 			$sheet->getStyle("A".$row_ctr.":".xls_column_name($column_count).$row_ctr)->applyFromArray([
		// 				'font' => ['bold' => TRUE]
		// 			]);
		// 			$sheet->SetCellValue('E'.$row_ctr, $balance); 

		// 			$row_ctr++;
		// 			$sheet->mergeCells('A'.$row_ctr.':'. xls_column_name($column_count).$row_ctr);
		// 			$row_ctr++;
		// 		}
		// 	}



		// 	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// 	header('Content-Disposition: attachment;filename="'. $filename .'"');
		// 	header('Cache-Control: max-age=0');

		// 	$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');

		// 	// This line will force the file to download
		// 	$writer->save('php://output');

		// }
		// else if($this->input->is_ajax_request())
		// {

		// 	// echo "<pre>";print_r($data);die();
		// 	$table = $this->load->view('/reports/tables/reconciliation_report', $data, true);
		// 	$return['information'] = $table;
		// 	echo json_encode($return);
		// 	// $this->ajaxResponse(['information' => $table]);
		// }
		// else
		// {
		// 	$active_tab = 'transactions';

		// 	$this->template->set('active_tab', $active_tab);
		// 	$this->template->set('tb_data', $data);
		// 	$this->index();
		// }


	}

	public function showItems()
	{
		$columnsDefault = [
			'id' => true,
			'name' => true,
			'description' => true,
			'company_id' => true,
			'created_by' => true,
			'updated_by' => true,
		];

		$draw = $_REQUEST['draw'];
		$start = $_REQUEST['start'];
		$rowperpage = $_REQUEST['length'];
		$columnIndex = $_REQUEST['order'][0]['column'];
		$columnName = $_REQUEST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_REQUEST['order'][0]['dir'];
		$searchValue = $_REQUEST['search']['value'] ?? null;

		$filters = [];
		parse_str($_POST['filter'], $filters);

		$items = [];
		$totalRecordwithFilter = 0;
		$filteredItems = [];

		for ($query_loop = 0; $query_loop < 2; $query_loop++) {

			$query = $this->M_report
				->with_company('fields:name')
				->order_by($columnName, $columnSortOrder)
				->as_array();

			// General Search
			if ($searchValue) {

				$query->or_where("(id like %$searchValue%");
				$query->or_where("name like %$searchValue%");
				$query->or_where("description like %$searchValue%)");
			}

			$advanceSearchValues = [
				'name' => [
					'data' => $filters['name'] ?? null,
					'operator' => 'like',
				],
				'description' => [
					'data' => $filters['description'] ?? null,
					'operator' => 'like',
				],
				'company_id' => [
					'data' => $filters['company_id'] ?? null,
					'operator' => '=',
				],
			];

			// Advance Search
			foreach ($advanceSearchValues as $key => $value) {

				if ($value['data']) {

					if ($key == 'date_range_start' || $key == 'date_range_end') {

						$query->where($value['column'], $value['operator'], $value['data']);
					} else {

						$query->where($key, $value['operator'], $value['data']);
					}
				}
			}

			if ($query_loop) {

				$totalRecordwithFilter = $query->count_rows();
			} else {

				$query->limit($rowperpage, $start);
				$items = $query->get_all();

				if (!!$items) {

					// Transform data
					foreach ($items as $key => $value) {

						$items[$key]['company_id'] = $value['company']['name'] ?? '';

						$items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

						$items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
					}

					foreach ($items as $item) {
						$filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
					}
				}
			}
		}

		$totalRecords = $this->M_report->count_rows();

		$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"data" => $filteredItems,
			"request" => $_REQUEST,
		);

		echo json_encode($response);
		exit();
	}
}
