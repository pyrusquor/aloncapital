<?php
    $id            =    isset($vehicle_equipment['id']) && $vehicle_equipment['id'] ? $vehicle_equipment['id'] : '';
    $company_id            =    isset($vehicle_equipment['company_id']) && $vehicle_equipment['company_id'] ? $vehicle_equipment['company_id'] : '';

    $company            =    isset($vehicle_equipment['company']) && $vehicle_equipment['company'] ? $vehicle_equipment['company']['name'] : '';

    $vehicle_equipment_group_id            =    isset($vehicle_equipment['vehicle_equipment_group_id']) && $vehicle_equipment['vehicle_equipment_group_id'] ? $vehicle_equipment['vehicle_equipment_group_id'] : '';

    $code            =    isset($vehicle_equipment['code']) && $vehicle_equipment['code'] ? $vehicle_equipment['code'] : '';

    $name            =    isset($vehicle_equipment['name']) && $vehicle_equipment['name'] ? $vehicle_equipment['name'] : '';

    $model            =    isset($vehicle_equipment['model']) && $vehicle_equipment['model'] ? $vehicle_equipment['model'] : '';

    $chassis_number            =    isset($vehicle_equipment['chassis_number']) && $vehicle_equipment['chassis_number'] ? $vehicle_equipment['chassis_number'] : '';

    $date_acquired            =    isset($vehicle_equipment['date_acquired']) && $vehicle_equipment['date_acquired'] ? $vehicle_equipment['date_acquired'] : '';

    $denomination            =    isset($vehicle_equipment['denomination']) && $vehicle_equipment['denomination'] ? $vehicle_equipment['denomination'] : '';

    $fuel_type_id            =    isset($vehicle_equipment['fuel_type']) && $vehicle_equipment['fuel_type'] ? $vehicle_equipment['fuel_type'] : '';

    $engine_number            =    isset($vehicle_equipment['engine_number']) && $vehicle_equipment['engine_number'] ? $vehicle_equipment['engine_number'] : '';

    $make            =    isset($vehicle_equipment['make']) && $vehicle_equipment['make'] ? $vehicle_equipment['make'] : '';

    $gross_weight            =    isset($vehicle_equipment['gross_weight']) && $vehicle_equipment['gross_weight'] ? $vehicle_equipment['gross_weight'] : '';

    $net_weight            =    isset($vehicle_equipment['net_weight']) && $vehicle_equipment['net_weight'] ? $vehicle_equipment['net_weight'] : '';

    $body_number            =    isset($vehicle_equipment['body_number']) && $vehicle_equipment['body_number'] ? $vehicle_equipment['body_number'] : '';

    $body_type_id            =    isset($vehicle_equipment['body_type_id']) && $vehicle_equipment['body_type_id'] ? $vehicle_equipment['body_type_id'] : '';

    $year_model            =    isset($vehicle_equipment['year_model']) && $vehicle_equipment['year_model'] ? $vehicle_equipment['year_model'] : '';

    $plate_number            =    isset($vehicle_equipment['plate_number']) && $vehicle_equipment['plate_number'] ? $vehicle_equipment['plate_number'] : '';

    $color            =    isset($vehicle_equipment['color']) && $vehicle_equipment['color'] ? $vehicle_equipment['color'] : '';
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Vehicle Equipment</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('vehicle_equipment/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('vehicle_equipment');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Company
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $company; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Vehicle Equipment Group
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('vehicle_equipment_group', $vehicle_equipment_group_id, 'view'); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Code
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $code; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Model
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $model; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Chassis Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $chassis_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Date Acquired
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo view_date($date_acquired); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Denomination
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $denomination; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Fuel Type
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('fuel_type', $fuel_type_id, 'view'); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Engine Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $engine_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Make
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $make; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Gross Weight
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $gross_weight; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Net Weight
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $net_weight; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Body Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $body_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Body Type
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('body_type', $body_type_id, 'view'); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Year Model
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $year_model; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Plate Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $plate_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Color
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $color; ?></span>
                                </div>

                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
