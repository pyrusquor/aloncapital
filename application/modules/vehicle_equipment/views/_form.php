<?php
    $company_id            =    isset($info['company_id']) && $info['company_id'] ? $info['company_id'] : '';

    $company            =    isset($info['company']) && $info['company'] ? $info['company']['name'] : '';

    $vehicle_equipment_group_id            =    isset($info['vehicle_equipment_group_id']) && $info['vehicle_equipment_group_id'] ? $info['vehicle_equipment_group_id'] : '';

    $code            =    isset($info['code']) && $info['code'] ? $info['code'] : '';

    $name            =    isset($info['name']) && $info['name'] ? $info['name'] : '';

    $model            =    isset($info['model']) && $info['model'] ? $info['model'] : '';

    $chassis_number            =    isset($info['chassis_number']) && $info['chassis_number'] ? $info['chassis_number'] : '';

    $date_acquired            =    isset($info['date_acquired']) && $info['date_acquired'] ? $info['date_acquired'] : '';

    $denomination            =    isset($info['denomination']) && $info['denomination'] ? $info['denomination'] : '';

    $fuel_type            =    isset($info['fuel_type']) && $info['fuel_type'] ? $info['fuel_type'] : '';

    $engine_number            =    isset($info['engine_number']) && $info['engine_number'] ? $info['engine_number'] : '';

    $make            =    isset($info['make']) && $info['make'] ? $info['make'] : '';

    $gross_weight            =    isset($info['gross_weight']) && $info['gross_weight'] ? $info['gross_weight'] : '';

    $net_weight            =    isset($info['net_weight']) && $info['net_weight'] ? $info['net_weight'] : '';

    $body_number            =    isset($info['body_number']) && $info['body_number'] ? $info['body_number'] : '';

    $body_type_id            =    isset($info['body_type_id']) && $info['body_type_id'] ? $info['body_type_id'] : '';

    $year_model            =    isset($info['year_model']) && $info['year_model'] ? $info['year_model'] : '';

    $plate_number            =    isset($info['plate_number']) && $info['plate_number'] ? $info['plate_number'] : '';

    $color            =    isset($info['color']) && $info['color'] ? $info['color'] : '';
?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="companies" id="company_id" name="company_id" required>
                    <option value="">Select Company</option>
                    <?php if ($company): ?>
                        <option value="<?php echo $company_id; ?>" selected><?php echo $company; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('or_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Vehicle Equipment Group <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('vehicle_equipment_group_id', Dropdown::get_static('vehicle_equipment_group'), set_value('vehicle_equipment_group_id', @$vehicle_equipment_group_id), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Code <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="code" value="<?php echo set_value('code', $code); ?>" placeholder="Code">

            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name">

            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Model <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="model" value="<?php echo set_value('model', $model); ?>" placeholder="Model">

            <?php echo form_error('model'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Chassis Number <span class="kt-font-danger">*</span></label>
            <input type="number" class="form-control" name="chassis_number" value="<?php echo set_value('chassis_number', $chassis_number); ?>" placeholder="Chassis Number">

            <?php echo form_error('chassis_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Date Acquired <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control kt_datepicker" name="date_acquired" value="<?php echo set_value('date_acquired', $date_acquired); ?>" placeholder="Date Acquired" readonly>

            <?php echo form_error('date_acquired'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Denomination <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="denomination" value="<?php echo set_value('denomination', $denomination); ?>" placeholder="Denomination">

            <?php echo form_error('denomination'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Fuel Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('fuel_type', Dropdown::get_static('fuel_type'), set_value('fuel_type', @$fuel_type), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('fuel_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Engine Number <span class="kt-font-danger">*</span></label>
            <input type="number" class="form-control" name="engine_number" value="<?php echo set_value('engine_number', $engine_number); ?>" placeholder="Engine Number">

            <?php echo form_error('engine_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Make <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="make" value="<?php echo set_value('make', $make); ?>" placeholder="Make">

            <?php echo form_error('make'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Gross Weight <span class="kt-font-danger">*</span></label>
            <input type="number" class="form-control" name="gross_weight" value="<?php echo set_value('gross_weight', $gross_weight); ?>" placeholder="Gross Weight">

            <?php echo form_error('gross_weight'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Net Weight <span class="kt-font-danger">*</span></label>
            <input type="number" class="form-control" name="net_weight" value="<?php echo set_value('net_weight', $net_weight); ?>" placeholder="Net Weight">

            <?php echo form_error('net_weight'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Body Number <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="body_number" value="<?php echo set_value('body_number', $body_number); ?>" placeholder="Body Number">

            <?php echo form_error('body_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Body Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('body_type_id', Dropdown::get_static('body_type'), set_value('body_type_id', @$body_type_id), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('body_type_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Year Model <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control yearPicker" name="year_model" value="<?php echo set_value('year_model', $year_model); ?>" placeholder="Year Model" readonly>

            <?php echo form_error('year_model'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Plate Number <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="plate_number" value="<?php echo set_value('plate_number', $plate_number); ?>" placeholder="Plate Number">

            <?php echo form_error('plate_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Color <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="color" value="<?php echo set_value('color', $color); ?>" placeholder="Color">

            <?php echo form_error('color'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Is Active <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('is_active', Dropdown::get_static('inventory_status'), set_value('is_active', @$is_active ? @$is_active : 1), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>