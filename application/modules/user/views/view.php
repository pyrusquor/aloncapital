<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">User Profile</h3>
		</div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
		<div class="col-md-12 col-xl-12">
			<!--Begin:: Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__body">
					<div class="kt-widget kt-widget--user-profile-3">
						<div class="kt-widget__top">
							<div class="kt-widget__media">
								<img src="<?php echo base_url(); ?>assets/media/users/300_21.jpg" alt="image">
							</div>
							<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-bolder kt-font-light kt-hidden">
								JM
							</div>
							<div class="kt-widget__content">
								<div class="kt-widget__head">
									<div class="kt-widget__user">
										<a href="#" class="kt-widget__username">
											Luca Doncic
										</a>
										&nbsp;&nbsp;
										<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success">Administrator</span>
									</div>
									<div class="kt-widget__action">
										<a href="#" class="btn btn-label-brand btn-sm btn-upper">Deactivate</a>
										<a href="#" class="btn btn-label-brand btn-sm btn-upper">Contact</a>
									</div>
								</div>
								<div class="kt-widget__subhead">
									<a href="#"><i class="flaticon2-new-email"></i>Mavs77</a>
									<a href="#"><i class="flaticon2-new-email"></i>luca@festudios.com</a>
									<a href="#"><i class="flaticon2-calendar-3"></i>Administrator</a>
									<a href="#"><i class="flaticon2-placeholder"></i>REMS</a>
									<a href="#"><i class="flaticon2-placeholder"></i>Active</a>
								</div>
								<!--<div class="kt-widget__info">
									<div class="kt-widget__desc">
										I distinguish three main text objektive could be merely to inform people.
										<br> A second could be persuade people.You want people to bay objective
									</div>
									<div class="kt-widget__progress">
										<div class="kt-widget__text">
											Goals
										</div>
										<div class="progress" style="height: 5px;width: 100%;">
											<div class="progress-bar kt-bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<div class="kt-widget__stats">
											45%
										</div>
									</div>
								</div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="col-md-4 col-xl-4">
			<!--Begin:: Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Company
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<a href="#" class="btn btn-label-brand btn-sm btn-bold">View Profile</a>
					</div>
				</div>
				<div class="kt-form kt-form--label-right">
					<div class="kt-portlet__body">
						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Name:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder">Loop Inc.</span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Location:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder">London, UK.</span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Revenue:</label>
							<div class="col-8">
								<span class="form-control-plaintext"><span class="kt-font-bolder">345,000M</span> &nbsp;<span class="kt-badge kt-badge--inline kt-badge--danger kt-badge--bold">Q4, 2019</span></span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Phone:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder">+456 7890456</span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Email:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder">
									<a href="#">info@loop.com</a>
								</span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Website:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder">
									<a href="#">www.loop.com</a>
								</span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Contact Person:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder">
									<a href="#">Nick Bold</a>
								</span>
							</div>
						</div>
					</div>
					<!--<div class="kt-portlet__foot"></div>-->
				</div>
			</div>

			<!--End:: Portlet-->
		</div>
		
		<div class="col-md-8 col-xl-8">
			<div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">User Activities</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="#" class="btn btn-label-brand btn-sm btn-bold">Filter</a>
                    </div>
                </div>
                <div class="kt-portlet__body">
					<div class="kt-notes kt-scroll kt-scroll--pull" data-scroll="true" style="height: 700px;">
						<div class="kt-notes__items">
							<div class="kt-notes__item">
								<div class="kt-notes__media">
									<img class="kt-hidden-" src="<?php echo base_url(); ?>assets/media/users/100_3.jpg" alt="image">
									<span class="kt-notes__icon kt-font-boldest kt-hidden">
										<i class="flaticon2-cup"></i>
									</span>
									<h3 class="kt-notes__user kt-font-boldest kt-hidden">
										N S
									</h3>
								</div>
								<div class="kt-notes__content">
									<div class="kt-notes__section">
										<div class="kt-notes__info">
											<a href="#" class="kt-notes__title">
												New order
											</a>
											<span class="kt-notes__desc">
												9:30AM 16 June, 2015
											</span>
											<span class="kt-badge kt-badge--success kt-badge--inline">new</span>
										</div>
										<div class="kt-notes__dropdown">
											<a href="#" class="btn btn-sm btn-icon-md btn-icon" data-toggle="dropdown">
												<i class="flaticon-more-1 kt-font-brand"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="kt-nav">
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-line-chart"></i>
															<span class="kt-nav__link-text">Reports</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-send"></i>
															<span class="kt-nav__link-text">Messages</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
															<span class="kt-nav__link-text">Charts</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-avatar"></i>
															<span class="kt-nav__link-text">Members</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-settings"></i>
															<span class="kt-nav__link-text">Settings</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<span class="kt-notes__body">
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.
									</span>
								</div>
							</div>
							<div class="kt-notes__item">
								<div class="kt-notes__media">
									<span class="kt-notes__icon">
										<i class="flaticon2-rocket kt-font-danger"></i>
									</span>
								</div>
								<div class="kt-notes__content">
									<div class="kt-notes__section">
										<div class="kt-notes__info">
											<a href="#" class="kt-notes__title">
												Notification
											</a>
											<span class="kt-notes__desc">
												10:30AM 23 May, 2013
											</span>
										</div>
										<div class="kt-notes__dropdown">
											<a href="#" class="btn btn-sm btn-icon-md btn-icon" data-toggle="dropdown">
												<i class="flaticon2-rectangular kt-font-brand"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="kt-nav">
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-line-chart"></i>
															<span class="kt-nav__link-text">Reports</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-send"></i>
															<span class="kt-nav__link-text">Messages</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
															<span class="kt-nav__link-text">Charts</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-avatar"></i>
															<span class="kt-nav__link-text">Members</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-settings"></i>
															<span class="kt-nav__link-text">Settings</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<span class="kt-notes__body">
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.
									</span>
								</div>
							</div>
							<div class="kt-notes__item">
								<div class="kt-notes__media">
									<h3 class="kt-notes__user kt-font-brand kt-font-boldest">
										DS
									</h3>
								</div>
								<div class="kt-notes__content">
									<div class="kt-notes__section">
										<div class="kt-notes__info">
											<a href="#" class="kt-notes__title">
												System alert
											</a>
											<span class="kt-notes__desc">
												7:10AM 21 February, 2016
											</span>
										</div>
										<div class="kt-notes__dropdown">
											<a href="#" class="btn btn-sm btn-icon-md btn-icon" data-toggle="dropdown">
												<i class="flaticon2-note kt-font-brand"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="kt-nav">
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-line-chart"></i>
															<span class="kt-nav__link-text">Reports</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-send"></i>
															<span class="kt-nav__link-text">Messages</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
															<span class="kt-nav__link-text">Charts</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-avatar"></i>
															<span class="kt-nav__link-text">Members</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-settings"></i>
															<span class="kt-nav__link-text">Settings</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<span class="kt-notes__body">
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.
									</span>
								</div>
							</div>
							<div class="kt-notes__item">
								<div class="kt-notes__media">
									<span class="kt-notes__icon">
										<i class="flaticon2-poll-symbol kt-font-success"></i>
									</span>
								</div>
								<div class="kt-notes__content">
									<div class="kt-notes__section">
										<div class="kt-notes__info">
											<a href="#" class="kt-notes__title">
												New order
											</a>
											<span class="kt-notes__desc">
												10:30AM 23 May, 2013
											</span>
										</div>
										<div class="kt-notes__dropdown">
											<a href="#" class="btn btn-sm btn-icon-md btn-icon" data-toggle="dropdown">
												<i class="flaticon2-gear kt-font-brand"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="kt-nav">
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-line-chart"></i>
															<span class="kt-nav__link-text">Reports</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-send"></i>
															<span class="kt-nav__link-text">Messages</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
															<span class="kt-nav__link-text">Charts</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-avatar"></i>
															<span class="kt-nav__link-text">Members</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-settings"></i>
															<span class="kt-nav__link-text">Settings</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<span class="kt-notes__body">
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.
									</span>
								</div>
							</div>
							<div class="kt-notes__item">
								<div class="kt-notes__media">
									<span class="kt-notes__icon">
										<i class="flaticon2-box-1 kt-font-brand"></i>
									</span>
								</div>
								<div class="kt-notes__content">
									<div class="kt-notes__section">
										<div class="kt-notes__info">
											<a href="#" class="kt-notes__title">
												Notification
											</a>
											<span class="kt-notes__desc">
												10:30AM 23 May, 2013
											</span>
										</div>
										<div class="kt-notes__dropdown">
											<a href="#" class="btn btn-sm btn-icon-md btn-icon" data-toggle="dropdown">
												<i class="flaticon2-sort kt-font-brand"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="kt-nav">
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-line-chart"></i>
															<span class="kt-nav__link-text">Reports</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-send"></i>
															<span class="kt-nav__link-text">Messages</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
															<span class="kt-nav__link-text">Charts</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-avatar"></i>
															<span class="kt-nav__link-text">Members</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-settings"></i>
															<span class="kt-nav__link-text">Settings</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<span class="kt-notes__body">
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.
									</span>
								</div>
							</div>
							<div class="kt-notes__item">
								<div class="kt-notes__media">
									<span class="kt-notes__icon">
										<i class="flaticon2-rocket kt-font-danger"></i>
									</span>
								</div>
								<div class="kt-notes__content">
									<div class="kt-notes__section">
										<div class="kt-notes__info">
											<a href="#" class="kt-notes__title">
												Notification
											</a>
											<span class="kt-notes__desc">
												10:30AM 23 May, 2013
											</span>
										</div>
										<div class="kt-notes__dropdown">
											<a href="#" class="btn btn-sm btn-icon-md btn-icon" data-toggle="dropdown">
												<i class="flaticon2-rectangular kt-font-brand"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="kt-nav">
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-line-chart"></i>
															<span class="kt-nav__link-text">Reports</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-send"></i>
															<span class="kt-nav__link-text">Messages</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
															<span class="kt-nav__link-text">Charts</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-avatar"></i>
															<span class="kt-nav__link-text">Members</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-settings"></i>
															<span class="kt-nav__link-text">Settings</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<span class="kt-notes__body">
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.
									</span>
								</div>
							</div>
							<div class="kt-notes__item">
								<div class="kt-notes__media">
									<img class="kt-hidden" src="<?php echo base_url(); ?>assets/media/users/100_1.jpg" alt="image">
									<span class="kt-notes__icon kt-font-boldest kt-hidden">
										<i class="flaticon2-cup"></i>
									</span>
									<h3 class="kt-notes__user kt-font-boldest ">
										M E
									</h3>
									<span class="kt-notes__circle kt-hidden"></span>
								</div>
								<div class="kt-notes__content">
									<div class="kt-notes__section">
										<div class="kt-notes__info">
											<a href="#" class="kt-notes__title">
												Order
											</a>
											<span class="kt-notes__desc">
												11:40AM 14 March, 2012
											</span>
											<span class="kt-badge kt-badge--danger kt-badge--inline">important</span>
										</div>
									</div>
									<span class="kt-notes__body">
										Sed ut sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.
									</span>
								</div>
							</div>
							<div class="kt-notes__item">
								<div class="kt-notes__media">
									<img class="" src="<?php echo base_url(); ?>assets/media/users/100_1.jpg" alt="image">
									<span class="kt-notes__icon kt-font-boldest kt-hidden">
										<i class="flaticon2-cup"></i>
									</span>
									<h3 class="kt-notes__user kt-font-boldest kt-hidden">
										N B
									</h3>
									<span class="kt-notes__circle kt-hidden"></span>
								</div>
								<div class="kt-notes__content">
									<div class="kt-notes__section">
										<div class="kt-notes__info">
											<a href="#" class="kt-notes__title">
												Remarks
											</a>
											<span class="kt-notes__desc">
												10:30AM 23 April, 2013
											</span>
										</div>
									</div>
									<span class="kt-notes__body">
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>