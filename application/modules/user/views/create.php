<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Create User</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="#" class="btn btn-label-success"><i class="la la-plus"></i> Submit</a>&nbsp;
                <a href="#" class="btn btn-label-instagram"><i class="la la-times"></i> Cancel</a>&nbsp;
            </div>
        </div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12 col-xl-12">
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">User's Profile Details</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar"></div>
                </div>
				<div class="kt-portlet__body">
					<!--begin: Form Wizard Step 1-->
					<div class="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
						<!--<div class="kt-heading kt-heading--md">User's Profile Details:</div>-->
						<div class="kt-section kt-section--first">
							<div class="kt-wizard-v1__form">
								<div class="row">
									<div class="col-xl-12">
										<div class="kt-section__body">
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
												<div class="col-lg-9 col-xl-6">
													<div class="kt-avatar kt-avatar--outline kt-avatar--circle--" id="kt_apps_user_add_avatar">
														<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url(); ?>assets/media/users/default.jpg)"></div>
														<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
															<i class="fa fa-pen"></i>
															<input type="file" name="kt_apps_contacts_add_avatar">
														</label>
														<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
															<i class="fa fa-times"></i>
														</span>
													</div>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
												<div class="col-lg-9 col-xl-9">
													<input class="form-control" type="text" value="">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
												<div class="col-lg-9 col-xl-9">
													<input class="form-control" type="text" value="">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Company Name</label>
												<div class="col-lg-9 col-xl-9">
													<input class="form-control" type="text" value="">
													<!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Position</label>
												<div class="col-lg-9 col-xl-9">
													<input class="form-control" type="text" value="">
													<!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
												<div class="col-lg-9 col-xl-9">
													<div class="input-group">
														<div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
														<input type="text" class="form-control" value="" placeholder="" aria-describedby="basic-addon1">
													</div>
													<!--<span class="form-text text-muted">We'll never share your email with anyone else.</span>-->
												</div>
											</div>
											<div class="form-group row">
												<label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
												<div class="col-lg-9 col-xl-9">
													<div class="input-group">
														<div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
														<input type="text" class="form-control" value="" placeholder="" aria-describedby="basic-addon1">
													</div>
												</div>
											</div>
											<div class="form-group form-group-last row">
												<label class="col-xl-3 col-lg-3 col-form-label">Username</label>
												<div class="col-lg-9 col-xl-9">
													<div class="input-group">
													<div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
														<input type="text" class="form-control" placeholder="" value="">
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!--end: Form Wizard Step 1-->
				</div>
			</div>
		</div>
	</div>
</div>