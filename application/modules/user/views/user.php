<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Users</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">54 TOTAL</span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search user..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="#" class="btn btn-label-primary"><i class="la la-plus"></i> Create User</a>&nbsp;
                <a href="#" class="btn btn-label-primary"><i class="la la-filter"></i> Filter</a>&nbsp;
                <a href="#" class="btn btn-label-primary"><i class="la la-upload"></i> Batch Upload</a>&nbsp;
                <a href="#" class="btn btn-label-primary"><i class="la la-download"></i> Export</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-4 col-xl-4">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head kt-portlet__head--noborder">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<a href="#" class="btn btn-icon" data-toggle="dropdown">
							<i class="flaticon-more-1 kt-font-brand"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<ul class="kt-nav">
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-line-chart"></i>
										<span class="kt-nav__link-text">Deactivate</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-send"></i>
										<span class="kt-nav__link-text">Reset Password</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin::Widget -->
					<div class="kt-widget kt-widget--user-profile-2">
						<div class="kt-widget__head">
							<div class="kt-widget__media">
								<img class="kt-widget__img kt-hidden-" src="<?php echo base_url(); ?>assets/media/users/300_21.jpg" alt="image">
								<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden">
									ChS
								</div>
							</div>
							<div class="kt-widget__info">
								<a href="#" class="kt-widget__username">
									Luca Doncic
								</a>
								<span class="kt-widget__desc">
									Head of Development
								</span>
							</div>
						</div>
						<div class="kt-widget__body">
							<!-- <div class="kt-widget__section">
								I distinguish three <a href="#" class="kt-font-brand kt-link kt-font-transform-u kt-font-bold">#xrs-54pq</a> objectsves First
								merely firsr <b>USD249/Annual</b> your been to giant
								esetablished and nice coocked rice
							</div> -->
							<div class="kt-widget__item">
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Username:</span>
									<span class="kt-widget__data">Mavs77</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Email:</span>
									<a href="#" class="kt-widget__data">luca@festudios.com</a>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Group:</span>
									<span class="kt-widget__data">Administrator</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Phone:</span>
									<a href="#" class="kt-widget__data">44(76)34254578</a>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Company:</span>
									<span class="kt-widget__data">REMS</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Status:</span>
									<span class="kt-widget__data">Active</span>
								</div>
							</div>
						</div>
						<div class="kt-widget__footer">
							<button type="button" class="btn btn-label-primary btn-lg btn-upper">View Profile</button>
						</div>
					</div>
					<!--end::Widget -->
				</div>
			</div>
			<!--End::Portlet-->
		</div>	
		<div class="col-md-4 col-xl-4">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head kt-portlet__head--noborder">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<a href="#" class="btn btn-icon" data-toggle="dropdown">
							<i class="flaticon-more-1 kt-font-brand"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<ul class="kt-nav">
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-line-chart"></i>
										<span class="kt-nav__link-text">Deactivate</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-send"></i>
										<span class="kt-nav__link-text">Reset Password</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin::Widget -->
					<div class="kt-widget kt-widget--user-profile-2">
						<div class="kt-widget__head">
							<div class="kt-widget__media">
								<img class="kt-widget__img kt-hidden-" src="<?php echo base_url(); ?>assets/media/users/300_21.jpg" alt="image">
								<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden">
									ChS
								</div>
							</div>
							<div class="kt-widget__info">
								<a href="#" class="kt-widget__username">
									Luca Doncic
								</a>
								<span class="kt-widget__desc">
									Head of Development
								</span>
							</div>
						</div>
						<div class="kt-widget__body">
							<!-- <div class="kt-widget__section">
								I distinguish three <a href="#" class="kt-font-brand kt-link kt-font-transform-u kt-font-bold">#xrs-54pq</a> objectsves First
								merely firsr <b>USD249/Annual</b> your been to giant
								esetablished and nice coocked rice
							</div> -->
							<div class="kt-widget__item">
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Username:</span>
									<span class="kt-widget__data">Mavs77</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Email:</span>
									<a href="#" class="kt-widget__data">luca@festudios.com</a>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Group:</span>
									<span class="kt-widget__data">Administrator</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Phone:</span>
									<a href="#" class="kt-widget__data">44(76)34254578</a>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Company:</span>
									<span class="kt-widget__data">REMS</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Status:</span>
									<span class="kt-widget__data">Active</span>
								</div>
							</div>
						</div>
						<div class="kt-widget__footer">
							<button type="button" class="btn btn-label-primary btn-lg btn-upper">View Profile</button>
						</div>
					</div>
					<!--end::Widget -->
				</div>
			</div>
			<!--End::Portlet-->
		</div>	
		<div class="col-md-4 col-xl-4">
			<!--Begin::Portlet-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head kt-portlet__head--noborder">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<a href="#" class="btn btn-icon" data-toggle="dropdown">
							<i class="flaticon-more-1 kt-font-brand"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<ul class="kt-nav">
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-line-chart"></i>
										<span class="kt-nav__link-text">Deactivate</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-send"></i>
										<span class="kt-nav__link-text">Reset Password</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin::Widget -->
					<div class="kt-widget kt-widget--user-profile-2">
						<div class="kt-widget__head">
							<div class="kt-widget__media">
								<img class="kt-widget__img kt-hidden-" src="<?php echo base_url(); ?>assets/media/users/300_21.jpg" alt="image">
								<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden">
									ChS
								</div>
							</div>
							<div class="kt-widget__info">
								<a href="#" class="kt-widget__username">
									Luca Doncic
								</a>
								<span class="kt-widget__desc">
									Head of Development
								</span>
							</div>
						</div>
						<div class="kt-widget__body">
							<!-- <div class="kt-widget__section">
								I distinguish three <a href="#" class="kt-font-brand kt-link kt-font-transform-u kt-font-bold">#xrs-54pq</a> objectsves First
								merely firsr <b>USD249/Annual</b> your been to giant
								esetablished and nice coocked rice
							</div> -->
							<div class="kt-widget__item">
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Username:</span>
									<span class="kt-widget__data">Mavs77</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Email:</span>
									<a href="#" class="kt-widget__data">luca@festudios.com</a>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Group:</span>
									<span class="kt-widget__data">Administrator</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Phone:</span>
									<a href="#" class="kt-widget__data">44(76)34254578</a>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Company:</span>
									<span class="kt-widget__data">REMS</span>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Status:</span>
									<span class="kt-widget__data">Active</span>
								</div>
							</div>
						</div>
						<div class="kt-widget__footer">
							<button type="button" class="btn btn-label-primary btn-lg btn-upper">View Profile</button>
						</div>
					</div>
					<!--end::Widget -->
				</div>
			</div>
			<!--End::Portlet-->
		</div>	
	
	
	</div>
</div>