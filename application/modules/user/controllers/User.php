<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->template->title('REMS', 'Users');

		// overwrite default theme and layout if needed
		$this->template->set_theme('default');
		$this->template->set_layout('default');

		$this->template->build('user');

	}
	
	public function view()
	{
		$this->template->build('view');
	}
	
	public function create()
	{
		$this->template->build('create');
	}
	
}