<?php
defined('BASEPATH') or exit('No direct script access allowed');
class User_model extends MY_Model
{
	public $table = 'users'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'ip_address', 'username', 'password', 'email', 'activation_code', 'created_on', 'last_login',
		'active', 'first_name', 'last_name', 'company', 'phone', 'created_by', 'created_at', 'updated_by', 'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [
		'ip_address' => array(
			'field' => 'ip_address',
			'label' => 'IP Address',
			'rules' => 'trim'
		),
		'username' => array(
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'trim|required'
		),
		'password' => array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required'
		),
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required'
		),
		'activation_code' => array(
			'field' => 'activation_code',
			'label' => 'Activation Code',
			'rules' => 'trim'
		),
		'created_on' => array(
			'field' => 'created_on',
			'label' => 'Created On',
			'rules' => 'trim'
		),
		'last_login' => array(
			'field' => 'last_login',
			'label' => 'Last Login',
			'rules' => 'trim'
		),
		'active' => array(
			'field' => 'active',
			'label' => 'Active',
			'rules' => 'trim|required'
		),
		'first_name' => array(
			'field' => 'first_name',
			'label' => 'First Name',
			'rules' => 'trim|required'
		),
		'last_name' => array(
			'field' => 'last_name',
			'label' => 'Last Name',
			'rules' => 'trim|required'
		),
		'company' => array(
			'field' => 'company',
			'label' => 'Company',
			'rules' => 'trim'
		),
		'phone' => array(
			'field' => 'phone',
			'label' => 'Phone',
			'rules' => 'trim'
		)
	];

	public function __construct()
	{
		parent::__construct();

		// $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
	}

	public function user_verification($user_email)
	{

		$this->db->select('email');
		$this->db->where('email', $user_email);
		$query = $this->db->get('users');

		if ($query->row_array() > 0) {
			$data['active'] = true;

			$this->db->where('email', $user_email);
			$this->db->update('users', $data);
			return $query->row_array();
		}
		return false;
	}

	function get_last_login_time($user_id)
	{
		$query = [];
		$this->db->select('created_at as time_in');
		$this->db->where('user_id',$user_id);
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get('login_history',1)->result_array();
		if($query){
			$date = new DateTime($query[0]['time_in']);
			return $date->format('M d, Y g:i A');
		}
		return $query;
	}
}
