<?php defined('BASEPATH') or exit('No direct script access allowed');

class Settings_m extends MY_Model {

	protected $_table = 'settings';

	public $name = 'General';

	public $validation_rules = array(
		array(
			'field' => 'id',
			'rules' => 'trim|numeric'
		),
		array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'code',
			'label' => 'Code',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'setting_key',
			'label' => 'Setting Key',
			'rules' => 'trim|required|callback__check_setting_key'
		),
		array(
			'field' => 'value',
			'label' => 'Value',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'trim|required'
		),
	);

	public function submenu($slug=false)
	{
		$submenu = [];

		switch($slug)
		{
			case 'company' :
				if(has_role('settings_company', 'company_information'))
					$submenu['company'] = ['url'=>'settings/company', 'name'=>'Company Information'];
				if(has_role('settings_company', 'company_structure'))
					$submenu['organizations'] = ['url'=>'organizations', 'name'=>'Company Structure'];
				if(has_role('settings_company', 'banks'))
					$submenu['banks'] = ['url'=>'banks', 'name'=>'Banks'];
				if(has_role('settings_company', 'permissions'))
					$submenu['permissions'] = ['url'=>'settings/permissions', 'name'=>'Admin Permissions'];

			break;
			case 'employee' :
				if(has_role('settings_employee', 'custom_fields'))
					$submenu['custom'] = ['url'=>'settings/custom-fields', 'name'=>'Profile Fields'];
				if(has_role('settings_employee', 'evaluation_templates'))
					$submenu['evaluations'] = ['url'=>'settings/evaluation', 'name'=>'Evaluation Templates'];
				if(has_role('settings_employee', 'disciplinary_categories'))
					$submenu['action-categories'] = ['url'=>'settings/options/index/15', 'name'=>'Disciplinary Categories'];

			break;
			case 'time-tracking' :
				if(has_role('settings_time_tracking', 'dtr'))
					$submenu['dtr'] = ['url'=>'settings/dtr', 'name'=>'DTR'];
				if(has_role('settings_time_tracking', 'shifts'))
					$submenu['shifts'] = ['url'=>'settings/shifts', 'name'=>'Shifts'];

			break;
			case 'payroll' :
				if(has_role('settings_payroll', 'payroll'))
					$submenu['payroll'] = ['url'=>'settings/payroll', 'name'=>'Payroll'];
				if(has_role('settings_payroll', 'rates'))
					$submenu['rates'] = ['url'=>'settings/rates', 'name'=>'Rates'];
				if(has_role('settings_payroll', 'deductions'))
					$submenu['deductions'] = ['url'=>'settings/deductions', 'name'=>'Deductions'];
				if(has_role('settings_payroll', 'allowances'))
					$submenu['allowances'] = ['url'=>'settings/allowances', 'name'=>'Allowances'];
				if(has_role('settings_payroll', 'leave_types'))
					$submenu['leaves'] = ['url'=>'settings/leaves', 'name'=>'Leave Types'];

			break;
			case 'communication' :

				if(has_role('settings_communication', 'email'))
					$submenu['email'] = ['url'=>'canned-messages', 'name'=>'Email'];
				if(has_role('settings_communication', 'sms'))
					$submenu['sms'] = ['url'=>'sms/history', 'name'=>'SMS'];

			break;

		}

		return $submenu;
	}

	public function save($data)
	{
		if( ! empty($data['data']))
		{
			$data['data'] = json_encode($data['data']);
		}

		if( ! empty($data['id']))
		{
			$this->db->where('id', $data['id']);
			$this->db->update($this->_table, $data);
			return $data['id'];
		}
		else
		{
			$this->db->insert($this->_table, $data);
			return $this->db->insert_id();
		}
	}

	public function save_config($data)
	{
		// check exist
		$this->db->where('key', $data['key']);
		$this->db->where('company_id', $data['company_id']);
		$exist = $this->db->select('id')->get('settings_metadata')->row();

		if($exist)
		{
			$data['id'] = $exist->id;
		}

		if( ! empty($data['value']) && (is_array($data['value']) || is_object($data['value'])))
		{
			$data['value'] = json_encode($data['value']);
		}

		if( ! empty($data['id']))
		{
			$this->db->where('id', $data['id']);
			$this->db->update('settings_metadata', $data);
			return $data['id'];
		}
		else
		{
			$this->db->insert('settings_metadata', $data);
			return $this->db->insert_id();
		}
	}

	public function get_by_code($code, $key= false, $global=false)
	{
		// if global settings, company_id should be 0


		$company_id = ($global ? 0 : @$this->current_user->company_id);



		if(! $key)
		{
			$post = $this->where(['code' => $code])->get_many_by([]);

			foreach($post as $row)
			{
				$this->db->where(['key' => $row->code .':'. $row->setting_key, 'company_id' => $company_id]);
				$meta = $this->db->get('settings_metadata')->row();

				if($meta)
				{
					$row->value= $meta->value;
				}
			}
		}
		else
		{
			$this->db->where(['key' => $code .':'. $key, 'company_id' => $company_id]);
			$meta = $this->db->get('settings_metadata')->row();


			$post  = $this->get_by(['code' => $code, 'setting_key' => $key]);


			if($meta)
			{
				if(empty($post))
				{
					$post = new stdClass();
				}

				$post->value = $meta->value;
			}
		}

		return $post;
	}


	public function get_dtr_fields()
	{
		$this->db->where_in('setting_key', ['started_date', 'nightshift_start', 'nightshift_end', 'login_method', 'biometric', 'sync_frequency', 'biometrics', 'app_sync', 'manual', 'time_card']);
		$settings = $this->db->get('settings')->result();

		array_key_name($settings, 'setting_key');


		$array_options = ['dropdown', 'checkbox'];
		foreach($settings as $row)
		{
			$this->db->where(['key' => $row->code .':'. $row->setting_key, 'company_id' => @$this->current_user->company_id]);
			$meta = $this->db->get('settings_metadata')->row();

			if($meta)
			{
				$row->value = $meta->value;
			}
			else
			{
				$row->value =  $row->default_value;
			}

			// check if there is value on the settings_metada
			if($row->type == 'label')
			{
				// get child
				$row->child = $this->db->get_where('fields', ['parent_id'=> $row->id])->result();
			}
			// if has options
			else if(in_array($row->type, $array_options) && $row->options)
			{
				$row->options = parse_options($row);
			}
		}


		return $settings;
	}

	public function get_payroll_fields()
	{
		// set settings value
		$post = $this->get_by_code('payroll');

		array_key_name($post, 'setting_key');

		$array_options = ['dropdown', 'checkbox', 'table'];
		foreach($post as $row)
		{
			// check if there is value on the settings_metada

			if($row->type == 'label')
			{
				// get child
				$row->child = $this->db->get_where('settings', ['parent_id'=> $row->id])->result();
			}
			// if has options
			else if(in_array($row->type, $array_options) && $row->options)
			{
				$row->options = parse_options($row);
			}
		}

		// convert 1.1 to 10% on nightshift rate
		if( ! empty($post['nightshift_rate']))
		{
			$field = $post['nightshift_rate'];
			$nightshift_rate_value = (! empty($field->value)) ? $field->value : $field->default_value;
			$field->value = ($nightshift_rate_value * 100) - 100;
		}

		return $post;
	}
}
