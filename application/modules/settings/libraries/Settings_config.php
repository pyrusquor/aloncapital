<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_config
{
	private $_ci;
	private static $cache = array();

	function __construct($config = array())
	{
		$this->_ci =& get_instance();
		ci()->load->model('settings/settings_m');
	}
	
	public static function get_all($code, $default = [], $company_id=false)
	{
		if( ! $company_id)
		{
			$company_id = ($company_id ? 0 : @ci()->current_user->company_id);
		}
		
		$cache_name = $code.'_config'.$company_id;
		
		if (isset(self::$cache[$cache_name]))
		{
			return self::$cache[$cache_name];
		}
		
		// get all meta data
		ci()->db->like('key', $code.':');
		$results = ci()->db->get_where('settings_metadata', ['company_id'=>$company_id])->result();

		$config = [];
		foreach($results as $row)
		{
			$config[str_replace($code.':', '', $row->key)] = $row->value;
		}
		
		return self::$cache[$cache_name] = $config;
	}
	
	public static function get($code, $key, $default='', $company_id=false)
	{
		$cache_name = $code.' '.$key;
		
		/*if (isset(self::$cache[$cache_name]))
		{
			return self::$cache[$cache_name];
		}*/


		$row = ci()->settings_m->get_by_code($code, $key, $company_id);
		

		if( $row )
		{
			return self::$cache[$cache_name] = empty($row->value)? $default : $row->value;
		}

		return $default;
	}

	
	
}