<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Module_permissions_model extends MY_Model
{
    public $table = 'module_permissions'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'permission_id',
        'module_id',
        'is_create',
        'is_read',
        'is_update',
        'is_delete',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
    public $fields = [
        array(
            'field' => 'permission_id',
            'label' => 'Permission ID',
            'rules' => 'trim',
        ),
        array(
            'field' => 'module_id',
            'label' => 'Module ID',
            'rules' => 'trim',
        ),
        array(
            'field' => 'is_create',
            'label' => 'Create',
            'rules' => 'trim',
        ),
        array(
            'field' => 'is_read',
            'label' => 'Read',
            'rules' => 'trim',
        ),
        array(
            'field' => 'is_update',
            'label' => 'is_update',
            'rules' => 'trim',
        ),
        array(
            'field' => 'is_delete',
            'label' => 'Delete',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert']	=	$this->fields;
        $this->rules['update']	=	$this->fields;

        $this->has_one['permission'] = array('foreign_model' => 'permission/permission_model', 'foreign_table' => 'permissions', 'foreign_key' => 'id', 'local_key' => 'permission_id');
        $this->has_one['module'] = array('foreign_model' => 'menu/menu_model', 'foreign_table' => 'modules', 'foreign_key' => 'id', 'local_key' => 'module_id');

    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}