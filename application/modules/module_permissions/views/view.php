<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$permission_id = isset($data['permission_id']) && $data['permission_id'] ? $data['permission_id'] : '';
$module_id = isset($data['module_id']) && $data['module_id'] ? $data['module_id'] : '';
$is_create = isset($data['is_create']) && $data['is_create'] ? '<span class="text-success"><i class="flaticon2-check-mark"></i></span>' : '<span class="text-danger"><i class="flaticon2-cross"></i></span>';
$is_read = isset($data['is_read']) && $data['is_read'] ? '<span class="text-success"><i class="flaticon2-check-mark"></i></span>' : '<span class="text-danger"><i class="flaticon2-cross"></i></span>';
$is_update = isset($data['is_update']) && $data['is_update'] ? '<span class="text-success"><i class="flaticon2-check-mark"></i></span>' : '<span class="text-danger"><i class="flaticon2-cross"></i></span>';
$is_delete = isset($data['is_delete']) && $data['is_delete'] ? '<span class="text-success"><i class="flaticon2-check-mark"></i></span>' : '<span class="text-danger"><i class="flaticon2-cross"></i></span>';

$permission_name = isset($data['permission']['name']) && $data['permission']['name'] ? $data['permission']['name'] : 'N/A';
$module_name = isset($data['module']->name) && $data['module']->name ? $data['module']->name : 'N/A';


// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Module Permissions</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('module_permissions/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('module_permissions');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Permission
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $permission_name; ?></span>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Module
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $module_name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Create
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $is_create; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Read
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $is_read; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Update
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $is_update; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Delete
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $is_delete; ?></span>
                                </div>
                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
