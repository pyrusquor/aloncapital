<!--begin::Form-->
<form method="POST" class="kt-form kt-form--label-right" id="form_module_permission" enctype="multipart/form-data"
      action="<?php form_open('module_permissions/form/' . @$info['id']); ?>">
    <!-- CONTENT HEADER -->
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title"><?= $method ?> Module Permission</h3>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="form_module_permission"
                            data-ktwizard-type="action-submit" id="mp_submit">
                        <i class="fa fa-plus-circle"></i> Submit
                    </button>
                    <a href="<?php echo base_url('permission/view/') . $permission['id'] ; ?>" class="btn btn-label-instagram btn-elevate btn-sm">
                        <i class="fa fa-reply"></i> Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- CONTENT -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class=" kt-portlet__body">
                    <?php $this->load->view('view/_form'); ?>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- begin:: Footer -->
</form>
<!--end::Form-->