<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$permission_id = isset($permission['id']) && $permission['id'] ? $permission['id'] : '';
$permission_name = isset($permission['name']) && $permission['name'] ? $permission['name'] : ''; 
$module_permissions = isset($module_permissions) && $module_permissions ? $module_permissions : '';
// ==================== end: Add model fields ====================
?>

<!--begin: Datatable -->
<h4 class="d-none">Permission ID: <span class="text-dark" id="permissionID"><?=$permission_id?></span></h4>
<h4>Permission: <span class="text-dark"><?=$permission_name?></span></h4>
<table
  class="table table-striped- table-bordered table-hover table-checkable"
  id="form_module_permission"
>
  <thead>
    <tr>
      <th>ID</th>
      <!-- ==================== begin: Add header fields ==================== -->
      <th>Parent</th>
      <th>Module</th>
      <th style="width: 1%;">
        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
          <input type="checkbox" name="action[]" class="m-checkable" 
          onChange="checkAll(this)">
          <span></span>
        </label>
      </th>
      <th>Create</th>
      <th>Read</th>
      <th>Update</th>
      <th>Delete</th>
      <!-- ==================== end: Add header fields ==================== -->
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($module_permissions)): ?>
      <?php foreach($modules as $id => $data): ?>
        <?php foreach($module_permissions as $key => $value): ?> 
          <?php if($data['id'] == $value['module_id']): ?>
            <tr id="row_<?=$data['id']?>">
              <td><?=$value['module_id']?></td>
              <td><?=$data['name']?></td>
              <td><?=$data['description']?></td>
              <td>
                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" name="action[]" class="m-checkable delete_check" 
                data-rowid="row_<?=$value['module_id']?>" onChange="checkPermissions(this)">
                <span></span>
                </label>
              </td>
              <td>
                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                  <input type="checkbox" name="is_create[]" data-id="<?=$value['module_id']?>" data-permission="create" value="<?=$value['is_create']?>" class="m-checkable" id="is_create[]" <?php if($value['is_create']):?>checked<?php endif; ?>/>
                  <span></span>
                  </label>
              </td>
              <td>
                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                  <input type="checkbox" name="is_read[]" data-id="<?=$value['module_id']?>" data-permission="read" value="<?=$value['is_read']?>" class="m-checkable" id="is_read[]" <?php if($value['is_read']):?>checked<?php endif; ?>/>
                  <span></span>
                  </label>
              </td>
              <td>
                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                  <input type="checkbox" name="is_update[]" data-id="<?=$value['module_id']?>" data-permission="update" value="<?=$value['is_update']?>" class="m-checkable" id="is_update[]" <?php if($value['is_update']):?>checked<?php endif; ?>/>
                  <span></span>
                  </label>
              </td>
              <td>
                  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                  <input type="checkbox" name="is_delete[]" data-id="<?=$value['module_id']?>" data-permission="delete" value="<?=$value['is_delete']?>" class="m-checkable" id="is_delete[]" <?php if($value['is_delete']):?>checked<?php endif; ?>/>
                  <span></span>
                  </label>
              </td>
            </tr>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endforeach; ?>
    <?php else: ?>
      <?php foreach($modules as $key => $value): ?>
        <tr id="row_<?=$value['id']?>">
          <td><?=$value['id']?></td>
          <td><?=$value['name'] ? $value['name'] : '' ?></td>
          <td><?=$value['description']?></td>
          <td>
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
            <input type="checkbox" name="action[]" class="m-checkable delete_check" 
            data-rowid="row_<?=$value['id']?>" onChange="checkPermissions(this)">
            <span></span>
            </label>
          </td>
          <td>
              <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
              <input type="checkbox" name="is_create[]" data-id="<?=$value['id']?>" data-permission="create" class="m-checkable" id="is_create[]"/>
              <span></span>
              </label>
          </td>
          <td>
              <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
              <input type="checkbox" name="is_read[]" data-id="<?=$value['id']?>" data-permission="read" class="m-checkable" id="is_read[]"/>
              <span></span>
              </label>
          </td>
          <td>
              <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
              <input type="checkbox" name="is_update[]" data-id="<?=$value['id']?>" data-permission="update" class="m-checkable" id="is_update[]"/>
              <span></span>
              </label>
          </td>
          <td>
              <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
              <input type="checkbox" name="is_delete[]" data-id="<?=$value['id']?>" data-permission="delete" class="m-checkable" id="is_delete[]"/>
              <span></span>
              </label>
          </td>
        </tr>
      <?php endforeach; ?>
    <?php endif; ?>
  </tbody>
</table>
<!--end: Datatable -->
