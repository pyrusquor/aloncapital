<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Module_permissions extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Module_permissions_model', 'M_Module_permission');
        $this->load->model('permission/Permission_model', 'M_Permission');
        $this->load->model('menu/Menu_model', 'M_Module');

        $this->load->helper('form');

        $this->_table_fillables = $this->M_Module_permission->fillable;
        $this->_table_columns = $this->M_Module_permission->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Module_permission->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Module_permission->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }

        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showModulePermissions()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'permission_name' => true,
            'module_name' => true,
            'is_create' => true,
            'is_read' => true,
            'is_update' => true,
            'is_delete' => true,
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $module_permissions = $this->M_Module_permission->with_permission()->with_module()->order_by('id', 'DESC')->as_array()->get_all();
        $data = [];

        if ($module_permissions) {
            foreach($module_permissions as $key => $value) {
                $module_permissions[$key]['permission_name'] = @$value['permission']['name'];
                $module_permissions[$key]['module_name'] = @$value['module']->name;
            }

            foreach ($module_permissions as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );

        }

        echo json_encode($output);
        exit();
    }

    public function form($id = false, $update = false)
    {
        $method = "Create";
        if ($update) {$method = "Update";}
        // $this->view_data['modules'] = $modules = $this->M_Module->order_by('parent_id, name', 'ASC')->as_array()->get_all();
        
        $this->db->select('a.name, b.description, b.parent_id, b.id');
        $this->db->from('modules as a');
        $this->db->join('modules as b', 'b.parent_id = a.id', 'right');
        // $this->db->where('a.parent_id = 0 or b.parent_id = 0');
        $this->db->order_by('b.id');
        $this->db->order_by('b.ctr');
        $this->view_data['modules'] = $modules = $this->db->get()->result_array();

        // vdebug( $this->view_data['modules'] );

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            if($this->input->is_ajax_request()) {
                $module_permissions = $this->input->post('module_permissions_dict');
                $additional = [
                    'created_by' => $this->user->id,
                    'created_at' => NOW,
                ];

                if($module_permissions) {
                    if($update) {
                        
                        foreach($module_permissions as $key => $value) {
                            // Check if module permission exists
                            $existing_data = $this->M_Module_permission->where(array('module_id' => $key, 'permission_id' => $id))->get();
                            
                            if ($existing_data) {
                                $data = [
                                    'permission_id' => $id,
                                    'module_id' => $key,
                                    'is_create' => isset($value['is_create']) ? $value['is_create'] : 0,
                                    'is_read' => @$value['is_read'] ? $value['is_read'] : 0,
                                    'is_update' => @$value['is_update'] ? $value['is_update'] : 0,
                                    'is_delete' => @$value['is_delete'] ? $value['is_delete'] : 0,
                                    'updated_by' => $this->user->id,
                                    'updated_at' => NOW,
                                ];
                                
                                $module_permission = $this->M_Module_permission->update($data, $existing_data['id']);
                            } else {
                                $data = [
                                    'permission_id' => $id,
                                    'module_id' => $key,
                                    'is_create' => isset($value['is_create']) ? $value['is_create'] : 0,
                                    'is_read' => @$value['is_read'] ? $value['is_read'] : 0,
                                    'is_update' => @$value['is_update'] ? $value['is_update'] : 0,
                                    'is_delete' => @$value['is_delete'] ? $value['is_delete'] : 0,
                                ];
                                $module_permission = $this->M_Module_permission->insert($data + $additional);
                            }
                        }
    
                        if ($module_permission !== 0) {
    
                            $response['status'] = 1;
                            $response['message'] = 'Module permission(s) updated!';
    
                            echo json_encode($response);
                            exit(); 
                        }
                    } else {
                        foreach($module_permissions as $key => $value) {
                            $data = [
                                'permission_id' => $id,
                                'module_id' => $key,
                                'is_create' => isset($value['is_create']) ? $value['is_create'] : 0,
                                'is_read' => @$value['is_read'] ? $value['is_read'] : 0,
                                'is_update' => @$value['is_update'] ? $value['is_update'] : 0,
                                'is_delete' => @$value['is_delete'] ? $value['is_delete'] : 0,
                            ];
    
                            $module_permission = $this->M_Module_permission->insert($data + $additional);
                        }
    
                        if ($module_permission !== 0) {
    
                            $response['status'] = 1;
                            $response['message'] = 'Module permission(s) created!';
    
                            echo json_encode($response);
                            exit(); 
                        }
                    }
                }
            }
        }

        if ($id) {
            $this->view_data['permission'] = $this->M_Permission->where('id', $id)->get(); 
            $this->view_data['info'] = $this->M_Module_permission->get($id);
            
        }

        if($update) {
            $this->view_data['permission'] = $this->M_Permission->where('id', $id)->get(); 
            $module_permissions = $this->M_Module_permission->where('permission_id', $id)->get_all();
            
            // Get module permissions setiing
            if($module_permissions) {
                foreach($module_permissions as $key => $value) {
                    foreach($modules as $id => $data) {
                        // Check if module exists in array
                        if($this->checkData($module_permissions, $data['id'])) {
                            if($data['id'] != $value['module_id']) {
                                array_push($module_permissions, array(
                                    'id' => $data['id'],
                                    'name' => $data['name'],
                                    'module_id' => $data['id'],
                                    'is_create' => 0,
                                    'is_read' => 0,
                                    'is_update' => 0,
                                    'is_delete' => 0,
                                ));
                            } 
                        } 
                    }
                }
                $this->view_data['module_permissions'] = $module_permissions;
            }
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Module_permission->get($id);
            if ($list) {

                $deleted = $this->M_Module_permission->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Permission Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('module_permissions', $data, array('id' => $value));
                    $deleted = $this->M_Module_permission->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Module Permission Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_Module_permission->with_permission()->with_module()->as_array()->get($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import() {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach($sheetData as $key => $upload_data)
        {

            if($key > 0)
            {

                if ($this->input->post('status') == '1'){
                    $fields = array(
                        /* ==================== begin: Add model fields ==================== */
                        'permission_id' => $upload_data[0],
                        'module_id' => $upload_data[1],
                        'is_create' => $upload_data[2],
                        'is_read' => $upload_data[3],
                        'is_update' => $upload_data[4],
                        'is_delete' => $upload_data[5],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $permission_id = $upload_data[0];
                    $module_permission = $this->M_Module_permission->get($permission_id);

                    if($module_permission){
                        $result = $this->M_Module_permission->update($fields, $permission_id);
                    }

                } else {

                    if( ! is_numeric($upload_data[0]))
                    {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[1],
                            'is_active' => $upload_data[2],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Module_permission->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[0],
                            'is_active' => $upload_data[1],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Module_permission->insert($fields);
                    }

                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if($err == 0)
        {
            $this->notify->success('CSV successfully imported.', 'module_permission');
        }
        else{
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: '. base_url().'module_permission');
        die();
    }

    public function export_csv()
    {
        if ( $this->input->post() && ($this->input->post('update_existing_data') !== '') ) {

            $_ued	=	$this->input->post('update_existing_data');

            $_is_update	=	$_ued === '1' ? TRUE : FALSE;

            $_alphas		=	[];
            $_datas			=	[];

            $_titles[]	=	'id';

            $_start	=	3;
            $_row		=	2;

            $_filename	=	'Module Permission CSV Template.csv';

            $_fillables	=	$this->_table_fillables;
            if ( !$_fillables ) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'module_permission');
            }

            foreach ( $_fillables as $_fkey => $_fill ) {

                if ( (strpos( $_fill, 'created_') === FALSE) && (strpos( $_fill, 'updated_') === FALSE) && (strpos( $_fill, 'deleted_') === FALSE) ) {

                    $_titles[]	=	$_fill;
                } else {

                    continue;
                }
            }

            if ( $_is_update ) {

                $_group	=	$this->M_Module_permission->as_array()->get_all();
                if ( $_group ) {

                    foreach ( $_titles as $_tkey => $_title ) {

                        foreach ( $_group as $_dkey => $li ) {

                            $_datas[$li['id']][$_title]	=	isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if ( isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id' ) {

                    unset($_titles[0]);
                }
            }

            $_alphas			=	$this->__get_excel_columns(count($_titles));
            $_xls_columns	=	array_combine($_alphas, $_titles);
            $_firstAlpha	=	reset($_alphas);
            $_lastAlpha		=	end($_alphas);

            $_objSheet	=	$this->excel->getActiveSheet();
            $_objSheet->setTitle('Module Permissions');
            $_objSheet->setCellValue('A1', 'MODULE PERMISSIONS');
            $_objSheet->mergeCells('A1:'.$_lastAlpha.'1');

            foreach ( $_xls_columns as $_xkey => $_column ) {

                $_objSheet->setCellValue($_xkey.$_row, $_column);
            }

            if ( $_is_update ) {

                if ( isset($_datas) && $_datas ) {

                    foreach ( $_datas as $_dkey => $_data ) {

                        foreach ( $_alphas as $_akey => $_alpha ) {

                            $_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha.$_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha.$_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha.$_start.':'.$_lastAlpha.$_start);

                    $_style	=	array(
                        'font'  => array(
                            'bold'	=>	FALSE,
                            'size'	=>	9,
                            'name'	=>	'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha.$_start.':'.$_lastAlpha.$_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ( $_alphas as $_alpha ) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style	=	array(
                'font'  => array(
                    'bold'	=>	TRUE,
                    'size'	=>	10,
                    'name'	=>	'Verdana'
                )
            );
            $_objSheet->getStyle('A1:'.$_lastAlpha.$_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="'.$_filename.'"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export() {

        $_db_columns	=	[];
        $_alphas			=	[];
        $_datas				=	[];

        $_titles[]	=	'#';

        $_start	=	3;
        $_row		=	2;
        $_no		=	1;

        $module_permissions	=	$this->M_Module_permission->as_array()->get_all();
        if ( $module_permissions ) {

            foreach ( $module_permissions as $_lkey => $module_permission ) {

                $_datas[$module_permission['id']]['#']	=	$_no;

                $_no++;
            }

            $_filename	=	'list_of_permissions_'.date('m_d_y_h-i-s',time()).'.xls';

            $_objSheet	=	$this->excel->getActiveSheet();

            if ( $this->input->post() ) {

                $_export_column	=	$this->input->post('_export_column');
                if ( $_export_column ) {

                    foreach ( $_export_column as $_ekey => $_column ) {

                        $_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'module_permission');
                }
            } else {

                $_filename	=	'list_of_permissions'.date('m_d_y_h-i-s',time()).'.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns	=	$this->_table_fillables;
                if ( !$_db_columns ) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'module_permission');
                }
            }

            if ( $_db_columns ) {

                foreach ( $_db_columns as $key => $_dbclm ) {

                    $_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ( (strpos( $_name, 'created_') === FALSE) && (strpos( $_name, 'updated_') === FALSE) && (strpos( $_name, 'deleted_') === FALSE) && ($_name !== 'id') ) {

                        if ( (strpos( $_name, '_id') !== FALSE) ) {

                            $_column	=	$_name;

                            $_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        } elseif ( (strpos( $_name, 'is_') !== FALSE) ) {

                            $_column	=	$_name;

                            $_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ( $module_permissions as $_lkey => $module_permission ) {

                                $_datas[$module_permission['id']][$_title]	=	isset($module_permission[$_column]) && ($module_permission[$_column] !== '') ? Dropdown::get_static('bool', $module_permission[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ( $module_permissions as $_lkey => $module_permission ) {

                                if ( $_name === 'status' ) {

                                    $_datas[$module_permission['id']][$_title]	=	isset($module_permission[$_name]) && $module_permission[$_name] ? Dropdown::get_static('inventory_status', $module_permission[$_name], 'view') : '';
                                } else {

                                    $_datas[$module_permission['id']][$_title]	=	isset($module_permission[$_name]) && $module_permission[$_name] ? $module_permission[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas	=	$this->__get_excel_columns(count($_titles));

                $_xls_columns	=	array_combine($_alphas, $_titles);
                $_firstAlpha	=	reset($_alphas);
                $_lastAlpha		=	end($_alphas);

                foreach ( $_xls_columns as $_xkey => $_column ) {

                    $_title	=	($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey.$_row, $_title);
                }

                $_objSheet->setTitle('List of Module Permissions');
                $_objSheet->setCellValue('A1', 'LIST OF MODULE PERMISSIONS');
                $_objSheet->mergeCells('A1:'.$_lastAlpha.'1');

                if ( isset($_datas) && $_datas ) {

                    foreach ( $_datas as $_dkey => $_data ) {

                        foreach ( $_alphas as $_akey => $_alpha ) {

                            $_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha.$_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha.$_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha.$_start.':'.$_lastAlpha.$_start);

                    $_style	=	array(
                        'font'  => array(
                            'bold'	=>	FALSE,
                            'size'	=>	9,
                            'name'	=>	'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha.$_start.':'.$_lastAlpha.$_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ( $_alphas as $_alpha ) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style	=	array(
                    'font'  => array(
                        'bold'	=>	TRUE,
                        'size'	=>	10,
                        'name'	=>	'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:'.$_lastAlpha.$_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="'.$_filename.'"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'module_permission');
            }
        } else {

            $this->notify->error('No Record Found', 'module_permission');
        }
    }

    /**
     * Check if item exists in an array
     * returns false if item exists
     * @param module permissions data
     * 
     * @return boolean  
     * */ 
    function checkData($arr, $item) {
        foreach($arr as $key => $value) {
            if (in_array($item, $value)) {
                return false;
            }
        }
        return true;
    }

}
