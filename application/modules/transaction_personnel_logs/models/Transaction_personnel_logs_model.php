<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_personnel_logs_model extends MY_Model {

	public $table = 'transaction_personnel_logs'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
        'transaction_id', 
        'personnel_id', 
        'assigned_at', 
        'is_active', 
        'created_by',
		'created_at',
		'updated_by',
		'updated_at'
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

	}

	public function total_overdue_collections($limit=null, $start=null, $where = array()) {

		$this->db->select(
			"tp.due_date as 'tp_due_date', 
			transactions.due_date,
			transactions.buyer_id,
			transactions.id,
			transactions.property_id,
			tp.total_amount,
			tp.particulars,
			transactions.reference,
			"
		);
		$this->db->from("transaction_payments tp");
		$this->db->where("tp.is_paid = 0");
		$this->db->where("tp.deleted_at IS NULL");
		$this->db->where("transactions.deleted_at IS NULL");
		$this->db->join("transactions", "transactions.id = tp.transaction_id");

		$this->db->where("tp.due_date < ", date("Y-m-d", strtotime("-1 days")));

		$this->db->limit(500);

		if(array_key_exists('personnel_id', $where)) {

			$this->db->where("transactions.personnel_id" , $where['personnel_id']);
			
		}

		if(array_key_exists('project_id', $where)) {

			$this->db->where("transactions.project_id" , $where['project_id']);
			
		}

		$this->db->limit($limit, $start);

		
		$query = $this->db->get();

		$result = $query->result_array();
		
		return $result;

	}

	public function total_due_today($limit=null, $start=null, $where = array()) {

		$this->db->select(
			"tp.due_date as 'tp_due_date', 
			transactions.due_date,
			transactions.buyer_id,
			transactions.id,
			transactions.property_id,
			tp.total_amount,
			tp.particulars,
			transactions.reference,
			"
		);
		$this->db->from("transaction_payments tp");
		$this->db->where("tp.is_paid = 0");
		$this->db->where("tp.deleted_at IS NULL");
		$this->db->where("transactions.deleted_at IS NULL");
		$this->db->join("transactions", "transactions.id = tp.transaction_id");
		
		$this->db->where("tp.due_date = ", date("Y-m-d"));
		$this->db->limit(500);

		if(array_key_exists('personnel_id', $where)) {

			$this->db->where("transactions.personnel_id" , $where['personnel_id']);
			
		}

		if(array_key_exists('project_id', $where)) {

			$this->db->where("transactions.project_id" , $where['project_id']);
			
		}

		$this->db->limit($limit, $start);

		
		$query = $this->db->get();

		$result = $query->result_array();
		
		return $result;

	}

	public function total_upcoming_collections($limit=null, $start=null, $where = array()) {

		$this->db->select(
			"tp.due_date as 'tp_due_date', 
			transactions.due_date,
			transactions.buyer_id,
			transactions.id,
			transactions.property_id,
			tp.total_amount,
			tp.particulars,
			transactions.reference,
			"
		);
		$this->db->from("transaction_payments tp");
		$this->db->where("tp.is_paid = 0");
		$this->db->where("tp.deleted_at IS NULL");
		$this->db->where("transactions.deleted_at IS NULL");
		$this->db->join("transactions", "transactions.id = tp.transaction_id");
		$this->db->limit(500);
		$date = NOW;
		$from_due = date("Y-m-d", strtotime($date));
		$to_due = date("Y-m-t", strtotime($date));
		// vdebug($to_due);

		$this->db->where('tp.due_date BETWEEN "' . $from_due .'" AND "' .  $to_due . '"');

		if(array_key_exists('personnel_id', $where)) {

			$this->db->where("transactions.personnel_id" , $where['personnel_id']);
			
		}

		if(array_key_exists('project_id', $where)) {

			$this->db->where("transactions.project_id" , $where['project_id']);
			
		}

		$this->db->limit($limit, $start);

		
		$query = $this->db->get();

		$result = $query->result_array();
		// vdebug($result);
		
		return $result;

	}

	public function calc_overdue($date = 0, $due_date = 0) {
        
		$now = strtotime($date);

        $your_date = strtotime($due_date);
        $datediff = $now - $your_date;
        $over_due = floor($datediff / (60 * 60 * 24));
        $months = floor($over_due / 30);
        $days = $over_due;

        return $days;

    }

}