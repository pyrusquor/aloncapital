<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Address_provinces_model extends MY_Model
{
    public $table = 'address_provinces'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = []; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public function __construct()
	{
		parent::__construct();

		$this->has_one['region'] = array('foreign_model' => 'locations/Address_regions_model', 'foreign_table' => 'address_regions', 'foreign_key' => 'regCode', 'local_key' => 'regCode');
	}
}