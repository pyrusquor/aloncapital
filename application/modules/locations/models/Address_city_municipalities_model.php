<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Address_city_municipalities_model extends MY_Model
{
    public $table = 'address_city_municipalities'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = []; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
}