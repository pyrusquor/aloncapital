<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Price_logs_model extends MY_Model {

	public $table = 'property_price_logs'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'date_of_price_increase',
		'total_selling_price',
		'percentage_increase',
		'property_id',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->has_one['project'] = array('foreign_model'=>'Project_model','foreign_table'=>'projects','foreign_key'=>'id','local_key'=>'project_id');
		$this->has_one['promo'] = array('foreign_model'=>'promo/Promo_model','foreign_table'=>'promos','foreign_key'=>'id','local_key'=>'promo_id');
		$this->has_one['model'] = array('foreign_model'=>'House_model','foreign_table'=>'house_models','foreign_key'=>'id','local_key'=>'model_id');
		$this->has_one['interior'] = array('foreign_model'=>'Interior_model','foreign_table'=>'house_model_interiors','foreign_key'=>'id','local_key'=>'interior_id');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

    }
    
}
