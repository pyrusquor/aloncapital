<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Property_model extends MY_Model
{

	public $table = 'properties'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'project_id',
		'name',
		'phase',
		'cluster',
		'block',
		'lot',
		'model_id',
		'interior_id',
		'package_type_id',
		'lot_area',
		'floor_area',
		'lot_price_per_sqm',
		'total_lot_price',
		'model_price',
		'total_selling_price',
		'discounted_total_selling_price',
		'adjusted_price',
		'hlurb_classification_id',
		'status',
		'progress',
		'promo_id',
		'promo_amount',
		'sales_arm_id',
		'cts_number',
		'cts_amount',
		'doas_amount',
		'date_registered',
		'cts_notarized_date',
		'doas_notarized_date',
		'title_number',
		'tax_declaration_lot',
		'tax_declaration_building',
		'max_selling_price',
		'tct_max_price_annotation',
		'is_active',
		'raw_land_cost',
		'development_cost',
		'construction_in_progress_cost',
		'turn_over_date',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = TRUE;
		$this->return_as = 'array';

		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->has_one['project'] = array('foreign_model' => 'project/Project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
		$this->has_many['projects'] = array('foreign_model' => 'Project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
		$this->has_one['promo'] = array('foreign_model' => 'promo/Promo_model', 'foreign_table' => 'promos', 'foreign_key' => 'id', 'local_key' => 'promo_id');
		$this->has_one['model'] = array('foreign_model' => 'House_model', 'foreign_table' => 'house_models', 'foreign_key' => 'id', 'local_key' => 'model_id');
		$this->has_one['interior'] = array('foreign_model' => 'Interior_model', 'foreign_table' => 'house_model_interiors', 'foreign_key' => 'id', 'local_key' => 'interior_id');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
	}


	public function insert_price_logs($property_id = 0)
	{

		$this->db->from("audit_trail audit");
		$this->db->join("properties", "properties.id = audit.affected_id");
		$this->db->where("audit.affected_table = ", "properties");
		$this->db->where("properties.id = ", $property_id);
		$this->db->where("audit.deleted_at IS NULL");
		$this->db->where("properties.deleted_at IS NULL");
		$query = $this->db->get();

		$result = $query->result_array();

		$price_logs = $this->get_price_logs();

		foreach ($result as $key => $pl) {
			$prev_record = json_decode($pl['previous_record'], true);
			$new_record = json_decode($pl['new_record'], true);

			$prev_price = @$pl["total_selling_price"];
			$updated_price = @$new_record['total_selling_price'];

			// if($price_logs[$key]["total_selling_price"] != $updated_price) {
			// 	vdebug("stopppp");
			// }
			if ($updated_price != $prev_price) {

				$data = array(
					"date_of_price_increase" => @$pl['updated_at'],
					"total_selling_price" => @$pl['total_selling_price'],
					"property_id" => $property_id,
					"percentage_increase" => get_percentage($prev_price, $updated_price),
				);

				$this->db->insert("property_price_logs", $data);
			}
		}
	}

	public function get_price_logs()
	{

		$this->db->from("property_price_logs");

		$query = $this->db->get();

		$result = $query->result_array();

		return $result;
	}
}
