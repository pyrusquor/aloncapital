<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Property extends MY_Controller
{
    private $fields = [
        array(
            'field' => 'info[project_id]',
            'label' => 'Project Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[name]',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[status]',
            'label' => 'General Status',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[model_id]',
            'label' => 'Property Model Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[interior_id]',
            'label' => 'House Interior',
            'rules' => 'trim|required',
        ),
        // array(
        //     'field' => 'info[phase]',
        //     'label' => 'Phase #',
        //     'rules' => 'trim',
        // ),
        // array(
        //     'field' => 'info[cluster]',
        //     'label' => 'Cluster #',
        //     'rules' => 'trim|numeric',
        // ),
        // array(
        //     'field' => 'info[block]',
        //     'label' => 'Block #',
        //     'rules' => 'trim|required|numeric',
        // ),
        // array(
        //     'field' => 'info[lot]',
        //     'label' => 'Lot #',
        //     'rules' => 'trim|required|numeric',
        // ),
        array(
            'field' => 'info[lot_area]',
            'label' => 'Lot Area',
            'rules' => 'trim|required|numeric',
        ),
        array(
            'field' => 'info[floor_area]',
            'label' => 'Floor Area',
            'rules' => 'trim|numeric',
        ),
        array(
            'field' => 'info[lot_price_per_sqm]',
            'label' => 'Lot Price per SQM',
            'rules' => 'trim|required|numeric',
        ),
        array(
            'field' => 'info[total_lot_price]',
            'label' => 'Total Lot Price',
            'rules' => 'trim|required|numeric',
        ),
        array(
            'field' => 'info[total_selling_price]',
            'label' => 'Total Selling Price',
            'rules' => 'trim|required|numeric',
        ),
        array(
            'field' => 'info[model_price]',
            'label' => 'Model Price',
            'rules' => 'trim|numeric',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Property_model', 'M_property');
        $this->load->model('project/Project_model', 'M_project');
        $this->load->model('house/House_model', 'M_house');
        $this->load->model('interior/Interior_model', 'M_interior');
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('promos/Promo_model', 'M_promo');
        $this->load->model('seller/Seller_model', 'M_seller');
        $this->load->model('prospect/Prospect_model', 'M_prospect');
        $this->load->model('sellers_reservations/Sellers_reservations_model', 'M_sellers_reservations');
        $this->load->model('seller_teams/Seller_teams_model', 'M_seller_team');
        $this->load->model('property/Price_logs_model', 'M_price_logs');
        $this->load->model('transaction/Transaction_model', 'M_transaction');
        $this->load->model('storage/Storage_model', 'M_Storage');
        $this->load->model('transaction/Transaction_discount_model', 'M_transaction_discount');

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Mortgage computation library
        $this->load->library('mortgage_computation');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 20;

        $this->_table_fillables = $this->M_property->fillable;
        $this->_table_columns = $this->M_property->__get_columns();
        $this->_table = 'properties';

        $this->additional = [
            'is_active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW
        ];
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills, 'properties');
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // // Get record count
        // // $conditions['returnType'] = 'count';
        // $this->view_data['totalRec'] = $totalRec = $this->M_property->count_rows();

        // // Pagination configuration
        // $config['target'] = '#propertyContent';
        // $config['base_url'] = base_url('property/paginationData');
        // $config['total_rows'] = $totalRec;
        // $config['per_page'] = $this->perPage;
        // $config['link_func'] = 'PropertyPagination';

        // // Initialize pagination library
        // $this->ajax_pagination->initialize($config);

        // // $records = $this->M_property->with_project('fields:name')
        // // ->as_array()
        // // ->get_all();

        // $records = [];

        // $records = $this->M_property
        //     ->with_project('fields:name')
        //     ->as_array()
        //     ->limit(5000, 0)
        //     ->get_all();

        // $records = [];

        // foreach ($records as $key => $r) {
        //     if (array_key_exists("property", $r)) {
        //         $this->M_Storage->create_folder($r['property']['name'], $r['name']);
        //     }
        // }

        // $this->view_data['records'] = $records = $this->M_property
        //     ->with_project('fields:name')
        //     ->with_model('fields:name')
        //     ->with_interior('fields:name')
        //     ->as_array()
        //     ->limit($this->perPage, 0)
        //     ->get_all();

        $status_tiles = array();

        $bg_colors = array(
            '1' => 'kt-portlet--solid-success',
            '2' => 'kt-portlet--solid-warning',
            '3' => 'kt-portlet--solid-danger',
            '4' => 'kt-portlet--solid-info',
            '5' => 'kt-portlet--solid-gray'
        );

        $icons = array(
            '1' => 'fas fa-check',
            '2' => 'far fa-calendar-check',
            '3' => 'fas fa-gavel',
            '4' => 'fas fa-info',
            '5' => 'fas fa-times'
        );

        foreach (Dropdown::get_static('property_status') as $key => $value) {
            if ($key) {

                $this->db->select('SUM(total_selling_price) as total')->select('COUNT(id) as count')->from('properties')->where('status', $key);

                $query = $this->db->get()->row_array();

                array_push($status_tiles, array(
                    'bg-color' => isset($bg_colors[$key]) ? $bg_colors[$key] : $bg_colors['1'],
                    'icon' => @$icons[$key],
                    'status' => $value,
                    'total' => $query['total'] ? $query['total'] : 0,
                    'count' => $query['count']
                ));
            }
        }

        $this->view_data['status_tiles'] = $status_tiles;

        // $this->view_data['records'] = [];

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function search($params)
    {
        $property_name = isset($params['property_name']) && !empty($params['property_name']) ? $params['property_name'] : null;
        $project_id = isset($params['project_id']) && !empty($params['project_id']) ? $params['project_id'] : null;
        $status = isset($params['status']) && !empty($params['status']) ? $params['status'] : null;
        $phase = isset($params['phase']) && !empty($params['phase']) ? $params['phase'] : null;
        $block = isset($params['block']) && !empty($params['block']) ? $params['block'] : null;
        $lot = isset($params['lot']) && !empty($params['lot']) ? $params['lot'] : null;
        $hlurb_classification = isset($params['hlurb_classification']) && !empty($params['hlurb_classification']) ? $params['hlurb_classification'] : null;

        if ($property_name) {
            $this->M_property->like('name', $property_name, 'both');
        }

        if ($project_id) {
            $this->M_property->where('project_id', $project_id);
        }

        if ($status) {
            $this->M_property->where('status', $status);
        }

        if ($phase) {
            $this->M_property->where('phase', $phase);
        }

        if ($block) {
            $this->M_property->where('block', $block);
        }

        if ($lot) {
            $this->M_property->where('lot', $lot);
        }

        if ($hlurb_classification) {
            $this->M_property->where('hlurb_classification_id', $hlurb_classification);
        }

        $properties = $this->M_property
            ->with_project()
            ->with_model()
            ->with_interior()
            ->as_array()
            ->limit(5000)
            ->order_by('created_at', 'DESC')
            ->get_all();

        return $properties;
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'name' => true,
            'project' => true,
            'project_id' => true,
            'phase' => true,
            'block' => true,
            'lot' => true,
            'lot_area' => true,
            'total_selling_price' => true,
            'model' => true,
            'interior' => true,
            'status' => true,
            'hlurb_classification_id' => true,
            'project_id' => true,
            'model_id' => true,
            'interior_id' => true,
            'status' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_property
                ->with_project('fields:name')
                ->with_model('fields:name')
                ->with_interior('fields:name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("name like '%$searchValue%'");
                $query->or_where("phase like '%$searchValue%'");
                $query->or_where("block like '%$searchValue%'");
                $query->or_where("lot like '%$searchValue%'");
                $query->or_where("lot_area like '%$searchValue%'");
                $query->or_where("total_selling_price like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'name' => [
                    'data' => $filters['property_name'] ?? null,
                    'operator' => 'like',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'status' => [
                    'data' => $filters['status'] ?? null,
                    'operator' => '=',
                ],
                'phase' => [
                    'data' => $filters['phase'] ?? null,
                    'operator' => 'like',
                ],
                'block' => [
                    'data' => $filters['block'] ?? null,
                    'operator' => 'like',
                ],
                'lot' => [
                    'data' => $filters['lot'] ?? null,
                    'operator' => 'like',
                ],
                'hlurb_classification_id' => [
                    'data' => $filters['hlurb_classification'] ?? null,
                    'operator' => 'like',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {
                        $items[$key]['project_id'] = isset($value['project']['name']) && $value['project']['name'] ? $value['project']['name'] : 'N/A';
                        $items[$key]['model_id'] = isset($value['model']['name']) && $value['model']['name'] ? $value['model']['name'] : 'N/A';
                        $items[$key]['interior_id'] = isset($value['interior']['name']) && $value['interior']['name'] ? $value['interior']['name'] : 'N/A';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_property->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems
        );

        echo json_encode($response);
        exit();
    }

    public function quotation($id = false)
    {
        if ($id) {
            if ($this->input->post()) {

                $formData = $this->input->post();
                $this->view_data['info'] = $info = $this->M_property->with_project('fields:name')->with_model('fields:name,is_lot')->with_interior('fields:name')->with_promo('fields:name')->as_array()->get($id);

                $billing['period_term'] = $formData['option1Terms']; //number of installment months
                $billing['effectivity_date'] = date('Y-m-d H:i:s'); //start date of ammort
                $billing['interest_rate'] = $formData['option1Equity']; //(interest rate)
                $opt_1_percentage_monthly = get_percentage_amount($formData['option1Equity'], $info['total_selling_price']);

                $opt_1_total_dp_diff = $info['total_selling_price'] - $opt_1_percentage_monthly;

                $opt_1_total_dp = $info['total_selling_price'] - $opt_1_total_dp_diff;

                $opt_1_monthly_payment = monthly_payment($opt_1_total_dp, 0, $formData['option1Terms']);
                $billing['period_amount'] = $formData['option1RFFee']; //(amount of dp or loan balance)
                $collectible_price = $this->view_data['info']['total_selling_price']; //price of property
                $monthly_payment = $opt_1_monthly_payment; //0 / default

                $this->view_data['formData'] = $formData;

                // vdebug($this->view_data['info']);

                $this->mortgage_computation->reconstruct($billing, $collectible_price, $monthly_payment);
                $steps = $this->mortgage_computation->breakdown();
                // vdebug($steps);

                $this->view_data['payments'] = $steps;

                $this->template->build('quotation', $this->view_data);

                $generateHTML = $this->load->view('quotation', $this->view_data, true);

                echo $generateHTML;
                die();

                pdf_create($generateHTML, 'generated_form');
            }
        } else {

            show_404();
        }
    }

    // public function reservation()
    // { 
    //     if ($this->input->post()) { 
    //         $response['status'] = 0;
    //         $response['msg'] = 'Oops! Please refresh the page and try again.';

    //         //  '1' => 'Available', 
    //         //  '2' => 'Reserved', 
    //         //  '3' => 'Sold', 
    //         //  '4' => 'Hold', 
    //         //  '5' => 'Not For Sale'

    //         $reservation = $this->input->post();
    //         $property_id = $reservation['property_id'];
    //         $property_data = ["status" => 2];



    //         $this->M_property->update($property_data, $property_id);



    //         $data = $this->input->post();
    //         $data['expiration_date'] = date('Y-m-d', strtotime($data['expected_reservation_date']. ' + 3 days'));
    //         $data['seller_id'] = $this->user->id;
    //         $data['is_active'] = 1;

    //         if($data) {
    //             $result = $this->M_sellers_reservations->insert($data);
    //             if ($result === false) {

    //                 // Validation
    //                 $this->notify->error('Oops something went wrong.');
    //             } else {
    //                 $reserved = ['is_active' => 2];
    //                 $this->M_sellers_reservations->update($reserved, $id);

    //                 // Success
    //                 $this->notify->success('Reserved successfully.', 'property');
    //             }
    //         }

    //     }  else {
    //         show_404();
    //     }
    // }

    public function reservation()
    {
        if ($this->input->post()) {
            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            // PROPERTY STATUS
            //  '1' => 'Available', 
            //  '2' => 'Reserved', 
            //  '3' => 'Sold', 
            //  '4' => 'Hold', 
            //  '5' => 'Not For Sale'

            // RESERVATION STATUS
            // 1 - Reserved
            // 2 - Completed
            // 3 - Cancelled
            // 4 - Expired
            $data = $this->input->post();
            $data['expected_reservation_date'] = date('Y-m-d H:i:s', strtotime(NOW));;
            $data['expiration_date'] = date('Y-m-d H:i:s', strtotime(NOW . ' + 3 days'));
            $data['seller_id'] = $this->user->id;
            $data['is_active'] = 1;

            if ($data) {
                $result = $this->M_sellers_reservations->insert($data);
                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                } else {
                    $form = $this->input->post();
                    $property_id = $form['property_id'];
                    $property_data = ["status" => 4];

                    $this->M_property->update($property_data, $property_id);

                    $reservation["is_active"] = 0;
                    $reservation["reservation_status"] = 1;
                    $this->M_sellers_reservations->update(
                        $reservation,
                        $result
                    );

                    // Success
                    $this->notify->success('Reserved successfully.', 'property');
                }
            }
        } else {
            show_404();
        }
    }


    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            // Input from Advanced Filter
            $project_id = $this->input->post('project_id');
            $status_id = $this->input->post('status_id');
            $phase = $this->input->post('phase');
            $block = $this->input->post('block');
            $lot = $this->input->post('lot');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_property->count_rows();
            $where = array();

            // Pagination configuration
            $config['target'] = '#propertyContent';
            $config['base_url'] = base_url('property/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'PropertyPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('name', $keyword, 'both');
                $this->db->or_like('block', $keyword, 'both');
                $this->db->or_like('lot', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id) && !empty($project_id)) :
                $this->db->where('project_id', $project_id);
            endif;

            if (!empty($status_id) && !empty($status_id)) :
                $this->db->where('status', $status_id);
            endif;

            if (!empty($block) && !empty($block)) :
                $this->db->where('block', $block);
            endif;

            if (!empty($lot) && !empty($lot)) :
                $this->db->where('lot', $lot);
            endif;

            if (!empty($phase) && !empty($phase)) :
                $this->db->where('phase', $phase);
            endif;

            $totalRec = $this->M_property->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('name', $keyword, 'both');
                $this->db->or_like('block', $keyword, 'both');
                $this->db->or_like('lot', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id) && !empty($project_id)) :
                $this->db->where('project_id', $project_id);
            endif;

            if (!empty($status_id) && !empty($status_id)) :
                $this->db->where('status', $status_id);
            endif;

            if (!empty($block) && !empty($block)) :
                $this->db->where('block', $block);
            endif;

            if (!empty($lot) && !empty($lot)) :
                $this->db->where('lot', $lot);
            endif;

            if (!empty($phase) && !empty($phase)) :
                $this->db->where('phase', $phase);
            endif;

            $this->view_data['records'] = $records = $this->M_property->with_project('fields:name')->with_model('fields:name')->with_interior('fields:name')
                ->limit($this->perPage, $offset)
                ->get_all();

            $this->load->view('property/_filter', $this->view_data, false);
        }
    }

    public function view($id = false, $_type = 'build')
    {

        if ($id) {

            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['info'] = $this->M_property->with_project('fields:name')->with_model('fields:name,is_lot')->with_interior('fields:name')->with_promo('fields:name')
                ->as_array()->get($id);

            $this->view_data['audit'] = $this->__get_audit($id);
            $this->view_data['price_logs'] = $this->M_price_logs->where("property_id", $id)->get_all();
            $this->view_data['sales_logs'] = $this->M_transaction->with_buyer()->with_property()->where("property_id", $id)->get_all();


            if ($this->view_data['info']) {

                if ($_type == "json") {
                    echo json_encode($this->view_data);
                } elseif ($_type == 'modal') {
                    $property = $this->load->view('modals/property_info_modal', $this->view_data, true);
                    echo json_encode($property);
                } else {
                    $this->template->build('view', $this->view_data);
                }
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            if ($this->form_validation->run() === true) {
                $post = $this->input->post();
                $property_info = $post['info'];
                // $promos = $post['promos'];

                // trim "0" prefixes
                $property_info["phase"] = ltrim($property_info["phase"], "0") ? ltrim($property_info["phase"], "0") : "0";

                $property_info["block"] = ltrim($property_info["block"], "0") ? ltrim($property_info["block"], "0") : "0";

                $property_info["lot"] = ltrim($property_info["lot"], "0") ? ltrim($property_info["lot"], "0") : "0";

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    // vdebug($property_info);
                    $propertyID = $this->M_property->update($property_info + $additional, $id);
                    $this->M_property->insert_price_logs($propertyID);
                } else {
                    $additional = [
                        'is_active' => 1,
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $project_id = $property_info['project_id'];

                    $_p = $this->M_project->get($project_id);

                    $project_name = $_p['name'];

                    $this->M_Storage->create_folder($project_name, $property_info['name']);

                    // $this->process_promos($promos);

                    $propertyID = $this->M_property->insert($property_info + $additional);
                    $this->M_property->insert_price_logs($propertyID);
                }

                $response['status'] = 1;
                $response['message'] = 'Property Successfully ' . $method . 'd!';
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['project'] = $this->M_project->as_array()->get_all();
        $this->view_data['method'] = $method;
        $this->view_data['promos'] = $this->M_promo->as_array()->get_all();

        if ($this->view_data['project']) {
            if ($id) {
                $this->view_data['info'] = $this->M_property->with_project('fields:name')->with_model('fields:name, sub_type_id')->with_interior('fields:name')
                    ->as_array()->get($id);
            }

            $this->template->build('form', $this->view_data);
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_property->get($id);
            if ($list) {

                $deleted = $this->M_property->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Property successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_property->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Property successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];
        $_extra_datas = [];
        $_adatas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $_filters = $this->input->post('filters');
        $filters = explode(';', $_filters);

        if ($filters) {
            foreach ($filters as $key => $filter) {
                # code...
                $filter = explode(':', $filter);

                if ($filter) {

                    $key = $filter[0];
                    $value = @$filter[1];

                    if ($value) {
                        if ($key == "status_id") {
                            $key = "status";
                        }
                        $this->db->where($key, $value);
                    }
                }
            }
        }
        $properties = $this->M_property->with_project('fields:name')
            ->with_model('fields:name')->with_interior('fields:name')
            ->as_array()->get_all();
        if ($properties) {

            foreach ($properties as $skey => $property) {

                $_datas[$property['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_properties' . date('m_d_y_h-i-s', time()) . '.xls';

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                $_additional_column = $this->input->post('_additional_column');

                if ($_export_column) {
                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id') && ($_name !== 'user_id')) {

                        $_column = $_name;

                        $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                        $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        foreach ($properties as $skey => $property) {

                            if ($_column === 'project_id') {

                                $_datas[$property['id']][$_title] = isset($property['project']) && $property['project'] ? $property['project']['name'] : '';
                            } else if ($_column === 'model_id') {

                                $_datas[$property['id']][$_title] = isset($property['model']) && $property['model'] ? $property['model']['name'] : '';
                            } else if ($_column === 'interior_id') {

                                $_datas[$property['id']][$_title] = isset($property['interior']) && $property['interior'] ? $property['interior']['name'] : '';
                            } else {
                                $_datas[$property['id']][$_title] = isset($property[$_name]) && $property[$_name] ? $property[$_name] : '';
                            }
                        }
                    } else {

                        continue;
                    }
                }

                // vdebug($_db_columns);
                // vdebug($_datas);

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_lastAlpha = end($_alphas);

                if (empty($_extra_datas)) {
                    foreach ($_xls_columns as $_xkey => $_column) {

                        $_title = ucwords(strtolower($_column));

                        $_objSheet->setCellValue($_xkey . $_row, $_title);
                        $_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                }

                $_objSheet->setTitle('List of Properties');
                $_objSheet->setCellValue('A1', 'LIST OF PROPERTIES');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                $col = 1;

                foreach ($properties as $key => $property) {

                    $property_id = isset($property['id']) && $property['id'] ? $property['id'] : '';

                    if (!empty($_extra_datas)) {

                        foreach ($_xls_columns as $_xkey => $_column) {

                            $_title = ucwords(strtolower($_column));

                            $_objSheet->setCellValue($_xkey . $_start, $_title);

                            $_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        }

                        $_start++;
                    }
                    // PRIMARY INFORMATION COLUMN
                    foreach ($_alphas as $_akey => $_alpha) {
                        $_value = isset($_datas[$property_id][$_xls_columns[$_alpha]]) && $_datas[$property_id][$_xls_columns[$_alpha]] ? $_datas[$property_id][$_xls_columns[$_alpha]] : '';
                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start += 1;
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'property');
            }
        } else {

            $this->notify->error('No Record Found', 'property');
        }
    }

    public function export_csv()
    {

        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');
            $_filters = $this->input->post('filters');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Property CSV Template.csv';

            // project_id:13;status_id:;phase:;cluster:;block:;cluster:;

            $filters = explode(';', $_filters);

            // $_fillables    =    $this->M_document->fillable;
            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'property');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false && ($_fill !== 'user_id'))) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                if ($filters) {
                    foreach ($filters as $key => $filter) {
                        # code...
                        $filter = explode(':', $filter);

                        if ($filter) {

                            $key = $filter[0];
                            $value = @$filter[1];

                            if ($value) {
                                if ($key == "status_id") {
                                    $key = "status";
                                }
                                $this->db->where($key, $value);
                            }
                        }
                    }
                }

                $records = $this->M_property->as_array()->get_all(); #up($_documents);

                // vdebug($records);

                if ($records) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($records as $_dkey => $record) {

                            $_datas[$record['id']][$_title] = isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Properties');
            $_objSheet->setCellValue('A1', 'PROPERTIES');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function import()
    {

        if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

            // if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
            if (true) {

                $_tmp_name = $_FILES['csv_file']['tmp_name'];
                $_name = $_FILES['csv_file']['name'];

                set_time_limit(0);

                $_columns = [];
                $_datas = [];

                $_failed_reasons = [];
                $_inserted = 0;
                $_updated = 0;
                $_failed = 0;

                /**
                 * Read Uploaded CSV File
                 */
                try {

                    $_file_type = PHPExcel_IOFactory::identify($_tmp_name);
                    $_objReader = PHPExcel_IOFactory::createReader($_file_type);
                    $_objPHPExcel = $_objReader->load($_tmp_name);
                } catch (Exception $e) {

                    $_msg = 'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

                    $this->notify->error($_msg, 'document');
                }

                $_objWorksheet = $_objPHPExcel->getActiveSheet();
                $_highestColumn = $_objWorksheet->getHighestColumn();
                $_highestRow = $_objWorksheet->getHighestRow();
                $_sheetData = $_objWorksheet->toArray();
                if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

                    if ($_sheetData[1][0] === 'id') {

                        $_columns[] = 'id';
                    }

                    // $_fillables    =    $this->M_document->fillable;
                    $_fillables = $this->_table_fillables;
                    if (!$_fillables) {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'property');
                    }

                    foreach ($_fillables as $_fkey => $_fill) {

                        if (in_array($_fill, $_sheetData[1])) {

                            $_columns[] = $_fill;
                        } else {

                            continue;
                        }
                    }

                    foreach ($_sheetData as $_skey => $_sd) {

                        if ($_skey > 1) {

                            if (count(array_filter($_sd)) !== 0) {

                                $_datas[] = array_combine($_columns, $_sd);
                            }
                        } else {

                            continue;
                        }
                    }

                    if (isset($_datas) && $_datas) {

                        $this->db->trans_begin();

                        foreach ($_datas as $_dkey => $_data) {

                            // trim "0" prefixes
                            $_data["phase"] = ltrim($_data["phase"], "0") ? ltrim($_data["phase"], "0") : "0";

                            $_data["block"] = ltrim($_data["block"], "0") ? ltrim($_data["block"], "0") : "0";

                            $_data["lot"] = ltrim($_data["lot"], "0") ? ltrim($_data["lot"], "0") : "0";

                            $_id = isset($_data['id']) && $_data['id'] ? $_data['id'] : false;

                            if ($_id) {
                                $data = $this->M_property->get($_id);
                                if ($data) {

                                    unset($_data['id']);

                                    // Update Property Info
                                    $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                    $_data['updated_at'] = NOW;

                                    $_update = $this->M_property->update($_data, $_id);
                                    if ($_update !== false) {

                                        $_updated++;
                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'update not working.';
                                        $_failed++;

                                        break;
                                    }
                                } else {

                                    $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                    $_data['created_at'] = NOW;
                                    $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                    $_data['updated_at'] = NOW;

                                    $_insert = $this->M_property->insert($_data);
                                    if ($_insert !== false) {

                                        $_inserted++;
                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'insert property not working';
                                        $_failed++;

                                        break;
                                    }
                                }
                            } else {

                                $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                $_data['created_at'] = NOW;
                                $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                $_data['updated_at'] = NOW;

                                $_insert = $this->M_property->insert($_data);

                                if ($_insert !== false) {
                                    $_inserted++;
                                } else {

                                    $_failed_reasons[$_dkey][] = 'insert property not working';

                                    $_failed++;

                                    break;
                                }
                            }
                        }

                        $_msg = '';

                        if ($_inserted > 0) {

                            $_msg = $_inserted . ' record/s was successfuly inserted';
                        }

                        if ($_updated > 0) {

                            $_msg .= ($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
                        }

                        if ($_failed > 0) {

                            $this->db->trans_rollback();

                            $this->notify->error('Upload Failed! Please follow upload guide. ', 'property');
                        } else {

                            $this->db->trans_commit();

                            $this->notify->success($_msg . '.', 'property');
                        }
                    }
                } else {

                    $this->notify->warning('CSV was empty.', 'property');
                }
            } else {

                $this->notify->warning('Not a CSV file!', 'property');
            }
        } else {

            $this->notify->error('Something went wrong!', 'property');
        }
    }

    public function dashboard($debug = "")
    {

        $status = Dropdown::get_static('property_status');
        $this->view_data['status'] = $status;

        $Projects = $this->M_project->as_array()->get_all();

        if ($Projects) {
            # code...
            foreach ($Projects as $key => $project) {
                # code...
                $this->view_data['projects'][$project['name']]['total'] = $this->M_property->where(array('project_id' => $project['id']))->as_array()->count_rows();

                if ($status) {
                    # code...
                    foreach ($status as $key => $stat) {

                        if ($key) {
                            # code...
                            $where['project_id'] = $project['id'];
                            $where['status'] = $key;
                            $count = $this->M_property->where($where)->as_array()->count_rows();

                            $this->view_data['projects'][$project['name']][$stat] = $count;
                        }
                    }
                }
            }
        }

        if ($debug == 1) {
            # code...
            vdebug($this->view_data);
        }

        $this->template->build('dashboard', $this->view_data);
    }

    public function getAllSellers()
    {

        $data = $this->M_seller->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function getAllProspects()
    {

        $data = $this->M_prospect->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function getAllProperties()
    {

        $data = $this->M_property->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    function fetchSellerTeams()
    {
        if ($this->input->is_ajax_request()) {
            $items    =    [];
            $seller_id = $this->input->post('id');


            $seller = $this->M_seller->get($seller_id);

            $seller_team_id = $seller['seller_team_id'];

            $seller_team = $this->M_seller_team->get($seller_team_id);

            if ($seller_team) {
                $items[$seller_team_id]['id'] = $seller_team['id'];
                $items[$seller_team_id]['name'] = $seller_team['name'];
                echo json_encode($items);
                exit();
            }
        } else {

            show_404();
        }
    }

    public function get_house_model($property_id = 0)
    {
        $post = $this->input->post();

        $project_id = $post['project_id'];

        switch ($property_id) {
            case 1:
                $sub_type_id = 1;
                break;
            case 2:
                $sub_type_id = 2;
                break;
            case 3:
                $sub_type_id = 3;
                break;
            case 4:
                $sub_type_id = 4;
                break;
            default:
                $sub_type_id = 0;
                break;
        }

        $where["project_id"] = $project_id;
        $where["sub_type_id"] = $sub_type_id;

        $data = $this->M_house->where($where)->as_array()->get();

        if ($data) {
            $output = array(
                'data' => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(0);
        }
    }
    public function get_all_house_models()
    {
        $post = $this->input->post();
        $project_id = $post['project_id'];
        $property_id = $post['property_id'];

        if ($project_id != 0) {
            $where["project_id"] = $project_id;
        }

        if ($property_id != 0) {
            $where["sub_type_id"] = $property_id;
        }

        $data = $this->M_house->where(@$where)->as_array()->get_all();

        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function get_lot_types()
    {
        $post = $this->input->post();
        $house_model_id = $post['house_model_id'];

        $where["house_model_id"] = $house_model_id;

        $data = $this->M_interior->where($where)->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function process_promos($promos = array(), $transaction_id = 0)
    {
        if ($promos) {
            foreach ($promos as $key => $promo) {

                if (isset($promo['discount_id'])) {
                    $info['transaction_id'] = $transaction_id;
                    $info['discount_id'] = $promo['discount_id'];
                    $info['discount_amount_rate'] = $promo['discount_amount_rate'];
                    $info['deduct_to'] = $promo['discount_id'] == 1 ? 'Loan' : 'TCP';
                    $info['remarks'] = $promo['discount_id'] == 1 ? 'Deductable to Loan' : 'Deductable to TCP';
                    // M_transaction_discount
                    $this->M_transaction_discount->insert($info + $this->additional);
                }
            }
        }
    }

    public function dashboard_inventory_report($debug = "")
    {

        $status = Dropdown::get_static('property_status');
        $view_data['status'] = $status;

        $Projects = $this->M_project->as_array()->get_all();

        if ($Projects) {

            foreach ($Projects as $key => $project) {

                $view_data['projects'][$project['name']]['total'] = $this->M_property->where(array('project_id' => $project['id']))->as_array()->count_rows();

                if ($status) {

                    foreach ($status as $key => $stat) {

                        if ($key) {

                            $where['project_id'] = $project['id'];
                            $where['status'] = $key;
                            $count = $this->M_property->where($where)->as_array()->count_rows();

                            $view_data['projects'][$project['name']][$stat] = $count;
                        }
                    }
                }
            }
        }



        $data['html'] = $this->load->view('inventory_report', $view_data, true);
        if ($debug == 1) {
            vdebug($view_data['projects']);
        } elseif ($debug == 2) {
            vdebug(0 / 0);
        }
        echo json_encode($data);
    }
}
