<!DOCTYPE html>
<html>
<head>
	<title><?php echo $info['project']['name']; ?></title>
    <!--begin::Global Theme Styles(used by all pages) -->
<link href="<?=base_url();?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css">
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="<?=base_url();?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css">
<link href="<?=base_url();?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="<?=base_url();?>assets/media/logos/favicon.ico"><!--end::Layout Skins -->
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

        input[type="text"],
select.form-control {
  background: transparent;
  border: none;
  border-bottom: 1px solid #000000;
  -webkit-box-shadow: none;
  box-shadow: none;
  border-radius: 0;
}

input[type="text"]:focus,
select.form-control:focus {
  -webkit-box-shadow: none;
  box-shadow: none;
}

	</style>
</head>
<body>

<?php

$lot_area = isset($info['lot_area']) && $info['lot_area'] ? $info['lot_area'] : '';
$unit_feature = isset($info['model']['name']) && $info['model']['name'] ? $info['model']['name'] : '';
$regular_contract_price = isset($info['total_selling_price']) && $info['total_selling_price'] ? $info['total_selling_price'] : '';
$promo_discount = isset($info['promo_amount']) && $info['promo_amount'] ? $info['promo_amount'] : '';
$discounted_total_selling_price = isset($info['discounted_total_selling_price']) && $info['discounted_total_selling_price'] ? $info['discounted_total_selling_price'] : '';

$monthNum = date("m");
$dateObj = DateTime::createFromFormat('!m', $monthNum);
$monthName = $dateObj->format('F');

$ra_date = date("Y-m-d");
$ra_date = strtotime(date("Y-m-d", strtotime($ra_date)) . " +1 month");
$ra_date = date("Y-m-d", $ra_date);

// OPTION 1 ================================================================
$opt_1_percentage_monthly = get_percentage_amount($formData['option1Equity'], $info['total_selling_price']);

$opt_1_total_dp_diff = $info['total_selling_price'] - $opt_1_percentage_monthly;

$opt_1_total_dp = $info['total_selling_price'] - $opt_1_total_dp_diff;

$opt_1_monthly_payment = monthly_payment($opt_1_total_dp, 0, $formData['option1Terms']);

$opt_1_remaining_bal_percentage = (100 - $formData['option1Equity']);

$opt_1_remaining_balance = ($opt_1_remaining_bal_percentage / 100) * $info['total_selling_price'];

// OPTION 2 ================================================================
$opt_2_percentage_monthly = get_percentage_amount($formData['option2Equity'], $info['total_selling_price']);

$opt_2_total_dp_diff = $info['total_selling_price'] - $opt_2_percentage_monthly;

$opt_2_total_dp = $info['total_selling_price'] - $opt_2_total_dp_diff;

$opt_2_monthly_payment = monthly_payment($opt_2_total_dp, 0, $formData['option2Terms']);

$opt_2_remaining_bal_percentage = (100 - $formData['option2Equity']);

$opt_2_remaining_balance = ($opt_2_remaining_bal_percentage / 100) * $info['total_selling_price'];

// OPTION 3 ================================================================
$opt_3_percentage_monthly = get_percentage_amount($formData['option3Equity'], $info['total_selling_price']);

$opt_3_total_dp_diff = $info['total_selling_price'] - $opt_3_percentage_monthly;

$opt_3_total_dp = $info['total_selling_price'] - $opt_3_total_dp_diff;

$opt_3_monthly_payment = monthly_payment($opt_3_total_dp, 0, $formData['option3Terms']);

$opt_3_remaining_bal_percentage = (100 - $formData['option3Equity']);

$opt_3_remaining_balance = ($opt_3_remaining_bal_percentage / 100) * $info['total_selling_price'];

$sales_team = get_person($formData['salesTeam'], 'seller_teams');

$sales_team_name = get_name($sales_team) ? get_name($sales_team) : "";

// vdebug($person);

?>

<!-- CONTENT -->
<div
    class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
    <!--begin:: Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="col-sm-12">
                <p class="mb-0">(Sales Team)</p>
                <span class="font-weight-bold"
                    ><?php echo $sales_team_name ?></span
                >
                <p class="mb-0">(Contact Number)</p>
                <span class="font-weight-bold"><?php echo $formData['contactNumber'] ?></span>

                <h3 class="text-center text-uppercase">
                <?php echo $info['project']['name']; ?>
                </h3>
                <p class="text-center text-uppercase">Pueblo de Panay, Inc</p>

                <p>As of <?php echo $monthName . ' ' . date('Y') ?></p>

                <!-- Computation -->
                <div class="row border p-2">
                    <div class="col-sm-6">
                        <p>Unit Area (sqm)</p>
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo $lot_area; ?></p>
                    </div>
                    <div class="col-sm-6">
                        <p>UNIT FEATURE:</p>
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo $unit_feature ?></p>
                    </div>
                    <div class="col-sm-6">
                        <p>REGULAR CONTRACT PRICE</p>
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo money_php($regular_contract_price); ?></p>
                    </div>
                    <div class="col-sm-6">
                        <p>LESS: PROMO DISCOUNT</p>
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo money_php($promo_discount); ?></p>
                    </div>
                    <div class="col-sm-6">
                        <p>PROMO CONTRACT PRICE:</p>
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo money_php($discounted_total_selling_price); ?></p>
                    </div>
                </div>
                <!-- /Computation -->

                <!-- Bank Financing -->
                <div class="row bg-light border p-2">
                    <h5 class="mx-auto">OPTION I/BANK FINANCING</h5>
                </div>

                <div class="row border-bottom-0 border p-2">
                    <div class="col-sm-4">
                        <p>RESERVATION FEE (NON REFUNDABLE)</p>
                    </div>
                    <div class="col-sm-2">
                        <p>
                            RA DATE:
                            <span class="font-weight-bold"><?php echo view_date($ra_date) ?></span>
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo money_php($formData['option1RFFee']) ?></p>
                    </div>
                    <div class="col-sm-6">
                        <p>DOWN PAYMENT/ EQUITY <?php echo $formData['option1Equity'] . '%' ?></p>
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo money_php($opt_1_total_dp) ?></p>
                    </div>
                    <div class="col-sm-6">
                        <p>STRAIGHT MONTHLY AMORT. FOR 24 MONTHS (W/ PDC)</p>
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo money_php($opt_1_monthly_payment) ?></p>
                    </div>
                </div>

                <div class="row border-top-0 border p-2">
                    <div class="col-sm-12 px-5">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th class="font-weight-bold" scope="col">
                                        <?php echo count($payments['monthly']) ?> Months Schedule (Downpayment)
                                    </th>
                                    <th class="font-weight-bold" scope="col">
                                        Due Date
                                    </th>
                                    <th class="font-weight-bold" scope="col">
                                        Monthly Payment
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($payments['monthly']): ?>
                                <?php foreach ($payments['monthly'] as $key => $payment):
?>
                                <tr>
                                    <td><?php echo 'Month ' . $payment['month']; ?></td>
                                    <td><?php echo view_date($payment['due_date']) ?></td>
                                    <td><?php echo money_php($payment['principal'] + $payment['interest']) ?></td>
                                </tr>
<?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h5>PRINCIPAL BALANCE</h5>
                    </div>
                    <div class="col-sm-6 text-center">
                        <h5><?php echo money_php($opt_1_remaining_balance); ?></h5>
                    </div>
                </div>
                <!-- /Bank Financing -->

                <!-- Amortization -->
                <div class="row border p-2">
                    <h5 class="mx-auto">AMORTIZATION</h5>
                </div>
                <div class="row border p-2">
                    <div class="col-sm-12">
                        <p class="text-center">
                            B1. BANK FINANCING - (More or Less depending on the
                            current interest rate upon application)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            5 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            <?php echo money_php(monthly_payment($opt_1_total_dp_diff, 7.5, 60)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            10 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_1_total_dp_diff, 7.5, 120)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            15 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_1_total_dp_diff, 7.5, 180)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            20 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_1_total_dp_diff, 7.5, 240)); ?>
                        </p>
                    </div>
                </div>
                <!-- /Amortization -->

                <!-- HDMF Financing -->
                <div class="row border p-2">
                    <h5 class="mx-auto">OPTION II/ HDMF FINANCING</h5>
                </div>
                <div class="row border-bottom-0 border-top-0 border p-2">
                    <div class="col-sm-6">RESERVATION FEE (NON REFUNDABLE)</div>
                    <div class="col-sm-6"><?php echo money_php($formData['option2RFFee']) ?></div>
                    <div class="col-sm-6">DOWN PAYMENT/ EQUITY <?php echo $formData['option2Equity'] . "%" ?></div>
                    <div class="col-sm-6"><?php echo money_php($opt_2_total_dp) ?></div>
                    <div class="col-sm-6">
                        STRAIGHT MONTHLY AMORT. FOR 24 MONTHS (W/ PDC)
                    </div>
                    <div class="col-sm-6"><?php echo money_php($opt_2_monthly_payment) ?></div>
                    <div class="col-sm-6">PRINCIPAL BALANCE</div>
                    <div class="col-sm-6"><?php echo money_php($opt_2_remaining_balance); ?></div>
                </div>
                <!-- /HDMF Financing -->

                <!-- Amortization -->
                <div class="row border p-2">
                    <h5 class="mx-auto">AMORTIZATION</h5>
                </div>
                <div class="row border p-2">
                    <div class="col-sm-12">
                        <p class="text-center">
                            B2. HDMF FINANCING - (More or Less depending on the
                            current interest rate upon application)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            5 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            <?php echo money_php(monthly_payment($opt_2_total_dp_diff, 7.5, 60)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            10 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_2_total_dp_diff, 7.5, 120)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            15 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_2_total_dp_diff, 7.5, 180)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            20 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_2_total_dp_diff, 7.5, 240)); ?>
                        </p>
                    </div>
                </div>
                <!-- /Amortization -->

                <!-- House Financing -->
                <div class="row border p-2">
                    <h5 class="mx-auto">OPTION III / IN HOUSE FINANCING</h5>
                </div>
                <div class="row border-bottom-0 border-top-0 border p-2">
                    <div class="col-sm-6">RESERVATION FEE (NON REFUNDABLE)</div>
                    <div class="col-sm-6"><?php echo money_php($formData['option3RFFee']) ?></div>
                    <div class="col-sm-6">DOWN PAYMENT/ EQUITY <?php echo $formData['option3Equity'] . "%" ?></div>
                    <div class="col-sm-6"><?php echo money_php($opt_3_total_dp) ?></div>
                    <div class="col-sm-6">
                        STRAIGHT MONTHLY AMORT. FOR 24 MONTHS (W/ PDC)
                    </div>
                    <div class="col-sm-6"><?php echo money_php($opt_3_monthly_payment) ?></div>
                    <div class="col-sm-6">PRINCIPAL BALANCE</div>
                    <div class="col-sm-6"><?php echo money_php($opt_3_remaining_balance); ?></div>
                </div>
                <!-- /House Financing -->

                 <!-- Amortization -->
                 <div class="row border p-2">
                    <h5 class="mx-auto">AMORTIZATION</h5>
                </div>
                <div class="row border p-2">
                    <div class="col-sm-12">
                        <p class="text-center">
                            B3. IN HOUSE FINANCING - (More or Less depending on the
                            current interest rate upon application)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            5 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            <?php echo money_php(monthly_payment($opt_3_total_dp_diff, 7.5, 60)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            10 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_3_total_dp_diff, 7.5, 120)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            15 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_3_total_dp_diff, 7.5, 180)); ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            20 Years
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            (at 7.5% per annum (3 years repricing)
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                        <?php echo money_php(monthly_payment($opt_3_total_dp_diff, 7.5, 240)); ?>
                        </p>
                    </div>
                </div>
                <!-- /Amortization -->

                <!-- Others -->
                <div class="row p-2">
                    <div class="col-sm-12">
                        <h5>PRICE IS INCLUSIVE OF:</h5>
                    </div>
                    <div class="col-sm-6">Transfer of Title Fee</div>
                    <div class="col-sm-6"><?php echo $formData['transferOfTitleFee'] ? $formData['transferOfTitleFee'] : '' ?></div>
                    <div class="col-sm-6">Miscellaenous Fee</div>
                    <div class="col-sm-6"><?php echo $formData['miscellaenaousFee'] ? money_php($formData['miscellaenaousFee']) : '' ?></div>
                    <div class="col-sm-6">HOA Fund</div>
                    <div class="col-sm-6"><?php echo $formData['hoaFund'] ? money_php($formData['hoaFund']) : '' ?></div>
                    <div class="col-sm-6">
                        Processing Fee (Annotation Fee/Mortgage Fee)
                    </div>
                    <div class="col-sm-6">P 100,000.00</div>
                    <div class="col-sm-12">
                        <h5>PRICE IS EXCLUSIVE OF:</h5>
                    </div>
                    <div class="col-sm-6">VAT</div>
                    <div class="col-sm-6"><?php echo $formData['vat'] ? $formData['vat'] : '' ?></div>
                    <div class="col-sm-6">HOA MONTHLY DUES</div>
                    <div class="col-sm-6"><?php echo $formData['hoaMonthlyDues'] ? $formData['hoaMonthlyDues'] : '' ?></div>
                </div>

                <div class="row p-2">
                    <div class="col-sm-6">
                        1. Prices are subject to change without prior notice
                    </div>
                    <div class="col-sm-6">
                        3. Sacred Heart of Jesus Prime Holdings Inc. reserves
                        the right to correct this pricelists in case
                        typographical error;
                    </div>
                    <div class="col-sm-6">
                        2. Final transactions or payments are to be made solely
                        at Sacred Heart of Jesus Prime Holdings Inc. and/or at
                        Twin Hearts Realty Sales Office.
                    </div>
                </div>
                <!-- Others -->

                <!-- Signatures -->
                <div class="row mt-3">
                    <div class="col-sm-6">
                        <div class="d-flex flex-column align-items-center">
                            <input type="text">
                            <p>Signature Over Printed Name</p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="d-flex flex-column align-items-center">
                            <input type="text">
                            <p>Signature Over Printed Name</p>
                        </div>

                    </div>
                </div>
                <!-- /Signatures -->
            </div>
        </div>
    </div>
</div>

</body>
</html>