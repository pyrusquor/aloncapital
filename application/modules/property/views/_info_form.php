<?php
// Property Information
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$status = isset($info['status']) && $info['status'] ? $info['status'] : '';
$model_id = isset($info['model_id']) && $info['model_id'] ? $info['model_id'] : '';
$interior_id = isset($info['interior_id']) && $info['interior_id'] ? $info['interior_id'] : '';
$promo_id = isset($info['promo_id']) && $info['promo_id'] ? $info['promo_id'] : '';
$package_type_id = isset($info['package_type_id']) && $info['package_type_id'] ? $info['package_type_id'] : '';
$phase = isset($info['phase']) && $info['phase'] ? $info['phase'] : '';
$block = isset($info['block']) && $info['block'] ? $info['block'] : '';
$lot = isset($info['lot']) && $info['lot'] ? $info['lot'] : '';
$lot_area = isset($info['lot_area']) && $info['lot_area'] ? $info['lot_area'] : '';

$date_registered = isset($info['date_registered']) && $info['date_registered'] ? db_date($info['date_registered']) : '';
$doas_notarized_date = isset($info['doas_notarized_date']) && $info['doas_notarized_date'] ? db_date($info['doas_notarized_date']) : '';
$doas_amount = isset($info['doas_amount']) && $info['doas_amount'] ? $info['doas_amount'] : '';
$sales_arm_id = isset($info['sales_arm_id']) && $info['sales_arm_id'] ? $info['sales_arm_id'] : '';
$cts_number = isset($info['cts_number']) && $info['cts_number'] ? $info['cts_number'] : '';
$cts_notarized_date = isset($info['cts_notarized_date']) && $info['cts_notarized_date'] ? db_date($info['cts_notarized_date']) : '';
$cts_amount = isset($info['cts_amount']) && $info['cts_amount'] ? $info['cts_amount'] : '';
$progress = isset($info['progress']) && $info['progress'] ? $info['progress'] : '';
$hlurb_classification_id = isset($info['hlurb_classification_id']) && $info['hlurb_classification_id'] ? $info['hlurb_classification_id'] : '';

$title_number = isset($info['title_number']) && $info['title_number'] ? $info['title_number'] : '';
$tax_declaration_lot = isset($info['tax_declaration_lot']) && $info['tax_declaration_lot'] ? $info['tax_declaration_lot'] : '';
$tax_declaration_building = isset($info['tax_declaration_building']) && $info['tax_declaration_building'] ? $info['tax_declaration_building'] : '';


$model_name = isset($info['model']['name']) && $info['model']['name'] ? $info['model']['name'] : 'N/A';
$property_type = isset($info['model']['sub_type_id']) && $info['model']['sub_type_id'] ? $info['model']['sub_type_id'] : 'N/A';
$interior_name = isset($info['interior']['name']) && $info['interior']['name'] ? $info['interior']['name'] : 'N/A';

$promo_name = isset($info['promo']['name']) && $info['promo']['name'] ? $info['promo']['name'] : 'N/A';


$max_selling_price = isset($info['max_selling_price']) && $info['max_selling_price'] ? $info['max_selling_price'] : '';
$tct_max_price_annotation = isset($info['tct_max_price_annotation']) && $info['tct_max_price_annotation'] ? $info['tct_max_price_annotation'] : '';

$raw_land_cost = isset($info['raw_land_cost']) && $info['raw_land_cost'] ? $info['raw_land_cost'] : '';
$development_cost = isset($info['development_cost']) && $info['development_cost'] ? $info['development_cost'] : '';
$construction_in_progress_cost = isset($info['construction_in_progress_cost']) && $info['construction_in_progress_cost'] ? $info['construction_in_progress_cost'] : '';
$turn_over_date = isset($info['turn_over_date']) && $info['turn_over_date'] ? date('Y-m-d', strtotime($info['turn_over_date'])) : '';

// vdebug($info);

?>


<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Project Name <span class="kt-font-danger">*</span></label>
            <select class="form-control kt-select2 populateCode" id="project_id" name="info[project_id]" <?php //echo ($this->router->fetch_method() === 'form' && !empty($this->uri->segment(3)) ) ? 'disabled' : ''; 
                                                                                                            ?>>
                <?php foreach ($project as $p) : ?>
                    <option value="<?php echo $p['id'] ?>" <?php echo ($project_id == $p['id']) ? 'selected' : '' ?>><?php echo ucwords($p['name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Property Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Property Name" id="name" name="info[name]" value="<?php echo set_value('info[name]"', @$name); ?>">
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label class="">General Status <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[status]', Dropdown::get_static('property_status'), set_value('info[status]', @$status), 'class="form-control" id="status"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label class="">Property Types <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('', Dropdown::get_static('property_types'), set_value('', @$property_type), 'class="form-control" id="property_types"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3 d-none" id="house_model_parent">
        <div class="form-group">
            <label class="">House Model <span class="kt-font-danger">*</span></label>
            <select class="form-control" id="house_model_id" name="info[model_id]">
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label id="interior_id_label" class="">Lot Type <span class="kt-font-danger">*</span></label>
            <select class="form-control" id="interior_id" name="info[interior_id]">
                <option value="0">Select Lot Type / Orientation</option>
                <?php if ($interior_name) : ?>
                    <option value="<?php echo $interior_id; ?>" selected><?php echo $interior_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="">HLURB Classification Type </label>
            <?php echo form_dropdown('info[hlurb_classification_id]', Dropdown::get_static('hlurb_classification'), set_value('info[hlurb_classification_id]', @$hlurb_classification_id), 'class="form-control" id="hlurb_classification_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>Phase # </label>
            <div class="kt-input-icon">
                <input type="text" class="form-control populateCode" id="phase" placeholder="Phase Number" name="info[phase]" value="<?php echo set_value('info[phase]"', @$phase); ?>" readonly>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-pin"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3 d-none">
        <div class="form-group">
            <label>Cluster # </label>
            <div class="kt-input-icon">
                <input type="text" class="form-control populateCode" id="cluster" placeholder="Cluster Number" name="info[cluster]" value="<?php echo set_value('info[cluster]"', @$cluster); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Block # <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control populateCode" id="block" placeholder="Block Number" name="info[block]" value="<?php echo set_value('info[block]"', @$block); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label id="lot_no_id">Lot # <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control populateCode" id="lot" placeholder="Lot Number" name="info[lot]" value="<?php echo set_value('info[lot]"', @$lot); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Sales Group</label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('info[sales_arm_id]', Dropdown::get_dynamic('seller_group'), set_value('info[sales_arm_id]', @$sales_arm_id), 'class="form-control kt-select2"'); ?>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Construction Progress</label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <input type="text" class="form-control" id="progress" placeholder="Progress" name="info[progress]" value="<?php echo set_value('info[progress]"', @$progress); ?>">
                    <div class="input-group-append"><span class="input-group-text">%</span></div>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


</div>


<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>CTS # </label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="CTS #" name="info[cts_number]" value="<?php echo set_value('info[cts_number]"', @$cts_number); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>CTS Notarized Date</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control kt_datepicker" placeholder="CTS Notarized Date" name="info[cts_notarized_date]" value="<?php echo set_value('info[cts_notarized_date]"', @$tct_number); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>CTS Amount</label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control" placeholder="CTS Amount" name="info[cts_amount]" value="<?php echo set_value('info[cts_amount]"', @$cts_amount); ?>">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Date Registered</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control kt_datepicker" placeholder="Date Registered" name="info[date_registered]" value="<?php echo set_value('info[date_registered]"', @$date_registered); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Deed of Absolute Sale Notarized Date</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control kt_datepicker" placeholder="DOAS Date" name="info[doas_notarized_date]" value="<?php echo set_value('info[doas_notarized_date]"', @$doas_notarized_date); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Deed of Sale Amount</label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control" placeholder="DOAS Amount" name="info[doas_amount]" value="<?php echo set_value('info[doas_amount]"', @$doas_amount); ?>">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Title Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control" placeholder="Title Number" name="info[title_number]" value="<?php echo set_value('info[title_number]"', @$title_number); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Tax Declaration Lot</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control" placeholder="Tax Declaration Lot" name="info[tax_declaration_lot]" value="<?php echo set_value('info[tax_declaration_lot]"', @$tax_declaration_lot); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Tax Declaration Building</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control" placeholder="Tax Declaration Building" name="info[tax_declaration_building]" value="<?php echo set_value('info[tax_declaration_building]"', @$tax_declaration_building); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Max Selling Price</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control" placeholder="Max Selling Price" name="info[max_selling_price]" value="<?php echo set_value('info[max_selling_price]"', @$max_selling_price); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>TCT Max Price Annotation</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control" placeholder="TCT Max Price Annotation" name="info[tct_max_price_annotation]" value="<?php echo set_value('info[tct_max_price_annotation]"', @$tct_max_price_annotation); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Raw Land Cost</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control" placeholder="Raw Land Cost" name="info[raw_land_cost]" value="<?php echo set_value('info[raw_land_cost]"', @$raw_land_cost); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Development Cost</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control" placeholder="Development Cost" name="info[development_cost]" value="<?php echo set_value('info[development_cost]"', @$development_cost); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Construction In Progress Cost</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control form-control" placeholder="Construction In Progress Cost" name="info[construction_in_progress_cost]" value="<?php echo set_value('info[construction_in_progress_cost]"', @$construction_in_progress_cost); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Turn Over Date <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" placeholder="Turn Over Date" name="info[turn_over_date]" value="<?php echo set_value('turn_over_date"', @$turn_over_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>