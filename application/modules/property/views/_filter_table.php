<?php if (!empty($records)) : ?>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row" id="propertyList">
                        <div class="col-md-12">
                            <table class="table table-striped table-sm">
                                    <thead class="thead-dark ">
                                        <tr>
                                            <td>ID</td>
                                            <td>Project</td>
                                            <td>Property</td>
                                            <td>Phase</td>
                                            <td>Block</td>
                                            <td>Lot</td>
                                            <td>Property Type</td>
                                            <td>Property Type Interior</td>
                                            <td>Selling Price</td>
                                            <td>Status</td>
                                            <td>Construction Progress</td>
                                            <td>Actions</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($records as $r) :
                                            $id = isset($r['id']) && $r['id'] ? $r['id'] : '';
                                            $name = isset($r['name']) && $r['name'] ? $r['name'] : '';
                                            $phase = isset($r['phase']) && $r['phase'] ? $r['phase'] : '';
                                            $cluster = isset($r['cluster']) && $r['cluster'] ? $r['cluster'] : '';
                                            $block = isset($r['block']) && $r['block'] ? $r['block'] : '';
                                            $lot = isset($r['lot']) && $r['lot'] ? $r['lot'] : '';
                                            $total_selling_price = isset($r['total_selling_price']) && $r['total_selling_price'] ? $r['total_selling_price'] : '';
                                            $progress = isset($r['progress']) && $r['progress'] ? $r['progress'] : 0;
                                            $status = isset($r['status']) && $r['status'] ? $r['status'] : '';
                                            $project_name = isset($r['project']['name']) && $r['project']['name'] ? $r['project']['name'] : '';
                                            $model_name = isset($r['model']['name']) && $r['model']['name'] ? $r['model']['name'] : '';
                                            $interior_name = isset($r['interior']['name']) && $r['interior']['name'] ? $r['interior']['name'] : '';
                                        ?>
                                        <tr>
                                            <td><?=$id; ?></td>
                                            <td><?=$project_name; ?></td>
                                            <td><?=$name; ?></td>
                                            <td><?=$phase; ?></td>
                                            <td><?=$block; ?></td>
                                            <td><?=$lot; ?></td>
                                            <td><?=$model_name; ?></td>
                                            <td><?=$interior_name; ?></td>
                                            <td><?=money_php($total_selling_price); ?></td>
                                            <td><?=$status; ?></td>
                                            <td><?=$progress; ?>%</td>
                                            <td><a href="<?php echo base_url('property/view/' . $r['id']); ?>">view</a></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12">

                            <!--begin:: Components/Pagination/Default-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">

                                    <!--begin: Pagination-->
                                    <div class="kt-pagination kt-pagination--brand">
                                        <?php echo $this->ajax_pagination->create_links(); ?>

                                        <div class="kt-pagination__toolbar">
                                            <span class="pagination__desc">
                                                <?php echo $this->ajax_pagination->show_count(); ?>
                                            </span>
                                        </div>
                                    </div>

                                    <!--end: Pagination-->
                                </div>
                            </div>

                            <!--end:: Components/Pagination/Default-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="<?php echo base_url('property/form'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>