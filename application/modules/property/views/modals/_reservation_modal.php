<div class="modal fade" id="reservationModal" tabindex="-1" role="dialog" aria-labelledby="reservationModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Sellers' Reservation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="reservationForm" method="POST" action="<?=base_url();?>property/reservation" method="post">
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <label class="">Prospect</label>
                            <select class="form-control" id="prospect_select" name="prospect_id">
                                <option value="0">Select Prospect</option>
                            </select>
                        </div>
                        <div class="col-sm-12 mb-3">
                            <label class="form-control-label">Property</label>
                            <select class="form-control" id="property_select">
                                <option value="0">Select Property</option>
                            </select>
                        </div>
                        <div class="col-sm-12 mb-3 d-none">
                            <input type="text" class="form-control form-control" id="selected_property" name="property_id">
                        </div>
                        <!-- <div class="col-sm-12 mb-3">
                            <label>Expected Reservation Date</label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <input type="text" class="form-control form-control kt_datepicker" placeholder="Expected Reservation Date" name="expected_reservation_date">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span>
                            </div>
                        </div> -->
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="reservationForm">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>