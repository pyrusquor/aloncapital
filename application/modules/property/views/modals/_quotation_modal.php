<?php

// vdebug($records);

?>

<div class="modal fade" id="quotationModal" tabindex="-1" role="dialog" aria-labelledby="quotationModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Quotation Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="quotationForm" method="POST" action="<?= base_url(); ?>property/quotation" method="post">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Seller Name:</label>
                            <select class="quotation_modal suggests" style="width: 100%" data-module="sellers" id="sellerNames" name="sellers" data-type="person">
                                <option value="">Select Seller</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Sales Team:</label>
                            <input class="form-control" style="width: 100%" id="salesTeam" name="salesTeam" readonly/>
                            
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Contact Number:</label>
                            <input type="number" name="contactNumber" class="form-control" id="contactNumber">
                        </div>
                    </div>
                    <h6>OPTION I / BANK FINANCING</h6>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Reservation Fee:</label>
                            <input type="text" value="15000" name="option1RFFee" class="form-control" id="option1RFFee">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">DP/Equity Percentage:</label>
                            <input type="number" value="10" name="option1Equity" class="form-control"
                                   id="option1Equity">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">DP Terms:</label>
                            <input type="number" value="24" name="option1Terms" class="form-control" id="option1Terms">
                        </div>
                    </div>
                    <h6>OPTION II / HDMF FINANCING</h6>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Reservation Fee:</label>
                            <input type="text" value="25000" name="option2RFFee" class="form-control" id="option2RFFee">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">DP/Equity Percentage:</label>
                            <input type="number" value="25" name="option2Equity" class="form-control"
                                   id="option2Equity">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">DP Terms:</label>
                            <input type="number" value="24" name="option2Terms" class="form-control" id="option2Terms">
                        </div>
                    </div>
                    <h6>OPTION III / HOUSE FINANCING</h6>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Reservation Fee:</label>
                            <input type="text" value="25000" name="option3RFFee" class="form-control" id="option3RFFee">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">DP/Equity Percentage:</label>
                            <input type="number" value="25" name="option3Equity" class="form-control"
                                   id="option3Equity">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">DP Terms:</label>
                            <input type="number" value="36" name="option3Terms" class="form-control" id="option3Terms">
                        </div>
                    </div>
                    <h6>PRICE IS INCLUSIVE OF: </h6>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Transfer of Title Fee:</label>
                            <input type="number" value="1500" name="transferOfTitleFee" class="form-control"
                                   id="transferOfTitleFee">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Miscellaenous Fee:</label>
                            <input type="number" value="10" name="miscellaenaousFee" class="form-control"
                                   id="miscellaenaousFee">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">HOA Fund:</label>
                            <input type="number" value="1500" name="hoaFund" class="form-control" id="hoaFund">
                        </div>
                    </div>
                    <h6>PRICE IS EXCLUSIVE OF: </h6>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">VAT:</label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <input type="text" name="vat" class="form-control" id="vat">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-user"></i></span>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">HOA Monthly Dues:</label>
                            <input type="number" name="hoaMonthlyDues" class="form-control"
                                   id="hoaMonthlyDues">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="quotationForm">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>