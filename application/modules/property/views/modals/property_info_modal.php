<?php
// Property Information
$id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
$project_id = isset($info['project_id']) && $info['id'] ? $info['project_id'] : 'N/A';
$name = isset($info['name']) && $info['name'] ? $info['name'] : 'N/A';
$status = isset($info['status']) && $info['status'] ? $info['status'] : 'N/A';
$model_id = isset($info['model_id']) && $info['model_id'] ? $info['model_id'] : 'N/A';
$interior_id = isset($info['interior_id']) && $info['interior_id'] ? $info['interior_id'] : 'N/A';
$phase = isset($info['phase']) && $info['phase'] ? $info['phase'] : 'N/A';
$cluster = isset($info['cluster']) && $info['cluster'] ? $info['cluster'] : 'N/A';
$block = isset($info['block']) && $info['block'] ? $info['block'] : 'N/A';
$lot = isset($info['lot']) && $info['lot'] ? $info['lot'] : 'N/A';
$progress = isset($info['progress']) && $info['progress'] ? $info['progress'] : 'N/A';

// Pricing Information
$total_selling_price = isset($info['total_selling_price']) && $info['total_selling_price'] ? $info['total_selling_price'] : 'N/A';

$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : 'N/A';
$model_name = isset($info['model']['name']) && $info['model']['name'] ? $info['model']['name'] : 'N/A';
$interior_name = isset($info['interior']['name']) && $info['interior']['name'] ? $info['interior']['name'] : 'N/A';

?>
<div class="modal fade" id="propertyModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Property: <?= $name ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <!-- <form method="POST" id="advanceSearch"> -->
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab" aria-selected="true">
                            <i class="flaticon2-information"></i> General Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#saleslog" role="tab" aria-selected="false">
                            <i class="flaticon-line-graph"></i> Sales Logs
                        </a>
                    </li>
                </ul>
                <div class="tab-content  kt-margin-t-20">
                    <div class="tab-pane active" id="basic_information" role="tabpanel-1">

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="form-label kt-font-transform-u">General Status:</label>
                                <span class="form-control-plaintext kt-font-bolder"><?= Dropdown::get_static('property_status', @$status, 'view'); ?></span>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label kt-font-transform-u">Project Name:</label>
                                <span class="form-control-plaintext kt-font-bolder"><?= $project_name; ?></span>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label kt-font-transform-u">Property Name:</label>
                                <span class="form-control-plaintext kt-font-bolder"><?= $name; ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="form-label kt-font-transform-u">Property Model Name:</label>
                                <span class="form-control-plaintext kt-font-bolder"><?= $model_name; ?></span>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label kt-font-transform-u">House Interior Name:</label>
                                <span class="form-control-plaintext kt-font-bolder"><?= $interior_name; ?></span>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label kt-font-transform-u">Lot:</label>
                                <span class="form-control-plaintext kt-font-bolder"><?= 'Cluster# ' . $cluster . ' Block# ' . $block . ' Lot# ' . $lot; ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label kt-font-transform-u">Total Selling Price:</label>
                                <span class="form-control-plaintext kt-font-bolder">
                                    <h3><?= money_php($total_selling_price); ?></h3>
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label kt-font-transform-u">Construction Progress:</label>
                                <span class="form-control-plaintext kt-font-bolder">
                                    <h3><?= $progress . '%'; ?></h3>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="saleslog" role="tabpanel-1">
                        <div class="table-responsive">
                            <?php if ($sales_logs) : ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <td style="width:20%">Property</td>
                                            <td style="width:25%">Reference Number</td>
                                            <td style="width:20%">Reservation Date</td>
                                            <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                            <td class="kt-align-center" style="width:22%">Buyer</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($sales_logs as $key => $sales_log) : ?>
                                            <?php
                                            $reference = $sales_log['reference'];
                                            $property_name = $sales_log['property']['name'];
                                            $reservation_date = $sales_log['reservation_date'];
                                            $this->transaction_library->initiate($sales_log['id']);
                                            $tpm_p = $this->transaction_library->get_amount_paid(1, 0, 1);
                                            $payment_percentage = $tpm_p . "%";
                                            $buyer_name = @$sales_log['buyer']["first_name"] . " " . @$sales_log['buyer']["middle_name"] . " " . @$sales_log['buyer']["last_name"];
                                            ?>
                                            <tr>
                                                <td><?= $property_name ?></td>
                                                <td><?= $reference ?></td>
                                                <td><?= view_date($reservation_date) ?></td>
                                                <td class="kt-align-center"><?= $payment_percentage ?></td>
                                                <td class="kt-align-center"><?= $buyer_name ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php else : ?>
                                <h4 class="font-weight-italic text-center">
                                    No records found
                                </h4>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php if($status=='1'): ?>
                    <a class="btn btn-primary reserve_property" href='<?= base_url() . 'sellers_reservations/form?project_id='.$project_id. '&property_id='.$id ?>' role="button">RESERVE NOW</a>
                <?php endif?>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>