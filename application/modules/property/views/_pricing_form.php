<?php
    // Pricing Information
    $lot_area = isset($info['lot_area']) && $info['lot_area'] ? $info['lot_area'] : 0;
    $floor_area = isset($info['floor_area']) && $info['floor_area'] ? $info['floor_area'] : 0;
    $lot_price_per_sqm = isset($info['lot_price_per_sqm']) && $info['lot_price_per_sqm'] ? $info['lot_price_per_sqm'] : 0;
    $model_price = isset($info['model_price']) && $info['model_price'] ? $info['model_price'] : 0;
    $total_lot_price = isset($info['total_lot_price']) && $info['total_lot_price'] ? $info['total_lot_price'] : 0;
    $total_selling_price = isset($info['total_selling_price']) && $info['total_selling_price'] ? $info['total_selling_price'] : 0;
    $total_selling_price = isset($info['total_selling_price']) && $info['total_selling_price'] ? $info['total_selling_price'] : 0;
    $adjusted_price = isset($info['adjusted_price']) && $info['adjusted_price'] ? $info['adjusted_price'] : 0;

    $promo_id = isset($info['promo_id']) && $info['promo_id'] ? $info['promo_id'] : '';
    $promo_name = isset($info['promo']['name']) && $info['promo']['name'] ? $info['promo']['name'] : 'N/A';

    $package_type_id = isset($info['package_type_id']) && $info['package_type_id'] ? $info['package_type_id'] : '';

?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Lot Area<span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <input type="number" class="form-control compute popField" id="lot_area" placeholder="Lot Area" name="info[lot_area]" value="<?php echo set_value('info[lot_area]"', @$lot_area); ?>">
                    <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-4">
        <div class="form-group">
            <label>Lot Price / SQM<span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="number" class="form-control compute popField" id="lot_price_per_sqm" placeholder="Lot Price / SQM" name="info[lot_price_per_sqm]" value="<?php echo set_value('info[lot_price_per_sqm]"', @$lot_price_per_sqm); ?>">
                </div>
            </div>
            <span class="form-text text-muted">price in this field is fetch from project base lot latest price</span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Total Lot Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="number" class="form-control compute" id="total_lot_price" placeholder="Total Lot Price" name="info[total_lot_price]" value="<?php echo set_value('info[total_lot_price]"', @$total_lot_price); ?>">
                </div>
            </div>
            <span class="form-text text-muted">auto-generated - Formula = Lot Price / SQM * Lot Area</span>
        </div>
    </div>

</div>

<div class="row">
    
    <div class="col-sm-4">
        <div class="form-group">
            <label>Floor Area</label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <input type="text" class="form-control compute popField" id="floor_area" placeholder="Floor Area" name="info[floor_area]" value="<?php echo set_value('info[floor_area]"', @$floor_area); ?>">
                    <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    
    <div class="col-sm-4">
        <div class="form-group">
            <label>Package Type</label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('info[package_type_id]', Dropdown::get_static('package_types'), set_value('info[package_type_id]', @$package_type_id), 'class="form-control" id="package_type_id"'); ?>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Property Model Price </label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control compute popField" placeholder="Property Model Price"  id="model_price" name="info[model_price]" value="<?php echo set_value('info[model_price]"', @$model_price); ?>" readonly>

                    <input type="hidden" class="compute popField" id="is_multiply_floor_area">
                    <input type="hidden" class="compute popField" id="is_multiply_lot_area">

                </div>
            </div>
            <span class="form-text text-muted">auto-generated - price in this field is fetch from house interior pagkage type latest price</span>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-sm-4">
        <div class="form-group">
            <label>Adjusted Price </label>
                <div class="kt-input-icon">
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                        <input type="number" class="form-control compute" id="adjusted_price" placeholder="Adjusted Price" name="info[adjusted_price]" value="<?php echo set_value('info[adjusted_price]"', @$adjusted_price); ?>">
                    </div>
                </div>
            <span class="form-text text-muted">Adjusted Price = New Property Price - Old Property Price</span>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-sm-4">
        <div class="form-group">
            <label>Total Selling Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control" id="total_selling_price" placeholder="Total Selling Price" name="info[total_selling_price]" value="<?php echo set_value('info[total_selling_price]"', @$total_selling_price); ?>" readonly>
                </div>
            </div>
            <span class="form-text text-muted">auto-generated - Formula = Total Lot Price + Property Model Price</span>
        </div>
    </div>  

    <div class="col-sm-4">
        <div class="form-group">
            <label>Total Contract Price </label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control" id="discounted_total_selling_price" placeholder="Total Contract Price" name="info[discounted_total_selling_price]" value="<?php echo set_value('info[discounted_total_selling_price]"', $discounted_total_selling_price ?  $discounted_total_selling_price : 0); ?>" readonly>
                </div>
            </div>
            <span class="form-text text-muted">auto-generated - Formula = Total Selling Price - Promo</span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Promos & Discounts </label>
            <div class="kt-input-icon ">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="number" class="form-control promoID" id="promo_amount" placeholder="Promo Amount" name="info[promo_amount]" value="<?php echo set_value('info[promo_amount]"', $promo_amount ? $promo_amount : 0); ?>">
                </div>
            </div>
        </div>
    </div>

</div>