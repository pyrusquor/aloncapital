<div class="row">
    <?php foreach ($status_tiles as $status) : ?>
        <div class="col-sm col-12">
            <div class="kt-portlet kt-portlet--fluid <?= @$status['bg-color'] ?>">
                <div class="kt-portlet__head <?php echo $status['bg-color'] == 'kt-portlet--solid-gray' ? '' : 'text-white'; ?>">
                    <div class="kt-portlet__head-label">
                        <div class="kt-portlet__head-titles d-flex" style="align-items: center;">
                            <span class="kt-portlet__head-icon">
                                <i class=" <?php echo $status['bg-color'] == 'kt-portlet--solid-gray' ? '' : 'text-white'; ?> <?= @$status['icon'] ?>" style="font-size: 20px;"></i>
                            </span>
                            <h4 class="mb-0">
                                <?= strtoupper($status['status']) ?>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body <?php echo $status['bg-color'] == 'kt-portlet--solid-gray' ? '' : 'text-white'; ?>">
                    <span class="d-block font-weight-bold mb-7 text-dark-50">
                        <h1><?= money_php($status['total']) ?></h1>
                    </span>
                    <span class="kt-widget20__desc">
                        <h5 class="mb-0"><?= number_format($status['count']) ?> Propert<?= $status['count'] > 1 ? 'ies' : 'y' ?></h5>
                    </span>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>