<?php if ($projects):
    foreach ($projects as $key => $project):
        if ($project['total']==0):
            $avail_percent = 0;
            $reserved_percent = 0;
            $sold_percent = 0;
            $hold_percent = 0;
            $nfs_percent = 0;
        else:
            $avail_percent = ($project['Available']/$project['total'])*100;
            $reserved_percent = ($project['Reserved']/$project['total'])*100;
            $sold_percent = ($project['Sold']/$project['total'])*100;
            $hold_percent = ($project['Hold']/$project['total'])*100;
            $nfs_percent = ($project['Not For Sale']/$project['total'])*100;
        endif ?>
        <tr>
            <th scope="row" style="vertical-align: middle;">
                <?php echo $key; ?>
            </th>
            <td style="vertical-align: middle;">
            <?php echo $project['total']; ?>
            </td>
            <td>
            <?php echo $project['Available']; ?>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-brand" role="progressbar" style="width: <?php echo $avail_percent; ?>%" aria-valuenow="<?php echo $avail_percent; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </td>
            <td>
            <?php echo $project['Reserved']; ?>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-primary" role="progressbar" style="width: <?php echo $reserved_percent; ?>%" aria-valuenow="<?php echo $reserved_percent; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </td>
            <td>
            <?php echo $project['Sold']; ?>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $sold_percent; ?>%" aria-valuenow="<?php echo $sold_percent; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </td>
            <td>
            <?php echo $project['Hold']; ?>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: <?php echo $hold_percent; ?>%" aria-valuenow="<?php echo $hold_percent; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </td>
            <td>
            <?php echo $project['Not For Sale']; ?>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo $nfs_percent; ?>%" aria-valuenow="<?php echo $nfs_percent; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </td>
        </tr>
        <?php endforeach ?>
<?php endif ?>