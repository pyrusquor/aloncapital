<?php if (!empty($records)): ?>
<div class="row" id="propertyList">
    <?php foreach ($records as $r):
    $name = isset($r['name']) && $r['name'] ? $r['name'] : '';
    $phase = isset($r['phase']) && $r['phase'] ? $r['phase'] : '';
    $cluster = isset($r['cluster']) && $r['cluster'] ? $r['cluster'] : '';
    $block = isset($r['block']) && $r['block'] ? $r['block'] : '';
    $lot = isset($r['lot']) && $r['lot'] ? $r['lot'] : '';
    $total_selling_price = isset($r['total_selling_price']) && $r['total_selling_price'] ? $r['total_selling_price'] : '';
    $progress = isset($r['progress']) && $r['progress'] ? $r['progress'] : 0;
    $status = isset($r['status']) && $r['status'] ? $r['status'] : '';
    $project_name = isset($r['project']['name']) && $r['project']['name'] ? $r['project']['name'] : '';
    $model_name = isset($r['model']['name']) && $r['model']['name'] ? $r['model']['name'] : '';
    $interior_name = isset($r['interior']['name']) && $r['interior']['name'] ? $r['interior']['name'] : '';
    $lot_price_per_sqm = isset($r['lot_price_per_sqm']) && $r['lot_price_per_sqm'] ? $r['lot_price_per_sqm'] : '';
    ?>
    <div class="col-md-4 col-xl-3">
        <!--Begin::Portlet-->
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head kt-portlet__head--noborder">
                <div class="kt-portlet__head-label">
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                        <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>"
                            class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                        <span></span>
                    </label>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <a href="#" class="btn btn-icon" data-toggle="dropdown">
                        <i class="flaticon-more-1 kt-font-brand"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item">
                                <a href="<?php echo base_url('property/view/' . $r['id']); ?>" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                    <span class="kt-nav__link-text">View</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="<?php echo base_url('property/form/' . $r['id']); ?>" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-edit"></i>
                                    <span class="kt-nav__link-text">Update</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="javascript: void(0)" class="kt-nav__link remove_property"
                                    data-id="<?php echo $r['id']; ?>">
                                    <i class="kt-nav__link-icon flaticon2-trash"></i>
                                    <span class="kt-nav__link-text">Delete</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="javascript:void(0)" data-target="#quotationModal" class="kt-nav__link process_change_quotation"
                                    data-id="<?php echo $r['id']; ?>">
                                    <i class="kt-nav__link-icon flaticon2-quotation-mark"></i>
                                    <span class="kt-nav__link-text">Create Quotation</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">

                                <?php if($status == 2 || $status == 3 || $status == 4 || $status == 5): ?>

                                <a href="javascript:void(0)" id="notAvailable" class="kt-nav__link" data-status="<?php echo Dropdown::get_static('property_status', $status, 'view'); ?>"
                                    >
                                    <i class="kt-nav__link-icon flaticon-cart"></i>
                                    <span class="kt-nav__link-text">Reserve</span>
                                </a>

                                <?php else: ?>
                                    <a href="javascript:void(0)" class="kt-nav__link process_reservation"  data-id="<?php echo $r['id']; ?>">
                                        <i class="kt-nav__link-icon flaticon-cart"></i>
                                    <span class="kt-nav__link-text">Reserve</span>
                                    </a>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body cards__custom">

                <!--begin::Widget -->
                <div class="kt-widget kt-widget--user-profile-2">
                    <div class="kt-widget__head card__head">
                        <div class="kt-widget__media">
                            <?php if (get_image('property', 'images', @$image)): ?>
                            <img class="kt-widget__img"
                                src="<?php echo base_url(get_image('property', 'images', $image)); ?>" width="90px"
                                height="90px" />
                            <?php else: ?>
                            <div
                                class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                <?php echo get_initials($project_name); ?>
                            </div>
                            <?php endif;?>
                        </div>
                        <!-- <div class="kt-widget__info">
                            <a href="<?php echo base_url('property/view/' . $r['id']); ?>"
                                class="kt-widget__titel kt-hidden-">
                                <?php echo $r['id'] . '. ' . ucwords($name); ?>
                            </a>
                            <span class="kt-widget__desc">
                                <?php echo ucwords($project_name); ?>
                            </span>
                        </div> -->
                    </div>

                    <div class="card__info">
                            <a href="<?php echo base_url('property/view/' . $r['id']); ?>"
                                class="kt-widget__titel kt-hidden-">
                                <?php echo $r['id'] . '. ' . ucwords($name); ?>
                            </a><br>
                            <span class="kt-widget__desc">
                                <?php echo ucwords($project_name); ?>
                            </span>
                    </div>

                    <div class="kt-widget__body">
                        <div class="kt-widget__item">

                            <?php if ($phase): ?>
                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">Phase:</span>
                                <span class="kt-widget__data"
                                    style="font-weight: 600"><?php echo lpad($phase); ?></span>
                            </div>
                            <?php endif?>

                            <?php if ($cluster): ?>
                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">Cluster:</span>
                                <span class="kt-widget__data"
                                    style="font-weight: 600"><?php echo lpad($cluster); ?></span>
                            </div>
                            <?php endif?>

                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">Block:</span>
                                <span class="kt-widget__data"
                                    style="font-weight: 600"><?php echo lpad($block); ?></span>
                            </div>
                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">Lot:</span>
                                <span class="kt-widget__data" style="font-weight: 600"><?php echo lpad($lot); ?></span>
                            </div>
                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">Lot Area / Price per SQM:</span>
                                <span class="kt-widget__data" style="font-weight: 600"><?php echo $r['lot_area']; ?> sqm
                                    / <?php echo money_php($lot_price_per_sqm); ?></span>
                            </div>
                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">Selling Price:</span>
                                <span class="kt-widget__data"
                                    style="font-weight: 600"><?php echo money_php($total_selling_price); ?></span>
                            </div>
                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">General Status:</span>
                                <span class="kt-widget__data"
                                    style="font-weight: 600"><?php echo Dropdown::get_static('property_status', $status, 'view'); ?></span>
                            </div>
                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">Property Model:</span>
                                <span class="kt-widget__data" style="font-weight: 600"><?php echo $model_name; ?></span>
                            </div>
                            <div class="kt-widget__contact">
                                <span class="kt-widget__label" style="font-weight: 400">Property Model Interior:</span>
                                <span class="kt-widget__data"
                                    style="font-weight: 600"><?php echo $interior_name; ?></span>
                            </div>
                            <div class="kt-widget__contact">
                                <div class="kt-widget__item flex-fill" style="margin-top: 0px">
                                    <span class="kt-widget__subtitel">Progress</span>
                                    <div class="kt-widget__progress d-flex  align-items-center">
                                        <div class="progress" style="width: 100%;">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated  bg-success"
                                                role="progressbar" aria-valuenow="<?php echo $progress; ?>"
                                                aria-valuemin="0" aria-valuemax="100"
                                                style="width: <?php echo $progress; ?>%"></div>
                                        </div>
                                        <span class="kt-widget__stat">
                                            <?php echo $progress; ?>%
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="kt-widget__footer">
                        <a href="<?php echo base_url('property/view/' . $r['id']); ?>"
                            class="btn btn-label-brand btn-lg btn-upper">View Information</a>
                    </div>
                </div>
                <!--end::Widget -->
            </div>
        </div>
        <!--End::Portlet-->
    </div>
    <?php endforeach;?>
</div>
<div class="row">
    <div class="col-xl-12">

        <!--begin:: Components/Pagination/Default-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">

                <!--begin: Pagination-->
                <div class="kt-pagination kt-pagination--brand">
                    <?php echo $this->ajax_pagination->create_links(); ?>

                    <div class="kt-pagination__toolbar">
                        <span class="pagination__desc">
                            <?php echo $this->ajax_pagination->show_count(); ?>
                        </span>
                    </div>
                </div>

                <!--end: Pagination-->
            </div>
        </div>

        <!--end:: Components/Pagination/Default-->
    </div>
</div>
<?php else: ?>
<div class="row">
    <div class="col-lg-12">
        <div class="kt-portlet kt-callout">
            <div class="kt-portlet__body">
                <div class="kt-callout__body">
                    <div class="kt-callout__content">
                        <h3 class="kt-callout__title">No Records Found</h3>
                        <p class="kt-callout__desc">
                            Sorry no record were found.
                        </p>
                    </div>
                    <div class="kt-callout__action">
                        <a href="<?php echo base_url('property/form'); ?>"
                            class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;?>