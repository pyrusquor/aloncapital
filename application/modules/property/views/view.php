<?php
// Property Information
$id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
$project_id = isset($info['project_id']) && $info['id'] ? $info['project_id'] : 'N/A';
$name = isset($info['name']) && $info['name'] ? $info['name'] : 'N/A';
$status = isset($info['status']) && $info['status'] ? $info['status'] : 'N/A';
$hlurb_classification_id = isset($info['hlurb_classification_id']) && $info['hlurb_classification_id'] ? $info['hlurb_classification_id'] : 'N/A';
$model_id = isset($info['model_id']) && $info['model_id'] ? $info['model_id'] : 'N/A';
$interior_id = isset($info['interior_id']) && $info['interior_id'] ? $info['interior_id'] : 'N/A';
$phase = isset($info['phase']) && $info['phase'] ? $info['phase'] : 'N/A';
$cluster = isset($info['cluster']) && $info['cluster'] ? $info['cluster'] : 'N/A';
$block = isset($info['block']) && $info['block'] ? $info['block'] : 'N/A';
$lot = isset($info['lot']) && $info['lot'] ? $info['lot'] : 'N/A';

$date_registered = isset($info['date_registered']) && $info['date_registered'] ? view_date($info['date_registered']) : 'N/A';
$doas_notarized_date = isset($info['doas_notarized_date']) && $info['doas_notarized_date'] ? view_date($info['doas_notarized_date']) : 'N/A';
$doas_amount = isset($info['doas_amount']) && $info['doas_amount'] ? $info['doas_amount'] : 'N/A';
$sales_arm_id = isset($info['sales_arm_id']) && $info['sales_arm_id'] ? $info['sales_arm_id'] : 'N/A';
$tct_number = isset($info['tct_number']) && $info['tct_number'] ? $info['tct_number'] : 'N/A';
$cts_number = isset($info['cts_number']) && $info['cts_number'] ? $info['cts_number'] : 'N/A';
$cts_notarized_date = isset($info['cts_notarized_date']) && $info['cts_notarized_date'] ? view_date($info['cts_notarized_date']) : 'N/A';
$cts_amount = isset($info['cts_amount']) && $info['cts_amount'] ? $info['cts_amount'] : 'N/A';
$progress = isset($info['progress']) && $info['progress'] ? $info['progress'] : 'N/A';

$title_number = isset($info['title_number']) && $info['title_number'] ? $info['title_number'] : 'N/A';
$tax_declaration_lot = isset($info['tax_declaration_lot']) && $info['tax_declaration_lot'] ? $info['tax_declaration_lot'] : 'N/A';
$tax_declaration_building = isset($info['tax_declaration_building']) && $info['tax_declaration_building'] ? $info['tax_declaration_building'] : 'N/A';

// Pricing Information
$lot_area = isset($info['lot_area']) && $info['lot_area'] ? $info['lot_area'] : 'N/A';
$floor_area = isset($info['floor_area']) && $info['floor_area'] ? $info['floor_area'] : 'N/A';
$lot_price_per_sqm = isset($info['lot_price_per_sqm']) && $info['lot_price_per_sqm'] ? $info['lot_price_per_sqm'] : 'N/A';
$model_price = isset($info['model_price']) && $info['model_price'] ? $info['model_price'] : 'N/A';
$total_lot_price = isset($info['total_lot_price']) && $info['total_lot_price'] ? $info['total_lot_price'] : 'N/A';
$total_selling_price = isset($info['total_selling_price']) && $info['total_selling_price'] ? $info['total_selling_price'] : 'N/A';

$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : 'N/A';
$model_name = isset($info['model']['name']) && $info['model']['name'] ? $info['model']['name'] : 'N/A';
$interior_name = isset($info['interior']['name']) && $info['interior']['name'] ? $info['interior']['name'] : 'N/A';

$discounted_total_selling_price = isset($info['discounted_total_selling_price']) && $info['discounted_total_selling_price'] ? $info['discounted_total_selling_price'] : '';
$promo_id = isset($info['promo_id']) && $info['promo_id'] ? $info['promo_id'] : '';
$promo_amount = isset($info['promo_amount']) && $info['promo_amount'] ? $info['promo_amount'] : '';
$promo_name = isset($info['promo']['name']) && $info['promo']['name'] ? $info['promo']['name'] : 'N/A';

$package_type_id = isset($info['package_type_id']) && $info['package_type_id'] ? $info['package_type_id'] : '1';

$max_selling_price = isset($info['max_selling_price']) && $info['max_selling_price'] ? $info['max_selling_price'] : 'N/A';
$tct_max_price_annotation = isset($info['tct_max_price_annotation']) && $info['tct_max_price_annotation'] ? $info['tct_max_price_annotation'] : 'N/A';

$raw_land_cost = isset($info['raw_land_cost']) && $info['raw_land_cost'] ? $info['raw_land_cost'] : '';
$development_cost = isset($info['development_cost']) && $info['development_cost'] ? $info['development_cost'] : '';
$construction_in_progress_cost = isset($info['construction_in_progress_cost']) && $info['construction_in_progress_cost'] ? $info['construction_in_progress_cost'] : '';
?>

<script type="text/javascript">
    window.property_id = "<?php echo $id ?>";
</script>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Property</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <?php if ($this->ion_auth->is_admin()) : ?>
                    <a href="<?php echo site_url('property/form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                        <i class="fa fa-edit"></i> Edit
                    </a>
                <?php endif; ?>
                <a href="<?php echo site_url('property'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!--begin:: Portlet-->
    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <?php if (get_image('property', 'images', @$image)) : ?>
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<?php echo base_url(get_image('property', 'images', $image)); ?>" width="110px" height="110px" />
                        </div>
                    <?php else : ?>
                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                            <?php echo get_initials($project_name); ?>
                        </div>
                    <?php endif; ?>

                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <span class="kt-widget__username"><?php echo ucwords($name); ?></span>
                        </div>

                        <div class="kt-widget__desc">
                            <?php echo ucwords($project_name); ?>
                        </div>
                    </div>
                    <div class="kt-widget__stats kt-margin-t-20">
                        <div class="kt-widget__icon">
                            <i class="flaticon2-architecture-and-city"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title kt-font-bold">Construction Progress</span><br>
                            <span class="kt-widget__value"><?php echo $progress; ?>%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab" aria-selected="true">
                            <i class="flaticon2-information"></i> General Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#pricing_information" role="tab" aria-selected="false">
                            <i class="flaticon-price-tag"></i> Pricing Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#price_logs" role="tab" aria-selected="false">
                            <i class="flaticon2-bar-chart"></i> Price Logs
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#saleslog" role="tab" aria-selected="false">
                            <i class="flaticon-line-graph"></i> Sales Logs
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payment_request" role="tab" aria-selected="false">
                            <i class="fa fa-hand-holding-usd"></i> Payment Request
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payment_voucher" role="tab" aria-selected="false">
                            <i class="flaticon2-list"></i> Payment Voucher
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#audittrail" role="tab" aria-selected="false">
                            <i class="flaticon2-layers-1"></i> Audit Trail
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#accounting_entries" role="tab" aria-selected="false">
                            <i class="flaticon-notepad"></i> Accounting Entries
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div class="tab-content  kt-margin-t-20">
                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="basic_information" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row ">
                                    <div class="col">

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">General Status:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('property_status', @$status, 'view'); ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Project Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $project_name; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Property Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $name; ?></span>
                                            </div>
                                        </div>


                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Property Model Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $model_name; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">House Interior Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $interior_name; ?></span>
                                            </div>
                                        </div>


                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Package Type:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('package_types', @$package_type_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">HLURB Classification:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('hlurb_classification', @$hlurb_classification_id, 'view'); ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Phase #:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $status; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row d-none">
                                            <label class="col col-form-label text-right">Cluster #:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $cluster; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Block #:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $block; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Lot #:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $lot; ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Date Registered:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $date_registered; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Sales Arm:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $sales_arm_id; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">TCT Lot No.:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $tct_number; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Deed of Absolute Sale Notarized Date:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $doas_notarized_date; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Deed of Absolute Sale Amount:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $doas_amount; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Max Selling Price:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $max_selling_price; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">TCT Max Price Annotation:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $tct_max_price_annotation; ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Title #:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $title_number; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">CTS #:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $cts_number; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">CTS Notarized Date:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $cts_notarized_date; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">CTS Amount:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $cts_amount; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Tax Declaration Building:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $tax_declaration_building; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Tax Declaration Lot:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $tax_declaration_lot; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Raw Land Cost:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $raw_land_cost; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Development Cost:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $development_cost; ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Construction In Progress Cost:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $construction_in_progress_cost; ?></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="pricing_information" role="tabpanel-2">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Lot Area:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $lot_area; ?> SQM</span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Floor Area:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $floor_area; ?> SQM</span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Lot Price / SQM:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo money_php($lot_price_per_sqm); ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Total Lot Price:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo money_php($total_lot_price); ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Property Model Price:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo money_php($model_price); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Total Selling Price:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo money_php($total_selling_price); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Promo Name and Amount:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $promo_name; ?> & <?php echo money_php($promo_amount); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Discounted Total Selling Price:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo money_php($discounted_total_selling_price); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            &nbsp;
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            &nbsp;
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Lot Price / SQM:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">Amount fetch from project base lot price</span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Total Lot Price:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">Lot Price / SQM * Lot Area</span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Property Model Price:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">Amount fetch from interior price</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Total Selling Price:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">Property Model Price + Total Lot Price</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="price_logs" role="tabpanel-3">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="kt-portlet__body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="kt_widget5_tab1_content">

                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td>Date of Price Increase</td>
                                                                <td>Total Selling Price</td>
                                                                <td>Percentage Increase</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if ($price_logs) : ?>
                                                                <?php foreach ($price_logs as $key => $pl) : ?>
                                                                    <?php
                                                                    $date_of_price_increase = view_date($pl['updated_at']);
                                                                    $total_selling_price = $pl['total_selling_price'];
                                                                    $percentage_increase = $pl['percentage_increase'];

                                                                    ?>
                                                                    <tr>
                                                                        <td><?= $date_of_price_increase ?></td>
                                                                        <td><?= $total_selling_price ?></td>
                                                                        <td><?= $percentage_increase . "%" ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="saleslog" role="tabpanel-4">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-toolbar">
                                        <ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#kt_widget5_tab1_content" role="tab" aria-selected="false">
                                                    Last Month
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#kt_widget5_tab2_content" role="tab" aria-selected="false">
                                                    Last Year
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#kt_widget5_tab3_content" role="tab" aria-selected="true">
                                                    All time
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="kt_widget5_tab1_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <?php if ($sales_logs) : ?>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <td style="width:20%">Property</td>
                                                                    <td style="width:25%">Reference Number</td>
                                                                    <td style="width:20%">Reservation Date</td>
                                                                    <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                    <td class="kt-align-center" style="width:22%">Buyer</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($sales_logs as $key => $sales_log) : ?>
                                                                    <?php
                                                                    $reference = $sales_log['reference'];
                                                                    $property_name = $sales_log['property']['name'];
                                                                    $reservation_date = $sales_log['reservation_date'];
                                                                    $this->transaction_library->initiate($sales_log['id']);
                                                                    $tpm_p = $this->transaction_library->get_amount_paid(1, 0, 1);
                                                                    $payment_percentage = $tpm_p . "%";
                                                                    $buyer_name = @$sales_log['buyer']["first_name"] . " " . @$sales_log['buyer']["middle_name"] . " " . @$sales_log['buyer']["last_name"];
                                                                    ?>
                                                                    <tr>
                                                                        <td><?= $property_name ?></td>
                                                                        <td><?= $reference ?></td>
                                                                        <td><?= view_date($reservation_date) ?></td>
                                                                        <td class="kt-align-center"><?= $payment_percentage ?></td>
                                                                        <td class="kt-align-center"><?= $buyer_name ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    <?php else : ?>
                                                        <h4 class="font-weight-italic text-center">
                                                            No records found
                                                        </h4>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_widget5_tab2_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td style="width:20%">Property</td>
                                                                <td style="width:25%">Reference Number</td>
                                                                <td style="width:20%">Reservation Date</td>
                                                                <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                <td class="kt-align-center" style="width:22%">Property</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_widget5_tab3_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td style="width:20%">Property</td>
                                                                <td style="width:25%">Reference Number</td>
                                                                <td style="width:20%">Reservation Date</td>
                                                                <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                <td class="kt-align-center" style="width:22%">Property</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="payment_request" role="tabpanel-5">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentrequest_table">
                                    <thead>
                                        <tr>
                                            <th width="1%">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Payee</th>
                                            <th>Payable Type</th>
                                            <th>Property</th>
                                            <th>Due Date</th>
                                            <th>Gross Amount</th>
                                            <th>Tax Rate (VAT - WHT)</th>
                                            <th>Paid Amount</th>
                                            <th>Remaining Amount</th>
                                            <th>Payment Status</th>
                                            <th>Status</th>
                                            <th>Particulars</th>
                                        </tr>
                                    </thead>
                                </table>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="payment_voucher" role="tabpanel-6">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentvoucher_table">
                                    <thead>
                                        <tr>
                                            <th width="1%">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Payment Type</th>
                                            <th>Payee</th>
                                            <th>Payee Type</th>
                                            <th>Paid Date</th>
                                            <th>Paid Amount</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                </table>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="audittrail" role="tabpanel-7">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">Audit Trail</h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="table-responsive">
                                        <?php if ($audit) : ?>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <td style="width:10%">ID</td>
                                                        <td style="width:15%">Affected Table</td>
                                                        <td style="width:15%">Action</td>
                                                        <td class="kt-align-center" style="width:22%">Date</td>
                                                        <td class="kt-align-center" style="width:25%">Created By</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($audit as $key => $value) : ?>
                                                        <tr class="clickable" data-toggle="collapse" style="cursor: pointer;" id="row-<?php echo $value['id'] ?>" data-target="#hidden-row-<?php echo $value['id'] ?>">
                                                            <td><?php echo $value['id'] ?></td>
                                                            <td><?php echo $value['affected_table'] ?></td>
                                                            <td><?php echo $value['action'] ?></td>
                                                            <td><?php echo $value['created_at'] ?></td>
                                                            <td><?php echo $value['user']['first_name'] . ' ' . $value['user']['last_name'] ?></td>
                                                        </tr>
                                                        <tr class="collapse" id="hidden-row-<?php echo $value['id'] ?>">
                                                            <td colspan="3">
                                                                Previous Record: <code><?php echo $value['previous_record'] ?></code>
                                                            </td>
                                                            <td colspan="2">
                                                                New Record: <code><?php echo $value['new_record'] ?></code>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        <?php else : ?>
                                            <h4 class="font-weight-italic text-center">
                                                No records found
                                            </h4>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="accounting_entries" role="tabpanel-8">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="accounting_entries_table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Company</th>
                                            <th>Project</th>
                                            <th>Payee</th>
                                            <th>Payee Name</th>
                                            <th>OR Number</th>
                                            <th>Invoice Number</th>
                                            <th>Journal Type</th>
                                            <th>Payment Date</th>
                                            <th>Total</th>
                                            <th>Remarks</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->
            </div>
        </div>
    </div>

</div>