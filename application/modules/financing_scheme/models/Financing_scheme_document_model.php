<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Financing_scheme_document_model extends MY_Model {

	public $primary_key	=	'id'; // you MUST mention the primary key
	public $protected		=	['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $fillable		=	[
													'financing_scheme_id',
													'document_id',
													'uploaded_document_id',
													'category_id',
													'start_date',
													'owner_id',
													'group_id',
													'dependency_id',
													'is_active',
													'created_by',
													'created_at',
													'updated_by',
													'updated_at',
													'deleted_by',
													'deleted_at'
												]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $table				=	'financing_scheme_documents'; // you MUST mention the table name
	public $rules				=	[];
	public $_fields			=	[];

	function __construct () {

		parent::__construct();

		$this->soft_deletes = TRUE;

		$this->return_as = 'array';

		$this->rules['insert']	=	$this->_fields;
		$this->rules['update']	=	$this->_fields;
	}

	function find_all ( $_where = FALSE, $_columns = FALSE, $_row = FALSE ) {

		$_return	=	FALSE;

		if ( $_where ) {

			if ( is_array($_where) ) {

				foreach ( $_where as $key => $_wang ) {

					$this->db->where( $key, $_wang );
				}
			} else {

				$this->db->where( 'id', $_where );
			}
		}
		// else {

		// 	$this->db->where('deleted_at IS NULL');
		// }

		if ( $_columns ) {

			if ( is_array($_columns) ) {

				foreach ( $_columns as $key => $_col ) {

					$this->db->select($_col);
				}
			} else {

				$this->db->select($_columns);
			}
		} else {

			$this->db->select('*');
		}

		$this->db->where('deleted_at IS NULL');

		// if ( $_limit ) {

		// 	if ( is_numeric($_limit) ) {

		// 		$this->db->limit($_limit);
		// 	}
		// }

		$_query	=	$this->db->get($this->table);
		if ( $_row ) {

			$_return	=	$_query->num_rows() > 0 ? $_query->row() :  FALSE;
		} else {

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}

	function insert_dummy () {

		require APPPATH.'/third_party/faker/autoload.php';

		$_faker	=	Faker\Factory::create();

		$_datas	=	[];

		for ( $i = 1; $i <= 5; $i++ ) {

			array_push($_datas, [
				'financing_scheme_id'	=>	rand(1,20),
				'document_id'	=>	rand(1,20),
				'uploaded_document_id'	=>	rand(1,20),
				'group_id'	=>	rand(1,31),
				'owner_id'	=>	rand(1,13),
				'dependency_id'	=>	rand(1,31),
				'category_id'	=>	rand(1,13),
				'created_by'	=>	rand(1,6),
				'created_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'updated_by'	=>	rand(1,6),
				'updated_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time()))
			]);
		}

		$this->db->insert_batch($this->table, $_datas);
	}

	function _get_document_checklist ( $_li_id = FALSE ) {

		$_return	=	FALSE;

		if ( $_li_id ) {

			$_sql	=	"
								SELECT d.`id`,
									d.`name`,
									d.`description`,
									d.`owner_id`,
									d.`classification_id`,
									lid.`uploaded_document_id`,
									IF( lid.`uploaded_document_id` <> '', 'uploaded', '' ) AS `status`,
									'' AS `timeline`,
									'' AS `due_date`,
									lid.`created_at` AS `date_created`,
									d.`updated_at`
								FROM financing_scheme_documents AS lid
								LEFT JOIN financing_schemes AS li ON li.`id` = lid.`financing_scheme_id`
								LEFT JOIN documents AS d ON d.`id` = lid.`document_id`
								WHERE lid.`deleted_at` IS NULL
									AND li.`deleted_at` IS NULL
									AND d.`deleted_at` IS NULL
									AND li.`id` = ?
							";

			$_query	=	$this->db->query($_sql, [$_li_id]);

			$_return	=	$_query->num_rows() > 0 ? $_query->result_array() : FALSE;
		}

		return $_return;
	}
}