<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Financing_scheme_history_model extends MY_Model {

	public $primary_key	=	'id'; // you MUST mention the primary key
	public $protected		=	['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $fillable		=	[
													'financing_scheme_id',
													'consolidate_id',
													'subdivide_id'
												]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $table				=	'financing_scheme_histories'; // you MUST mention the table name
	public $rules				=	[];
	public $_fields			=	[];

	function __construct () {

		parent::__construct();

		$this->soft_deletes = TRUE;

		$this->return_as = 'array';

		$this->rules['insert']	=	$this->_fields;
		$this->rules['update']	=	$this->_fields;
	}

	function find_all ( $_where = FALSE, $_columns = FALSE, $_row = FALSE ) {

		$_return	=	FALSE;

		if ( $_where ) {

			if ( is_array($_where) ) {

				foreach ( $_where as $key => $_wang ) {

					$this->db->where( $key, $_wang );
				}
			} else {

				$this->db->where( 'id', $_where );
			}
		} else {

			$this->db->where('deleted_at IS NULL');
		}

		if ( $_columns ) {

			if ( is_array($_columns) ) {

				foreach ( $_columns as $key => $_col ) {

					$this->db->select($_col);
				}
			} else {

				$this->db->select($_columns);
			}
		} else {

			$this->db->select('*');
		}

		$_query	=	$this->db->get($this->table);
		if ( $_row ) {

			$_return	=	$_query->num_rows() > 0 ? $_query->row() :  FALSE;
		} else {

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}
}