<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 
 */
class Financing_scheme_model extends MY_Model
{

	public $primary_key	=	'id'; // you MUST mention the primary key
	public $protected		=	['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $fillable		=	[
		'name',
		'description',
		'scheme_type',
		'category_id',
		'is_active',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
		'deleted_by',
		'deleted_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $table				=	'financing_schemes'; // you MUST mention the table name
	public $rules				=	[];
	public $_fields			=	[
		'name'	=>	array(
			'field'	=>	'name',
			'label'	=>	'Financing Scheme Name',
			'rules'	=>	'trim|required'
		),
		'scheme_type' => array(
			'field' => 'scheme_type',
			'label' => 'Scheme Type',
			'rules' => 'trim'
		),
		'category_id' => array(
			'field' => 'category_id',
			'label' => 'Category',
			'rules' => 'trim'
		),
	];

	function __construct()
	{

		parent::__construct();

		$this->soft_deletes = TRUE;

		$this->return_as = 'array';

		$this->rules['insert']	=	$this->_fields;
		$this->rules['update']	=	$this->_fields;


		$this->has_many['values'] = array('foreign_model' => 'financing_scheme/financing_scheme_values_model', 'foreign_table' => 'financing_scheme_values', 'foreign_key' => 'financing_id', 'local_key' => 'id');

		$this->has_many['commission_values'] = array('foreign_model' => 'financing_scheme_commission_values_model', 'foreign_table' => 'financing_scheme_commission_values', 'foreign_key' => 'financing_id', 'local_key' => 'id');
		$this->has_one['category'] = array('foreign_model' => 'financing_scheme_categories/financing_scheme_category_model', 'foreign_table' => 'financing_scheme_categories', 'foreign_key' => 'id', 'local_key' => 'category_id');
	}


	function find_all($_where = FALSE, $_columns = FALSE, $_row = FALSE)
	{

		$_return	=	FALSE;

		if ($_where) {

			if (is_array($_where)) {

				foreach ($_where as $key => $_wang) {

					$this->db->where($key, $_wang);
				}
			} else {

				$this->db->where('id', $_where);
			}
		} else {

			$this->db->where('deleted_at IS NULL');
		}

		if ($_columns) {

			if (is_array($_columns)) {

				foreach ($_columns as $key => $_col) {

					$this->db->select($_col);
				}
			} else {

				$this->db->select($_columns);
			}
		} else {

			$this->db->select('*');
		}

		// if ( $_limit ) {

		// 	if ( is_numeric($_limit) ) {

		// 		$this->db->limit($_limit);
		// 	}
		// }

		$_query	=	$this->db->get($this->table);
		if ($_row) {

			$_return	=	$_query->num_rows() > 0 ? $_query->row() :  FALSE;
		} else {

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}

	function insert_dummy()
	{

		require APPPATH . '/third_party/faker/autoload.php';

		$_faker	=	Faker\Factory::create();

		$_datas	=	[];

		for ($i = 1; $i <= 5; $i++) {

			array_push($_datas, [
				'financing_scheme_owner_name'	=>	$_faker->name,
				'location'	=>	$_faker->state,
				'owner_contact_info'	=>	$_faker->tollFreePhoneNumber,
				'financing_scheme_area'	=>	$_faker->randomFloat(2, 1111, 9999),
				'estimated_price'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'negotiated_price'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'final_price'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'agent_id'	=>	rand(1, 13),
				'financing_scheme_classification_id'	=>	rand(1, 37),
				'location_of_registration'	=>	$_faker->state,
				'title'	=>	$_faker->lastName,
				'title_details'	=>	$_faker->lastName,
				'lot_number'	=>	$_faker->secondaryAddress,
				'encumbrance'	=>	NULL,
				'ownership_classification_id'	=>	rand(1, 2),
				'status'	=>	rand(1, 15),
				'date_offered'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'decision_date'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'date_purchased'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'date_transfer_of_title'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'ep_code'	=>	rand(11234, 33456),
				'market_value'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'date_of_market_value_assessment'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'appraised_value'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'date_of_appraisal'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'assessed_value'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'mother_lot_tax_declaration'	=>	rand(1234, 3456),
				'tax_declaration'	=>	rand(1234, 3456),
				'company'	=>	$_faker->Company,
				'remarks'	=>	'Unidentified Lots',
				'created_by'	=>	rand(1, 6),
				'created_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'updated_by'	=>	rand(1, 6),
				'updated_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time()))
			]);
		}

		$this->db->insert_batch($this->table, $_datas);
	}

	function getReservationAmounts()
	{

		return $this->db->select('period_rate_amount')
			->distinct('period_rate_amount')
			->from('financing_scheme_values')
			->where('deleted_at IS NULL')
			->where('period_id', 1)
			->order_by('period_rate_amount')
			->get()->result_array();
	}

	function getDownpaymentPercentages()
	{

		return $this->db->select('period_rate_percentage')
			->distinct('period_rate_percentage')
			->from('financing_scheme_values')
			->where('deleted_at IS NULL')
			->where('period_id', 2)
			->order_by('period_rate_percentage')
			->get()->result_array();
	}

	function getDownpaymentTerms()
	{

		return $this->db->select('period_term')
			->distinct('period_term')
			->from('financing_scheme_values')
			->where('deleted_at IS NULL')
			->where('period_id', 2)
			->order_by('period_term')
			->get()->result_array();
	}

	function getDownpaymentInterestRates()
	{

		return $this->db->select('period_interest_percentage')
			->distinct('period_interest_percentage')
			->from('financing_scheme_values')
			->where('deleted_at IS NULL')
			->where('period_id', 2)
			->order_by('period_interest_percentage')
			->get()->result_array();
	}

	function getLoanPercentages()
	{

		return $this->db->select('period_rate_percentage')
			->distinct('period_rate_percentage')
			->from('financing_scheme_values')
			->where('deleted_at IS NULL')
			->where('period_id', 3)
			->order_by('period_rate_percentage')
			->get()->result_array();
	}

	function getLoanTerms()
	{

		return $this->db->select('period_term')
			->distinct('period_term')
			->from('financing_scheme_values')
			->where('deleted_at IS NULL')
			->where('period_id', 3)
			->order_by('period_term')
			->get()->result_array();
	}

	function getLoanInterestRates()
	{

		return $this->db->select('period_interest_percentage')
			->distinct('period_interest_percentage')
			->from('financing_scheme_values')
			->where('deleted_at IS NULL')
			->where('period_id', 3)
			->order_by('period_interest_percentage')
			->get()->result_array();
	}
}
