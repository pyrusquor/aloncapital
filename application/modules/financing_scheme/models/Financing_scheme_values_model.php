<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Financing_scheme_values_model extends MY_Model
{

	public $table = 'financing_scheme_values'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'financing_id',
		'period_id',
		'period_term',
		'period_rate_amount',
		'period_rate_percentage',
		'period_interest_percentage',
		'order_id',
		'is_active',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
		'deleted_by',
		'deleted_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = FALSE;
		// $this->timestamps = FALSE;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
	}
}
