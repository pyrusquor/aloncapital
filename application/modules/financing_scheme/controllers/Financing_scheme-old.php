<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 */
class Financing_scheme extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();

        $this->load->model('financing_scheme/Financing_scheme_model', 'M_financing_scheme');
        $this->load->model('financing_scheme/Financing_scheme_values_model', 'M_financing_scheme_value');
        $this->load->model('financing_scheme/Financing_scheme_commission_values_model', 'M_financing_scheme_commission_value');
        $this->load->model('financing_scheme/Financing_scheme_document_model', 'M_li_document');

        $this->_table_fillables = $this->M_financing_scheme->fillable;
        $this->_table_columns = $this->M_financing_scheme->__get_columns();
    }

    public function financing_scheme_by_category() {
        $return = $this->M_financing_scheme->with_values()->where('category_id =', $this->input->post('categoryID'))->as_array()->get_all();
        foreach($return as $key => $value) {
            $return[$key]['scheme_type'] = Dropdown::get_static('scheme_types', $value['scheme_type']);
        }

        echo json_encode($return);
    }

    public function financing_scheme_query() {
        $params = array();
        $financing_scheme_type      =   $this->input->post('financing_scheme_type');
        $reservation_amount         =   $this->input->post('reservation_amount');
        $downpayment                =   $this->input->post('downpayment');
        $downpayment_terms          =   $this->input->post('downpayment_terms');
        $downpayment_interest_rate  =   $this->input->post('downpayment_interest_rate');
        $loan                       =   $this->input->post('loan');
        $loan_terms                 =   $this->input->post('loan_terms');
        $loan_interest_rate         =   $this->input->post('loan_interest_rate');
        $category_id                =   $this->input->post('category_id');

        $this->db->select('f_scheme.id');
        $this->db->distinct();
        $this->db->from('financing_schemes as f_scheme');
        $this->db->join('financing_scheme_values as f_value', 'f_scheme.id = f_value.financing_id', 'both');

        if($financing_scheme_type) {
            $this->db->where('f_scheme.scheme_type', $financing_scheme_type);        
        }

        if($category_id) {
            $this->db->where('f_scheme.category_id', $category_id);        
        }

        if($reservation_amount) {
           $this->db->where('f_value.period_rate_amount', $reservation_amount);
        }

        if($downpayment) {
            $this->db->where('f_value.period_rate_percentage', $downpayment);
        }

        if($downpayment_terms) {
            $this->db->where('f_value.period_term', $downpayment_terms);
        }

        if($downpayment_interest_rate) {
            $this->db->where('f_value.period_interest_percentage', $downpayment_interest_rate);
        }

        if($loan) {
            $this->db->where('f_value.period_rate_percentage', $loan);
        }

        if($loan_terms) {
            $this->db->where('f_value.period_term', $loan_terms);
        }

        if($loan_interest_rate) {
            $this->db->where('f_value.period_interest_percentage', $loan_interest_rate);
        }
       
        $query = $this->db->get();
        $result = $query->result_array();

        if(count($result) != 0) {
            foreach($result as $key => $value) {
                $params[] =  $value['id'];
            }
            
            $result = $this->M_financing_scheme->with_values()->where('id', $params)->as_array()->get_all();
        } else {
            $result = 0;
        }
        
        echo json_encode($result);

    }

    public function get_detail() {
        $return = $this->M_financing_scheme->with_values()->where('id =', $this->input->post('id'))->get_all();

        echo json_encode($return);
    }

    public function index()
    {

        $this->view_data['_largescreen'] = false;

        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills); #pd($this->view_data['_fillables']);
        $this->view_data['_columns'] = $this->__get_columns($_fills); #ud($this->view_data['_columns']);

        $_financing_scheme = $this->M_financing_scheme->get_all();
        if ($_financing_scheme) {

            $this->view_data['_total'] = count($_financing_scheme);
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function get_financing_schemes()
    {

        $_response = [
            'iTotalDisplayRecords' => '',
            'iTotalRecords' => '',
            'sColumns' => '',
            'sEcho' => '',
            'data' => '',
        ];

        $_total['_displays'] = 0;
        $_total['_records'] = 0;

        $_columns = [
            'id' => true,
            'name' => true,
            'description' => true,
            'scheme_type' => true,
            'created_at' => true,
            'updated_at' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

            $_columns = [];

            foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

                $_columns[$_def] = true;
            }
        }

        $_financing_scheme = $this->M_financing_scheme->as_array()->get_all();
        if ($_financing_scheme) {

            $_datas = [];

            foreach ($_financing_scheme as $_likey => $_li) {
                $_li['scheme_type'] = Dropdown::get_static('scheme_types', $_li['scheme_type'], 'view');
                $_datas[] = $this->filterArray($_li, $_columns);
            }

            $_total['_displays'] = $_total['_records'] = count($_datas);

            if (isset($_REQUEST['search'])) {

                $_datas = $this->filterKeyword($_datas, $_REQUEST['search']);

                $_total['_displays'] = $_datas ? count($_datas) : 0;
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

                foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

                    if ($_clm['search']) {

                        $_datas = $this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

                        $_total['_displays'] = $_datas ? count($_datas) : 0;
                    }
                }
            }

            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($_datas, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            if (isset($_REQUEST['length'])) {

                $_datas = array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
            }

            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

                $_temp = $_datas;
                $_datas = [];

                foreach ($_temp as $key => $_tmp) {

                    $_datas[] = array_values($_tmp);
                }
            }

            $_secho = 0;
            if (isset($_REQUEST['sEcho'])) {

                $_secho = intval($_REQUEST['sEcho']);
            }

            $_response = [
                'iTotalDisplayRecords' => $_total['_displays'],
                'iTotalRecords' => $_total['_records'],
                'sColumns' => '',
                'sEcho' => $_secho,
                'data' => $_datas,
            ];
        }

        echo json_encode($_response);

        exit();
    }

    public function view($_id = false, $_type = 'html', $_val = 0, $_date = 0)
    {

        if ($_id) {

            $this->view_data['_financing_scheme'] = $this->M_financing_scheme
                ->with_values()->with_commission_values()->with_category()
                ->get($_id);

            if ($this->view_data['_financing_scheme']) {

                if ($_type == "html") {

                    $this->template->build('view', $this->view_data);

                } else if ($_type == "json") {
                    echo json_encode($this->view_data['_financing_scheme']);

                } else if ($_type == "transaction") {

                    $_date = isLeapDay($_date);

                    $this->view_data['collectible_price'] = $_val;
                    $this->view_data['reservation_date'] = $_date;

                    if ($this->input->is_ajax_request()) {
                        $info['html'] = $this->load->view('transaction/_financing_scheme_form', $this->view_data, true);
                        $info['expiration_date'] = add_months_to_date($_date, 1);

                        echo json_encode($info);

                    } else {
                        $this->template->build('transaction/_financing_scheme_form', $this->view_data);

                    }

                }
            } else {

                show_404();
            }

        } else {

            show_404();
        }
    }

    public function create()
    {

        if ($this->input->post()) {

            $_input = $this->input->post();

            $additional = [
                'is_active' => 1,
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            $info = $_input['info'];
            $scheme_values = $_input['scheme_values'];
            $commission_values = $_input['commission_values'];

            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
            $_insert = $this->M_financing_scheme->insert($info + $additional); #lqq(); ud($_insert);

            if ($scheme_values['period_id']) {

                foreach ($scheme_values['period_id'] as $key => $period_id) {

                    $s_value['financing_id'] = $_insert;
                    $s_value['period_id'] = $period_id;
                    $s_value['period_term'] = cln($scheme_values['period_term'][$period_id]);
                    $s_value['period_rate_amount'] = cln($scheme_values['period_rate_amount'][$period_id]);
                    $s_value['period_rate_percentage'] = cln($scheme_values['period_rate_percentage'][$period_id]);
                    $s_value['period_interest_percentage'] = cln($scheme_values['period_interest_percentage'][$period_id]);

                    $this->M_financing_scheme_value->insert($s_value);

                }

            }

            if ($commission_values) {

                foreach ($commission_values['period_id'] as $key => $period_id) {

                    $c_values['financing_id'] = $_insert;
                    $c_values['period_id'] = $period_id;
                    $c_values['percentage_to_finish'] = cln($commission_values['percentage_to_finish'][$period_id]);
                    $c_values['commission_rate_amount'] = cln($commission_values['commission_rate_amount'][$period_id]);
                    $c_values['commission_rate_percentage'] = cln($commission_values['commission_rate_percentage'][$period_id]);
                    $c_values['commission_term'] = cln($commission_values['commission_term'][$period_id]);
                    $c_values['commission_vat'] = cln($commission_values['commission_vat'][$period_id]);

                    $this->M_financing_scheme_commission_value->insert($c_values);

                }

            }

            $this->db->trans_complete(); # Completing transaction

            /*Optional*/
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $this->notify->error('Oh snap! Please refresh the page and try again.');
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $this->notify->success('Financing Scheme successfully created.', 'financing_scheme');
            }

        }

        $this->template->build('create', $this->view_data);
    }

    public function update($_id = false, $type = "")
    {

        if ($_id) {

            $_financing_scheme = $this->M_financing_scheme->with_values()->with_commission_values()->with_category()->get($_id);

            $_input = $this->input->post();

            if ($type == "debug") {

                vdebug($_financing_scheme);

            }

            if ($_financing_scheme) {

                $this->view_data['_financing_scheme'] = $_financing_scheme;

                if ($this->input->post()) {

                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $info = $_input['info'];
                    $scheme_values = $_input['scheme_values'];
                    $commission_values = $_input['commission_values'];

                    // vdebug($_input);

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    // $_updated    =    $this->M_financing_scheme->update($_update, $_id);

                    $this->M_financing_scheme->update($info + $additional, $_id); #lqq(); ud($_insert);

                    if ($scheme_values['period_id']) {

                        foreach ($scheme_values['period_id'] as $key => $period_id) {

                            $s_id = cln($scheme_values['id'][$period_id]);

                            $s_value['financing_id'] = $_id;
                            $s_value['period_id'] = $period_id;
                            $s_value['period_term'] = cln($scheme_values['period_term'][$period_id]);
                            $s_value['period_rate_amount'] = cln($scheme_values['period_rate_amount'][$period_id]);
                            $s_value['period_rate_percentage'] = cln($scheme_values['period_rate_percentage'][$period_id]);
                            $s_value['period_interest_percentage'] = cln($scheme_values['period_interest_percentage'][$period_id]);

                            $this->M_financing_scheme_value->update($s_value, $s_id); #lqq(); ud($_insert);
                        }

                    }

                    // vdebug($commission_values);

                    // if ($commission_values) {

                    //     foreach ($commission_values['period_id'] as $key => $period_id) {

                    //         $c_id = cln($commission_values['id'][$period_id]);

                    //         $c_values['financing_id'] = $_id;
                    //         $c_values['period_id'] = $period_id;
                    //         $c_values['percentage_to_finish'] = cln($commission_values['percentage_to_finish'][$period_id]);
                    //         $c_values['commission_rate_amount'] = cln($commission_values['commission_rate_amount'][$period_id]);
                    //         $c_values['commission_rate_percentage'] = cln($commission_values['commission_rate_percentage'][$period_id]);
                    //         $c_values['commission_term'] = cln($commission_values['commission_term'][$period_id]);
                    //         $c_values['commission_vat'] = cln($commission_values['commission_vat'][$period_id]);

                    //         $this->M_financing_scheme_commission_value->update($c_values, $c_id); #lqq(); ud($_insert);

                    //     }

                    // }

                    $this->db->trans_complete(); # Completing transaction

                    if ($_updated !== false) {

                        $this->notify->success('Financing Scheme successfully updated.', 'financing_scheme');
                    } else {

                        $this->notify->error('Oh snap! Please refresh the page and try again.');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {

        $_response['_status'] = 0;
        $_response['_msg'] = 'Oops! Please refresh the page and try again.';

        $_id = $this->input->post('id');
        if ($_id) {

            $_financing_scheme = $this->M_financing_scheme->get($_id);
            if ($_financing_scheme) {

                $_deleted = $this->M_financing_scheme->delete($_financing_scheme['id']);
                if ($_deleted !== false) {

                    $_response['_status'] = 1;
                    $_response['_msg'] = 'Financing Scheme successfully deleted';
                } else {

                    $_response['_msg'] = 'Proccess Failed. Please refresh the page and try again';
                }
            }
        }

        echo json_encode($_response);

        exit();
    }

    public function bulkDelete()
    {

        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_financing_scheme->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Financing Scheme Inventories successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function document_checklist($_li_id = false)
    {

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->load->model('financing_scheme/Financing_scheme_document_model', 'M_li_document');

        if ($_li_id) {

            $this->view_data['_li_id'] = $_li_id;

            $_documents = $this->M_li_document->_get_document_checklist($_li_id);
            if ($_documents) {

                $this->view_data['_documents'] = $_documents; #ud($_documents);
                $this->view_data['_total'] = count($_documents);
            }

            $this->template->build('document_checklist', $this->view_data);
        } else {

            show_404();
        }
    }

    public function process_checklist_upload($_li_id = false, $_doc_id = false)
    {

        if ($_li_id && $_doc_id && isset($_FILES['_file']['name']) && ($_FILES['_file']['name'] !== '')) {

            $this->load->model('financing_scheme/Financing Scheme_inventory_document_model', 'M_li_document');

            unset($_where);
            $_where['financing_scheme_id'] = $_li_id;
            $_where['document_id'] = $_doc_id;

            $_li_document = $this->M_li_document->find_all($_where, ['id'], true);
            if ($_li_document && isset($_li_document->id) && $_li_document->id) {

                $_ext = ['.jpg', '.jpeg', '.png'];

                set_time_limit(0);

                unset($_config);
                $_config['upload_path'] = $_location = 'assets/img/_financing_scheme/_documents/_process';
                $_config['allowed_types'] = 'jpeg|jpg|png';
                $_config['overwrite'] = true;
                $_config['max_size'] = '1000000';
                $_config['file_name'] = $_filename = $_doc_id . '_thumb';

                if (isset($_ext) && $_ext) {

                    $_image = $_location . '/' . $_filename;

                    foreach ($_ext as $key => $x) {

                        if (file_exists($_image . $x)) {

                            unlink($_image . $x);
                        }
                    }
                }

                $this->load->library('upload', $_config);

                if (!$this->upload->do_upload('_file')) {

                    // ud($this->upload->displaY_errors());

                    $this->notify->error($this->upload->displaY_errors(), 'financing_scheme/process_checklist/' . $_li_id);
                } else {

                    $_uploaded_document_id = false;

                    $this->load->model('document/Financing Scheme_document_upload_model', 'M_financing_scheme_uploaded_document');

                    // up($this->user);

                    // ud($this->upload->data());

                    $_udatas = $this->upload->data();
                    if ($_udatas) {

                        unset($_where);
                        $_where['financing_scheme_id'] = $_li_id;
                        $_where['document_id'] = $_doc_id;

                        $_uploaded_document = $this->M_financing_scheme_uploaded_document->find_all($_where, ['id'], true); #lqq(); ud($_uploaded_document);
                        if ($_uploaded_document && isset($_uploaded_document->id) && $_uploaded_document->id) {

                            $_uploaded_document_id = $_uploaded_document->id;

                            unset($_update);
                            $_update['financing_scheme_id'] = $_li_id;
                            $_update['document_id'] = $_doc_id;
                            $_update['file_name'] = isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : null;
                            $_update['file_type'] = isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : null;
                            $_update['file_src'] = isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : null;
                            $_update['file_path'] = isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : null;
                            $_update['full_path'] = isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : null;
                            $_update['raw_name'] = isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : null;
                            $_update['orig_name'] = isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : null;
                            $_update['client_name'] = isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : null;
                            $_update['file_ext'] = isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : null;
                            $_update['file_size'] = isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : null;
                            $_update['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                            $_update['updated_at'] = NOW;

                            $_updated = $this->M_financing_scheme_uploaded_document->update($_update, $_uploaded_document_id);
                            if (!$_updated) {

                                $this->notify->error('Upload Failed. Please refresh the page and try again.', 'financing_scheme/process_checklist/' . $_li_id);
                            }
                        } else {

                            unset($_insert);
                            $_insert['financing_scheme_id'] = $_li_id;
                            $_insert['document_id'] = $_doc_id;
                            $_insert['file_name'] = isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : null;
                            $_insert['file_type'] = isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : null;
                            $_insert['file_src'] = isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : null;
                            $_insert['file_path'] = isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : null;
                            $_insert['full_path'] = isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : null;
                            $_insert['raw_name'] = isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : null;
                            $_insert['orig_name'] = isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : null;
                            $_insert['client_name'] = isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : null;
                            $_insert['file_ext'] = isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : null;
                            $_insert['file_size'] = isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : null;
                            $_insert['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                            $_insert['created_at'] = NOW;
                            $_insert['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                            $_insert['updated_at'] = NOW;

                            $_inserted = $this->M_financing_scheme_uploaded_document->insert($_insert);
                            if ($_inserted) {

                                $_uploaded_document_id = $this->db->insert_id();
                            } else {

                                $this->notify->error('Upload Failed. Please refresh the page and try again.', 'financing_scheme/process_checklist/' . $_li_id);
                            }
                        }

                        // ud($_uploaded_document_id);

                        if ($_uploaded_document_id) {

                            unset($_update);
                            $_update['uploaded_document_id'] = $_uploaded_document_id;
                            $_update['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                            $_update['updated_at'] = NOW;

                            $_updated = $this->M_li_document->update($_update, $_li_document->id);
                            if ($_updated) {

                                $this->notify->success('Success! Image uploaded.', 'financing_scheme/process_checklist/' . $_li_id);
                            } else {

                                $this->notify->error('Upload Failde. Please refresh the page and try again.', 'financing_scheme/process_checklist/' . $_li_id);
                            }
                        } else {

                            $this->notify->error('Something went wrong. Please refresh the page and try again.', 'financing_scheme/process_checklist/' . $_li_id);
                        }
                    }
                }
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function get_document_checklist($_li_id = false)
    {

        $_docuemnt_ids = [];

        $this->load->model('financing_scheme/Financing Scheme_inventory_document_model', 'M_li_document');
        $this->load->model('document/Document_model', 'M_document');

        $_response = [];

        $_total['_displays'] = 0;
        $_total['_records'] = 0;
        $_datas = [];
        $_secho = 0;

        if ($_li_id) {

            $_columns = [
                'id' => true,
                'name' => true,
                'description' => true,
                'owner_id' => true,
                'classification_id' => true,
                'updated_at' => true,
            ];

            if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

                $_columns = [];

                foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

                    $_columns[$_def] = true;
                }
            }

            // $_documents    =    $this->M_document->where('id', $_docuemnt_ids)->as_array()->get_all(); #vd($_documents);
            $_documents = $this->M_li_document->_get_document_checklist($_li_id);
            if ($_documents) {

                foreach ($_documents as $_ckkey => $_ck) {

                    $_datas[] = $this->filterArray($_ck, $_columns);
                }

                $_total['_displays'] = $_total['_records'] = count($_datas);

                if (isset($_REQUEST['search'])) {

                    $_datas = $this->filterKeyword($_datas, $_REQUEST['search']);

                    $_total['_displays'] = $_datas ? count($_datas) : 0;
                }

                if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

                    foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

                        if ($_clm['search']) {

                            $_datas = $this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

                            $_total['_displays'] = $_datas ? count($_datas) : 0;
                        }
                    }
                }

                if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                    $_column = $_REQUEST['order'][0]['column'];
                    $_dir = $_REQUEST['order'][0]['dir'];

                    usort($_datas, function ($x, $y) use ($_column, $_dir) {

                        $x = array_slice($x, $_column, 1);
                        $x = array_pop($x);

                        $y = array_slice($y, $_column, 1);
                        $y = array_pop($y);

                        if ($_dir === 'asc') {

                            return $x > $y ? true : false;
                        } else {

                            return $x < $y ? true : false;
                        }
                    });
                }

                if (isset($_REQUEST['length'])) {

                    $_datas = array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
                }

                if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

                    $_temp = $_datas;
                    $_datas = [];

                    foreach ($_temp as $key => $_tmp) {

                        $_datas[] = array_values($_tmp);
                    }
                }

                if (isset($_REQUEST['sEcho'])) {

                    $_secho = intval($_REQUEST['sEcho']);
                }
            }
        }

        $_response = [
            'iTotalDisplayRecords' => $_total['_displays'],
            'iTotalRecords' => $_total['_records'],
            'sColumns' => '',
            'sEcho' => $_secho,
            'data' => $_datas,
        ];

        echo json_encode($_response);

        exit();
    }

    public function documents($_li_id = false)
    {

        $this->view_data['_largescreen'] = true;

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        // $this->load->model('financing_scheme/Financing Scheme_inventory_document_model', 'M_li_document');
        $this->load->model('checklist/Checklist_model', 'M_checklist');

        if ($_li_id) {

            $this->view_data['_li_id'] = $_li_id;

            $_checklists = $this->M_checklist->find_all(false, ['id', 'name']);
            if ($_checklists) {

                $this->view_data['_checklists'] = $_checklists; #ud($_checklists);
            }

            $this->template->build('documents', $this->view_data);
        } else {

            show_404();
        }
    }

    public function get_filtered_document_checklist()
    {

        $_respo['_status'] = 0;
        $_respo['_msg'] = '';
        $_view_data = [];

        $_checklist_id = $this->input->post('value');
        $_li_id = $this->input->post('li_id');
        if ($_checklist_id && $_li_id) {

            $_view_data['_li_id'] = $_li_id;

            $this->load->model('checklist/Document_checklist_model', 'M_document_checklist');

            $_documents = $this->M_document_checklist->_get_documents($_checklist_id);
            if ($_documents) {

                $_view_data['_documents'] = $_documents;
            }

            $_respo['_status'] = 1;

            $_respo['_html'] = $this->load->view('_checklist/documents', $_view_data, true);
        } else {

            $_respo['_msg'] = 'Oops! Please refresh the page and try again.';
        }

        echo json_encode($_respo);

        exit();
    }

    public function insert_document_checklist($_li_id = false)
    {

        if ($_li_id) {

            $_dcs = [];

            $_lid_ids = [];

            $_inserted = 0;
            $_updated = 0;
            $_removed = 0;
            $_failed = 0;

            $_inputs = $this->input->post();
            if ($_inputs) {

                foreach ($_inputs as $key => $_input) {

                    if ($key !== '_document_checklist_length') {

                        $_dcs[$key]['financing_scheme_id'] = $_li_id;
                        $_dcs[$key]['document_id'] = $key;

                        // up($_input);

                        foreach ($_input as $_ikey => $_int) {

                            if ($_ikey === 'start_date') {

                                $_dcs[$key][$_ikey] = date_format(date_create($_int), 'Y-m-d H:i:s');
                            } else {

                                $_dcs[$key][$_ikey] = $_int;
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $this->load->model('financing_scheme/Financing Scheme_inventory_document_model', 'M_li_document');

                if ($_dcs && !empty($_dcs)) {

                    foreach ($_dcs as $_dkey => $_dc) {

                        unset($_where);
                        $_where['financing_scheme_id'] = $_dc['financing_scheme_id'];
                        $_where['document_id'] = $_dc['document_id'];

                        $_lid = $this->M_li_document->find_all($_where, false, true); #lqq(); up($_lid);
                        if ($_lid) {

                            unset($_datas);
                            foreach ($_dc as $_dkey => $_d) {

                                if (($_dkey !== 'financing_scheme_id') && ($_dkey !== 'document_id')) {

                                    $_datas[$_dkey] = $_d;

                                    $_datas[$_dkey]['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                    $_datas[$_dkey]['updated_at'] = NOW;
                                }
                            }

                            $_update = $this->M_li_document->update($_datas, $_lid->id);
                            if ($_update) {

                                $_updated++;
                            } else {

                                $_failed++;

                                break;
                            }
                        } else {

                            unset($_datas);
                            $_datas = $_dc;

                            $_datas['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                            $_datas['created_at'] = NOW;
                            $_datas['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                            $_datas['updated_at'] = NOW;

                            $_insert = $this->M_li_document->insert($_datas);
                            if ($_insert) {

                                $_inserted++;
                            } else {

                                $_failed++;

                                break;
                            }
                        }
                    }

                    $_msg = '';
                    if ($_inserted > 0) {

                        $_msg = $_inserted . ' document/s was successfuly inserted';
                    }

                    if ($_updated > 0) {

                        $_msg .= ($_inserted ? ' and ' : '') . $_updated . ' document/s was successfuly updated';
                    }

                    if ($_failed > 0) {

                        $this->notify->error('Something went wrong! Please refresh the page and try again.', 'financing_scheme/document_checklist/' . $_li_id);
                    } else {

                        $this->notify->success($_msg . '.', 'financing_scheme/document_checklist/' . $_li_id);
                    }
                }
            }
        } else {

            show_404();
        }
    }

    public function process_checklist($_li_id = false)
    {

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->load->model('financing_scheme/Financing Scheme_inventory_document_model', 'M_li_document');

        if ($_li_id) {

            $this->view_data['_li_id'] = $_li_id;

            // ud(Dropdown::get_static('document_owner'));

            $_documents = $this->M_li_document->_get_document_checklist($_li_id); #lqq(); ud($_documents);
            if ($_documents) {

                $this->view_data['_documents'] = $_documents; #ud($_documents);
                $this->view_data['_total'] = count($_documents);
            }

            $this->template->build('process_checklist', $this->view_data);
        } else {

            show_404();
        }
    }

    public function get_process_checklist($_li_id = false)
    {

        $_docuemnt_ids = [];

        $this->load->model('financing_scheme/Financing Scheme_inventory_document_model', 'M_li_document');
        $this->load->model('document/Document_model', 'M_document');

        $_response = [];

        $_total['_displays'] = 0;
        $_total['_records'] = 0;
        $_datas = [];
        $_secho = 0;

        if ($_li_id) {

            $_columns = [
                'id' => true,
                'name' => true,
                'owner_id' => true,
                'status' => true,
                'date_created' => true,
            ];

            if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

                $_columns = [];

                foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

                    $_columns[$_def] = true;
                }
            }

            // $_documents    =    $this->M_document->where('id', $_docuemnt_ids)->as_array()->get_all(); #vd($_documents);
            $_documents = $this->M_li_document->_get_document_checklist($_li_id);
            if ($_documents) {

                foreach ($_documents as $_ckkey => $_ck) {

                    $_datas[] = $this->filterArray($_ck, $_columns);
                }

                $_total['_displays'] = $_total['_records'] = count($_datas);

                if (isset($_REQUEST['search'])) {

                    $_datas = $this->filterKeyword($_datas, $_REQUEST['search']);

                    $_total['_displays'] = $_datas ? count($_datas) : 0;
                }

                if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

                    foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

                        if ($_clm['search']) {

                            $_datas = $this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

                            $_total['_displays'] = $_datas ? count($_datas) : 0;
                        }
                    }
                }

                if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                    $_column = $_REQUEST['order'][0]['column'];
                    $_dir = $_REQUEST['order'][0]['dir'];

                    usort($_datas, function ($x, $y) use ($_column, $_dir) {

                        $x = array_slice($x, $_column, 1);
                        $x = array_pop($x);

                        $y = array_slice($y, $_column, 1);
                        $y = array_pop($y);

                        if ($_dir === 'asc') {

                            return $x > $y ? true : false;
                        } else {

                            return $x < $y ? true : false;
                        }
                    });
                }

                if (isset($_REQUEST['length'])) {

                    $_datas = array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
                }

                if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

                    $_temp = $_datas;
                    $_datas = [];

                    foreach ($_temp as $key => $_tmp) {

                        $_datas[] = array_values($_tmp);
                    }
                }

                if (isset($_REQUEST['sEcho'])) {

                    $_secho = intval($_REQUEST['sEcho']);
                }
            }
        }

        $_response = [
            'iTotalDisplayRecords' => $_total['_displays'],
            'iTotalRecords' => $_total['_records'],
            'sColumns' => '',
            'sEcho' => $_secho,
            'data' => $_datas,
        ];

        echo json_encode($_response);

        exit();
    }
}
