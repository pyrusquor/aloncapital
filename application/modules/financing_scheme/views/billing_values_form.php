<?php
$values = isset($_financing_scheme['values']) && $_financing_scheme['values'] ? $_financing_scheme['values'] : [];
$scheme_type 	= 	isset($_financing_scheme['scheme_type']) && $_financing_scheme['scheme_type'] ?  $_financing_scheme['scheme_type'] : '0';
// if (empty($values)) {
// 	for ($i = 1; $i < 4; $i++) {
// 		$values[]['period_id'] = $i;
// 	}

// 	// for ($i = 1; $i < 6; $i++) {
// 	// 	$values[]['period_id'] = $i;
// 	// }
// }

$periods = Dropdown::get_static('periods');

unset($periods['']);
unset($periods[4]);
unset($periods[5]);

?>

<?php if ($values) : ?>

	<?php
	// Skip EQ field
	// if ($period_id == 4) {
	// 	continue;
	// }

	// if (($period_id >= 2) && ($period_id <= 4)) {
	// 	$period_id = 2;
	// }

	// if ($period_id == 5) {
	// 	# code...
	// 	$period_id = 3;
	// }
	?>
	<div data-repeater-list="scheme_values" class="col-lg-12 ">
		<?php foreach ($values as $key => $value) :
			// $period_id = $value['period_id'];
			// $id		=	isset($value['id']) && $value['id'] ? $value['id'] : '';
		?>
			<div data-repeater-item class="form-group row align-items-center item-repeater">
				<div class="col-md-1">
					<div class="kt-form__group--inline">
						<div class="kt-form__control">
							<!-- <?php echo Dropdown::get_static('periods', $period_id, 'view'); ?> -->
							<?php echo form_dropdown('scheme_values[period_id]', $periods, @$value['period_id'], 'class="form-control period"'); ?>
							<input type="hidden" id="id" name="scheme_values[id]" value="<?php echo @$value['id']; ?>">
							<!-- <input type="hidden" id="period_id" name="scheme_values[period_id]" value="<?php echo $period_id; ?>"> -->
						</div>
					</div>
				</div>

				<div class="col-md-2">
					<div class="kt-form__group--inline">
						<div class="kt-form__control">

							<div class="kt-input-icon kt-input-icon--left">
								<div class="input-group">
									<div class="input-group-prepend"><span class="input-group-text">Months</span></div>
									<input type="text" class="form-control" id="period_term" placeholder="Months" name="scheme_values[period_term]" value="<?php echo @$value['period_term']; ?>">
								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="col-md-2">
					<div class="kt-form__group--inline">
						<div class="kt-form__control">

							<div class="kt-input-icon kt-input-icon--left">
								<div class="input-group">
									<input type="text" class="form-control" id="interest_rate" placeholder="Interest Rate" name="scheme_values[period_interest_percentage]" value="<?php echo @$value['period_interest_percentage']; ?>">
									<div class="input-group-append"><span class="input-group-text">%</span></div>
								</div>
								<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
							</div>

						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="kt-form__group--inline">
						<div class="kt-form__control">

							<div class="kt-input-icon kt-input-icon--left">
								<div class="input-group">
									<input type="text" class="form-control" id="percentage_rate" placeholder="Percentage Rate" name="scheme_values[period_rate_percentage]" value="<?php echo @$value['period_rate_percentage']; ?>">
									<div class="input-group-append"><span class="input-group-text">%</span></div>
								</div>
								<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
							</div>

						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="kt-form__group--inline">

						<div class="kt-input-icon kt-input-icon--left">
							<div class="input-group">
								<div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
								<input type="text" class="form-control period_rate_amount <?= $value['period_id'] == 2 ? 'dp_input' : '' ?>" id="period_rate_amount<?= $period_id == 2 ? '_dp' : '' ?>" placeholder="Period Amount" name="scheme_values[period_rate_amount]" value="<?php echo @$value['period_rate_amount']; ?>" <?= $value['period_id'] == 2 ? ($scheme_type == 2 || $scheme_type == 3 ? 'disabled' : '') : '' ?>>
							</div>
						</div>

					</div>
				</div>


				<div class="col-md-1">
					<div class="kt-form__group--inline">
						<a href="javascript:;" data-repeater-delete class="btn btn-danger btn-icon">
							<i class="la la-remove"></i>
						</a>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>

<?php else : ?>
	<div data-repeater-list="scheme_values" class="col-lg-12 ">
		<div data-repeater-item class="form-group row align-items-center item-repeater">

			<div class="col-md-1">
				<div class="kt-form__group--inline">
					<div class="kt-form__control">
						<!-- <?php echo Dropdown::get_static('periods', $period_id, 'view'); ?> -->
						<?php echo form_dropdown('scheme_values[period_id]', $periods, @$value['period_id'], 'class="form-control period"'); ?>
						<input type="hidden" id="id" name="scheme_values[id]" value="<?php echo $id; ?>">
						<!-- <input type="hidden" id="period_id" name="scheme_values[period_id]" value="<?php echo $period_id; ?>"> -->
					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="kt-form__group--inline">
					<div class="kt-form__control">

						<div class="kt-input-icon kt-input-icon--left">
							<div class="input-group">
								<div class="input-group-prepend"><span class="input-group-text">Months</span></div>
								<input type="text" class="form-control" id="period_term" placeholder="Months" name="scheme_values[period_term]" value="<?php echo @$value['period_term']; ?>">
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-2">
				<div class="kt-form__group--inline">
					<div class="kt-form__control">

						<div class="kt-input-icon kt-input-icon--left">
							<div class="input-group">
								<input type="text" class="form-control" id="interest_rate" placeholder="Interest Rate" name="scheme_values[period_interest_percentage]" value="<?php echo @$value['period_interest_percentage']; ?>">
								<div class="input-group-append"><span class="input-group-text">%</span></div>
							</div>
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="kt-form__group--inline">
					<div class="kt-form__control">

						<div class="kt-input-icon kt-input-icon--left">
							<div class="input-group">
								<input type="text" class="form-control" id="percentage_rate" placeholder="Percentage Rate" name="scheme_values[period_rate_percentage]" value="<?php echo @$value['period_rate_percentage']; ?>">
								<div class="input-group-append"><span class="input-group-text">%</span></div>
							</div>
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
						</div>

					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="kt-form__group--inline">

					<div class="kt-input-icon kt-input-icon--left">
						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
							<input type="text" class="form-control period_rate_amount" id="period_rate_amount<?= $period_id == 2 ? '_dp' : '' ?>" placeholder="Period Amount" name="scheme_values[period_rate_amount]" value="<?php echo @$value['period_rate_amount']; ?>">
						</div>
					</div>

				</div>
			</div>


			<div class="col-md-1">
				<div class="kt-form__group--inline">
					<a href="javascript:;" data-repeater-delete class="btn btn-danger btn-icon">
						<i class="la la-remove"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>