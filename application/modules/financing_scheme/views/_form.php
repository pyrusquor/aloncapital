<?php
$id				=	isset($_financing_scheme['id']) && $_financing_scheme['id'] ? $_financing_scheme['id'] : '';
$name			=	isset($_financing_scheme['name']) && $_financing_scheme['name'] ? $_financing_scheme['name'] : '';
$description	=	isset($_financing_scheme['description']) && $_financing_scheme['description'] ? $_financing_scheme['description'] : '';
$scheme_type 	= 	isset($_financing_scheme['scheme_type']) && $_financing_scheme['scheme_type'] ?  $_financing_scheme['scheme_type'] : '0';
$category_id 	= 	isset($_financing_scheme['category_id']) && $_financing_scheme['category_id'] ? $_financing_scheme['category_id'] : '';
$category 		= 	isset($_financing_scheme['category']['name']) && $_financing_scheme['category']['name'] ? $_financing_scheme['category']['name'] : 'N/A';
?>

<div class="kt-section kt-section--first">

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Name </label>
				<div class="kt-input-icon">
					<input type="text" name="info[name]" class="form-control" id="name" placeholder="Name" value="<?php echo set_value('name', @$name); ?>" autocomplete="off">
					<!-- <input type="hidden" name="info[id]" class="form-control" id="id" value="<?php //echo set_value('id', @$id);
																									?>"> -->
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('name'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Description</label>
				<div class="kt-input-icon">
					<input type="text" name="info[description]" class="form-control" id="description" placeholder="Description" value="<?php echo set_value('description', @$description); ?>" autocomplete="off">
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('description'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="">Scheme Type <span class="kt-font-danger">*</span></label>
				<?php echo form_dropdown('info[scheme_type]', Dropdown::get_static('scheme_types'), set_value('scheme_type', @$scheme_type), 'class="form-control" id="scheme_type"'); ?>
				<?php echo form_error('info[scheme_type]'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="">Category <span class="kt-font-danger">*</span></label>
				<select class="form-control" name="info[category_id]" value="<?php echo set_value('category_id', @$category_id); ?>" placeholder="Type" autocomplete="off" id="category_id" data-category="<?= $category_id ?>">
					<option>Select option</option>
				</select>
				<?php echo form_error('info[category_id]'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>

	<div class="row">

		<div class="col-sm-12" id="billing_form_repeater">

			<div class="kt-section__content kt-section__content--solid">
				<div class="kt-divider">
					<span></span>
					<span>Billing Information</span>
					<span></span>
				</div>
			</div><br>

			<div class="col-lg-12">
				<div class="form-group row align-items-center">

					<div class="col-md-1">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<label>&nbsp;</label>
							</div>

						</div>
					</div>

					<div class="col-md-2">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">Terms (Months)</label></center>
							</div>

						</div>
					</div>

					<div class="col-md-2">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">Interest</label></center>
							</div>

						</div>
					</div>

					<div class="col-md-3">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">Percentage</label></center>
							</div>

						</div>
					</div>

					<div class="col-md-3">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">Amount</label></center>
							</div>

						</div>
					</div>

					<div class="col-md-1">
						<div class="kt-form__group--inline">
							<div data-repeater-create class="btn btn-sm btn-primary repeater-create">
								<span>
									<i class="la la-plus"></i>
									<span>Add</span>
								</span>
							</div>
						</div>
					</div>

				</div>
			</div>


			<?php $this->load->view('billing_values_form'); ?>

		</div>

	</div>


	<div class="row" style="display: none">

		<div class="col-sm-12">

			<div class="kt-section__content kt-section__content--solid">
				<div class="kt-divider">
					<span></span>
					<span>Commission Information</span>
					<span></span>
				</div>
			</div><br>


			<div data-repeater-list="" class="col-lg-12">
				<div data-repeater-item="" class="form-group row align-items-center">

					<div class="col-md-1">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<label>&nbsp;</label>
							</div>
						</div>
					</div>

					<div class="col-md-2">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">Percentage to Finish</label></center>
							</div>

						</div>
					</div>

					<div class="col-md-2">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">Amount to Release</label></center>
							</div>

						</div>
					</div>

					<div class="col-md-3">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">Percentage to Release</label></center>
							</div>

						</div>
					</div>

					<div class="col-md-2">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">Terms (Months)</label></center>
							</div>

						</div>
					</div>

					<div class="col-md-2">
						<div class="kt-form__group--inline">
							<div class="kt-form__label">
								<center><label class="kt-label m-label--single">WHT</label></center>
							</div>

						</div>
					</div>

				</div>
			</div>


			<?php $this->load->view('commission_values_form'); ?>

		</div>

	</div>


</div>