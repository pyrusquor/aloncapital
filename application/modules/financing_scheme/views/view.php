<?php	
	$_id									=	isset($_financing_scheme['id']) && $_financing_scheme['id'] ? $_financing_scheme['id'] : '';
	# GENERAL INFORMATION
	$name			=	isset($_financing_scheme['name']) && $_financing_scheme['name'] ? $_financing_scheme['name'] : '';
	$description	=	isset($_financing_scheme['description']) && $_financing_scheme['description'] ? $_financing_scheme['description'] : '';
	$scheme_type 	= 	isset($_financing_scheme['scheme_type']) && $_financing_scheme['scheme_type'] ? Dropdown::get_static('scheme_types',$_financing_scheme['scheme_type'],'view') : 'N/A';
	$category_id 	= 	isset($_financing_scheme['category_id']) && $_financing_scheme['category_id'] ? $_financing_scheme['category_id'] : '';
	$category 		= 	isset($_financing_scheme['category']['name']) && $_financing_scheme['category']['name'] ? $_financing_scheme['category']['name'] : 'N/A';
?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Financing Scheme</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('financing_scheme/update/'.$_id);?>" class="btn btn-label-success btn-elevate btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<a href="<?php echo site_url('financing_scheme');?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<!-- General Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
				<div class="card">
					<div class="card-header" id="head_general_information">
						<div class="card-title" data-toggle="collapse" data-target="#general_information" aria-expanded="true" aria-controls="general_information">
							General Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="general_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
						<div class="card-body">
							<div class="kt-form kt-form--label-left">
								<div class="form-group form-group-xsss row">
									<label class="col-5 col-form-label kt-font-primary kt-font-bolder">
										Financing Scheme Name
									</label>
									<div class="col-7">
										<span class="form-control-plaintext kt-font-dark">
											<?php echo $name; ?>
										</span>
									</div>
								</div>
								<div class="form-group form-group-xsss row">
									<label class="col-5 col-form-label kt-font-primary kt-font-bolder">
										Description
									</label>
									<div class="col-7">
										<span class="form-control-plaintext kt-font-dark">
											<?php echo $description; ?>
										</span>
									</div>
								</div>
								<div class="form-group form-group-xsss row">
									<label class="col-5 col-form-label kt-font-primary kt-font-bolder">
										Type
									</label>
									<div class="col-7">
										<span class="form-control-plaintext kt-font-dark">
											<?php echo $scheme_type; ?>
										</span>
									</div>
								</div>
								<div class="form-group form-group-xsss row">
									<label class="col-5 col-form-label kt-font-primary kt-font-bolder">
										Category
									</label>
									<div class="col-7">
										<span class="form-control-plaintext kt-font-dark">
											<?php echo $category; ?>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<!-- Billing Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_billing_information">
				<div class="card">
					<div class="card-header" id="head_billing_information">
						<div class="card-title" data-toggle="collapse" data-target="#billing_information" aria-expanded="true" aria-controls="billing_information">
							Billing Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="billing_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
						<div class="card-body">
							<div data-repeater-list="" class="col-lg-12">
					            <div data-repeater-item="" class="form-group row align-items-center">

					                <div class="col-md-1">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <label>&nbsp;</label>
					                        </div>
					                       
					                    </div>
					                </div>

					                <div class="col-md-2">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">Terms (Months)</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					                <div class="col-md-3">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">Interest</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					                <div class="col-md-3">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">Percentage</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					                <div class="col-md-3">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">Amount</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					            </div>
					        </div>


					        <?php $this->load->view('billing_values'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-12">
		<!-- Billing Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_commission_information">
				<div class="card">
					<div class="card-header" id="head_commission_information">
						<div class="card-title" data-toggle="collapse" data-target="#commission_information" aria-expanded="true" aria-controls="commission_information">
							Commission Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="commission_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_commission_information">
						<div class="card-body">
							<div data-repeater-list="" class="col-lg-12">
					            <div data-repeater-item="" class="form-group row align-items-center">

					                <div class="col-md-1">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <label>&nbsp;</label>
					                        </div>
					                    </div>
					                </div>

					                <div class="col-md-2">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">Percentage to Finish</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					                <div class="col-md-2">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">Amount to Release</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					                <div class="col-md-3">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">Percentage to Release</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					                <div class="col-md-2">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">Terms (Months)</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					                <div class="col-md-2">
					                    <div class="kt-form__group--inline">
					                        <div class="kt-form__label">
					                            <center><label class="kt-label m-label--single">WHT</label></center>
					                        </div>
					                       
					                    </div>
					                </div>

					            </div>
					        </div>

							
					        <?php $this->load->view('commission_values'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>