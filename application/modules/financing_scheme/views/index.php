<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Financing scheme</h3>
			<?php if (isset($_total) && $_total) : ?>

				<span class="kt-subheader__separator kt-subheader__separator--v"></span>
				<span class="kt-subheader__desc" id="_total"><?php echo $_total; ?> TOTAL</span>
			<?php endif; ?>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
				<input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
				<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<!-- <a href="<?php echo site_url('financing_scheme/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
					<i class="fa fa-plus"></i> Create
				</a>
				<button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
					<i class="fa fa-filter"></i> Filter
				</button> -->
				<a href="<?php echo site_url('financing_scheme_categories'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
					Financing Scheme Categories
				</a>
				<a href="<?php echo site_url('commissions'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
					Commission Setup
				</a>
			</div>
		</div>
	</div>
</div>

<div class="module__cta">
	<div class="kt-container  kt-container--fluid ">
		<div class="module__create">
			<a href="<?php echo site_url('financing_scheme/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
				<i class="fa fa-plus"></i> Add Financing Scheme
			</a>
		</div>

		<div class="module__filter">
			<button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true">
				<i class="fa fa-filter"></i> Filter
			</button>
		</div>
	</div>
</div>


<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__body">
		<div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label class="form-control-label">Name</label>
						<div class="kt-input-icon  kt-input-icon--left">
							<input type="text" class="form-control form-control-sm _filter" placeholder="Name" id="_column_2" data-column="2" autocomplete="off">
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="form-control-label">Scheme Type</label>
						<select class="form-control form-control-sm _filter" id="_column_4" data-column="4">
							<option value="">Select Option</option>
							<?php foreach (Dropdown::get_static('scheme_types') as $type) : ?>
								<option><?= $type ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label class="form-control-label">Category</label>
						<div class="kt-input-icon  kt-input-icon--left">
							<select class="form-control form-control-sm suggests _filter" id="_column_6" data-column="6" name="category_id" data-module="financing_scheme_categories">
								<option value="">Select Company</option>
							</select>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="form-control-label">Reservation Amount</label>
						<select class="form-control form-control-sm _filter" id="_column_9" data-column="9">
							<option value="">Select Option</option>
							<?php foreach ($reservation_amounts as $reservation_amount) : ?>
								<option><?= $reservation_amount['period_rate_amount'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-md-4">
					<div class="form-group">
						<label class="form-control-label">Downpayment</label>
						<select class="form-control form-control-sm _filter" id="_column_10" data-column="10">
							<option value="">Select Option</option>
							<?php foreach ($downpayment_percentages as $downpayment_percentage) : ?>
								<option><?= rtrim(rtrim($downpayment_percentage['period_rate_percentage'], 0), '.') ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="form-control-label">Downpayment Terms</label>
						<select class="form-control form-control-sm _filter" id="_column_11" data-column="11">
							<option value="">Select Option</option>
							<?php foreach ($downpayment_terms as $downpayment_term) : ?>
								<option><?= $downpayment_term['period_term'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="form-control-label">Downpayment Interest Rate</label>
						<select class="form-control form-control-sm _filter" id="_column_12" data-column="12">
							<option value="">Select Option</option>
							<?php foreach ($downpayment_interest_percentages as $downpayment_interest_percentage) : ?>
								<option><?= rtrim(rtrim($downpayment_interest_percentage['period_interest_percentage'], 0), '.') ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="form-control-label">Loan</label>
						<select class="form-control form-control-sm _filter" id="_column_13" data-column="13">
							<option value="">Select Option</option>
							<?php foreach ($loan_percentages as $loan_percentage) : ?>
								<option><?= rtrim(rtrim($loan_percentage['period_rate_percentage'], 0), '.') ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="form-control-label">Loan Terms</label>
						<select class="form-control form-control-sm _filter" id="_column_14" data-column="14">
							<option value="">Select Option</option>
							<?php foreach ($loan_terms as $loan_term) : ?>
								<option><?= $loan_term['period_term'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="form-control-label">Loan Interest Rate</label>
						<select class="form-control form-control-sm _filter" id="_column_15" data-column="15">
							<option value="">Select Option</option>
							<?php foreach ($loan_interest_percentages as $loan_interest_percentage) : ?>
								<option><?= rtrim(rtrim($loan_interest_percentage['period_interest_percentage'], 0), '.') ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>

		</div>
		<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
			<form class="kt-form" id="_export_csv" action="<?php echo site_url('financing_scheme/export_csv'); ?>" method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label class="form-control-label">File Type</label>
							<div class="kt-input-icon  kt-input-icon--left">
								<select class="form-control form-control-sm" name="update_existing_data">
									<option value=""> -- Update Existing Data -- </option>
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
								<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
							</div>
							<?php echo form_error('update_existing_data'); ?>
							<span class="form-text text-muted"></span>
						</div>
					</div>
				</div>
			</form>
			<form class="kt-form" id="_upload_form" action="<?php echo site_url('financing_scheme/import'); ?>" method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label class="form-control-label">Upload CSV file:</label>
							<label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
							<input type="file" name="csv_file" class="" size="1000" accept="*.csv">
							<span class="form-text text-muted"></span>
						</div>
					</div>
				</div>
			</form>
			<div class="form-group form-group-last row custom_import_style">
				<div class="col-lg-3">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm" form="_upload_form">
								<i class="fa fa-upload"></i> Upload
							</button>
						</div>
						<div class="col-lg-6 kt-align-right">
							<button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" form="_export_csv">
								<i class="fa fa-file-csv"></i>
							</button>
							<button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
								<i class="fa fa-info-circle"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_financing_scheme_table">
			<thead>
				<tr class="text-center">
					<th width="1%">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<input type="checkbox" value="all" class="m-checkable" id="select-all">
							<span></span>
						</label>
					</th>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Scheme Type</th>
					<th>Category</th>
					<th>Category ID</th>
					<!-- <th>Date Created</th>
					<th>Last Update</th> -->
					<th>Reservation Amount</th>
					<th>Downpayment Percentage</th>
					<th>Downpayment Term</th>
					<th>Downpayment Interest Percentage</th>
					<th>Loan Percentage</th>
					<th>Loan Term</th>
					<th>Loan Interest Percentage</th>
					<th>Created By</th>
					<th>Last Update By</th>
					<th>Actions</th>
				</tr>
			</thead>
		</table>
		<!--end: Datatable -->
	</div>
</div>