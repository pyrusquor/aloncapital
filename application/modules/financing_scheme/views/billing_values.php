<?php 
	$values			=	isset($_financing_scheme['values']) && $_financing_scheme['values'] ? $_financing_scheme['values'] : '';
?>
	        	
<?php if ($values): ?>
	<?php foreach ($values as $key => $value): $period_id = $value['period_id']?>
		<div data-repeater-list="" class="col-lg-12">
		    <div data-repeater-item="" class="form-group row align-items-center">

		        <div class="col-md-1">
		            <div class="kt-form__group--inline">
		                <div class="kt-form__control">
		                    <?php echo Dropdown::get_static('periods', $period_id, 'view'); ?>
		                    <input type="hidden" id="period_id"  name="scheme_values[period_id][]" value="<?php echo $period_id; ?>">
		                </div>
		            </div>
		        </div>

		        <div class="col-md-2">
		            <div class="kt-form__group--inline">
		                <div class="kt-form__control">

		                    <center><?php echo @$value['period_term']; ?> Mo(s)</center>

		                </div>
		            </div>
		        </div>

		        <div class="col-md-3">
		            <div class="kt-form__group--inline">
		                <div class="kt-form__control">

		                    <center><?php echo @$value['period_interest_percentage']; ?>%</center>

		                </div>
		            </div>
		        </div>

		        <div class="col-md-3">
		            <div class="kt-form__group--inline">
		                <div class="kt-form__control">

		                    <center><?php echo @$value['period_rate_percentage']; ?>%</center>

		                </div>
		            </div>
		        </div>

		        <div class="col-md-3">
		            <div class="kt-form__group--inline">
		               
		            
		                  <center><?php echo @money_php($value['period_rate_amount']); ?></center>

		            </div>
		        </div>
		        
		    </div>
		</div>
	<?php endforeach ?>
<?php endif; ?>
