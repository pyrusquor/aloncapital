<?php
	$_id											= isset($_li->id) && $_li->id ? $_li->id : '';
	$_financing_scheme_owner_name					= isset($_li->financing_scheme_owner_name) && $_li->financing_scheme_owner_name ? $_li->financing_scheme_owner_name : '';
	$_title										= isset($_li->title) && $_li->title ? $_li->title : '';
	$_lot_number							= isset($_li->lot_number) && $_li->lot_number ? $_li->lot_number : '';
	$_location								= isset($_li->location) && $_li->location ? $_li->location : '';
	$_financing_scheme_area								= isset($_li->financing_scheme_area) && $_li->financing_scheme_area ? $_li->financing_scheme_area : '';
	$_financing_scheme_classification_id	= isset($_li->financing_scheme_classification_id) && $_li->financing_scheme_classification_id ? $_li->financing_scheme_classification_id : '';
	$_status									= isset($_li->status) && $_li->status ? $_li->status : '';
	$_updated_by							= isset($_li->updated_by) && $_li->updated_by ? $_li->updated_by : '';
	$_updated_at							= isset($_li->updated_at) && $_li->updated_at ? $_li->updated_at : '';
?>

<div class="kt-section kt-section--first">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Financing_scheme Owner Name <code>*</code></label>
				<div class="kt-input-icon">
					<input type="text" name="financing_scheme_owner_name" class="form-control" id="financing_scheme_owner_name" placeholder="Financing_scheme Owner Name" value="<?php echo set_value('financing_scheme_owner_name', @$_financing_scheme_owner_name);?>" autocomplete="off">
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('financing_scheme_owner_name'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Financing_scheme Classification <code>*</code></label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('financing_scheme_classification_id', Dropdown::get_static('financing_scheme_classification'), set_value('financing_scheme_classification_id', @$_financing_scheme_classification_id), 'class="form-control" id="financing_scheme_classification_id"'); ?>
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('financing_scheme_classification_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Location <code>(Full Address) *</code></label>
				<div class="kt-input-icon">
					<input type="text" name="location" class="form-control" id="location" placeholder="Full Address" value="<?php echo set_value('location', @$_location);?>" autocomplete="off">
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('location'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Company</label>
				<div class="kt-input-icon">
					<input type="text" name="company" class="form-control" placeholder="Company" value="<?php echo set_value('company', @$_company);?>" autocomplete="off">
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('company'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Lot Number</label>
				<div class="kt-input-icon">
					<input type="text" name="lot_number" class="form-control" placeholder="Lot Number" value="<?php echo set_value('lot_number', @$_lot_number);?>" autocomplete="off">
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('lot_number'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Status <code>*</code></label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('status', Dropdown::get_static('financing_scheme_status'), set_value('status', @$_status), 'class="form-control" id="status"'); ?>
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('status'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Financing_scheme Area <code>*</code></label>
				<div class="kt-input-icon">
					<div class="input-group">
						<input type="text" name="financing_scheme_area" class="form-control" placeholder="Financing_scheme Area" value="<?php echo set_value('financing_scheme_area', @$_financing_scheme_area);?>" autocomplete="off">
						<div class="input-group-append"><span class="input-group-text">SQM</span></div>
					</div>
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('financing_scheme_area'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Remarks</label>
				<div class="kt-input-icon">
					<input type="text" name="remarks" class="form-control" placeholder="Remarks" value="<?php echo set_value('remarks', @$_remarks);?>" autocomplete="off">
					<span class="kt-input-icon__icon"></span>
				</div>
				<?php echo form_error('remarks'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
</div>