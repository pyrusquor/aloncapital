<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Update Financing Scheme</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="_financing_scheme_form">
					<i class="fa fa-sync"></i> Update
				</button>
				<a href="<?php echo site_url('financing_scheme');?>" class="btn btn-label-instagram btn-elevate btn-sm">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<!--begin::Portlet-->
<div class="kt-portlet">

	<!--begin::Form-->
	<form class="kt-form" id="_financing_scheme_form" method="POST">
		<div class="kt-portlet__body">
			<div class="kt-section kt-section--first">
				<div class="kt-section__body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group form-group-last kt-hide">
								<div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
									<div class="alert-icon"><i class="flaticon-warning"></i></div>
									<div class="alert-text">
										Oh snap! Change a few things up and try submitting again.
									</div>
									<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<?php echo $this->load->view('_form');?>
				</div>
			</div>
		</div>
	</form>
	<!--end::Form-->
</div>
<!--end::Portlet-->