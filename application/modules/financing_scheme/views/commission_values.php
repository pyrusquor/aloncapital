<?php 
	$commission_values			=	isset($_financing_scheme['commission_values']) && $_financing_scheme['commission_values'] ? $_financing_scheme['commission_values'] : '';
?>


<?php if ($commission_values): ?>
	<?php foreach ($commission_values as $key => $commission_value): $period_id = $commission_value['period_id']?>
		<div data-repeater-list="" class="col-lg-12">
            <div data-repeater-item="" class="form-group row align-items-center">

                <div class="col-md-1">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">
                            <?php //echo Dropdown::get_static('periods', $period_id, 'view'); ?>
                            <input type="hidden" id="period_id"  name="commission_values[period_id][<?=$period_id;?>]" value="<?php echo $period_id; ?>">
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">
                            <center><?php echo @$commission_value['percentage_to_finish']; ?>%</center>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">

                           <center><?php echo money_php($commission_value['commission_rate_amount']); ?></center>

                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">


                           <center><?php echo ($commission_value['commission_rate_percentage']); ?>%</center>


                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__control">


                           <center><?php echo ($commission_value['commission_term']); ?> Mo(s)</center>

                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                       

                        <center><?php echo ($commission_value['commission_vat']); ?>%</center>

                    </div>
                </div>
                
            </div>
        </div>
	<?php endforeach ?>
<?php endif ?>