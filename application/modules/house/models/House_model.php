<?php
defined('BASEPATH') or exit('No direct script access allowed');
class House_model extends MY_Model
{
	public $table = 'house_models'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'project_id',
		// 'property_type_id',
		'sub_type_id',
		'house_template_id',
		'code',
		'name',
		'no_bedroom',
		'no_bathroom',
		'no_floor',
		'no_ac',
		'color',
		'floor_area',
		'lot_area',
		'reservation_fee',
		'furnishing_fee',
		'is_lot',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
		'deleted_by'
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [
		'project_id' => array(
			'field' => 'project_id',
			'label' => 'Project ID',
			'rules' => 'trim|required'
		),
		'code' => array(
			'field' => 'code',
			'label' => 'Code',
			'rules' => 'trim|required'
		),
		'sub_type_id' => array(
			'field' => 'sub_type_id',
			'label' => 'Sub-Type',
			'rules' => 'trim'
		),
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required'
		),
		'no_bedroom' => array(
			'field' => 'no_bedroom',
			'label' => 'No of Bedroom',
			'rules' => 'trim'
		),
		'no_bathroom' => array(
			'field' => 'no_bathroom',
			'label' => 'No of Bathroom',
			'rules' => 'trim'
		),
		'no_floor' => array(
			'field' => 'no_floor',
			'label' => 'No of Floor',
			'rules' => 'trim'
		),
		'no_ac' => array(
			'field' => 'no_ac',
			'label' => 'No of AC',
			'rules' => 'trim'
		),
		'color' => array(
			'field' => 'color',
			'label' => 'Color',
			'rules' => 'trim'
		),
		'floor_area' => array(
			'field' => 'floor_area',
			'label' => 'Floor Area',
			'rules' => 'trim'
		),
		'lot_area' => array(
			'field' => 'lot_area',
			'label' => 'Lot Area',
			'rules' => 'trim'
		),
		'actual_image' => array(
			'field' => 'actual_image',
			'label' => 'Actual Image',
			'rules' => 'trim'
		),
		'floor_image' => array(
			'field' => 'floor_image',
			'label' => 'Floor Image',
			'rules' => 'trim'
		),
		'reservation_fee' => array(
			'field' => 'reservation_fee',
			'label' => 'Reservation Fee',
			'rules' => 'trim'
		)
	];

	public $delete_fields = [
		'deleted_by' => array(
			'field' => 'deleted_by',
			'label' => 'Deleted By',
			'rules' => 'trim'
		)
	];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		$this->has_one['project'] = array('foreign_model' => 'project/Project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');

		$this->has_many['interior'] = array('foreign_model' => 'Interior_model', 'foreign_table' => 'house_model_interiors', 'foreign_key' => 'house_model_id', 'local_key' => 'id');
		$this->has_many['actual'] = array('foreign_model' => 'House_actual_image_model', 'foreign_table' => 'house_actual_images', 'foreign_key' => 'house_model_id', 'local_key' => 'id');
		$this->has_many['floor'] = array('foreign_model' => 'House_floor_image_model', 'foreign_table' => 'house_floor_images', 'foreign_key' => 'house_model_id', 'local_key' => 'id');
	}

	public function get_name($house_model_id = '')
	{
		$this->db->select("name");
		$this->db->where('id', $house_model_id);
		$model = $this->db->get('house_models')->result_array();
		return $model[0]['name'];
	}
}
