<?php
defined('BASEPATH') or exit('No direct script access allowed');

class House extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('house/House_model', 'M_house');
		$this->load->model('house/House_actual_image_model', 'M_house_actual_image');
		$this->load->model('house/House_floor_image_model', 'M_house_floor_image');
		$this->load->model('project/Project_model', 'M_project');
		$this->load->model('house_templates/House_template_model', 'M_house_template');

		// Load pagination library 
		$this->load->library('ajax_pagination');

		$this->load->helper('format_helper');

		// Per page limit 
		$this->perPage = 12;

		$this->_table_fillables		=	$this->M_house->fillable;
		$this->_table_columns		=	$this->M_house->__get_columns();
	}

	public function index()
	{
		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns']		=	$this->__get_columns($_fills);

		$this->view_data['projects'] = $this->M_project->as_array()->get_all();

		// Get record count 
		// $conditions['returnType'] = 'count'; 
		$this->view_data['totalRec'] = $totalRec = $this->M_house->count_rows();

		// Pagination configuration 
		$config['target']      = '#houseContent';
		$config['base_url']    = base_url('house/paginationData');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'HousePagination';

		// Initialize pagination library 
		$this->ajax_pagination->initialize($config);

		// Get records 
		$this->view_data['records'] = $this->M_house->with_project('fields: name')
			->limit($this->perPage, 0)
			->get_all();

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function paginationData()
	{
		if ($this->input->is_ajax_request()) {

			// Input from General Search
			$keyword = $this->input->post('keyword');

			// Input from Advanced Filter
			$project = $this->input->post('project');
			$code = $this->input->post('code');
			$name = $this->input->post('name');
			$floor = $this->input->post('floor');
			$lot = $this->input->post('lot');

			$page = $this->input->post('page');

			if (!$page) {
				$offset = 0;
			} else {
				$offset = $page;
			}

			$totalRec = $this->M_house->count_rows();

			// Pagination configuration 
			$config['target']      = '#houseContent';
			$config['base_url']    = base_url('house/paginationData');
			$config['total_rows']  = $totalRec;
			$config['per_page']    = $this->perPage;
			$config['link_func']   = 'HousePagination';

			// Query
			if (!empty($keyword)) :

				$this->db->group_start();
				$this->db->like('name', $keyword, 'both');
				$this->db->or_like('code', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (!empty($name)) :

				$this->db->group_start();
				$this->db->like('name', $name, 'both');
				$this->db->group_end();
			endif;

			if (!empty($code)) :

				$this->db->group_start();
				$this->db->like('code', $code, 'both');
				$this->db->group_end();
			endif;

			if (!empty($project)) :

				$this->db->where('project_id', $project);
			endif;

			if (!empty($floor)) :

				$this->db->where('floor_area', $project);
			endif;

			if (!empty($lot)) :

				$this->db->where('lot_area', $project);
			endif;


			$totalRec = $this->M_house->count_rows();

			// Pagination configuration 
			$config['total_rows']  = $totalRec;

			// Initialize pagination library 
			$this->ajax_pagination->initialize($config);

			// Query
			if (!empty($keyword)) :

				$this->db->group_start();
				$this->db->like('name', $keyword, 'both');
				$this->db->or_like('code', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (!empty($name)) :

				$this->db->group_start();
				$this->db->like('name', $name, 'both');
				$this->db->group_end();
			endif;

			if (!empty($code)) :

				$this->db->group_start();
				$this->db->like('code', $code, 'both');
				$this->db->group_end();
			endif;

			if (!empty($project)) :

				$this->db->where('project_id', $project);
			endif;

			if (!empty($floor)) :

				$this->db->where('floor_area', $floor);
			endif;

			if (!empty($lot)) :

				$this->db->where('lot_area', $lot);
			endif;

			$this->view_data['records'] = $this->M_house
				->with_project('fields: name')
				->limit($this->perPage, $offset)
				->get_all();


			$this->load->view('house/_filter', $this->view_data, false);
		}
	}

	public function view($id = FALSE)
	{
		if ($id) {
			$this->load->helper('images');

			$this->view_data['house'] = $this->M_house->with_project('fields:name')->with_actual('fields:filename')->with_floor('fields:filename')->as_array()->get($id);

			if ($this->view_data['house']) {

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function create()
	{

		if ($this->input->is_ajax_request()) {
			$response['status']	= 0;
			$response['message'] = 'Something went wrong! Try again.';

			$data = $this->input->post();

			if ($data) {

				$additional = [
					'created_by' => $this->user->id,
					'created_at' => NOW
				];

				$result = $this->M_house->insert($data + $additional);
				// $this->db->last_query(); exit;

				if ($result !== FALSE) {
					// last ID inserted
					$id = $result;

					unset($data);

					// Actual image upload
					if (!empty($_FILES['actual']['name'])) {

						$actualCount = count($_FILES['actual']['name']);

						for ($i = 0; $i < $actualCount; $i++) {

							$_FILES['uploadFile']['name'] = str_replace(",", "_", $_FILES['actual']['name'][$i]);
							$_FILES['uploadFile']['type'] = $_FILES['actual']['type'][$i];
							$_FILES['uploadFile']['tmp_name'] = $_FILES['actual']['tmp_name'][$i];
							$_FILES['uploadFile']['error'] = $_FILES['actual']['error'][$i];
							$_FILES['uploadFile']['size'] = $_FILES['actual']['size'][$i];


							$config['upload_path'] = './assets/uploads/house_models/actual';
							$config['allowed_types'] = 'gif|jpg|png';
							$config['max_size'] = 5000;
							$config['encrypt_name'] = TRUE;

							$this->load->library('upload', $config, 'actual_upload');
							$this->actual_upload->initialize($config);

							if ($this->actual_upload->do_upload('uploadFile')) {
								$fileData = $this->actual_upload->data();
								$uploadActualImg[$i]['house_model_id'] = $id;
								$uploadActualImg[$i]['filename'] = $fileData['file_name'];
							}
						}

						if (!empty($uploadActualImg)) {
							// Insert Actual Img
							$this->M_house_actual_image->insert($uploadActualImg);
						}
					}


					unset($data);

					// Floor image upload
					if (!empty($_FILES['floor']['name'])) {

						$floorCount = count($_FILES['floor']['name']);

						for ($i = 0; $i < $floorCount; $i++) {

							$_FILES['uploadFile']['name'] = str_replace(",", "_", $_FILES['floor']['name'][$i]);
							$_FILES['uploadFile']['type'] = $_FILES['floor']['type'][$i];
							$_FILES['uploadFile']['tmp_name'] = $_FILES['floor']['tmp_name'][$i];
							$_FILES['uploadFile']['error'] = $_FILES['floor']['error'][$i];
							$_FILES['uploadFile']['size'] = $_FILES['floor']['size'][$i];


							$config['upload_path'] = './assets/uploads/house_models/floor';
							$config['allowed_types'] = 'gif|jpg|png';
							$config['max_size'] = 5000;
							$config['encrypt_name'] = TRUE;

							$this->load->library('upload', $config, 'floor_upload');
							$this->floor_upload->initialize($config);

							if ($this->floor_upload->do_upload('uploadFile')) {
								$fileData = $this->floor_upload->data();
								$uploadFloorImg[$i]['house_model_id'] = $id;
								$uploadFloorImg[$i]['filename'] = $fileData['file_name'];
							}
						}

						if (!empty($uploadFloorImg)) {
							// Insert Floor Img
							$this->M_house_floor_image->insert($uploadFloorImg);
						}
					}

					$response['status']	= 1;
					$response['message'] = 'House successfully created';
				}
			}

			echo json_encode($response);
			exit();
		}

		$this->view_data['project'] = $this->M_project->as_array()->get_all();
		$this->view_data['house_templates'] = $this->M_house_template->as_array()->get_all();

		if ($this->view_data['project']) {
			$this->template->build('create', $this->view_data);
		} else {
			show_404();
		}
	}

	public function update($id = FALSE)
	{

		// $this->js_loader->queue("js/demo1/pages/crud/forms/widgets/dropzone.js");

		if ($id) {
			$this->view_data['house'] = $house = $this->M_house->as_array()->get($id);
			$this->view_data['project'] = $this->M_project->as_array()->get_all();
			$this->view_data['house_templates'] = $this->M_house_template->as_array()->get_all();

			if ($house) {
				if ($this->input->is_ajax_request()) {
					$response['status']	= 0;
					$response['message'] = 'Something went wrong! Try again.';

					$data = $this->input->post();

					if ($data) {
						$additional_fields = array(
							'updated_by' => $this->user->id,
							'updated_at' => NOW
						);

						$result = $this->M_house->update($data + $additional_fields, $house['id']);

						if ($result !== FALSE) {

							$folders = ['actual', 'floor'];

							// Unlink OLD images
							foreach ($folders as $folder) :

								if (!empty($_FILES[$folder])) {
									$path = 'assets/uploads/house_models/' . $folder;
									$model = 'M_house_' . $folder . '_image';
									$getIMGs = $this->$model->where('house_model_id', $house['id'])->get_all();

									if (!empty($getIMGs)) {

										foreach ($getIMGs as $akey => $a) :

											if (is_readable($path . '/' . $a['filename'])) {
												unlink($path . '/' . $a['filename']);
											}

										endforeach;
									}
									// force Delete old filename in database
									$this->$model->delete(array('house_model_id' => $house['id']));

									// Add NEW IMAGE
									if (!empty($_FILES[$folder]['name'])) {

										$floorCount = count($_FILES[$folder]['name']);

										for ($i = 0; $i < $floorCount; $i++) {

											$_FILES['uploadFile']['name'] = str_replace(",", "_", $_FILES[$folder]['name'][$i]);
											$_FILES['uploadFile']['type'] = $_FILES[$folder]['type'][$i];
											$_FILES['uploadFile']['tmp_name'] = $_FILES[$folder]['tmp_name'][$i];
											$_FILES['uploadFile']['error'] = $_FILES[$folder]['error'][$i];
											$_FILES['uploadFile']['size'] = $_FILES[$folder]['size'][$i];


											$config['upload_path'] = './assets/uploads/house_models/' . $folder;
											$config['allowed_types'] = 'gif|jpg|png';
											$config['max_size'] = 5000;
											$config['encrypt_name'] = TRUE;

											$this->load->library('upload', $config);
											$this->upload->initialize($config);

											if ($this->upload->do_upload('uploadFile')) {
												$fileData = $this->upload->data();
												$uploadImg[$i]['house_model_id'] = $house['id'];
												$uploadImg[$i]['filename'] = $fileData['file_name'];
											}
										}

										if (!empty($uploadImg)) {
											// Insert  Img
											$this->$model->insert($uploadImg);
										}
									}
								}

							endforeach;

							$response['status']	= 1;
							$response['message'] = 'Successfully Updated Property Model';
						}
					}

					echo json_encode($response);
					exit();
				}

				$this->template->build('update', $this->view_data);
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}

	public function delete($id = FALSE)
	{
		$id = $this->input->post('id');

		$result = (bool)$this->M_house->delete($id);
		if ($result) {
			$fields = array(
				'deleted_by' => $this->session->userdata('user_id')
			);

			$this->M_house->rules['update'] = $this->M_house->delete_fields;

			$project_result = $this->M_house->update($fields, $id);

			$response['status']  = TRUE;
			$response['message'] = 'Property Model Deleted Successfully ...';
		} else {

			$response['status']  = FALSE;
			$response['message'] = 'Unable to delete Project ...';
		}

		echo json_encode($response);
	}

	public function bulkDelete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_house->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Property Model successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function get_image($folder = '', $id = FALSE)
	{
		if ($this->input->is_ajax_request()) {

			$response = FALSE;
			$path = 'assets/uploads/house_models/' . $folder;
			$model = 'M_house_' . $folder . '_image';
			if ($id) {
				$response = [];
				$result = $this->$model->where('house_model_id', $id)->get_all();
				if (!empty($result)) {

					foreach ($result as $key => $value) {

						if (file_exists($path . '/' . $value['filename'])) {

							$obj['id'] = $value['id'];
							$obj['name'] = $value['filename'];
							$obj['size'] = filesize($path . '/' . $value['filename']);
							$obj['path'] = base_url($path . '/' . $value['filename']);

							$response[] = $obj;
						}
					}
				}
			} else {
				$response = FALSE;
			}
			echo json_encode($response);
			exit();
		}
	}

	public function remove_pic($folder = FALSE)
	{
		if ($this->input->is_ajax_request()) {
			if ($folder) {
				$response['status']	= 0;
				$response['msg'] = 'Oops! Please refresh the page and try again.';

				$fileName = $this->input->post('fileName');
				$imgID = $this->input->post('img_id');

				$path = 'assets/uploads/house_models/' . $folder;
				$model = 'M_house_' . $folder . '_image';

				// unlink images
				if (is_readable($path . '/' . $fileName)) {
					unlink($path . '/' . $fileName);
				}

				$get_image = $this->$model->get($imgID);

				if ($get_image) {
					$deleteImg = $this->$model->delete(array('id' => $imgID));

					if ($deleteImg !== FALSE) {
						$response['status']	= 1;
						$response['msg']	= ucwords($folder) . ' image successfully removed.';
					}
				}

				echo json_encode($response);
				exit(0);
			}
		} else {
			show_404();
		}
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];

		$_titles[]	=	'#';
		// $_titles[]	=	'ID';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		$records = $this->M_house->with_project('fields: name')->get_all();
		if ($records) {

			foreach ($records as $_dkey => $_hm) {

				$_datas[$_hm['id']]['#']	=	$_no;

				$_no++;
			}

			$_filename	=	'list_of_house_models_' . date('m_d_y_h-i-s', time()) . '.xls';

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				if ($_export_column) {

					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project');
				}
			} else {

				$_filename	=	'list_of_house_models_' . date('m_d_y_h-i-s', time()) . '.csv';

				// $_db_columns	=	$this->M_document->fillable;
				$_db_columns	=	$this->_table_fillables;
				if (!$_db_columns) {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

						if ((strpos($_name, '_id') !== FALSE)) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($records as $_dkey => $_hm) {

								if ($_column === 'project_id') {

									$_datas[$_hm['id']][$_title]	=	isset($_hm[$_column]) && $_hm[$_column] ? Dropdown::get_dynamic('projects', $_hm[$_column], 'name', 'id', 'view') : '';
								} else {

									$_datas[$_hm['id']][$_title]	=	isset($_hm[$_column]) && $_hm[$_column] ? $_hm[$_column] : '';
								}
							}
						} elseif ((strpos($_name, 'is_') !== FALSE)) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($records as $_dkey => $_hm) {

								$_datas[$_hm['id']][$_title]	=	isset($_hm[$_column]) && ($_hm[$_column] !== '') ? Dropdown::get_static('bool', $_hm[$_column], 'view') : '';
							}
						} else {

							$_column	=	$_name;

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($records as $_dkey => $_hm) {

								$_datas[$_hm['id']][$_title]	=	isset($_hm[$_name]) && $_hm[$_name] ? $_hm[$_name] : '';
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_firstAlpha	=	reset($_alphas);
				$_lastAlpha		=	end($_alphas);

				foreach ($_xls_columns as $_xkey => $_column) {

					$_title	=	($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

					$_objSheet->setCellValue($_xkey . $_row, $_title);
				}

				$_objSheet->setTitle('List of Property Models');
				$_objSheet->setCellValue('A1', 'LIST OF PROPERTY MODELS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}

				$_style	=	array(
					'font'  => array(
						'bold'	=>	TRUE,
						'size'	=>	10,
						'name'	=>	'Verdana'
					)
				);
				$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project');
			}
		} else {

			$this->notify->error('No Record Found', 'project');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Property Models CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'house');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_house->as_array()->get_all();
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Property Models');
			$_objSheet->setCellValue('A1', 'HOUSE MODELS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'house');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {

							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							if ($_id) {

								$data	=	$this->M_house->get($_id);
								if ($data) {

									unset($_data['id']);

									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_update	=	$this->M_house->update($_data, $_id);
									if ($_update !== FALSE) {

										$_updated++;
									} else {

										$_failed++;

										break;
									}
								} else {

									$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['created_at']	=	NOW;
									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_insert	=	$this->M_house->insert($_data);
									if ($_insert !== FALSE) {

										$_inserted++;
									} else {

										$_failed++;

										break;
									}
								}
							} else {

								$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['created_at']	=	NOW;
								$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['updated_at']	=	NOW;

								$_insert	=	$this->M_house->insert($_data);
								if ($_insert !== FALSE) {

									$_inserted++;
								} else {

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {

							$this->notify->error('Upload Failed! Please follow upload guide.', 'house');
						} else {

							$this->notify->success($_msg . '.', 'house');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'house');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'house');
			}
		} else {

			$this->notify->error('Something went wrong!', 'house');
		}
	}
}
