<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Property Models</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc"><?php echo (!empty($totalRec)) ? $totalRec : 0; ?> TOTAL</span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search models..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
                <?php if ($this->ion_auth->is_admin()): ?>
                    <!-- <a href="<?php echo site_url('house/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                        <i class="fa fa-plus"></i> Create
                    </a>
                    <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                        <i class="fa fa-filter"></i> Filter
                    </button> -->
                    <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
                        <i class="fa fa-upload"></i> Import
                    </button>
                    <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#_export_option">
                        <i class="fa fa-download"></i> Export
                    </button>
                    <button type="button" class="btn btn-label-primary btn-elevate btn-sm" id="bulkDelete" disabled>
                        <i class="fa fa-trash"></i> Delete Selected
                    </button>
                <?php endif;?>
			</div>
		</div>
	</div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
                <a href="<?php echo site_url('house/create'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Create Property Model
                </a>             
        </div>

        <div class="module__filter">
                <button class="btn btn-label-primary btn-elevate btn-sm btn-filter"
                    data-toggle="modal" aria-expanded="true" data-target="#filterModal">
                    <i class="fa fa-filter"></i> Filter
                </button>
        </div>
    </div>
</div>

<!-- Batch Upload -->
<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <form class="kt-form" id="_export_csv" action="<?php echo site_url('house/export_csv'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">File Type</label>
                            <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control form-control-sm" name="update_existing_data">
                                    <option value=""> -- Update Existing Data -- </option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
                            </div>
                            <?php echo form_error('update_existing_data'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <form class="kt-form" id="_upload_form" action="<?php echo site_url('house/import'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Upload CSV file:</label>
                            <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
                            <input type="file" name="csv_file" class="" size="1000" accept="*.csv">
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group form-group-last row custom_import_style">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm" form="_upload_form">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                        <div class="col-lg-6 kt-align-right">
                            <!-- <a href="<?php echo site_url('document/export') ?>" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm">
								<i class="fa fa-file-csv"></i>
							</a> -->
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" form="_export_csv">
                                <i class="fa fa-file-csv"></i>
                            </button>
                            <button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Batch Upload Guide -->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="upload_guide_label">Property Model Upload Guide</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<table class="table table-head-bg-brand table-striped table-hover table-bordered table-condensed table-checkable" id="_upload_guide_modal">
					<thead>
						<tr class="text-center">
							<th width="1%" scope="col">#</th>
							<th scope="col">Name</th>
							<th scope="col">Type</th>
							<th scope="col">Format</th>
							<th scope="col">Option</th>
							<th scope="col">Required</th>
						</tr>
					</thead>
					<?php if (isset($_fillables) && $_fillables): ?>

                        <tbody>
							<?php foreach ($_fillables as $_fkey => $_fill): ?>

								<?php
$_no = isset($_fill['no']) && $_fill['no'] ? $_fill['no'] : '';
$_name = isset($_fill['name']) && $_fill['name'] ? $_fill['name'] : '';
$_type = isset($_fill['type']) && $_fill['type'] ? $_fill['type'] : '';
$_required = isset($_fill['required']) && $_fill['required'] ? $_fill['required'] : '';
$_dropdown = isset($_fill['dropdown']) && $_fill['dropdown'] && !empty($_fill['dropdown']) ? $_fill['dropdown'] : '';
?>

								<tr>
									<td><?php echo @$_no; ?></td>
									<td class="_ug_name"><?php echo @$_name; ?></td>
									<td><?php echo @$_type; ?></td>
									<td>
                                        <?php if ($_name === 'birth_date'): ?>
                                            yyyy-mm-dd (e.g. 2020-01-28)
                                        <?php endif;?>
                                    </td>
									<td class="_ug_option">
										<?php if ($_dropdown): ?>

											<ul class="_ul">
												<?php foreach ($_dropdown as $_dkey => $_drop): ?>

													<li><?php echo @$_drop; ?></li>
												<?php endforeach;?>
											</ul>
										<?php endif;?>
										<!-- <ul class="_ul">
									    <li>1 = Legal Staff</li>
											<li>2 = Legal Head</li>
											<li>3 = Agent</li>
									   </ul> -->
									</td>
									<td>
										<?php if ($_required === 'Yes'): ?>

											<span class="kt-font-danger">Yes</span>
										<?php elseif ($_required === 'No'): ?>

											No
										<?php endif;?>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					<?php endif;?>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm btn-icon-sm" data-dismiss="modal">
					<i class="la la-close"></i> Close
				</button>
			</div>
		</div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="houseContent">
<?php if (!empty($records)): ?>
    <div class="row" id="houseList">
		<?php foreach ($records as $r): ?>
			<div class="col-md-4 col-xl-3">
				<div class="kt-portlet">
					<div class="kt-portlet__head kt-portlet__head--noborder">
						<div class="kt-portlet__head-label">
							<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
								<input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>" class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
								<span></span>
							</label>
						</div>
						<div class="kt-portlet__head-toolbar">
							<a href="#" class="btn btn-icon" data-toggle="dropdown">
								<i class="flaticon-more-1 kt-font-brand"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<ul class="kt-nav">
                                    <?php if ($this->ion_auth->is_admin()): ?>
                                        <li class="kt-nav__item">
                                            <a href="<?php echo base_url('house/view/' . $r['id']); ?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                <span class="kt-nav__link-text">View</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="<?php echo base_url('house/update/' . $r['id']); ?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                <span class="kt-nav__link-text">Update</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="javascript: void(0)" class="kt-nav__link remove_house" data-id="<?php echo $r['id']; ?>">
                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                <span class="kt-nav__link-text">Delete</span>
                                            </a>
                                        </li>
                                    <?php else: ?>
                                        <li class="kt-nav__item">
                                            <a href="<?php echo base_url('house/view/' . $r['id']); ?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                <span class="kt-nav__link-text">View</span>
                                            </a>
                                        </li>
                                    <?php endif;?>
								</ul>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body cards__custom" style="padding: 5px 20px 15px;">
						<!--begin::Widget -->
						<div class="kt-widget kt-widget--user-profile-4" style="margin-top: -40px;">
							<div class="kt-widget__head card__head">
								<div class="kt-widget__media">
									<img class="kt-widget__img kt-hidden" src="<?php echo base_url('assets/media/users/300_21.jpg'); ?>" alt="image">
									<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-" style="width: 150px; height: 150px;">
										<h1><?php echo get_initials($r['name']); ?></h1>
									</div>
								</div>
								<div class="kt-widget__content">
									<div class="kt-widget__section" style="text-align: center;">
										<a href="<?php echo base_url('house/view/' . $r['id']); ?>" class="kt-widget__username"><h2><?php echo $r['id'] . '. ' . ucwords($r['name']); ?></h2></a>
										<span class="kt-widget__desc"><h5><?php echo ucwords($r['code']); ?></h5></span>
										<br />
										<br />
										<div class="kt-widget__text kt-align-left" style="float: left;">Project</div>
										<div class="kt-widget__text kt-align-right" style="font-weight: 600;"><?php echo $r['project']['name']; ?></div>
										<div class="kt-widget__text kt-align-left" style="float: left;">Reservation Fee</div>

										<div class="kt-widget__text kt-align-right" style="font-weight: 600;"><?php echo $r['reservation_fee']; ?></div>

										<div class="kt-widget__button">
											<a href="<?php echo base_url('house/view/' . $r['id']); ?>" class="btn btn-brand btn-upper">View</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--end::Widget -->
					</div>
				</div>
			</div>

		<?php endforeach;?>
	</div>
    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="<?php echo base_url('house/create'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="filter_project" class="form-control-label">Project:</label>
                            <select class="form-control kt-select2" id="filter_project_id" name="filter_project_id">
								<option value="">Choose project...</option>
								<?php foreach ($projects as $p): ?>
									<option value="<?php echo $p['id'] ?>" <?php echo ((isset($filter['filter_project_id'])) ? $filter['filter_project_id'] : '' == $p['id']) ? 'selected' : '' ?>><?php echo ucwords($p['name']); ?></option>
								<?php endforeach;?>
							</select>
                        </div>
						<div class="form-group col-md-6">
                            <label for="filter_code" class="form-control-label">Model Code:</label>
                            <input type="text" class="form-control" placeholder="Model Code" id="filter_code" name="filter_code" value="<?php echo set_value('filter_code', (isset($filter['filter_code'])) ? $filter['filter_code'] : ''); ?>">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="filter_name" class="form-control-label">Model Name:</label>
                            <input type="text" class="form-control" placeholder="Model Name" id="filter_name" name="filter_name" value="<?php echo set_value('filter_name', (isset($filter['filter_name'])) ? $filter['filter_name'] : ''); ?>">
                        </div>
						<div class="form-group col-md-6">
                            <label for="message-text" class="form-control-label">Floor Area:</label>
                            <input type="text" class="form-control" placeholder="Floor Area" id="filter_floor_area" name="filter_floor_area" value="<?php echo set_value('filter_floor_area', (isset($filter['filter_floor_area'])) ? $filter['filter_floor_area'] : ''); ?>">
                        </div>
						<div class="form-group col-md-6">
                            <label for="message-text" class="form-control-label">Lot Area:</label>
                            <input type="text" class="form-control" placeholder="Lot Area" id="filter_lot_area" name="filter_lot_area" value="<?php echo set_value('filter_lot_area', (isset($filter['filter_lot_area'])) ? $filter['filter_lot_area'] : ''); ?>">
                        </div>
                    </div>
                </form>
            </div>
			<div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- EXPORT -->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form" target="_blank" action="<?php echo site_url('house/export'); ?>" method="POST">
                    <div class="row">
                        <?php if (isset($_columns) && $_columns): ?>

                            <div class="col-lg-11 offset-lg-1">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" id="_export_select_all"> Field
                                        <span></span>
                                    </label>
                                    <label class="kt-checkbox kt-checkbox--bold"></label>
                                </div>
                            </div>

                            <?php foreach ($_columns as $key => $_column): ?>

                                <?php if ($_column): ?>

                                    <?php
$_offset = '';
if ($_column === reset($_columns)) {

    $_offset = 'offset-lg-1';
}
?>

                                    <div class="col-lg-5 <?php echo isset($_offset) && $_offset ? $_offset : ''; ?>">
                                        <div class="kt-checkbox-list">
                                            <?php foreach ($_column as $_ckey => $_clm): ?>

                                                <?php
$_label = isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
$_value = isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
?>

                                                <label class="kt-checkbox kt-checkbox--bold">
                                                    <input type="checkbox" name="_export_column[]" class="_export_column" value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
                                                    <span></span>
                                                </label>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                <?php endif;?>
                            <?php endforeach;?>

                        <?php else: ?>

                            <div class="col-lg-10 offset-lg-1">
                                <div class="form-group form-group-last">
                                    <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            Something went wrong. Please contact your system administrator.
                                        </div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" form="_export_form">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>