<?php
	// print_r($house); exit;
	
	$id =	isset($house['id']) && $house['id'] ? $house['id'] : '';
    $project_id =	isset($house['project_id']) && $house['project_id'] ? $house['project_id'] : '';
    $code =	isset($house['code']) && $house['code'] ? $house['code'] : '';
    $name =	isset($house['name']) && $house['name'] ? $house['name'] : '';
    $no_bedroom =	isset($house['no_bedroom']) && $house['no_bedroom'] ? $house['no_bedroom'] : '0';
    $no_bathroom =	isset($house['no_bathroom']) && $house['no_bathroom'] ? $house['no_bathroom'] : '0';
    $no_floor =	isset($house['no_floor']) && $house['no_floor'] ? $house['no_floor'] : '0';
    $no_ac =	isset($house['no_ac']) && $house['no_ac'] ? $house['no_ac'] : '0';
    $color =	isset($house['color']) && $house['color'] ? $house['color'] : '';
    $floor_area =	isset($house['floor_area']) && $house['floor_area'] ? $house['floor_area'] : '0.00';
    $lot_area =	isset($house['lot_area']) && $house['lot_area'] ? $house['lot_area'] : '0.00';
    $actual_image =	isset($house['actual_image']) && $house['actual_image'] ? $house['actual_image'] : '';
    $floor_image =	isset($house['floor_image']) && $house['floor_image'] ? $house['floor_image'] : '';
    $reservation_fee =	isset($house['reservation_fee']) && $house['reservation_fee'] ? $house['reservation_fee'] : '0.00';
    $furnishing_fee =	isset($house['furnishing_fee']) && $house['furnishing_fee'] ? $house['furnishing_fee'] : '0.00';
    $active =	isset($house['is_active']) && $house['is_active'] ? $house['is_active'] : '';
?>

<div class="row">
	<div class="col-md-6 col-xl-6" >
		<!-- HIDDEN INPUT -->
		<?php if($id !== ''):?>
			<input type="hidden" id="house_model_id" value="<?php echo $id; ?>">
		<?php endif; ?>

		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Project<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<label class="col-8 col-form-label">
				<select class="form-control kt-select2" id="project_id" name="project_id" <?php echo ($this->router->fetch_method() === 'create' && !empty($this->uri->segment(3)) ) ? 'disabled' : ''; ?>>
					<?php foreach ($project as $p): ?>

						<?php if($this->router->fetch_method() === 'create'): ?>

							<option value="<?php echo $p['id'] ?>" <?php echo ($this->uri->segment(3) == $p['id']) ? 'selected' : '' ?>><?php echo ucwords($p['name']); ?></option>
						<?php else: ?>

							<option value="<?php echo $p['id'] ?>" <?php echo ($project_id == $p['id']) ? 'selected' : '' ?>><?php echo ucwords($p['name']); ?></option>
						<?php endif; ?>

					<?php endforeach;?>
				</select>
			</label>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Property Model<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<input type="text" class="form-control" id="name" name="name" value="<?php echo set_value('name', $name); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Property Model Code<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<input type="text" class="form-control" id="code" name="code" value="<?php echo set_value('code', $code); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">No. of Bedroom</label>
			<div class="col-8">
				<input type="text" class="form-control" id="no_bedroom" name="no_bedroom" value="<?php echo set_value('no_bedroom', $no_bedroom); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">No. of Bathroom</label>
			<div class="col-8">
				<input type="text" class="form-control" id="no_bathroom" name="no_bathroom" value="<?php echo set_value('no_bathroom', $no_bathroom); ?>" placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">No. of Floor</label>
			<div class="col-8">
				<input type="text" class="form-control" id="no_floor" name="no_floor" value="<?php echo set_value('no_floor', $no_floor); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">No. of AC</label>
			<div class="col-8">
				<input type="text" class="form-control" id="no_ac" name="no_ac" value="<?php echo set_value('no_ac', $no_ac); ?>" placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Color</label>
			<div class="col-8">
				<input type="color" class="form-control" id="color" name="color" value="<?php echo set_value('color', $color); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Floor Area<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<input type="text" class="form-control" id="floor_area" name="floor_area" value="<?php echo set_value('floor_area', $floor_area); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Lot Area<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<input type="text" class="form-control" id="lot_area" name="lot_area" value="<?php echo set_value('lot_area', $lot_area); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Reservation Fee<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<input type="text" class="form-control" id="reservation_fee" name="reservation_fee" value="<?php echo set_value('reservation_fee', $reservation_fee); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Furnishing Fee<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<input type="text" class="form-control" id="furnishing_fee" name="furnishing_fee" value="<?php echo set_value('furnishing_fee', $furnishing_fee); ?>"  placeholder="">
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-6" >
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">&nbsp;</label>
			<div class="col-8">&nbsp;</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label">
				<span class="kt-font-bolder">Image</span>
				<span class="form-text text-muted ">Formats: .jpeg, .gif, .png</span>
				<span class="form-text text-muted ">File Sizes: up to 5mb</span>
			</label>
			<div class="col-8">
				<div class="kt-dropzone dropzone" id="actual">
					<div class="kt-dropzone__msg dz-message needsclick">
						<h3 class="kt-dropzone__msg-title">Property Model Image</h3>
						<span class="kt-dropzone__msg-desc">Drop file here or click to upload.</span>
					</div>
				</div>

				<div id="previews"></div>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label">
				<span class="kt-font-bolder">Floor Image</span>
				<span class="form-text text-muted ">Formats: .jpeg, .gif, .png</span>
				<span class="form-text text-muted ">File Sizes: up to 5mb</span>
			</label>
			<div class="col-8">
				<div class="kt-dropzone dropzone" id="floor">
					<div class="kt-dropzone__msg dz-message needsclick">
						<h3 class="kt-dropzone__msg-title">Floor Image</h3>
						<span class="kt-dropzone__msg-desc">Drop file here or click to upload.</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>