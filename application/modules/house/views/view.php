<?php
    // print_r($house); exit;
	
	$id =	isset($house['id']) && $house['id'] ? $house['id'] : '';
    $project_id =	isset($house['project_id']) && $house['project_id'] ? $house['project_id'] : '';
    $project_name =	isset($house['project']['name']) && $house['project']['name'] ? $house['project']['name'] : '';
    $code =	isset($house['code']) && $house['code'] ? $house['code'] : '';
    $name =	isset($house['name']) && $house['name'] ? $house['name'] : '';
    $no_bedroom =	isset($house['no_bedroom']) && $house['no_bedroom'] ? $house['no_bedroom'] : '0';
    $no_bathroom =	isset($house['no_bathroom']) && $house['no_bathroom'] ? $house['no_bathroom'] : '0';
    $no_floor =	isset($house['no_floor']) && $house['no_floor'] ? $house['no_floor'] : '0';
    $no_ac =	isset($house['no_ac']) && $house['no_ac'] ? $house['no_ac'] : '0';
    $color =	isset($house['color']) && $house['color'] ? $house['color'] : '';
    $floor_area =	isset($house['floor_area']) && $house['floor_area'] ? $house['floor_area'] : '0.00';
    $lot_area =	isset($house['lot_area']) && $house['lot_area'] ? $house['lot_area'] : '0.00';
    $actual_image =	isset($house['actual_image']) && $house['actual_image'] ? $house['actual_image'] : '';
    $floor_image =	isset($house['floor_image']) && $house['floor_image'] ? $house['floor_image'] : '';
    $reservation_fee =	isset($house['reservation_fee']) && $house['reservation_fee'] ? $house['reservation_fee'] : '0.00';
    $furnishing_fee =	isset($house['furnishing_fee']) && $house['furnishing_fee'] ? $house['furnishing_fee'] : '0.00';
    $active =	isset($house['is_active']) && $house['is_active'] ? $house['is_active'] : '';

	$actual_imgs = isset($house['actual']) && $house['actual'] ? $house['actual'] : '';
	$floor_imgs = isset($house['floor']) && $house['floor'] ? $house['floor'] : '';

	// print_r($actual_files); exit;

?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Property Model Details</h3>
		</div>
		<div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <?php if($this->ion_auth->is_admin()): ?>
                    <a href="<?php echo site_url('house/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                        <i class="fa fa-edit"></i> Edit
                    </a>
                <?php endif; ?>
				<a href="<?php echo site_url('house');?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
            </div>
        </div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
		<div class="col-md-12 col-xl-12">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body">
					<div class="row">
						<div class="col-md-6 col-xl-6" >
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">Project</label>
								<label class="col-8 col-form-label">
									<?php echo ucwords($project_name); ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">Property Model</label>
								<label class="col-8 col-form-label">
									<?php echo ucwords($name); ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">No. of Bedroom</label>
								<label class="col-8 col-form-label">
									<?php echo $no_bedroom; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">No. of Floor</label>
								<label class="col-8 col-form-label">
									<?php echo $no_floor; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">Floor Area</label>
								<label class="col-8 col-form-label">
									<?php echo $floor_area; ?> SQM
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">Reservation Fee</label>
								<label class="col-8 col-form-label">
									<?php echo $reservation_fee; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">Color</label>
								<label class="col-8 col-form-label">
									<?php echo $color; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">House</label>
								<label class="col-8 col-form-label">
									<!-- <img src="<?php echo base_url('assets/media/project-logos/3.png'); ?>" alt="image" style="width: 300px; height: 150px;"> -->
									<?php if ( ! empty($actual_imgs) ): ?>
										<?php foreach ($actual_imgs as $ai): ?>

											<img src="<?php echo get_house_img('actual', $ai['filename']); ?>" alt="image" style="width: 300px; height: 150px; margin-bottom: 1rem;"/>

										<?php endforeach;?>
									<?php endif; ?>
								</label>
							</div>
						</div>
						<div class="col-md-6 col-xl-6">
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">&nbsp;</label>
								<label class="col-8 col-form-label">&nbsp;</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">Property Model Code</label>
								<label class="col-8 col-form-label">
									<?php echo $code; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">No. of Bathroom</label>
								<label class="col-8 col-form-label">
									<?php echo $no_bathroom; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">No. of AC</label>
								<label class="col-8 col-form-label">
									<?php echo $no_ac; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">Floor Area</label>
								<label class="col-8 col-form-label">
									<?php echo $lot_area; ?> SQM
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">&nbsp;</label>
								<label class="col-8 col-form-label">
									&nbsp;
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">&nbsp;</label>
								<label class="col-8 col-form-label">&nbsp;</label>
							</div>
							<div class="form-group row" style="margin-bottom: 0;">
								<label class="col-4 col-form-label kt-font-bolder">Floor Plan</label>
								<label class="col-8 col-form-label">
									<!-- <img src="<?php echo base_url('assets/media/project-logos/3.png'); ?>" alt="image" style="width: 300px; height: 150px;"> -->
									<?php if ( ! empty($floor_imgs) ): ?>
										<?php foreach ($floor_imgs as $fi): ?>
											<img src="<?php echo get_house_img('floor', $fi['filename']); ?>" alt="image" style="width: 300px; height: 150px; margin-bottom: 1rem;"/>
										<?php endforeach;?>
									<?php endif; ?>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>