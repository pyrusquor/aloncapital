<?php if (!empty($records)) : ?>
    <div class="row" id="houseList">
		<?php foreach ($records as $r): ?>
			<div class="col-md-4 col-xl-3">
				<div class="kt-portlet">
					<div class="kt-portlet__head kt-portlet__head--noborder">
						<div class="kt-portlet__head-label">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
								<input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>" class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
								<span></span>
							</label>
						</div>
						<div class="kt-portlet__head-toolbar">
							<a href="#" class="btn btn-icon" data-toggle="dropdown">
								<i class="flaticon-more-1 kt-font-brand"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<ul class="kt-nav">
                                    <?php if($this->ion_auth->is_admin()): ?>
                                        <li class="kt-nav__item">
                                            <a href="<?php echo base_url('house/view/'.$r['id']); ?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                <span class="kt-nav__link-text">View</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="<?php echo base_url('house/update/'.$r['id']); ?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                <span class="kt-nav__link-text">Update</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="javascript: void(0)" class="kt-nav__link remove_house" data-id="<?php echo $r['id']; ?>">
                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                <span class="kt-nav__link-text">Delete</span>
                                            </a>
                                        </li>
                                    <?php else: ?>
                                        <li class="kt-nav__item">
                                            <a href="<?php echo @$_view;?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                <span class="kt-nav__link-text">View</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body" style="padding: 5px 20px 15px;">
						<!--begin::Widget -->
						<div class="kt-widget kt-widget--user-profile-4" style="margin-top: -40px;">
							<div class="kt-widget__head">
								<div class="kt-widget__media">
									<img class="kt-widget__img kt-hidden" src="<?php echo base_url('assets/media/users/300_21.jpg'); ?>" alt="image">
									<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-" style="width: 150px; height: 150px;">
									    <h1><?php echo get_initials($r['name']); ?></h1>
									</div>
								</div>
								<div class="kt-widget__content">
									<div class="kt-widget__section" style="text-align: center;">
										<a href="<?php echo base_url('house/view/'.$r['id']); ?>" class="kt-widget__username"><h2><?php echo $r['id'].'. '.ucwords($r['name']); ?></h2></a>
										<span class="kt-widget__desc"><h5><?php echo ucwords($r['code']); ?></h5></span>
										<br />
										<br />
										<div class="kt-widget__text kt-align-left" style="float: left;">Project</div>
										<div class="kt-widget__text kt-align-right" style="font-weight: 600;"><?php echo $r['project']['name']; ?></div>
										<div class="kt-widget__text kt-align-left" style="float: left;">Reservation Fee</div>
										<div class="kt-widget__text kt-align-right" style="font-weight: 600;"><?php echo $r['reservation_fee']; ?></div>
										<div class="kt-widget__text kt-align-left" style="float: left;">Furnishing Fee</div>
										<div class="kt-widget__text kt-align-right" style="font-weight: 600;"><?php echo $r['furnishing_fee']; ?></div>
										<br />
										<div class="kt-widget__button">
											<a href="<?php echo base_url('house/view/'.$r['id']); ?>" class="btn btn-brand btn-sm">View</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--end::Widget -->
					</div>
				</div>
			</div>

		<?php endforeach;?>
	</div>
    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found. Please adjust your search criteria and try again.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <button class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand" data-toggle="modal" data-target="#filterModal">Try Again</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>