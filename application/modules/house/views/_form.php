<?php
// print_r($house); exit;

$id =	isset($house['id']) && $house['id'] ? $house['id'] : '';
$project_id =	isset($house['project_id']) && $house['project_id'] ? $house['project_id'] : '';
$sub_type_id =	isset($house['sub_type_id']) && $house['sub_type_id'] ? $house['sub_type_id'] : '';
$code =	isset($house['code']) && $house['code'] ? $house['code'] : '';
$name =	isset($house['name']) && $house['name'] ? $house['name'] : '';
$no_bedroom =	isset($house['no_bedroom']) && $house['no_bedroom'] ? $house['no_bedroom'] : '0';
$no_bathroom =	isset($house['no_bathroom']) && $house['no_bathroom'] ? $house['no_bathroom'] : '0';
$no_floor =	isset($house['no_floor']) && $house['no_floor'] ? $house['no_floor'] : '0';
$no_ac =	isset($house['no_ac']) && $house['no_ac'] ? $house['no_ac'] : '0';
$color =	isset($house['color']) && $house['color'] ? $house['color'] : '';
$floor_area =	isset($house['floor_area']) && $house['floor_area'] ? $house['floor_area'] : '0.00';
$lot_area =	isset($house['lot_area']) && $house['lot_area'] ? $house['lot_area'] : '0.00';
$actual_image =	isset($house['actual_image']) && $house['actual_image'] ? $house['actual_image'] : '';
$floor_image =	isset($house['floor_image']) && $house['floor_image'] ? $house['floor_image'] : '';
$reservation_fee =	isset($house['reservation_fee']) && $house['reservation_fee'] ? $house['reservation_fee'] : '0.00';
// $furnishing_fee =	isset($house['furnishing_fee']) && $house['furnishing_fee'] ? $house['furnishing_fee'] : '0.00';
$active =	isset($house['is_active']) && $house['is_active'] ? $house['is_active'] : '';
$house_template_id = $house['house_template_id'] ?? '';
?>

<div class="row">
	<div class="col-md-6 col-xl-6">
		<!-- HIDDEN INPUT -->
		<?php if ($id !== '') : ?>
			<input type="hidden" id="house_model_id" value="<?php echo $id; ?>">
		<?php endif; ?>

		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Project<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<label class="col-8 col-form-label">
				<select class="form-control kt-select2" id="project_id" name="project_id" <?php echo ($this->router->fetch_method() === 'create' && !empty($this->uri->segment(3))) ? 'disabled' : ''; ?>>
					<?php foreach ($project as $p) : ?>

						<?php if ($this->router->fetch_method() === 'create') : ?>

							<option value="<?php echo $p['id'] ?>" <?php echo ($this->uri->segment(3) == $p['id']) ? 'selected' : '' ?>><?php echo ucwords($p['name']); ?></option>
						<?php else : ?>

							<option value="<?php echo $p['id'] ?>" <?php echo ($project_id == $p['id']) ? 'selected' : '' ?>><?php echo ucwords($p['name']); ?></option>
						<?php endif; ?>

					<?php endforeach; ?>
				</select>
			</label>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Property Type <span class="kt-font-danger">*</span></label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<?php echo form_dropdown('sub_type_id', Dropdown::get_static('property_types'), set_value('sub_type_id', @$sub_type_id), 'class="form-control" id="sub_type_id"'); ?>
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
				</div>
				<?php echo form_error('sub_type_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Property Model<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="name" class="form-control" placeholder="Property Model" value="<?php echo set_value('name', @$name); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-certificate"></i></span></span>
				</div>
				<?php echo form_error('name'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Property Model Code<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="code" class="form-control" placeholder="Property Model Code" value="<?php echo set_value('code', @$code); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-certificate"></i></span></span>
				</div>
				<?php echo form_error('code'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">House Template</label>
			<label class="col-8 col-form-label">
				<select class="form-control kt-select2" id="house_template_id" name="house_template_id">
					<option value="">Select a template</option>
					<?php foreach ($house_templates as $house_template) : ?>
						<option value="<?= $house_template['id'] ?>" <?= $house_template_id == $house_template['id'] ? 'selected' : '' ?>><?= $house_template['name'] ?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">No. of Bedroom</label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="no_bedroom" class="form-control" placeholder="No. of Bedroom" value="<?php echo set_value('no_bedroom', @$no_bedroom); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span></span>
				</div>
				<?php echo form_error('no_bedroom'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">No. of Bathroom</label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="no_bathroom" class="form-control" placeholder="No. of Bathroom" value="<?php echo set_value('no_bathroom', @$no_bathroom); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="fas fa-bath"></i></span></span>
				</div>
				<?php echo form_error('no_bathroom'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">No. of Floor</label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="no_floor" class="form-control" placeholder="No. of Floor" value="<?php echo set_value('no_floor', @$no_floor); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="fas fa-wave-square"></i></span></span>
				</div>
				<?php echo form_error('no_floor'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">No. of AC</label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="no_ac" class="form-control" placeholder="No. of AC" value="<?php echo set_value('no_ac', @$no_ac); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="fas fa-qrcode"></i></span></span>
				</div>
				<?php echo form_error('no_ac'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Color</label>
			<div class="col-8">
				<input type="color" class="form-control" id="color" name="color" value="<?php echo set_value('color', $color); ?>" placeholder="Color">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Floor Area<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="floor_area" class="form-control" placeholder="Floor Area" value="<?php echo set_value('floor_area', @$floor_area); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map"></i></span></span>
				</div>
				<?php echo form_error('floor_area'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Lot Area<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<!-- <div class="input-group">
						<input type="text" name="lot_area" class="form-control" placeholder="Lot Area" value="<?php echo set_value('lot_area', @$lot_area); ?>" autocomplete="off">
						<div class="input-group-append"><span class="input-group-text">SQM</span></div>
					</div> -->
					<input type="text" name="lot_area" class="form-control" placeholder="Lot Area" value="<?php echo set_value('lot_area', @$lot_area); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map"></i></span></span>
				</div>
				<?php echo form_error('lot_area'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Reservation Fee<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="reservation_fee" class="form-control" placeholder="Reservation Fee" value="<?php echo set_value('reservation_fee', @$reservation_fee); ?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-money"></i></span></span>
				</div>
				<?php echo form_error('reservation_fee'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<!-- <div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Furnishing Fee<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<div class="col-8">
				<div class="kt-input-icon  kt-input-icon--left">
					<input type="text" name="furnishing_fee" class="form-control" placeholder="Furnishing Fee" value="<?php //echo set_value('furnishing_fee', @$furnishing_fee);
																														?>" autocomplete="off">
					<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-money"></i></span></span>
				</div>
				<?php //echo form_error('furnishing_fee'); 
				?>
				<span class="form-text text-muted"></span>
			</div>
		</div> -->
	</div>
	<div class="col-md-6 col-xl-6">
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">&nbsp;</label>
			<div class="col-8">&nbsp;</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label">
				<span class="kt-font-bolder">Image</span>
				<span class="form-text text-muted ">Formats: .jpeg, .gif, .png</span>
				<span class="form-text text-muted ">File Sizes: up to 5mb</span>
			</label>
			<div class="col-8">
				<div class="kt-dropzone dropzone" id="actual">
					<div class="kt-dropzone__msg dz-message needsclick">
						<h3 class="kt-dropzone__msg-title">Property Model Image</h3>
						<span class="kt-dropzone__msg-desc">Drop file here or click to upload.</span>
					</div>
				</div>

				<div id="previews"></div>
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label">
				<span class="kt-font-bolder">Floor Image</span>
				<span class="form-text text-muted ">Formats: .jpeg, .gif, .png</span>
				<span class="form-text text-muted ">File Sizes: up to 5mb</span>
			</label>
			<div class="col-8">
				<div class="kt-dropzone dropzone" id="floor">
					<div class="kt-dropzone__msg dz-message needsclick">
						<h3 class="kt-dropzone__msg-title">Floor Image</h3>
						<span class="kt-dropzone__msg-desc">Drop file here or click to upload.</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>