<?php
$id = isset($item_class['id']) && $item_class['id'] ? $item_class['id'] : '';
$name = isset($item_class['name']) && $item_class['name'] ? $item_class['name'] : '';
$class_code = isset($item_class['class_code']) && $item_class['class_code'] ? $item_class['class_code'] : '';
$description = isset($item_class['description']) && $item_class['description'] ? $item_class['description'] : '';
$is_active = isset($item_class['is_active']) && $item_class['is_active'] ? $item_class['is_active'] : '0';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Company</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('item_class/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('item_class');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Type Name
							</span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
								<?php echo $name; ?>
							</span>
                                </div>
                                <div class="kt-widget13__item">
							<span class="kt-widget13__desc kt-align-right">
								Type Code:
							</span>
                                    <span class="kt-widget13__text">
								<?php echo $class_code; ?>
							</span>
                                </div>
                                <div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Group:
							</span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
								<?php echo $description; ?>
							</span>
                                </div>
                                <div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Status:
							</span>
                                    <span class="kt-widget13__text">
								<span class="kt-widget__data" style="font-weight: 500"><?php echo Dropdown::get_static('inventory_status', $is_active, 'view'); ?></span>

                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Items
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">


                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->
