<?php

$name = isset($item_class['name']) && $item_class['name'] ? $item_class['name'] : '';
$item_type_id = isset($item_class['item_type']) && $item_class['item_type'] ? $item_class['item_type']['id'] : '';
$item_type_name = isset($item_class['item_type']) && $item_class['item_type'] ? $item_class['item_type']['name'] : '';
$class_code = isset($item_class['class_code']) && $item_class['class_code'] ? $item_class['class_code'] : '';
$description = isset($item_class['description']) && $item_class['description'] ? $item_class['description'] : '';
$status = isset($item_class['is_active']) && $item_class['is_active'] ? $item_class['is_active'] : '';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item Type <span class="kt-font-danger">*</span></label>
            <select name="item_type_id" class="form-control form-control-sm suggests" data-module="item_type">
                <option value="<?= $item_type_id ?>"><?= $item_type_name ?></option>
            </select>
            <?php echo form_error('class_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Class Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="class_code" value="<?php echo set_value('class_code', $class_code); ?>" placeholder="Type Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('class_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Description </label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-align-left"></i></span>
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Status</label>
            <div class="kt-input-icon kt-input-icon--left">

                <?php echo form_dropdown('is_active', Dropdown::get_static('inventory_status'), set_value('is_active', @$status), 'class="form-control"'); ?>
                <?php echo form_error('is_active'); ?>

                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-check-square"></i></span>
            </div>

            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>