<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_permission_model extends MY_Model
{
    public $table = 'user_permissions'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'user_id',
        'permission_id',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
    public $fields = [
        array(
            'field' => 'user_id',
            'label' => 'User ID',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'permission_id',
            'label' => 'Permission ID',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert']	=	$this->fields;
        $this->rules['update']	=	$this->fields;

        $this->has_one['user'] = array('foreign_model' => 'user/user_model', 'foreign_table' => 'users', 'foreign_key' => 'id', 'local_key' => 'user_id');
        $this->has_one['permission'] = array('foreign_model' => 'permission/permission_model', 'foreign_table' => 'permissions', 'foreign_key' => 'id', 'local_key' => 'permission_id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}