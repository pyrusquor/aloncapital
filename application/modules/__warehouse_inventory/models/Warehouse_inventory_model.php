<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Warehouse_inventory_model extends MY_Model
{
    public $table = 'warehouse_inventory'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = ['company', 'sbu',
        'warehouse',
        'item_code',
        'item_name',
        'current_physical_count',
        'is_active',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'company' => array(
            'field' => 'company',
            'label' => 'Company',
            'rules' => 'trim|required',
        ),
        'sbu' => array(
            'field' => 'sbu',
            'label' => 'SBU',
            'rules' => 'trim|required',
        ),
        'warehouse' => array(
            'field' => 'warehouse',
            'label' => 'Warehouse',
            'rules' => 'trim|required',
        ),
        'item_code' => array(
            'field' => 'item_code',
            'label' => 'Item Code',
            'rules' => 'trim|required',
        ),
        'item_name' => array(
            'field' => 'item_name',
            'label' => 'Item Name',
            'rules' => 'trim|required',
        ),
        'current_physical_count' => array(
            'field' => 'current_physical_count',
            'label' => 'Current Physical Count',
            'rules' => 'trim|required',
        ),
        'is_active' => array(
            'field' => 'is_active',
            'label' => 'Is Active',
            'rules' => 'trim|required',
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'company' => $faker->word,
                'sbu' => $faker->word,
                'warehouse' => $faker->word,
                'item_code' => $faker->word,
                'item_name' => $faker->word,
                'current_physical_count' => $faker->word,
                'is_active' => $faker->numberBetween(0, 1),
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}
