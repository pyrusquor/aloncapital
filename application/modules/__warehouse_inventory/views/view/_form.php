<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$company = isset($data['company']) && $data['company'] ? $data['company'] : '';
$sbu = isset($data['sbu']) && $data['sbu'] ? $data['sbu'] : '';
$warehouse = isset($data['warehouse']) && $data['warehouse'] ? $data['warehouse'] : '';
$item_code = isset($data['item_code']) && $data['item_code'] ? $data['item_code'] : '';
$item_name = isset($data['item_name']) && $data['item_name'] ? $data['item_name'] : '';
$current_physical_count = isset($data['current_physical_count']) && $data['current_physical_count'] ? $data['current_physical_count'] : '';
$is_active = isset($data['is_active']) && $data['is_active'] ? $data['is_active'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="company" value="<?php echo set_value('company', $company); ?>" placeholder="Company" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building"></i></span>
            </div>
            <?php echo form_error('company'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>SBU <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="sbu" value="<?php echo set_value('sbu', $sbu); ?>" placeholder="SBU" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('sbu'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="warehouse" value="<?php echo set_value('warehouse', $warehouse); ?>" placeholder="Warehouse" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-industry"></i></span>
            </div>
            <?php echo form_error('warehouse'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_code" value="<?php echo set_value('item_code', $item_code); ?>" placeholder="Item Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('item_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_name" value="<?php echo set_value('item_name', $item_name); ?>" placeholder="Item name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-archive"></i></span>
            </div>
            <?php echo form_error('item_name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Current Physical Count <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="current_physical_count" value="<?php echo set_value('current_physical_count', $current_physical_count); ?>" placeholder="Current Physical Count" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sort-numeric-asc"></i></span>
            </div>
            <?php echo form_error('current_physical_count'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Is Active <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), set_value('is_active', @$is_active), 'class="form-control"'); ?>

            <?php echo form_error('is_active'); ?>
            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-check-square"></i></span>
        </div>
                    </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->

    <!-- ==================== end: Add form model fields ==================== -->
</div>