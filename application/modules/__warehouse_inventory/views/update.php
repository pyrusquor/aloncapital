<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Update Warehouse Inventory</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="warehouse_inventory_form">
					<i class="fa fa-sync"></i> Update
				</button>
				<a href="<?php echo site_url('warehouse_inventory'); ?>" class="btn btn-label-instagram btn-elevate btn-sm">
					<i class="fa fa-reply"></i> Back
				</a>
            </div>
        </div>
	</div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <!--begin::Form-->
                <form method="POST" class="kt-form kt-form--label-right" id="warehouse_inventory_form">
                <div class="kt-portlet__body">

                <?php $this->load->view('view/_form');?>

                </div>
            </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->