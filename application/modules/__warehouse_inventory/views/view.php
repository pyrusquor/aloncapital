<?php

$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$company = isset($data['company']) && $data['company'] ? $data['company'] : '';
$sbu = isset($data['sbu']) && $data['sbu'] ? $data['sbu'] : '';
$warehouse = isset($data['warehouse']) && $data['warehouse'] ? $data['warehouse'] : '';
$item_code = isset($data['item_code']) && $data['item_code'] ? $data['item_code'] : '';
$item_name = isset($data['item_name']) && $data['item_name'] ? $data['item_name'] : '';
$current_physical_count = isset($data['current_physical_count']) && $data['current_physical_count'] ? $data['current_physical_count'] : '';
$is_active = isset($data['is_active']) && $data['is_active'] ? $data['is_active'] : '0';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Warehouse Inventory</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('warehouse_inventory/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('warehouse_inventory'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Company
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $company; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        SBU
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $sbu; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Warehouse
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $warehouse; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Item Code
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $item_code; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Item Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $item_name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Current Physical Count
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $current_physical_count; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Status
                                    </span>
                                    <span class="kt-widget__data" style="font-weight: 500"><?php echo Dropdown::get_static('inventory_status', $is_active, 'view'); ?></span>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->

                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
