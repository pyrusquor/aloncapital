<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Material_issuance_item_model extends MY_Model
{
    public $table = 'material_issuance_items'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        "id",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
        "material_issuance_id",
        "material_receiving_item_id",
        "quantity",
        "material_request_id",
    ];
    public $form_fillables = [
        "id",
        "material_issuance_id",
        "material_receiving_item_id",
        "quantity",
        "material_request_id"
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [


        "material_issuance_id" => array(
            "field" => "material_issuance_id",
            "label" => "Material Issuance",
            "rules" => "numeric|required"
        ),

        "material_receiving_item_id" => array(
            "field" => "material_receiving_item_id",
            "label" => "Material Receiving Item",
            "rules" => "numeric|required"
        ),

        "quantity" => array(
            "field" => "quantity",
            "label" => "Quantity",
            "rules" => "numeric|required"
        ),

    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;


        $this->has_one["material_issuance"] = array('foreign_model' => 'material_issuance/material_issuance_model', 'foreign_table' => 'material_issuances', 'foreign_key' => 'id', 'local_key' => 'material_issuance_id');


        $this->has_one["material_receiving_item"] = array('foreign_model' => 'material_receiving_item/material_receiving_item_model', 'foreign_table' => 'material_receiving_items', 'foreign_key' => 'id', 'local_key' => 'material_receiving_item_id');
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
