<div class="row">








    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Material Issuance <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="material_issuances" id="material_issuance_id"
                            name="id">
                        <?php if ($material_issuance_id): ?>
                            <option value="<?php echo $material_issuance_id['id']; ?>"
                                    selected><?php echo $material_issuance_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Material Receiving Item <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="material_receiving_items" id="material_receiving_item_id"
                            name="id">
                        <?php if ($material_receiving_item_id): ?>
                            <option value="<?php echo $material_receiving_item_id['id']; ?>"
                                    selected><?php echo $material_receiving_item_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Quantity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                    <input type="number" min="0" step="0.01" class="form-control" id="quantity" name="quantity" placeholder="Quantity" v-model="info.form.quantity">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

</div>