<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_voucher extends MY_Controller
{

    private $fields = [
        array(
            'field' => 'voucher[reference]',
            'label' => 'Reference',
            'rules' => 'trim',
        ),
        array(
            'field' => 'voucher[payment_request_id]',
            'label' => 'Payment Request ID',
            'rules' => 'trim',
        ),
        array(
            'field' => 'voucher[payment_type_id]',
            'label' => 'Payment Type ID',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'voucher[paid_amount]',
            'label' => 'Paid Amount',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'voucher[paid_date]',
            'label' => 'Paid Date',
            'rules' => 'trim',
        ),
        array(
            'field' => 'voucher[check_id]',
            'label' => 'Check ID',
            'rules' => 'trim',
        ),
    ];
    private $cv_fields = [
        array(
            'field' => 'cheque_voucher_number',
            'label' => 'Cheque Voucher Number',
            'rules' => 'trim',
        ),
        array(
            'field' => 'cheque_number',
            'label' => 'Cheque Voucher',
            'rules' => 'trim',
        ),
        array(
            'field' => 'bank_id',
            'label' => 'Bank',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'remarks',
            'label' => 'Remarks',
            'rules' => 'trim',
        ),
        array(
            'field' => 'payment_voucher_id',
            'label' => 'Payment Voucher ID',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $payment_voucher_models = array(
            'payment_voucher/Payment_voucher_model' => 'M_payment_voucher',
            'payment_voucher/Cheque_voucher_model' => 'M_cheque_voucher',
            'payment_voucher/Payment_voucher_origin_model' => 'M_payment_voucher_origins',
            'payment_voucher/Payment_voucher_property_model' => 'M_payment_voucher_properties',
            'payment_voucher/Payment_voucher_project_model' => 'M_payment_voucher_projects'
        );

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model($payment_voucher_models);
        $this->load->model('payment_request/Payment_request_model', 'M_payment_request');
        // $this->load->model('payment_request/Payment_request_item_model', 'M_payment_request_items');
        $this->load->model('item/Item_model', 'M_item');
        $this->load->model('payable_items/Payable_items_model', 'M_payable_item');
        $this->load->model('accounting_settings/Accounting_settings_model', 'M_accounting_settings');
        $this->load->model('accounting_entries/Accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');
        $this->load->model('transaction_commission/Transaction_commission_model', 'M_Transaction_commission');
        $this->load->model('transaction/Transaction_model', 'M_Transaction');

        $this->load->model('company/Company_model', 'M_company');

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper
        $this->load->helper('form');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_payment_voucher->fillable;
        $this->_table_columns = $this->M_payment_voucher->__get_columns();
    }

    public function template($page = "")
    {

        $this->template->build($page);
    }

    // public function index($debug = 0)
    // {
    //     $_fills = $this->_table_fillables;
    //     $_colms = $this->_table_columns;

    //     $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
    //     $this->view_data['_columns'] = $this->__get_columns($_fills);

    //     // Get record count
    //     // $conditions['returnType'] = 'count';
    //     $this->view_data['totalRec'] = $totalRec = $this->M_payment_voucher->count_rows();

    //     // Pagination configuration
    //     $config['target'] = '#payment_voucherContent';
    //     $config['base_url'] = base_url('payment_voucher/paginationData');
    //     $config['total_rows'] = $totalRec;
    //     $config['per_page'] = $this->perPage;
    //     $config['link_func'] = 'PaymentVoucherPagination';

    //     // Initialize pagination library
    //     $this->ajax_pagination->initialize($config);

    //     // Get records
    //     $this->view_data['records'] = $this->M_payment_voucher
    //         ->with_requests()
    //         ->with_project()
    //         ->with_payable_type()
    //         ->with_property()
    //         ->with_approving_department()
    //         ->with_requesting_department()
    //         ->with_list_items()
    //         ->with_entry_items()
    //         ->with_entry_item()
    //         ->order_by('paid_date', 'DESC')
    //         ->limit($this->perPage, 0)
    //         ->get_all();

    //     if($debug){
    //         vdebug($this->view_data['records']);
    //     }

    //     $this->template->build('index', $this->view_data);
    // }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;
        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);
        $db_columns = $this->M_payment_voucher->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';
                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }
            $column_count = count($column);
            $cceil = ceil(($column_count / 2));
            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_payment_voucher->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);
        $this->template->build('index', $this->view_data);
    }

    public function showPaymentVouchers()
    {

        $where = $this->input->post('where') ? $this->input->post('where') : [];

        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'payment_type_id' => true,
            'payee' => true,
            'payee_type' => true,
            'paid_date' => true,
            'paid_amount' => true,
            'particulars' => true,
            'check_id' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        $paid_date = $this->input->post('paid_date');
        $date_range = [];

        if (!empty($paid_date)) {
            $date_range = explode('-', $paid_date);

            $from_str   = strtotime($date_range[0]);
            $from       = date('Y-m-d H:i:s', $from_str);

            $to_str     = strtotime($date_range[1] . '23:59:59');
            $to         = date('Y-m-d H:i:s', $to_str);
            $date_range = array($from, $to);
        }

        // get all raw data
        // $accounting_entries = $this->M_Accounting_entries->with_accounting_entry_items()->as_array()->get_all();
        // $payment_voucher = $this->M_payment_voucher->as_array()->get_all();
        $payment_voucher = $this->M_payment_voucher->where($where)->get_search_payment_voucher($date_range);
        // vdebug($payment_voucher);
        $data = [];

        if ($payment_voucher) {

            foreach ($payment_voucher as $key => $value) {

                // $transaction = $this->M_Transaction->get($value["commission"]["transaction_id"]);   
                // vdebug($transaction);
                $payee = get_person($value['payee_type_id'], $value['payee_type']);
                $payee_name = get_fname($payee);

                $payment_voucher[$key]['paid_date'] = view_date($value['paid_date']);
                $payment_voucher[$key]['paid_amount'] = money_php($value['paid_amount']);
                $payment_voucher[$key]['payee'] = $payee_name;
                $payment_voucher[$key]['check_id'] = $value['check_id'];
                // $payment_voucher[$key]['transaction_reference'] = $transaction["reference"] ;

                // $payment_voucher[$key]['transaction_reference'] = isset($value['transaction']) && $value['transaction'] ? $value['transaction']['reference'] : 'N/A';
                // $accounting_entries[$key]['payee_name'] = ( $value['payee_type_id'] ? @$r['name']." ".@$r['first_name']." ". @$r['last_name'] : 'N/A');

                $payment_voucher[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                $payment_voucher[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
            }

            foreach ($payment_voucher as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'] - 1;
                $dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }


        echo json_encode($output);
        exit();
    }

    public function showPropertyPaymentVouchers()
    {

        $where = $this->input->post('where') ? $this->input->post('where') : [];

        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'payment_type_id' => true,
            'payee' => true,
            'payee_type' => true,
            'paid_date' => true,
            'paid_amount' => true,
            'particulars' => true,
            'check_id' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        $paid_date = $this->input->post('paid_date');
        $date_range = [];

        if (!empty($paid_date)) {
            $date_range = explode('-', $paid_date);

            $from_str   = strtotime($date_range[0]);
            $from       = date('Y-m-d H:i:s', $from_str);

            $to_str     = strtotime($date_range[1] . '23:59:59');
            $to         = date('Y-m-d H:i:s', $to_str);
            $date_range = array($from, $to);
        }

        // get all raw data
        // $accounting_entries = $this->M_Accounting_entries->with_accounting_entry_items()->as_array()->get_all();
        // $payment_voucher = $this->M_payment_voucher->as_array()->get_all();

        $payment_voucher_properties = $this->M_payment_voucher_properties->where('property_id', $where['property_id'])->fields('id')->with_payment_voucher('fields:id')->get_all();

        $voucher_ids = [];

        if ($payment_voucher_properties) {

            foreach ($payment_voucher_properties as $value) {

                array_push($voucher_ids, $value['payment_voucher']['id']);
            }
        }

        $payment_voucher = $this->M_payment_voucher->where('id', !empty($voucher_ids) ? $voucher_ids : 0)->get_search_payment_voucher($date_range);

        $data = [];

        if ($payment_voucher) {

            foreach ($payment_voucher as $key => $value) {

                // $transaction = $this->M_Transaction->get($value["commission"]["transaction_id"]);   
                // vdebug($transaction);
                $payee = get_person($value['payee_type_id'], $value['payee_type']);
                $payee_name = get_fname($payee);

                $payment_voucher[$key]['paid_date'] = view_date($value['paid_date']);
                $payment_voucher[$key]['paid_amount'] = money_php($value['paid_amount']);
                $payment_voucher[$key]['payee'] = $payee_name;
                $payment_voucher[$key]['check_id'] = $value['check_id'];
                // $payment_voucher[$key]['transaction_reference'] = $transaction["reference"] ;

                // $payment_voucher[$key]['transaction_reference'] = isset($value['transaction']) && $value['transaction'] ? $value['transaction']['reference'] : 'N/A';
                // $accounting_entries[$key]['payee_name'] = ( $value['payee_type_id'] ? @$r['name']." ".@$r['first_name']." ". @$r['last_name'] : 'N/A');

                $payment_voucher[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                $payment_voucher[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
            }

            foreach ($payment_voucher as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'] - 1;
                $dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }


        echo json_encode($output);
        exit();
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            $payable_type_id = $this->input->post('payable_type_id');
            $project_id = $this->input->post('project_id');
            $property_id = $this->input->post('property_id');
            $payee_type_id = $this->input->post('payee_type_id');
            $payee_type = $this->input->post('payee_type');
            $is_paid = $this->input->post('is_paid');
            $due_date = $this->input->post('due_date');
            $page = $this->input->post('page');

            if (!empty($due_date)) {
                $date_range = explode('-', $due_date);

                $from_str   = strtotime($date_range[0]);
                $from       = date('Y-m-d H:i:s', $from_str);

                $to_str     = strtotime($date_range[1] . '23:59:59');
                $to         = date('Y-m-d H:i:s', $to_str);
            }

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_payment_voucher->count_rows();
            //            $where = array();

            // Pagination configuration
            $config['target'] = '#payment_voucherContent';
            $config['base_url'] = base_url('payment_voucher/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'PaymentVoucherPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                //                $this->db->or_like('payable_type_id', $keyword, 'both');
                //                $this->db->or_like('property_id', $keyword, 'both');
                //                $this->db->or_like('project_id', $keyword, 'both');
                //                $this->db->or_like('is_paid', $keyword, 'both');
                //                $this->db->or_like('due_date', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payable_type_id)) :
                $this->db->group_start();
                $this->db->where('payable_type_id', $payable_type_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payee_type)) :
                $this->db->group_start();
                $this->db->where('payee_type', $payee_type, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payee_type_id)) :
                $this->db->group_start();
                $this->db->where('payee_type_id', $payee_type_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($is_paid)) :
                $this->db->group_start();
                $this->db->where('is_paid', $is_paid, 'both');
                $this->db->group_end();
            endif;

            if (!empty($due_date)) :
                $this->db->group_start();
                $this->db->where('due_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            $totalRec = $this->M_payment_voucher->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                //                $this->db->or_like('payable_type_id', $keyword, 'both');
                //                $this->db->or_like('property_id', $keyword, 'both');
                //                $this->db->or_like('project_id', $keyword, 'both');
                //                $this->db->or_like('is_paid', $keyword, 'both');
                //                $this->db->or_like('due_date', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payable_type_id)) :
                $this->db->group_start();
                $this->db->where('payable_type_id', $payable_type_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payee_type)) :
                $this->db->group_start();
                $this->db->where('payee_type', $payee_type, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payee_type_id)) :
                $this->db->group_start();
                $this->db->where('payee_type_id', $payee_type_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($is_paid)) :
                $this->db->group_start();
                $this->db->where('is_paid', $is_paid, 'both');
                $this->db->group_end();
            endif;

            if (!empty($due_date)) :
                $this->db->group_start();
                $this->db->where('due_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            $this->view_data['records'] = $records = $this->M_payment_voucher
                ->with_requests()
                ->with_project()
                ->with_payable_type()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_items()
                ->with_entry_item()
                ->order_by('paid_date', 'DESC')
                ->limit($this->perPage, $offset)
                ->get_all();

            $this->load->view('payment_voucher/_filter', $this->view_data, false);
        }
    }

    public function view($id = false, $type = '')
    {

        if ($id) {

            $this->view_data['info'] = $info = $this->M_payment_voucher
                ->with_requests()
                ->with_project()
                ->with_payable_type()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_items()
                ->with_entry_item()
                ->get($id);

            $request_id = $info['payment_request_id'];

            $this->view_data['requests'] = $this->M_payment_request->with_vouchers()->with_list_items()->get($request_id);

            if ($this->view_data['info']) {

                if ($type == "debug") {

                    vdebug($this->view_data);
                }

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function form($pr_id = false, $id = false, $debug = 0, $seller_id = false, $commision_amount = 0, $commissions = false)
    {
        $this->view_data['payment_voucher_id'] = 0;
        $this->view_data['payment_request_id'] = 0;

        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {
            // vdebug($this->input->post());
            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            if ($this->form_validation->run() === true) {

                $oof = $this->input->post();

                // if (isset($oof['payment_request_id'])) {
                //     $payment_request = $this->M_payment_request->get($oof['payment_request_id']);
                // };

                if (isset($oof['items'])) {
                    $items = $oof['items'];
                }
                if (isset($oof['commissions'])) {
                    $commission_ids = $oof['commissions'];
                }
                $voucher = $oof['voucher'];
                $entry_items = $oof['entry_item'];
                $accounting_entry = $oof['accounting_entry'];

                $project_ids = $oof['project_id'];
                $property_ids = $oof['property_id'];

                $payment_request_id = $oof['payment_request_id'];
                $payment_voucher_id = $oof['payment_voucher_id'];

                if ($payment_voucher_id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    // $paymentVoucherID = $this->M_payment_voucher->update($request + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    // begin:Save accounting entries
                    $accounting_entry['or_number'] = 0;
                    $accounting_entry['journal_type'] = 5;
                    $accounting_entry['invoice_number'] = uniqidPV();
                    $accounting_entry['payment_date'] = $voucher['paid_date'];
                    $accounting_entry['payee_type'] = $voucher['payee_type'];
                    $accounting_entry['payee_type_id'] = $voucher['payee_type_id'];
                    $accounting_entry['remarks'] = $voucher['particulars'];
                    $accounting_entry['company_id'] = $accounting_entry['company_id'];

                    $accounting_entry_id = $this->M_Accounting_entries->insert($accounting_entry + $additional);
                    // end:Save accounting entries

                    // begin:Save payment request
                    // $request['accounting_entry_id'] = $accounting_entry_id;
                    // $payment_request = $this->M_payment_request->insert($request + $additional);
                    // $payment_request_id = $this->db->insert_id();
                    // end:Save payment request

                    if ($entry_items) {
                        foreach ($entry_items as $key => $entry) {
                            $entry_item['accounting_entry_id'] = $accounting_entry_id;
                            $entry_item['ledger_id'] = $entry['ledger_id'];
                            $entry_item['amount'] = $entry['amount'];
                            $entry_item['dc'] = $entry['dc'];
                            $entry_item['payee_type'] = $accounting_entry['payee_type'];
                            $entry_item['payee_type_id'] = $accounting_entry['payee_type_id'];
                            $entry_item['is_reconciled'] = 0;
                            $entry_item['description'] = $entry['description'];

                            $this->M_Accounting_entry_items->insert($entry_item + $additional);
                        }
                    }

                    $voucher['reference'] = uniqidPV(); // change to pr id helper
                    $voucher['payment_request_id'] = $payment_request_id;
                    $voucher['accounting_entry_id'] = $accounting_entry_id;
                    $voucher['is_paid'] = 1;
                    $voucher['is_complete'] = 1;

                    $cv['bank_id'] = $oof['bank_id'];
                    $cv['cheque_number'] = $oof['cheque_number'];
                    $cv['remarks'] = $oof['remarks'];
                    $cv['reference'] = uniqidCV();

                    $payment_voucher_id = $this->M_payment_voucher->insert($voucher + $additional);

                    $cv['payment_voucher_id'] = $payment_voucher_id;

                    $cheque_voucher = $this->M_cheque_voucher->insert($cv + $additional);

                    $cv_info['check_id'] =  $cheque_voucher;

                    $this->M_payment_voucher->update($cv_info + $additional, $payment_voucher_id);


                    if (isset($items)) {
                        foreach ($items as $key => $item) {
                            if (isset($item['item_id'])) {
                                $item_id = $item['item_id'];
                                $item_info = $this->M_item->with_tax()->get($item_id);
                                $list_item['payment_voucher_id'] = $payment_voucher_id;
                                $list_item['item_id'] = $item_id;
                                $list_item['payable_type'] = 'PV';
                                $list_item['name'] = $item_info['name'];
                                $list_item['unit_price'] = $item_info['unit_price'];
                                $list_item['quantity'] = $item['quantity'];
                                $list_item['tax_id'] = $item['tax_id'];
                                $list_item['rate'] = $item_info['tax']['rate'];
                                $list_item['tax_amount'] = $item['tax_amount'];
                                $list_item['total_amount'] = $item['total_amount'];

                                $this->M_payable_item->insert($list_item + $additional);
                            }
                        }
                    }

                    foreach ($project_ids as $pj_id) {
                        $pj_info['payment_voucher_id'] = $payment_voucher_id;
                        $pj_info['project_id'] = $pj_id;
                        $this->M_payment_voucher_projects->insert($pj_info + $additional);
                    }
                    foreach ($property_ids as $pr_id) {
                        $pr_info['payment_voucher_id'] = $payment_voucher_id;
                        $pr_info['property_id'] = $pr_id;
                        $this->M_payment_voucher_properties->insert($pr_info + $additional);
                    }

                    if ($voucher['payable_type_id'] == '1') {
                        $origin_ids = explode(',', $voucher['origin_id']);

                        foreach ($origin_ids as $id) {

                            $request_item = ['payment_voucher_id' => $payment_voucher_id, 'transaction_commission_id' => $id];
                            $this->M_payment_voucher_origins->insert($request_item + $additional);

                            $commission_data = [
                                'status' => 4,
                                'updated_by' => $this->user->id,
                                'updated_at' => NOW,
                                'payment_voucher_id' => $payment_voucher_id
                            ];
                            $this->M_Transaction_commission->update($commission_data, $id);
                        }
                    }

                    $this->db->trans_complete(); # Completing payment_voucher
                }

                # Check total payment
                if ($payment_request_id) {

                    $total_paid_amount = count_paid_amount($payment_request_id, 'payment_vouchers', 'payment_request_id', 'paid_amount');

                    if ($total_paid_amount == $voucher['gross_amount'] || $total_paid_amount >= $voucher['gross_amount']) {
                        $this->updatePaymentRequest($payment_request_id, 1, 1);
                    } else {
                        $this->updatePaymentRequest($payment_request_id, 1, 0);
                    }
                }

                /*Optional*/
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $response['status'] = 1;
                    $response['message'] = 'Payment Voucher Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;

        if ($id) {
            $this->view_data['payment_voucher_id'] = $id;

            $this->view_data['info'] = $this->M_payment_voucher->with_requests()->get($id);
        }

        if ($pr_id) {

            $this->view_data['payment_request_id'] = $pr_id;

            $this->view_data['payment_request'] = $payment_request = $this->M_payment_request
                ->with_project()
                ->with_payable_type()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_item()
                ->with_entry_items()
                ->with_origins()
                ->with_properties()
                ->with_projects()
                ->get($pr_id);
            $this->view_data['origins'] = array_column($payment_request['origins'], 'transaction_commission_id');
            $this->view_data['method'] = $method;

            // Mirror method that was used in payment request. Change filter settings to get the settings for payment voucher

            $net_amount = $payment_request['total_due_amount'];
            $entry_items = [];
            $amounts = [$net_amount, $net_amount];
            $accounting_setting_filter = [
                'category_type' => '4',
                'period_id' => '5',
                'module_types_id' => '1',
                'entry_types_id' => '2',
                'for_recognize' => '0',
                'company_id' => $payment_request['entry_item']['company_id']
            ];
            $accounting_setting = $this->M_accounting_settings->with_accounting_setting_item()->get($accounting_setting_filter);
            if ($accounting_setting) {
                foreach ($accounting_setting['accounting_setting_item'] as $setting_key => $setting_item) {
                    $entry_items[] = [
                        'dc' => $setting_item['accounting_entry_type'],
                        'ledger_id' => $setting_item['accounting_entry'],
                        'ledger_name' => Dropdown::get_dynamic('accounting_ledgers', $setting_item['accounting_entry'], 'name', 'id', 'view'),
                        'amount' => $amounts[$setting_key] ? $amounts[$setting_key] : '0',
                        'description' => 'Commission'
                    ];
                }

                $this->view_data['type'] = 'commission';
                $this->view_data['entry_items'] = $entry_items;
                $this->view_data['company'] = [
                    'id' => $payment_request['entry_item']['company_id'],
                    'name' => Dropdown::get_dynamic('companies', $payment_request['entry_item']['company_id'], 'name', 'id', 'view')
                ];
            }
            // vdebug($this->view_data);
            $this->template->build('form', $this->view_data);
        } else {
            $this->template->build('form', $this->view_data);
        }

        if ($seller_id) {


            $this->view_data['payment_voucher']['payee_type'] = "sellers";
            $this->view_data['payment_voucher']['payee_type_id'] = $seller_id;
            $this->view_data['payment_voucher']['gross_amount'] = $commision_amount;
            $this->view_data['payment_voucher']['commission_amount'] = $commision_amount;

            $commission_arr = explode("-", $commissions);
            $this->view_data['commissions'] = $commission_arr;
        }

        if ($debug) {
            vdebug($this->view_data);
        }
        $this->template->build('form', $this->view_data);
    }

    public function commission()
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->view_data['data'] = $this->M_Transaction_commission->show_rfp();

        $this->template->build('commission', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_payment_voucher->get($id);
            if ($list) {

                $deleted = $this->M_payment_voucher->delete($list['id']);
                $this->M_payment_voucher_origins->where(['payment_voucher_id' => $list['id']])->delete();
                $this->M_payment_voucher_properties->where(['payment_voucher_id' => $list['id']])->delete();
                $this->M_payment_voucher_projects->where(['payment_voucher_id' => $list['id']])->delete();

                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Payment Voucher successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_payment_voucher->delete($value);
                    $this->M_payment_voucher_origins->where(['payment_voucher_id' => $value])->delete();
                    $this->M_payment_voucher_properties->where(['payment_voucher_id' => $value])->delete();
                    $this->M_payment_voucher_projects->where(['payment_voucher_id' => $value])->delete();
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Payment voucher successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function updatePaymentRequest($id = false, $is_paid = 0, $is_complete = 0)
    {
        $data = array(
            'is_paid' => $is_paid,
            'is_complete' => $is_complete,
        );

        $this->M_payment_request->update($data, array('id' => $id));
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            // $this->view_data['info'] = $info = $this->M_payment_voucher->with_requests()->with_payable_items()->with_entry_items()->with_item()->as_array()->get($id);

            $this->view_data['info'] = $info = $this->M_payment_voucher->with_requests()->with_project()
                ->with_payable_type()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_items()
                ->with_entry_item()
                ->get($id);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->view_data['company'] = $this->M_company->get(1);

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }

    //Cheque Voucher Stuff

    public function generate_cv($pv_id = 0, $id = 0)
    {
        $pv = $this->M_payment_voucher->get($pv_id);
        if (!$pv) {
            redirect('payment_voucher');
        }
        $method = "Generate";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {
            // vdebug($this->input->post());
            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->cv_fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $reference = ['reference' => uniqidCV()];
                    $cheque_voucher = $this->M_cheque_voucher->update($info + $reference + $additional);
                    $info_cv = ['cheque_voucher_id' => $cheque_voucher];
                    $payment_voucher = $this->M_payment_voucher->update($info_cv + $additional, $pv_id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];
                    $reference = ['reference' => uniqidCV()];
                    $cheque_voucher = $this->M_cheque_voucher->insert($info + $reference + $additional);
                    $info_cv = ['cheque_voucher_id' => $cheque_voucher];
                    $payment_voucher = $this->M_payment_voucher->update($info_cv + $additional, $pv_id);
                }

                if ($cheque_voucher) {
                    $response['status'] = 1;
                    $response['message'] = 'Cheque Voucher Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_cheque_voucher->get($id);
        }

        $this->view_data['method'] = $method;
        $this->view_data['pv_id'] = $pv_id;
        $this->template->build('generate_cv_form', $this->view_data);
    }

    public function print_cv($id = false, $debug = 0)
    {
        if ($id) {

            // $this->view_data['info'] = $info = $this->M_payment_voucher->with_requests()->with_payable_items()->with_entry_items()->with_item()->as_array()->get($id);

            $this->view_data['pv'] = $info_pv = $this->M_payment_voucher->with_requests()->with_project()
                ->with_payable_type()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_items()
                ->with_entry_item()
                ->get($id);

            $this->view_data['cv'] = $info_cv = $this->M_cheque_voucher->get($info_pv['check_id']);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->view_data['company'] = $this->M_company->get(1);

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('cheque_voucher/printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }
    public function print_cheque($id)
    {
        $this->load->library('excel');
        $info_pv = $this->M_payment_voucher->with_requests()->with_project()
            ->with_payable_type()
            ->with_property()
            ->with_approving_department()
            ->with_requesting_department()
            ->with_list_items()
            ->with_entry_items()
            ->with_entry_item()
            ->get($id);

        $info_cv = $this->M_cheque_voucher->get($info_pv['cheque_voucher_id']);

        $payee_id = get_person($info_pv['payee_type_id'], $info_pv['payee_type']);

        $payee_name = strtoupper(get_fname($payee_id));

        if ($info_cv) {
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();
            //Set paper size and orientation
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_NO10_ENVELOPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
            $sheet = $objPHPExcel->getActiveSheet();
            //assigning data to row
            $date_released = date('d F, Y', strtotime($info_pv['paid_date']));

            $payee_name = '***' . $payee_name . '***';
            $raw_amount = $info_pv['entry_item']['dr_total'];
            // convert number to word
            $amount = explode('.', $raw_amount);
            $word_money = '***' . $this->convert_number($amount[0]);
            if (@$amount[1]) {
                $word_money .= ' and ' . sprintf("%02d", $amount[1]) . '/100 only***';
            } else {
                $word_money .= '***';
            }
            $amounts = '***' . number_format($raw_amount, 2) . '***';

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $sheet->setCellValue('G2', date('m/d/Y', strtotime($date_released)));
            $sheet->setCellValue('C4', $payee_name);
            $objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('G3')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('G4')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
            $sheet->setCellValue('G4', $amounts);
            $sheet->getStyle("G4")->getNumberFormat()->setFormatCode('0.00');
            $objPHPExcel->getActiveSheet()->getStyle('G6')->getFont()->setBold(true);
            $sheet->setCellValue('C6', $word_money);
            $sheet->setCellValue('B11', $info_cv['reference'] . ' #' . $info_cv['cheque_number']);


            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_end_clean();
            $date = date('YmdHis');
            $filename = "Voucher Cheque - " . $payee_name . '-' . $date;
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            header('Cache-Control: max-age=0');

            $objWriter->save('php://output');
        }
    }

    function convert_number($number)
    {
        if (($number < 0) || ($number > 999999999)) {
            throw new Exception("Number is out of range");
        }
        $Gn = floor($number / 1000000);
        /* Millions (giga) */
        $number -= $Gn * 1000000;
        $kn = floor($number / 1000);
        /* Thousands (kilo) */
        $number -= $kn * 1000;
        $Hn = floor($number / 100);
        /* Hundreds (hecto) */
        $number -= $Hn * 100;
        $Dn = floor($number / 10);
        /* Tens (deca) */
        $n = $number % 10;
        /* Ones */
        $res = "";
        if ($Gn) {
            $res .= $this->convert_number($Gn) .  "Million";
        }
        if ($kn) {
            $res .= (empty($res) ? "" : " ") . $this->convert_number($kn) . " Thousand";
        }
        if ($Hn) {
            $res .= (empty($res) ? "" : " ") . $this->convert_number($Hn) . " Hundred";
        }
        $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
        $tens = array("", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");
        if ($Dn || $n) {
            if (!empty($res)) {
                $res .= " ";
            }
            if ($Dn < 2) {
                $res .= $ones[$Dn * 10 + $n];
            } else {
                $res .= $tens[$Dn];
                if ($n) {
                    $res .= " " . $ones[$n];
                }
            }
        }
        if (empty($res)) {
            $res = "zero";
        }
        return $res;
    }
}
