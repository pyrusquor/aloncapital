<?php
$payment_request_id = isset($payment_request_id) && $payment_request_id ? $payment_request_id : '';
$payment_voucher_id = isset($payment_voucher_id) && $payment_voucher_id ? $payment_voucher_id : '';

$reference = isset($payment_voucher['reference']) && $payment_voucher['reference'] ? $payment_voucher['reference'] : '';
$payable_type_id = isset($payment_voucher['payable_type_id']) && $payment_voucher['payable_type_id'] ? $payment_voucher['payable_type_id'] : '';
$origin_id = isset($payment_voucher['origin_id']) && $payment_voucher['origin_id'] ? $payment_voucher['origin_id'] : '';
$payee_type = isset($payment_voucher['payee_type']) && $payment_voucher['payee_type'] ? $payment_voucher['payee_type'] : '';
$payee_type_id = isset($payment_voucher['payee_type_id']) && $payment_voucher['payee_type_id'] ? $payment_voucher['payee_type_id'] : '';
$date_requested = isset($payment_voucher['date_requested']) && $payment_voucher['date_requested'] ? $payment_voucher['date_requested'] : date("Y-m-d");
$project_id = isset($payment_voucher['project_id']) && $payment_voucher['project_id'] ? $payment_voucher['project_id'] : '';
$property_id = isset($payment_voucher['property_id']) && $payment_voucher['property_id'] ? $payment_voucher['property_id'] : '';
$approving_department_id = isset($payment_voucher['approving_department_id']) && $payment_voucher['approving_department_id'] ? $payment_voucher['approving_department_id'] : '';
$approving_staff_id = isset($payment_voucher['approving_staff_id']) && $payment_voucher['approving_staff_id'] ? $payment_voucher['approving_staff_id'] : '';
$requesting_department_id = isset($payment_voucher['requesting_department_id']) && $payment_voucher['requesting_department_id'] ? $payment_voucher['requesting_department_id'] : '';
$requesting_staff_id = isset($payment_voucher['requesting_staff_id']) && $payment_voucher['requesting_staff_id'] ? $payment_voucher['requesting_staff_id'] : '';
$prepared_by = isset($payment_voucher['prepared_by']) && $payment_voucher['prepared_by'] ? $payment_voucher['prepared_by'] : '';
$due_date = isset($payment_voucher['due_date']) && $payment_voucher['due_date'] ? $payment_voucher['due_date'] : date("Y-m-d");
$land_inventory_id = isset($payment_voucher['land_inventory_id']) && $payment_voucher['land_inventory_id'] ? $payment_voucher['land_inventory_id'] : '';

$payable_name = isset($payment_voucher['payable_type']['name']) && $payment_voucher['payable_type']['name'] ? $payment_voucher['payable_type']['name'] : 'N/A';
$department_name = isset($payment_voucher['department']['name']) && $payment_voucher['department']['name'] ? $payment_voucher['department']['name'] : 'N/A';
$staff_name = isset($payment_voucher['staff']['name']) && $payment_voucher['staff']['name'] ? $payment_voucher['staff']['name'] : 'N/A';
$project_name = isset($payment_voucher['project']['name']) && $payment_voucher['project']['name'] ? $payment_voucher['project']['name'] : 'N/A';
$property_name = isset($payment_voucher['property']['name']) && $payment_voucher['property']['name'] ? $payment_voucher['property']['name'] : 'N/A';
$approving_department_name = isset($payment_voucher['approving_department']['name']) && $payment_voucher['approving_department']['name'] ? $payment_voucher['approving_department']['name'] : 'N/A';
$requesting_department_name = isset($payment_voucher['requesting_department']['name']) && $payment_voucher['requesting_department']['name'] ? $payment_voucher['requesting_department']['name'] : 'N/A';

if (isset($payee_type_id) && !empty($payee_type_id)) {
    $payee_info = get_person($payee_type_id, $payee_type);
    $payee_name = get_fname($payee_info);
}

if (isset($approving_staff_id) && !empty($approving_staff_id)) {
    $approving_staff_info = get_person($approving_staff_id, 'staff');
    $approving_staff_name = get_fname($approving_staff_info);
}

if (isset($prepared_by) && !empty($prepared_by)) {
    $prepared_by_info = get_person($prepared_by, 'staff');
    $prepared_by_name = get_fname($prepared_by_info);
}

if (isset($requesting_staff_id) && !empty($requesting_staff_id)) {
    $requesting_staff_id_info = get_person($requesting_staff_id, 'staff');
    $requesting_staff_name = get_fname($requesting_staff_id_info);
}
?>

<?php if (empty($payment_request)) : ?>
    <input type="hidden" name="payment_request_id" value="<?= $payment_request_id; ?>">
    <input type="hidden" name="payment_voucher_id" value="<?= $payment_voucher_id; ?>">

    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Origin</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control" placeholder="Origin" id="origin_id" name="voucher[origin_id]" value="<?= $origins ?>" autocomplete="off">
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-bookmark"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Payable Type <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="payable_types" id="payable_type_id" name="voucher[payable_type_id]">
                            <option value="">Select Payable Type</option>
                            <?php if ($payable_name) : ?>
                                <option value="<?php echo $payable_type_id; ?>" selected><?php echo $payable_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Project Name <span class="kt-font-danger"></span></label>
                        <select class="form-control suggests-multiple" data-module="projects" id="project_id" name="project_id[]">
                            <?php if ($project_name) : ?>
                                <option value="<?php echo $project_id; ?>" selected><?php echo $project_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Property Name <span class="kt-font-danger"></span></label>
                        <select class="form-control suggests-multiple" data-module="properties" id="property_id" name="property_id[]">
                            <?php if ($property_name) : ?>
                                <option value="<?php echo $property_id; ?>" selected><?php echo $property_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Payee Type <span class="kt-font-danger">*</span></label>
                        <?php echo form_dropdown('voucher[payee_type]', Dropdown::get_static('payee_type'), set_value('$voucher[payee_type]', @$payee_type), 'class="form-control" id="payee_type"'); ?>
                    </div>
                </div>


                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Land Inventory <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon">
                            <input type="text" class="form-control" placeholder="Land Inventory" name="voucher[land_inventory_id]" value="<?php echo set_value('land_inventory_id"', @$land_inventory_id); ?>">
                            <span class="kt-input-icon__icon"></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Date of Request <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control kt_datepicker" placeholder="Date of Request" name="voucher[date_requested]" value="<?php echo set_value('date_requested"', @$date_requested); ?>" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Payee <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-type="person" id="payee_type_id" name="voucher[payee_type_id]">
                            <option value="">Select Payee</option>
                            <?php if ($payee_name) : ?>
                                <option value="<?php echo $payee_type_id; ?>" selected><?php echo $payee_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Approving Department <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="departments" id="approving_department_id" name="voucher[approving_department_id]">
                            <option value="">Select Property</option>
                            <?php if ($approving_department_name) : ?>
                                <option value="<?php echo $approving_department_id; ?>" selected><?php echo $approving_department_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Request Due Date <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control kt_datepicker" placeholder="Request Due Date" name="voucher[due_date]" value="<?php echo set_value('due_date"', @$due_date); ?>" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Requesting Department <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="departments" id="requesting_department_id" name="voucher[requesting_department_id]">
                            <option value="">Select Department</option>
                            <?php if ($requesting_department_name) : ?>
                                <option value="<?php echo $requesting_department_id; ?>" selected><?php echo $requesting_department_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id" name="voucher[approving_staff_id]">
                            <option value="">Select Property</option>
                            <?php if ($approving_staff_name) : ?>
                                <option value="<?php echo $approving_staff_id; ?>" selected><?php echo $approving_staff_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Prepared by <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="prepared_by" name="voucher[prepared_by]">
                            <option value="">Select Staff</option>
                            <?php if ($prepared_by_name) : ?>
                                <option value="<?php echo $prepared_by; ?>" selected><?php echo $prepared_by_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="requesting_staff_id" name="voucher[requesting_staff_id]">
                            <option value="">Select Staff</option>
                            <?php if ($requesting_staff_name) : ?>
                                <option value="<?php echo $requesting_staff_id; ?>" selected><?php echo $requesting_staff_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php else : ?>

    <?php

    $payment_request_id = isset($payment_request_id) && $payment_request_id ? $payment_request_id : '';
    $payment_voucher_id = isset($payment_voucher_id) && $payment_voucher_id ? $payment_voucher_id : '';

    $reference = isset($payment_request['reference']) && $payment_request['reference'] ? $payment_request['reference'] : '';
    $payable_type_id = isset($payment_request['payable_type_id']) && $payment_request['payable_type_id'] ? $payment_request['payable_type_id'] : '';
    $origin_id = isset($payment_request['origin_id']) && $payment_request['origin_id'] ? $payment_request['origin_id'] : '';
    $payee_type = isset($payment_request['payee_type']) && $payment_request['payee_type'] ? $payment_request['payee_type'] : '';
    $payee_type_id = isset($payment_request['payee_type_id']) && $payment_request['payee_type_id'] ? $payment_request['payee_type_id'] : '';
    $date_requested = isset($payment_request['date_requested']) && $payment_request['date_requested'] ? $payment_request['date_requested'] : date("Y-m-d");
    $approving_department_id = isset($payment_request['approving_department_id']) && $payment_request['approving_department_id'] ? $payment_request['approving_department_id'] : '';
    $approving_staff_id = isset($payment_request['approving_staff_id']) && $payment_request['approving_staff_id'] ? $payment_request['approving_staff_id'] : '';
    $requesting_department_id = isset($payment_request['requesting_department_id']) && $payment_request['requesting_department_id'] ? $payment_request['requesting_department_id'] : '';
    $requesting_staff_id = isset($payment_request['requesting_staff_id']) && $payment_request['requesting_staff_id'] ? $payment_request['requesting_staff_id'] : '';
    $prepared_by = isset($payment_request['prepared_by']) && $payment_request['prepared_by'] ? $payment_request['prepared_by'] : '';
    $due_date = isset($payment_request['due_date']) && $payment_request['due_date'] ? $payment_request['due_date'] : '';
    $land_inventory_id = isset($payment_request['land_inventory_id']) && $payment_request['land_inventory_id'] ? $payment_request['land_inventory_id'] : '';

    $payable_name = isset($payment_request['payable_type']['name']) && $payment_request['payable_type']['name'] ? $payment_request['payable_type']['name'] : 'N/A';
    $department_name = isset($payment_request['department']['name']) && $payment_request['department']['name'] ? $payment_request['department']['name'] : 'N/A';
    $staff_name = isset($payment_request['staff']['name']) && $payment_request['staff']['name'] ? $payment_request['staff']['name'] : 'N/A';
    $project_name = isset($payment_request['project']['name']) && $payment_request['project']['name'] ? $payment_request['project']['name'] : 'N/A';
    $property_name = isset($payment_request['property']['name']) && $payment_request['property']['name'] ? $payment_request['property']['name'] : 'N/A';
    $approving_department_name = isset($payment_request['approving_department']['name']) && $payment_request['approving_department']['name'] ? $payment_request['approving_department']['name'] : 'N/A';
    $requesting_department_name = isset($payment_request['requesting_department']['name']) && $payment_request['requesting_department']['name'] ? $payment_request['requesting_department']['name'] : 'N/A';

    if (isset($payee_type_id) && !empty($payee_type_id)) {
        $payee_info = get_person($payee_type_id, $payee_type);
        $payee_name = get_fname($payee_info);
    }

    if (isset($approving_staff_id) && !empty($approving_staff_id)) {
        $approving_staff_info = get_person($approving_staff_id, 'staff');
        $approving_staff_name = get_fname($approving_staff_info);
    }

    if (isset($prepared_by) && !empty($prepared_by)) {
        $prepared_by_info = get_person($prepared_by, 'staff');
        $prepared_by_name = get_fname($prepared_by_info);
    }

    if (isset($requesting_staff_id) && !empty($requesting_staff_id)) {
        $requesting_staff_id_info = get_person($requesting_staff_id, 'staff');
        $requesting_staff_name = get_fname($requesting_staff_id_info);
    }

    ?>

    <input type="hidden" id='payment_request' name="payment_request_id" value="<?= $payment_request_id; ?>">
    <input type="hidden" id='projects' value="<?= implode(',',array_column($payment_request['projects'],'project_id')); ?>">
    <input type="hidden" id='properties' value="<?= implode(',',array_column($payment_request['properties'],'property_id')); ?>">
    <input type="hidden" name="payment_voucher_id" value="<?= $payment_voucher_id; ?>">

    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Origin</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control" placeholder="Origin" id="origin_id" name="voucher[origin_id]" value="<?= implode(',',$origins) ?>" autocomplete="off" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-bookmark"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Payable Type <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="payable_types" id="payable_type_id" name="voucher[payable_type_id]" readonly>
                            <option value="">Select Payable Type</option>
                            <?php if ($payable_name) : ?>
                                <option value="<?php echo $payable_type_id; ?>" selected><?php echo $payable_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Project Name <span class="kt-font-danger"></span></label>
                        <select class="form-control suggests-multiple" data-module="projects" id="project_id" name="project_id[]" readonly>

                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Property Name <span class="kt-font-danger"></span></label>
                        <select class="form-control suggests-multiple" data-module="properties" id="property_id" name="property_id[]" readonly>
                            <?php if ($property_name) : ?>
                                <option value="<?php echo $property_id; ?>" selected><?php echo $property_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Payee Type <span class="kt-font-danger">*</span></label>
                        <?php echo form_dropdown('voucher[payee_type]', Dropdown::get_static('payee_type'), set_value('$voucher[payee_type]', @$payee_type), 'class="form-control" id="payee_type" readonly'); ?>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Land Inventory <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon">
                            <input type="text" class="form-control" placeholder="Land Inventory" name="voucher[land_inventory_id]" value="<?php echo set_value('land_inventory_id"', @$land_inventory_id); ?>">
                            <span class="kt-input-icon__icon"></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Date of Request <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control kt_datepicker" placeholder="Date of Request" name="voucher[date_requested]" value="<?php echo set_value('date_requested"', @$date_requested); ?>" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Payee <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-type="person" id="payee_type_id" name="voucher[payee_type_id]" readonly>
                            <option value="">Select Payee</option>
                            <?php if ($payee_name) : ?>
                                <option value="<?php echo $payee_type_id; ?>" selected><?php echo $payee_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Approving Department <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="departments" id="approving_department_id" name="voucher[approving_department_id]" readonly>
                            <option value="">Select Property</option>
                            <?php if ($approving_department_name) : ?>
                                <option value="<?php echo $approving_department_id; ?>" selected><?php echo $approving_department_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Request Due Date <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control kt_datepicker" placeholder="Request Due Date" name="voucher[due_date]" value="<?php echo set_value('due_date"', @$due_date); ?>" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Requesting Department <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="departments" id="requesting_department_id" name="voucher[requesting_department_id]" readonly>
                            <option value="">Select Department</option>
                            <?php if ($requesting_department_name) : ?>
                                <option value="<?php echo $requesting_department_id; ?>" selected><?php echo $requesting_department_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id" name="voucher[approving_staff_id]" readonly>
                            <option value="">Select Property</option>
                            <?php if ($approving_staff_name) : ?>
                                <option value="<?php echo $approving_staff_id; ?>" selected><?php echo $approving_staff_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Prepared by <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="prepared_by" name="voucher[prepared_by]" readonly>
                            <option value="">Select Staff</option>
                            <?php if ($prepared_by_name) : ?>
                                <option value="<?php echo $prepared_by; ?>" selected><?php echo $prepared_by_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="requesting_staff_id" name="voucher[requesting_staff_id]" readonly>
                            <option value="">Select Staff</option>
                            <?php if ($requesting_staff_name) : ?>
                                <option value="<?php echo $requesting_staff_id; ?>" selected><?php echo $requesting_staff_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>