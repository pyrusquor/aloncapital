<?php
$id = isset($info['id']) && $info['id'] ? $info['id'] : "";
$reference = isset($info['reference']) && $info['reference'] ? $info['reference'] : "N/A";
$origin_id = isset($info['origin_id']) && $info['origin_id'] ? $info['origin_id'] : "N/A";
$payee_type = isset($info['payee_type']) && $info['payee_type'] ? $info['payee_type'] : "N/A";
$payee_type_id = isset($info['payee_type_id']) && $info['payee_type_id'] ? $info['payee_type_id'] : "N/A";
$gross_amount = isset($info['gross_amount']) && $info['gross_amount'] ? $info['gross_amount'] : "N/A";
$tax_amount = isset($info['tax_amount']) && $info['tax_amount'] ? $info['tax_amount'] : "N/A";
$tax_percentage = isset($info['tax_percentage']) && $info['tax_percentage'] ? $info['tax_percentage'] : "N/A";
$net_amount = isset($info['net_amount']) && $info['net_amount'] ? $info['net_amount'] : "N/A";
$payment_voucher_id = isset($info['payment_voucher_id']) && $info['payment_voucher_id'] ? $info['payment_voucher_id'] : "N/A";
$date_requested = isset($info['date_requested']) && $info['date_requested'] ? view_date($info['date_requested']) : "N/A";
$due_date = isset($info['due_date']) && $info['due_date'] ? view_date($info['due_date']) : "N/A";
$particulars = isset($info['particulars']) && $info['particulars'] ? $info['particulars'] : "N/A";
$vouchers = isset($info['vouchers']) && $info['vouchers'] ? $info['vouchers'] : "N/A";
$is_paid = ($info['is_paid'] ? "Paid" : "Unpaid");
$is_complete = ($info['is_complete'] ? "Completed" : "Incomplete");
$created_date = view_date($info['created_at']);
$updated_date = view_date($info['updated_at']);
$encoded_by = $info['created_by'];
$updated_by = $info['updated_by'];
$payment_status = ($info['is_paid'] ? "kt-badge--success" : "kt-badge--danger");
$status = ($info['is_complete'] ? "kt-badge--success" : "kt-badge--danger");

$payable_type_id = ($info['payable_type_id'] ? Dropdown::get_dynamic('payable_types', $info['payable_type_id'], 'name', 'id', 'view') : "N/A");
$project = ($info['project_id'] ? Dropdown::get_dynamic('projects', $info['project_id'], 'name', 'id', 'view') : "N/A");
$property = ($info['property_id'] ? Dropdown::get_dynamic('properties', $info['property_id'], 'name', 'id', 'view') : "N/A");
$approving_department = ($info['approving_department_id'] ? Dropdown::get_dynamic('departments', $info['approving_department_id'], 'name', 'id', 'view') : "N/A");
$requesting_department = ($info['requesting_department_id'] ? Dropdown::get_dynamic('departments', $info['requesting_department_id'], 'name', 'id', 'view') : "N/A");

$approving_staff_id = get_person($info['approving_staff_id'], 'staff');
$approving_staff = get_fname($approving_staff_id);

$prepared_by_id = get_person($info['prepared_by'], 'staff');
$prepared_by_name = get_fname($prepared_by_id);

$requesting_staff_id = get_person($info['requesting_staff_id'], 'staff');
$requesting_staff_name = get_fname($requesting_staff_id);

$payee = get_person($payee_type_id, $payee_type);
$payee_name = get_fname($payee);

$encoder = get_person($encoded_by, 'staff');
$encoder_name = get_fname($encoder);

$updater = get_person($updated_by, 'staff');
$updater_name = get_fname($updater);
?>

<div class="row">
    <div class="col-sm-12">
        <!-- General Information -->
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
                <div class="card">
                    <div class="card-header" id="head_general_information">
                        <div class="card-title" data-toggle="collapse" data-target="#general_information"
                             aria-expanded="true" aria-controls="general_information">
                            General Information
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24"/>
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
                                          id="Path-94" fill="#000000" fill-rule="nonzero"/>
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
                                          id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3"
                                          transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                    <div id="general_information" class="collapse show" aria-labelledby="head_general_information"
                         data-parent="#accord_general_information">
                        <div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Reference #</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $reference; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Origin</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $origin_id; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Project Name</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $project; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Payable Type</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $payable_type_id; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Property Name</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $property; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Payee Type</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo strtoupper($payee_type); ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Approving Department</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $approving_department; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Payee</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $payee_name; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Approving Staff</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $approving_staff; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Date of Request</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $date_requested; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Due Date</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $due_date; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Requesting Department</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $requesting_department; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Prepared By</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $prepared_by_name; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <div href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h6 class="kt-portlet__head-title kt-font-primary">Requesting Staff</h6>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?php echo $requesting_staff_name; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>