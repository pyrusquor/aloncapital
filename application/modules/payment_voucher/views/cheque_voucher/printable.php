<!DOCTYPE html>
<html>

<head>
    <title><?php echo $cv['reference']; ?></title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 15px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th {
            padding: 5px 8px;
        }

        .table.table-borderless>thead>tr>th {
            border: 2px solid #000000;
        }

        .table>tfoot>tr>td {
            border: 2px solid #000000;
        }
        .table>tbody>tr>td {
            border-left: 2px solid #000000;
            border-right: 2px solid #000000;
        }

        .table>tfoot {
            border-top: 2px solid #000000;
        }

        .table>tbody {
            border-bottom: 1px solid #000000;
        }

        .table.table-striped>thead>tr>th {
            background: #00529c;
            color: #fff;
            border: 0;
            padding: 12px 8px;
            text-transform: uppercase;
        }

        .table.table-striped>thead>tr>th a {
            color: #fff;
            font-weight: 400;
        }

        .table.table-striped>thead>tr:nth-child(2)>th {
            background: #0075de;
        }

        .table.table-striped td {
            border: 0;
            vertical-align: middle;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background: #fff;
        }

        .table-striped>tbody>tr:nth-of-type(even) {
            background: #f1f1f1;
        }

        .color-bluegreen {
            color: #169f98;
        }

        .color-white {
            color: #fff;
        }

        .peso_currency {
            font-family: DejaVu Sans;
        }

        .bg-bluegreen {
            background-color: #169f98;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .margin0 {
            margin: 0;
        }

        .padding10 {
            padding: 10px;
        }

        p {
            margin: 0 0 15px;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-warning {
            background-color: #fcf8e3;
            border-color: #faebcc;
            color: #8a6d3b;
        }

        input[type="text"],
        select.form-control {
            background: transparent;
            border: none;
            border-bottom: 1px solid #000000;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        input[type="text"]:focus,
        select.form-control:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>

<body>
    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <?php if (isset($company['header'])) : ?>
                <!-- <img src="<?php echo base_url(get_image('company', 'header', $company['header'])) ?>" width="100%" height="150px" style="margin-left: -15px" /> -->
                <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'header', $company['header'])); ?>" style="display:block;margin:auto" width="950px" height="150px" style='position:absolute;margin-bottom:-70px' />
            <?php else : ?>
                <h2> <?php echo $company['name']; ?> </h2>
            <?php endif ?>
            <div class="kt-portlet__body m-5 p-5">

                <?php
                // vdebug($pv);
                $payable_items = $pv['list_items'];
                $entry_items = $pv['entry_items'];
                $entry_item = $pv['entry_item'];

                $payment_type_id = $pv['payment_type_id'];
                $particulars = $pv['particulars'] ? $pv['particulars'] : "None";

                $payee_email_ref = get_person($pv['payee_type_id'], 'users');
                $payee_id = get_person($pv['payee_type_id'], $pv['payee_type']);

                $payee_name = get_fname($payee_id);
                $payee_email = get_email($payee_email_ref) ? get_email($payee_email_ref) : "N/A";

                $bank_object = get_person($cv['bank_id'], 'banks');
                $bank_name = get_name($bank_object) ? get_name($bank_object) : 'N/A';

                ?>
                <div class="d-flex justify-content-between">
                    <div class="col-md-5">
                        <h4> <?php echo $company['location']; ?> </h4>

                    </div>
                    <div class="col-md-5" style="text-align: right">
                        <h2><span class="text-danger"><?php echo $cv['reference']; ?></span> </h2>
                    </div>
                </div>
                <hr>

                <center>
                    <h4 class='kt-font-boldest'> CHEQUE VOUCHER </h4>
                </center>
                <div class="d-flex justify-content-betweenpy-5">
                    <!-- <div class="col-md-5">
                            <h2>Payment to:</h2>
                            <div class="border p-4">
                                <h4>
                                    <?php echo $payee_name; ?><br>
                                    <?php echo $payee_email; ?>
                                </h4>
                            </div>
                        </div> -->
                </div>
                <div class="py-5">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th class="font-weight-bold text-center" scope="col">
                                    CODE
                                </th>
                                <th class="font-weight-bold text-center" scope="col">
                                    ACCOUNT TITLE
                                </th>
                                <th class="font-weight-bold text-center" scope="col">
                                    DEBIT
                                </th>
                                <th class="font-weight-bold text-center" scope="col">
                                    CREDIT
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($entry_items) : ?>
                                <?php foreach ($entry_items as $key => $e_item) { //vdebug($e_item);
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo get_value_field($e_item['ledger_id'], 'accounting_ledgers', 'account_number'); ?>
                                        </td>
                                        <td>
                                            <!-- Type -->
                                            <?php echo get_value_field($e_item['ledger_id'], 'accounting_ledgers', 'name'); ?>
                                        </td>
                                        <td class='text-right'>
                                            <!-- Dr -->
                                            <?php if (strtolower($e_item['dc']) == "d") : ?>
                                                <?php echo money_php($e_item['amount']) ?>
                                            <?php else: ?>
                                                -
                                            <?php endif ?>
                                        </td>
                                        <!-- Cr -->
                                        <td class='text-right'>
                                            <?php if (strtolower($e_item['dc']) == "c") : ?>
                                                <?php echo money_php($e_item['amount']) ?>
                                            <?php else: ?>
                                                -
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php endif ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class='kt-font-boldest' colspan='2'>TOTAL</td>
                                <td class='kt-font-boldest text-right'><?php echo money_php($entry_item['dr_total']) ?></td>
                                <td class='kt-font-boldest text-right'><?php echo money_php($entry_item['cr_total']) ?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <table class="table table-borderless"style="margin-top:-56px">
                    <thead>
                        <tr>
                            <th class='text-center' colspan='2'>TREASURY DEPARTMENT </th>
                            <th class='text-center' colspan='2'>CASH DEPARTMENT</th>
                            <th class='text-center'>APPROVED BY</th>
                            <th class='text-center'>AUDITED BY</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class='text-center'>
                                <br>
                                <br>
                                JOHN PAUL CARAGOS<br>
                                <span class='kt-font-boldest'>PREPARED BY</span>
                            </td>
                            <td class='text-center'>
                                <br>
                                <br>
                                JANINE ANGOR<br>
                                <span class='kt-font-boldest'>CHECKED BY</span>
                            </td>
                            <td class='text-center'>
                                <br>
                                <br>
                                JOHN PAUL CARAGOS<br>
                                <span class='kt-font-boldest'>PREPARED BY</span>
                            </td>
                            <td class='text-center'>
                                <br>
                                <br>
                                ELYTA ALCAYAGA<br>
                                <span class='kt-font-boldest'>CHECKED BY</span>
                            </td>
                            <td class='text-center'>
                                <br>
                                <br>
                                <br>
                                HYACINTH VITERBO<br>
                            </td>
                            <td class='text-center'>
                                <br>
                                <br>
                                <br>
                                MARK RANDY ARAQUE
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                            <td colspan='6'>
                                <br>
                                <h6 class='text-uppercase'style="word-wrap:break-word"><span class='kt-font-boldest'>Paid to:&nbsp;&nbsp;&nbsp;</span>*** <?= $payee_name ?> *** <span style='float:right;'> <strong> Date: </strong><?= date('m/d/Y',strtotime(@$pv['paid_date'])) ?> </span> </h6>
                                <br>
                                <h6 class='text-uppercase'style="word-wrap:break-word"><span class='kt-font-boldest'>Amount in words:&nbsp;&nbsp;&nbsp;</span>*** <?=  number_to_words($entry_item['cr_total']) ?> *** <span style='float:right;'> <strong> <?=  money_php($entry_item['cr_total']) ?> </strong></h6>
                                <br>
                                <br>
                                <h6 class='text-uppercase' style="word-wrap:break-word">
                                    &nbsp;&nbsp;&nbsp;Bank:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $bank_name ?>
                                    <span style='float:right;'>
                                        Received from <?= $company['name']; ?>
                                    </span>
                                </h6>
                                <br>
                                <h6 class='text-uppercase' style="word-wrap:break-word">
                                &nbsp;&nbsp;&nbsp;Check No.:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $cv['cheque_number'] ?>
                                    <span style='float:right;'>
                                        <?= money_php($entry_item['cr_total']) ?> in settlement of the above account
                                    </span>
                                </h6>
                                <br>
                                <h6 class='text-uppercase' style="word-wrap:break-word">
                                &nbsp;&nbsp;&nbsp;Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= date('m/d/Y',strtotime(@$pv['paid_date'])) ?>
                                    <span style='float:right;'>
                                        By:___________________________ Date:____________________
                                    </span>
                                </h6>
                                <h6 class='text-uppercase text-center' style="word-wrap:break-word">
                                    Please attach supporting documents
                                </h6>
                                <br>

                            </td>
                        </tr>
                        <?php if($cv['remarks']): ?>
                        <tr>
                            <td colspan='6'>
                                <?= $cv['remarks'] ?>
                            </td>
                        </tr>
                        <?php endif ?>
                        </tfoot>
                </table>
                <hr>
            </div>
        </div>
    </div>
</body>

</html>