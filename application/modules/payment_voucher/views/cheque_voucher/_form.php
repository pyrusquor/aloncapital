<?php
// ==================== begin: Add model fields ====================
// $id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$pv_id = isset($pv_id) && $pv_id ? $pv_id : '';
// $company_id = isset($info['company']['id']) && $info['company']["id"] ? $info['company']["id"] : '';
// $company_name = isset($info['company']['name']) && $info['company']["name"] ? $info['company']["name"] : '';
// $payee_type = isset($info['payee_type']) && $info['payee_type'] ? $info['payee_type'] : '';
// $payee_id = isset($info['payee_id']) && $info['payee_id'] ? $info['payee_id'] : '';
// $payee_name = isset($info['payee_name']) && $info['payee_name'] ? $info['payee_name'] : '';
// $payment_date = isset($info['payment_date']) && $info['payment_date'] ? $info['payment_date'] : date('Y-m-d');
// $payment_amount = isset($info['payment_amount']) && $info['payment_amount'] ? $info['payment_amount'] : '';
// $payment_type_id = isset($info['payment_type_id']) && $info['payment_type_id'] ? $info['payment_type_id'] : '';
// $receipt_type = isset($info['receipt_type']) && $info['receipt_type'] ? $info['receipt_type'] : '';
// $bank = isset($info['bank']) && $info['bank'] ? $info['bank'] : '';
// $cheque_number = isset($info['cheque_number']) && $info['cheque_number'] ? $info['cheque_number'] : '';
// $receipt_number = isset($info['receipt_number']) && $info['receipt_number'] ? $info['receipt_number'] : '';
// $remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label>Cheque Voucher Number <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="cheque_voucher_number" id="cheque_voucher_number" value="<?php echo $payment_amount;  ?>" placeholder="" autocomplete="off">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>



    <div class="col-sm-6">
        <div class="form-group">
            <label>Cheque Number <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="cheque_number" id="cheque_number" value="<?php echo $cheque_number;  ?>" placeholder="" autocomplete="off">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Bank<span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="banks" id="bank_id" name="bank_id">
                <option value="">Select Bank</option>
                <?php if ($company_id) : ?>
                    <option value="<?php echo @$company_id; ?>" selected><?php echo $company_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Remarks</label>
            <!-- <div class="col-sm-12"> -->
                <input type="text" class="form-control" id="remarks" placeholder="Remarks" name="remarks" value="<?php echo $remarks;  ?>">
            <!-- </div> -->
        </div>
    </div>

    <input type="hidden" class="form-control" name="payment_voucher_id" id="payment_voucher_id" value="<?php echo $pv_id;  ?>" placeholder="" autocomplete="off">

    <!-- ==================== end: Add form model fields ==================== -->
</div>