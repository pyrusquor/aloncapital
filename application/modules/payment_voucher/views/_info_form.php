<?php
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$reference = isset($info['reference']) && $info['reference'] ? $info['reference'] : '';
$payment_request_id = isset($payment_request['id']) && $payment_request['id'] ? $payment_request['id'] : '';
$payment_type_id = isset($info['payment_type_id']) && $info['payment_type_id'] ? $info['payment_type_id'] : '';
$paid_amount = isset($info['paid_amount']) && $info['paid_amount'] ? $info['paid_amount'] : '';
$check_id = isset($info['check_id']) && $info['check_id'] ? $info['check_id'] : '';
$paid_date = isset($info['paid_date']) && $info['paid_date'] ? $info['paid_date'] : date("Y-m-d");
$request_id = isset($info['payment_request_id']) && $info['payment_request_id'] ? $info['payment_request_id'] : '';
$payment_reference = isset($payment_request['reference']) && $payment_request['reference'] ? $payment_request['reference'] : 'N/A';

$check_branch = isset($info['check']['branch']) && $info['check']['branch'] ? $info['check']['branch'] : '';
?>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Payment Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('voucher[payment_type_id]', Dropdown::get_static('payment_types'), set_value('$voucher[payment_type_id]', @$payment_type_id), 'class="form-control" id="payment_type_id"'); ?>
                </div>
            </div>
            <!-- </div> -->

            <!-- <div class="row"> -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Bank</label>

                    <select class="form-control suggests" data-module="banks" id="bank_id" name="bank_id">
                        <option value="">Select Bank</option>
                        <?php if ($company_id) : ?>
                            <option value="<?php echo @$company_id; ?>" selected><?php echo $company_name; ?></option>
                        <?php endif ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Paid Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="paid_amount" placeholder="Paid Amount" name="voucher[paid_amount]" value="<?= $payment_request['total_due_amount'] ?>">
                        </div>
                    </div>
                </div>
            </div>
            <!-- </div> -->

            <!-- <div class="row"> -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Paid Date <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control kt_datepicker" placeholder="Paid Date" name="voucher[paid_date]" value="<?php echo set_value('paid_date"', @$paid_date); ?>" readonly>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Cheque Number</label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" placeholder="Cheque Number" name="cheque_number" id="cheque_number">
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Remarks</label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" placeholder="Cheque Voucher Remarks" name="remarks" id="remarks">
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

    </div>
</div>