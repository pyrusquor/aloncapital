<?php
    $gross_amount = isset($payment_voucher['gross_amount']) && $payment_voucher['gross_amount'] ? $payment_voucher['gross_amount'] : '';
    $tax_amount = isset($payment_voucher['tax_amount']) && $payment_voucher['tax_amount'] ? $payment_voucher['tax_amount'] : '';
    $tax_percentage = isset($payment_voucher['tax_percentage']) && $payment_voucher['tax_percentage'] ? $payment_voucher['tax_percentage'] : '';
    $wht_amount = isset($payment_voucher['wht_amount']) && $payment_voucher['wht_amount'] ? $payment_voucher['wht_amount'] : '';
    $wht_percentage = isset($payment_voucher['wht_percentage']) && $payment_voucher['wht_percentage'] ? $payment_voucher['wht_percentage'] : '';
    $payment_voucher_id = isset($payment_voucher['payment_voucher_id']) && $payment_voucher['payment_voucher_id'] ? $payment_voucher['payment_voucher_id'] : '';
    $is_paid = isset($payment_voucher['is_paid']) && $payment_voucher['is_paid'] ? $payment_voucher['is_paid'] : '';
    $is_complete = isset($payment_voucher['is_complete']) && $payment_voucher['is_complete'] ? $payment_voucher['is_complete'] : '';
    $total_due_amount = isset($payment_voucher['total_due_amount']) && $payment_voucher['total_due_amount'] ? $payment_voucher['total_due_amount'] : '';
    $particulars = isset($payment_voucher['particulars']) && $payment_voucher['particulars'] ? $payment_voucher['particulars'] : '';
?>

<!-- 
a. Gross Amount : 10000
b. VAT Tax Percent : 10%
c. Tax Amount : 1000 (b * a)
d. Withholding Tax Percent : 10%
d. Withholding Tax Amount : 1000 (d * a)
e. Total Due Amount : 10000 (a + c - d) -->

<?php if(empty($payment_request)): ?>

<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">

                <div class="form-group">
                    <label class="">Gross Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="gross_amount" placeholder=""
                                   name="voucher[gross_amount]"
                                   value="<?php echo set_value('gross_amount"', @$gross_amount); ?>">
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="">VAT Tax Percentage</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">%</span></div>
                            <input type="number" class="form-control" id="tax_percentage" placeholder=""
                                   name="voucher[tax_percentage]"
                                   value="<?php echo set_value('tax_percentage"', @$tax_percentage); ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="">Tax Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="vat_tax_amount" placeholder=""
                                   name="voucher[tax_amount]"
                                   value="<?php echo set_value('tax_amount"', @$tax_amount); ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="">Withholding Tax Percentage</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">%</span></div>
                            <input type="number" class="form-control" id="wht_percentage" placeholder=""
                                   name="voucher[wht_percentage]"
                                   value="<?php echo set_value('$wht_percentage"', @$wht_percentage); ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="">Withholding Tax Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="wht_amount" placeholder=""
                                   name="voucher[wht_amount]"
                                   value="<?php echo set_value('wht_amount"', @$wht_amount); ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="">Total Due Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="total_due_amount" placeholder=""
                                   name="voucher[total_due_amount]"
                                   value="<?php echo set_value('total_due_amount"', @$total_due_amount); ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="message">Particulars</label>
                    <textarea type="text" class="form-control" id="particulars" name="voucher[particulars]" placeholder="Particulars" rows="7"><?php echo set_value('particulars', @$particulars); ?></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<?php else: ?>

<?php 
    $gross_amount = isset($payment_request['gross_amount']) && $payment_request['gross_amount'] ? $payment_request['gross_amount'] : '';
    $tax_amount = isset($payment_request['tax_amount']) && $payment_request['tax_amount'] ? $payment_request['tax_amount'] : '';
    $tax_percentage = isset($payment_request['tax_percentage']) && $payment_request['tax_percentage'] ? $payment_request['tax_percentage'] : '';
    $wht_amount = isset($payment_request['wht_amount']) && $payment_request['wht_amount'] ? $payment_request['wht_amount'] : '';
    $wht_percentage = isset($payment_request['wht_percentage']) && $payment_request['wht_percentage'] ? $payment_request['wht_percentage'] : '';
    $is_paid = isset($payment_request['is_paid']) && $payment_request['is_paid'] ? $payment_request['is_paid'] : '';
    $is_complete = isset($payment_request['is_complete']) && $payment_request['is_complete'] ? $payment_request['is_complete'] : '';
    $total_due_amount = isset($payment_request['total_due_amount']) && $payment_request['total_due_amount'] ? $payment_request['total_due_amount'] : '';
    $particulars = isset($payment_request['particulars']) && $payment_request['particulars'] ? $payment_request['particulars'] : '';
    
    ?>

<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">

                <div class="form-group">
                    <label class="">Gross Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="gross_amount" placeholder=""
                                   name="voucher[gross_amount]"
                                   value="<?php echo set_value('gross_amount"', @$gross_amount); ?>" readonly>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="">VAT Tax Percentage</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">%</span></div>
                            <input type="number" class="form-control" id="tax_percentage" placeholder=""
                                   name="voucher[tax_percentage]"
                                   value="<?php echo set_value('tax_percentage"', @$tax_percentage); ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="">Tax Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="vat_tax_amount" placeholder=""
                                   name="voucher[tax_amount]"
                                   value="<?php echo set_value('tax_amount"', @$tax_amount); ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="">Withholding Tax Percentage</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">%</span></div>
                            <input type="number" class="form-control" id="wht_percentage" placeholder=""
                                   name="voucher[wht_percentage]"
                                   value="<?php echo set_value('$wht_percentage"', @$wht_percentage); ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="">Withholding Tax Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="wht_amount" placeholder=""
                                   name="voucher[wht_amount]"
                                   value="<?php echo set_value('wht_amount"', @$wht_amount); ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="">Total Due Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="total_due_amount" placeholder=""
                                   name="voucher[total_due_amount]"
                                   value="<?php echo set_value('total_due_amount"', @$total_due_amount); ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="message">Particulars</label>
                    <textarea type="text" class="form-control" id="particulars" name="voucher[particulars]" placeholder="Particulars" rows="7" readonly><?php echo set_value('particulars', @$particulars); ?></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif ?>

<div class="row">
    <div class="col-sm-12">
        <?php if (isset($commissions)): ?>
            <?php foreach ($commissions as $key => $value): ?>
                <div class="form-group">
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder=""
                                   name="commissions[<?= $key ?>]"
                                   value="<?php echo set_value('commissions[' + $key + ']', @$value); ?>"
                                   readonly hidden>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
