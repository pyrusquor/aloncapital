<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cashier_model extends MY_Model {

    public $table = 'cashier';
    public $primary_key = 'id';
    public $fillable = [
        'id',
        'company_id',
        'payee_type',
        'payee_id',
        'payment_date',
        'payment_type_id',
        'payment_amount',
        'bank',
        'cheque_number',
        'receipt_type',
        'receipt_number',
        'remarks',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'company_id' => array(
            'field' => 'company_id',
            'label' => 'Company',
            'rules' => 'trim|required',
        ),
        'payee_type' => array(
            'field' => 'payee_type',
            'label' => 'Payee Type',
            'rules' => 'trim|required',
        ),
        'payee_id' => array(
            'field' => 'payee_id',
            'label' => 'Payee',
            'rules' => 'trim|required',
        ),
        'payment_date' => array(
            'field' => 'payment_date',
            'label' => 'Payment Date',
            'rules' => 'trim|required',
        ),
        'payment_type_id' => array(
            'field' => 'payment_type_id',
            'label' => 'Payment Type',
            'rules' => 'trim|required',
        ),
        'payment_amount' => array(
            'field' => 'payment_amount',
            'label' => 'Payment Amount',
            'rules' => 'trim|required',
        ),
        'payment_amount' => array(
            'field' => 'payment_amount',
            'label' => 'Bank',
            'rules' => 'trim',
        ),
        'bank' => array(
            'field' => 'bank',
            'label' => 'Bank',
            'rules' => 'trim',
        ),
        'cheque_number' => array(
            'field' => 'cheque_number',
            'label' => 'Cheque Number',
            'rules' => 'trim',
        ),
        'remarks' => array(
            'field' => 'remarks',
            'label' => 'Remarks',
            'rules' => 'trim',
        ),
        'receipt_type' => array(
            'field' => 'receipt_type',
            'label' => 'Receipt Type',
            'rules' => 'trim',
        ),
        'receipt_number' => array(
            'field' => 'receipt_number',
            'label' => 'Receipt Number',
            'rules' => 'trim',
        )

    ];

    public function __construct(){

		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

        $this->has_one['company'] = array('foreign_model' => 'company/Company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
        $this->has_one['buyers'] = array('foreign_model' => 'Buyer_model', 'foreign_table' => 'buyers', 'foreign_key' => 'id', 'local_key' => 'payee_id');
        $this->has_one['sellers'] = array('foreign_model' => 'Seller_model', 'foreign_table' => 'sellers', 'foreign_key' => 'id', 'local_key' => 'payee_id');
    }

    function get_columns() {

        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;

    }

}