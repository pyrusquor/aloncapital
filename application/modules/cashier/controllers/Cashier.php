<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cashier extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('cashier/Cashier_model', 'M_Cashier');

        // Load models
        $this->load->model('seller/Seller_model', 'M_seller');
        $this->load->model('buyer/Buyer_model', 'M_buyer');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('accounting_entries/accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_entry_items/accounting_entry_items_model', 'M_Accounting_entry_items');

        // Load pagination library
        $this->load->library('ajax_pagination');
        // $this->load->library('..transaction/controllers/Transaction');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        $this->load->library('mortgage_computation');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Cashier->fillable;
        $this->_table_columns = $this->M_Cashier->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Cashier->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Cashier->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }

        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showCashier()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'payee' => true,
            'company' => true,
            'payment_type' => true,
            'payment_amount' => true,
            'payment_date' => true,
            'receipt_type' => true,
            'receipt_number' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        
        // get all raw data
        $cashier = $this->M_Cashier->with_company('fields:name')->as_array()->get_all();
        $data = [];

        if ($cashier) {

            foreach ($cashier as $d) {

                $payee_data = get_person($d['payee_id'], $d['payee_type']);

                if(array_key_exists('first_name',$payee_data)){
                    $d['payee'] = $payee_data['first_name'] . ' ' . $payee_data['last_name'];
                } else {
                    $d['payee'] = $payee_data['name'];
                }

                if ($d['payment_type_id']){
                    $d['payment_type'] = "Cash";
                }
                else{
                    $d['payment_type'] = "Cheque";
                }
                $d['company'] = $d['company']['name'];
                $d['payment_type'] = strtoupper(Dropdown::get_static('payment_type',$d['payment_type_id'],'view'));
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );

        }

        echo json_encode($output);
        exit();
    }

    public function view($id = FALSE){
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_Cashier->with_company('fields:name')->get($id);
            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Cashier->get($id);
            if ($list) {

                $deleted = $this->M_Cashier->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete(){
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Cashier->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Cashier successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {
        $method = "Create";
        if ($id) {$method = "Update";}

        if ($this->input->post()) {
            // vdebug($this->input->post());
            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Cashier->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $cashier_status = $this->M_Cashier->update($info + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];
                    $cashier_status = $this->M_Cashier->insert($info + $additional);
                }

                if($cashier_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Cashier Successfully ' . $method . 'd!';
                }                
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_Cashier->with_company("fields:name")->get($id);
            $payee_id = $this->view_data['info']['payee_id'];
            $payee_type = $this->view_data['info']['payee_type'];
            $payee_data = get_person($payee_id, $payee_type);

            if(array_key_exists('first_name',$payee_data)){
                $this->view_data['info']['payee_name'] = $payee_data['first_name'] . ' ' . $payee_data['last_name'];
            } else {
                $this->view_data['info']['payee_name'] = $payee_data['name'];
            }
            // vdebug($this->view_data['payee_id']);
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }    

    public function entries_form($id = false)
    {
        $transaction_payment = $this->M_Cashier->with_company('fields:name')->get($id);
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            // $this->form_validation->set_rules($this->fields);

            if ($id) {

                $oof = $this->input->post();
                $entry_items = $oof['entry_item'];
                $accounting_entry = $oof['accounting_entry'];

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    // begin:Save accounting entries
                    $accounting_entry['or_number'] = 0;
                    $accounting_entry['company_id'] = 3;
                    $accounting_entry['invoice_number'] = $transaction_payment['receipt_number'];
                    $accounting_entry['journal_type'] = 3;
                    $accounting_entry['payment_date'] = $transaction_payment['payment_date'];
                    $accounting_entry['payee_type'] = $transaction_payment['payee_type'];
                    $accounting_entry['payee_type_id'] = $transaction_payment['payee_id'];
                    $accounting_entry['remarks'] = $transaction_payment['remarks'];

                    $accounting_entry_info = $this->M_Accounting_entries->insert($accounting_entry + $additional);
                    $accounting_entry_id = $this->db->insert_id();
                    // end:Save accounting entries

                    foreach ($entry_items as $key => $entry) {
                        $entry_item['accounting_entry_id'] = $accounting_entry_id;
                        $entry_item['ledger_id'] = $entry['ledger_id'];
                        $entry_item['amount'] = $entry['amount'];
                        $entry_item['dc'] = $entry['dc'];
                        $entry_item['payee_type'] = 'buyers';
                        $entry_item['payee_type_id'] = $accounting_entry['payee_type_id'];
                        $entry_item['is_reconciled'] = 0;
                        $entry_item['description'] = $entry['description'];

                        $this->M_Accounting_entry_items->insert($entry_item + $additional);
                    }
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                }

                /*Optional*/
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $response['status'] = 1;
                    $response['message'] = 'Cashier Payment Successfully ' . $method . 'd!';
                }

            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;

        if ($id) {
            $this->view_data['info'] = $data = $this->M_Cashier->with_company('fields:name')->get($id);

            // vdebug($data);

        }

        $this->template->build('entries_form', $this->view_data);
    }

}