<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$company_id = isset($info['company']['id']) && $info['company']["id"] ? $info['company']["id"] : '';
$company_name = isset($info['company']['name']) && $info['company']["name"] ? $info['company']["name"] : '';
$payee_type = isset($info['payee_type']) && $info['payee_type'] ? $info['payee_type'] : '';
$payee_id = isset($info['payee_id']) && $info['payee_id'] ? $info['payee_id'] : '';
$payee_name = isset($info['payee_name']) && $info['payee_name'] ? $info['payee_name'] : '';
$payment_date = isset($info['payment_date']) && $info['payment_date'] ? $info['payment_date'] : date('Y-m-d');
$payment_amount = isset($info['payment_amount']) && $info['payment_amount'] ? $info['payment_amount'] : '';
$payment_type_id = isset($info['payment_type_id']) && $info['payment_type_id'] ? $info['payment_type_id'] : '';
$receipt_type = isset($info['receipt_type']) && $info['receipt_type'] ? $info['receipt_type'] : '';
$bank = isset($info['bank']) && $info['bank'] ? $info['bank'] : '';
$cheque_number = isset($info['cheque_number']) && $info['cheque_number'] ? $info['cheque_number'] : '';
$receipt_number = isset($info['receipt_number']) && $info['receipt_number'] ? $info['receipt_number'] : '';
$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
                    <label class="form-control-label">Company</label>
                    <div class="kt-input-icon">
                        <select class="form-control suggests" data-module="companies"  id="company_id" name="company_id">
                            <option value="">Select Company</option>
                            <?php if ($company_id): ?>
                                <option value="<?php echo @$company_id; ?>" selected><?php echo $company_name; ?></option>
                            <?php endif ?>
                        </select>
                    </div>
                    <?php echo form_error('company_id'); ?>
                    <span class="form-text text-muted"></span>
        </div>
    </div>
    
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Payee Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('payee_type', Dropdown::get_static('payee_type'), set_value('$payee_type', @$payee_type), 'class="form-control" id="payee_type"'); ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Payee <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <select class="form-control suggests" data-type="person" data-module="<?php echo $payee_type ? $payee_type : "buyers" ?>" id="payee_id" name="payee_id"
                        required>
                        <option value="">Select Payee</option>
                        <?php if ($payee_name): ?>
                        <option value="<?php echo $payee_id; ?>" selected><?php echo $payee_name; ?></option>
                        <?php endif?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Payment Date <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker datePicker" name="payment_date"
                    value="<?php echo set_value('payment_date', date('Y-m-d',strtotime($payment_date))); ?>" placeholder="Date"
                    autocomplete="off">

            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Payment Type <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                     <?php echo form_dropdown('payment_type_id', Dropdown::get_static('payment_type'), set_value('payment_type_id', $payment_type_id), 'class="form-control" id="payment_type_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Amount <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">                            
                    <input type="text" class="form-control" name="payment_amount" id="payment_amount" value="<?php echo $payment_amount;  ?>" placeholder="" autocomplete="off">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Bank <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">                            
                    <input type="text" class="form-control" name="bank" id="bank" value="<?php echo $bank;  ?>" placeholder="" autocomplete="off" disabled>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Cheque Number <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">                            
                    <input type="text" class="form-control" name="cheque_number" id="cheque_number" value="<?php echo $cheque_number;  ?>" placeholder="" autocomplete="off" disabled>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Receipt Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('receipt_type', Dropdown::get_static('receipt_type'), set_value('$receipt_type', $receipt_type), 'class="form-control" id="receipt_type"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            <label>Receipt Number</label>
            <div class="col-sm-12">
                <input type="text" class="form-control" id="receipt_number" placeholder=""
                    name="receipt_number" value="<?php echo $receipt_number;  ?>">
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Remarks</label>
            <!-- <div class="col-sm-12"> -->
                <input type="text" class="form-control" id="remarks" placeholder="Remarks"
                    name="remarks" value="<?php echo $remarks;  ?>">
            <!-- </div> -->
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>