<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
// ==================== begin: Add model fields ====================
$payee = isset($data['payee']) && $data['payee'] ? $data['payee'] : '';
$receipt_number = isset($data['receipt_number']) && $data['receipt_number'] ? $data['receipt_number'] : '';
$company = isset($data['company']['name']) && $data['company']['name'] ? $data['company']['name'] : '';
$amount = isset($data['amount']) && $data['amount'] ? $data['amount'] : '';
$receipt_type = isset($data['receipt_type']) && $data['receipt_type'] ? $data['receipt_type'] : '';
$payment_type = strtoupper(Dropdown::get_static('payment_type',$data['payment_type_id'],'view'));
$payment_date = isset($data['payment_date']) && $data['payment_date'] ? $data['payment_date'] : '';

$payee_data = get_person($data['payee_id'], $data['payee_type']);
$payee = $payee_data['first_name'] . ' ' . $payee_data['last_name'];
$payee_type = strtoupper(Dropdown::get_static('payee_type',$data['payee_type'],'view'));

$bank = isset($data['bank']) && $data['bank'] ? $data['bank'] : '';
$cheque_number = isset($data['cheque_number']) && $data['cheque_number'] ? $data['cheque_number'] : '';

$payment_amount = isset($data['payment_amount']) && $data['payment_amount'] ? $data['payment_amount'] : '';
// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Chashier</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('cashier/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('cashier');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Receipt Number
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $receipt_number; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Receipt Type
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $receipt_type; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Payment Type
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $payment_type; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Payment Date
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo date_format(date_create($payment_date), 'F j, Y'); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Payee Name
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $payee; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Payee Type
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $payee_type; ?>
                                        </p>
                                    </div>
                                    
                                    <?php if ($data['payment_type_id']): ?>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Bank
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $bank ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Cheque Number
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $cheque_number; ?></p>
                                    </div>
                                    <?php endif; ?>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Amount
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo money_php($payment_amount); ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Remarks
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $remarks; ?></p>
                                    </div>

                                </div>
                                <!--end::Form-->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                    
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
