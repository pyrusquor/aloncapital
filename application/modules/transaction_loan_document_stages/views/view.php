<?php
$id = isset($transaction_document_stages['id']) && $transaction_document_stages['id'] ? $transaction_document_stages['id'] : '';
$name = isset($transaction_document_stages['name']) && $transaction_document_stages['name'] ? $transaction_document_stages['name'] : '';
$order_by = isset($transaction_document_stages['order_by']) && $transaction_document_stages['order_by'] ? $transaction_document_stages['order_by'] : '';
$description = isset($transaction_document_stages['description']) && $transaction_document_stages['description'] ? $transaction_document_stages['description'] : '';
$end_at = isset($transaction_document_stages['count_of_days']) && $transaction_document_stages['count_of_days'] ? $transaction_document_stages['count_of_days'] : '';
$start_at = isset($transaction_document_stages['start_at']) && $transaction_document_stages['start_at'] ? $transaction_document_stages['start_at'] : '';
$category = isset($transaction_document_stages['category_id']) && $transaction_document_stages['category_id'] ? $transaction_document_stages['category_id'] : '';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Transaction Document Stages</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('transaction_document_stages/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('transaction_document_stages');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Order By
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $order_by; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Count of Days
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $end_at; ?></span>
                                </div>
                                 <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Category
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $category; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Description
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $description; ?></span>
                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
