<?php
    $name = isset($transaction_loan_document_stages['name']) && $transaction_loan_document_stages['name'] ? $transaction_loan_document_stages['name'] : '';
    $order_by = isset($transaction_loan_document_stages['order_by']) && $transaction_loan_document_stages['order_by'] ? $transaction_loan_document_stages['order_by'] : '';
    $description = isset($transaction_loan_document_stages['description']) && $transaction_loan_document_stages['description'] ? $transaction_loan_document_stages['description'] : '';
    $count_of_days = isset($transaction_loan_document_stages['count_of_days']) && $transaction_loan_document_stages['count_of_days'] ? $transaction_loan_document_stages['count_of_days'] : '';
    $category_id = isset($transaction_loan_document_stages['category_id']) && $transaction_loan_document_stages['category_id'] ? $transaction_loan_document_stages['category_id'] : '';
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <!-- <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span> -->
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Category  <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                 <select class="form-control" id="category_id" name="category_id">
                    <option <?php echo ($category_id == 0) ?  "selected" : ""; ?>  value="0">Select Option</option>
                    <option <?php echo ($category_id == 1) ? "selected" : ""; ?>  value="1">HDMF Window 2 </option>
                    <option <?php echo ($category_id == 2) ? "selected" : ""; ?>  value="2">HDMF Retail Window</option>
                    <option <?php echo ($category_id == 3) ? "selected" : ""; ?>  value="3">Bank</option>
                </select>
            </div>
            <?php echo form_error('category_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    


    <div class="col-sm-6">
        <div class="form-group">
            <label>End At (Days) <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="count_of_days" value="<?php echo set_value('count_of_days', $count_of_days); ?>" placeholder="End At (Days)" autocomplete="off">
            </div>
            <?php echo form_error('count_of_days'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Order By <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="order_by" value="<?php echo set_value('order_by', $order_by); ?>" placeholder="Order By" autocomplete="off">
                <!-- <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span> -->
            </div>
            <?php echo form_error('order_by'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Description <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
                <!-- <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-align-left"></i></span> -->
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>