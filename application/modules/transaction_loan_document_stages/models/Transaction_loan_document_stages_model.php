<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_loan_document_stages_model extends MY_Model
{
    public $table = 'transaction_loan_document_stages'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name', 
        'order_by', 
        'description', 
        'category_id', 
        'count_of_days', 
        'start_at', 
        'description', 
        'created_by', 
        'updated_by', 
        'deleted_by'
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'order_by' => array(
            'field' => 'order_by',
            'label' => 'Order By',
            'rules' => 'trim|required',
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}
