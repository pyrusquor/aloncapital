<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 */
class Laborer extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();

        $this->load->model('laborer/laborer_model', 'M_laborers');

        $this->_table_fillables = $this->M_laborers->fillable;
        $this->_table_columns = $this->M_laborers->__get_columns();
        $this->created_by = [
            'created_by' => $this->user->id,
            'created_at' => NOW
        ];
        $this->updated_by = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW
        ];
    }

    public function index()
    {
        $this->view_data['total_rows'] = $this->M_laborers->count_rows();
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);
        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'last_name' => true,
            'first_name' => true,
            'middle_name' => true,
            'laborer_type' => true,
            'designation' => true,
            'hour_rate' => true,
            'day_rate' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);
        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];
        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_laborers
                    ->order_by($columnName, $columnSortOrder)
                    ->as_array();
            // General Search
            if ($searchValue) {

                $query->or_where("(first_name like '%$searchValue%'");
                $query->or_where("middle_name like '%$searchValue%'");
                $query->or_where("last_name like '%$searchValue%'");
                $query->or_where("designation like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'laborer_type' => [
                    'data' => $filters['laborer_type'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {
                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {
                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();
                if (!!$items) {

                    foreach ($items as $key => $value) {

                        $items[$key]['designation'] = $items[$key]['designation'] ? $items[$key]['designation'] : 'None';
                        $items[$key]['laborer_type'] = Dropdown::get_static('laborer_type', $items[$key]['laborer_type'], 'View');
                        $items[$key]['hour_rate'] = money_php($items[$key]['hour_rate']);
                        $items[$key]['day_rate'] = money_php($items[$key]['day_rate']);

                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_laborers->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = 0)
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $operation = 'Create';
        if ($id) {
            $operation = "Update";
            $this->view_data['info'] = $exists = $this->M_laborers->get($id);
            $exists ?: redirect(base_url('fixed_assets'));
        }

        if ($this->input->post()) {
            $post_data = $this->input->post();
            if ($id) {
                $success = $this->M_laborers->update($post_data + $this->updated_by, $id);
            } else {
                $success = $this->M_laborers->insert($post_data + $this->created_by);
            }
            if ($success) {
                $response['status'] = 1;
                $response['message'] = $operation . 'd laborer!';
            }
            echo json_encode($response);
            exit();
        }

        $this->view_data['operation'] = $operation;
        $this->template->build('form', $this->view_data);
    }

    public function view($id = 0)
    {
        $fixed_asset = $this->M_laborers->get($id);
        $fixed_asset ?: redirect(base_url('fixed_assets'));
        $fixed_asset['laborer_type'] = Dropdown::get_static('laborer_type', $fixed_asset['laborer_type'], 'view');
        $this->view_data['info'] = $fixed_asset;
        $this->template->build('view', $this->view_data);
    }

    public function delete()
    {
        $this->input->post() ? : redirect('fixed_assets') ;
        $response = [
            'status' => '0',
            'message' => 'Delete failed!'
        ];

        $deleteids_arr = $this->input->post('deleteids_arr');

        if($deleteids_arr){
            foreach ($deleteids_arr as $key => $id){
                $status = $this->M_laborers->delete($id);
                if($status){
                    $response = [
                        'status' => '1',
                        'message' => 'Deleted laborer(s)!'
                    ];
                }
            }
        }
        echo json_encode($response);
        exit();
    }
}
