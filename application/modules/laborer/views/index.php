<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Laborers</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc"><?= $total_rows ?> TOTAL</span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search Laborers..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">

            </div>
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">

        <div class="module__create">
            <a href="<?= site_url('laborer/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Create
            </a>
        </div>
        <div class="module__filter">
            <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true">
                <i class="fa fa-filter"></i> Filter
            </button>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!--begin: Advance Search -->
        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form" id="advance_search">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">ABC</label>
                                <div class="kt-input-icon">
                                <?php echo form_dropdown('laborer_type', Dropdown::get_static('laborer_type'), set_value('laborer_type', @$laborer_type), 'class="form-control _filter"'); ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end: Advance Search -->

        <!-- Batch Upload -->
        <div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__body">
                    <form class="kt-form" id="_export_csv" action="<?php echo site_url('item/export_csv'); ?>" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">File Type</label>
                                    <div class="kt-input-icon  kt-input-icon--left">
                                        <select class="form-control form-control-sm" name="update_existing_data" required>
                                            <option value=""> -- Update Existing Data --</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
                                    </div>
                                    <?php echo form_error('update_existing_data'); ?>
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form class="kt-form" id="_upload_form" action="<?php echo site_url('item/import'); ?>" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Upload CSV file:</label>
                                    <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per
                                        file.</label>
                                    <input type="file" name="csv_file" class="" size="1000" accept="*.csv" required>
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-group form-group-last row custom_import_style">
                        <div class="col-lg-3">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm" form="_upload_form">
                                        <i class="fa fa-upload"></i> Upload
                                    </button>
                                </div>
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" form="_export_csv">
                                        <i class="fa fa-file-csv"></i>
                                    </button>
                                    <button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
                                        <i class="fa fa-info-circle"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="laborer_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <!-- ==================== begin: Add header fields ==================== -->
                    <th>Laborer Type</th>
                    <th>Designation</th>
                    <th>Hour Rate</th>
                    <th>Day Rate</th>
                    <!-- ==================== end: Add header fields ==================== -->
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>