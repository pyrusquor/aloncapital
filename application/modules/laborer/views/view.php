<!-- CONTENT HEADER -->
<?php
$info ? extract($info) : '';
?>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Laborer</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?= base_url('laborer/form/' . $id); ?>" class="btn btn-label-instagram"><i class="fa fa-edit"></i>
                    Edit</a>&nbsp;
                <a href="javascript:void(0);" class="btn btn-label-instagram remove_laborer" data-id="<?= $id ?>"><i class="fa fa-trash-alt"></i>
                    Delete</a>&nbsp;
                <a href="<?= base_url('laborer'); ?>" class="btn btn-label-instagram"><i class="fa fa-reply"></i>
                    Back</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h1 class="kt-portlet__head-title display-3">
                    <?= strtoupper($first_name . ' ' . $middle_name . ' ' . $last_name) ?> <small><?= $code ?></small>
                </h1>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                <div class="row">
                    <div class="col-sm-4">
                        <div href="#" class="kt-notification-v2__item">
                            <div class="kt-notification-v2__itek-wrapper">
                                <div class="kt-notification-v2__item-title">
                                    <h6 class="kt-portlet__head-title kt-font-primary">Laborer Type</h6>
                                </div>
                                <div class="kt-notification-v2__item-desc">
                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                        <?= $laborer_type ? $laborer_type : 'N/A' ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div href="#" class="kt-notification-v2__item">
                            <div class="kt-notification-v2__itek-wrapper">
                                <div class="kt-notification-v2__item-title">
                                    <h6 class="kt-portlet__head-title kt-font-primary">Designation</h6>
                                </div>
                                <div class="kt-notification-v2__item-desc">
                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                        <?= $designation ? $designation : 'N/A' ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>