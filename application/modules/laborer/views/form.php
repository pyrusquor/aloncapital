<!-- CONTENT HEADER -->
<?php
$info ? extract($info) : '';
?>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><?= $operation ?> Laborer</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?= base_url('laborer'); ?>" class="btn btn-label-instagram"><i class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<input type='hidden' value='<?= $id ?>' id='id'>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                <form class="kt-form" method="POST" action="" id="form_laborers" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>First Name <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon">
                                    <input type="text" class="form-control" placeholder="Enter first name..." name="first_name" value="<?= $first_name ?>" required>
                                    <span class="kt-input-icon__icon"></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Middle Name</label>
                                <div class="kt-input-icon">
                                    <input type="text" class="form-control" placeholder="Enter middle name..." name="middle_name" value="<?= $middle_name ?>">
                                    <span class="kt-input-icon__icon"></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Last Name <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon">
                                    <input type="text" class="form-control" placeholder="Enter last name..." name="last_name" value="<?= $last_name ?>" required>
                                    <span class="kt-input-icon__icon"></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>

                    </div>
                    <div class='row'>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Hour Rate <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon">
                                    <input class="form-control" min="0" step="0.25" value="<?= $hour_rate ?>" type="number" name="hour_rate" placeholder="Hour Rate" required />
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Day Rate <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon">
                                    <input class="form-control" min="0" step="0.25" value="<?= $day_rate ?>" type="number" name="day_rate" placeholder="Day Rate" required />
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Laborer Type <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon">
                                    <?php echo form_dropdown('laborer_type', Dropdown::get_static('laborer_type'), set_value('laborer_type', @$laborer_type), 'class="form-control" required'); ?>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Designation <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon">
                                    <input type="text" class="form-control" placeholder="Designation" name="designation" value="<?= $designation ?>" required>
                                    <span class="kt-input-icon__icon"></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class='col-lg-12 kt-align-right'>
                            <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
                                Submit
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>