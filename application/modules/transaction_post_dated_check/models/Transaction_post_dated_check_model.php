<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_post_dated_check_model extends MY_Model {

	public $table = 'transaction_post_dated_checks'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id', 
		'bank_id', 
		'branch',
		'amount', 
		'unique_number', 
		'due_date', 
		'particulars', 
		'is_active',
		'pdc_status',
		'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [
		'transaction_id' => array(
			'field'=>'transaction_id',
			'label'=>'Transaction ID',
			'rules'=>'trim'
		),
		'bank_id' => array(
			'field'=>'bank_id',
			'label'=>'Bank ID',
			'rules'=>'trim|required'
		),
		'amount' => array(
			'field'=>'amount',
			'label'=>'Amount',
			'rules'=>'trim|required',
		),
		'unique_number' => array(
			'field'=>'unique_number',
			'label'=>'Unique Number',
			'rules'=>'trim|required'
		),
		'due_date' => array(
			'field'=>'due_date',
			'label'=>'Due Date',
			'rules'=>'trim|required'
		),
		'particulars' => array(
			'field'=>'particulars',
			'label'=>'Particulars',
			'rules'=>'trim'
		),
		'pdc_status' => array(
			'field'=>'pdc_status',
			'label'=>'PDC Status',
			'rules'=>'trim'
		),
	];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['transaction'] = array('foreign_model'=>'transaction/Transaction_model','foreign_table'=>'transactions','foreign_key'=>'id','local_key'=>'transaction_id');


		$this->has_one['bank'] = array('foreign_model'=>'banks/Banks_model','foreign_table'=>'banks','foreign_key'=>'id','local_key'=>'bank_id');

	}

	function get_columns () 
	{

		$_return	=	FALSE;

		if ( $this->fillable ) {

			$_return	=	$this->fillable;
		}

		return $_return;
	}

	public function for_clearing_today($limit=null, $start=null, $where = array()) 
	{
		$this->db->select(array(
				'transaction_post_dated_checks.due_date as pdc_due_date',
				'transaction_post_dated_checks.amount as pdc_amount',
				'transaction_post_dated_checks.pdc_status as pdc_status',
				'transactions.period_id as transaction_period_id',
				'transactions.reference as transaction_reference',
				'properties.name as property_name',
				'transactions.buyer_id as buyer_id',
				'transactions.id as transaction_id',
			))
			->from('transaction_post_dated_checks')
			->where('transaction_post_dated_checks.deleted_at', NULL)
			->where('transaction_post_dated_checks.due_date', date('Y-m-d 00:00:00'))
			->join('transactions', 'transactions.id = transaction_post_dated_checks.transaction_id')
			->where('transactions.deleted_at', NULL)
			->join('properties', 'properties.id = transactions.property_id', 'left')
			->group_by('transaction_post_dated_checks.id')
			->limit(500);

		if(array_key_exists('personnel_id', $where)) {
			$this->db->where("transactions.personnel_id" , $where['personnel_id']);
		}

		if(array_key_exists('project_id', $where)) {
			$this->db->where("transactions.project_id" , $where['project_id']);
		}
	
		$this->db->limit($limit, $start);

		$query = $this->db->get()->result_array();

		return $query;
	}

	public function overdue_checks($limit=null, $start=null, $where = array()) 
	{
		$this->db->select(array(
				'transaction_post_dated_checks.due_date as pdc_due_date',
				'transaction_post_dated_checks.amount as pdc_amount',
				'transaction_post_dated_checks.pdc_status as pdc_status',
				'transactions.period_id as transaction_period_id',
				'transactions.reference as transaction_reference',
				'properties.name as property_name',
				'transactions.buyer_id as buyer_id',
				'transactions.id as transaction_id',
			))
			->from('transaction_post_dated_checks')
			->where('transaction_post_dated_checks.deleted_at', NULL)
			->where('transaction_post_dated_checks.due_date <', date('Y-m-d 00:00:00'))
			->where('transaction_post_dated_checks.pdc_status !=', '3')
			->join('transactions', 'transactions.id = transaction_post_dated_checks.transaction_id', 'left')
			->where('transactions.deleted_at', NULL)
			->join('properties', 'properties.id = transactions.property_id', 'left')
			->group_by('transaction_post_dated_checks.id')
			->limit(500);

		if(array_key_exists('personnel_id', $where)) {
			$this->db->where("transactions.personnel_id" , $where['personnel_id']);
		}

		if(array_key_exists('project_id', $where)) {
			$this->db->where("transactions.project_id" , $where['project_id']);
		}
	
		$this->db->limit($limit, $start);

		$query = $this->db->get()->result_array();

		return $query;
	}

	public function upcoming_clearing($limit=null, $start=null, $where = array()) 
	{
		$this->db->select(array(
				'transaction_post_dated_checks.due_date as pdc_due_date',
				'transaction_post_dated_checks.amount as pdc_amount',
				'transaction_post_dated_checks.pdc_status as pdc_status',
				'transactions.period_id as transaction_period_id',
				'transactions.reference as transaction_reference',
				'properties.name as property_name',
				'transactions.buyer_id as buyer_id',
				'transactions.id as transaction_id',
			))
			->from('transaction_post_dated_checks')
			->where('transaction_post_dated_checks.deleted_at', NULL)
			->where('transaction_post_dated_checks.due_date >', date('Y-m-d 00:00:00'))
			->join('transactions', 'transactions.id = transaction_post_dated_checks.transaction_id')
			->where('transactions.deleted_at', NULL)
			->join('properties', 'properties.id = transactions.property_id', 'left')
			->group_by('transaction_post_dated_checks.id')
			->limit(500);

		if(array_key_exists('personnel_id', $where)) {
			$this->db->where("transactions.personnel_id" , $where['personnel_id']);
		}

		if(array_key_exists('project_id', $where)) {
			$this->db->where("transactions.project_id" , $where['project_id']);
		}
	
		$this->db->limit($limit, $start);

		$query = $this->db->get()->result_array();

		return $query;
	}

	public function total_for_clearing_today()
	{
		$this->db->select_sum('amount')
			->select('COUNT(id) as count')
			->from('transaction_post_dated_checks')
			->where('transaction_post_dated_checks.deleted_at', NULL)
			->where('transaction_post_dated_checks.due_date', date('Y-m-d 00:00:00'));
		
		$query = $this->db->get()->row_array();

		return $query;
	}

	public function total_overdue_checks()
	{
		$this->db->select_sum('amount')
			->select('COUNT(id) as count')
			->from('transaction_post_dated_checks')
			->where('transaction_post_dated_checks.deleted_at', NULL)
			->where('transaction_post_dated_checks.due_date <', date('Y-m-d 00:00:00'))
			->where('transaction_post_dated_checks.pdc_status !=', '3');
		
		$query = $this->db->get()->row_array();

		return $query;
	}

	public function total_upcoming_clearing()
	{
		$this->db->select_sum('amount')
			->select('COUNT(id) as count')
			->from('transaction_post_dated_checks')
			->where('transaction_post_dated_checks.deleted_at', NULL)
			->where('transaction_post_dated_checks.due_date >', date('Y-m-d 00:00:00'));
		
		$query = $this->db->get()->row_array();

		return $query;
	}

	public function get_due_payments_today($project_id=0){
		$result = [];
		$this->db->join('transactions',' transaction_post_dated_checks.transaction_id = transactions.id','inner');
		$this->db->where("transactions.deleted_at IS NULL");
		$this->db->where("transaction_post_dated_checks.deleted_at IS NULL");
		$this->db->where("transaction_post_dated_checks.due_date = curdate()");
		if($project_id){
			$this->db->where("transactions.project_id = $project_id");
		}
		$query = $this->db->get('transaction_post_dated_checks')->result_array();
		if($query){
			$result = $query;
		}

		return $result;
	}

}