<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_post_dated_check extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('transaction_post_dated_check/Transaction_post_dated_check_model', 'M_transaction_post_dated_check');
        $this->load->model('transaction/Transaction_model', 'M_Transaction');
        $this->load->model('banks/Banks_model', 'M_banks');

        $this->_table_fillables = $this->M_transaction_post_dated_check->fillable;
        $this->_table_columns = $this->M_transaction_post_dated_check->__get_columns();
    }

    public function index()
    {

        // $db_columns = $this->M_transaction_post_dated_check->get_columns();

        // // vdebug($db_columns);
        // if ($db_columns) {

        //     $column = [];
        //     foreach ($db_columns as $key => $dbclm) {

        //         $name = isset($dbclm) && $dbclm ? $dbclm : '';

        //         if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {

        //             if (strpos($name, '_id') !== false) {
        //                 // $column    =    $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } elseif (strpos($name, 'is_') !== false) {

        //                 // $column    =    $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } else {

        //                 $column[$key]['value'] = $name;
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             }
        //         } else {

        //             continue;
        //         }
        //     }

        //     $ccount = count($column);
        //     $cceil = ceil(($ccount / 2));

        //     // $this->view_data['columns']    = array_chunk($column, $cceil);

        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);


        //     $ctransaction_post_dated_check = $this->M_transaction_post_dated_check->count_rows();

        //     if ($ctransaction_post_dated_check) {

        //         $this->view_data['total'] = $ctransaction_post_dated_check;
        //     }
        // }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function view($id = false)
    {
        if ($id) {

            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['transaction_post_dated_check'] = $this->M_transaction_post_dated_check->with_transaction('fields:reference')->get($id);

            if ($this->view_data['transaction_post_dated_check']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function form($transaction_id = 0, $id = 0)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $info = $this->input->post();

            $additional = [
                'is_active' => 1,
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            $start = $info['cheque_number'];
            $end = $info['cheque_number_end'];

            $a = 0;

            for ($i = $start; $i <= $end; $i++) {


                $pdc['transaction_id'] = $info['transaction_id'];
                $pdc['unique_number'] = $i; // THE VALUE OF THIS VARIABLE WILL CHANGE EVERY TIME IT LOOPS
                $pdc['amount'] = $info['amount'];
                $pdc['due_date'] = add_months_to_date($info['due_date'], $a);
                $pdc['bank_id'] = $info['bank_id'];
                $pdc['particulars'] = $info['particulars'];
                $pdc['pdc_status'] = $info['pdc_status'];

                // add more fields here from the form

                $this->M_transaction_post_dated_check->insert($pdc);
                $a++;
            }

            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Transaction post dated check successfully created.', 'transaction_post_dated_check');
            }
        } else {

            if ($transaction_id) {

                $this->view_data['id'] = $id;

                $this->view_data['transaction_id'] = $transaction_id;

                $this->view_data['transaction'] = $this->M_Transaction->get($transaction_id);

                if ($id) {

                    $this->view_data['row'] = $this->M_transaction_post_dated_check->get($id);
                }

                $this->template->build('form', $this->view_data);
            } else {
                redirect('transaction');
            }
        }
    }

    public function delete()
    {

        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_transaction_post_dated_check->get($id);
            if ($list) {

                $deleted = $this->M_transaction_post_dated_check->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Transaction_post_dated_check successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'transaction' => true,
            'transaction_id' => true,
            'buyer_id' => true,
            'project_id' => true,
            'property_id' => true,
            'unique_number' => true,
            'bank_id' => true,
            'branch' => true,
            'amount' => true,
            'due_date' => true,
            'pdc_status' => true,
            'bank' => true,
            'created_by' => true,
            'updated_by' => true,
            'Actions' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_transaction_post_dated_check
                ->with_bank('fields:name')
                ->with_transaction([
                    'fields' => 'reference',
                    'with' => [
                        [
                            'relation' => 'project',
                            'fields' => 'name'
                        ],
                        [
                            'relation' => 'property',
                            'fields' => 'name'
                        ],
                        [
                            'relation' => 'buyer',
                            'fields' => 'first_name,middle_name,last_name'
                        ],
                    ]
                ])
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("unique_number like '%$searchValue%'");
                $query->or_where("branch like '%$searchValue%'");
                $query->or_where("amount like '%$searchValue%'");
                $query->or_where("due_date like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'transaction_id' => [
                    'data' => $filters['transction_id'] ?? null,
                    'operator' => '=',
                ],
                'bank_id' => [
                    'data' => $filters['bank_id'] ?? null,
                    'operator' => '=',
                ],
                'due_date' => [
                    'data' => $filters['due_date'] ?? null,
                    'operator' => 'like',
                ],
                'date_range_start' => [
                    'data' => $filters['date_range_start'] ?? null,
                    'operator' => '>=',
                    'column' => 'due_date',
                ],
                'date_range_end' => [
                    'data' => $filters['date_range_end'] ?? null,
                    'operator' => '<=',
                    'column' => 'due_date',
                ],
                'pdc_status' => [
                    'data' => $filters['pdc_status'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['transaction'] = isset($value['transaction']) ? "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['transaction']['id'] . "'>" . $value['transaction']['reference'] . "</a>" : '';

                        $items[$key]['project_id'] = isset($value['transaction']['project']) ? "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['transaction']['project']['id'] . "'>" . $value['transaction']['project']['name'] . "</a>" : '';

                        $items[$key]['property_id'] = isset($value['transaction']['property']) ? "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['transaction']['property']['id'] . "'>" . $value['transaction']['property']['name'] . "</a>" : '';

                        $items[$key]['buyer_id'] = isset($value['transaction']['buyer']) ? "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $value['transaction']['buyer']['id'] . "'>" . get_fname($value['transaction']['buyer']) . "</a>" : '';

                        $items[$key]['amount'] = money_php($value['amount']) ?? '';
                        $items[$key]['bank'] = $value['bank']['name'] ?? '';
                        $items[$key]['due_date'] = view_date($value['due_date']) ?? '';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_transaction_post_dated_check->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
        );

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_transaction_post_dated_check->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Transaction_post_dated_check successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function get_table_schema()
    {
        $query = $this->db->query('SHOW COLUMNS FROM transaction_post_dated_checks');
        $queryResultArray = $query->result_array();
        $array = array();
        $transaction_post_dated_check_fillable_fields = $this->M_transaction_post_dated_check->fillable;
        $ctr = 1;
        foreach ($queryResultArray as $field) {
            if (in_array($field['Field'], $transaction_post_dated_check_fillable_fields)) {
                $det = array();
                $det['no'] = $ctr;
                $det['name'] = $field['Field'];
                $det['type'] = $field['Type'];
                $det['format'] = '';
                $det['option'] = '';
                $det['required'] = ($field['Null'] == 'NO') ? 'Yes' : 'No';

                $array[] = $det;
                $ctr++;
            }
        }

        $_response = [];

        $_total['_displays'] = 0;
        $_total['_records'] = 0;

        $_columns = [
            'no' => true,
            'name' => true,
            'type' => true,
            'format' => true,
            'option' => true,
            'required' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

            $_columns = [];

            foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

                $_columns[$_def] = true;
            }
        }

        $_checklists = $array;

        if ($_checklists) {

            $_datas = [];

            foreach ($_checklists as $_ckkey => $_ck) {

                $_datas[] = $this->filterArray($_ck, $_columns);
            }

            $_total['_displays'] = $_total['_records'] = count($_datas);

            if (isset($_REQUEST['search'])) {

                $_datas = $this->filterKeyword($_datas, $_REQUEST['search']);

                $_total['_displays'] = $_datas ? count($_datas) : 0;
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

                foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

                    if ($_clm['search']) {

                        $_datas = $this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

                        $_total['_displays'] = $_datas ? count($_datas) : 0;
                    }
                }
            }

            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'];
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($_datas, function ($x, $y) use ($_column, $_dir) {

                    $x = array_slice($x, $_column, 1);
                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            if (isset($_REQUEST['length'])) {

                $_datas = array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
            }

            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

                $_temp = $_datas;
                $_datas = [];

                foreach ($_temp as $key => $_tmp) {

                    $_datas[] = array_values($_tmp);
                }
            }

            $_secho = 0;
            if (isset($_REQUEST['sEcho'])) {

                $_secho = intval($_REQUEST['sEcho']);
            }

            $_response = [
                'iTotalDisplayRecords' => $_total['_displays'],
                'iTotalRecords' => $_total['_records'],
                'sColumns' => '',
                'sEcho' => $_secho,
                'data' => $_datas,
            ];
        }

        // vdebug($_response);
        echo json_encode($_response);
    }

    public function export()
    {
        extract($this->input->post());
        $order = explode(',', $order);
        $_db_columns = [];
        $_alphas = [];
        $_datas = [];
        $_extra_datas = [];
        $_adatas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        if ($transction_id) {
            $this->db->where("transction_id = $transction_id");
        }

        if ($bank_id) {
            $this->db->where("bank_id = $bank_id");
        }

        if ($due_date) {
            $this->db->where("due_date = '$due_date'");
        }

        if ($pdc_status) {
            $this->db->where("pdc_status = $pdc_status");
        }

        if ($order) {
            $this->db->order_by($order[0], $order[1]);
        }

        $pdcs = $this->M_transaction_post_dated_check->with_transaction()->as_array()->get_all();

        if ($pdcs) {

            foreach ($pdcs as $skey => $pdc) {

                $_datas[$pdc['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_transaction_post_dated_checks' . date('m_d_y_h-i-s', time()) . '.xls';

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                $_additional_column = $this->input->post('_additional_column');

                if ($_export_column) {
                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id') && ($_name !== 'user_id')) {

                        $_column = $_name;

                        $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                        $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        foreach ($pdcs as $skey => $pdc) {

                            $_datas[$pdc['id']][$_title] = isset($pdc[$_name]) && $pdc[$_name] ? $pdc[$_name] : '';
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_lastAlpha = end($_alphas);

                if (empty($_extra_datas)) {
                    foreach ($_xls_columns as $_xkey => $_column) {

                        $_title = ucwords(strtolower($_column));

                        $_objSheet->setCellValue($_xkey . $_row, $_title);
                        $_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                }

                $_objSheet->setTitle('List of Transaction Post Dated Checks');
                $_objSheet->setCellValue('A1', 'LIST OF TRANSACTION POST DATED CHECKS');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                $col = 1;

                foreach ($pdcs as $key => $pdc) {

                    $pdc_id = isset($pdc['id']) && $pdc['id'] ? $pdc['id'] : '';

                    if (!empty($_extra_datas)) {

                        foreach ($_xls_columns as $_xkey => $_column) {

                            $_title = ucwords(strtolower($_column));

                            $_objSheet->setCellValue($_xkey . $_start, $_title);

                            $_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        }

                        $_start++;
                    }
                    // PRIMARY INFORMATION COLUMN
                    foreach ($_alphas as $_akey => $_alpha) {
                        $_value = isset($_datas[$pdc_id][$_xls_columns[$_alpha]]) && $_datas[$pdc_id][$_xls_columns[$_alpha]] ? $_datas[$pdc_id][$_xls_columns[$_alpha]] : '';
                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    // ADDITIONAL INFORMATION COLUMN
                    if (!empty($_extra_datas)) {
                        $_start += 2;

                        $_addtional_columns = $_extra_titles;

                        foreach ($_addtional_columns as $adkey => $_a_column) {

                            // MAIN TITLE OF ADDITIONAL DATA

                            $a_title = ucwords(str_replace('_', ' ', strtolower($ad_title)));

                            $_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

                            // Style
                            $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
                            $_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                            // LOOP DATAS
                            if ((strpos($_a_column, 'contact') !== false) or (strpos($_a_column, 'source') !== false or (strpos($_a_column, 'work_experience') !== false))) {

                                $col = 1;
                                if (!empty($_extra_datas[$pdc_id][$_a_column])) {

                                    foreach ($_extra_datas[$pdc_id][$_a_column] as $key => $value) {

                                        if ((strpos($key, '_id') === false) && (strpos($key, 'id') === false)) {

                                            if ($key === 'to_report' or $key === 'to_attend' or $key === 'commission_based' or $key === 'is_member') {
                                                $xa_value = isset($_extra_datas[$pdc_id][$_a_column][$key]) && ($_extra_datas[$pdc_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$pdc_id][$_a_column][$key], 'view') : '';
                                            } else {
                                                $xa_value = $_extra_datas[$pdc_id][$_a_column][$key];
                                            }

                                            $xa_titles = isset($key) && $key ? str_replace('_', ' ', $key) : '';

                                            $_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
                                            $_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
                                        }

                                        $_start++;
                                    }
                                } else {
                                    $_start++;

                                    $_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

                                    // Style
                                    $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
                                    $_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                                    $_start += 1;
                                }
                            }

                            $_start++;
                        }
                    }

                    $_start += 1;
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
            }
        } else {

            $this->notify->error('No Record Found', 'transaction');
        }
    }

    public function export_csv()
    {

        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Transaction CSV Template.csv';

            // $_fillables    =    $this->M_document->fillable;
            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false && ($_fill !== 'user_id'))) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $records = $this->M_transaction->as_array()->get_all(); #up($_documents);
                if ($records) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($records as $_dkey => $record) {

                            $_datas[$record['id']][$_title] = isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Transaction Post Dated Checks');
            $_objSheet->setCellValue('A1', 'TRANSACTION POST DATED CHECKS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function import()
    {

        if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

            // if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
            if (true) {

                $_tmp_name = $_FILES['csv_file']['tmp_name'];
                $_name = $_FILES['csv_file']['name'];

                set_time_limit(0);

                $_columns = [];
                $_datas = [];

                $_failed_reasons = [];
                $_inserted = 0;
                $_updated = 0;
                $_failed = 0;

                /**
                 * Read Uploaded CSV File
                 */
                try {

                    $_file_type = PHPExcel_IOFactory::identify($_tmp_name);
                    $_objReader = PHPExcel_IOFactory::createReader($_file_type);
                    $_objPHPExcel = $_objReader->load($_tmp_name);
                } catch (Exception $e) {

                    $_msg = 'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

                    $this->notify->error($_msg, 'document');
                }

                $_objWorksheet = $_objPHPExcel->getActiveSheet();
                $_highestColumn = $_objWorksheet->getHighestColumn();
                $_highestRow = $_objWorksheet->getHighestRow();
                $_sheetData = $_objWorksheet->toArray();
                if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

                    if ($_sheetData[1][0] === 'id') {

                        $_columns[] = 'id';
                    }

                    // $_fillables    =    $this->M_document->fillable;
                    $_fillables = $this->_table_fillables;
                    if (!$_fillables) {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction_post_dated_check');
                    }

                    foreach ($_fillables as $_fkey => $_fill) {

                        if (in_array($_fill, $_sheetData[1])) {

                            $_columns[] = $_fill;
                        } else {

                            continue;
                        }
                    }

                    foreach ($_sheetData as $_skey => $_sd) {

                        if ($_skey > 1) {

                            if (count(array_filter($_sd)) !== 0) {

                                $_datas[] = array_combine($_columns, $_sd);
                            }
                        } else {

                            continue;
                        }
                    }

                    if (isset($_datas) && $_datas) {

                        $this->db->trans_begin();

                        foreach ($_datas as $_dkey => $_data) {

                            $date = date_create_from_format('m/d/Y', $_data['due_date']);

                            if (!$date) {
                                $this->notify->error('Invalid date format for "due_date". Set date format to short date. Valid example: "1/25/2021".', 'transaction_post_dated_check');
                            }

                            $due_date = date_format(date_create_from_format('m/d/Y', $_data['due_date']), 'Y-m-d');
                            $_data['due_date'] = $due_date;

                            $_id = isset($_data['id']) && $_data['id'] ? $_data['id'] : false;

                            if ($_id) {

                                $data = $this->M_transaction_post_dated_check->get($_id);

                                if ($data) {

                                    unset($_data['id']);

                                    // Update Transaction Info
                                    $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                    $_data['updated_at'] = NOW;

                                    $_update = $this->M_transaction_post_dated_check->update($_data, $_id);
                                } else {

                                    $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                    $_data['created_at'] = NOW;
                                    $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                    $_data['updated_at'] = NOW;

                                    $_insert = $this->M_transaction_post_dated_check->insert($_data);
                                    if ($_insert !== false) {

                                        $_inserted++;
                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'insert transaction not working';
                                        $_failed++;

                                        break;
                                    }
                                }
                            } else {

                                $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                $_data['created_at'] = NOW;
                                $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                $_data['updated_at'] = NOW;

                                $_insert = $this->M_transaction_post_dated_check->insert($_data);
                                if ($_insert !== false) {

                                    $_inserted++;
                                } else {

                                    $_failed_reasons[$_dkey][] = 'insert transaction not working';

                                    $_failed++;

                                    break;
                                }
                            }
                        }

                        $_msg = '';
                        if ($_inserted > 0) {

                            $_msg = $_inserted . ' record/s was successfuly inserted';
                        }

                        if ($_updated > 0) {

                            $_msg .= ($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
                        }

                        if ($_failed > 0) {

                            $this->db->trans_rollback();
                            $this->notify->error('Upload Failed! Please follow upload guide. ', 'transaction_post_dated_check');
                        } else {

                            $this->db->trans_commit();
                            $this->notify->success($_msg . '.', 'transaction_post_dated_check');
                        }
                    }
                } else {

                    $this->notify->warning('CSV was empty.', 'transaction_post_dated_check');
                }
            } else {

                $this->notify->warning('Not a CSV file!', 'transaction_post_dated_check');
            }
        } else {
            $this->notify->error('Something went wrong!', 'transaction_post_dated_check');
        }
    }

    public function get_pdc_summary()
    {

        $pending_count = 0;
        $processing_count = 0;
        $cleared_count = 0;
        $bounced_count = 0;
        $pending_amount = 0;
        $processing_amount = 0;
        $cleared_amount = 0;
        $bounced_amount = 0;

        if ($this->input->post()) {

            $start = $this->input->post('start');
            $end = $this->input->post('end');

            if ($start == '' && $end == '') {
                $pdc_collection_query = $this->db->query('SELECT * FROM transaction_post_dated_checks WHERE due_date = CURDATE()');
                $pdc_collection_array = $pdc_collection_query->result_array();
            } else {
                $pdc_collection_query = $this->db->query("SELECT * FROM transaction_post_dated_checks WHERE (due_date BETWEEN CAST('$start' AS DATE)  AND CAST('$end' AS DATE))");
                $pdc_collection_array = $pdc_collection_query->result_array();
            }
            if ($pdc_collection_query) {

                foreach ($pdc_collection_array as $row) {
                    switch ($row['pdc_status']) {
                        case '1':
                            $pending_count += 1;
                            $pending_amount += $row['amount'];
                            break;
                        case '2':
                            $processing_count += 1;
                            $processing_amount += $row['amount'];
                            break;
                        case '3':
                            $cleared_count += 1;
                            $cleared_amount += $row['amount'];
                            break;
                        case '4':
                            $bounced_count += 1;
                            $bounced_amount += $row['amount'];
                            break;
                    }
                }
            }
        }
        $result['pending_count'] = $pending_count;
        $result['processing_count'] = $processing_count;
        $result['cleared_count'] = $cleared_count;
        $result['bounced_count'] = $bounced_count;
        $result['pending_amount'] = money_php($pending_amount);
        $result['processing_amount'] = money_php($processing_amount);
        $result['cleared_amount'] = money_php($cleared_amount);
        $result['bounced_amount'] = money_php($bounced_amount);
        echo json_encode($result);
        exit();
    }
}
