<?php
    $id = isset($row['id']) && $row['id'] ? $row['id'] : '';
    $transaction_id = isset($row['transaction_id']) && $row['transaction_id'] ? $row['transaction_id'] : '';
    //$transaction_id     =    isset($row['transaction']['reference']) && $row['transaction']['reference'] ? $row['transaction']['reference'] : '';
    $bank_id = isset($row['bank_id']) && $row['bank_id'] ? $row['bank_id'] : '';
    $cheque_number = isset($row['unique_number']) && $row['unique_number'] ? $row['unique_number'] : '';
    $due_date = isset($row['due_date']) && $row['due_date'] ? $row['due_date'] : '';
    $particulars = isset($row['particulars']) && $row['particulars'] ? $row['particulars'] : '';
    $pdc_status = isset($row['pdc_status']) && $row['pdc_status'] ? $row['pdc_status'] : '';
    $amount = isset($row['amount']) && $row['amount'] ? $row['amount'] : '';
    $reference_id   = isset($transaction['id']) && $transaction['id'] ? $transaction['id'] : '';
    $reference      = isset($transaction['reference']) && $transaction['reference'] ? $transaction['reference'] : '';

    if ($id) {
        $cheque_number_end = $cheque_number++;
    }
?>

<div class="row">
    <input type="hidden" id="transaction_id" name="transaction_id" value="<?php echo $reference_id;?>">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Reference Number </label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="reference" value="<?php echo set_value('reference', $reference); ?>" placeholder="Cheque Number Start" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-o"></i></span>
            </div>
            <?php echo form_error('reference'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Cheque Number Start <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="cheque_number" value="<?php echo set_value('cheque_number', $cheque_number); ?>" placeholder="Cheque Number Start" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-o"></i></span>
            </div>
            <?php echo form_error('cheque_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
     <div class="col-sm-6">
        <div class="form-group">
            <label>Cheque Number End</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="cheque_number_end" value="<?php echo set_value('cheque_number_end', @$cheque_number_end); ?>" placeholder="Cheque Number End" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-o"></i></span>
            </div>
            <?php echo form_error('cheque_number_end'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Amount <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="amount" value="<?php echo set_value('amount', $amount); ?>" placeholder="Amount" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-money-o"></i></span>
            </div>
            <?php echo form_error('amount'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
     <div class="col-sm-6">
        <div class="form-group">
            <label>Due Date <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control datePicker" name="due_date" value="<?php echo set_value('due_date', $due_date); ?>" placeholder="Due Date" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-o"></i></span>
            </div>
            <?php echo form_error('due_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Bank Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select name="bank_id" id="bank_id" class="form-control suggests" data-module="banks"><option>Select Option</option></select>
            </div>
            <?php echo form_error('bank_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Status <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <?php echo form_dropdown('pdc_status', Dropdown::get_static('pdc_status'), set_value('pdc_status', @$pdc_status), 'class="form-control" id="pdc_status"'); ?>

            </div>
            <?php echo form_error('pdc_status'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Particulars </label>
            <div class="kt-input-icon kt-input-icon--left">
               <textarea name="particulars" id="particulars" class="form-control" cols="30" rows="10">
                   <?php echo set_value('particulars', $particulars); ?>
               </textarea>
            </div>
            <?php echo form_error('particulars'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    
</div>
