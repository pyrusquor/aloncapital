<?php

// print_r($columns); exit;
$transaction_id =    isset($row['transaction']['reference']) && $row['transaction']['reference'] ? $row['transaction']['reference'] : '';
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Post Dated Checks</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="total"></span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper d-flex">

                <form method="POST" id="advance_search_date_range">
                    <div class="kt-input-icon  kt-input-icon--left kt-subheader__search">
                        <input type="text" class="form-control kt_datepicker _filter_date_range" id="filter_start_date" name="date_range_start" placeholder="Filter Start Date" autocomplete="off" readonly>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                    </div>

                    <div class="kt-input-icon  kt-input-icon--left kt-subheader__search">
                        <input type="text" class="form-control kt_datepicker _filter_date_range" id="filter_end_date" name="date_range_end" placeholder="Filter End Date" autocomplete="off" readonly>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                    </div>
                </form>

                <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
                    <i class="fa fa-upload"></i> Import
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#_export_option">
                    <i class="fa fa-download"></i> Export
                </button>
                <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true">
                    <i class="fa fa-filter"></i> Filter
                </button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('_information_tiles'); ?>

<!-- CONTENT -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">

        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form" id="advance_search">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label class="form-control-label">Reference:</label>
                                <select class="form-control suggests _filter" data-module="transactions" name="transction_id" data-select="reference">
                                    <option value="">Select Reference</option>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label class="form-control-label">Bank</label>
                                <select class="form-control suggests _filter" data-module="banks" name="bank_id">
                                    <option value="">Select Reference</option>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label class="form-control-label">Due Date</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="due_date" class="form-control form-control-sm _filter kt_datepicker" placeholder="Due Date" readonly>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label class="form-control-label">Status</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('pdc_status', Dropdown::get_static('pdc_status'), set_value('pdc_status', @$pdc_status), 'class="form-control form-control-sm _filter"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-certificate"></i></span></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
            <form class="kt-form" id="export_csv" action="<?php echo site_url('transaction_post_dated_check/export_csv'); ?>" method="POST">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Document Type</label>
                            <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control form-control-sm" name="status" id="import_status">
                                    <option value=""> -- Update Existing Data -- </option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
                            </div>
                            <?php echo form_error('status'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <form class="kt-form" id="upload_form" action="<?php echo site_url('transaction_post_dated_check/import'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Upload CSV file:</label>
                            <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per
                                file.</label>
                            <input type="file" name="csv_file" class="" size="1000" accept="*.csv">
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group form-group-last row custom_import_style">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm disabled" form="upload_form">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                        <div class="col-lg-6 kt-align-right">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm disabled" form="export_csv">
                                <i class="fa fa-file-csv"></i>
                            </button>
                            <button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm disabled" disabled id="btn_upload_guide" data-toggle="modal" data-target="#upload_guide">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="transaction_post_dated_check_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Transaction</th>
                    <th>Project</th>
                    <th>Property</th>
                    <th>Buyer</th>
                    <th>Cheque Number</th>
                    <th>Bank Name</th>
                    <th>Branch</th>
                    <th>Amount</th>
                    <th>Due Date</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Last Update By</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>

<!-- EXPORT -->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form" target="_blank" action="javascript:void(0);">
                    <div class="row">
                        <?php if (isset($_columns) && $_columns) : ?>

                            <div class="col-lg-12">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" id="_export_select_all"> Field
                                        <span></span>
                                    </label>
                                    <label class="kt-checkbox kt-checkbox--bold"></label>
                                </div>
                            </div>

                            <?php foreach ($_columns as $key => $_column) : ?>

                                <?php if ($_column) : ?>

                                    <?php
                                    $_offset    =    '';
                                    if ($_column === reset($_columns)) {

                                        $_offset    =    'offset-lg-1';
                                    }
                                    ?>

                                    <div class="col-lg-6">
                                        <div class="kt-checkbox-list">
                                            <?php foreach ($_column as $_ckey => $_clm) : ?>

                                                <?php
                                                $_label    =    isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
                                                $_value    =    isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
                                                ?>

                                                <label class="kt-checkbox kt-checkbox--bold">
                                                    <input type="checkbox" name="_export_column[]" class="_export_column" value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
                                                    <span></span>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        <?php else : ?>

                            <div class="col-lg-10 offset-lg-1">
                                <div class="form-group form-group-last">
                                    <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            Something went wrong. Please contact your system administrator.
                                        </div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" id="_export_form_btn">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- UPLOAD GUIDE MODAL -->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upload_guide_label">Transaction Post Dated Check - Upload Guide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="upload_guide_table">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Format</th>
                            <th>Options</th>
                            <th>Required</th>
                        </tr>
                    </thead>
                    <?php if (isset($_fillables) && $_fillables) : ?>

                        <tbody>
                            <?php foreach ($_fillables as $_fkey => $_fill) : ?>
                                <?php
                                $_no = isset($_fill['no']) && $_fill['no'] ? $_fill['no'] : '';
                                $_name = isset($_fill['name']) && $_fill['name'] ? $_fill['name'] : '';
                                $_type = isset($_fill['type']) && $_fill['type'] ? $_fill['type'] : '';
                                $_required = isset($_fill['required']) && $_fill['required'] ? $_fill['required'] : '';
                                $_dropdown = isset($_fill['dropdown']) && $_fill['dropdown'] && !empty($_fill['dropdown']) ? $_fill['dropdown'] : '';
                                ?>

                                <tr>
                                    <td><?php echo @$_no; ?></td>
                                    <td class="_ug_name"><?php echo @$_name; ?></td>
                                    <td><?php echo @$_type; ?></td>
                                    <td>
                                        <?php if ($_type === 'datetime') : ?>
                                            yyyy-mm-dd (e.g. 2020-01-28)
                                        <?php endif; ?>
                                    </td>
                                    <td class="_ug_option">
                                        <?php if ($_dropdown) : ?>

                                            <?php echo form_dropdown('dropdown', $_dropdown, '', 'class="form-control"'); ?>

                                            <!-- <ul class="_ul"> -->
                                            <?php //foreach ($_dropdown as $_dkey => $_drop): 
                                            ?>
                                            <!-- <li><?php //echo @$_drop; 
                                                        ?></li> -->
                                            <?php //endforeach;
                                            ?>
                                            <!-- </ul> -->

                                        <?php endif; ?>
                                        <!-- <ul class="_ul">
                <li>1 = Legal Staff</li>
                    <li>2 = Legal Head</li>
                    <li>3 = Agent</li>
               </ul> -->
                                    </td>
                                    <td>
                                        <?php if ($_required === 'Yes') : ?>
                                            <span class="kt-font-danger">Yes</span>
                                        <?php elseif ($_required === 'No') : ?>
                                            No
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php endif; ?>
                </table>
                <!--end: Datatable -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>