<?php
	$id 			= isset($transaction_post_dated_check['id']) && $transaction_post_dated_check['id'] ? $transaction_post_dated_check['id'] : '';
	$reference		=	isset($transaction_post_dated_check['transaction']['reference']) && $transaction_post_dated_check['transaction']['reference'] ? $transaction_post_dated_check['transaction']['reference'] : 'Not Available';
	$bank_id		=	isset($transaction_post_dated_check['bank_id']) && $transaction_post_dated_check['bank_id'] ? $transaction_post_dated_check['bank_id'] : 'Not Available';
	$due_date		=	isset($transaction_post_dated_check['due_date']) && $transaction_post_dated_check['due_date'] ? $transaction_post_dated_check['due_date'] : '';
	$amount			=	isset($transaction_post_dated_check['amount']) && $transaction_post_dated_check['amount'] ? $transaction_post_dated_check['amount'] : '';
	$pdc_status			=	isset($transaction_post_dated_check['pdc_status']) && $transaction_post_dated_check['pdc_status'] ? $transaction_post_dated_check['pdc_status'] : '';
	$particulars	=	isset($transaction_post_dated_check['particulars']) && $transaction_post_dated_check['particulars'] ? $transaction_post_dated_check['particulars'] : '';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">View Transaction</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('transaction_post_dated_check/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<a href="<?php echo site_url('transaction_post_dated_check');?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-5">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							General Information
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin::Form-->
					<div class="kt-widget13">
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Reference
							</span>
							<span class="kt-widget13__text kt-widget13__text--bold">
								<?php echo $reference; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc kt-align-right">
								Bank Name
							</span>
							<span class="kt-widget13__text">
								<?php echo $bank_id; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Due Date
							</span>
							<span class="kt-widget13__text kt-widget13__text--bold">
								<?php echo date('F d, Y', strtotime($due_date)); ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Particulars
							</span>
							<span class="kt-widget13__text">
								<?php echo $particulars; ?>
							</span>
						</div>
						<div class="kt-widget13__item">
							<span class="kt-widget13__desc">
								Status
							</span>
							<span class="kt-widget13__text">
								<?php echo $pdc_status; ?>
							</span>
						</div>

					</div>

					<!--end::Form-->
				</div>
			</div>
			<!--end::Portlet-->

		</div>
	</div>
</div>
<!-- begin:: Footer -->