<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_setting_items_model extends MY_Model
{
    public $table = 'accounting_setting_items'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'accounting_settings_id',
        'accounting_entry_type',
        'accounting_entry',
        'origin_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'accounting_settings_id' => array(
            'field' => 'accounting_settings_id',
            'label' => 'Accounting Settings',
            'rules' => 'trim|required',
        ),
        'accounting_entry_type' => array(
            'field' => 'accounting_entry_type',
            'label' => 'Accounting Entry Type',
            'rules' => 'trim|required',
        ),
        'accounting_entry' => array(
            'field' => 'accounting_entry',
            'label' => 'Accounting Entry',
            'rules' => 'trim|required',
        ),
        'origin_id' => array(
            'field' => 'origin_id',
            'label' => 'Accounting Entry',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // $this->has_many['table_name'] = array();
        // $this->has_one['accounting_setting_item'] = array('foreign_model' => 'company/Company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
        // $this->has_many['accounting_entry_items'] = array('foreign_model' => 'Accounting_entry_items_model', 'foreign_table' => 'accounting_entry_items', 'foreign_key' => 'accounting_entry_id', 'local_key' => 'id');

    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}
