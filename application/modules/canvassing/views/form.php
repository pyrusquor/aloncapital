<?php
    if($id) {
        $purchase_order_request = get_object_from_table($form_data['purchase_order_request_id'], 'purchase_order_requests');
    }else{
        $purchase_order_request = new stdClass();
        $purchase_order_request->reference = null;
        $purchase_order_request->request_date = null;
        $purchase_order_request->company_id = null;
        $purchase_order_request->accounting_ledger_id = null;
    }
?>
<script type="text/javascript">
    window.object_request_id = "<?php echo $id;?>";
    window.object_request_fillables = {};
    <?php foreach($fillables as $fillable):?>
    window.object_request_fillables["<?php echo $fillable;?>"] = null;
    <?php endforeach;?>

    window.purchase_order_request = {
        reference: "<?php echo $purchase_order_request->reference;?>",
        request_date: "<?php echo $purchase_order_request->request_date;?>",
        company_id: "<?php echo $purchase_order_request->company_id;?>",
        accounting_ledger_id: "<?php echo $purchase_order_request->accounting_ledger_id;?>",
    }

</script>
<!--begin::Form-->
<div id="canvassing_app">
    <form method="POST" class="kt-form kt-form--label-right" id="form_canvassing" enctype="multipart/form-data"
          action="<?php form_open('canvassing/form/' . @$info['id']); ?>">
        <!-- CONTENT HEADER -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title"><?= $method ?> Canvassing</h3>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="form_canvassing"
                                data-ktwizard-type="action-submit" @click.prevent="submitRequest()">
                            <i class="fa fa-plus-circle"></i> Submit
                        </button>
                        <a href="<?php echo site_url('canvassing'); ?>"
                           class="btn btn-label-instagram btn-elevate btn-sm">
                            <i class="fa fa-reply"></i> Cancel
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- CONTENT -->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class=" kt-portlet__body">
                        <?php $this->load->view('view/_form'); ?>
                        <?php if (!$id): ?>
                            <?php $this->load->view('view/_filter'); ?>
                        <?php else:?>
                            <?php $this->load->view('view/_rpo'); ?>
                        <?php endif; ?>
                        <?php $this->load->view('view/_items'); ?>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!-- begin:: Footer -->
    </form>
</div>
<!--end::Form-->
