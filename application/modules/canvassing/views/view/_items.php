<hr>
<div v-if="items.selected">
    <div class="row">
        <div class="col-sm-12">
            <h5>New Canvass for:</h5>
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Group</th>
                    <th>Class</th>
                    <th>Type</th>
                    <th>Brand</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ items.selected.item_name }}</td>
                    <td>{{ items.selected.item_group_name }}</td>
                    <td>{{ items.selected.item_class_name }}</td>
                    <td>{{ items.selected.item_type_name }}</td>
                    <td>{{ items.selected.item_brand_name }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Supplier <span class="kt-font-danger">*</span></label>
                <select class="form-control" id="supplier_id" v-model="items.form.supplier_id">
                    <option v-for="supplier in sources.suppliers" :value="supplier.id">
                        {{ supplier.name }}
                    </option>
                </select>
            </div>
        </div>
        <div class="fol-sm-12 col-md-6">
            <div class="form-group">
                <label>Unit Cost <span class="kt-font-danger">*</span></label>
                <input type="number" min="0" step="0.01" class="form-control" id="unit_cost" v-model="items.form.unit_cost">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-sm btn-success" @click.prevent="addItemToCart(true, null)">
                Add Item
            </button>
        </div>
    </div>
</div>
<div v-if="items.data.length > 0">
    <div class="row">
        <div class="col-sm-12">
            <h4>Canvassed Items</h4>

            <div class="responsive-table">
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Canvass Unit Price</th>
                        <th>Supplier</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(citem, idx) in items.data">
                        <td>{{ citem.item_name }}</td>
                        <td>{{ citem.unit_cost }}</td>
                        <td>{{ citem.supplier }}</td>
                        <td>
                            <a class="btn btn-sm btn-warning" href="#!" @click.prevent="loadItem(idx)">
                                Edit
                            </a>
                            <a class="btn btn-sm btn-danger" href="#!" @click.prevent="removeItem(idx)">
                                Delete
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>