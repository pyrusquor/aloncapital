<?php
    if ($id) {
        $approving_staff_info = get_person($form_data['approving_staff_id'], 'staff');
        $approving_staff_name = get_fname($approving_staff_info);

        $requesting_staff_id_info = get_person($form_data['requesting_staff_id'], 'staff');
        $requesting_staff_name = get_fname($requesting_staff_id_info);

        $company_data = get_object_from_table($form_data['company_id'], 'companies');
        $company_name = $company_data->name;

//        $warehouse_data = get_object_from_table($form_data['warehouse_id'], 'warehouses');
//        $warehouse_name = $warehouse_data->name;
//
//        $ledger_data = get_object_from_table($form_data['accounting_ledger_id'], 'accounting_ledgers');
//        $ledger_name = $ledger_data->name;


    } else {
        $approving_staff_info = null;
        $approving_staff_name = '';
        $requesting_staff_id_info = null;
        $requesting_staff_name = '';
        $company_data = null;
        $company_name = '';

        $warehouse_data = null;
        $warehouse_name = '';
        $ledger_data = null;
        $ledger_name = '';

    }
?>
<?php //if ($id): ?>
<!--    <div class="row mb-4">-->
<!--        <div class="col-sm-12">-->
<!--            <label>Status</label>-->
<!--            --><?php //echo form_dropdown('status', Dropdown::get_static("cnv_status"), $cnv_status, 'class="form-control form-control-sm _filter" v-model="info.form.status"') ?>
<!--        </div>-->
<!--    </div>-->
<?php //endif; ?>
<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="companies" id="company_id">
                <?php if ($company_name): ?>
                    <option value="<?php echo $form_data['company_id']; ?>"
                            selected><?php echo $company_name; ?></option>
                <?php endif ?>
            </select>
        </div>
    </div>
    <?php /*
    <div class="col-sm-6 col-md-4">
        <div class="form-group">
            <label>Warehouse <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="warehouses" id="warehouse_id">
                <?php if ($warehouse_name): ?>
                    <option value="<?php echo $form_data['warehouse_id']; ?>"
                            selected><?php echo $warehouse_name; ?></option>
                <?php endif ?>
            </select>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="form-group">
            <label>Expense Account <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="accounting_ledgers" id="accounting_ledger_id">
                <?php if ($ledger_name): ?>
                    <option value="<?php echo $form_data['accounting_ledger_id']; ?>"
                            selected><?php echo $ledger_name; ?></option>
                <?php endif ?>
            </select>
        </div>
    </div>
 */ ?>
</div>
<div class="row">

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id"
                    name="request[approving_staff_id]">
                <?php if ($approving_staff_name): ?>
                    <option value="<?php echo $form_data['approving_staff_id']; ?>"
                            selected>
                        <?php echo $approving_staff_name; ?></option>
                <?php endif ?>
            </select>

            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="staff" data-type="person"
                    id="requesting_staff_id" name="request[requesting_staff_id]">
                <?php if ($requesting_staff_name): ?>
                    <option value="<?php echo $form_data['requesting_staff_id']; ?>"
                            selected><?php echo $requesting_staff_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
