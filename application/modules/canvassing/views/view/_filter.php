<h3>Filter Purchase Order Requests</h3>
<div class="row">
    <div class="col-sm-6 col-md-3">
        <div class="form-group">
            <label>Company</label>
            <select class="form-control suggests" data-module="companies" id="filter_company_id">
            </select>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="form-group">
            <label>Expense Account</label>
            <select class="form-control suggests" data-module="accounting_ledgers" id="filter_accounting_ledger_id">
            </select>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="form-group">
            <label>Reference #</label>
            <input type="text" class="form-control" id="filter_reference" v-model="rpo.filter.reference">
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="form-group">
            <label>Date of Request <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control kt_datepicker" placeholder="Date of Request"
                       name="request[request_date]" id="filter_request_date"
                       v-model="rpo.filter.request_date" readonly>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                class="la la-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <div class="btn-group btn-group-sm">
                <button class="btn btn-primary" @click.prevent="filterRPO()">Filter</button>
                <button class="btn btn-secondary" @click.prevent="resetRPOFilter()">Clear</button>
            </div>
        </div>
    </div>
</div>
<div class="row" v-if="rpo.data.length > 0">
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Reference</th>
                <th>Date of Request</th>
                <th>Company</th>
                <th>Expense Account</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(r, r_key) in rpo.data">
                <td>
                    {{ r.reference }}
                </td>
                <td>
                    {{ r.request_date }}
                </td>
                <td>
                    {{ r.company.name }}
                </td>
                <td>
                    {{ r.accounting_ledger.name }}
                </td>
                <td>
                    <a class="btn-success btn btn-sm" href="#!"
                       @click.prevent="selectRPO(r_key);fetchItemObjects('purchase_order_request_items', 'rpo', 'items', r.id, 'purchase_order_request_id', null)">
                        Load Items
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row" v-if="rpo.selected">
    <div class="col-sm-12">
        <h4>Items for {{ rpo.selected.reference }}</h4>
        <div class="table-responsive">
            <table class="table table-condensed" v-if="rpo.items.length > 0">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Group</th>
                    <th>Class</th>
                    <th>Type</th>
                    <th>Brand</th>
                    <th>Quantity</th>
                    <th>Unit/Total Cost</th>
                    <!--                        <th>MR Reference</th>-->
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(rpo_item, rpo_item_key) in rpo.items">
                    <td>
                        {{ rpo_item.item_name }}
                    </td>
                    <td>
                        {{ rpo_item.item_group_name }}
                    </td>
                    <td>
                        {{ rpo_item.item_class_name }}
                    </td>
                    <td>
                        {{ rpo_item.item_type_name }}
                    </td>
                    <td>
                        {{ rpo_item.item_brand_name }}
                    </td>
                    <td>
                        {{ rpo_item.quantity }}
                    </td>
                    <td>
                        {{ rpo_item.unit_cost }} /
                        {{ rpo_item.total_cost }}
                    </td>
                    <td>
                        <a class="btn btn-sm btn-success" href="#!" @click.prevent="selectItemForCanvass(rpo_item_key)">
                            Canvas
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
