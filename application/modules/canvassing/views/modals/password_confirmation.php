<div class="modal fade" id="password_confirm_modal" tabindex="-1" role="dialog"
     aria-labelledby="password_confirm_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Identity</label>
                    <input type="email" class="form-control" id="identity" name="identity" v-model="confirm_form.identity">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" id="password" name="password" v-model="confirm_form.password">
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" @click.prevent="confirmIdentity()">Submit</button>
                <button type="button" class="btn btn-secondary">Close</button>
            </div>
        </div>
    </div>
</div>