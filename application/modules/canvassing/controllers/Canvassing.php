<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Canvassing extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Canvassing_model', 'M_Canvassing');

        $this->load->helper('form');

        $this->_table_fillables = $this->M_Canvassing->fillable;
        $this->_table_columns = $this->M_Canvassing->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Canvassing->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Canvassing->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue([
            'vendors/custom/datatables/datatables.bundle.js',
            'js/vue2.js',
            'js/axios.min.js'
        ]);

        $this->template->build('index', $this->view_data);
    }

    public function showCanvassings()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'canvassing' => true,
            'canvassing_id' => true,
            'purchase_order_request' => true,
            'purchase_order_request_id' => true,
            'supplier' => true,
            'supplier_id' => true,
            'company' => true,
            'company_id' => true,
            'item' => true,
            'item_id' => true,
            'unit_cost' => true,
            'created_by' => true,
            'updated_by' => true
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        //            if (isset($_REQUEST['filter'])) {
        //
        //            } else {
        //                $canvassings = $this->M_Canvassing->order_by('id', 'DESC')->as_array()->get_all();
        //            }
        $_filters = $_GET;
        $canvassings = $this->__search_items($_filters);

        $data = [];

        if ($canvassings) {

            foreach ($canvassings as $key => $value) {

                $canvassings[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $canvassings[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($canvassings as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }
        header('Content-Type: application/json');
        echo json_encode($output);
        exit();
    }

    private function __search_items($params)
    {
        $this->load->model('canvassing_item/canvassing_item_model', 'M_Canvassing_item');

        $canvassing_id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';

        if (!$canvassing_id) {
            $supplier_id = isset($params['supplier_id']) && !empty($params['supplier_id']) ? $params['supplier_id'] : null;
            $purchase_order_request_id = isset($params['purchase_order_request_id']) && !empty($params['purchase_order_request_id']) ? $params['purchase_order_request_id'] : null;
            $warehouse_id = isset($params['warehouse_id']) && !empty($params['warehouse_id']) ? $params['warehouse_id'] : null;
            $company_id = isset($params['company_id']) && !empty($params['company_id']) ? $params['company_id'] : null;
            $accounting_ledger_id = isset($params['accounting_ledger_id']) && !empty($params['accounting_ledger_id']) ? $params['accounting_ledger_id'] : null;
            $item_id = isset($params['item_id']) && !empty($params['item_id']) ? $params['item_id'] : null;
            $status = isset($params['status']) && !empty($params['status']) ? $params['status'] : null;

            if ($accounting_ledger_id) {
                $this->M_Canvassing_item->where('accounting_ledger_id', $accounting_ledger_id);
            }

            if ($supplier_id) {
                $this->M_Canvassing_item->where('supplier_id', $supplier_id);
            }

            if ($item_id) {
                $this->M_Canvassing_item->where('item_id', $item_id);
            }

            if ($company_id) {
                $this->M_Canvassing_item->where('company_id', $company_id);
            }

            if ($warehouse_id) {
                $this->M_Canvassing_item->where('warehouse_id', $warehouse_id);
            }

            if ($status) {
                $canvassings = $this->db->query("SELECT * FROM `canvassings` WHERE `status` = '$status'")->result();
                if ($canvassings) {
                    $canvassing_ids = concat_ids($canvassings, true);
                    $this->M_Canvassing_item->where_canvassing_id('canvassing_id', $canvassing_ids);
                } else {
                    return [];
                }
            }

            if ($purchase_order_request_id) {
                $this->M_Canvassing_item->where('purchase_order_request_id', $purchase_order_request_id);
            }
        } else {
            $this->M_Canvassing_item->where('canvassing_id', $canvassing_id);
        }

        $canvassings = $this->db->query("SELECT * FROM `canvassings` WHERE `deleted_at` IS NULL")->result();
        if ($canvassings) {
            $canvassing_ids = concat_ids($canvassings, true);
            $this->M_Canvassing_item->where_canvassing_id('canvassing_id', $canvassing_ids);
        } else {
            return [];
        }

        if ($with_relations === 'yes') {
            $result = $this->M_Canvassing_item->with_canvassing()->with_supplier()->with_company()->with_warehouse()->with_material_request()->with_purchase_order_request()->with_item_group()->with_item_type()->with_item_brand()->with_item_class()->with_item()->with_unit_of_measurement()->get_all();
        } else {
            $result = $this->M_Canvassing_item->get_all();
        }
        return $result;
    }

    public function search_items()
    {
        $result = $this->__search_items($_GET);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function __search($params)
    {
        $id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';

        if (!$id) {
            $company_id = isset($params['company_id']) && !empty($params['company_id']) ? $params['company_id'] : null;
            $request_date = isset($params['request_date']) && !empty($params['request_date']) ? $params['request_date'] : null;
            $accounting_ledger_id = isset($params['accounting_ledger_id']) && !empty($params['accounting_ledger_id']) ? $params['accounting_ledger_id'] : null;
            $status = isset($params['status']) && !empty($params['status']) ? $params['status'] : null;
            $reference = isset($params['reference']) && !empty($params['reference']) ? $params['reference'] : null;

            if ($company_id) {
                $this->M_Canvassing->where('company_id', $company_id);
            }
            if ($request_date) {
                $this->M_Canvassing->where('request_date', $request_date);
            }
            if ($accounting_ledger_id) {
                $this->M_Canvassing->where('accounting_ledger_id', $accounting_ledger_id);
            }
            if ($status) {
                $this->M_Canvassing->where('status', $status);
            }
            if ($reference) {
                $this->M_Canvassing->like('reference', $reference, 'both');
            }
        } else {
            $this->M_Canvassing->where('id', $id);
        }

        if ($with_relations === 'yes') {
            $result = $this->M_Canvassing->with_company()->with_approving_staff()->with_requesting_staff()->with_accounting_ledger()->get_all();
        } else {
            $result = $this->M_Canvassing->get_all();
        }

        return $result;
    }

    public function search()
    {
        $result = $this->__search($_GET);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function process_create_master($data, $additional)
    {
        $data['reference'] = uniqidCNV();
        // $request = $this->M_Purchase_order_request->insert($data);
        $request = $this->db->insert('canvassings', $data + $additional);
        if ($request) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    private function process_create_slave($data, $references, $additional)
    {
        $this->load->model('canvassing_item/Canvassing_item_model', 'M_Canvassing_item');

        $item = array(
            'canvassing_id' => $references['canvassing_id'],
            'supplier_id' => $data['supplier_id'],
            'warehouse_id' => $references['warehouse_id'],
            'company_id' => $references['company_id'],
            'material_request_id' => $data['material_request_id'],
            'purchase_order_request_id' => $data['purchase_order_request_id'],
            'purchase_order_request_item_id' => $data['purchase_order_request_item_id'],
            'item_group_id' => $data['item_group_id'],
            'item_type_id' => $data['item_type_id'],
            'item_brand_id' => $data['item_brand_id'],
            'item_class_id' => $data['item_class_id'],
            'item_id' => $data['item_id'],
            'unit_of_measurement_id' => $data['unit_of_measurement_id'],
            'item_brand_name' => $data['item_brand_name'],
            'item_class_name' => $data['item_class_name'],
            'item_name' => $data['item_name'],
            'unit_of_measurement_name' => $data['unit_of_measurement_name'],
            'item_group_name' => $data['item_group_name'],
            'item_type_name' => $data['item_type_name'],
            'unit_cost' => $data['unit_cost'],
            'created_at' => $additional['created_at'],
            'created_by' => $additional['created_by'],
        );

        return $this->db->insert('canvassing_items', $item);
    }

    public function process_create()
    {
        header('Content-Type: application/json');
        if ($this->input->post()) {
            $__request = $this->input->post('request');
            $request = [];
            $items = $this->input->post('items');

            foreach ($this->M_Canvassing->form_fillables as $key) {
                if (array_key_exists($key, $__request)) {
                    $request[$key] = $__request[$key];
                }
            }

            $this->db->trans_start();
            $this->db->trans_strict(false);

            $additional = array(
                'created_at' => NOW,
                'created_by' => $this->user->id
            );

            $request_object = $this->process_create_master($request, $additional);
            if ($request_object) {
                $references = array(
                    'canvassing_id' => $request_object,
                    'warehouse_id' => $request['warehouse_id'],
                    'company_id' => $request['company_id']
                );
                foreach ($items as $item) {
                    $this->process_create_slave($item, $references, $additional);
                }
            }

            $this->db->trans_complete(); # Completing request
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
                $response['message'] = 'Error!';
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();

                $response['status'] = 1;
                $response['message'] = 'Request Successfully saved!';
            }

            echo json_encode($response);
            exit();
        }
    }

    private function process_update_master($id, $data, $additional)
    {
        return $this->M_Canvassing->update($data + $additional, $id);
    }

    function process_update_slave($data, $additional, $references)
    {
        $id = isset($data['id']) ? $data['id'] : null;
        $this->load->model('canvassing_item/Canvassing_item_model', 'M_Canvassing_item');

        $temp_data = $data + $additional;
        $update_data = [];
        foreach ($this->M_Canvassing_item->form_fillables as $key) {
            if (array_key_exists($key, $temp_data)) {
                $update_data[$key] = $temp_data[$key];
            }
        }

        if ($id) {
            // return $this->db->update('canvassing_items');
            return $this->M_Canvassing_item->update($update_data, $id);
        } else {
            $additional = [
                'created_by' => $additional['updated_by'],
                'created_at' => $additional['updated_at']
            ];

            return $this->process_create_slave($data, $references, $additional);
        }
    }

    public function process_update($id)
    {
        header('Content-Type: application/json');
        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        if ($this->input->post()) {
            $__request = $this->input->post('request');
            $request = [];
            $items = $this->input->post('items');

            foreach ($this->M_Canvassing->form_fillables as $key) {
                if (array_key_exists($key, $__request)) {
                    $request[$key] = $__request[$key];
                }
            }

            $this->db->trans_start();
            $this->db->trans_strict(false);
            $request_object = $this->process_update_master($id, $request, $additional);
            if ($request_object) {
                $references = [
                    'canvassing_id' => $id,
                    'warehouse_id' => $request['warehouse_id']
                ];
                foreach ($items as $item) {
                    $this->process_update_slave($item, $additional, $references);
                }
            }
            $deleted_items = $this->input->post('deleted_items');

            $this->db->trans_complete(); # Completing request
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
                $response['message'] = 'Error!';
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $this->process_purge_deleted($deleted_items, $additional);

                $response['status'] = 1;
                $response['message'] = 'Request Successfully saved!';
            }

            echo json_encode($response);
            exit();
        }
    }

    private function process_purge_deleted($items, $additional)
    {
        $this->load->model('canvassing_item/Canvassing_item_model', 'M_Canvassing_item');
        if ($items) {
            foreach ($items as $item) {
                if ($item) {
                    $this->M_Canvassing_item->delete($item);
                }
            }
        }
    }

    public function form($id = false)
    {
        if ($id) {
            $method = "Update";
            $canvassing = $this->M_Canvassing->get($id);
            $form_data = fill_form_data($this->M_Canvassing->form_fillables, $canvassing);
            $reference = $form_data['reference'];
        } else {
            $method = "Create";
            $canvassing = null;
            $reference = null;
            $form_data = fill_form_data($this->M_Canvassing->form_fillables);
        }

        $this->view_data['fillables'] = $this->M_Canvassing->form_fillables;
        $this->view_data['current_user'] = $this->user->id;
        $this->view_data['method'] = $method;
        $this->view_data['canvassing'] = $canvassing;
        $this->view_data['id'] = $id;
        $this->view_data['form_data'] = $form_data;
        $this->view_data['reference'] = $reference;

        $this->js_loader->queue([
            'js/vue2.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);

        $this->template->build('form', $this->view_data);
    }

    public function __form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Canvassing->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $canvassing_status = $this->M_Canvassing->update($info + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $canvassing_status = $this->M_Canvassing->insert($info + $additional);
                }

                if ($canvassing_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Canvassing Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_Canvassing->get($id);
        }

        $this->view_data['method'] = $method;
        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Canvassing->get($id);
            if ($list) {

                $deleted = $this->M_Canvassing->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Canvassing Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_Canvassing->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Canvassing Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = FALSE)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_Canvassing->get($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'name' => $upload_data[0],
                        /* ==================== begin: Add model fields ==================== */

                        /* ==================== end: Add model fields ==================== */
                    );

                    $canvassing_id = $upload_data[0];
                    $canvassing = $this->M_Canvassing->get($canvassing_id);

                    if ($canvassing) {
                        $result = $this->M_Canvassing->update($fields, $canvassing_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'name' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Canvassing->insert($fields);
                    } else {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Canvassing->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'canvassing');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'canvassing');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? TRUE : FALSE;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Canvassing CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'canvassing');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Canvassing->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Canvassing');
            $_objSheet->setCellValue('A1', 'CANVASSING');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $canvassings = $this->M_Canvassing->as_array()->get_all();
        if ($canvassings) {

            foreach ($canvassings as $_lkey => $canvassing) {

                $_datas[$canvassing['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_canvassings_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'canvassing');
                }
            } else {

                $_filename = 'list_of_canvassings_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'canvassing');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($canvassings as $_lkey => $canvassing) {

                                $_datas[$canvassing['id']][$_title] = isset($canvassing[$_column]) && ($canvassing[$_column] !== '') ? Dropdown::get_static('bool', $canvassing[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($canvassings as $_lkey => $canvassing) {

                                if ($_name === 'status') {

                                    $_datas[$canvassing['id']][$_title] = isset($canvassing[$_name]) && $canvassing[$_name] ? Dropdown::get_static('inventory_status', $canvassing[$_name], 'view') : '';
                                } else {

                                    $_datas[$canvassing['id']][$_title] = isset($canvassing[$_name]) && $canvassing[$_name] ? $canvassing[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Canvassing');
                $_objSheet->setCellValue('A1', 'LIST OF CANVASSING');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'canvassing');
            }
        } else {

            $this->notify->error('No Record Found', 'canvassing');
        }
    }
}
