<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Land_inventory_model extends MY_Model {

	public $primary_key	=	'id'; // you MUST mention the primary key
	public $protected		=	['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $fillable		=	[
													'land_owner_name',
													'location',
													'owner_contact_info',
													'land_area',
													'estimated_price',
													'negotiated_price',
													'final_price',
													'agent_id',
													'land_classification_id',
													'location_of_registration',
													'title',
													'title_details',
													'lot_number',
													'number_of_lot_to_subdivide',
													'project',
													'encumbrance',
													'ownership_classification_id',
													'status',
													'date_offered',
													'decision_date',
													'date_purchased',
													'date_transfer_of_title',
													'ep_code',
													'market_value',
													'date_of_market_value_assessment',
													'appraised_value',
													'date_of_appraisal',
													'assessed_value',
													'mother_lot_tax_declaration',
													'tax_declaration',
													'company_id',
													'subdivide_company_id',
													'remarks',
													'created_by',
													'created_at',
													'updated_by',
													'updated_at',
													'deleted_by',
													'deleted_at'
												]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $table				=	'land_inventories'; // you MUST mention the table name
	public $rules				=	[];
	public $_fields			=	[
													'land_owner_name'	=>	array(
														'field'	=>	'land_owner_name',
														'label'	=>	'Land Owner Name',
														'rules'	=>	'trim|required|min_length[2]'
													),
													'location'	=>	array(
														'field'	=>	'location',
														'label'	=>	'Location',
														'rules'	=>	'trim|required|min_length[2]'
													),
													'owner_contact_info'	=>	array(
														'field'	=>	'owner_contact_info',
														'label'	=>	'Owner Contact Info',
														'rules'	=>	'trim'
													),
													'land_area'	=>	array(
														'field'	=>	'land_area',
														'label'	=>	'Land Area',
														'rules'	=>	'trim|required'
													),
													'estimated_price'	=>	array(
														'field'	=>	'estimated_price',
														'label'	=>	'Estimated Price',
														'rules'	=>	'trim'
													),
													'negotiated_price'	=>	array(
														'field'	=>	'negotiated_price',
														'label'	=>	'Negotiated Price',
														'rules'	=>	'trim'
													),
													'final_price'	=>	array(
														'field'	=>	'final_price',
														'label'	=>	'Final Price',
														'rules'	=>	'trim'
													),
													'date_offered'	=>	array(
														'field'	=>	'date_offered',
														'label'	=>	'Date Offered',
														'rules'	=>	'trim'
													),
													'decision_date'	=>	array(
														'field'	=>	'decision_date',
														'label'	=>	'Decision Date',
														'rules'	=>	'trim'
													),
													'ep_code'	=>	array(
														'field'	=>	'ep_code',
														'label'	=>	'EP Code',
														'rules'	=>	'trim'
													),
													'assessed_value'	=>	array(
														'field'	=>	'assessed_value',
														'label'	=>	'Assessed Value',
														'rules'	=>	'trim'
													),
													'mother_lot_tax_declaration'	=>	array(
														'field'	=>	'mother_lot_tax_declaration',
														'label'	=>	'Mother Lot Tax Declaration',
														'rules'	=>	'trim'
													),
													'land_classification_id'	=>	array(
														'field'	=>	'land_classification_id',
														'label'	=>	'Land Classification',
														'rules'	=>	'trim|required'
													),
													'location_of_registration'	=>	array(
														'field'	=>	'location_of_registration',
														'label'	=>	'Location of Registration',
														'rules'	=>	'trim'
													),
													'title'	=>	array(
														'field'	=>	'title',
														'label'	=>	'Title',
														'rules'	=>	'trim'
													),
													'title_details'	=>	array(
														'field'	=>	'title_details',
														'label'	=>	'Title Details',
														'rules'	=>	'trim'
													),
													'lot_number'	=>	array(
														'field'	=>	'lot_number',
														'label'	=>	'Lot Number',
														'rules'	=>	'trim'
													),
													'encumbrance'	=>	array(
														'field'	=>	'encumbrance',
														'label'	=>	'Encumbrance',
														'rules'	=>	'trim'
													),
													'date_transfer_of_title'	=>	array(
														'field'	=>	'date_transfer_of_title',
														'label'	=>	'Date Transfer of Title',
														'rules'	=>	'trim'
													),
													'ownership_classification_id'	=>	array(
														'field'	=>	'ownership_classification_id',
														'label'	=>	'Ownership Classification',
														'rules'	=>	'trim|required'
													),
													'status'	=>	array(
														'field'	=>	'status',
														'label'	=>	'Status',
														'rules'	=>	'trim|required'
													),
													'tax_declaration'	=>	array(
														'field'	=>	'tax_declaration',
														'label'	=>	'Tax Declaration',
														'rules'	=>	'trim'
													),
													'company_id'	=>	array(
														'field'	=>	'company_id',
														'label'	=>	'Company',
														'rules'	=>	'trim'
													),
													'project'	=>	array(
														'field'	=>	'project',
														'label'	=>	'Project',
														'rules'	=>	'trim'
													),
													'remarks'	=>	array(
														'field'	=>	'remarks',
														'label'	=>	'Remarks',
														'rules'	=>	'trim'
													),
												];

	function __construct () {

		parent::__construct();

		$this->soft_deletes = TRUE;

		$this->return_as = 'array';

		$this->rules['insert']	=	$this->_fields;
		$this->rules['update']	=	$this->_fields;

		$this->has_one['company'] = array('foreign_model' => 'company/Company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
	}

	function find_all ( $_where = FALSE, $_columns = FALSE, $_row = FALSE ) {

		$_return	=	FALSE;

		if ( $_where ) {

			if ( is_array($_where) ) {

				foreach ( $_where as $key => $_wang ) {

					$this->db->where( $key, $_wang );
				}
			} else {

				$this->db->where( 'id', $_where );
			}
		} else {

			$this->db->where('deleted_at IS NULL');
		}

		if ( $_columns ) {

			if ( is_array($_columns) ) {

				foreach ( $_columns as $key => $_col ) {

					$this->db->select($_col);
				}
			} else {

				$this->db->select($_columns);
			}
		} else {

			$this->db->select('*');
		}

		// if ( $_limit ) {

		// 	if ( is_numeric($_limit) ) {

		// 		$this->db->limit($_limit);
		// 	}
		// }

		$_query	=	$this->db->get($this->table);
		if ( $_row ) {

			$_return	=	$_query->num_rows() > 0 ? $_query->row() :  FALSE;
		} else {

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}

	function insert_dummy () {

		require APPPATH.'/third_party/faker/autoload.php';

		$_faker	=	Faker\Factory::create();

		$_datas	=	[];

		for ( $i = 1; $i <= 5; $i++ ) {

			array_push($_datas, [
				'land_owner_name'	=>	$_faker->name,
				'location'	=>	$_faker->state,
				'owner_contact_info'	=>	$_faker->tollFreePhoneNumber,
				'land_area'	=>	$_faker->randomFloat(2, 1111, 9999),
				'estimated_price'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'negotiated_price'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'final_price'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'agent_id'	=>	rand(1,13),
				'land_classification_id'	=>	rand(1,37),
				'location_of_registration'	=>	$_faker->state,
				'title'	=>	$_faker->lastName,
				'title_details'	=>	$_faker->lastName,
				'lot_number'	=>	$_faker->secondaryAddress,
				'encumbrance'	=>	NULL,
				'ownership_classification_id'	=>	rand(1,2),
				'status'	=>	rand(1,15),
				'date_offered'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'decision_date'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'date_purchased'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'date_transfer_of_title'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'ep_code'	=>	rand(11234,33456),
				'market_value'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'date_of_market_value_assessment'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'appraised_value'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'date_of_appraisal'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'assessed_value'	=>	$_faker->randomFloat(2, 1234567, 9999999),
				'mother_lot_tax_declaration'	=>	rand(1234,3456),
				'tax_declaration'	=>	rand(1234,3456),
				'company'	=>	$_faker->Company,
				'remarks'	=>	'Unidentified Lots',
				'created_by'	=>	rand(1,6),
				'created_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'updated_by'	=>	rand(1,6),
				'updated_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time()))
			]);
		}

		$this->db->insert_batch($this->table, $_datas);
	}
}