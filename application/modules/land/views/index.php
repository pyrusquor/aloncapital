<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Land Inventory</h3>
			<span class="kt-subheader__desc" id="total"></span>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
				<input type="text" class="form-control" placeholder="Search property..." id="generalSearch">
				<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<!-- <a href="<?php echo site_url('land/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
					<i class="fa fa-plus"></i> Create
				</a>
				<button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm"
					data-toggle="collapse" data-target="#_advance_search" aria-expanded="true"
					aria-controls="_advance_search">
					<i class="fa fa-filter"></i> Filter
				</button> -->
				<a href="<?= base_url(); ?>land/dashboard" class="btn btn-label-success btn-elevate btn-sm" target="_BLANK">
					<i class="fa fa-chart-bar"></i> Dashboard
				</a>
				<button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
					<i class="fa fa-upload"></i> Import
				</button>
				<button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#_export_option">
					<i class="fa fa-download"></i> Export
				</button>
			</div>
		</div>
	</div>
</div>


<div class="module__cta">
	<div class="kt-container  kt-container--fluid ">
		<div class="module__create">
			<a href="<?php echo site_url('land/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
				<i class="fa fa-plus"></i> Create Land Inventory
			</a>
		</div>

		<div class="module__filter">
			<button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
				<i class="fa fa-filter"></i> Filter
			</button>
		</div>
	</div>
</div>


<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__body">
		<div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
			<div class="row">
				<div class="col-lg-12">
					<form class="kt-form" id="advance_search">
						<div class="form-group row">
							<div class="col-sm-3 mb-3">
								<label class="form-control-label">Company</label>
								<div class="kt-input-icon">
									<select class="form-control form-control-sm _filter suggests" data-module="companies" name="company_id">
										<option value="">Select Company</option>
									</select>
								</div>
							</div>
							<div class="col-sm-3 mb-3">
								<label class="form-control-label">Land Owner</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" class="form-control _filter" placeholder="Land Owner" name="land_owner_name">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3 mb-3">
								<label class="form-control-label">Title</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" class="form-control _filter" placeholder="Title" name="title">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3 mb-3">
								<label class="form-control-label">Lot Number</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" class="form-control _filter" placeholder="Lot Number" name="lot_number">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3 mb-3">
								<label class="form-control-label">Location</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" class="form-control _filter" placeholder="Location" name="location">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3 mb-3">
								<label class="form-control-label">Land Classification</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select class="form-control _filter" name="land_classification_id">
										<?php foreach (Dropdown::get_static('land_classification') as $key => $value) : ?>
											<option value="<?= $key ? $key : '' ?>"><?= $value ?></option>
										<?php endforeach; ?>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3 mb-3">
								<label class="form-control-label">Status</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select class="form-control _filter" name="status">
										<?php foreach (Dropdown::get_static('land_inventory_status') as $key => $value) : ?>
											<option value="<?= $key ? $key : '' ?>"><?= $value ?></option>
										<?php endforeach; ?>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
			<form class="kt-form" id="_export_csv" action="<?php echo site_url('land/export_csv'); ?>" method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label class="form-control-label">File Type</label>
							<div class="kt-input-icon  kt-input-icon--left">
								<select class="form-control form-control-sm" name="update_existing_data">
									<option value=""> -- Update Existing Data -- </option>
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
								<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
							</div>
							<?php echo form_error('update_existing_data'); ?>
							<span class="form-text text-muted"></span>
						</div>
					</div>
				</div>
			</form>
			<form class="kt-form" id="_upload_form" action="<?php echo site_url('land/import'); ?>" method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label class="form-control-label">Upload CSV file:</label>
							<label class="form-control-label text-muted">Note: Maximum of 1,000 items only per
								file.</label>
							<input type="file" name="csv_file" class="" size="1000" accept="*.csv">
							<span class="form-text text-muted"></span>
						</div>
					</div>
				</div>
			</form>
			<div class="form-group form-group-last row custom_import_style">
				<div class="col-lg-3">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm" form="_upload_form">
								<i class="fa fa-upload"></i> Upload
							</button>
						</div>
						<div class="col-lg-6 kt-align-right">
							<button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" form="_export_csv">
								<i class="fa fa-file-csv"></i>
							</button>
							<button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
								<i class="fa fa-info-circle"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_land_inventory_table">
			<thead>
				<tr class="text-center">
					<th width="1%">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<input type="checkbox" value="all" class="m-checkable" id="select-all">
							<span></span>
						</label>
					</th>
					<th>ID</th>
					<th>Company</th>
					<th>Land Owner Name</th>
					<th>Title</th>
					<th>Lot Number</th>
					<th>Location</th>
					<th>Land Area</th>
					<th>Land Classification</th>
					<th>Status</th>
					<th>Created By</th>
					<th>Last Update By</th>
					<th>Actions</th>
				</tr>
			</thead>
		</table>
		<!--end: Datatable -->
	</div>
</div>

<!--begin::Modal-->
<!-- <div class="modal fade show" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true" style="padding-right: 15px; display: block;"> -->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_export_option_label">Export Options</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="_export_form" target="_blank" action="<?php echo site_url('land/export'); ?>" method="POST">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group form-group-last kt-hide">
								<div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
									<div class="alert-icon"><i class="flaticon-warning"></i></div>
									<div class="alert-text">
										Oh snap! You need select at least one.
									</div>
									<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
									</div>
								</div>
							</div>
						</div>
						<?php if (isset($_columns) && $_columns) : ?>

							<div class="col-lg-11 offset-lg-1">
								<div class="kt-checkbox-list">
									<label class="kt-checkbox kt-checkbox--bold">
										<input type="checkbox" id="_export_select_all"> Field
										<span></span>
									</label>
									<label class="kt-checkbox kt-checkbox--bold"></label>
								</div>
							</div>

							<?php foreach ($_columns as $key => $_column) : ?>

								<?php if ($_column) : ?>

									<?php
									$_offset = '';
									if ($_column === reset($_columns)) {

										$_offset = 'offset-lg-1';
									}
									?>

									<div class="col-lg-5 <?php echo isset($_offset) && $_offset ? $_offset : ''; ?>">
										<div class="kt-checkbox-list">
											<?php foreach ($_column as $_ckey => $_clm) : ?>

												<?php
												$_label = isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
												$_value = isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
												?>

												<label class="kt-checkbox kt-checkbox--bold">
													<input type="checkbox" name="_export_column[]" class="_export_column" value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
													<span></span>
												</label>
											<?php endforeach; ?>
										</div>
									</div>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php else : ?>

							<div class="col-lg-10 offset-lg-1">
								<div class="form-group form-group-last">
									<div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">
											Something went wrong. Please contact your system administrator.
										</div>
										<div class="alert-close">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true"><i class="la la-close"></i></span>
											</button>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<button type="submit" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" form="_export_form">
						<i class="fa fa-file-export"></i> Export
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<!-- <div class="modal fade show" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true" style="padding-right: 15px; display: block;"> -->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="upload_guide_label">Document Upload Guide</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<table class="table table-head-bg-brand table-striped table-hover table-bordered table-condensed table-checkable" id="_upload_guide_modal">
					<thead>
						<tr class="text-center">
							<th width="1%" scope="col">#</th>
							<th scope="col">Name</th>
							<th scope="col">Type</th>
							<th scope="col">Format</th>
							<th scope="col">Option</th>
							<th scope="col">Required</th>
						</tr>
					</thead>
					<?php if (isset($_fillables) && $_fillables) : ?>

						<tbody>
							<?php foreach ($_fillables as $_fkey => $_fill) : ?>
								<?php
								$_no = isset($_fill['no']) && $_fill['no'] ? $_fill['no'] : '';
								$_name = isset($_fill['name']) && $_fill['name'] ? $_fill['name'] : '';
								$_type = isset($_fill['type']) && $_fill['type'] ? $_fill['type'] : '';
								$_required = isset($_fill['required']) && $_fill['required'] ? $_fill['required'] : '';
								$_dropdown = isset($_fill['dropdown']) && $_fill['dropdown'] && !empty($_fill['dropdown']) ? $_fill['dropdown'] : '';
								?>

								<tr>
									<td><?php echo @$_no; ?></td>
									<td class="_ug_name"><?php echo @$_name; ?></td>
									<td><?php echo @$_type; ?></td>
									<td>
										<?php if ($_type === 'datetime') : ?>
											yyyy-mm-dd (e.g. 2020-01-28)
										<?php endif; ?>
									</td>
									<td class="_ug_option">


										<?php if ($_dropdown) : ?>
											<?php echo form_dropdown('dropdown', $_dropdown, '', 'class="form-control"'); ?>

											<!-- <ul class="_ul"> -->
											<?php //foreach ($_dropdown as $_dkey => $_drop): 
											?>
											<!-- <li><?php //echo @$_drop; 
														?></li> -->
											<?php //endforeach;
											?>
											<!-- </ul> -->

										<?php endif; ?>


									</td>
									<td>
										<?php if ($_required === 'Yes') : ?>
											<span class="kt-font-danger">Yes</span>
										<?php elseif ($_required === 'No') : ?>
											No
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					<?php endif; ?>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm btn-icon-sm" data-dismiss="modal">
					<i class="la la-close"></i> Close
				</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!-- Start Consolidate Modal-->
<div class="modal fade" id="_consolidate_modal" tabindex="-1" role="dialog" aria-labelledby="_consolidate_modal_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_consolidate_modal_label">Consolidate</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" action="<?php echo site_url('land/consolidate'); ?>" id="_consolidate_form" method="POST">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Owner Name <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="land_owner_name" class="form-control" placeholder="Land Owner Name" value="<?php echo set_value('land_owner_name', @$_land_owner_name); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
								</div>
								<?php echo form_error('land_owner_name'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Classification <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php echo form_dropdown('land_classification_id', Dropdown::get_static('land_classification'), set_value('land_classification_id', @$_land_classification_id), 'class="form-control"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
								</div>
								<?php echo form_error('land_classification_id'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Location <code>(Full Address) *</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="location" class="form-control" placeholder="Full Address" value="<?php echo set_value('location', @$_location); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
								</div>
								<?php echo form_error('location'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Company</label>
								<div class="kt-input-icon kt-input-icon--left">
									<select class="form-control" name="company_id" placeholder="Type" autocomplete="off" id="company_id">
										<option>Select Company</option>
										<?php foreach ($companies as $key => $value) : ?>
											<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
										<?php endforeach; ?>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
								</div>
								<?php echo form_error('company_id'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Lot Number</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="lot_number" class="form-control" placeholder="Lot Number" value="<?php echo set_value('lot_number', @$_lot_number); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-legal"></i></span></span>
								</div>
								<?php echo form_error('lot_number'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Status <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php echo form_dropdown('status', Dropdown::get_static('land_inventory_status'), set_value('status', 12), 'class="form-control"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-leaf"></i></span></span>
								</div>
								<?php echo form_error('status'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Area <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<div class="input-group">
										<input type="text" name="land_area" class="form-control" placeholder="Land Area" value="<?php echo set_value('land_area', @$_land_area); ?>" autocomplete="off">
										<div class="input-group-append"><span class="input-group-text">SQM</span></div>
									</div>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map"></i></span></span>
								</div>
								<?php echo form_error('land_area'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Remarks</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="remarks" class="form-control" placeholder="Remarks" value="<?php echo set_value('remarks', @$_remarks); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-comment"></i></span></span>
								</div>
								<?php echo form_error('remarks'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_consolidate_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Consolidate Modal-->

<!-- Start Subdivide Modal-->
<div class="modal fade" id="_subdivide_modal" tabindex="-1" role="dialog" aria-labelledby="_subdivide_modal_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_subdivide_modal_label">Subdivide</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" action="<?php echo site_url('land/subdivide'); ?>" id="_subdivide_form" method="POST">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Owner Name <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="land_owner_name" class="form-control" placeholder="Land Owner Name" value="<?php echo set_value('land_owner_name', @$_land_owner_name); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
								</div>
								<?php echo form_error('land_owner_name'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Classification <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php echo form_dropdown('land_classification_id', Dropdown::get_static('land_classification'), set_value('land_classification_id', @$_land_classification_id), 'class="form-control"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
								</div>
								<?php echo form_error('land_classification_id'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Location <code>(Full Address) *</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="location" class="form-control" placeholder="Full Address" value="<?php echo set_value('location', @$_location); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
								</div>
								<?php echo form_error('location'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Company</label>
								<div class="kt-input-icon kt-input-icon--left">
									<select class="form-control" name="company_id" placeholder="Type" autocomplete="off" id="subdivide_company_id">
										<option>Select Company</option>
										<?php foreach ($companies as $key => $value) : ?>
											<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
										<?php endforeach; ?>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
								</div>
								<?php echo form_error('company_id'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Area <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<div class="input-group">
										<input type="text" name="land_area" class="form-control" placeholder="Land Area" value="<?php echo set_value('land_area', @$_land_area); ?>" autocomplete="off">
										<div class="input-group-append"><span class="input-group-text">SQM</span></div>
									</div>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map"></i></span></span>
								</div>
								<?php echo form_error('land_area'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Status <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php echo form_dropdown('status', Dropdown::get_static('land_inventory_status'), set_value('status', 12), 'class="form-control"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-leaf"></i></span></span>
								</div>
								<?php echo form_error('status'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Lot Number</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="lot_number" class="form-control" placeholder="Lot Number" value="<?php echo set_value('lot_number', @$_lot_number); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-legal"></i></span></span>
								</div>
								<?php echo form_error('lot_number'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="form-control-label">Number of Lots to Subdivide</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="number_of_lot_to_subdivide" class="form-control" placeholder="Number of Lots to Subdivide" value="<?php echo set_value('number_of_lot_to_subdivide', @$_number_of_lot_to_subdivide); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span></span>
								</div>
								<?php echo form_error('number_of_lot_to_subdivide'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="form-control-label">Suffix</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select name="suffix" id="suffix" class="form-control">
										<option value="1">Letter</option>
										<option value="2">Number</option>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span></span>
								</div>
								<?php echo form_error('number_of_lot_to_subdivide'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Project</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="project" class="form-control" placeholder="Project" value="<?php echo set_value('project', @$_project); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-tag"></i></span></span>
								</div>
								<?php echo form_error('project'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Remarks</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="remarks" class="form-control" placeholder="Remarks" value="<?php echo set_value('remarks', @$_remarks); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-comment"></i></span></span>
								</div>
								<?php echo form_error('remarks'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_subdivide_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Subdivide Modal-->

<!-- Start Consolidate and Subdivide Modal-->
<div class="modal fade" id="_consolidate_and_subdivide_modal" tabindex="-1" role="dialog" aria-labelledby="_consolidate_and_subdivide_modal_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_consolidate_and_subdivide_modal_label">Consolidate and Subdivide</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="_consolidate_and_subdivide_form" method="POST">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Owner Name <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="land_owner_name" class="form-control" placeholder="Land Owner Name" value="<?php echo set_value('land_owner_name', @$_land_owner_name); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
								</div>
								<?php echo form_error('land_owner_name'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Classification <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php echo form_dropdown('land_classification_id', Dropdown::get_static('land_classification'), set_value('land_classification_id', @$_land_classification_id), 'class="form-control"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
								</div>
								<?php echo form_error('land_classification_id'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Location <code>(Full Address) *</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="location" class="form-control" placeholder="Full Address" value="<?php echo set_value('location', @$_location); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
								</div>
								<?php echo form_error('location'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Company</label>
								<div class="kt-input-icon kt-input-icon--left">
									<select class="form-control" name="company_id" id="consolidate_and_subdivide_company_id">
										<option>Select Company</option>
										<?php foreach ($companies as $key => $value) : ?>
											<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
										<?php endforeach; ?>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
								</div>
								<?php echo form_error('company_id'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Land Area <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<div class="input-group">
										<input type="text" name="land_area" class="form-control" placeholder="Land Area" value="<?php echo set_value('land_area', @$_land_area); ?>" autocomplete="off">
										<div class="input-group-append"><span class="input-group-text">SQM</span></div>
									</div>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map"></i></span></span>
								</div>
								<?php echo form_error('land_area'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Status <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php echo form_dropdown('status', Dropdown::get_static('land_inventory_status'), set_value('status', 12), 'class="form-control"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-leaf"></i></span></span>
								</div>
								<?php echo form_error('status'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Lot Number</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="lot_number" class="form-control" placeholder="Lot Number" value="<?php echo set_value('lot_number', @$_lot_number); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-legal"></i></span></span>
								</div>
								<?php echo form_error('lot_number'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="form-control-label">Number of Lots to Consolidate and Subdivide</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="number_of_lot_to_subdivide" class="form-control" placeholder="Number of Lots to Consolidate and Subdivide" value="<?php echo set_value('number_of_lot_to_subdivide', @$_number_of_lot_to_subdivide); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span></span>
								</div>
								<?php echo form_error('number_of_lot_to_subdivide'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="form-control-label">Suffix</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select name="suffix" id="suffix" class="form-control">
										<option value="1">Letter</option>
										<option value="2">Number</option>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span></span>
								</div>
								<?php echo form_error('number_of_lot_to_subdivide'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Project</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="project" class="form-control" placeholder="Project" value="<?php echo set_value('project', @$_project); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-tag"></i></span></span>
								</div>
								<?php echo form_error('project'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="form-control-label">Remarks</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="remarks" class="form-control" placeholder="Remarks" value="<?php echo set_value('remarks', @$_remarks); ?>" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-comment"></i></span></span>
								</div>
								<?php echo form_error('remarks'); ?>
								<span class="form-text text-muted"></span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_consolidate_and_subdivide_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Consolidate and Subdivide Modal-->