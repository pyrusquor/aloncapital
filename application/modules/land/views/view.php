<?php
// vdebug($_land);
$_id = isset($_land['id']) && $_land['id'] ? $_land['id'] : '';
# GENERAL INFORMATION
$_land_owner_name = isset($_land['land_owner_name']) && $_land['land_owner_name'] ? $_land['land_owner_name'] : '';
$_location = isset($_land['location']) && $_land['location'] ? $_land['location'] : '';
$_owner_contact_info = isset($_land['owner_contact_info']) && $_land['owner_contact_info'] ? $_land['owner_contact_info'] : '';
$_date_offered = isset($_land['date_offered']) && $_land['date_offered'] && (strtotime($_land['date_offered']) > 0) ? date_format(date_create($_land['date_offered']), 'F j, Y') : '';
$_decision_date = isset($_land['decision_date']) && $_land['decision_date'] && (strtotime($_land['decision_date']) > 0) ? date_format(date_create($_land['decision_date']), 'F j, Y') : '';
$_date_transfer_of_title = isset($_land['date_transfer_of_title']) && $_land['date_transfer_of_title'] ? $_land['date_transfer_of_title'] : '';
$_status = isset($_land['status']) && $_land['status'] ? $_land['status'] : '';
$_created_at = isset($_land['created_at']) && $_land['created_at'] && (strtotime($_land['created_at']) > 0) ? date_format(date_create($_land['created_at']), 'F j, Y') : '';
$_updated_at = isset($_land['updated_at']) && $_land['updated_at'] && (strtotime($_land['updated_at']) > 0) ? date_format(date_create($_land['updated_at']), 'F j, Y') : '';

# LAND INFORMATION
$_land_classification_id = isset($_land['land_classification_id']) && $_land['land_classification_id'] ? $_land['land_classification_id'] : '';
$_location_of_registration = isset($_land['location_of_registration']) && $_land['location_of_registration'] ? $_land['location_of_registration'] : '';
$_title = isset($_land['title']) && $_land['title'] ? $_land['title'] : '';
$_title_details = isset($_land['title_details']) && $_land['title_details'] ? $_land['title_details'] : '';
$_encumbrance = isset($_land['encumbrance']) && $_land['encumbrance'] ? $_land['encumbrance'] : '';
$_lot_number = isset($_land['lot_number']) && $_land['lot_number'] ? $_land['lot_number'] : '';
$_ownership_classification_id = isset($_land['ownership_classification_id']) && $_land['ownership_classification_id'] ? $_land['ownership_classification_id'] : '';
$_estimated_price = isset($_land['estimated_price']) && $_land['estimated_price'] ? $_land['estimated_price'] : '';
$_negotiated_price = isset($_land['negotiated_price']) && $_land['negotiated_price'] ? $_land['negotiated_price'] : '';
$_final_price = isset($_land['final_price']) && $_land['final_price'] ? $_land['final_price'] : '';
$_ep_code = isset($_land['ep_code']) && $_land['ep_code'] ? $_land['ep_code'] : '';
$_assessed_value = isset($_land['assessed_value']) && $_land['assessed_value'] ? $_land['assessed_value'] : '';
$_mother_lot_tax_declaration = isset($_land['mother_lot_tax_declaration']) && $_land['mother_lot_tax_declaration'] ? $_land['mother_lot_tax_declaration'] : '';
$_tax_declaration = isset($_land['tax_declaration']) && $_land['tax_declaration'] ? $_land['tax_declaration'] : '';
$_company_id = isset($_land['company_id']) && $_land['company_id'] ? $_land['company_id'] : '';
$_remarks = isset($_land['remarks']) && $_land['remarks'] ? $_land['remarks'] : '';
$_market_value = isset($_land['market_value']) && $_land['market_value'] ? $_land['market_value'] : '';
$_date_of_market_value_assessment = isset($_land['date_of_market_value_assessment']) && $_land['date_of_market_value_assessment'] && (strtotime($_land['date_of_market_value_assessment']) > 0) ? date_format(date_create($_land['date_of_market_value_assessment']), 'F j, Y') : '';
$_appraised_value = isset($_land['appraised_value']) && $_land['appraised_value'] ? $_land['appraised_value'] : '';
$_date_of_appraisal = isset($_land['date_of_appraisal']) && $_land['date_of_appraisal'] && (strtotime($_land['date_of_appraisal']) > 0) ? date_format(date_create($_land['date_of_appraisal']), 'F j, Y') : '';
?>

<script type="text/javascript">
    window.land_inventory_id = "<?php echo $_id ?>";
</script>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Land Inventory</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('land/update/' . $_id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('land'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#general_information" role="tab" aria-selected="true">
                            <i class="flaticon2-information"></i> General Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#land_information" role="tab" aria-selected="false">
                            <i class="flaticon-price-tag"></i> Land Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payment_request_information" role="tab" aria-selected="false">
                            <i class="fa fa-hand-holding-usd"></i> Payment Request Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payment_voucher_information" role="tab" aria-selected="false">
                            <i class="flaticon2-list"></i> Payment Voucher Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#accounting_entries" role="tab" aria-selected="false">
                            <i class="flaticon2-list"></i> Accounting Entries
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div class="tab-content  kt-margin-t-20">
                <!-- General Information -->
                <div class="tab-pane active" id="general_information" role="tabpanel-1">
                    <?php $this->load->view('view/general_information'); ?>
                </div>

                <!-- Land Information -->
                <div class="tab-pane" id="land_information" role="tabpanel-2">
                    <?php $this->load->view('view/land_information'); ?>
                </div>

                <!-- Payment Request Information -->
                <div class="tab-pane" id="payment_request_information" role="tabpanel-3">
                    <?php $this->load->view('view/payment_request_information'); ?>
                </div>

                <!-- Payment Voucher Information -->
                <div class="tab-pane" id="payment_voucher_information" role="tabpanel-4">
                    <?php $this->load->view('view/payment_voucher_information'); ?>
                </div>

                <!-- Accounting Entries Information -->
                <div class="tab-pane" id="accounting_entries" role="tabpanel-5">
                    <?php $this->load->view('view/accounting_entries_information'); ?>
                </div>
            </div>
        </div>
    </div>
</div>