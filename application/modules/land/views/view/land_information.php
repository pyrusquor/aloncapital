<?php

# LAND INFORMATION
$_land_classification_id = isset($_land['land_classification_id']) && $_land['land_classification_id'] ? $_land['land_classification_id'] : '';
$_location_of_registration = isset($_land['location_of_registration']) && $_land['location_of_registration'] ? $_land['location_of_registration'] : '';
$_title = isset($_land['title']) && $_land['title'] ? $_land['title'] : '';
$_title_details = isset($_land['title_details']) && $_land['title_details'] ? $_land['title_details'] : '';
$_encumbrance = isset($_land['encumbrance']) && $_land['encumbrance'] ? $_land['encumbrance'] : '';
$_lot_number = isset($_land['lot_number']) && $_land['lot_number'] ? $_land['lot_number'] : '';
$_ownership_classification_id = isset($_land['ownership_classification_id']) && $_land['ownership_classification_id'] ? $_land['ownership_classification_id'] : '';
$_estimated_price = isset($_land['estimated_price']) && $_land['estimated_price'] ? $_land['estimated_price'] : '';
$_negotiated_price = isset($_land['negotiated_price']) && $_land['negotiated_price'] ? $_land['negotiated_price'] : '';
$_final_price = isset($_land['final_price']) && $_land['final_price'] ? $_land['final_price'] : '';
$_ep_code = isset($_land['ep_code']) && $_land['ep_code'] ? $_land['ep_code'] : '';
$_assessed_value = isset($_land['assessed_value']) && $_land['assessed_value'] ? $_land['assessed_value'] : '';
$_mother_lot_tax_declaration = isset($_land['mother_lot_tax_declaration']) && $_land['mother_lot_tax_declaration'] ? $_land['mother_lot_tax_declaration'] : '';
$_tax_declaration = isset($_land['tax_declaration']) && $_land['tax_declaration'] ? $_land['tax_declaration'] : '';
$_company_id = isset($_land['company_id']) && $_land['company_id'] ? $_land['company_id'] : '';
$_remarks = isset($_land['remarks']) && $_land['remarks'] ? $_land['remarks'] : '';
$_market_value = isset($_land['market_value']) && $_land['market_value'] ? $_land['market_value'] : '';
$_date_of_market_value_assessment = isset($_land['date_of_market_value_assessment']) && $_land['date_of_market_value_assessment'] && (strtotime($_land['date_of_market_value_assessment']) > 0) ? date_format(date_create($_land['date_of_market_value_assessment']), 'F j, Y') : '';
$_appraised_value = isset($_land['appraised_value']) && $_land['appraised_value'] ? $_land['appraised_value'] : '';
$_date_of_appraisal = isset($_land['date_of_appraisal']) && $_land['date_of_appraisal'] && (strtotime($_land['date_of_appraisal']) > 0) ? date_format(date_create($_land['date_of_appraisal']), 'F j, Y') : '';

?>
<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Land Classification
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo Dropdown::get_static('land_classification', @$_land_classification_id, 'view'); ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Location of Registration
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_location_of_registration) && $_location_of_registration ? $_location_of_registration : 'Location of Registration'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Title
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_title) && $_title ? $_title : 'Title'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Title Details
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_title_details) && $_title_details ? $_title_details : 'Title Details'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Encumbrance
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_encumbrance) && $_encumbrance ? $_encumbrance : 'Encumbrance'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Lot Number
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_lot_number) && $_lot_number ? $_lot_number : 'Lot Number'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Ownership Classification
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo Dropdown::get_static('classification', @$_ownership_classification_id, 'view'); ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Estimated Price
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_estimated_price) && $_estimated_price ? $_estimated_price : 'Estimated Price'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Negotiated Price
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_negotiated_price) && $_negotiated_price ? $_negotiated_price : 'Negotiated Price'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Final Price
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_final_price) && $_final_price ? $_final_price : 'Final Price'; ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            EP Code
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_ep_code) && $_ep_code ? $_ep_code : 'EP Code'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Assessed Value
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_assessed_value) && $_assessed_value ? $_assessed_value : 'Assessed Value'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Mother Lot Tax Declaration
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_mother_lot_tax_declaration) && $_mother_lot_tax_declaration ? $_mother_lot_tax_declaration : 'Mother Lot Tax Declaration'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Tax Declaration
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_tax_declaration) && $_tax_declaration ? $_tax_declaration : 'Tax Declaration'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Company
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo Dropdown::get_static('companies', @$_company_id, 'view'); ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Remarks
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_remarks) && $_remarks ? $_remarks : 'Remarks'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Market Value
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_market_value) && $_market_value ? $_market_value : 'Market Value'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Date of Market Value Assessment
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_date_of_market_value_assessment) && $_date_of_market_value_assessment ? $_date_of_market_value_assessment : 'Date of Market Value Assessment'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Appraised Value
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_appraised_value) && $_appraised_value ? $_appraised_value : 'Appraised Value'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Date of Appraisal
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_date_of_appraisal) && $_date_of_appraisal ? $_date_of_appraisal : 'Date of Appraisal'; ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>