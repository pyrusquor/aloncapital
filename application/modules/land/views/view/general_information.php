<?php

# GENERAL INFORMATION
$_land_owner_name = isset($_land['land_owner_name']) && $_land['land_owner_name'] ? $_land['land_owner_name'] : '';
$_location = isset($_land['location']) && $_land['location'] ? $_land['location'] : '';
$_owner_contact_info = isset($_land['owner_contact_info']) && $_land['owner_contact_info'] ? $_land['owner_contact_info'] : '';
$_date_offered = isset($_land['date_offered']) && $_land['date_offered'] && (strtotime($_land['date_offered']) > 0) ? date_format(date_create($_land['date_offered']), 'F j, Y') : '';
$_decision_date = isset($_land['decision_date']) && $_land['decision_date'] && (strtotime($_land['decision_date']) > 0) ? date_format(date_create($_land['decision_date']), 'F j, Y') : '';
$_date_transfer_of_title = isset($_land['date_transfer_of_title']) && $_land['date_transfer_of_title'] ? $_land['date_transfer_of_title'] : '';
$_status = isset($_land['status']) && $_land['status'] ? $_land['status'] : '';
$_created_at = isset($_land['created_at']) && $_land['created_at'] && (strtotime($_land['created_at']) > 0) ? date_format(date_create($_land['created_at']), 'F j, Y') : '';
$_updated_at = isset($_land['updated_at']) && $_land['updated_at'] && (strtotime($_land['updated_at']) > 0) ? date_format(date_create($_land['updated_at']), 'F j, Y') : '';

?>
<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Land Owner Name
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_land_owner_name) && $_land_owner_name ? $_land_owner_name : 'Land Owner Name'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Location
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_location) && $_location ? $_location : 'Location'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Owner Contact Info
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_owner_contact_info) && $_owner_contact_info ? $_owner_contact_info : 'Owner Contact Info'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Date Offered
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_date_offered) && $_date_offered ? $_date_offered : 'Date Offered'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Decision Date
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_decision_date) && $_decision_date ? $_decision_date : 'Decision Date'; ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Date Transfer of Title
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_date_transfer_of_title) && $_date_transfer_of_title ? $_date_transfer_of_title : 'Date Transfer of Title '; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Status
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo Dropdown::get_static('land_inventory_status', @$_status, 'view'); ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Created By
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_created_at) && $_created_at ? $_created_at : 'Created By'; ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Updated by
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?php echo isset($_updated_at) && $_updated_at ? $_updated_at : 'Updated by'; ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>