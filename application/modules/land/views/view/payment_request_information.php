<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentrequest_table">
                <thead>
                    <tr>
                        <th width="1%">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                <span></span>
                            </label>
                        </th>
                        <th>ID</th>
                        <th>Reference</th>
                        <th>Payee</th>
                        <th>Payable Type</th>
                        <th>Property</th>
                        <th>Due Date</th>
                        <th>Gross Amount</th>
                        <th>Tax Rate (VAT - WHT)</th>
                        <th>Paid Amount</th>
                        <th>Remaining Amount</th>
                        <th>Payment Status</th>
                        <th>Status</th>
                        <th>Particulars</th>
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>