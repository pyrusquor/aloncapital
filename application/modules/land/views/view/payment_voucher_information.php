<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentvoucher_table">
                <thead>
                    <tr>
                        <th width="1%">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                <span></span>
                            </label>
                        </th>
                        <th>ID</th>
                        <th>Reference</th>
                        <th>Payment Type</th>
                        <th>Payee</th>
                        <th>Payee Type</th>
                        <th>Paid Date</th>
                        <th>Paid Amount</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>