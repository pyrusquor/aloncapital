<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="accounting_entries_table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Company</th>
                        <th>Project</th>
                        <th>Payee</th>
                        <th>Payee Name</th>
                        <th>OR Number</th>
                        <th>Invoice Number</th>
                        <th>Journal Type</th>
                        <th>Payment Date</th>
                        <th>Total</th>
                        <th>Remarks</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>