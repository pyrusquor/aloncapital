<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Land Inventory Dashboard</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
                <input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
                <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('land'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon kt-hidden">
                <i class="la la-gear"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Land Inventory Status
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div id="dashboard_chart" style="height: 800px;"></div>
        <div id="dashboard_pie_chart"></div>
    </div>
</div>