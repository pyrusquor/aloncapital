<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Land Inventory</h3>
			<?php if ( isset($_total) && $_total ): ?>

				<span class="kt-subheader__separator kt-subheader__separator--v"></span>
				<span class="kt-subheader__desc" id="_total"><?php echo $_total;?> TOTAL</span>
			<?php endif; ?>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
				<input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
				<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('land/create');?>" class="btn btn-label-primary btn-elevate btn-icon-sm">
					<i class="fa fa-plus-circle"></i> Land Inventory
				</a>
				<button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
					<i class="fa fa-upload"></i> Import
				</button>
				<button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
					<i class="fa fa-filter"></i> Filter
				</button>
			</div>
		</div>
	</div>
</div>


<div class="kt-portlet kt-portlet--mobile">

	<div class="kt-portlet__body">
		<div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
			<div class="row">
				<div class="col-lg-12">
					<form class="kt-form">
						<div class="form-group row">
							<div class="col-sm-3">
								<label class="form-control-label">Name</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="name" class="form-control form-control-sm" placeholder="Name" value="">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3">
								<label class="form-control-label">Description</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="description" class="form-control form-control-sm" placeholder="Description" value="">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
								</div>
							</div>
							<div class="col-sm-4">
								<label class="form-control-label">Date</label>
								<div class="input-daterange input-group" id="kt_datepicker">
									<input type="text" class="form-control form-control-sm kt-input" name="start" placeholder="From" data-col-index="5" />
									<div class="input-group-append">
										<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
									</div>
									<input type="text" class="form-control form-control-sm kt-input" name="end" placeholder="To" data-col-index="5" />
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
			<form class="kt-form">
				<div class="form-group row">
					<div class="col-lg-3">
						<label class="form-control-label">Land Inventory Type</label>
						<div class="kt-input-icon  kt-input-icon--left">
							<select class="form-control form-control-sm" name="status">
								<option value=""> -- Update Existing Data -- </option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-lg-3">
						<label class="form-control-label">Upload CSV file:</label>
						<label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
						<input type="file" name="" class="">
					</div>
				</div>
				<div class="form-group form-group-last row">
					<div class="col-lg-3">
						<div class="row">
							<div class="col-lg-6">
								<button type="button" class="btn btn-brand btn-success btn-elevate btn-sm">
									<i class="fa fa-upload"></i> Upload
								</button>
							</div>
							<div class="col-lg-6 kt-align-right">
								<button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#_export_option">
									<i class="fa fa-file-csv"></i>
								</button>
								<button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
									<i class="fa fa-info-circle"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_land_inventory_table">
			<thead>
				<tr class="text-center">
					<th width="1%">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<input type="checkbox" value="" class="m-checkable">
							<span></span>
						</label>
					</th>
					<th>ID</th>
					<th>Land Owner</th>
					<th>Title</th>
					<th>Lot Number</th>
					<th>Location</th>
					<th>Land Area</th>
					<th>Land Classification</th>
					<th>Status</th>
					<th>Last Update</th>
					<th width="1%">Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php if ( isset($_land_inventories) && $_land_inventories ): ?>

					<?php foreach ( $_land_inventories as $_lkey => $_li ): ?>

						<?php
							$_id											= isset($_li->id) && $_li->id ? $_li->id : '';
							$_owner_name							= isset($_li->land_owner_name) && $_li->land_owner_name ? $_li->land_owner_name : '';
							$_title										= isset($_li->title) && $_li->title ? $_li->title : '';
							$_lot_number							= isset($_li->lot_number) && $_li->lot_number ? $_li->lot_number : '';
							$_location								= isset($_li->location) && $_li->location ? $_li->location : '';
							$_land_area								= isset($_li->land_area) && $_li->land_area ? $_li->land_area : '';
							$_land_classification_id	= isset($_li->land_classification_id) && $_li->land_classification_id ? $_li->land_classification_id : '';
							$_status									= isset($_li->status) && $_li->status ? $_li->status : '';
							$_updated_by							= isset($_li->updated_by) && $_li->updated_by ? $_li->updated_by : '';
							$_updated_at							= isset($_li->updated_at) && $_li->updated_at ? $_li->updated_at : '';
						?>

						<tr>
							<td class="text-center">
								<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
									<input type="checkbox" value="" class="m-checkable">
									<span></span>
								</label>
							</td>
							<td class="text-center"><?php echo @$_id; ?></td>
							<td><?php echo @$_owner_name; ?></td>
							<td><?php echo @$_title; ?></td>
							<td><?php echo @$_lot_number; ?></td>
							<td class="text-center"><?php echo @$_location; ?></td>
							<td class="text-center"><?php echo @$_land_area; ?></td>
							<td class="text-center"><?php echo @$_land_classification_id; ?></td>
							<td class="text-center"><?php echo @$_status; ?></td>
							<td class="text-center"><?php echo @$_updated_at; ?></td>
							<td class="text-center">
								<span class="dropdown">
									<a href="#" class="btn btn-xs btn-clean btn-icon btn-icon-sm" data-toggle="dropdown" aria-expanded="true">
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<a href="<?php echo site_url('land/update/'.$_id);?>" class="dropdown-item"><i class="la la-edit"></i> Update</a>
										<button type="button" class="dropdown-item _consolidate_docx" data-toggle="modal" data-target="#_consolidate_modal"><i class="la la-edit"></i> Consolidate</button>
										<button type="button" class="dropdown-item _subdivide_docx" data-toggle="modal" data-target="#_subdivide_modal"><i class="la la-code"></i> Subdivide</button>
										<button type="button" class="dropdown-item _delete_docx" data-id="<?php echo @$_id;?>"><i class="la la-trash"></i> Delete</button>
									</div>
								</span>
								<a href="<?php echo site_url('land/view/'.$_id);?>" class="btn btn-xs btn-clean btn-icon btn-icon-sm" title="View">
									<i class="la la-eye"></i>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>

					<tr>
						<td class="text-center" colspan="10">
							No data available in table
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<!--end: Datatable -->
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_export_option_label">Export Options</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-4 offset-lg-1">
						<div class="kt-checkbox-list">
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Field
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Name
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Description
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Owner
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Classification
								<span></span>
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm">
					<i class="fa fa-file-export"></i> Export
				</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="upload_guide_label">Document</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<h2>UPLOAD GUIDE</h2>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->