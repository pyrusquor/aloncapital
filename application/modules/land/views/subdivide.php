<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Subdivide
					</h3>
				</div>
			</div>

			<form class="kt-form" id="kt_form_subdivide">
				<div class="kt-portlet__body">
					<div class="form-group form-group-last kt-hide">
						<div class="alert alert-danger" role="alert" id="form_msg">
							<div class="alert-icon"><i class="flaticon-warning"></i></div>
							<div class="alert-text">
								Oh snap! Change a few things up and try submitting again.
							</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					</div>
					<div class="kt-section kt-section--first">
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="form-control-label">Land Owner Name <span class="kt-font-danger">*</span></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="owner_name" class="form-control" placeholder="Land Owner Name" value="">
									<!-- <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span> -->
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
									
								</div><span class="form-text text-muted">Please enter only digits</span>
							</div>
							<div class="col-lg-6">
								<label class="form-control-label">Land Classification <span class="kt-font-danger">*</span></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select class="form-control" name="classification">
										<option value=""> Land Classification </option>
										<option value="1">Classification 1</option>
										<option value="2">Classification 2</option>
										<option value="3">Classification 3</option>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="form-control-label">Location (Full Address) <span class="kt-font-danger">*</span></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="location" class="form-control" placeholder="Full Address" value="">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
								</div>
							</div>
							<div class="col-lg-6">
								<label class="form-control-label">Company</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select class="form-control" name="company">
										<option value=""> Company </option>
										<option value="1">Company 1</option>
										<option value="2">Company 2</option>
										<option value="3">Company 3</option>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="form-control-label">Land Area <span class="kt-font-danger">*</span></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="area" class="form-control" placeholder="Land Area" value="">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-compass"></i></span></span>
								</div>
							</div>
							<div class="col-lg-6">
								<label class="form-control-label">Status <span class="kt-font-danger">*</span></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select class="form-control" name="status">
										<option value=""> Status </option>
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-line-chart"></i></span></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="form-control-label">Lot Number Prefix</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="lot_number" class="form-control" placeholder="Lot Number Prefix" value="">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-tag"></i></span></span>
								</div>
							</div>
							<div class="col-lg-6">
								<label class="form-control-label">Number of Lots to Subdivide</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="subdivide_number" class="form-control" placeholder="Number of Lots to Subdivide" value="">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-tags"></i></span></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="form-control-label">Project</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="project" class="form-control" placeholder="Project" value="">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square-o"></i></span></span>
								</div>
							</div>
							<div class="col-lg-6">
								<label class="form-control-label">Remarks</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="remarks" class="form-control" placeholder="Remarks" value="">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-bookmark-o"></i></span></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="reset" class="btn btn-primary">Submit</button>
						<button type="reset" class="btn btn-secondary">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>