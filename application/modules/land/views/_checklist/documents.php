<style type="text/css">
	th, td {
		vertical-align: middle !important;
	}
</style>

<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_document_checklist">
	<thead>
		<tr class="text-center">
			<th width="1%">
				<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
					<input type="checkbox" value="all" class="m-checkable" id="select-all">
					<span></span>
				</label>
			</th>
			<th>ID</th>
			<th>Name</th>
			<th>Description</th>
			<th>Category</th>
			<th width="12%">Start Date</th>
			<th>Owner</th>
			<th>Group</th>
			<th>Dependency</th>
		</tr>
	</thead>
	<tbody>
		<?php if ( isset($_documents) && $_documents ): ?>

			<?php foreach ( $_documents as $_dkey => $_docx ): ?>

				<?php
					$_description	=	isset($_docx->description) && $_docx->description ? $_docx->description : '';
					$_name				=	isset($_docx->name) && $_docx->name ? $_docx->name : '';
					$_id					=	isset($_docx->id) && $_docx->id ? $_docx->id : '';
				?>

				<tr>
					<td width="1%">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<!-- <input type="checkbox" name="id[]" value="<?php echo @$_id;?>" class="m-checkable"> -->
							<input type="checkbox" name="<?php echo @$_id;?>[document_id]" value="<?php echo @$_id;?>" class="m-checkable">
							<span></span>
						</label>
					</td>
					<td class="text-center" width="1%">
						<?php echo @$_id;?>
					</td>
					<td>
						<?php echo @$_name;?>
					</td>
					<td>
						<?php echo @$_description;?>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[category_id]', Dropdown::get_static('document_checklist_category'), '', 'class="form-control form-control-sm"'); ?>
					</td>
					<td class="text-center">
						<input type="text" class="form-control form-control-sm _start_date" name="<?php echo @$_id;?>[start_date]" placeholder="mm/dd/yyyy" readonly>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[owner_id]', Dropdown::get_static('document_owner'), '', 'class="form-control form-control-sm"'); ?>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[group_id]', Dropdown::get_static('group'), '', 'class="form-control form-control-sm"'); ?>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[dependency_id]', Dropdown::get_static('group'), '', 'class="form-control form-control-sm"'); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php else: ?>

			<!-- <tr>
				<td class="text-center" colspan="9">
					No record Found
				</td>
			</tr> -->
		<?php endif; ?>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready( function () {

		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": true,
		  "progressBar": false,
		  "positionClass": "toast-top-right",
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "0",
		  "extendedTimeOut": "0",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		// $('#_document_checklist').DataTable({
		// 	order: [[ 1, 'desc']],
		// 	pagingType: 'full_numbers',
		// 	lengthMenu: [5, 10, 25, 50, 100],
		// 	pageLength : 5,
		// 	responsive: true,
		// 	searchDelay: 500,
		// 	processing: true,
		// 	serverSide: false,
		// 	deferRender: true,
		// 	dom:	
		// 				// "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'btn-block'B>>>" +
		// 				"<'row'<'col-sm-12 col-md-6'l>>" +
		// 				"<'row'<'col-sm-12'tr>>" +
		// 				"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
		// 	language: {
		// 		'lengthMenu': 'Show _MENU_',
  //       'infoFiltered': '(filtered from _MAX_ total records)'
		// 	},
		// 	columnDefs: [
		// 		{
		// 			targets: [0, 1, 4, 5, 6, 7, 8],
		// 			className: 'dt-center',
		// 		},
		// 		{
		// 			targets: [0],
		// 			orderable: false,
		// 		}
		// 	]
		// });

		$('._start_date').datepicker({
    	orientation: "bottom left",
    	todayBtn: "linked",
    	clearBtn: true,
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

		// $(document).on( 'click', '#_document_checklist_form', function (e) { console.log('qqqqqq');

		// 	var _table	=	$('#_document_checklist').DataTable();

		// 	e.preventDefault();

		// 	var docx_ids	=	[];
		// 	var category_id	=	[];
		// 	var inputs		=	$('form#_document_checklist_form').serializeArray();
		// 	var inputs2 = _table.$('input, select').serializeArray(); console.log(inputs2);

		// 	$.each($('input[name="id[]"]:checked'), function () {

		// 		docx_ids.push($(this).val());
		// 	});

		// 	$.each($('select[name="category_id[]"]'), function () {

		// 		category_id.push($(this).val());
		// 	}); console.log(category_id);

		// 	$.ajax({
		// 		type: 'post',
		// 		dataType: 'json',
		// 		url: $(this).attr('action'),
		// 		data: {
		// 			ids: docx_ids,
		// 			input: inputs
		// 		},
		// 		beforeSend: function () {

		// 			// KTApp.blockPage({
  //    //        overlayColor: '#000000',
  //    //        type: 'v2',
  //    //        state: 'primary',
  //    //        message: 'Processing...'
	 //    //     });
		// 		},
		// 		success: function ( _respo ) {

		// 			$('button._consolidate').addClass('disabled', true);

		// 			if ( _respo._status === 1 ) {

		// 				toastr.success(_respo._msg, "Success");

		// 				$('form#_consolidate_form').find('input, select').addClass('is-valid');
		// 			} else {

		// 				toastr.error(_respo._msg, "Failed");

		// 				$('form#_consolidate_form').find('input, select').addClass('is-invalid');
		// 			}

		// 			KTApp.unblockPage();
		// 		}
		// 	}).fail ( function () {

		// 		$('button._consolidate').addClass('disabled', true);

		// 		KTApp.unblockPage();

		// 		swal.fire(
		// 			'Oops!',
		// 			'Please refresh the page and try again.',
		// 			'error'
		// 		);
		// 	});
		// });

		// $(document).on( 'submit', 'form#_document_checklist_form', function (e) { console.log('qqqqqq');

		// 	var _table	=	$('#_document_checklist').DataTable();

		// 	e.preventDefault();

		// 	var docx_ids	=	[];
		// 	var category_id	=	[];
		// 	var inputs		=	$('form#_document_checklist_form').serializeArray();
		// 	var inputs2 = _table.$('input, select').serializeArray(); console.log(inputs2);

		// 	$.each($('input[name="id[]"]:checked'), function () {

		// 		docx_ids.push($(this).val());
		// 	});

		// 	$.each($('select[name="category_id[]"]'), function () {

		// 		category_id.push($(this).val());
		// 	}); console.log(category_id);

		// 	$.ajax({
		// 		type: 'post',
		// 		dataType: 'json',
		// 		url: $(this).attr('action'),
		// 		data: {
		// 			ids: docx_ids,
		// 			input: inputs
		// 		},
		// 		beforeSend: function () {

		// 			// KTApp.blockPage({
  //    //        overlayColor: '#000000',
  //    //        type: 'v2',
  //    //        state: 'primary',
  //    //        message: 'Processing...'
	 //    //     });
		// 		},
		// 		success: function ( _respo ) {

		// 			$('button._consolidate').addClass('disabled', true);

		// 			if ( _respo._status === 1 ) {

		// 				toastr.success(_respo._msg, "Success");

		// 				$('form#_consolidate_form').find('input, select').addClass('is-valid');
		// 			} else {

		// 				toastr.error(_respo._msg, "Failed");

		// 				$('form#_consolidate_form').find('input, select').addClass('is-invalid');
		// 			}

		// 			KTApp.unblockPage();
		// 		}
		// 	}).fail ( function () {

		// 		$('button._consolidate').addClass('disabled', true);

		// 		KTApp.unblockPage();

		// 		swal.fire(
		// 			'Oops!',
		// 			'Please refresh the page and try again.',
		// 			'error'
		// 		);
		// 	});
		// });
	});
</script>