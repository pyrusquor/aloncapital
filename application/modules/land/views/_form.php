<?php
	$_land_owner_name							=	isset($_land['land_owner_name']) && $_land['land_owner_name'] ? $_land['land_owner_name'] : '';
	$_location										=	isset($_land['location']) && $_land['location'] ? $_land['location'] : '';
	$_owner_contact_info					=	isset($_land['owner_contact_info']) && $_land['owner_contact_info'] ? $_land['owner_contact_info'] : '';
	$_land_area										=	isset($_land['land_area']) && $_land['land_area'] ? $_land['land_area'] : '';
	$_estimated_price							=	isset($_land['estimated_price']) && $_land['estimated_price'] ? $_land['estimated_price'] : '';
	$_negotiated_price						=	isset($_land['negotiated_price']) && $_land['negotiated_price'] ? $_land['negotiated_price'] : '';
	$_final_price									=	isset($_land['final_price']) && $_land['final_price'] ? $_land['final_price'] : '';
	$_date_offered								=	isset($_land['date_offered']) && $_land['date_offered'] && (strtotime($_land['date_offered']) > 0) ? date_format(date_create($_land['date_offered']), 'm/d/Y') : '';
	$_decision_date								=	isset($_land['decision_date']) && $_land['decision_date'] && (strtotime($_land['decision_date']) > 0) ? date_format(date_create($_land['decision_date']), 'm/d/Y') : '';
	$_ep_code											=	isset($_land['ep_code']) && $_land['ep_code'] ? $_land['ep_code'] : '';
	$_assessed_value							=	isset($_land['assessed_value']) && $_land['assessed_value'] ? $_land['assessed_value'] : '';
	$_mother_lot_tax_declaration	=	isset($_land['mother_lot_tax_declaration']) && $_land['mother_lot_tax_declaration'] ? $_land['mother_lot_tax_declaration'] : '';
	$_land_classification_id			=	isset($_land['land_classification_id']) && $_land['land_classification_id'] ? $_land['land_classification_id'] : '';
	$_location_of_registration		=	isset($_land['location_of_registration']) && $_land['location_of_registration'] ? $_land['location_of_registration'] : '';
	$_title												=	isset($_land['title']) && $_land['title'] ? $_land['title'] : '';
	$_title_details								=	isset($_land['title_details']) && $_land['title_details'] ? $_land['title_details'] : '';
	$_lot_number									=	isset($_land['lot_number']) && $_land['lot_number'] ? $_land['lot_number'] : '';
	$_encumbrance									=	isset($_land['encumbrance']) && $_land['encumbrance'] ? $_land['encumbrance'] : '';
	$_date_transfer_of_title			=	isset($_land['date_transfer_of_title']) && $_land['date_transfer_of_title'] && (strtotime($_land['date_transfer_of_title']) > 0) ? date_format(date_create($_land['date_transfer_of_title']), 'm/d/Y') : '';
	$_ownership_classification_id	=	isset($_land['ownership_classification_id']) && $_land['ownership_classification_id'] ? $_land['ownership_classification_id'] : '';
	$_status											=	isset($_land['status']) && $_land['status'] ? $_land['status'] : '';
	$_tax_declaration							=	isset($_land['tax_declaration']) && $_land['tax_declaration'] ? $_land['tax_declaration'] : '';
	$_company_id									=	isset($_land['company_id']) && $_land['company_id'] ? $_land['company_id'] : '';
	$_remarks											=	isset($_land['remarks']) && $_land['remarks'] ? $_land['remarks'] : '';
	

	$company_name = isset($_land['company']['name']) && $_land['company']['name'] ? $_land['company']['name'] : 'N/A';
	
	if($company_name == "N/A") {
		$company_name = isset($_land['company']['name']) && $_land['company']['name'] ? $_land['company']['name'] : 'N/A';
	}
?>

<div class="kt-section kt-section--first">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Land Owner Name <code>*</code></label>
				<div class="kt-input-icon">
					<input type="text" name="land_owner_name" class="form-control" id="land_owner_name" placeholder="Land Owner Name" value="<?php echo set_value('land_owner_name', @$_land_owner_name);?>" autocomplete="off">
				</div>
				<?php echo form_error('land_owner_name'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Land Classification <code>*</code></label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('land_classification_id', Dropdown::get_static('land_classification'), set_value('land_classification_id', @$_land_classification_id), 'class="form-control" id="land_classification_id"'); ?>
				</div>
				<?php echo form_error('land_classification_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Location <code>(Full Address) *</code></label>
				<div class="kt-input-icon">
					<input type="text" name="location" class="form-control" id="location" placeholder="Full Address" value="<?php echo set_value('location', @$_location);?>" autocomplete="off">
				</div>
				<?php echo form_error('location'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Location of Registration</label>
				<div class="kt-input-icon">
					<input type="text" name="location_of_registration" class="form-control" id="location_of_registration" placeholder="Location of Registration" value="<?php echo set_value('location_of_registration', @$_location_of_registration);?>" autocomplete="off">
				</div>
				<?php echo form_error('location_of_registration'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Owner Contact Info <code>(Mobile)</code></label>
				<div class="kt-input-icon">
					<input type="text" name="owner_contact_info" class="form-control" id="owner_contact_info" placeholder="Owner Contact Info ( Mobile )" value="<?php echo set_value('owner_contact_info', @$_owner_contact_info);?>" autocomplete="off">
				</div>
				<?php echo form_error('owner_contact_info'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Title</label>
				<div class="kt-input-icon">
					<input type="text" name="title" class="form-control" id="title" placeholder="Title" value="<?php echo set_value('title', @$_title);?>" autocomplete="off">
				</div>
				<?php echo form_error('title'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Land Area <code>*</code></label>
				<div class="kt-input-icon">
					<div class="input-group">
						<input type="text" name="land_area" class="form-control" placeholder="Land Area" value="<?php echo set_value('land_area', @$_land_area);?>" autocomplete="off">
						<div class="input-group-append"><span class="input-group-text">SQM</span></div>
					</div>
				</div>
				<?php echo form_error('land_area'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Title Details</label>
				<div class="kt-input-icon">
					<input type="text" name="title_details" class="form-control" placeholder="Title Details" value="<?php echo set_value('title_details', @$_title_details);?>" autocomplete="off">
				</div>
				<?php echo form_error('title_details'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Estimated Price</label>
				<div class="kt-input-icon">
					<input type="text" name="estimated_price" class="form-control" placeholder="Estimated Price" value="<?php echo set_value('estimated_price', @$_estimated_price);?>" autocomplete="off">
				</div>
				<?php echo form_error('estimated_price'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Lot Number</label>
				<div class="kt-input-icon">
					<input type="text" name="lot_number" class="form-control" placeholder="Lot Number" value="<?php echo set_value('lot_number', @$_lot_number);?>" autocomplete="off">
				</div>
				<?php echo form_error('lot_number'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Negotiated Price</label>
				<div class="kt-input-icon">
					<input type="text" name="negotiated_price" class="form-control" placeholder="Negotiated Price" value="<?php echo set_value('negotiated_price', @$_negotiated_price);?>" autocomplete="off">
				</div>
				<?php echo form_error('negotiated_price'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Encumbrance</label>
				<div class="kt-input-icon">
					<input type="text" name="encumbrance" class="form-control" placeholder="Encumbrance" value="<?php echo set_value('encumbrance', @$_encumbrance);?>" autocomplete="off">
				</div>
				<?php echo form_error('encumbrance'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Final Price</label>
				<div class="kt-input-icon">
					<input type="text" name="final_price" class="form-control" placeholder="Final Price" value="<?php echo set_value('final_price', @$_final_price);?>" autocomplete="off">
				</div>
				<?php echo form_error('final_price'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Date Transfer of Title <code>(mm/dd/yyyy)</code></label>
				<div class="kt-input-icon">
					<input type="text" name="date_transfer_of_title" class="form-control kt_datepicker" placeholder="Date Transfer of Title" value="<?php echo set_value('date_transfer_of_title', @$_date_transfer_of_title);?>" autocomplete="off">
				</div>
				<?php echo form_error('date_transfer_of_title'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Date Offered <code>(mm/dd/yyyy)</code></label>
				<div class="kt-input-icon">
					<input type="text" name="date_offered" class="form-control kt_datepicker" placeholder="Date Offered" value="<?php echo set_value('date_offered', @$_date_offered);?>" autocomplete="off">
				</div>
				<?php echo form_error('date_offered'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Ownership Classification <code>*</code></label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('ownership_classification_id', Dropdown::get_static('ownership_classification'), set_value('ownership_classification_id', @$_ownership_classification_id), 'class="form-control" id="ownership_classification_id"'); ?>
				</div>
				<?php echo form_error('ownership_classification_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Decision Date <code>(mm/dd/yyyy)</code></label>
				<div class="kt-input-icon">
					<input type="text" name="decision_date" class="form-control kt_datepicker" placeholder="Decision Date" value="<?php echo set_value('decision_date', @$_decision_date);?>" autocomplete="off">
				</div>
				<?php echo form_error('decision_date'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Status <code>*</code></label>
				<div class="kt-input-icon ">
					<?php echo form_dropdown('status', Dropdown::get_static('land_inventory_status'), set_value('status', @$_status), 'class="form-control" id="status"'); ?>
				</div>
				<?php echo form_error('status'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">EP Code</label>
				<div class="kt-input-icon">
					<input type="text" name="ep_code" class="form-control" placeholder="EP Code" value="<?php echo set_value('ep_code', @$_ep_code);?>" autocomplete="off">
				</div>
				<?php echo form_error('ep_code'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Tax Declaration</label>
				<div class="kt-input-icon">
					<input type="text" name="tax_declaration" class="form-control" placeholder="Tax Declaration" value="<?php echo set_value('tax_declaration', @$_tax_declaration);?>" autocomplete="off">
				</div>
				<?php echo form_error('tax_declaration'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Assessed Value</label>
				<div class="kt-input-icon">
					<input type="text" name="assessed_value" class="form-control" placeholder="Assessed Value" value="<?php echo set_value('assessed_value', @$_assessed_value);?>" autocomplete="off">
				</div>
				<?php echo form_error('assessed_value'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Company</label>
				<div class="kt-input-icon">
					<select class="form-control suggests" data-module="companies"  id="company_id" name="company_id">
                        <option value="">Select Company</option>
                        <?php if ($company_name): ?>
                            <option value="<?php echo @$_company_id; ?>" selected><?php echo $company_name; ?></option>
                        <?php endif ?>
                    </select>
				</div>
				<?php echo form_error('company_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Mother Lot Tax Declaration</label>
				<div class="kt-input-icon">
					<input type="text" name="mother_lot_tax_declaration" class="form-control" placeholder="Mother Lot Tax Declaration" value="<?php echo set_value('mother_lot_tax_declaration', @$_mother_lot_tax_declaration);?>" autocomplete="off">
				</div>
				<?php echo form_error('mother_lot_tax_declaration'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Remarks</label>
				<div class="kt-input-icon">
					<input type="text" name="remarks" class="form-control" placeholder="Remarks" value="<?php echo set_value('remarks', @$_remarks);?>" autocomplete="off">
				</div>
				<?php echo form_error('remarks'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
</div>

<?php if ( FALSE ): ?>
	
	<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
	<h3 class="kt-section__title">Additional Custom Fields</h3>
	<div class="kt-section__body">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label class="form-control-label">First Name <code>*</code></label>
					<div class="kt-input-icon">
						<input type="text" name="first_name" class="form-control" placeholder="First Name" value="<?php echo set_value('name', @$_name);?>" autocomplete="off">
					</div>
					<?php echo form_error('name'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label class="form-control-label">Last Name <code>*</code></label>
					<div class="kt-input-icon">
						<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?php echo set_value('name', @$_name);?>" autocomplete="off">
					</div>
					<?php echo form_error('name'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>