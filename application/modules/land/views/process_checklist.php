<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Land Inventory Documents</h3>
			<?php if ( isset($_total) && $_total ): ?>

				<span class="kt-subheader__separator kt-subheader__separator--v"></span>
				<span class="kt-subheader__desc" id="_total"><?php echo $_total;?> TOTAL</span>
			<?php endif; ?>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
				<input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
				<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<?php if ( FALSE ): ?>

					<a href="javascript:void(0);" class="btn btn-label-primary btn-elevate btn-icon-sm">
					<!-- <a href="<?php echo site_url('checklist/create_land_inventory_document');?>" class="btn btn-label-primary btn-elevate btn-icon-sm"> -->
						<i class="fa fa-plus-circle"></i> Document
					</a>
					<button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
						<i class="fa fa-upload"></i> Import
					</button>
				<?php endif; ?>
				<?php if ( isset($_li_id) && $_li_id ): ?>

					<a href="<?php echo site_url('land/documents/'.$_li_id);?>" class="btn btn-label-primary btn-elevate btn-icon-sm btn-sm">
						<i class="fa fa-plus-circle"></i> Document
					</a>
				<?php endif; ?>
				<button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-icon-sm btn-sm" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
					<i class="fa fa-filter"></i> Filter
				</button>
				<a href="<?php echo site_url('land');?>" class="btn btn-label-primary btn-elevate btn-icon-sm btn-sm">
					<i class="fa fa-reply"></i> Cancel
				</a>
			</div>
		</div>
	</div>
</div>


<div class="kt-portlet kt-portlet--mobile">

	<div class="kt-portlet__body">
		<div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
			<div class="row">
				<div class="col-lg-12">
					<form class="kt-form">
						<div class="form-group row">
							<div class="col-sm-3">
								<label class="form-control-label">Document Name</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" name="land_owner_name" class="form-control form-control-sm _filter" placeholder="Document Name"  id="_column_2" data-column="2">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-book"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3">
								<label class="form-control-label">Owner</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php
										$_document_owner	=	Dropdown::get_static('document_owner');
										$_owners	=	[];
										if ( isset($_document_owner) && $_document_owner ) {

											foreach ( $_document_owner as $_okey => $_do ) {

												$_owners[($_okey === '') ? '' : $_do]	=	$_do;
											}
										}
									?>
									<?php echo form_dropdown('owner_id', $_owners, '', 'class="form-control form-control-sm _filter" id="_column_3" data-column="3"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-certificate"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3">
								<label class="form-control-label">Document Classification</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php
										$_document_classifications	=	Dropdown::get_static('classification');
										$_classifications	=	[];
										if ( isset($_document_classifications) && $_document_classifications ) {

											foreach ( $_document_classifications as $_ckey => $_dc ) {

												$_classifications[($_ckey === '') ? '' : $_dc]	=	$_dc;
											}
										}
									?>
									<?php echo form_dropdown('classification_id', $_classifications, '', 'class="form-control form-control-sm _filter" id="_column_9"  data-column="9"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_land_inventory_document_table">
			<thead>
				<tr class="text-center">
					<th width="1%">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<input type="checkbox" value="all" class="m-checkable" id="select-all">
							<span></span>
						</label>
					</th>
					<th>ID</th>
					<th>Name</th>
					<th>Owner</th>
					<th>File</th>
					<th>Status</th>
					<th>Timeline</th>
					<th>Due Date</th>
					<th>Date Created</th>
					<th>Classification</th>
					<th>Actions</th>
				</tr>
			</thead>
			<?php if ( isset($_documents) && $_documents ): ?>

				<tbody>
					<?php foreach ( $_documents as $key => $_document ): ?>

						<?php
							$_id										=	isset($_document['id']) && $_document['id'] ? $_document['id'] : '';
							$_name									=	isset($_document['name']) && $_document['name'] ? $_document['name'] : '';
							$_owner_id							=	isset($_document['owner_id']) && $_document['owner_id'] ? $_document['owner_id'] : '';
							$_owner									=	isset($_owner_id) && $_owner_id ? Dropdown::get_static('document_owner', $_owner_id, 'view') : '';
							$_status								=	isset($_document['status']) && $_document['status'] ? ucwords(strtolower($_document['status'])) : '';
							$_timeline							=	isset($_document['timeline']) && $_document['timeline'] ? $_document['timeline'] : '';
							$_due_date							=	isset($_document['due_date']) && $_document['due_date'] ? date_format(date_create($_document['due_date']), 'F j, Y') : '';
							$_date_created					=	isset($_document['date_created']) && $_document['date_created'] ? date_format(date_create($_document['date_created']), 'F j, Y') : '';
							$_classification_id			=	isset($_document['classification_id']) && $_document['classification_id'] ? $_document['classification_id'] : '';
							$_classification				=	isset($_classification_id) && $_classification_id ? Dropdown::get_static('classification', $_classification_id, 'view') : '';
							$_uploaded_document_id	=	isset($_document['uploaded_document_id']) && $_document['uploaded_document_id'] ? $_document['uploaded_document_id'] : '';
						?>

						<tr>
							<td>
								<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                  <input type="checkbox" name="id[]" value="<?php echo @$_id;?>" class="m-checkable _select" data-id="<?php echo @$_id;?>">
                  <span></span>
                </label>
							</td>
							<td><?php echo @$_id;?></td>
							<td>
								<?php echo @$_name;?>
							</td>
							<td>
								<?php echo @$_owner;?>
							</td>
							<td>
								<div class="kt-subheader__wrapper">
									<button class="btn btn-linkedin btn-sm btn-elevate btn-icon _upload_file" data-toggle="modal" data-target="#_upload_modal" data-id="<?php echo @$_id;?>">
										<i class="fa fa-pen"></i>
									</button>
									<?php if ( isset($_status) && $_status ): ?>

										<button class="btn btn-linkedin btn-sm btn-elevate btn-icon _view_file" id="_view_file_<?php echo @$_uploaded_document_id;?>" data-toggle="modal" data-target="#_view_file_modal" data-id="<?php echo @$_uploaded_document_id;?>">
											<i class="la la-eye"></i>
										</button>
									<?php endif; ?>
								</div>
							</td>
							<td>
								<?php echo @$_status;?>
							</td>
							<td>
								<?php echo @$_timeline;?>
							</td>
							<td>
								<?php echo @$_due_date;?>
							</td>
							<td>
								<?php echo @$_date_created;?>
							</td>
							<td>
								<?php echo @$_classification;?>
							</td>
							<td>
								<a href="<?php echo site_url('document/view/'.@$_id);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                  <i class="la la-eye"></i>
                </a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			<?php endif; ?>
		</table>
		<!--end: Datatable -->
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_export_option_label">Export Options</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-4 offset-lg-1">
						<div class="kt-checkbox-list">
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Field
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Name
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Description
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Owner
								<span></span>
							</label>
							<label class="kt-checkbox kt-checkbox--bold">
								<input type="checkbox" name=""> Classification
								<span></span>
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm">
					<i class="fa fa-file-export"></i> Export
				</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!-- Start Upload Modal-->
<div class="modal fade" id="_upload_modal" tabindex="-1" role="dialog" aria-labelledby="_upload_modal_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_upload_modal_label">Upload</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" method="POST" id="_upload_form" enctype="multipart/form-data">
					<div class="form-group row text-center">
						<div class="col-lg-12 col-xl-12">
							<div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar">
								<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/img/default/_img.png');?>);"></div>
								<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
									<i class="fa fa-pen"></i>
									<!-- <input type="file" id="process_checklist_file" name="process_image" accept=".png, .jpg, .jpeg"> -->
									<input type="file" class="process_checklist_file" id="process_checklist_file" name="_file" accept=".png, .jpg, .jpeg">
								</label>
								<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
									<i class="fa fa-times"></i>
								</span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_upload_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Upload Modal-->

<!-- Start Upload Modal-->
<div class="modal fade" id="_view_file_modal" tabindex="-1" role="dialog" aria-labelledby="_view_file_modal_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_view_file_modal_label">View</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="_display_process_document_image"></div>
				<!-- <div class="form-group row text-center">
					<div class="col-lg-12 col-xl-12">
						<div class="kt-avatar kt-avatar--outline kt-avatar--circle-">
							<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/img/default/_img.png');?>);"></div>
						</div>
					</div>
				</div> -->
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<!-- <button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_upload_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button> -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Upload Modal-->

<script type="text/javascript">
	var _li_id	=	'<?php echo isset($_li_id) && $_li_id ? $_li_id : '';?>';
</script>