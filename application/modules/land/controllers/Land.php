<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 
 */
class Land extends MY_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->model('land/Land_inventory_model', 'M_land_inventory');
		$this->load->model('payment_request/Payment_request_model', 'M_payment_request');
		$this->load->model('payment_voucher/Payment_voucher_model', 'M_payment_voucher');
		$this->load->model('land/Land_inventory_history_model', 'M_li_history');
		$this->load->model('company/Company_model', 'M_company');

		$this->load->library('ajax_pagination');

		// Per page limit
		$this->perPage = 12;

		$this->_table_fillables	=	$this->M_land_inventory->fillable;
		$this->_table_columns		=	$this->M_land_inventory->__get_columns();
	}

	function index()
	{

		// $this->view_data['_largescreen']	=	TRUE;

		// $_fills	=	$this->_table_fillables;
		// $_colms	=	$this->_table_columns;

		// $this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills); #pd($this->view_data['_fillables']);
		// $this->view_data['_columns']		=	$this->__get_columns($_fills); #ud($this->view_data['_columns']);

		// $_land_inventories =	$this->M_land_inventory->get_all();
		// if ($_land_inventories) {

		// 	$this->view_data['_total']	= count($_land_inventories);
		// }

		// $this->load->model('company/Company_model', 'M_company');

		$this->view_data['companies'] = $this->M_company->get_all();

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	function get_land_inventories()
	{

		$_response	=	[
			'iTotalDisplayRecords'	=>	'',
			'iTotalRecords'					=>	'',
			'sColumns'							=>	'',
			'sEcho'									=>	'',
			'data'									=>	'',
		];

		$_total['_displays']	=	0;
		$_total['_records']		=	0;

		$_columns	=	[
			'id'	=>	TRUE,
			'land_owner_name'	=>	TRUE,
			'title'	=>	TRUE,
			'lot_number'	=>	TRUE,
			'location'	=>	TRUE,
			'land_area'	=>	TRUE,
			'land_classification_id'	=>	TRUE,
			'status'	=>	TRUE,
			'updated_at'	=>	TRUE,
			'company_id'	=>	TRUE,
			'company'	=>	TRUE,

		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

			$_columns	=	[];

			foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

				$_columns[$_def]	=	TRUE;
			}
		}

		$_land_inventories	=	$this->M_land_inventory->with_company()->as_array()->get_all();
		if ($_land_inventories) {

			$_datas	=	[];

			foreach ($_land_inventories as $key => $value) {
				$_land_inventories[$key]['company_id'] = @$value['company']['name'];
				$_land_inventories[$key]['company'] = @$value['company']['id'];
				$_land_inventories[$key]['updated_at'] = view_date($value['updated_at']);

				$_land_inventories[$key]['land_area'] = $value['land_area'] . " sqm";

				$_land_inventories[$key]['land_classification_id'] = Dropdown::get_static('land_classification', $value['land_classification_id'], 'view');

				$red = ['8', '9', '10', '14', '15'];
				$blue = ['12'];
				$clr = "btn btn-primary";
				if (in_array($value['status'], $red)) {
					$clr = "btn btn-danger";
				} elseif (in_array($value['status'], $blue)) {
					$clr = "btn btn-info";
				}

				$_land_inventories[$key]['status'] = "<span class='" . $clr . "'>" . Dropdown::get_static('land_inventory_status', $value['status'], 'view') . "</span>";
			}

			foreach ($_land_inventories as $_likey => $_li) {

				$_datas[]	=	$this->filterArray($_li, $_columns);
			}

			$_total['_displays']	=	$_total['_records']	=	count($_datas);

			if (isset($_REQUEST['search'])) {

				$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

				$_total['_displays']	=	$_datas ? count($_datas) : 0;
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

				foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

					if ($_clm['search']) {

						$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

						$_total['_displays']	=	$_datas ? count($_datas) : 0;
					}
				}
			}

			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

				$_column	=	$_REQUEST['order'][0]['column'] - 1;
				$_dir		=	$_REQUEST['order'][0]['dir'];


				usort($_datas, function ($x, $y) use ($_column, $_dir) {

					// echo "<pre>";
					// print_r($x) ;echo "<br>";
					// echo $_column;echo "<br>";

					$x	=	array_slice($x, $_column, 1);

					// vdebug($x);

					$x	=	array_pop($x);

					$y	=	array_slice($y, $_column, 1);
					$y	=	array_pop($y);


					if ($_dir === 'asc') {

						return $x > $y ? TRUE : FALSE;
					} else {

						return $x < $y ? TRUE : FALSE;
					}
				});
			}

			if (isset($_REQUEST['length'])) {

				$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
			}

			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

				$_temp	=	$_datas;
				$_datas	=	[];

				foreach ($_temp as $key => $_tmp) {

					$_datas[]	=	array_values($_tmp);
				}
			}

			$_secho	=	0;
			if (isset($_REQUEST['sEcho'])) {

				$_secho	=	intval($_REQUEST['sEcho']);
			}

			$_response	=	[
				'iTotalDisplayRecords'	=>	$_total['_displays'],
				'iTotalRecords'					=>	$_total['_records'],
				'sColumns'							=>	'',
				'sEcho'									=>	$_secho,
				'data'									=>	$_datas
			];
		}

		echo json_encode($_response);

		exit();
	}

	public function showItems()
	{
		$columnsDefault = [
			'id' => true,
			'land_owner_name' => true,
			'title' => true,
			'lot_number' => true,
			'location' => true,
			'land_area' => true,
			'land_classification_id' => true,
			'status' => true,
			'company_id' => true,
			'company' => true,
			'created_by' => true,
			'updated_by' => true
		];

		$draw = $_REQUEST['draw'];
		$start = $_REQUEST['start'];
		$rowperpage = $_REQUEST['length'];
		$columnIndex = $_REQUEST['order'][0]['column'];
		$columnName = $_REQUEST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_REQUEST['order'][0]['dir'];
		$searchValue = $_REQUEST['search']['value'] ?? null;

		$filters = [];
		parse_str($_POST['filter'], $filters);

		$items = [];
		$totalRecordwithFilter = 0;
		$filteredItems = [];

		for ($query_loop = 0; $query_loop < 2; $query_loop++) {

			$query = $this->M_land_inventory
				->with_company('fields:name')
				->order_by($columnName, $columnSortOrder)
				->as_array();

			// General Search
			if ($searchValue) {

				$query->or_where("(id like '%$searchValue%'");
				$query->or_where("land_owner_name like '%$searchValue%'");
				$query->or_where("title like '%$searchValue%'");
				$query->or_where("lot_number like '%$searchValue%'");
				$query->or_where("location like '%$searchValue%')");
			}

			$advanceSearchValues = [
				'company_id' => [
					'data' => $filters['company_id'] ?? null,
					'operator' => '=',
				],
				'land_owner_name' => [
					'data' => $filters['land_owner_name'] ?? null,
					'operator' => 'like',
				],
				'title' => [
					'data' => $filters['title'] ?? null,
					'operator' => 'like',
				],
				'lot_number' => [
					'data' => $filters['lot_number'] ?? null,
					'operator' => 'like',
				],
				'location' => [
					'data' => $filters['location'] ?? null,
					'operator' => 'like',
				],
				'land_classification_id' => [
					'data' => $filters['land_classification_id'] ?? null,
					'operator' => '=',
				],
				'status' => [
					'data' => $filters['status'] ?? null,
					'operator' => '=',
				],
			];

			// Advance Search
			foreach ($advanceSearchValues as $key => $value) {

				if ($value['data']) {

					if ($key == 'date_range_start' || $key == 'date_range_end') {

						$query->where($value['column'], $value['operator'], $value['data']);
					} else {

						$query->where($key, $value['operator'], $value['data']);
					}
				}
			}

			if ($query_loop) {

				$totalRecordwithFilter = $query->count_rows();
			} else {

				$query->limit($rowperpage, $start);
				$items = $query->get_all();

				if (!!$items) {

					// Transform data
					foreach ($items as $key => $value) {
						$items[$key]['company_id'] = $value['company']['name'] ?? 'N/A';
						// $items[$key]['updated_at'] = view_date($value['updated_at']);

						$items[$key]['land_area'] = $value['land_area'] . " sqm";

						$items[$key]['land_classification_id'] = Dropdown::get_static('land_classification', $value['land_classification_id'], 'view');

						$red = ['8', '9', '10', '14', '15'];
						$blue = ['12'];
						$clr = "btn btn-primary";
						if (in_array($value['status'], $red)) {
							$clr = "btn btn-danger";
						} elseif (in_array($value['status'], $blue)) {
							$clr = "btn btn-info";
						}

						$items[$key]['status'] = "<span class='" . $clr . "'>" . Dropdown::get_static('land_inventory_status', $value['status'], 'view') . "</span>";

						$items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

						$items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
					}

					foreach ($items as $item) {
						$filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
					}
				}
			}
		}

		$totalRecords = $this->M_land_inventory->count_rows();

		$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"data" => $filteredItems,
		);

		echo json_encode($response);
		exit();
	}

	function view($_id = FALSE)
	{

		if (isset($_POST['ids']) || $_id) {

			if (isset($_POST['ids']) && isset($_POST['type'])) {

				$_id = $_POST['ids'][0];

				$this->view_data['_land'] = $this->M_land_inventory->get($_id);

				if ($_POST['type'] == "debug") {
					vdebug($this->view_data['_land']);
				} else if ($_POST['type'] == "json") {
					echo json_encode($this->view_data['_land']);
					return true;
				}
			}

			$this->view_data['_land'] = $this->M_land_inventory->get($_id);

			if ($this->view_data['_land']) {

				$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
				$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function create()
	{

		if ($this->input->post()) {

			$_input	=	$this->input->post();

			// $_inserted = $this->M_land_inventory->from_form()->insert();
			$_insert	=	$this->M_land_inventory->insert($_input); #lqq(); ud($_insert);
			if ($_insert !== FALSE) {

				// $_lid	=	$this->db->insert_id();

				// $this->notify->success('Land Inventory successfully created.', 'land/update/'.$_lid);

				$this->notify->success('Land Inventory successfully created.', 'land');
			} else {

				$this->notify->error('Oh snap! Please refresh the page and try again.');
			}
		}

		$this->template->build('create', $this->view_data);
	}

	function update($_id = FALSE)
	{

		if ($_id) {

			$_land	=	$this->M_land_inventory->with_company()->get($_id);
			if ($_land) {

				$this->view_data['_land']	=	$_land;

				if ($this->input->post()) {

					$_update	=	[];
					foreach ($this->input->post() as $_key => $_input) {

						if (($_key === 'date_transfer_of_title') || ($_key === 'date_offered') || ($_key === 'decision_date')) {

							$_update[$_key]	=	date_format(date_create($_input), 'Y-m-d H:i:s');
						} else {

							$_update[$_key]	=	$_input;
						}
					}

					$_updated	=	$this->M_land_inventory->update($_update, $_id);
					if ($_updated !== FALSE) {

						$this->notify->success('Land Inventory successfully updated.', 'land');
					} else {

						$this->notify->error('Oh snap! Please refresh the page and try again.');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function delete()
	{

		$_response['_status']	=	0;
		$_response['_msg']		=	'Oops! Please refresh the page and try again.';

		$_id	=	$this->input->post('id');
		if ($_id) {

			$_land	=	$this->M_land_inventory->get($_id);
			if ($_land) {

				$_deleted	=	$this->M_land_inventory->delete($_land['id']);
				if ($_deleted !== FALSE) {

					$_response['_status']	=	1;
					$_response['_msg']		=	'Land Inventory successfully deleted';
				} else {

					$_response['_msg']	=	'Proccess Failed. Please refresh the page and try again';
				}
			}
		}

		echo json_encode($_response);

		exit();
	}

	function bulkDelete()
	{

		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_land_inventory->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Land Inventories successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];

		$_titles[]	=	'#';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		$_land_inventories	=	$this->M_land_inventory->as_array()->get_all();
		if ($_land_inventories) {

			foreach ($_land_inventories as $_lkey => $_land) {

				$_datas[$_land['id']]['#']	=	$_no;

				$_no++;
			}

			$_filename	=	'list_of_land_inventories_' . date('m_d_y_h-i-s', time()) . '.xls';

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				if ($_export_column) {

					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'land');
				}
			} else {

				$_filename	=	'list_of_land_inventories_' . date('m_d_y_h-i-s', time()) . '.csv';

				// $_db_columns	=	$this->M_land_inventory->fillable;
				$_db_columns	=	$this->_table_fillables;
				if (!$_db_columns) {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'land');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

						if ((strpos($_name, '_id') !== FALSE)) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($_land_inventories as $_lkey => $_land) {

								if ($_column === 'land_classification_id') {

									$_datas[$_land['id']][$_title]	=	isset($_land[$_column]) && $_land[$_column] ? Dropdown::get_static('land_classification', $_land[$_column], 'view') : '';
								} elseif ($_column === 'ownership_classification_id') {

									$_datas[$_land['id']][$_title]	=	isset($_land[$_column]) && $_land[$_column] ? Dropdown::get_static('ownership_classification', $_land[$_column], 'view') : '';
								} elseif ($_column === 'company_id') {

									$_datas[$_land['id']][$_title]	=	isset($_land[$_column]) && $_land[$_column] ? Dropdown::get_static('companies', $_land[$_column], 'view') : '';
								} else {

									$_datas[$_land['id']][$_title]	=	isset($_land[$_column]) && $_land[$_column] ? $_land[$_column] : '';
								}
							}
						} elseif ((strpos($_name, 'is_') !== FALSE)) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($_land_inventories as $_lkey => $_land) {

								$_datas[$_land['id']][$_title]	=	isset($_land[$_column]) && ($_land[$_column] !== '') ? Dropdown::get_static('bool', $_land[$_column], 'view') : '';
							}
						} else {

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($_land_inventories as $_lkey => $_land) {

								if ($_name === 'status') {

									$_datas[$_land['id']][$_title]	=	isset($_land[$_name]) && $_land[$_name] ? Dropdown::get_static('land_inventory_status', $_land[$_name], 'view') : '';
								} else {

									$_datas[$_land['id']][$_title]	=	isset($_land[$_name]) && $_land[$_name] ? $_land[$_name] : '';
								}
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_firstAlpha	=	reset($_alphas);
				$_lastAlpha		=	end($_alphas);

				foreach ($_xls_columns as $_xkey => $_column) {

					$_title	=	($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

					$_objSheet->setCellValue($_xkey . $_row, $_title);
				}

				$_objSheet->setTitle('List of Land Inventories');
				$_objSheet->setCellValue('A1', 'LIST OF LAND INVENTORIES');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}

				$_style	=	array(
					'font'  => array(
						'bold'	=>	TRUE,
						'size'	=>	10,
						'name'	=>	'Verdana'
					)
				);
				$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'land');
			}
		} else {

			$this->notify->error('No Record Found', 'land');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Land Inventory CSV Template.csv';

			// $_fillables	=	$this->M_land_inventory->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'land');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$_land	=	$this->M_land_inventory->as_array()->get_all(); #up($_land);
				if ($_land) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($_land as $_dkey => $li) {

							$_datas[$li['id']][$_title]	=	isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Land Inventory');
			$_objSheet->setCellValue('A1', 'LAND INVENTORY');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded Excel File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'land');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_land_inventory->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'land');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {

							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							if ($_id) {

								$_land	=	$this->M_land_inventory->get($_id);
								if ($_land) {

									unset($_data['id']);

									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_update	=	$this->M_land_inventory->update($_data, $_id);
									if ($_update !== FALSE) {

										$_updated++;
									} else {

										$_failed++;

										break;
									}
								} else {

									$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['created_at']	=	NOW;
									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_insert	=	$this->M_land_inventory->insert($_data);
									if ($_insert !== FALSE) {

										$_inserted++;
									} else {

										$_failed++;

										break;
									}
								}
							} else {

								$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['created_at']	=	NOW;
								$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['updated_at']	=	NOW;

								$_insert	=	$this->M_land_inventory->insert($_data);
								if ($_insert !== FALSE) {

									$_inserted++;
								} else {

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {

							$this->notify->error('Upload Failde! Please call your system administrator.', 'land');
						} else {

							$this->notify->success($_msg . '.', 'land');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'land');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'document');
			}
		} else {

			$this->notify->error('Something went wrong!', 'document');
		}
	}

	function consolidate()
	{

		$_response['_status']	=	0;
		$_response['_msg']		=	'Oops! Please refresh the page and try again.';

		sleep(1);

		$_ids	=	$this->input->post('ids');

		if ($_ids) {

			$_hitory_ids	=	[];
			$_datas				=	[];
			$i						=	0;

			$_inputs	=	$this->input->post('input');

			if ($_inputs) {

				foreach ($_inputs as $_input) {

					$_datas[$_input['name']]	=	$_input['value'];
				}

				if ($_datas) {

					$_inserted = $this->M_land_inventory->insert($_datas);
					if ($_inserted !== FALSE) {

						$_lid	=	$this->db->insert_id();

						foreach ($_ids as $_ikey => $_id) {

							unset($_datas);
							$_datas['land_inventory_id']	=	$_lid;
							$_datas['consolidate_id']			=	$_id;

							$land = $this->M_land_inventory->get($_id);

							if ($land['status'] == 9) {

								$this->M_land_inventory->update([
									'status' => 15
								], $_id);
							} else {

								$this->M_land_inventory->update([
									'status' => 10
								], $_id);
							}

							$_history	=	$this->M_li_history->insert($_datas);
							if ($_history !== FALSE) {

								$_hitory_ids[]	=	$this->db->insert_id();

								$i++;
							}
						}

						if (count($_ids) == $i) {

							$_response['_status']	=	1;
							$_response['_msg']		=	'Consolidate! Success';
						} else {

							$this->M_land_inventory->delete($_lid);

							if ($_hitory_ids) {

								foreach ($_hitory_ids as $key => $id) {

									$this->M_li_history->delete($id);
								}
							}

							$_response['_msg']	=	'Some data were not saved. Please contact your system administrator.';
						}
					}
				}
			}
		}

		echo json_encode($_response);

		exit();
	}

	function subdivide()
	{

		$_response['_status']	=	0;
		$_response['_msg']		=	'Oops! Please refresh the page and try again.';

		sleep(1);

		$_land_id = $this->input->post('id');

		if ($_land_id) {

			$_li_ids = [];
			$_h_ids = [];
			$_datas = [];
			$_subs = 0;
			$h = 0;
			$i = 0;

			$_inputs = $this->input->post('input');

			if ($_inputs) {

				foreach ($_inputs as $_input) {

					$_datas[$_input['name']] = $_input['value'];
				}

				$suffix = null;

				if ($_datas['suffix'] == 1) {

					$letter = 'A';
					$suffix = ord($letter);
				} else {

					$suffix = 1;
				}

				if ($_datas) {

					if (isset($_datas['number_of_lot_to_subdivide']) && $_datas['number_of_lot_to_subdivide'] && is_numeric($_datas['number_of_lot_to_subdivide'])) {

						$_subs = $_datas['number_of_lot_to_subdivide'];

						for ($x = 1; $x <= $_subs; $x++) {

							unset($_insert);

							$_insert = $_datas;

							if ($_insert['suffix'] == 1) {

								if ($x > 26) {

									if ($x == 27) {

										$letter = 'A';
										$suffix = ord($letter);
									}

									$_insert['lot_number'] = $_insert['lot_number'] . '-A' . chr($suffix++);
								} else {

									$_insert['lot_number'] = $_insert['lot_number'] . '-' . chr($suffix++);
								}

								// $_insert['lot_number'] = $_insert['lot_number'] . '-' . chr($suffix++);
							} else {

								$_insert['lot_number'] = $_insert['lot_number'] . '-' . $suffix++;
							}

							unset($_insert['suffix']);

							$_inserted = $this->M_land_inventory->insert($_insert);
							if ($_inserted !== FALSE) {

								$_li_ids[]	=	$this->db->insert_id();

								$i++;
							}
						}

						$land = $this->M_land_inventory->get($_land_id);

						if ($land['status'] == 10) {

							$this->M_land_inventory->update([
								'status' => 15
							], $_land_id);
						} else {

							$this->M_land_inventory->update([
								'status' => 9
							], $_land_id);
						}

						if ($_li_ids) {

							foreach ($_li_ids as $_lkey => $_id) {

								unset($_insert);
								$_insert['land_inventory_id']	=	$_id;
								$_insert['subdivide_id']			=	$_land_id;

								$_history = $this->M_li_history->insert($_insert);
								if ($_history !== FALSE) {

									$_h_ids[]	=	$this->db->insert_id();

									$h++;
								}
							}
						}
					}

					if (($_subs == $i) && ($h == $_subs)) {

						$_response['_status']	=	1;
						$_response['_msg']		=	'Subdivide! Success';
					} else {

						if ($_li_ids) {

							foreach ($_li_ids as $key => $id) {

								$this->M_land_inventory->delete($id);
							}
						}

						if ($_h_ids) {

							foreach ($_h_ids as $key => $id) {

								$this->M_li_history->delete($id);
							}
						}

						$_response['_msg']	=	'Some data were not saved. Please contact your system administrator.';
					}
				}
			}
		}

		echo json_encode($_response);

		exit();
	}

	public function consolidate_and_subdivide()
	{
		$response['_status'] = 0;
		$response['_msg'] = 'Oops! Please refresh the page and try again.';

		$land_ids	=	$this->input->post('ids');

		if ($land_ids) {

			$land_data = [];
			$input_fields = $this->input->post('input');

			if ($input_fields) {

				foreach ($input_fields as $field) {
					$land_data[$field['name']] = $field['value'];
				}

				if ($land_data) {

					if (isset($land_data['number_of_lot_to_subdivide']) && $land_data['number_of_lot_to_subdivide'] && is_numeric($land_data['number_of_lot_to_subdivide'])) {

						$number_of_lot_to_subdivide = $land_data['number_of_lot_to_subdivide'];

						$suffix = null;

						if ($land_data['suffix'] == 1) {

							$letter = 'A';
							$suffix = ord($letter);
						} else {

							$suffix = 1;
						}

						$this->db->trans_begin();

						// Create Land Items
						foreach (range(1, $number_of_lot_to_subdivide) as $index) {

							$insert_data = $land_data;

							if ($insert_data['suffix'] == 1) {

								if ($index > 26) {

									if ($index == 27) {

										$letter = 'A';
										$suffix = ord($letter);
									}

									$insert_data['lot_number'] = $insert_data['lot_number'] . '-A' . chr($suffix++);
								} else {

									$insert_data['lot_number'] = $insert_data['lot_number'] . '-' . chr($suffix++);
								}
							} else {

								$insert_data['lot_number'] = $insert_data['lot_number'] . '-' . $suffix++;
							}

							unset($insert_data['suffix']);

							$new_land_id = $this->M_land_inventory->insert($insert_data);

							$this->M_li_history->insert([
								'land_inventory_id' => $new_land_id,
								'consolidate_and_subdivide_id' => json_encode($land_ids)
							]);
						}

						// Update Selected Land items
						foreach ($land_ids as $land_id) {
							$this->M_land_inventory->update([
								'status' => 15
							], $land_id);
						}

						if ($this->db->trans_status() === FALSE) {

							$this->db->trans_rollback();

							$_response['_msg'] = 'Some data were not saved. Please contact your system administrator.';
						} else {

							$this->db->trans_commit();

							$_response['_status'] = 1;
							$_response['_msg'] = 'Consolidate and Subdivide Success';
						}
					}
				}
			}
		}

		echo json_encode($_response);
		exit();
	}

	function document_checklist($_li_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('land/Land_inventory_document_model', 'M_li_document');

		if ($_li_id) {

			$this->view_data['_li_id']	=	$_li_id;

			$_documents	=	$this->M_li_document->_get_document_checklist($_li_id);
			if ($_documents) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('document_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function process_checklist_upload($_li_id = FALSE, $_doc_id = FALSE)
	{

		if ($_li_id && $_doc_id && isset($_FILES['_file']['name']) && ($_FILES['_file']['name'] !== '')) {

			$this->load->model('land/Land_inventory_document_model', 'M_li_document');

			unset($_where);
			$_where['land_inventory_id']	=	$_li_id;
			$_where['document_id']				=	$_doc_id;

			$_li_document	=	$this->M_li_document->find_all($_where, ['id'], TRUE);
			if ($_li_document && isset($_li_document->id) && $_li_document->id) {

				$_ext	=	['.jpg', '.jpeg', '.png'];

				set_time_limit(0);

				unset($_config);
				$_config['upload_path']	=	$_location	=	'assets/img/_land/_documents/_process';
				$_config['allowed_types'] = 'jpeg|jpg|png';
				$_config['overwrite'] = TRUE;
				$_config['max_size'] = '1000000';
				$_config['file_name'] = $_filename = $_doc_id . '_thumb';

				if (isset($_ext) && $_ext) {

					$_image	=	$_location . '/' . $_filename;

					foreach ($_ext as $key => $x) {

						if (file_exists($_image . $x)) {

							unlink($_image . $x);
						}
					}
				}

				$this->load->library('upload', $_config);

				if (!$this->upload->do_upload('_file')) {

					// ud($this->upload->displaY_errors());

					$this->notify->error($this->upload->displaY_errors(), 'land/process_checklist/' . $_li_id);
				} else {

					$_uploaded_document_id	=	FALSE;

					$this->load->model('document/Land_document_upload_model', 'M_land_uploaded_document');

					// up($this->user);

					// ud($this->upload->data());

					$_udatas	=	$this->upload->data();
					if ($_udatas) {

						unset($_where);
						$_where['land_inventory_id']	=	$_li_id;
						$_where['document_id']				=	$_doc_id;

						$_uploaded_document	=	$this->M_land_uploaded_document->find_all($_where, ['id'], TRUE); #lqq(); ud($_uploaded_document);
						if ($_uploaded_document && isset($_uploaded_document->id) && $_uploaded_document->id) {

							$_uploaded_document_id	=	$_uploaded_document->id;

							unset($_update);
							$_update['land_inventory_id']	=	$_li_id;
							$_update['document_id']				=	$_doc_id;
							$_update['file_name']					=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_update['file_type']					=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_update['file_src']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_update['file_path']					=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_update['full_path']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_update['raw_name']					=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_update['orig_name']					=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_update['client_name']				=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_update['file_ext']					=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_update['file_size']					=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_update['updated_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']				=	NOW;

							$_updated	=	$this->M_land_uploaded_document->update($_update, $_uploaded_document_id);
							if (!$_updated) {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'land/process_checklist/' . $_li_id);
							}
						} else {

							unset($_insert);
							$_insert['land_inventory_id']	=	$_li_id;
							$_insert['document_id']				=	$_doc_id;
							$_insert['file_name']					=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_insert['file_type']					=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_insert['file_src']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_insert['file_path']					=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_insert['full_path']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_insert['raw_name']					=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_insert['orig_name']					=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_insert['client_name']				=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_insert['file_ext']					=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_insert['file_size']					=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_insert['created_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['created_at']				=	NOW;
							$_insert['updated_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['updated_at']				=	NOW;

							$_inserted	=	$this->M_land_uploaded_document->insert($_insert);
							if ($_inserted) {

								$_uploaded_document_id	=	$this->db->insert_id();
							} else {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'land/process_checklist/' . $_li_id);
							}
						}

						// ud($_uploaded_document_id);

						if ($_uploaded_document_id) {

							unset($_update);
							$_update['uploaded_document_id']	=	$_uploaded_document_id;
							$_update['updated_by']						=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']						=	NOW;

							$_updated	=	$this->M_li_document->update($_update, $_li_document->id);
							if ($_updated) {

								$this->notify->success('Success! Image uploaded.', 'land/process_checklist/' . $_li_id);
							} else {

								$this->notify->error('Upload Failde. Please refresh the page and try again.', 'land/process_checklist/' . $_li_id);
							}
						} else {

							$this->notify->error('Something went wrong. Please refresh the page and try again.', 'land/process_checklist/' . $_li_id);
						}
					}
				}
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function get_document_checklist($_li_id = FALSE)
	{

		$_docuemnt_ids	=	[];

		$this->load->model('land/Land_inventory_document_model', 'M_li_document');
		$this->load->model('document/Document_model', 'M_document');

		$_response	=	[];

		$_total['_displays']	=	0;
		$_total['_records']		=	0;
		$_datas	=	[];
		$_secho	=	0;

		if ($_li_id) {

			$_columns	=	[
				'id'	=>	TRUE,
				'name'	=>	TRUE,
				'description'	=>	TRUE,
				'owner_id'	=>	TRUE,
				'classification_id'	=>	TRUE,
				'updated_at'	=>	TRUE
			];

			if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

				$_columns	=	[];

				foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

					$_columns[$_def]	=	TRUE;
				}
			}

			// $_documents	=	$this->M_document->where('id', $_docuemnt_ids)->as_array()->get_all(); #vd($_documents);
			$_documents	=	$this->M_li_document->_get_document_checklist($_li_id);
			if ($_documents) {

				foreach ($_documents as $_ckkey => $_ck) {

					$_datas[]	=	$this->filterArray($_ck, $_columns);
				}

				$_total['_displays']	=	$_total['_records']	=	count($_datas);

				if (isset($_REQUEST['search'])) {

					$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

					$_total['_displays']	=	$_datas ? count($_datas) : 0;
				}

				if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

					foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

						if ($_clm['search']) {

							$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

							$_total['_displays']	=	$_datas ? count($_datas) : 0;
						}
					}
				}

				if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

					$_column	=	$_REQUEST['order'][0]['column'];
					$_dir			=	$_REQUEST['order'][0]['dir'];

					usort($_datas, function ($x, $y) use ($_column, $_dir) {

						$x	=	array_slice($x, $_column, 1);
						$x	=	array_pop($x);

						$y	=	array_slice($y, $_column, 1);
						$y	=	array_pop($y);

						if ($_dir === 'asc') {

							return $x > $y ? TRUE : FALSE;
						} else {

							return $x < $y ? TRUE : FALSE;
						}
					});
				}

				if (isset($_REQUEST['length'])) {

					$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
				}

				if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

					$_temp	=	$_datas;
					$_datas	=	[];

					foreach ($_temp as $key => $_tmp) {

						$_datas[]	=	array_values($_tmp);
					}
				}

				if (isset($_REQUEST['sEcho'])) {

					$_secho	=	intval($_REQUEST['sEcho']);
				}
			}
		}

		$_response	=	[
			'iTotalDisplayRecords'	=>	$_total['_displays'],
			'iTotalRecords'					=>	$_total['_records'],
			'sColumns'							=>	'',
			'sEcho'									=>	$_secho,
			'data'									=>	$_datas
		];

		echo json_encode($_response);

		exit();
	}

	function documents($_li_id = FALSE)
	{

		$this->view_data['_largescreen']	=	TRUE;

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		// $this->load->model('land/Land_inventory_document_model', 'M_li_document');
		$this->load->model('checklist/Checklist_model', 'M_checklist');

		if ($_li_id) {

			$this->view_data['_li_id']	=	$_li_id;

			$_checklists	=	$this->M_checklist->find_all(FALSE, ['id', 'name']);
			if ($_checklists) {

				$this->view_data['_checklists']	=	$_checklists; #ud($_checklists);
			}

			$this->template->build('documents', $this->view_data);
		} else {

			show_404();
		}
	}

	function get_filtered_document_checklist()
	{

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'';
		$_view_data	=	[];

		$_checklist_id	=	$this->input->post('value');
		$_li_id					=	$this->input->post('li_id');
		if ($_checklist_id && $_li_id) {

			$_view_data['_li_id']	=	$_li_id;

			$this->load->model('checklist/Document_checklist_model', 'M_document_checklist');

			$_documents	=	$this->M_document_checklist->_get_documents($_checklist_id);
			if ($_documents) {

				$_view_data['_documents']	=	$_documents;
			}

			$_respo['_status']	=	1;

			$_respo['_html']	=	$this->load->view('_checklist/documents', $_view_data, TRUE);
		} else {

			$_respo['_msg']	=	'Oops! Please refresh the page and try again.';
		}

		echo json_encode($_respo);

		exit();
	}

	function insert_document_checklist($_li_id = FALSE)
	{

		if ($_li_id) {

			$_dcs	=	[];

			$_lid_ids	=	[];

			$_inserted	=	0;
			$_updated		=	0;
			$_removed		=	0;
			$_failed		=	0;

			$_inputs	=	$this->input->post();
			if ($_inputs) {

				foreach ($_inputs as $key => $_input) {

					if ($key !== '_document_checklist_length') {

						$_dcs[$key]['land_inventory_id']	=	$_li_id;
						$_dcs[$key]['document_id']	=	$key;

						// up($_input);

						foreach ($_input as $_ikey => $_int) {

							if ($_ikey === 'start_date') {

								$_dcs[$key][$_ikey]	=	date_format(date_create($_int), 'Y-m-d H:i:s');
							} else {

								$_dcs[$key][$_ikey]	=	$_int;
							}
						}
					} else {

						continue;
					}
				}

				$this->load->model('land/Land_inventory_document_model', 'M_li_document');

				if ($_dcs && !empty($_dcs)) {

					foreach ($_dcs as $_dkey => $_dc) {

						unset($_where);
						$_where['land_inventory_id']	=	$_dc['land_inventory_id'];
						$_where['document_id']				=	$_dc['document_id'];

						$_lid	=	$this->M_li_document->find_all($_where, FALSE, TRUE); #lqq(); up($_lid);
						if ($_lid) {

							unset($_datas);
							foreach ($_dc as $_dkey => $_d) {

								if (($_dkey !== 'land_inventory_id') && ($_dkey !== 'document_id')) {

									$_datas[$_dkey]	=	$_d;

									$_datas[$_dkey]['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_datas[$_dkey]['updated_at']	=	NOW;
								}
							}

							$_update	=	$this->M_li_document->update($_datas, $_lid->id);
							if ($_update) {

								$_updated++;
							} else {

								$_failed++;

								break;
							}
						} else {

							unset($_datas);
							$_datas	=	$_dc;

							$_datas['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['created_at']	=	NOW;
							$_datas['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['updated_at']	=	NOW;

							$_insert	=	$this->M_li_document->insert($_datas);
							if ($_insert) {

								$_inserted++;
							} else {

								$_failed++;

								break;
							}
						}
					}

					$_msg	=	'';
					if ($_inserted > 0) {

						$_msg	=	$_inserted . ' document/s was successfuly inserted';
					}

					if ($_updated > 0) {

						$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' document/s was successfuly updated';
					}

					if ($_failed > 0) {

						$this->notify->error('Something went wrong! Please refresh the page and try again.', 'land/document_checklist/' . $_li_id);
					} else {

						$this->notify->success($_msg . '.', 'land/document_checklist/' . $_li_id);
					}
				}
			}
		} else {

			show_404();
		}
	}

	function process_checklist($_li_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('land/Land_inventory_document_model', 'M_li_document');

		if ($_li_id) {

			$this->view_data['_li_id']	=	$_li_id;

			// ud(Dropdown::get_static('document_owner'));

			$_documents	=	$this->M_li_document->_get_document_checklist($_li_id); #lqq(); ud($_documents);
			if ($_documents) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('process_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function get_process_checklist($_li_id = FALSE)
	{

		$_docuemnt_ids	=	[];

		$this->load->model('land/Land_inventory_document_model', 'M_li_document');
		$this->load->model('document/Document_model', 'M_document');

		$_response	=	[];

		$_total['_displays']	=	0;
		$_total['_records']		=	0;
		$_datas	=	[];
		$_secho	=	0;

		if ($_li_id) {

			$_columns	=	[
				'id'	=>	TRUE,
				'name'	=>	TRUE,
				'owner_id'	=>	TRUE,
				'status'	=>	TRUE,
				'date_created'	=>	TRUE
			];

			if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

				$_columns	=	[];

				foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

					$_columns[$_def]	=	TRUE;
				}
			}

			// $_documents	=	$this->M_document->where('id', $_docuemnt_ids)->as_array()->get_all(); #vd($_documents);
			$_documents	=	$this->M_li_document->_get_document_checklist($_li_id);
			if ($_documents) {

				foreach ($_documents as $_ckkey => $_ck) {

					$_datas[]	=	$this->filterArray($_ck, $_columns);
				}

				$_total['_displays']	=	$_total['_records']	=	count($_datas);

				if (isset($_REQUEST['search'])) {

					$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

					$_total['_displays']	=	$_datas ? count($_datas) : 0;
				}

				if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

					foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

						if ($_clm['search']) {

							$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

							$_total['_displays']	=	$_datas ? count($_datas) : 0;
						}
					}
				}

				if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

					$_column	=	$_REQUEST['order'][0]['column'];
					$_dir			=	$_REQUEST['order'][0]['dir'];

					usort($_datas, function ($x, $y) use ($_column, $_dir) {

						$x	=	array_slice($x, $_column, 1);
						$x	=	array_pop($x);

						$y	=	array_slice($y, $_column, 1);
						$y	=	array_pop($y);

						if ($_dir === 'asc') {

							return $x > $y ? TRUE : FALSE;
						} else {

							return $x < $y ? TRUE : FALSE;
						}
					});
				}

				if (isset($_REQUEST['length'])) {

					$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
				}

				if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

					$_temp	=	$_datas;
					$_datas	=	[];

					foreach ($_temp as $key => $_tmp) {

						$_datas[]	=	array_values($_tmp);
					}
				}

				if (isset($_REQUEST['sEcho'])) {

					$_secho	=	intval($_REQUEST['sEcho']);
				}
			}
		}

		$_response	=	[
			'iTotalDisplayRecords'	=>	$_total['_displays'],
			'iTotalRecords'					=>	$_total['_records'],
			'sColumns'							=>	'',
			'sEcho'									=>	$_secho,
			'data'									=>	$_datas
		];

		echo json_encode($_response);

		exit();
	}

	public function dashboard()
	{
		$this->css_loader->queue([
			'//www.amcharts.com/lib/3/plugins/export/export.css'
		]);

		$this->js_loader->queue([
			'//www.amcharts.com/lib/3/amcharts.js',
			'//www.amcharts.com/lib/3/serial.js',
			'//www.amcharts.com/lib/3/plugins/export/export.min.js'
		]);

		$this->js_loader->queue(['vendors/custom/highcharts/code/highcharts.js']);
		$this->js_loader->queue(['vendors/custom/highcharts/code/modules/series-label.js']);
		$this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
		$this->js_loader->queue(['vendors/custom/highcharts/code/modules/accessibility.js']);
		$this->js_loader->queue(['modules/dashboard/hc-themes.js']);

		$this->template->build('dashboard', $this->view_data);
	}

	public function get_dashboard_data()
	{
		$land_inventory_status = Dropdown::get_static('land_inventory_status');

		$data = [];

		foreach ($land_inventory_status as $key => $status) {
			if ($key) {

				$this->db->select('count(id) as count')
					->from('land_inventories')
					->where('deleted_at IS NULL')
					->where('status', $key);

				$query = $this->db->get()->row_array();

				array_push($data, [
					'status' => $status,
					'count' => $query['count']
				]);
			}
		}

		echo json_encode($data);
	}



	public function pie()
	{



		$land_classification = Dropdown::get_static('land_classification');

		$series_2 = [];
		$v = 0;
		foreach ($land_classification as $key => $c) {

			if (!$key) {
				continue;
			}

			$this->db->select('count(id) as count')
				->from('land_inventories')
				->where('deleted_at IS NULL')
				->where('land_classification_id', $key);
			$query = $this->db->get()->row_array();


			$s['name'] = $c;
			$s['y'] = (int) $query['count'];
			array_push($series_2, $s);

			if ($s['y']) {
				$v = 1;
			}
		}
		if (empty($v)) {
			$series_2 = [];
		}

		echo json_encode($series_2);
	}
}
