<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Leasing_tenant_employment_information_model extends My_Model
{
    public $table = 'leasing_tenant_employment_information';
    public $primary_key = 'id';
    public $fillable = [
        'leasing_tenant_id',
        'occupation_type_id',
        'industry_id',
        'occupation_id',
        'designation',
        'employer',
        'gross_salary',
        'location_id',
        'address',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id'];
    public $rules = [];
    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // $this->has_one['leasing_project'] = array('foreign_model' => 'leasing_project/Leasing_project_model', 'foreign_table' => 'leasing_projects', 'foreign_key' => 'id', 'local_key' => 'leasing_project_id');
    }
}
