<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Leasing_tenant_model extends My_Model
{
    public $table = 'leasing_tenants';
    public $primary_key = 'id';
    public $fillable = [
        'tenant_id',
        'tenant_type_id',
        'company_name',
        'nature_of_business',
        'company_tin_id',
        'first_name',
        'middle_name',
        'last_name',
        'birth_place',
        'birth_date',
        'gender_id',
        'civil_status_id',
        'nationality_id',
        'file_name',
        'full_path',
        'mobile_number',
        'email',
        'landline',
        'other_mobile',
        'present_address',
        'mailing_address',
        'business_address',
        'social_media_address',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id'];
    public $rules = [];
    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['employment_information'] = array('foreign_model' => 'Leasing_tenant_employment_information_model', 'foreign_table' => 'leasing_tenant_employment_information', 'foreign_key' => 'leasing_tenant_id', 'local_key' => 'id');

        $this->has_many['proof_of_identification'] = array('foreign_model' => 'Leasing_tenant_proof_of_identification_model', 'foreign_table' => 'leasing_tenant_proof_of_identification', 'foreign_key' => 'leasing_tenant_id', 'local_key' => 'id');
    }
}
