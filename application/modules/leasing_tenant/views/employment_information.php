<?php

$occupation_type_id = $info['employment_information']['occupation_type_id'] ?? null;
$industry_id = $info['employment_information']['industry_id'] ?? null;
$occupation_id = $info['employment_information']['occupation_id'] ?? null;
$designation = $info['employment_information']['designation'] ?? null;
$employer = $info['employment_information']['employer'] ?? null;
$gross_salary = $info['employment_information']['gross_salary'] ?? null;
$location_id = $info['employment_information']['location_id'] ?? null;
$address = $info['employment_information']['address'] ?? null;

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Occupation Type<span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('employment_information[occupation_type_id]', Dropdown::get_static('occupation_type'), set_value('employment_information[occupation_type_id]', @$occupation_type_id), 'class="form-control" id="occupation_type_id" required'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Industry</label>
            <?php echo form_dropdown('employment_information[industry_id]', Dropdown::get_static('industry'), set_value('employment_information[industry_id]', @$industry_id), 'class="form-control" id="industry_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Job / Occupation<span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('employment_information[occupation_id]', Dropdown::get_static('occupation'), set_value('employment_information[occupation_id]', @$occupation_id), 'class="form-control" id="occupation_id" required'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Designation / Title</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Designation" name="employment_information[designation]" value="<?php echo set_value('employment_information[designation]"', @$designation); ?>">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square-o"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Business / Employer Name</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Employer" name="employment_information[employer]" value="<?php echo set_value('employment_information[employer]"', @$employer); ?>">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square-o"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Gross Salary<span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" placeholder="Gross Salary" name="employment_information[gross_salary]" value="<?php echo set_value('employment_information[gross_salary]"', @$gross_salary); ?>" required>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square-o"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Location<span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('employment_information[location_id]', Dropdown::get_static('occupation_location'), set_value('employment_information[location_id]', @$location_id), 'class="form-control" id="location_id" required'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Full Address:</label>
            <textarea class="form-control" id="address" rows="3" spellcheck="false" name="employment_information[address]"><?php echo set_value('info[address]"', @$address); ?></textarea>
        </div>
    </div>
</div>