<?php

$area = $info['area'] ?? '';
$cusa_price = $info['cusa_price'] ?? '';
$price_per_sqm = $info['price_per_sqm'] ?? '';
$base_rental_rate = $info['base_rental_rate'] ?? '';

?>

<div class="row">
    <div class="col-sm-3">
        <label>Area <span class="kt-font-danger">*</span></label>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Area" name="info[area]" value="<?= $area; ?>">
            <div class="input-group-append">
                <span class="input-group-text">SQM</span>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <label>Cusa Price <span class="kt-font-danger">*</span></label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">PHP</span>
            </div>
            <input type="number" class="form-control" placeholder="Cusa Price" name="info[cusa_price]" value="<?= $cusa_price; ?>">
        </div>
    </div>
    <div class="col-sm-3">
        <label>Price / SQM <span class="kt-font-danger">*</span></label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">PHP</span>
            </div>
            <input type="number" class="form-control" placeholder="Price / SQM" name="info[price_per_sqm]" value="<?= $price_per_sqm; ?>">
        </div>
    </div>
    <div class="col-sm-3">
        <label>Base Rental Rate <span class="kt-font-danger">*</span></label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">PHP</span>
            </div>
            <input type="number" class="form-control" placeholder="Base Rental Rate" name="info[base_rental_rate]" value="<?= $base_rental_rate; ?>">
        </div>
    </div>
</div>