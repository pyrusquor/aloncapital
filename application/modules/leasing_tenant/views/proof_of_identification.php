<?php

$proof_of_identification = $info['proof_of_identification'] ?? [];
?>
<div class="row">
    <div class="col-lg-12">
        <!--begin::Accordion-->
        <div class="accordion  accordion-toggle-arrow" id="accordionExample4">

            <?php foreach (range(1, 3) as $key => $value) : ?>
                <div class="card">
                    <div class="card-header" id="heading-<?= $value; ?>">
                        <div class="card-title <?= ($value == '1') ? '' : 'collapsed'; ?>" data-toggle="collapse" data-target="#collapse-<?= $value; ?>" aria-expanded="<?= ($value == '1') ? 'true' : 'false'; ?>" aria-controls="collapse-<?= $value; ?>">
                            <i class="flaticon2-file-2"></i> Identification # <?= $value; ?>
                        </div>
                    </div>
                    <input type="hidden" name="proof_of_identity[id][]" value="<?= $proof_of_identification[$key]['id'] ?>">
                    <div id="collapse-<?= $value; ?>" class="collapse <?= ($value == '1') ? 'show' : ''; ?>" aria-labelledby="headingTwo1" data-parent="#accordionExample4">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="">Type of ID</label>
                                <?= form_dropdown('proof_of_identity[type_of_id][]', Dropdown::get_static('ids'), $proof_of_identification[$key]['type_of_id'], 'class="form-control" id="type_of_id"'); ?>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="form-group">
                                <label class="">ID Number</label>
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="ID Number" name="proof_of_identity[id_number][]" value="<?= $proof_of_identification[$key]['id_number'] ?>">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon2-information"></i></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="form-group">
                                <label class="">Date Issued</label>
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control datePicker" placeholder="Date Issued" name="proof_of_identity[date_issued][]" value="<?= $proof_of_identification[$key]['date_issued'] ?>" readonly>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon-calendar"></i></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="form-group">
                                <label class="">Date Expiration</label>
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control datePicker" placeholder="Date Expiration" name="proof_of_identity[date_expiration][]" value="<?= $proof_of_identification[$key]['date_expiration'] ?>" readonly>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon-calendar"></i></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="form-group">
                                <label class="">Place Issued</label>
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Place Issued" name="proof_of_identity[place_issued][]" value="<?= $proof_of_identification[$key]['place_issued'] ?>">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-o"></i></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="form-group">
                                <label class="">File Upload</label>
                                <input type="file" name="proof_of_identity_file_<?= $key ?>" class="" aria-invalid="false">
                            </div>
                            <?php if ($proof_of_identification[$key]['full_path']) : ?>
                                <div class="form-group">
                                    <label>Current Photo</label>
                                    <br>
                                    <img src="<?= $proof_of_identification[$key]['full_path'] ?>" alt="photo" width="200" height="200" class="img-fluid">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>

        <!--end::Accordion-->

    </div>
</div>