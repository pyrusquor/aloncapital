<?php

$tenant_id = $info['tenant_id'] ?? '';
$tenant_type_id = $info['tenant_type_id'] ?? '';
$company_name = $info['company_name'] ?? '';
$nature_of_business = $info['nature_of_business'] ?? '';
$company_tin_id = $info['company_tin_id'] ?? '';
$first_name = $info['first_name'] ?? '';
$middle_name = $info['middle_name'] ?? '';
$last_name = $info['last_name'] ?? '';
$birth_place = $info['birth_place'] ?? '';
$birth_date = $info['birth_date'] ?? '';
$gender_id = $info['gender_id'] ?? '';
$civil_status_id = $info['civil_status_id'] ?? '';
$nationality_id = $info['nationality_id'] ?? '';
$image_full_path = $info['full_path'] ?? '';

?>

<div class="row">

    <div class="col-sm-4">
        <div class="form-group">
            <label>Tenant ID <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[tenant_id]" value="<?= $tenant_id ?>" placeholder="Tenant ID" required>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Tenant Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[tenant_type_id]', Dropdown::get_static("leasing_tenant_type"), $tenant_type_id, 'class="form-control" required id="tenant_type_id"') ?>
        </div>
    </div>

    <div class="col-sm-4"></div>

    <div class="col-sm-4 company-tenant-info">
        <div class="form-group">
            <label>Company Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[company_name]" value="<?= $company_name ?>" placeholder="Company Name">
        </div>
    </div>

    <div class="col-sm-4 company-tenant-info">
        <div class="form-group">
            <label>Nature of Business <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[nature_of_business]" value="<?= $nature_of_business ?>" placeholder="Nature of Business">
        </div>
    </div>

    <div class="col-sm-4 company-tenant-info">
        <div class="form-group">
            <label>Company TIN ID <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[company_tin_id]" value="<?= $company_tin_id ?>" placeholder="Company TIN ID">
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Lastname <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[last_name]" value="<?= $last_name ?>" placeholder="Lastname" required>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Firstname <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[first_name]" value="<?= $first_name ?>" placeholder="Firstname" required>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Middlename</label>
            <input type="text" class="form-control" name="info[middle_name]" value="<?= $middle_name ?>" placeholder="Middlename">
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Birth Place</label>
            <input type="text" class="form-control" name="info[birth_place]" value="<?= $birth_place ?>" placeholder="Birth Place">
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Birth Date</label>
            <input type="text" class="form-control datePicker" name="info[birth_date]" value="<?= $birth_date ?>" placeholder="Birth Date" readonly>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Gender <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[gender_id]', Dropdown::get_static("sex"), $gender_id, 'class="form-control" required') ?>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Civil Status <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[civil_status_id]', Dropdown::get_static("civil_status"), $civil_status_id, 'class="form-control" required') ?>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Nationality <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[nationality_id]', Dropdown::get_static("nationality"), $nationality_id, 'class="form-control" id="nationality_id" required') ?>
        </div>
    </div>

    <div class="col-sm-4"></div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label>Photo</label>
                <br>
                <input type="file" name="file" accept="image/png, image/jpeg, image/jpg">
            </div>
        </div>

        <div class="col-sm-8"></div>

        <?php if ($image_full_path) : ?>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Current Photo</label>
                    <br>
                    <img src="<?= $image_full_path ?>" alt="photo" width="200" height="200" class="img-fluid">
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>