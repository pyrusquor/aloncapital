<?php

$mobile_number = $info['mobile_number'] ?? '';
$email = $info['email'] ?? '';
$landline = $info['landline'] ?? '';
$other_mobile = $info['other_mobile'] ?? '';
$present_address = $info['present_address'] ?? '';
$mailing_address = $info['mailing_address'] ?? '';
$business_address = $info['business_address'] ?? '';
$social_media_address = $info['social_media_address'] ?? '';

?>

<div class="row">

    <div class="col-sm-6">
        <div class="form-group">
            <label>Mobile <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[mobile_number]" value="<?= $mobile_number ?>" placeholder="Mobile">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Email <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[email]" value="<?= $email ?>" placeholder="Email">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Landline </label>
            <input type="text" class="form-control" name="info[landline]" value="<?= $landline ?>" placeholder="Landline">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Other Mobile </label>
            <input type="text" class="form-control" name="info[other_mobile]" value="<?= $other_mobile ?>" placeholder="Other Mobile">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Present Address <span class="kt-font-danger">*</span></label>
            <textarea class="form-control" name="info[present_address]" rows="5">
                <?= $present_address ?>
            </textarea>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Mailing Address</label>
            <textarea class="form-control" name="info[mailing_address]" rows="5">
                <?= $mailing_address ?>
            </textarea>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Business Address</label>
            <textarea class="form-control" name="info[business_address]" rows="5">
                <?= $business_address ?>
            </textarea>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Social Media Address (URL)</label>
            <textarea class="form-control" name="info[social_media_address]" rows="5">
                <?= $social_media_address ?>
            </textarea>
        </div>
    </div>
</div>