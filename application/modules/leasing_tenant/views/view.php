<?php

// Basic Info
$id = $info['id'] ?? '';
$tenant_id = $info['tenant_id'] ?? '';
$tenant_type_id = $info['tenant_type_id'] ?? '';
$company_name = $info['company_name'] ?? '';
$nature_of_business = $info['nature_of_business'] ?? '';
$company_tin_id = $info['company_tin_id'] ?? '';
$first_name = $info['first_name'] ?? '';
$middle_name = $info['middle_name'] ?? '';
$last_name = $info['last_name'] ?? '';
$birth_place = $info['birth_place'] ?? '';
$birth_date = $info['birth_date'] ?? '';
$gender_id = $info['gender_id'] ?? '';
$civil_status_id = $info['civil_status_id'] ?? '';
$nationality_id = $info['nationality_id'] ?? '';
$image_full_path = $info['full_path'] ?? '';

// Contact Info
$mobile_number = $info['mobile_number'] ?? '';
$email = $info['email'] ?? '';
$landline = $info['landline'] ?? '';
$other_mobile = $info['other_mobile'] ?? '';
$present_address = $info['present_address'] ?? '';
$mailing_address = $info['mailing_address'] ?? '';
$business_address = $info['business_address'] ?? '';
$social_media_address = $info['social_media_address'] ?? '';

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Leasing Tenant</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('leasing_tenant/form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('leasing_tenant'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab" aria-selected="true">
                            <i class="flaticon2-user-outline-symbol"></i> Basic Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#contact_information" role="tab" aria-selected="false">
                            <i class="flaticon-support"></i> Contact Info
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#work_experience" role="tab" aria-selected="false">
                            <i class="flaticon-trophy"></i> Employment Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#source_identification" role="tab" aria-selected="false">
                            <i class="flaticon-medal"></i> Proof of Identification
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div class="tab-content kt-margin-t-20">
                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="basic_information" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row ">
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Last Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords($last_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">First Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords($first_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Middle Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords($middle_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Tenant ID:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $tenant_id ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Tenant Type:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords(Dropdown::get_static("leasing_tenant_type", $tenant_type_id)) ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Company Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords($company_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Nature of Business:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords($nature_of_business); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Company TIN ID:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords($company_tin_id); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Birth Place:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords($birth_place); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Birth Date:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= view_date($birth_date) ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Gender:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords(Dropdown::get_static("sex", $gender_id)); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Civil Status:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords(Dropdown::get_static("civil_status", $civil_status_id)); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Nationality:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= ucwords(Dropdown::get_static("nationality", $nationality_id)); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Photo:</label>
                                            <div class="col">
                                                <?php if ($image_full_path) : ?>
                                                    <div class="form-group">
                                                        <img src="<?= $image_full_path ?>" alt="photo" width="200" height="200" class="img-fluid">
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="contact_information" role="tabpanel-2">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row ">
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Mobile:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $mobile_number ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Email:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $email ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Landline:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $landline ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Other Mobile:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $other_mobile ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Present Address:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $present_address ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Mailing Address:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $mailing_address ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Business Address:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $business_address ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label  text-right">Social Media Address (URL):</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?= $social_media_address ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->
            </div>
        </div>
    </div>