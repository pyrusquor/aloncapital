<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Leasing_tenant extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $models = [
            'Leasing_tenant_model' => 'M_leasing_tenant',
            'Leasing_tenant_employment_information_model' => 'M_leasing_tenant_employment_information',
            'Leasing_tenant_proof_of_identification_model' => 'M_leasing_tenant_proof_of_identification',
        ];

        // Load models
        $this->load->model($models);
    }

    public function index()
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index');
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'name' => true,
            'tenant_id' => true,
            'mobile_number' => true,
            'email' => true,
            'present_address' => true,
            'created_by' => true,
            'updated_by' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_leasing_tenant
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("first_name like '%$searchValue%'");
                $query->or_where("last_name like '%$searchValue%'");
                $query->or_where("tenant_id like '%$searchValue%'");
                $query->or_where("mobile_number like '%$searchValue%'");
                $query->or_where("email_address like '%$searchValue%'");
                $query->or_where("present_address like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'leasing_tenant_id' => [
                    'data' => $filters['leasing_tenant_id'] ?? null,
                    'operator' => '=',
                ],
                'company_id' => [
                    'data' => $filters['company_id'] ?? null,
                    'operator' => '=',
                ],
                'unit_type_id' => [
                    'data' => $filters['unit_type_id'] ?? null,
                    'operator' => '=',
                ],
                'unit_number' => [
                    'data' => $filters['unit_number'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $first_name = $value['first_name'] ?? '';
                        $last_name = $value['last_name'] ?? '';

                        $items[$key]['name'] = $first_name . ' ' . $last_name;

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_leasing_tenant->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {
        $method = !!$id ? "Update" : "Create";

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->db->trans_begin();

            $info = $this->input->post('info');

            $employment_information = $this->input->post('employment_information');

            $proof_of_identity = $this->input->post('proof_of_identity');

            $photo = $_FILES['file'] ?? null;

            $proof_of_identity_file_0 = $_FILES['proof_of_identity_file_0'] ?? [];
            $proof_of_identity_file_1 = $_FILES['proof_of_identity_file_1'] ?? [];
            $proof_of_identity_file_2 = $_FILES['proof_of_identity_file_2'] ?? [];

            if ($id) {

                $updated_by = ['updated_by' => $this->user->id ?? NULL];

                $this->M_leasing_tenant->update($info + $updated_by, $id);

                $this->M_leasing_tenant_employment_information->where('leasing_tenant_id', $id)->update($employment_information + $updated_by);

                $proof_of_identity_info = [];

                $index = 0;

                foreach ($proof_of_identity as $key => $value) {

                    foreach ($value as $_key => $_value) {

                        $proof_of_identity_info[$index++][$key] = $_value;
                    }

                    $index = 0;
                }

                $proofs =  $this->M_leasing_tenant->with_proof_of_identification()->get($id)['proof_of_identification'];

                foreach ($proof_of_identity_info as $key => $value) {

                    $value += [
                        'leasing_tenant_id' => $id
                    ];

                    $_id = $value['id'];

                    unset($value['id']);

                    $this->M_leasing_tenant_proof_of_identification->update($value, $_id);

                    if (isset(${'proof_of_identity_file_' . $key}['name'])) {

                        $this->uploadProofOfIdentityImage($_id, 'proof_of_identity_file_' . $key);
                    }
                }

                $this->M_leasing_tenant_proof_of_identification->where();
            } else {

                $id = $this->M_leasing_tenant->insert($info);

                $employment_information['leasing_tenant_id'] = $id;

                $this->M_leasing_tenant_employment_information->insert($employment_information);

                $proof_of_identity_info = [];

                $index = 0;

                foreach ($proof_of_identity as $key => $value) {

                    foreach ($value as $_key => $_value) {

                        $proof_of_identity_info[$index++][$key] = $_value;
                    }

                    $index = 0;
                }

                foreach ($proof_of_identity_info as $key => $value) {

                    $value += [
                        'leasing_tenant_id' => $id
                    ];

                    unset($value['id']);

                    $proof_of_identification_id = $this->M_leasing_tenant_proof_of_identification->insert($value);

                    if (!!${'proof_of_identity_file_' . $key}['name']) {

                        $this->uploadProofOfIdentityImage($proof_of_identification_id, 'proof_of_identity_file_' . $key);
                    }
                }
            }

            if (!!$photo['name']) {

                $msg = $this->uploadImage($id);

                if ($msg) {

                    $response['msg'] = $msg;
                }
            }

            if ($this->db->trans_status() === FALSE) {

                $this->db->trans_rollback();
            } else {

                $this->db->trans_commit();
                $response['status'] = 1;
                $response['message'] = 'Leasing Property Successfully ' . $method . 'd!';
            }

            echo json_encode($response);
            exit();
        } else {

            if ($id) {

                $this->view_data['info'] = $this->M_leasing_tenant
                    ->with_employment_information()
                    ->with_proof_of_identification()
                    ->get($id);
            }

            $this->template->build('form', $this->view_data);
        }
    }

    private function uploadImage($leasing_tenant_id)
    {
        $_location = 'assets/img/leasing/tenant';

        // Create dir if does not exist
        if (!file_exists($_location)) {

            mkdir($_location, 0777, true);
        }

        $leasing_tenant = $this->M_leasing_tenant->get($leasing_tenant_id);

        if (file_exists($leasing_tenant['full_path'])) {

            unlink($leasing_tenant['full_path']);
        }

        $_config['upload_path'] = $_location;
        $_config['allowed_types'] = '*';
        $_config['overwrite'] = TRUE;
        $_config['max_size'] = '1000000';
        $_config['file_name'] = $leasing_tenant_id;

        $this->load->library('upload', $_config);

        if (!$this->upload->do_upload('file')) {

            return $this->upload->display_errors() ?? null;
        } else {

            $photo_info['file_name'] = $this->upload->data('file_name');
            $photo_info['full_path'] = strstr($this->upload->data('full_path'), 'assets');

            $this->M_leasing_tenant->update($photo_info, $leasing_tenant_id);

            return null;
        }
    }

    private function uploadProofOfIdentityImage($leasing_tenant_proof_of_identity_id, $post_file_name)
    {
        $_location = 'assets/img/leasing/tenant_proof_of_identity';

        // Create dir if does not exist
        if (!file_exists($_location)) {

            mkdir($_location, 0777, true);
        }

        $leasing_tenant_proof_of_identity = $this->M_leasing_tenant_proof_of_identification->get($leasing_tenant_proof_of_identity_id);

        $_config['upload_path'] = $_location;
        $_config['allowed_types'] = '*';
        $_config['overwrite'] = TRUE;
        $_config['max_size'] = '1000000';
        $_config['file_name'] = $leasing_tenant_proof_of_identity['id'];

        $this->load->library('upload', $_config);
        $this->upload->initialize($_config);

        if (!$this->upload->do_upload($post_file_name)) {

            return $this->upload->display_errors() ?? null;
        } else {

            $photo_info['file_name'] = $this->upload->data('file_name');
            $photo_info['full_path'] = strstr($this->upload->data('full_path'), 'assets');

            $this->M_leasing_tenant_proof_of_identification->update($photo_info, $leasing_tenant_proof_of_identity['id']);

            return null;
        }
    }

    public function view($id = null)
    {
        if ($id) {

            $this->view_data['info'] = $this->M_leasing_tenant
                ->with_leasing_project('fields:project_name')
                ->with_company('fields:name')
                ->get($id);

            if ($this->view_data['info']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_leasing_tenant->get($id);
            if ($list) {

                $deleted = $this->M_leasing_tenant->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Leasing Project Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_leasing_tenant->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Leasing Project/s Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
}
