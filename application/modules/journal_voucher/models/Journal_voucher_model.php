<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Journal_voucher_model extends MY_Model
{

    public $table = 'journal_vouchers'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'id',
        'reference',
        'origin_id',
        'payable_type_id',
        'due_date',
        'accounting_entry_id',
        'payment_request_id',
        'date_requested',
        'project_id',
        'property_id',
        'total_amount',
        'approving_department_id',
        'approving_staff_id',
        'requesting_department_id',
        'requesting_staff_id',
        'prepared_by',
        'gross_amount',
        'total_due_amount',
        'tax_amount',
        'tax_percentage',
        'wht_amount',
        'wht_percentage',
        'net_amount',
        'payment_type_id',
        'check_id',
        'paid_amount',
        'paid_date',
        'is_paid',
        'is_complete',
        'particulars',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_many['list_items'] = array('foreign_model' => 'payable_items/payable_items_model', 'foreign_table' => 'payable_items', 'foreign_key' => 'payment_voucher_id', 'local_key' => 'id');
        
        $this->has_many['entry_items'] = array('foreign_model' => 'accounting_entry_items/accounting_entry_items_model', 'foreign_table' => 'accounting_entry_items', 'foreign_key' => 'accounting_entry_id', 'local_key' => 'accounting_entry_id');

        $this->has_one['accounting_entry'] = array('foreign_model' => 'accounting_entries/accounting_entries_model', 'foreign_table' => 'accounting_entries', 'foreign_key' => 'id', 'local_key' => 'accounting_entry_id');

        $this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');

        $this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');

        $this->has_one['approving_department'] = array('foreign_model' => 'department/department_model', 'foreign_table' => 'departments', 'foreign_key' => 'id', 'local_key' => 'approving_department_id');

        $this->has_one['requesting_department'] = array('foreign_model' => 'department/department_model', 'foreign_table' => 'departments', 'foreign_key' => 'id', 'local_key' => 'requesting_department_id');

        $this->has_one['commission'] = array('foreign_model' => 'transaction_commission/Transaction_commission_model', 'foreign_table' => 'transaction_commissions', 'foreign_key' => 'payment_voucher_id', 'local_key' => 'id');
        $this->has_one['encoder'] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'user_id', 'local_key' => 'created_by');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
