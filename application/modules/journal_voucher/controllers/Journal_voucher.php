<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Journal_voucher extends MY_Controller
{

    private $fields = [
        array(
            'field' => 'voucher[reference]',
            'label' => 'Reference',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $journal_voucher_models = array('journal_voucher/Journal_voucher_model' => 'M_journal_voucher');

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model($journal_voucher_models);
        $this->load->model('payment_request/Payment_request_model', 'M_payment_request');
        $this->load->model('item/Item_model', 'M_item');
        $this->load->model('payable_items/Payable_items_model', 'M_payable_item');
        $this->load->model('accounting_entries/Accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');
        $this->load->model('transaction_commission/Transaction_commission_model', 'M_Transaction_commission');
        $this->load->model('transaction/Transaction_model', 'M_Transaction');

        $this->load->model('company/Company_model', 'M_company');

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper
        $this->load->helper('form');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_journal_voucher->fillable;
        $this->_table_columns = $this->M_journal_voucher->__get_columns();
    }

    public function template($page = "")
    {

        $this->template->build($page);
    }

    // public function index($debug = 0)
    // {
    //     $_fills = $this->_table_fillables;
    //     $_colms = $this->_table_columns;

    //     $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
    //     $this->view_data['_columns'] = $this->__get_columns($_fills);

    //     // Get record count
    //     // $conditions['returnType'] = 'count';
    //     $this->view_data['totalRec'] = $totalRec = $this->M_journal_voucher->count_rows();

    //     // Pagination configuration
    //     $config['target'] = '#journal_voucher$journal_voucherContent';
    //     $config['base_url'] = base_url('journal_voucher$journal_voucher/paginationData');
    //     $config['total_rows'] = $totalRec;
    //     $config['per_page'] = $this->perPage;
    //     $config['link_func'] = 'PaymentVoucherPagination';

    //     // Initialize pagination library
    //     $this->ajax_pagination->initialize($config);

    //     // Get records
    //     $this->view_data['records'] = $this->M_journal_voucher
    //         ->with_requests()
    //         ->with_project()
    //         ->with_payable_type()
    //         ->with_property()
    //         ->with_approving_department()
    //         ->with_requesting_department()
    //         ->with_list_items()
    //         ->with_entry_items()
    //         ->with_entry_item()
    //         ->order_by('paid_date', 'DESC')
    //         ->limit($this->perPage, 0)
    //         ->get_all();

    //     if($debug){
    //         vdebug($this->view_data['records']);
    //     }

    //     $this->template->build('index', $this->view_data);
    // }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;
        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);
        $db_columns = $this->M_journal_voucher->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';
                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }
            $column_count = count($column);
            $cceil = ceil(($column_count / 2));
            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_journal_voucher->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);
        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'total_amount' => true,
            'particulars' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_journal_voucher
                ->with_commission()
                ->with_transaction()
                ->with_encoder('fields:first_name,last_name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("reference like '%$searchValue%'");
                $query->or_where("total_amount like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'reference' => [
                    'data' => $filters['reference'] ?? null,
                    'operator' => 'like',
                ],
                'total_amount' => [
                    'data' => $filters['total_amount'] ?? null,
                    'operator' => 'like',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_journal_voucher->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            $payable_type_id = $this->input->post('payable_type_id');
            $project_id = $this->input->post('project_id');
            $property_id = $this->input->post('property_id');
            $payee_type_id = $this->input->post('payee_type_id');
            $payee_type = $this->input->post('payee_type');
            $is_paid = $this->input->post('is_paid');
            $due_date = $this->input->post('due_date');
            $page = $this->input->post('page');

            if (!empty($due_date)) {
                $date_range = explode('-', $due_date);

                $from_str   = strtotime($date_range[0]);
                $from       = date('Y-m-d H:i:s', $from_str);

                $to_str     = strtotime($date_range[1] . '23:59:59');
                $to         = date('Y-m-d H:i:s', $to_str);
            }

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_journal_voucher->count_rows();
            //            $where = array();

            // Pagination configuration
            $config['target'] = '#journal_voucherContent';
            $config['base_url'] = base_url('journal_voucher/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'JournalVoucherPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                //                $this->db->or_like('payable_type_id', $keyword, 'both');
                //                $this->db->or_like('property_id', $keyword, 'both');
                //                $this->db->or_like('project_id', $keyword, 'both');
                //                $this->db->or_like('is_paid', $keyword, 'both');
                //                $this->db->or_like('due_date', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payable_type_id)) :
                $this->db->group_start();
                $this->db->where('payable_type_id', $payable_type_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($is_paid)) :
                $this->db->group_start();
                $this->db->where('is_paid', $is_paid, 'both');
                $this->db->group_end();
            endif;

            if (!empty($due_date)) :
                $this->db->group_start();
                $this->db->where('due_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            $totalRec = $this->M_journal_voucher->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                //                $this->db->or_like('payable_type_id', $keyword, 'both');
                //                $this->db->or_like('property_id', $keyword, 'both');
                //                $this->db->or_like('project_id', $keyword, 'both');
                //                $this->db->or_like('is_paid', $keyword, 'both');
                //                $this->db->or_like('due_date', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($payable_type_id)) :
                $this->db->group_start();
                $this->db->where('payable_type_id', $payable_type_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($is_paid)) :
                $this->db->group_start();
                $this->db->where('is_paid', $is_paid, 'both');
                $this->db->group_end();
            endif;

            if (!empty($due_date)) :
                $this->db->group_start();
                $this->db->where('due_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            $this->view_data['records'] = $records = $this->M_journal_voucher
                ->with_requests()
                ->with_project()
                ->with_payable_type()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_items()
                ->with_entry_item()
                ->order_by('paid_date', 'DESC')
                ->limit($this->perPage, $offset)
                ->get_all();

            $this->load->view('journal_voucher/_filter', $this->view_data, false);
        }
    }

    public function view($id = false, $type = '')
    {

        if ($id) {

            $this->view_data['info'] = $info = $this->M_journal_voucher
                ->with_requests()
                ->with_project()
                ->with_payable_type()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_items()
                ->with_entry_item()
                ->get($id);

            // $request_id = $info['journal_request_id'];

            // $this->view_data['requests'] = $this->M_payment_request->with_vouchers()->with_list_items()->get($request_id);

            if ($this->view_data['info']) {

                if ($type == "debug") {

                    vdebug($this->view_data);
                }

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function form($id = false, $debug = 0, $seller_id = false, $commision_amount = 0, $commissions = false)
    {
        $this->view_data['journal_voucher$journal_voucher_id'] = 0;
        $this->view_data['payment_request_id'] = 0;

        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            if ($this->form_validation->run() === true) {

                $oof = $this->input->post();

                // if (isset($oof['payment_request_id'])) {
                //     $payment_request = $this->M_payment_request->get($oof['payment_request_id']);
                // };

                // if (isset($oof['items'])) {
                //     $items = $oof['items'];
                // }
                if (isset($oof['commissions'])) {
                    $commission_ids = $oof['commissions'];
                }
                $voucher = $oof['voucher'];
                $entry_items = $oof['entry_item'];
                $accounting_entry = $oof['accounting_entry'];

                $journal_voucher_id = $oof['journal_voucher_id'];
                // vdebug($oof);
                if ($journal_voucher_id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    // $paymentVoucherID = $this->M_journal_voucher->update($request + $additional, $id);
                } else {

                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
                    $uniqid = uniqidJV();
                    // begin:Save accounting entries
                    $accounting_entry['or_number'] = 0;
                    $accounting_entry['journal_type'] = 5;
                    $accounting_entry['invoice_number'] = $uniqid;
                    $accounting_entry['remarks'] = $voucher['particulars'];
                    $accounting_entry['company_id'] = $accounting_entry['company_id'];
                    $accounting_entry['cr_total'] = $accounting_entry['dr_total'];

                    $accounting_entry_id = $this->M_Accounting_entries->insert($accounting_entry + $additional);
                    // end:Save accounting entries

                    // begin:Save payment request
                    // $request['accounting_entry_id'] = $accounting_entry_id;
                    // $payment_request = $this->M_payment_request->insert($request + $additional);
                    // $payment_request_id = $this->db->insert_id();
                    // end:Save payment request

                    if ($entry_items) {
                        foreach ($entry_items as $key => $entry) {
                            $entry_item['accounting_entry_id'] = $accounting_entry_id;
                            $entry_item['ledger_id'] = $entry['ledger_id'];
                            $entry_item['amount'] = $entry['amount'];
                            $entry_item['dc'] = $entry['dc'];
                            $entry_item['is_reconciled'] = 0;
                            $entry_item['description'] = $entry['description'];

                            $this->M_Accounting_entry_items->insert($entry_item + $additional);
                        }
                    }

                    $voucher['reference'] = $uniqid; // change to pr id helper
                    $voucher['accounting_entry_id'] = $accounting_entry_id;
                    $voucher['is_paid'] = 1;
                    $voucher['is_complete'] = 1;
                    $voucher['total_amount'] = $accounting_entry['dr_total'];

                    $journal_voucher_id = $this->M_journal_voucher->insert($voucher + $additional);

                    // if (isset($items)) {
                    //     foreach ($items as $key => $item) {
                    //         if (isset($item['item_id'])) {
                    //             $item_id = $item['item_id'];
                    //             $item_info = $this->M_item->with_tax()->get($item_id);
                    //             $list_item['journal_voucher$journal_voucher_id'] = $journal_voucher_id;
                    //             $list_item['item_id'] = $item_id;
                    //             $list_item['payable_type'] = 'PV';
                    //             $list_item['name'] = $item_info['name'];
                    //             $list_item['unit_price'] = $item_info['unit_price'];
                    //             $list_item['quantity'] = $item['quantity'];
                    //             $list_item['tax_id'] = $item['tax_id'];
                    //             $list_item['rate'] = $item_info['tax']['rate'];
                    //             $list_item['tax_amount'] = $item['tax_amount'];
                    //             $list_item['total_amount'] = $item['total_amount'];

                    //             $this->M_payable_item->insert($list_item + $additional);
                    //         }
                    //     }
                    // }

                    if (isset($commission_ids)) {
                        $commission_data = [
                            'status' => 4,
                            'updated_by' => $this->user->id,
                            'updated_at' => NOW,
                            'journal_voucher$journal_voucher_id' => $journal_voucher_id
                        ];
                        foreach ($commission_ids as $key => $value) {
                            $this->M_Transaction_commission->update($commission_data, $value);
                        }
                    }

                    $this->db->trans_complete(); # Completing journal_voucher$journal_voucher
                }

                # Check total payment
                // if($payment_request_id){

                //     $total_paid_amount = count_paid_amount($payment_request_id, 'journal_voucher$journal_vouchers', 'payment_request_id', 'paid_amount');

                //     if ($total_paid_amount == $payment_request['gross_amount'] || $total_paid_amount >= $payment_request['gross_amount']) {
                //         $this->updatePaymentRequest($payment_request_id, 1, 1);
                //     } else {
                //         $this->updatePaymentRequest($payment_request_id, 1, 0);
                //     }

                // }

                /*Optional*/
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $response['status'] = 1;
                    $response['message'] = 'Journal Voucher Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;

        if ($id) {
            // $this->view_data['journal_voucher$journal_voucher_id'] = $id;

            $this->view_data['info'] = $this->M_journal_voucher->with_approving_department()->with_requesting_department()->with_entry_item()->with_accounting_entry()->with_entry_items()->get($id);
            // $this->view_data['accounting_entries'] = $this->M_journal_voucher->->get($id);
            // $this->view_data['entry_item'] = $this->M_journal_voucher->with_entry_items()->get($id);
        }

        $this->template->build('form', $this->view_data);

        // if ($seller_id) {


        //     $this->view_data['journal_voucher$journal_voucher']['payee_type'] = "sellers";
        //     $this->view_data['journal_voucher$journal_voucher']['payee_type_id'] = $seller_id;
        //     $this->view_data['journal_voucher$journal_voucher']['gross_amount'] = $commision_amount;
        //     $this->view_data['journal_voucher$journal_voucher']['commission_amount'] = $commision_amount;

        //     $commission_arr = explode("-", $commissions);
        //     $this->view_data['commissions'] = $commission_arr;
        // }

        if ($debug) {
            vdebug($this->view_data);
        }
        $this->template->build('form', $this->view_data);
    }

    public function commission()
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->view_data['data'] = $this->M_Transaction_commission->show_rfp();

        $this->template->build('commission', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_journal_voucher->get($id);
            if ($list) {

                $deleted = $this->M_journal_voucher->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Payment Voucher successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_journal_voucher->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Payment voucher successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function updatePaymentRequest($id = false, $is_paid = 0, $is_complete = 0)
    {
        $data = array(
            'is_paid' => $is_paid,
            'is_complete' => $is_complete,
        );

        $this->M_payment_request->update($data, array('id' => $id));
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            // $this->view_data['info'] = $info = $this->M_journal_voucher->with_requests()->with_payable_items()->with_entry_items()->with_item()->as_array()->get($id);

            $this->view_data['info'] = $info = $this->M_journal_voucher->with_requests()->with_project()
                ->with_payable_type()
                ->with_property()
                ->with_approving_department()
                ->with_requesting_department()
                ->with_list_items()
                ->with_entry_items()
                ->with_entry_item()
                ->get($id);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->view_data['company'] = $this->M_company->get(1);

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }
}
