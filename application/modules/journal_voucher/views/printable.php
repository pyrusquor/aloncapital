<!DOCTYPE html>
<html>
    <head>
        <title><?php echo "PV-" . $info['reference']; ?></title>
        <!--begin::Global Theme Styles(used by all pages) -->
        <link
            href="<?=base_url();?>assets/css/demo1/style.bundle.css"
            rel="stylesheet"
            type="text/css"
        />
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->
        <link
            href="<?=base_url();?>assets/css/demo1/skins/header/base/light.css"
            rel="stylesheet"
            type="text/css"
        />
        <link
            href="<?=base_url();?>assets/css/demo1/skins/header/menu/light.css"
            rel="stylesheet"
            type="text/css"
        />
        <link
            href="<?=base_url();?>assets/css/demo1/skins/brand/dark.css"
            rel="stylesheet"
            type="text/css"
        />
        <link
            href="<?=base_url();?>assets/css/demo1/skins/aside/dark.css"
            rel="stylesheet"
            type="text/css"
        />
        <link
            rel="shortcut icon"
            href="<?=base_url();?>assets/media/logos/favicon.ico"
        />
        <!--end::Layout Skins -->
        <style>
            body {
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 13px;
                line-height: 1.42857143;
                color: #333;
                background-color: #fff;
            }

            .table {
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
                margin-bottom: 15px;
            }

            .table > thead > tr > th,
            .table > tbody > tr > td,
            .table > tbody > tr > th,
            .table > tfoot > tr > td,
            .table > tfoot > tr > th {
                padding: 5px 8px;
            }

            .table > thead > tr > th {
                border-bottom: 2px solid #00529c;
            }

            .table > tfoot > tr > td {
                border-top: 2px solid #00529c;
            }

            .table > tfoot {
                border-top: 2px solid #ddd;
            }

            .table > tbody {
                border-bottom: 1px solid #fff;
            }

            .table.table-striped > thead > tr > th {
                background: #00529c;
                color: #fff;
                border: 0;
                padding: 12px 8px;
                text-transform: uppercase;
            }

            .table.table-striped > thead > tr > th a {
                color: #fff;
                font-weight: 400;
            }

            .table.table-striped > thead > tr:nth-child(2) > th {
                background: #0075de;
            }

            .table.table-striped td {
                border: 0;
                vertical-align: middle;
            }

            .table-striped > tbody > tr:nth-of-type(odd) {
                background: #fff;
            }

            .table-striped > tbody > tr:nth-of-type(even) {
                background: #f1f1f1;
            }

            .color-bluegreen {
                color: #169f98;
            }

            .color-white {
                color: #fff;
            }

            .peso_currency {
                font-family: DejaVu Sans;
            }

            .bg-bluegreen {
                background-color: #169f98;
            }

            .text-center {
                text-align: center;
            }

            .text-left {
                text-align: left;
            }

            .text-right {
                text-align: right;
            }

            .margin0 {
                margin: 0;
            }

            .padding10 {
                padding: 10px;
            }

            p {
                margin: 0 0 15px;
            }

            .alert {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
            }

            .alert-warning {
                background-color: #fcf8e3;
                border-color: #faebcc;
                color: #8a6d3b;
            }

            input[type="text"],
            select.form-control {
                background: transparent;
                border: none;
                border-bottom: 1px solid #000000;
                -webkit-box-shadow: none;
                box-shadow: none;
                border-radius: 0;
            }

            input[type="text"]:focus,
            select.form-control:focus {
                -webkit-box-shadow: none;
                box-shadow: none;
            }
        </style>
    </head>

    <body>
        <!-- CONTENT -->
        <div
            class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
        >
            <!--begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body m-5 p-5">
                    <?php
                        // vdebug($info);
                        $payable_items = $info['list_items'];
                        $entry_items = $info['entry_items'];
                        $entry_item = $info['entry_item'];

                        $payment_type_id = $info['payment_type_id'];
                        $particulars = $info['particulars'] ? $info['particulars'] : "None";

                        $payee_email_ref = get_person($info['payee_type_id'], 'users');
                        $payee_id = get_person($info['payee_type_id'], $info['payee_type']);

                        $payee_name = get_fname($payee_id);
                        $payee_email = get_email($payee_email_ref) ? get_email($payee_email_ref) : "N/A";
                    ?>
                    <div class="d-flex justify-content-between border-bottom">
                        <div class="col-md-5">
                            <?php if (isset($company['image'])): ?>
                                <img src="<?php echo base_url(get_image('company', 'images', $company['image'])) ?>" alt="Company Logo" class="img-fluid" width="350px" height="350px" style="margin-left: -15px" />
                            <?php endif ?>
                            <h2> <?php echo $company['name'];?> </h2>
                            <h4> <?php echo $company['location'];?> </h4>
                        </div>
                        <div class="col-md-5" style="text-align: right">
                            <h2> J.V. No: <span class="text-danger"><?php echo $info['reference']; ?></span> </h2>

                        </div>
                    </div>
                    <div class="d-flex justify-content-between py-5" >
                        <div class="col-md-5">

                        </div>
                        <div class="col-md-5" style="text-align: right">
                            <h2> Date:
                                <span><?php echo view_date($info['paid_date']) ?></span>
                            </h2>
                        </div>
                    </div>
                    <div class="row py-5">
                        <div class="col-sm-6">
                            <h4>Accounting Entries</h4>
                            <div class="row">

                                <div class="col-md-12">
                                    <table class="table table-borderless">
                                        <thead>
                                            <td>Type</td>
                                            <td>Ledger Name</td>
                                            <td>Debit Amount</td>
                                            <td>Credit Amount</td>
                                        </thead>
                                        <tbody>
                                            <?php if ($entry_items): ?>
                                            <?php foreach ($entry_items as $key => $e_item) { //vdebug($e_item);?>
                                                <tr>
                                                    <td>
                                                        <?php echo ( strtolower($e_item['dc']) == "d" ? "Debit" : "Credit" );?>
                                                    </td>
                                                    <td>
                                                        <!-- Type -->
                                                        <?php echo get_value_field($e_item['ledger_id'],'accounting_ledgers','name'); ?>
                                                    </td>
                                                    <td>
                                                        <!-- Dr -->
                                                            <?php if ( strtolower($e_item['dc']) == "d" ): ?>
                                                                <?php echo money_php($e_item['amount']) ?>
                                                            <?php endif ?>
                                                    </td>
                                                        <!-- Cr -->
                                                    <td>
                                                       <?php if ( strtolower($e_item['dc']) == "c" ): ?>
                                                            <?php echo money_php($e_item['amount']) ?>
                                                        <?php endif ?>
                                                    </td>
                                               </tr>
                                            <?php } ?>
                                            <?php endif ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>Total</td>
                                                <td>&nbsp;</td>
                                                <td><?php echo money_php($entry_item['dr_total']) ?></td>
                                                <td><?php echo money_php($entry_item['cr_total']) ?></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="col-sm-1">&nbsp;</div>
                       

                        <!-- <div class="col-md-5">
                            <div class="border p-5">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h4>Total Due</h4>
                                        <h5>Total Amount</h5>
                                        <h5>Taxable</h5>
                                        <h5>Tax Breakdown</h5>
                                        <h5>Total Fees</h5>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>
                                            <?php echo money_php($info['paid_amount']) ?>
                                        </h4>
                                        <h5>
                                            <?php echo money_php($info['paid_amount']) ?>
                                        </h5>
                                        <h5><?php money_php(0)?></h5>
                                        <h5><?php money_php(0)?></h5>
                                        <h5><?php money_php(0)?></h5>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="row py-5">
                        <div class="col-sm-4 py-3 border">
                            <h4>Particulars:</h4>
                            <p><?php echo $particulars ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="small font-italic">
                                *** This voucher is computer-generated. Any
                                erasure or alteration made herein will
                                invalidate this.
                            </p>
                            <p class="small font-italic">
                                Printed by/Date: ____________________ / <?php echo view_date(date('Y-m-d')) ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
