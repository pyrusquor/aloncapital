<?php if (!empty($records)): ?>
    <div class="row" id="kt_content">
        <?php foreach ($records as $r): ?>
            <?php
            $id = ($r['id'] ? $r['id'] : "");
            $reference = ($r['reference'] ? $r['reference'] : "N/A");

            $payment_type_id = ( $r['payment_type_id'] ? Dropdown::get_static('payment_types', $r['payment_type_id'] ) : "");

            $payee_type = $r['payee_type'] ? $r['payee_type'] : 'N/A';
            
            $payee_type_id = ( $r['payee_type_id'] ? $r['payee_type_id'] : "" );

            $payee = "N/A";
            $payee_name = "N/A";

            if($payee_type_id){
                 $payee = get_person($payee_type_id, $payee_type);
                 $payee_name = get_fname($payee);
            }
          

            $paid_amount = ($r['paid_amount'] ? $r['paid_amount'] : "");
            $paid_date = ($r['paid_date'] ? view_date($r['paid_date']) : "");
            $remarks = ($r['particulars'] ? $r['particulars'] : "");

            // $b = ( $r['is_paid'] ? "kt-badge--success" : "kt-badge--danger" );

            $requests = ( isset($r['requests']) && !empty($r['requests']) ? $r['requests'] : array());

            $transaction_id = get_value_field($r['origin_id'], 'transaction_commissions', 'transaction_id', 'id');

            $this->load->model('transaction/transaction_model');

            $transaction = $this->transaction_model->with_property()->get($transaction_id);

            $payable_type_id = ($r['payable_type_id'] ? Dropdown::get_dynamic('payable_types', $r['payable_type_id'], 'name', 'id', 'view') : "");

            $payment_request_reference = ($r['reference'] ? $r['reference'] : "");
            $origin_id = ($r['origin_id'] ? $r['origin_id'] : "");

            $payment_request_id = ($r['id'] ? $r['id'] : "");

            $property_id = $r['property_id'];
            $property = $r['property']['name'];

            $project_id = $r['project']['id'];
            $project = $r['project']['name'];

            ?>
            <!--begin:: Portlet-->
            <div class="kt-portlet col-sm-12">
                <div class="kt-portlet__body custom-transaction_payments">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden">
                                <img src="./assets/media/project-logos/3.png" alt="image">
                            </div>
                            <div
                                    class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                                <?php echo get_initials($payee_name); ?>
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="<?php echo base_url(); ?>payment_voucher/view/<?php echo $id ?>"
                                       class="kt-widget__username">
                                        Reference : <?php echo $reference; ?>
                                        <i class="flaticon2-correct"></i>
                                    </a>


                                    <div class="kt-widget__action custom_portlet_header">

                                        <div class="kt-portlet__head kt-portlet__head--noborder"
                                             style="min-height: 0px;">
                                            <div class="kt-portlet__head-label">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>"
                                                           class="m-checkable delete_check"
                                                           data-id="<?php echo $r['id']; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                    <i class="flaticon-more-1 kt-font-brand"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('payment_voucher/view/' . $id); ?>"
                                                               class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon-eye"></i>
                                                                <span class="kt-nav__link-text">View</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('payment_voucher/form/0/' . $id); ?>" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-pen"></i>
                                                                <span class="kt-nav__link-text">Update</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('payment_voucher/printable/' . $id); ?>" class="kt-nav__link" target="_blank" rel="noopener noreferrer">
                                                                <i class="kt-nav__link-icon flaticon2-printer"></i>
                                                                <span class="kt-nav__link-text">Print Payment Voucher</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="kt-widget__info">

                                    <div class="kt-widget__desc">
                                        <a href="#" class="kt-widget__username">
                                            Request Type : <?php echo ucwords($payable_type_id); ?>
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>transaction/view/<?php echo $transaction_id; ?>"
                                           class="kt-widget__username">
                                            Transaction Reference : <?php echo $transaction['reference']; ?>
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>project/view/<?php echo $project_id; ?>"
                                           target="_BLANK" class="kt-widget__username">
                                            Project : <?=$project;?>
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>property/view/<?php echo $property_id; ?>"
                                           target="_BLANK" class="kt-widget__username">
                                            Property : <?=$property;?>
                                        </a>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-widget__bottom">

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Payment Type</span>
                                    <span class="kt-widget__value"><?php echo $payment_type_id; ?></span>
                                </div>
                            </div>
                            
                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Payee Type</span>
                                    <span class="kt-widget__value"><?php echo ucwords($payee_type); ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Payee</span>
                                    <span class="kt-widget__value"><?php echo ucwords($payee_name); ?></span>
                                </div>
                            </div>


                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Paid Date</span>
                                    <span class="kt-widget__value"><?php echo view_date($paid_date); ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Paid Amount</span>
                                    <span class="kt-widget__value"><?php echo money_php($paid_amount); ?></span>
                                </div>
                            </div>
                            
                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Remarks</span>
                                    <span class="kt-widget__value"><?php echo $remarks; ?></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach;?>
    </div>

    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="<?php echo base_url('payment_voucher/form'); ?>"
                               class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>