<?php
// $info_id = isset($info_id) && $info_id ? $info_id : '';

$reference = isset($info['reference']) && $info['reference'] ? $info['reference'] : '';
$payable_type_id = isset($info['payable_type_id']) && $info['payable_type_id'] ? $info['payable_type_id'] : '';
$origin_id = isset($info['origin_id']) && $info['origin_id'] ? $info['origin_id'] : '';
$date_requested = isset($info['date_requested']) && $info['date_requested'] ? $info['date_requested'] : date("Y-m-d");
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
$property_id = isset($info['property_id']) && $info['property_id'] ? $info['property_id'] : '';
$approving_department_id = isset($info['approving_department_id']) && $info['approving_department_id'] ? $info['approving_department_id'] : '';
$approving_staff_id = isset($info['approving_staff_id']) && $info['approving_staff_id'] ? $info['approving_staff_id'] : '';
$requesting_department_id = isset($info['requesting_department_id']) && $info['requesting_department_id'] ? $info['requesting_department_id'] : '';
$requesting_staff_id = isset($info['requesting_staff_id']) && $info['requesting_staff_id'] ? $info['requesting_staff_id'] : '';
$prepared_by = isset($info['prepared_by']) && $info['prepared_by'] ? $info['prepared_by'] : '';
$due_date = isset($info['due_date']) && $info['due_date'] ? $info['due_date'] : date("Y-m-d");
$particulars = isset($info['particulars']) && $info['particulars'] ? $info['particulars'] : '';

// $payable_name = isset($info['payable_type']['name']) && $info['payable_type']['name'] ? $info['payable_type']['name'] : 'N/A';
$payable_ref_id = get_person($payable_type_id, 'payable_types');
$payable_name = get_name($payable_ref_id) ? get_name($payable_ref_id) : "N/A";

$department_name = isset($info['department']['name']) && $info['department']['name'] ? $info['department']['name'] : 'N/A';
$staff_name = isset($info['staff']['name']) && $info['staff']['name'] ? $info['staff']['name'] : 'N/A';
$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : 'N/A';
$property_name = isset($info['property']['name']) && $info['property']['name'] ? $info['property']['name'] : 'N/A';
$approving_department_name = isset($info['approving_department']['name']) && $info['approving_department']['name'] ? $info['approving_department']['name'] : 'N/A';
$requesting_department_name = isset($info['requesting_department']['name']) && $info['requesting_department']['name'] ? $info['requesting_department']['name'] : 'N/A';

if (isset($approving_staff_id) && !empty($approving_staff_id)) {
    $approving_staff_info = get_person($approving_staff_id, 'staff');
    $approving_staff_name = get_fname($approving_staff_info);
}

if (isset($prepared_by) && !empty($prepared_by)) {
    $prepared_by_info = get_person($prepared_by, 'staff');
    $prepared_by_name = get_fname($prepared_by_info);
}

if (isset($requesting_staff_id) && !empty($requesting_staff_id)) {
    $requesting_staff_id_info = get_person($requesting_staff_id, 'staff');
    $requesting_staff_name = get_fname($requesting_staff_id_info);
}

?>

<?php if (empty($payment_request)) : ?>
    <input type="hidden" name="journal_voucher_id" id ="journal_voucher_id"value="<?= $info['id']; ?>">

    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Origin</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control" placeholder="Origin" id="origin_id" name="voucher[origin_id]" value="<?php echo set_value('origin_id"', @$origin_id); ?>" autocomplete="off">
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-bookmark"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Payable Type <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="payable_types" id="payable_type_id" name="voucher[payable_type_id]">
                            <option value="">Select Payable Type</option>
                            <?php if ($payable_name) : ?>
                                <option value="<?php echo $payable_type_id; ?>" selected><?php echo $payable_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Project Name <span class="kt-font-danger"></span></label>
                        <select class="form-control suggests" data-module="projects" id="project_id" name="voucher[project_id]">
                            <option value="">Select Project</option>
                            <?php if ($project_name) : ?>
                                <option value="<?php echo $project_id; ?>" selected><?php echo $project_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Property Name <span class="kt-font-danger"></span></label>
                        <select class="form-control suggests" data-module="properties" id="property_id" name="voucher[property_id]">
                            <option value="">Select Property</option>
                            <?php if ($property_name) : ?>
                                <option value="<?php echo $property_id; ?>" selected><?php echo $property_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Date of Request <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control kt_datepicker" placeholder="Date of Request" name="voucher[date_requested]" value="<?php echo set_value('date_requested"', @$date_requested); ?>" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Request Due Date <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control kt_datepicker" placeholder="Request Due Date" name="voucher[due_date]" value="<?php echo set_value('due_date"', @$due_date); ?>" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Approving Department <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="departments" id="approving_department_id" name="voucher[approving_department_id]">
                            <option value="">Select Property</option>
                            <?php if ($approving_department_name) : ?>
                                <option value="<?php echo $approving_department_id; ?>" selected><?php echo $approving_department_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>


                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id" name="voucher[approving_staff_id]">
                            <option value="">Select Property</option>
                            <?php if ($approving_staff_name) : ?>
                                <option value="<?php echo $approving_staff_id; ?>" selected><?php echo $approving_staff_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>


            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Requesting Department <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="departments" id="requesting_department_id" name="voucher[requesting_department_id]">
                            <option value="">Select Department</option>
                            <?php if ($requesting_department_name) : ?>
                                <option value="<?php echo $requesting_department_id; ?>" selected><?php echo $requesting_department_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="requesting_staff_id" name="voucher[requesting_staff_id]">
                            <option value="">Select Staff</option>
                            <?php if ($requesting_staff_name) : ?>
                                <option value="<?php echo $requesting_staff_id; ?>" selected><?php echo $requesting_staff_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>



            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="">Prepared by <span class="kt-font-danger">*</span></label>
                        <select class="form-control suggests" data-module="staff" data-type="person" id="prepared_by" name="voucher[prepared_by]">
                            <option value="">Select Staff</option>
                            <?php if ($prepared_by_name) : ?>
                                <option value="<?php echo $prepared_by; ?>" selected><?php echo $prepared_by_name; ?></option>
                            <?php endif ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="message">Particulars</label>
                        <textarea type="text" class="form-control" id="particulars" name="voucher[particulars]" placeholder="Particulars" rows="7"><?php echo set_value('particulars', @$particulars); ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php else : ?>

<?php endif; ?>