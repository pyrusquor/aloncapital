<div class="card my-4" v-if="items.form.length > 0">
    <div class="card-header">
        <h3 class="card-title">Items to Issue</h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Unit Cost</th>
                    <th>MRR</th>
                    <th>Quantity</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, idx) in items.form">
                    <td>{{ item.ref.item.name }}</td>
                    <td>{{ item.ref.unit_cost }}</td>
                    <td>{{ item.material_receiving_ref  }}</td>
                    <td>{{ item.quantity }}</td>
                    <td>
                        <a class="text-danger" href="#!" @click.prevent="removeItemFromCart(idx)">Remove</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-sm-6">
                <strong>Total Qty:</strong>
                {{ items.quantity }}
            </div>
            <div class="col-sm-6">
                <strong>Total Amount</strong>
                {{ items.total }}
            </div>
        </div>
    </div>
</div>