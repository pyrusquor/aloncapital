<div class="row">
    <div class="container-fluid">
        <div class="form-group">
            <div class="label">Warehouse</div>
            <select class="form-control suggests" data-module="warehouses" id="warehouse_id" name="filter_warehouse">
                <option>Select</option>
            </select>
        </div>
        <div class="row">
            <div class="form-group col-sm-12 col-md-4">
                <div class="label">Issued To:</div>
                <input type="text" class="form-control" v-model="info.form.issued_to">
            </div>

            <div class="col-sm-12 col-md-4 form-group">
                <div class="label">Issued To Others</div>
                <select class="form-control suggests" data-module="staff" data-type="person" id="issued_to_others" name="request[issued_to_others]">
                    <?php if (isset($issued_to_others_staff_object)) : ?>
                        <option value="<?= $issued_to_others_staff_object['id'] ?>" selected><?= $issued_to_others_staff_object['last_name'] . " " . $issued_to_others_staff_object['first_name'] ?></option>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group col-sm-12 col-md-4">
                <div class="label">Remarks:</div>
                <input type="text" class="form-control" v-model="info.form.remarks">
            </div>
        </div>
    </div>
</div>
<div class="row" v-if="info.form.warehouse_id">
    <div class="container-fluid" v-if="material_requests.show_filter">
        <h4>Filter Material Requests</h4>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Material Request</div>
                    <input type="text" class="form-control" v-model="material_requests.filter.reference">
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Project</div>
                    <select class="form-control suggests" data-module="projects" id="filter_project" name="filter_project" data-key="project_id">
                        <option>Select</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Sub Project</div>
                    <select v-model="material_requests.filter.sub_project_id" class="form-control" v-if="sources.sub_projects.length > 0">
                        <option></option>
                        <option v-for="obj in sources.sub_projects" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                    <div v-else>
                        <select class="form-control">
                            <option>No sub-projects for this project &hellip;</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Property</div>
                    <select v-model="material_requests.filter.property_id" class="form-control" v-if="sources.properties.length > 0">
                        <option></option>
                        <option v-for="obj in sources.properties" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                    <div v-else>
                        <select class="form-control">
                            <option>No properties for this project &hellip;</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Amenity</div>
                    <select class="form-control suggests" data-module="amenities" id="filter_amenity" name="filter_amenity" data-key="amenity_id">
                        <option>Select</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Sub Amenity</div>
                    <select v-model="material_requests.filter.sub_amenity_id" class="form-control" v-if="sources.sub_amenities.length > 0">
                        <option></option>
                        <option v-for="obj in sources.sub_amenities" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                    <div v-else>
                        <select class="form-control">
                            <option>No sub-amenities for this amenity &hellip;</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Department</div>
                    <select class="form-control suggests" data-module="departments" id="filter_department" name="filter_department" data-key="department_id">
                        <option>Select</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Vehicle/Equipment</div>
                    <select class="form-control suggests" data-module="vehicle_equipments" id="filter_vehicle_equipment" name="filter_vehicle_equipment" data-key="vehicle_equipment_id">
                        <option>Select</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="form-group">
            <a class="btn btn-brand kt-font-bold kt-font-transform-u" href="#!" @click.prevent="fetchMaterialRequests()">
                Filter
            </a>
        </div>

        <div class="table-responsive">
            <div class="spinner-border text-success" role="status" v-if="material_requests.loading">
                <span class="sr-only">Loading...</span>
            </div>
            <table class="table table-condensed" v-else>
                <thead>
                    <tr>
                        <th>Request Date</th>
                        <th>Reference</th>
                        <th>Status</th>
                        <th>Approving Staff</th>
                        <th>Requesting Staff</th>
                        <th>Items</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(mr, key) in material_requests.results">
                        <td>{{ mr.request_date }}</td>
                        <td>{{ mr.reference }}</td>
                        <td>{{ lookupMRRStatus(mr.request_status) }}</td>
                        <td>{{ mr.approving_staff_display_name }}</td>
                        <td>{{ mr.requesting_staff_display_name }}</td>
                        <td>
                            <a class="btn btn-sm btn-success" href="#!" @click.prevent="fetchMaterialRequestItems(mr.id);fetchMaterialRequestItemsSelection(mr.id, key)">Load</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="spinner-border text-success" role="status" v-if="material_requests.items_loading">
        <span class="sr-only">Loading...</span>
    </div>
    <div class="container-fluid" v-if="material_requests.selected">
        <h4>Material Request Items</h4>
        <a href="#!" class="btn btn-secondary btn-sm mr-2" @click.prevent="material_requests.show_items_ref = !material_requests.show_items_ref">
            Toggle Material Request Details
        </a>
        <a href="#!" class="btn btn-primary btn-sm" @click.prevent="resetMaterialRequestItems()">&laquo; Back</a>

        <div class="card my-3" v-show="material_requests.show_items_ref">
            <div class="card-header">
                <h4 class="card-title">Material Request Reference</h4>
            </div>
            <div class="card-body">
                <h5>
                    <a :href="material_requests.selected_url">
                        {{ material_requests.selected.reference }}
                    </a>
                </h5>
                <div class="table-response mb-2">
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Expense Account</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ material_requests.selected.request_date }}</td>
                                <td v-if="material_requests.selected.accounting_ledger">{{ material_requests.selected.accounting_ledger.name }}</td>
                                <td v-else>No Expense Account specified</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive mb-2">
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Quantity Requested</th>
                                <th>Quantity Issued</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="ir in material_requests.items_ref">
                                <td>{{ ir.item.name }}</td>
                                <td>{{ ir.quantity }}</td>
                                <td>{{ ir.quantity_issued }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row" v-if="material_requests.items.length > 0 && material_requests.items_form.length > 0">
            <div v-for="(mr_item, key) in material_requests.items" class="container-fluid">
                <div class="col-sm-12 mt-4" v-if="key <= material_requests.items_meta.end_count && key >= material_requests.items_meta.start_count && mr_item.quantity != 0">
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-sm-1">
                                <a href="#!" class="btn btn-sm btn-clean btn-icon btn-icon-md" @click.prevent="toggleDetail(key)">
                                    <i class="la la-eye"></i>
                                </a>
                            </div>
                            <div class="col-sm-2">
                                <small>Item</small><br>
                                {{ mr_item.item.name }}
                            </div>
                            <div class="col-sm-2">
                                <small>Reference</small><br>
                                {{ mr_item.material_receiving.reference }}
                            </div>
                            <div class="col-sm-2">
                                <small>Date</small><br>
                                {{ mr_item.material_receiving.created_at }}
                            </div>
                            <div class="col-sm-1">
                                <small>Available</small><br>
                                {{ mr_item.stock_data.remaining }}
                            </div>
                            <div class="col-sm-1">
                                <small>Unit Cost</small><br>
                                {{ mr_item.unit_cost }}
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <input class="form-control" type="number" step="0.01" min="0" v-model="material_requests.items_form[key].quantity" :max="mr_item.stock_data.remaining" placeholder="Quantity to Issue">
                                    </div>
                                    <div class="col-sm-4">
                                        <a class="ml-2 btn btn-sm btn-primary" href="#!" @click.prevent="addToItemsCart(key)">Add</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" v-if="material_requests.items_meta.show_detail == key">
                        <div class="card-header">
                            <h4 class="card-title">
                                {{ mr_item.item.name }}
                            </h4>
                        </div>
                        <div class="card-body">
                            <h4>Material Receiving Data</h4>
                            <div class="row mb-3">
                                <div class="col-sm-6 col-md-3 mb-4">
                                    <h6>Reference</h6>
                                    {{ mr_item.material_receiving.reference }}
                                </div>
                                <div class="col-sm-6 col-md-3 mb-4">
                                    <h6>Date</h6>
                                    {{ mr_item.material_receiving.created_at }}
                                </div>
                                <div class="col-sm-6 col-md-3 mb-4">
                                    <h6>Initial Quantity</h6>
                                    {{ mr_item.quantity }}
                                </div>
                                <div class="col-sm-6 col-md-3 mb-4">
                                    <h6>Unit Cost</h6>
                                    {{ mr_item.unit_cost }}
                                </div>
                                <div class="col-sm-6 col-md-3 mb-4">
                                    <h6>Total Cost</h6>
                                    {{ mr_item.material_receiving.total_cost }}
                                </div>
                                <div class="col-sm-6 col-md-3 mb-4">
                                    <h6>Landed Cost</h6>
                                    {{ mr_item.material_receiving.landed_cost }}
                                </div>
                                <div class="col-sm-6 col-md-3 mb-4">
                                    <h6>Freight</h6>
                                    {{ mr_item.material_receiving.freight_amount }}
                                </div>
                                <div class="col-sm-6 col-md-3 mb-4">
                                    <h6>Input Tax</h6>
                                    {{ mr_item.material_receiving.input_tax }}
                                </div>
                            </div>

                            <h4>Item Data</h4>
                            <div class="row mb-3">
                                <div class="col-sm-6 col-md-2 mb-4" v-if="mr_item.item_group">
                                    <h6>Group</h6>
                                    {{ mr_item.item_group.name }}
                                </div>
                                <div class="col-sm-6 col-md-2 mb-4" v-if="mr_item.item_type">
                                    <h6>Type</h6>
                                    {{ mr_item.item_type.name }}
                                </div>
                                <div class="col-sm-6 col-md-2 mb-4" v-if="mr_item.item_brand">
                                    <h6>Brand</h6>
                                    {{ mr_item.item_brand.name }}
                                </div>
                                <div class="col-sm-6 col-md-2 mb-4" v-if="mr_item.item_class">
                                    <h6>Class</h6>
                                    {{ mr_item.item_class.name }}
                                </div>
                                <div class="col-sm-6 col-md-2 mb-4">
                                    <h6>Available Quantity</h6>
                                    {{ mr_item.stock_data.remaining }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid my-4">
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" role="status" aria-live="polite">
                            Showing {{ material_requests.items_meta.start_count + 1 }} to {{ material_requests.items_meta.end_count + 1 }} of {{ material_requests.items_meta.num_entries }} entries
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_full_numbers">
                            <ul class="pagination">
                                <li class="paginate_button page-item first" v-if="material_requests.items_meta.current_page > 0">
                                    <a href="#" class="page-link" @click.prevent="material_requests.items_meta.current_page = 0">
                                        <i class="la la-angle-double-left"></i>
                                    </a>
                                </li>
                                <li class="paginate_button page-item previous" v-if="material_requests.items_meta.current_page > 0">
                                    <a href="#" class="page-link" @click.prevent="material_requests.items_meta.current_page = material_requests.items_meta.current_page - 1">
                                        <i class="la la-angle-left"></i>
                                    </a>
                                </li>
                                <li class="paginate_button page-item" v-for="n in material_requests.items_meta.num_pages">
                                    <a href="#" class="page-link" @click.prevent="material_requests.items_meta.current_page = n - 1">
                                        {{ n }}
                                    </a>
                                </li>
                                <li class="paginate_button page-item next" v-if="material_requests.items_meta.current_page < material_requests.items_meta.num_pages - 1">
                                    <a href="#" class="page-link" @click.prevent="material_requests.items_meta.current_page = material_requests.items_meta.current_page + 1">
                                        <i class="la la-angle-right"></i>
                                    </a>
                                </li>
                                <li class="paginate_button page-item last" v-if="material_requests.items_meta.current_page < material_requests.items_meta.num_pages - 1">
                                    <a href="#" class="page-link" @click.prevent="material_requests.items_meta.current_page = material_requests.items_meta.num_pages - 1">
                                        <i class="la la-angle-double-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $this->load->view('modals/form__select_item'); ?>