<div class="modal modal-wide fade" id="selectItemModal" tabindex="-1" role="dialog" aria-labelledby="selectItemModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-wide" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="selectItemModalLabel">Select Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Select</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>