<script type="text/javascript">
    window.object_id = "<?=$id;?>";
    window.current_user_id = "<?php current_user_id();?>";
</script>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Material Issuance</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="
                    <?php echo site_url('material_issuance/form/' . $data['id']); ?>"
                   class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('material_issuance'); ?>"
                   class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->

<div id="material_issuance_app" class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <?php $this->load->view('modals/note_form');?>
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-widget13">
                        
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Reference
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">                            
                                <?php echo $data['reference']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Type
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo issuance_type_lookup($data['issuance_type']); ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Warehouse
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['warehouse']['name']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Issued To
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['issued_to']?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Material Request
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php if ($data['material_request_id']): ?>
                                    <?php echo $data['material_request']['reference']; ?>
                                <?php else: ?>
                                    No associated Material Request
                                <?php endif; ?>
                            </span>
                        </div>
                        <?php if ($data['issuance_type'] == 3): ?>
                            <div class="kt-widget13__item">
                                <span class="kt-widget13__desc">
                                    Source Warehouse
                                </span>
                                <span class="kt-widget13__text kt-widget13__text--bold">
                                    <?php echo get_object_from_table($log['source_warehouse_id'], 'warehouses', false)['name']; ?>
                                </span>
                            </div>
                            <div class="kt-widget13__item">
                                <span class="kt-widget13__desc">
                                    Destination Warehouse
                                </span>
                                <span class="kt-widget13__text kt-widget13__text--bold">
                                    <?php echo get_object_from_table($log['destination_warehouse_id'], 'warehouses', false)['name']; ?>
                                </span>
                            </div>
                            <div class="kt-widget13__item">
                                <span class="kt-widget13__desc">
                                    Transfer Type
                                </span>
                                <span class="kt-widget13__text kt-widget13__text--bold">
                                    <?php echo $log['remarks'];?>
                                </span>
                            </div>
                        <?php endif; ?>
                        
                        <!-- ==================== end: Add model details ==================== -->
                    </div>
                    <!--end::Portlet-->
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Created
                                    </span>
                        <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['created_at']; ?> by <?php echo get_person_name($data['created_by'], 'staff'); ?></span>
                    </div>
                    <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Updated
                                    </span>
                        <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['updated_at']; ?> by <?php echo get_person_name($data['updated_by'], 'staff'); ?></span>
                    </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head py-4">
                    <h4>Notes</h4>
                    <a href="#!" @click.prevent="addNote()" class="btn btn-sm btn-success float-right">
                        Add Note
                    </a>
                </div>
                <div class="kt-portlet__body">
                    <div class="spinner-border text-success" role="status" v-if="notes.loading">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <div v-if="notes.data.length > 0">
                        <div class="card mb-2" v-for="(n, idx) in notes.data">
                            <div class="card-header">
                                <h6>{{ n.creator.first_name }} {{ n.creator.last_name }} <small>on {{ n.created_at }}</small></h6>
                            </div>
                            <div class="card-body">
                                {{ n.content }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Items
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="table-responsive">
                                <table class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Unit Cost</th>
                                        <th>Origin</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($items as $item): ?>
                                        <tr>
                                            <td><?= $item['mri']['item']['name']; ?></td>
                                            <td><?= $item['quantity']; ?></td>
                                            <td><?= money_php($item['mri']['unit_cost']); ?></td>
                                            <td>
                                                <a href="<?php echo site_url('material_receiving/view/' . $item['mri']['material_receiving_id']); ?>">
                                                    <?php echo $item['mri']['material_receiving']['reference']; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-danger" @click="cancelItem(<?= $item['id'] ?>, <?= $item['quantity'] ?>)">Cancel</button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Footer -->
