<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Material_issuance_model extends MY_Model
{
    public $table = 'material_issuances'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        "id",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
        "material_request_id",
        "warehouse_id",
        "reference",
        "issuance_type",
        "issued_to",
        "issued_to_others",
        "remarks"

    ];
    public $form_fillables = [
        "id",
        "reference",
        "material_request_id",
        "warehouse_id",
        "issuance_type",
        "issued_to",
        "remarks",
        "issued_to_others"

    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [


        "material_request_id" => array(
            "field" => "material_request_id",
            "label" => "Material Request",
            "rules" => "numeric|required"
        ),

    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;


        $this->has_one["material_request"] = array('foreign_model' => 'material_request/material_request_model', 'foreign_table' => 'material_requests', 'foreign_key' => 'id', 'local_key' => 'material_request_id');
        $this->has_one["warehouse"] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouses', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');
        $this->has_one["issued_to_others_staff"] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staffs', 'foreign_key' => 'id', 'local_key' => 'issued_to_others');
        $this->has_many["items"] = array('foreign_model' => 'material_issuance_item/material_issuance_item_model', 'foreign_table' => 'material_issuances', 'foreign_key' => 'material_issuance_id', 'local_key' => 'id');
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
