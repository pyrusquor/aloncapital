<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Material_receiving_model extends MY_Model
    {
        public $table = 'material_receivings'; // you MUST mention the table name
        public $primary_key = 'id';
        public $fillable = [
            'id',
            'created_by',
            'created_at',
            'updated_at',
            'updated_by',
            'deleted_by',
            'deleted_at',
            'reference',
            'status',
            'purchase_order_id',
            'warehouse_id', // get from PO if available
            'supplier_id', // get from PO - readonly
            'accounting_ledger', // get from PO - readonly
            'freight_amount',
            'landed_cost', // PO total amount
            'total_cost', // freight + landed_cost - readonly
            'input_tax', // 12% of total_cost - readonly
            'receiving_type'
        ];
        public $form_fillables = [
            'id',
            'reference',
            'status',
            'purchase_order_id',
            'warehouse_id', // get from PO if available
            'supplier_id', // get from PO - readonly
            'accounting_ledger', // get from PO - readonly
            'freight_amount',
            'landed_cost', // PO total amount
            'total_cost', // freight + landed_cost - readonly
            'input_tax', // 12% of total_cost - readonly
            'receiving_type'
        ];
        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [
            /* ==================== begin: Add model fields ==================== */

            /* ==================== end: Add model fields ==================== */
        ];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = true;
            $this->return_as = 'array';

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;

            // for relationship tables
            $this->has_one['accounting_ledger'] = array('foreign_model' => 'accounting_ledgers/accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'accounting_ledger_id');
            $this->has_one['purchase_order'] = array('foreign_model' => 'purchase_order/purchase_order_model', 'foreign_table' => 'purchase_orders', 'foreign_key' => 'id', 'local_key' => 'purchase_order_id');
            $this->has_one['supplier'] = array('foreign_model' => 'suppliers/suppliers_model', 'foreign_table' => 'suppliers', 'foreign_key' => 'id', 'local_key' => 'supplier_id');
            $this->has_one['warehouse'] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouse', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');
        }

        function get_columns()
        {
            $_return = FALSE;

            if ($this->fillable) {
                $_return = $this->fillable;
            }

            return $_return;
        }

        public function insert_dummy()
        {
            require APPPATH . '/third_party/faker/autoload.php';
            $faker = Faker\Factory::create();

            $data = [];

            for ($x = 0; $x < 10; $x++) {
                array_push($data, array(
                    'name' => $faker->word,
                ));
            }
            $this->db->insert_batch($this->table, $data);

        }
    }