<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Material Receiving</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <!--                <a href="-->
                <?php //echo site_url('material_receiving/update/'.$id);
                ?>
                <!--" class="btn btn-label-success btn-elevate btn-sm">-->
                <!--                    <i class="fa fa-edit"></i> Edit-->
                <!--                </a>-->
                <a href="<?php echo site_url('material_receiving'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="material_receiving_app">
    <div class="row">
        <div class="col-md-3">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-widget13">
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Reference
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['reference']; ?></span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Status
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo mrr_status_lookup($data['status']); ?></span>
                        </div>
                        <!-- ==================== begin: Add fields details  ==================== -->
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Purchase Order
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['purchase_order']['reference']; ?></span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Supplier
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['supplier']['name']; ?></span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Landed Cost
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($data['landed_cost']); ?></span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Freight
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($data['freight_amount']); ?></span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Total Cost
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($data['total_cost']); ?></span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Input Tax
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($data['input_tax']); ?></span>
                        </div>
                        <!-- ==================== end: Add model details ==================== -->
                    </div>
                    <!--end::Portlet-->
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-widget13__item">
                        <span class="kt-widget13__desc">
                            Created
                        </span>
                        <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['created_at']; ?> by <?php echo get_person_name($data['created_by'], 'staff'); ?></span>
                    </div>
                    <div class="kt-widget13__item">
                        <span class="kt-widget13__desc">
                            Updated
                        </span>
                        <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['updated_at']; ?> by <?php echo get_person_name($data['updated_by'], 'staff'); ?></span>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-9">
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Items
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="table-responsive">
                                <table class="table table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Stock Remaining</th>
                                            <th>Status</th>
                                            <th>Unit Cost</th>
                                            <th>Total</th>
                                            <th>Freight Cost</th>
                                            <th>Issuances</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($items as $item) : ?>
                                            <tr>
                                                <td><?= $item['item']['name']; ?></td>
                                                <td><?= $item['quantity']; ?></td>
                                                <td><?= $item['stock_data']['remaining']; ?></td>
                                                <td><?= mrr_item_status_lookup($item['status']); ?></td>
                                                <td><?= money_php($item['unit_cost']); ?></td>
                                                <td><?= money_php($item['total_cost']); ?></td>
                                                <td><?= money_php($item['freight_cost']); ?></td>
                                                <td>
                                                    <a href="#!" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="modal" data-target="#itemModal_<?php echo $item['id']; ?>">
                                                        <i class="la la-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>

                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php foreach ($items as $item) : ?>
        <div class="modal fade" id="itemModal_<?php echo $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="itemModal_<?php echo $item['id']; ?>_Label" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="itemModal_<?php echo $item['id']; ?>_Label">Issuances</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>Material Issuance</th>
                                    <th>Type</th>
                                    <th>Date</th>
                                    <th>Staff</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($item['issuances']) : ?>
                                    <?php foreach ($item['issuances'] as $issuance) : ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo site_url('material_issuance/view/' . $issuance['material_issuance_id']); ?>">
                                                    <?php echo $issuance['material_issuance']['reference']; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <?php echo issuance_type_lookup($issuance['material_issuance']['issuance_type']); ?>
                                            </td>
                                            <td>
                                                <?php echo $issuance['material_issuance']['created_at']; ?>
                                            </td>
                                            <td>
                                                <?php echo get_person_name($issuance['material_issuance']['created_by'], 'staff'); ?>
                                            </td>
                                            <td>
                                                <?php echo $issuance['quantity']; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<!-- begin:: Footer -->