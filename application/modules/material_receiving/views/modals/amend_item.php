<div class="modal fade" id="amend_item_modal" tabindex="-1" role="dialog"
     aria-labelledby="amend_item_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" v-if="items.selected">
            <div class="modal-header">
                <h5 class="modal-title">Amend Item: {{ items.selected.item.name }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Quantity</label>
                    <input type="number" min="0" class="form-control" v-model="items.selected.quantity">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" @click.prevent="amendItem()">Submit</button>
                <button type="button" class="btn btn-secondary">Close</button>
            </div>
        </div>
    </div>
</div>