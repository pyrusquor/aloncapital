<div v-if="! this.po.selected">
    <h3>Filter Purchase Orders</h3>
    <div class="row">
        <div class="col-md-6">
            <label class="">Purchase Order <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="purchase_orders" id="filter_purchase_order_id" data-select="reference" name="filter_purchase_order_id">
                <?php if ($purchase_order) : ?>
                    <option value="<?php echo $purchase_order['id']; ?>" selected><?php echo $purchase_order['reference']; ?></option>
                <?php endif ?>
            </select>
            <p class="text-secondary">
                <small>Only Approved or Amended Purchase Orders will be displayed</small>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Company</label>
                <select class="form-control suggests" data-module="companies" id="filter_company_id">
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Created At <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input type="text" class="form-control kt_datepicker" placeholder="Created At" name="request[request_date]" id="filter_request_date" readonly>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                </div>
            </div>
        </div>
        <div class="col-12 my-3">
            <div class="form-group">
                <div class="btn-group btn-group-sm">
                    <button class="btn btn-primary mr-2" @click.prevent="fetchPO(null)">Filter</button>
                    <button class="btn btn-secondary" @click.prevent="resetPOFilter()">Clear</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div v-else style="margin: 20px 0;">
    <h3>Purchase Order: {{ po.selected.reference }}</h3>
    <p>Supplier: {{ po.selected.supplier.name }}</p>
    <a class="btn btn-secondary btn-sm" href="<?php echo site_url('material_receiving/form'); ?>">
        &laquo; Reset
    </a>
</div>
<div class="spinner-border text-success" role="status" v-if="po.loading">
    <span class="sr-only">Loading...</span>
</div>
<div class="row" v-if="po.data.length > 0 && !po.selected">
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Reference</th>
                    <th>Supplier</th>
                    <th>Company</th>
                    <th>Amount</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(r, r_key) in po.data">
                    <td>
                        {{ r.reference }}
                    </td>
                    <td>
                        {{ r.supplier.name }}
                    </td>
                    <td>
                        {{ r.company.name ?? '' }}
                    </td>
                    <td>
                        {{ r.total }}
                    </td>
                    <td>
                        {{ r.created_at }}
                    </td>
                    <td>
                        <a class="btn-success btn btn-sm" href="#!" @click.prevent="selectPO(r_key, true);">
                            Select
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row" v-if="po.selected && ! info.is_active">
    <div class="col-sm-12">
        <div class="table-responsive" v-show="po.items.length > 0">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Brand</th>
                        <th>Quantity</th>
                        <th>Unit/Total Cost</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(po_item, po_item_key) in po.items">
                        <td>
                            {{ po_item.item_name }}
                        </td>
                        <td>
                            {{ po_item.item_brand_name }}
                        </td>
                        <td>
                            {{ po_item.quantity }}
                        </td>
                        <td>
                            {{ po_item.unit_cost }} /
                            {{ po_item.total_cost }}
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3" style="text-align: right;">Total</th>
                        <th>{{ po.selected.total }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="form-group my-3">
        <div class="spinner-border text-success" role="status" v-if="items.loading">
            <span class="sr-only">Loading...</span>
        </div>
        <div v-else>
            <a class="btn btn-success" href="#!" @click.prevent="addItemsToMaterialReceiving()" v-if="po.items.length > 0">
                Create a Material Receipt with this Purchase Order
            </a>
            <div class="p-4 alert alert-warning" v-else>
                There are no outstanding items in this PO
            </div>
        </div>
    </div>
</div>