<div v-show="items.data.length > 0 && info.is_active" class="my-3">
    <h3>Material Receiving</h3>
    <div class="mb-4">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group py-2">
                    <label>Status <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('status', Dropdown::get_static("mrr_status"), null, 'class="form-control form-control-sm _filter" v-model="info.form.status"') ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Purchase Order</label>
                    <p v-if="po.selected">
                        <strong>{{ po.selected.reference }}</strong>
                    </p>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Supplier</label>
                    <p v-if="po.selected">
                        <strong>{{ po.selected.supplier.name }}</strong>
                    </p>
                </div>
            </div>
            <?php /*
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Expense Account</label>
                    <p v-if="po.selected">
                        <strong>{{ po.selected.accounting_ledger.name }}</strong>
                    </p>
                </div>
            </div>
            */ ?>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Warehouse <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="warehouse_id" name="warehouse_id" v-model="info.form.warehouse_id">
                        <option v-for="w in sources.warehouses" :value="w.id">
                            {{ w.name }}
                        </option>
                    </select>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <label>Landed Cost</label>
                <input type="number" min="0" step=".01" disabled v-model="info.form.landed_cost" class="form-control">
            </div>
            <div class="col-sm-6 col-md-3">
                <label>Freight Amount</label>
                <input type="number" min="0" step=".01" v-model="info.form.freight_amount" class="form-control">
            </div>
            <div class="col-sm-6 col-md-3">
                <label>Total Cost</label>
                <input type="number" min="0" step=".01" disabled v-model="info.form.total_cost" class="form-control">
            </div>
            <div class="col-sm-6 col-md-3">
                <label>Input Tax</label>
                <input type="number" min="0" step=".01" disabled v-model="info.form.input_tax" class="form-control">
            </div>
        </div>
    </div>

    <h4>Items</h4>
    <?php $this->load->view('modals/amend_item'); ?>
    <div class="mb-4">
        <div class="row">
            <div class="col-sm-12 col-md-6" v-for="(item, item_idx) in items.data">
                <div class="card">
                    <div class="card-header">
                        <h5>{{ item.item.name }}</h5>
                    </div>
                    <div class="card-body">
                        <div class="text-white alert alert-success" v-if="item.status == 1">
                            Received
                        </div>
                        <div class="text-white alert alert-warning" v-if="item.status == 2">
                            Amended
                        </div>
                        <div class="text-white alert alert-danger" v-if="item.status == 3">
                            Rejected
                        </div>
                        <div class="my-4">
                            <h6>Material Request Details</h6>
                            <div class="row">
                                <div class="col-sm-3">
                                    <a :href="generateURL('material_request/view', item.material_request_id)">
                                        {{ item.material_request.reference }}
                                    </a>
                                    <br>
                                    <small>Reference</small>
                                </div>
                                <div class="col-sm-3" v-if="item.accounting_ledger">
                                    <a :href="generateURL('accounting_ledgers/view', item.accounting_ledger.id)">
                                        {{ item.accounting_ledger.name }}
                                    </a>
                                    <br>
                                    <small>Expense Account</small>
                                </div>
                                <div class="col-sm-3">
                                    {{ item.material_request.request_date }}
                                    <br>
                                    <small>Request Date</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <strong>Brand:</strong>
                                {{ item.item_brand.name }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Class:</strong>
                                {{ item.item_class.name }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Type:</strong>
                                {{ item.item_type.name }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Group:</strong>
                                {{ item.item_group.name }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Unit:</strong>
                                {{ item.unit_of_measurement.name }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Quantity:</strong>
                                {{ item.quantity }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Unit Cost:</strong>
                                {{ item.unit_cost }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Total Cost:</strong>
                                {{ item.total_cost }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Freight Cost:</strong>
                                {{ item.freight_cost }}
                            </div>
                            <div class="col-sm-6">
                                <strong>Expiration Date:</strong>
                                {{ item.expiration_date }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a class="card-link text-warning" href="#!" @click.prevent="addFreightCost(item_idx)">
                            Add Freight Cost
                        </a>
                        <a class="card-link text-warning" href="#!" @click.prevent="addExpirationDate(item_idx)">
                            Add Expiration Date
                        </a>
                        <a class="card-link text-warning" href="#!" @click.prevent="modalAmendPOItem(item_idx)">
                            Amend Item
                        </a>
                        <a class="card-link text-danger" href="#!" @click.prevent="rejectItem(item_idx)">
                            Reject Item
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>