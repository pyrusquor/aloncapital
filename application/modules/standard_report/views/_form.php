<?php

$filters  =    isset($form['filters']) && $form['filters'] ? (array) json_decode($form['filters']) : '0';
$signatory  =    isset($form['signatory']) && $form['signatory'] ? (array) json_decode($form['signatory']) : '';
$id  =    isset($form['id']) && $form['id'] ? $form['id'] : '';

?>

<input type="hidden" name="slug" id="slug" value="<?= $form['slug']; ?>">
<input type="hidden" name="standard_report_id" id="standard_report_id" value="<?= $form['id']; ?>">

<div class="row">
    <div class="col-md-6">

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="">Companies <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="company_id" id="company_id" class="suggests form-control" data-module="companies">
                            <option value="0">Select Company</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <?php if (in_array('project_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Projects </label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[project_id]" id="project_id" class="suggests form-control" data-module="projects">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('aris', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Report Type <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[report_type]" id="report_type" class="form-control">
                                <option value="0">Select Option</option>

                                <?php if ($id == '26') : ?>
                                    <option value="a" style=""> Past Due Accounts </option>
                                    <option value="b" style=""> Past Due Accounts by Ammortization </option>
                                    <option value="c" style=""> Past Due for Legal Notice </option>
                                <?php endif ?>

                                <?php if ($id == '25') : ?>
                                    <!-- if ($id == 2) { // lot inventory by customer -->
                                    <option value="d" style=""> Lot Inventory by Customer </option>
                                    <option value="e" style=""> Lot Inventory by CTS No </option>
                                    <option value="f" style=""> Lot Inventory by Lot No </option>
                                    <option value="g" style=""> Lot Inventory by RA No </option>
                                <?php endif ?>

                                <?php if ($id == '22') : ?>
                                    <option value="h" style=""> Collection Report by Customer </option>
                                    <option value="i" style=""> Collection Report by Month </option>
                                    <option value="j" style=""> Collection Report by Year </option>
                                    <option value="k" style=""> Collection Report by Date Due </option>
                                <?php endif ?>

                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Customer Name <span class="kt-font-danger"></span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[customer_id]" id="customer_id" class="suggests form-control" data-module="aris_customers">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Projects <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[project_id]" id="project_id" class="suggests form-control" data-module="aris_project_information" data-select="ProjectName">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Lots <span class="kt-font-danger"></span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[lot_id]" id="property_id" class="suggests form-control" data-module="aris_lots" data-param="project_id" data-select="Lot_no">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>



        <?php if (in_array('property_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Property <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[property_id]" id="property_id" class="suggests form-control" data-module="properties" data-param="project_id">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('buyer_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Buyers <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[buyer_id]" id="buyer" class="suggests form-control" data-type="person" data-module="buyers">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('seller_id', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Sellers <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[seller_id]" id="seller" class="suggests form-control" data-type="person" data-module="sellers">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('date', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Date <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control datePicker" value="<?= date('Y-m-d'); ?>" readonly name="filters[date]" id="datePicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('year', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Year <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control yearPicker" value="<?= date('Y'); ?>" readonly name="filters[year]" id="yearPicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('month_year', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Month Year <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control yearmonthPicker" value="<?= date('Y-m'); ?>" readonly name="filters[month_year]" id="yearmonthPicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('daterange', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Date Range <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control" name="filters[daterange]" id="daterangepicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('reservation_daterange', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="">Reservation Date Range <span class="kt-font-danger">*</span></label>
                        <input type="text" class="form-control" name="filters[reservation_daterange]" id="reservation_daterangepicker">

                        <span class="form-text text-muted"></span>
                    </div>

                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('collectible_type', $filters)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Collectible Type <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[collectible_type]" id="collectible_type" class="form-control">
                                <option value="due_date">Due Date</option>
                                <option value="monthly">Montly</option>
                                <option value="yearly">Yearly</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('sort_by', $filters)) : ?>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="">Sort By</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select multiple name="filters[sort_by][]" id="sort_by" class="form-control kt-select2" name="param" multiple="multiple">
                                <option value="payment_date">Payment Date</option>
                                <option value="receipt_date">Receipt Date</option>
                                <option value="receipt_number">Receipt Number</option>
                                <option value="customer">Customer</option>
                                <option value="property">Property</option>
                                <option value="period_id">Period</option>
                                <option value="project">Project</option>
                                <option value="due_date">Due Date</option>
                                <!-- <option value="property">Property</option> -->
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="">Sort Direction</label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="filters[sort_direction]" id="sort_by" class="form-control">
                                <option value="ASC">Ascending</option>
                                <option value="DESC">Descending</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

    </div>


    <div class="col-md-6">





        <?php if (in_array('prepared_id', $signatory)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Prepared By <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="signatory[prepared_id]" id="prepared" class="suggests form-control" data-module="staff" data-type="person">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <?php if (in_array('checked_id', $signatory)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Checked By <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="signatory[checked_id]" id="checked" class="suggests form-control" data-module="staff" data-type="person">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>


        <?php if (in_array('approved_id', $signatory)) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="">Approved By <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <select name="signatory[approved_id]" id="approved" class="suggests form-control" data-module="staff" data-type="person">
                                <option value="0">Select Option</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>

        <?php endif ?>


    </div>
</div>