<html>
  <head>
    <title><?php echo $fileTitle ?></title>
    <style>
		html,body{
			font-size: 12px;
		}
		@page{
			margin: 20px 20px 20px 20px;
		}
		table{
			
			text-align:left;
			text-transform: capitalize;
		}

		
		table tbody tr td{padding: 5px;}
		table tbody tr td{text-transform: uppercase;}
		table tbody tr td,table tbody tr td:nth-child(2){
			text-align: left !important;
		}
		table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
		table thead tr th{text-align: left;vertical-align: bottom;}
		
		p{
			margin: 0;
		}
		p:last-child{
			margin-bottom: 10px;
		}
		.title{
			text-align: center;
			font-size: 16px
		}
		.divider-table td{
			border: 1px solid #fff;
			text-align: left;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
  </head>
  <body>
	<div class="title">
		<p>Pueblo de Panay Corporation Inc.<br><?php echo $project_name; ?><br><?php echo $fileTitle.$report_type ?></p>
	</div>
	<table width="100%" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th style="width:5%" >No.</th>

					<?php if ($report_type == "d") { ?>
						<th style="width:15%">Customer's Name</th>
						<th style="width:15%">Lot No</th>
					<?php } else if ($report_type == "e"){ ?>
						<th style="width:15%">CTS No</th>
						<th style="width:15%">Lot No</th>
						<th style="width:15%">Customer's Name</th>
					<?php } else if ($report_type == "f"){ ?>
						<th style="width:15%">Lot No</th>
						<th style="width:15%">Customer's Name</th>
					<?php } else if ($report_type == "g"){ ?>
						<th style="width:15%">RA No</th>
						<th style="width:15%">Lot No</th>
						<th style="width:15%">Customer's Name</th>
					<?php } else { ?>
						<th style="width:15%">Lot No</th>
						<th style="width:15%">Customer's Name</th>
					<?php } ?>

					<th style="width:20%">Area (sqm)</th>
					<th style="width:20%">Price / SQM</th>
					<th style="width:15%">TCP</th>
					<th style="width:20%">Lot Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$total_rows = count($results);
					if ($results) {
						$totalarea = 0;
						$averagepricepersqm = 0;
						foreach ($results as $key => $result) {
							$totalarea =  $result['Area'] + $totalarea;
							$averagepricepersqm =  ($result['Price'] + $averagepricepersqm) ;
							$total_tcp += ($result['Area']*$result['Price']);
						?>

						 <tr>
							<td><?php echo $key + 1; ?></td>

							<?php if ($report_type == "d"){ ?>
								<td><?php echo $result['LASTNAME'].", ".$result['MI']." ".$result['FIRSTNAME']; ?></td>
								<td><?php echo format_aris_property($result['Lot_no']); ?></td>
							<?php } else if ($report_type == "e"){ ?>
								<td><?php echo $result['CTS']; ?></td>
								<td><?php echo format_aris_property($result['Lot_no']); ?></td>
								<td><?php echo $result['LASTNAME'].", ".$result['MI']." ".$result['FIRSTNAME']; ?></td>
							<?php } else if ($report_type == "f"){ ?>
								<td><?php echo format_aris_property($result['Lot_no']); ?></td>
								<td><?php echo $result['LASTNAME'].", ".$result['MI']." ".$result['FIRSTNAME']; ?></td>
							<?php } else if ($report_type == "g"){ ?>
								<td><?php echo $result['RA']; ?></td>
								<td><?php echo format_aris_property($result['Lot_no']); ?></td>
								<td><?php echo $result['LASTNAME'].", ".$result['MI']." ".$result['FIRSTNAME']; ?></td>
							<?php } else{ ?>
								<td><?php echo format_aris_property($result['Lot_no']); ?></td>
								<td><?php echo $result['LASTNAME'].", ".$result['MI']." ".$result['FIRSTNAME']; ?></td>
							<?php } ?>
							<td><?php echo $result['Area']; ?></td>
							<td><?php echo format_currency($result['Price']); ?></td>
							<td><?php echo format_currency($result['Price'] * $result['Area']); ?></td>
							<td>&nbsp;</td>
						</tr><?php }
					} 
				?>
			</tbody>

	</table>
	<table>
		<tr>
						
			<td style="width:20%">
			<?php echo "<br /><B>Total Area: ".$totalarea."</B>"; ?>
			</td>
			
			<td style="width:30%">
			<?php echo "<B><br />Average Price per sqm: ".format_currency($averagepricepersqm / $total_rows)."</B>"; ?>
			</td>
			<td style="width:30%">
			<?php echo "<B><br />Total Price per sqm: ".format_currency($averagepricepersqm)."</B>"; ?>
			</td>
			<td style="width:15%">
			<?php echo "<B><br />Total TCP: ".format_currency($total_tcp)."</B>"; ?>
			</td>
		</tr>
	</table>
</body>
</html>