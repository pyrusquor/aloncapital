<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

	</style>
</head>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<h1 class="color-bluegreen margin0"><?php 
	$company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
	$company_name = get_value_field($company_id,'companies','name');
	echo $company_name;
?></h1>
		<p class="margin0"><?=$data['report']['name']; ?></p>
		<p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p>
		<p>For <?=view_date($result['filters']['date']);?></p>
	</div>

	<br />
	
	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th style="text-align: left;">Month</th>
				<th style="text-align: left;">Target</th>
				<th style="text-align: left;">Actual</th>
				<th style="text-align: left;">%</th>

		</thead>
		<tbody>
			<?php $tg1 = 0; $ttg1 = 0; $tg2 = 0;
				foreach($result['months'] as $month){ ?>
					<tr>
						<td style="text-align: left;">
							<?=view_date($month,"year_month");?>
						</td>
						<?php
							$g1 = 34000000;
							$g2 = $result['rows'][$month][0]['t_amount'];
							$tg1 += $g1;
							$tg2 += $g2;
							$ttg1 = $g1 * 12;
						?>
						<td> <?=money_php($g1);?> </td>
						<td> <?=money_php($g2);?> </td>
						<td> <?=convertPercentage(get_percentage($g1,$g2));?>%  </td>
					</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<td> TOTAL  </td>
				<td> <?=money_php($tg1);?> </td>
				<td> <?=money_php($tg2);?> </td>
				<td> <?=convertPercentage(get_percentage($tg1,$tg2));?>%  </td>
			</tr>
			
			<tr>
				<td> Annual Target (YTD)</td>
				<td> <?=money_php($ttg1);?> </td>
				<td> <?=money_php($tg2);?> </td>
				<td> <?=convertPercentage(get_percentage($ttg1,$tg2));?>%  </td>
			</tr>

			<tr>
				<td colspan="4">  As of  <?=view_date($result['filters']['date']);?> </td>
			</tr>
			
		</tfoot>
	</table>
	
	
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>
</html>