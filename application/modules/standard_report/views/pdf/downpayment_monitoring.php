<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}
	
		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}
		
		/*.main { width:200% }*/
	</style>
</head>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<h1 class="color-bluegreen margin0"><?php 
	$company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
	$company_name = get_value_field($company_id,'companies','name');
	echo $company_name;
?></h1>
		<p class="margin0"><?=$data['report']['name']; ?></p>
		<p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p>
		<p>For <?=$date = view_date($result['filters']['daterange'],'range');?></p>
	</div>

	<br />

	<table class="table table-condensed table-striped main">
		<thead>
			<tr>
				<th style="text-align: left;">No.</th>
				<th style="text-align: left;">Buyer</th>
				<th style="text-align: left;">Property</th>

				<th style="text-align: left;">Model Unit</th>
				<th style="text-align: left;">Lot Area</th>
				<th style="text-align: left;">Floor Area</th>

				<th style="text-align: left;">Bldg/Subd Status</th>
				

				<th style="text-align: left;">RA Date</th>
				<th style="text-align: left;">F. Scheme</th>

				<th style="text-align: left;">Sales Arm</th>
				<th style="text-align: left;">Agent Arm</th>


				<th style="text-align: left;">TCP</th>

				<th style="text-align: left;">DP %</th>
				<th style="text-align: left;">DP Amt</th>
				<th style="text-align: left;">DP Terms</th>

				<th style="text-align: left;">AR Balance</th>

				<th style="text-align: left;">Payments (as of)</th>

				<th style="text-align: left;">DP Balance</th>
				<th style="text-align: left;">Percentage</th>

				<th style="text-align: left;">DP Remaining # of Mos.</th>

				<th style="text-align: left;">Due Date</th>
		
				<?php 
					if($result['months']) {
						foreach ($result['months'] as $key => $month) { ?>
							<th style="text-align: left;"><?=view_date($month,"month_year");?></th>
						<?php }
					}
				?>

				<th style="text-align: left;">Total</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($result['payments']): ?>
				<?php foreach ($result['payments'] as $key => $payment) { 

					$CI = &get_instance();
            		$CI->transaction_library->initiate( $payment['transaction_id'] );
           			$tpm = $CI->transaction_library->get_amount_paid(1, 0);

               		$tpm_p = $this->transaction_library->get_amount_paid(1, 0, 1);

           			$dp_amount =  $CI->transaction_library->get_amount(2);

           			$lv_amount =  $CI->transaction_library->get_amount(3);

           			$dp_percent =  $CI->transaction_library->get_amount(2,1);
           			$rem_dp_terms =  $CI->transaction_library->get_remaining_terms(2);
           			$dp_terms =  $CI->transaction_library->get_remaining_terms(2,1);

           			$dp_rem_balance =  $CI->transaction_library->get_remaining_balance(1,2);
           			@$t1 += $payment['total_contract_price'];
           			@$t2 += $dp_amount;
           			@$t3 += $lv_amount;
           			@$t4 += $tpm;
           			@$t5 += $dp_rem_balance;
				?>
					<tr>
						<td style="text-align: left;"><?=$key += 1;?></td>
						<td style="text-align: left;"><?=get_person_name($payment['buyer_id'],'buyers');?></td>
						<td style="text-align: left;"><?=get_value_field($payment['property_id'],'properties','name');?></td>
						
						<td style="text-align: left;"><?=get_value_field($payment['model_id'],'house_models','name');?></td>
						<td style="text-align: left;"><?=($payment['lot_area']);?> SQM</td>
						<td style="text-align: left;"><?=($payment['floor_area']);?> SQM</td>
						
						<td style="text-align: left;">&nbsp;</td>

						<td style="text-align: left;"><?=view_date($payment['reservation_date']);?></td>
						<td style="text-align: left;"><?=get_value_field($payment['financing_scheme_id'],'financing_schemes','name');?></td>

						<td style="text-align: left;">

							<?php $seller = get_person($payment['seller_id'],'sellers');?>
							<?php if ($seller['sales_group_id']): ?>
								<?php echo Dropdown::get_static('group_type',$seller['sales_group_id'],'view'); ?>
							<?php endif ?>

						</td>

						<td style="text-align: left;"><?=get_person_name($payment['seller_id'],'sellers');?></td>
	
						<td style="text-align: left;"><?=money_php($payment['total_contract_price']);?></td>

						<td style="text-align: left;"><?=convertPercentage($dp_percent);?>%</td>
						<td style="text-align: left;"><?=money_php($dp_amount);?></td>
						<td style="text-align: left;"><?=$dp_terms;?></td>

						<td style="text-align: left;"><?=money_php($lv_amount);?></td> 

						<td style="text-align: left;"><?=money_php($tpm);?></td>

						<td style="text-align: left;"><?=money_php($dp_rem_balance);?></td>
						<td style="text-align: left;"><?=$tpm_p;?>%</td>

						<td style="text-align: left;"><?=$rem_dp_terms;?></td>

						<td style="text-align: left;">&nbsp;</td>

						<?php 
							$t_a_id = 0;
							if( $result['months'] ) {
								foreach ( $result['months'] as $key => $month ) { ?>
										<td style="text-align: left;">
											<?php @$t_m_id[$month] += $result['rows'][$payment['transaction_id']][$month][0]['total_amount']; ?> 
											<?php $t_a_id += $result['rows'][$payment['transaction_id']][$month][0]['total_amount']; ?>
											<?=money_php($result['rows'][$payment['transaction_id']][$month][0]['total_amount']);?>
										</td>
								<?php } ?>

								<td style="text-align: left;"> <?=money_php($t_a_id)?></td>
						<?php } ?>
					</tr>

				<?php } ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="11" style="text-align: right">TOTAL</td>
				<td style="text-align: right"><?=money_php($t1);?></td>
				<td style="text-align: right">&nbsp;</td>
				<td style="text-align: right"><?=money_php($t2);?></td>
				<td style="text-align: right">&nbsp;</td>
				<td style="text-align: right"><?=money_php($t3);?></td>
				<td style="text-align: right"><?=money_php($t4);?></td>
				<td style="text-align: right"><?=money_php($t5);?></td>
				<td colspan="3" style="text-align: right">&nbsp;</td>

				<?php 
					if( $result['months'] ) {
						foreach ( $result['months'] as $key => $month ) { ?>
								<td style="text-align: left;">
									<?php @$tt +=  $t_m_id[$month]; ?>
									<?=money_php($t_m_id[$month]);?>
								</td>
						<?php } ?>
						<td style="text-align: left;"> <?=money_php($tt)?></td>
				<?php } ?>
			</tr>
		</tfoot>
	</table>
	
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>
				<td ><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>
</html>