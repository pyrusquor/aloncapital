<html>
  <head>
    <title><?php echo $fileTitle ?></title>
    <style>
		html,body{
			font-size: 12px;
		}
		@page{
			margin: 20px 20px 20px 20px;
		}
		table{
			
			text-align:left;
			text-transform: capitalize;
		}

		
		table tbody tr td{padding: 5px;}
		table tbody tr td{text-transform: uppercase;}
		table tbody tr td,table tbody tr td:nth-child(2){
			text-align: left !important;
		}
		table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
		table thead tr th{text-align: left;vertical-align: bottom;}
		
		p{
			margin: 0;
		}
		p:last-child{
			margin-bottom: 10px;
		}
		.title{
			text-align: center;
			font-size: 16px
		}
		.divider-table td{
			border: 1px solid #fff;
			text-align: left;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
  </head>
  <body>
	<div class="title">
		<p>Pueblo de Panay Corporation Inc.<br><?php echo $project_name; ?><br><?php echo $fileTitle ?></p>
	</div>
	<table width="100%" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th style="width:20%">Customers' Name</th>
					<th style="width:5%">R. Date</th>
					<th style="width:5%">Receipt No.</th>
					<th style="width:10%">Lot No.</th>
					<th style="width:10%">Reservation</th>
					<th style="width:5%">Partial DP</th>
					<th style="width:5%">Full DP</th>
					<th style="width:10%">Principal</th>
					<th style="width:10%">Interest</th>
					<th style="width:10%">Penalty</th>
					<th style="width:10%">Total</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					if ($results) {
						foreach ($results as $key => $result) { ?><tr>
							<td><?php echo $result['LASTNAME'].", ".$result['MI']." ".$result['FIRSTNAME']; ?></td>
							<td><?php echo db_date_format($result['DATE_PAID'],true); ?></td>
							<td><?php echo $result['OR_NO']; ?></td>
							<td><?php echo format_aris_property($result['Lot_no']); ?></td>
							<td>
								<?php 
									if (!$result['PERIOD']) {
										echo format_currency($result['RESAMT_P']); 
									} else {
										echo format_currency("0.00"); 
									}
								?>
							</td>
							<td><?php //echo $result['PROVINCE']; ?></td>
							<td><?php //echo $result['COUNTRY']; ?></td>
							<td><?php echo format_currency($result['PRINCIPAL']); ?></td>
							<td><?php echo format_currency($result['INTEREST']); ?></td>
							<td><?php echo format_currency($result['PENALTY']); ?></td>
							<td><?php echo format_currency($result['AmountPaid']); ?></td>
						</tr><?php }
					} 
				?>
			</tbody>
	</table>
</body>
</html>