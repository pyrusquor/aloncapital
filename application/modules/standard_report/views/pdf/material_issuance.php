<!DOCTYPE html>
<html>
<head>
	<title>Material Issuance - <?= date('F Y',strtotime($filters['month_year'])) ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}
		td.bottom-border {
			border-bottom: 1px solid black;
		}
		p {margin: 0 0 15px;}
		tr.bottom-border {
			border-bottom: 1px solid black;
		}
		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

		.nospace {
			margin: 0px;
		}

	</style>
</head>
<body>
	<?php
	if($material_issuances):
		foreach($material_issuances as $key => $material_issuance):
	?>
	<div class="text-center">
		<h1 class="color-bluegreen margin0"></h1>
		<strong><p class='nospace'>Liquidation Report</p></strong>
		<strong><p class='nospace'>Technical Department</p></strong>
		<strong><p class='nospace'><?= date('F j, Y',strtotime($material_issuance['created_at'])) ?></p></strong>
		
		<table class='table' cellspacing='0'>
			<tr>
				<th style="width:7%;" bgcolor='red'><?= $material_issuance['reference'] ?></th>
				<th style="width:5%;">CR#</th>
				<th style="width:25%;"></th>
				<th style="width:7%;"></th>
				<th style="width:7%;"></th>
				<th style="width:7%;"></th>
				<th style="width:7%;"></th>
				<th style="width:7%;"></th>
				<th style="width:7%;"></th>
			</tr>
			<tr>
				<td></td>
				<td>Given by:</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td class='bottom-border'>Purpose:</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td class='bottom-border'><strong>Date:</strong></td>
				<td class='bottom-border'></td>
				<td class='bottom-border'><strong>QTY</strong></td>
				<td class='bottom-border'><strong>Unit</strong></td>
				<td class='bottom-border'><strong>Unit Cost</strong></td>
				<td class='bottom-border'><strong>Amount</strong></td>
				<td class='bottom-border'><strong>Remarks</strong></td>
				<td></td>
			</tr>
			<?php 
				if($material_issuance['items']):
					$total_balance = 0;
					foreach($material_issuance['items'] as $i_key => $item):
						$total_balance += $item['material_receiving_item']['total_cost'];
			?>
			<tr>
				<td></td>
				<td><?= $i_key=='0' ? date('m.d',strtotime($material_issuance['created_at'])) : '' ; ?></td>
				<td><strong><?= $item['material_receiving_item']['item_name'] ?></strong></td>
				<td><?= number_format($item['quantity'],0) ?></td>
				<td><?= $item['material_receiving_item']['unit_of_measurement_name'] ?></td>
				<td><?= $item['material_receiving_item']['unit_cost'] ?></td>
				<td><?= $item['material_receiving_item']['total_cost'] ?></td>
				<td></td>
				<td></td>
			</tr>
			<?php endforeach; ?>
			<tr>
				<td></td>
				<td></td>
				<td><strong>Balance</strong></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td bgcolor="yellow"><?= money_php($total_balance) ?></td>
			</tr>
			
			<?php endif; ?>
		</table>
	</div>
	<?php
	endforeach;
	endif; 
	?>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html>