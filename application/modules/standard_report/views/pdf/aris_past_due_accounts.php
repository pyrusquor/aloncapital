<html>
  <head>
    <title><?php echo $fileTitle ?></title>
    <style>
		html,body{
			font-size: 12px;
		}
		@page{
			margin: 20px 20px 20px 20px;
		}
		table{
			
			text-align:left;
			text-transform: capitalize;
		}

		
		table tbody tr td{padding: 5px;}
		table tbody tr td{text-transform: uppercase;}
		table tbody tr td,table tbody tr td:nth-child(2){
			text-align: left !important;
		}
		table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
		table thead tr th{text-align: left;vertical-align: bottom;}
		
		p{
			margin: 0;
		}
		p:last-child{
			margin-bottom: 10px;
		}
		.title{
			text-align: center;
			font-size: 16px
		}
		.divider-table td{
			border: 1px solid #fff;
			text-align: left;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
  </head>
  <body>
	<div class="title">
		<p>Pueblo de Panay Corporation Inc.<br><?php echo $project_name; ?><br><?php echo $fileTitle ?></p>
	</div>
	<table width="100%" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th style="width:1%">No.</th>
					<th style="width:33%">Customer's Name</th>
					<th style="width:10%">Lot No.</th>
					<th style="width:10%">Next Due Date</th>
					<th style="width:1%">Periods Delayed</th>
					<th style="width:10%">Amortization</th>
					<?php if ($report_type != "c"): ?>
						<th style="width:10%">Principal</th>
						<th style="width:10%">Interest</th>
					<?php endif ?>
					<th style="width:15%">Amount Due</th>
					<?php if ($report_type == "c"): ?>
						<th style="width:20%">Address</th>
					<?php endif ?>
				</tr>
			</thead>
			<tbody>
				<?php 
					if ($results) {
						foreach ($results as $key => $res) { if(($report_type == "c")&&($res['count_c'] > 2)){ continue; }?>
							<tr>
								<td><?php echo $key + 1; ?></td>
								<td><?php echo $res['LASTNAME'].", ".$res['MI']." ".$res['FIRSTNAME']; ?></td>
								<td><?php echo format_aris_property($res['Lot_no']); ?></td>
								<td><?php echo db_date_format($res['col_DateDue']); ?></td>
								<td><?php echo $res['count_c']; ?></td>
								<td><?php echo format_currency($res['AMORTAMT']); ?></td>
								<?php if ($report_type != "c"): ?>
									<td><?php echo format_currency($res['sum_p']); ?></td>
									<td><?php echo format_currency($res['sum_i']); ?></td>
								<?php endif ?>
								<td><?php echo format_currency($res['sum_p'] +  $res['sum_i']); ?></td>
								<?php if ($report_type == "c"): ?>
									<td><?php echo $res['ADDRESS'].' '.$res['CITY'].' '.$res['PROVINCE']; ?></td>
								<?php endif ?>
							</tr>
					<?php }
					} 
				?>
			</tbody>
	</table>
</body>
</html>