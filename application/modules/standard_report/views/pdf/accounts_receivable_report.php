<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

	</style>
</head>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<h1 class="color-bluegreen margin0"><?php 
	$company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
	$company_name = get_value_field($company_id,'companies','name');
	echo $company_name;
?></h1>
		<p class="margin0"><?=$data['report']['name']; ?></p>
		<p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p>
		<p>For <?=view_date($result['filters']['daterange'],'range');?></p>
	</div>

	<br />

	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th style="text-align: left;">Ref #</th>
				<th style="text-align: left;">Buyer</th>
				<th style="text-align: left;">Rsv Date</th>

				<th style="text-align: left;">Property</th>
				<th style="text-align: left;">CTS #</th>
				<th style="text-align: left;">CTS Date</th>


				<th style="text-align: left;">TCP</th>
				<th style="text-align: left;">TPM<br>(P & I)</th>
				<th style="text-align: left;">% </th>

				<th style="text-align: left;">F. Scheme</th>
				<th style="text-align: left;">Agent <br> Sales Group </th>

				<th style="text-align: left;">Due Date</th>

				<th style="text-align: left;">Amort</th>

				<th style="text-align: left;">Principal</th>

				<th style="text-align: left;">Sum of Interest</th>

				<th style="text-align: left;">TRP</th>

				<th style="text-align: left;">TRB</th>

				<th style="text-align: left;">Remarks</th>
			</tr>
		</thead>
		<tbody><?php $tp_amt = 0;$ti_amt = 0;$tpen_amt = 0;$ta_amt = 0;$t_trb = 0;$t_trp = 0;$t_tri = 0; ?>
			<?php if ($result['payments']): ?>
				<?php foreach ($result['payments'] as $key => $payment) {
   						$CI = &get_instance();
                		$CI->transaction_library->initiate( $payment['transaction_id'] );

               			$tpm = $CI->transaction_library->get_amount_paid(1, 0);
               			$trp = $this->transaction_library->get_remaining_balance(2, 0);
               			$tri = $this->transaction_library->get_remaining_balance(3, 0, 0, 8);
               			// $tri = $CI->transaction_library->get_amount_paid(3, 0);

               			$trb = $trp + $tri;

                		// $tpm_p = $CI->transaction_library->get_amount_paid(1, 0, 1);
				 ?>
					<tr>
						<td style="text-align: left;"><?=$payment['reference'];?></td>
						<td style="text-align: left;"><?=get_person_name($payment['buyer_id'],'buyers');?></td>
						<td style="text-align: left;"><?=view_date($payment['reservation_date']);?></td>


						<td style="text-align: left;"><?=get_value_field($payment['property_id'],'properties','name');?></td>
						<td style="text-align: left;"><?=get_value_field($payment['property_id'],'properties','cts_number');?></td>
						<td style="text-align: left;"><?=view_date(get_value_field($payment['property_id'],'properties','cts_notarized_date'));?></td>
	

						<td style="text-align: left;"><?=money_php($payment['total_contract_price']);?></td>
						<td style="text-align: left;"><?=money_php($tpm);?></td>
						<td style="text-align: left;"><?=$tpm_p;?>%</td>

						<td style="text-align: left;"><?=get_value_field($payment['financing_scheme_id'],'financing_schemes','name');?></td>
						<td style="text-align: left;">
							<?=get_person_name($payment['seller_id'],'sellers');?><br>
							<?php $seller = get_person($payment['seller_id'],'sellers');?>
							<?php echo Dropdown::get_static('group_type',$seller['sales_group_id'],'view'); ?>
						</td>

						<td style="text-align: left;">
							<?php echo view_date($payment['due_date']) ;?>
						</td>

						<td style="text-align: left;">
							<?php $ta_amt += $payment['total_amount']; echo money_php($payment['total_amount']);?>
						</td>

						<td style="text-align: left;">
							<?php $tp_amt += $payment['principal_amount']; echo money_php($payment['principal_amount']);?>
						</td>

						<td style="text-align: left;">
							<?php //$ti_amt += $payment['interest_amount']; echo money_php($payment['interest_amount']);?>
							<?php $tri_amt += $tri; echo money_php($tri);?>
						</td>

						<td style="text-align: left;">
							<?php $t_trp += $trp; echo money_php($trp); ?>
						</td>

						<td style="text-align: left;">
							<?php $t_trb += $trb; echo money_php($trb); ?>
						</td>

						<td style="text-align: left;">
							<?php echo $payment['particulars'] ;?>
						</td>

					</tr>
				<?php } ?>
			<?php endif ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="12" style="text-align: right">TOTAL</td>
				<td><?=money_php($ta_amt);?></td>

				<td><?=money_php($tp_amt);?></td>

				<td><?=money_php($tri_amt);?></td>

				<td><?=money_php($t_trp);?></td>

				<td><?=money_php($t_trb);?></td>
			</tr>
		</tfoot>
	</table>
	
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>
				<td ><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>
</html>