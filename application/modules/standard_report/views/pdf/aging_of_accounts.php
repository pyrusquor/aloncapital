<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

	</style>
</head>
<script type="text/javascript">
	function exportTableToExcel(tableID, filename = ''){
	    var downloadLink;
	    var dataType = 'application/vnd.ms-excel';
	    var tableSelect = document.getElementById(tableID);
	    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
	    
	    // Specify file name
	    filename = filename?filename+'.xls':'excel_data.xls';

	    // window.open('data:application/vnd.ms-excel;base64,' + $.base64.encode(html));
	    
	    // Create download link element
	    downloadLink = document.createElement("a");
	    
	    document.body.appendChild(downloadLink);
	    
	    if(navigator.msSaveOrOpenBlob){
	        var blob = new Blob(['\ufeff', tableHTML], {
	            type: dataType
	        });
	        navigator.msSaveOrOpenBlob( blob, filename);
	    }else{
	        // Create a link to the file
	        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
	    
	        // Setting the file name
	        downloadLink.download = filename;
	        
	        //triggering the function
	        downloadLink.click();
	    }
	}
</script>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<h1 class="color-bluegreen margin0"><?php 
	$company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
	$company_name = get_value_field($company_id,'companies','name');
	echo $company_name;
?></h1>
		<p class="margin0"><?=$data['report']['name']; ?></p>
		<p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p>
		<p>For <?=view_date($result['filters']['daterange'],'range');?></p>
	</div>

	<br />
    <button class="btn btn-success" onclick="exportTableToExcel('aging-of-accounts', 'aging-of-accounts')">Export Table Data To Excel File</button>
	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th rowspan="2" style="text-align: left;">Project</th>
				<th rowspan="2" style="text-align: left;">Reference & Property</th>
				<th rowspan="2" style="text-align: left;">Buyer & Seller</th>
				<th rowspan="2" style="text-align: center;">Periods Delayed</th>

				<th colspan="3" style="text-align: center;">1-30 Days</th>
				<th colspan="3" style="text-align: center;">31-60 Days</th>
				<th colspan="3" style="text-align: center;">61-90 Days</th>
				<th colspan="3" style="text-align: center;">Over 90 Days</th>

				<th colspan="3" style="text-align: center;">Outstanding Balance</th>
			</tr>

			<tr>
				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>

				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>

				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>

				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>

				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>
			</tr>
		</thead>
		<tbody><?php $tp = 0;$ti = 0;$ta = 0;$tpa = 0;$tia = 0;$taa = 0;$tpb = 0;$tib = 0;$tab = 0; $tpc = 0;$tic = 0;$tac = 0;$tpd = 0;$tid = 0;$tad = 0;?>
			<?php if ($result['payments']): ?>
				<?php foreach ($result['payments'] as $key => $payment) { ?>
					<tr>

						<td style="text-align: left;"><?=get_value_field($payment['project_id'],'projects','name');?></td>
						<td style="text-align: left;">
							<?=get_value_field($payment['property_id'],'properties','name');?><br>
							<a target="_blank" href="<?=base_url();?>transaction/view/<?=$payment['transaction_id'];?>">
								<?=$payment['reference'];?>
							</a>
						</td>
						<td style="text-align: left;">
							<?=get_person_name($payment['buyer_id'],'buyers');?>
							<br>
							<?=get_person_name($payment['seller_id'],'sellers');?>
						</td>
						<td style="text-align: center;"><?=$payment['t_count'];?></td>
	
						<th style="text-align: left;"><?=money_php($payment['total_principala']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_interesta']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_amounta']);?></th>

						<th style="text-align: left;"><?=money_php($payment['total_principalb']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_interestb']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_amountb']);?></th>

						<th style="text-align: left;"><?=money_php($payment['total_principalc']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_interestc']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_amountac']);?></th>

						<th style="text-align: left;"><?=money_php($payment['total_principald']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_interestd']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_amountad']);?></th>

						<th style="text-align: left;"><?=money_php($payment['total_principal']);?></th>
						<th style="text-align: left;"><?=money_php($payment['total_interest']);?></th>
						<th style="text-align: left;"><?=money_php($payment['t_amount']);?></th>



						<?php 
							$tp += $payment['total_principal'];
							$ti += $payment['total_interest'];
							$ta += $payment['t_amount'];

							$tpa += $payment['total_principala'];
							$tia += $payment['total_interesta'];
							$taa += $payment['t_amounta'];

							$tpb += $payment['total_principalb'];
							$tib += $payment['total_interestb'];
							$tab += $payment['t_amountb'];

							$tpc += $payment['total_principalc'];
							$tic += $payment['total_interestac'];
							$tac += $payment['t_amountc'];

							$tpd += $payment['total_principald'];
							$tid += $payment['total_interestd'];
							$tad += $payment['t_amountd'];
						?>

						
					</tr>
				<?php } ?>
			<?php endif ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4" style="text-align: right">TOTAL</td>
				<td><?=money_php($tpa);?></td>
				<td><?=money_php($tia);?></td>
				<td><?=money_php($taa);?></td>
				<td><?=money_php($tpb);?></td>
				<td><?=money_php($tib);?></td>
				<td><?=money_php($tab);?></td>
				<td><?=money_php($tpc);?></td>
				<td><?=money_php($tic);?></td>
				<td><?=money_php($tac);?></td>
				<td><?=money_php($tpd);?></td>
				<td><?=money_php($tid);?></td>
				<td><?=money_php($tad);?></td>
				<td><?=money_php($tp);?></td>
				<td><?=money_php($ti);?></td>
				<td><?=money_php($ta);?></td>
			</tr>
		</tfoot>
	</table>
	
	<table style="display:none" class="table table-condensed table-striped" id="aging-of-accounts">
		<thead>
			<tr>
				<th rowspan="2" style="text-align: left;">Project</th>
				<th rowspan="2" style="text-align: left;">Reference & Property</th>
				<th rowspan="1" style="text-align: left;">Buyer</th>
				<th rowspan="1" style="text-align: left;">Seller</th>
				<th rowspan="2" style="text-align: center;">Periods Delayed</th>

				<th colspan="3" style="text-align: center;">1-30 Days</th>
				<th colspan="3" style="text-align: center;">31-60 Days</th>
				<th colspan="3" style="text-align: center;">61-90 Days</th>
				<th colspan="3" style="text-align: center;">Over 90 Days</th>

				<th colspan="3" style="text-align: center;">Outstanding Balance</th>
			</tr>

			<tr>
				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>

				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>

				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>

				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>

				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Total</th>
			</tr>
		</thead>
		<tbody><?php $tp = 0;$ti = 0;$ta = 0;$tpa = 0;$tia = 0;$taa = 0;$tpb = 0;$tib = 0;$tab = 0; $tpc = 0;$tic = 0;$tac = 0;$tpd = 0;$tid = 0;$tad = 0;?>
			<?php if ($result['payments']): ?>
				<?php foreach ($result['payments'] as $key => $payment) { ?>
					<tr>

						<td style="text-align: left;"><?=get_value_field($payment['project_id'],'projects','name');?></td>
						<td style="text-align: left;">
							<?=get_value_field($payment['property_id'],'properties','name');?><br>
							<a target="_blank" href="<?=base_url();?>transaction/view/<?=$payment['transaction_id'];?>">
								<?=$payment['reference'];?>
							</a>
						</td>
						<td style="text-align: left;">
							<?=get_person_name($payment['buyer_id'],'buyers');?>
						</td>

						<td style="text-align: left;">
							<?=get_person_name($payment['seller_id'],'sellers');?>
						</td>
						
						<td style="text-align: center;"><?=$payment['t_count'];?></td>
	
						<th style="text-align: left;"><?=($payment['total_principala']);?></th>
						<th style="text-align: left;"><?=($payment['total_interesta']);?></th>
						<th style="text-align: left;"><?=($payment['total_amounta']);?></th>

						<th style="text-align: left;"><?=($payment['total_principalb']);?></th>
						<th style="text-align: left;"><?=($payment['total_interestb']);?></th>
						<th style="text-align: left;"><?=($payment['total_amountb']);?></th>

						<th style="text-align: left;"><?=($payment['total_principalc']);?></th>
						<th style="text-align: left;"><?=($payment['total_interestc']);?></th>
						<th style="text-align: left;"><?=($payment['total_amountac']);?></th>

						<th style="text-align: left;"><?=($payment['total_principald']);?></th>
						<th style="text-align: left;"><?=($payment['total_interestd']);?></th>
						<th style="text-align: left;"><?=($payment['total_amountad']);?></th>

						<th style="text-align: left;"><?=($payment['total_principal']);?></th>
						<th style="text-align: left;"><?=($payment['total_interest']);?></th>
						<th style="text-align: left;"><?=($payment['t_amount']);?></th>



						<?php 
							$tp += $payment['total_principal'];
							$ti += $payment['total_interest'];
							$ta += $payment['t_amount'];

							$tpa += $payment['total_principala'];
							$tia += $payment['total_interesta'];
							$taa += $payment['t_amounta'];

							$tpb += $payment['total_principalb'];
							$tib += $payment['total_interestb'];
							$tab += $payment['t_amountb'];

							$tpc += $payment['total_principalc'];
							$tic += $payment['total_interestac'];
							$tac += $payment['t_amountc'];

							$tpd += $payment['total_principald'];
							$tid += $payment['total_interestd'];
							$tad += $payment['t_amountd'];
						?>

						
					</tr>
				<?php } ?>
			<?php endif ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4" style="text-align: right">TOTAL</td>
				<td><?=($tpa);?></td>
				<td><?=($tia);?></td>
				<td><?=($taa);?></td>
				<td><?=($tpb);?></td>
				<td><?=($tib);?></td>
				<td><?=($tab);?></td>
				<td><?=($tpc);?></td>
				<td><?=($tic);?></td>
				<td><?=($tac);?></td>
				<td><?=($tpd);?></td>
				<td><?=($tid);?></td>
				<td><?=($tad);?></td>
				<td><?=($tp);?></td>
				<td><?=($ti);?></td>
				<td><?=($ta);?></td>
			</tr>
		</tfoot>
	</table>
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>
				<td ><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>
</html>