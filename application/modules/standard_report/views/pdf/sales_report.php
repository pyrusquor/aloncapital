<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

	</style>
</head>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<h1 class="color-bluegreen margin0"><?php 
	$company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
	$company_name = get_value_field($company_id,'companies','name');
	echo $company_name;
?></h1>
		<p class="margin0"><?=$data['report']['name']; ?></p>
		<p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p>
		<p>For <?=view_date($result['filters']['date']);?></p>
	</div>

	<br />
	
	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th style="text-align: left;"><?=view_date($result['filters']['year']);?></th>
				<?php foreach ($result['groups'] as $key => $group) { if(!$key){ continue; }?>
					<th style="text-align: left;" colspan="2"><?=$group;?></th>	
				<?php } ?>
				<th style="text-align: left;" colspan="2">Total</th>	
			</tr>
			<tr>
				<th style="text-align: left;">Month</th>
				<?php foreach ($result['groups'] as $key => $group) { ?>
					<th style="text-align: left;">Unit</th>	
					<th style="text-align: left;">TCP (PESO)</th>	
				<?php } ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($result['months'] as $key => $month) {  $t1=0;$t2=0;?>
				<tr> 
					<td valign="left" align="left"> <?=view_date($month,'month_year');?> </td>
					<?php foreach ($result['groups'] as $key => $group) { if(!$key){continue;} $t1 += $result['rows'][$month][$group][0]['t_count']; $t2 += $result['rows'][$month][$group][0]['total_selling_price']; ?>
							<td > <?=$result['rows'][$month][$group][0]['t_count'];?> </td>
							<td > <?=money_php($result['rows'][$month][$group][0]['total_selling_price']);?> </td>
					<?php } ?>
					<td > <?=$t1;?> </td>
					<td > <?=money_php($t2);?> </td>
				</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<td> TOTAL</td>
				<td> &nbsp; </td>
			</tr>
		</tfoot>
	</table>
	
	
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>
				<td ><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>
</html>