<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}
		.column {
		float: left;
		width: 33.33%;
		}

		/* Clear floats after the columns */
		.row:after {
		content: "";
		display: table;
		clear: both;
		}

	</style>
</head>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<h1 class="color-bluegreen margin0"><?php 
	$company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
	$company_name = get_value_field($company_id,'companies','name');
	echo $company_name;

	$ar_clearing = $result['ar_clearing'];

?></h1>
		<p class="margin0"><?=$data['report']['name']; ?></p>
		<p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p>
		<p>For <?=view_date($result['filters']['daterange'],'range');?></p>
	</div>

	<br />

	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th style="text-align: left;">Project</th>
				<th style="text-align: left;">Property</th>
				<th style="text-align: left;">Buyer</th>
				<th style="text-align: left;">Principal</th>
				<th style="text-align: left;">Interest</th>
				<th style="text-align: left;">Penalty</th>
				<th style="text-align: left;">Rebates</th>
				<th style="text-align: left;">Total</th>
			</tr>
		</thead>
		<tbody><?php $tp_amt = 0;$ti_amt = 0;$tpen_amt = 0;$t_amt = 0; ?>
			<?php if ($result['payments']): ?>
				<?php foreach ($result['payments'] as $key => $payment) { ?>
					<tr>
						<td style="text-align: left;"><?= get_value_field($payment['project_id'], 'projects', 'name'); ?></td>
						<td style="text-align: left;"><?= get_value_field($payment['transaction_id'], 'transactions', 'reference'); ?><br><?= get_value_field($payment['property_id'], 'properties', 'name'); ?></td>


						<td style="text-align: left;"><?= get_person_name($payment['buyer_id'], 'buyers'); ?></td>


						<td style="text-align: left;"><?php $tp_amt += $payment['total_principal'];
														echo money_php($payment['total_principal']); ?></td>
						<td style="text-align: left;"><?php $ti_amt += $payment['total_interest'];
														echo money_php($payment['total_interest']); ?></td>
						<td style="text-align: left;"><?php $tpen_amt += $payment['total_penalty'];
														echo money_php($payment['total_penalty']); ?></td>
						<td style="text-align: left;"><?php $r_amt += $payment['total_rebate'];
														echo money_php($payment['total_rebate']); ?></td>
						<td style="text-align: left;"><?php $t_amt += $payment['t_amount'];
														echo money_php($payment['t_amount']); ?></td>

					</tr>
				<?php } ?>
			<?php endif ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3" style="text-align: right">TOTAL</td>
				<td><?=money_php($tp_amt);?></td>
				<td><?=money_php($ti_amt);?></td>
				<td><?=money_php($tpen_amt);?></td>
				<td><?=money_php($r_amt);?></td>
				<td><?=money_php($t_amt);?></td>
			</tr>
		</tfoot>
	</table>
	
	<br />
	<br />
	<?php if($ar_clearing): ?>
		<strong>CHEQUES </strong>
		<div class='row'>
		<br />

			<?php foreach($ar_clearing as $key=>$value):?>
				<div class='column'>
				<strong><?= $key ?></strong>: <?= money_php($value) ?>
				</div>
			<?php endforeach ?>
			<br />
			<br />
			<br />
			<br />
		</div>
	<?php endif ?>
	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>
				<td ><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>
</html>