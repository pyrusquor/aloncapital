<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

	</style>
</head>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<h1 class="color-bluegreen margin0"><?php 
	$company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
	$company_name = get_value_field($company_id,'companies','name');
	echo $company_name;
?></h1>
		<p class="margin0"><?=$data['report']['name']; ?></p>
		<p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p>
		<p>For <?=view_date($result['filters']['daterange'],'range');?></p>
	</div>

	<br />
	
	<?php if ($result['filters']['type'] == "due_date"): ?>
		<table class="table table-condensed table-striped">
			<thead>
				<tr>
					<th style="text-align: left;">Project</th>
					<th style="text-align: left;">Property</th>
					<th style="text-align: left;">Buyer</th>
					<th style="text-align: left;">Address</th>

					<th style="text-align: left;">Due Date</th>
					<th style="text-align: left;">Principal</th>
					<th style="text-align: left;">Interest</th>

					<th style="text-align: left;">Total</th>
				</tr>
			</thead>
			<tbody><?php $t_p = 0;$t_i = 0;$t_t = 0; ?>
				<?php if ($result['rows']): ?>
					<?php foreach ($result['rows'] as $key => $row) { ?>
						<tr>
							<td style="text-align: left;"><?=get_value_field($row['project_id'],'projects','name');?></td>
							<td style="text-align: left;"><?=get_value_field($row['property_id'],'properties','name');?></td>
							<td style="text-align: left;"><?=get_person_name($row['buyer_id'],'buyers');?></td>
							<td style="text-align: left;"><?=get_value_field($row['buyer_id'],'buyers','present_address');?></td>

							<td style="text-align: left;"><?=view_date($row['due_date'])?></td>

							<td style="text-align: left;">
								<?php $t_p += $row['principal_amount']; ?>
								<?=money_php($row['principal_amount'])?>
							</td>

							<td style="text-align: left;">
								<?php $t_i += $row['interest_amount']; ?>
								<?=money_php($row['interest_amount'])?>
							</td>

							<td style="text-align: left;">
								<?php $t_t += $row['total_amount']; ?>
								<?=money_php($row['total_amount'])?>
							</td>

						</tr>
					<?php } ?>
				<?php endif ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5" style="text-align: right">TOTAL</td>
					<td><?=money_php($t_p);?></td>
					<td><?=money_php($t_i);?></td>
					<td><?=money_php($t_t);?></td>
				</tr>
			</tfoot>
		</table>
	<?php elseif(($result['filters']['type'] == "monthly")||($result['filters']['type'] == "yearly")): ?>
		<table class="table table-condensed table-striped">
			<thead>
				<tr>
					<th style="text-align: left;">
						<?php if ($result['filters']['type'] == "yearly"): ?>
							Year
						<?php else: ?>
							Month and Year
						<?php endif; ?>
					</th>
					<th style="text-align: left;">Principal</th>
					<th style="text-align: left;">Interest</th>
					<th style="text-align: left;">Total</th>
				</tr>
			</thead>
			<tbody>

				<?php $t_p = 0;$t_i = 0;$t_t = 0; ?>

				<?php if ($result['months']): ?>
					<?php foreach ($result['months'] as $key => $row) { ?>
						<tr>
							<?php $t_p += $result['rows'][$row][0]['total_principal'];?>
							<?php $t_i += $result['rows'][$row][0]['total_interest'];?>
							<?php $t_t += $result['rows'][$row][0]['t_amount'];?>
							<td style="text-align: left;">
								<?php if ($result['filters']['type'] == "yearly"): ?>
									<?=$row;?>
								<?php else: ?>
									<?=view_date($row,'month_year');?>
								<?php endif; ?>
							</td>
							<td style="text-align: left;">
								<?=money_php($result['rows'][$row][0]['total_principal']);?>
							</td>
							<td style="text-align: left;"><?=money_php($result['rows'][$row][0]['total_interest']);?></td>
							<td style="text-align: left;"><?=money_php($result['rows'][$row][0]['t_amount']);?></td>
						</tr>
					<?php } ?>
				<?php endif ?>

			</tbody>
			<tfoot>
				<tr>
					<td style="text-align: right">TOTAL</td>
					<td><?=money_php($t_p);?></td>
					<td><?=money_php($t_i);?></td>
					<td><?=money_php($t_t);?></td>
				</tr>
			</tfoot>
		</table>
	<?php else: ?>
		<!-- hello world -->
	<?php endif; ?>
	
	
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>
				<td ><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>
</html>