<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border-bottom: 2px solid #00529C;}
		.table > tfoot > tr > td {border-top: 2px solid #00529C;}
		.table > tfoot {border-top:2px solid #DDD;}
		.table > tbody {border-bottom: 1px solid #fff;}

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

	</style>
</head>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<h1 class="color-bluegreen margin0"><?php 
	$company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
	$company_name = get_value_field($company_id,'companies','name');
	echo $company_name;
?></h1>
		<p class="margin0"><?=$data['report']['name']; ?></p>
		<p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p>
		<p>For <?=$date = view_date($result['filters']['daterange'],'range');?></p>
	</div>

	<br />

	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th style="text-align: left;">No.</th>
				<th style="text-align: left;">Buyer</th>
				<th style="text-align: left;">Property</th>
				<th style="text-align: left;">AR Bal Beginning<br><?=$date;?></th>
				<th style="text-align: left;">Collection<br><?=$date;?></th>
				<th style="text-align: left;">AR Bal New Clients #<br><?=$date;?></th>
				<th style="text-align: left;">Cancelled<br><?=$date;?></th>
				<th style="text-align: left;">AR End Balance</th>
			</tr>
		</thead>
		<tbody><?php $t_1 = 0;$t_2 = 0;$t_5 = 0; ?>
			<?php if ($result['payments']): ?>
				<?php foreach ($result['payments'] as $key => $payment) {
					$dates = explode('-', $result['filters']['daterange']);
			        $f_date_from = db_date($dates[0]);
			        $f_date_to  = db_date($dates[1]);
   					$CI = &get_instance();
   					$CI->db->select('transaction_official_payments.*');
   					$CI->db->where('transaction_official_payments.payment_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
   					$CI->db->where('transaction_official_payments.deleted_at IS NULL');
   					$CI->db->where('transaction_official_payments.transaction_id',$payment['transaction_id']);
		 			$off = $CI->db->get('transaction_official_payments')->row_array();
		 			$amt_paid = ($off) ? $off['amount_paid'] : 0 ;
				  ?>
					<tr>
						<?php $t_1 += $payment['beginning_balance']; $t_2 += $amt_paid; ?>

						<td style="text-align: left;"><?=$key += 1;?></td>
						<td style="text-align: left;"><?=get_person_name($payment['buyer_id'],'buyers');?></td>
						<td style="text-align: left;"><?=get_value_field($payment['property_id'],'properties','name');?></td>


						<td style="text-align: left;"><?=money_php($payment['beginning_balance']);?></td>
						<td style="text-align: left;"><?=money_php($amt_paid);?></td>

						
						<td style="text-align: left;"></td>
						<td style="text-align: left;"></td>
						
						<?php $ending_balance = $payment['beginning_balance'] - $amt_paid; $t_5 += $ending_balance; ?>

						<td style="text-align: left;"><?=money_php($ending_balance);?></td>
					</tr>
				<?php } ?>
			<?php endif ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3" style="text-align: right">TOTAL</td>
				<td><?=money_php($t_1);?></td>
				<td><?=money_php($t_2);?></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><?=money_php($t_5);?></td>
			</tr>
		</tfoot>
	</table>
	
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>
				<td ><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>


</body>
</html>