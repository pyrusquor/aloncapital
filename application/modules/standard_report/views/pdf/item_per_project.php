<!DOCTYPE html>
<html>
<head>
	<title><?php echo $data['report']['name']; ?></title>
	<style>
		body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:1.42857143;color:#333;background-color:#fff}
		.table {width: 100%;    border-collapse: collapse; border-spacing: 0;margin-bottom: 15px;}
		.table > thead > tr > th,
		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th
		 {padding: 5px 8px }

		.table > thead > tr > th {border: 1px solid #00529C;}
		/* .table > tfoot > tr > td {border: 1px solid #00529C;} */
		/* .table > tfoot {border-top:1px solid #DDD;} */
		.table > tbody {
            border-left: 1px solid #fff !important;
            border-right: 1px solid #fff !important;
        }

		.table.table-striped > thead > tr > th {background: #00529C;color: #FFF;border: 0;padding: 12px 8px; text-transform: uppercase;}
		.table.table-striped > thead > tr > th a {color:#FFF;font-weight:400}
		.table.table-striped > thead > tr:nth-child(2) > th {background: #0075de;}
		.table.table-striped td {border: 0; vertical-align: middle;}
		.table-striped > tbody > tr:nth-of-type(odd) {background:#FFF}
		.table-striped > tbody > tr:nth-of-type(even) {background: #F1F1F1;}

		.color-bluegreen {color: #169F98;}
		.color-white {color: #fff;}

		.peso_currency { font-family: DejaVu Sans;}

		.bg-bluegreen {background-color:#169F98;}
		.text-center {text-align: center;}
		.text-left {text-align: left;}
		.text-right {text-align: right;}
		.margin0 {margin: 0;}
		.padding10 {padding: 10px;}
		p {margin: 0 0 15px;}

		.alert {
			padding: 15px;
			margin-bottom: 20px;
			border: 1px solid transparent;
			border-radius: 4px;
		}

		.alert-warning {
			background-color: #fcf8e3;
		  border-color: #faebcc;
		  color: #8a6d3b;
		}

	</style>
</head>
<body>
	<?php //vdebug($result); ?>

	<div class="text-center">
		<!-- <h1 class="color-bluegreen margin0"><?php 
            $company_id = get_value_field($result['filters']['project_id'],'projects','company_id');
            $company_name = get_value_field($company_id,'companies','name');
            echo $company_name;
        ?></h1> -->

        <h1 class="color-bluegreen margin0">SHJDC</h1>
        
        <p class="margin0">Rsb Consumption per SBU/Project 2021</p>
		<!-- <p class="margin0"><?=$data['report']['name']; ?></p> -->
		<!-- <p class="margin0"><?=get_value_field($result['filters']['project_id'],'projects','name');?></p> -->
		<!-- <p>For <?=view_date($result['filters']['date']);?></p> -->
	</div>

	<br />
	
	<table class="table table-condensed">
		<thead>
			<tr>
				<th style="text-align: left;" rowspan="2"></th>
				<th style="text-align: left;">JUNE</th>
				<th style="text-align: left;">JULY</th>
				<th style="text-align: left;">AUGUST</th>
				<th style="text-align: left;">SEPTEMBER</th>
				<th style="text-align: left;">OCTOBER</th>
				<th style="text-align: left;">NOVEMBER</th>
				<th style="text-align: left;" rowspan="2">Unit Price</th>
				<th style="text-align: left;" rowspan="2">Total Issuance Price</th>
				<th style="text-align: left;" rowspan="2">Total Cost of RSB Issued January to December 2021</th>
			</tr>
            <tr>
				<th style="text-align: left;">Total Qty Issued (OUT)</th>
				<th style="text-align: left;">Total Qty Issued (OUT)</th>
				<th style="text-align: left;">Total Qty Issued (OUT)</th>
				<th style="text-align: left;">Total Qty Issued (OUT)</th>
				<th style="text-align: left;">Total Qty Issued (OUT)</th>
				<th style="text-align: left;">Total Qty Issued (OUT)</th> 
            </tr>
		</thead>
		<tbody>
            <tr style="background-color: lightgreen !important;">
                <td style="text-align: left;">
                    <strong>ONE WATER FRONT VILLAGE</strong>
                </td>
                <td colspan="9"></td>
            </tr>
			<tr> 
				<td valign="left" align="left">RSB 10mm x 6m</td>
                <td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td align="right" rowspan="4">115,145.49</td>
			</tr>
            <tr> 
                <td valign="left" align="left">RSB 10mm x 6m (GR.33)</td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
			</tr>
            <tr>
                <td valign="left" align="left">RSB 12mm x 6m (GR. 33)</td> 
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
			</tr>
            <tr style="color: red; font-weight: bold;">
				<td> TOTAL</td>
				<td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
			</tr>
            <tr>
				<td colspan="10">&nbsp;</td>
			</tr>

            <tr style="background-color: lightgreen !important;">
                <th style="text-align: left;">
                    <strong>THR</strong>
                </th>
                <th colspan="9"></th>
            </tr>
			<tr> 
				<td valign="left" align="left">RSB 10mm x 6m</td>
                <td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
                <td align="right" rowspan="4">115,145.49</td>
			</tr>
            <tr> 
                <td valign="left" align="left">RSB 10mm x 6m (GR.33)</td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
			</tr>
            <tr>
                <td valign="left" align="left">RSB 12mm x 6m (GR. 33)</td> 
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
			</tr>
            <tr style="color: red; font-weight: bold;">
				<td> TOTAL</td>
				<td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
			</tr>
            <tr>
				<td colspan="10">&nbsp;</td>
			</tr>

            <tr style="background-color: lightgreen !important;">
                <th style="text-align: left;">
                    <strong>BPO</strong>
                </th>
                <th colspan="9"></th>
            </tr>
			<tr> 
				<td valign="left" align="left">RSB 10mm x 6m</td>
                <td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td align="right" rowspan="4">115,145.49</td>
			</tr>
            <tr> 
                <td valign="left" align="left">RSB 10mm x 6m (GR.33)</td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
			</tr>
            <tr>
                <td valign="left" align="left">RSB 12mm x 6m (GR. 33)</td> 
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
				<td valign="left" align="left"> Test </td>
			</tr>
            <tr style="color: red; font-weight: bold;">
				<td> TOTAL</td>
				<td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
			</tr>
            <tr>
				<td colspan="10">&nbsp;</td>
			</tr>

            <tr style="background-color: yellow; color: red; font-weight: bold;">
				<td>OVERALL TOTAL</td>
				<td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right">6,250,368.96</td>
			</tr>
		</tbody>
	</table>
	
	
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed: </strong><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong><?=get_person_name($signatory,'staff');?></td>
					<?php } ?>
				<?php endif ?>
			</tr>
		</tbody>
	</table>


</body>
</html>