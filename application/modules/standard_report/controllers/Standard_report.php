<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Standard_report extends MY_Controller
{

	public $fields = [
		'slug' => array(
			'field' => 'slug',
			'label' => 'Template Name',
			'rules' => 'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Standard_report_model', 'M_report');
		$this->load->model('Standard_report_log_model', 'M_report_logs');
		$this->load->model('material_issuance/Material_issuance_model', 'M_Material_issuance');

		$this->load->model('aris/Aris_report_model', 'M_aris_report');
		$this->load->model('seller_group/Seller_group_model', 'M_Seller_group');



		$transaction_models = array('transaction/Transaction_billing_model' => 'M_transaction_billing', 'transaction/Transaction_client_model' => 'M_transaction_client', 'transaction/Transaction_discount_model' => 'M_transaction_discount', 'transaction/Transaction_fee_model' => 'M_transaction_fee', 'transaction/Transaction_payment_model' => 'M_transaction_payment', 'transaction/Transaction_property_model' => 'M_transaction_property', 'transaction/Transaction_seller_model' => 'M_transaction_seller', 'transaction_commission/Transaction_commission_model' => 'M_Transaction_commission', 'ticketing/Ticketing_model' => 'M_Transaction_ticketing', 'transaction_post_dated_check/Transaction_post_dated_check_model' => 'M_transaction_post_dated_check', 'transaction_payment/Transaction_official_payment_model' => 'M_Transaction_official_payment', 'seller/Seller_model' => 'M_seller', 'buyer/Buyer_model' => 'M_buyer', 'transaction/Transaction_billing_logs_model' => 'M_Tbilling_logs', 'transaction_payment/Transaction_penalty_logs_model' => 'M_tpenalty_logs', 'transaction/Transaction_refund_logs_model' => 'M_refund_logs', 'project/Project_model' => 'M_Project');

		// Load models
		$this->load->model('transaction/Transaction_model', 'M_transaction');
		$this->load->model($transaction_models);
	}

	public function index()
	{
		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index');
	}

	public function update($id = FALSE)
	{
		if ($id) {

			$this->view_data['form']  = $this->M_report->get($id);
			if ($post = $this->input->post()) {

				$this->form_validation->set_rules($this->fields);

				if ($this->form_validation->run() === TRUE) {
					$signatory = $post['signatory'];

					$data['signatory'] = json_encode($post['signatory']);
					$data['filters'] =  json_encode($post['filters']);
					$data['slug'] =  $post['slug'];
					$data['standard_report_id'] =  $post['standard_report_id'];
					$data['created_at'] =  NOW;
					$data['created_by'] =  $this->user->id;

					$_ar_data['company_id'] =  $post['company_id'];

					if ($post['company_id']) {

						$this->M_report->update($_ar_data, $id);
					}

					$result = $this->M_report_logs->insert($data);

					if ($result !== FALSE) {

						redirect('standard_report/generate/' . $result, 'refresh');
					}
				} else {

					// Error
					$this->notify->error('Oops something went wrong.');
				}
			}

			$this->template->build('update', $this->view_data);
		} else {

			show_404();
		}
	}

	public function view($id = FALSE, $debug = FALSE)
	{
		if ($id) {

			$data['standard_report'] = $this->M_report->with_logs("order_by:id,desc")->get($id);

			$this->template->build('view', $data);

			// vdebug($data);

		}
	}

	public function generate($id = FALSE, $debug = 0)
	{
		if ($id) {

			$this->view_data['data'] = $data =  $this->M_report_logs->with_report()->get($id);
			// vdebug(Dropdown::get_static('payment_types', '', 'form'));

			if ($data) {

				switch ($data['slug']) {
					case 'collection_report':
						$this->view_data['result'] = $this->collection_report($data);
						break;
					case 'collection_report_per_transaction':
						$this->view_data['result'] = $this->collection_report_per_transaction($data);
						break;
					case 'accounts_receivable_report':
						$this->view_data['result'] = $this->accounts_receivable_report($data);
						break;

					case 'past_due_accounts':
						$this->view_data['result'] = $this->past_due_accounts($data);
						break;
					case 'fully_paid_accounts':
						$this->view_data['result'] = $this->fully_paid_accounts($data);
						break;
					case 'collectible_report':
						$this->view_data['result'] = $this->collectible_report($data);
						break;
					case 'aging_of_accounts':
						$this->view_data['result'] = $this->aging_of_accounts($data);
						break;
					case 'lot_inventory':
						$this->view_data['result'] = $this->lot_inventory($data);
						break;
					case 'ar_principal_monitoring':
						$this->view_data['result'] = $this->ar_principal_monitoring($data);
						break;
					case 'downpayment_monitoring':
						$this->view_data['result'] = $this->downpayment_monitoring($data);
						break;
					case 'paid_up_sales':
						$this->view_data['result'] = $this->paid_up_sales($data);
						break;
					case 'monthly_unit_inventory':
						$this->view_data['result'] = $this->monthly_unit_inventory($data);
						break;
					case 'transferred_title_report':
						$this->view_data['result'] = $this->transferred_title_report($data);
						break;
					case 'net_reservation_sales':
						$this->view_data['result'] = $this->net_reservation_sales($data);
						break;
					case 'collection_monitoring':
						$this->view_data['result'] = $this->collection_monitoring($data);
						break;
					case 'projects_by_tcp':
						$this->view_data['result'] = $this->projects_by_tcp($data);
						break;
					case 'actual_collection_report':
						$this->view_data['result'] = $this->actual_collection_report($data);
						break;
					case 'sales_report':
						$this->view_data['result'] = $this->sales_report($data);
						break;
					case 'aris_ar_report':
						$this->view_data['result'] = $this->aris_ar_report($data);
						break;
					case 'aris_cancellation_report':
						$this->view_data = $this->aris_cancellation_report($data);
						break;
					case 'aris_collectibles':
						$this->view_data = $this->aris_collectibles($data);
						break;
					case 'aris_collection_report':
						$this->view_data = $this->aris_collection_report($data);
						break;
					case 'aris_customer_directory':
						$this->view_data = $this->aris_customer_directory($data);
						break;
					case 'aris_lot_inventory':
						$this->view_data = $this->aris_lot_inventory($data);
						break;
					case 'aris_past_due_accounts':
						$this->view_data = $this->aris_past_due_accounts($data);
						break;
					case 'material_issuance':
						$this->view_data = $this->material_issuance_report($data);
						break;
					default:
						# code...
						break;
				}

				if ($debug) {
					vdebug($this->view_data);
				}

				$generateHTML = $this->load->view('pdf/' . $data['slug'], $this->view_data, true);

				echo $generateHTML;
				die();

				pdf_create($generateHTML, 'generated_form');
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function material_issuance_report($data = array()){
		$data['filters'] = $params = (array) json_decode($data['filters']);
		if($params['project_id']){
			$project_name = Dropdown::get_dynamic('projects', $params['project_id'], 'name', 'id', 'view');
			$this->db->where("issued_to = '$project_name'");
		}
		if($params['month_year']){
			$exploded_date = explode('-',$params['month_year']);
			$year = isset($exploded_date[0]) ? $exploded_date[0] : '';
			$month = isset($exploded_date[1]) ? $exploded_date[1] : '';
			if($year){
				$this->db->where("year(created_at) = $year");
			}
			if($month){
				$this->db->where("month(created_at) = $month");
			}
		}
		$data['material_issuances'] = $this->M_Material_issuance->with_items(['with'=>['relation'=>'material_receiving_item']])->get_all();
		return $data;
	}                                                                                                                       

	public function collection_report($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		// $data['checks'] = $this->M_report->collection_report($params);
		$ar_clearing_status = Dropdown::get_static('ar_clearing_status');

		array_shift($ar_clearing_status);

		foreach($ar_clearing_status as $key=>$value){
			$ar_clearing = $this->M_report->get_ar_clearing_count($params,$value);
			$data['ar_clearing'][$ar_clearing_status[$key]] = $ar_clearing;
		}


		$data['payments'] = $this->M_report->collection_report($params);

		// vdebug($data['payments']);
		return $data;
	}

	public function collection_report_per_transaction($data = array()){

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];
		$params['group_by_transaction'] = 1;

		// $data['checks'] = $this->M_report->collection_report($params);
		$ar_clearing_status = Dropdown::get_static('ar_clearing_status');
		
		array_shift($ar_clearing_status);

		foreach ($ar_clearing_status as $key => $value) {
			$ar_clearing = $this->M_report->get_ar_clearing_count($params, $value);
			$data['ar_clearing'][$ar_clearing_status[$key]] = $ar_clearing;
		}


		$data['payments'] = $this->M_report->collection_report($params);

		return $data;
	}


	public function accounts_receivable_report($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		$data['payments'] = $this->M_report->ar_report($params);

		// vdebug($data['payments']);

		return $data;
	}

	public function past_due_accounts($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		$data['payments'] = $this->M_report->past_due_accounts($params);

		// vdebug($data['payments']);

		return $data;
	}

	public function fully_paid_accounts($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		$data['rows'] = $this->M_report->fully_paid_accounts($params);

		// vdebug($data['payments']);

		return $data;
	}

	public function collectible_report($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		if ($params['collectible_type'] == "monthly") {

			$dates = explode('-', $params['daterange']);
			$date_from = db_date($dates[0]);
			$date_to  = db_date($dates[1]);
			$period = $this->M_report->get_periods($date_from, $date_to);

			$months = array();

			foreach ($period as $key => $dt) {
				$months[] = $dt->format("Y-m");
			}

			unset($params['daterange']);

			foreach ($months as $date) {
				$params['year_month'] = $date;
				$data['rows'][$date] = $this->M_report->collectible_report($params);
			}

			$data['months'] = $months;
		} else if ($params['collectible_type'] == "yearly") {

			$dates = explode('-', $params['daterange']);
			$date_from = db_date($dates[0]);
			$date_to  = db_date($dates[1]);
			$period = $this->M_report->get_year_periods($date_from, $date_to);

			$years = array();

			foreach ($period as $key => $dt) {
				$years[] = $dt->format("Y");
			}

			unset($params['daterange']);

			foreach ($years as $date) {
				$params['year'] = $date;
				$data['rows'][$date] = $this->M_report->collectible_report($params);
			}

			$data['months'] = $years;
		} else {
			$data['rows'] = $this->M_report->collectible_report($params);
		}


		return $data;
	}

	public function aging_of_accounts($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		$data['payments'] = $this->M_report->aging_of_accounts($params);

		// vdebug($data['payments']);

		return $data;
	}

	public function lot_inventory($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		$data['rows'] = $this->M_report->lot_inventory($params);

		// vdebug($data['payments']);

		return $data;
	}

	public function ar_principal_monitoring($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		$data['payments'] = $this->M_report->ar_principal_monitoring($params);


		return $data;
	}


	public function downpayment_monitoring($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];

		$date = $params['date'];
		$date_from = db_date($date);
		$date_to  = date("Y-m-d", strtotime($date . " +1 year"));

		$period = $this->M_report->get_periods($date_from, $date_to);


		$months = array();

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}

		$data['payments'] = $this->M_report->downpayment_monitoring($params);
		unset($params['daterange']);

		if ($data['payments']) {
			foreach ($data['payments'] as $key => $payment) {
				$params['transaction_id'] = $payment['transaction_id'];
				$params['period_id'] = 2;
				foreach ($months as $date) {
					$params['year_month'] = $date;
					$data['rows'][$payment['transaction_id']][$date] = $this->M_report->collectible_report($params);
				}
			}
		}

		$data['months'] = $months;

		return $data;
	}

	public function paid_up_sales($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);


		$params = $data['filters'];

		$data['payments'] = $this->M_report->paid_up_sales($params);

		// vdebug($data['payments']);

		return $data;
	}

	public function monthly_unit_inventory($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$data['projects'] = $this->M_Project->as_array()->get_all();
		foreach ($data['projects'] as $key => $project) {
			foreach ($status as $key => $stat) {

				$params['project_id'] = $project['id'];
				$params['status'] = $stat;
				$data['rows'][$project['id']][$stat] = $this->M_report->monthly_unit_inventory($params);
			}
			$data['status'] = $status;
		}

		// vdebug($data['rows']);
		return $data;
	}

	public function transferred_title_report($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);
		$period = $this->M_report->get_periods($date_from, $date_to);

		$months = array();

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}

		unset($params['daterange']);

		$data['months'] = $months;

		$data['projects'] = $this->M_Project->as_array()->get_all();

		foreach ($data['projects'] as $key => $project) {
			foreach ($months as $key => $date) {

				$params['project_id'] = $project['id'];
				$params['year_month'] = $date;

				$data['rows'][$project['id']][$date] = $this->M_report->transferred_title_report($params);
			}
		}

		//vdebug($data['rows']);

		return $data;
	}

	public function net_reservation_sales($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);
		$period = $this->M_report->get_periods($date_from, $date_to);

		$months = array();

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}

		unset($params['daterange']);

		$data['months'] = $months;
		$data['groups'] = $this->M_Seller_group->as_array()->get_all();
		$data['projects'] = $this->M_Project->as_array()->get_all();

		foreach ($data['groups'] as $key => $group) {
			foreach ($months as $key => $date) {

				$params['group'] = $group['id'];
				$params['year_month'] = $date;

				$data['rows'][$group['id']][$date] = $this->M_report->net_reservation_sales($params);
			}
		}

		// vdebug($data['rows']);

		return $data;
	}

	public function collection_monitoring($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];



		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);
		$period = $this->M_report->get_periods($date_from, $date_to);

		$months = array();

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}

		unset($params['daterange']);

		$data['months'] = $months;
		$data['groups'] = Dropdown::get_static('group_type');


		foreach ($months as $key => $date) {

			$params['year_month'] = $date;
			$params['group_by'] = 1;

			$data['rows'][$date] = $this->M_report->collection_report($params);
		}

		// vdebug($data['rows']);

		return $data;
	}

	public function projects_by_tcp($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 4];
		$types = [1, 2, 3, 4];

		$data['projects'] = $this->M_Project->as_array()->get_all();

		foreach ($types as $key => $type) {

			foreach ($data['projects'] as $key => $project) {

				$e = "";


				foreach ($status as $key => $stat) {

					$v = "";
					$params['project_id'] = $project['id'];
					$params['status'] = $stat;
					$params['sub_type_id'] = $type;

					$v = $this->M_report->monthly_unit_inventory($params);

					if ($v) {
						$e = 1;
					};

					$data['rows'][$type][$project['id']][$stat] = $v;
				}

				if (empty($e)) {
					unset($data['rows'][$type][$project['id']]);
				};
			}
		}

		$data['status'] = $status;
		$data['types'] = $types;

		// vdebug($data['rows']);
		return $data;
	}


	public function actual_collection_report($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$periods = [1, 2, 3];
		$types = [1, 2, 3, 4];


		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);
		$period = $this->M_report->get_periods($date_from, $date_to);

		$months = array();

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}

		unset($params['daterange']);

		foreach ($months as $date) {

			foreach ($types as $key => $type) {

				foreach ($periods as $key => $period) {

					$e = "";

					$v = "";
					$params['project_id'] = $period;
					$params['sub_type_id'] = $type;
					$params['year_month'] = $date;
					$params['group_by'] = 1;

					$v = $this->M_report->collection_report($params);

					if ($v) {
						$e = 1;
					};

					$data['rows'][$date][$type][$period] = $v;

					// if( empty($e) ){ unset($data['rows'][$type][$period]); };


				}
			}
		}

		$data['periods'] = $periods;
		$data['types'] = $types;
		$data['months'] = $months;

		// vdebug($data['rows']);
		return $data;
	}

	public function aris_ar_report($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);
		$period = $this->M_report->get_periods($date_from, $date_to);

		$months = array();

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}

		unset($params['daterange']);

		$data['months'] = $months;
		$data['groups'] = Dropdown::get_static('group_type');


		foreach ($months as $key => $date) {
			foreach ($data['groups'] as $key => $group) {
				// if(!$key){continue;}

				$params['year_month'] = $date;
				$params['group'] = $key;

				$data['rows'][$date][$group] = $this->M_report->sales_report($params);
			}
		}

		// vdebug($data['rows']);

		return $data;
	}

	public function aris_cancellation_report($data = array())
	{

		// vdebug($data);

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);

		$customer_id = 0;
		$lot_id = 0;
		$report_type = 0;
		$project_id = $data['filters']['project_id'];

		if (isset($data['filters']['customer_id'])) {
			$customer_id = $data['filters']['customer_id'];
		}
		if (isset($data['filters']['lot_id'])) {
			$lot_id = $data['filters']['lot_id'];
		}
		if (isset($data['filters']['report_type'])) {
			$report_type = $data['report_type'] = $data['filters']['report_type'];
		}

		if (isset($_POST) && !empty($_POST)) {

			// $id = $_POST['standard_report_id'];
			$post = $_POST;
			$project_id = $_POST['project_id'];

			$from = $date_from;
			$to = $date_to;

			$doc_type = @$_POST['doc_type'];

			if (isset($_POST['customer_id'])) {
				$customer_id = $_POST['customer_id'];
			}
			if (isset($_POST['lot_id'])) {
				$lot_id = $_POST['lot_no'];
			}
			if (isset($_POST['report_type'])) {
				$report_type = $data['report_type'] = $_POST['report_type'];
			}
		} else {
			$from = date('Y-m-d', strtotime('-1 year'));
			$to = date('Y-m-d');
			$customer_id = 0;
		}

		$data['project_name'] = "ALL";

		if ($project_id) {
			$params['where']['aris_documents.project_id'] = $project_id;
			$params['where']['aris_lots.project_id'] = $project_id;
			$params['where']['aris_customers.project_id'] = $project_id;
			$params['where']['LASTNAME !='] = "";

			$data['project_name'] = get_value_field($project_id, 'aris_project_information', 'ProjectName');
		}



		$params['where']['aris_payments.project_id'] = $project_id;
		$params['where']['aris_documents.DateCancelled !='] = "";

		if ($customer_id) {
			$params['where']['aris_customers.id'] = $customer_id;
		}
		if ($lot_id) {
			$params['where']['aris_lots.id'] = $lot_id;
		}

		$params['sort_order'] = "ASC";
		$params['group_by'] = "aris_payments.DocumentID";
		$params['select'] = 'aris_customers.LASTNAME, aris_customers.FIRSTNAME, aris_customers.MI, aris_lots.Lot_no, aris_documents.DateCancelled, aris_payments.OR_NO, aris_payments.PRINCIPAL, aris_payments.INTEREST, aris_payments.PENALTY, aris_payments.AmountPaid, aris_documents.RA, aris_documents.CTS, aris_documents.DAS, aris_lots.Area, aris_lots.Price';

		// echo "<pre>";print_r($params);print_r($post);die();

		$start = $month = strtotime($from);
		$end = strtotime($to);
		$result = array();
		while ($month < $end) {
			$date = date('Y-m', $month);
			$month = strtotime("+1 month", $month);
			$params['like']['aris_documents.DateCancelled'] = $date;
			$result[$date] = $this->M_aris_report->get_all_cancelled($params);
		}

		$data['results'] = $result;
		$data['fileTitle'] = "Cancellation Report by Customer";
		$filename = "cancelled_report_customer";

		// vdebug($data);
		return $data;
	}


	public function aris_collectibles($data = array())
	{


		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);

		$customer_id = 0;
		$lot_id = 0;
		$report_type = 0;
		$project_id = $data['filters']['project_id'];

		if (isset($data['filters']['customer_id'])) {
			$customer_id = $data['filters']['customer_id'];
		}
		if (isset($data['filters']['lot_id'])) {
			$lot_id = $data['filters']['lot_id'];
		}
		if (isset($data['filters']['report_type'])) {
			$report_type = $data['report_type'] = $data['filters']['report_type'];
		}

		if (isset($_POST) && !empty($_POST)) {
			// $id = $_POST['standard_report_id'];
			$post = $_POST;
			$project_id = $_POST['project_id'];

			$from = $date_from;
			$to = $date_to;

			$doc_type = @$_POST['doc_type'];

			if (isset($_POST['customer_id'])) {
				$customer_id = $_POST['customer_id'];
			}
			if (isset($_POST['lot_id'])) {
				$lot_id = $_POST['lot_no'];
			}
			if (isset($_POST['report_type'])) {
				$report_type = $data['report_type'] = $_POST['report_type'];
			}
		} else {
			$from = date('Y-m-d', strtotime('-1 year'));
			$to = date('Y-m-d');
			$customer_id = 0;
		}

		$data['project_name'] = "ALL";

		if ($project_id) {
			$params['where']['aris_documents.project_id'] = $project_id;
			$params['where']['aris_lots.project_id'] = $project_id;
			$params['where']['aris_customers.project_id'] = $project_id;
			$params['where']['LASTNAME !='] = "";

			$data['project_name'] = get_value_field($project_id, 'aris_project_information', 'ProjectName');
		}

		$params['where']['aris_collectibles.DateDue >='] = date('Y-m-d 00:00:00', strtotime($from));
		$params['where']['aris_collectibles.DateDue <='] = date('Y-m-d 23:59:59', strtotime($to));
		$params['where']['aris_collectibles.project_id'] = $project_id;

		if ($customer_id) {
			$params['where']['customers.id'] = $customer_id;
		}
		if ($lot_id) {
			$params['where']['lots.id'] = $lot_id;
		}

		$params['sort_by'] =  "aris_customers.LASTNAME";
		$params['sort_order'] = "ASC";
		$params['select'] = "aris_customers.LASTNAME, aris_customers.FIRSTNAME, aris_customers.MI, aris_lots.Lot_no, aris_collectibles.DateDue, aris_collectibles.Principal, aris_collectibles.Interest, aris_customers.ADDRESS, aris_customers.CITY, aris_customers.POSTALCODE, aris_customers.PROVINCE, COUNTRY";

		if ($report_type == 'h') { // customer
			$data['fileTitle'] = 'Collectible Report by Customer';
		} else if ($report_type == 'i') { // cts
			echo "ongoing";
			die();
			$data['fileTitle'] = 'Collectible Report by Month';
		} else if ($report_type == 'j') { // lot
			echo "ongoing";
			die();
			$data['fileTitle'] = 'Collectible Report by Year';
		} else if ($report_type == 'k') { // ra
			$params['sort_by'] = "aris_collectibles.DateDue";
			$data['fileTitle'] = 'Collectible Report by Date Due';
		}

		$data['results'] = $this->M_aris_report->get_all_collectibles($params);
		$data['fileTitle'] = "Collectibles Report by Customer";
		$filename = "aris_customer_collectibles";

		return $data;
	}


	public function aris_collection_report($data = array())
	{


		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);

		$customer_id = 0;
		$lot_id = 0;
		$report_type = 0;
		$project_id = $data['filters']['project_id'];

		if (isset($data['filters']['customer_id'])) {
			$customer_id = $data['filters']['customer_id'];
		}
		if (isset($data['filters']['lot_id'])) {
			$lot_id = $data['filters']['lot_id'];
		}
		if (isset($data['filters']['report_type'])) {
			$report_type = $data['report_type'] = $data['filters']['report_type'];
		}

		if (isset($_POST) && !empty($_POST)) {

			// $id = $_POST['standard_report_id'];
			$post = $_POST;
			$project_id = $_POST['project_id'];

			$from = $date_from;
			$to = $date_to;

			$doc_type = @$_POST['doc_type'];

			if (isset($_POST['customer_id'])) {
				$customer_id = $_POST['customer_id'];
			}
			if (isset($_POST['lot_id'])) {
				$lot_id = $_POST['lot_no'];
			}
			if (isset($_POST['report_type'])) {
				$report_type = $data['report_type'] = $_POST['report_type'];
			}
		} else {
			$from = date('Y-m-d', strtotime('-1 year'));
			$to = date('Y-m-d');
			$customer_id = 0;
		}

		$data['project_name'] = "ALL";

		if ($project_id) {
			$params['where']['aris_documents.project_id'] = $project_id;
			$params['where']['aris_lots.project_id'] = $project_id;
			$params['where']['aris_customers.project_id'] = $project_id;
			$params['where']['LASTNAME !='] = "";

			$data['project_name'] = get_value_field($project_id, 'aris_project_information', 'ProjectName');
		}


		$params['where']['aris_payments.DATE_PAID >='] = date('Y-m-d 00:00:00', strtotime($from));
		$params['where']['aris_payments.DATE_PAID <='] = date('Y-m-d 23:59:59', strtotime($to));
		$params['where']['aris_payments.project_id'] = $project_id;
		if ($customer_id) {
			$params['where']['aris_customers.id'] = $customer_id;
		}
		if ($lot_id) {
			$params['where']['aris_lots.id'] = $lot_id;
		}

		$params['sort_by'] = "aris_payments.DATE_PAID";
		$params['sort_order'] = "ASC";
		$params['select'] = "aris_customers.LASTNAME, aris_customers.FIRSTNAME, aris_customers.MI, aris_payments.OR_NO, aris_lots.Lot_no, aris_documents.RESAMT_P, aris_payments.PRINCIPAL, aris_payments.INTEREST, aris_payments.PENALTY, aris_payments.PERIOD, aris_payments.AmountPaid, aris_payments.DATE_PAID";


		$data['results'] = $this->M_aris_report->get_all_collections($params);

		$data['fileTitle'] = "Collection Report by Customer";
		$filename = "aris_customer_payments";

		return $data;
	}


	public function aris_customer_directory($data = array())
	{

		// vdebug($data);

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);

		$customer_id = 0;
		$lot_id = 0;
		$report_type = 0;
		$project_id = $data['filters']['project_id'];

		if (isset($data['filters']['customer_id'])) {
			$customer_id = $data['filters']['customer_id'];
		}
		if (isset($data['filters']['lot_id'])) {
			$lot_id = $data['filters']['lot_id'];
		}
		if (isset($data['filters']['report_type'])) {
			$report_type = $data['report_type'] = $data['filters']['report_type'];
		}

		if (isset($_POST) && !empty($_POST)) {

			// $id = $_POST['standard_report_id'];
			$post = $_POST;
			$project_id = $_POST['project_id'];

			$from = $date_from;
			$to = $date_to;

			$doc_type = @$_POST['doc_type'];

			if (isset($_POST['customer_id'])) {
				$customer_id = $_POST['customer_id'];
			}
			if (isset($_POST['lot_id'])) {
				$lot_id = $_POST['lot_no'];
			}
			if (isset($_POST['report_type'])) {
				$report_type = $data['report_type'] = $_POST['report_type'];
			}
		} else {
			$from = date('Y-m-d', strtotime('-1 year'));
			$to = date('Y-m-d');
			$customer_id = 0;
		}

		$data['project_name'] = "ALL";

		if ($project_id) {
			$params['where']['aris_documents.project_id'] = $project_id;
			$params['where']['aris_lots.project_id'] = $project_id;
			$params['where']['aris_customers.project_id'] = $project_id;
			$params['where']['LASTNAME !='] = "";

			$data['project_name'] = get_value_field($project_id, 'aris_project_information', 'ProjectName');
		}


		unset($params['where']['aris_lots.project_id']);
		unset($params['where']['aris_documents.project_id']);
		if ($customer_id) {
			$params['where']['customers.id'] = $customer_id;
		}
		$params['sort_by'] = "aris_customers.LASTNAME";
		$params['sort_order'] = "ASC";
		$params['select'] = "aris_customers.ADDRESS, aris_customers.CITY, aris_customers.POSTALCODE, aris_customers.LASTNAME, aris_customers.FIRSTNAME, aris_customers.MI, aris_customers.POSTALCODE, aris_customers.PROVINCE, aris_customers.COUNTRY, aris_customers.CEL_NO, aris_customers.PHONE_NO, aris_customers.FAX_NO, aris_customers.EMAIL_ADDR";

		$data['results'] = $this->M_aris_report->get_all_customers($params);
		$data['fileTitle'] = "Customer's Directory";
		$filename = "customer_directory";


		// vdebug($data);
		return $data;
	}


	public function aris_lot_inventory($data = array())
	{

		// vdebug($data);

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];


		if ($params['daterange']) {
			$dates = explode('-', $params['daterange']);
			$date_from = db_date($dates[0]);
			$date_to  = db_date($dates[1]);
		}


		$customer_id = 0;
		$lot_id = 0;
		$report_type = 0;
		$project_id = $data['filters']['project_id'];

		if (isset($data['filters']['customer_id'])) {
			$customer_id = $data['filters']['customer_id'];
		}
		if (isset($data['filters']['lot_id'])) {
			$lot_id = $data['filters']['lot_id'];
		}
		if (isset($data['filters']['report_type'])) {
			$report_type = $data['report_type'] = $data['filters']['report_type'];
		}

		if (isset($_POST) && !empty($_POST)) {

			// $id = $_POST['standard_report_id'];
			$post = $_POST;
			$project_id = $_POST['project_id'];

			$from = @$date_from;
			$to = @$date_to;

			$doc_type = @$_POST['doc_type'];

			if (isset($_POST['customer_id'])) {
				$customer_id = $_POST['customer_id'];
			}
			if (isset($_POST['lot_id'])) {
				$lot_id = $_POST['lot_no'];
			}
			if (isset($_POST['report_type'])) {
				$report_type = $data['report_type'] = $_POST['report_type'];
			}
		} else {
			$from = date('Y-m-d', strtotime('-1 year'));
			$to = date('Y-m-d');
			$customer_id = 0;
		}

		$data['project_name'] = "ALL";

		if ($project_id) {
			$params['where']['aris_documents.project_id'] = $project_id;
			$params['where']['aris_lots.project_id'] = $project_id;
			$params['where']['aris_customers.project_id'] = $project_id;
			$params['where']['LASTNAME !='] = "";

			$data['project_name'] = get_value_field($project_id, 'aris_project_information', 'ProjectName');
		}



		if ($lot_id) {
			$params['where']['lots.id'] = $lot_id;
		}
		if ($customer_id) {
			$params['where']['customers.id'] = $customer_id;
		}

		$params['select'] = "aris_customers.customerID, aris_customers.FIRSTNAME, aris_customers.MI, aris_customers.LASTNAME, aris_lots.Lot_no, aris_lots.Lot_no, aris_lots.Area, aris_lots.Price, aris_lots.Price, aris_lots.Price, aris_documents.RA, aris_documents.CTS";

		if ($report_type == 'd') { // customer
			$params['sort_by'] = "aris_customers.LASTNAME";
			$params['sort_order'] = "ASC";
			$data['fileTitle'] = 'Lot Inventory by Customer';
		} else if ($report_type == 'e') { // cts
			$params['sort_by'] = "aris_documents.CTS";
			$params['sort_order'] = "DESC";
			$params['where']['aris_documents.CTS !='] = "";
			$data['fileTitle'] = 'Lot Inventory by CTS';
		} else if ($report_type == 'f') { // lot
			$params['sort_by'] = "aris_lots.Lot_no";
			$params['sort_order'] = "ASC";
			$data['fileTitle'] = 'Lot Inventory by Lot No';
		} else if ($report_type == 'g') { // ra
			$params['sort_by'] = "aris_documents.RA";
			$params['sort_order'] = "DESC";
			$params['where']['aris_documents.RA'] = NULL;
			$data['fileTitle'] = 'Lot Inventory by RA No';
		}

		$filename = "lot_inventory_customer";

		$data['results'] = $this->M_aris_report->get_all_documents($params);

		return $data;
	}


	public function aris_past_due_accounts($data = array())
	{

		// vdebug($data);

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);

		$customer_id = 0;
		$lot_id = 0;
		$report_type = 0;
		$project_id = $data['filters']['project_id'];

		if (isset($data['filters']['customer_id'])) {
			$customer_id = $data['filters']['customer_id'];
		}
		if (isset($data['filters']['lot_id'])) {
			$lot_id = $data['filters']['lot_id'];
		}
		if (isset($data['filters']['report_type'])) {
			$report_type = $data['report_type'] = $data['filters']['report_type'];
		}

		if (isset($_POST) && !empty($_POST)) {

			// $id = $_POST['standard_report_id'];
			$post = $_POST;
			$project_id = $_POST['project_id'];

			$from = $date_from;
			$to = $date_to;

			$doc_type = @$_POST['doc_type'];

			if (isset($_POST['customer_id'])) {
				$customer_id = $_POST['customer_id'];
			}
			if (isset($_POST['lot_id'])) {
				$lot_id = $_POST['lot_no'];
			}
			if (isset($_POST['report_type'])) {
				$report_type = $data['report_type'] = $_POST['report_type'];
			}
		} else {
			$from = date('Y-m-d', strtotime('-1 year'));
			$to = date('Y-m-d');
			$customer_id = 0;
		}

		$data['project_name'] = "ALL";

		if ($project_id) {
			$params['where']['aris_documents.project_id'] = $project_id;
			$params['where']['aris_lots.project_id'] = $project_id;
			$params['where']['aris_customers.project_id'] = $project_id;
			$params['where']['LASTNAME !='] = "";

			$data['project_name'] = get_value_field($project_id, 'aris_project_information', 'ProjectName');
		}

		$params['where']['aris_collectibles.DateDue >='] = date('Y-m-d 00:00:00', strtotime($from));
		$params['where']['aris_collectibles.DateDue <='] = date('Y-m-d 23:59:59', strtotime($to));
		$params['where']['aris_collectibles.project_id'] = $project_id;

		if ($customer_id) {
			$params['where']['customers.id'] = $customer_id;
		}
		if ($lot_id) {
			$params['where']['lots.id'] = $lot_id;
		}

		$params['where']['aris_documents.DateCancelled'] = NULL;
		$params['sort_order'] = "ASC";
		$filename = "pastdue_report_customer";
		$data['report_type'] = $report_type;

		$params['sort_by'] =  "aris_collectibles.DateDue";
		$params['group_by'] = "aris_collectibles.DocumentID";

		$params['select'] = 'aris_documents.DocumentID,aris_customers.ADDRESS,aris_customers.PROVINCE,
		aris_customers.CITY,aris_customers.LASTNAME, aris_customers.FIRSTNAME, aris_customers.MI, aris_lots.Lot_no, aris_collectibles.DateDue as col_DateDue,  aris_collectibles.PRINCIPAL, aris_collectibles.INTEREST';

		$params['select'] = 'aris_documents.DocumentID,aris_customers.ADDRESS,aris_customers.PROVINCE,
		aris_customers.CITY,aris_customers.LASTNAME, aris_customers.FIRSTNAME, aris_customers.MI, aris_lots.Lot_no, aris_collectibles.DateDue as col_DateDue,  count(aris_collectibles.id) as count_c, sum(aris_collectibles.PRINCIPAL) as sum_p, sum(aris_collectibles.INTEREST) as sum_i, aris_documents.AMORTAMT';

		$table = "aris_collectibles";

		$data['fileTitle'] = "Past Due Accounts";

		if ($report_type == 'b') {
			$data['fileTitle'] = "Past Due Accounts for Amortization";
		} else if ($report_type == 'c') { // Past Due for Legal Notice
			$data['fileTitle'] = "Past Due Accounts for Legal Notice";
		}


		$data['results'] = $this->M_aris_report->get_all_pastdue($params, '', $table);

		// vdebug($data);
		return $data;
	}


	public function sales_report($data = array())
	{

		$data['filters'] = (array) json_decode($data['filters']);
		$data['signatory'] = json_decode($data['signatory']);

		$params = $data['filters'];
		$status = [0, 1, 2, 3, 4];

		$dates = explode('-', $params['daterange']);
		$date_from = db_date($dates[0]);
		$date_to  = db_date($dates[1]);
		$period = $this->M_report->get_periods($date_from, $date_to);

		$months = array();

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}

		unset($params['daterange']);

		$data['months'] = $months;
		$data['groups'] = Dropdown::get_static('group_type');


		foreach ($months as $key => $date) {
			foreach ($data['groups'] as $key => $group) {
				// if(!$key){continue;}

				$params['year_month'] = $date;
				$params['group'] = $key;

				$data['rows'][$date][$group] = $this->M_report->sales_report($params);
			}
		}

		// vdebug($data['rows']);

		return $data;
	}


	public function show()
	{

		$columnsDefault = [
			'id'     => true,
			'name'      => true,
			'description'      => true,
			'created_by' => true,
			'updated_by' => true
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}

		// get all raw data
		$alldata = $this->M_report->as_array()->get_all();

		foreach ($alldata as $key => $value) {

			$alldata[$key]['company_id'] = $value['company']['name'] ?? '';

			$alldata[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

			$alldata[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
		}

		$data = [];

		if ($alldata) {
			// internal use; filter selected columns only from raw data
			foreach ($alldata as $d) {
				$data[] = $this->filterArray($d, $columnsDefault);
			}
		}


		// count data
		$totalRecords = $totalDisplay = count($data);

		// filter by general search keyword
		if (isset($_REQUEST['search'])) {
			$data         = $this->filterKeyword($data, $_REQUEST['search']);
			$totalDisplay = count($data);
		}

		if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
			foreach ($_REQUEST['columns'] as $column) {
				if (isset($column['search'])) {
					$data         = $this->filterKeyword($data, $column['search'], $column['data']);
					$totalDisplay = count($data);
				}
			}
		}

		// sort
		if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
			$column = $_REQUEST['order'][0]['column'];
			$dir    = $_REQUEST['order'][0]['dir'];
			usort($data, function ($a, $b) use ($column, $dir) {
				$a = array_slice($a, $column, 1);
				$b = array_slice($b, $column, 1);
				$a = array_pop($a);
				$b = array_pop($b);

				if ($dir === 'asc') {
					return $a > $b ? true : false;
				}

				return $a < $b ? true : false;
			});
		}

		// pagination length
		if (isset($_REQUEST['length'])) {
			$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
		}

		// return array values only without the keys
		if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
			$tmp  = $data;
			$data = [];
			foreach ($tmp as $d) {
				$data[] = array_values($d);
			}
		}
		$secho = 0;
		if (isset($_REQUEST['sEcho'])) {
			$secho = intval($_REQUEST['sEcho']);
		}

		$output = array(
			'sEcho' => $secho,
			'sColumns' => '',
			'iTotalRecords' => $totalRecords,
			'iTotalDisplayRecords' => $totalDisplay,
			'data' => $data
		);

		echo json_encode($output);
		exit();
	}
}
