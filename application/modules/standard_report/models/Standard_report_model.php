<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Standard_report_model extends MY_Model
{

	public $table = 'standard_reports'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'name',
		'description',
		'content',
		'header',
		'footer',
		'company_id',
		'filters',
		'signatory',
		'slug',
		'created_by',
		'created_at',
		'updated_at',
		'updated_by',
		'deleted_by',
		'deleted_at',
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];
	public $fields = [];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_many['logs']  = array('foreign_model' => 'standard_report_log_model', 'foreign_table' => 'standard_report_logs', 'foreign_key' => 'standard_report_id', 'local_key' => 'id');
	}

	public function insert_default()
	{

		// -- Adminer 4.2.2 MySQL dump

		// SET NAMES utf8;
		// SET time_zone = '+00:00';
		// SET foreign_key_checks = 0;
		// SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

		// INSERT INTO `standard_reports` (`id`, `name`, `description`, `filters`, `signatory`, `slug`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
		// (1,	'Collection Report',	'Collection Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'collection_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (2,	'Accounts Receivable Report',	'Accounts Receivable Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'accounts_receivable_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (3,	'Past Due Accounts',	'Past Due Accounts',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'past_due_accounts',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (4,	'Fully Paid Accounts',	'Fully Paid Accounts',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'fully_paid_accounts',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (5,	'Collectible Report',	'Collectible Report',	'{\"0\":\"project_id\",\"1\":\"daterange\",\"2\":\"collectible_type\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'collectible_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (6,	'Aging of Accounts',	'Aging of Accounts',	'{\"0\":\"project_id\",\"1\":\"date\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'aging_of_accounts',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (7,	'Lot Inventory',	'Lot Inventory',	'{\"0\":\"project_id\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'lot_inventory',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (8,	'AR Principal Monitoring',	'AR Principal Monitoring',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'ar_principal_monitoring',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (9,	'Downpayment Monitoring',	'Downpayment Monitoring',	'{\"0\":\"project_id\",\"1\":\"date\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'downpayment_monitoring',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (10,	'Paid Up Sales',	'Paid Up Sales',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'paid_up_sales',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (11,	'Monthly Unit Inventory Report',	'monthly_unit_inventory',	'{\"0\":\"project_id\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'monthly_unit_inventory',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (12,	'Transferred Title Report',	'Transferred Title Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'transferred_title_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (13,	'Net Reservation Sales Monitoring',	'Net Reservation Sales Monitoring',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'net_reservation_sales',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (14,	'Collection Monitoring',	'Collection Monitoring',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'collection_monitoring',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (15,	'Projects by TCP',	'Projects by TCP',	'{\"0\":\"project_id\",\"1\":\"sub_type\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'projects_by_tcp',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (16,	'Actual Collection Report',	'Actual Collection Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'actual_collection_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (17,	'Sales Report',	'Sales Report',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'sales_report',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL),
		// (18,	'Summary of Current and Non Current Accounts Receivable',	'Summary of Current and Non Current Accounts Receivable',	'{\"0\":\"project_id\",\"1\":\"daterange\"}',	'{\"0\":\"prepared_id\",\"1\":\"checked_id\",\"2\":\"approved_id\"}',	'current_nc_ar',	'2020-05-07 11:12:52',	1,	NULL,	NULL,	NULL,	NULL);

		// -- 2020-05-10 11:44:50

	}

	public function get_day_periods($start, $end)
	{

		$dstart = date("Y-m-d", strtotime($start));
		$dend = date("Y-m-d", strtotime($end));

		$diff = date_converter($dstart, $dend);
		$count = 0;
		$rem = 0;

		if ($diff->format("%a") > 0) {
			$count =  $diff->format("%a");
		}

		$count = (int) $count;

		for ($i = 0; $i <= $count; $i++) {

			$date_initial = date('Y-m-d', strtotime($dstart . ' +' . $i . ' day'));
			$months[] = $date_initial;
		}

		return $months;
	}

	public function get_week_periods($start, $end)
	{
		$start_day = date('Y-m-d');
		$dstart = date("Y-m-d", strtotime($start));
		$dend = date("Y-m-d", strtotime($end));

		$diff = date_converter($dstart, $dend);
		$count = 0;
		$rem = 0;

		if ($diff->format("%a") > 0) {
			$count =  $diff->format("%a") / 7;
		}

		$count = (int) $count;
		$start_day = $dstart;

		for ($i = 0; $i <= $count; $i++) {
			$days1 = 7 * $i;
			$days2 = (7 * $i) + 7;

			$date_initial = date('Y/m/d', strtotime($start_day . "+" . $days1 . " days"));
			$date_til =  date('Y/m/d', strtotime($start_day . "+" . $days2 . " days"));

			$months[] = $date_initial . "-" . $date_til;
		}

		return $months;
	}

	public function get_periods($start, $end)
	{
		$start 		= new DateTime($start);
		$start->modify('first day of this month');
		$end 		= new DateTime($end);
		$end->modify('first day of next month');
		$interval 	= DateInterval::createFromDateString('1 month');
		$period 	= new DatePeriod($start, $interval, $end);

		return $period;
	}

	public function get_year_periods($start, $end)
	{
		$start 		= new DateTime($start);
		$start->modify('first day of this year');
		$end 		= new DateTime($end);
		$end->modify('first day of next year');
		$interval 	= DateInterval::createFromDateString('1 year');
		$period 	= new DatePeriod($start, $interval, $end);

		return $period;
	}

	public function collection_report($params = [], $count = false)
	{
		$this->db->select('
			transaction_official_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transactions.buyer_id as buyer_id,
			transactions.reservation_date,
		');


		if (!empty($params['group_by'])) {
			$this->db->select('
				SUM(transaction_official_payments.principal_amount) as total_principal,
				SUM(transaction_official_payments.interest_amount) as total_interest,
				SUM(transaction_official_payments.amount_paid) as t_amount,
				COUNT(transaction_official_payments.id) as t_count,
			', false);

			$this->db->group_by('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m")');
		}

		$this->db->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('transaction_payments', 'transaction_payments.id = transaction_official_payments.transaction_payment_id', 'left');


		if (!empty($params['sub_type_id'])) {
			// $this->db->join('properties', 'projects.id = properties.project_id', 'left');
			$this->db->join('house_models', 'house_models.id = transaction_properties.house_model_id', 'left');
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);
		}

		if (!empty($params['period_id'])) {
			$this->db->where('transaction_official_payments.period_id', $params['period_id']);
		}

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_official_payments.payment_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_official_payments.payment_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['reservation_daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transactions.reservation_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		if (!empty($params['group_by_transaction'])) {

			$this->db->select('
				SUM(transaction_official_payments.principal_amount) as total_principal,
				SUM(transaction_official_payments.interest_amount) as total_interest,
				SUM(transaction_official_payments.penalty_amount) as total_penalty,
				SUM(transaction_official_payments.rebate_amount) as total_rebate,
				SUM(transaction_official_payments.amount_paid) as t_amount,
				COUNT(transaction_official_payments.id) as t_count,
			', false);


			$this->db->group_by('transaction_official_payments.transaction_id');

		}

		if (isset($params['sort_by']) && count($params['sort_by'])) {

			foreach ($params['sort_by'] as $sort_by) {

				switch ($sort_by) {
					case 'payment_date':
						$this->db->order_by('transaction_official_payments.payment_date', $params['sort_direction']);
						break;
					case 'receipt_date':
						$this->db->order_by('transaction_official_payments.or_date', $params['sort_direction']);
						break;
					case 'receipt_number':
						// $this->db->order_by('transaction_official_payments.or_number', $params['sort_direction']);
						$this->db->order_by('transaction_official_payments.or_number', 'DESC');
						break;
					case 'customer':
						$this->db->join('buyers', 'transactions.buyer_id = buyers.id', 'left');
						$this->db->order_by('buyers.last_name', $params['sort_direction']);
						break;
					case 'property':
						$this->db->join('properties', 'transactions.property_id = properties.id', 'left');
						$this->db->order_by('properties.name', $params['sort_direction']);
						break;
					case 'period_id':
						$this->db->order_by('transaction_official_payments.period_id', $params['sort_direction']);
						break;
					case 'project':
						$this->db->join('projects', 'transactions.project_id = projects.id', 'left');
						$this->db->order_by('projects.name', $params['sort_direction']);
						break;
					case 'due_date':
						$this->db->order_by('transactions.due_date', $params['sort_direction']);
						break;
				}
			}
		} else {

			$this->db->order_by('payment_date', 'ASC');
			$this->db->order_by('or_number', 'ASC');
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_official_payments.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transaction_official_payments')->result_array();
	}

	public function ar_report($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			buyers.last_name,
			transactions.buyer_id,
			transactions.seller_id,
			transactions.financing_scheme_id,
			transactions.reference,
			transactions.reservation_date,
		');

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('buyers', 'transactions.buyer_id = buyers.id', 'left');

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['transaction_id'])) {
			$this->db->where('transaction_payments.transaction_id', $params['transaction_id']);
		}

		// if (!empty($params['daterange'])) {
		// 	$dates = explode('-', $params['daterange']);
		// 	$f_date_from = db_date($dates[0]);
		// 	$f_date_to  = db_date($dates[1]);
		// 	$this->db->where('transaction_payments.due_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		// }

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		// $this->db->where('transaction_payments.is_paid', 1);
		$this->db->where('transaction_payments.is_complete', 0);

		$this->db->order_by('buyers.last_name', 'ASC');
		$this->db->order_by('transaction_payments.due_date', 'ASC');

		$this->db->group_by('transaction_payments.transaction_id');
		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');
		$this->db->where('transactions.general_status !=', 4);
		$this->db->where('transactions.period_id >', 2);

		return $this->db->get('transaction_payments')->result_array();
	}


	public function past_due_accounts($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transactions.buyer_id,
		');

		$this->db->select('
			SUM(transaction_payments.principal_amount) as total_principal,
			SUM(transaction_payments.interest_amount) as total_interest,
			SUM(transaction_payments.total_amount) as t_amount,
			COUNT(transaction_payments.id) as t_count,
		', false);

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['date'])) {

			$this->db->where('transaction_payments.due_date < ', $params['date']);
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		$this->db->where('transaction_payments.is_paid', 0);

		$this->db->group_by('transaction_payments.transaction_id');

		return $this->db->get('transaction_payments')->result_array();
	}


	public function fully_paid_accounts($params = [], $count = false)
	{
		$this->db->select('
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transactions.reference,
			properties.cts_number,
			properties.doas_notarized_date,
			properties.lot_area,
		');

		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('properties', 'properties.id = transaction_properties.property_id', 'left');

		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			// $this->db->where('transaction_payments.due_date BETWEEN "'. $f_date_from .'" AND "'. $f_date_to .'"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		// $this->db->where('transactions.collection_status', 3);

		return $this->db->get('transactions')->result_array();
	}

	public function aging_of_accounts($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transactions.buyer_id,
			transactions.seller_id,
			transactions.reference,
		');

		$this->db->select('
			SUM(transaction_payments.principal_amount) as total_principal,
			SUM(transaction_payments.interest_amount) as total_interest,
			SUM(transaction_payments.total_amount) as t_amount,
			COUNT(transaction_payments.id) as t_count,

		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 1 AND 30, SUM(transaction_payments.principal_amount), 0) as total_principala,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 31 AND 60, SUM(transaction_payments.principal_amount), 0) as total_principalb,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 61 AND 90, SUM(transaction_payments.principal_amount), 0) as total_principalc,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) > 90, SUM(transaction_payments.principal_amount), 0) as total_principald,

		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 1 AND 30, SUM(transaction_payments.interest_amount), 0) as total_interesta,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 31 AND 60, SUM(transaction_payments.interest_amount), 0) as total_interestb,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 61 AND 90, SUM(transaction_payments.interest_amount), 0) as total_interestc,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) > 90, SUM(transaction_payments.interest_amount), 0) as total_interestd,


		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 1 AND 30, SUM(transaction_payments.total_amount), 0) as total_amounta,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 31 AND 60, SUM(transaction_payments.total_amount), 0) as total_amountb,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) BETWEEN 61 AND 90, SUM(transaction_payments.total_amount), 0) as total_amountc,
		   	IF(DATEDIFF(NOW(), transaction_payments.due_date) > 90, SUM(transaction_payments.total_amount), 0) as total_amountd
		', false);

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');


		if (!empty($params['date'])) {

			$this->db->where('transaction_payments.due_date < ', $params['date']);
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		$this->db->where('transaction_payments.is_paid', 0);

		$this->db->where('transactions.general_status !=',4);

		$this->db->order_by('transaction_properties.project_id', 'ASC');

		$this->db->group_by('transaction_payments.transaction_id');

		return $this->db->get('transaction_payments')->result_array();
	}

	public function collectible_report($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transactions.seller_id,
			transactions.financing_scheme_id,
			transactions.reference,
			transactions.reservation_date,
		');

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
			$this->db->select('
				SUM(transaction_payments.principal_amount) as total_principal,
				SUM(transaction_payments.interest_amount) as total_interest,
				SUM(transaction_payments.total_amount) as t_amount,
				COUNT(transaction_payments.id) as t_count,
			', false);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
			$this->db->select('
				SUM(transaction_payments.principal_amount) as total_principal,
				SUM(transaction_payments.interest_amount) as total_interest,
				SUM(transaction_payments.total_amount) as t_amount,
				COUNT(transaction_payments.id) as t_count,
			', false);
		}

		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_payments.due_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		if (!empty($params['transaction_id'])) {
			$this->db->where('transaction_payments.transaction_id', $params['transaction_id']);
		}

		if (!empty($params['period_id'])) {
			$this->db->where('transaction_payments.period_id', $params['period_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transaction_payments')->result_array();
	}


	public function lot_inventory($params = [], $count = false)
	{
		$this->db->select('
			properties.*,
			transactions.buyer_id,
			buyers.*
		');

		$this->db->join('transaction_properties', 'transaction_properties.property_id = properties.id', 'left');
		$this->db->join('transactions', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('buyers', 'transactions.buyer_id = buyers.id', 'left');


		if (!empty($params['project_id'])) {
			$this->db->where('properties.project_id', $params['project_id']);
		}
		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transactions.reservation_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('buyers.deleted_at IS NULL');

		return $this->db->get('properties')->result_array();
	}


	public function ar_principal_monitoring($params = [], $count = false)
	{
		$this->db->select('
			transaction_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transaction_payments.beginning_balance,
		');

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_payments.due_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transaction_payments')->result_array();
	}

	public function downpayment_monitoring($params = [], $count = false)
	{
		$this->db->select('
			transaction_properties.project_id,
			transaction_properties.property_id,
			transaction_properties.total_contract_price,
			transactions.buyer_id,
			transactions.seller_id,
			transactions.id as transaction_id,  
			transactions.financing_scheme_id,
			transactions.reservation_date,
			properties.lot_area,
			properties.floor_area,
			properties.model_id,
		');

		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('properties', 'properties.id = transaction_properties.property_id', 'left');
		// $this->db->join('transaction_payments', 'transaction_payments.id = transaction_official_payments.transaction_payment_id', 'left');

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		// $this->db->where('transaction_payments.deleted_at IS NULL');

		$this->db->where('transactions.general_status', 2);
		$this->db->where('transactions.period_id', 2);

		return $this->db->get('transactions')->result_array();
	}

	public function paid_up_sales($params = [], $count = false)
	{
		$this->db->select(
			'transaction_official_payments.*,
			transaction_properties.project_id,
			transaction_properties.property_id,
			transactions.buyer_id'
		);

		$this->db->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('transaction_payments', 'transaction_payments.id = transaction_official_payments.transaction_payment_id', 'left');

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_official_payments.payment_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_official_payments.payment_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_official_payments.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');

		return $this->db->get('transaction_official_payments')->result_array();
	}

	public function monthly_unit_inventory($params = [], $count = false)
	{
		$this->db->select('
			projects.name as project_name,
			COUNT(properties.id) as total_count,
			SUM(properties.lot_area) as total_lot,
			SUM(properties.total_selling_price) as total_pesos,
		', FALSE);

		$this->db->join('properties', 'projects.id = properties.project_id', 'left');

		if (!empty($params['sub_type_id'])) {
			$this->db->join('house_models', 'house_models.id = properties.model_id', 'left');
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);
		}

		if (!empty($params['project_id'])) {
			$this->db->where('projects.id', $params['project_id']);
		}

		if (!empty($params['status'])) {
			$this->db->where('properties.status', $params['status']);
		}

		$this->db->where('properties.deleted_at IS NULL');
		$this->db->where('projects.deleted_at IS NULL');

		$this->db->group_by('properties.project_id');

		return $this->db->get('projects')->row_array();
	}

	public function transferred_title_report($params = [], $count = false)
	{
		$this->db->select('
			projects.name as project_name,
			COUNT(properties.id) as total_count,
		', FALSE);

		$this->db->join('properties', 'projects.id = properties.project_id', 'left');

		if (!empty($params['project_id'])) {
			$this->db->where('projects.id', $params['project_id']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(properties.date_registered, "%Y-%m") = ', $params['year_month']);
		}

		$this->db->where('properties.deleted_at IS NULL');
		$this->db->where('projects.deleted_at IS NULL');

		$this->db->group_by('properties.project_id');

		return $this->db->get('projects')->row_array();
	}

	public function net_reservation_sales_($params = [], $count = false)
	{
		$this->db->select('
			projects.name as project_name,
			COUNT(properties.id) as total_count,
		', FALSE);

		$this->db->join('properties', 'projects.id = properties.project_id', 'left');

		if (!empty($params['project_id'])) {
			$this->db->where('projects.id', $params['project_id']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(properties.date_registered, "%Y-%m") = ', $params['year_month']);
		}

		$this->db->where('properties.deleted_at IS NULL');
		$this->db->where('projects.deleted_at IS NULL');

		$this->db->group_by('properties.project_id');

		return $this->db->get('projects')->row_array();
	}

	public function sales_report($params = [], $count = false)
	{	
		if (!empty($params['select'])) {
			if ($params['select'] == "property_name") {
				$this->db->select('properties.name', false);
			}
		} else {
			$this->db->select('
				SUM(transaction_properties.total_selling_price) as total_selling_price,
				COUNT(transactions.id) as t_count,
			', false);
		}

		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('properties', 'properties.id = transaction_properties.property_id', 'left');

		if (!empty($params['group'])) {
			$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			$this->db->where('sellers.sales_group_id', $params['group']);
		}

		if (!empty($params['position'])) {
			if (empty($params['group'])) {
				$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			}
			$this->db->where('sellers.seller_position_id', $params['position']);
		}



		if (!empty($params['tops'])) {
			$this->db->select('
				sellers.first_name, sellers.last_name, sellers.sales_group_id, seller_positions.name as sp_name, sellers.id as id
			', false);
			$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			$this->db->join('seller_positions', 'seller_positions.id = sellers.seller_position_id', 'left');
			$this->db->group_by('transactions.seller_id');
			$this->db->order_by('total_selling_price', 'DESC');
			if ($params['general_limit']) {
				$this->db->limit(3);
			} else {
				$this->db->limit(10);
			}

			if ($params['project_id']) {
				$this->db->where('transactions.project_id', $params['project_id']);
			}
		}

		if (!empty($params['sub_type_id'])) {
			// $this->db->join('properties', 'projects.id = properties.project_id', 'left');
			$this->db->join('house_models', 'house_models.id = transaction_properties.house_model_id', 'left');
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);
		}

		if (!empty($params['date'])) {
			$this->db->where('DATE(transactions.reservation_date) = ', $params['date']);
		}

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transactions.reservation_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transactions.reservation_date, "%Y-%m") = ', $params['year_month']);
		}
		if (!empty($params['year_month_range'])) {
			$this->db->where("DATE_FORMAT(transactions.reservation_date, '%Y-%m') between '" .  $params['year_month_range'][0] . "' AND '" . $params['year_month_range'][1] . "'");
		}

		if (!empty($params['transaction_id'])) {
			$this->db->where('transactions.transaction_id', $params['transaction_id']);
		}


		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transactions.reservation_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transactions.general_status !=',4);
		$this->db->where('transaction_properties.deleted_at IS NULL');


		return $this->db->get('transactions')->result_array();
	}
	public function net_reservation_sales($params = [], $count = false)
	{
		$this->db->select('
			SUM(transaction_properties.total_selling_price) as total_selling_price,
			SUM(CASE WHEN transactions.general_status = 4 THEN transaction_properties.total_selling_price END) cancelled_tsp,
			COUNT(transactions.id) as t_count,
		', false);

		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');

		if (!empty($params['group'])) {
			$this->db->where('transactions.sales_group_id', $params['group']);
		}

		if (!empty($params['date'])) {
			$this->db->where('DATE(transactions.reservation_date) = ', $params['date']);
		}

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transactions.reservation_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transactions.reservation_date, "%Y-%m") = ', $params['year_month']);
		}
		if (!empty($params['year_month_range'])) {
			$this->db->where("DATE_FORMAT(transactions.reservation_date, '%Y-%m') between '" .  $params['year_month_range'][0] . "' AND '" . $params['year_month_range'][1] . "'");
		}

		if (!empty($params['transaction_id'])) {
			$this->db->where('transactions.transaction_id', $params['transaction_id']);
		}


		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transactions.reservation_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');

		return $this->db->get('transactions')->result_array();
	}

	public function receivable($params = [], $count = false)
	{
		$this->db->select('
			SUM(transaction_payments.principal_amount) as total_principal,
			SUM(transaction_payments.interest_amount) as total_interest,
			SUM(transaction_payments.total_amount) as t_amount,
			COUNT(transaction_payments.id) as t_count,
		', false);

		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');

		if (!empty($params['date'])) {
			$this->db->where('DATE(transaction_payments.due_date) = ', $params['date']);
		}

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_payments.due_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}
		$this->db->where('transaction_payments.is_paid=0');
		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transactions.deleted_by IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_by IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_by IS NULL');
		// $this->db->group_by('transaction_payments.transaction_id');

		return $this->db->get('transaction_payments')->row_array();
	}
	public function collection($params = [], $count = false)
	{
		$this->db->select('
			SUM(transaction_official_payments.principal_amount) as total_principal,
			SUM(transaction_official_payments.interest_amount) as total_interest,
			SUM(transaction_official_payments.amount_paid) as t_amount,
			COUNT(transaction_official_payments.id) as t_count,
		', false);

		$this->db->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');

		if (!empty($params['date'])) {
			$this->db->where('DATE(transaction_official_payments.payment_date) = ', $params['date']);
		}

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_official_payments.payment_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_official_payments.payment_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_official_payments.deleted_at IS NULL');
		// $this->db->group_by('transaction_official_payments.transaction_id');

		return $this->db->get('transaction_official_payments')->row_array();
	}


	public function get_projects($params = [], $count = false)
	{
		$this->db->select('projects.id,projects.name', false);

		$this->db->join('house_models', 'house_models.project_id = projects.id', 'left');

		if (!empty($params['sub_type_id'])) {
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);
		}

		if (!empty($params['project_id'])) {
			$this->db->where('house_models.project_id', $params['project_id']);
		}

		$this->db->where('house_models.deleted_at IS NULL');
		$this->db->where('projects.deleted_at IS NULL');

		$this->db->group_by('house_models.project_id');

		return $this->db->get('projects')->result_array();
	}

	public function get_property_demography($params = array())
	{
		$this->db->select('COUNT(id) as count');
		$this->db->from('properties');

		if (!empty($params['status'])) {
			$this->db->where('status', $params['status']);
		}

		$this->db->where('properties.deleted_at IS NULL');

		$query = $this->db->get()->row_array();

		return $query;
	}

	public function get_recognized_sales($transaction_id)
	{
		$this->transaction_library->initiate($transaction_id);
		$result = $this->transaction_library->get_amount_paid(1, 0, 1);

		if ($result > 25) {
			return 1;
		}
		return 0;
	}

	public function get_monthly_po($date = "", $status = 0, $operation = '')
	{

		// $date = "$date-01";
		if ($operation == 'both') {
			$this->db->select("count(total) as count");
			$this->db->select("sum(total) as sum");
		} else {
			$this->db->select("$operation(total) as $operation");
		}
		if ($status != 100) {
			$this->db->where("status=$status");
		}
		$this->db->where("deleted_at is NULL");
		// $this->db->where("year(created_at)=year('$date')");
		// $this->db->where("month(created_at)=month('$date')");
		$this->db->where("DATE_FORMAT(created_at, '%Y-%m') = '$date'");
		$query = $this->db->get('purchase_orders')->result_array();
		if ($operation == 'both') {
			if ($query) {
				if ($query[0]['sum'] == 0.00) {
					$result['sum'] = 0.00;
				} else {
					$result['sum'] = $query[0]['sum'];
				}
				if ($query[0]['count'] == 0) {
					$result['count'] = 0;
				} else {
					$result['count'] = $query[0]['count'];
				}
			} else {
				$result['sum'] = 0.00;
				$result['count'] = 0;
			}
		} else {
			if ($query) {
				if ($query[0][$operation] == 0.00) {
					$result = 0.00;
				} else {
					$result = $query[0][$operation];
				}
			} else {
				$result = 0.00;
			}
		}
		return $result;
	}

	public function top_suppliers($range = [], $status = 0, $get_stat = '')
	{

		$this->db->select("$get_stat(purchase_orders.total) as stat_count");
		$this->db->select('suppliers.name as name');
		$this->db->select('suppliers.address as location');
		$this->db->select('suppliers.id as id');

		$this->db->where("status=$status");
		$this->db->where("purchase_orders.deleted_at is NULL");

		$this->db->join('suppliers', 'suppliers.id = purchase_orders.supplier_id', 'left');

		$this->db->where('date_format(purchase_orders.created_at, "%Y/%m") BETWEEN "' . $range[0] . '" AND "' . $range[1] . '"');
		$this->db->group_by('purchase_orders.supplier_id');
		$this->db->order_by('stat_count', 'DESC');
		$this->db->limit(10);
		$query = $this->db->get('purchase_orders')->result_array();
		return $query;
	}

	public function payment_rv_monthly($date = '', $select_type = 0, $project_id = 0)
	{
		$this->db->where('deleted_at is NULL');

		if ($select_type) {
			$this->db->select('coalesce(sum(paid_amount),0)  as sum');
			$this->db->where("DATE_FORMAT(paid_date, '%Y-%m') = '$date'");
			$query = $this->db->get('payment_vouchers')->result_array();
		} else {
			$this->db->select('coalesce(sum(total_due_amount),0)  as sum');
			$this->db->where("DATE_FORMAT(date_requested, '%Y-%m') = '$date'");
			if ($project_id) {
				$this->db->where("project_id", $project_id);
			}
			$query = $this->db->get('payment_requests')->result_array();
		}



		if ($query) {
			$result = $query[0]['sum'];
		} else {
			$result = 0.00;
		}

		return $result;
	}

	public function payment_request_pie($date_start, $date_end, $project_id)
	{

		$this->db->select('count(total_due_amount)  as count');
		$this->db->select('payable_type_id as ids');
		$this->db->where('deleted_at is NULL');
		$this->db->group_by('ids');
		if ($project_id) {
			$this->db->where("project_id", $project_id);
		}
		$this->db->where("DATE_FORMAT(date_requested, '%Y-%m') BETWEEN '" . $date_start . "' AND '" . $date_end . "'");
		$query = $this->db->get('payment_requests')->result_array();
		if ($query) {
			if (count($query) == 1) {
				$result['count'] = [$query[0]['count']];
				$payable_ref = get_person($query[0]['ids'], 'payable_types');
				$result['ids'][] = get_name($payable_ref);
			} else {
				foreach ($query as $key => $value) {
					$payable_ref = get_person($value['ids'], 'payable_types');
					$result['ids'][] = get_name($payable_ref);
					$result['count'][] = $value['count'];
				}
			}

			$result['total'] = array_sum($result['count']);
		}
		return $result;
	}

	public function sales_monthly($date = '', $project_id = 0, $select = '')
	{
		if ($select == 'count') {
			$result['count'] = 0;
			$this->db->select("count(transaction_properties.collectible_price) as count");
		} else {
			$result['sum'] = 0;
			$this->db->select("coalesce(sum(transaction_properties.collectible_price),0) as sum");
		}

		$this->db->join('transactions', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->where('transactions.deleted_at is NULL');
		$this->db->where('transaction_properties.deleted_at is NULL');

		if ($project_id) {
			$this->db->where('transactions.project_id', $project_id);
		}


		$this->db->where('DATE_FORMAT(transactions.reservation_date, "%Y-%m") =', $date);
		$query = $this->db->get('transaction_properties')->result_array();
		if ($query) {
			$result = $query[0][$select];
		}

		return $result;
		exit();
	}

	public function collections_monthly($date = '', $project_id = 0)
	{
		$result['sum'] = 0;
		$this->db->select("coalesce(sum(amount_paid),0) as sum");
		$this->db->join('transactions', 'transaction_official_payments.transaction_id = transactions.id', 'left');
		$this->db->where('transactions.deleted_at is NULL');
		$this->db->where('transaction_official_payments.deleted_at is NULL');

		if ($project_id) {
			$this->db->where('transactions.project_id', $project_id);
		}


		$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") =', $date);
		$query = $this->db->get('transaction_official_payments')->result_array();
		if ($query) {
			$result = $query[0]['sum'];
		}

		return $result;
		exit();
	}

	public function buyer_count($date = '')
	{
		$result['count'] = 0;
		$this->db->select("count(id) as count");
		$this->db->where('deleted_at is NULL');
		$this->db->where('DATE_FORMAT(created_at, "%Y-%m") =', $date);
		$query = $this->db->get('buyers')->result_array();
		if ($query) {
			$result = $query[0]['count'];
		}

		return $result;
		exit();
	}

	public function sum_collections($type, $project_id = 0)
	{
		$result['count'] = 0;
		$result['sum'] = 0;
		$this->db->select("count(transaction_payments.id) as count");
		$this->db->select("coalesce(sum(transaction_payments.total_amount),0) as sum");
		$this->db->join('transactions', 'transaction_payments.transaction_id = transactions.id', 'inner');
		$this->db->where('transaction_payments.deleted_at is NULL');
		$this->db->where('transactions.deleted_at is NULL');
		$this->db->where('transaction_payments.is_paid', 0);
		if ($project_id) {
			$this->db->where('transactions.project_id', $project_id);
		}
		if ($type == 'today') {
			$this->db->where('transaction_payments.due_date=CURDATE()');
		} elseif ($type == 'past_due') {
			$this->db->where('transaction_payments.due_date<CURDATE()');
		}
		$query = $this->db->get('transaction_payments')->result_array();
		if ($query) {
			$result['sum'] = $query[0]['sum'];
			$result['count'] = $query[0]['count'];
		}
		return $result;
		exit();
	}
	public function sum_pdc($type, $project_id = 0)
	{
		$result['count'] = 0;
		$result['sum'] = 0;
		$this->db->select("count(transaction_post_dated_checks.id) as count");
		$this->db->select("coalesce(sum(transaction_post_dated_checks.amount),0) as sum");
		$this->db->join('transactions', 'transaction_post_dated_checks.transaction_id = transactions.id', 'inner');
		$this->db->where('transaction_post_dated_checks.deleted_at is NULL');
		$this->db->where('transactions.deleted_at is NULL');
		$this->db->where('transaction_post_dated_checks.pdc_status', 1);
		if ($project_id) {
			$this->db->where('transactions.project_id', $project_id);
		}
		if ($type == 'today') {
			$this->db->where('transaction_post_dated_checks.due_date=CURDATE()');
		} elseif ($type == 'past_due') {
			$this->db->where('transaction_post_dated_checks.due_date<CURDATE()');
		}
		$query = $this->db->get('transaction_post_dated_checks')->result_array();
		if ($query) {
			$result['sum'] = $query[0]['sum'];
			$result['count'] = $query[0]['count'];
		}
		return $result;
		exit();
	}

	public function collection_list($type, $project_id = 0)
	{
		$result = [];
		$this->db->select("transaction_payments.total_amount");
		$this->db->select("transactions.reference");
		$this->db->select("transactions.id as transactions_id");
		$this->db->select("buyers.first_name");
		$this->db->select("buyers.last_name");
		$this->db->select("projects.name as project_name");
		$this->db->join('transactions', 'transaction_payments.transaction_id=transactions.id', 'inner');
		$this->db->join('buyers', 'transactions.buyer_id=buyers.id', 'inner');
		$this->db->join('projects', 'transactions.project_id=projects.id', 'inner');
		$this->db->where('transaction_payments.deleted_at is NULL');
		$this->db->where('transactions.deleted_at is NULL');
		$this->db->where('buyers.deleted_at is NULL');
		$this->db->where('projects.deleted_at is NULL');
		$this->db->where('transaction_payments.is_paid', 0);
		if ($project_id) {
			$this->db->where('transactions.project_id', $project_id);
		}
		if ($type == 'today') {
			$this->db->where('transaction_payments.due_date=CURDATE()');
		} elseif ($type == 'past_due') {
			$this->db->where('transaction_payments.due_date<CURDATE()');
		}
		$query = $this->db->get('transaction_payments')->result_array();
		if ($query) {
			$result = $query;
		}
		return $result;
		exit();
	}
	public function pdc_list($type, $project_id = 0)
	{
		$result = [];
		$this->db->select("banks.name as bank_name");
		$this->db->select("transaction_post_dated_checks.amount");
		$this->db->select("transaction_post_dated_checks.unique_number");
		$this->db->select("projects.name as project_name");
		$this->db->join('transactions', 'transaction_post_dated_checks.transaction_id=transactions.id', 'inner');
		$this->db->join('banks', 'transaction_post_dated_checks.bank_id=banks.id', 'inner');
		$this->db->join('buyers', 'transactions.buyer_id=buyers.id', 'inner');
		$this->db->join('projects', 'transactions.project_id=projects.id', 'inner');
		$this->db->where('transaction_post_dated_checks.deleted_at is NULL');
		$this->db->where('transactions.deleted_at is NULL');
		$this->db->where('banks.deleted_at is NULL');
		$this->db->where('buyers.deleted_at is NULL');
		$this->db->where('projects.deleted_at is NULL');
		$this->db->where('transaction_post_dated_checks.pdc_status', 1);
		if ($project_id) {
			$this->db->where('transactions.project_id', $project_id);
		}
		if ($type == 'today') {
			$this->db->where('transaction_post_dated_checks.due_date=CURDATE()');
		} elseif ($type == 'past_due') {
			$this->db->where('transaction_post_dated_checks.due_date<CURDATE()');
		}
		$query = $this->db->get('transaction_post_dated_checks')->result_array();
		if ($query) {
			$result = $query;
		}
		return $result;
		exit();
	}
	public function get_monthly_cr($project_id = 0, $date, $type)
	{
		$query = 0;
		$this->db->where('transactions.deleted_at is NULL');
		if ($project_id) {
			$this->db->where('transactions.project_id', $project_id);
		}
		if ($type == "collections") {
			$this->db->select("coalesce(sum(transaction_official_payments.amount_paid),0) as sum");
			$this->db->where('transaction_official_payments.deleted_at is NULL');
			$this->db->join('transactions', 'transaction_official_payments.transaction_id=transactions.id', 'inner');
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") = ', $date);
			$query = $this->db->get('transaction_official_payments')->result_array();
		} elseif ($type == "receivables") {
			$this->db->select("coalesce(sum(transaction_payments.total_amount),0) as sum");
			$this->db->where('transaction_payments.is_paid', 0);
			$this->db->where('transaction_payments.deleted_at is NULL');
			$this->db->where('transaction_properties.deleted_at is NULL');
			$this->db->join('transactions', 'transaction_payments.transaction_id=transactions.id', 'inner');
			$this->db->join('transaction_properties', 'transaction_properties.transaction_id=transactions.id', 'inner');
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $date);
			$query = $this->db->get('transaction_payments')->result_array();
		}

		if ($query) {
			$result = $query[0]['sum'];
		}
		return $result;
		exit();
	}
	public function get_count_past_due_notices($date)
	{
		$result = 0;
		$this->db->select("count(transaction_past_due_notices.id) as count");
		$this->db->join('transactions', 'transaction_past_due_notices.transaction_id=transactions.id', 'inner');
		$this->db->where('transaction_past_due_notices.deleted_at is null');
		$this->db->where('transactions.deleted_at is null');
		$this->db->where("DATE_FORMAT(transaction_past_due_notices.past_due_date, '%Y/%m/%d') =", $date);
		$query = $this->db->get('transaction_past_due_notices')->result_array();
		if ($query) {
			$result = $query[0]['count'];
		}
		return $result;
	}
	public function get_count_transaction_letter_request($date)
	{
		$result = 0;
		$this->db->select("count(transaction_letter_request.id) as count");
		$this->db->join('transactions', 'transaction_letter_request.transaction_id=transactions.id', 'inner');
		$this->db->where('transaction_letter_request.deleted_at is null');
		$this->db->where('transaction_letter_request.status is null');
		$this->db->where('transactions.deleted_at is null');
		$this->db->where("DATE_FORMAT(transaction_letter_request.request_date, '%Y/%m/%d') =", $date);
		$query = $this->db->get('transaction_letter_request')->result_array();
		if ($query) {
			$result = $query[0]['count'];
		}
		return $result;
	}
	public function get_recent_activity()
	{
		$history = $this->db->query(
			"SELECT concat(first_name,' ',last_name) as name, activity_date as date, detail, detail_2, table_name, t1.id, t1.type
			from (select created_by as user_id, action as detail, remarks as detail_2, created_at as activity_date,
			affected_table as table_name, affected_id as id, 'log' as type from audit_trail
			union all
			select user_id as user_id, ip_address as detail, id as detail_2, created_at as activity_date,
			ip_address as table_name, user_id as id, 'login' as type from login_history) t1
			inner join
			staff
			where staff.user_id = t1.user_id
			order by activity_date DESC
			limit 50"
		)->result_array();
		return $history;
	}

	public function get_recent_tickets()
	{
		$history = $this->db->query(
			"SELECT t1.id, concat(first_name,' ',last_name) as name, activity_date, type,message, status
			from (select id, buyer_id as user_id, description as message, created_at as activity_date, 'complaint_tickets' as type, status
			from complaint_tickets
			where status between 1 and 4
			union all
			select id, buyer_id as user_id, description as message, created_at as activity_date, 'inquiry_tickets' as type, status
			from inquiry_tickets
			where status between 1 and 4) t1
			inner join
			buyers
			where t1.user_id = buyers.id
			order by activity_date DESC
			limit 15"
		)->result_array();
		return $history;
	}
	public function get_latest_buyers()
	{
		$buyer_history = $this->db->query(
			"SELECT max(id) as id ,project_id, buyer_id, max(reservation_date) as date
			from transactions
			where deleted_at is null
			group by buyer_id
			order by date desc
			limit 10"
		)->result_array();
		return $buyer_history;
	}

	public function get_ar_clearing_count($params = [], $type = 0)
	{
		$results = 0;
		$this->db->select('
			coalesce(sum(ar_clearing.grand_total),0) as sum
		');

		$this->db->join('transaction_payments', 'transaction_payments.id = ar_clearing.transaction_payment_id', 'left');
		$this->db->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transaction_payments.due_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_payments.due_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transaction_payments.due_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		if ($type) {
			$this->db->where('ar_clearing.is_active', $type);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('transaction_payments.deleted_at IS NULL');
		$this->db->where('ar_clearing.deleted_at IS NULL');

		$query = $this->db->get('ar_clearing')->result_array();
		if ($query) {
			$result = $query[0]['sum'];
		}
		return $result;
	}
	public function sales_recognition($params = [], $count = false)
	{	
		if (!empty($params['select'])) {
			if ($params['select'] == "property_name") {
				$this->db->select('properties.name', false);
			}
		} else {
			$this->db->select('
				SUM(transaction_properties.total_selling_price) as total_selling_price,
				SUM(if(transactions.is_recognized = 1, 1, 0)) AS total_recognized,
				SUM(if(transactions.is_recognized = 0, 1, 0)) AS total_not_recognized,
				COUNT(transactions.id) as t_count,
			', false);
		}

		$this->db->join('transaction_properties', 'transaction_properties.transaction_id = transactions.id', 'left');
		$this->db->join('properties', 'properties.id = transaction_properties.property_id', 'left');

		if (!empty($params['group'])) {
			$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			$this->db->where('sellers.sales_group_id', $params['group']);
		}

		if (!empty($params['position'])) {
			if (empty($params['group'])) {
				$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			}
			$this->db->where('sellers.seller_position_id', $params['position']);
		}



		if (!empty($params['tops'])) {
			$this->db->select('
				sellers.first_name, sellers.last_name, sellers.sales_group_id, seller_positions.name as sp_name, sellers.id as id
			', false);
			$this->db->join('sellers', 'sellers.id = transactions.seller_id', 'left');
			$this->db->join('seller_positions', 'seller_positions.id = sellers.seller_position_id', 'left');
			$this->db->group_by('transactions.seller_id');
			$this->db->order_by('total_selling_price', 'DESC');
			if ($params['general_limit']) {
				$this->db->limit(3);
			} else {
				$this->db->limit(10);
			}

			if ($params['project_id']) {
				$this->db->where('transactions.project_id', $params['project_id']);
			}
		}

		if (!empty($params['sub_type_id'])) {
			// $this->db->join('properties', 'projects.id = properties.project_id', 'left');
			$this->db->join('house_models', 'house_models.id = transaction_properties.house_model_id', 'left');
			$this->db->where('house_models.sub_type_id', $params['sub_type_id']);
		}

		if (!empty($params['date'])) {
			$this->db->where('DATE(transactions.reservation_date) = ', $params['date']);
		}

		if (!empty($params['year'])) {
			$this->db->where('YEAR(transactions.reservation_date) = ', $params['year']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transactions.reservation_date, "%Y-%m") = ', $params['year_month']);
		}
		if (!empty($params['year_month_range'])) {
			$this->db->where("DATE_FORMAT(transactions.reservation_date, '%Y-%m') between '" .  $params['year_month_range'][0] . "' AND '" . $params['year_month_range'][1] . "'");
		}

		if (!empty($params['transaction_id'])) {
			$this->db->where('transactions.transaction_id', $params['transaction_id']);
		}


		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transactions.reservation_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (!empty($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transactions.general_status !=',4);
		$this->db->where('transaction_properties.deleted_at IS NULL');


		return $this->db->get('transactions')->result_array();
	}
}
