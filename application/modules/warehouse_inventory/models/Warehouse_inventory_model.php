<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Warehouse_inventory_model extends MY_Model
    {
        public $table = 'warehouse_inventories'; // you MUST mention the table name
        public $primary_key = 'id';
        public $fillable = [
            "actual_quantity",
            "available_quantity",
            "created_at",
            "created_by",
            "deleted_at",
            "deleted_by",
            "id",
            "item_id",
            "unit_price",
            "unit_of_measurement_id",
            "updated_at",
            "updated_by",
            "warehouse_id",
        ];
        public $form_fillables = [
            "id",
            "actual_quantity",
            "available_quantity",
            "item_id",
            "unit_price",
            "unit_of_measurement_id",
            "warehouse_id",
        ];
        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [

            "actual_quantity" => array(
                "field" => "actual_quantity",
                "label" => "Actual Quantity",
                "rules" => "required"
            ),

            "item_id" => array(
                "field" => "item_id",
                "label" => "Item",
                "rules" => "required"
            ),

            "unit_price" => array(
                "field" => "unit_price",
                "label" => "Unit Price",
                "rules" => "required"
            ),

            "unit_of_measurement_id" => array(
                "field" => "unit_of_measurement_id",
                "label" => "Unit of Measurement",
                "rules" => "required"
            ),


            "warehouse_id" => array(
                "field" => "warehouse_id",
                "label" => "Warehouse",
                "rules" => "required"
            ),

        ];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = true;
            $this->return_as = 'array';

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;


            $this->has_one["item"] = array('foreign_model' => 'item/item_model', 'foreign_table' => 'item', 'foreign_key' => 'id', 'local_key' => 'item_id');


            $this->has_one["unit_of_measurement"] = array('foreign_model' => 'inventory_settings_unit_of_measurement/Inventory_settings_unit_of_measurement_model', 'foreign_table' => 'inventory_settings_unit_of_measurements', 'foreign_key' => 'id', 'local_key' => 'unit_of_measurement_id');


            $this->has_one["warehouse"] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouses', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');


        }

        function get_columns()
        {
            $_return = FALSE;

            if ($this->fillable) {
                $_return = $this->fillable;
            }

            return $_return;
        }

        public function insert_dummy()
        {
            require APPPATH . '/third_party/faker/autoload.php';
            $faker = Faker\Factory::create();

            $data = [];

            for ($x = 0; $x < 10; $x++) {
                array_push($data, array(
                    'name' => $faker->word,
                ));
            }
            $this->db->insert_batch($this->table, $data);

        }

    }