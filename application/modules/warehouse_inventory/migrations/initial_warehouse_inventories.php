<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Initial_warehouse_inventories extends CI_Migration {
    protected $tbl = "warehouse_inventories";
    protected $fields = array(
        
            "id" =>  array(
                
                    "type" => "INT",
                    
                
                    "constraint" => "11",
                    
                
                    "unsigned" => True,
                    
                
                    "auto_increment" => True,
                    
                
                    "NOT NULL" => True,
                    
                
            ),
        
            "created_by" =>  array(
                
                    "type" => "INT",
                    
                
                    "NOT NULL" => True,
                    
                
                    "unsigned" => True,
                    
                
            ),
        
            "created_at" =>  array(
                
                    "type" => "DATETIME",
                    
                
                    "NOT NULL" => True,
                    
                
            ),
        
            "updated_by" =>  array(
                
                    "type" => "INT",
                    
                
                    "NOT NULL" => True,
                    
                
                    "unsigned" => True,
                    
                
            ),
        
            "updated_at" =>  array(
                
                    "type" => "DATETIME",
                    
                
                    "NOT NULL" => True,
                    
                
            ),
        
            "deleted_by" =>  array(
                
                    "type" => "INT",
                    
                
                    "NOT NULL" => True,
                    
                
                    "unsigned" => True,
                    
                
            ),
        
            "deleted_at" =>  array(
                
                    "type" => "DATETIME",
                    
                
                    "NOT NULL" => True,
                    
                
            ),
        
            "warehouse_id" =>  array(
                
                    "type" => "INT",
                    
                
                    "constraint" => "11",
                    
                
                    "NOT NULL" => True,
                    
                
            ),
        
            "item_id" =>  array(
                
                    "type" => "INT",
                    
                
                    "constraint" => "11",
                    
                
                    "NOT NULL" => True,
                    
                
            ),
        
            "unit_of_measurement_id" =>  array(
                
                    "type" => "INT",
                    
                
                    "constraint" => "11",
                    
                
                    "NOT NULL" => True,
                    
                
            ),
        
            "actual_quantity" =>  array(
                
                    "type" => "FLOAT",
                    
                
                    "NOT NULL" => True,
                    
                
                    "default" => "0",
                    
                
            ),
        
            "available_quantity" =>  array(
                
                    "type" => "FLOAT",
                    
                
                    "NOT NULL" => True,
                    
                
                    "default" => "0",
                    
                
            ),
        
            "unit_cost" =>  array(
                
                    "type" => "DOUBLE",
                    
                
                    "NOT NULL" => True,
                    
                
                    "constraint" => "20,2",
                    
                
            ),
        
    );

    public function up() {     
        if (! $this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }
        
    }

    public function down() {
        if (! $this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
