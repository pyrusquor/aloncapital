<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Warehouse_inventory extends MY_Controller
    {

        private $__errors = array(
            'create_master' => null,
            'create_slave' => null,
            'update_master' => null,
            'update_slave' => null,
        );
        private $_lib = null;

        public function __construct()
        {
            parent::__construct();

            $this->load->model('Warehouse_inventory_model', 'M_Warehouse_inventory');

            $this->load->helper('form');
            $this->load->library('WarehouseInventoryLibrary');
            $this->_lib = new WarehouseInventoryLibrary();

            $this->_table_fillables = $this->M_Warehouse_inventory->fillable;
            $this->_form_fillables = $this->M_Warehouse_inventory->form_fillables;
            $this->_table_columns = $this->M_Warehouse_inventory->__get_columns();
        }

        private function __search($params)
        {
            return $this->_lib->search($params);
        }


        private function process_create_master($data, $additional)
        {

            $form_data = $data + $additional;
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules($this->M_Warehouse_inventory->fields);

            if ($this->form_validation->run() === true) {

                return $this->_lib->create_master($data, $additional);
            } else {
                $this->_form_errors['create_master'] = validation_errors();
                return false;
            }
        }

        private function process_update_master($id, $data, $additional)
        {
            return $this->_lib->update_master($id, $data, $additional);
        }

        private function __get_obj($id, $with_relations = TRUE)
        {
            return $this->_lib->get_obj($id, $with_relations);
        }

        private function __get_item($item_id, $warehouse_id = NULL, $with_relations = TRUE)
        {
            return $this->_lib->get_item($item_id, $warehouse_id, $with_relations);
        }

        private function __get_mr_item($item_id, $warehouse_id = null)
        {
            $this->load->library('MaterialReceivingibrary');
            $mr_lib = new MaterialReceivingLibrary();
            $items_in_warehouse = $mr_lib->get_items_by_item_id($item_id, $warehouse_id);
            return $items_in_warehouse;
        }

        public function get_mr_item()
        {
            header('Content-Type: application/json');
            $item_id = isset($_POST['item_id']) ? $_POST['item_id'] : null;
            $source_id = isset($_POST['source_id']) ? $_POST['source_id'] : null;
            if (!$item_id || !$source_id) {
                show_404();
            }
            return $this->__get_mr_item($item_id, $source_id);
        }

        private function process_update_slave($data, $additional, $references)
        {
            $id = isset($data['id']) ? $data['id'] : null;
            $this->load->model('warehouse_inventory_item/Warehouse_inventory_item_model', 'M_item');

            $temp_data = $data + $additional;
            $update_data = [];
            if (property_exists($this->M_item, 'form_fillables')) {
                $fillables = $this->M_item->form_fillables;
            } else {
                $fillables = $this->M_item->fillable;
            }
            foreach ($fillables as $key) {
                if (array_key_exists($key, $temp_data)) {
                    $update_data[$key] = $temp_data[$key];
                }
            }

            if ($id) {
                // return $this->db->update('material_receiving_items');
                return $this->M_item->update($update_data, $id);
            } else {
                $additional = [
                    'created_by' => $additional['updated_by'],
                    'created_at' => $additional['updated_at']
                ];

                return $this->process_create_slave($data, $references, $additional);
            }
        }

        private function process_create_slave($data, $references, $additional)
        {
            $this->load->model('warehouse_inventory_item/Warehouse_inventory_item_model', 'M_item');
            if (property_exists($this->M_item, 'form_fillables')) {
                $fillables = $this->M_item->form_fillables;
            } else {
                $fillables = $this->M_item->fillable;
            }

            $item = [];
            foreach ($fillables as $field) {
                if (key_exists($field, $data)) {
                    $item[$field] = $data[$field];
                } else {
                    if (key_exists($field, $references)) {
                        $item[$field] = $references[$field];
                    }
                }
            }

            return $this->M_item->insert($item);
        }

        private function process_purge_deleted($items, $additional)
        {
            $this->load->model('warehouse_inventory_item/Warehouse_inventory_item_model', 'M_item');
            if ($items) {
                foreach ($items as $item) {
                    if ($item) {
                        $this->M_item->delete($item);
                    }
                }
            }
        }

        public function transfer()
        {
            /* items:
             *  - quantity
             *  - material_receiving_item_id
             *
             */
            header('Content-Type: application/json');
            $items = isset($_POST['items']) ? $_POST['items'] : null;
            $source_id = isset($_POST['source_id']) ? $_POST['source_id'] : null;
            $destination_id = isset($_POST['destination_id']) ? $_POST['destination_id'] : null;

//            if (!$items || !$source_id || !$destination_id) {
//                echo json_encode("Invalid data");
//                exit();
//            }

            if ($source_id == $destination_id) {
                $response['message'] = "Invalid destination";
                echo json_encode($response);
                exit();
            }

            $meta = [
                'actor' => $this->user->id,
                'time' => NOW,
                'source_id' => $source_id,
                'destination_id' => $destination_id
            ];
            $this->load->model('warehouse/warehouse_model', 'M_Warehouse');
            $source_warehouse = $this->M_Warehouse->get($source_id);
            $destination_warehouse = $this->M_Warehouse->get($destination_id);

            if($destination_warehouse['warehouse_type'] == 3){
                $meta['virtual_destination'] = true;
            }else{
                $meta['virtual_destination'] = false;
            }

            if($source_warehouse['warehouse_type'] == 3){
                $meta['virtual_source'] = true;
            }else{
                $meta['virtual_source'] = false;

            }

            $this->load->library('MaterialMovementLibrary');
            $this->load->library('MaterialIssuanceLibrary');
            $this->load->library('MaterialIssuanceItemLibrary');
            $this->load->library('MaterialReceivingLibrary');
            $this->load->library('MaterialReceivingItemLibrary');

            $mri_lib = new MaterialReceivingItemLibrary();

            $issuance_lib = new MaterialIssuanceLibrary();
            $issuance_item_lib = new MaterialIssuanceItemLibrary();

            $receiving_lib = new MaterialReceivingLibrary();

            $release = new MaterialMovementLibrary();

            $issuance_data = array(
                'reference' => uniqidMI(),
                'material_request_id' => null,
                'warehouse_id' => $source_id,
                'issuance_type' => 3
            );

            $pre_create_checks = $issuance_lib->pre_create($items);
            if (!$pre_create_checks['status']) {
                echo json_encode($pre_create_checks['errors']);
                exit();
            }
            $issuance = $issuance_lib->create_master($issuance_data, array('created_by' => $meta['actor'], 'created_at' => $meta['time']));
            $mi_data = array(
                'material_issuance_id' => $issuance,
                'created_by' => $meta['actor'],
                'created_at' => $meta['time']
            );
            foreach ($items as $item) {
                $obj = $issuance_item_lib->create_master($item, $mi_data);
                if($obj){
                    $issuance_item_lib->update_issuances($obj);
                }
            }

            $release_movement_meta = array(
                'transaction' => 'transfer_subtract',
            );

            $release->init('release', $issuance, $meta + $release_movement_meta);
            $release->move();

            $receive = new MaterialMovementLibrary();
            $mr_data = array(
                'status' => 2,
                'purchase_order_id' => null,
                'warehouse_id' => $destination_id,
                'supplier_id' => null,
                'accounting_ledger_id' => null,
                'freight_amount' => 0,
                'landed_cost' => 0,
                'total_cost' => 0,
                'input_tax' => 0,
                'receiving_type' => 3
            );

            $receiving = $receiving_lib->create_master($mr_data, array('created_by' => $meta['actor'], 'created_at' => $meta['time']));
            $receiving_items = [];
            foreach ($items as $item) {
                $mri = $mri_lib->get($item['material_receiving_item_id']);
                $total_cost = (1.0 * $mri['unit_cost']) * $item['quantity'];
                $__item = array(
                    'supplier_id' => $mri['supplier_id'],
                    'material_request_id' => $mri['material_request_id'],
                    'purchase_order_id' => $mri['purchase_order_id'],
                    'purchase_order_item_id' => $mri['purchase_order_item_id'],
                    'purchase_order_request_item_id' => $mri['purchase_order_request_item_id'],
                    'purchase_order_request_id' => $mri['purchase_order_request_id'],
                    'warehouse_id' => $destination_id,
                    'item_group_id' => $mri['item_group_id'],
                    'item_type_id' => $mri['item_type_id'],
                    'item_brand_id' => $mri['item_brand_id'],
                    'item_class_id' => $mri['item_class_id'],
                    'item_id' => $mri['item_id'],
                    'unit_of_measurement_id' => $mri['unit_of_measurement_id'],
                    'item_brand_name' => $mri['item_brand_name'],
                    'item_class_name' => $mri['item_class_name'],
                    'item_name' => $mri['item_name'],
                    'unit_of_measurement_name' => $mri['unit_of_measurement_name'],
                    'item_group_name' => $mri['item_group_name'],
                    'item_type_name' => $mri['item_type_name'],
                    'unit_cost' => $mri['unit_cost'],
                    'total_cost' => $total_cost,
                    'quantity' => $item['quantity'],
                    'status' => 1,
                );
                $receiving_lib->create_slave($__item, array('material_receiving_id' => $receiving), array('created_at' => $meta['time'], 'created_by' => $meta['actor']));
            }

            $receive_movement_meta = array(
                'transaction' => 'transfer_add'
            );
            $receive->init('receive', $receiving, $meta + $receive_movement_meta);
            $receive->move();

            $response = array(
                'status' => 1,
                'message' => 'yes'
            );
            echo json_encode($response);
            exit();
        }

        // deprecated
        public function __transfer()
        {
            header('Content-Type: application/json');
            $item_id = isset($_POST['item_id']) ? $_POST['item_id'] : null;
            $id = isset($_POST['id']) ? $_POST['id'] : null;
            $destination_id = isset($_POST['destination_id']) ? $_POST['destination_id'] : null;
            $destination_type = isset($_POST['destination_type']) ? $_POST['destination_type'] : null;
            $source_id = isset($_POST['source_id']) ? $_POST['source_id'] : null;
            $source_type = isset($_POST['source_type']) ? $_POST['source_type'] : null;
            $quantity = isset($_POST['quantity']) ? (float)$_POST['quantity'] : 0;

            // response
            $response = array(
                'status' => false,
                'message' => ''
            );

            // checks
            if (!$id || !$item_id) {
                $response['message'] = "Invalid object";
                echo json_encode($response);
                exit();
            }

            if ($quantity <= 0) {
                $response['message'] = "Invalid quantity";
                echo json_encode($response);
                exit();
            }

            $meta = [
                'actor' => $this->user->id,
                'time' => NOW
            ];

            $this->load->library('ItemTransferLibrary');
            $transfer = new ItemTransferLibrary();
            $transfer->init($id, $source_type, $source_id, $destination_type, $destination_id, $quantity, $meta);

            $obj = $transfer->source_inventory_item;

            if (!$obj) {
                $response['message'] = "Invalid object";
                echo json_encode($response);
                exit();
            }

            if ($obj['available_quantity'] < $quantity) {
                $response['message'] = "Not enough available quantity";
                echo json_encode($response);
                exit();
            }

            // todo: prevent transfer to same destination
            $transfer->transfer();

            // $this->_lib->add_to_or_update_destination($obj, $destination_id, $destination_type, $quantity, $this->user->id);
            $response['status'] = true;
            $response['message'] = 'Item transferred';

            echo json_encode($response);
            exit();
        }

        public function search()
        {
            $result = $this->__search($_GET);

            header('Content-Type: application/json');
            echo json_encode($result);

        }

        public function get_item()
        {
            $item_id = isset($_GET['item_id']) ? $_GET['item_id'] : null;
            $warehouse_id = isset($_GET['warehouse_id']) ? $_GET['warehouse_id'] : null;
            header('Content-Type: application/json');
            echo json_encode($this->__get_item($item_id, $warehouse_id));
            exit();
        }

        public function showWarehouseInventories()
        {
            header('Content-Type: application/json');
            $output = ['data' => ''];

            if (property_exists($this->M_Warehouse_inventory, 'form_fillables')) {
                $fillables = $this->M_Warehouse_inventory->form_fillables;
            } else {
                $fillables = $this->M_Warehouse_inventory->fillable;
            }

            $columnsDefault = [];
            foreach ($fillables as $field) {
                $columnsDefault[$field] = true;
            }
            // optionally add relations
            $columnsDefault['item'] = true;
            $columnsDefault['unit_of_measurement'] = true;
            $columnsDefault['warehouse'] = true;

            if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
                $columnsDefault = [];
                foreach ($_REQUEST['columnsDef'] as $field) {
                    $columnsDefault[$field] = true;
                }
            }

            // get all raw data
            $objects = $this->__search($_GET);
            $data = [];

            if ($objects) {

                foreach ($objects as $d) {
                    $data[] = $this->filterArray($d, $columnsDefault);
                }

                // count data
                $totalRecords = $totalDisplay = count($data);

                // filter by general search keyword
                if (isset($_REQUEST['search'])) {
                    $data = $this->filterKeyword($data, $_REQUEST['search']);
                    $totalDisplay = count($data);
                }

                if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                    foreach ($_REQUEST['columns'] as $column) {
                        if (isset($column['search'])) {
                            $data = $this->filterKeyword($data, $column['search'], $column['data']);
                            $totalDisplay = count($data);
                        }
                    }
                }

                // sort
                if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                    $column = $_REQUEST['order'][0]['column'];
                    $dir = $_REQUEST['order'][0]['dir'];
                    usort($data, function ($a, $b) use ($column, $dir) {
                        $a = array_slice($a, $column, 1);
                        $b = array_slice($b, $column, 1);
                        $a = array_pop($a);
                        $b = array_pop($b);

                        if ($dir === 'asc') {
                            return $a > $b ? true : false;
                        }

                        return $a < $b ? true : false;
                    });
                }

                // pagination length
                if (isset($_REQUEST['length'])) {
                    $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
                }

                // return array values only without the keys
                if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                    $tmp = $data;
                    $data = [];
                    foreach ($tmp as $d) {
                        $data[] = array_values($d);
                    }
                }
                $secho = 0;
                if (isset($_REQUEST['sEcho'])) {
                    $secho = intval($_REQUEST['sEcho']);
                }

                $output = array(
                    'sEcho' => $secho,
                    'sColumns' => '',
                    'iTotalRecords' => $totalRecords,
                    'iTotalDisplayRecords' => $totalDisplay,
                    'data' => $data,
                );

            }

            echo json_encode($output);
            exit();
        }

        public function process_create()
        {
            header('Content-Type: application/json');
            if ($this->input->post()) {
                $__request = $this->input->post('request');
                $request = [];
                $items = $this->input->post('items');

                if (property_exists($this->M_Warehouse_inventory, 'form_fillables')) {
                    $fillables = $this->M_Warehouse_inventory->form_fillables;
                } else {
                    $fillables = $this->M_Warehouse_inventory->fillable;
                }

                foreach ($fillables as $field) {
                    if (array_key_exists($field, $__request)) {
                        $request[$field] = $__request[$field];
                    }
                }

                $this->db->trans_start();
                $this->db->trans_strict(false);

                $additional = array(
                    'created_at' => NOW,
                    'created_by' => $this->user->id
                );

                $request_object = $this->process_create_master($request, $additional);
                if ($request_object) {
                    $references = array(
                        'warehouse_inventory_id' => $request_object,
                    );
                }

                $this->db->trans_complete(); # Completing request
                if ($this->db->trans_status() === false || !$request_object) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = $this->__errors['create_master'] + $this->__errors['create_slave'];
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();

                    $response['status'] = 1;
                    $response['message'] = 'Request Successfully saved!';
                }

                echo json_encode($response);
                exit();
            }
        }

        public function process_update($id)
        {
            header('Content-Type: application/json');
            $additional = [
                'updated_by' => $this->user->id,
                'updated_at' => NOW,
            ];
            if ($this->input->post()) {
                $__request = $this->input->post('request');
                $request = [];
                $items = $this->input->post('items');
                $update = $this->_lib->update_master($id, $items, $additional);

                if (!$update) {
                    # Something went wrong.
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.

                    $response['status'] = 1;
                    $response['message'] = 'Successfully saved!';
                }

                echo json_encode($response);
                exit();

            }
        }

        public function index()
        {
            $_fills = $this->_table_fillables;
            $_colms = $this->_table_columns;

            $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
            $this->view_data['_columns'] = $this->__get_columns($_fills);

            $db_columns = $this->_table_columns;
            if ($db_columns) {
                $column = [];
                foreach ($db_columns as $key => $dbclm) {
                    $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                    if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                        if (strpos($name, '_id') !== false) {
                            $column = $name;
                            $column[$key]['value'] = $column;
                            $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                            $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                            $column[$key]['label'] = ucwords(strtolower($_title));
                        } elseif (strpos($name, 'is_') !== false) {
                            $column = $name;
                            $column[$key]['value'] = $column;
                            $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                            $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                            $column[$key]['label'] = ucwords(strtolower($_title));
                        } else {
                            $column[$key]['value'] = $name;
                            $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                            $column[$key]['label'] = ucwords(strtolower($_title));
                        }
                    } else {
                        continue;
                    }
                }

                $column_count = count($column);
                $cceil = ceil(($column_count / 2));

                $this->view_data['columns'] = array_chunk($column, $cceil);
                $column_group = $this->M_Warehouse_inventory->count_rows();
                if ($column_group) {
                    $this->view_data['total'] = $column_group;
                }

            }

            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue([
                'vendors/custom/datatables/datatables.bundle.js',
                'js/vue2.js',
                'js/axios.min.js',
                'js/utils.js'
            ]);

            $this->template->build('index', $this->view_data);
        }

        public function update($id)
        {
            $obj = $this->__get_obj($id);
            if (property_exists($this->M_Warehouse_inventory, 'form_fillables')) {
                $fillables = $this->M_Warehouse_inventory->form_fillables;
            } else {
                $fillables = $this->M_Warehouse_inventory->fillable;
            }

            $method = "Update";
            if (in_array('reference', $fillables)) {
                $reference = $obj['reference'];
            } else {
                $reference = null;
            }

            $form_data = fill_form_data($fillables, $obj);
            $warehouse = get_object_from_table($obj['warehouse_id'], 'warehouses', false);
            $this->view_data['warehouse_name'] = $warehouse['name'];
            $this->view_data['warehouse_id'] = $warehouse['id'];

            $this->view_data['item'] = $obj['item'];
            $this->view_data['unit_of_measurement'] = $obj['unit_of_measurement'];

            $this->view_data['fillables'] = $fillables;
            $this->view_data['current_user'] = $this->user->id;
            $this->view_data['method'] = $method;
            $this->view_data['obj'] = $obj;
            $this->view_data['id'] = $id ? $id : null;
            $this->view_data['form_data'] = $form_data;
            $this->view_data['reference'] = $reference;


            $this->js_loader->queue([
                'js/vue2.js',
                'js/axios.min.js',
                'js/utils.js'
            ]);

            $this->template->build('update', $this->view_data);
        }

        public function form($id = false)
        {
            if (property_exists($this->M_Warehouse_inventory, 'form_fillables')) {
                $fillables = $this->M_Warehouse_inventory->form_fillables;
            } else {
                $fillables = $this->M_Warehouse_inventory->fillable;
            }
            if ($id) {
                header('Location:' . site_url('warehouse_inventory/update/' . $id));
                $method = "Update";
                $obj = $this->__get_obj($id);
                if (in_array('reference', $fillables)) {
                    $reference = $obj['reference'];
                } else {
                    $reference = null;
                }

                $form_data = fill_form_data($fillables, $obj);
                $this->view_data['warehouse'] = $obj['warehouse'];
                $this->view_data['item'] = $obj['item'];
                $this->view_data['unit_of_measurement'] = $obj['unit_of_measurement'];
            } else {
                $method = "Create";
                $obj = null;
                $reference = null;
                $form_data = fill_form_data($fillables);
                $this->view_data['item'] = null;
                $this->view_data['unit_of_measurement'] = null;

                if(isset($_GET['warehouse_id'])){
                    $this->view_data['warehouse_id'] = $_GET['warehouse_id'];
                    $warehouse = get_object_from_table($this->view_data['warehouse_id'], 'warehouses', false);
                    $this->view_data['warehouse_name'] = $warehouse['name'];
                }else{
                    $this->view_data['warehouse_id'] = null;
                    $this->view_data['warehouse_name'] = "";
                }
            }

            $this->view_data['fillables'] = $fillables;
            $this->view_data['current_user'] = $this->user->id;
            $this->view_data['method'] = $method;
            $this->view_data['obj'] = $obj;
            $this->view_data['id'] = $id ? $id : null;
            $this->view_data['form_data'] = $form_data;
            $this->view_data['reference'] = $reference;


            $this->js_loader->queue([
                'js/vue2.js',
                'js/axios.min.js',
                'js/utils.js'
            ]);

            $this->template->build('form', $this->view_data);


        }

        public function delete()
        {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $id = $this->input->post('id');
            if ($id) {

                $list = $this->M_Warehouse_inventory->get($id);
                if ($list) {

                    $deleted = $this->M_Warehouse_inventory->delete($list['id']);
                    if ($deleted !== false) {

                        $response['status'] = 1;
                        $response['message'] = 'Warehouse Inventory Successfully Deleted';
                    }
                }
            }

            echo json_encode($response);
            exit();
        }

        public function bulkDelete()
        {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            if ($this->input->is_ajax_request()) {
                $delete_ids = $this->input->post('deleteids_arr');

                if ($delete_ids) {
                    foreach ($delete_ids as $value) {
                        $data = [
                            'deleted_by' => $this->session->userdata['user_id']
                        ];
                        $this->db->update('item_group', $data, array('id' => $value));
                        $deleted = $this->M_Warehouse_inventory->delete($value);
                    }
                    if ($deleted !== false) {

                        $response['status'] = 1;
                        $response['message'] = 'Warehouse Inventory Successfully Deleted';
                    }
                }
            }

            echo json_encode($response);
            exit();
        }

        public function view($id = FALSE)
        {
            if ($id) {
                $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
                $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

                $this->view_data['data'] = $this->__get_obj($id);

                if ($this->view_data['data']) {

                    $this->template->build('view', $this->view_data);
                } else {

                    show_404();
                }
            } else {
                show_404();
            }
        }

        public function import()
        {

            $file = $_FILES['csv_file']['tmp_name'];
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);

            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $err = 0;


            foreach ($sheetData as $key => $upload_data) {

                if ($key > 0) {

                    if ($this->input->post('status') == '1') {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $warehouse_inventory_id = $upload_data[0];
                        $warehouse_inventory = $this->M_Warehouse_inventory->get($warehouse_inventory_id);

                        if ($warehouse_inventory) {
                            $result = $this->M_Warehouse_inventory->update($fields, $warehouse_inventory_id);
                        }

                    } else {

                        if (!is_numeric($upload_data[0])) {
                            $fields = array(
                                'name' => $upload_data[1],
                                /* ==================== begin: Add model fields ==================== */

                                /* ==================== end: Add model fields ==================== */
                            );

                            $result = $this->M_Warehouse_inventory->insert($fields);
                        } else {
                            $fields = array(
                                'name' => $upload_data[0],
                                /* ==================== begin: Add model fields ==================== */

                                /* ==================== end: Add model fields ==================== */
                            );

                            $result = $this->M_Warehouse_inventory->insert($fields);
                        }

                    }
                    if ($result === FALSE) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                        $err = 1;
                        break;
                    }
                }
            }

            if ($err == 0) {
                $this->notify->success('CSV successfully imported.', 'warehouse_inventory');
            } else {
                $this->notify->error('Oops something went wrong.');
            }

            header('Location: ' . base_url() . 'warehouse_inventory');
            die();
        }

        public function export_csv()
        {
            if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

                $_ued = $this->input->post('update_existing_data');

                $_is_update = $_ued === '1' ? TRUE : FALSE;

                $_alphas = [];
                $_datas = [];

                $_titles[] = 'id';

                $_start = 3;
                $_row = 2;

                $_filename = 'Warehouse Inventory CSV Template.csv';

                $_fillables = $this->_table_fillables;
                if (!$_fillables) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse_inventory');
                }

                foreach ($_fillables as $_fkey => $_fill) {

                    if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                        $_titles[] = $_fill;
                    } else {

                        continue;
                    }
                }

                if ($_is_update) {

                    $_group = $this->M_Warehouse_inventory->as_array()->get_all();
                    if ($_group) {

                        foreach ($_titles as $_tkey => $_title) {

                            foreach ($_group as $_dkey => $li) {

                                $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                            }
                        }
                    }
                } else {

                    if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                        unset($_titles[0]);
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));
                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                $_objSheet = $this->excel->getActiveSheet();
                $_objSheet->setTitle('Warehouse Inventory');
                $_objSheet->setCellValue('A1', 'WAREHOUSE INVENTORY');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_objSheet->setCellValue($_xkey . $_row, $_column);
                }

                if ($_is_update) {

                    if (isset($_datas) && $_datas) {

                        foreach ($_datas as $_dkey => $_data) {

                            foreach ($_alphas as $_akey => $_alpha) {

                                $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                                $_objSheet->setCellValue($_alpha . $_start, $_value);
                            }

                            $_start++;
                        }
                    } else {

                        $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                        $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                        $_style = array(
                            'font' => array(
                                'bold' => FALSE,
                                'size' => 9,
                                'name' => 'Verdana'
                            )
                        );
                        $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                show_404();
            }
        }

        public function export()
        {

            $_db_columns = [];
            $_alphas = [];
            $_datas = [];

            $_titles[] = '#';

            $_start = 3;
            $_row = 2;
            $_no = 1;

            $warehouse_inventories = $this->M_Warehouse_inventory->as_array()->get_all();
            if ($warehouse_inventories) {

                foreach ($warehouse_inventories as $_lkey => $warehouse_inventory) {

                    $_datas[$warehouse_inventory['id']]['#'] = $_no;

                    $_no++;
                }

                $_filename = 'list_of_warehouse_inventories_' . date('m_d_y_h-i-s', time()) . '.xls';

                $_objSheet = $this->excel->getActiveSheet();

                if ($this->input->post()) {

                    $_export_column = $this->input->post('_export_column');
                    if ($_export_column) {

                        foreach ($_export_column as $_ekey => $_column) {

                            $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                        }
                    } else {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse_inventory');
                    }
                } else {

                    $_filename = 'list_of_warehouse_inventories_' . date('m_d_y_h-i-s', time()) . '.csv';

                    // $_db_columns	=	$this->M_land_inventory->fillable;
                    $_db_columns = $this->_table_fillables;
                    if (!$_db_columns) {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse_inventory');
                    }
                }

                if ($_db_columns) {

                    foreach ($_db_columns as $key => $_dbclm) {

                        $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                        if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                            if ((strpos($_name, '_id') !== FALSE)) {

                                $_column = $_name;

                                $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                                $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            } elseif ((strpos($_name, 'is_') !== FALSE)) {

                                $_column = $_name;

                                $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                                $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                                foreach ($warehouse_inventories as $_lkey => $warehouse_inventory) {

                                    $_datas[$warehouse_inventory['id']][$_title] = isset($warehouse_inventory[$_column]) && ($warehouse_inventory[$_column] !== '') ? Dropdown::get_static('bool', $warehouse_inventory[$_column], 'view') : '';
                                }
                            } else {

                                $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                                foreach ($warehouse_inventories as $_lkey => $warehouse_inventory) {

                                    if ($_name === 'status') {

                                        $_datas[$warehouse_inventory['id']][$_title] = isset($warehouse_inventory[$_name]) && $warehouse_inventory[$_name] ? Dropdown::get_static('inventory_status', $warehouse_inventory[$_name], 'view') : '';
                                    } else {

                                        $_datas[$warehouse_inventory['id']][$_title] = isset($warehouse_inventory[$_name]) && $warehouse_inventory[$_name] ? $warehouse_inventory[$_name] : '';
                                    }
                                }
                            }
                        } else {

                            continue;
                        }
                    }

                    $_alphas = $this->__get_excel_columns(count($_titles));

                    $_xls_columns = array_combine($_alphas, $_titles);
                    $_firstAlpha = reset($_alphas);
                    $_lastAlpha = end($_alphas);

                    foreach ($_xls_columns as $_xkey => $_column) {

                        $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                        $_objSheet->setCellValue($_xkey . $_row, $_title);
                    }

                    $_objSheet->setTitle('List of Warehouse Inventory');
                    $_objSheet->setCellValue('A1', 'LIST OF WAREHOUSE INVENTORY');
                    $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                    if (isset($_datas) && $_datas) {

                        foreach ($_datas as $_dkey => $_data) {

                            foreach ($_alphas as $_akey => $_alpha) {

                                $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                                $_objSheet->setCellValue($_alpha . $_start, $_value);
                            }

                            $_start++;
                        }
                    } else {

                        $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                        $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                        $_style = array(
                            'font' => array(
                                'bold' => FALSE,
                                'size' => 9,
                                'name' => 'Verdana'
                            )
                        );
                        $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }

                    foreach ($_alphas as $_alpha) {

                        $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                    }

                    $_style = array(
                        'font' => array(
                            'bold' => TRUE,
                            'size' => 10,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment; filename="' . $_filename . '"');
                    header('Cache-Control: max-age=0');
                    $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                    @ob_end_clean();
                    $_objWriter->save('php://output');
                    @$_objSheet->disconnectWorksheets();
                    unset($_objSheet);
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse_inventory');
                }
            } else {

                $this->notify->error('No Record Found', 'warehouse_inventory');
            }
        }


    }
