<script type="text/javascript">
    window.object_request_id = "<?php echo $id;?>";
    window.item_id = "<?php echo $item['id'];?>";
    window.object_request_fillables = {};
    window.current_user_id = "<?=current_user_id();?>";
    window.object_status = "<?php echo $form_data['status'];?>";
    <?php foreach($fillables as $fillable):?>
    window.object_request_fillables["<?php echo $fillable;?>"] = null;
    <?php endforeach;?>

    window.warehouse_id = "<?php echo $warehouse_id;?>";
    window.warehouse_name = "<?php echo $warehouse_name;?>";
</script>
<div id="warehouse_inventory_app">
    <!--begin::Form-->
    <form method="POST" class="kt-form kt-form--label-right" id="form_warehouse_inventory" enctype="multipart/form-data"
          action="<?php form_open('warehouse_inventory/update/' . @$obj['id']); ?>">
        <!-- CONTENT HEADER -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title"><?= $method ?> <?php echo $warehouse_name;?> Inventory</h3>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <button type="submit" class="btn btn-label-success btn-elevate btn-sm"
                                form="form_warehouse_inventory"
                                data-ktwizard-type="action-submit" @click.prevent="submitRequest()">
                            <i class="fa fa-plus-circle"></i> Submit
                        </button>
                        <a href="<?php echo site_url('warehouse/view/' . $warehouse_id); ?>"
                           class="btn btn-label-instagram btn-elevate btn-sm">
                            <i class="fa fa-reply"></i> Cancel
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- CONTENT -->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class=" kt-portlet__body">
                        <div class="row">
                            <?php $this->load->view('form/_update_info'); ?>
                            <?php $this->load->view('form/_update_items'); ?>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!-- begin:: Footer -->
    </form>
    <!--end::Form-->
</div>