<div class="col-sm-12 col-md-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><?php echo $item['name']; ?></h3>
        </div>
        <div class="card-body">
            <div class="kt-widget13">
                <!-- ==================== begin: Add fields details  ==================== -->
                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Code
                                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $item['code']; ?></span>
                </div>

                <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $item['description']; ?></span>
            </div>
            <!-- ==================== end: Add model details ==================== -->
        </div>
    </div>
</div>