<div class="col-sm-12 col-md-9">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Inventory Breakdown</h3>
        </div>
        <div class="card-body">
            <div class="spinner-border text-success" role="status" v-if="material_items.loading">
                <span class="sr-only">Loading...</span>
            </div>
            <div class="table-responsive" v-else>
                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>Receiving</th>
                        <th>Receiving Type</th>
                        <th>Unit Price</th>
                        <th>Unit</th>
                        <th>Original Qty</th>
                        <th>Available Qty</th>
                        <th>Updated Original Qty</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(mi, idx) in material_items.data">
                        <td>{{ mi.material_receiving.reference }}</td>
                        <td>{{ mi.receiving_type_label }}</td>
                        <td>{{ mi.unit_cost }}</td>
                        <td>{{ mi.unit_of_measurement.name }}</td>
                        <td>{{ mi.quantity }}</td>
                        <td>{{ mi.stock_data.remaining }}</td>
                        <td>
                            <input type="number" step="1" min="0" :max="mi.stock_data.remaining" v-model="material_items.form[idx].quantity" class="form-control">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>