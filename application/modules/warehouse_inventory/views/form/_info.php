<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <select class="form-control suggests" data-module="warehouses" id="warehouse_id"
                        name="id" disabled>
                    <?php if ($warehouse_id): ?>
                        <option value="<?php echo $warehouse_id; ?>"
                                selected><?php echo $warehouse_name; ?></option>
                    <?php endif ?>
                </select>

            </div>
            <span class="form-text text-muted"></span>
        </div>

        <div class="form-group my-3">
            <label>Item <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <select class="form-control suggests" data-module="item" id="item_id"
                        name="id">
                    <?php if ($item): ?>
                        <option value="<?php echo $item['id']; ?>"
                                selected><?php echo $item['name']; ?></option>
                    <?php endif ?>
                </select>

            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group my-3">
            <label>Unit of Measurement <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <select class="form-control suggests" data-module="inventory_settings_unit_of_measurements"
                        id="unit_of_measurement_id"
                        name="id">
                    <?php if ($unit_of_measurement): ?>
                        <option value="<?php echo $unit_of_measurement['id']; ?>"
                                selected><?php echo $unit_of_measurement['name']; ?></option>
                    <?php endif ?>
                </select>

            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group my-3">
            <label>Actual Quantity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <input type="number" min="0" step="0.01" class="form-control" id="actual_quantity"
                       name="actual_quantity" placeholder="Actual Quantity" v-model="info.form.actual_quantity">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>


            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Unit Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" min="0" step="0.01" class="form-control" id="unit_price" name="unit_price"
                       placeholder="Unit Price" v-model="info.form.unit_price">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
                <div class="spinner-border text-success" role="status" v-if="canvass.loading">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div v-if="sources.canvass" class="my-3">
            <h5>Item Unit Price Data Points</h5>
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>Source</th>
                    <th>Unit Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Item DB</td>
                    <td>{{ sources.canvass.item.unit_price }}</td>
                    <td>
                        <a href="#!" class="btn btn-sm btn-primary"
                           @click.prevent="changeInfo('unit_price', sources.canvass.item.unit_price)">
                            Set
                        </a>
                    </td>
                </tr>
                <tr v-for="c in sources.canvass.canvasses">
                    <td>
                        Canvass {{ c.canvass.created_at }}
                    </td>
                    <td>{{ c.item.unit_cost }}</td>
                    <td>
                        <a href="#!" class="btn btn-sm btn-primary"
                           @click.prevent="changeInfo('unit_price', c.item.unit_cost)">
                            Set
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Average of Canvass</td>
                    <td>{{ sources.canvass.canvass_avg }}</td>
                    <td><a href="#!" class="btn btn-sm btn-primary"
                           @click.prevent="changeInfo('unit_price', sources.canvass.canvass_avg)">
                            Set
                        </a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>