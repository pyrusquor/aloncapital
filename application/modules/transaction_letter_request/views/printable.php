<!DOCTYPE html>
<html>

<head>
    <title>REQUEST LETTER</title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 15px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th {
            padding: 5px 8px;
        }

        .table>thead>tr>th {
            border-bottom: 2px solid #00529c;
        }

        .table>tfoot>tr>td {
            border-top: 2px solid #00529c;
        }

        .table>tfoot {
            border-top: 2px solid #ddd;
        }

        .table>tbody {
            border-bottom: 1px solid #fff;
        }

        .table.table-striped>thead>tr>th {
            background: #00529c;
            color: #fff;
            border: 0;
            padding: 12px 8px;
            text-transform: uppercase;
        }

        .table.table-striped>thead>tr>th a {
            color: #fff;
            font-weight: 400;
        }

        .table.table-striped>thead>tr:nth-child(2)>th {
            background: #0075de;
        }

        .table.table-striped td {
            border: 0;
            vertical-align: middle;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background: #fff;
        }

        .table-striped>tbody>tr:nth-of-type(even) {
            background: #f1f1f1;
        }

        .color-bluegreen {
            color: #169f98;
        }

        .color-white {
            color: #fff;
        }

        .peso_currency {
            font-family: DejaVu Sans;
        }

        .bg-bluegreen {
            background-color: #169f98;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .margin0 {
            margin: 0;
        }

        .padding10 {
            padding: 10px;
        }

        p {
            margin: 0px;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-warning {
            background-color: #fcf8e3;
            border-color: #faebcc;
            color: #8a6d3b;
        }

        .d-grid {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            gap: 1rem 3rem;
        }

        input[type='text'],
        select.form-control {
            background: transparent;
            border: none;
            border-bottom: 1px solid #000000;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        input[type='text']:focus,
        select.form-control:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>

<body style="max-width: 1050px;">
    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body m-5 p-5">
                <?php

                $id = isset($info['id']) && $info['id'] ? $info['id'] : "";

                $request_date = isset($info['request_date']) && $info['request_date'] ? $info['request_date'] : "";

                $request_body = isset($info['request_body']) && $info['request_body'] ? $info['request_body'] : "";

                $subject_id = isset($info['subject']) && $info['subject'] ? $info['subject'] : "";

                $letter_request_subjects = isset($letter_request_subjects) && $letter_request_subjects ? $letter_request_subjects : [];

                // vdebug($letter_request_subjects);

                $buyer = isset($transaction['buyer']) && $transaction['buyer'] ? $transaction['buyer']['first_name'] . ' ' . $transaction['buyer']['middle_name'] . ' ' . $transaction['buyer']['last_name'] : "";

                $project_name = isset($transaction['project']['name']) && $transaction['project']['name'] ? $transaction['project']['name'] : "";

                $project_location = isset($transaction['project']['location']) && $transaction['project']['location'] ? $transaction['project']['location'] : "";

                $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : "";

                ?>

                <div class="WordSection1 mb-4">
                    <p class="MsoNormal" style="margin-left: 187.0pt; mso-line-height-alt: 0pt;">
                        <span style="font-size: 12.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Cambria','serif'; mso-fareast-font-family: Cambria;">
                            REAL ESTATE BUSINESS GROUP
                        </span>
                    </p>
                    <p class="MsoNormal" style="margin-left: 213.0pt; line-height: 95%;">
                        <strong>
                            <span style="font-size: 14.0pt; mso-bidi-font-size: 10.0pt; line-height: 95%; font-family: 'Cambria','serif'; mso-fareast-font-family: Cambria;">
                                REQUEST LETTER
                            </span>
                        </strong>
                    </p>
                    <p class="MsoNormal" style="line-height: 10.1pt; mso-line-height-rule: exactly;">
                        <span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span>
                    </p>
                    <div class="d-flex mb-2">
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                Client/s
                            </span>
                        </p>
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span>
                        </p>
                        <p class="border-bottom border-dark w-100 pl-2">
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                <?= $buyer ?>
                            </span>
                        </p>
                    </div>
                    <p class="MsoNormal" style="line-height: .55pt; mso-line-height-rule: exactly;">
                        <span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span>
                    </p>
                    <div class="d-flex mb-4">
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                Project
                            </span>
                        </p>
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</span>
                        </p>
                        <p>
                            <span class="border-bottom border-dark px-2" style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                <?= $property_name ?>
                            </span>
                        </p>&nbsp; &nbsp; &nbsp;
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                Phase/Block/Lot :
                            </span>
                            <span class="border-bottom border-dark px-2" style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                <?= $project_name ?>
                            </span>
                        </p>&nbsp; &nbsp; &nbsp;
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                Area:
                            </span>
                            <span class="border-bottom border-dark px-2" style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                <?= $project_location ?>
                            </span>
                        </p>
                    </div>
                    <div class="d-flex mb-4">
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;" class="text-nowrap">
                                Account Status
                            </span>
                        </p>
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;:&nbsp;</span>
                        </p>
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;" class="text-nowrap">
                                Pls. Check
                            </span>
                        </p>
                        <div class="d-flex flex-wrap align-content-start ml-3">
                            <?php foreach (Dropdown::get_static('period_names') as $key => $period) : ?>
                                <?php if ($key && $key + 1 != count(Dropdown::get_static('period_names'))) : ?>
                                    <p class="MsoNormal mr-5" style="margin-left: 2.0pt; mso-line-height-alt: 0pt;">
                                        <span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                            <?php if ($transaction['general_status'] == '3') : ?>
                                                (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;) <?= $period ?>
                                            <?php else : ?>
                                                (<?= $transaction['period_id'] == $key ? '&nbsp;<span style="font-weight: bold;">&check;</span>&nbsp;' : '&nbsp;&nbsp;&nbsp;&nbsp;' ?>) <?= $period ?>
                                            <?php endif ?>
                                        </span>
                                    </p>
                                <?php endif ?>
                            <?php endforeach ?>
                            <p class="MsoNormal mr-5" style="margin-left: 2.0pt; mso-line-height-alt: 0pt;">
                                <span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                    (<?= $transaction['general_status'] == '3' ? '&nbsp;<span style="font-weight: bold;">&check;</span>&nbsp;' : '&nbsp;&nbsp;&nbsp;&nbsp;' ?>) Fully Paid
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="d-flex mb-2">
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                Subject
                            </span>
                        </p>
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</span>
                        </p>
                        <p>
                            <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;" class="text-nowrap">
                                Pls. Check
                            </span>
                        </p>
                        <div class="d-flex flex-wrap align-content-start ml-3">
                            <table>
                                <?php foreach ($letter_request_subjects as $key => $subject) : ?>
                                    <?php if (!fmod($key, 3)) : ?>
                                        <tr>
                                        <?php endif ?>
                                        <td class="pr-5">
                                            <p class="MsoNormal" style="margin-left: 2.0pt; mso-line-height-alt: 0pt;">
                                                <span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                                                    (<?= $subject_id == $subject['id'] ? '&nbsp;<span style="font-weight: bold;">&check;</span>&nbsp;' : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>) <?php if ($subject['name'] == 'Others') : ?>
                                                        <?= $subject['name'] . ' ________________' ?>
                                                    <?php else : ?>
                                                        <?= $subject['name'] ?>
                                                    <?php endif ?>
                                                </span>
                                            </p>
                                        </td>
                                        <?php if (!fmod($key + 1, 3)) : ?>
                                        </tr>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </table>
                        </div>
                    </div>
                    <p class="MsoNormal" style="line-height: 10.0pt; mso-line-height-rule: exactly;">
                        <!-- [if gte vml 1]><v:shapetype
                        id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
                        path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
                        <v:stroke joinstyle="miter"/>
                        <v:formulas>
                        <v:f eqn="if lineDrawn pixelLineWidth 0"/>
                        <v:f eqn="sum @0 1 0"/>
                        <v:f eqn="sum 0 0 @1"/>
                        <v:f eqn="prod @2 1 2"/>
                        <v:f eqn="prod @3 21600 pixelWidth"/>
                        <v:f eqn="prod @3 21600 pixelHeight"/>
                        <v:f eqn="sum @0 0 1"/>
                        <v:f eqn="prod @6 1 2"/>
                        <v:f eqn="prod @7 21600 pixelWidth"/>
                        <v:f eqn="sum @8 21600 0"/>
                        <v:f eqn="prod @7 21600 pixelHeight"/>
                        <v:f eqn="sum @10 21600 0"/>
                        </v:formulas>
                        <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
                        <o:lock v:ext="edit" aspectratio="t"/>
                        </v:shapetype><v:shape id="Picture_x0020_3" o:spid="_x0000_s1026" type="#_x0000_t75"
                        style='position:absolute;margin-left:-.6pt;margin-top:3.8pt;width:543pt;
                        height:.4pt;z-index:-251658752;mso-wrap-style:square;
                        mso-position-horizontal-relative:text;mso-position-vertical-relative:text'
                        o:allowincell="f">
                        <v:imagedata src="file:///C:\Users\User\AppData\Local\Temp\msohtmlclip1\01\clip_image001.jpg"
                        o:title=""/>
                        </v:shape><![endif]-->
                        <!-- [if !vml]--><span style="mso-ignore: vglayout; position: absolute; z-index: 251657725; margin-left: -1px; margin-top: 5px; width: 724px; height: 1px;"><img src="file:///C:/Users/User/AppData/Local/Temp/msohtmlclip1/01/clip_image002.jpg" width="724" height="1" /></span>
                        <!--[endif]-->
                    </p>
                    <p class="MsoNormal" style="line-height: 99%;">
                        <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;" class="mr-1">
                            Date:
                        </span>
                        <span class="px-4 border-bottom border-dark" style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                            <?= view_date($request_date) ?>
                        </span>
                    </p>
                    <p class="MsoNormal" style="line-height: 9.8pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                    <p class="MsoNormal" style="line-height: 99%;"><strong><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">MR. JOSE NERY D. ONG<br /></span></strong><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Chief Executive Officer<br /></span><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Sacred Heart of Jesus Prime Holdings, Inc.<br /></span><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Punta Dulog Commercial Complex, St. Joseph Avenue, Pueblo de Panay Township, Lawaan, Roxas City, Capiz.</span></p>
                </div>
                <!-- <div class="WordSection1">______________________________________________________________________________________________________________</div>
                <div class="WordSection1">______________________________________________________________________________________________________________</div>
                <div class="WordSection1">______________________________________________________________________________________________________________</div>
                <div class="WordSection1">______________________________________________________________________________________________________________</div> -->
                <div class="mb-4">
                    <p class="MsoNormal mb-2" style="line-height: 99%;">
                        <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                            Dear Sir:
                        </span>
                    </p>
                    <p class="w-100">
                        <span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $request_body ?>
                        </span>
                    </p>
                </div>
                <div class="WordSection1">
                    <p class="MsoNormal" style="line-height: 99%;"><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Sincerely yours,</span></p>
                    <p class="MsoNormal" style="margin-left: 39.0pt; text-indent: 31.5pt; mso-char-indent-count: 3.5; line-height: 99%; tab-stops: 290.0pt;"><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">___________________________________</span><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span lang="EN-US" style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">___________________________________<br /></span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Signature over printed name</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Signature over printed name</span></p>
                    <p class="MsoNormal" style="line-height: 14.55pt; mso-line-height-rule: exactly;">
                        <!-- [if gte vml 1]><v:shape
                            id="Picture_x0020_4" o:spid="_x0000_s1027" type="#_x0000_t75" style='position:absolute;
                            margin-left:-1.35pt;margin-top:1pt;width:543pt;height:.4pt;z-index:-251657728;
                            mso-wrap-style:square' o:allowincell="f">
                            <v:imagedata src="file:///C:\Users\User\AppData\Local\Temp\msohtmlclip1\01\clip_image001.jpg"
                            o:title=""/>
                            </v:shape><![endif]-->
                        <!-- [if !vml]--><span style="mso-ignore: vglayout; position: absolute; z-index: 251658749; margin-left: -2px; margin-top: 1px; width: 724px; height: 1px;"><img src="file:///C:/Users/User/AppData/Local/Temp/msohtmlclip1/01/clip_image002.jpg" width="724" height="1" /></span>
                        <!--[endif]--><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">RECOMMENDATIONS:</span>
                    </p>
                    <p class="MsoNormal" style="line-height: 4.65pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">SHJDC CORPORATE SALES (GSM)</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">SHJDC CORPORATE SALES HEAD/ THR SUPERVISOR:</span></p>
                    <p class="MsoNormal"><span style="font-size: 9.0pt; line-height: 80%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_________________________________________________________</span><span style="font-size: 9.0pt; line-height: 80%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;__</span><span style="font-size: 9.0pt; line-height: 80%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_______________________________________________________</span></p>
                    <p class="MsoNormal"><span style="font-size: 9.0pt; line-height: 80%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_________________________________________________________&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;_________________________________________________________</span></p>
                    <p class="MsoNormal"><span style="font-size: 9.0pt; line-height: 80%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_________________________________________________________&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;_________________________________________________________</span></p>
                    <p class="MsoNormal"><span style="font-size: 9.0pt; line-height: 80%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_________________________________________________________&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;_________________________________________________________</span></p>
                    <p class="MsoNormal">&nbsp;</p>
                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">By: _______________________</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Date: __________________</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">By: _______________________</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Date:___________________</span></p>
                    <p class="MsoNormal" style="line-height: 10.35pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                    <p class="MsoNormal" style="line-height: 99%;"><strong><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Credit and Collection Dept.:</span></strong></p>
                    <p class="MsoNormal" style="line-height: 99%;"><strong><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">______________________________________________________________________________________________________________</span></strong></p>
                    <p class="MsoNormal" style="line-height: 99%;"><strong><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">______________________________________________________________________________________________________________</span></strong></p>
                    <p class="MsoNormal" style="line-height: 99%;"><strong><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">______________________________________________________________________________________________________________</span></strong></p>
                    <p class="MsoNormal" style="line-height: 99%;"><strong><span style="font-size: 9.0pt; line-height: 99%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">______________________________________________________________________________________________________________</span></strong></p>
                    <p class="MsoNormal" style="line-height: 99%;">&nbsp;</p>
                    <span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">__________________________</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Date: ____________________</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Pre-Approved by: _________________</span>__________</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp; </span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Date: _______________</span>
                    <p class="MsoNormal" style="line-height: 98%; tab-stops: 35.0pt 334.0pt;"><span style="font-size: 9.0pt; line-height: 98%; font-family: 'Times New Roman','serif';">By:</span><span style="font-size: 9.0pt; line-height: 98%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><strong><span style="font-size: 9.0pt; line-height: 98%; font-family: 'Times New Roman','serif';">JUDITH ALFORTE</span></strong><span style="font-size: 9.0pt; line-height: 98%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><strong><span style="font-size: 9.0pt; line-height: 98%; font-family: 'Times New Roman','serif';">GENALYN C. BAGUIO</span></strong></p>
                    <p class="MsoNormal" style="line-height: 4.3pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                    <p class="MsoNormal"><strong><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Real Estate Business Group:</span></strong></p>
                    <p class="MsoNormal" style="line-height: 10.4pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">______________________________________________________________________________________________________________</span></p>
                    <p class="MsoNormal" style="line-height: 10.4pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">______________________________________________________________________________________________________________</span></p>
                    <p class="MsoNormal" style="line-height: 10.4pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">______________________________________________________________________________________________________________</span></p>
                    <p class="MsoNormal" style="line-height: 10.4pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">______________________________________________________________________________________________________________</span></p>
                    <p class="MsoNormal" style="line-height: 10.4pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                    <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 8.7pt;">
                                <td style="width: 115.65pt; padding: 0in 0in 0in 0in; height: 8.7pt;" colspan="2" valign="bottom" width="154">&nbsp;</td>
                                <td style="width: 131.75pt; padding: 0in 0in 0in 0in; height: 8.7pt;" colspan="2" valign="bottom" width="176">
                                    <p class="MsoNormal" style="margin-left: 9.0pt; mso-line-height-alt: 0pt;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Date: _____________________</span></p>
                                </td>
                                <td style="width: 289.6pt; padding: 0in 0in 0in 0in; height: 8.7pt;" valign="bottom" width="386">
                                    <p class="MsoNormal" style="text-align: right; mso-line-height-alt: 0pt;" align="right"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Pre-Approved by: ___________________________&nbsp;&nbsp; Date: _______________</span></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 1; height: 10.9pt;">
                                <td style="width: 106.6pt; padding: 0in 0in 0in 0in; height: 10.9pt;" valign="bottom" width="142">
                                    <p class="MsoNormal" style="line-height: 12.1pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif';">By:&nbsp; <strong>DOROTY E. BABIS</strong></span></p>
                                </td>
                                <td style="width: 9.05pt; padding: 0in 0in 0in 0in; height: 10.9pt;" valign="bottom" width="12">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 131.75pt; padding: 0in 0in 0in 0in; height: 10.9pt;" colspan="2" valign="bottom" width="176">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 289.6pt; padding: 0in 0in 0in 0in; height: 10.9pt;" valign="bottom" width="386">
                                    <p class="MsoNormal" style="margin-right: 107.1pt; text-align: right; line-height: 12.1pt; mso-line-height-rule: exactly;" align="right"><strong><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif';">LOTTA S. FRANCISCO</span></strong></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 2; height: 15.6pt;">
                                <td style="width: 115.65pt; padding: 0in 0in 0in 0in; height: 15.6pt;" colspan="2" valign="bottom" width="154">
                                    <p class="MsoNormal" style="line-height: 10.8pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">&nbsp;</span></p>
                                    <p class="MsoNormal" style="line-height: 10.8pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Chief Executive Officer/VPs:</span></p>
                                </td>
                                <td style="width: 131.75pt; padding: 0in 0in 0in 0in; height: 15.6pt;" colspan="2" valign="bottom" width="176">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 289.6pt; padding: 0in 0in 0in 0in; height: 15.6pt;" valign="bottom" width="386">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 3; height: 12.6pt;">
                                <td style="width: 106.6pt; padding: 0in 0in 0in 0in; height: 12.6pt;" valign="bottom" width="142">
                                    <p class="MsoNormal" style="line-height: 10.8pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Action :&nbsp; ( </span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma; mso-ansi-language: EN-US;">&nbsp;</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">&nbsp;) Approved</span></p>
                                </td>
                                <td style="width: 9.05pt; padding: 0in 0in 0in 0in; height: 12.6pt;" valign="bottom" width="12">
                                    <p class="MsoNormal" style="text-align: right; line-height: 10.8pt; mso-line-height-rule: exactly;" align="right"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">(</span></p>
                                </td>
                                <td style="width: 131.75pt; padding: 0in 0in 0in 0in; height: 12.6pt;" colspan="2" valign="bottom" width="176">
                                    <p class="MsoNormal" style="margin-left: 2.0pt; text-indent: 4.5pt; mso-char-indent-count: .5; line-height: 10.8pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">) Disapproved</span></p>
                                </td>
                                <td style="width: 289.6pt; padding: 0in 0in 0in 0in; height: 12.6pt;" valign="bottom" width="386">
                                    <p class="MsoNormal" style="margin-right: 198.1pt; text-align: right; mso-line-height-alt: 0pt;" align="right"><strong><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">REMARKS:</span></strong></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 4; height: 11.85pt;">
                                <td style="width: 106.6pt; padding: 0in 0in 0in 0in; height: 11.85pt;" valign="bottom" width="142">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 9.05pt; padding: 0in 0in 0in 0in; height: 11.85pt;" valign="bottom" width="12">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 131.75pt; padding: 0in 0in 0in 0in; height: 11.85pt;" colspan="2" valign="bottom" width="176">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 289.6pt; padding: 0in 0in 0in 0in; height: 11.85pt;" valign="bottom" width="386">
                                    <p class="MsoNormal" style="text-align: right; line-height: 13.15pt; mso-line-height-rule: exactly;" align="right"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_________________________________________</span></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 5; height: 11.85pt;">
                                <td style="width: 106.6pt; padding: 0in 0in 0in 0in; height: 11.85pt;" rowspan="2" valign="bottom" width="142">
                                    <p class="MsoNormal"><span lang="EN-US" style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma; mso-ansi-language: EN-US;">___</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">____________________</span></p>
                                </td>
                                <td style="width: 9.05pt; padding: 0in 0in 0in 0in; height: 11.85pt;" valign="bottom" width="12">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 131.75pt; padding: 0in 0in 0in 0in; height: 11.85pt;" colspan="2" rowspan="2" valign="bottom" width="176">
                                    <p class="MsoNormal" style="margin-left: 13.0pt; mso-line-height-alt: 0pt;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Date:</span><span lang="EN-US" style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma; mso-ansi-language: EN-US;">_______________</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_______</span></p>
                                </td>
                                <td style="width: 289.6pt; padding: 0in 0in 0in 0in; height: 11.85pt;" valign="bottom" width="386">
                                    <p class="MsoNormal" style="text-align: right; line-height: 13.15pt; mso-line-height-rule: exactly;" align="right"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_________________________________________</span></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 6; height: 4.5pt;">
                                <td style="width: 9.05pt; padding: 0in 0in 0in 0in; height: 4.5pt;" valign="bottom" width="12">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 289.6pt; padding: 0in 0in 0in 0in; height: 4.5pt;" rowspan="2" valign="bottom" width="386">
                                    <p class="MsoNormal" style="text-align: right; line-height: 13.2pt; mso-line-height-rule: exactly;" align="right"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_________________________________________</span></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 7; height: 4.5pt;">
                                <td style="width: 106.6pt; padding: 0in 0in 0in 0in; height: 4.5pt;" valign="bottom" width="142">
                                    <p class="MsoNormal" style="margin-left: 3.0pt; line-height: 8.75pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">By:&nbsp; <strong>JOSE NERY D. ONG</strong></span></p>
                                </td>
                                <td style="width: 9.05pt; padding: 0in 0in 0in 0in; height: 4.5pt;" valign="bottom" width="12">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                                <td style="width: 131.75pt; padding: 0in 0in 0in 0in; height: 4.5pt;" colspan="2" valign="bottom" width="176">
                                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;</span></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 8; height: 11.85pt;">
                                <td style="width: 148.85pt; padding: 0in 0in 0in 0in; height: 11.85pt;" colspan="3" rowspan="2" valign="bottom" width="198">
                                    <p class="MsoNormal"><strong><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">or</span></strong></p>
                                </td>
                                <td style="width: 388.15pt; padding: 0in 0in 0in 0in; height: 11.85pt;" colspan="2" valign="bottom" width="518">
                                    <p class="MsoNormal" style="text-align: right; line-height: 13.15pt; mso-line-height-rule: exactly;" align="right"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">_________________________________________</span></p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 9; mso-yfti-lastrow: yes; height: 11.85pt;">
                                <td style="width: 388.15pt; padding: 0in 0in 0in 0in; height: 11.85pt;" colspan="2" valign="bottom" width="518">
                                    <p class="MsoNormal" style="text-align: right; line-height: 13.2pt; mso-line-height-rule: exactly;" align="right"><span lang="EN-US" style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma; mso-ansi-language: EN-US;">_________________________________________</span></p>
                                </td>
                            </tr>
                            <!-- [if !supportMisalignedColumns]-->
                            <tr>
                                <td style="border: initial none initial;" width="142">&nbsp;</td>
                                <td style="border: initial none initial;" width="12">&nbsp;</td>
                                <td style="border: initial none initial;" width="44">&nbsp;</td>
                                <td style="border: initial none initial;" width="131">&nbsp;</td>
                                <td style="border: initial none initial;" width="379">&nbsp;</td>
                            </tr>
                            <!--[endif]-->
                        </tbody>
                    </table>
                    <p class="MsoNormal" style="line-height: 15.8pt; mso-line-height-rule: exactly;"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">__________________________</span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">Date: __________________</span><span lang="EN-US" style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma; mso-ansi-language: EN-US;">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <em>R</em></span><em><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">evised REBG Form 08-2018</span></em></p>
                    <p class="MsoNormal"><span style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma;">By: <strong>HYACINTH O. VITERB</strong></span><strong><span lang="EN-US" style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Tahoma; mso-ansi-language: EN-US;">O</span></strong></p>
                </div>
                <p><span lang="EN-US" style="font-size: 9.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US; mso-fareast-language: EN-PH; mso-bidi-language: AR-SA;">&nbsp;</span></p>
            </div>
        </div>
    </div>
</body>

</html>