<?php

// vdebug($records);

?>

<div class="modal fade" id="requestApproveModal" tabindex="-1" role="dialog" aria-labelledby="requestApproveModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Approved Letter Request File Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="requestApproveForm" method="POST"
                    action="<?= base_url(); ?>transaction_letter_request/approve_request" method="post">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Approved Date <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon kt-input-icon--left">
                                    <div class="input-group">
                                        <input type="text" class="form-control kt_datepicker datePicker"
                                            name="approved_date"
                                            value="<?php echo set_value('approved_date', @$approved_date); ?>"
                                            placeholder="Approved Date" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Upload Scanned Copy of the Approved Request:</label>
                                <div>
                                    <?php if (get_image('transaction_letter_request', 'approved_file', $image)): ?>
                                    <img class="kt-widget__img"
                                        src="<?php echo base_url(get_image('transaction_letter_request', 'approved_file', $image)); ?>"
                                        width="90px" height="90px" />
                                    <?php endif; ?>
                                    <span class="btn btn-sm">
                                        <input type="file" name="approved_file" class="" aria-invalid="false">
                                    </span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="requestApproveForm">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>