<?php

$id = isset($transaction_letter_request['id']) && $transaction_letter_request['id'] ? $transaction_letter_request['id'] : '';

$transaction_id = isset($transaction_letter_request['transaction_id']) && $transaction_letter_request['transaction_id'] ? $transaction_letter_request['transaction_id'] : '';

$request_date = isset($transaction_letter_request['request_date']) && $transaction_letter_request['request_date'] && (strtotime($transaction_letter_request['request_date']) > 0) ? date_format(date_create($transaction_letter_request['request_date']), 'F j, Y') : 'N/A';

$subject_id = isset($transaction_letter_request['subject']) && $transaction_letter_request['subject'] ? $transaction_letter_request['subject'] : '';

$request_body = isset($transaction_letter_request['request_body']) && $transaction_letter_request['request_body'] ? $transaction_letter_request['request_body'] : 'N/A';

$image = isset($transaction_letter_request['image']) && $transaction_letter_request['image'] ? $transaction_letter_request['image'] : '';
$approved_image = isset($transaction_letter_request['approved_image']) && $transaction_letter_request['approved_image'] ? $transaction_letter_request['approved_image'] : '';
$status = isset($transaction_letter_request['status']) && $transaction_letter_request['status'] ? $transaction_letter_request['status'] : '';
$approve_date = isset($transaction_letter_request['approve_date']) && $transaction_letter_request['approve_date'] && (strtotime($transaction_letter_request['approve_date']) > 0) ? date_format(date_create($transaction_letter_request['approve_date']), 'F j, Y') : 'N/A';

$encoded_date = isset($transaction_letter_request['created_at']) && $transaction_letter_request['created_at'] && (strtotime($transaction_letter_request['created_at']) > 0) ? date_format(date_create($transaction_letter_request['created_at']), 'F j, Y') : 'N/A';

$subject = get_person($subject_id, 'letter_request_subjects');
$subject_name = get_name($subject) ? get_name($subject) : '';

$status_name = "Open Request";

if($status == 1) {
    $status_name = "Approved";
}

?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Letter Requests</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('transaction_letter_request'); ?>"
                    class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Letter Request Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>Request Date: <span class="font-weight-bold"><?php echo $request_date; ?></span></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Approved Date: <span class="font-weight-bold"><?php echo $approve_date; ?></span></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Encoded Date: <span class="font-weight-bold"><?php echo $encoded_date; ?></span></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>File Upload Date: <span class="font-weight-bold"><?php echo $encoded_date; ?></span></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Subject: <span class="font-weight-bold"><?php echo $subject_name; ?></span></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Status: <span class="font-weight-bold"><?php echo $status_name; ?></span></p>
                                </div>

                                <div class="col-sm-12 mt-3">
                                    <p>Request Body:</p>
                                    <p><?php echo $request_body; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Portlet-->
                     
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">
             <!--begin::Portlet-->
             <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Scanned Letter Request
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <p><a href="<?php echo base_url(get_image('letter_request', 'images', $image)); ?>" target="_blank" rel="noopener noreferrer">Document Link</a></p>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        <div class="col-md-6">
             <!--begin::Portlet-->
             <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Scanned Approved Letter Request
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <?php if($status != 1): ?>
                            <p class="font-italic font-weight-bold">Not yet approved.</p>
                        <?php else: ?>
                            <p><a href="<?php echo base_url(get_image('letter_request', 'images', $approved_image)); ?>" target="_blank" rel="noopener noreferrer">Document Link</a></p>
                        <?php endif; ?>
                        
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->