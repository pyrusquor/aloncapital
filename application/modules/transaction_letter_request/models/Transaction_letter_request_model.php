<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaction_letter_request_model extends MY_Model
{

	public $table = 'transaction_letter_request'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
		'reference',
		'request_date',
		'subject',
		'request_body',
		'image',
		'approved_image',
		'status',
		'approve_date',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = TRUE;
		$this->return_as = 'array';

		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		$this->has_one['letter_request_subjects'] = array('foreign_model' => 'letter_request_subjects/Letter_request_subjects_model', 'foreign_table' => 'letter_request_subjects', 'foreign_key' => 'id', 'local_key' => 'subject');

		$this->has_one['transaction'] = array('foreign_model' => 'transaction/Transaction_model', 'foreign_table' => 'tranactions', 'foreign_key' => 'id', 'local_key' => 'transaction_id');
	}

	public function get_columns()
	{
		$_return = false;

		if ($this->fillable) {
			$_return = $this->fillable;
		}

		return $_return;
	}

	public function get_search_letter_request($date_range=[]){
		$result = [];
        if(count($date_range)>1){
            $this->db->where("transaction_letter_request.request_date between '$date_range[0]' and '$date_range[1]'");
        }
        $this->db->select('letter_request_subjects.*');
        $this->db->select('transaction_letter_request.id as id');
        $this->db->select('transaction_letter_request.request_date as request_date');
        $this->db->select('transaction_letter_request.created_by as created_by');
        $this->db->select('letter_request_subjects.name as letter_request_subjects_name');
        $this->db->select('transactions.id as transaction_id');
        $this->db->select('transactions.reference as transaction_reference');
        $this->db->select('transactions.project_id as transaction_project_id');
        $this->db->select('transactions.property_id as transaction_property_id');
        $this->db->select('transactions.buyer_id as transaction_buyer_id');
        $this->db->join('letter_request_subjects','transaction_letter_request.subject=letter_request_subjects.id and letter_request_subjects.deleted_at is null','left');
        $this->db->join('transactions','transaction_letter_request.transaction_id=transactions.id and transactions.deleted_at is null','left');
        $this->db->where('transaction_letter_request.deleted_at is null');

        $query = $this->db->get('transaction_letter_request')->result_array();

        if($query){
            $result = $query;
        }
        
        return $result;
	}
}
