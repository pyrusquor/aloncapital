<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_letter_request extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('transaction_letter_request/Transaction_letter_request_model', 'M_Transaction_letter_request');
        $this->_table_fillables = $this->M_Transaction_letter_request->fillable;
        $this->_table_columns = $this->M_Transaction_letter_request->__get_columns();

        // Format Helper
        $this->load->helper(['format', 'images']);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Transaction_letter_request->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Transaction_letter_request->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showLetterRequests()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'transaction' => true,
            'project' => true,
            'property' => true,
            'buyer' => true,
            'request_date' => true,
            'subject' => true,
            'creator' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        // $transaction_letter_requests = $this->M_Transaction_letter_request
        //     ->with_letter_request_subjects()
        //     ->with_transaction()
        //     ->as_array()
        //     ->get_all();

        $request_date = $this->input->post('request_date');
        $date_range = [];

        if (!empty($request_date)) {
            $date_range = explode('-', $request_date);

            $from_str   = strtotime($date_range[0]);
            $from       = date('Y-m-d H:i:s', $from_str);

            $to_str     = strtotime($date_range[1] . '23:59:59');
            $to         = date('Y-m-d H:i:s', $to_str);
            $date_range = array($from, $to);
        }

        $transaction_letter_requests = $this->M_Transaction_letter_request->get_search_letter_request($date_range);

        $data = [];

        if ($transaction_letter_requests) {

            foreach ($transaction_letter_requests as $key => $value) {
                $transaction_letter_requests[$key]['subject'] = @$value['letter_request_subjects_name'];

                $transaction_letter_requests[$key]['transaction'] = '<a target="_blank" href="' . base_url() . 'transaction/view/' . @$value['transaction_id'] . '">' . @$value['transaction_reference'] . '</a>';

                // Project
                $project = isset($value['transaction_project_id']) && $value['transaction_project_id'] ? get_value_field(@$value['transaction_project_id'], 'projects', 'name') : '';
                $transaction_letter_requests[$key]['project'] = '<a target="_blank" href="' . base_url() . 'project/view/' . @$value['transaction']['project_id'] . '">' . $project . '</a>';

                // Property
                $property = isset($value['transaction_property_id']) && $value['transaction_property_id'] ? get_value_field(@$value['transaction_property_id'], 'properties', 'name') : '';
                $transaction_letter_requests[$key]['property'] = '<a target="_blank" href="' . base_url() . 'property/view/' . @$value['transaction']['property_id'] . '">' . $property . '</a>';

                // Buyer
                $buyer = get_person(@$value['transaction_buyer_id'], "buyers");
                $transaction_letter_requests[$key]['buyer'] = '<a target="_blank" href="' . base_url() . 'buyer/view/' . @$value['transaction_buyer_id'] . '">' . get_fname($buyer) . '</a>';

                // Creator
                $user = get_person(@$value['created_by'], "users");
                $transaction_letter_requests[$key]['creator'] = get_fname($user);

                $transaction_letter_requests[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $transaction_letter_requests[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($transaction_letter_requests as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }


    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['transaction_letter_request'] = $this->M_Transaction_letter_request->get($id);

            if ($this->view_data['transaction_letter_request']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function generate($id = false, $debug = 0)
    {
        if ($id) {
            $this->view_data['transaction_letter_request'] = $this->M_Transaction_letter_request->get($id);

            $this->view_data['info'] = $this->M_Transaction_letter_request
                ->with_letter_request_subjects()
                ->get($id);

            $this->load->model('transaction/Transaction_model', 'M_Transaction');
            $this->load->model('letter_request_subjects/Letter_request_subjects_model', 'M_Letter_request_subjects');

            $this->view_data['transaction'] = $this->M_Transaction
                ->with_buyer()
                ->with_project()
                ->with_property()
                ->get($this->view_data['info']['transaction_id']);

            $this->view_data['letter_request_subjects'] = $this->M_Letter_request_subjects->get_all();

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }

    public function approve_request($id = false)
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        if ($this->input->post()) {

            // Image Upload
            $upload_path = './assets/uploads/letter_request/approved_images';
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 5000;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config, 'approved_file');
            $this->approved_file->initialize($config);

            $info = $this->input->post();
            $data['approved_image'] =  "";

            if (!empty($_FILES['approved_file']['name'])) {

                if (!$this->approved_file->do_upload('approved_file')) {
                    $this->notify->error($this->approved_file->display_errors(), 'transaction_letter_request' . $id);
                } else {
                    $img_data = $this->approved_file->data();
                    $data['approved_image'] =  $img_data['file_name'];
                }
            }

            $data['status'] = 1;
            $data['approve_date'] = $info['approved_date'];

            // Get the request
            $request = $this->M_Transaction_letter_request->get($id);

            // Check if already approve
            $is_approved = $request['status'];


            // If not change status to approve
            if ($is_approved != 1) {
                $result = $this->M_Transaction_letter_request->update($data + $additional, $id);

                // If successfully changed notify user
                if ($result) {
                    $response['status'] = 1;
                    $response['message'] = 'Request is now approved.';
                } else {
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                }
                if ($response['status'] == 1) {
                    $this->notify->success($response['message'], 'transaction_letter_request');
                } else {
                    $this->notify->error($response['message'], 'transaction_letter_request');
                }
            } else {
                // If already approve notify user
                $response['status'] = 0;
                $response['message'] = 'This Request is already approved!';
                $this->notify->error($response['message'], 'transaction_letter_request');
            }

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Transaction_letter_request->get($id);
            if ($list) {

                $deleted = $this->M_Transaction_letter_request->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Letter Request Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Transaction_letter_request->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Vehicle/s Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
}
