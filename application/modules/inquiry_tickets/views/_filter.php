<?php if (!empty($records)): ?>
    <div class="row" id="kt_content">
        <?php foreach ($records as $r): ?>
            <?php
            $id = ($r['id'] ? $r['id'] : "");
            $reference = ($r['reference'] ? $r['reference'] : "N/A");
            
            $project_id = $r['project']['id'];
            $property_id = $r['property_id'];
            $house_model_id = $r['house_model_id'];
            $buyer_id = $r['buyer_id'];
            $department_id = $r['department_id'];
            $turn_over_date = $r['turn_over_date'];
            $warranty = $r['warranty'] ? 'Yes' : 'No';
            $inquiry_category_id = $r['inquiry_category_id'];
            $inquiry_sub_category_id = isset($r['inquiry_sub_category_id']) && $r['inquiry_sub_category_id'] ? $r['inquiry_sub_category_id'] : 'N/A';
            $description = isset($r['description']) && $r['description'] ? $r['description'] : 'N/A';
            $status = $r['status'];
            $status_value = Dropdown::get_static('customer_care_status', $r['status'], 'view');
            $created_at = $r['created_at'];
            $updated_at = isset($r['updated_at']) && $r['updated_at'] ? $r['updated_at'] : 'N/A';

            $project_name = $r['project']['name'];
            $property_name = $r['property']['name'];
            $house_model_name = $r['house_model']['name'];
            $buyer = get_person($buyer_id, 'buyers');
            $buyer_name = get_fname($buyer);
            $department_name = isset($r['department']['name']) && $r['department']['name'] ? $r['department']['name'] : 'N/A';
            $inquiry_category = $r['inquiry_category']['name'];
            $inquiry_sub_category = isset($r['inquiry_sub_category']['name']) && $r['inquiry_sub_category']['name'] ? $r['inquiry_sub_category']['name'] : '';     
            
            $overdue = cc_date_diff($r);
            $status_code = cc_status($status, $overdue['status']);
  
            ?>
            <!--begin:: Portlet-->
            <div class="kt-portlet col-sm-12">
                <div class="kt-portlet__body custom-transaction_payments">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden">
                                <img src="./assets/media/project-logos/3.png" alt="image">
                            </div>

                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="<?php echo base_url(); ?>inquiry_tickets/view/<?php echo $id ?>"
                                       class="kt-widget__username text-primary">
                                        Reference : <?php echo $reference; ?>
                                        <i class="flaticon2-correct"></i>
                                    </a>

                                    <div class="kt-widget__action custom_portlet_header">

                                        <div class="kt-portlet__head kt-portlet__head--noborder"
                                             style="min-height: 0px;">
                                            <div class="kt-portlet__head-label">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>"
                                                           class="m-checkable delete_check"
                                                           data-id="<?php echo $r['id']; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                                <?php if ($status<5): ?>
                                                    <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                        <i class="flaticon-more-1 kt-font-brand"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__item">
                                                                <a href="#" data-toggle="modal" data-target="#forwardStatusModal" onclick="setInquiryTicketID(<?php echo $id; ?>,  '<?php echo $reference; ?>', 2 )"
                                                                   class="kt-nav__link">
                                                                    <span class="kt-nav__link-text">Forward</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="work_in_progress"  data-id="<?=$id?>" data-status="3" onclick="setInquiryTicketID(<?php echo $id; ?>,  '<?php echo $reference; ?>', 3 )"
                                                                   class="kt-nav__link">
                                                                    <span class="kt-nav__link-text">Work in progress</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="on_hold" data-id="<?=$id?>" data-status="4" onclick="setInquiryTicketID(<?php echo $id; ?>,  '<?php echo $reference; ?>', 4 )"
                                                                   class="kt-nav__link">
                                                                    <span class="kt-nav__link-text">On hold</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="completed" data-id="<?=$id?>" data-status="5" onclick="setInquiryTicketID(<?php echo $id; ?>,  '<?php echo $reference; ?>', 5 )"
                                                                   class="kt-nav__link">
                                                                    <span class="kt-nav__link-text">Completed</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="javascript:void(0);" data-toggle="modal" data-target="#closeStatusModal" id="closed" data-id="<?=$id?>" data-status="6" onclick="setInquiryTicketID(<?php echo $id; ?>,  '<?php echo $reference; ?>', 6 )"
                                                                   class="kt-nav__link">
                                                                    <span class="kt-nav__link-text">Closed</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="cancel_ticket" data-id="<?=$id?>" data-status="7" onclick="setInquiryTicketID(<?php echo $id; ?>,  '<?php echo $reference; ?>', 7 )"
                                                                   class="kt-nav__link">
                                                                    <span class="kt-nav__link-text  text-danger">Cancel Ticket</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                <?php elseif($status==5): ?>
                                                    <a href="javascript:void(0);" class="btn btn-label-dark btn-lg"  data-toggle="modal" data-target="#closeStatusModal" id="closed" data-id="<?=$id?>" data-status="6" onclick="setInquiryTicketID(<?php echo $id; ?>,  '<?php echo $reference; ?>', 6 )">
                                                                Close Ticket
                                                    </a>
                                                <?php else: ?>
                                                    <a href="<?php echo current_url() . "/form/" . $id; ?>" class="btn btn-label-primary btn-elevate btn-lg">
                                                            Re-open ticket
                                                    </a>
                                                <?php endif?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a href="<?php echo base_url(); ?>project/view/<?php echo $project_id ?>" class="kt-widget__username">
                                                Project : <?php echo ucwords($project_name); ?>
                                                </a>
                                                <br>
                                                <a href="<?php echo base_url(); ?>property/view/<?php echo $property_id ?>" class="kt-widget__username">
                                                    Property : <?=$property_name;?>
                                                </a>
                                                <br>
                                                <span class="kt-widget__username">
                                                    Turn Over Date : <?=$turn_over_date;?>
                                                </spanaspan>
                                            </div>
                                            <div class="col-md-4">
                                                <a href="<?php echo base_url(); ?>house/view/<?php echo $house_model_id ?>" class="kt-widget__username">
                                                    House Model : <?=$house_model_name;?>
                                                </a>
                                                <br>
                                                <a href="<?php echo base_url(); ?>buyer/view/<?php echo $buyer_id ?>" class="kt-widget__username">
                                                    Buyer : <?=$buyer_name;?>
                                                </a>
                                                <br>
                                                <span class="kt-widget__username">
                                                    Warranty : <?=$warranty;?>
                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card text-white <?=$status_code?> mb-3 pr-5" style="max-width: 18rem;">
                                                    <div class="card-body">
                                                        <h5 class="card-title"><?=$status_value?></h5>
                                                        <p class="card-text"><?=$overdue['date_diff']?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-widget__bottom mt-0 pt-0">
                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Inquiry</span>
                                    <span class="kt-widget__value"><?php echo $inquiry_category . " - " . $inquiry_sub_category; ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Department</span>
                                    <span class="kt-widget__value"><?php echo $department_name; ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Date Encoded</span>
                                    <span class="kt-widget__value"><?php echo $created_at; ?></span>
                                </div>
                            </div>
                            
                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Date Updated</span>
                                    <span class="kt-widget__value"><?php echo $updated_at; ?></span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach;?>
    </div>

    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="<?php echo base_url('inquiry_tickets/form'); ?>"
                               class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>
