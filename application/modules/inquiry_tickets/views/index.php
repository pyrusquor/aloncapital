<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Inquiry Ticket
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="total">s</span>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                    <input type="text" class="form-control" placeholder="Search Inquiry Ticket..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"></path>
                                </g>
                            </svg>

                            <!--<i class="flaticon2-search-1"></i>-->
                        </span>
                    </span>
                </div>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <!-- <a href="<?php echo site_url('inquiry_tickets/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Add Inquiry Ticket
            </a>
            <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                <i class="fa fa-filter"></i> Filter
            </button> -->
            <a type="button" class="btn btn-label-primary btn-elevate btn-sm" href="<?= base_url(); ?>complaint_tickets">
                Complaint Tickets
            </a>
            <button type="button" class="btn btn-label-primary btn-elevate btn-sm" id="bulkDelete" disabled>
                <i class="fa fa-trash"></i> Delete Selected
            </button>
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
            <a href="<?php echo site_url('inquiry_tickets/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Add Inquiry Ticket
            </a>
        </div>

        <div class="module__filter">
            <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                <i class="fa fa-filter"></i> Filter
            </button>
        </div>
    </div>
</div>


<!-- CONTENT -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">

        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form" id="advance_search">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">Project Name:</label>
                                <div class="kt-input-icon">
                                    <select type="text" name="project_id" class="form-control form-control-sm _filter suggests" placeholder="Project Name" data-module="projects">
                                        <option value="">Select option</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Property Number:</label>
                                <div class="kt-input-icon">
                                    <select type="text" name="property_id" class="form-control form-control-sm _filter suggests" placeholder="Property Name" data-module="properties">
                                        <option value="">Select option</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <label class="form-control-label">House Model:</label>
                                <div class="kt-input-icon">
                                    <select type="text" name="house_model_id" class="form-control form-control-sm _filter suggests" placeholder="House Model" data-module="house_models">
                                        <option value="">Select option</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">Department:</label>
                                <div class="kt-input-icon">
                                    <select type="text" name="department_id" class="form-control form-control-sm _filter suggests" placeholder="House Model" data-module="departments">
                                        <option value="">Select option</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <label class="form-control-label">Buyer Name:</label>
                                <div class="kt-input-icon">
                                    <select type="text" name="buyer_id" class="form-control form-control-sm _filter suggests" placeholder="Buyer" data-module="buyers" data-type="person">
                                        <option value="">Select option</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Status:</label>
                                <div class="kt-input-icon">
                                    <?php echo form_dropdown('status', Dropdown::get_static('customer_care_status'), '', 'class="form-control _filter"'); ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
            <form class="kt-form" id="export_csv" action="<?php echo site_url('company/export_csv'); ?>" method="POST">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Document Type</label>
                            <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control form-control-sm" name="status" id="import_status">
                                    <option value=""> -- Update Existing Data -- </option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
                            </div>
                            <?php echo form_error('status'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <form class="kt-form" id="upload_form" action="<?php echo site_url('company/import'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Upload CSV file:</label>
                            <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
                            <input type="file" name="file" class="" size="1000" accept="*.csv">
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group form-group-last row custom_import_style">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm disabled" form="upload_form">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                        <div class="col-lg-6 kt-align-right">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm disabled" form="export_csv">
                                <i class="fa fa-file-csv"></i>
                            </button>
                            <button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm disabled" disabled id="btn_upload_guide" data-toggle="modal" data-target="#upload_guide">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="inquiry_tickets_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Reference</th>
                    <th>Project Name</th>
                    <th>Property Number</th>
                    <th>House Model</th>
                    <th>Buyer</th>
                    <th>Turn Over Date</th>
                    <th>Warranty Date</th>
                    <th>Inquiry</th>
                    <th>Department</th>
                    <th>Warranty</th>
                    <th>Date Encoded</th>
                    <th>Date Updated</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Last Update By</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Project:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="projects" id="project_id" name="filterProject">
                                <option value="">Select Project</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Property:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="properties" data-param="project_id" id="property_id" name="filterProperty">
                                <option value="">Select Property</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">House Model:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="house_models" data-param="project_id" id="house_model_id" name="filterHouseModel">
                                <option value="">Select House Model</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Buyer:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="buyers" data-type="person" id="filterBuyer" name="filterBuyer">
                                <option value="">Select Payee</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Turn Over Date:</label>
                            <input type="text" class="form-control kt_transaction_daterangepicker _filter" name="filterTurnOverDate" id="filterTurnOverDate" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Category:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="inquiry_categories" id="inquiry_category_id" name="filterInquiryCategory">
                                <option value="">Select Category</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Sub Category:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="inquiry_sub_categories" data-param="inquiry_category_id" id="filterInquirySubCategory" name="filterInquirySubCategory">
                                <option value="">Select Sub Category</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- FORWARD STATUS MODAL -->
<div class="modal fade" id="forwardStatusModal" tabindex="-1" role="dialog" aria-labelledby="forwardStatusModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="forwardStatusModalLabel">Forward to Department-in-charge</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="forwardStatus">
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <h5 class="statusHeaderNo">Ticket No.: </h5>
                            <div class="form-group">
                                <input type="text" class="form-control" name="inquiry_ticket_id" id="forward_status_id" value="" hidden>
                            </div>
                            <div class="form-group">
                                <label class="">Department-in-charge <span class="kt-font-danger"></span></label>
                                <select class="form-control suggests_modal" data-module="departments" name="department_in_charge" placeholder="Type" autocomplete="off" id="department_in_charge">
                                    <option>Select option</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="forward_status" form="forwardStatus">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- UPDATE STATUS MODAL -->
<div class="modal fade" id="updateStatusModal" tabindex="-1" role="dialog" aria-labelledby="updateStatusModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateStatusModalLabel">Forward to Department-in-charge</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="updateStatus">
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <h5 class="statusHeaderNo">Ticket No.: </h5>
                            <div class="form-group">
                                <input type="text" class="form-control" name="inquiry_ticket_id" id="inquiry_ticket_id" value="" hidden>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="status_id" id="status_id" value="" hidden>
                            </div>
                            <div class="form-group">
                                <label class="">Remarks <span class="kt-font-danger"></span></label>
                                <textarea class="form-control" id="remarks" rows="3" spellcheck="false" name="remarks"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="update_status" form="updateStatus">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- CLOSE STATUS MODAL -->
<div class="modal fade" id="closeStatusModal" tabindex="-1" role="dialog" aria-labelledby="closeStatusModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="closeStatusModalLabel">Close Ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="closeStatus" enctype="multipart/form-data">
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <h5 class="statusHeaderNo">Ticket No.: </h5>
                            <div class="form-group">
                                <input type="text" class="form-control" name="inquiry_ticket_id" id="inquiry_ticket_id" value="" hidden>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="status_id" id="status_id" value="" hidden>
                            </div>
                            <div class="form-group">
                                <label>Upload Acknowledgement Form <span class="kt-font-danger"></span></label>
                                <span class="btn btn-sm">
                                    <input type="file" id="image" name="image" class="" aria-invalid="false" accept="image/*">
                                </span>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="confirmation" name="confirmation">
                                <label for="confirmation">I agree that the inquiry filed is already resolve</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="close_status" form="closeStatus">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>