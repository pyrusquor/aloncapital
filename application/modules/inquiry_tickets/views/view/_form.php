<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
$property_id = isset($info['property_id']) && $info['property_id'] ? $info['property_id'] : '';
$house_model_id = isset($info['house_model_id']) && $info['house_model_id'] ? $info['house_model_id'] : '';
$buyer_id = isset($info['buyer_id']) && $info['buyer_id'] ? $info['buyer_id'] : '';
$department_id = isset($info['department_id']) && $info['department_id'] ? $info['department_id'] : '';
// $turn_over_date = isset($info['turn_over_date']) && $info['turn_over_date'] ? $info['turn_over_date'] : '';
$turn_over_date = isset($info['turn_over_date']) && $info['turn_over_date'] ? date('Y-m-d', strtotime($info['turn_over_date'])) : '';
$warranty_date = isset($info['warranty_date']) && $info['warranty_date'] ? date('Y-m-d', strtotime($info['warranty_date'])) : '';
$warranty = isset($info['warranty']) && $info['warranty'] ? $info['warranty'] : '';
$inquiry_category_id = isset($info['inquiry_category_id']) && $info['inquiry_category_id'] ? $info['inquiry_category_id'] : '';
$inquiry_sub_category_id = isset($info['inquiry_sub_category_id']) && $info['inquiry_sub_category_id'] ? $info['inquiry_sub_category_id'] : '';
$description = isset($info['description']) && $info['description'] ? $info['description'] : '';
$image = isset($info['image']) && $info['image'] ? $info['image'] : '';
$status = isset($info['status']) && $info['status'] ? $info['status'] : '';

$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : '';
$property_name = isset($info['property']['name']) && $info['property']['name'] ? $info['property']['name'] : '';
$house_model_name = isset($info['house_model']['name']) && $info['house_model']['name'] ? $info['house_model']['name'] : '';
$buyer_name = isset($info['buyer']['name']) && $info['buyer']['name'] ? $info['buyer']['name'] : '';
$department_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : '';
$inquiry_category = isset($info['inquiry_category']['name']) && $info['inquiry_category']['name'] ? $info['inquiry_category']['name'] : 'N/A';
$inquiry_sub_category = isset($info['inquiry_sub_category']['name']) && $info['inquiry_sub_category']['name'] ? $info['inquiry_sub_category']['name'] : 'N/A';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Buyer <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="buyers" data-type="person" id="buyer_id" name="buyer_id">
                <option value="">Select Buyer</option>
                <?php if ($buyer_name) : ?>
                    <option value="<?php echo $buyer_id; ?>" selected><?php echo $buyer_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Project <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="projects" data-type='buyer_filter' data-select='project_id' data-param='buyer_id' id="project_id" name="project_id">
                <option value="">Select Project</option>
                <?php if ($project_name) : ?>
                    <option value="<?php echo $project_id; ?>" selected><?php echo $project_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Property <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests suggests-property" data-type='buyer_filter' data-select='property_id' data-module="properties" data-param='buyer_id' id="property_id" name="property_id" data-param="buyer_id" data-param-2="project_id">
                <option value="">Select Project</option>
                <?php if ($property_name) : ?>
                    <option value="<?php echo $property_id; ?>" selected><?php echo $property_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">House Model <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="house_models" id="house_model_id" name="house_model_id" data-param="project_id">
                <option value="">Select House Model</option>
                <?php if ($house_model_name) : ?>
                    <option value="<?php echo $house_model_id; ?>" selected><?php echo $house_model_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Department <span class="kt-font-danger"></span></label>
            <select class="form-control suggests" data-module="departments" id="department_id" name="department_id">
                <option value="">Select Department</option>
                <?php if ($department_name) : ?>
                    <option value="<?php echo $department_id; ?>" selected><?php echo $department_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Turn Over Date <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control kt_datepicker" placeholder="Turn Over Date" name="turn_over_date" id="turn_over_date" value="<?php echo set_value('turn_over_date"', @$turn_over_date); ?>" readonly>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Warranty Date</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Warranty Date" name="warranty_date" id="warranty_date" value="<?php echo set_value('warranty_date"', @$warranty_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Inquiry Category</label>
            <div class="kt-input-icon kt-input-icon--left">
                <select class="form-control" name="inquiry_category_id" placeholder="Type" autocomplete="off" id="inquiry_category_id">
                    <option>Select option</option>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
            </div>
            <?php echo form_error('inquiry_category_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Sub Category</label>
            <div class="kt-input-icon kt-input-icon--left">
                <select class="form-control" name="inquiry_sub_category_id" placeholder="Type" autocomplete="off" id="inquiry_sub_category_id">
                    <option>Select option</option>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
            </div>
            <?php echo form_error('inquiry_sub_category_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Warranty</label>
            <div class="kt-input-icon kt-input-icon--left">
                <select class="form-control" name="warranty" placeholder="Type" autocomplete="off" id="warranty">
                    <option>Select option</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
            </div>
            <?php echo form_error('warranty'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Description <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off" id="description">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Supporting Image</label>
            <div>
                <?php if (get_image('inquiry_ticket', 'images', $image)) : ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('inquiry_ticket', 'images', $image)); ?>" width="90px" height="90px" />
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="image" class="" aria-invalid="false">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>