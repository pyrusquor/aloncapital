<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inquiry_tickets extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $payment_voucher_models = array('Inquiry_ticket_model' => 'M_Inquiry_ticket', 'Inquiry_ticket_log_model' => 'M_Inquiry_ticket_log');

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model($payment_voucher_models);

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper
        $this->load->helper('form');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Inquiry_ticket->fillable;
        $this->_table_columns = $this->M_Inquiry_ticket->__get_columns();
    }

    public function template($page = "")
    {

        $this->template->build($page);
    }

    public function index($debug = 0)
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // // Get record count
        // // $conditions['returnType'] = 'count';
        // $this->view_data['totalRec'] = $totalRec = $this->M_Inquiry_ticket->count_rows();

        // // Pagination configuration
        // $config['target'] = '#inquiry_ticketContent';
        // $config['base_url'] = base_url('inquiry_tickets/paginationData');
        // $config['total_rows'] = $totalRec;
        // $config['per_page'] = $this->perPage;
        // $config['link_func'] = 'InquiryTicketPagination';

        // // Initialize pagination library
        // $this->ajax_pagination->initialize($config);

        // // Get records
        // $this->view_data['records'] = $this->M_Inquiry_ticket
        //     ->with_project()
        //     ->with_property()
        //     ->with_house_model()
        //     ->with_buyer()
        //     ->with_department()
        //     ->with_inquiry_category()
        //     ->with_inquiry_sub_category()
        //     ->order_by('id', 'DESC')
        //     ->limit($this->perPage, 0)
        //     ->get_all();

        // if($debug){
        //     vdebug($this->view_data['records']);
        // }

        // $this->template->build('index', $this->view_data);

        $_fills    =    $this->_table_fillables;
        $_colms    =    $this->_table_columns;
        $_table =    $this->M_Inquiry_ticket->table;

        $this->view_data['_fillables']    =    $this->__get_fillables($_colms, $_fills, $_table); #pd($this->view_data['_fillables']);
        $this->view_data['_columns']        =    $this->__get_columns($_fills); #ud($this->view_data['_columns']);

        // Get record count 
        $this->view_data['totalRec'] = $totalRec = $this->M_Inquiry_ticket->count_rows();
        // $this->view_data['totalRec'] = $totalRec = 0;

        // Pagination configuration 
        $config['target']      = '#inquiryContent';
        $config['base_url']    = base_url('inquiry_tickets/paginationData');
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['link_func']   = 'InquiryPagination';

        // Initialize pagination library 
        $this->ajax_pagination->initialize($config);

        // Get records 
        $this->view_data['records'] = $this->M_Inquiry_ticket->with_project()->with_property()->with_house_model()->with_buyer()->with_department()->as_array()->limit($this->perPage, 0)->get_all();
        // $this->view_data['records'] = [];

        // $this->view_data['records'] = [];

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'project_id' => true,
            'property_id' => true,
            'house_model_id' => true,
            'buyer_id' => true,
            'turn_over_date' => true,
            'warranty_date' => true,
            'inquiry_category_id' => true,
            'department_id' => true,
            'warranty' => true,
            'created_at' => true,
            'updated_at' => true,
            'status' => true,
            'created_by' => true,
            'updated_by' => true,
            'status_id' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Inquiry_ticket
                ->with_project('fields:name')
                ->with_property('fields:name')
                ->with_house_model('fields:name')
                ->with_buyer('fields:first_name,middle_name,last_name')
                ->with_department('fields:name')
                ->with_inquiry_category('fields:name')
                // ->with_inquiry_sub_category('fields:name')
                // ->with_inquiry_inquiry_logs()
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("reference like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'house_model_id' => [
                    'data' => $filters['house_model_id'] ?? null,
                    'operator' => '=',
                ],
                'department_id' => [
                    'data' => $filters['department_id'] ?? null,
                    'operator' => '=',
                ],
                'buyer_id' => [
                    'data' => $filters['buyer_id'] ?? null,
                    'operator' => '=',
                ],
                'status' => [
                    'data' => $filters['status'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $transaction = "<a target='_BLANK'  target='_BLANK' href='/transaction/view/" . $value['transaction_id'] . "'>" . get_value_field($value['transaction_id'], 'transactions', 'reference') . "</a>";

                        $items[$key]['reference'] = isset($value['reference']) ? "<a href='/inquiry_tickets/view/" . $value['id'] . "'>" . $value['reference'] . "</a> <br>" . $transaction : 'N/A';


                        $items[$key]['project_id'] = isset($value['project']) ? "<a href='/project/view/" . $value['project']['id'] . "'>" . $value['project']['name'] . "</a>" : 'N/A';
                        $items[$key]['property_id'] = isset($value['property']) ? "<a href='/property/view/" . $value['property']['id'] . "'>" . $value['property']['name'] . "</a>" : 'N/A';
                        $items[$key]['buyer_id'] = isset($value['buyer']) ? "<a href='/buyer/view/" . $value['buyer']['id'] . "'>" . get_fname($value['buyer']) . "</a>" : 'N/A';
                        $items[$key]['house_model_id'] = isset($value['house_model']) ? "<a href='/house/view/" . $value['house_model']['id'] . "'>" . $value['house_model']['name'] . "</a>" : 'N/A';
                        $items[$key]['department_id'] = isset($value['department']) ? "<a href='/department/view/" . $value['department']['id'] . "'>" . $value['department']['name'] . "</a>" : 'N/A';
                        $items[$key]['inquiry_category_id'] = isset($value['inquiry_category']) ? "<a href='/inquiry_categories/view/" . $value['inquiry_category']['id'] . "'>" . $value['inquiry_category']['name'] . "</a>" : 'N/A';

                        $items[$key]['turn_over_date'] = view_date($value['turn_over_date']);
                        $items[$key]['warranty_date'] = $value['warranty_date'] ? view_date($value['warranty_date']) : "";
                        $items[$key]['created_at'] = view_date($value['created_at']);
                        $items[$key]['updated_at'] = view_date($value['updated_at']);

                        $status = $value['status'];
                        $items[$key]['status_id'] = $status;
                        $status_value = Dropdown::get_static('customer_care_status', $value['status'], 'view');

                        $overdue = cc_date_diff($value);
                        $status_code = cc_status($status, $overdue['status']);

                        $a  = '<div class="card text-white ' . $status_code . ' mb-3 pr-5" style="max-width: 18rem;"><div class="card-body"><h5 class="card-title">' . $status_value . '</h5><p class="card-text">' . $overdue['date_diff'] . '</p></div></div>';

                        $items[$key]['status'] = $a;

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Inquiry_ticket->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            $project_id = $this->input->post('project_id');
            $property_id = $this->input->post('property_id');
            $house_model_id = $this->input->post('house_model_id');
            $buyer_id = $this->input->post('buyer_id');
            $department_id = $this->input->post('department_id');
            $turn_over_date = $this->input->post('turn_over_date');
            $inquiry_category_id = $this->input->post('inquiry_category_id');
            $inquiry_sub_category_id = $this->input->post('inquiry_sub_category_id');
            $page = $this->input->post('page');

            if (!empty($turn_over_date)) {
                $date_range = explode('-', $turn_over_date);

                $from_str   = strtotime($date_range[0]);
                $from       = date('Y-m-d H:i:s', $from_str);

                $to_str     = strtotime($date_range[1] . '23:59:59');
                $to         = date('Y-m-d H:i:s', $to_str);
            }

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_Inquiry_ticket->count_rows();
            //            $where = array();

            // Pagination configuration
            $config['target'] = '#inquiry_ticketContent';
            $config['base_url'] = base_url('inquiry_tickets/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'InquiryTicketPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($house_model_id)) :
                $this->db->group_start();
                $this->db->where('house_model_id', $house_model_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_id)) :
                $this->db->group_start();
                $this->db->where('buyer_id', $buyer_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($department_id)) :
                $this->db->group_start();
                $this->db->where('department_id', $department_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($turn_over_date)) :
                $this->db->group_start();
                $this->db->where('turn_over_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            if (!empty($inquiry_category_id)) :
                $this->db->group_start();
                $this->db->where('inquiry_category_id', $inquiry_category_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($inquiry_sub_category_id)) :
                $this->db->group_start();
                $this->db->where('inquiry_sub_category_id', $inquiry_sub_category_id, 'both');
                $this->db->group_end();
            endif;

            $totalRec = $this->M_Inquiry_ticket->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($house_model_id)) :
                $this->db->group_start();
                $this->db->where('house_model_id', $house_model_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_id)) :
                $this->db->group_start();
                $this->db->where('buyer_id', $buyer_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($department_id)) :
                $this->db->group_start();
                $this->db->where('department_id', $department_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($turn_over_date)) :
                $this->db->group_start();
                $this->db->where('turn_over_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            if (!empty($inquiry_category_id)) :
                $this->db->group_start();
                $this->db->where('inquiry_category_id', $inquiry_category_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($inquiry_sub_category_id)) :
                $this->db->group_start();
                $this->db->where('inquiry_sub_category_id', $inquiry_sub_category_id, 'both');
                $this->db->group_end();
            endif;

            $this->view_data['records'] = $records = $this->M_Inquiry_ticket
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_inquiry_category()
                ->with_inquiry_sub_category()
                ->order_by('id', 'DESC')
                ->limit($this->perPage, $offset)
                ->get_all();

            $this->load->view('inquiry_tickets/_filter', $this->view_data, false);
        }
    }

    public function view($id = false, $type = '')
    {

        if ($id) {

            $this->view_data['info'] = $info = $this->M_Inquiry_ticket
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_inquiry_category()
                ->with_inquiry_sub_category()
                ->with_inquiry_logs()
                ->get($id);

            if ($this->view_data['info']) {

                if ($type == "debug") {

                    vdebug($this->view_data);
                }

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function form($id = false, $debug = 0)
    {

        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Inquiry_ticket->fields);

            if ($this->form_validation->run() === true) {

                $oof = $this->input->post();
                //get the status of the current inquiry ticket
                $status = $this->M_Inquiry_ticket->get($id)['status'];

                //if the inquiry ticket is closed, cancelled or completed, the method will create a new ticket with the data from the old ticket
                if ($id && $status < 5) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $inquiry_ticket_id = $this->M_Inquiry_ticket->update($oof + $additional, $id);
                } else {


                    $w['project_id'] = $oof['project_id'];
                    $w['property_id'] = $oof['property_id'];
                    $w['buyer_id'] = $oof['buyer_id'];

                    $oof['transaction_id'] = get_value_field_multiple($w, 'transactions', 'id');

                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                        'reference' => uniqidIT(),
                        'status' => 1,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    $inquiry_ticket_id = $this->M_Inquiry_ticket->insert($oof + $additional);

                    $this->db->trans_complete(); # Completing inquiry_tickets
                }

                /*Optional*/
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $response['status'] = 1;
                    $response['message'] = 'Inquiry ticket Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;

        if ($id) {

            $this->view_data['info'] = $this->M_Inquiry_ticket
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_inquiry_category()
                ->with_inquiry_sub_category()
                ->get($id);
        }

        if ($debug) {
            vdebug($this->view_data);
        }
        $this->template->build('form', $this->view_data);
    }

    public function generate_transactions()
    {
        // code...
        $rows = $this->M_Inquiry_ticket->get_all();

        if ($rows) {
            foreach ($rows as $key => $row) {

                $id = $row['id'];

                $w['project_id'] = $row['project_id'];
                $w['property_id'] = $row['property_id'];
                $w['buyer_id'] = $row['buyer_id'];

                $oof['transaction_id'] = get_value_field_multiple($w, 'transactions', 'id');

                $this->M_Inquiry_ticket->update($oof, $id);
            }
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Inquiry_ticket->get($id);
            if ($list) {

                $deleted = $this->M_Inquiry_ticket->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Inquiry ticket successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Inquiry_ticket->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Inquiry ticket successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            // $this->view_data['info'] = $info = $this->M_Inquiry_ticket->with_requests()->with_payable_items()->with_entry_items()->with_item()->as_array()->get($id);

            $this->view_data['info'] = $info = $this->M_Inquiry_ticket
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_inquiry_category()
                ->with_inquiry_sub_category()
                ->get($id);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }

    public function forward_status()
    {
        $inquiry_ticket_id = $this->input->post('inquiry_ticket_id');
        $status_id = $this->input->post('status_id');
        $department_id = $this->input->post('department_id');
        $remarks = "Fowarded to department-in-charge";

        if ($inquiry_ticket_id) {
            $info = [
                'inquiry_id' => $inquiry_ticket_id,
                'department_id' => $department_id,
                'remarks' => $remarks,
                'status' => $status_id
            ];

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            $update = [
                'department_id' => $department_id,
                'status' => $status_id,
                'updated_by' => $this->user->id,
                'updated_at' => NOW,
            ];

            // Insert inquiry ticket log
            $inquiry_ticket_log_id = $this->M_Inquiry_ticket_log->insert($info + $additional);
            $inquiry_ticket = $this->M_Inquiry_ticket->update($update, $inquiry_ticket_id);

            if ($inquiry_ticket_log_id) {
                $response['status'] = 1;
                $response['message'] = $remarks;
            } else {
                $response['status'] = 0;
                $response['message'] = 'Error! 1';
            }
        } else {
            $response['status'] = 0;
            $response['message'] = 'Error! 2';
        }

        echo json_encode($response);
        exit();
    }

    public function update_status()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';
        $inquiry_ticket_id = $this->input->post('inquiry_ticket_id');
        $status = $this->input->post('status_id');
        $remarks = $this->input->post('remarks');
        $image = $this->input->post('image');

        if ($inquiry_ticket_id) {
            $info = [
                'inquiry_id' => $inquiry_ticket_id,
                'remarks' => $remarks,
                'status' => $status
            ];

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            if ($status == 6) {
                // Image Upload
                $upload_path = './assets/uploads/inquiry_tickets/images';
                $config = array();
                $config['upload_path'] = $upload_path;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = 5000;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config, 'inquiry_ticket_image');
                $this->inquiry_ticket_image->initialize($config);

                if (!empty($_FILES['image']['name'])) {
                    if (!$this->inquiry_ticket_image->do_upload('image')) {
                        $this->notify->error($this->inquiry_ticket_image->display_errors(), 'inquiry_tickets/form');
                    } else {
                        $img_data = $this->inquiry_ticket_image->data();
                    }
                }

                $update = [
                    'status' => $status,
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                    'image' => @$img_data
                ];
            } else {
                $update = [
                    'status' => $status,
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];
            }


            // Insert inquiry ticket log
            $inquiry_ticket_log_id = $this->M_Inquiry_ticket_log->insert($info + $additional);
            $inquiry_ticket = $this->M_Inquiry_ticket->update($update, $inquiry_ticket_id);

            if ($inquiry_ticket_log_id) {
                $response['status'] = 1;
                $response['message'] = $remarks;
            } else {
                $response['status'] = 0;
                $response['message'] = 'Error!';
            }
        } else {
            $response['status'] = 0;
            $response['message'] = 'Error!';
        }

        echo json_encode($response);
        exit();
    }
}
