<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inquiry_ticket_log_model extends MY_Model
{
    public $table = 'inquiry_ticket_logs'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'inquiry_id',
        'department_id',
        'remarks',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        /* ==================== begin: Add model fields ==================== */
        'inquiry_id' => array(
            'field' => 'inquiry_id',
            'label' => 'Inquiry',
            'rules' => 'trim|required'
        ),
        'remarks' => array(
            'field' => 'remarks',
            'label' => 'Remarks',
            'rules' => 'trim|required'
        ),
        'status' => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['project'] = array('foreign_model' => 'inquiry_ticket_model', 'foreign_table' => 'inquiry_tickets', 'foreign_key' => 'id', 'local_key' => 'inquiry_ticket_id');
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
