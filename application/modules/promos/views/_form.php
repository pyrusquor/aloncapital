<?php
    $name            =    isset($promo['name']) && $promo['name'] ? $promo['name'] : '';
    $description        =    isset($promo['description']) && $promo['description'] ? $promo['description'] : '';
    $value            =    isset($promo['value']) && $promo['value'] ? $promo['value'] : '';
    $value_type    =    isset($promo['value_type']) && $promo['value_type'] ? $promo['value_type'] : '';
    $deduct_to            =    isset($promo['deduct_to']) && $promo['deduct_to'] ? $promo['deduct_to'] : '';
    $promo_type    =    isset($promo['promo_type']) && $promo['promo_type'] ? $promo['promo_type'] : '';
?>


<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-edit"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Description</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-align-left"></i></span>
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Value<span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="value" value="<?php echo set_value('value', $value); ?>" placeholder="Value" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span>
            </div>
            <?php echo form_error('value'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Value Type</label>
            <div class="kt-input-icon kt-input-icon--left">
                <?php echo form_dropdown('value_type', Dropdown::get_static('value_type'), set_value('value_type', $value_type), 'class="form-control" id="value_type"'); ?>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-flag"></i></span>
            </div>
            <?php echo form_error('value_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Deduct From</label>
            <div class="kt-input-icon kt-input-icon--left">
                <?php echo form_dropdown('deduct_to', Dropdown::get_static('deduct_to'), set_value('deduct_to', $deduct_to), 'class="form-control" id="deduct_to"'); ?>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-minus-square-o"></i></span>
            </div>
            <?php echo form_error('deduct_to'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Promos Type</label>
            <div class="kt-input-icon kt-input-icon--left">
                <?php echo form_dropdown('promo_type', Dropdown::get_static('fees_type'), set_value('promo_type', $promo_type), 'class="form-control" id="promo_type"'); ?>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-leaf"></i></span>
            </div>
            <?php echo form_error('promo_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
