<?php
    $id            =    isset($promo['id']) && $promo['id'] ? $promo['id'] : '';
    $name            =    isset($promo['name']) && $promo['name'] ? $promo['name'] : '';
    $description        =    isset($promo['description']) && $promo['description'] ? $promo['description'] : '';
    $value            =    isset($promo['value']) && $promo['value'] ? $promo['value'] : '';
    $value_type    =    isset($promo['value_type']) && $promo['value_type'] ? Dropdown::get_static('value_type',$promo['value_type']) : '';
    $deduct_to            =    isset($promo['deduct_to']) && $promo['deduct_to'] ? Dropdown::get_static('deduct_to',$promo['deduct_to']) : '';
    $promo_type    =    isset($promo['promo_type']) && $promo['promo_type'] ? Dropdown::get_static('fees_type',$promo['promo_type']) : '';
    $created_at    =    isset($promo['created_at']) && $promo['created_at'] ? nice_date($promo['created_at'],'F j, Y') : '';
    $updated_at    =    isset($promo['updated_at']) && $promo['updated_at'] ? nice_date($promo['updated_at'],'F j, Y') : '';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">View Promo</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('promos/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<a href="<?php echo site_url('promos');?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-6">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							General Information
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin::Form-->
					<div class="kt-widget4">
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Name
							</span>
							<span class="kt-widget4__text kt-widget4__text--bold">
								<?php echo $name; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc kt-align-right">
								Description
							</span>
							<span class="kt-widget4__text">
								<?php echo $description; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Value
							</span>
							<span class="kt-widget4__text kt-widget4__text--bold">
								<?php echo $value; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Value Type
							</span>
							<span class="kt-widget4__text">
								<?php echo $value_type; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Deduct To:
							</span>
							<span class="kt-widget4__text">
								<?php echo $deduct_to; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Promo Type:
							</span>
							<span class="kt-widget4__text">
								<?php echo $promo_type; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Date Created:
							</span>
							<span class="kt-widget4__text">
								<?php echo $created_at; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Date Updated:
							</span>
							<span class="kt-widget4__text">
								<?php echo $updated_at; ?>
							</span>
						</div>
					</div>

					<!--end::Form-->
				</div>
			</div>
			<!--end::Portlet-->

		</div>
	</div>
</div>
<!-- begin:: Footer -->