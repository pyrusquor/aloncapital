<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Letter_of_request_model extends MY_Model {

	public $table = 'letter_of_request'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
        'reference',
		'transaction_id',
        'buyer_id',
        'request_type_id',
		'status',
		'assignatory_id',
		'remarks',
		'file_id',
		'requested_at',
		'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [
		'reference' => array(
            'field' => 'reference',
            'label' => 'Reference',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */
        'transaction_id' => array(
            'field' => 'transaction_id',
            'label' => 'Transaction ID',
            'rules' => 'trim'
        ),
        'buyer_id' => array(
            'field' => 'buyer_id',
            'label' => 'Buyer',
            'rules' => 'trim|required'
        ),
		'request_type_id' => array(
            'field' => 'request_type_id',
            'label' => 'Buyer',
            'rules' => 'trim|required'
        ),
		'status' => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim|required'
        ),
		'assignatory_id' => array(
            'field' => 'assignatory_id',
            'label' => 'Assignatory',
            'rules' => 'trim|required'
        ),
		'remarks' => array(
            'field' => 'remarks',
            'label' => 'Remarks',
            'rules' => 'trim|required'
        ),
		'file_id' => array(
            'field' => 'file_id',
            'label' => 'File',
            'rules' => 'trim|required'
        ),
        'file_id' => array(
            'field' => 'requested_at',
            'label' => 'Requested',
            'rules' => 'trim|required'
        ),
       
        /* ==================== end: Add model fields ==================== */
	];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['tax'] = array('foreign_model' => 'taxes/Taxes_model', 'foreign_table' => 'taxes', 'foreign_key' => 'id', 'local_key' => 'tax_id');
        $this->has_one['abc'] = array('foreign_model' => 'item_abc/item_abc_id', 'foreign_table' => 'item_abc', 'foreign_key' => 'id', 'local_key' => 'item_abc_id');
        $this->has_one['brand'] = array('foreign_model' => 'item_brand/item_brand_model', 'foreign_table' => 'item_brand', 'foreign_key' => 'id', 'local_key' => 'item_brand_id');
        $this->has_one['class'] = array('foreign_model' => 'item_class/item_class_model', 'foreign_table' => 'item_class', 'foreign_key' => 'id', 'local_key' => 'item_class_id');
        $this->has_one['group'] = array('foreign_model' => 'item_group/item_group_model', 'foreign_table' => 'item_group', 'foreign_key' => 'id', 'local_key' => 'item_group_id');
        $this->has_one['type'] = array('foreign_model' => 'item_type/item_type_model', 'foreign_table' => 'item_type', 'foreign_key' => 'id', 'local_key' => 'item_type_id');

	}
	
	public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}