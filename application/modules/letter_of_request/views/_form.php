<?php
    $name = isset($info['name']) && $info['name'] ? $info['name'] : '';

    $code = isset($info['code']) && $info['code'] ? $info['code'] : '';

    $is_active = isset($info['is_active']) && $info['is_active'] ? $info['is_active'] : '0';
?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Reference <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="name" value="<?php echo set_value('name', @$name); ?>" placeholder="Reference..." required>

            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Buyer <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="code" value="<?php echo set_value('code', @$code); ?>" placeholder="Code">

            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Request Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
            <?php echo form_dropdown('is_active', Dropdown::get_static('letter_requests'), set_value('is_active', @$is_active), 'class="form-control" required'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Request Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
            <?php echo form_dropdown('is_active', Dropdown::get_static('general_status'), set_value('is_active', @$is_active), 'class="form-control" required'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label class="">File <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="code" value="<?php echo set_value('code', @$code); ?>" placeholder="Code">

            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Requested At <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="code" value="<?php echo set_value('code', @$code); ?>" placeholder="Code">

            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label class="">Remarks <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="code" value="<?php echo set_value('code', @$code); ?>" placeholder="Remarks...">

            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <!-- <div class="col-sm-4">
        <div class="form-group">
            <label>Status <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('is_active', Dropdown::get_static('bank_status'), set_value('is_active', @$is_active), 'class="form-control" required'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div> -->
</div>