<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$code = isset($info['code']) && $info['code'] ? $info['code'] : '';
$complaint_category_id = isset($info['complaint_category_id']) && $info['complaint_category_id'] ? $info['complaint_category_id'] : '';
$description = isset($info['description']) && $info['description'] ? $info['description'] : '';

$complaint_category = isset($info['complaint_category']['name']) && $info['complaint_category']['name'] ? $info['complaint_category']['name'] : '';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="name"
                   value="<?php echo set_value('name', $name); ?>" placeholder="Name"
                   autocomplete="off" id="name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Code <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="code"
                   value="<?php echo set_value('code', $code); ?>" placeholder="Code"
                   autocomplete="off" id="code">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Description <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="description"
                   value="<?php echo set_value('description', $description); ?>" placeholder="Description"
                   autocomplete="off" id="description">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Complaint Category <span class="kt-font-danger">*</span></label>
            <select class="form-control" name="complaint_category_id"
                    placeholder="Type"
                    autocomplete="off" id="complaint_category_id" required>
                <option value="">Select option</option>
                <?php if ($complaint_category): ?>
                    <option value="<?php echo $complaint_category_id; ?>" selected><?php echo $complaint_category; ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>