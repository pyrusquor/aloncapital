<?php

?>

<div class="row">

    <div class="col-lg-4">
        <div class="form-group">
            <label>Job Request <span class="kt-font-danger">*</span></label>
            <select class="form-control kt-select2" id="job_request_id" name="job_request_id">
                <?php foreach ($job_requests as $p): ?>
                        <option value="<?php echo $p['id'] ?>"><?php echo ucwords($p['description']); ?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Job Order No. <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="number" value="<?php echo $number ?  $number : $job_order_number ?>" placeholder="Job Order No." readonly>
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Mode of Payment <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('payment_type', Dropdown::get_static('payment_types'), set_value('$payment_type', @$payment_type), 'class="form-control" id="payment_type"'); ?>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Date <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control kt_datepicker" name="job_order_date" value="<?php echo set_value('job_order_date', $job_order_date); ?>">
            <?php echo form_error('job_order_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Prepared By <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
            <select class="form-control suggests" data-module="staff"  data-type="person" id="staff_id" name="staff_id" required>
                <option value="">Select Staff</option>
                <?php if ($staff_name): ?>
                    <option value="<?php echo $staff_id; ?>" selected><?php echo $staff_name; ?></option>
                <?php endif?>
            </select>
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('staff_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Priority Level <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('priority_level', Dropdown::get_static('job_order_priority_level'), set_value('priority_level', @$priority_level), 'class="form-control" id="priority_level"'); ?>
            <?php echo form_error('priority_level'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Approved By <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
            <select class="form-control suggests" data-module="staff"  data-type="person" id="approved_by" name="approved_by" required>
                <option value="">Select Approver</option>
                <?php if ($staff_name): ?>
                    <option value="<?php echo $approved_by; ?>" selected><?php echo $staff_name; ?></option>
                <?php endif?>
            </select>
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('approved_by'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

</div>