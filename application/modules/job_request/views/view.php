<?php
    // vdebug($job_request);
    
    $number = isset($job_request['number']) && $job_request['number'] ? $job_request['number'] : '';
    $company = isset($job_request['company']) && $job_request['company'] ? $job_request['company']['name'] : '';
    $project = isset($job_request['project']) && $job_request['project'] ? $job_request['project']['name'] : '';
    $description = isset($job_request['description']) && $job_request['description'] ? $job_request['description'] : '';
    $staff = isset($job_request['staff']) && $job_request['staff'] ? $job_request['staff']['first_name'] . " " . $job_request['staff']['last_name'] : '';
    $status = isset($job_request['status']) && $job_request['status'] ? $job_request['status'] : '';
    $date_requested = isset($job_request['created_at']) && $job_request['created_at'] ? view_date($job_request['created_at']) : '';
    
    $job_order_number = isset($job_request['job_order']['number']) && $job_request['job_order']['number'] ? $job_request['job_order']['number'] : '';
    $job_order_payment_type = isset($job_request['job_order']['payment_type']) && $job_request['job_order']['payment_type'] ? $job_request['job_order']['payment_type'] : '';
    $job_order_date = isset($job_request['job_order']['job_order_date']) && $job_request['job_order']['job_order_date'] ? $job_request['job_order']['job_order_date'] : '';
    $job_order_prepared_by = isset($job_request['job_order']['staff_id']) && $job_request['job_order']['staff_id'] ? $job_request['job_order']['staff_id'] : '';
    $job_order_priority = isset($job_request['job_order']['priority_level']) && $job_request['job_order']['priority_level'] ? $job_request['job_order']['priority_level'] : '';
    $job_order_approved_by = isset($job_request['job_order']['approved_by']) && $job_request['job_order']['approved_by'] ? $job_request['job_order']['approved_by'] : '';
    
    ?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Job Request</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('job_request'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Job Request
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Job Request No.:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $number; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Sub-Project:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Justification:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        SBU:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Sub-Amenity:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Company:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $company; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Amenity:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Approved By:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Project:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $project; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Request Description:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $description; ?></p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Project Category:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Equipment:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Date Requested:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $date_requested; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Property:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Requested By:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $staff; ?></p>
                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Job Order
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Job Order No.:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $job_order_number; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Mode of Payment:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $job_order_payment_type ?></p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Date:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo view_date($job_order_date) ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Prepared By:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $job_order_prepared_by ?></p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Priority Level
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $job_order_priority ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Approved By
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $job_order_approved_by; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-lg-3">
                                <p class="font-weight-bold">
                                    Scope of Work
                                </p>
                                <p class="font-weight-bold">
                                    Schematic Design for Cassandra
                                </p>
                            </div>
                            <div class="col-lg-3">
                                <p class="font-weight-bold">
                                    Entry Items
                                </p>
                                <p class="font-weight-bold">
                                    Professional Fee
                                </p>
                                <p class="font-weight-bold">
                                    Accounts Payable - Repairs
                                </p>
                            </div>
                            <div class="col-lg-3">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Entry Type
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">Amount</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Debit
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">2000.00</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Credit
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">2000.00</p>
                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->