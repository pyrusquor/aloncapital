<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Job_request_model extends MY_Model
{
    public $table = 'job_requests'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'number', 
        'company_id',
        'project_id',
        'description',
        'staff_id',
        'status',
        'request_type',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['staff'] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'staff_id');

        $this->has_one['company'] = array('foreign_model' => 'company/Company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');

        $this->has_one['project'] = array('foreign_model' => 'project/Project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');


        $this->has_one['job_order'] = array('foreign_model' => 'job_order/Job_order_model', 'foreign_table' => 'job_orders', 'foreign_key' => 'job_request_id', 'local_key' => 'id');

    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
