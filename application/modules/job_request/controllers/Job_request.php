<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Job_request extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$job_request_models = array(
			'job_request/Job_request_model' => 'M_job_request',
			'job_order/Job_order_model' => 'M_job_order',
		);

		// Load models
		$this->load->model('project/Project_model', 'M_project');
		$this->load->model($job_request_models);


		// Load pagination library
		$this->load->library('ajax_pagination');

		// Format Helper
		$this->load->helper(['format', 'images']); // Load Helper
		$this->load->helper('form');

		// Per page limit
		$this->perPage = 12;

		$this->_table_fillables = $this->M_job_request->fillable;
		$this->_table_columns = $this->M_job_request->__get_columns();

		$this->u_additional = [
			'updated_by' => $this->user->id,
			'updated_at' => NOW,
		];

		$this->additional = [
			'is_active' => 1,
			'created_by' => $this->user->id,
			'created_at' => NOW,
		];

		$this->load->helper('format');
	}

	public function index()
	{
		$_fills = $this->_table_fillables;
		$_colms = $this->_table_columns;

		$this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns'] = $this->__get_columns($_fills);

		$db_columns = $this->M_job_request->get_columns();
		if ($db_columns) {
			$column = [];
			foreach ($db_columns as $key => $dbclm) {
				$name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

				if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
					if (strpos($name, '_id') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('_id', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} elseif (strpos($name, 'is_') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('is_', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} else {
						$column[$key]['value'] = $name;
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					}
				} else {
					continue;
				}
			}

			$column_count = count($column);
			$cceil = ceil(($column_count / 2));

			$this->view_data['columns'] = array_chunk($column, $cceil);
			$column_group = $this->M_job_request->count_rows();
			if ($column_group) {
				$this->view_data['total'] = $column_group;
			}
		}

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function showJobRequests()
	{
		$output = ['data' => ''];

		$columnsDefault = [
			'id' => true,
			'number' => true,
			'company_id' => true,
			'project_id' => true,
			'description' => true,
			'staff_id' => true,
			'status' => true,
			'created_at' => true,
			'request_type' => true,
			'company' => true,
			'project' => true,
			'staff' => true,
			'status_id' => true,
			'created_by' => true,
			'updated_by' => true
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}

		// get all raw data
		$job_requests = $this->M_job_request->with_company()->with_project()->with_staff()->as_array()->get_all();
		$data = [];

		if ($job_requests) {

			foreach ($job_requests as $key => $value) {
				$job_requests[$key]['staff_id'] = @$value['staff']['first_name'] . " " .  @$value['staff']['last_name'];
				$job_requests[$key]['staff'] = @$value['staff']['id'];
				$job_requests[$key]['company_id'] = @$value['company']['name'];
				$job_requests[$key]['company'] = @$value['company']['id'];
				$job_requests[$key]['project_id'] = @$value['project']['name'];
				$job_requests[$key]['project'] = @$value['project']['id'];
				$job_requests[$key]['created_at'] = view_date($value['created_at']);
				$job_requests[$key]['status_id'] = $value['status'];
				$job_requests[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';
				$job_requests[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
			}

			foreach ($job_requests as $d) {
				$data[] = $this->filterArray($d, $columnsDefault);
			}

			// count data
			$totalRecords = $totalDisplay = count($data);

			// filter by general search keyword
			if (isset($_REQUEST['search'])) {
				$data = $this->filterKeyword($data, $_REQUEST['search']);
				$totalDisplay = count($data);
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
				foreach ($_REQUEST['columns'] as $column) {
					if (isset($column['search'])) {
						$data = $this->filterKeyword($data, $column['search'], $column['data']);
						$totalDisplay = count($data);
					}
				}
			}

			// sort
			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

				$_column = $_REQUEST['order'][0]['column'] - 1;
				$_dir = $_REQUEST['order'][0]['dir'];

				usort($data, function ($x, $y) use ($_column, $_dir) {

					// echo "<pre>";
					// print_r($x) ;echo "<br>";
					// echo $_column;echo "<br>";

					$x = array_slice($x, $_column, 1);

					// vdebug($x);

					$x = array_pop($x);

					$y = array_slice($y, $_column, 1);
					$y = array_pop($y);

					if ($_dir === 'asc') {

						return $x > $y ? true : false;
					} else {

						return $x < $y ? true : false;
					}
				});
			}

			// pagination length
			if (isset($_REQUEST['length'])) {
				$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
			}

			// return array values only without the keys
			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
				$tmp = $data;
				$data = [];
				foreach ($tmp as $d) {
					$data[] = array_values($d);
				}
			}
			$secho = 0;
			if (isset($_REQUEST['sEcho'])) {
				$secho = intval($_REQUEST['sEcho']);
			}

			$output = array(
				'sEcho' => $secho,
				'sColumns' => '',
				'iTotalRecords' => $totalRecords,
				'iTotalDisplayRecords' => $totalDisplay,
				'data' => $data,
			);
		}

		echo json_encode($output);
		exit();
	}

	public function form($id = false)
	{

		$method = "Create";
		if ($id) {
			$method = "Update";
		}

		if ($this->input->post()) {

			$response['status'] = 0;
			$response['msg'] = 'Oops! Please refresh the page and try again.';

			$info = $this->input->post();

			$data['number'] = $info['number'];
			$data['project_id'] = $info['project_id'];
			$data['company_id'] = $info['company_id'];
			$data['description'] = $info['request_description'];
			$data['staff_id'] = $info['staff_id'];
			$data['status'] = 1;
			$data['request_type'] = $info['request_type'];

			if ($id) {

				$result = $this->M_job_request->update($data, $id);
			} else {

				$result = $this->M_job_request->insert($data);
			}


			if ($result === false) {

				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Job Request successfully' . " " . $method, 'job_request');
			}
		}

		$this->view_data['project'] = $this->M_project->as_array()->get_all();

		if ($id) {
			$this->view_data['info'] = $info = $this->M_job_request->with_company()->with_project()->with_staff()->get($id);
		}

		$this->view_data['method'] = $method;
		$this->view_data['job_request_number'] = uniqidJR();

		$this->template->build('form', $this->view_data);
	}

	public function view($id = false)
	{
		if ($id) {
			$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
			$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

			$this->view_data['job_request'] = $this->M_job_request->with_company()->with_project()->with_staff()->with_job_order()->get($id);

			if ($this->view_data['job_request']) {

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {
			show_404();
		}
	}

	public function delete()
	{
		$response['status'] = 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id = $this->input->post('id');
		if ($id) {

			$list = $this->M_job_request->get($id);
			if ($list) {

				$deleted = $this->M_job_request->delete($list['id']);
				if ($deleted !== false) {

					$response['status'] = 1;
					$response['message'] = 'Job Request Successfully Deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function bulkDelete()
	{
		$response['status'] = 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');
			vdebug($delete_ids);

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_job_request->delete($value);
				}
				if ($deleted !== false) {

					$response['status'] = 1;
					$response['message'] = 'Job Request/s Successfully Deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function add_job_order($id = 0)
	{

		// Check if theres an existing job order for this job request
		$has_job_order = $this->M_job_order->where("job_request_id", $id)->get();

		if (!$has_job_order) {

			if ($this->input->post()) {

				$response['status'] = 0;
				$response['msg'] = 'Oops! Please refresh the page and try again.';

				$info = $this->input->post();

				$data['number'] = $info['number'];
				$data['payment_type'] = $info['payment_type'];
				$data['job_order_date'] = $info['job_order_date'];
				$data['staff_id'] = $info['staff_id'];
				$data['priority_level'] = $info['priority_level'];
				$data['approved_by'] = $info['approved_by'];
				$data['job_request_id'] = $info['job_request_id'];

				$result = $this->M_job_order->insert($data);

				if ($result === false) {

					// Validation
					$this->notify->error('Oops something went wrong.');
				} else {

					// Success
					$this->notify->success('Job Order successfully added', 'job_order');
				}
			}

			if ($id) {

				$this->view_data['job_requests'] = $this->M_job_request->get_all();
				$this->view_data['job_order_number'] = uniqidJO();

				$this->template->build('add_job_order_form', $this->view_data);
			} else {

				show_404();
			}
		} else {
			$this->notify->error('This Job Request has an existing Job Order.');
		}
	}

	public function update_status()
	{

		$response['status'] = 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$status_names = array(
			"2" => "Approved",
			"3" => "Disapproved",
			"4" => "Cancelled",
		);

		$id = $this->input->post('id');
		$status = $this->input->post('status');
		if ($id) {

			$job_request = $this->M_job_request->get($id);

			$current_status = $job_request['status'];


			if ($job_request) {

				if ($current_status == $status) {

					$response['status'] = 0;
					$response['message'] = 'Job Request is already ' . $status_names[$status];
				} else {
					$data['status'] = $status;
					$results = $this->M_job_request->update($data, $id);

					if ($results !== false) {

						$response['status'] = 1;
						$response['message'] = 'Job Request is now ' . $status_names[$status];
					}
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function printable($id = false, $debug = 0)
	{
		if ($id) {

			$this->view_data['info'] = $info = $this->M_job_request->with_company()->with_project()->with_staff()->get($id);

			if ($debug) {
				vdebug($this->view_data);
			}

			$this->template->build('printable', $this->view_data);

			$generateHTML = $this->load->view('printable', $this->view_data, true);

			echo $generateHTML;
			die();

			pdf_create($generateHTML, 'generated_form');
		} else {

			show_404();
		}
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];
		$_extra_datas		=	[];
		$_adatas			=	[];

		$_titles[]	=	'#';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		$job_requests	=	$this->M_job_request->as_array()->get_all();


		if ($job_requests) {

			foreach ($job_requests as $skey => $job_request) {

				$_datas[$job_request['id']]['#']	=	$_no;

				$_no++;
			}


			$_filename	=	'list_of_job_requests' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				$_additional_column	=	$this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

						if ((strpos($_name, 'source') !== FALSE) or (strpos($_name, 'reference') !== FALSE) or (strpos($_name, 'academic') !== FALSE)) {

							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($job_requests as $skey => $job_request) {

								$_extra_datas[$job_request['id']][$_title]	=	isset($job_request[$_name]) && $job_request[$_name] ? $job_request[$_name] : '';
							}
						} elseif ((strpos($_name, 'work_experience') !== FALSE)) {
							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? $_name : '';

							foreach ($job_requests as $skey => $job_request) {

								$_extra_datas[$job_request['id']][$_title]	=	isset($job_request[$_name]) && $job_request[$_name] ? $job_request[$_name] : '';
							}
						} else {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($job_requests as $skey => $job_request) {

								if ($_column === 'sales_group_id') {

									$_datas[$job_request['id']][$_title]	=	isset($job_request[$_column]) && $job_request[$_column] ? Dropdown::get_static('group_type', $job_request[$_column], 'view') : '';
								} elseif ($_column === 'job_request_position_id') {

									$_datas[$job_request['id']][$_title]	=	isset($job_request[$_column]) && $job_request[$_column] ? Dropdown::get_dynamic('job_request_positions', $job_request[$_column], 'name', 'id', 'view') : '';
								} elseif ($_column === 'birth_date') {

									$_datas[$job_request['id']][$_title]	=	isset($job_request[$_column]) && $job_request[$_column] && (strtotime($job_request[$_column]) > 0) ? date_format(date_create($job_request[$_column]), 'm/d/Y') : '';
								} elseif ($_column === 'gender') {

									$_datas[$job_request['id']][$_title]	=	isset($job_request[$_column]) && $job_request[$_column] ? Dropdown::get_static('sex', $job_request[$_column], 'view') : '';
								} elseif ($_column === 'is_active') {

									$_datas[$job_request['id']][$_title]	=	isset($job_request[$_column]) && $job_request[$_column] ? Dropdown::get_static('bool', $job_request[$_column], 'view') : '';
								} else {
									$_datas[$job_request['id']][$_title]	=	isset($job_request[$_name]) && $job_request[$_name] ? $job_request[$_name] : '';
								}
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title	=	ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('List of Job Requests');
				$_objSheet->setCellValue('A1', 'LIST OF JOB REQUESTS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($job_requests as $key => $job_request) {

					$job_request_id	=	isset($job_request['id']) && $job_request['id'] ? $job_request['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title	=	ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$job_request_id][$_xls_columns[$_alpha]]) && $_datas[$job_request_id][$_xls_columns[$_alpha]] ? $_datas[$job_request_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns	=	$_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							if ($_a_column === 'contact') {

								$ad_title	=	'Contact Informations';
							} elseif ($_a_column === 'reference') {

								$ad_title	=	'References';
							} elseif ($_a_column === 'source') {

								$ad_title	=	'Source of Informations';
							} elseif ($_a_column === 'academic') {

								$ad_title	=	'Academic History';
							} else {

								$ad_title = $_a_column;
							}

							$a_title	=	ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$job_request_id][$_a_column])) {

									foreach ($_extra_datas[$job_request_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if ($key === 'to_report' or $key === 'to_attend' or $key === 'commission_based' or $key === 'is_member') {
												$xa_value	=	isset($_extra_datas[$job_request_id][$_a_column][$key]) && ($_extra_datas[$job_request_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$job_request_id][$_a_column][$key], 'view') : '';
											} else {
												$xa_value	=	$_extra_datas[$job_request_id][$_a_column][$key];
											}

											$xa_titles =	isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$job_request_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$job_request_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$job_request_id][$_a_column])) {

									foreach ($_extra_datas[$job_request_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					}

					$_start += 1;
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'job_request');
			}
		} else {

			$this->notify->error('No Record Found', 'job_request');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Job Request CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'job_request');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_job_request->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Job Requests');
			$_objSheet->setCellValue('A1', 'JOB REQUESTS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_failed_reasons = [];
				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'job_request');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {
							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							$_data['birth_date']	=	isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
							$job_requestGroupID = ['3'];

							if ($_id) {
								$data	=	$this->M_job_request->get($_id);
								if ($data) {

									unset($_data['id']);

									$oldPwd = password_format($data['last_name'], $data['birth_date']);
									$newPwd = password_format($_data['last_name'], $_data['birth_date']);

									$oldEmail = $data['email'];
									$newEmail = $_data['email'];

									if ($this->M_auth->email_check($oldEmail)) {

										if ($oldPwd !== $newPwd) {
											// Update User Password
											$this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
										}

										if ($oldEmail !== $newEmail) {
											// Update User Email
											$_user_data = [
												'email' => $newEmail,
												'username' => $newEmail,
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											];
											$this->M_user->update($_user_data, array('id' => $data['user_id']));
										}

										// Update Job Request Info
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_update	=	$this->M_job_request->update($_data, $_id);
										if ($_update !== FALSE) {

											$user_data = array(
												'first_name' => $_data['first_name'],
												'last_name' => $_data['last_name'],
												'active' => $_data['is_active'],
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											);

											$this->M_user->update($user_data, $data['user_id']);

											$_updated++;
										} else {

											$_failed_reasons[$_data['id']][] = 'update not working.';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email already used';
										$_failed++;

										break;
									}
								} else {

									// Generate user password
									$job_requestPwd = password_format($_data['last_name'], $_data['birth_date']);
									$job_requestEmail = $_data['email'];

									if (!$this->M_auth->email_check($_data['email'])) {

										$user_data = array(
											'first_name' => $_data['first_name'],
											'last_name' => $_data['last_name'],
											'active' => $_data['is_active'],
											'created_by' => $this->user->id,
											'created_at' => NOW
										);

										$userID = $this->ion_auth->register($job_requestEmail, $job_requestPwd, $job_requestEmail, $user_data, $job_requestGroupID);

										if ($userID !== FALSE) {

											$_data['user_id']	= $userID;
											$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['created_at']	=	NOW;
											$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['updated_at']	=	NOW;

											$_insert	=	$this->M_job_request->insert($_data);
											if ($_insert !== FALSE) {

												$_inserted++;
											} else {

												$_failed_reasons[$_data['id']][] = 'insert job_request not working';
												$_failed++;

												break;
											}
										} else {

											$_failed_reasons[$_data['id']][] = 'insert ion auth not working';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email check already existing';
										$_failed++;

										break;
									}
								}
							} else {

								// Generate user password
								$job_requestPwd = password_format($_data['last_name'], $_data['birth_date']);
								$job_requestEmail = $_data['email'];

								if (!$this->M_auth->email_check($_data['email'])) {

									$user_data = array(
										'first_name' => $_data['first_name'],
										'last_name' => $_data['last_name'],
										'active' => $_data['is_active'],
										'created_by' => $this->user->id,
										'created_at' => NOW
									);

									$userID = $this->ion_auth->register($job_requestEmail, $job_requestPwd, $job_requestEmail, $user_data, $job_requestGroupID);

									if ($userID !== FALSE) {

										$_data['user_id']	= $userID;
										$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['created_at']	=	NOW;
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_insert	=	$this->M_job_request->insert($_data);
										if ($_insert !== FALSE) {

											$_inserted++;
										} else {

											$_failed_reasons[$_dkey][] = 'insert job_request not working';

											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_dkey][] = 'insert ion auth not working';

										$_failed++;

										break;
									}
								} else {

									$_failed_reasons[$_dkey][] = 'email check already existing';

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {
							$this->notify->error('Upload Failed! Please follow upload guide. ', 'job_request');
						} else {

							$this->notify->success($_msg . '.', 'job_request');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'job_request');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'job_request');
			}
		} else {

			$this->notify->error('Something went wrong!', 'job_request');
		}
	}
}
