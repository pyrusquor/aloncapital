<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_payment extends MY_Controller
{

    private $fields = [
        array(
            'field' => 'info[period_id]',
            'label' => 'Period Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[payment_type_id]',
            'label' => 'Payment Type',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[amount_paid]',
            'label' => 'Amount Paid',
            'rules' => 'trim|required',
        ),

        array(
            'field' => 'info[payment_date]',
            'label' => 'Payment Date',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $transaction_models = array('transaction/Transaction_model' => 'M_Transaction', 'transaction/Transaction_payment_model' => 'M_Transaction_payment', 'Transaction_official_payment_model' => 'M_Transaction_official_payment', 'buyer/Buyer_model' => 'M_buyer', 'project/Project_model' => 'M_project', 'transaction_post_dated_check/Transaction_post_dated_check_model' => 'M_pdc', 'transaction/Transaction_billing_logs_model' => 'M_Tbilling_logs', 'transaction/Transaction_billing_model' => 'M_Tbilling', 'transaction_payment/Transaction_penalty_logs_model' => 'M_tpenalty_logs', 'transaction/Transaction_seller_model' => 'M_Transaction_seller', 'transaction_commission/Transaction_commission_model' => 'M_Transaction_commission', 'standard_report/Standard_report_model' => 'M_report');

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('property/Property_model', 'M_property');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('accounting_entries/accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_entry_items/accounting_entry_items_model', 'M_Accounting_entry_items');
        $this->load->model('accounting_settings/accounting_settings_model', 'M_Accounting_settings');
        $this->load->model('ar_clearing/ar_clearing_model', 'M_Ar_clearing');
        $this->load->model('ticketing/Ticketing_model', 'M_ticketing');
        $this->load->model('transaction/Transaction_adhoc_fee_schedule_model', 'M_adhoc_fee_schedule');
        $this->load->model('transaction/Transaction_adhoc_fee_model', 'M_adhoc_fee');
        $this->load->model('transaction_payment/Transaction_adhoc_fee_payments_model', 'M_adhoc_fee_payment');

        $this->load->model('aris/Aris_model', 'M_Aris');
        $this->load->model('aris/Aris_document_model', 'M_aris_document');
        $this->load->model('aris/Aris_lot_model', 'M_aris_lot');
        $this->load->model('aris/Aris_payment_model', 'M_aris_payment');
        $this->load->model('aris/Aris_report_model', 'M_aris_report');


        $this->load->model($transaction_models);

        // Load pagination library
        $this->load->library('ajax_pagination');
        // $this->load->library('..transaction/controllers/Transaction');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        $this->load->library('mortgage_computation');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Transaction_official_payment->fillable;
        $this->_table_columns = $this->M_Transaction_official_payment->__get_columns();
    }

    public function get_total_amount_paid()
    {
        $return = $this->M_Transaction_official_payment->total_payment($this->input->post('transaction_id'));
        echo json_encode($return);
    }

    public function get_official_payments()
    {
        $return = $this->M_Transaction_official_payment->where('transaction_id', $this->input->post('transaction_id'))->get_all();
        echo json_encode($return);
    }

    public function get_completed_terms()
    {
        $return = $this->M_Transaction_payment->completed_terms_count($this->input->post('transaction_id'), $this->input->post('period_id'));
        echo json_encode($return);
    }

    public function check_partial_payments()
    {
        $return = $this->M_Transaction_payment->check_partial_payments($this->input->post('transaction_id'), $this->input->post('period_id'));
        echo $return;
    }

    public function template($page = "")
    {

        $this->template->build($page);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        // $_colms = $this->_table_columns;

        // $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // $db_columns = $this->M_Transaction_official_payment->get_columns();
        // if ($db_columns) {
        //     $column = [];
        //     foreach ($db_columns as $key => $dbclm) {
        //         $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

        //         if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
        //             if (strpos($name, '_id') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } elseif (strpos($name, 'is_') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } else {
        //                 $column[$key]['value'] = $name;
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             }
        //         } else {
        //             continue;
        //         }
        //     }

        //     $column_count = count($column);
        //     $cceil = ceil(($column_count / 2));

        //     $this->view_data['columns'] = array_chunk($column, $cceil);
        //     $column_group = $this->M_Transaction_official_payment->count_rows();
        //     if ($column_group) {
        //         $this->view_data['total'] = $column_group;
        //     }
        // }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'transaction' => true,
            'transaction_id' => true,
            'buyer_id' => true,
            'project_id' => true,
            'property_id' => true,
            'entry_id' => true,
            'or_number' => true,
            'or_date' => true,
            'period_id' => true,
            'payment_date' => true,
            'amount_paid' => true,
            'remarks' => true,
            'principal_amount' => true,
            'interest_amount' => true,
            'entry' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Transaction_official_payment
                ->with_transaction('fields:reference,property_id,project_id,buyer_id')
                ->order_by('transaction_official_payments.' . $columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("or_number like '%$searchValue%'");
                $query->or_where("or_date like '%$searchValue%'");
                $query->or_where("payment_date like '%$searchValue%'");
                $query->or_where("amount_paid like '%$searchValue%'");
                $query->or_where("remarks like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'or_number' => [
                    'data' => $filters['or_number'] ?? null,
                    'operator' => 'like',
                ],
                'remarks' => [
                    'data' => $filters['remarks'] ?? null,
                    'operator' => 'like',
                ],
                'transaction_official_payments.period_id' => [
                    'data' => $filters['period_id'] ?? null,
                    'operator' => '=',
                ],
                'date_range' => [
                    'data' => $filters['date_range'] ?? null,
                    'operator' => 'range',
                    'column' => 'payment_date',
                    'type' => 'daterange'
                ],
                'or_date' => [
                    'data' => $filters['or_date'] ?? null,
                    'operator' => 'range',
                    'column' => 'or_date',
                    'type' => 'daterange'
                ],

            ];
            $company_id = $filters['company_id'] ?? '0';
            $project_id = $filters['project_id'] ?? '0';
            $buyer_id = $filters['buyer_id'] ?? '0';
            $seller_id = $filters['seller_id'] ?? '0';
            $trans_ref = $filters['trans_ref'] ?? '0';

            // Advance Search
            $with_args = [$company_id, $project_id, $buyer_id, $seller_id, $trans_ref];

            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {
                    if (isset($value['type'])) {
                        if ($value['type'] == 'daterange') {

                            $query->where($value['column'], '>=', explode(' - ', $value['data'])[0]);
                            $query->where($value['column'], '<=', explode(' - ', $value['data'])[1]);

                            continue;
                        }
                    }
                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if (array_filter($with_args)) {
                $args = $this->M_Transaction_official_payment->get_transactions($company_id, $project_id, $buyer_id, $seller_id, $trans_ref);
                if ($args) {
                    $query->where('transaction_id', $args);
                } else {
                    $query->where('id', '=', '0');
                }
            }
            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {
                $query->order_by('transaction_official_payments.' . $columnName, $columnSortOrder);
                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {
                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['transaction'] = @$value['transaction']['reference'] ?? "";
                        $items[$key]['or_date'] = view_date($value['or_date']);
                        $items[$key]['payment_date'] = view_date($value['payment_date']);
                        $items[$key]['amount_paid'] = money_php($value['amount_paid']);
                        $items[$key]['entry'] =  $value['entry_id'] ? "<center><a class='btn btn-primary' target='_BLANK' href='" . base_url() . "accounting_entries/view/" . $value['entry_id'] . "'>VIEW ENTRY</a></center>" : "No entries found";

                        $project_name = get_value_field($value['transaction']['project_id'], 'projects', 'name') ?? "";
                        $property_name = get_value_field($value['transaction']['property_id'], 'properties', 'name') ?? "";
                        $buyer = get_person(@$value['transaction']['buyer_id'], "buyers");
                        $buyer_name = get_fname($buyer) ?? "";

                        $items[$key]['transaction_id'] = "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['transaction']['id'] . "'>" . $value['transaction']['reference'] . "</a>";
                        $items[$key]['property_id'] = "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['transaction']['property_id'] . "'>" . $property_name . "</a>";
                        $items[$key]['project_id'] = "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['transaction']['project_id'] . "'>" . $project_name . "</a>";
                        $items[$key]['buyer_id'] = "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $value['transaction']['buyer_id'] . "'>" . $buyer_name . "</a>";

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Transaction_official_payment->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
        );

        echo json_encode($response);
        exit();
    }

    public function view($id = false, $type = '')
    {
        $this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

        $this->js_loader->queue([
            '//www.amcharts.com/lib/3/amcharts.js',
            '//www.amcharts.com/lib/3/serial.js',
            '//www.amcharts.com/lib/3/radar.js',
            '//www.amcharts.com/lib/3/pie.js',
            '//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
            '//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
            '//www.amcharts.com/lib/3/plugins/export/export.min.js',
            '//www.amcharts.com/lib/3/themes/light.js',
        ]);

        if ($id) {

            $this->view_data['info'] = $this->M_Transaction
                ->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
                ->with_t_property()->with_billings()->with_buyers()
                ->with_sellers()->with_fees()->with_promos()->with_payments()->with_payments()
                ->get($id);

            $this->view_data['payments'] = $this->view_data['info']['payments'];

            $this->view_data['sellers'] = $this->M_Transaction_seller->where('transaction_id', $id)
                ->with_seller()->with_commissions()->get_all();

            $this->view_data['printables'] = $this->M_printable->as_array()->get_all();

            $this->view_data['transaction_id'] = $id;

            if ($this->view_data['info']) {

                if ($type == "debug") {

                    vdebug($this->view_data);
                }

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function form($transaction_id = 0, $id = 0)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $transaction_id = $this->input->post('transaction_id');

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            if ($this->form_validation->run() === true) {

                $post = $this->input->post();

                $this->transaction_library->initiate($transaction_id);

                $response = $this->insert_payment($post);
                $this->calculate_commissions($transaction_id);
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        // $this->view_data['projects'] = $this->M_project->as_array()->get_all();

        if ($transaction_id) {

            $this->view_data['transaction_id'] = $transaction_id;

            $this->view_data['transaction'] = $this->M_Transaction
                ->with_buyer()->with_property()
                ->get($transaction_id);

            $this->view_data['info'] = $transaction = $this->M_Transaction->with_scheme()->with_tbillings()->with_billings()->with_adhoc_fee()->with_adhoc_fee_schedule()->get($transaction_id);

            if ($transaction['general_status'] == 4) {
                $response['status'] = 0;
                $response['msg'] = 'Oops! Please refresh the page and try again.';

                $this->notify->error("Sorry! The current transaction was already cancelled! Please contact the administration if you wan't to proceed with this transaction.", 'transaction');
            }

            $this->view_data['checks'] = $this->M_pdc->with_bank()->get_all(array('transaction_id' => $transaction_id));

            $this->template->build('form/form', $this->view_data);
        } else {

            redirect('transaction_payment');
        }
    }

    public function insert_payment($post = array(), $internal = 0, $debug = 0, $array_payment = 0)
    {
        // vdebug($post);

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        $transaction_id = $post['transaction_id'];

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

        $transaction = $this->M_Transaction->get($transaction_id);


        $is_waived = $post['info']['is_waived'];
        $payment_application = $post['payment_application']; // 1 = normal, 2 = balloon, 3 continuous payment

        // remove commas from the $post data so that the ar clearing will display the correct decimal places

        $post['info']['amount_paid'] = remove_commas($post['info']['amount_paid']);
        $post['info']['adhoc_payment'] = remove_commas($post['info']['adhoc_payment']);
        $post['info']['grand_total'] = remove_commas($post['info']['grand_total']);
        $post['info']['check_deposit_amount'] = remove_commas($post['info']['check_deposit_amount']);

        // unset($post['info']['is_waived']);
        // unset($post['info']'payment_application']);

        $info = $post['info'];

        // $off_p['entry_id'] = 0;
        // $off_p['ar_number']     = $info['AR_number'];
        $off_p['transaction_id'] = $post['transaction_id'];
        $off_p['or_number'] = $info['OR_number'];
        $off_p['receipt_type']     = $info['receipt_type'];
        $off_p['or_date'] = $info['or_date'];
        $off_p['payment_date'] = $info['payment_date'];
        $off_p['amount_paid'] = $info['amount_paid'];
        $off_p['adhoc_payment'] = $info['adhoc_payment'];
        $off_p['rebate_amount'] = $info['rebate_amount'];
        $off_p['grand_total'] = $info['grand_total'];
        $off_p['remarks'] = $info['remarks'];
        $off_p['payment_type_id'] = $info['payment_type_id'];
        $off_p['period_id'] = $info['period_id'];
        $off_p['period_count'] = $info['period_count'];
        $off_p['rc_check_number'] = $info['rc_check_number'];
        $off_p['rc_bank_name'] = $info['rc_bank_name'];
        $off_p['payment_application'] = $payment_application;
        $period_id = $info['period_id'];

        $amount_paid = $off_p['amount_paid'];

        if(!$internal){
            if($off_p['rebate_amount']){
                $amount_paid = $off_p['amount_paid'] + $off_p['rebate_amount'];
                // $payment_application = 2;
                // $off_p['payment_application'] = 2;
            }
        }

        if (empty($is_waived)) {
            $off_p['penalty_amount'] = remove_commas($info['penalty_amount']);
            $amount_paid = $off_p['amount_paid'] - $off_p['penalty_amount'];
        }

        $relative = $this->get_relative_amount($amount_paid, $off_p['transaction_id'], $off_p['period_id'], $payment_application, $debug, $array_payment);
        $u_payments = $relative['payments'];

        if ($u_payments) {

            if ($payment_application == "3") {
                $off_p['principal_amount'] = floatval($relative['principal']) + floatval($relative['interest']);
                $off_p['interest_amount'] = 0;
                $off_p['post_dated_check_id'] = $info['post_dated_check_id'];

                $relative['principal'] = $relative['principal'] + $relative['interest'];
                $relative['interest'] = 0;
            } else {
                $off_p['principal_amount'] = $relative['principal'];
                $off_p['interest_amount'] = $relative['interest'];
                $off_p['post_dated_check_id'] = $info['post_dated_check_id'];
            }

            $off_p['period_count'] = $relative['period_count'];

            $off_payment_id = $this->M_Transaction_official_payment->insert($off_p + $additional);

            $is_recognized = $transaction['is_recognized'];

            if ($is_recognized) {

                $this->transaction_library->generate_entries($off_payment_id, false, true, $post);
            } else {

                $_entry_res = $this->transaction_library->generate_entries($off_payment_id, false, false, $post);
            }

            $off_p_entry_id['entry_id'] = @$_entry_res['entry_id'];

            $this->M_Transaction_official_payment->update($off_p_entry_id + $additional, $off_payment_id);

            if (!empty($off_p['penalty_amount'])) {
                $this->insert_penalty_logs($off_payment_id, $transaction_id, $post);
            }

            if ($u_payments) {
                foreach ($u_payments as $key => $u_payment) {
                    $u_additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $_u_payment['is_paid'] = $u_payment['is_paid'];
                    $_u_payment['is_complete'] = $u_payment['is_complete'];
                    $this->M_Transaction_payment->update($_u_payment + $u_additional, array('id' => $u_payment['id']));

                    $off_p_update['transaction_payment_id'] = $u_payment['id'];
                    $this->M_Transaction_official_payment->update($off_p_update, array('id' => $off_payment_id));

                    $period_id = $u_payment['period_id'];
                }
            }

            if ($info['period_id'] == 1) {
                $period_id = 2;
            }

            if ((($payment_application == 3) || ($payment_application == 2)) && ($relative['amount_paid'])) {
                $b_amount_paid = $relative['amount_paid'];
                $b_period_id = $info['period_id'];
                $b_transaction_payment_id = $relative['transaction_payment_id'];

                $this->balloon($b_transaction_payment_id, $relative, 1);

                if ($array_payment) {
                    $this->M_transaction_payment->update(array('interest_amount' => $array_payment[1]), $b_transaction_payment_id);
                }
            }

            if (($payment_application == 5) && ($relative['amount_paid'])) {
                $b_amount_paid = $relative['amount_paid'];
                $b_period_id = $info['period_id'];
                $b_transaction_payment_id = $relative['transaction_payment_id'];
                $relative['aris_is_paid'] = 1;

                $this->balloon($b_transaction_payment_id, $relative, 1);
            }

            // do this as library
            // $_transaction_data['period_id'] = $period_id;
            // $this->M_Transaction->update($_transaction_data + $u_additional, array('id' => $off_p['transaction_id']));
            $this->transaction_library->initiate($transaction_id);

            $this->transaction_library->update_period_status();

            $_property_data['status'] = 3;
            $this->M_property->update($_property_data + $u_additional, array('id' => $transaction['property_id']));
        } elseif ($payment_application == 9) {

            $off_p['transaction_payment_id'] = $post['transaction_payment_id'];
            $off_p['amount_paid'] = $off_p['penalty_amount'];
            $off_p['remarks'] = 'Penalty Only';

            $off_payment_id = $this->M_Transaction_official_payment->insert($off_p + $additional);

            $this->transaction_library->generate_entries($off_payment_id, false, true, $post);

            if (!empty($off_p['penalty_amount'])) {
                $this->insert_penalty_logs($off_payment_id, $transaction_id, $post);
            }
            $this->transaction_library->initiate($transaction_id);

            $this->transaction_library->update_period_status();
        } else {
            $response['status'] = 0;
            $response['message'] = 'Oops! Something went wrong. Please check your payment details';

            return $response;
        }
        $payment_percentage = $this->transaction_library->get_amount_paid(1, 0, 1);
        if ($payment_percentage >= 25) {
            $this->M_Transaction->update(array('sales_recognize_threshold_date' => $off_p['payment_date']), $transaction_id);
        }


        $this->db->trans_complete(); # Completing transaction

        /*Optional*/
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $this->db->trans_commit();
            $response['status'] = 1;
            $response['message'] = 'Payment Successfully inserted!';
        }
        return $response;
    }

    public function insert_payment_by_official_id($official_payment_id = 0, $debug = 0)
    {

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

        $official_payment = $this->M_Transaction_official_payment->get($official_payment_id);

        $accounting_entry_id = $official_payment['entry_id'];
        $transaction_payment_id = $official_payment['transaction_payment_id'];

        // // Delete entries first
        // $deleted = $this->M_Accounting_entries->delete($accounting_entry_id);

        // if($deleted) {
        //     // Get all entry items base on entry id
        //     $entry_items = $this->M_Accounting_entry_items->get_all(["accounting_entry_id" => $accounting_entry_id]);

        //     // Delete it
        //     if(!empty($entry_items)) {
        //         foreach ($entry_items as $key => $item) {
        //             $this->M_Accounting_entry_items->delete($item['id']);
        //         }
        //     }
        // }

        $_u_payment['is_paid'] = 1;
        $_u_payment['is_complete'] = 0;
        $a = $this->M_Transaction_payment->update($_u_payment + $u_additional, $transaction_payment_id);

        $transaction_id = $official_payment['transaction_id'];
        $transaction = $this->M_Transaction->get($transaction_id);

        $is_waived = 0;
        $payment_application = $official_payment['payment_application']; // 1 = normal, 2 = balloon, 3 continuous payment

        // unset($post['info']['is_waived']);
        // unset($post['info']'payment_application']);

        $info = $official_payment;

        // $off_p['entry_id'] = 0;
        // $off_p['ar_number']     = $info['AR_number'];
        $off_p['transaction_id'] = $transaction_id;
        $off_p['or_number'] = $info['or_number'];
        $off_p['receipt_type']     = $info['receipt_type'];
        $off_p['or_date'] = $info['or_date'];
        $off_p['payment_date'] = $info['payment_date'];
        $off_p['amount_paid'] = remove_commas($info['amount_paid']);
        $off_p['adhoc_payment'] = remove_commas($info['adhoc_payment']);
        $off_p['grand_total'] = remove_commas($info['grand_total']);
        $off_p['remarks'] = $info['remarks'];
        $off_p['payment_type_id'] = $info['payment_type_id'];
        $off_p['period_id'] = $info['period_id'];
        $off_p['period_count'] = $info['period_count'];
        $off_p['rc_check_number'] = $info['rc_check_number'];
        $off_p['rc_bank_name'] = $info['rc_bank_name'];
        $off_p['payment_application'] = $payment_application;
        $period_id = $info['period_id'];

        $amount_paid = $off_p['amount_paid'];


        if (empty($is_waived)) {
            $off_p['penalty_amount'] = remove_commas($info['penalty_amount']);
            $amount_paid = $off_p['amount_paid'] - $off_p['penalty_amount'];
        }


        $relative = $this->get_relative_amount($amount_paid, $off_p['transaction_id'], $off_p['period_id'], $payment_application);

        $u_payments = $relative['payments'];

        if ($u_payments) {
            if ($payment_application == "3") {
                $off_p['principal_amount'] = floatval($relative['principal']) + floatval($relative['interest']);
                $off_p['interest_amount'] = 0;
                $off_p['post_dated_check_id'] = $info['post_dated_check_id'];
            } else {
                $off_p['principal_amount'] = $relative['principal'];
                $off_p['interest_amount'] = $relative['interest'];
                $off_p['post_dated_check_id'] = $info['post_dated_check_id'];
            }


            $off_payment_id = $this->M_Transaction_official_payment->update($off_p, array('id' => $official_payment_id));
            $is_recognized = $transaction['is_recognized'];

            // if($is_recognized) {
            //     $this->transaction_library->generate_entries($official_payment_id, false, true, $info);
            // } else {
            //     $_entry_res = $this->transaction_library->generate_entries($official_payment_id, false, false, $info);
            // }

            // $off_p_entry_id['entry_id'] = @$_entry_res['entry_id'];
            // $this->M_Transaction_official_payment->update($off_p_entry_id + $additional, $official_payment_id);

            // if (!empty($off_p['penalty_amount'])) {
            //     $this->insert_penalty_logs($official_payment_id, $transaction_id, $info);
            // }

            if ($u_payments) {
                foreach ($u_payments as $key => $u_payment) {
                    $u_additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $_u_payment['is_paid'] = $u_payment['is_paid'];
                    $_u_payment['is_complete'] = $u_payment['is_complete'];
                    $this->M_Transaction_payment->update($_u_payment + $u_additional, array('id' => $u_payment['id']));

                    $off_p_update['transaction_payment_id'] = $u_payment['id'];
                    $this->M_Transaction_official_payment->update($off_p_update, array('id' => $official_payment_id));

                    $period_id = $u_payment['period_id'];
                }
            }

            if ($info['period_id'] == 1) {
                $period_id = 2;
            }

            if ((($payment_application == 2) || ($payment_application == 3)) && ($relative['amount_paid'])) {
                $b_amount_paid = $relative['amount_paid'];
                $b_period_id = $info['period_id'];
                $b_transaction_payment_id = $relative['transaction_payment_id'];

                $this->balloon($b_transaction_payment_id, $relative, 1);
            }

            // do this as library
            // $_transaction_data['period_id'] = $period_id;
            // $this->M_Transaction->update($_transaction_data + $u_additional, array('id' => $off_p['transaction_id']));
            $this->transaction_library->initiate($transaction_id);

            $this->transaction_library->update_period_status();

            $_property_data['status'] = 3;
            $this->M_property->update($_property_data + $u_additional, array('id' => $transaction['property_id']));
        } else {
            $response['status'] = 0;
            $response['message'] = 'Oops! Something went wrong. Please check your payment details';

            return $response;
        }

        $this->db->trans_complete(); # Completing transaction

        /*Optional*/
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $this->db->trans_commit();
            $response['status'] = 1;
            $response['message'] = 'Payment Successfully inserted!';
        }
        return $response;
    }

    public function update_period_status($transaction_id = 0)
    {
        $this->transaction_library->initiate($transaction_id);
        $a = $this->transaction_library->update_period_status();
        vdebug($a);
    }

    public function insert_penalty_logs($official_payment_id = 0, $transaction_id = 0, $post = array())
    {
        $info = $post['info'];

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

        $off_p['transaction_official_payment_id'] = $official_payment_id;
        $off_p['transaction_id'] = $transaction_id;
        $off_p['transaction_payment_id'] = $post['transaction_payment_id'];
        $off_p['is_waived'] = $info['is_waived'];
        $off_p['amount'] = $info['penalty_amount'];
        $off_p['days_lapsed'] = 0;

        $off_payment_id = $this->M_tpenalty_logs->insert($off_p + $additional);
        $this->db->trans_complete(); # Completing transaction

        /*Optional*/
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $this->db->trans_commit();
            $response['status'] = 1;
            $response['message'] = 'Penalty Log Successfully inserted!';
        }
        return $response;
    }

    public function get_relative_amount($amount_paid = 0, $transaction_id = 0, $period_id = 0, $payment_application = 0, $debug = 0, $array_payment = 0)
    {
        // http://13.251.190.131/rems-sales/transaction_payment/get_relative_amount/100000/30/3/0/1
        // http://localhost/rems/transaction_payment/get_relative_amount/56800/46/2/2/1
        $original_amount_paid = $amount_paid;
        $params['transaction_id'] = $transaction_id;
        $params['period_id'] = $period_id;
        $params['is_complete'] = 0;

        $order['period_id'] = 'ASC';
        $order['id'] = 'ASC';

        $payments = $this->M_Transaction_payment->where($params)->order_by($order)->get_all();

        $return['payments'] = array();

        $no = 0;

        if ($debug == 1) {
            vdebug($payments);
        }

        if ($debug == 2) {
            vdebug($amount_paid);
        }
        $stop = 0;
        $return['principal'] = 0;
        $return['interest'] = 0;
        $f_principal = 0;
        $f_interest = 0;

        if ($payments) {
            foreach ($payments as $key => $payment) {

                if ((($payment_application == 3)  || ($payment_application == 2) || $payment_application == 5 || $payment_application == 9)  && ($stop)) { // advance payment go to balloon payment function
                    $return['principal'] = $amount_paid + $return['principal'];
                    break;
                }
                $p_id = $payment['id'];
                $stop = 1;

                $is_complete = 0;

                $interest = $payment['interest_amount'];
                $principal = $payment['principal_amount'];
                $total = $interest + $principal;

                if ($payment['is_paid'] == 1) {
                    $existing_principal = 0;
                    $existing_interest = 0;
                    $param['transaction_id'] = $transaction_id;
                    $param['transaction_payment_id'] = $payment['id'];
                    $official_payments = $this->M_Transaction_official_payment->where($param)->get_all();
                    $f_op_id = 0;

                    if ($official_payments) {
                        foreach ($official_payments as $key => $official_payment) {
                            $existing_principal = $official_payment['principal_amount'] + $existing_principal;
                            $existing_interest = $official_payment['interest_amount'] + $existing_interest;


                            if ($payment_application == 4) {
                                # code...
                                $paramsx['transaction_id'] = $transaction_id;
                                $paramsx['period_id'] = $period_id;
                                $paramsx['is_complete'] = 1;
                                $paramsx['id <'] = $official_payment['transaction_payment_id'];

                                $order['period_id'] = 'DESC';
                                $order['id'] = 'DESC';

                                $f_payments = $this->M_Transaction_payment->with_official_payments()->where($paramsx)->order_by($order)->get_all();

                                if ($f_payments) {
                                    $f_principal = 0;
                                    $f_interest = 0;
                                    foreach ($f_payments as $key => $f_payment) {

                                        if (isset($f_payment['official_payments']) && !empty($f_payment['official_payments'])) {
                                            break;
                                        }

                                        $f_principal = $f_payment['principal_amount'] + $f_principal;
                                        $f_interest = $f_payment['interest_amount'] + $f_interest;
                                    }
                                }

                                $f_op_id = $official_payment['transaction_payment_id'];
                            }
                        }
                    }


                    // vdebug($official_payments);

                    if ($payment['id'] == $f_op_id) {
                        $payment['principal_amount'] = $f_principal +  $payment['principal_amount'];
                        $payment['interest_amount'] = $f_interest + $payment['interest_amount'];
                    }

                    $principal = number_format($payment['principal_amount'], 2, '.', '') - number_format($existing_principal, 2, '.', '');
                    $interest = number_format($payment['interest_amount'], 2, '.', '') - number_format($existing_interest, 2, '.', ''); // 4385.24 - 9311.77
                    $total = $interest + $principal;
                    $stop = 0;
                }

                if ($debug == 6) {
                    echo "<pre>";
                    echo "amount_paid: " . $amount_paid;
                    echo "<br>";

                    echo "existing_principal: " . $existing_principal;
                    echo "<br>";
                    echo "existing_interest: " . $existing_interest;
                    echo "<br>";

                    echo "f_principal: " . $f_principal;
                    echo "<br>";
                    echo "f_interest: " . $f_interest;
                    echo "<br>";

                    echo "p_principal: " . $payment['principal_amount'];
                    echo "<br>";
                    echo "p_interest: " . $payment['interest_amount'];
                    echo "<br>";

                    echo "principal: " . $principal;
                    echo "<br>";
                    echo "interest: " . $interest;
                    echo "<br>";
                    echo "<br>";
                    vdebug($f_payments);
                }


                if (($amount_paid == 0) || ($amount_paid <= .1)) {
                    break;
                }

                if ($interest) {

                    if (trim($amount_paid) >= trim($interest)) {
                        $return['interest'] = @$return['interest'] + $interest;
                        $amount_paid = $amount_paid - $interest;
                    } else {
                        $return['interest'] = @$return['interest'] + $amount_paid;
                        $amount_paid = 0;
                    }
                }

                // $rem_p = 0;

                if ($principal) {

                    // $rem_p = trim($amount_paid) - trim($principal);

                    if (trim($amount_paid) >= trim($principal)) {

                        $return['principal'] = @$return['principal'] + $principal;
                        $amount_paid = $amount_paid - $principal;
                        $is_complete = 1;
                    } else {

                        $return['principal'] = @$return['principal'] + $amount_paid;
                        $amount_paid = 0;
                    }
                }

                $return['payments'][$key]['id'] = $payment['id'];
                $return['payments'][$key]['is_paid'] = 1;

                // Temporarily force payment application 3 to is_complete, remove after syncing.
                if ($payment_application == 3) {
                    $return['payments'][$key]['is_complete'] = 1;
                } else {
                    $return['payments'][$key]['is_complete'] = $is_complete;
                }
                $return['payments'][$key]['period_id'] = $payment['period_id'];

                // if($payment_application==9){
                //     $return['payments'][$key]['amount_paid'] = 0;
                //     $return['payments'][$key]['principal'] = 0;
                //     $return['payments'][$key]['interest'] = 0;
                //     $total_amount = 0;

                // } else{
                $return['payments'][$key]['amount_paid'] = $amount_paid;
                $return['payments'][$key]['principal'] = $principal;
                $return['payments'][$key]['interest'] = $interest;
                $total_amount = $payment['total_amount'];
                // }
            }

            // if ( ($amount_paid < $return['principal']) && ($payment_application == 2) ) {
            //     $amount_paid = $return['principal'];
            //     $no = 1;
            // }
            if ($payment_application == 9) {
                $original_amount_paid = 0;
                $total_amount = 0;
            }

            $return['transaction_payment_id'] = $p_id;
            $return['amount_paid'] = $original_amount_paid;
            $return['total_amount'] = $total_amount;
            $return['period_count'] = $payment_application == 9 ? 0 : is_nan(round(($total_amount / $original_amount_paid))) ? 1 : round(($total_amount / $original_amount_paid));
            $return['exact'] = $no;
        }

        if ($array_payment) {
            $return['payments']['0']['principal'] = $array_payment[0];
            $return['payments']['0']['interest'] = $array_payment[1];
            $return['principal'] = $array_payment[0];
            $return['interest'] = $array_payment[1];
        }

        if ($debug == 3) {
            vdebug($amount_paid);
        }

        if ($debug == 4) {
            vdebug($principal);
        }

        if ($debug == 5) {
            vdebug($return);
        }
        return $return;
    }

    public function balloon($transaction_payment_id = 0, $relative = [], $save = 0, $debug = 0)
    {

        $u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];

        // http://localhost/rems/transaction_payment/balloon/

        $payment = $this->M_Transaction_payment->get($transaction_payment_id);

        if ($payment) {

            $transaction_id = $payment['transaction_id'];
            $this->transaction_library->initiate($transaction_id);

            $period_id = $payment['period_id'];

            $transaction = $this->M_Transaction->with_tbillings()->with_billings()->get($transaction_id);
            $billings = $transaction['billings'];

            if ($billings) {
                foreach ($billings as $key => $billing) {
                    if ($billing['period_id'] == $period_id) {
                        $terms = $billing['period_term'];
                        $interest_rate = $billing['interest_rate'];
                    }
                }
            }

            $total_beginning_balance = $this->transaction_library->get_remaining_balance(2);
            $remaining_total_beginning_balance = $total_beginning_balance - $relative['principal'];

            $beginning_balance = $payment['beginning_balance'] - $relative['principal'];
            $effectivity_date = add_months_to_date($payment['due_date'], 1);

            if (($beginning_balance == 0) || ($beginning_balance <= 0)) {
                $payment['principal_amount'] = 0;
            }

            $s_u_payment['principal_amount'] = $relative['principal'];
            $s_u_payment['ending_balance'] = $beginning_balance;
            $s_u_payment['total_amount'] = $relative['principal'] + $relative['interest'];

            // $s_u_payment['total_amount'] = $payment['principal_amount'] + $payment['interest_amount'] + $relative['amount'];

            if ($save) {
                if ($relative['aris_is_paid']) {
                    $s_u_payment += array('is_complete' => '1');
                }
                $a = $this->M_Transaction_payment->update($s_u_payment + $u_additional, array('id' => $payment['id']));
            }

            if (($beginning_balance == 0) || ($beginning_balance <= 0)) {
                $u_payment['transaction_id'] = $transaction_id;
                $u_payment['is_paid'] = 0;
                $u_payment['period_id'] = $period_id;
                $this->M_Transaction_payment->update(array('deleted_reason' => 'balloon-payment'), $u_payment);
                $this->M_Transaction_payment->delete($u_payment);
                // return true;
            }

            $tb_logs['transaction_id'] = $transaction_id;
            $tb_logs['transaction_payment_id'] = $transaction_payment_id;
            $tb_logs['period_amount'] = $beginning_balance;
            $tb_logs['period_term'] = $terms;
            $tb_logs['effectivity_date'] = $effectivity_date;
            $tb_logs['starting_balance'] = $beginning_balance;
            $tb_logs['ending_balance'] = "";
            $tb_logs['interest_rate'] = $interest_rate;
            $tb_logs['period_id'] = $period_id;
            $tb_logs['slug'] = 'balloon-payment';
            $tb_logs['remarks'] = "Balloon Payment stop at " . $payment['particulars'];
            // vdebug($a);

            if ($save) {

                // vdebug($a);

                $tb_id = $this->M_Tbilling_logs->insert($tb_logs + $additional);

                $breakdown = $this->process_mortgage($transaction_id, 'save', 'balloon-payment', $tb_id);
            }
        }

        if ($debug == 1) {
            echo $save;
            echo "<br>";
            vdebug($payment);
        }
        if ($debug == 2) {
            echo $save;
            echo "<br>";
            vdebug($breakdown);
        }
        if ($debug == 3) {
            echo $save;
            echo "<br>";
            vdebug($s_u_payment);
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Transaction_payment->get($id);
            if ($list) {

                $deleted = $this->M_Transaction_payment->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Transaction successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Transaction_payment->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Transaction successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function export()
    {
        extract($this->input->post());
        $order = explode(',', $order);
        $_db_columns = [];
        $_alphas = [];
        $_datas = [];
        $_extra_datas = [];
        $_adatas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        if ($num_rows) {
            $this->db->limit($num_rows);
        }
        if ($or_number) {
            $this->db->where("or_number like '%$or_number%'");
        }
        if ($remarks) {
            $this->db->where("transaction_official_payments.remarks like '%$remarks%'");
        }
        if ($trans_ref) {
            $this->db->where("transactions.reference like '%$trans_ref%'");
        }
        if ($period_id) {
            $this->db->where("transaction_official_payments.period_id = $period_id");
        }
        if ($company_id) {
            $this->db->where("projects.company_id = $company_id");
        }
        if ($project_id) {
            $this->db->where("transactions.project_id = $project_id");
        }
        if ($buyer_id) {
            $this->db->where("transactions.buyer_id = $buyer_id");
        }
        if ($seller_id) {
            $this->db->where("transactions.seller_id = $seller_id");
        }
        if ($order) {
            $this->db->order_by($order[0], $order[1]);
        }
        if ($date_range) {
            $this->db->where('payment_date >=', explode(' - ', $date_range)[0]);
            $this->db->where('payment_date <=', explode(' - ', $date_range)[1]);
        }
        if ($or_date) {
            $this->db->where('or_date >=', explode(' - ', $or_date)[0]);
            $this->db->where('or_date <=', explode(' - ', $or_date)[1]);
        }
        $this->db->join('transactions', 'transaction_official_payments.transaction_id = transactions.id');
        $this->db->join('projects', 'transactions.project_id = projects.id');
        $this->db->select('transactions.id as t_id, transactions.period_id as t_period, projects.id as p_id');
        $transaction_payments = $this->M_Transaction_official_payment->fields('id,payment_status,period_id,transaction_id,entry_id,ar_number,or_number,or_date,receipt_type,amount_paid,principal_amount,interest_amount,penalty_amount,rebate_amount,adhoc_payment,grand_total,payment_type_id,post_dated_check_id,transaction_payment_id,payment_application,remarks,payment_date,period_count,rc_check_number,rc_bank_name,created_at,created_by,updated_at,updated_by,deleted_at,deleted_by')->get_all();

        if ($transaction_payments) {

            foreach ($transaction_payments as $skey => $transaction_payment) {

                $_datas[$transaction_payment['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_transaction_payments' . date('m_d_y_h-i-s', time()) . '.xls';

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');

                if ($_export_column) {
                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id')) {

                        $_column = $_name;

                        $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                        $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        foreach ($transaction_payments as $skey => $transaction_payment) {

                            if ($_column === 'transaction_id') {
                                $reference =  Dropdown::get_dynamic('transactions', $transaction_payment[$_column], 'reference', 'id', 'value');
                                $_datas[$transaction_payment['id']][$_title] = ($reference);
                            } elseif ($_column === 'period_id') {
                                $period =  Dropdown::get_static('period_names', $transaction_payment[$_column]);
                                $_datas[$transaction_payment['id']][$_title] = ($period);
                            } elseif ($_column === 'receipt_type') {
                                $receipt_type =  Dropdown::get_static('receipt_type', $transaction_payment[$_column]);
                                $_datas[$transaction_payment['id']][$_title] = ($receipt_type);
                            } elseif ($_column === 'entry_id') {

                                $_datas[$transaction_payment['id']][$_title] = isset($transaction_payment[$_column]) && $transaction_payment[$_column] ? $transaction_payment[$_column] : '';
                            } elseif ($_column === 'ar_number') {

                                $_datas[$transaction_payment['id']][$_title] = isset($transaction_payment[$_column]) && $transaction_payment[$_column] ? $transaction_payment[$_column] : '';
                            } elseif ($_column === 'transaction_payment_id') {

                                $_datas[$transaction_payment['id']][$_title] = isset($transaction_payment[$_column]) && $transaction_payment[$_column] ? $transaction_payment[$_column] : '';
                            } elseif ($_column === 'post_dated_check_id') {

                                $_datas[$transaction_payment['id']][$_title] = isset($transaction_payment[$_column]) && $transaction_payment[$_column] ? $transaction_payment[$_column] : '';
                            } elseif ($_column === 'payment_type_id') {
                                $payment_type =  Dropdown::get_static('payment_types', $transaction_payment[$_column]);
                                $_datas[$transaction_payment['id']][$_title] = ($payment_type);
                            } else {
                                $_datas[$transaction_payment['id']][$_title] = isset($transaction_payment[$_name]) && $transaction_payment[$_name] ? $transaction_payment[$_name] : '';
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_lastAlpha = end($_alphas);

                if (empty($_extra_datas)) {
                    foreach ($_xls_columns as $_xkey => $_column) {

                        $_title = ucwords(strtolower($_column));

                        $_objSheet->setCellValue($_xkey . $_row, $_title);
                        $_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                }

                $_objSheet->setTitle('List of Transaction Payments');
                $_objSheet->setCellValue('A1', 'LIST OF PAYMENTS');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                $col = 1;

                foreach ($transaction_payments as $key => $transaction_payment) {

                    $transaction_id = isset($transaction_payment['id']) && $transaction_payment['id'] ? $transaction_payment['id'] : '';

                    // PRIMARY INFORMATION COLUMN
                    foreach ($_alphas as $_akey => $_alpha) {
                        $_value = isset($_datas[$transaction_id][$_xls_columns[$_alpha]]) && $_datas[$transaction_id][$_xls_columns[$_alpha]] ? $_datas[$transaction_id][$_xls_columns[$_alpha]] : '';
                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start += 1;
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
            }
        } else {

            $this->notify->error('No Record Found', 'transaction');
        }
    }

    public function export_csv()
    {

        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Transaction Payment CSV Template.csv';

            // $_fillables    =    $this->M_document->fillable;
            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false && ($_fill !== 'user_id'))) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $records = $this->M_Transaction_official_payment->as_array()->get_all(); #up($_documents);
                if ($records) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($records as $_dkey => $record) {

                            $_datas[$record['id']][$_title] = isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Transaction Payments');
            $_objSheet->setCellValue('A1', 'COLLECTIONS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function import()
    {

        if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

            // if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
            if (true) {

                $_tmp_name = $_FILES['csv_file']['tmp_name'];
                $_name = $_FILES['csv_file']['name'];

                set_time_limit(0);

                $_columns = [];
                $_datas = [];

                $_failed_reasons = [];
                $_inserted = 0;
                $_updated = 0;
                $_failed = 0;

                /**
                 * Read Uploaded CSV File
                 */
                try {

                    $_file_type = PHPExcel_IOFactory::identify($_tmp_name);
                    $_objReader = PHPExcel_IOFactory::createReader($_file_type);
                    $_objPHPExcel = $_objReader->load($_tmp_name);
                } catch (Exception $e) {

                    $_msg = 'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

                    $this->notify->error($_msg, 'document');
                }

                $_objWorksheet = $_objPHPExcel->getActiveSheet();
                $_highestColumn = $_objWorksheet->getHighestColumn();
                $_highestRow = $_objWorksheet->getHighestRow();
                $_sheetData = $_objWorksheet->toArray();
                if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

                    if ($_sheetData[1][0] === 'id') {

                        $_columns[] = 'id';
                    }

                    // $_fillables    =    $this->M_document->fillable;
                    $_fillables = $this->_table_fillables;
                    if (!$_fillables) {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
                    }

                    foreach ($_fillables as $_fkey => $_fill) {

                        if (in_array($_fill, $_sheetData[1])) {

                            $_columns[] = $_fill;
                        } else {

                            continue;
                        }
                    }

                    foreach ($_sheetData as $_skey => $_sd) {

                        if ($_skey > 1) {

                            if (count(array_filter($_sd)) !== 0) {

                                $_datas[] = array_combine($_columns, $_sd);
                            }
                        } else {

                            continue;
                        }
                    }

                    if (isset($_datas) && $_datas) {

                        foreach ($_datas as $_dkey => $_data) {
                            $_id = isset($_data['id']) && $_data['id'] ? $_data['id'] : false;
                            $_data['birth_date'] = isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
                            $transactionGroupID = ['3'];

                            if ($_id) {
                                $data = $this->M_Transaction_official_payment->get($_id);
                                if ($data) {

                                    unset($_data['id']);

                                    $oldPwd = password_format($data['last_name'], $data['birth_date']);
                                    $newPwd = password_format($_data['last_name'], $_data['birth_date']);

                                    $oldEmail = $data['email'];
                                    $newEmail = $_data['email'];

                                    if ($this->M_auth->email_check($oldEmail)) {

                                        if ($oldPwd !== $newPwd) {
                                            // Update User Password
                                            $this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
                                        }

                                        if ($oldEmail !== $newEmail) {
                                            // Update User Email
                                            $_user_data = [
                                                'email' => $newEmail,
                                                'username' => $newEmail,
                                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : null,
                                                'updated_at' => NOW,
                                            ];
                                            $this->M_user->update($_user_data, array('id' => $data['user_id']));
                                        }

                                        // Update Transaction Info
                                        $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                        $_data['updated_at'] = NOW;

                                        $_update = $this->M_Transaction_payment->update($_data, $_id);
                                        if ($_update !== false) {

                                            $user_data = array(
                                                'first_name' => $_data['first_name'],
                                                'last_name' => $_data['last_name'],
                                                'active' => $_data['is_active'],
                                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : null,
                                                'updated_at' => NOW,
                                            );

                                            $this->M_user->update($user_data, $data['user_id']);

                                            $_updated++;
                                        } else {

                                            $_failed_reasons[$_data['id']][] = 'update not working.';
                                            $_failed++;

                                            break;
                                        }
                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'email already used';
                                        $_failed++;

                                        break;
                                    }
                                } else {

                                    // Generate user password
                                    $transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
                                    $transactionEmail = $_data['email'];

                                    if (!$this->M_auth->email_check($_data['email'])) {

                                        $user_data = array(
                                            'first_name' => $_data['first_name'],
                                            'last_name' => $_data['last_name'],
                                            'active' => $_data['is_active'],
                                            'created_by' => $this->user->id,
                                            'created_at' => NOW,
                                        );

                                        $userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

                                        if ($userID !== false) {

                                            $_data['user_id'] = $userID;
                                            $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                            $_data['created_at'] = NOW;
                                            $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                            $_data['updated_at'] = NOW;

                                            $_insert = $this->M_Transaction_payment->insert($_data);
                                            if ($_insert !== false) {

                                                $_inserted++;
                                            } else {

                                                $_failed_reasons[$_data['id']][] = 'insert transaction not working';
                                                $_failed++;

                                                break;
                                            }
                                        } else {

                                            $_failed_reasons[$_data['id']][] = 'insert ion auth not working';
                                            $_failed++;

                                            break;
                                        }
                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'email check already existing';
                                        $_failed++;

                                        break;
                                    }
                                }
                            } else {

                                // Generate user password
                                $transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
                                $transactionEmail = $_data['email'];

                                if (!$this->M_auth->email_check($_data['email'])) {

                                    $user_data = array(
                                        'first_name' => $_data['first_name'],
                                        'last_name' => $_data['last_name'],
                                        'active' => $_data['is_active'],
                                        'created_by' => $this->user->id,
                                        'created_at' => NOW,
                                    );

                                    $userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

                                    if ($userID !== false) {

                                        $_data['user_id'] = $userID;
                                        $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                        $_data['created_at'] = NOW;
                                        $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                        $_data['updated_at'] = NOW;

                                        $_insert = $this->M_Transaction_payment->insert($_data);
                                        if ($_insert !== false) {

                                            $_inserted++;
                                        } else {

                                            $_failed_reasons[$_dkey][] = 'insert transaction not working';

                                            $_failed++;

                                            break;
                                        }
                                    } else {

                                        $_failed_reasons[$_dkey][] = 'insert ion auth not working';

                                        $_failed++;

                                        break;
                                    }
                                } else {

                                    $_failed_reasons[$_dkey][] = 'email check already existing';

                                    $_failed++;

                                    break;
                                }
                            }
                        }

                        $_msg = '';
                        if ($_inserted > 0) {

                            $_msg = $_inserted . ' record/s was successfuly inserted';
                        }

                        if ($_updated > 0) {

                            $_msg .= ($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
                        }

                        if ($_failed > 0) {
                            $this->notify->error('Upload Failed! Please follow upload guide. ', 'transaction');
                        } else {

                            $this->notify->success($_msg . '.', 'transaction');
                        }
                    }
                } else {

                    $this->notify->warning('CSV was empty.', 'transaction');
                }
            } else {

                $this->notify->warning('Not a CSV file!', 'transaction');
            }
        } else {

            $this->notify->error('Something went wrong!', 'transaction');
        }
    }

    public function view_collections($transaction_id = 0, $type = "")
    {

        $params['transaction_id'] = $transaction_id;
        $this->view_data['payments'] = $this->M_Transaction_official_payment->where($params)->get_all($transaction_id);

        if ($type == "view") {

            $this->template->build('transaction/view/_collection_information', $this->view_data);
        } else {
            $return['html'] = $this->load->view('transaction/view/_collection_information', $this->view_data, true);

            echo json_encode($return);
        }
    }

    public function prnt($id = 0, $transaction_id = 0, $debug = 0)
    {

        $this->view_data['r'] = $r = $this->M_Transaction_official_payment->with_transaction()->get($id);

        // <option value="0">Select Type</option>
        // <option value="1">Collection Receipt</option>
        // <option value="2">Acknowledgement Receipt</option>
        // <option value="3">Provisionary Receipt</option>
        // <option value="4">Official Receipt</option>
        // <option value="5">Manual Receipt</option>

        $receipt_type = $r['receipt_type'];

        switch ($receipt_type) {
            case 1:
                $file = 'pdp_cr';
                break;
            case 2:
                $file = 'pdp_ar';
                break;
            case 3:
                $file = 'pdp_pr';
                break;
            case 4:
                $file = 'pdp_or';
                break;
            case 5:
                $file = 'receipt';
                break;
            default:
                $file = 'receipt';
                break;
        }

        if ($debug) {
            vdebug($this->view_data);
        }



        $this->view_data['amount_paid_in_words'] = number_to_words($r['amount_paid']);


        $this->load->view('transaction_payment/view/' . $file, $this->view_data);
    }

    public function computePenalty($transaction_id = '', $payment_date = '')
    {

        $return['amount'] = 0;

        $this->transaction_library->initiate($transaction_id);

        $period_id = $this->transaction_library->current_period(1);

        $payment_date = isLeapDay($payment_date);

        $amount_paid = $this->transaction_library->total_amount_due(3, $payment_date);

        $p_detail = $this->transaction_library->get_penalty($payment_date, 1);

        $return['amount'] = $p_detail['amount'];
        $return['new_amount'] = round($return['amount'] + $amount_paid, 2);
        $return['p_detail'] = $p_detail;

        echo json_encode($return);
    }

    public function tablePenalty($transaction_id = '', $payment_date = '', $debug = 0)
    {

        $return['amount'] = 0;

        $this->transaction_library->initiate($transaction_id);
        $transaction = $this->M_Transaction->get($transaction_id);

        $data['penalty_type'] = $transaction['penalty_type'];
        $data['d_billings'] = $this->transaction_library->get_remaining_dp($transaction_id);

        if (!$payment_date) {
            $payment_date = date('Y-m-d');
        }

        $payment_date = isLeapDay($payment_date);

        $data['payments'] = $this->transaction_library->get_overdue_table($payment_date, $debug);
        $return['period_count'] = count($data['payments']);

        if ($debug) {
            vdebug($data['payments']);
        }

        $return['html'] = $this->load->view('transaction/view/_detailed_due_information', $data, true);

        echo json_encode($return);
    }

    public function process_mortgage($transaction_id = 0, $type = "", $process = "", $tb_id = "")
    {
        // function to recalculate billing schedule
        // http://localhost/rems/transaction_payment/process_mortgage/46//balloon-payment/

        $this->transaction_library->initiate($transaction_id);

        $monthly_payment = 0;

        $breakdown = $this->transaction_library->process_mortgage($transaction_id, $type, $process, $this->user->id, $tb_id);

        if ($type != "save") {

            vdebug($breakdown);
        }
    }

    public function entries_form($id = false)
    {

        $transaction_payment = $this->M_Transaction_official_payment->with_transaction()->get($id);

        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            // $this->form_validation->set_rules($this->fields);

            if ($id) {

                $oof = $this->input->post();
                $entry_items = $oof['entry_item'];
                $accounting_entry = $oof['accounting_entry'];

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    // begin:Save accounting entries
                    $accounting_entry['or_number'] = 0;
                    $accounting_entry['company_id'] = 3;
                    $accounting_entry['invoice_number'] = $transaction_payment['transaction']['reference'];
                    $accounting_entry['journal_type'] = 3;
                    $accounting_entry['payment_date'] = $transaction_payment['due_date'];
                    $accounting_entry['payee_type'] = 'buyers';
                    $accounting_entry['payee_type_id'] = $transaction_payment['transaction']['buyer_id'];
                    $accounting_entry['remarks'] = $transaction_payment['particulars'];

                    $accounting_entry_info = $this->M_Accounting_entries->insert($accounting_entry + $additional);
                    $accounting_entry_id = $this->db->insert_id();
                    // end:Save accounting entries

                    foreach ($entry_items as $key => $entry) {
                        $entry_item['accounting_entry_id'] = $accounting_entry_id;
                        $entry_item['ledger_id'] = $entry['ledger_id'];
                        $entry_item['amount'] = $entry['amount'];
                        $entry_item['dc'] = $entry['dc'];
                        $entry_item['payee_type'] = 'buyers';
                        $entry_item['payee_type_id'] = $accounting_entry['payee_type_id'];
                        $entry_item['is_reconciled'] = 0;
                        $entry_item['description'] = $entry['description'];

                        $this->M_Accounting_entry_items->insert($entry_item + $additional);
                    }
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];
                }

                /*Optional*/
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $response['status'] = 1;
                    $response['message'] = 'Transaction Payment Successfully ' . $method . 'd!';
                }
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;

        if ($id) {
            $this->view_data['info'] = $data = $this->M_Transaction_official_payment->with_transaction()->get($id);

            // vdebug($data);

        }

        $this->template->build('entries_form', $this->view_data);
    }

    public function check_ticket($id = 0)
    {
        if ($id) {
            $data = $this->input->post();
            $transaction_id = $data['transaction_id'];
            $remarks = $data['remarks'];

            $ticket = [
                "transaction_id" => $transaction_id,
                "remarks" => $remarks
            ];

            $result = $this->M_ticketing->get($ticket);

            if ($result) {
                $response['status'] = 1;
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = 0;
                echo json_encode($response);
                exit();
            }
        }
    }

    public function generate_entries_by_project($project_id = 0, $transaction_id = 0, $debug = 0)
    {
        // if ($project_id) {

        if ($project_id) {
            $this->db->where('project_id', $project_id);
        } else if ($transaction_id) {
            $this->db->where('id', $transaction_id);
        }
        $transactions = $this->M_Transaction->order_by('id', 'DESC')->get_all();

        if ($transactions) {

            foreach ($transactions as $key => $transaction) {

                $official_payments = $this->M_Transaction_official_payment->order_by('id', 'ASC')->get_all(['transaction_id' => $transaction['id']]);

                if ($official_payments) {

                    foreach ($official_payments as $key => $official_payment) {
                        // code...
                        $off_payment_id = $official_payment['id'];

                        $this->re_generate_entries($off_payment_id, $debug, 0);
                    }
                }
            }
        }
        // }
    }

    public function delete_entries_by_project($project_id = 0, $debug = 0)
    {
        if ($project_id) {

            $this->M_Accounting_entries->delete(['project_id' => $project_id]);
        }
    }

    public function re_generate_entries($off_payment_id = 0, $debug = 0, $p = 1)
    {

        if ($off_payment_id) {

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            // Get official payment data
            $off_payment_data = $this->M_Transaction_official_payment->get($off_payment_id);

            // Get transaction_id from payment data
            $transaction = $this->M_Transaction->get($off_payment_data['transaction_id']);

            $transaction_id = $transaction['id'];
            $or_number = $off_payment_data['or_number'];

            // Checking string count due to accounting entries or_number column only has 12 char
            if (strlen($or_number) > 12) $or_number = substr($or_number, 0, 12);

            // Get accounting entry
            $accounting_entries = $this->M_Accounting_entries->get(["or_number" => $or_number]);

            if ($accounting_entries) {

                $accounting_entry_id = $accounting_entries['id'];

                // Delete entry
                $deleted = $this->M_Accounting_entries->delete($accounting_entry_id);

                if ($deleted) {
                    // Get all entry items base on entry id
                    $entry_items = $this->M_Accounting_entry_items->get_all(["accounting_entry_id" => $accounting_entry_id]);

                    // Delete it
                    if (!empty($entry_items)) {
                        foreach ($entry_items as $key => $item) {
                            $this->M_Accounting_entry_items->delete($item['id']);
                        }
                    }


                    $this->transaction_library->initiate($transaction_id);

                    $_entry_res = $this->transaction_library->generate_entries($off_payment_data['id'], false, false, false, $debug);

                    $off_p_entry_id['entry_id'] = @$_entry_res['entry_id'];

                    $this->M_Transaction_official_payment->update($off_p_entry_id + $additional, $off_payment_id);

                    if ($p) {
                        // $this->notify->success('Successfully generated entries.', 'transaction_payment');
                        redirect('transaction_payment');
                    }
                } else {
                    if ($p) {
                        $this->notify->error('Oops something went wrong.');
                    }
                }
            } else {

                $this->transaction_library->initiate($transaction_id);
                $_entry_res = $this->transaction_library->generate_entries($off_payment_data['id'], false);
                $off_p_entry_id['entry_id'] = @$_entry_res['entry_id'];
                $this->M_Transaction_official_payment->update($off_p_entry_id + $additional, $off_payment_id);

                if ($p) {

                    // $this->CI->session->set_flashdata('success', "Successfully generated entries.");
                    redirect('transaction_payment');
                    // $this->notify->success('Successfully generated entries.', 'transaction_payment');

                }
            }
        }
    }

    public function cancel_payment()
    {

        // Cancel payment
        // Change transaction_official_payments 'payment_status' to 1
        // Change accounting_entries 'cancelled_at' to date NOW

        $response['status'] = 0;
        $response['message'] = "Something went wrong. Payment not cancelled!";

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];

        $id = $this->input->post('id');

        if ($id) {

            $t_off_payment = $this->M_Transaction_official_payment->get($id);

            $entry_id = $t_off_payment['entry_id'];

            #code to reverse the accounting entries

            $accounting_entry = $this->M_Accounting_entries->with_accounting_entry_items()->get($entry_id);

            // vdebug($accounting_entry);

            if ($accounting_entry) {
                $rev_entry['company_id'] = $accounting_entry['company_id'];
                $rev_entry['or_number'] = $accounting_entry['or_number'];
                $rev_entry['invoice_number'] = $accounting_entry['invoice_number'];
                $rev_entry['journal_type'] = $accounting_entry['journal_type'];
                $rev_entry['payment_date'] = $accounting_entry['payment_date'];
                $rev_entry['payee_type'] = $accounting_entry['payee_type'];
                $rev_entry['payee_type_id'] = $accounting_entry['payee_type_id'];
                $rev_entry['remarks'] = 'Cancelled ' . $accounting_entry['remarks'];
                $rev_entry['cr_total'] = $accounting_entry['cr_total'];
                $rev_entry['dr_total'] = $accounting_entry['dr_total'];
                $rev_entry['is_approve'] = $accounting_entry['is_approve'];
                $rev_entry['land_inventory_id'] = $accounting_entry['land_inventory_id'];
                $rev_entry['project_id'] = $accounting_entry['project_id'];
                $rev_entry['property_id'] = $accounting_entry['property_id'];

                $cancelled = $this->M_Accounting_entries->insert($rev_entry + $additional);

                $accounting_entry_items = $accounting_entry['accounting_entry_items'];

                if ($accounting_entry_items) {
                    foreach ($accounting_entry_items as $key => $value) {
                        $new_item['accounting_entry_id'] = $cancelled;
                        $new_item['ledger_id'] = $value['ledger_id'];
                        $new_item['amount'] = $value['amount'];
                        if ($value['dc'] == 'd') {
                            $new_item['dc'] = 'c';
                        } else {
                            $new_item['dc'] = 'd';
                        }
                        $new_item['payee_type'] = $value['payee_type'];
                        $new_item['payee_type_id'] = $value['payee_type_id'];
                        $new_item['is_reconciled'] = $value['is_reconciled'];
                        $new_item['description'] = $value['description'];
                        $this->M_Accounting_entry_items->insert($new_item + $additional);
                        $this->M_Accounting_entry_items->delete($value['id']);
                    }
                }
            }

            $data['payment_status'] = 1; // Cancelled
            $data['remarks'] = "Cancelled";
            $data['deleted_at'] = NOW;
            $data['entry_id'] = $cancelled;

            $res = $this->M_Transaction_official_payment->update($data, $id);

            if ($res) {
                $response['status'] = 1;
                $response['message'] = "Payment successfully cancelled!";
            }
        }
        echo json_encode($response);
        exit();
    }

    public function delete_payment($id = 0, $debug = 0)
    {

        // Scenario
        // 1. Balloon Payment - Return to previouse schedule after delete collection
        // 2. Advance Payment - Return to previouse schedule after delete collection make sure that all affected rows will roll back. Do not leave a row with empy payment details where is_paid and is_complete is 1
        // 3. Delete Regular Payment (applied in 1 row) - standard delete return the is_paid and is_is complete. Retain existing function
        // 4. Delete Regular Payment (applied in multiple rows - upon delete, change all affected transaction payment values of is_paid and is_complete to 0 - OKAY
        // 5. Partial Payment - Retain existing function
        
        // 0 Regular Payment (Exact amount only, with interest or penalty)
        // 2 Balloon Payment (Interest and apply all surplus to Principal)
        // 1 Advance Payment (Principal & Interest)
        // 3 Continuous Payment (Apply all surplus to Principal)
        // 4 Partial Payment
        // 5 Special Payment

        $response['status'] = 0;
        $response['message'] = "Something went wrong. Payment not deleted!";

        if (!$id) {
            $id = $this->input->post('id');
        }

        if ($id) {

            $t_off_payment = $this->M_Transaction_official_payment->get($id);

            $entry_id = $t_off_payment['entry_id'];
            $payment_application = $t_off_payment['payment_application'];
            $transaction_payment_id = $t_off_payment['transaction_payment_id'];

            if (($t_off_payment) && ($t_off_payment['entry_id'])) {
                $this->M_Accounting_entries->delete($entry_id);

                $this->db->where('accounting_entry_id', $entry_id);
                $this->M_Accounting_entry_items->delete();
            }

            $t_payment = $this->M_Transaction_payment->get($transaction_payment_id);
            $transaction_id = $t_payment['transaction_id'];
            $trans_info = $this->M_Tbilling->fields('interest_rate,period_term')->where(['transaction_id' => $transaction_id, 'period_id' => $t_payment['period_id']])->get();
            $interest_rate = $trans_info['interest_rate'];
            $period_term = $trans_info['period_term'];

            $data['payment_status'] = 2; // Deleted
            $data['remarks'] = "Deleted";
            $data['deleted_at'] = NOW;

            if (!$debug) {

                $res = $this->M_Transaction_official_payment->update($data, $id);

                if ($res) {
                    $t_off_payments = $this->M_Transaction_official_payment->where('transaction_payment_id', $t_payment['id'])->get_all();
                }

                if ($t_off_payments) {
                    $t_payment_data['is_complete'] = 0;
                } else {
                    $t_payment_data['is_complete'] = 0;
                    $t_payment_data['is_paid'] = 0;
                }



                $t = $this->M_Transaction_payment->update($t_payment_data, $t_payment['id']);
            }

            $order['period_id'] = 'DESC';
            $order['id'] = 'DESC';
            $prev_payments = $this->M_Transaction_payment
                ->where('transaction_id', $transaction_id)
                ->where('id <', $t_payment['id'])
                ->where('is_paid', 1)
                ->where('is_complete', 1)
                ->order_by($order)
                ->with_official_payments()
                ->get_all();

            if ($debug) {
                vdebug([$prev_payments, $t_payment]);
            }

            if ($prev_payments) {
                foreach ($prev_payments as $key => $prev_payment) {
                    if (($prev_payment['is_paid']) && ($prev_payment['is_complete']) && !isset($prev_payment['official_payments'])) {

                        $t_payment_data['is_complete'] = 0;
                        $t_payment_data['is_paid'] = 0;
                        $t = $this->M_Transaction_payment->update($t_payment_data, $prev_payment['id']);

                    } else if ($prev_payment['official_payments']) {
                        break;
                    }
                }
            }

            if ($payment_application == 2 || $payment_application == 3) { // balloon payment and Continuous Payment
                $this->M_Tbilling_logs->insert([
                    'transaction_id' => $transaction_id,
                    'transaction_payment_id' => $t_payment['id'],
                    'period_id' => $t_payment['period_id'],
                    'period_amount' => $t_payment['beginning_balance'],
                    'period_term' => $period_term,
                    'effectivity_date' => $t_payment['due_date'],
                    'starting_balance' => $t_payment['beginning_balance'],
                    'ending_balance' => 0,
                    'interest_rate' => $interest_rate,
                    'slug' => 'deleted-balloon',
                    'remarks' => returnDecimal($t_payment['particulars']) - 1 . 'Recompute Start',
                    'created_at' => NOW,
                    'created_by' => $this->user->id

                ]);
                $bdown = $this->process_mortgage($transaction_id, 'save', 'balloon-payment', 0);
            } elseif ($payment_application == 1) { // Advance Payment
                $a_ids = [];
                $this->db->where("transaction_id = '$transaction_id'");
                $this->db->where("id <= '" . $t_off_payment['transaction_payment_id'] . "'");
                $this->db->order_by('id', 'desc');
                $trans_payments = $this->M_Transaction_payment->get_all();
                $total_advance_payments = $t_off_payment['principal_amount'];

                foreach ($trans_payments as $t_key => $t_payment) {
                    $total_advance_payments -= $t_payment['principal_amount'];
                    array_push($a_ids, $t_payment['id']);
                    if ($total_advance_payments < 0) {
                        break;
                    }
                }
                $ids = implode(',', $a_ids);
                $this->db->where("id in ($ids)");
                $this->db->where("transaction_id = $transaction_id");
                $this->db->where("deleted_at is null");
                $this->db->update('transaction_payments', ['is_paid' => 0, 'is_complete' => 0]);
            }


            if ($t) {
                $response['status'] = 1;
                $response['message'] = "Payment successfully deleted!";
            }
        }
        echo json_encode($response);
        exit();
    }

    public function cancelled_deleted_payments()
    {
        $this->view_data['data'] = $data = $this->M_Transaction_official_payment->where_payment_status(1, 2)->with_transaction()->get_all();

        $this->view_data['records'] = isset($data) && $data ? $data : 0;

        $this->template->build('cancelled_deleted_payments_view', $this->view_data);
    }

    public function total_day_payments()
    {

        $today = $today = date("Y-m-d");

        $collections_today = $this->M_Transaction_official_payment->where('payment_date', $today)->get_all();
        $total_collections = 0;

        if ($this->input->post()) {

            if ($collections_today) {

                foreach ($collections_today as $collection) {

                    $total_collections += $collection['amount_paid'];
                }
            }
        }
        $result['amount'] = money_php($total_collections);
        echo json_encode($result);
    }

    public function total_week_payments()
    {

        $total_collections = 0;
        $collections_count = 0;

        if ($this->input->post()) {
            $filter = $this->input->post('filter');
            $weekly_collections = $this->db->query('SELECT * FROM  transaction_official_payments WHERE  YEARWEEK(payment_date, 0) = YEARWEEK(CURDATE() - INTERVAL ' . $filter . ' WEEK, 0)');
            $result['week'] = get_date_range("week", $filter);
            if ($weekly_collections) {

                foreach ($weekly_collections->result_array() as $row) {
                    $total_collections += $row['amount_paid'];
                    $collections_count += 1;
                }
            }
        }
        $result['amount'] = money_php($total_collections);
        $result['count'] = $collections_count;
        $result['raw_amount'] = $total_collections;

        echo json_encode($result);
        exit();
    }

    public function total_month_payments()
    {

        $total_collections = 0;

        if ($this->input->post()) {
            $filter = $this->input->post('filter');
            $monthly_collections = $this->db->query('SELECT * FROM transaction_official_payments WHERE YEAR(payment_date) = YEAR(CURDATE() - INTERVAL ' . $filter . ' MONTH) AND MONTH(payment_date) = MONTH(CURDATE() - INTERVAL ' . $filter . ' MONTH) ORDER BY payment_date');
            $result['month'] = get_date_range("month", $filter);
            $result['dates'] = [];
            $result['indiv_amounts'] = [];

            if ($monthly_collections) {

                foreach ($monthly_collections->result_array() as $row) {
                    $total_collections += $row['amount_paid'];
                    array_push($result['dates'], date('F-d', strtotime($row['payment_date'])));
                    array_push($result['indiv_amounts'], $row['amount_paid']);
                }
            }
        }
        $result['amount'] = money_php($total_collections);
        echo json_encode($result);
        exit();
    }

    public function dashboard($debug = "")
    {

        $status = Dropdown::get_static('payment_types');
        $this->view_data['status'] = $status;

        $Projects = $this->M_project->order_by('name', 'ASC')->as_array()->get_all();

        if ($Projects) {
            # code...
            foreach ($Projects as $key => $project) {
                # code...
                $params['project_id'] =  $project['id'];
                // $params['date'] = date('Y-m-d');
                $r = $this->M_Transaction_official_payment->collections_dashboard($params);

                $this->view_data['projects'][$project['name']]['total'] = $r['amount_paid'] ? $r['amount_paid'] : 0;

                if ($status) {
                    # code...
                    foreach ($status as $key => $stat) {

                        if ($key) {
                            # code...
                            $w['project_id'] = $project['id'];
                            // $w['date'] = date('Y-m-d');
                            $w['payment_type_id'] = $key;

                            $rr = $this->M_Transaction_official_payment->collections_dashboard($w);
                            $this->view_data['projects'][$project['name']][$stat] =  $rr['amount_paid'] ? $rr['amount_paid'] : 0;
                        }
                    }
                }
            }
        }

        if ($debug == 1) {
            # code...
            vdebug($this->view_data);
        }

        $this->template->build('dashboard', $this->view_data);
    }

    public function dashboard_monthly($debug = "")
    {

        $date = NOW;
        $date_from = date("Y/m/d", strtotime($date . " -1 year"));
        $date_to  = date("Y/m/d", strtotime($date));

        $date_option_range = "Y-m";

        $period = $this->M_report->get_periods($date_from, $date_to);

        $months = array();
        $month_opt = "year_month";

        foreach ($period as $key => $dt) {
            $months[] = $dt->format("Y-m");
        }

        $daterange = view_date($date_from) . " - " . view_date($date_to);

        $projects = $this->M_project->order_by('name', 'ASC')->as_array()->get_all();

        //per project
        foreach ($projects as $key => $project) {

            $rows = [];

            foreach ($months as $key2 => $month) {

                $params['project_id'] = $project['id'];

                if (!$key2) {
                    // code...
                    unset($params["year_month"]);

                    $params["prev_year_month"] =  date("Y-m", strtotime($month." -1 month"));
                    $data['rows'][$project['id']]['previous'] =  $this->M_Transaction_official_payment->collections_dashboard($params);

                    // if (!$key) {
                        // vdebug( $data['rows'][$project['id']]['previous'] );
                    // }

                    $this->view_data['projects'][$project['name']]['previous']['amount_paid'] = ($data['rows'][$project['id']]['previous']['amount_paid'] ? (float) $data['rows'][$project['id']]['previous']['amount_paid'] : (float) 0);
                }

                unset($params["prev_year_month"]);
                
                $params[$month_opt] = $month;
                $data['rows'][$project['id']][$month] =  $this->M_Transaction_official_payment->collections_dashboard($params);
                $this->view_data['projects'][$project['name']][$month]['amount_paid'] = ($data['rows'][$project['id']][$month]['amount_paid'] ? (float) $data['rows'][$project['id']][$month]['amount_paid'] : (float) 0);

            }

            $j[$key]['name'] = $project['name'];
            $j[$key]['data'] = $rows;
        }

        array_unshift($months , 'previous');
        $this->view_data['months'] = $months;


        if ($debug == 1) {
            # code...
            vdebug($this->view_data);
        }

        $this->template->build('dashboard_monthly', $this->view_data);
    }


    public function dashboard_rems_vs_aris($debug = "")
    {

        $status = Dropdown::get_static('payment_types');
        $this->view_data['status'] = $status;

        $Projects = $this->M_project->order_by('name', 'ASC')->as_array()->get_all();

        if ($Projects) {
            # code...
            foreach ($Projects as $key => $project) {
                # code...
                $params['project_id'] =  $project['id'];
                // $params['date'] = date('Y-m-d');
                $r = $this->M_Transaction_official_payment->collections_dashboard($params);

                $this->view_data['projects'][$project['name']]['total'] = $r['principal_amount'] ? $r['principal_amount'] : 0;
            
            }
        }


        $aris_projects = $this->M_Aris->order_by('ProjectName', 'ASC')->as_array()->get_all();

         if ($aris_projects) {
            # code...
            foreach ($aris_projects as $key => $project) {
                # code...
                $p['project_id'] = $aris_project_id =  $project['id'];
              
                // $r = $this->M_aris_payment->get_sum($p);

                if ($aris_project_id) {
                    $params['where']['aris_documents.project_id'] = $aris_project_id;
                    $params['where']['aris_lots.project_id'] = $aris_project_id;
                    $params['where']['aris_customers.project_id'] = $aris_project_id;
                    // $params['where']['LASTNAME !='] = "";

                    $data['project_name'] = get_value_field($aris_project_id, 'aris_project_information', 'ProjectName');
                }

                $params['where']['aris_payments.project_id'] = $aris_project_id;
                $params['sort_by'] = "aris_customers.LASTNAME";
                $params['sort_order'] = "ASC";
                $params['group_by'] = "project_id";
                

                $r = $this->M_aris_report->get_all_collections_v2($params);

                // vdebug($r);

                $this->view_data['aris_projects'][$project['ProjectName']]['total'] = $r[0]['total_principal'] ? $r[0]['total_principal'] : 0;

            }
        }

        if ($debug == 1) {
            # code...
            vdebug($this->view_data);
        }

        $this->template->build('dashboard_rems_vs_aris', $this->view_data);
    }

    public function dashboard_per_project($project_id = 0,$debug = "")
    {

        $params['transactions.project_id'] = $project_id;
        $params['transactions.general_status !='] = 4;

        $project_name = get_value_field($project_id,'projects','name');
        $aris_project_id = get_value_field($project_name,'aris_project_information','id','ProjectName');

        // $transactions = $this->M_Transaction->order_by('transactions.reservation_date', 'ASC')->where($params)->as_array()->get_all();

        $transactions = $this->M_Transaction->fields('transactions.id as id, transactions.reference, transactions.reservation_date, transactions.project_id, transactions.property_id, transactions.buyer_id, transactions.seller_id, buyers.last_name')->join('buyers', 'transactions.buyer_id = buyers.id')->join('properties', 'transactions.property_id = properties.id')->order_by('buyers.last_name', 'ASC')->order_by('buyers.first_name', 'ASC')->order_by('transactions.reservation_date', 'ASC')->order_by('properties.name', 'ASC')->where($params)->as_array()->get_all();

        if ($transactions) {
            # code...
            foreach ($transactions as $key => $transaction) {

                $transaction_id =  $transaction['id'];
                $this->transaction_library->initiate($transaction_id);

                $transactions[$key]['total_payments_made'] = $this->transaction_library->get_amount_paid(2, 0);
                $transactions[$key]['collectible_price'] = get_value_field($transaction_id,'transaction_properties','collectible_price','transaction_id');
                $transactions[$key]['total_payments_made_percent'] = $this->transaction_library->get_amount_paid(2, 0, 1);
            
            }

            $this->view_data['projects'] = $transactions;
        }
        // vdebug($transactions);



        if ($aris_project_id) {
            $params['where']['aris_documents.project_id'] = $aris_project_id;
            $params['where']['aris_lots.project_id'] = $aris_project_id;
            $params['where']['aris_customers.project_id'] = $aris_project_id;
            // $params['where']['LASTNAME !='] = "";

            $data['project_name'] = get_value_field($aris_project_id, 'aris_project_information', 'ProjectName');
        }


        // $params['where']['aris_payments.DATE_PAID >='] = date('Y-m-d 00:00:00', strtotime($from));
        // $params['where']['aris_payments.DATE_PAID <='] = date('Y-m-d 23:59:59', strtotime($to));
        $params['where']['aris_payments.project_id'] = $aris_project_id;

        // if ($customer_id) {
        //     $params['where']['aris_customers.id'] = $customer_id;
        // }
        // if ($lot_id) {
        //     $params['where']['aris_lots.id'] = $lot_id;
        // }

        $params['sort_by'] = "aris_customers.LASTNAME";
        $params['sort_order'] = "ASC";
        $params['group_by'] = "DocumentID";

        $this->view_data['aris_projects'] = $this->M_aris_report->get_all_collections_v2($params);


        if ($debug == 1) {
            # code...
            vdebug($this->view_data['aris_projects']);
        }

        $this->template->build('dashboard_per_project', $this->view_data);
    }
    public function calculate_commissions($transaction_id = 0, $debug = 0)
    {

        $w['transaction_id'] = $transaction_id;
        $officials = $this->M_Transaction_official_payment->where($w)->with_transaction()->as_array()->get_all();
        $commissions = $this->M_Transaction_commission->where(array('transaction_id' => $transaction_id, 'status' => 1))->get_all(); // get all pending

        $principal = get_sum($officials, 1);

        if ($debug) {
            echo "<pre>";
            echo $principal;
            echo "<br>";
            vdebug($commissions);
        }

        // 1 - Pending
        // 2 - For RFP
        // 3 - Processing
        // 4 - Released

        if ($commissions) {
            # code...
            foreach ($commissions as $key => $commission) {
                # code...
                if ($principal >= $commission['amount_rate_to_collect']) {
                    # code...
                    $id = $commission['id'];
                    $data['status'] = 2;
                    $this->M_Transaction_commission->update($data, $id);
                }
            }
        }
    }

    public function adhoc_fee($transaction_id = 0)
    {
        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        $this->view_data['transaction_id'] = $transaction_id;
        if ($this->input->post()) {
            // vdebug($this->input->post());

            // Get the data from the form

            $maximum_value = $this->input->post('maximum_value');

            $amount_paid = $this->input->post('amount_paid');
            $transaction_id = $this->input->post('transaction_id');
            $payment_date = $this->input->post('payment_date');
            $receipt_date = $this->input->post('receipt_date');
            $is_waived = $this->input->post('is_waived');
            $penalty_amount = $this->input->post('penalty_amount');
            $payment_type_id = $this->input->post('payment_type_id');
            // $rc_bank_name = $this->input->post('rc_bank_name');
            // $rc_check_number = $this->input->post('rc_check_number');
            $cash_amount = $this->input->post('cash_amount');
            $check_deposit_amount = $this->input->post('check_deposit_amount');
            $receipt_type = $this->input->post('receipt_type');
            $receipt_number = $this->input->post('OR_number');
            $remarks = $this->input->post('remarks');
            // Get the different adhoc fees tables

            $transaction =  $this->M_Transaction->with_adhoc_fee()->get($transaction_id);
            $schedule = $this->M_adhoc_fee_schedule->where(array('is_complete' => '0', 'transaction_id' => $transaction_id))->get();

            $adhoc_fee = $transaction['adhoc_fee'];

            // Compare the maximum value with the amount paid. If it is equal to the maximum value,
            // set the schedule as complete.

            if ($amount_paid == $maximum_value) {
                $schedule_info = array('is_complete' => 1);
                $this->M_adhoc_fee_schedule->update($schedule_info + $u_additional, $schedule['id']);
            }

            $general_payment_info = array(
                'transaction_id' => $transaction_id,
                'adhoc_schedule' => $schedule['id'],
                'payment_date' => $payment_date,
                'receipt_date' => $receipt_date,
                'waive_penalty' => $is_waived,
                'penalty_amount' => $penalty_amount,
                'payment_type_id' => $payment_type_id,
                'cash_amount' => $cash_amount,
                'cheque_amount' => $check_deposit_amount,
                'receipt_type' => $receipt_type,
                'remarks' => $remarks,
            );

            // Iterate over every adhoc fee type to see if they have fully paid for it
            $count = 0;
            foreach ($adhoc_fee as $key => $value) {
                $initial_value = $value['amount'];
                $inc_value = $amount_paid - $initial_value;
                $app_number = $key + 1;
                if ($value['is_complete'] == 0) {
                    // if the incremental value goes below 1 meaning zero or a negative number, this means that there is an less money than the required amount
                    // we just save this amount and break the loop
                    if ($inc_value <= 0) {
                        // array_push($test,array('type' => $value['fee_id'], 'amount' => $amount_paid, 'count' => $key+1));
                        $specific_payment_info = array(
                            'amount_paid' => $amount_paid,
                            'adhoc_fee' => $value['id'],
                        );
                        if ($count > 0) {
                            $r_number = array('receipt_number' => $receipt_number . '-' . $app_number,);
                        } else {
                            $r_number = array('receipt_number' => $receipt_number,);
                        }
                        $this->M_adhoc_fee_payment->insert($general_payment_info + $specific_payment_info + $r_number + $additional);
                        break;

                        // if the incremental value is  more than 0 that means that they paid an amount greater than this fee requires, save the maximum amount for this fee as
                        // the payment amount.
                    } else {
                        $amount_paid = $inc_value;
                        // array_push($test,array('type' => $value['fee_id'], 'amount' => $initial_value, 'count' => $key+1));
                        $specific_payment_info = array(
                            'amount_paid' => $initial_value,
                            'adhoc_fee' => $value['id'],
                            'receipt_number' => $receipt_number . '-' . $app_number,
                        );
                        $this->M_adhoc_fee_payment->insert($general_payment_info + $specific_payment_info + $additional);
                        $this->M_adhoc_fee->update(array('is_complete' => '1'), $value['id']);
                        $count++;
                    }
                }
            }
            $response['status'] = 1;
            $response['message'] = 'Successfully added Adhoc Fee Payment!';
            echo json_encode($response);
            exit();
        }

        if ($transaction_id) {

            $this->view_data['transaction_id'] = $transaction_id;

            $this->view_data['transaction'] = $this->M_Transaction
                ->with_buyer()->with_property()
                ->get($transaction_id);

            $this->view_data['info'] = $transaction = $this->M_Transaction->with_scheme()->with_tbillings()->with_billings()->with_adhoc_fee()->with_adhoc_fee_schedule()->with_adhoc_fee_payments()->get($transaction_id);
            // vdebug($this->view_data['info']);

            if ($transaction['general_status'] == 4) {
                $response['status'] = 0;
                $response['msg'] = 'Oops! Please refresh the page and try again.';

                $this->notify->error("Sorry! The current transaction was already cancelled! Please contact the administration if you wan't to proceed with this transaction.", 'transaction');
            }

            // Redirect to transaction view if adhoc fees has not been setup yet

            if (!$transaction['adhoc_fee']) {
                $this->notify->error("Sorry! The current transaction does not have adhoc fees setup!", "transaction/view/$transaction_id");
            }
            $maximum_value = 0;
            foreach ($transaction['adhoc_fee_schedule'] as $key => $value) {
                if (!$value['is_complete']) {
                    $adhoc_payments = $this->M_adhoc_fee_schedule->with_payments()->get($value['id']);
                    $maximum_value = $value['total_amount'];
                    if ($adhoc_payments['payments']) {
                        foreach ($adhoc_payments['payments'] as $key => $payment) {
                            $maximum_value = $maximum_value - $payment['amount_paid'];
                        }
                    }
                    break;
                }
            }
            // vdebug($transaction);
            $this->db->where('transaction_id', $transaction_id);
            $this->view_data['adhoc_fee'] = $this->M_adhoc_fee->with_payments()->get_all();

            // vdebug($this->view_data['adhoc_fee']);

            // if the maximum values is 0, this means that all of the adhoc fees have been fully paid! Redirect to view and post appropriate message
            if ($maximum_value == 0) {
                $this->notify->success("All Ad Hoc Fees are fully Paid!", "transaction/view/$transaction_id");
            }
            $this->view_data['maximum_value'] = $maximum_value;

            $this->view_data['checks'] = $this->M_pdc->with_bank()->get_all(array('transaction_id' => $transaction_id));

            // $this->template->build('form/form', $this->view_data);
            $this->template->build('adhoc_fee/form.php', $this->view_data);
        } else {

            redirect('transaction_payment');
        }
    }

    public function get_aris_document($transaction_id){
        $response['project_id'] = 0;
		$response['document_id'] = 0;
		$transaction = $this->M_Transaction->fields('reservation_date, ra_number')->with_property()->with_project()->get($transaction_id);
		if($transaction){
			$transaction_ra = $transaction['ra_number'];
			$transaction_radate = $transaction['reservation_date'];
			if($transaction['project'] && $transaction['property']){
				$property = $transaction['property'];
				$project_name = $transaction['project']['name'];
				if($project_name){
					$this->db->where("projectname like '%$project_name%'");
					$aris_project = $this->M_Aris->fields('id')->get();
					if($aris_project){
						$aris_project_id = $aris_project['id'];
						$lot = sprintf('%03d',$property['lot']);
						$block = sprintf('%03d',$property['block']);
						$property_string = $block . $lot;
						$this->db->where("project_id = $aris_project_id");
						$this->db->where("lot_no like '%$property_string'");
						$aris_lot = $this->M_aris_lot->fields('LotID')->get_all();
					}
				}
			}
		}
		if(isset($aris_lot) && $aris_lot){
			$this->db->where("(radate = '$transaction_radate' or ra = '$transaction_ra' )");
			$this->db->where("project_id = $aris_project_id");
			$aris_lots = implode(',',array_column($aris_lot,'LotID'));
			$this->db->where("lotid in ($aris_lots)");
			$aris_document = $this->M_aris_document->fields('project_id, DocumentID')->get();
            if($aris_document){
                $response['project_id'] = $aris_document['project_id'];
                $response['document_id'] = $aris_document['DocumentID'];
            }
		}
        return $response;
    }

    // public function test($transaction_id){
    //     $data = $this->M_Transaction_official_payment->get_or_nos($transaction_id);
    // }

    public function sync_aris_collections($project_id = FALSE, $document_id = FALSE, $internal_sync = 0, $ignore_bp = 0, $reverse =0 )
    {

        if ($this->input->post() || $internal_sync || $reverse) {
            if (!$internal_sync && !$reverse) {
                $project_id = $this->input->post('project_id');
                $document_id = $this->input->post('document_id');
            }

            if($reverse){
                $transaction_id = $reverse;
                $aris_data = $this->get_aris_document($transaction_id);
                if($aris_data['document_id']){
                    $project_id = $aris_data['project_id'];
                    $document_id = $aris_data['document_id'];
                }else{
                    $response['status'] = 0;
                    $response['message'] = 'No matching aris document found!';
                    echo json_encode($response);
                    exit();
                }
            }

            // Get ARIS Project Data

            $aris_project = $this->M_Aris->get($project_id);

            // Get REMS Project using ARIS Project name
            $project = $this->M_project->where('name', $aris_project['ProjectName'])->get();

            if (!$project) {
                $response['status'] = 0;
                $response['message'] = 'Project not found! Make sure that the project is in REMS.';
                if ($internal_sync) {
                    return $response;
                } else {
                    echo json_encode($response);
                    exit();
                }
            }

            $this->db->where('project_id', $project_id);
            $this->db->where('DocumentID', $document_id);
            $document = $this->M_aris_document->get();

            if (!$document) {
                $response['status'] = 0;
                $response['message'] = 'ARIS Record not found in REMS! Make sure that the records are synced.';
                if ($internal_sync) {
                    return $response;
                } else {
                    echo json_encode($response);
                    exit();
                }
            }
            if($reverse){
                $this->db->where("id = $transaction_id");
            }
            else{
                $this->db->where('general_status > 0');
                $this->db->where('general_status < 4');
                $this->db->where("aris_id = '$project_id-$document_id'");
            }

            $transaction = $this->M_Transaction->with_scheme_values()->get();

            if (!$transaction) {
                $response['status'] = 0;
                $response['message'] = 'Transaction not found! Sync the ARIS transaction first. ' . "$project_id-$document_id";
                if ($internal_sync) {
                    return $response;
                } else {
                    echo json_encode($response);
                    exit();
                }
            }

            $transaction_id = $transaction['id'];

            $loan_terms = $transaction['scheme_values']['2']['period_term'];
            $dp_terms = $transaction['scheme_values']['1']['period_rate_amount'] + $transaction['scheme_values']['1']['period_rate_percentage'];
            $loan_interest = end($transaction['scheme_values']);
            $loan_interest = $loan_interest['period_interest_percentage'];

            if($reverse){
                $or_numbers = $this->M_Transaction_official_payment->get_or_nos($transaction_id);
                if($or_numbers){
                    $this->db->where("OR_NO not in ('$or_numbers')");
                }
            }
            $this->db->where('project_id', $project_id);
            $this->db->where('DocumentID', $document_id);
            $this->db->where('AmountPaid > 0');
            $this->db->where('official_payment_id', '0');
            $this->db->order_by('PaymentID', 'asc');

            $aris_payments = $this->M_aris_payment->get_all();
            
            $dp_flag = 0;
            $has_rebate = 0;
            $write = 0;
            $balloon_partial = 0;
            $debug = 0;

            $response['status'] = 0;
            $response['message'] = 'Error syncing payments!';

            $synced = $this->M_aris_payment->if_synced($document_id, $project_id);

            $this->db->where('project_id', $project_id);
            $this->db->where('DocumentID', $document_id);
            $this->db->where('REMARKS', 'Full Payment');
            $fully_paid = $this->M_aris_payment->get();

            if ($synced && !$reverse) {
                $response['status'] = 0;
                $response['message'] = 'Collection already Synced!';
                if ($internal_sync) {
                    return $response;
                } else {
                    echo json_encode($response);
                    exit();
                }
            }

            $this->transaction_library->initiate($transaction_id);

            if (!$aris_payments) {
                $response['status'] = 0;
                $response['message'] = 'No ARIS Payments Found!';
                if ($internal_sync) {
                    return $response;
                } else {
                    echo json_encode($response);
                    exit();
                }
            }

            foreach ($aris_payments as $key => $payment) {
                $diff_interest = 0;
                $rebate = 0.00;
                $this->db->reset_query();


                if ($has_rebate) {
                    $has_rebate -= 1;
                    $this->M_aris_payment->update(array('official_payment_id' => $latest_payment['id']), $payment['id']);
                    continue;
                }

                if (!$dp_flag && (strpos($payment['REMARKS'], 'Installment') !== false || $payment['REMARKS'] == 'Balloon Payment')) {
                    $dp_flag = 1;
                    $write = 1;
                }

                if (strpos($payment['REMARKS'], 'Full Down Payment') !== false) {
                    $dp_flag = 0;
                    $write = 0;
                }

                $this->db->order_by('due_date', 'asc');
                $transaction_payment = $this->M_Transaction_payment->with_official_payments()->where(array('transaction_id' => $transaction_id, 'is_complete' => '0'))->get();

                $period_id = $transaction_payment['period_id'];
                $payment_date = $payment['DATE_PAID'];
                $or_date = $payment['OR_DATE'];
                $penalty_amount = floatval($payment['PENALTY']);
                $is_waived = $penalty_amount ? '0' : '1';
                $amount_paid = floatval($payment['AmountPaid']);
                $cash_amount = $amount_paid;
                $or_number = substr_replace($payment['OR_NO'], '-', 2, 0);
                $grand_total = $amount_paid;
                $remarks = $payment['REMARKS'];

                // $rems_amount_aris_penalty = floatval($transaction_payment['total_amount']) + $penalty_amount;
                $this->db->order_by('id', 'DESC');
                $this->db->where('deleted_at is null');
                $t_last_payment = $this->M_Transaction_payment->where(array('transaction_id' => $transaction_id))->get();

                if ($loan_terms > 1 && $t_last_payment['due_date'] != $payment['DATE_DUE']) {
                    // $transaction_payment = $this->M_Transaction_payment->where(array('transaction_id' => $transaction_id, 'due_date' => $payment['DATE_DUE']))->get();
                    // $current_due = number_format(floatval($transaction_payment['total_amount']), 2, '.', '');
                    $current_due = number_format($this->transaction_library->monthly_ammort($period_id), 2, '.', '');
                } else {
                    $current_due = number_format(floatval($this->transaction_library->monthly_ammort($period_id)), 2, '.', '');
                }

                $current_due = $current_due + $penalty_amount;

                if ($amount_paid == $current_due) {
                    $payment_application = '0';
                } elseif ($amount_paid > $current_due) {
                    $payment_application = '2';
                } elseif ($amount_paid < $current_due) {
                    $payment_application = '4';
                }

                if ($or_number[0] == 'R') {
                    $rebate = $amount_paid;
                }

                if (!$ignore_bp) {
                    if ($dp_flag && $payment['REMARKS'] == 'Balloon Payment' && $loan_terms > 1) {
                        $payment_application = '3';
                    }
                }

                if ($dp_flag && !$has_rebate && !$balloon_partial && $loan_terms > 1 && $payment_application != '3') {

                    $ap_params = array(
                        'project_id' => $project_id,
                        'DocumentID' => $document_id,
                        'DATE_DUE' => $payment['DATE_DUE'],
                        'official_payment_id' => '0'
                    );
                    $this->db->where('AmountPaid>0');
                    $dupe_aris_payments = $this->M_aris_payment->where($ap_params)->get_all();
                    $penalty_amounts = [];
                    $amount_paids = [];
                    $rebates = [];

                    if ($dupe_aris_payments) {

                        if (count($dupe_aris_payments) > 1) {
                            foreach ($dupe_aris_payments as $key => $dupe_payment) {
                                $penalty_amounts[] = floatval($dupe_payment['PENALTY']);
                                $amount_paids[] = floatval($dupe_payment['PRINCIPAL']) + floatval($dupe_payment['INTEREST']);
                                $rebates[] = $dupe_payment['OR_NO'][0] == 'R' ? floatval($dupe_payment['AmountPaid']) : 0.00;
                            }
                            $penalty_amount_i = array_sum($penalty_amounts);
                            $amount_paid_i = array_sum($amount_paids);
                            $rebate_i = array_sum($rebates);
                            $amount_paid_i = number_format($amount_paid_i, 2, '.', '');
                            $t_payment_due = number_format(floatval($transaction_payment['total_amount']), 2, '.', '');

                            if ($t_payment_due < $amount_paid_i) {
                                $is_waived = $penalty_amount_i ? '0' : '1';
                                $penalty_amount = $penalty_amount_i;
                                $amount_paid_i += $penalty_amount_i;
                                $amount_paid = $amount_paid_i;
                                $cash_amount = $amount_paid_i;
                                $grand_total = $amount_paid_i;
                                $has_rebate = count($dupe_aris_payments) - 1;
                                $payment_application = '2';
                                $rebate = $rebate_i;
                            } elseif (($t_payment_due > $amount_paid_i) && $fully_paid) {
                                $is_waived = $penalty_amount_i ? '0' : '1';
                                $penalty_amount = $penalty_amount_i;
                                $amount_paid_i += $penalty_amount_i;
                                $amount_paid = $amount_paid_i;
                                $cash_amount = $amount_paid_i;
                                $grand_total = $amount_paid_i;
                                $has_rebate = count($dupe_aris_payments) - 1;
                                $payment_application = '5';
                                $rebate = $rebate_i;
                            } elseif ($t_payment_due > $amount_paid_i) {
                                // $is_waived = $penalty_amount_i ? '0' : '1';
                                // $penalty_amount = $penalty_amount_i;
                                // $amount_paid = $amount_paid_i;
                                // $cash_amount = $amount_paid_i;
                                // $grand_total = $amount_paid_i;
                                // $has_rebate = count($dupe_aris_payments)-1;
                                $balloon_partial = count($dupe_aris_payments);
                                // $rebate = $rebate_i;
                            }
                        } elseif (count($dupe_aris_payments) == 1) {
                            $t_payment = number_format(floatval($transaction_payment['total_amount']), 2, '.', '');
                            $a_penalty = number_format(floatval($payment['PENALTY']), 2, '.', '');
                            $a_amountpaid = number_format(floatval($payment['AmountPaid']), 2, '.', '');
                            if ($current_due > $a_amountpaid) {
                                $payment_application = '5';
                            }
                        }
                    }
                }

                if ($balloon_partial) {
                    $balloon_partial--;
                    $payment_application = '4';
                }

                $transaction_payment_id = $transaction_payment['id'];

                switch ($or_number[0]) {
                    case 'C':
                        $receipt_type = '1';
                        break;
                    case 'A':
                        $receipt_type = '2';
                        break;
                    case 'P':
                        $receipt_type = '3';
                        break;
                    case 'O':
                        $receipt_type = '4';
                        break;
                    default:
                        $receipt_type = '5';
                }

                if (($payment['PRINCIPAL'] + $payment['INTEREST']) == 0) {
                    $payment_application = 9;
                }

                if ($loan_interest && $dp_flag && $loan_terms > 1) {
                    if ($payment['INTEREST'] == 0) {
                        $payment_application = 3;
                    }
                }

                $payment_info = array(
                    'transaction_id' => $transaction_id,
                    'info' => array(
                        'period_count' => '1',
                        'period_id' => $period_id,
                        'payment_date' => $payment_date,
                        'or_date' => $or_date,
                        'is_waived' => $is_waived,
                        'penalty_amount' => $penalty_amount,
                        'rebate_amount' => $rebate,
                        'amount_paid' => $amount_paid,
                        'payment_type_id' => '1',
                        'post_dated_check_id' => '0',
                        'rc_bank_name' => '',
                        'rc_check_number' => '0',
                        'cash_amount' => $cash_amount,
                        'check_deposit_amount' => '0',
                        'receipt_type' => $receipt_type,
                        'OR_number' => $or_number,
                        'adhoc_payment' => '0',
                        'grand_total' => $grand_total,
                        'remarks' => $remarks,
                    ),
                    'transaction_payment_id' => $transaction_payment_id,
                    'payment_application' => $payment_application
                );


                if ($payment_application == 9) {
                    $payment_info['info']['amount_paid'] = 0;
                    $payment_info['info']['grand_total'] = 0;
                    $payment_info['info']['cash_amount'] = 0;
                }

                $transaction_p = $this->M_Transaction_payment->get($transaction_payment_id);
                $difference = abs($transaction_p['interest_amount'] - $payment['INTEREST']);
                if ($difference > 1 && !$balloon_partial && !$has_rebate) {
                    $diff_interest = array($payment['PRINCIPAL'], $payment['INTEREST']);
                    $payment_info['payment_application'] = '2';
                }

                if (!$dp_flag || $write) {
                    $payment_status = $this->insert_payment($payment_info, 1, $debug, $diff_interest);

                    if ($payment_status['status'] == 0) {
                        $response['status'] = 0;
                        $response['message'] = 'Error syncing payments! Check the aris_payment for ' . date('Y-m-d', strtotime($payment['DATE_DUE'])) . ', OR No. ' . $or_number . 'Transaction: ' . $transaction_id;
                        break;
                    } else {
                        $latest_payment = $this->M_Transaction_official_payment->where(array('transaction_id' => $transaction_id))->order_by('id', 'desc')->get();
                        $this->M_aris_payment->update(array('official_payment_id' => $latest_payment['id']), $payment['id']);
                        $response['status'] = 1;
                        $response['message'] = 'ARIS Collections successfully synced!';
                    }
                }

                if (!$dp_flag) {
                    if ($payment['REMARKS'] == 'Full Down Payment' || $dp_terms == 0) {
                        $dp_flag = 1;
                        $write = 1;
                    }
                }
            }

            if ($document['DateCancelled']) {
                $this->transaction_library->initiate($transaction_id);
                $this->transaction_library->update_transaction_status('1', '0', '4', 'Cancelled');
            }

            if ($internal_sync) {
                return $response;
            } else {
                echo json_encode($response);
                exit();
            }
        } else {
            show_403();
        }
    }
    public function generate_all_aris_collections()
    {
        // $skip = ['50',"51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175","176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191","192","193","194","195","196","197","198","199","200","201","202","203","204","205","206","207","208","209","210","211","212","213","214","215","216","217","218","219","220","221","222","223","224","412"];
        // $this->db->where('aris_id is not null');
        // $this->db->where('id<51');
        // $this->db->where('id>49');
        $synced_trans = $this->M_Transaction->get_all();
        $this->db->reset_query();
        // vdebug(explode('-',$synced_trans[0]['aris_id']));
        foreach ($synced_trans as $key => $transaction) {
            // if(in_array($transaction['id'],$skip)){
            //     continue;
            // }
            $params = explode('-', $transaction['aris_id']);
            $sync = $this->sync_aris_collections($params[0], $params[1], 1);
            if ($sync['status'] == '0') {
                break;
            }
        }
        return;
    }

    public function import_new_aris_payments($internal = 0, $project_id = 0, $document_id = 0)
    {
        if ($internal) {
            $ids = $this->M_aris_payment->get_new_payments($project_id, $document_id);
            if ($ids) {
                foreach ($ids as $key => $id) {
                    $transaction = $this->M_Transaction->with_scheme_values()->where(array('id' => $id))->get();
                    $this->db->order_by('id', 'DESC');
                    $this->db->where('deleted_at is null');
                    $t_last_payment = $this->M_Transaction_payment->where(array('transaction_id' => $id))->get();
                    $aris_id = explode('-', $transaction['aris_id']);
                    $this->transaction_library->initiate($id);

                    $this->db->where('project_id', $aris_id[0]);
                    $this->db->where('DocumentID', $aris_id[1]);
                    $this->db->where('DATE_PAID > "2021-08-11 00:00:00"');
                    $this->db->where('deleted_at is null');
                    $this->db->where('official_payment_id=0');
                    $this->db->order_by('project_id', 'ASC');
                    $this->db->order_by('PaymentID', 'ASC');
                    $payments = $this->db->get('aris_payments')->result_array();
                    // $payments = $aris;

                    $loan_terms = end($transaction['scheme_values'])['period_term'];
                    $has_rebate = 0;


                    foreach ($payments as $payment_key => $payment) {
                        $rebate = 0.00;

                        if ($has_rebate) {
                            $has_rebate -= 1;
                            $this->M_aris_payment->update(array('official_payment_id' => $latest_payment['id']), $payment['id']);
                            continue;
                        }
                        $period_id = $this->transaction_library->current_period(1);
                        $payment_date = $payment['DATE_PAID'];
                        $or_date = $payment['OR_DATE'];
                        $penalty_amount = number_format(floatval($payment['PENALTY']), 2, '.', '');
                        $is_waived = $penalty_amount ? '0' : '1';
                        $amount_paid = number_format(floatval($payment['AmountPaid']), 2, '.', '');
                        $cash_amount = $amount_paid;
                        $or_number = substr_replace($payment['OR_NO'], '-', 2, 0);
                        $grand_total = $amount_paid;
                        $remarks = $payment['REMARKS'];
                        // vdebug();
                        // $monthly = number_format($this->transaction_library->monthly_ammort($period_id),2,'.','');

                        if ($loan_terms > 1 && $t_last_payment['due_date'] != $payment['DATE_DUE']) {
                            $transaction_payment = $this->M_Transaction_payment->where(array('transaction_id' => $id, 'due_date' => $payment['DATE_DUE']))->get();
                            $current_due = number_format(floatval($transaction_payment['total_amount']), 2, '.', '');;
                        } else {
                            $current_due = number_format(floatval($this->transaction_library->total_amount_due(3)), 2, '.', '');
                        }




                        $ap_params = array(
                            'project_id' => $aris_id[0],
                            'DocumentID' => $aris_id[1],
                            'DATE_DUE' => $payment['DATE_DUE'],
                            'official_payment_id' => '0'
                        );
                        $this->db->where('DATE_PAID > "2021-08-11 00:00:00"');
                        $this->db->where('deleted_at is null');
                        $this->db->where('AmountPaid>0');
                        $dupe_aris_payments = $this->M_aris_payment->where($ap_params)->get_all();

                        if ($amount_paid == $current_due + $penalty_amount) {
                            $payment_application = '0';
                        } elseif ($amount_paid > $current_due + $penalty_amount) {
                            $payment_application = '2';
                        } elseif ($amount_paid < $current_due + $penalty_amount) {
                            $payment_application = '4';
                        }

                        if ($period_id > 2) {


                            if (count($dupe_aris_payments) > 1) {

                                foreach ($dupe_aris_payments as $key => $dupe_payment) {
                                    $penalty_amounts[] = floatval($dupe_payment['PENALTY']);
                                    $amount_paids[] = floatval($dupe_payment['AmountPaid']);
                                    $rebates[] = $dupe_payment['OR_NO'][0] == 'R' ? floatval($dupe_payment['AmountPaid']) : 0.00;
                                }

                                $penalty_amount = number_format(array_sum($penalty_amounts), 2, '.', '');
                                $is_waived = $penalty_amount ? '0' : '1';
                                $amount_paid_i = array_sum($amount_paids);
                                $amount_paid_i = number_format($amount_paid_i, 2, '.', '');
                                $amount_paid = $amount_paid_i;
                                $cash_amount = $amount_paid_i;
                                $grand_total = $amount_paid_i;
                                $has_rebate = count($dupe_aris_payments) - 1;
                                $rebate = number_format(array_sum($rebates), 2, '.', '');


                                if ($amount_paid == $current_due + $penalty_amount) {
                                    $payment_application = '0';
                                } elseif ($amount_paid > $current_due + $penalty_amount) {
                                    $payment_application = '2';
                                } elseif ($amount_paid < $current_due + $penalty_amount) {
                                    $payment_application = '5';
                                }
                            }
                        } else {
                            $payment_application = '4';
                        }


                        switch ($or_number[0]) {
                            case 'C':
                                $receipt_type = '1';
                                break;
                            case 'A':
                                $receipt_type = '2';
                                break;
                            case 'P':
                                $receipt_type = '3';
                                break;
                            case 'O':
                                $receipt_type = '4';
                                break;
                            default:
                                $receipt_type = '5';
                        }

                        $transaction_payment = $this->M_Transaction_payment->where(array('transaction_id' => $id, 'due_date' => $payment['DATE_DUE']))->get();
                        // vdebug($transaction_payment);
                        $payment_info = array(
                            'transaction_id' => $id,
                            'info' => array(
                                'period_count' => '1',
                                'period_id' => $period_id,
                                'payment_date' => $payment_date,
                                'or_date' => $or_date,
                                'is_waived' => $is_waived,
                                'penalty_amount' => $penalty_amount,
                                'rebate_amount' => $rebate,
                                'amount_paid' => $amount_paid,
                                'payment_type_id' => '1',
                                'post_dated_check_id' => '0',
                                'rc_bank_name' => '',
                                'rc_check_number' => '0',
                                'cash_amount' => $cash_amount,
                                'check_deposit_amount' => '0',
                                'receipt_type' => $receipt_type,
                                'OR_number' => $or_number,
                                'adhoc_payment' => '0',
                                'grand_total' => $grand_total,
                                'remarks' => $remarks,
                            ),
                            'transaction_payment_id' => $transaction_payment['id'],
                            'payment_application' => $payment_application
                        );
                        // vdebug($payment_info);
                        $payment_status = $this->insert_payment($payment_info, 1);
                        if ($payment_status['status'] == 0) {
                            $response['status'] = 0;
                            $response['message'] = 'Error syncing payments! Check the aris_payment for ' . date('Y-m-d', strtotime($payment['DATE_DUE'])) . ', OR No. ' . $or_number . 'Transaction: ' . $id;
                            break;
                        } else {
                            $latest_payment = $this->M_Transaction_official_payment->where(array('transaction_id' => $id))->order_by('id', 'desc')->get();
                            $this->M_aris_payment->update(array('official_payment_id' => $latest_payment['id']), $payment['id']);
                            $response['status'] = 1;
                            $response['message'] = 'ARIS Collections successfully synced!';
                        }
                        // $this->M_aris_payment->update(array('official_payment_id' => $transaction_payment['id']), $payment['id']);
                        // if($payment['id']==110967){
                        //     vdebug($payment_info);
                        // }

                        // if()
                    }
                }
            } else {
                $response['status'] = 0;
                $response['message'] = 'No new ARIS Payments Found!';
            }
            return json_encode($response);
        } else {
            show_403();
        }
    }

    public function sync_old_aris($project_id = 0)
    {
        $except = [];
        $project_id ?: show_error('Please provide project_id');
        $table = [];
        $this->db->where('project_id', $project_id);
        // $this->db->where('DocumentID > 179');
        $this->db->order_by('DocumentID', 'ASC');
        $documents = $this->M_aris_document->get_all();

        foreach ($documents as $key => $document) {
            if (in_array($document['DocumentID'], $except)) {
                continue;
            }
            $response = $this->sync_aris_collections($project_id, $document['DocumentID'], 1);
            array_push($table, $response);
        }
        echo json_encode($table);
        exit();
    }
}
