<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_official_payment_model extends MY_Model {

	public $table = 'transaction_official_payments'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
		'entry_id',
		'ar_number',
		'or_number',
		'or_date',
		'receipt_type',
		'amount_paid',
		'period_id',
		'principal_amount',
		'interest_amount',
		'penalty_amount',
		'rebate_amount',
		'adhoc_payment',
		'grand_total',
		'payment_type_id',
		'post_dated_check_id',
		'transaction_payment_id',
		'payment_application',
		'remarks',
		'payment_date',
		'period_count',
		'payment_status',
		'rc_bank_name',
		'rc_check_number',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['transaction'] = array('foreign_model'=>'transaction/transaction_model','foreign_table'=>'transactions','foreign_key'=>'id','local_key'=>'transaction_id');

		$this->has_one['payment'] = array('foreign_model'=>'transaction/Transaction_payment_model','foreign_table'=>'transaction_payments','foreign_key'=>'id','local_key'=>'transaction_payment_id');
		
		// $this->has_many['commissions'] = array('foreign_model'=>'Transaction_payment_model','foreign_table'=>'transaction_payments','foreign_key'=>'id','local_key'=>'transaction_id');
	}

	function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

	public function total_payment_per_period($transaction_id, $period_id) {
		$this->db->select_sum("amount_paid");
		$this->db->from("transaction_official_payments");
		$this->db->where("transaction_id", $transaction_id);
		$this->db->where("period_id", $period_id);
		$this->db->where("deleted_at IS NULL");
		$query = $this->db->get();

		$result = $query->result_array();
		
		return $result[0]['amount_paid'] ? $result[0]['amount_paid'] : 0;
	}

	public function total_payment($transaction_id) {
		$this->db->select_sum("amount_paid");
		$this->db->from("transaction_official_payments");
		$this->db->where("transaction_id", $transaction_id);
		$this->db->where("deleted_at IS NULL");
		$query = $this->db->get();

		$result = $query->result_array();

		return $result[0]['amount_paid'] ? $result[0]['amount_paid'] : 0;
	}

	public function get_or_nos($transaction_id){
		$result = '';
		$this->db->select('or_number');
		$this->db->where('deleted_at is null');
		$this->db->where('transaction_id', $transaction_id);
		$query = $this->db->get('transaction_official_payments')->result_array();
		if($query){
			$query = array_column($query, 'or_number');
			$result = implode("','", $query);
			$result = str_replace('-','',$result);
		}
		return $result;
	}

	public function collections_dashboard($params = [], $count = false)
	{
		// $this->db->select_sum('grand_total');
		$this->db->select_sum('principal_amount');

		$this->db->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left');

		if( ! empty($params['project_id']))
		{
			$this->db->where('transactions.project_id', $params['project_id']);
		}

		if( ! empty($params['payment_type_id']))
		{
			$this->db->where('transaction_official_payments.payment_type_id', $params['payment_type_id']);
		}

		if( ! empty($params['date']) )
		{
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m-%d") = ', $params['date']);
		}

		if (!empty($params['year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") = ', $params['year_month']);
		}

		if (!empty($params['prev_year_month'])) {
			$this->db->where('DATE_FORMAT(transaction_official_payments.payment_date, "%Y-%m") <= ', $params['prev_year_month']);
		}

		if (!empty($params['year_month_range'])) {
			$this->db->where("DATE_FORMAT(transaction_official_payments.payment_date, '%Y-%m') between '" .  $params['year_month_range'][0] . "' AND '" . $params['year_month_range'][1] . "'");
		}

		$this->db->where('transaction_official_payments.deleted_at IS NULL');
		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transactions.general_status !=', 4);


		return $this->db->get('transaction_official_payments')->row_array();
	}

	public function get_transactions($company_id=0,$project_id=0,$buyer=0,$seller=0,$trans_ref=0){
		$results = [];
		$this->db->reset_query();
		$this->db->select('transactions.id as id',false);
		$this->db->join('projects', 'transactions.project_id = projects.id');
		if($company_id){
			$this->db->where('projects.company_id',$company_id);
		}
		if($project_id){
			$this->db->where('transactions.project_id',$project_id);
		}
		if($buyer){
			$this->db->where('transactions.buyer_id',$buyer);
		}
		if($seller){
			$this->db->where('transactions.seller_id',$seller);
		}
		if($trans_ref){
			$this->db->where("transactions.reference like '%$trans_ref%'");
		}
		$query=$this->db->get('transactions')->result_array();
		foreach($query as $item){
			$results[] = $item['id'];
		}
		return $results;
	}

}
