<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Total Monthly Collections
            </h3>
        </div>
    </div>
</div>

<?php 
    $total_s = 0;
?>

<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="propertyContent">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <td>Project Name</td>
                                <?php if ($months): ?>
                                    <?php foreach ($months as $key => $month) { //if(empty($key)){ continue; } ?>
                                         <td><?php echo $month; ?></td>
                                    <?php } ?>
                                <?php endif ?>
                                <td>Total</td>
                            </tr>
                        </thead>
                        <tbody>

                        <?php if ($projects): ?>
                            <?php foreach ($projects as $key => $project) { $total_c = 0; $total_collected = 0; ?>
                                <tr>
                                    <td><?=$key; ?></td>

                                    <?php if ($months): ?>
                                        <?php foreach ($months as $key => $month) { //if(empty($key)){ continue; } ?>

                                            <?php $total_collected += $project[$month]['amount_paid']; ?>

                                            <td> <?php echo money_php($project[$month]['amount_paid']); ?></td>

                                        <?php } ?>

                                    <?php endif ?>

                                    <td> <?=money_php($total_collected); ?></td>

                                </tr>
                            <?php } ?>
                        <?php endif ?>

                        </tbody>

                     
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
