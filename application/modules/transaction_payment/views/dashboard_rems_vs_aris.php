<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Collections Dashboard REMS vs ARIS
            </h3>
        </div>
    </div>
</div>

<?php 
    $total_s = 0;
?>

<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="propertyContent">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <!-- <button class="btn btn-success" onclick="exportTableToExcel('rems-payments', 'rems-payments')">Export Table Data To Excel File</button> -->
                    <table class="table table-striped" id="rems-payments">
                        <thead class="thead-dark">
                            <tr>
                                <td>REMS Project Name</td>
                                <td>Total</td>
                                
                            </tr>
                        </thead>
                        <tbody>

                        <?php if ($projects): ?>
                            <?php foreach ($projects as $key => $project) { $project_id = get_value_field($key,'projects','id','name'); //if(empty($project['total'])){continue;}?>
                                <tr>
                                    <td><?=$key; ?></td>
                                    <td>
                                        <?php   //echo $project['total']; 
                                                $total_s += $project['total']; 
                                        ?>
                                        <!-- <div class="progress progress-lg"> -->
                                            <!-- <div class="progress-bar progress-bar-striped progress-lg" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"> -->
                                                <b>
                                                    <a href="<?=base_url();?>transaction_payment/dashboard_per_project/<?=$project_id;?>" target="_BLANK">
                                                        <?=money_php($project['total']);?>
                                                    </a>  
                                                </b>
                                            <!-- </div> -->
                                        <!-- </div>    -->
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php endif ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td><?php echo money_php($total_s); ?></td>
                            </tr>
                        </tfoot>
                    </table>

                </div>

                <div class="col-md-6 col-sm-6">

                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <td>ARIS Project Name</td>
                                <td>Total</td>
                                <td>Difference</td>
                                <td>Percentage</td>
                                <td>&nbsp;</td>
                            </tr>
                        </thead>
                        <tbody>

                        <?php if ($aris_projects): ?>
                            <?php foreach ($aris_projects as $key => $project) { //if(empty($project['total'])){continue;}?>
                                <tr>
                                    <td><?=$key; ?></td>
                                    <td>
                                        <?php   //echo $project['total']; 
                                                $total_ss += $project['total']; 
                                        ?>
                                        <!-- <div class="progress progress-lg"> -->
                                            <!-- <div class="progress-bar progress-bar-striped progress-lg" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"> -->
                                                <b><?=money_php($project['total']);?></b>
                                            <!-- </div> -->
                                        <!-- </div>    -->
                                    </td>
                                    <td><?=money_php($diff = $project['total'] - $projects[$key]['total']); ?></td>
                                    <td><?php echo $dd = round(get_percentage($project['total'],$projects[$key]['total']) * 100,2)."%"; ?></td>
                                    <td><?php if($dd >= 100){ echo '<i class="flaticon2-correct"></i>'; } else { echo '<i class="flaticon2-cross"></i>'; } ?></td>
                                </tr>
                            <?php } ?>
                        <?php endif ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td><?php echo money_php($total_ss); ?></td>
                                <td><?php echo money_php($total_ss - $total_s); ?></td>
                                <td><?php echo $de = round(get_percentage($total_ss,$total_s) * 100,2)."%"; ?></td>
                                <td><?php if($de >= 100){ echo '<i class="flaticon2-correct"></i>'; } else { echo '<i class="flaticon2-cross"></i>'; } ?></td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>



<style type="text/css">
    .flaticon2-correct{
        color: green;
    }
     .flaticon2-cross{
        color: red;
    }
</style>