<?php

    // $remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
    // $amount_paid = isset($info['amount_paid']) && $info['amount_paid'] ? $info['amount_paid'] : 0;

    // $rc_bank_name = isset($info['rc_bank_name']) && $info['rc_bank_name'] ? $info['rc_bank_name'] : "";
    // $rc_check_number = isset($info['rc_check_number']) && $info['rc_check_number'] ? $info['rc_check_number'] : 0;

    // $check_deposit_amount = isset($info['check_deposit_amount']) && $info['check_deposit_amount'] ? $info['check_deposit_amount'] : 0;
    // $cash_amount = isset($info['cash_amount']) && $info['cash_amount'] ? $info['cash_amount'] : 0;
    // $adhoc_payment = isset($info['adhoc_payment']) && $info['adhoc_payment'] ? $info['adhoc_payment'] : 0;
    // $grand_total = isset($info['grand_total']) && $info['grand_total'] ? $info['grand_total'] : 0;

    // $payment_date = isset($info['payment_date']) && $info['payment_date'] ? $info['payment_date'] : '';
    // $or_date = isset($info['or_date']) && $info['or_date'] ? $info['or_date'] : '';
    // $OR_number = isset($info['OR_number']) && $info['OR_number'] ? $info['OR_number'] : '';
    // $AR_number = isset($info['AR_number']) && $info['AR_number'] ? $info['AR_number'] : '';

    // $transaction = isset($transaction) && $transaction ? $transaction : array();
    // $reference = isset($transaction['reference']) && $transaction['reference'] ? $transaction['reference'] : '';

    $buyer_id = isset($transaction['buyer_id']) && $transaction['buyer_id'] ? $transaction['buyer_id'] : '';

    $buyer = get_person($buyer_id, 'buyers');
    $buyer_name = get_fname($buyer);

    $property_id = isset($transaction['property_id']) && $transaction['property_id'] ? $transaction['property_id'] : '';
    $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : '';

    $this->transaction_library->initiate($transaction_id);
    // $period_id = $this->transaction_library->current_period(1);
    // $amount_due = $this->transaction_library->total_amount_due();
    // $principal_due = $this->transaction_library->total_amount_due(1);
    // $interest_due = $this->transaction_library->total_amount_due(2);
    // $overdue = $this->transaction_library->payment_status(1);
    // $dp_remaining_balance = $this->transaction_library->get_remaining_balance(2, 2) > 0 ? $this->transaction_library->get_remaining_balance(2, 2) : 0;
    // $lo_remaining_balance = $this->transaction_library->get_remaining_balance(2, 3) > 0 ? $this->transaction_library->get_remaining_balance(2, 3) : 0;

    // $penalty_due = round($this->transaction_library->get_penalty(), 2);

    // $data['payments'] = $this->transaction_library->get_overdue_table(date('Y-m-d'));

    // $is_waived = 1;
    // $dp_amount = isset($info['billings'][1]['period_amount']) && $info['billings'][1]['period_amount'] ? $info['billings'][1]['period_amount'] : 0;
    // $penalty_type = isset($info['penalty_type']) && $info['penalty_type'] ? $info['penalty_type'] : 0;

    // $dp_remaining_interest_due = ($period_id == 2) ? $interest_due : 0;
    // $lv_remaining_interest_due = ($period_id == 3) ? $interest_due : 0;
?>


<div class="row">
    <div class="col-md-5">
        <input type="hidden" id="transaction_id" name="transaction_id" value="<?php echo $transaction_id; ?>">
        <input type="hidden" id="dp_amount" value="<?php echo $dp_amount; ?>">
        <input type="hidden" id="period_count" name="info[period_count]" value="">
        <input type="hidden" id="penalty_type" value="<?=$penalty_type?>">
        <input type="hidden" id="transaction_payment_id" name="transaction_payment_id">
        <input type="hidden" id="maximum_value" name="maximum_value" value='<?= $maximum_value ?>'>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Buyer Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" data-module="buyers" id="buyer_id">
                        <?php if ($buyer_id): ?>
                        <option value="<?=$buyer_id;?>" selected><?php echo $buyer_name; ?></option>
                        <?php endif?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Property <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="property_id">
                        <?php if ($property_id): ?>
                        <option value="<?=$property_id;?> " selected><?php echo $property_name; ?></option>
                        <?php endif?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date of Payment <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="payment_date"
                            placeholder="Date of Payment" name="payment_date"
                            value="<?php echo date("Y-m-d"); ?>" readonly>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Receipt Date</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="or_date" placeholder="OR Date"
                            name="receipt_date" value="<?php echo date("Y-m-d"); ?>" readonly>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Waive Penalty</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <?php echo form_dropdown('is_waived', Dropdown::get_static('bool'), set_value('is_waived', @$is_waived), 'class="form-control" id="is_waived"'); ?>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Penalty Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="penalty_amount" placeholder="Amount Paid"
                            name="penalty_amount"
                            value="<?php echo set_value('penalty_amount"', $penalty_due); ?>">
                    </div>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Amount Paid <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="amount_paid" placeholder="Amount Paid"
                            name="amount_paid"
                            value="">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Payment Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('payment_type_id', Dropdown::get_static('payment_types'), set_value('payment_type_id', @$payment_type_id), 'class="form-control" id="payment_type_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Checks </label>
                    <select class="form-control" name="info[post_dated_check_id]" id="post_dated_check_id">
                        <option value="0">Select Checks</option>
                        <?php if ($checks): ?>
                        <?php foreach ($checks as $key => $check) {?>
                        <option value="<?php echo $check['id']; ?>">
                            <?php echo $check['unique_number']; ?>
                        </option>
                        <?php }?>
                        <?php endif?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row hide" data-field-type="regular_cheque">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Bank Name</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="rc_bank_name" placeholder="Bank Name"
                            name="rc_bank_name"
                            value="<?php echo set_value('rc_bank_name"', $rc_bank_name); ?>">
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Check Number</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="rc_check_number"
                            placeholder="Check Number" name="rc_check_number"
                            value="<?php echo set_value('rc_check_number"', $rc_check_number); ?>">
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Cash Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input readonly type="text" class="form-control" id="cash_amount" placeholder="Cash Amount"
                            name="cash_amount"
                            value="<?php echo set_value('cash_amount"', $cash_amount); ?>">
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Check / Deposit Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input readonly type="text" class="form-control" id="check_deposit_amount" value='0.00'
                            placeholder="Check / Deposit Amt" name="check_deposit_amount"
                            value="<?php echo set_value('check_deposit_amount"', $check_deposit_amount); ?>">
                    </div>
                </div>
            </div>

        </div>



        <div class="row">
            <!--  <div class="col-sm-6">
                <div class="form-group">
                    <label>AR # <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control popField" id="AR_number" placeholder="AR Number" name="info[AR_number]" value="<?php echo set_value('info[AR_number]"', $AR_number); ?>">
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Receipt Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="receipt_type" id="receipt_type">
                        <option value="0">Select Type</option>
                        <option value="1">Collection Receipt</option>
                        <option value="2">Acknowledgement Receipt</option>
                        <option value="3">Provisionary Receipt</option>
                        <option value="4">Official Receipt</option>
                        <option value="5">Manual Receipt</option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Receipt Number</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="OR_number" placeholder="OR Number"
                            name="OR_number" value="<?php echo set_value('OR_number"', $OR_number); ?>">
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row mt-3">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Remarks <span class="kt-font-danger"></span></label>
                    <textarea class="form-control" id="remarks"
                        name="remarks"></textarea>                      
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <?php echo $this->load->view('adhoc_fee/_adhoc_fee_information'); ?>
        <?php echo $this->load->view('adhoc_fee/_adhoc_payment_logs'); ?>

    <div class="col-md-7">
        <button class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u mx-3"
        data-ktwizard-type="action-submit" id="submit_btn">
        Submit
    </button>
    </div>
</div>