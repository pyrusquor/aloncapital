<?php
$adhoc_fee_items = isset($adhoc_fee) && $adhoc_fee ? $adhoc_fee : '';
$fees = isset($info['adhoc_fee_schedule']) && $info['adhoc_fee_schedule'] ? $info['adhoc_fee_schedule'] : '';
?>
<div class="row">
	<div class="col-sm-12">
		<!-- Fee Information -->
		<div class="kt-portlet">
			<!-- <div class="accordion accordion-solid accordion-toggle-svg" id="accord_fee_information"> -->
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<?php if ($fees) : ?>

								<table class="table table-hover table-borderless" id="billing_table">
									<thead>
										<tr>
											<th class='align-middle' colspan="1">Monthly Due</th>
											<th colspan="1">
												<input type="text" class="form-control" placeholder="Monthly Due" value="<?= money_php($maximum_value) ?>" readonly>
											</th>
										</tr>
										<tr>
											<th></th>
											<th>Amount Due</th>
											<th>Amount Paid</th>
											<th>Remaining Balance</th>
										</tr>
									</thead>
									<tbody id="billing_tbody">
										<?php
										foreach ($adhoc_fee_items as $key => $value) :
											$paid_amount = 0;
											$fee = get_person($value['fee_id'], 'fees');
											$name = get_name($fee);
											if($value['payments']){
												foreach($value['payments'] as $key => $payment){
													$paid_amount += $payment['amount_paid'];
												}
											}
											else{
												$paid_amount = 0;
											}
										?>
											<tr>
												<td class='align-middle'><?= $name ?></td>
												<td>
													<input type="text" class="form-control" placeholder="Monthly Due" value="<?= money_php($value['amount']) ?>" readonly>
												</td>
												<td>
													<input type="text" class="form-control" placeholder="Monthly Due" value="<?=money_php($paid_amount) ?>" readonly>
												</td>
												<td>
													<input type="text" class="form-control" placeholder="Monthly Due" value="<?= money_php($value['amount'] - $paid_amount) ?>" readonly>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<!-- </div> -->
			</div>
			<!-- </div> -->
		</div>
	</div>
</div>