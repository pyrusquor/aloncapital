<?php

$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
$amount_paid = isset($info['amount_paid']) && $info['amount_paid'] ? $info['amount_paid'] : 0;

$rc_bank_name = isset($info['rc_bank_name']) && $info['rc_bank_name'] ? $info['rc_bank_name'] : "";
$rc_check_number = isset($info['rc_check_number']) && $info['rc_check_number'] ? $info['rc_check_number'] : 0;

$check_deposit_amount = isset($info['check_deposit_amount']) && $info['check_deposit_amount'] ? $info['check_deposit_amount'] : 0;
$cash_amount = isset($info['cash_amount']) && $info['cash_amount'] ? $info['cash_amount'] : 0;
$adhoc_payment = isset($info['adhoc_payment']) && $info['adhoc_payment'] ? $info['adhoc_payment'] : 0;
$grand_total = isset($info['grand_total']) && $info['grand_total'] ? $info['grand_total'] : 0;
$rebate_amount = isset($info['rebate_amount']) && $info['rebate_amount'] ? $info['rebate_amount'] : 0;

$payment_date = isset($info['payment_date']) && $info['payment_date'] ? $info['payment_date'] : '';
$or_date = isset($info['or_date']) && $info['or_date'] ? $info['or_date'] : '';
$OR_number = isset($info['OR_number']) && $info['OR_number'] ? $info['OR_number'] : '';
$AR_number = isset($info['AR_number']) && $info['AR_number'] ? $info['AR_number'] : '';

$transaction = isset($transaction) && $transaction ? $transaction : array();
$reference = isset($transaction['reference']) && $transaction['reference'] ? $transaction['reference'] : '';

$buyer_id = isset($transaction['buyer_id']) && $transaction['buyer_id'] ? $transaction['buyer_id'] : '';

$buyer = get_person($buyer_id, 'buyers');
$buyer_name = get_fname($buyer);

$property_id = isset($transaction['property_id']) && $transaction['property_id'] ? $transaction['property_id'] : '';
$property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : '';

$this->transaction_library->initiate($transaction_id);
$period_id = $this->transaction_library->current_period(1);
$amount_due = $this->transaction_library->total_amount_due();
$principal_due = $this->transaction_library->total_amount_due(1);
$interest_due = $this->transaction_library->total_amount_due(2);
$overdue = $this->transaction_library->payment_status(1);
$dp_remaining_balance = $this->transaction_library->get_remaining_balance(2, 2) > 0 ? $this->transaction_library->get_remaining_balance(2, 2) : 0;
$lo_remaining_balance = $this->transaction_library->get_remaining_balance(2, 3) > 0 ? $this->transaction_library->get_remaining_balance(2, 3) : 0;
    

$monthly_ra = $this->transaction_library->monthly_ammort(1);
$monthly_dp = $this->transaction_library->monthly_ammort(2);
$monthly_loan = $this->transaction_library->monthly_ammort(3);

$penalty_due = round($this->transaction_library->get_penalty(), 2);

$data['payments'] = $this->transaction_library->get_overdue_table(date('Y-m-d'));

$is_waived = 1;
$dp_amount = isset($info['billings'][1]['period_amount']) && $info['billings'][1]['period_amount'] ? $info['billings'][1]['period_amount'] : 0;
$penalty_type = isset($info['penalty_type']) && $info['penalty_type'] ? $info['penalty_type'] : 0;

$dp_remaining_interest_due = ($period_id == 2) ? $interest_due : 0;
$lv_remaining_interest_due = ($period_id == 3) ? $interest_due : 0;
?>

<div class="row">
    <div class="col-md-5">
        <input type="hidden" id="transaction_id" name="transaction_id" value="<?php echo $transaction_id; ?>">
        <input type="hidden" id="dp_amount" value="<?php echo $dp_amount; ?>">
        <input type="hidden" id="period_count" name="info[period_count]" value="">
        <input type="hidden" id="penalty_type" value="<?= $penalty_type ?>">
        <input type="hidden" id="transaction_payment_id" name="transaction_payment_id">

        <input type="hidden" id="monthly_ra" value="<?=$monthly_ra;?>">
        <input type="hidden" id="monthly_dp" value="<?=round($monthly_dp,2);?>">
        <input type="hidden" id="monthly_loan" value="<?=round($monthly_loan,2);?>">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Buyer Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" data-module="buyers" id="buyer_id">
                        <?php if ($buyer_id) : ?>
                            <option value="<?= $buyer_id; ?>" selected><?php echo $buyer_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Reference <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="reference">
                        <?php if ($reference) : ?>
                            <option selected><?php echo $reference; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Property <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="property_id">
                        <?php if ($property_id) : ?>
                            <option value="<?= $property_id; ?> " selected><?php echo $property_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Period Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('info[period_id]', Dropdown::get_static('period_names'), set_value('info[period_id]', @$period_id), 'class="form-control" id="period_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date of Payment <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="payment_date" placeholder="Date of Payment" name="info[payment_date]" value="<?php echo date("Y-m-d"); ?>" readonly>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Receipt Date</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="or_date" placeholder="OR Date" name="info[or_date]" value="<?php echo date("Y-m-d"); ?>" readonly>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Waive Penalty</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <?php echo form_dropdown('info[is_waived]', Dropdown::get_static('bool'), set_value('info[is_waived]', @$is_waived), 'class="form-control" id="is_waived"'); ?>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Penalty Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="penalty_amount" placeholder="Amount Paid" name="info[penalty_amount]" value="<?php echo set_value('info[penalty_amount]"', $penalty_due); ?>">
                    </div>
                </div>
            </div>


        </div>

        <div class="row">


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Payment Application <span class="kt-font-danger">*</span></label>
                    <div class="">
                        <select class="form-control" name="payment_application" id="payment_application">
                            <option value="0">Regular Payment (Exact amount only, with interest or penalty)</option>
                            <option value="2">Balloon Payment (Interest and apply all surplus to Principal)</option>
                            <option value="1">Advance Payment / Multiple Months (Principal & Interest)</option>
                            <option value="3">Continuous Payment / Multiple Months (Apply all surplus to Principal)</option>
                            <option value="4">Partial Payment</option>
                            <option value="5">Special Payment</option>
                            <!-- <option value="9">TEST</option> -->
                            <!-- <option value="3">Fully Paid (Pay all remaining principal)</option> -->
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Amount Paid <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="amount_paid" placeholder="Amount Paid" name="info[amount_paid]" value="<?php echo set_value('info[amount_paid]"', $amount_paid); ?>" data-total-amount="<?php echo $amount_paid; ?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Payment Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('info[payment_type_id]', Dropdown::get_static('payment_types'), set_value('info[payment_type_id]', @$payment_type_id), 'class="form-control" id="payment_type_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Checks </label>
                    <select class="form-control" name="info[post_dated_check_id]" id="post_dated_check_id">
                        <option value="0">Select Checks</option>
                        <?php if ($checks) : ?>
                            <?php foreach ($checks as $key => $check) { ?>
                                <option value="<?php echo $check['id']; ?>">
                                    <?php echo $check['unique_number']; ?>
                                </option>
                            <?php } ?>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row hide" data-field-type="regular_cheque">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Bank Name</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="rc_bank_name" placeholder="Bank Name" name="info[rc_bank_name]" value="<?php echo set_value('info[rc_bank_name]"', $rc_bank_name); ?>">
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Check Number</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="rc_check_number" placeholder="Check Number" name="info[rc_check_number]" value="<?php echo set_value('info[rc_check_number]"', $rc_check_number); ?>">
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Cash Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input readonly type="text" class="form-control" id="cash_amount" placeholder="Cash Amount" name="info[cash_amount]" value="<?php echo set_value('info[cash_amount]"', $cash_amount); ?>">
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Check / Deposit Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input readonly type="text" class="form-control" id="check_deposit_amount" placeholder="Check / Deposit Amt" name="info[check_deposit_amount]" value="<?php echo set_value('info[check_deposit_amount]"', $check_deposit_amount); ?>">
                    </div>
                </div>
            </div>

        </div>



        <div class="row">
            <!--  <div class="col-sm-6">
                <div class="form-group">
                    <label>AR # <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control popField" id="AR_number" placeholder="AR Number" name="info[AR_number]" value="<?php echo set_value('info[AR_number]"', $AR_number); ?>">
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Receipt Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="info[receipt_type]" id="receipt_type">
                        <option value="0">Select Type</option>
                        <option value="1">Collection Receipt</option>
                        <option value="2">Acknowledgement Receipt</option>
                        <option value="3">Provisionary Receipt</option>
                        <option value="4">Official Receipt</option>
                        <option value="5">Manual Receipt</option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Receipt Number</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="OR_number" placeholder="OR Number" name="info[OR_number]" value="<?php echo set_value('info[OR_number]"', $OR_number); ?>">
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-6">
                <label>Adhoc Fee Payment </label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input type="text" class="form-control" id="adhoc_payment" placeholder="Amount" name="info[adhoc_payment]" value="<?php echo set_value('info[adhoc_payment]"', $adhoc_payment); ?>">
                </div>
            </div>
            <div class="col-sm-6">
                <label>Grand Total </label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input readonly type="text" class="form-control" id="grand_total" placeholder="Amount" name="info[grand_total]" value="<?php echo set_value('info[grand_total]"', $grand_total); ?>">
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-12">
                <label>Rebates</label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input type="text" class="form-control" id="rebate_amount" placeholder="Amount" name="info[rebate_amount]" value="<?php echo set_value('info[rebate_amount]"', $rebate_amount); ?>">
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Remarks <span class="kt-font-danger"></span></label>
                    <textarea class="form-control" id="remarks" name="info[remarks]"></textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <button class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit-arnote">
                    Save AR Note
                </button>
            </div>
        </div>

    </div>

    <div class="col-md-7">

        <div class="row">
            <div class="col-sm-3">
                <a class="btn btn-info modal_view view_ar_note" href="javascript: void(0)" class="modal_view" data-url="ticketing/view/0/<?php echo $transaction_id; ?>/ar_notes">VIEW AR NOTES</a>

            </div>

            <div class="col-sm-5">
                <a class="btn btn-success" target="_BLANK" href="<?php echo base_url(); ?>transaction/view/<?php echo $transaction_id; ?>">VIEW MORTGAGE AND
                    COLLECTIONS</a>

            </div>

            <div class="col-sm-3">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#collection_detail">
                    COLLECTIONS
                </button>
                </a>
            </div>
        </div>

        <div id="detailed_due">
            <?php echo $this->load->view('transaction/view/_detailed_due_information', $data); ?>
        </div>

        <?php echo $this->load->view('transaction/view/_account_information'); ?>

        <?php echo $this->load->view('transaction/view/_billing_information'); ?>

        <?php echo $this->load->view('transaction/view/_account_remaining_balance'); ?>

        <?php echo $this->load->view('transaction/view/_adhoc_fee_information'); ?>
    </div>
</div>

<div class="modal fade" id="collection_detail" tabindex="-1" role="dialog" aria-labelledby="collection_detail_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="collection_detail_label">Collections</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-head-bg-brand table-striped table-hover table-bordered table-condensed">
                    <thead>
                        <tr class="text-center">
                            <th>Period</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Downpayment</td>
                            <td><?= money_php($dp_remaining_balance + $dp_remaining_interest_due) ?></td>
                        </tr>
                        <tr>
                            <td>Loan</td>
                            <td><?= money_php($lo_remaining_balance + $lv_remaining_interest_due) ?></td>
                        </tr>
                    </tbody>
                </table>
                <?php if ($data['payments']) : ?>
                    <? //=$data['payments'];
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>