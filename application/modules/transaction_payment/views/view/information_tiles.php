<div class=row>
    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--solid-brand kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-bill-wave"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        DAILY COLLECTIONS <small><?php echo date('F j, Y'); ?></small>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <span class="kt-widget20__number display-4">
                    <div id="daily_collection"></div>
                </span>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--solid-success kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-bill-wave"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        WEEKLY COLLECTIONS <small id="daterange_weekly"></small>
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-outline-hover-light btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="text-light flaticon-more-1"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-149px, 33px, 0px);">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="javascript:void(0)" id="this_week" class="kt-nav__link">
                                            <span class="kt-nav__link-text">This Week</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="javascript:void(0)" id="prev_week" class="kt-nav__link">
                                            <span class="kt-nav__link-text">Previous Week</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
            <span class="kt-widget20__number display-4">
                <div id="weekly_collection"></div>
            </span>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--solid-danger kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-bill-wave kt-font-light"></i>
                    </span>
                    <div class="kt-portlet__head-title kt-font-light">
                        MONTHLY COLLECTIONS <small id="daterange_monthly"></small>
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-outline-hover-light btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="text-light flaticon-more-1"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-149px, 33px, 0px);">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="javascript:void(0)" id="this_month" class="kt-nav__link">
                                            <span class="kt-nav__link-text">This Month</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="javascript:void(0)" id="prev_month" class="kt-nav__link">
                                            <span class="kt-nav__link-text">Previous Month</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <span class="kt-widget20__number display-4 kt-font-light">
                <div id="monthly_collection"></div>
                </span>
            </div>
        </div>
    </div>

</div>