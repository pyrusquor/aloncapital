<?php
    $id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
    
?>


<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Transactions</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('transaction/form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<a href="<?php echo site_url('transaction'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="view" data-ktwizard-state="step-first">
	<div class="kt-portlet">
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-grid__item">

				<!--begin: Form Wizard Nav -->
				<div class="kt-wizard-v3__nav">
					<div class="kt-wizard-v3__nav-items">
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									General Information
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Mortgage Schedule
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Collections
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Post Dated Checks
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>

						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Commission Schedule
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>

						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									 Printable Forms
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									 Tickets and Concerns								
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Graphs and Reports
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
					</div>
				</div>
				<!--end: Form Wizard Nav -->
			</div>
		</div>
	</div>

	<div class="kt-grid__item kt-grid__item--fluid --kt-wizard-v3__wrapper">

		<!--begin: Form Wizard Step 1-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
			<?php $this->load->view('view/_general_information'); ?>
		</div>
		<!--end: Form Wizard Step 1-->

		<!--begin: Form Wizard Step 2-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_mortgage_schedule_information'); ?>
		</div>
		<!--end: Form Wizard Step 2-->

		<!--begin: Form Wizard Step 3-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_collection_information'); ?>
		</div>
		<!--end: Form Wizard Step 3-->

		<!--begin: Form Wizard Step 4-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_post_dated_check_information'); ?>
		</div>
		<!--end: Form Wizard Step 4-->

		<!--begin: Form Wizard Step 5-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_commission_schedule_information'); ?>
		</div>
		<!--end: Form Wizard Step 5-->

		<!--begin: Form Wizard Step 6-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_document_information'); ?>
		</div>
		<!--end: Form Wizard Step 6-->

		<!--begin: Form Wizard Step 7-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_tickets_information'); ?>
		</div>
		<!--end: Form Wizard Step 7-->

		<!--begin: Form Wizard Step 8-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_graph_reports_information'); ?>
		</div>
		<!--end: Form Wizard Step 8-->

		<?php if ( FALSE ): ?>

			<!--begin: Form Actions -->
			<div class="kt-form__actions">
				<div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
					Previous
				</div>
				<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
					Submit
				</div>
				<div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
					Next Step
				</div>
			</div>
			<!--end: Form Actions -->
		<?php endif; ?>
	</div>
</div>