<?php
	$id = $r['id'];

	$transaction = $r['transaction'];
	$transaction_id = $transaction['id'];
	$reference = $transaction['reference'];

	$project_name = $transaction['project_id'];
	$property_name = $transaction['property_id'];

	$buyer_id = $transaction['buyer_id'];

	$buyer = $this->M_buyer->get($buyer_id);

	$buyer_name = get_fname($buyer);

	$payment_method = @$r['payment_type_id'];
	$period_type_id = @$r['period_id'];

	$project_id = $transaction['project_id'];
	$property_id = $transaction['property_id'];

	$payment_date = view_date($r['payment_date']);
	$amount_paid =  money_php($r['amount_paid']);
?>
<style type="text/css">
	html { margin:10px 15px 0 10px; font-family:Helvetica; }
	table { width:100%; }

	table#account-form tr td {
		width:26%;
		padding:4px;
		font-size:9pt;
	}
	table#account-form tr td:nth-child(2) { width:30%; }
	table#account-form tr td:first-child { width:15%; }

	table#account-settle tr td { padding:3px; font-size:9pt; }

	.account-info p, .account-info h3 { text-align: center; }
</style>

<html>
	<body>
		<?php for ($i=1; $i <= 2; $i++) { ?>
			<div id="<?php echo $i == 1? 'original' : 'duplicate'; ?>">
				<h3 style="color:red; text-align:right"><?php echo $i == 1? 'ORIGINAL' : 'DUPLICATE'; ?> COPY</h3>

				<div class="account-info">
			            <h3 style="margin-top:-5px">SACRED HEART OF JESUS DEVELOPMENT CORPORATION</h3>
			            	<p style="font-size:8pt; line-height:18px; margin-top:-15px">
				            	<span style="font-size:10pt">Punta Dulog Commercial Complex St. Joseph Avenue, Pueblo De Panay Township, Brgy. Lawaan, Roxas City</span><br/>
				            	(036) 6212 - 806 * 6215 - 844<br/>
				            	Fax No. 000 - 0000; Email Address: shjdc@gmail.com; Website: https://www.shjdc.com<br/>
				            	VAT Reg TIN #: <u>002 - 240 - 754 - 000</u> 
				            </p>
				        	<p style="font-size:8pt; line-height:18px; margin-top:-15px">&nbsp;</p>
				</div>

				<table id="account-form">
					<tr>
						<td style="text-align:right">Payment Date</td>
						<td style="border-bottom:1px solid #888888; font-weight:800"><?php echo $payment_date; ?></td>
						<td style="text-align:right" colspan="2">Automated Acknowledgement Receipt No:</td>
						<td style="border-bottom:1px solid #888888; font-weight:800"></td>
					</tr>
					<tr>
						<td colspan="5"></td>
					</tr>
					<tr>
						<td style="text-align:right">Receive From</td>
						<td style="border-bottom:1px solid #888888; font-weight:800" colspan="2"><?php echo strtoupper($buyer_name); ?></td>
						<td style="text-align:right;border-bottom: 1px solid #fff;">Amount</td>
						<td style="border:2px solid #7c7c7c; font-size:12pt; font-weight:800; text-align:right">Php <?php echo $amount_paid; ?></td>
					</tr>
					<tr>
						<td style="text-align:right">Amount</td>
						<td style="border-bottom:0.5px solid #888888; font-weight:800" colspan="4">
							*** <?php 
								/**
								 * @todo
								 * not working in localhost
								 */
								//$numberFormatter = new NumberFormatter("en", NumberFormatter::SPELLOUT);
								//echo strtoupper($numberFormatter->format($payment['payment_amount']));
							?> PESOS ***
						</td>
					</tr>
					<tr>
						<td style="text-align:right">In Payment For</td>
						<td style="border-bottom:1px solid #888888; font-weight:800" colspan="4"><?php echo $project_name ?>: <?php echo $property_name; ?></td>
					</tr>
					<tr>
						<td style="text-align:right" rowspan="2">Address</td>
						<td style="border-bottom:1px solid #888888; font-weight:800" colspan="2" rowspan="2"><?php echo !empty($address)? $address : 'Not Available'; ?></td>
						<td style="text-align:right">Mode of Payment:</td>
						<td style="border-bottom:1px solid #888888; font-weight:800"><?php echo $payment_method; ?></td>
					</tr>
					<tr>
						<td style="text-align:right">Type of Business:</td>
						<td style="border-bottom:1px solid #888888; font-weight:800">Not Applicable</td>
					</tr>
				</table>

				<table id="account-settle">
					<tr>
						<td style="width:50%; padding:0 10px">
							<div style="border:2px dotted #989898; padding:28px 16px;">
								<i>In settlement of the following:</i>
								<table>
									<tr>
										<td><b>Billing Invoice No.</b> <?php echo $r['ar_number']; ?></td>
										<td style="text-align:right"><b>Amount</b></td>
									</tr>
									<tr>
										<td style="text-indent:10px">VATable Sales</td>
										<td style="text-align:right">0.00</td>
									</tr>
									<tr>
										<td style="text-indent:10px">VATable Excempt Sales</td>
										<td style="text-align:right"><?php echo $amount_paid; ?></td>
									</tr>
									<tr>
										<td style="text-indent:10px">Zero-Related Sales</td>
										<td style="text-align:right">0.00</td>
									</tr>
									<tr>
										<td style="text-indent:10px">VAT amount</td>
										<td style="text-align:right">0.00</td>
									</tr>
									<tr>
										<td><b>Total Sales (Php)</b></td>
										<td style="text-align:right"><b><?php echo $amount_paid; ?></b></td>
									</tr>
								</table>
							</div>
						</td>
						<td style="width:50%">
							<div align="center">
								<h3>[VAT EXEMPT]</h3>
								<br/>
								<hr style="border: 0; border-bottom: 1px solid #888888; background: #999;"/>
								<p style="text-align:center;margin-top:-15px">
									<b style="font-size:10pt">Authorized Personnel</b><br/>
									<span style="letter-spacing:1px">Permit No.: 00000</span>
								</p>
							</div>
							<div style="height:70px; border:1px solid #8a8a8a; border-radius:2px; padding: 5px 10px;">
								<i style="color:#343434">Remarks</i>
							</div>
						</td>
					</tr>
				</table>

				<p style="font-style:italic; color:#7e7e7e; font-size:12px">NOTE: This is a computer-generated document and serves as your acknowledgement receipt.</p>
			</div>
				
			<?php if ($i == 1): ?>
				<hr style="border: 0; border-bottom: 3px dashed #ccc; margin-top:-25px; margin-bottom:-25px;">
			<?php endif ?>
		<?php } ?>
	</body>
</html>


