<?php if (!empty($records)) : ?>
<div class="row" id="kt-content">
    <?php foreach ($records as $r) : //if(!isset($r['seller'])){ continue ;} ?>
    <?php
                $id = $r['id'];

                $transaction = @$r['transaction'];
                $transaction_id = $transaction['id'];
                $reference = $transaction['reference'];

                $project_name = $transaction['project_id'];
                $property_name = $transaction['property_id'];

                $buyer_id = @$transaction['buyer_id'];
                $buyer = get_person($buyer_id, 'buyers');
                $buyer_name = get_fname($buyer);

                $payment_method = @$r['payment_type_id'];
                $period_type_id = @$r['period_id'];

                $project_id = $transaction['project_id'];
                $property_id = $transaction['property_id'];

                $encoder_data = get_person($r['created_by'], "users");
                $encoder_name = get_fname($encoder_data);

                $or_number = $r['or_number'];

                // Checking string count due to accounting entries or_number column only has 12 char
                if(strlen($or_number) > 12) $or_number = substr($or_number, 0, 12);

                // Get accounting entry
                $accounting_entries = $this->M_Accounting_entries->get(["or_number" => $or_number]);

                $entry_id = @$accounting_entries['id'];

            ?>
    <!--begin:: Portlet-->
    <div class="kt-portlet col-sm-12">
        <div class="kt-portlet__body custom-transaction_payments">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media kt-hidden">
                        <img src="./assets/media/project-logos/3.png" alt="image">
                    </div>
                    <div
                        class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                        <?php echo get_initials(@$buyer['first_name'] . ' ' . @$buyer['last_name']); ?>
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <a href="<?php echo base_url(); ?>transaction/view/<?php echo $transaction_id; ?>"
                                class="kt-widget__username">
                                Reference : <?php echo $reference; ?>
                                <i class="flaticon2-correct"></i>
                            </a>


                            <div class="kt-widget__action custom_portlet_header">

                                <div class="kt-portlet__head kt-portlet__head--noborder" style="min-height: 0px;">
                                    <div class="kt-portlet__head-label">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>"
                                                class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                            <i class="flaticon-more-1 kt-font-brand"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo base_url('transaction_payment/prnt/' . $r['id']); ?>"
                                                        class="kt-nav__link" target="_BLANK">
                                                        <i class="kt-nav__link-icon fa fa-print"></i>
                                                        <span class="kt-nav__link-text">Print</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo base_url('transaction_payment/entries_form/' . $id . '/0/' . $r['principal_amount'] . '/' . $r['interest_amount'] . '/0' ); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit-1"></i>
                                                        <span class="kt-nav__link-text">Add Entries</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo base_url('transaction_payment/re_generate_entries/' . $id); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit-1"></i>
                                                        <span class="kt-nav__link-text">Generate Entries</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">
                                <a href="<?php echo base_url(); ?>project/view/<?php echo $project_id; ?>"
                                    target="_BLANK" class="kt-widget__username">
                                    Project : <?php echo get_value_field($project_name,'projects','name'); ?>
                                </a>
                                <br>
                                <a href="<?php echo base_url(); ?>property/view/<?php echo $property_id; ?>"
                                    target="_BLANK" class="kt-widget__username">
                                    Property : <?php echo get_value_field($property_id,'properties','name'); ?>
                                </a>
                                <br>
                                <a href="<?php echo base_url(); ?>buyer/view/<?php echo $buyer_id; ?>" target="_BLANK"
                                    class="kt-widget__username">
                                    Buyer Name : <?php echo $buyer_name; ?>
                                </a>
                                <br>

                                <a href="#" target="_BLANK" class="kt-widget__username">
                                    Payment Method :
                                    <?php echo Dropdown::get_static('payment_types', $payment_method, 'view'); ?>
                                </a>
                                <br>

                                <a href="#" target="_BLANK" class="kt-widget__username">
                                    Period :
                                    <?php echo Dropdown::get_static('period_names', $period_type_id, 'view'); ?>
                                </a>
                            </div>

                            <div class="kt-widget__desc">

                                <a class="kt-widget__username">
                                    Paid Date:
                                    <u><b><?php echo view_date($r['payment_date']); ?></u></b>
                                </a>
                                <br>


                                <span class="kt-widget__username">
                                    Receipt Number and Receipt Date :
                                    <u><b><?php echo $r['or_number'].' - '.view_date($r['or_date']); ?></u></b>
                                </span>
                                <br>


                                <span class="kt-widget__username">
                                    Encoded By and Created Date:
                                    <u><b><?php echo $encoder_name.' - '.view_date($r['created_at']); ?></u></b>
                                </span>
                                <br>

                                <span class="kt-widget__username">
                                    Accounting Entries:
                                    <?php if($entry_id): ?>
                                        <u><b><a href="<?php echo base_url("accounting_entries/view/" . $entry_id) ?>">View Entry</a></u></b>
                                    <?php else: ?>
                                    <u><b>None</u></b>
                                    <?php endif; ?>
                                </span>
                            </div>

                            <div class="kt-widget__desc">
                                &nbsp;
                            </div>

                            <div class="kt-widget__desc">
                                &nbsp;
                            </div>

                        </div>

                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Reservation Date</span>
                            <span class="kt-widget__value"><?php echo view_date($r['payment_date']); ?></span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Paid Amount</span>
                            <span class="kt-widget__value"><?php echo money_php($r['amount_paid']); ?></span>
                        </div>
                    </div>

                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Principal Amount</span>
                            <span class="kt-widget__value"><?php echo money_php($r['principal_amount']); ?></span>
                        </div>
                    </div>

                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Interest Amount</span>
                            <span class="kt-widget__value"><?php echo money_php($r['interest_amount']); ?></span>
                        </div>
                    </div>

                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Penalty Amount</span>
                            <span class="kt-widget__value"><?php echo money_php($r['penalty_amount']); ?></span>
                        </div>
                    </div>

                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Remarks</span>
                            <span class="kt-widget__value"><?php echo $r['remarks']; ?></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
        <?php endforeach; ?>
</div>

<div class="row">
    <div class="col-xl-12">

        <!--begin:: Components/Pagination/Default-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">

                <!--begin: Pagination-->
                <div class="kt-pagination kt-pagination--brand">
                    <?php echo $this->ajax_pagination->create_links(); ?>

                    <div class="kt-pagination__toolbar">
                        <span class="pagination__desc">
                            <?php echo $this->ajax_pagination->show_count(); ?>
                        </span>
                    </div>
                </div>

                <!--end: Pagination-->
            </div>
        </div>

        <!--end:: Components/Pagination/Default-->
    </div>
</div>
<?php else : ?>
<div class="row">
    <div class="col-lg-12">
        <div class="kt-portlet kt-callout">
            <div class="kt-portlet__body">
                <div class="kt-callout__body">
                    <div class="kt-callout__content">
                        <h3 class="kt-callout__title">No Records Found</h3>
                        <p class="kt-callout__desc">
                            Sorry no record were found.
                        </p>
                    </div>
                    <div class="kt-callout__action">
                        <a href="<?php echo base_url('transaction'); ?>"
                            class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>