<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seller_group_model extends MY_Model
{
    public $table = 'seller_group'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name',
        'code',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    // public function insert_dummy()
    // {
    //     require APPPATH . '/third_party/faker/autoload.php';
    //     $faker = Faker\Factory::create();

    //     $data = [];

    //     for ($x = 0; $x < 10; $x++) {
    //         array_push($data, array(
    //             'name' => $faker->word,
    //             'code' => $faker->numberBetween(1, 3),
    //         ));
    //     }
    //     $this->db->insert_batch($this->table, $data);

    // }
}
