<?php
$id = isset($seller_group['id']) && $seller_group['id'] ? $seller_group['id'] : '';
$name = isset($seller_group['name']) && $seller_group['name'] ? $seller_group['name'] : '';
$code = isset($seller_group['code']) && $seller_group['code'] ? $seller_group['code'] : '';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="code" value="<?php echo set_value('code', $code); ?>" placeholder="Code" autocomplete="off">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>