<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$project_id = isset($info['project']['id']) && $info['project']["id"] ? $info['project']["id"] : '';
$project_name = isset($info['project']['name']) && $info['project']["name"] ? $info['project']["name"] : '';
$property_id = isset($info['property']['id']) && $info['property']['id'] ? $info['property']['id'] : '';
$property_name = isset($info['property']['name']) && $info['property']['name'] ? $info['property']['name'] : '';
$land_id = isset($info['land']['id']) && $info['land']["id"] ? $info['land']["id"] : '';
$land_name = isset($info['land']['name']) && $info['land']["name"] ? $info['land']["name"] : '';
$department_id = isset($info['department']['id']) && $info['department']["id"] ? $info['department']["id"] : '';
$department_name = isset($info['department']['name']) && $info['department']["name"] ? $info['department']["name"] : '';

// vdebug($info);
// ==================== end: Add model fields ====================
?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Project</label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="projects" id="project_id" name="project_id">
                    <option value="">Select Project</option>
                    <?php if ($project_id) : ?>
                        <option value="<?php echo @$project_id; ?>" selected><?php echo $project_name; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('company_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Property</label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="properties" id="property_id" name="property_id" data-param='project_id' disabled>
                    <option value="">Select Property</option>
                    <?php if ($property_id) : ?>
                        <option value="<?php echo @$property_id; ?>" selected><?php echo $property_name; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('company_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Land Inventory</label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="land_inventories" data-type='land_inventory' id="land_inventory_id" name="land_inventory_id">
                    <option value="">Select Land Inventory</option>
                    <?php if ($land_id) : ?>
                        <option value="<?php echo @$land_id; ?>" selected><?php echo $land_name; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('company_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Department</label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="departments" id="department_id" name="department_id">
                    <option value="">Select Department</option>
                    <?php if ($department_id) : ?>
                        <option value="<?php echo @$department_id; ?>" selected><?php echo $department_name; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('company_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Ledger <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo form_dropdown('ledger_id', $ledger_dropdown, '', 'class="form-control" id="ledger_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Amount <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="amount" id="amount" value="<?php echo $info['amount'];  ?>" placeholder="" autocomplete="off">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Month <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                <?php echo form_dropdown('month', $months, set_value('month',$info['month']), 'class="form-control" id="month"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Year <span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <input type="text" class="form-control yearPicker" name="year" id="year" value="<?php echo $info['year'];  ?>" readonly placeholder="" autocomplete="off">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>