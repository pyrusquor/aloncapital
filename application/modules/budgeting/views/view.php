<?php
// $id = isset($data['id']) && $data['id'] ? $data['id'] : '';
if ($data) {
    $category = isset($data['category_id']) && $data['category_id'] ? $data['category_id'] : '';
    $name = isset($data['name']) && $data['name'] ? $data['name'] : '';
    $sub_category = isset($data['sub_category_id']) && $data['sub_category_id'] ? $data['sub_category_id'] : '';
    $sub_name = isset($data['sub_name']) && $data['sub_name'] ? $data['sub_name'] : '';
    $ledger_id = isset($data['ledger_id']) && $data['ledger_id'] ? $data['ledger_id'] : '';
    $amount = isset($data['amount']) && $data['amount'] ? $data['amount'] : '';
    $month = isset($data['month']) && $data['month'] ? $data['month'] : '';
    $year = isset($data['year']) && $data['year'] ? $data['year'] : '';
}
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Budgeting</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <!-- <a href="<?php echo site_url('cashier/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a> -->
                <a href="<?php echo site_url('budgeting'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="row">
    <div class="col-sm-4">

        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Budgeting Information
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">

                <div class="kt-widget13">
                    <div class="kt-widget13__item">
                        <span class="kt-widget13__desc">
                            <?= $category ?>
                        </span>
                        <span class="kt-widget13__text kt-widget13__text--bold">
                            <?= $name ?>
                        </span>
                    </div>
                    <?php if($sub_category): ?>
                    <div class="kt-widget13__item">
                        <span class="kt-widget13__desc">
                            <?= $sub_category ?>
                        </span>
                        <span class="kt-widget13__text kt-widget13__text--bold">
                            <?= $sub_name ?>
                        </span>
                    </div>
                    <?php endif ?>
                </div>

                <div class="kt-widget13">
                    <div class="kt-widget13__item">
                        <span class="kt-widget13__desc">
                            Ledger
                        </span>
                        <span class="kt-widget13__text kt-widget13__text--bold">
                            <?= $ledger_id ?>
                        </span>
                    </div>
                    <div class="kt-widget13__item">
                        <span class="kt-widget13__desc">
                            Amount
                        </span>
                        <span class="kt-widget13__text kt-widget13__text--bold">
                            <?= money_php($amount) ?>
                        </span>
                    </div>
                </div>

                <div class="kt-widget13">
                    <div class="kt-widget13__item">
                        <span class="kt-widget13__desc">
                            Month
                        </span>
                        <span class="kt-widget13__text kt-widget13__text--bold">
                            <?= $month ?>
                        </span>
                    </div>
                    <div class="kt-widget13__item">
                        <span class="kt-widget13__desc">
                            Year
                        </span>
                        <span class="kt-widget13__text kt-widget13__text--bold">
                            <?= $year ?>
                        </span>
                    </div>
                </div>

            </div>
            <!--end::Portlet-->

        </div>

    </div>

    <div class="col-sm-6">

    </div>
</div>
<!-- begin:: Footer -->