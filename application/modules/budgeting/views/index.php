<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Budgeting Setup
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="total"></span>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                    <input type="text" class="form-control" placeholder="Search Budgets..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"></path>
                                </g>
                            </svg>

                            <!--<i class="flaticon2-search-1"></i>-->
                        </span>
                    </span>
                </div>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <a href="<?php echo site_url('budgeting/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Add setup
            </a>

        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__filter">
            <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                <i class="fa fa-filter"></i> Filter
            </button>
        </div>
    </div>

</div>

<!-- CONTENT -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">

        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form" id="advance_search">
                        <div class="form-group row">
                            <div class="form-group col-md-3">
                                <label class="form-control-label">Ledger</label>
                                <select name="ledger_id" class="form-control form-control-sm _filter suggests" id='ledger_id' placeholder="ledger" data-module="accounting_ledgers">
                                    <option value="">Select option</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-control-label">Year:</label>
                                <?php echo form_dropdown('month', $months, '', 'class="form-control _filter" id="month"'); ?>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-control-label">Year:</label>
                                <input type="text" class="form-control yearPicker _filter" name="year" id="year" readonly>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="budgeting_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Ledger</th>
                    <th>Amount</th>
                    <th>Month</th>
                    <th>Year</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>