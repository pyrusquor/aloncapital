<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Budgeting extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $budgeting_model = array(
            'budgeting/Budgeting_setup_model' => 'M_budget_setup',
            'project/Project_model' => 'M_project',
            'property/Property_model' => 'M_property',
            'department/Department_model' => 'M_department',
            'land/Land_inventory_model' => 'M_land',
        );

        // Load models
        $this->load->model($budgeting_model);


        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_budget_setup->fillable;
        $this->_table_columns = $this->M_budget_setup->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'is_active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];

        $this->load->helper('format');
        $this->load->helper('accounting_db');
    }

    public function index()
    {

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        for ($x = 0; $x < 13; $x++) {
            $months[$x] = get_month_name($x);
        }

        $this->view_data['months'] = $months;

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'name' => true,
            'ledger_id' => true,
            'amount' => true,
            'month' => true,
            'year' => true,
            'status' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];
        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_budget_setup
                ->with_project('fields:name')
                ->with_property('fields:name,project_id')
                ->with_land('fields:title')
                ->with_department('fields:name')
                ->with_ledger('fields:name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(amount like '%$searchValue%'");
                $query->or_where("month like '%$searchValue%'");
                $query->or_where("year like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'ledger_id' => [
                    'data' => $filters['ledger_id'] ?? null,
                    'operator' => '=',
                ],
                'year' => [
                    'data' => $filters['year'] ?? null,
                    'operator' => '=',
                ],
                'month' => [
                    'data' => $filters['month'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {
                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {
                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {
                        if (isset($value['project'])) {
                            $items[$key]['name'] = $value['project']['name'];
                        }
                        if (isset($value['land'])) {
                            $items[$key]['name'] = $value['land']['title'];
                        }
                        if (isset($value['property'])) {
                            $items[$key]['name'] = $value['property']['name'];
                        }
                        if (isset($value['department'])) {
                            $items[$key]['name'] = $value['department']['name'];
                        }
                        $items[$key]['ledger_id'] = $value['ledger']['name'];

                        $items[$key]['status'] = Dropdown::get_static('budgeting_setup_status',$value['status'], 'view');
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_budget_setup->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }
    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_budget_setup->get($id);
            if ($list) {

                $deleted = $this->M_budget_setup->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Budgeting Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_budget_setup->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Budgeting successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
    public function view($id = FALSE)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $raw_data = $this->M_budget_setup->with_project('fields:name')->with_property('fields:name,project_id')->with_land('fields:title')->with_department('fields:name')->get($id);

            if ($raw_data) {
                $raw_ledger_id = $raw_data['ledger_id'];

                $ledger_id = get_value_field($raw_ledger_id, 'accounting_ledgers', 'name');

                if ($ledger_id) {
                    $raw_data['ledger_id'] = $ledger_id;
                }

                if (isset($raw_data['project'])) {
                    $raw_data['name'] = $raw_data['project']['name'];
                    $raw_data['category_id'] = 'Project';
                }
                if (isset($raw_data['land'])) {
                    $raw_data['name'] = $raw_data['land']['title'];
                    $raw_data['category_id'] = 'Land Inventory';
                }
                if (isset($raw_data['property'])) {
                    $project_object = get_person($raw_data['property']['project_id'], 'projects');
                    $raw_data['name'] = get_name($project_object);
                    $raw_data['sub_category_id'] = 'Property';
                    $raw_data['sub_name'] = $raw_data['property']['name'];
                    $raw_data['category_id'] = 'Project';
                }
                if (isset($raw_data['department'])) {
                    $raw_data['name'] = $raw_data['department']['name'];
                    $raw_data['category_id'] = 'Department';
                }
                $raw_data['month'] = get_month_name($raw_data['month']);
                $this->view_data['data'] = $raw_data;
                $this->template->build('view', $this->view_data);
                // vdebug($raw_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }
    public function form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        $ledgers = get_expense_ledgers(null, true);
        foreach ($ledgers as $key => $value) {
            $ledger_dropdown[$value['id']] = $value['name'];
        }
        for ($x = 1; $x < 13; $x++) {
            $months[$x] = get_month_name($x);
        }
        $this->view_data['ledger_dropdown'] = $ledger_dropdown;
        $this->view_data['months'] = $months;

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_budget_setup->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();
                $info['status'] = 1;

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $cashier_status = $this->M_budget_setup->update($info + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];
                    $cashier_status = $this->M_budget_setup->insert($info + $additional);
                }

                if ($cashier_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Budgeting Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_budget_setup->with_project()->with_property()->with_land()->with_department()->get($id);
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }

    public function change_status(){
        $response['status'] = 0;
        $response['message'] = 'Change status failed!';
        $post = $this->input->post();
        if($post){
            $id = $post['id'];
            $info['status'] = $post['status'];

            $result = $this->M_budget_setup->update($info,$id);
            if($result){
                $response['status'] = 1;
                $response['message'] = 'Successfully changed status!';
            }
        }
        print json_encode($response);
        exit();
    }
}
