<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Budgeting_logs_model extends MY_Model
{

    public $table = 'budgeting_logs'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'id',
        'ledger_id',
        'budgeting_setup_id',
        'material_request_id',
        'amount',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',

    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['ledger'] = array('foreign_model' => 'accounting_ledgers/Accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'ledger_id');
        $this->has_one['budget'] = array('foreign_model' => 'budgeting/Budgeting_setup_model', 'foreign_table' => 'budgeting_setup', 'foreign_key' => 'id', 'local_key' => 'budgeting_setup_id');
        $this->has_one['material_request'] = array('foreign_model' => 'material_request/Material_request_model', 'foreign_table' => 'material_requests', 'foreign_key' => 'id', 'local_key' => 'material_request_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
