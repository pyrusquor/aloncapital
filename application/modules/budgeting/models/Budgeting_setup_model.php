<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Budgeting_setup_model extends MY_Model
{

    public $table = 'budgeting_setup'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'id',
        'project_id',
        'property_id',
        'land_inventory_id',
        'department_id',
        'ledger_id',
        'amount',
        'month',
        'year',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',

    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'ledger_id' => array(
            'field' => 'ledger_id',
            'label' => 'Ledger',
            'rules' => 'trim|required',
        ),
        'amount' => array(
            'field' => 'amount',
            'label' => 'Amount',
            'rules' => 'trim|required',
        ),
        'month' => array(
            'field' => 'month',
            'label' => 'Month',
            'rules' => 'trim',
        ),
        'year' => array(
            'field' => 'year',
            'label' => 'Year',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['project'] = array('foreign_model' => 'project/Project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');

        $this->has_one['property'] = array('foreign_model' => 'property/Property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');

        $this->has_one['department'] = array('foreign_model' => 'department/Department_model', 'foreign_table' => 'departments', 'foreign_key' => 'id', 'local_key' => 'department_id');

        $this->has_one['land'] = array('foreign_model' => 'land/Land_inventory_model', 'foreign_table' => 'land_inventories', 'foreign_key' => 'id', 'local_key' => 'land_inventory_id');

        $this->has_one['ledger'] = array('foreign_model' => 'accounting_ledgers/accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'ledger_id');

    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
