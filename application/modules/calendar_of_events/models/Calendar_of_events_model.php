<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Calendar_of_events_model extends MY_Model
{

    public $table = 'transaction_payments'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = []; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert']    =    $this->fields;
        $this->rules['update']    =    $this->fields;
    }

    public function due_date_receivable_payments($limit = null, $start = null, $where = array(), $getCount = false)
    {
        $where['due_date'] = isset($where['due_date']) ? date("Y-m-d", strtotime($where['due_date'])) : date("Y-m-d");

        $this->db->select($getCount ? 'COUNT(transaction_payments.id) as count' : [
            'transaction_payments.id as transaction_payment_id',
            'transaction_payments.total_amount as transaction_payment_amount',

            'transactions.id as transaction_id',
            'transactions.reference as transaction_reference',

            'projects.id as project_id',
            'projects.name as project_name',

            'properties.id as property_id',
            'properties.name as property_name',

            'buyers.id as buyer_id',
            'buyers.first_name as buyer_first_name',
            'buyers.last_name as buyer_last_name'
        ])
            ->from("transaction_payments")
            ->where("transaction_payments.deleted_at IS NULL")
            ->where('transaction_payments.due_date', $where['due_date'])
            ->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left')
            ->where('transactions.deleted_at IS NULL')
            ->join('projects', 'projects.id = transactions.project_id', 'left')
            ->where('projects.deleted_at IS NULL')
            ->join('properties', 'properties.id = transactions.property_id', 'left')
            ->where('properties.deleted_at IS NULL')
            ->join('buyers', 'buyers.id = transactions.buyer_id', 'left')
            ->where('buyers.deleted_at IS NULL');

        $this->db->limit($limit, $start);

        if ($getCount) {

            $query = $this->db->get()->row_array();

            return $query['count'];
        } else {

            $query = $this->db->get()->result_array();

            return $query;
        }
    }

    public function due_date_post_dated_checks($limit = null, $start = null, $where = array(), $getCount = false)
    {
        $where['due_date'] = isset($where['due_date']) ? date("Y-m-d", strtotime($where['due_date'])) : date("Y-m-d");

        $this->db->select($getCount ? 'COUNT(transaction_post_dated_checks.id) as count' : [
            'transaction_post_dated_checks.id as transaction_post_dated_check_id',
            'transaction_post_dated_checks.amount as transaction_post_dated_check_amount',

            'transactions.id as transaction_id',
            'transactions.reference as transaction_reference',

            'projects.id as project_id',
            'projects.name as project_name',

            'properties.id as property_id',
            'properties.name as property_name',

            'buyers.id as buyer_id',
            'buyers.first_name as buyer_first_name',
            'buyers.last_name as buyer_last_name'
        ])
            ->from("transaction_post_dated_checks")
            ->where("transaction_post_dated_checks.deleted_at IS NULL")
            ->where('transaction_post_dated_checks.due_date', $where['due_date'])
            ->join('transactions', 'transactions.id = transaction_post_dated_checks.transaction_id', 'left')
            ->where('transactions.deleted_at IS NULL')
            ->join('projects', 'projects.id = transactions.project_id', 'left')
            ->where('projects.deleted_at IS NULL')
            ->join('properties', 'properties.id = transactions.property_id', 'left')
            ->where('properties.deleted_at IS NULL')
            ->join('buyers', 'buyers.id = transactions.buyer_id', 'left')
            ->where('buyers.deleted_at IS NULL');

        $this->db->limit($limit, $start);

        if ($getCount) {

            $query = $this->db->get()->row_array();

            return $query['count'];
        } else {

            $query = $this->db->get()->result_array();

            return $query;
        }
    }


    public function encoded_due_date_post_dated_checks($limit = null, $start = null, $where = array(), $getCount = false)
    {
        $where['due_date'] = isset($where['due_date']) ? date("Y-m-d", strtotime($where['due_date'])) : date("Y-m-d");

        $this->db->select($getCount ? 'COUNT(transaction_post_dated_checks.id) as count' : [
            'transaction_post_dated_checks.id as transaction_post_dated_check_id',
            'transaction_post_dated_checks.amount as transaction_post_dated_check_amount',

            'transactions.id as transaction_id',
            'transactions.reference as transaction_reference',

            'projects.id as project_id',
            'projects.name as project_name',

            'properties.id as property_id',
            'properties.name as property_name',

            'buyers.id as buyer_id',
            'buyers.first_name as buyer_first_name',
            'buyers.last_name as buyer_last_name'
        ])
            ->from("transaction_post_dated_checks")
            ->where("transaction_post_dated_checks.deleted_at IS NULL")
            ->where('transaction_post_dated_checks.created_at', $where['due_date'])
            ->join('transactions', 'transactions.id = transaction_post_dated_checks.transaction_id', 'left')
            ->where('transactions.deleted_at IS NULL')
            ->join('projects', 'projects.id = transactions.project_id', 'left')
            ->where('projects.deleted_at IS NULL')
            ->join('properties', 'properties.id = transactions.property_id', 'left')
            ->where('properties.deleted_at IS NULL')
            ->join('buyers', 'buyers.id = transactions.buyer_id', 'left')
            ->where('buyers.deleted_at IS NULL');

        $this->db->limit($limit, $start);

        if ($getCount) {

            $query = $this->db->get()->row_array();

            return $query['count'];
        } else {

            $query = $this->db->get()->result_array();

            return $query;
        }
    }

    public function reservation_date_reserved_transactions($limit = null, $start = null, $where = array(), $getCount = false)
    {
        $where['due_date'] = isset($where['due_date']) ? date("Y-m-d", strtotime($where['due_date'])) : date("Y-m-d");

        $this->db->select($getCount ? 'COUNT(transactions.id) as count' : [

            'transactions.id as transaction_id',
            'transactions.reference as transaction_reference',

            'projects.id as project_id',
            'projects.name as project_name',

            'properties.id as property_id',
            'properties.name as property_name',

            'buyers.id as buyer_id',
            'buyers.first_name as buyer_first_name',
            'buyers.last_name as buyer_last_name'
        ])
            ->from("transactions")
            ->where("transactions.deleted_at IS NULL")
            ->where('transactions.reservation_date', $where['due_date'])
            ->join('projects', 'projects.id = transactions.project_id', 'left')
            ->where('projects.deleted_at IS NULL')
            ->join('properties', 'properties.id = transactions.property_id', 'left')
            ->where('properties.deleted_at IS NULL')
            ->join('buyers', 'buyers.id = transactions.buyer_id', 'left')
            ->where('buyers.deleted_at IS NULL');

        $this->db->limit($limit, $start);

        if ($getCount) {

            $query = $this->db->get()->row_array();

            return $query['count'];
        } else {

            $query = $this->db->get()->result_array();

            return $query;
        }
    }

    public function encoded_transactions($limit = null, $start = null, $where = array(), $getCount = false)
    {
        $where['date'] = isset($where['date']) ? date("Y-m-d", strtotime($where['date'])) : date("Y-m-d");

        $this->db->select($getCount ? 'COUNT(transactions.id) as count' : [

            'transactions.id as transaction_id',
            'transactions.reference as transaction_reference',
            'transactions.reservation_date',

            'projects.id as project_id',
            'projects.name as project_name',

            'properties.id as property_id',
            'properties.name as property_name',

            'buyers.id as buyer_id',
            'buyers.first_name as buyer_first_name',
            'buyers.last_name as buyer_last_name',

            'transactions.created_by'
        ])
            ->from("transactions")
            ->where("transactions.deleted_at IS NULL")
            ->like('transactions.created_at', $where['date'])
            ->join('projects', 'projects.id = transactions.project_id', 'left')
            ->where('projects.deleted_at IS NULL')
            ->join('properties', 'properties.id = transactions.property_id', 'left')
            ->where('properties.deleted_at IS NULL')
            ->join('buyers', 'buyers.id = transactions.buyer_id', 'left')
            ->where('buyers.deleted_at IS NULL');

        $this->db->limit($limit, $start);

        if ($getCount) {

            $query = $this->db->get()->row_array();

            return $query['count'];
        } else {

            $query = $this->db->get()->result_array();

            return $query;
        }
    }

    public function payment_date_collected_payments($limit = null, $start = null, $where = array(), $getCount = false)
    {
        $where['due_date'] = isset($where['due_date']) ? date("Y-m-d", strtotime($where['due_date'])) : date("Y-m-d");

        $this->db->select($getCount ? 'COUNT(transaction_official_payments.id) as count' : [
            'transaction_official_payments.id as transaction_official_payment_id',
            'transaction_official_payments.amount_paid as transaction_official_payment_amount',

            'transactions.id as transaction_id',
            'transactions.reference as transaction_reference',

            'projects.id as project_id',
            'projects.name as project_name',

            'properties.id as property_id',
            'properties.name as property_name',

            'buyers.id as buyer_id',
            'buyers.first_name as buyer_first_name',
            'buyers.last_name as buyer_last_name'
        ])
            ->from("transaction_official_payments")
            ->where("transaction_official_payments.deleted_at IS NULL")
            ->where('transaction_official_payments.payment_date', $where['due_date'])
            ->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left')
            ->where('transactions.deleted_at IS NULL')
            ->join('projects', 'projects.id = transactions.project_id', 'left')
            ->where('projects.deleted_at IS NULL')
            ->join('properties', 'properties.id = transactions.property_id', 'left')
            ->where('properties.deleted_at IS NULL')
            ->join('buyers', 'buyers.id = transactions.buyer_id', 'left')
            ->where('buyers.deleted_at IS NULL');

        $this->db->limit($limit, $start);

        if ($getCount) {

            $query = $this->db->get()->row_array();

            return $query['count'];
        } else {

            $query = $this->db->get()->result_array();

            return $query;
        }
    }

    public function encoded_collections($limit = null, $start = null, $where = array(), $getCount = false)
    {
        $where['date'] = isset($where['date']) ? date("Y-m-d", strtotime($where['date'])) : date("Y-m-d");

        $this->db->select($getCount ? 'COUNT(transactions.id) as count' : [

            'transactions.id as transaction_id',
            'transactions.reference as transaction_reference',

            'projects.id as project_id',
            'projects.name as project_name',

            'properties.id as property_id',
            'properties.name as property_name',

            'buyers.id as buyer_id',
            'buyers.first_name as buyer_first_name',
            'buyers.last_name as buyer_last_name',

            'transaction_official_payments.amount_paid',
            'transaction_official_payments.amount_paid',
            'transaction_official_payments.created_by',
            'transaction_official_payments.payment_date'
        ])
            ->from("transactions")
            ->where("transactions.deleted_at IS NULL")
            ->join('transaction_official_payments', 'transaction_official_payments.transaction_id = transactions.id', 'left')
            ->like('transaction_official_payments.created_at', $where['date'])
            ->join('projects', 'projects.id = transactions.project_id', 'left')
            ->where('projects.deleted_at IS NULL')
            ->join('properties', 'properties.id = transactions.property_id', 'left')
            ->where('properties.deleted_at IS NULL')
            ->join('buyers', 'buyers.id = transactions.buyer_id', 'left')
            ->where('buyers.deleted_at IS NULL');

        $this->db->limit($limit, $start);

        if ($getCount) {

            $query = $this->db->get()->row_array();

            return $query['count'];
        } else {

            $query = $this->db->get()->result_array();

            return $query;
        }
    }

    public function encoded_buyers($limit = null, $start = null, $where = array(), $getCount = false)
    {
        $where['date'] = isset($where['date']) ? date("Y-m-d", strtotime($where['date'])) : date("Y-m-d");

        $this->db->select($getCount ? 'COUNT(buyers.id) as count' : [

            'transactions.id as transaction_id',
            'transactions.reference as transaction_reference',

            'projects.id as project_id',
            'projects.name as project_name',

            'properties.id as property_id',
            'properties.name as property_name',

            'buyers.id as buyer_id',
            'buyers.first_name as buyer_first_name',
            'buyers.last_name as buyer_last_name'
        ])
            ->from("buyers")
            ->where("buyers.deleted_at IS NULL")
            ->like('buyers.created_at', $where['date'])
            ->join('transactions', 'transactions.buyer_id = buyers.id', 'left')
            ->where("transactions.deleted_at IS NULL")
            ->join('projects', 'projects.id = transactions.project_id', 'left')
            ->where('projects.deleted_at IS NULL')
            ->join('properties', 'properties.id = transactions.property_id', 'left')
            ->where('properties.deleted_at IS NULL');

        $this->db->limit($limit, $start);

        if ($getCount) {

            $query = $this->db->get()->row_array();

            return $query['count'];
        } else {

            $query = $this->db->get()->result_array();

            return $query;
        }
    }
}
