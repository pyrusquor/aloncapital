<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Calendar of Events
            </h3>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-lg-2">
                <?php $this->load->view('_links'); ?>
            </div>
            <div class="col-lg-6">
                <?php $this->load->view('_calendar'); ?>
            </div>
            <div class="col-lg-4" id="datatable_content">
            </div>
        </div>
    </div>
</div>