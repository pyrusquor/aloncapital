<div class="kt-portlet kt-portlet--mobile border">
    <div class="kt-portlet__head kt-bg-success d-flex justify-content-center">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title text-white">
                QUICK LINKS
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="d-flex justify-content-center align-items-center mb-4">
            <a href="<?php echo site_url('transaction/form'); ?>" class="btn btn-label-success btn-elevate btn-block">
                New Contract
            </a>
        </div>
        <div class="d-flex justify-content-between align-items-center mb-4" style="column-gap: 20px;">
            <a href="<?php echo site_url('buyer/create'); ?>" class="btn btn-label-success btn-elevate btn-block">
                Create Buyer
            </a>
            <a href="<?php echo site_url('seller/create'); ?>" class="btn btn-label-success btn-elevate btn-block m-0">
                Create Seller
            </a>
        </div>
        <div class="d-flex justify-content-between align-items-center" style="column-gap: 20px;">
            <a href="<?php echo site_url('storage?p='); ?>" class="btn btn-label-success btn-elevate btn-block">
                Upload Files
            </a>
            <a href="<?php echo site_url('reconciliation'); ?>" class="btn btn-label-success btn-elevate btn-block m-0">
                Reconcile Entry
            </a>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile border d-none" id="sales_links">
    <div class="kt-portlet__head d-flex text-uppercase kt-bg-success">
        <div class="row flex-fill">
            <div class="col-lg-6 d-flex align-items-center justify-content-center">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title text-white">
                        Sales
                    </h3>
                </div>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-center">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title text-white">
                        Weekly Change
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body p-0">
        <div class="d-flex">
            <div class="flex-fill border border-left-0 border-top-0 border-bottom-0">
                <div id="previous_week_sales_date" class="font-weight-bold text-center p-3 kt-bg-light-primary">
                </div>
                <div id="previous_week_sales_amount" class="font-weight-bold text-center p-3">
                </div>
                <div class="font-weight-bold text-center p-3 kt-bg-light-primary" id="previous_week_sales_count">
                </div>
                <div class="p-3">
                    <a href="<?php echo site_url('dashboard/sales_report'); ?>" class="btn btn-label-success btn-elevate btn-block">
                        <span>
                            <i class="fa fa-chart-line"></i>
                        </span>
                        View Matrix
                    </a>
                </div>
            </div>
            <div class="flex-fill">
                <div id="this_week_sales_date" class="font-weight-bold text-center p-3 kt-bg-light-primary">
                </div>
                <div class="font-weight-bold text-center p-3" id="this_week_sales_amount">
                </div>
                <div class="font-weight-bold text-center p-3 kt-bg-light-primary" id="this_week_sales_count">
                </div>
                <div class="p-3">
                    <a href="<?php echo site_url('dashboard/sales_report'); ?>" class="btn btn-label-success btn-elevate btn-block m-0">
                        <span>
                            <i class="fa fa-chart-line"></i>
                        </span>
                        View Matrix
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile border d-none" id="collections_links">
    <div class="kt-portlet__head d-flex text-uppercase kt-bg-success">
        <div class="row flex-fill">
            <div class="col-lg-6 d-flex align-items-center justify-content-center">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title text-white">
                        Collections
                    </h3>
                </div>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-center">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title text-white">
                        Weekly Change
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body p-0">
        <div class="d-flex">
            <div class="flex-fill border border-left-0 border-top-0 border-bottom-0">
                <div id="previous_week_collections_date" class="font-weight-bold text-center p-3 kt-bg-light-primary">
                </div>
                <div id="previous_week_collections_amount" class="font-weight-bold text-center p-3">
                </div>
                <div class="font-weight-bold text-center p-3 kt-bg-light-primary" id="previous_week_collections_count">
                </div>
                <div class="p-3">
                    <a href="<?php echo site_url('dashboard/financials_graph'); ?>" class="btn btn-label-success btn-elevate btn-block">
                        <span>
                            <i class="fa fa-chart-line"></i>
                        </span>
                        View Matrix
                    </a>
                </div>
            </div>
            <div class="flex-fill">
                <div id="this_week_collections_date" class="font-weight-bold text-center p-3 kt-bg-light-primary">
                </div>
                <div id="this_week_collections_amount" class="font-weight-bold text-center p-3">
                </div>
                <div class="font-weight-bold text-center p-3 kt-bg-light-primary" id="this_week_collections_count">
                </div>
                <div class="p-3">
                    <a href="<?php echo site_url('dashboard/financials_graph'); ?>" class="btn btn-label-success btn-elevate btn-block m-0">
                        <span>
                            <i class="fa fa-chart-line"></i>
                        </span>
                        View Matrix
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>