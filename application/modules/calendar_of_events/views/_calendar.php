<div class="kt-portlet kt-portlet--mobile border">
    <div class="kt-portlet__body">
        <div id="kt_calendar" style="min-height: 800px;">
        </div>
        <div class="text-uppercase mt-3">
            <div class="kt-menu__link-text">LEGEND:</div>
            <div class="d-flex flex-wrap" style="column-gap: 16px;">
                <div class="kt-menu__link-text d-flex align-items-center" style="gap:8px;">
                    <div class="rounded-circle" style="width: 12px; height:12px; background-color: #FFB822;"></div>
                    <div>Reserved Transactions</div>
                </div>
                <div class="kt-menu__link-text d-flex align-items-center" style="gap:8px;">
                    <div class="rounded-circle" style="width: 12px; height:12px; background-color: #0ABB87;"></div>
                    <div>Collected Payments</div>
                </div>
                <div class="kt-menu__link-text d-flex align-items-center" style="gap:8px;">
                    <div class="rounded-circle" style="width: 12px; height:12px; background-color: #5867DD;"></div>
                    <div>Receivable Payments</div>
                </div>
                <div class="kt-menu__link-text d-flex align-items-center" style="gap:8px;">
                    <div class="rounded-circle" style="width: 12px; height:12px; background-color: #FD397A;"></div>
                    <div>PDC to be cleared</div>
                </div>
                <div class="kt-menu__link-text d-flex align-items-center" style="gap:8px;">
                    <div class="rounded-circle fc-event-orange" style="width: 12px; height:12px;"></div>
                    <div>Encoded PDC</div>
                </div>
                <div class="kt-menu__link-text d-flex align-items-center" style="gap:8px;">
                    <div class="rounded-circle fc-event-lime" style="width: 12px; height:12px;"></div>
                    <div>Encoded Transactions</div>
                </div>
                <div class="kt-menu__link-text d-flex align-items-center" style="gap:8px;">
                    <div class="rounded-circle fc-event-purple" style="width: 12px; height:12px;"></div>
                    <div>Encoded Collections</div>
                </div>
                <div class="kt-menu__link-text d-flex align-items-center" style="gap:8px;">
                    <div class="rounded-circle fc-event-cyan" style="width: 12px; height:12px;"></div>
                    <div>Encoded Buyers</div>
                </div>
            </div>
        </div>
        <div class="border border-bottom-0 mt-3">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Collections
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar" id="collections_date">
                </div>
            </div>
            <div class="kt-portlet__body p-0">
                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="reserved_transactions_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle" style="width: 12px; height:12px; background-color: #FFB822;">
                        </div>
                        <div>
                            <div>Reserved Transactions</div>
                            <div>Count: <b id="reserved_transactions_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="collected_payments_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle" style="width: 12px; height:12px; background-color: #0ABB87;">
                        </div>
                        <div>
                            <div>Collected Payments: <b id="collected_payments_amount"></b></div>
                            <div>Count: <b id="collected_payments_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="receivable_payments_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle" style="width: 12px; height:12px; background-color: #5867DD;">
                        </div>
                        <div>
                            <div>Receivable Payments <b id="receivable_payments_amount"></b></div>
                            <div>Count: <b id="receivable_payments_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="post_dated_checks_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle" style="width: 12px; height:12px; background-color: #FD397A;">
                        </div>
                        <div>
                            <div>PDC to be cleared <b id="post_dated_checks_amount"></b></div>
                            <div>Count: <b id="post_dated_checks_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="encoded_transactions_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle fc-event-lime" style="width: 12px; height:12px;">
                        </div>
                        <div>
                            <div>Encoded Transactions</div>
                            <div>Count: <b id="encoded_transactions_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="encoded_collections_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle fc-event-purple" style="width: 12px; height:12px;">
                        </div>
                        <div>
                            <div>Encoded Collections</div>
                            <div>Count: <b id="encoded_collections_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="encoded_buyers_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle fc-event-cyan" style="width: 12px; height:12px;">
                        </div>
                        <div>
                            <div>Encoded Buyers</div>
                            <div>Count: <b id="encoded_buyers_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>

                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="encoded_post_dated_checks_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle fc-event-orange" style="width: 12px; height:12px;">
                        </div>
                        <div>
                            <div>Encoded PDC <b id="encoded_post_dated_checks_amount"></b></div>
                            <div>Count: <b id="encoded_post_dated_checks_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>

                <div class="d-none justify-content-between align-items-center px-4 py-2 border border-top-0 border-left-0 border-right-0 collections_item" id="encoded_post_dated_checks_item">
                    <div class="d-flex align-items-center" style="column-gap: 16px;">
                        <div class="rounded-circle" style="width: 12px; height:12px; background-color: #FD397A;">
                        </div>
                        <div>
                            <div>Encoded PDC <b id="encoded_post_dated_checks_amount"></b></div>
                            <div>Count: <b id="encoded_post_dated_checks_count"></b></div>
                        </div>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999) " />
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>