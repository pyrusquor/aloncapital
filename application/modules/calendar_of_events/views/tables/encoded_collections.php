<div class="kt-portlet kt-portlet--mobile border">
    <div class="kt-portlet__head fc-event-purple">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title text-white">
                ENCODED COLLECTIONS
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar text-white">
            <?= @$date ?>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped- table-bordered table-hover" id="receivable_payments_table">
                    <thead>
                        <tr>
                            <th>Transaction</th>
                            <th>Project</th>
                            <th>Amount Paid</th>
                            <th>Encoded By</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $key => $value) : ?>
                            <?php
                            $transaction_id = isset($value['transaction_id']) && $value['transaction_id'] ? $value['transaction_id'] : "";

                            $transaction_reference = isset($value['transaction_reference']) && $value['transaction_reference'] ? $value['transaction_reference'] : "";

                            $project_id = isset($value['project_id']) && $value['project_id'] ? $value['project_id'] : "";

                            $project_name = isset($value['project_name']) && $value['project_name'] ? $value['project_name'] : "";

                            $property_id = isset($value['property_id']) && $value['property_id'] ? $value['property_id'] : "";

                            $property_name = isset($value['property_name']) && $value['property_name'] ? $value['property_name'] : "";

                            $buyer_id = isset($value['buyer_id']) && $value['buyer_id'] ? $value['buyer_id'] : "";

                            $buyer_first_name = isset($value['buyer_first_name']) && $value['buyer_first_name'] ? $value['buyer_first_name'] : "";

                            $buyer_last_name = isset($value['buyer_last_name']) && $value['buyer_last_name'] ? $value['buyer_last_name'] : "";

                            $payment_date = isset($value['payment_date']) && $value['payment_date'] ? view_date($value['payment_date']) : "";
                            ?>
                            <tr>
                                <th>
                                    <a href="/transaction/view/<?= $transaction_id ?>">
                                        <?= $transaction_reference ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="/project/view/<?= $project_id ?>">
                                        <?= $project_name ?>
                                    </a><br>
                                     <a href="/property/view/<?= $property_id ?>">
                                        <?= $property_name ?>
                                    </a>
                                </th>
                                <th>
                                    <?= money_php($value['amount_paid']) ?><br>
                                    <?= $payment_date;?>
                                </th>
                                <th>
                                     <?= get_person_name($value['created_by'],'users') ?>

                                </th>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row px-5 pb-5">
        <div class="col-xl-12">
            <!--begin: Pagination-->
            <div class="kt-pagination kt-pagination--brand">
                <?php echo $this->ajax_pagination->create_links(); ?>

                <div class="kt-pagination__toolbar">
                    <span class="pagination__desc">
                        <?php echo $this->ajax_pagination->show_count(); ?>
                    </span>
                </div>
            </div>
            <!--end: Pagination-->
        </div>
    </div>
</div>