<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calendar_of_events extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('calendar_of_events/Calendar_of_events_model', 'M_calendar_of_events');

        // Load pagination library 
        $this->load->library('ajax_pagination');
        // Per page limit 
        $this->perPage = 12;
    }

    public function index()
    {
        $this->css_loader->queue([
            'vendors/custom/datatables/datatables.bundle.css'
        ]);

        $this->js_loader->queue([
            'vendors/custom/datatables/datatables.bundle.js'
        ]);

        $this->template->build('index', $this->view_data);
    }

    public function get_year_month_events()
    {
        if ($this->input->is_ajax_request()) {
            $post = $this->input->post();

            $year_month = $post['year_month'];
            $first_day_of_month = date($year_month . "-1");
            $last_day_of_month = date($year_month . "-t");
            $days_this_month = cal_days_in_month(CAL_GREGORIAN, explode('-', $year_month)[1], explode('-', $year_month)[0]);

            $this->db->select('created_at')
                ->from('transactions')
                ->where('deleted_at', NULL)
                ->where('created_at >=', $first_day_of_month)
                ->where('created_at <=', $last_day_of_month);

            $encoded_transactions_array = $this->db->get()->result_array();

            $this->db->select('reservation_date')
                ->from('transactions')
                ->where('deleted_at', NULL)
                ->where('reservation_date >=', $first_day_of_month)
                ->where('reservation_date <=', $last_day_of_month);

            $reserved_transactions_array = $this->db->get()->result_array();

            $this->db->select('transaction_official_payments.payment_date as payment_date, transaction_official_payments.amount_paid as amount_paid')
                ->from('transaction_official_payments')
                ->where('transaction_official_payments.deleted_at', NULL)
                ->where('transaction_official_payments.payment_date >=', $first_day_of_month)
                ->where('transaction_official_payments.payment_date <=', $last_day_of_month)
                ->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left')
                ->where('transactions.deleted_at IS NULL');

            $collected_payments_array = $this->db->get()->result_array();

            $this->db->select('transaction_payments.due_date as due_date, transaction_payments.total_amount as total_amount')
                ->from('transaction_payments')
                ->where('transaction_payments.deleted_at', NULL)
                ->where('transaction_payments.due_date >=', $first_day_of_month)
                ->where('transaction_payments.due_date <=', $last_day_of_month)
                ->join('transactions', 'transactions.id = transaction_payments.transaction_id', 'left')
                ->where('transactions.deleted_at IS NULL');

            $receivable_payments_array = $this->db->get()->result_array();

            $this->db->select('transaction_post_dated_checks.due_date as due_date, transaction_post_dated_checks.amount as amount')
                ->from('transaction_post_dated_checks')
                ->where('transaction_post_dated_checks.deleted_at', NULL)
                ->where('transaction_post_dated_checks.due_date >=', $first_day_of_month)
                ->where('transaction_post_dated_checks.due_date <=', $last_day_of_month)
                ->join('transactions', 'transactions.id = transaction_post_dated_checks.transaction_id', 'left')
                ->where('transactions.deleted_at IS NULL');

            $post_dated_checks_array = $this->db->get()->result_array();

            $this->db->select('transaction_post_dated_checks.due_date as due_date, transaction_post_dated_checks.amount as amount')
                ->from('transaction_post_dated_checks')
                ->where('transaction_post_dated_checks.deleted_at', NULL)
                ->where('transaction_post_dated_checks.created_at >=', $first_day_of_month)
                ->where('transaction_post_dated_checks.created_at <=', $last_day_of_month)
                ->join('transactions', 'transactions.id = transaction_post_dated_checks.transaction_id', 'left')
                ->where('transactions.deleted_at IS NULL');

            $encoded_post_dated_checks_array = $this->db->get()->result_array();

            $this->db->select('transaction_official_payments.created_at')
                ->from('transaction_official_payments')
                ->where('transaction_official_payments.deleted_at', NULL)
                ->join('transactions', 'transactions.id = transaction_official_payments.transaction_id', 'left')
                ->where('transactions.deleted_at', NULL)
                ->where('transaction_official_payments.created_at >=', $first_day_of_month)
                ->where('transaction_official_payments.created_at <=', $last_day_of_month);

            $encoded_collections_array = $this->db->get()->result_array();

            $this->db->select('buyers.created_at')
                ->from('buyers')
                ->where('buyers.deleted_at', NULL)
                ->join('transactions', 'transactions.buyer_id = buyers.id', 'left')
                ->where('buyers.deleted_at', NULL)
                ->where('buyers.created_at >=', $first_day_of_month)
                ->where('buyers.created_at <=', $last_day_of_month);

            $encoded_buyers_array = $this->db->get()->result_array();

            // Calendar Events
            $events = [];

            // loop each day of the month
            foreach (range(1, $days_this_month) as $day) {

                $date = date("Y-m-d", strtotime($year_month . "-" . $day));

                $encoded_transaction = $this->filter_by_date($encoded_transactions_array, 'created_at', $date);

                $reserved_transaction = $this->filter_by_date($reserved_transactions_array, 'reservation_date', $date);

                $collected_payments = $this->filter_by_date($collected_payments_array, 'payment_date', $date);

                $collected_payments_amount = 0;

                foreach ($collected_payments as $item) {
                    $collected_payments_amount = $collected_payments_amount + $item['amount_paid'];
                }

                $receivable_payments = $this->filter_by_date($receivable_payments_array, 'due_date', $date);

                $receivable_payments_amount = 0;

                foreach ($receivable_payments as $item) {
                    $receivable_payments_amount = $receivable_payments_amount + $item['total_amount'];
                }

                $post_dated_checks = $this->filter_by_date($post_dated_checks_array, 'due_date', $date);
                $encoded_post_dated_checks = $this->filter_by_date($encoded_post_dated_checks_array, 'due_date', $date);

                $post_dated_checks_amount = 0;
                $encoded_post_dated_checks_amount = 0;

                foreach ($post_dated_checks as $item) {
                    $post_dated_checks_amount = $post_dated_checks_amount + $item['amount'];
                }

                foreach ($encoded_post_dated_checks as $item) {
                    $encoded_post_dated_checks_amount = $encoded_post_dated_checks_amount + $item['amount'];
                }

                $encoded_collections = $this->filter_by_date($encoded_collections_array, 'created_at', $date);

                $encoded_buyers = $this->filter_by_date($encoded_buyers_array, 'created_at', $date);

                array_push($events, [
                    'date' => date("Y-m-d", strtotime($date)),
                    'reserved_transactions' => count($reserved_transaction) ? [
                        'count' => count($reserved_transaction),
                        'amount' => 0
                    ] : false,
                    'encoded_transactions' => count($encoded_transaction) ? [
                        'count' => count($encoded_transaction),
                        'amount' => 0
                    ] : false,
                    'collected_payments' => count($collected_payments) ? [
                        'count' => count($collected_payments),
                        'amount' => number_format($collected_payments_amount, 2, '.', ',')
                    ] : false,
                    'receivable_payments' => count($receivable_payments) ? [
                        'count' => count($receivable_payments),
                        'amount' => number_format($receivable_payments_amount, 2, '.', ',')
                    ] : false,
                    'post_dated_checks' => count($post_dated_checks) ? [
                        'count' => count($post_dated_checks),
                        'amount' => number_format($post_dated_checks_amount, 2, '.', ',')
                    ] : false,
                    'encoded_post_dated_checks' => count($encoded_post_dated_checks) ? [
                        'count' => count($encoded_post_dated_checks),
                        'amount' => number_format($encoded_post_dated_checks_amount, 2, '.', ',')
                    ] : false,
                    'encoded_collections' => count($encoded_collections) ? [
                        'count' => count($encoded_collections),
                        'amount' => 0
                    ] : false,
                    'encoded_buyers' => count($encoded_buyers) ? [
                        'count' => count($encoded_buyers),
                        'amount' => 0
                    ] : false,
                ]);
            }

            echo json_encode($events);
        }
    }

    public function filter_by_date($array, $index, $value)
    {
        $new_array = [];

        if (is_array($array) && count($array) > 0) {
            foreach (array_keys($array) as $key) {

                $temp[$key] = $array[$key][$index];

                if (date("Y-m-d", strtotime($temp[$key])) == $value) {

                    $new_array[$key] = $array[$key];
                }
            }
        }

        return $new_array;
    }

    public function pagination_receivable_payments()
    {
        if ($this->input->is_ajax_request()) {

            $due_date = $this->input->post('due_date');

            // Load pagination library
            $this->load->library('ajax_pagination');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = $this->M_calendar_of_events->due_date_receivable_payments($this->perPage, $offset, ['due_date' => $due_date], false);


            $this->view_data['records'] = $data;

            $totalRec = $this->M_calendar_of_events->due_date_receivable_payments(1, 0, ['due_date' => $due_date], true);

            // Pagination configuration
            $config['target']      = '#datatable_content';
            $config['base_url']    = base_url('calendar_of_events/pagination_receivable_payments');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'receivable_payments_pagination';

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            $this->view_data['total'] = $totalRec;

            $this->view_data['date'] = date("M j, Y", strtotime($due_date));

            $this->load->view('tables/receivable_payments', $this->view_data, false);
        }
    }

    public function pagination_post_dated_checks()
    {
        if ($this->input->is_ajax_request()) {

            $due_date = $this->input->post('due_date');

            // Load pagination library
            $this->load->library('ajax_pagination');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = $this->M_calendar_of_events->due_date_post_dated_checks($this->perPage, $offset, ['due_date' => $due_date], false);


            $this->view_data['records'] = $data;

            $totalRec = $this->M_calendar_of_events->due_date_post_dated_checks(1, 0, ['due_date' => $due_date], true);

            // Pagination configuration
            $config['target']      = '#datatable_content';
            $config['base_url']    = base_url('calendar_of_events/post_dated_checks_pagination');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'post_dated_checks_pagination';

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            $this->view_data['total'] = $totalRec;

            $this->view_data['date'] = date("M j, Y", strtotime($due_date));

            $this->load->view('tables/post_dated_checks', $this->view_data, false);
        }
    }
    public function encoded_pagination_post_dated_checks()
    {
        if ($this->input->is_ajax_request()) {

            $due_date = $this->input->post('due_date');

            // Load pagination library
            $this->load->library('ajax_pagination');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = $this->M_calendar_of_events->encoded_due_date_post_dated_checks($this->perPage, $offset, ['due_date' => $due_date], false);


            $this->view_data['records'] = $data;

            $totalRec = $this->M_calendar_of_events->encoded_due_date_post_dated_checks(1, 0, ['due_date' => $due_date], true);

            // Pagination configuration
            $config['target']      = '#datatable_content';
            $config['base_url']    = base_url('calendar_of_events/encoded_post_dated_checks_pagination');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'encoded_post_dated_checks_pagination';

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            $this->view_data['total'] = $totalRec;

            $this->view_data['date'] = date("M j, Y", strtotime($due_date));

            $this->load->view('tables/encoded_post_dated_checks', $this->view_data, false);
        }
    }

    public function pagination_reserved_transactions()
    {
        if ($this->input->is_ajax_request()) {

            $due_date = $this->input->post('due_date');

            // Load pagination library
            $this->load->library('ajax_pagination');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = $this->M_calendar_of_events->reservation_date_reserved_transactions($this->perPage, $offset, ['due_date' => $due_date], false);


            $this->view_data['records'] = $data;

            $totalRec = $this->M_calendar_of_events->reservation_date_reserved_transactions(1, 0, ['due_date' => $due_date], true);

            // Pagination configuration
            $config['target']      = '#datatable_content';
            $config['base_url']    = base_url('calendar_of_events/pagination_reserved_transactions');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'reserved_transactions_pagination';

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            $this->view_data['total'] = $totalRec;

            $this->view_data['date'] = date("M j, Y", strtotime($due_date));

            $this->load->view('tables/reserved_transactions', $this->view_data, false);
        }
    }

    public function pagination_encoded_transactions()
    {
        if ($this->input->is_ajax_request()) {

            $date = $this->input->post('date');

            // Load pagination library
            $this->load->library('ajax_pagination');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = $this->M_calendar_of_events->encoded_transactions($this->perPage, $offset, ['date' => $date], false);

            $this->view_data['records'] = $data;

            $totalRec = $this->M_calendar_of_events->encoded_transactions(1, 0, ['date' => $date], true);

            // Pagination configuration
            $config['target']      = '#datatable_content';
            $config['base_url']    = base_url('calendar_of_events/pagination_encoded_transactions');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'encoded_transactions_pagination';

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            $this->view_data['total'] = $totalRec;

            $this->view_data['date'] = date("M j, Y", strtotime($date));

            $this->load->view('tables/encoded_transactions', $this->view_data, false);
        }
    }

    public function pagination_collected_payments()
    {
        if ($this->input->is_ajax_request()) {

            $due_date = $this->input->post('due_date');

            // Load pagination library
            $this->load->library('ajax_pagination');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = $this->M_calendar_of_events->payment_date_collected_payments($this->perPage, $offset, ['due_date' => $due_date], false);


            $this->view_data['records'] = $data;

            $totalRec = $this->M_calendar_of_events->payment_date_collected_payments(1, 0, ['due_date' => $due_date], true);

            // Pagination configuration
            $config['target']      = '#datatable_content';
            $config['base_url']    = base_url('calendar_of_events/pagination_collected_payments');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'collected_payments_pagination';

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            $this->view_data['total'] = $totalRec;

            $this->view_data['date'] = date("M j, Y", strtotime($due_date));

            $this->load->view('tables/collected_payments', $this->view_data, false);
        }
    }

    public function pagination_encoded_collections()
    {
        if ($this->input->is_ajax_request()) {

            $date = $this->input->post('date');

            // Load pagination library
            $this->load->library('ajax_pagination');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = $this->M_calendar_of_events->encoded_collections($this->perPage, $offset, ['date' => $date], false);

            $this->view_data['records'] = $data;

            $totalRec = $this->M_calendar_of_events->encoded_collections(1, 0, ['date' => $date], true);

            // Pagination configuration
            $config['target']      = '#datatable_content';
            $config['base_url']    = base_url('calendar_of_events/pagination_encoded_collections');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'encoded_collections_pagination';

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            $this->view_data['total'] = $totalRec;

            $this->view_data['date'] = date("M j, Y", strtotime($date));

            $this->load->view('tables/encoded_collections', $this->view_data, false);
        }
    }

    public function pagination_encoded_buyers()
    {
        if ($this->input->is_ajax_request()) {

            $date = $this->input->post('date');

            // Load pagination library
            $this->load->library('ajax_pagination');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $data = $this->M_calendar_of_events->encoded_buyers($this->perPage, $offset, ['date' => $date], false);

            $this->view_data['records'] = $data;

            $totalRec = $this->M_calendar_of_events->encoded_buyers(1, 0, ['date' => $date], true);

            // Pagination configuration
            $config['target']      = '#datatable_content';
            $config['base_url']    = base_url('calendar_of_events/pagination_encoded_buyers');
            $config['total_rows']  = $totalRec;
            $config['per_page']    = $this->perPage;
            $config['link_func']   = 'encoded_buyers_pagination';

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            $this->view_data['total'] = $totalRec;

            $this->view_data['date'] = date("M j, Y", strtotime($date));

            $this->load->view('tables/encoded_buyers', $this->view_data, false);
        }
    }
}
