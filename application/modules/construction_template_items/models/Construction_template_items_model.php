<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Construction_template_items_model extends MY_Model {
    public $table = 'construction_template_items'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'id',
        'construction_template_id',
        'item_id',
        'unit_of_measurement_id',
        'quantity',
        'unit_cost',
        'total_cost',
        'item_group_id',
        'item_type_id',
        'item_brand_id',
        'item_class_id',
        'stage_id',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        /* ==================== begin: Add model fields ==================== */
        // 'construction_template_id' => array(
        //     'field' => 'construction_template_id',
        //     'label' => 'Construction Template ID',
        //     'rules' => 'trim|required'
        // ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['template'] = array('foreign_model' => 'construction_template/construction_template_model', 'foreign_table' => 'construction_templates', 'foreign_key' => 'id', 'local_key' => 'construction_template_id');
        $this->has_one['item_brand'] = array('foreign_model' => 'item_brand/item_brand_model', 'foreign_table' => 'item_brand', 'foreign_key' => 'id', 'local_key' => 'item_brand_id');
        $this->has_one['item_group'] = array('foreign_model' => 'item_group/item_group_model', 'foreign_table' => 'item_group', 'foreign_key' => 'id', 'local_key' => 'item_group_id');
        $this->has_one['item_type'] = array('foreign_model' => 'item_type/item_type_model', 'foreign_table' => 'item_type', 'foreign_key' => 'id', 'local_key' => 'item_type_id');
        $this->has_one['item_class'] = array('foreign_model' => 'item_class/item_class_model', 'foreign_table' => 'item_class', 'foreign_key' => 'id', 'local_key' => 'item_class_id');
        $this->has_one['item'] = array('foreign_model' => 'item/item_model', 'foreign_table' => 'item', 'foreign_key' => 'id', 'local_key' => 'item_id');
        $this->has_one['unit_of_measurement'] = array('foreign_model' => 'inventory_settings_unit_of_measurement/inventory_settings_unit_of_measurement_model', 'foreign_table' => 'inventory_settings_unit_of_measurements', 'foreign_key' => 'id', 'local_key' => 'unit_of_measurement_id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}