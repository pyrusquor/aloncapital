<?php
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================

?>

<div class="row">
  
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Construction Template</label>
            <div class="kt-input-icon kt-input-icon--left">
                <select name="construction_template_id"  id="construction_template_id" class="suggests form-control" data-module="construction_templates"><option value="0">Select Option</option></select>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Stage Template</label>
            <div class="kt-input-icon kt-input-icon--left">
                <select name="construction_template_stage_id"  id="construction_template_stage_id" class="suggests form-control" data-module="construction_template_stages" data-param="construction_template_id"><option value="0">Select Option</option></select>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_code" value="<?php echo set_value('item_code', $item_code); ?>" placeholder="Item Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('item_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item" value="<?php echo set_value('item', $item); ?>" placeholder="Item" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('item'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Unit of Measurement <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item" value="<?php echo set_value('item', $item); ?>" placeholder="Item" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('item'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Quantity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="quantity" value="<?php echo set_value('quantity', $quantity); ?>" placeholder="Quantity" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('quantity'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Unit Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="unit_price" value="<?php echo set_value('unit_price', $unit_price); ?>" placeholder="Unit Price" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('unit_price'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Sub Total <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="sub_total" value="<?php echo set_value('sub_total', $sub_total); ?>" placeholder="Sub Total" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('sub_total'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Grand Total <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="grand_total" value="<?php echo set_value('grand_total', $grand_total); ?>" placeholder="Grand Total" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('grand_total'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>