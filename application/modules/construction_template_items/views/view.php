<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
// ==================== begin: Add model fields ====================
$construction_template_id = isset($data['construction_template_id']) && $data['construction_template_id'] ? $data['construction_template_id'] : '';
$construction_template = isset($data['template']['name']) && $data['template']['name'] ? $data['template']['name'] : '';
$item_code = isset($data['item_code']) && $data['item_code'] ? $data['item_code'] : '';
$item = isset($data['item']) && $data['item'] ? $data['item'] : '';
$unit_of_measurement = isset($data['unit_of_measurement']) && $data['unit_of_measurement'] ? $data['unit_of_measurement'] : '';
$quantity = isset($data['quantity']) && $data['quantity'] ? $data['quantity'] : '';
$unit_price = isset($data['unit_price']) && $data['unit_price'] ? $data['unit_price'] : '';
$sub_total = isset($data['sub_total']) && $data['sub_total'] ? $data['sub_total'] : '';
$grand_total = isset($data['grand_total']) && $data['grand_total'] ? $data['grand_total'] : '';
// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Construction Template Items</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('construction_template_items/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('construction_template_items');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Construction Template
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $construction_template; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Item Code
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $item_code; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Item
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $item; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Unit of Measurement
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $unit_of_measurement; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Quantity
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $quantity; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Unit Price
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $quantity; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Sub Total
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $sub_total; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Grand Total
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $grand_total; ?></span>
                                </div>

                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
