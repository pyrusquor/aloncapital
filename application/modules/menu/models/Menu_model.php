<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Menu_model extends MY_Model {

    public $table = 'modules'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array('name'); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array('id'); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public function __construct()
    {
        parent::__construct();
    }

    public function load_menu()
    {
        $this->table = 'modules';
        $query = $this->where('parent_id', '0')->where('is_active', '1')->order_by('ctr','ASC')->as_array()->get_all();
        return $query;
    }

    public function load_submenu($parent_id)
    {
        $this->table = 'modules';
        $query = $this->where('parent_id', $parent_id)->where('is_active', '1')->order_by('ctr','ASC')->as_array()->get_all();
        return $query;
    }

    public function has_child($parent_id)
    {
        $this->table = 'modules';
        $query = $this->where('parent_id', $parent_id)->where('is_active', '1')->as_array()->get_all();
        // echo $this->db->last_query(); 
        if(!$query)
        {
            $query = array();
        }
        return count(array_filter($query));
    }

    public function has_active_child($parent_id, $parent_page)
    {
        $this->table = 'modules';
        $query = $this->where('parent_id', $parent_id)->where('is_active', '1')->where('link', $parent_page)->as_array()->get_all();
        // echo $this->db->last_query(); exit;
        if(!$query)
        {
            $query = array();
        }
        return count(array_filter($query));
    }

    public function has_access($group_id, $module_id)
    {
        $this->table = 'group_access';
        $query = $this->where('group_id', $group_id)->where('module_id', $module_id)->as_array()->get_all();
        if(!$query)
        {
            $query = array();
        }
        return count(array_filter($query));
    }

    public function has_permission($module_id) 
    {   
        $user_id = $this->session->userdata['user_id'];

        $this->table = 'user_permissions';
        $permission = $this->where('user_id', $user_id)->get('permission_id');
        
        $this->table = 'module_permissions';
        $query = $this->where('permission_id', $permission['permission_id'])->where('module_id', $module_id)->where('is_read', 1)->get();
        
        // if ($module_id == 2) {
        //     vdebug($query);
        // }

        if($query) 
        {
            return 1;
        }
        return 0;
    }

    public function get_parent_id($parent_page)
    {
        $this->table = 'modules';
        $query = $this->where('link', $parent_page)->where('is_active', '1')->as_array()->get_all();
        return $query;
    }
    
}