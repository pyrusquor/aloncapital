<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$value = isset($info['value']) && $info['value'] ? $info['value'] : '';
$value_type = isset($info['value_type']) && $info['value_type'] ? $info['value_type'] : '';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="name"
                   value="<?php echo set_value('name', $name); ?>" placeholder="Name"
                   autocomplete="off" id="name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Value <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="value"
                   value="<?php echo set_value('value', $value); ?>" placeholder="Value"
                   autocomplete="off" id="value">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Value Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('value_type', Dropdown::get_static('app_value_types'), set_value('value_type', @$value_type), 'class="form-control" id="value_type"'); ?>
            <?php echo form_error('value_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>