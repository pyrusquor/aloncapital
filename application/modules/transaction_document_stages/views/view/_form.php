<?php
$name = isset($transaction_document_stages['name']) && $transaction_document_stages['name'] ? $transaction_document_stages['name'] : '';
$order_by = isset($transaction_document_stages['order_by']) && $transaction_document_stages['order_by'] ? $transaction_document_stages['order_by'] : '';
$description = isset($transaction_document_stages['description']) && $transaction_document_stages['description'] ? $transaction_document_stages['description'] : '';
$start_at = isset($transaction_document_stages['start_at']) && $transaction_document_stages['start_at'] ? $transaction_document_stages['start_at'] : '';
$category_id = isset($transaction_document_stages['category_id']) && $transaction_document_stages['category_id'] ? $transaction_document_stages['category_id'] : '';
$count_of_days = isset($transaction_document_stages['count_of_days']) && $transaction_document_stages['category_id'] ? $transaction_document_stages['count_of_days'] : '';
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Category  <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                 <select class="form-control" id="category_id" name="category_id">
                    <option  <?php echo ($category_id == 0) ?  "selected": ""; ?> value="0">Select Option</option>
                    <option  <?php echo ($category_id == 1) ?  "selected": ""; ?> value="1">Lots Only </option>
                    <option  <?php echo ($category_id == 2) ?  "selected": ""; ?> value="2">Downpayment </option>
                    <option  <?php echo ($category_id == 3) ?  "selected": ""; ?> value="3">TCT </option>
                </select>
            </div>
            <?php echo form_error('category_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            <label>Start At <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select class="form-control" id="start_at" name="start_at">
                    <option <?php echo ($start_at == 0) ?  "selected": ""; ?>  value="0">Select Option</option>
                    <option <?php echo ($start_at == 1) ?  "selected": ""; ?> value="1">Reservation</option>
                    <option <?php echo ($start_at == 2) ?  "selected": ""; ?> value="2">Start of DP</option>
                    <option <?php echo ($start_at == 3) ?  "selected": ""; ?> value="3">End of DP</option>
                    <option <?php echo ($start_at == 4) ?  "selected": ""; ?> value="4">Start of Loan</option>
                    <option <?php echo ($start_at == 5) ?  "selected": ""; ?> value="5">Construction Completion</option>
                    <option <?php echo ($start_at == 6) ?  "selected": ""; ?> value="6">Full Payment</option>
                </select>
            </div>
            <?php echo form_error('start_at'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>



    <div class="col-sm-6">
        <div class="form-group">
            <label>End At (Days) <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="count_of_days" value="<?php echo set_value('count_of_days', $count_of_days); ?>" placeholder="End At (Days)" autocomplete="off">
            </div>
            <?php echo form_error('count_of_days'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Order By <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="number" class="form-control" name="order_by" value="<?php echo set_value('order_by', $order_by); ?>" placeholder="Order By" autocomplete="off">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('order_by'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Description <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
                <span class="kt-input-icon__icon"><span></span>
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>