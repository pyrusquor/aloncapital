<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Commission_setup_values_model extends MY_Model
{
    public $table = 'commission_setup_values'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'commission_id',
        'period_id',
        'percentage_to_finish',
        'commission_rate_amount',
        'commission_rate_percentage',
        'commission_term',
        'commission_vat',
        'is_cash_advance',
        'is_active',
        'created_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_one['warehouse_name'] = array('foreign_model' => 'Warehouse_model', 'foreign_table' => 'warehouse', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    // public function insert_dummy()
    // {
    //     require APPPATH . '/third_party/faker/autoload.php';
    //     $faker = Faker\Factory::create();

    //     $data = [];

    //     for ($x = 0; $x < 10; $x++) {
    //         array_push($data, array(
    //             'sub_warehouse_name' => $faker->word,
    //             'sub_warehouse_code' => $faker->word,
    //             'warehouse_id' => $faker->numberBetween(1, 10),
    //             'address' => $faker->word,
    //             'telephone_number' => $faker->numberBetween(10000000, 99999999),
    //             'fax_number' => $faker->numberBetween(10000000, 99999999),
    //             'mobile_number' => $faker->numberBetween(10000000, 99999999),
    //             'email_address' => $faker->word,
    //             'contact_person' => $faker->word,
    //             'is_active' => $faker->numberBetween(0, 1),
    //         ));
    //     }
    //     $this->db->insert_batch($this->table, $data);

    // }
}
