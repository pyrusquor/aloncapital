<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Generic_api extends MY_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper(['db', 'material_request']);

            $this->user_id = @$this->user->id;
            if (!$this->user_id) die('Unauthorized');

        }

        public function index()
        {

        }

        public function fetch_specific()
        {
            $table = $this->input->get('table');
            $field = $this->input->get('field');
            $value = $this->input->get('value');
            $order = $this->input->get('order');
            $start = $this->input->get('start');
            $limit = $this->input->get('limit');
            $select = isset($_GET['select']) ? $_GET['select'] : null;

            $order = $order ? $order : 'id DESC';
            $start = $start ? $start : null;
            $limit = $limit ? $limit : null;

            $data = get_objects_from_table_by_field($table, $field, $value, $order, $start, $limit, $select)->result();
            header('Content-Type: application/json');
            echo json_encode($data);
        }

        public function fetch_all()
        {
            $table = $this->input->get('table');
            $order = $this->input->get('order');
            $select = isset($_GET['select']) ? $_GET['select'] : null;

            $order = $order ? $order : 'name ASC';

            if (!$select) {
                $default_fields = default_table_fields($table);
                if ($default_fields) {
                    $this->db->select($default_fields);
                }
            } else {
                $this->db->select($select);
            }

            $this->db->where('deleted_at is NULL');
            $this->db->from($table);
            $this->db->order_by($order);
            $data = $this->db->get()->result();
            header('Content-Type: application/json');
            echo json_encode($data);
        }

        public function check_credentials()
        {
            /*
             * Reference: auth/Ion_auth_model
             */
            $identity = $this->input->post('identity');
            $password = $this->input->post('password');
            $validate_against = $this->input->post('user_id');
            $validate_against = $validate_against ? $validate_against : null;

            $this->load->model('auth/Ion_auth_model', 'M_Ion');
            if (empty($identity) || empty($password)) {
                return FALSE;
            }
            $query = $this->db->select($this->M_Ion->identity_column . ', email, username, first_name, last_name, users.id, password, users_groups.group_id, user_permissions.permission_id, active, last_login')
                ->where($this->M_Ion->identity_column, $identity)
                ->limit(1)
                ->order_by('users.id', 'desc')
                ->join('users_groups', 'users_groups.user_id = users.id')
                ->join('user_permissions', 'user_permissions.user_id = users.id', 'left')
                ->get($this->M_Ion->tables['users']);

            if ($query->num_rows() === 1) {
                $user = $query->row();
                if ($validate_against) {
                    if ($user->id != $validate_against) {
                        $result = FALSE;
                        $message = "Validator mismatch! Expecting " . $validate_against . ", got " . $user->id;
                    }
                }

                if ($this->M_Ion->verify_password($password, $user->password, $identity)) {
                    $result = TRUE;
                    $message = "Validated";
                } else {
                    $result = FALSE;
                    $message = "Invalid credentials";
                }
            } else {
                $result = FALSE;
                $message = "User does not exist";
            }

            header('Content-Type: application/json');
            echo json_encode(array(
                "status" => $result,
                "message" => $message
            ));
        }

        public function authorized_update()
        {
            $table = $this->input->post('table');
            $id = $this->input->post('id');
            $field = $this->input->post('field');
            $value = $this->input->post('value');
            $actor = $this->input->post('actor');

            $row = get_objects_from_table_by_field($table, 'id', $id)->row_array();

            $process = false;

            if ($actor === 'approving_staff_id' || $actor === 'requesting_staff_id') {
                $actor_id = get_staff_id($this->user->id);
            } else {
                $actor_id = $this->user->id;
            }

            if ($actor) {
                if ($row[$actor] === $actor_id) {
                    $process = set_field($table, $field, $value, $id, current_user_id());
                }
            } else {
                $process = set_field($table, $field, $value, $id, current_user_id());
            }
            if ($process) {
                $result = json_encode(array(
                    "status" => true,
                    "message" => "Success"
                ));
            } else {
                $result = json_encode(array(
                    "status" => false,
                    "message" => "Fail"
                ));
            }
            header('Content-Type: application/json');
            echo $result;

        }

        public function expense_ledgers()
        {
            $order_by = $this->input->get('order_by');
            $order_by = $order_by ? $order_by : 'name ASC';

            $this->load->helper('accounting_db');
            $expense_ledgers = get_expense_ledgers($order_by);

            header('Content-Type: application/json');
            echo json_encode($expense_ledgers);

        }

        public function helper_lookup()
        {
            $helper = $this->input->get('helper');
            $func = $this->input->get('func');
            $value = $this->input->get('value');

            $this->load->helper($helper);

            header('Content-Type: application/json');
            echo json_encode($func($value));
        }

        public function test()
        {


        }
    }