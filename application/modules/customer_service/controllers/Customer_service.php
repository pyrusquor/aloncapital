<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_service extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $payment_voucher_models = ['Customer_service_model' => 'M_Customer_service',
                                'Customer_service_log_model' => 'M_Customer_service_log',
                                'Customer_service_note_log_model' => 'M_customer_service_note_log'];

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model($payment_voucher_models);

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper
        $this->load->helper('form');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Customer_service->fillable;
        $this->_table_columns = $this->M_Customer_service->__get_columns();
        $this->additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW
        ];
        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW
        ];
    }

    public function template($page = "")
    {

        $this->template->build($page);
    }

    public function index($debug = 0)
    {
        $_fills = $this->_table_fillables;
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'ref' => true,
            'project_id' => true,
            'property_id' => true,
            'house_model_id' => true,
            'buyer_id' => true,
            'turn_over_date' => true,
            'warranty_date' => true,
            'category_id' => true,
            'department_id' => true,
            'warranty' => true,
            'created_at' => true,
            'updated_at' => true,
            'status' => true,
            'sub_status' => true,
            'forwarded_status' => true,
            'created_by' => true,
            'updated_by' => true,
            'status_id' => true,
            'notes' => true,
            'sub_status_id' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Customer_service
                ->with_project('fields:name')
                ->with_property('fields:name')
                ->with_house_model('fields:name')
                ->with_buyer('fields:first_name,middle_name,last_name')
                ->with_department('fields:name')
                ->with_inquiry_category('fields:name')
                // ->with_inquiry_sub_category('fields:name')
                // ->with_inquiry_inquiry_logs()
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("reference like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'house_model_id' => [
                    'data' => $filters['house_model_id'] ?? null,
                    'operator' => '=',
                ],
                'department_id' => [
                    'data' => $filters['department_id'] ?? null,
                    'operator' => '=',
                ],
                'status' => [
                    'data' => $filters['status'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // vdebug($items);

                    // Transform data
                    foreach ($items as $key => $value) {

                        $transaction = "<a target='_BLANK'  target='_BLANK' href='/transaction/view/" . $value['transaction_id'] . "'>" . get_value_field($value['transaction_id'], 'transactions', 'reference') . "</a>";

                        $items[$key]['reference'] = isset($value['reference']) ? "<a href='/customer_service/view/" . $value['id'] . "'>" . $value['reference'] . "</a>" : 'N/A';

                        $items[$key]['ref'] = isset($value['reference']) ? $value['reference'] : 'N/A';


                        $items[$key]['project_id'] = isset($value['project']) ? "<a href='/project/view/" . $value['project']['id'] . "'>" . $value['project']['name'] . "</a>" : 'N/A';
                        $items[$key]['property_id'] = isset($value['property']) ? "<a href='/property/view/" . $value['property']['id'] . "'>" . $value['property']['name'] . "</a>" : 'N/A';
                        $items[$key]['buyer_id'] = ($value['first_name'] . ' ' . $value['last_name']) == ' ' ? 'N/A' : $value['first_name'] . ' ' . $value['last_name'];
                        $items[$key]['house_model_id'] = isset($value['house_model']) ? "<a href='/house/view/" . $value['house_model']['id'] . "'>" . $value['house_model']['name'] . "</a>" : 'N/A';
                        $items[$key]['department_id'] = isset($value['department']) ? "<a href='/department/view/" . $value['department']['id'] . "'>" . $value['department']['name'] . "</a>" : 'N/A';

                        $items[$key]['category_id'] = isset($value['category_id']) ? "<a href='/inquiry_categories/view/" . $value['category_id'] . "'>" . Dropdown::get_static('customer_service_category', $value['category_id']) . "</a>" : 'N/A';

                        $items[$key]['turn_over_date'] = view_date($value['turn_over_date']);
                        $items[$key]['warranty_date'] = $value['warranty_date'] ? view_date($value['warranty_date']) : "";
                        $items[$key]['created_at'] = view_date($value['created_at']);
                        $items[$key]['updated_at'] = $value['updated_at'] ? view_date($value['updated_at']) : '';
                        
                        if(strlen($value['notes'])>40){
                            $id = $value['id'];
                            $note_string = substr_replace($value['notes'], "<span data-id='$id' class='more'>", 40, 0);
                            $l_post = strlen($note_string);
                            $note_string = substr_replace($note_string, "</span><a href='javascript:void(0);' data-id='$id' class='ellipsis'> Show More</a>", $l_post, 0);
                            $items[$key]['notes'] = $note_string;
                        }

                        $items[$key]['status'] = $value['status'] ? Dropdown::get_static('customer_service_status', $value['status']) : '';

                        $items[$key]['status_id'] = $value['status'];
                        $items[$key]['sub_status_id'] = $value['sub_status'];


                        $items[$key]['sub_status'] = $value['sub_status'] ? Dropdown::get_static('customer_service_sub_status', $value['sub_status']) : '';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Customer_service->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            $project_id = $this->input->post('project_id');
            $property_id = $this->input->post('property_id');
            $house_model_id = $this->input->post('house_model_id');
            $buyer_id = $this->input->post('buyer_id');
            $department_id = $this->input->post('department_id');
            $turn_over_date = $this->input->post('turn_over_date');
            $inquiry_category_id = $this->input->post('inquiry_category_id');
            $inquiry_sub_category_id = $this->input->post('inquiry_sub_category_id');
            $page = $this->input->post('page');

            if (!empty($turn_over_date)) {
                $date_range = explode('-', $turn_over_date);

                $from_str   = strtotime($date_range[0]);
                $from       = date('Y-m-d H:i:s', $from_str);

                $to_str     = strtotime($date_range[1] . '23:59:59');
                $to         = date('Y-m-d H:i:s', $to_str);
            }

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_Customer_service->count_rows();
            //            $where = array();

            // Pagination configuration
            $config['target'] = '#inquiry_ticketContent';
            $config['base_url'] = base_url('customer_service/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'InquiryTicketPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($house_model_id)) :
                $this->db->group_start();
                $this->db->where('house_model_id', $house_model_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_id)) :
                $this->db->group_start();
                $this->db->where('buyer_id', $buyer_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($department_id)) :
                $this->db->group_start();
                $this->db->where('department_id', $department_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($turn_over_date)) :
                $this->db->group_start();
                $this->db->where('turn_over_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            if (!empty($inquiry_category_id)) :
                $this->db->group_start();
                $this->db->where('inquiry_category_id', $inquiry_category_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($inquiry_sub_category_id)) :
                $this->db->group_start();
                $this->db->where('inquiry_sub_category_id', $inquiry_sub_category_id, 'both');
                $this->db->group_end();
            endif;

            $totalRec = $this->M_Customer_service->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($house_model_id)) :
                $this->db->group_start();
                $this->db->where('house_model_id', $house_model_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_id)) :
                $this->db->group_start();
                $this->db->where('buyer_id', $buyer_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($department_id)) :
                $this->db->group_start();
                $this->db->where('department_id', $department_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($turn_over_date)) :
                $this->db->group_start();
                $this->db->where('turn_over_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            if (!empty($inquiry_category_id)) :
                $this->db->group_start();
                $this->db->where('inquiry_category_id', $inquiry_category_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($inquiry_sub_category_id)) :
                $this->db->group_start();
                $this->db->where('inquiry_sub_category_id', $inquiry_sub_category_id, 'both');
                $this->db->group_end();
            endif;

            $this->view_data['records'] = $records = $this->M_Customer_service
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_inquiry_category()
                ->with_inquiry_sub_category()
                ->order_by('id', 'DESC')
                ->limit($this->perPage, $offset)
                ->get_all();

            $this->load->view('customer_service/_filter', $this->view_data, false);
        }
    }

    public function view($id = false, $type = '')
    {
        if ($id) {

            $this->view_data['info'] = $info = $this->M_Customer_service
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_category()
                ->with_inquiry_category()
                ->with_inquiry_sub_category()
                ->with_customer_service_logs()
                ->with_note_logs(['order_inside' => 'created_at'])
                ->get($id);

            if ($this->view_data['info']) {

                if ($type == "debug") {

                    vdebug($this->view_data);
                }

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function test(){
        vdebug(uniqidIT());
    }

    public function form($id = false, $debug = 0)
    {
        $related_ticket_id = $this->input->get('ticket_id') != null ? $this->input->get('ticket_id') : null;

        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $photo = $_FILES['image'] ?? null;

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Customer_service->fields);

            if ($this->form_validation->run() === true) {

                $oof = $this->input->post();

                unset($oof['image']);

                //get the status of the current inquiry ticket
                $status = $this->M_Customer_service->get($id)['status'];

                //if the inquiry ticket is closed, cancelled or completed, the method will create a new ticket with the data from the old ticket
                if ($id && $status < 5) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $this->M_Customer_service->update($oof + $additional, $id);
                    $this->db->order_by('created_by','DESC');
                    $this->db->where('customer_service_id', $id);
                    $latest_log = $this->M_customer_service_note_log->get();

                    if($latest_log){
                        if($oof['notes']!=$latest_log['note']){
                            $note_info = [
                                'customer_service_id' => $id,
                                'note' => $oof['notes']
                            ];
                            $this->M_customer_service_note_log->insert($note_info + $this->additional);
                        }
                    }
                } else {


                    $w['project_id'] = $oof['project_id'];
                    $w['property_id'] = $oof['property_id'];
                    $w['buyer_id'] = $oof['buyer_id'];

                    $oof['transaction_id'] = get_value_field_multiple($w, 'transactions', 'id');

                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                        'reference' => uniqidIT($oof['ticket_date']),
                        'status' => 1,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    $customer_service_id = $this->M_Customer_service->insert($oof + $additional);
                    if($customer_service_id && $oof['notes']){
                        $note_info['note'] = $oof['notes'];
                        $note_info['customer_service_id'] = $customer_service_id;
                        $this->M_customer_service_note_log->insert($note_info + $this->additional);
                    }
                }

                $this->db->trans_complete(); # Completing customer_service

                if (!!$photo['name']) {

                    $msg = $this->uploadImage($customer_service_id);

                    if ($msg) {

                        $response['msg'] = $msg;
                    }
                }

                /*Optional*/
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $response['status'] = 1;
                    $response['message'] = 'Inquiry ticket Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;

        $id = $related_ticket_id ? $related_ticket_id : $id;

        if ($id) {

            $this->view_data['info'] = $this->M_Customer_service
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_category()
                ->with_inquiry_category()
                ->with_inquiry_sub_category()
                ->get($id);
        }

        if ($debug) {
            vdebug($this->view_data);
        }
        $this->template->build('form', $this->view_data);
    }

    private function uploadImage($model_id)
    {
        $_location = 'assets/uploads/customer_service/';

        // Create dir if does not exist
        if (!file_exists($_location)) {

            mkdir($_location, 0777, true);
        }

        $model = $this->M_Customer_service->get($model_id);

        if (file_exists($model['file_path'])) {

            unlink($model['file_path']);
        }

        $_config['upload_path'] = $_location;
        $_config['allowed_types'] = '*';
        $_config['overwrite'] = TRUE;
        $_config['max_size'] = '1000000';
        $_config['file_name'] = $model_id;

        $this->load->library('upload', $_config);

        if (!$this->upload->do_upload('image')) {

            return $this->upload->display_errors() ?? null;
        } else {

            // vdebug($this->upload->data());

            $photo_info['file_path'] = $_location . $this->upload->data('file_name');

            $this->db->where('id', $model_id);
            $this->db->update('customer_services', $photo_info);

            return null;
        }
    }

    public function generate_transactions()
    {
        // code...
        $rows = $this->M_Customer_service->get_all();

        if ($rows) {
            foreach ($rows as $key => $row) {

                $id = $row['id'];

                $w['project_id'] = $row['project_id'];
                $w['property_id'] = $row['property_id'];
                $w['buyer_id'] = $row['buyer_id'];

                $oof['transaction_id'] = get_value_field_multiple($w, 'transactions', 'id');

                $this->M_Customer_service->update($oof, $id);
            }
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Customer_service->get($id);
            if ($list) {

                $deleted = $this->M_Customer_service->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Inquiry ticket successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Customer_service->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Ticket successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            // $this->view_data['info'] = $info = $this->M_Customer_service->with_requests()->with_payable_items()->with_entry_items()->with_item()->as_array()->get($id);

            $this->view_data['info'] = $info = $this->M_Customer_service
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_inquiry_category()
                ->with_inquiry_sub_category()
                ->get($id);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }

    public function change_forward_status(){
        $response['status'] = '0';
        $response['message'] = 'Change status failed!';
        $post = $this->input->post();
        if($post){
            $id = $post['id'];
            unset($post['id']);
            if($post['forwarded_status']=='2'){
                $post['forward_completion_date'] = NOW;
            }
            $post = $post +  $this->u_additional;
            $status = $this->M_Customer_service_log->update($post, $id);
            if($status){
                $response['status'] = '1';
                $response['message'] = 'Change status successful!';
            }
        }
        echo json_encode($response);
        exit();
    }

    public function forward_status()
    {
        $customer_service_id = $this->input->post('customer_service_id');
        $status_id = $this->input->post('status_id');
        $category_id = $this->input->post('category_id');
        $remarks = "Fowarded";

        if ($customer_service_id) {
            $info = [
                'customer_service_id' => $customer_service_id,
                'forwarded_to' => $category_id,
                'remarks' => $remarks,
            ];

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            $update = [
                'category_id' => $category_id,
                'sub_status' => $status_id,
                'forwarded_to' => $category_id,
                'forwarded_on' => NOW,
                'updated_by' => $this->user->id,
                'updated_at' => NOW,
            ];

            // Insert inquiry ticket log
            $customer_service_log_id = $this->M_Customer_service_log->insert($info + $additional);

            $this->M_Customer_service->update($update, $customer_service_id);

            if ($customer_service_log_id) {
                $response['status'] = 1;
                $response['message'] = $remarks;
            } else {
                $response['status'] = 0;
                $response['message'] = 'Error! 1';
            }
        } else {
            $response['status'] = 0;
            $response['message'] = 'Error! 2';
        }

        echo json_encode($response);
        exit();
    }

    public function update_status()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';
        $customer_service_id = $this->input->post('customer_service_id');
        $status = $this->input->post('status_id');
        $remarks = $this->input->post('remarks');

        // $image = $this->input->post('image');

        if ($customer_service_id) {

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            // Close id on Dropdown
            if ($status == 2) {

                $info = [
                    'customer_service_id' => $customer_service_id,
                    'remarks' => $remarks,
                ];

                $update = [
                    'status' => $status,
                    'closed_on' => NOW,
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];
            } else {

                $info = [
                    'customer_service_id' => $customer_service_id,
                    'remarks' => $remarks,
                ];

                $update = [
                    'sub_status' => $status,
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];
            }


            // Insert inquiry ticket log
            $customer_service_log_id = $this->M_Customer_service_log->insert($info + $additional);

            $this->M_Customer_service->update($update, $customer_service_id);

            if ($customer_service_log_id) {
                $response['status'] = 1;
                $response['message'] = $remarks;
            } else {
                $response['status'] = 0;
                $response['message'] = 'Error!';
            }
        } else {
            $response['status'] = 0;
            $response['message'] = 'Error!';
        }

        echo json_encode($response);
        exit();
    }

    public function update_notes(){
        $response['status'] = 0;
        $response['message'] = 'Failed to update notes!';

        $post = $this->input->post();
        if($post){
            $id = $this->input->post('customer_service_id');
            $notes = $this->input->post('notes');
            $update_data = [
                'notes' => $notes
            ];
            $note_data = [
                'customer_service_id' => $id,
                'note' => $notes
            ];
            $customer_service_transaction_status = $this->M_Customer_service->update($update_data,$id);
            $this->M_customer_service_note_log->insert($note_data + $this->additional);
            if($customer_service_transaction_status){
                $response['status'] = 1;
                $response['message'] = 'Successfully updated notes!';
            }
        }
        echo json_encode($response);
        exit();
    }

    public function export()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $inquiries    =    $this->M_Customer_service
        ->with_transaction('fields:reference')
        ->with_project('fields:name')
        ->with_property('fields:name')
        ->with_house_model('fields:name')
        ->with_buyer('fields:first_name,middle_name,last_name')
        ->with_department('fields:name')
        ->get_all();

        if ($inquiries) {

            foreach ($inquiries as $_lkey => $inquiry) {

                $_datas[$inquiry['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_inquiries_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
                }
            } else {

                $_filename    =    'list_of_inquiries' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;

                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
                }
            }

            foreach ($_db_columns as $key => $value) {

                if ('file_path' == $value) {

                    unset($_db_columns[$key]);
                } else if ('warranty_date' == $value) {

                    unset($_db_columns[$key]);
                } else if ('created_by' == $value) {

                    unset($_db_columns[$key]);
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($inquiries as $_lkey => $inquiry) {

                                if ($_name == "buyer") {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_name]) && ($inquiry[$_name] !== '') ? $inquiry[$_name]['first_name'] . " " . $inquiry[$_name]['last_name'] : '';
                                } else if ($_name == "transaction") {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_name]) && ($inquiry[$_name] !== '') ? $inquiry[$_name]['reference'] : '';
                                }else if ($_name == "category") {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_column]) && ($inquiry[$_column] !== '') ? Dropdown::get_static('customer_service_category', $inquiry[$_column], 'vview') : '';
                                } else {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_name]) && ($inquiry[$_name] !== '') ? $inquiry[$_name]['name'] : '';
                                }
                            }
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($inquiries as $_lkey => $inquiry) {

                                $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_column]) && ($inquiry[$_column] !== '') ? Dropdown::get_static('bool', $inquiry[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($inquiries as $_lkey => $inquiry) {

                                if ($_name === 'status') {

                                    $_datas[$inquiry['id']][$_name] = isset($inquiry[$_name]) && $inquiry[$_name] ? Dropdown::get_static('customer_service_status', $inquiry[$_name], 'view') : '';
                                } else if ($_name === 'sub_status') {

                                    $_datas[$inquiry['id']][$_title] = isset($inquiry[$_name]) && $inquiry[$_name] ? Dropdown::get_static('customer_service_sub_status', $inquiry[$_name], 'view') : '';
                                } else if ($_name === 'forwarded_status') {

                                    $_datas[$inquiry['id']][$_title] = isset($inquiry[$_name]) && $inquiry[$_name] ? Dropdown::get_static('customer_service_forward_status', $inquiry[$_name], 'view') : '';
                                } else if ($_name === 'warranty') {

                                    $_datas[$inquiry['id']][$_name] = Dropdown::get_static('bool', $inquiry[$_name], 'view') ?? '';
                                } else if ($_name === 'turn_over_date') {

                                    $_datas[$inquiry['id']][$_title] = $inquiry[$_name] != "0000-00-00 00:00:00" ? (view_date(substr($inquiry[$_name], 0, 10)) ?? '') : '';
                                } else if ($_name === 'forwarded_on') {

                                    $_datas[$inquiry['id']][$_title] = $inquiry[$_name] != "0000-00-00 00:00:00" ? (view_date(substr($inquiry[$_name], 0, 10)) ?? '') : '';
                                } else if ($_name === 'forward_completion_date') {

                                    $_datas[$inquiry['id']][$_title] = $inquiry[$_name] != "0000-00-00 00:00:00" ? (view_date(substr($inquiry[$_name], 0, 10)) ?? '') : '';
                                } else if ($_name === 'created_at') {

                                    $_datas[$inquiry['id']][$_title] = $inquiry[$_name] != "0000-00-00 00:00:00" ? (view_date(substr($inquiry[$_name], 0, 10)) ?? '') : '';
                                } else if ($_name === 'closed_on') {

                                    $_datas[$inquiry['id']][$_title] = $inquiry[$_name] != "0000-00-00 00:00:00" ? (view_date(substr($inquiry[$_name], 0, 10)) ?? '') : '';
                                } else {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_name]) && $inquiry[$_name] ? $inquiry[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                // array_push($_titles, 'started', 'forwarded', 'work in progress', 'completed', 'closed');

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of inquiries');
                $_objSheet->setCellValue('A1', 'LIST OF inquiries');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');
                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {
                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';
                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
            }
        } else {

            $this->notify->error('No Record Found', 'customer_care');
        }
    }
}
