<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_service_model extends MY_Model
{
    public $table = 'customer_services'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'reference',
        'landline',
        'mobile',
        'email',
        'project_id',
        'property_id',
        'phase',
        'block',
        'lot',
        'house_model_id',
        'buyer_id',
        'first_name',
        'last_name',
        'department_id',
        'turn_over_date',
        'ticket_date',
        // 'warranty_date',
        'warranty',
        'category_id',
        'description',
        'notes',
        'status',
        'sub_status',
        'closed_on',
        'forwarded_to',
        'forwarded_on',
        'forwarded_status',
        'forward_completion_date',
        'file_path',
        'transaction_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        /* ==================== begin: Add model fields ==================== */

        'reference' => array(
            'field' => 'reference',
            'label' => 'Reference',
            'rules' => 'trim'
        ),
        // 'project_id' => array(
        //     'field' => 'project_id',
        //     'label' => 'Project',
        //     'rules' => 'trim|required'
        // ),
        // 'property_id' => array(
        //     'field' => 'property_id',
        //     'label' => 'Property',
        //     'rules' => 'trim|required'
        // ),
        'department_id' => array(
            'field' => 'department_id',
            'label' => 'Department',
            'rules' => 'trim'
        ),
        'turn_over_date' => array(
            'field' => 'turn_over_date',
            'label' => 'Turn Over Date',
            'rules' => 'trim'
        ),
        'warranty' => array(
            'field' => 'warranty',
            'label' => 'Warranty',
            'rules' => 'trim'
        ),
        'category_id' => array(
            'field' => 'category_id',
            'label' => 'Category',
            'rules' => 'trim'
        ),
        // 'inquiry_sub_category_id' => array(
        //     'field' => 'inquiry_sub_category_id',
        //     'label' => 'Inquiry Sub Category',
        //     'rules' => 'trim'
        // ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim'
        ),
        'image' => array(
            'field' => 'image',
            'label' => 'Supporting Image',
            'rules' => 'trim'
        ),
        'status' => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
        $this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');
        $this->has_one['house_model'] = array('foreign_model' => 'house/house_model', 'foreign_table' => 'house_models', 'foreign_key' => 'id', 'local_key' => 'house_model_id');
        $this->has_one['buyer'] = array('foreign_model' => 'buyer/buyer_model', 'foreign_table' => 'buyers', 'foreign_key' => 'id', 'local_key' => 'buyer_id');
        $this->has_one['category'] = array('foreign_model' => 'department/department_model', 'foreign_table' => 'departments', 'foreign_key' => 'id', 'local_key' => 'department_id');
        $this->has_many['note_logs'] = array('foreign_model' => 'Customer_service_note_log_model', 'foreign_table' => 'customer_service_note_logs', 'foreign_key' => 'customer_service_id', 'local_key' => 'id');
        $this->has_many['customer_service_logs'] = array('foreign_model' => 'customer_service_log_model', 'foreign_table' => 'customer_service_logs', 'foreign_key' => 'customer_service_id', 'local_key' => 'id');
        $this->has_one['transaction'] = array('foreign_model' => 'transaction/transaction_model', 'foreign_table' => 'transactions', 'foreign_key' => 'id', 'local_key' => 'transaction_id');
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
