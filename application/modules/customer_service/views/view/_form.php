<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
$property_id = isset($info['property_id']) && $info['property_id'] ? $info['property_id'] : '';
$house_model_id = isset($info['house_model_id']) && $info['house_model_id'] ? $info['house_model_id'] : '';
$buyer_id = isset($info['buyer_id']) && $info['buyer_id'] ? $info['buyer_id'] : '';
$department_id = isset($info['department_id']) && $info['department_id'] ? $info['department_id'] : '';
$turn_over_date = isset($info['turn_over_date']) && $info['turn_over_date'] && $info['turn_over_date']!='0000-00-00 00:00:00' ? date('Y-m-d', strtotime($info['turn_over_date'])) : '';
$warranty_date = isset($info['warranty_date']) && $info['warranty_date'] ? date('Y-m-d', strtotime($info['warranty_date'])) : '';
$warranty = isset($info['warranty']) && $info['warranty'] ? $info['warranty'] : '';
$category_id = isset($info['category_id']) && $info['category_id'] ? $info['category_id'] : '';
// $inquiry_sub_category_id = isset($info['inquiry_sub_category_id']) && $info['inquiry_sub_category_id'] ? $info['inquiry_sub_category_id'] : '';
$description = isset($info['description']) && $info['description'] ? $info['description'] : '';
$file_path = isset($info['file_path']) && $info['file_path'] ? $info['file_path'] : '';
$status = isset($info['status']) && $info['status'] ? $info['status'] : '';
$category = $info['category'];
$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : '';
$property_name = isset($info['property']['name']) && $info['property']['name'] ? $info['property']['name'] : '';
$house_model_name = isset($info['house_model']['name']) && $info['house_model']['name'] ? $info['house_model']['name'] : '';
$buyer_name = isset($info['buyer']) && $info['buyer'] ? $info['buyer']['last_name'] . ' ' . $info['buyer']['first_name'] : '';
$department_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : '';
$inquiry_category = isset($info['inquiry_category']['name']) && $info['inquiry_category']['name'] ? $info['inquiry_category']['name'] : 'N/A';
$inquiry_sub_category = isset($info['inquiry_sub_category']['name']) && $info['inquiry_sub_category']['name'] ? $info['inquiry_sub_category']['name'] : 'N/A';
$ticket_date = $info['ticket_date'];
$landline = isset($info['landline']) && $info['landline'] ? $info['landline'] : '';
$mobile = isset($info['mobile']) && $info['mobile'] ? $info['mobile'] : '';
$email = isset($info['email']) && $info['email'] ? $info['email'] : '';
$phase = isset($info['phase']) && $info['phase'] ? $info['phase'] : '';
$block = isset($info['block']) && $info['block'] ? $info['block'] : '';
$lot = isset($info['lot']) && $info['lot'] ? $info['lot'] : '';
$notes = isset($info['notes']) && $info['notes'] ? $info['notes'] : '';
// ==================== end: Add model fields ====================
// vdebug($info);
?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Client <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests select_buyer" data-module="buyers" data-type="person" id="buyer_id" name="buyer_id">
                <option value="">Select Buyer</option>
                <?php if ($buyer_name) : ?>
                    <option value="<?php echo $buyer_id; ?>" selected><?php echo $buyer_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="">First Name <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="first_name" value="<?= $info['first_name'] ?>" placeholder="First Name" autocomplete="off" id="first_name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="">Last Name <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="last_name" value="<?= $info['last_name'] ?>" placeholder="Last Name" autocomplete="off" id="last_name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Landline <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="landline" value="<?= $info['landline'] ?>" placeholder="Landline" autocomplete="off" id="landline">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Mobile <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="mobile" value="<?= $info['mobile'] ?>" placeholder="Mobile" autocomplete="off" id="mobile">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Email <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="email" value="<?= $info['email'] ?>" placeholder="Email" autocomplete="off" id="email">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Project <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="projects" data-type='buyer_filter' data-select='project_id' data-param='buyer_id' id="project_id" name="project_id">
                <option value="">Select Project</option>
                <?php if ($project_name) : ?>
                    <option value="<?php echo $project_id; ?>" selected><?php echo $project_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Property <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests suggests-property" data-type='buyer_filter' data-select='property_id' data-module="properties" data-param='buyer_id' id="property_id" name="property_id" data-param="buyer_id" data-param-2="project_id">
                <option value="">Select Project</option>
                <?php if ($property_name) : ?>
                    <option value="<?php echo $property_id; ?>" selected><?php echo $property_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Phase <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="phase" value="<?php echo set_value('phase', $phase); ?>" placeholder="Phase" autocomplete="off" id="phase">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Block <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="block" value="<?php echo set_value('block', $block); ?>" placeholder="Block" autocomplete="off" id="block">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Lot <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="lot" value="<?php echo set_value('lot', $lot); ?>" placeholder="Lot" autocomplete="off" id="lot">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">House Model</label>
            <select class="form-control suggests" data-module="house_models" id="house_model_id" name="house_model_id" data-param="project_id">
                <option value="">Select House Model</option>
                <?php if ($house_model_name) : ?>
                    <option value="<?php echo $house_model_id; ?>" selected><?php echo $house_model_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Category <span class="kt-font-danger"></span></label>
            <select class="form-control suggests-show-options" data-module="departments" id="department_id" name="category_id">
                <option value="">Select Department</option>
                <?php if ($category) : ?>
                    <option value="<?php echo $category['id']; ?>" selected><?php echo $category['name']; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Ticket Date <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control kt_datepicker" placeholder="Ticket Date" name="ticket_date" id="ticket_date" value="<?= date("Y-m-d"); ?>" readonly>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Turn Over Date <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control kt_datepicker" placeholder="Turn Over Date" name="turn_over_date" id="turn_over_date" value="<?php echo set_value('turn_over_date"', @$turn_over_date); ?>" readonly>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Warranty</label>
            <div class="kt-input-icon kt-input-icon--left">
                <select class="form-control" name="warranty" placeholder="Type" autocomplete="off" id="warranty">
                    <option>Select option</option>
                    <option value="1" <?php if ($warranty == 1) : ?> selected <?php endif ?>>Yes</option>
                    <option value="0" <?php if ($warranty == 0) : ?> selected <?php endif ?>>No</option>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
            </div>
            <?php echo form_error('warranty'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Description <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off" id="description">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Supporting Image</label>
            <div>
                <?php if ($file_path) : ?>
                    <img class="kt-widget__img" src="<?php echo base_url($file_path); ?>" width="90px" height="90px" />
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="image" class="" aria-invalid="false">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Notes <span class="kt-font-danger"></span></label>
            <textarea name="notes" id="notes" rows="10" class="form-control"><?= $notes ?></textarea>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>