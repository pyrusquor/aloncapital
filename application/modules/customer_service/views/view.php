<?php
$id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
// ==================== begin: Add model fields ====================
$reference = isset($info['reference']) && $info['reference'] ? $info['reference'] : 'N/A';
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : 'N/A';
$property_id = isset($info['property_id']) && $info['property_id'] ? $info['property_id'] : 'N/A';
$house_model_id = isset($info['house_model_id']) && $info['house_model_id'] ? $info['house_model_id'] : 'N/A';
$buyer_id = isset($info['buyer_id']) && $info['buyer_id'] ? $info['buyer_id'] : 'N/A';
$department_id = isset($info['department_id']) && $info['department_id'] ? $info['department_id'] : 'N/A';
$turn_over_date = isset($info['turn_over_date']) && $info['turn_over_date'] ? $info['turn_over_date'] : 'N/A';
$warranty_date = isset($info['warranty_date']) && $info['warranty_date'] ? $info['warranty_date'] : 'N/A';
$warranty = isset($info['warranty']) && $info['warranty'] ? "Yes" : "No";
$category = $info['category'];
// $inquiry_sub_category_id = isset($info['inquiry_sub_category_id']) && $info['inquiry_sub_category_id'] ? $info['inquiry_sub_category_id'] : 'N/A';
$description = isset($info['description']) && $info['description'] ? $info['description'] : 'N/A';
$image = isset($info['image']) && $info['image'] ? $info['image'] : 'N/A';
$status = isset($info['status']) && $info['status'] ? $info['status'] : 'N/A';
$status_value = Dropdown::get_static('customer_care_status', $info['status'], 'view');
$sub_status_value = Dropdown::get_static('customer_service_sub_status', $info['sub_status'], 'view');
$created_at = isset($info['created_at']) && $info['created_at'] ? $info['created_at'] : 'N/A';
$updated_at = isset($info['updated_at']) && $info['updated_at'] ? $info['updated_at'] : 'N/A';
$customer_service_logs = isset($info['customer_service_logs']) && $info['customer_service_logs'] ? $info['customer_service_logs'] : '';
$note_log = $info['note_logs'];
$latest_log = array_reverse($customer_service_logs);

if ($latest_log) {
    $latest_log_id = $latest_log[0]['id'];
}

$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : 'N/A';
$property_name = isset($info['property']['name']) && $info['property']['name'] ? $info['property']['name'] : 'N/A';
$house_model_name = isset($info['house_model']['name']) && $info['house_model']['name'] ? $info['house_model']['name'] : 'N/A';
$buyer = get_person($buyer_id, 'buyers');
$buyer_name = get_fname($buyer);
$department_name = isset($info['department']['name']) && $info['department']['name'] ? $info['department']['name'] : 'N/A';
// $inquiry_category = isset($info['inquiry_category']['name']) && $info['inquiry_category']['name'] ? $info['inquiry_category']['name'] : 'N/A';
// $inquiry_sub_category = isset($info['inquiry_sub_category']['name']) && $info['inquiry_sub_category']['name'] ? $info['inquiry_sub_category']['name'] : 'N/A';
$file_path = isset($info['file_path']) && $info['file_path'] ? $info['file_path'] : '';

$overdue = cc_date_diff($info);
$status_code = cc_status($status, $overdue['status']);

$landline = isset($info['landline']) && $info['landline'] ? $info['landline'] : '';
$mobile = isset($info['mobile']) && $info['mobile'] ? $info['mobile'] : '';
$email = isset($info['email']) && $info['email'] ? $info['email'] : '';
$phase = isset($info['phase']) && $info['phase'] ? $info['phase'] : '';
$block = isset($info['block']) && $info['block'] ? $info['block'] : '';
$lot = isset($info['lot']) && $info['lot'] ? $info['lot'] : '';
$notes = isset($info['notes']) && $info['notes'] ? $info['notes'] : '';
// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Ticket View</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <span class="dropdown">

                    <a href="#" class="btn btn-label-instagram btn-sm btn-elevate" data-toggle="dropdown" aria-expanded="true">
                        Change Status
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <a href="javascript:void(0);" class="dropdown-item" data-toggle="modal" data-target="#updateStatusModal" onclick="setInquiryTicketID(<?= $id ?>,'<?= $reference ?>', 1 )">Ongoing</a>

                        <a href="javascript:void(0);" data-toggle="modal" data-target="#forwardStatusModal" id="work_in_progress" data-id="<?= $id ?>" data-status="3" onclick="setInquiryTicketID(<?= $id ?>,'<?= $reference ?>', 2 )" class="dropdown-item">Forwarded</a>

                        <a href="javascript:void(0);" data-toggle="modal" data-target="#updateStatusModal" id="on_hold" data-id="<?= $id ?>" data-status="4" onclick="setInquiryTicketID(<?= $id ?>,'<?= $reference ?>', 3 )" class="dropdown-item">Completed</a>
                    </div>
                </span>
                <a href="<?php echo base_url('customer_service/printable/' . $id); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa flaticon-technology"></i> Print Ticket
                </a>
                <a href="<?php echo site_url('customer_service'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <h3 class="kt-widget13__desc">
                                        Reference: <strong><?= $reference ?></strong>
                                    </h3>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Project:</h6>
                                            <h6 class="kt-widget13__text"><?= $project_name ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">House Model:</h6>
                                            <h6 class="kt-widget13__text"><?= $house_model_name ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Buyer:</h6>
                                            <h6 class="kt-widget13__text"><?= $buyer_name ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Landline:</h6>
                                            <h6 class="kt-widget13__text"><?= $landline ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Mobile:</h6>
                                            <h6 class="kt-widget13__text"><?= $mobile ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Email:</h6>
                                            <h6 class="kt-widget13__text"><?= $email ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Turn Over Date:</h6>
                                            <h6 class="kt-widget13__text"><?= view_date($turn_over_date) ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Warranty Date:</h6>
                                            <h6 class="kt-widget13__text"><?= view_date($warranty_date) ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Warranty:</h6>
                                            <h6 class="kt-widget13__text"><?= $warranty ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Date Encoded:</h6>
                                            <h6 class="kt-widget13__text"><?= view_date($created_at) ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Date Updated:</h6>
                                            <h6 class="kt-widget13__text"><?= view_date($updated_at) ?></h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Phase:</h6>
                                            <h6 class="kt-widget13__text"><?= $phase ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Block:</h6>
                                            <h6 class="kt-widget13__text"><?= $block ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Lot:</h6>
                                            <h6 class="kt-widget13__text"><?= $lot ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Category:</h6>
                                            <h6 class="kt-widget13__text"><?= $category['name'] ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Description:</h6>
                                            <h6 class="kt-widget13__text"><?= $description ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Notes:</h6>
                                            <h6 class="kt-widget13__text"><?= $notes ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Status:</h6>
                                            <div class="card text-white <?= $status_code ?> mb-3 pr-5">
                                                <div class="card-body">
                                                    <h6 class="card-title"><?= $status_value ?></h6>
                                                    <p class="card-text"><?= $overdue['date_diff'] ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Sub Status:</h6>
                                            <h6 class="kt-widget13__text"><?= $sub_status_value ?></h6>
                                        </div>

                                        <?php if ($file_path) : ?>

                                            <div class="kt-widget13__item">
                                                <h6 class="kt-widget13__desc">Supporting Image:</h6>
                                                <img class="kt-widget__img" src="<?php echo base_url($file_path); ?>" width="90px" height="90px" />
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        <div class="col-md-6">
            <div class="kt-portlet kt-portlet--head-noborder kt-portlet--height-fluid-half">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Remarks Log
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <span class="dropdown">

                            <a href="#" class="btn btn-label-instagram btn-sm btn-elevate" data-toggle="dropdown" aria-expanded="true">
                                Change Forward Status
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">

                                <a href="javascript:void(0);" class="dropdown-item" onclick="setForwardStatus(<?= $latest_log_id ?>,'1', 'Ongoing' )">Ongoing</a>
                                <a href="javascript:void(0);" class="dropdown-item" onclick="setForwardStatus(<?= $latest_log_id ?>,'2','Completed' )">Completed</a>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="300" data-scrollbar-shown="true" style="height: 300px; overflow: hidden;">
                        <!--begin::Table-->
                        <table class="table table-striped- table-bordered table-hover table-checkable">
                            <thead>
                                <tr>
                                    <th>Remarks</th>
                                    <th>Forwarded To</th>
                                    <th>Forwarded Status</th>
                                    <th>Forwarded Completion Date</th>
                                    <th>Created At</th>
                                    <th>Created By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($customer_service_logs) : $customer_service_logs = array_reverse($customer_service_logs); ?>
                                    <?php foreach ($customer_service_logs as $key => $value) : ?>
                                        <?php
                                        $user = get_person($value['created_by'], 'users');
                                        $value['created_by'] = get_fname($user);
                                        ?>
                                        <tr>
                                            <td><?= $value['remarks'] ?></td>
                                            <td><?= $value['forwarded_to'] ? Dropdown::get_static('customer_service_category', $value['forwarded_to']) : '' ?></td>
                                            <td><?= $value['forwarded_status'] ? Dropdown::get_static('customer_service_forward_status', $value['forwarded_status']) : '' ?></td>
                                            <td><?= $value['forward_completion_date'] ? view_hrdate($value['forward_completion_date']) : '' ?></td>
                                            <td><?= view_hrdate($value['created_at']) ?></td>
                                            <td><?= $value['created_by'] ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <tr>
                                        <td colspan="6" class="text-center">No record found</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="kt-portlet">
                <div class="accordion accordion-solid accordion-toggle-svg" id="accord_note_log">
                    <div class="card">
                        <div class="card-header" id="head_client_information">
                            <div class="card-title" data-toggle="collapse" data-target="#note_log" aria-expanded="true" aria-controls="note_log">
                                Note Logs <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div id="note_log" class="collapse show" aria-labelledby="head_client_information" data-parent="#accord_note_log">
                            <div class="card-body">
                                <table class="table table-striped- table-bordered table-hover table-checkable">
                                    <thead>
                                        <tr>
                                            <th class='col-md-3'>Date</th>
                                            <th>Note</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($note_log): ?>
                                            <?php foreach ($note_log as $n_key => $n_value) : ?>
                                                <tr>
                                                    <td><?= date_format(date_create($n_value['created_at']),"M j, Y \ng:i a") ?></td>
                                                    <td><?= $n_value['note'] ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <tr>
                                                <td colspan="2" class="text-center">No Notes Found!</td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<div class="modal fade" id="forwardStatusModal" tabindex="-1" role="dialog" aria-labelledby="forwardStatusModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="forwardStatusModalLabel">Forward</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="forwardStatus">
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <h5 class="statusHeaderNo">Ticket No.: <span id="ticket_number"></span></h5>
                            <div class="form-group">
                                <input type="text" class="form-control" name="customer_service_id" id="forward_status_id" value="" hidden>
                            </div>
                            <div class="form-group">
                                <label class="">Category <span class="kt-font-danger"></span></label>
                                <?php echo form_dropdown('category_id', Dropdown::get_static('customer_service_category'), set_value('category_id', $category_id), 'class="form-control kt-select2" id="category_id"'); ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="forward_status" form="forwardStatus">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- UPDATE STATUS MODAL -->
<div class="modal fade" id="updateStatusModal" tabindex="-1" role="dialog" aria-labelledby="updateStatusModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateStatusModalLabel">Change Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="updateStatus">
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <h5 class="statusHeaderNo">Ticket No.: <span id="ticket_number_status"></span></h5>
                            <div class="form-group">
                                <input type="text" class="form-control" name="customer_service_id" id="customer_service_id" value="" hidden>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="status_id" id="status_id" value="" hidden>
                            </div>
                            <div class="form-group">
                                <label class="">Remarks <span class="kt-font-danger"></span></label>
                                <textarea class="form-control" id="remarks" rows="3" spellcheck="false" name="remarks"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="update_status" form="updateStatus">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Footer -->