<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Chart_of_accounts extends MY_Controller
{

    public $fields = [
        'group_id' => array(
            'field' => 'group_id',
            'label' => 'Group',
            'rules' => 'trim|required',
        ),
        'account_id' => array(
            'field' => 'account_id',
            'label' => 'Account',
            'rules' => 'trim|required',
        ),
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'color' => array(
            'field' => 'color',
            'label' => 'Color',
            'rules' => 'trim|required',
        ),
        'op_balance' => array(
            'field' => 'op_balance',
            'label' => 'OP Balance',
            'rules' => 'trim|required',
        ),
        'op_balance_dc' => array(
            'field' => 'op_balance_dc',
            'label' => 'OP Balance Dc',
            'rules' => 'trim|required',
        ),
        'type' => array(
            'field' => 'type',
            'label' => 'Type',
            'rules' => 'trim|required',
        ),
        'reconciliation' => array(
            'field' => 'reconciliation',
            'label' => 'Reconciliation',
            'rules' => 'trim|required',
        ),
        'accounts_receivable' => array(
            'field' => 'accounts_receivable',
            'label' => 'Accounts Receivable',
            'rules' => 'trim|required',
        ),
        'accounts_payable' => array(
            'field' => 'accounts_payable',
            'label' => 'Accounts Payable',
            'rules' => 'trim|required',
        ),
        'non_operating' => array(
            'field' => 'non_operating',
            'label' => 'Non-operating',
            'rules' => 'trim|required',
        ),
        'asset_type' => array(
            'field' => 'asset_type',
            'label' => 'Asset Type',
            'rules' => 'trim|required',
        ),
        'slug' => array(
            'field' => 'slug',
            'label' => 'Slug',
            'rules' => 'trim|required',
        ),
        'expense_type' => array(
            'field' => 'expense_type',
            'label' => 'Expense Type',
            'rules' => 'trim|required',
        ),
        'company_id' => array(
            'field' => 'company_id',
            'label' => 'Company',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('chart_of_accounts/Chart_of_accounts_model', 'M_accounting_ledgers');
        $this->load->model('accounting_groups/Accounting_groups_model', 'M_accounting_groups');
        $this->load->model('company/company_model', 'M_Company');

        $this->_table_fillables_ledgers = $this->M_accounting_ledgers->fillable;
        $this->_table_columns_ledgers = $this->M_accounting_ledgers->__get_columns();
        $this->_table_fillables_groups = $this->M_accounting_groups->fillable;
        $this->_table_columns_groups = $this->M_accounting_groups->__get_columns();

    }

    public function index($company_id = 0)
    {

        $_fills_ledgers = $this->_table_fillables_ledgers;
        $_colms_ledgers = $this->_table_columns_ledgers;

        $_fills_groups = $this->_table_fillables_groups;
        $_colms_groups = $this->_table_columns_groups;

        $this->view_data['_fillables_ledgers'] = $this->__get_fillables($_colms_ledgers, $_fills_ledgers);
        $this->view_data['_columns_ledgers'] = $this->__get_columns($_fills_ledgers);

        $this->view_data['_fillables_groups'] = $this->__get_fillables($_colms_groups, $_fills_groups);
        $this->view_data['_columns_groups'] = $this->__get_columns($_fills_groups);


        if (!$company_id) {
            show_404();
        }

        // $this->view_data['company'] = $this->M_Company->get_all();

        $this->view_data['chart'] = $this->M_accounting_groups->tree($company_id);

        // $data = $this->M_accounting_groups->get_children(1);

        // vdebug($this->view_data['chart']);

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);

    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_accounting_ledgers->with_accounting_groups()->with_user()->get($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                $this->view_data['data'] = $this->M_accounting_groups->get($id);
                $this->template->build('view_groups', $this->view_data);
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_accounting_ledgers->get($id);
            if ($list) {

                $deleted = $this->M_accounting_ledgers->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Ledger Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
    public function delete_group()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_accounting_groups->get($id);
            if ($list) {

                $deleted = $this->M_accounting_groups->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'a Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function update($type = false, $id = false)
    {

        $this->view_data['create_type'] = $type;
        $this->view_data['chart'] = $this->M_accounting_groups->tree();

        if ($id) {

            if ($type == "ledger") {

                $this->view_data['data'] = $data = $this->M_accounting_ledgers->get($id);

                if ($data) {

                    if ($this->input->post()) {

                        $_input = $this->input->post();

                        $result = $this->M_accounting_ledgers->from_form()->update($_input, $data['id']);

                        if ($result === false) {

                            // Validation
                            $this->notify->error('Oops something went wrong.');
                        } else {

                            // Success
                            $this->notify->success('Successfully Updated.', 'chart_of_accounts');

                        }
                    }

                    $this->template->build('update', $this->view_data);
                } else {

                    show_404();
                }
            } elseif ($type == "group") {

                $this->view_data['group'] = $data = $this->M_accounting_groups->get($id);

                if ($data) {

                    if ($this->input->post()) {

                        $_input = $this->input->post();

                        $result = $this->M_accounting_groups->from_form()->update($_input, $data['id']);

                        if ($result === false) {

                            // Validation
                            $this->notify->error('Oops something went wrong.');
                        } else {

                            // Success
                            $this->notify->success('Successfully Updated.', 'chart_of_accounts');

                        }
                    }

                    $this->template->build('update', $this->view_data);
                } else {

                    show_404();
                }

            }
        } else {

            show_404();
        }
    }

    public function form($id = false)
    {
        $this->view_data['create_type'] = $id;

        $this->view_data['chart'] = $this->M_accounting_groups->tree();

        if ($id == "ledger") {
            if ($this->input->post()) {
                $_input = $this->input->post();

                $result = $this->M_accounting_ledgers->from_form()->insert($_input);

                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                } else {

                    // Success
                    $this->notify->success('Ledger successfully created.', 'chart_of_accounts/index/'.$_input['company_id']);
                }
            }

            $this->template->build('form', $this->view_data);
        } else {
            if ($this->input->post()) {
                $_input = $this->input->post();

                $result = $this->M_accounting_groups->from_form()->insert($_input);

                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                } else {

                    // Success
                    $this->notify->success('Chart successfully created.', 'chart_of_accounts/index/'.$_input['company_id']);
                }

            }
            $this->template->build('form', $this->view_data);
        }

    }

    public function import_ledgers()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        // print_r($sheetData); exit;
        $err = 0;

        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'group_id' => $upload_data[1],
                        'account_id' => $upload_data[2],
                        'name' => $upload_data[3],
                        'op_balance' => $upload_data[4],
                        'op_balance_dc' => $upload_data[5],
                        'reconciliation' => $upload_data[6],
                        'created_by' => $this->session->userdata['user_id'],
                    );

                    $accounting_ledger_id = $upload_data[0];
                    $chart_of_accounts = $this->M_accounting_ledgers->get($accounting_ledger_id);

                    if ($chart_of_accounts) {
                        $result = $this->M_accounting_ledgers->update($fields, $accounting_ledger_id);
                    }

                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'group_id' => $upload_data[1],
                            'account_id' => $upload_data[2],
                            'name' => $upload_data[3],
                            'op_balance' => $upload_data[4],
                            'op_balance_dc' => $upload_data[5],
                            'reconciliation' => $upload_data[6],
                            'created_by' => $this->session->userdata['user_id'],
                        );

                        $result = $this->M_accounting_ledgers->insert($fields);
                    } else {
                        $fields = array(
                            'group_id' => $upload_data[1],
                            'account_id' => $upload_data[2],
                            'name' => $upload_data[3],
                            'op_balance' => $upload_data[4],
                            'op_balance_dc' => $upload_data[5],
                            'reconciliation' => $upload_data[6],
                            'created_by' => $this->session->userdata['user_id'],
                        );

                        $result = $this->M_accounting_ledgers->insert($fields);
                    }

                }
                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'chart_of_accounts');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'chart_of_accounts');
        die();
    }
    public function import_groups()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        // print_r($sheetData); exit;
        $err = 0;

        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'parent_id' => $upload_data[1],
                        'name' => $upload_data[2],
                        'slug' => $upload_data[3],
                        'affects_gross' => $upload_data[4],
                        'account_title' => $upload_data[5],
                        'company_id' => $upload_data[6],
                        'created_by' => $this->session->userdata['user_id'],
                    );

                    $accounting_ledger_id = $upload_data[0];
                    $chart_of_accounts = $this->M_accounting_groups->get($accounting_ledger_id);

                    if ($chart_of_accounts) {
                        $result = $this->M_accounting_groups->update($fields, $accounting_ledger_id);
                    }

                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'parent_id' => $upload_data[1],
                            'name' => $upload_data[2],
                            'slug' => $upload_data[3],
                            'affects_gross' => $upload_data[4],
                            'account_title' => $upload_data[5],
                            'company_id' => $upload_data[6],
                            'created_by' => $this->session->userdata['user_id'],
                        );

                        $result = $this->M_accounting_groups->insert($fields);
                    } else {
                        $fields = array(
                            'parent_id' => $upload_data[1],
                            'name' => $upload_data[2],
                            'slug' => $upload_data[3],
                            'affects_gross' => $upload_data[4],
                            'account_title' => $upload_data[5],
                            'company_id' => $upload_data[6],
                            'created_by' => $this->session->userdata['user_id'],
                        );

                        $result = $this->M_accounting_groups->insert($fields);
                    }

                }
                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'chart_of_accounts');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'chart_of_accounts');
        die();
    }

    public function export_ledgers()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $accounting_ledgers = $this->M_accounting_ledgers->as_array()->get_all();
        if ($accounting_ledgers) {

            foreach ($accounting_ledgers as $_lkey => $accounting_ledger) {

                $_datas[$accounting_ledger['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_accounting_ledgers_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'chart_of_accounts');
                }
            } else {

                $_filename = 'list_of_accounting_ledgers_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns    =    $this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'chart_of_accounts');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        } elseif ((strpos($_name, 'is_') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($accounting_ledgers as $_lkey => $chart_of_accounts) {

                                $_datas[$chart_of_accounts['id']][$_title] = isset($chart_of_accounts[$_column]) && ($chart_of_accounts[$_column] !== '') ? Dropdown::get_static('bool', $chart_of_accounts[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($accounting_ledgers as $_lkey => $chart_of_accounts) {

                                if ($_name === 'status') {

                                    $_datas[$chart_of_accounts['id']][$_title] = isset($chart_of_accounts[$_name]) && $chart_of_accounts[$_name] ? Dropdown::get_static('inventory_status', $chart_of_accounts[$_name], 'view') : '';
                                } else {

                                    $_datas[$chart_of_accounts['id']][$_title] = isset($chart_of_accounts[$_name]) && $chart_of_accounts[$_name] ? $chart_of_accounts[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Accounting Ledgers');
                $_objSheet->setCellValue('A1', 'LIST OF ACCOUNTING LEDGERS');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_style = array(
                    'font' => array(
                        'bold' => true,
                        'size' => 10,
                        'name' => 'Verdana',
                    ),
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'chart_of_accounts');
            }
        } else {

            $this->notify->error('No Record Found', 'chart_of_accounts');
        }
    }
    public function export_groups()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $accounting_groups = $this->M_accounting_groups->as_array()->get_all();
        if ($accounting_groups) {

            foreach ($accounting_groups as $_lkey => $accounting_group) {

                $_datas[$accounting_group['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_accounting_groups_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'chart_of_accounts');
                }
            } else {

                $_filename = 'list_of_accounting_groups_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns    =    $this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'chart_of_accounts');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        } elseif ((strpos($_name, 'is_') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($accounting_groups as $_lkey => $chart_of_accounts) {

                                $_datas[$chart_of_accounts['id']][$_title] = isset($chart_of_accounts[$_column]) && ($chart_of_accounts[$_column] !== '') ? Dropdown::get_static('bool', $chart_of_accounts[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($accounting_groups as $_lkey => $chart_of_accounts) {

                                if ($_name === 'status') {

                                    $_datas[$chart_of_accounts['id']][$_title] = isset($chart_of_accounts[$_name]) && $chart_of_accounts[$_name] ? Dropdown::get_static('inventory_status', $chart_of_accounts[$_name], 'view') : '';
                                } else {

                                    $_datas[$chart_of_accounts['id']][$_title] = isset($chart_of_accounts[$_name]) && $chart_of_accounts[$_name] ? $chart_of_accounts[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Accounting Groups');
                $_objSheet->setCellValue('A1', 'LIST OF ACCOUNTING GROUPS');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_style = array(
                    'font' => array(
                        'bold' => true,
                        'size' => 10,
                        'name' => 'Verdana',
                    ),
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'chart_of_accounts');
            }
        } else {

            $this->notify->error('No Record Found', 'chart_of_accounts');
        }
    }

    public function export_csv_ledgers()
    {

        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');
            $_filters = $this->input->post('filters');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Property CSV Template.csv';

            // project_id:13;status_id:;phase:;cluster:;block:;cluster:;

            $filters = explode(';', $_filters);

            // $_fillables    =    $this->M_document->fillable;
            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'property');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false && ($_fill !== 'user_id'))) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                if ($filters) {
                    foreach ($filters as $key => $filter) {
                        # code...
                        $filter = explode(':', $filter);

                        if ($filter) {

                            $key = $filter[0];
                            $value = @$filter[1];

                            if ($value) {
                                if ($key == "status_id") {$key = "status";}
                                $this->db->where($key, $value);
                            }
                        }

                    }
                }

                $records = $this->M_accounting_ledgers->as_array()->get_all(); #up($_documents);

                // vdebug($records);

                if ($records) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($records as $_dkey => $record) {

                            $_datas[$record['id']][$_title] = isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Properties');
            $_objSheet->setCellValue('A1', 'PROPERTIES');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }
    public function export_csv_groups()
    {

        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');
            $_filters = $this->input->post('filters');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Property CSV Template.csv';

            // project_id:13;status_id:;phase:;cluster:;block:;cluster:;

            $filters = explode(';', $_filters);

            // $_fillables    =    $this->M_document->fillable;
            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'property');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false && ($_fill !== 'user_id'))) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                if ($filters) {
                    foreach ($filters as $key => $filter) {
                        # code...
                        $filter = explode(':', $filter);

                        if ($filter) {

                            $key = $filter[0];
                            $value = @$filter[1];

                            if ($value) {
                                if ($key == "status_id") {$key = "status";}
                                $this->db->where($key, $value);
                            }
                        }

                    }
                }

                $records = $this->M_accounting_ledgers->as_array()->get_all(); #up($_documents);

                // vdebug($records);

                if ($records) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($records as $_dkey => $record) {

                            $_datas[$record['id']][$_title] = isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Properties');
            $_objSheet->setCellValue('A1', 'PROPERTIES');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }


    public function get_company_ledgers($id = 0)
    {
        
    }

    public function update_account_id()
    {
        $ledgers = $this->M_accounting_ledgers->get_all();

        if ($ledgers) {
            # code...
            foreach ($ledgers as $key => $ledger) {
                # code...

                if ($ledger['group_id'] > 5) {

                    $group_id = get_value_field($ledger['group_id'],'accounting_groups','parent_id');

                    if ($group_id > 5) {

                        $sgroup_id = get_value_field($group_id,'accounting_groups','parent_id');

                        if ($sgroup_id > 5) {

                            $ssgroup_id = get_value_field($sgroup_id,'accounting_groups','parent_id');
                            $acct_id = $ssgroup_id;

                        } else {
                            $acct_id = $sgroup_id;
                        }
                    } else {
                        $acct_id = $group_id;
                    }
                } else {
                    $acct_id = $ledger['group_id'];
                }


                $this->M_accounting_ledgers->update(['account_id' => $acct_id], $ledger['id']);

            }
        }
    }


}
