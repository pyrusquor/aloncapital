<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Chart_of_accounts_model extends MY_Model
{
    public $table = 'accounting_ledgers'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'group_id',
        'account_id',
        'company_id',
        'name',
        'op_balance',
        'op_balance_dc',
        'reconciliation',
        'updated_by',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'group_id' => array(
            'field' => 'group_id',
            'label' => 'Group',
            'rules' => 'trim|required',
        ),
        'account_id' => array(
            'field' => 'account_id',
            'label' => 'Account',
            'rules' => 'trim|required',
        ),
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'op_balance' => array(
            'field' => 'op_balance',
            'label' => 'OP Balance',
            'rules' => 'trim|required',
        ),
        'op_balance_dc' => array(
            'field' => 'op_balance_dc',
            'label' => 'OP Balance Dc',
            'rules' => 'trim|required',
        ),

        'reconciliation' => array(
            'field' => 'reconciliation',
            'label' => 'Reconciliation',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['accounting_groups'] = array('foreign_model' => 'accounting_groups/Accounting_groups_model', 'foreign_table' => 'accounting_groups', 'foreign_key' => 'id', 'local_key' => 'group_id');

        $this->has_one['user'] = array('foreign_model' => 'user/User_model', 'foreign_table' => 'users', 'foreign_key' => 'id', 'local_key' => 'account_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function get_many_by($param= [], $count=false)
    {
        $this->db->select('ledgers.*');
        
        if( ! empty($param['where']))
        {
            $this->db->where($param['where']);
        }
        
        if( ! empty($param['where_in']))
        {
            foreach($param['where_in'] as $field => $array)
            {
                $this->db->where_in($field, $array);
            }
        }

        if(! empty($param['ledger_ids']))
        {
            $this->db->where_in('ledgers.id', $param['ledger_ids']);
        }
        
        $this->db->where_in('groups.company_id', [$this->account['company_id'], 0]);
        
        $this->db->join('groups', 'groups.id = ledgers.group_id');
        
        if( ! $count)
        {
            if( ! empty($param['limit']))
            {
                $offset = isset($param['offset']) ? $param['offset'] : 0;
                $this->db->limit($param['limit'], $offset);
            }
            
            if( ! empty($param['order_by']))
            {
                $sort_by = isset($param['sort_by']) ? $param['sort_by'] : 'asc';                
                $this->db->order_by($param['order_by'], $sort_by);
            }
            
            $result = $this->db->get('ledgers')->result();
            
            return $result;
        }
        else
        {
            return $this->db->count_all_results('ledgers');
        }
        
    }
    
    public function save($data)
    {
        if( ! empty($data['id']))
        {
            $this->db->where('id', $data['id']);
            $this->db->update('ledgers', $data);
            
            return $data['id'];
        }
        else
        {
            $this->db->insert('ledgers', $data);
            
            return $this->db->insert_id();
        }
    }

    public function get_group_ledgers($params = [])
    {
        $ledgers = [];

        $affects = -1;

        if(! empty($params['parent_id']))
        {
            $this->db->where('parent_id', $params['parent_id']);
        }
        else
        {
            $this->db->where('parent_id', 0);
        }

        if(! empty($params['affects']))
        {
            $this->db->where('affects', $params['affects']);
        }

        $this->db->where_in('company_id', [$this->account['company_id'], 0]);

        $result = $this->db->get('groups')->result();


        $ledger_params = [];


        if(! empty($params['ledger_ids']))
        {
            $ledger_params['ledger_ids'] = $params['ledger_ids'];
        }

        // check
        foreach($result as $row)
        {
            // check if there is subgroups
            $params['parent_id'] = $row->id;
            $sub_ledgers = $this->get_group_ledgers($params);

            $ledger_params['where']['group_id'] = $row->id;
            $_ledgers = $this->get_many_by($ledger_params);

            if(! empty($subgroups))
            {
                $row->subgroups =  $subgroups;
            }

            $ledgers = array_merge($ledgers, $_ledgers, $sub_ledgers);
            /*if(! empty($ledgers))
            {
                $row->ledgers = $ledgers;
            }*/
        }

        return $ledgers;
    }

    // di na nagamit
    public function get_top_parent($group_id)
    {
        // get group
        $this->db->where_in('company_id', [$this->account['company_id'], 0]);
        $group = $this->db->get_where('groups', ['id'=>$group_id])->row();
        
        // has parent
        if( ! empty($group->parent_id))
        {
            // get the parent
            return $this->get_top_parent($group->parent_id);
        }
        // no parent, top na this
        else
        {
            return $group->id;
        }
    }
    
    public function select2($param=[], $count=false)
    {
        if(empty($param['select']))
        {
            $param['select'] = 'ledgers.id, ledgers.name as text, account_id';
        }

        $this->db->select($param['select']);


        if( ! empty($param['like']))
        {
            $this->db->where($param['like']);
        }

        if( ! empty($param['where']))
        {
            $this->db->where($param['where']);
        }
        
        $this->db->join('groups', 'groups.id = ledgers.group_id');
        
        if( ! $count)
        {
            if( ! empty($param['limit']))
            {
                $offset = isset($param['offset']) ? $param['offset'] : 0;
                $this->db->limit($param['limit'], $offset);
            }

            if( ! empty($param['order_by']))
            {
                $sort_by = isset($param['sort_by']) ? $param['sort_by'] : 'asc';
                $this->db->order_by($param['order_by'], $sort_by);
            }

            $data = $this->db->get($this->_table)->result();

            return $data;
        }
        else
        {
            return $this->db->count_all_results($this->_table);
        }
    }

    public function account_select2()
    {
        // get parent account and all ledgers in the account
        $this->db->select('id, name');
        $this->db->where('company_id', 0); //4 Main Groups ;  Universal
        $this->db->where('parent_id', 0);
        $groups = $this->db->get('groups')->result();

        $tmp = [];
        foreach($groups as $group)
        {
            $tmp[$group->name] = $this->select2(['order_by' => 'ledgers.name', 'where' => ['account_id' => $group->id]]);
        }

        return $tmp;
    }

    public function generate_color($company_id = 0)
    {
        $color = RandomColor::one(array(
            "luminosity" =>  'dark',
            "format" =>  'rgba',
            "alpha" =>  '0.5' //
        ));

            // check if already exist 
        $color = '';
        $flag = true;

        $params = ['company_id' => @$this->account['company_id']];

        if(! empty($company_id))
        {
            $params['company_id'] = $company_id;
        }

        
        while($flag)
        {
            $color = RandomColor::one(array(
                "luminosity" =>  'dark',
                "format" =>  'rgba',
                "alpha" =>  '0.5' //
            ));

            $params['color'] = $color;

            $exist  = $this->db->where($params)->count_all_results('ledgers');

            if(! $exist)
                $flag = false;
        }


        return $color;
    }

    

}
