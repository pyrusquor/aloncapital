<?php foreach ($ledgers as $key => $ledger) {?>
<!-- <div class="accordion accordion-light accordion-toggle-plus" id="accordionExample<?=$ledger['id'];?>">
    <div class="card">
        <div class="card-header" id="headingOne<?=$ledger['id'];?>">
            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse<?=$ledger['id'];?>" aria-expanded="false" aria-controls="collapseOne3">
                <?=$ledger['name'];?>
                ako budoy
            </div>
        </div>

    </div>
</div> -->
<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head kt-portlet__head">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="flaticon-graphic-1"></i>
            </span>
            <h3 class="kt-portlet__head-title">
            <?=$ledger['name'];?>
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-actions">
                <a href="<?php echo site_url('chart_of_accounts/update/ledger/' . $ledger['id']); ?>" class="btn btn-outline-success btn-sm btn-icon btn-icon-md">
                    <i class="flaticon-edit"></i>
                </a>
                <a href="<?php echo site_url('chart_of_accounts/view/' . $ledger['id']); ?>" class="btn btn-outline-primary btn-sm btn-icon btn-icon-md">
                    <i class="flaticon-eye"></i>
                </a>
                <a href="javascript:void(0)" class="btn btn-outline-danger btn-sm btn-icon btn-icon-md remove_ledger" data-id="<?php echo $ledger['id']; ?>">
                    <i class="flaticon-delete"></i>
                </a>
            </div>
        </div>
    </div>
</div>

<!--end::Portlet-->
<?php }?>