<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Create <?php echo $create_type; ?></h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="create_chart_of_accounts">
                    <i class="fa fa-plus-circle"></i> Submit
                </button>
                <a href="<?php echo base_url('chart_of_accounts'); ?>" class="btn btn-label-instagram"><i class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                <div class="kt-grid__item">


                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">

                    <!--begin: Form Wizard Form-->
                    <form class="kt-form" method="POST" action="<?php echo base_url('chart_of_accounts/form/' . @$create_type); ?>" id="create_chart_of_accounts"  enctype="multipart/form-data">
                        <?php $this->load->view('view/_form', $create_type);?>
                    </form>

                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
</div>