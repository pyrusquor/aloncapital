<?php

$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$parent_id = isset($data['parent_id']) && $data['parent_id'] ? $data['parent_id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
$slug = isset($data['slug']) && $data['slug'] ? $data['slug'] : '';
$affects_gross = isset($data['affects_gross']) && $data['affects_gross'] ? $data['affects_gross'] : '';
$account_title = isset($data['account_title']) && $data['account_title'] ? $data['account_title'] : '';
$company_id = isset($data['company_id']) && $data['company_id'] ? $data['company_id'] : '';

print_r($data);

?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Viewing
                '<?php echo $name ?>'
            </h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a
                    href="<?php echo site_url('chart_of_accounts/update/' . $id); ?>"
                    class="btn btn-label-success btn-elevate btn-sm"
                >
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a
                    href="<?php echo site_url('chart_of_accounts'); ?>"
                    class="btn btn-label-instagram btn-sm btn-elevate"
                >
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<div
    class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
    <div class="row">
        <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Details
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Parent
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $parent_id ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Name
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $name ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Slug
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $slug ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Affects Gross
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $affects_gross ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Account Title
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $account_title ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Company
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $company_id ?></span
                                    >
                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>

        <div class="col-md-6"></div>
    </div>
</div>
<!-- begin:: Footer -->