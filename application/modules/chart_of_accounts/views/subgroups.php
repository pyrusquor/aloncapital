<?php foreach ($subgroups as $key => $subgroups) {?>
<div class="accordion accordion-light accordion-toggle-plus" id="accordionExample<?=$subgroups['id'];?>">
    <div class="card">
        <div class="card-header" id="headingOne<?=$subgroups['id'];?>">
            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse<?=$subgroups['id'];?>" aria-expanded="false" aria-controls="collapseOne3">
                 <?=$subgroups['name'];?>
                <?php if ($subgroups['ledgers']): ?> &nbsp;
                    <span class="kt-badge kt-badge--brand kt-badge--sm">
                        <?=count($subgroups['ledgers']);?>
                    </span>
                <?php endif?>
            </div>
        </div>
        <div id="collapse<?=$subgroups['id'];?>" class="collapse" aria-labelledby="headingOne<?=$subgroups['id'];?>" data-parent="#accordionExample<?=$subgroups['id'];?>" style="">
            <div class="card-body">
                <?php if ($subgroups['subgroups']): ?>

                    <?php $this->load->view('subgroups', $subgroups);?>

                <?php endif?>

                <?php if ($subgroups['ledgers']): ?>

                    <?php $this->load->view('ledgers', $subgroups);?>

                <?php endif?>
            </div>
        </div>
    </div>
</div>
<?php }?>