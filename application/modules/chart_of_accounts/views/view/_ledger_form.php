<?php
    $id = isset($data['id']) && $data['id'] ? $data['id'] : '';
    $name = isset($data['name']) && $data['name'] ? $data['name'] : '';
    $group_id = isset($data['group_id']) && $data['group_id'] ? $data['group_id'] : '';
    $account_id = isset($data['account_id']) && $data['account_id'] ? $data['account_id'] : '';
    $color = isset($data['color']) && $data['color'] ? $data['color'] : '';
    $op_balance = isset($data['op_balance']) && $data['op_balance'] ? $data['op_balance'] : '';
    $op_balance_dc = isset($data['op_balance_dc']) && $data['op_balance_dc'] ? $data['op_balance_dc'] : '';
    $type = isset($data['type']) && $data['type'] ? $data['type'] : '';
    $reconciliation = isset($data['reconciliation']) && $data['reconciliation'] ? $data['reconciliation'] : '';
    $accounts_receivable = isset($data['accounts_receivable']) && $data['accounts_receivable'] ? $data['accounts_receivable'] : '';
    $accounts_payable = isset($data['accounts_payable']) && $data['accounts_payable'] ? $data['accounts_payable'] : '';
    $non_operating = isset($data['non_operating']) && $data['non_operating'] ? $data['non_operating'] : '';
    $asset_type = isset($data['asset_type']) && $data['asset_type'] ? $data['asset_type'] : '';
    $slug = isset($data['slug']) && $data['slug'] ? $data['slug'] : '';
    $expense_type = isset($data['expense_type']) && $data['expense_type'] ? $data['expense_type'] : '';
    $company_id = isset($data['company_id']) && $data['company_id'] ? $data['company_id'] : '';
    // print_r($chart);
?>

<div class="row">

    <div class="col-sm-12">
        <div class="form-group">
            <label>Company Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select id="company_id" class="form-control suggests" name="company_id" data-module="companies">
                    <?php if ($company_id): ?>
                        <option value="<?php echo $company_id; ?>" selected><?php echo get_value_field($company_id,'companies','name'); ?></option>
                    <?php endif?>
                </select>
            </div>
            <?php echo form_error('company_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-12">
        <div class="form-group">
            <label>Ledger Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label>Account <span class="kt-font-danger">*</span></label>
            <select class="form-control kt-select2 " id="account_id_select" name="account_id">
                <?php foreach ($chart as $key => $row): ?>
                    <option value="<?php echo $row['id'] ?>" <?php echo ($account_id == $row['id']) ? 'selected' : '' ?>><?php echo ucwords($row['name']); ?></option>
                <?php endforeach?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label>Select Group <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" name="group_id" data-module="accounting_groups" data-param="company_id">
                <?php if ($group_id): ?>
                    <option value="<?php echo $group_id; ?>" selected><?php echo get_value_field($group_id,'accounting_groups','name'); ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-12">
        <div class="form-group">
            <label>Opening Balance <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon">
            <div class="row">
                    <div class="col-sm-2">

                    <select class="form-control kt-select2 populateCode" id="op_balance_dc" name="op_balance_dc">

                        <?php if ($op_balance_dc): ?>
                        <option value="<?php echo $op_balance_dc ?>" <?php echo ($op_balance_dc == $op_balance_dc) ? 'selected' : '' ?>><?php echo ucwords($op_balance_dc); ?></option>
                        <?php endif;?>

                        <option value="D">D</option>
                        <option value="D">C</option>

            </select>
                    </div>
                    <div class="col-sm-10">
                    <input type="number" class="form-control" name="op_balance" value="<?php echo set_value('op_balance', $op_balance); ?>" placeholder="0.00" autocomplete="off">

                    </div>
            </div>
            </div>
            <?php echo form_error('op_balance_dc'); ?>
            <?php echo form_error('op_balance'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label class="kt-checkbox">
                <input type="hidden" name="reconciliation" value="0" />
                <input type="checkbox" name="reconciliation" value="1" /> Reconciliation
                <span></span>
            </label>
            <span class="form-text text-muted"></span>
        </div>
    </div>

</div>
