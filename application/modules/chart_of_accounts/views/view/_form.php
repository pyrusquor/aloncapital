<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">

                <div class="kt-portlet__body">
                    <?php if ($create_type == "ledger"): ?>
                        <?php $this->load->view('_ledger_form.php');?>
                    <?php elseif ($create_type == "group"): ?>
                        <?php $this->load->view('_group_form.php');?>
                    <?php endif?>
                </div>

            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
</div>