<?php

$id = isset($group['id']) && $group['id'] ? $group['id'] : '';
$parent_id = isset($group['parent_id']) && $group['parent_id'] ? $group['parent_id'] : '';
$name = isset($group['name']) && $group['name'] ? $group['name'] : '';
$slug = isset($group['slug']) && $group['slug'] ? $group['slug'] : '';
$affects_gross = isset($group['affects_gross']) && $group['affects_gross'] ? $group['affects_gross'] : '';
$account_title = isset($group['account_title']) && $group['account_title'] ? $group['account_title'] : '';
$company_id = isset($group['company_id']) && $group['company_id'] ? $group['company_id'] : '';

// print_r($chart);

?>

<div class="row">

    <div class="col-sm-6">
        <div class="form-group">
            <label>Company Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select id="company_id" class="form-control suggests" name="company_id" data-module="companies">
                    <?php if ($company_id): ?>
                        <option value="<?php echo $company_id; ?>" selected><?php echo get_value_field($company_id,'companies','name'); ?></option>
                    <?php endif?>
                </select>
            </div>
            <?php echo form_error('company_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Parent Group <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" name="parent_id" data-module="accounting_groups" data-param="parent_id">
                <?php if ($parent_id): ?>
                    <option value="<?php echo $parent_id; ?>" selected><?php echo get_value_field($parent_id,'accounting_groups','name'); ?></option>
                <?php endif?>
            </select>

            <span class="form-text text-muted"></span>
        </div>
  </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Group Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
  </div>
  
</div>