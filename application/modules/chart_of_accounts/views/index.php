<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Charts of Accounts</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <?php echo form_dropdown('filterCompany', Dropdown::get_dynamic('companies'), set_value('filterCompany', $company_id), 'class="form-control" id="filterCompany"'); ?>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <!-- <a href="<?php echo site_url('chart_of_accounts/form/ledger'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Add Ledger
                </a>
                <a href="<?php echo site_url('chart_of_accounts/form/group'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Add Group
                </a> -->
                <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse"
                    data-target="#_batch_upload_ledger" aria-expanded="true" aria-controls="_batch_upload_ledger">
                    <i class="fa fa-upload"></i> Import Ledger
                </button>
                <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse"
                    data-target="#_batch_upload_group" aria-expanded="true" aria-controls="_batch_upload_group">
                    <i class="fa fa-upload"></i> Import Groups
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal"
                    data-target="#_export_option_ledgers">
                    <i class="fa fa-download"></i>
                    Export Ledgers
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal"
                    data-target="#_export_option_groups">
                    <i class="fa fa-download"></i>
                    Export Groups
                </button>
            </div>
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">

                <a href="<?php echo site_url('chart_of_accounts/form/ledger'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm" style="margin-right: 10px;">
                    <i class="fa fa-plus"></i> Add Ledger
                </a>

                <a href="<?php echo site_url('chart_of_accounts/form/group'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Add Group
                </a>
        
        </div>

        <div class="module__filter">
                <!-- <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn"
                data-toggle="modal" data-target="#filterModal" aria-expanded="true" >
                    <i class="fa fa-filter"></i> Filter
                </button> -->
        </div>
    </div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!-- Batch Upload -->
    <div id="_batch_upload_ledger" class="collapse kt-margin-b-35 kt-margin-t-10">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <form class="kt-form" id="_export_csv_ledger"
                    action="<?php echo site_url('chart_of_accounts/export_csv_ledgers'); ?>" method="POST"
                    enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="form-control-label">File Type</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <select class="form-control form-control-sm" name="update_existing_data">
                                        <option value=""> -- Update Existing Data -- </option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-cloud-upload"></i></span></span>
                                </div>
                                <?php echo form_error('update_existing_data'); ?>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <form class="kt-form" id="_upload_form_ledgers"
                    action="<?php echo site_url('chart_of_accounts/import_ledgers'); ?>" method="POST"
                    enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="form-control-label">Upload CSV file:</label>
                                <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per
                                    file.</label>
                                <input type="file" name="csv_file" class="" size="1000" accept="*.csv">
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="form-group form-group-last row custom_import_style">
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm"
                                    form="_upload_form_ledgers">
                                    <i class="fa fa-upload"></i> Upload
                                </button>
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <!-- <a href="<?php echo site_url('document/export') ?>" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm">
								<i class="fa fa-file-csv"></i>
							</a> -->
                                <button type="submit"
                                    class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm"
                                    form="_export_csv_ledger">
                                    <i class="fa fa-file-csv"></i>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Batch Upload -->
    <div id="_batch_upload_group" class="collapse kt-margin-b-35 kt-margin-t-10">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <form class="kt-form" id="_export_csv_group"
                    action="<?php echo site_url('chart_of_accounts/export_csv_groups'); ?>" method="POST"
                    enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="form-control-label">File Type</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <select class="form-control form-control-sm" name="update_existing_data">
                                        <option value=""> -- Update Existing Data -- </option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-cloud-upload"></i></span></span>
                                </div>
                                <?php echo form_error('update_existing_data'); ?>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <form class="kt-form" id="_upload_form_groups"
                    action="<?php echo site_url('chart_of_accounts/import_groups'); ?>" method="POST"
                    enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="form-control-label">Upload CSV file:</label>
                                <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per
                                    file.</label>
                                <input type="file" name="csv_file" class="" size="1000" accept="*.csv">
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="form-group form-group-last row custom_import_style">
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm"
                                    form="_upload_form_groups">
                                    <i class="fa fa-upload"></i> Upload
                                </button>
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <!-- <a href="<?php echo site_url('document/export') ?>" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm">
								<i class="fa fa-file-csv"></i>
							</a> -->
                                <button type="submit"
                                    class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm"
                                    form="_export_csv_ledger">
                                    <i class="fa fa-file-csv"></i>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet kt-portlet--mobile">

                <!-- <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Chart of Accounts
                    </h3>
                </div>
            </div> -->


                <div class="kt-portlet__body">


                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($chart as $key => $row): ?>
                        <li class="nav-item">
                            <a class="nav-link <?=($key == 0 ? 'active' : '');?>" data-toggle="tab"
                                href="#kt_tabs_<?=$row['id'];?>" role="tab"><i class="la la-cloud-upload"></i>
                                <?=$row['name'];?> &nbsp; <span
                                    class="kt-badge kt-badge--success kt-badge--sm"><?=count($row['children']);?></span></a>
                        </li>
                        <?php endforeach;?>
                    </ul>

                    <div class="tab-content">

                        <?php foreach ($chart as $key => $row): ?>

                        <div class="tab-pane <?=($key == 0 ? 'active' : '');?>" id="kt_tabs_<?=$row['id'];?>"
                            role="tabpanel">
                            <div class="accordion accordion-light  accordion-toggle-arrow" id="accordionExample5">
                                <?php if ($row['children']): ?>
                                <?php foreach ($row['children'] as $key => $children) {?>
                                <div class="card">
                                    <div class="card-header" id="headingOne5">
                                        <div class="card-title" data-toggle="collapse"
                                            data-target="#collapse<?=$children['id'];?>" aria-expanded="true"
                                            aria-controls="collapseOne5">
                                            <i class="flaticon2-pie-chart-4"></i> <?=$children['name'];?>
                                            <?php if ($children['subgroups']): ?>&nbsp;
                                            <span class="kt-badge kt-badge--brand kt-badge--sm">
                                                <?=count($children['subgroups']);?>
                                            </span>
                                            <?php endif?>

                                        </div>
                                    </div>
                                    <div id="collapse<?=$children['id'];?>" class="collapse"
                                        aria-labelledby="headingOne5" data-parent="#accordionExample5">
                                        <div class="card-body">

                                            <?php if ($children['subgroups']): ?>

                                            <?php $this->load->view('subgroups', $children);?>

                                            <?php endif?>

                                            <?php if ($children['ledgers']): ?>

                                            <?php $this->load->view('ledgers', $children);?>

                                            <?php endif?>

                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <?php endif;?>
                            </div>
                        </div>

                        <?php endforeach;?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- EXPORT -->
<div class="modal fade" id="_export_option_ledgers" tabindex="-1" role="dialog"
    aria-labelledby="_export_option_ledgers_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_ledgers_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form_ledgers" target="_blank"
                    action="<?php echo site_url('chart_of_accounts/export_ledgers'); ?>" method="POST">
                    <input type="hidden" id="filters" name="filters" val="">
                    <div class="row">
                        <?php if (isset($_columns_ledgers) && $_columns_ledgers): ?>

                        <div class="col-lg-12">
                            <div class="kt-checkbox-list">
                                <label class="kt-checkbox kt-checkbox--bold">
                                    <input type="checkbox" id="_export_select_all_ledgers"> Select All
                                    <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold"></label>
                            </div>
                        </div>

                        <?php foreach ($_columns_ledgers as $key => $_column): ?>

                        <?php if ($_column): ?>

                        <?php
$_offset = '';
if ($_column === reset($_columns_ledgers)) {

    $_offset = 'offset-lg-1';
}
?>

                        <div class="col-lg-6">
                            <div class="kt-checkbox-list">
                                <?php foreach ($_column as $_ckey => $_clm): ?>

                                <?php
$_label = isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
$_value = isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
?>

                                <label class="kt-checkbox kt-checkbox--bold">
                                    <input type="checkbox" name="_export_column[]" class="_export_column_ledgers"
                                        value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
                                    <span></span>
                                </label>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <?php endif;?>
                        <?php endforeach;?>
                        <?php else: ?>

                        <div class="col-lg-10 offset-lg-1">
                            <div class="form-group form-group-last">
                                <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                    <div class="alert-text">
                                        Something went wrong. Please contact your system administrator.
                                    </div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left"
                        data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit"
                        class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right"
                        form="_export_form_ledgers">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- EXPORT -->
<div class="modal fade" id="_export_option_groups" tabindex="-1" role="dialog"
    aria-labelledby="_export_option_groups_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_groups_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form_groups" target="_blank"
                    action="<?php echo site_url('chart_of_accounts/export_groups'); ?>" method="POST">
                    <input type="hidden" id="filters" name="filters" val="">
                    <div class="row">
                        <?php if (isset($_columns_groups) && $_columns_groups): ?>

                        <div class="col-lg-12">
                            <div class="kt-checkbox-list">
                                <label class="kt-checkbox kt-checkbox--bold">
                                    <input type="checkbox" id="_export_select_all_groups"> Select All
                                    <span></span>
                                </label>
                                <label class="kt-checkbox kt-checkbox--bold"></label>
                            </div>
                        </div>

                        <?php foreach ($_columns_groups as $key => $_column): ?>

                        <?php if ($_column): ?>

                        <?php
$_offset = '';
if ($_column === reset($_columns_groups)) {

    $_offset = 'offset-lg-1';
}
?>

                        <div class="col-lg-6">
                            <div class="kt-checkbox-list">
                                <?php foreach ($_column as $_ckey => $_clm): ?>

                                <?php
$_label = isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
$_value = isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
?>

                                <label class="kt-checkbox kt-checkbox--bold">
                                    <input type="checkbox" name="_export_column[]" class="_export_column_groups"
                                        value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
                                    <span></span>
                                </label>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <?php endif;?>
                        <?php endforeach;?>
                        <?php else: ?>

                        <div class="col-lg-10 offset-lg-1">
                            <div class="form-group form-group-last">
                                <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                    <div class="alert-text">
                                        Something went wrong. Please contact your system administrator.
                                    </div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left"
                        data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit"
                        class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right"
                        form="_export_form_groups">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>