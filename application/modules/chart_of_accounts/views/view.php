<?php

$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
$group_id = isset($data['group_id']) && $data['group_id'] ? $data['group_id'] : '';
$account_id = isset($data['account_id']) && $data['account_id'] ? $data['account_id'] : '';
// $color = isset($data['color']) && $data['color'] ? $data['color'] : '';
$op_balance = isset($data['op_balance']) && $data['op_balance'] ? $data['op_balance'] : '';
$op_balance_dc = isset($data['op_balance_dc']) && $data['op_balance_dc'] ? $data['op_balance_dc'] : 'None';
$type = isset($data['type']) && $data['type'] ? $data['type'] : 'None';
$reconciliation = isset($data['reconciliation']) && $data['reconciliation'] ? $data['reconciliation'] : 'None';
$accounts_receivable = isset($data['accounts_receivable']) && $data['accounts_receivable'] ? $data['accounts_receivable'] : 'None';
$accounts_payable = isset($data['accounts_payable']) && $data['accounts_payable'] ? $data['accounts_payable'] : 'None';
$non_operating = isset($data['non_operating']) && $data['non_operating'] ? $data['non_operating'] : 'None';
$asset_type = isset($data['asset_type']) && $data['asset_type'] ? $data['asset_type'] : '';
$slug = isset($data['slug']) && $data['slug'] ? $data['slug'] : '';
$expense_type = isset($data['expense_type']) && $data['expense_type'] ? $data['expense_type'] : 'None';
$company_id = isset($data['company_id']) && $data['company_id'] ? $data['company_id'] : 'None';

$group_name = $data['accounting_groups']['name'];
$account_name = $data['user']['first_name'] . ' ' .$data['user']['last_name'];

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Viewing
                '<?php echo $name ?>'
            </h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a
                    href="<?php echo site_url('chart_of_accounts/update/' . $id); ?>"
                    class="btn btn-label-success btn-elevate btn-sm"
                >
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a
                    href="<?php echo site_url('chart_of_accounts'); ?>"
                    class="btn btn-label-instagram btn-sm btn-elevate"
                >
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div
    class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
    <div class="row">
        <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Details
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Group
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                        <?php echo $group_name ?>
                                        </span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Account
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $account_name ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Name
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $name ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        OP Balance
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $op_balance ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        OP Balance DC
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $op_balance_dc == "D" ? "Debit" : "Credit" ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Type
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $type ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Reconciliation
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $reconciliation ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Accounts Receivable
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $accounts_receivable ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Accounts Payable
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $accounts_payable ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Non-operating
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $non_operating ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Asset Type
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $asset_type ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Slug
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $slug ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Expense Type
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        ><?php echo $expense_type ?></span
                                    >
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Company
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                        <?php echo Dropdown::get_static('companies', $company_id, 'view'); ?>
                                        </span
                                    >
                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>

        <div class="col-md-6"></div>
    </div>
</div>
<!-- begin:: Footer -->
