<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchase_order_request_item_model extends MY_Model
{

    public $table = 'purchase_order_request_items'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'id',
        /* ==================== begin: Add model fields ==================== */
        'item_group_id',
        'item_type_id',
        'item_brand_id',
        'item_class_id',
        'item_id',
        'unit_of_measurement_id',
        'material_request_id',
        'purchase_order_request_id',
        'item_brand_name',
        'item_class_name',
        'item_name',
        'unit_of_measurement_name',
        'item_group_name',
        'item_type_name',
        'quantity',
        'unit_cost',
        'total_cost',
        /* ==================== end: Add model fields ==================== */
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
        $this->has_one['item_group'] = array('foreign_model' => 'item_group/item_group_model', 'foreign_table' => 'item_groups', 'foreign_key' => 'id', 'local_key' => 'item_group_id');
        $this->has_one['item_type'] = array('foreign_model' => 'item_type/item_type_model', 'foreign_table' => 'item_types', 'foreign_key' => 'id', 'local_key' => 'item_type_id');
        $this->has_one['item_brand'] = array('foreign_model' => 'item_brand/item_brand_model', 'foreign_table' => 'item_brands', 'foreign_key' => 'id', 'local_key' => 'item_brand_id');
        $this->has_one['item_class'] = array('foreign_model' => 'item_class/item_class_model', 'foreign_table' => 'item_classs', 'foreign_key' => 'id', 'local_key' => 'item_class_id');
        $this->has_one['item'] = array('foreign_model' => 'item/item_model', 'foreign_table' => 'items', 'foreign_key' => 'id', 'local_key' => 'item_id');
        $this->has_one['unit_of_measurement'] = array('foreign_model' => 'inventory_settings_unit_of_measurement/inventory_settings_unit_of_measurement_model', 'foreign_table' => 'inventory_settings_units_of_measurement', 'foreign_key' => 'id', 'local_key' => 'unit_of_measurement_id');
        $this->has_one['material_request'] = array('foreign_model' => 'material_request/material_request_model', 'foreign_table' => 'material_requests', 'foreign_key' => 'id', 'local_key' => 'material_request_id');
        $this->has_one['purchase_order_request'] = array('foreign_model' => 'purchase_order_request/purchase_order_request_model', 'foreign_table' => 'purchase_order_requests', 'foreign_key' => 'id', 'local_key' => 'purchase_order_request_id');
    }
}
