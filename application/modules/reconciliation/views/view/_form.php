<?php
// ==================== begin: Add model fields ====================
$bank_id = isset($reconciliation['bank_id']) && $reconciliation['bank_id'] ? $reconciliation['bank_id'] : '';
$month_year = isset($reconciliation['month_year']) && $reconciliation['month_year'] ? $reconciliation['month_year'] : '';
$ending_balance = isset($reconciliation['ending_balance']) && $reconciliation['ending_balance'] ? $reconciliation['ending_balance'] : '';

$bank_name = isset($reconciliation['accounting_ledger']['name']) && $reconciliation['accounting_ledger']['name'] ? $reconciliation['accounting_ledger']['name'] : '';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Bank Name <span class="kt-font-danger"></span></label>
            <select class="form-control suggests" data-module="accounting_ledgers" data-type="bank"  id="bank_id" name="bank_id">
                <option value="">Select Bank</option>
                <?php if ($bank_name): ?>
                    <option value="<?php echo $bank_id; ?>" selected><?php echo $bank_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label>Date <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control monthYearPicker" placeholder="Date" name="month_year" value="<?php echo set_value('month_year"', @$month_year); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Ending Balance</label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="number" class="form-control" id="ending_balance" placeholder="Ending Balance"
                           name="ending_balance"
                           value="<?php echo set_value('ending_balance', @$ending_balance); ?>">
                </div>
            </div>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>