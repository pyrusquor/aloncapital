<?php
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$reconciliation = isset($info['reconciliation']) && $info['reconciliation'] ? $info['reconciliation'] : '';
$name = isset($reconciliation['accounting_ledger']['name']) && $reconciliation['accounting_ledger']['name'] ? $reconciliation['accounting_ledger']['name'] : 'N/A';
$month_year = isset($reconciliation['month_year'])  && $reconciliation['month_year'] ? date('M Y', strtotime($reconciliation['month_year'])) : 'N/A';
// ==================== begin: Add model fields ====================
$ending_balance = isset($info['reconciliation']['ending_balance']) && $info['reconciliation']['ending_balance'] ? $info['reconciliation']['ending_balance'] : 0;
$reconciled_count = isset($info['reconciled_count']) && $info['reconciled_count'] ? $info['reconciled_count'] : 0;
$reconciled_amount = isset($info['reconciled_amount']) && $info['reconciled_amount'] ? $info['reconciled_amount'] : 0;
$unreconciled_amount = isset($info['unreconciled_amount']) && $info['unreconciled_amount'] ? $info['unreconciled_amount'] : 0;
$entry_items = isset($info['total']) && $info['total'] ? $info['total'] : 0;
$difference = isset($info['difference']) && $info['difference'] ? $info['difference'] : 0;

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Accounting Entry Items</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="total"><?= $reconciled_count ?>/<?= $entry_items ?></span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="submit" class="btn btn-label-success btn-elevate btn-sm" id="reconcileItem">
                    <i class="fa fa-check"></i> Reconcile
                </button>
                <a href="<?php echo site_url('reconciliation'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-sm-4">
                    Bank: <?= $name ?>
                </div>
                <div class="col-sm-4">
                    Date: <?= $month_year ?>
                </div>
                <div class="col-sm-4">
                    Ending Balance: <?= money_php($ending_balance) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    Reconciled Amount: <?= money_php($reconciled_amount) ?>
                </div>
                <div class="col-sm-4">
                    Unreconciled Amount: <?= money_php($unreconciled_amount) ?>
                </div>
                <div class="col-sm-3">
                    Difference: <?= money_php($difference) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="entry_item_table">
                <thead>
                <tr>
                    <th>ID</th>
                    <!-- ==================== begin: Add header fields ==================== -->
                    <th>Date</th>
                    <th>Reference No.</th>
                    <th>Journal Type</th>
                    <th>Payee</th>
                    <th>Particulars</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Reconciled</th>
                    <!-- ==================== end: Add header fields ==================== -->
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if (isset($data)): ?>
                    <?php foreach ($data as $key => $value): ?>
                        <tr>
                            <td><?= $value['id'] ?></td>
                            <td><?= view_date($value['payment_date']) ?></td>
                            <td><?= $value['invoice_number'] ?></td>
                            <td><?=Dropdown::get_static('accounting_entries_journal_type',$value['journal_type'],'view') ?></td>
                            <td><?= $value['payee_type']." ".$value['payee_name'] ?></td>
                            <td><?= $value['remarks'] ?></td>
                            <td><?= $value['dc'] ?></td>
                            <td><?= money_php($value['amount']) ?></td>
                            <td><?= $value['is_reconciled'] ?></td>
                            <td>
                                <?php if ($value['is_reconciled'] !== "True"): ?>
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" name="id[]" value="<?= $value['id'] ?>"
                                               class="m-checkable recon_check" data-id="<?= $value['id'] ?>">
                                        <span></span>
                                    </label>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="10" class="text-center">No record found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
        <!-- begin:: Footer -->
    </div>
</div>