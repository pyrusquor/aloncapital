<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reconciliation_model extends MY_Model {
    public $table = 'reconciliation'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'bank_id',
        'month_year',
        'ending_balance',
        'created_by',
        'updated_by',
        'deleted_by'
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'bank_id' => array(
            'field' => 'bank_id',
            'label' => 'Bank',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */
        'month_year' => array(
            'field' => 'month_year',
            'label' => 'Date',
            'rules' => 'trim|required'
        ),
        'ending_balance' => array(
            'field' => 'ending_balance',
            'label' => 'Amount',
            'rules' => 'trim|required'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['accounting_ledger'] = array('foreign_model' => 'accounting_ledgers/accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'bank_id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}