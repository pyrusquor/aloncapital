<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reconciliation extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('reconciliation/Reconciliation_model', 'M_Reconciliation');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_accounting_entry_item');
        $this->_table_fillables = $this->M_Reconciliation->fillable;
        $this->_table_columns = $this->M_Reconciliation->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Reconciliation->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Reconciliation->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showReconciliations()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'bank_id' => true,
            'bank' => true,
            'month_year' => true,
            'ending_balance' => true,
            'created_by' => true,
            'updated_by' => true
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $reconciliation = $this->M_Reconciliation->with_accounting_ledger()->order_by('id', 'DESC')->as_array()->get_all();
        $data = [];

        if ($reconciliation) {

            foreach ($reconciliation as $key => $value) {
                $reconciliation[$key]['month_year'] = date('Y-m', strtotime($value['month_year']));
                $reconciliation[$key]['ending_balance'] = money_php($value['ending_balance']);
                $reconciliation[$key]['bank'] = $value['accounting_ledger']['name'];

                $reconciliation[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $reconciliation[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($reconciliation as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();
            $_input['created_by'] = $this->session->userdata['user_id'];

            $filter = ['bank_id' => $_input['bank_id'], 'month_year' => $_input['month_year']];
            $reconciliation = $this->M_Reconciliation->where($filter)->get();

            if (!$reconciliation === false) {
                $result = false;
                $this->notify->error('Record already exists');
                $this->template->build('create');
            } else {
                $result = $this->M_Reconciliation->from_form()->insert($_input);
            }

            if ($result === false) {
                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Reconciliation successfully created.', 'reconciliation');
            }
        }

        $this->template->build('create');
    }

    public function update($id = false)
    {
        if ($id) {

            $this->view_data['reconciliation'] = $data = $this->M_Reconciliation->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();
                    $_input['updated_by'] = $this->session->userdata['user_id'];

                    $result = $this->M_Reconciliation->from_form()->update($_input, $data['id']);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'reconciliation');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Reconciliation->get($id);
            if ($list) {

                $deleted = $this->M_Reconciliation->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Reconciliation Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_Reconciliation->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Reconciliation Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function reconItem()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $update_ids = $this->input->post('reconids_arr');

            if ($update_ids) {
                foreach ($update_ids as $value) {
                    $data = [
                        'is_reconciled' => 1,
                        'updated_by' => $this->session->userdata['user_id'],
                        'updated_at' => date('Y-m-d')
                    ];
                    $entry_item_id = $this->db->update('accounting_entry_items', $data, array('id' => $value));
                }
                if ($entry_item_id !== 0) {

                    $response['status'] = 1;
                    $response['message'] = 'Reconciliation Sucessfully Reconciled';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view_item($id = false, $bank_id = false, $month_year = false)
    {

        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            //            $this->view_data['data'] = $this->M_accounting_entry_item->with_accounting_entry()->with_accounting_ledger()->where($filter)->get_all();
            $this->view_data['info']['reconciliation'] =  $this->M_Reconciliation->with_accounting_ledger()->get($id);

            $this->db->select('accounting_entry_items.*, accounting_entries.journal_type, accounting_entries.payment_date, accounting_entries.invoice_number, accounting_entries.remarks');
            $this->db->from('accounting_entry_items');
            $this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id');
            $this->db->where('accounting_entry_items.ledger_id', $bank_id);
            $this->db->where('accounting_entries.payment_date >', date('Y-m-1', strtotime($month_year)));
            $this->db->where('accounting_entries.payment_date <', date('Y-m-t', strtotime($month_year)));

            $this->db->where('accounting_entries.deleted_at', NULL);
            $this->db->where('accounting_entry_items.deleted_at', NULL);
            $query = $this->db->get();

            $this->view_data['data'] = $query->result_array();

            // vdebug( $this->view_data['data'] );

            $column_group = $query->num_rows();

            if ($column_group) {
                $this->view_data['info']['total'] = $column_group;
            }

            $this->view_data['info']['reconciled_count'] = $this->count_reconciled_item($bank_id, $month_year);
            $this->view_data['info']['reconciled_amount'] = $this->count_entry_item_amount($bank_id, $month_year, 1);
            $this->view_data['info']['unreconciled_amount'] = $this->count_entry_item_amount($bank_id, $month_year, 0);
            $this->view_data['info']['difference'] = $this->view_data['info']['unreconciled_amount']  - $this->view_data['info']['reconciled_amount'];
            if ($this->view_data['data']) {

                foreach ($this->view_data['data'] as $key => $value) {

                    $r = get_payee($value);

                    $this->view_data['data'][$key]['payee_type'] = ($value['payee_type'] ? ucfirst(Dropdown::get_static('accounting_entries_payee_type', $value['payee_type'], 'view')) : 'N/A');

                    $this->view_data['data'][$key]['payee_name'] = ($value['payee_type_id'] ? @$r['name'] . " " . @$r['first_name'] . " " . @$r['last_name'] : 'N/A');

                    $this->view_data['data'][$key]['dc'] = $value['dc'] == 'c' ? 'Credit' : 'Debit';

                    $this->view_data['data'][$key]['is_reconciled'] = $value['is_reconciled'] == '1' ? 'True' : 'False';
                }

                $this->template->build('view_item', $this->view_data);
            } else {

                $this->template->build('view_item', $this->view_data);
            }
        } else {
            show_404();
        }
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_Reconciliation->with_accounting_ledger()->get($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        /* ==================== begin: Add model fields ==================== */
                        'name' => $upload_data[1],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $reconciliation_id = $upload_data[0];
                    $reconciliation = $this->M_Reconciliation->get($reconciliation_id);

                    if ($reconciliation) {
                        $result = $this->M_Reconciliation->update($fields, $reconciliation_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[0],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Reconciliation->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'bank_id' => $upload_data[1],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Reconciliation->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'reconciliation');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'reconciliation');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued    =    $this->input->post('update_existing_data');

            $_is_update    =    $_ued === '1' ? TRUE : FALSE;

            $_alphas        =    [];
            $_datas            =    [];

            $_titles[]    =    'id';

            $_start    =    3;
            $_row        =    2;

            $_filename    =    'Reconciliation CSV Template.csv';

            $_fillables    =    $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'reconciliation');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[]    =    $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group    =    $this->M_Reconciliation->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title]    =    isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas            =    $this->__get_excel_columns(count($_titles));
            $_xls_columns    =    array_combine($_alphas, $_titles);
            $_firstAlpha    =    reset($_alphas);
            $_lastAlpha        =    end($_alphas);

            $_objSheet    =    $this->excel->getActiveSheet();
            $_objSheet->setTitle('Reconciliation');
            $_objSheet->setCellValue('A1', 'RECONCILIATION');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style    =    array(
                'font'  => array(
                    'bold'    =>    TRUE,
                    'size'    =>    10,
                    'name'    =>    'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $reconciliations    =    $this->M_Reconciliation->as_array()->get_all();
        if ($reconciliations) {

            foreach ($reconciliations as $_lkey => $reconciliation) {

                $_datas[$reconciliation['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_reconciliations_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'reconciliation');
                }
            } else {

                $_filename    =    'list_of_reconciliations_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'reconciliation');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($reconciliations as $_lkey => $reconciliation) {

                                $_datas[$reconciliation['id']][$_title]    =    isset($reconciliation[$_column]) && ($reconciliation[$_column] !== '') ? Dropdown::get_static('bool', $reconciliation[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($reconciliations as $_lkey => $reconciliation) {

                                if ($_name === 'status') {

                                    $_datas[$reconciliation['id']][$_title]    =    isset($reconciliation[$_name]) && $reconciliation[$_name] ? Dropdown::get_static('inventory_status', $reconciliation[$_name], 'view') : '';
                                } else {

                                    $_datas[$reconciliation['id']][$_title]    =    isset($reconciliation[$_name]) && $reconciliation[$_name] ? $reconciliation[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Reconciliation');
                $_objSheet->setCellValue('A1', 'LIST OF RECONCILIATION');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'reconciliation');
            }
        } else {

            $this->notify->error('No Record Found', 'reconciliation');
        }
    }

    function count_reconciled_item($id, $month_year)
    {
        if ($id) {
            $this->db->from('accounting_entry_items');
            $this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id');
            $this->db->where('accounting_entry_items.ledger_id', $id);
            $this->db->where('accounting_entry_items.is_reconciled =', 1);
            $this->db->where('accounting_entries.payment_date >', date('Y-m-1', strtotime($month_year)));
            $this->db->where('accounting_entries.payment_date <', date('Y-m-t', strtotime($month_year)));

            $this->db->where('accounting_entries.deleted_at', NULL);
            $this->db->where('accounting_entry_items.deleted_at', NULL);

            $query = $this->db->get();
            $result = $query->num_rows();

            return $result;
        }
    }
    function count_entry_item_amount($id, $month_year,  $is_reconciled)
    {
        if ($id) {
            $this->db->select('SUM(accounting_entry_items.amount) AS total');
            $this->db->from('accounting_entry_items');
            $this->db->join('accounting_entries', 'accounting_entries.id = accounting_entry_items.accounting_entry_id');
            $this->db->where('accounting_entry_items.ledger_id', $id);
            $this->db->where('accounting_entry_items.is_reconciled =', $is_reconciled);
            $this->db->where('accounting_entries.payment_date >', date('Y-m-1', strtotime($month_year)));
            $this->db->where('accounting_entries.payment_date <', date('Y-m-t', strtotime($month_year)));

            $this->db->where('accounting_entries.deleted_at', NULL);
            $this->db->where('accounting_entry_items.deleted_at', NULL);

            $query = $this->db->get();
            $result = $query->result_array();

            return $result[0]['total'];
        }
    }
}
