<script type="text/javascript">
    <?php if($item_id): ?>
        window.item_id = "<?php echo $item_id;?>";
    <?php else:?>
        window.item_id = null;
    <?php endif;?>
</script>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Inventory Search</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('item');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="item_search_app">
    <div class="card mb-3">
        <div class="card-body">
            <div class="form-group">
                <label class="form-control-label">Item:</label>
                <select class="form-control suggests" style="width:100%;"
                        data-module="item" id="item_id"
                        name="item_id">
                    <option value="">Select Item</option>
                </select>
            </div>
        </div>
    </div>
    <div class="spinner-border text-success" role="status" v-if="inventory.loading">
        <span class="sr-only">Loading...</span>
    </div>
    <div class="card mb-3" v-if="inventory.data.length > 0">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Unit</th>
                            <th>Available Quantity</th>
                            <th>Actual Quantity</th>
                            <th>Warehouse</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="row in inventory.data">
                            <td>{{ row.item.name }}</td>
                            <td>{{ row.unit_of_measurement.name }}</td>
                            <td>{{ row.available_quantity }}</td>
                            <td>{{ row.actual_quantity }}</td>
                            <th>
                                <a :href="warehouseURL(row.warehouse_id)">
                                    {{ row.warehouse.name }}
                                </a>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>