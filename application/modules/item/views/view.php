<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
// ==================== begin: Add model fields ====================
$code = isset($data['code']) && $data['code'] ? $data['code'] : '';
$beginning_unit_price = isset($data['beginning_unit_price']) && $data['beginning_unit_price'] ? $data['beginning_unit_price'] : '';
$unit_price = isset($data['unit_price']) && $data['unit_price'] ? $data['unit_price'] : '';
$tax_id = isset($data['tax_id']) && $data['tax_id'] ? $data['tax_id'] : '';
$total_price = isset($data['total_price']) && $data['total_price'] ? $data['total_price'] : '';
$description = isset($data['description']) && $data['description'] ? $data['description'] : '';

$tax = isset($data['tax']['name']) && $data['tax']['name'] ? $data['tax']['name'] : '';
// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Item</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('item/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('item');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $name; ?></span>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Code
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $code; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Beginning Unit Price
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $beginning_unit_price; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Latest Unit Price
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $unit_price; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Tax
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $tax; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Total Price
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $total_price; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Description
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $description; ?></span>
                                </div>
                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
