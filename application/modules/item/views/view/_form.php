<?php

$name = isset($item['name']) && $item['name'] ? $item['name'] : '';
// ==================== begin: Add model fields ====================
$code = isset($item['code']) && $item['code'] ? $item['code'] : '';
$beginning_unit_price = isset($item['beginning_unit_price']) && $item['beginning_unit_price'] ? $item['beginning_unit_price'] : '';
$unit_price = isset($item['unit_price']) && $item['unit_price'] ? $item['unit_price'] : '';
$tax_id = isset($item['tax_id']) && $item['tax_id'] ? $item['tax_id'] : '';
$total_price = isset($item['total_price']) && $item['total_price'] ? $item['total_price'] : '';
$description = isset($item['description']) && $item['description'] ? $item['description'] : '';

$tax = isset($item['tax']['name']) && $item['tax']['name'] ? $item['tax']['name'] : 'N/A';
$abc = isset($item['abc']['name']) && $item['abc']['name'] ? $item['abc']['name'] : 'N/A';
$brand = isset($item['brand']['name']) && $item['brand']['name'] ? $item['brand']['name'] : 'N/A';
$class = isset($item['class']['name']) && $item['class']['name'] ? $item['class']['name'] : 'N/A';
$group = isset($item['group']['name']) && $item['group']['name'] ? $item['group']['name'] : 'N/A';
$type = isset($item['type']['name']) && $item['type']['name'] ? $item['type']['name'] : 'N/A';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <?php echo form_error('name'); ?>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sticky-note"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="code" value="<?php echo set_value('code', $code); ?>" placeholder="Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Beginning Unit Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control" name="beginning_unit_price" id="beginning_unit_price" value="<?php echo set_value('beginning_unit_price', $beginning_unit_price); ?>" placeholder="Beginning Unit Price" autocomplete="off">

                </div>
            </div>
            <?php echo form_error('beginning_unit_price'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Unit Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control" name="unit_price" id="unit_price" value="<?php echo set_value('unit_price', $unit_price); ?>" placeholder="Unit Price" autocomplete="off">

                </div>
            </div>
            <?php echo form_error('unit_price'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <input type="text" id="tax_rate" class="form-control" hidden>
            <label class="">Tax</label>
            <select class="form-control suggests" data-module="taxes" id="tax_id" name="tax_id">
                <option value="">Select Tax</option>
                <?php if ($tax) : ?>
                    <option value="<?php echo $tax_id; ?>" selected><?php echo $tax; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">ABC</label>
            <select class="form-control suggests" data-module="item_abc" id="item_abc_id" name="item_abc_id">
                <option value="">Select ABC Type</option>
                <?php if ($abc) : ?>
                    <option value="<?php echo $abc_id; ?>" selected><?php echo $abc; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Brand </label>
            <select class="form-control suggests" data-module="item_brand" id="item_brand_id" name="item_brand_id">
                <option value="">Select Brand Type</option>
                <?php if ($brand) : ?>
                    <option value="<?php echo $brand_id; ?>" selected><?php echo $brand; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Class <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="item_class" id="item_class_id" name="item_class_id" required>
                <option value="">Select Class Type</option>
                <?php if ($class) : ?>
                    <option value="<?php echo $class; ?>" selected><?php echo $class; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Group <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="item_group" id="item_group_id" name="item_group_id" required>
                <option value="">Select Group Type</option>
                <?php if ($group) : ?>
                    <option value="<?php echo $group_id; ?>" selected><?php echo $group; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Type <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="item_type" id="item_type_id" name="item_type_id" required>
                <option value="">Select Type</option>
                <?php if ($type) : ?>
                    <option value="<?php echo $type_id; ?>" selected><?php echo $type; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Total Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control" name="total_price" id="total_price" value="<?php echo set_value('total_price', $total_price); ?>" placeholder="Total Price" autocomplete="off">
                </div>
            </div>
            <?php echo form_error('total_price'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Description </label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-edit"></i></span>
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>