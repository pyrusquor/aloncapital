<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('item/Item_model', 'M_Item');
        $this->load->model('taxes/Taxes_model', 'M_taxes');
        $this->_table_fillables = $this->M_Item->fillable;
        $this->_table_columns = $this->M_Item->__get_columns();
    }

    public function inventory_search()
    {
        $this->js_loader->queue([
            'js/vue2.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);

        $item_id = isset($_GET['item_id']) ? $_GET['item_id'] : null;
        $this->view_data['item_id'] = $item_id;
        $this->template->build('inventory_search', $this->view_data);
    }

    public function get_all_items()
    {
        /*
             * Parse $_GET for parameters
             */
        $class_id = isset($_GET['class_id']) ? $_GET['class_id'] : NULL;
        $brand_id = isset($_GET['brand_id']) ? $_GET['brand_id'] : NULL;
        $abc_id = isset($_GET['abc_id']) ? $_GET['abc_id'] : NULL;
        $group_id = isset($_GET['group_id']) ? $_GET['group_id'] : NULL;
        $type_id = isset($_GET['type_id']) ? $_GET['type_id'] : NULL;

        $where = NULL;
        if ($class_id) {
            $where['item_class_id'] = $class_id;
        }
        if ($brand_id) {
            $where['item_brand_id'] = $brand_id;
        }
        if ($abc_id) {
            $where['item_abc_id'] = $abc_id;
        }
        if ($group_id) {
            $where['item_group_id'] = $group_id;
        }
        if ($group_id) {
            $where['item_group_id'] = $group_id;
        }
        if ($type_id) {
            $where['item_type_id'] = $type_id;
        }

        $data = $this->M_Item->as_array()->get_all($where);
        $output = array(
            'data' => $data
        );
        echo json_encode($output);
    }

    public function get_item()
    {
        $id = $this->input->post('itemID');
        $data = $this->M_Item->with_tax()->as_array()->get($id);
        echo json_encode($data);
    }

    public function item_query()
    {
        $params = array();
        $item_abc_id = $this->input->post('item_abc_id') ? $this->input->post('item_abc_id') : 0;
        $item_brand_id = $this->input->post('item_brand_id') ? $this->input->post('item_brand_id') : 0;
        $item_class_id = $this->input->post('item_class_id') ? $this->input->post('item_class_id') : 0;
        $item_group_id = $this->input->post('item_group_id') ? $this->input->post('item_group_id') : 0;
        $item_type_id = $this->input->post('item_type_id') ? $this->input->post('item_type_id') : 0;

        if ($item_abc_id)
            $params['item_abc_id'] = $item_abc_id;
        if ($item_brand_id)
            $params['item_brand_id'] = $item_brand_id;
        if ($item_class_id)
            $params['item_class_id'] = $item_class_id;
        if ($item_group_id)
            $params['item_group_id'] = $item_group_id;
        if ($item_type_id)
            $params['item_type_id'] = $item_type_id;

        $result = $this->M_Item->where($params)->get_all();

        echo json_encode($result);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Item->with_tax()->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Item->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'name' => true,
            'code' => true,
            'beginning_unit_price' => true,
            'unit_price' => true,
            'tax_id' => true,
            'total_price' => true,
            'description' => true,
            'tax' => true,
            'group' => true,
            'type' => true,
            'class' => true,
            'brand' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Item
                ->with_tax()
                ->with_brand()
                ->with_class()
                ->with_group()
                ->with_type()
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("name like '%$searchValue%'");
                $query->or_where("code like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'name' => [
                    'data' => $filters['name'] ?? null,
                    'operator' => 'like',
                ],
                'code' => [
                    'data' => $filters['code'] ?? null,
                    'operator' => 'like',
                ],
                'tax_id' => [
                    'data' => $filters['tax_id'] ?? null,
                    'operator' => '=',
                ],
                'item_group_id' => [
                    'data' => $filters['item_group_id'] ?? null,
                    'operator' => '=',
                ],
                'item_type_id' => [
                    'data' => $filters['item_type_id'] ?? null,
                    'operator' => '=',
                ],
                'item_class_id' => [
                    'data' => $filters['item_class_id'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['tax_id'] = $value['tax']['name'] ?? '';

                        $items[$key]['group'] = $value['group']['name'] ?? '';

                        $items[$key]['type'] = $value['type']['name'] ?? '';

                        $items[$key]['class'] = $value['class']['name'] ?? '';

                        $items[$key]['brand'] = $value['brand']['name'] ?? '';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Item->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();
            $_input['created_by'] = $this->session->userdata['user_id'];

            $result = $this->M_Item->from_form()->insert($_input);

            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Item successfully created.', 'item');
            }
        }

        $this->template->build('create');
    }

    public function update($id = false)
    {
        if ($id) {

            $this->view_data['item'] = $data = $this->M_Item->with_tax()->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();
                    $_input['updated_by'] = $this->session->userdata['user_id'];

                    $result = $this->M_Item->from_form()->update($_input, $data['id']);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'item');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Item->get($id);
            if ($list) {

                $deleted = $this->M_Item->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_Item->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Type Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = FALSE)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_Item
                ->with_tax()
                ->with_abc()
                ->with_brand()
                ->with_class()
                ->with_group()
                ->with_type()
                ->get($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'name' => $upload_data[1],
                        /* ==================== begin: Add model fields ==================== */
                        'code' => $upload_data[2],
                        'unit_price' => $upload_data[3],
                        'tax_id' => $upload_data[4],
                        'total_price' => $upload_data[5],
                        'description' => $upload_data[6],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $item_id = $upload_data[0];
                    $item = $this->M_Item->get($item_id);

                    if ($item) {
                        $result = $this->M_Item->update($fields, $item_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */
                            'code' => $upload_data[1],
                            'unit_price' => $upload_data[2],
                            'tax_id' => $upload_data[3],
                            'total_price' => $upload_data[4],
                            'description' => $upload_data[5],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Item->insert($fields);
                    } else {
                        $fields = array(
                            'name' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */
                            'code' => $upload_data[2],
                            'unit_price' => $upload_data[3],
                            'tax_id' => $upload_data[4],
                            'total_price' => $upload_data[5],
                            'description' => $upload_data[6],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Item->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'item');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'item');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? TRUE : FALSE;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Item CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'item');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Item->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Item');
            $_objSheet->setCellValue('A1', 'ITEM');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $items = $this->M_Item->as_array()->get_all();
        if ($items) {

            foreach ($items as $_lkey => $item) {

                $_datas[$item['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_items_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'item');
                }
            } else {

                $_filename = 'list_of_items_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'item');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($items as $_lkey => $item) {

                                $_datas[$item['id']][$_title] = isset($item[$_column]) && ($item[$_column] !== '') ? Dropdown::get_static('bool', $item[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($items as $_lkey => $item) {

                                if ($_name === 'status') {

                                    $_datas[$item['id']][$_title] = isset($item[$_name]) && $item[$_name] ? Dropdown::get_static('inventory_status', $item[$_name], 'view') : '';
                                } else {

                                    $_datas[$item['id']][$_title] = isset($item[$_name]) && $item[$_name] ? $item[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Item');
                $_objSheet->setCellValue('A1', 'LIST OF ITEM');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'item');
            }
        } else {

            $this->notify->error('No Record Found', 'item');
        }
    }
}
