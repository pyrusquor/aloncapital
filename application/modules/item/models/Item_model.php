<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item_model extends MY_Model
{
    public $table = 'item'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'id',
        'name',
        'code',
        'beginning_unit_price',
        'unit_price',
        'tax_id',
        'item_abc_id',
        'item_brand_id',
        'item_class_id',
        'item_group_id',
        'item_type_id',
        'total_price',
        'description',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */
        'code' => array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim'
        ),
        'beginning_unit_price' => array(
            'field' => 'beginning_unit_price',
            'label' => 'Beginning Unit Price',
            'rules' => 'trim|required'
        ),
        'unit_price' => array(
            'field' => 'unit_price',
            'label' => 'Unit Price',
            'rules' => 'trim|required'
        ),
        'tax_id' => array(
            'field' => 'tax_id',
            'label' => 'Tax',
            'rules' => 'trim'
        ),
        'total_price' => array(
            'field' => 'total_price',
            'label' => 'Total Price',
            'rules' => 'trim|required'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['tax'] = array('foreign_model' => 'taxes/Taxes_model', 'foreign_table' => 'taxes', 'foreign_key' => 'id', 'local_key' => 'tax_id');
        $this->has_one['abc'] = array('foreign_model' => 'item_abc/Item_abc_model', 'foreign_table' => 'item_abc', 'foreign_key' => 'id', 'local_key' => 'item_abc_id');
        $this->has_one['brand'] = array('foreign_model' => 'item_brand/item_brand_model', 'foreign_table' => 'item_brand', 'foreign_key' => 'id', 'local_key' => 'item_brand_id');
        $this->has_one['class'] = array('foreign_model' => 'item_class/item_class_model', 'foreign_table' => 'item_class', 'foreign_key' => 'id', 'local_key' => 'item_class_id');
        $this->has_one['group'] = array('foreign_model' => 'item_group/item_group_model', 'foreign_table' => 'item_group', 'foreign_key' => 'id', 'local_key' => 'item_group_id');
        $this->has_one['type'] = array('foreign_model' => 'item_type/item_type_model', 'foreign_table' => 'item_type', 'foreign_key' => 'id', 'local_key' => 'item_type_id');
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
