<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchase_order_model extends MY_Model {
    public $table = 'purchase_orders'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'id',
        'reference',
        'status',
        'total',
        'supplier_id',
        'accounting_ledger_id',
        'purchase_order_request_id',
        'company_id',
        'warehouse_id',
        'approving_staff_id',
        'requesting_staff_id',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',

        ];
    public $form_fillables = [
        'id',
        'reference',
        'status',
        'total',
        'supplier_id',
        'accounting_ledger_id',
        'purchase_order_request_id',
        'company_id',
        'warehouse_id',
        'approving_staff_id',
        'requesting_staff_id',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
        $this->has_one['company'] = array('foreign_model' => 'company/company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
        $this->has_one['accounting_ledger'] = array('foreign_model' => 'accounting_ledgers/accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'accounting_ledger_id');
        $this->has_one['purchase_order_request'] = array('foreign_model' => 'purchase_order_request/purchase_order_request_model', 'foreign_table' => 'purchase_order_requests', 'foreign_key' => 'id', 'local_key' => 'purchase_order_request_id');
        $this->has_one['supplier'] = array('foreign_model' => 'suppliers/suppliers_model', 'foreign_table' => 'suppliers', 'foreign_key' => 'id', 'local_key' => 'supplier_id');
        $this->has_one['approving_staff'] = array('foreign_model' => 'staff/staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'approving_staff_id');
        $this->has_one['requesting_staff'] = array('foreign_model' => 'staff/staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'requesting_staff_id');

    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    function calculate_totals($id) {
        $total = $this->db->query("SELECT SUM(`total_cost`) AS 'ttl' FROM `purchase_order_items` WHERE `purchase_order_id` = '$id'")->row();
        $this->db->set('total', $total->ttl);
        $this->db->where('id', $id);
        $this->db->update($this->table);
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}