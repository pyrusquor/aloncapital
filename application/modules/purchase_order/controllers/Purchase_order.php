<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchase_order extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Purchase_order_model', 'M_Purchase_order');
        $this->load->model('purchase_order_item/purchase_order_item_model', 'M_Purchase_order_item');
        $this->load->model('staff/Staff_model', 'M_staff');
        $this->load->model('staff/Staff_model', 'M_staff');

        $this->load->helper('form');

        $this->_table_fillables = $this->M_Purchase_order->fillable;
        $this->_table_columns = $this->M_Purchase_order->__get_columns();
        $this->module_name = $this->uri->segment(1, '');
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Purchase_order->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Purchase_order->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue([
            'vendors/custom/datatables/datatables.bundle.js',
            'js/vue2.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);

        $this->template->build('index', $this->view_data);
    }

    function filterKeyword($data, $search, $field = '')
    {
        $filter = '';

        if (isset($search['value'])) {
            $filter = $search['value'];
        }

        if (!empty($filter)) {
            if (!empty($field)) {
                if (strpos(strtolower($field), 'date') !== false) {
                    // filter by date range
                    $data = $this->filterByDateRange($data, $filter, $field);
                } else {
                    // filter by column
                    $data = array_filter($data, function ($a) use ($field, $filter) {

                        if ($a[$field] ==  $filter) {
                            return 1;
                        } else {
                            return 0;
                        }

                        // return (boolean) preg_match( "/$filter/i", $a[ $field ] );

                    });
                }
            } else {
                // general filter
                $data = array_filter($data, function ($a) use ($filter) {
                    return (bool) preg_grep("/$filter/i", (array) $a);
                });
            }
        }

        return $data;
    }

    public function showPurchaseOrders()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'reference' => true,
            'total' => true,
            'status' => true,
            'supplier_id' => true,
            'supplier' => true,
            'accounting_ledger_id' => true,
            'accounting_ledger' => true,
            'company_id' => true,
            'company' => true,
            'approving_staff_id' => true,
            'requesting_staff_id' => true,
            'purchase_order_request_id' => true,
            'purchase_order_request' => true,
            // 'created_at' => true,
            'created_by' => true,
            'updated_by' => true,
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $purchase_orders = $this->__search($_GET); // $this->M_Purchase_order->order_by('id', 'DESC')->as_array()->get_all();
        $data = [];

        if ($purchase_orders) {
            $this->load->helper('purchase_order');

            foreach ($purchase_orders as $key => $value) {

                // $purchase_orders[$key]['created_at'] = view_date($value['created_at']);
                $purchase_orders[$key]['total'] = money_php($value['total']);
                $purchase_orders[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';
                $purchase_orders[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($purchase_orders as $d) {
                $__data = $this->filterArray($d, $columnsDefault);
                $items = $this->__get_items($d['id']);
                $__data['items'] = $items;
                $__data['status_label'] = po_status_lookup($d['status']);
                array_push($data, $__data);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search'], 'reference');
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    private function __get_items($id, $with_relations = TRUE)
    {
        $__items = $this->M_Purchase_order_item
            ->where('purchase_order_id', $id)
            ->with_item_group()
            ->with_item_type()
            ->with_item_brand()
            ->with_item_class()
            ->with_item()
            ->with_unit_of_measurement()
            ->with_material_request()
            ->get_all();
        return $__items;
    }

    public function get_items($id)
    {
        $result = $this->__get_items($id);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function __search($params)
    {
        $id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
        $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

        if ($status_list) {
            $status_list_arr = explode(",", $status_list);
            $this->M_Purchase_order->where('status', $status_list_arr);
        }

        if (!$id) {

            $company_id = isset($params['company_id']) && !empty($params['company_id']) ? $params['company_id'] : null;
            $accounting_ledger_id = isset($params['accounting_ledger_id']) && !empty($params['accounting_ledger_id']) ? $params['accounting_ledger_id'] : null;
            $warehouse_id = isset($params['warehouse_id']) && !empty($params['warehouse_id']) ? $params['warehouse_id'] : null;
            $purchase_order_request_id = isset($params['purchase_order_request_id']) && !empty($params['purchase_order_request_id']) ? $params['purchase_order_request_id'] : null;
            $supplier_id = isset($params['supplier_id']) && !empty($params['supplier_id']) ? $params['supplier_id'] : null;
            $status = isset($params['status']) && !empty($params['status']) ? $params['status'] : null;
            $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;
            $reference = isset($params['reference']) && !empty($params['reference']) ? $params['reference'] : null;
            $date_filter = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;

            if (!empty($date_filter)) {
                $date_range = explode('-', $date_filter);

                $from_str   = strtotime($date_range[0]);
                $from       = date('Y-m-d H:i:s', $from_str);

                $to_str     = strtotime($date_range[1] . '23:59:59');
                $to         = date('Y-m-d H:i:s', $to_str);
            }

            if (!empty($date_filter) && !empty($date_filter)) {
                // $this->M_Purchase_order->where('created_at BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->M_Purchase_order->where("created_at >=", $from);
                $this->M_Purchase_order->where("created_at <=", $to);
            }

            if ($company_id) {
                $this->M_Purchase_order->where('company_id', $company_id);
            }
            if ($accounting_ledger_id) {
                $this->M_Purchase_order->where('accounting_ledger_id', $accounting_ledger_id);
            }
            if ($warehouse_id) {
                $this->M_Purchase_order->where('warehouse_id', $warehouse_id);
            }
            if ($purchase_order_request_id) {
                $this->M_Purchase_order->where('purchase_order_request_id', $purchase_order_request_id);
            }
            if ($supplier_id) {
                $this->M_Purchase_order->where('supplier_id', $supplier_id);
            }

            if ($status) {
                $this->M_Purchase_order->where('status', $status);
            }
            if ($reference) {
                $this->M_Purchase_order->like('reference', $reference, 'both');
            }
        } else {
            $this->M_Purchase_order->where('id', $id);
        }

        $this->M_Purchase_order->order_by('id', 'DESC');

        $item_id = isset($params['item_id']) && !empty($params['item_id']) ? $params['item_id'] : null;

        if ($item_id) {
            $ids = [];
            $items = $this->M_Purchase_order_item->where('item_id', $item_id)->get_all();
            foreach ($items as $item) {
                $ids[] = $item['purchase_order_id'];
            }

            if (sizeof($ids) > 0) {
                $this->M_Purchase_order->where('id', $ids);
            }
        }

        if ($with_relations === 'yes') {
            $result = $this->M_Purchase_order->with_company()->with_supplier()->with_purchase_order_request()->with_approving_staff()->with_requesting_staff()->with_accounting_ledger()->get_all();
        } else {
            $result = $this->M_Purchase_order->get_all();
        }

        if ($result) {

            foreach ($result as $key => $value) {

                $result[$key]['created_at'] = view_date($value['created_at']);
            }
        }

        return $result;
    }

    public function search()
    {
        $result = $this->__search($_GET);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function process_create_master($data, $additional)
    {
        $data['reference'] = uniqidPO();
        // $request = $this->M_Purchase_order_request->insert($data);
        $obj = $data + $additional;
        $request = $this->db->insert('purchase_orders', $obj);
        if ($request) {
            $ref = $this->db->insert_id();
            return $ref;
        } else {
            return false;
        }
    }

    private function process_create_slave($data, $references, $additional)
    {
        $this->load->model('purchase_order_item/Purchase_order_item_model', 'M_Purchase_order_item');
        $material_request_id = array_key_exists('material_request_id', $data) ? $data['material_request_id'] : null;
        $purchase_order_request_item_id = array_key_exists('purchase_order_request_item_id', $data) ? $data['purchase_order_request_item_id'] : null;
        $purchase_order_request_id = array_key_exists('purchase_order_request_id', $data) ? $data['purchase_order_request_id'] : null;

        $item = array(
            'purchase_order_id' => $references['purchase_order_id'],
            'warehouse_id' => $references['warehouse_id'],
            'supplier_id' => $references['supplier_id'],
            'material_request_id' => $material_request_id,
            'purchase_order_request_item_id' => $purchase_order_request_item_id,
            'purchase_order_request_id' => $purchase_order_request_id,
            'item_group_id' => $data['item_group_id'],
            'item_type_id' => $data['item_type_id'],
            'item_brand_id' => $data['item_brand_id'],
            'item_class_id' => $data['item_class_id'],
            'item_id' => $data['item_id'],
            'unit_of_measurement_id' => $data['unit_of_measurement_id'],
            'item_brand_name' => $data['item_brand_name'],
            'item_class_name' => $data['item_class_name'],
            'item_name' => $data['item_name'],
            'unit_of_measurement_name' => $data['unit_of_measurement_name'],
            'item_group_name' => $data['item_group_name'],
            'item_type_name' => $data['item_type_name'],
            'unit_cost' => $data['unit_cost'],
            'total_cost' => $data['total_cost'],
            'quantity' => $data['quantity'],
        );

        return $this->M_Purchase_order_item->insert($item);
        //            $this->db->insert('purchase_order_items, $item');
        //            return $this->db->insert_id();
    }

    public function process_create()
    {
        header('Content-Type: application/json');
        if ($this->input->post()) {
            $__request = $this->input->post('request');
            $request = [];
            $items = $this->input->post('items');

            $this->load->model('item/item_model', 'M_Item');

            if ($items) {

                foreach ($items as $item) {

                    $current_item = $this->M_Item->get($item['item_id']);

                    if (floatval($current_item['unit_price']) != floatval($item['unit_cost'])) {

                        $this->M_Item->update([
                            'unit_price' => $item['unit_cost'],
                            'tax_id' => NULL,
                            'total_price' => $item['unit_cost'],
                            'updated_by' => $this->user->id,
                        ], $item['item_id']);
                    }
                }
            }

            foreach ($this->M_Purchase_order->form_fillables as $key) {
                if (array_key_exists($key, $__request)) {
                    $request[$key] = $__request[$key];
                }
            }

            $this->db->trans_start();
            $this->db->trans_strict(false);

            $additional = array(
                'created_at' => NOW,
                'created_by' => $this->user->id
            );

            $request_object = $this->process_create_master($request, $additional);

            if ($request_object) {
                $references = array(
                    'purchase_order_id' => $request_object,
                    'warehouse_id' => $request['warehouse_id'],
                    'supplier_id' => $request['supplier_id']
                );
                foreach ($items as $item) {
                    $this->process_create_slave($item, $references, $additional);
                }
            }

            $this->db->trans_complete(); # Completing request
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
                $response['message'] = 'Error!';
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();

                $log = $this->M_Purchase_order->get($request_object);
                unset($log['id']);
                $log['purchase_order_id'] = $request_object;
                $log['log_time'] = NOW;
                $log['description'] = 'PO created';
                $this->db->insert('purchase_order_logs', $log);
                $log_ref = $this->db->insert_id();

                $items_saved = $this->M_Purchase_order_item->where('purchase_order_id', $request_object)->get_all();
                foreach ($items_saved as $item_log) {
                    $item_id = $item_log['id'];
                    unset($item_log['id']);
                    $item_log['purchase_order_log_id'] = $log_ref;
                    $item_log['purchase_order_item_id'] = $item_id;
                    $item_log['description'] = 'PO item created';
                    $this->db->insert('purchase_order_item_logs', $item_log);
                }
                $response['status'] = 1;
                $response['message'] = 'Request Successfully saved!';
            }

            echo json_encode($response);
            exit();
        }
    }

    private function process_update_master($id, $data, $additional)
    {
        $old_object = $this->M_Purchase_order->get($id);
        if ($old_object['status'] == 2) {
            $data['status'] = 5;
        }
        return $this->M_Purchase_order->update($data + $additional, $id);
    }

    function process_update_slave($data, $additional, $references)
    {
        $id = isset($data['id']) ? $data['id'] : null;
        $this->load->model('purchase_order_item/Purchase_order_item_model', 'M_Purchase_order_item');

        $temp_data = $data + $additional;
        $update_data = [];
        foreach ($this->M_Purchase_order_item->form_fillables as $key) {
            if (array_key_exists($key, $temp_data)) {
                $update_data[$key] = $temp_data[$key];
            }
        }

        if ($id) {
            // return $this->db->update('purchase_order_items');
            return $this->M_Purchase_order_item->update($update_data, $id);
        } else {
            $additional = [
                'created_by' => $additional['updated_by'],
                'created_at' => $additional['updated_at']
            ];

            return $this->process_create_slave($data, $references, $additional);
        }
    }

    public function log_status()
    {
        $id = $this->input->post('id');
        $log = $this->M_Purchase_order->get($id);
        unset($log['id']);
        $log['purchase_order_id'] = $id;
        $log['log_time'] = NOW;
        $log['description'] = "Status changed";
        $this->db->insert('purchase_order_logs', $log);
        $log_ref = $this->db->insert_id();
    }

    public function process_update($id)
    {
        header('Content-Type: application/json');
        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        if ($this->input->post()) {
            $__request = $this->input->post('request');
            $request = [];
            $items = $this->input->post('items');

            foreach ($this->M_Purchase_order->form_fillables as $key) {
                if (array_key_exists($key, $__request)) {
                    $request[$key] = $__request[$key];
                }
            }

            $this->db->trans_start();
            $this->db->trans_strict(false);
            $request_object = $this->process_update_master($id, $request, $additional);
            if ($request_object) {

                $references = [
                    'purchase_order_id' => $id,
                    'warehouse_id' => $request['warehouse_id'],
                    'supplier_id' => $request['supplier_id']
                ];
                foreach ($items as $item) {
                    $item_id = $this->process_update_slave($item, $additional, $references);

                    // if (array_key_exists('id', $item)) {
                    //     $item_id = $item['id'];
                    // }

                    // $item_log = $this->M_Purchase_order_item->get($item_id);
                    // unset($item_log['id']);
                    // $item_log['purchase_order_log_id'] = $log_ref;
                    // $item_log['purchase_order_item_id'] = $item_id;
                    // $item_log['description'] = 'PO item updated';
                    // $this->db->insert('purchase_order_item_logs', $item_log);
                }
            }
            $deleted_items = $this->input->post('deleted_items');

            $this->db->trans_complete(); # Completing request
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
                $response['message'] = 'Error!';
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $this->process_purge_deleted($deleted_items, $additional);

                $log = $this->M_Purchase_order->get($id);
                unset($log['id']);
                $log['purchase_order_id'] = $id;
                $log['log_time'] = NOW;
                $log['description'] = 'PO updated';
                $this->db->insert('purchase_order_logs', $log);
                $log_ref = $this->db->insert_id();

                $items_saved = $this->M_Purchase_order_item->where('purchase_order_id', $id)->get_all();
                foreach ($items_saved as $item_log) {
                    $item_id = $item_log['id'];
                    unset($item_log['id']);
                    $item_log['purchase_order_log_id'] = $log_ref;
                    $item_log['purchase_order_item_id'] = $item_id;
                    $item_log['description'] = 'PO item updated';
                    $this->db->insert('purchase_order_item_logs', $item_log);
                }


                $response['status'] = 1;
                $response['message'] = 'Request Successfully saved!';
            }

            echo json_encode($response);
            exit();
        }
    }

    private function process_purge_deleted($items, $additional)
    {
        $this->load->model('purchase_order_item/Purchase_order_item_model', 'M_Purchase_order_item');
        if ($items) {
            foreach ($items as $item) {
                if ($item) {
                    $this->M_Purchase_order_item->delete($item);
                }
            }
        }
    }

    public function form($id = false)
    {
        $this->load->helper('approver');

        $approver_setting = get_approver($this->module_name);
        $this->db->where('user_id = ' . $this->user->id);
        $staff = $this->M_staff->get();
        if ($id) {
            $method = "Update";
            $purchase_order = $this->M_Purchase_order->get($id);
            $form_data = fill_form_data($this->M_Purchase_order->form_fillables, $purchase_order);
            $reference = $form_data['reference'];
            if ($purchase_order['status'] == 2 || $purchase_order['status'] == 5) {
                $is_editable = 0;
            } else {
                $is_editable = 1;
            }
        } else {
            $method = "Create";
            $purchase_order = null;
            $reference = null;
            $form_data = fill_form_data($this->M_Purchase_order->form_fillables);
            $is_editable = 1;
            $this->view_data['approver'] = $approver_setting;
            $this->view_data['staff'] = $staff;
        }

        $this->view_data['fillables'] = $this->M_Purchase_order->form_fillables;
        $this->view_data['current_user'] = $this->user->id;
        $this->view_data['method'] = $method;
        $this->view_data['purchase_order'] = $purchase_order;
        $this->view_data['id'] = $id ? $id : null;
        $this->view_data['form_data'] = $form_data;
        $this->view_data['reference'] = $reference;
        $this->view_data['is_editable'] = $is_editable;

        $this->load->model('suppliers/Suppliers_model', 'M_Supplier');

        $this->view_data['suppliers'] = $this->M_Supplier->get_all();

        $this->js_loader->queue([
            'js/vue2.js',
            // 'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);

        $this->template->build('form', $this->view_data);
    }

    public function __form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Purchase_order->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $purchase_order_status = $this->M_Purchase_order->update($info + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $purchase_order_status = $this->M_Purchase_order->insert($info + $additional);
                }

                if ($purchase_order_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Purchase Order Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_Purchase_order->get($id);
        }

        $this->view_data['method'] = $method;
        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Purchase_order->get($id);
            if ($list) {
                if ($list['status'] == 2 || $list['status'] == 5) {
                    $response['status'] = 0;
                    $response['message'] = 'Cannot delete an approved or amended Purchase Order!';
                    echo json_encode($response);
                    exit();
                }
                $deleted = $this->M_Purchase_order->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Purchase Order Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $obj = $this->M_Purchase_order->get($value);
                    if ($obj['status'] == 2 || $obj['status'] == 5) {
                        // twiddlethumbs
                    } else {
                        $data = [
                            'deleted_by' => $this->session->userdata['user_id']
                        ];
                        $this->db->update('item_group', $data, array('id' => $value));
                        $deleted = $this->M_Purchase_order->delete($value);
                    }
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Purchase Order Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function history($id)
    {
        $this->load->helper('purchase_order');
        $this->load->model('purchase_order_log/purchase_order_log_model', 'M_Purchase_order_log');
        $this->load->model('purchase_order_log/purchase_order_item_log_model', 'M_Purchase_order_item_log');

        $this->M_Purchase_order_log->order_by('id', 'DESC');
        $logs = $this->M_Purchase_order_log->where('purchase_order_id', $id)->get_all();

        foreach ($logs as $key => $log) {
            $logs[$key]['log_items'] = $this->M_Purchase_order_item_log->where('purchase_order_log_id', $log['id'])->get_all();
        }

        $this->view_data['logs'] = $logs;
        $this->view_data['id'] = $id;
        $this->template->build('history', $this->view_data);
    }

    public function view($id = FALSE)
    {
        $this->load->helper('purchase_order');
        if ($id) {
            $this->js_loader->queue([
                'js/vue2.js',
                'js/axios.min.js',
                'js/utils.js'
            ]);

            $purchase_order = $this->M_Purchase_order->with_company()->with_accounting_ledger()->with_purchase_order_request()->with_supplier()->with_approving_staff()->with_requesting_staff()->get($id);

            $this->load->model('purchase_order_item/purchase_order_item_model', 'M_Purchase_order_item');
            $items = $this->M_Purchase_order_item->where(array('purchase_order_id' => $id))->with_item_brand()->with_item_group()->with_item_type()->with_item_class()->with_item()->with_unit_of_measurement()->with_supplier()->with_material_request()->with_purchase_order()->with_purchase_order_request_item()->with_purchase_order_request()->with_warehouse()->get_all();

            if ($purchase_order['status'] == 2 || $purchase_order['status'] == 5) {
                $is_editable = 0;
            } else {
                $is_editable = 1;
            }

            $this->view_data['data'] = $purchase_order;
            $this->view_data['items'] = $items;
            $this->view_data['is_editable'] = $is_editable;
            $this->view_data['id'] = $id;

            if ($this->view_data['data']) {
                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'name' => $upload_data[0],
                        /* ==================== begin: Add model fields ==================== */

                        /* ==================== end: Add model fields ==================== */
                    );

                    $purchase_order_id = $upload_data[0];
                    $purchase_order = $this->M_Purchase_order->get($purchase_order_id);

                    if ($purchase_order) {
                        $result = $this->M_Purchase_order->update($fields, $purchase_order_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'name' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Purchase_order->insert($fields);
                    } else {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Purchase_order->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'purchase_order');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'purchase_order');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? TRUE : FALSE;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Purchase Order CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'purchase_order');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Purchase_order->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Purchase Order');
            $_objSheet->setCellValue('A1', 'PURCHASE ORDER');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $purchase_orders = $this->M_Purchase_order->as_array()->get_all();
        if ($purchase_orders) {

            foreach ($purchase_orders as $_lkey => $purchase_order) {

                $_datas[$purchase_order['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_purchase_orders_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'purchase_order');
                }
            } else {

                $_filename = 'list_of_purchase_orders_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'purchase_order');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($purchase_orders as $_lkey => $purchase_order) {

                                $_datas[$purchase_order['id']][$_title] = isset($purchase_order[$_column]) && ($purchase_order[$_column] !== '') ? Dropdown::get_static('bool', $purchase_order[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($purchase_orders as $_lkey => $purchase_order) {

                                if ($_name === 'status') {

                                    $_datas[$purchase_order['id']][$_title] = isset($purchase_order[$_name]) && $purchase_order[$_name] ? Dropdown::get_static('inventory_status', $purchase_order[$_name], 'view') : '';
                                } else {

                                    $_datas[$purchase_order['id']][$_title] = isset($purchase_order[$_name]) && $purchase_order[$_name] ? $purchase_order[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Purchase Order');
                $_objSheet->setCellValue('A1', 'LIST OF PURCHASE ORDER');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'purchase_order');
            }
        } else {

            $this->notify->error('No Record Found', 'purchase_order');
        }
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            $this->view_data['info'] = $info = $this->M_Purchase_order
                ->with_company()
                ->with_accounting_ledger()
                ->with_purchase_order_request()
                ->with_supplier()
                ->with_approving_staff()
                ->with_requesting_staff()
                ->get($id);

            $this->load->model('purchase_order_item/Purchase_order_item_model', 'M_Purchase_order_item');

            $purchase_order_items = $this->M_Purchase_order_item
                ->where('purchase_order_request_id', $id)
                // ->with_item_group()
                // ->with_item_type()
                // ->with_item_brand()
                // ->with_item_class()
                ->with_item()
                ->with_unit_of_measurement()
                ->get_all();

            $this->view_data['purchase_order_items'] = $purchase_order_items;

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }
}
