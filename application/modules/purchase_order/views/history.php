<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Purchase Order History</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('purchase_order/view/' . $id); ?>"
                   class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-eye"></i>
                </a>
                <a href="<?php echo site_url('purchase_order'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="card card-body">
        <div id="po_history">
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Log Time</th>
                    <th>Description</th>
                    <th>Total</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($logs as $log): ?>
                    <tr>
                        <td><?= $log['id']; ?></td>
                        <td ><?= $log['log_time']; ?></td>
                        <td ><?= $log['description']; ?></td>
                        <td ><?= $log['total']; ?></td>
                        <td ><?= po_status_lookup($log['status']); ?></td>
                    </tr>
                    <?php if($log['log_items']):?>
                        <tr>
                            <th colspan="5">Items</th>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <table class="table-condensed table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Status</th>
                                        <th>Qty</th>
                                        <th>Unit</th>
                                        <th>Unit Cost</th>
                                        <th>Total Cost</th>
                                        <th>Description</th>
                                        <th>Material Receiving</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($log['log_items'] as $li): ?>
                                        <tr>
                                            <td><?= $li['item_name']; ?></td>
                                            <td><?= po_item_status_lookup($li['receiving_status']) ?></td>
                                            <td><?= $li['quantity']; ?></td>
                                            <td><?= $li['unit_of_measurement_name']; ?></td>
                                            <td><?= $li['unit_cost']; ?></td>
                                            <td><?= $li['total_cost']; ?></td>
                                            <td><?= $li['description']; ?></td>
                                            <td>
                                                <?php if ($li['material_receiving_id']): ?>
                                                    <a href="<?= site_url('material_receiving/view/' . $li['material_receiving_id']) ?>">
                                                        View
                                                    </a>
                                                <?php else: ?>
                                                    N/A
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    <?php endif;?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- begin:: Footer -->
