<script type="text/javascript">
    window.object_id = "<?= $id; ?>";
    window.current_user_id = "<?php current_user_id(); ?>";
</script>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Purchase Order</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('purchase_order/history/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-eye"></i> Logs
                </a>
                <a href="<?php echo site_url('purchase_order/form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="<?php echo site_url('purchase_order'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div id="purchase_order_app" class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <?php $this->load->view('modals/note_form'); ?>
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Reference
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['reference']; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Status
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo po_status_lookup($data['status']); ?></span>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Supplier
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['supplier']['name']; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Accounting Ledger
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['accounting_ledger']['name']; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Company
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['company']['name']; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Purchase Order Request
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['purchase_order_request']['reference']; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Requesting Staff
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo get_person_name($data['requesting_staff_id'], "staff"); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Approving Staff
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo get_person_name($data['approving_staff_id'], "staff"); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Total
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($data['total']); ?></span>
                                </div>
                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-widget13__item">
                                <span class="kt-widget13__desc">
                                    Created
                                </span>
                                <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['created_at']; ?> by <?php echo get_person_name($data['created_by'], 'staff'); ?></span>
                            </div>
                            <div class="kt-widget13__item">
                                <span class="kt-widget13__desc">
                                    Updated
                                </span>
                                <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['updated_at']; ?> by <?php echo get_person_name($data['updated_by'], 'staff'); ?></span>
                            </div>
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head py-4">
                    <h4>Notes</h4>
                    <a href="#!" @click.prevent="addNote()" class="btn btn-sm btn-success float-right">
                        Add Note
                    </a>
                </div>
                <div class="kt-portlet__body">
                    <div class="spinner-border text-success" role="status" v-if="notes.loading">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <div v-if="notes.data.length > 0">
                        <div class="card mb-2" v-for="(n, idx) in notes.data">
                            <div class="card-header">
                                <h6>{{ n.creator.first_name }} {{ n.creator.last_name }} <small>on {{ n.created_at }}</small></h6>
                            </div>
                            <div class="card-body">
                                {{ n.content }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Items
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="table-responsive">
                                <table class="table table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Unit Cost</th>
                                            <th>SubTotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($items as $item) : ?>
                                            <tr>
                                                <td><?= $item['item']['name']; ?></td>
                                                <td><?= $item['quantity']; ?></td>
                                                <td><?= money_php($item['unit_cost']); ?></td>
                                                <td><?= money_php($item['total_cost']); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Footer -->