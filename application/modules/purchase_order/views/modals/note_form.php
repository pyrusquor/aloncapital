<div class="modal fade" id="note_modal" tabindex="-1" role="dialog"
     aria-labelledby="note_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Content</label>
                    <textarea class="form-control" rows="4" v-model="notes.form.content"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" @click.prevent="saveNote()">Submit</button>
                <button type="button" class="btn btn-secondary">Close</button>
            </div>
        </div>
    </div>
</div>