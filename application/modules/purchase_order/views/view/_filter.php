<div v-if="! this.rpo.selected">
    <h3>Filter Purchase Order Requests</h3>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="form-group">
                <label>Company</label>
                <select class="form-control suggests" data-module="companies" id="filter_company_id">
                </select>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="form-group">
                <label>Reference #</label>
                <input type="text" class="form-control" id="filter_reference" v-model="rpo.filter.reference">
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="form-group">
                <label>Date of Request <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input type="text" class="form-control kt_datepicker" placeholder="Date of Request"
                           name="request[request_date]" id="filter_request_date"
                           v-model="rpo.filter.request_date" readonly>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                    class="la la-calendar"></i></span>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <div class="btn-group btn-group-sm">
                    <button class="btn btn-primary" @click.prevent="filterRPO()">Filter</button>
                    <button class="btn btn-secondary" @click.prevent="resetRPOFilter()">Clear</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div v-else style="margin: 20px 0;">
    <h3>Purchase Order Request: {{ rpo.selected.reference }}</h3>

    <a class="btn btn-secondary btn-sm" href="<?= base_url("purchase_order/form") ?>" v-if="isEditable()">
        &laquo; Filter RPO
    </a>
</div>
<div class="row" v-if="rpo.data.length > 0 && !rpo.selected">
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Reference</th>
                <th>Date of Request</th>
                <th>Company</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(r, r_key) in rpo.data">
                <td>
                    {{ r.reference }}
                </td>
                <td>
                    {{ r.request_date }}
                </td>
                <td>
                    {{ r.company.name }}
                </td>
                <td>
                    <a class="btn-success btn btn-sm" href="#!" v-if="isEditable()"
                       @click.prevent="selectRPO(r_key); fetchItemsWithCanvass()">
                        Select
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row" v-if="rpo.selected && isEditable()">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Select Supplier</label>
            <select class="form-control" id="supplier_id" v-model="info.form.supplier_id" @change="updateCanvassFilter(); fetchItemsWithCanvass()">
                <option></option>
                <option v-for="supplier in sources.suppliers" :value="supplier.id">
                    {{ supplier.name }}
                </option>
            </select>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="table-responsive" v-if="rpo.items.length > 0">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Group</th>
                    <th>Class</th>
                    <th>Type</th>
                    <th>Brand</th>
                    <th>Quantity</th>
                    <th>Unit/Total Cost</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(rpo_item, rpo_item_key) in rpo.items">
                    <td colspan="8">
                        <table class="table table-condensed table-striped">
                            <tr>
                                <td>
                                    {{ rpo_item.item_name }}
                                </td>
                                <td>
                                    {{ rpo_item.item_group_name }}
                                </td>
                                <td>
                                    {{ rpo_item.item_class_name }}
                                </td>
                                <td>
                                    {{ rpo_item.item_type_name }}
                                </td>
                                <td>
                                    {{ rpo_item.item_brand_name }}
                                </td>
                                <td>
                                    {{ rpo_item.quantity }}
                                </td>
                                <td>
                                    {{ rpo_item.unit_cost }} /
                                    {{ rpo_item.total_cost }}
                                </td>
                                <td>
                                    <a href="#!" class="btn btn-sm btn-warning" @click.prevent="changePrice(rpo_item_key)" v-if="info.form.supplier_id">
                                        Change Price
                                    </a>
                                    <a href="#!" class="btn btn-sm btn-success" @click.prevent="addItemToPurchaseOrder(rpo_item_key)" v-if="info.form.supplier_id">
                                        Add to PO
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="8" style="background-color: #ccc; text-align: center;">Canvass Results</th>
                            </tr>
                            <tbody>
                                <tr v-for="(cnv, cnv_idx) in rpo_item.canvass" v-if="rpo_item.canvass.length > 0">
                                    <th colspan="2">Supplier</th>
                                    <td colspan="2">{{ cnv.supplier.name }}</td>
                                    <th colspan="2">Unit Price</th>
                                    <td colspan="2">{{ cnv.unit_cost }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
