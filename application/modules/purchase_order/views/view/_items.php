<div class="row" v-if="items.data.length > 0">
    <div class="col-sm-12">
        <h4>Purchase Order Items </h4>

        <div class="card" v-for="(po_item, po_item_key) in items.data">
            <div class="card-header">
                <a href="#!" class="btn btn-sm btn-danger float-right" @click.prevent="removeItemFromPurchaseOrder(po_item_key)" style="color: white !important;" v-if="isEditable()">
                    Remove
                </a>
                <h5>{{ po_item.item_name }}</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label>Supplier</label>
                            <select class="form-control" v-model="items.data[po_item_key].supplier_id" disabled>
                                <option v-for="supplier in sources.suppliers" :value="supplier.id">
                                    {{ supplier.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label>Quantity</label>
                            <input min="0" type="number" class="form-control" v-model="items.data[po_item_key].quantity" @change="updateSubTotal(po_item_key); calculateTotal()">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label>Sub Total</label>
                            <input type="number" disabled class="form-control" :value="items.data[po_item_key].total_cost">
                            <p>
                                <small>Unit Cost: {{ items.data[po_item_key].unit_cost }}</small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-sm-12 my-4">
        <h5>Total: {{ info.form.total }}</h5>
    </div>
</div>