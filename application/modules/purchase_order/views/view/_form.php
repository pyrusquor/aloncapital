<?php
    if ($id) {
        $approving_staff_info = get_person($form_data['approving_staff_id'], 'staff');
        $approving_staff_name = get_fname($approving_staff_info);

        $requesting_staff_id_info = get_person($form_data['requesting_staff_id'], 'staff');
        $requesting_staff_name = get_fname($requesting_staff_id_info);


    } else {
        $approving_staff_info = null;
        $approving_staff_name = '';
        $requesting_staff_id_info = null;
        $requesting_staff_name = '';
        if(isset($staff)){
            $form_data['requesting_staff_id'] = $staff['id'];
            $requesting_staff_name = $staff['last_name'] . ' ' . $staff['first_name'];
        }
        if(isset($approver)){
            $form_data['approving_staff_id'] = $approver['staff']['id'];
            $approving_staff_name = $approver['staff']['last_name'] . ' ' . $approver['staff']['first_name'];
        }

    }
?>
<script>
    window.requesting_staff_id = <?= $form_data['requesting_staff_id'] ?>
</script>
<div class="row">
    <?php if($is_editable == 1):?>
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id"
                    name="request[approving_staff_id]">
                <?php if ($approving_staff_name): ?>
                    <option value="<?php echo $form_data['approving_staff_id']; ?>"
                            selected>
                        <?php echo $approving_staff_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="staff" data-type="person"
                    id="requesting_staff_id" name="request[requesting_staff_id]">
                <?php if ($requesting_staff_name): ?>
                    <option value="<?php echo $form_data['requesting_staff_id']; ?>"
                            selected><?php echo $requesting_staff_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <?php endif;?>
</div>