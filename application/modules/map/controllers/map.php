<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Map extends MY_Controller
{

	public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('property/Property_model', 'M_property');
        $this->load->model('map/map_model', 'M_map');
    }
	
	public function index()
	{

		$this->template->title('REMS', 'Interactive Map');

		// overwrite default theme and layout if needed
		$this->template->set_theme('default');
		$this->template->set_layout('default');

		$this->css_loader->queue(['vendors/custom/map/css/style.css']);
		$this->css_loader->queue(['vendors/custom/map/css/magnific-popup.css']);
		$this->css_loader->queue(['vendors/custom/map/mapplic/mapplic.css']);

		$this->js_loader->queue(['vendors/custom/map/js/jquery.min.js']);
		$this->js_loader->queue(['vendors/custom/map/js/jquery.mousewheel.js']);
		$this->js_loader->queue(['vendors/custom/map/js/magnific-popup.js']);
		$this->js_loader->queue(['vendors/custom/map/js/script.js']);
		$this->js_loader->queue(['vendors/custom/map/js/csvparser.js']);
		$this->js_loader->queue(['vendors/custom/map/mapplic/mapplic.js']);

		$data = $this->M_map->with_property()->as_array()->get_all();

		$this->view_data['data'] = $data;
		

		$this->template->build('index');
	}

	public function view_map() {

		$data = $this->M_map->with_property()->as_array()->get_all();

		$path = FCPATH . 'assets' . DIRECTORY_SEPARATOR . 'coordinates' . DIRECTORY_SEPARATOR . "json" . DIRECTORY_SEPARATOR;

		$map_path =  FCPATH . 'assets' . DIRECTORY_SEPARATOR . 'coordinates' . DIRECTORY_SEPARATOR . "map" . DIRECTORY_SEPARATOR;

		$mapwidth = '1300';
		$mapheight = '1300';

		$map_array = array();
		$svg_array = array();

		$locations = $this->get_locations($data, $path);
		

		$map_array = array(
			"mapwidth" => $mapwidth,
			'mapheight' => $mapheight,
			"defaultstyle" => "default",
			"levels" => array(
				array (
					"id" => "underground",
					"title" => "Underground",
					"map" => base_url("map/_map.php"),
					"locations" => $locations,
				)
			),
		);

		$map_data = array_merge($map_array, $locations);
	
		file_put_contents($path . "1.json", json_encode($map_data));

		if(!empty($data)) {

			foreach ($data as $key => $p) {
				array_push($svg_array, $p['path']);
				file_put_contents($map_path . "1.svg", $svg_array);
			}
			
		}

        $output = array(
            'data' => $map_array,
		);
		
        echo json_encode($output);
		
	}

	public function get_locations($data = array(), $path = "") {
		$locations = [];

		if(!empty($data)) {

			foreach ($data as $key => $location) {
				$property = $location['property'];

				$prop_status = $this->property_status($property['status']);
				
				$loc = array(
					"id" => "lot" . $key ,
					"title" => $property['name'],
					"pin" => "hidden",
					"description" => $prop_status['name'],
					"link" => '#more',
					"x" => $location['x_axis'],
					"y" => $location['y_axis'],
				);

				array_push($locations, $loc);

			}

		}
		

		return $locations;
		

	}

	public function property_status($status = 0) {

		$prop_status = [];

		if($status) {

			switch ($status) {
				case 1:
					$prop_status['name'] = "Available";
					$prop_status['color'] = "#009688";
					break;
				case 2:
					$prop_status['name'] = "Reserved";
					$prop_status['color'] = "#ffea00";
					break;
				case 3:
					$prop_status['name'] = "Sold";
					$prop_status['color'] = "#ff1744";
					break;
				case 4:
					$prop_status['name'] = "Hold";
					$prop_status['color'] = "#ff1744";
					break;
				case 5:
					$prop_status['name'] = "Not For Sale";
					$prop_status['color'] = "#ff1744";
					break;
				
				default:
					# code...
					break;
			}

			return $prop_status;
			
		}

	}

	public function view($project_id = '', $floor_id = '') {
		// define('DS', DIRECTORY_SEPARATOR);
		// chmod(FCPATH . 'assets' . DS . 'coordinates', 0777);
		// chmod(FCPATH . 'assets' . DS . 'js' . DS . 'map_json', 0777);
		// chown(FCPATH . 'assets' . DS . 'js' . DS . 'coordinates', 'ec2-user');
		// chown(FCPATH . 'assets' . DS . 'js' . DS . 'map_json', 'ec2-user');
		$company_item = $this->config->item('company');
		// $company_item['folder'] = 'ptr';
		$printable		= 0;
		$format			= 'pdf';
		$type			= '';
		$size			= 'A4';
		$orientation	= 'L';
		$get = $this->input->get();
		if (!empty($get)) {
			$printable		= !empty($get['printable']) ? (int) $get['printable'] : $printable;
			$type			= !empty($get['type']) ? (int) $get['type'] : $type;
			$format			= !empty($get['format']) ? $get['format'] : $format;
			$size			= !empty($get['size']) ? $get['size'] : $size;
			$orientation	= !empty($get['orientation']) ? $get['orientation'] : $orientation;
		}
		
        // $this->output->cache(100);
		$project_id = explode('#',$project_id);
		$project_id = $project_id[0];
		if (empty($project_id)) {
			redirect('dashboard');
		} else {
			$count = $this->Virtual_map_model->get_all_map(array('project_id'=>$project_id, 'floor_id'=>$floor_id), true );
			if (empty($count)) {
				redirect('dashboard');
			}
		}
		$data['house_types'] 	= $this->Unit_type_model->get_all_unit_type(array('project_id'=>$project_id));
		$data['minmaxprice'] 	= $this->Virtual_map_model->get_min_max_price($project_id);
		$data['minmaxlotarea']	= $this->Virtual_map_model->get_min_max_lot_area($project_id);
		if ($company_item['folder'] == 'ptr') {
			if ($project_id == 1) {
				$mapwidth = '694.465';//must be same with the width of the real svg
	      		$mapheight = '844.458';//must be same with the height of the real svg
			} else {
				$mapwidth = '564.153';//must be same with the width of the real svg
	      		$mapheight = '798.583';//must be same with the height of the real svg
			}
		}
 		$loc = array();
		// $project_id = 1;
		$paramsProp = array('project_id'=>$project_id, 'no_need' => '1');
		if (!empty($floor_id)) { $paramsProp['block'] = $floor_id; }
		$properties = $this->Property_model->get_all_property($paramsProp);
		foreach ($properties as $key => $property) {
			if ($property['property_status_id'] == 1) {
				$status = 'sold';
			} else if ($property['property_status_id'] == 2) {
				$status = 'available';
			} else if (($property['property_status_id'] == 3)||($property['property_status_id'] == 4)) { 
				$status = 'sold';
			} else {
				$status = 'not-available';
			}
			$unit_type_logo= $this->db->get_where('cornerstone_core_documents',array('module'=>'unit_types_logo','module_id'=>$property['id']))->result_array();
			if (!empty($unit_type_logo))
				$unit_type_logo = $unit_type_logo[0]['filename'];
			else
				$unit_type_logo = '';
			array_push($loc,array(
		      // 'id' => $property['code'].'blk'.$property['block'],
			  'id' => $property['property_id'] .'-B'. $property['block'] .'-L'. $property['lot'],
		      'label' => $property['unit_type_name'],
		      'description' =>  $property['floor_area'],
		      'type' =>  $property['unit_type_name'],
		      'category' => $status,
		      'image' => base_url().'assets/img/unit_types/'.$unit_type_logo,
		      'x' => $property['x_axis'],
		      'y' => $property['y_axis']
		      )
		    );
		}
	    $project = array(
		  	'mapwidth' => $mapwidth,//must be same with the width of the real svg
	      	'mapheight' => $mapheight,//must be same with the height of the real svg
	      	'categories' => array(
              	array(
	                'id'  =>  'available',
	                'title' =>  'Available',
	                'color' =>  '#BCD86B'
                ),
              	array(
	                'id'  =>  'not-available',
	                'title' =>  'Not Available',
	                'color' =>  '#E26684'
                ),
            ),
	      	'levels' => array(
	        	array(
	            	'id'  => 'ground',
	            	'name'  => '0',
	            	'title' => 'Ground Floor',
	            	'map' => base_url().'assets/coordinates/'.$company_item['folder'].'/'.$project_id.'.php',
	            	'minimap' => base_url().'assets/img/mini_map/'.$company_item['folder'].'/'.$project_id.'.png',
           			'show'  => 'true',
	            	'locations' => array(
                    	array(
	                    	'id' => '192',
	                    	'label' => '192',
	                    	'category' => 'available',
	                    	'x' => '0.195',
	                    	'y' => '0.715',
	                    	'zoom' => '10'
	                  	)
	                )
	            ),
	        ),
	    );
		$project['levels'][0]['locations'] = $loc;
		if (!empty($floor_id)) {
			$fp = fopen(FCPATH . 'assets/js/map_json/'.$company_item['folder'].'/'.$project_id.'/'.$floor_id.'.json', 'w');
		} else {
			$fp = fopen(FCPATH . 'assets/js/map_json/'.$company_item['folder'].'/'.$project_id.'.json', 'w');
		}
		
		fwrite($fp, str_replace('\\/', '/',json_encode($project)));
		fclose($fp);
		$data['project_id'] = $project_id;
		$data['floor_id'] = $floor_id? $floor_id : 0;
		$data['printable'] = (int) $printable;
		$data['format'] = $format;
		$data['size'] = @$size;
		$data['orientation'] = $orientation;
		$data['type'] = $type;
		if ($format === 'pdf') {
			$this->load->view('map/header', $data);
			$this->load->view('map/map', $data);
			$this->load->view('map/footer', $data);
		} else if ($format === 'jpg') {
			//$html_content = $this->load->view('map/header', $data, true);
			//$html_content = $this->load->view('map/map', $data, true);
			$this->load->view('map/map2', $data);
			$this->output->set_content_type('svg');
			$content = $this->output->get_output();
			$path = FCPATH . 'assets' . DIRECTORY_SEPARATOR . 'coordinates' . DIRECTORY_SEPARATOR . $company_item['folder'] . DIRECTORY_SEPARATOR;
			$filename  = $project_id;
			write_file($path . $filename . '.svg', $content, 'w');
			$html2pdfclassfile = dirname(__FILE__) . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'libraries' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'tecnickcom' . DIRECTORY_SEPARATOR . 'tcpdf' . DIRECTORY_SEPARATOR . 'tcpdf.php';
			require_once($html2pdfclassfile);
			try {
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->AddPage();
				$pdf->ImageSVG($path . $filename . '.svg', $x=15, $y=30, $w='', $h='', $link='http://www.tcpdf.org', $align='', $palign='', $border=0, $fitonpage=true);
				$pdf->Output($path . $filename . '.pdf', 'F');
				exec('convert ' . $path . $filename . '.pdf' . ' ' . $path . $filename . '.jpg');
				//$pdfFile = file_get_contents($path . $filename . '.pdf');
				// $im = new Imagick();
				// // $im->readImageBlob($pdfFile);
				// chmod($path . $filename . '.pdf', 0777);
				// $f = fopen($path . $filename . '.pdf', 'r');
				// fseek($f, 0);
				// $im->readimagefile($f);
				// fclose($f);
				// $im->flattenImages();
				// // $im->readImage($path . $filename . '.pdf[0]');
				// $im->setImageFormat('jpeg');
				// //$im->writeImage($path . $filename . '.jpg');
				// $jpg = $im->getImageBlob();
				// echo '<img src='image/png;base64,'.base64_encode($jpg).'' />';
				// $im->clear(); 
				// $im->destroy();
				exit();
				//write_file($path . $filename . '.jpg', $im, 'w');
				//header('Content-Type: image/jpeg');
				//$thumbnail = $im->getImageBlob();
				//exit($thumbnail);
				//$im->clear();
				//$im->destroy();
		    } catch(exception $e) {
		        echo $e;
		        exit;
		    }
		}
	}

}

