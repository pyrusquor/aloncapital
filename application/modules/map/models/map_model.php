<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map_model extends MY_Model
{
    public $table = 'virtual_map'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'x_axis',
        'y_axis',
        'path',
        'property_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['property'] = array('foreign_model' => 'property/Property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');

    }

    public function get_properties() {
        $this->db->select('*');
        $this->db->from("virtual_map vm");
        $this->db->from("properties p", "vm.property_id = p.id", "left");

        $result = $this->db->get('virtual_map')->result_array();

        return $result;

    }
}