<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Interactive Map </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Features </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Dashboard </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->


<div class="row">
    <div class="col-md-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div id="wrap">

                    <!-- Site content -->
                    <div id="content">
                        
                        <section id="map-section" class="inner over">
                          
                            <div class="map-container" style="margin-top:200px;">
                                <div id="mapplic"></div> <!-- Map -->
                            </div>

                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


