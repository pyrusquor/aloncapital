<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aris extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Aris_model', 'M_Aris');
        $this->load->model('Aris_document_model', 'M_aris_document');
        $this->load->model('Aris_lot_model', 'M_aris_lot');
        $this->load->model('Aris_collectible_model', 'M_collectible');
        $this->load->model('Aris_payment_model', 'M_aris_payment');
        $this->load->model('Aris_ar_model', 'M_aris_ar');
        // $this->load->model('Aris_project_information_model', 'M_aris_project_information');
        $this->load->model('Aris_past_due_model', 'M_aris_pastdue');
        $this->load->model('Aris_customer_model', 'M_aris_customer');
        $this->load->model('Aris_sales_agent_model', 'M_aris_sales_agent');
        $this->load->model('buyer/Buyer_model', 'M_buyer');
        $this->load->model('seller/Seller_model', 'M_seller');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('project/Project_model', 'M_project');
        $this->load->model('property/Property_model', 'M_property');
        $this->load->model('transaction/Transaction_model', 'M_transaction');
        $this->load->model('transaction/Transaction_property_model', 'M_transaction_property');
        $this->load->model('transaction/Transaction_billing_model', 'M_transaction_billing');
        $this->load->model('transaction/Transaction_seller_model', 'M_transaction_seller');
        $this->load->model('transaction/Transaction_client_model', 'M_transaction_client');
        $this->load->model('transaction/Transaction_payment_model', 'M_transaction_payment');
        $this->load->model('transaction_payment/Transaction_official_payment_model', 'M_Transaction_official_payment');
        $this->load->model('commissions/commissions_model', 'M_commission');
        $this->load->model('financing_scheme_categories/financing_scheme_category_model', 'M_financing_scheme_category');
        $this->load->model('financing_scheme/financing_scheme_values_model', 'M_financing_scheme_value');
        $this->load->model('financing_scheme/financing_scheme_model', 'M_financing_scheme');
        $this->load->model('transaction/Transaction_discount_model', 'M_transaction_discount');
        $this->load->model('accounting_entries/Accounting_entries_model', 'M_accounting_entries');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_accounting_entry_items');

        $this->load->helper('form');

        $this->load->library('mortgage_computation');

        $this->_table_fillables = $this->M_Aris->fillable;
        $this->_table_columns = $this->M_Aris->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW
        ];

        $this->additional = [
            'is_active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW
        ];
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Aris->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            // $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Aris->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
            $this->view_data['buyers'] = $this->M_buyer->as_array()->get_all();
            $this->view_data['sellers'] = $this->M_seller->as_array()->get_all();
            $this->view_data['customers'] = $this->M_aris_customer->as_array()->get_all();
            $this->view_data['sales_agent'] = $this->M_aris_sales_agent->as_array()->get_all();
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'SetupID' => true,
            'ProjectName' => true,
            'Address' => true,
            'City' => true,
            'Province' => true,
            'PostalCode' => true,
            'Country' => true,
            'PhoneNumber' => true,
            'FaxNumber' => true,
            'Remarks' => true,
            'last_date_synced' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Aris
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("SetupID like '%$searchValue%'");
                $query->or_where("ProjectName like '%$searchValue%'");
                $query->or_where("Address like '%$searchValue%'");
                $query->or_where("City like '%$searchValue%'");
                $query->or_where("Province like '%$searchValue%'");
                $query->or_where("PostalCode like '%$searchValue%'");
                $query->or_where("Country like '%$searchValue%'");
                $query->or_where("PhoneNumber like '%$searchValue%'");
                $query->or_where("FaxNumber like '%$searchValue%'");
                $query->or_where("Remarks like '%$searchValue%'");
                $query->or_where("last_date_synced like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'ProjectName' => [
                    'data' => $filters['ProjectName'] ?? null,
                    'operator' => 'like',
                ],
                'Address' => [
                    'data' => $filters['Address'] ?? null,
                    'operator' => 'like',
                ],
                'City' => [
                    'data' => $filters['City'] ?? null,
                    'operator' => 'like',
                ],
                'Province' => [
                    'data' => $filters['Province'] ?? null,
                    'operator' => 'like',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    // foreach ($items as $key => $value) {

                    //     //
                    // }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Aris->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Aris->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $aris_status = $this->M_Aris->update($info + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $aris_status = $this->M_Aris->insert($info + $additional);
                }

                if ($aris_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Aris Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_Aris->get($id);
        }

        $this->view_data['method'] = $method;
        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Aris->get($id);
            if ($list) {

                $deleted = $this->M_Aris->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Aris Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_Aris->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Aris Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = FALSE)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $view_data = $this->M_Aris
                ->with_documents('order_inside:id asc')
                ->with_aris_lots()
                ->with_customers()
                ->with_sales_agent()
                ->get($id);

            $aris_project = $this->M_Aris->get($id);
            $project = $this->M_project->where('name', $aris_project['ProjectName'])->get();

            if ($view_data['documents']) {
                foreach ($view_data['documents'] as $key => $value) {
                    $transaction = $this->M_transaction->where(array('aris_id' => $id . '-' . $value['DocumentID']))->get();
                    if ($transaction) {
                        $view_data['documents'][$key]['rems_transaction_id'] = $transaction['id'];
                    } else {
                        $view_data['documents'][$key]['rems_transaction_id'] = 0;
                    }
                }
            }

            $this->view_data['data'] = $view_data;
            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function load_document_information($id = 0)
    {
        $results = '';
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $view_data = $this->M_Aris
                ->with_documents('order_inside:id asc')
                ->with_aris_lots()
                ->with_customers()
                ->with_sales_agent()
                ->get($id);

            $aris_project = $this->M_Aris->get($id);
            $project = $this->M_project->where('name', $aris_project['ProjectName'])->get();

            if ($view_data['documents']) {
                foreach ($view_data['documents'] as $key => $value) {
                    $transaction = $this->M_transaction->where(array('aris_id' => $id . '-' . $value['DocumentID']))->get();
                    if ($transaction) {
                        $view_data['documents'][$key]['rems_transaction_id'] = $transaction['id'];
                    } else {
                        $view_data['documents'][$key]['rems_transaction_id'] = 0;
                    }
                }
            }
            $results = $this->load->view('view/_document_information', $view_data, TRUE);
        }
        echo $results;
    }

    public function document($project_id = FALSE, $id = FALSE)
    {
        // id = Document ID
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);
            $params['project_id'] = $project_id;
            $params['DocumentID'] = $id;

            $this->view_data['data'] = $data = $this->M_aris_document->where($params)->get();
            $this->view_data['payments'] = $this->M_aris_payment->where($params)->order_by('DATE_DUE', 'desc')->get_all();
            $this->view_data['lot'] = $this->M_aris_lot->where(array('project_id' => $project_id, 'LotID' => $data['LOTID']))->get();


            if ($this->view_data['data']) {

                $this->template->build('view/_document_detail', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function lot($project_id = FALSE, $id = FALSE)
    {
        // id = Document ID
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_aris_lot->where(array('project_id' => $project_id, 'LotID' => $id))->get();

            if ($this->view_data['data']) {

                $this->template->build('view/_lot_detail', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function sync_transaction($project_id = FALSE, $id = FALSE, $multiple = 0, $special_sync = 0, $special_dates = 0)
    {
        $response['status'] = 0;
        $response['message'] = 'Oops something went wrong.';
        // id = Document ID
        if ($id) {
            $this->db->trans_begin();
            $document = $this->M_aris_document->where(array('project_id' => $project_id, 'DocumentID' => $id))->get();
            $params['project_id'] = $project_id;
            $params['LotID'] = $document['LOTID'];

            // vdebug($document);
            // Get ARIS Project Data
            $aris_project = $this->M_Aris->get($project_id);

            // Get REMS Project using ARIS Project name
            $project = $this->M_project->where('name', $aris_project['ProjectName'])->get();

            // vdebug($aris_project);

            if (!$project) {
                $response['status'] = 0;
                $response['message'] = 'Cannot find a project for this transaction. Please contact your administrator ' . print_r($aris_project);

                $this->db->trans_rollback();
                if ($multiple) {
                    return $response;
                } else {
                    echo json_encode($response);
                    exit();
                }
            }

            // Check related financing scheme
            $financing_id = $this->get_financing_scheme($document, 'sync');

            if (!$financing_id) {
                $response['status'] = 0;
                $response['message'] = 'Cannot find a financing scheme for this transaction. Please contact your administrator';

                $this->db->trans_rollback();
                if ($multiple) {
                    return $response;
                } else {
                    echo json_encode($response);
                    exit();
                }
            }



            $lot = $this->M_aris_lot->where($params)->get();
            if ($lot) {

                $lot_no = $lot['Lot_no'];

                // Get property phase, block and lot value
                $data = str_split($lot_no, 3);
                $phase = $data[0];
                $block = $data[1];
                $_lot = $data[2];

                $param_one = 'project_id = ' . $project['id'] . ' AND phase = ' . $phase . ' AND block = ' . $block . ' AND lot = ' . $_lot;
                $param_two = 'project_id = ' . $project['id'] . ' AND block = ' . $block . ' AND lot = ' . $_lot;

                // get REMS groperty using aris lot number
                $this->db->from('properties');
                $this->db->where($param_one);
                $this->db->or_where($param_two);
                $query = $this->db->get()->row();

                $property = $query;

                if ($property) {
                    // Check if transaction exist
                    $transaction = $this->M_transaction->where(array('aris_id' => $project_id . '-' . $id))->get();

                    if ($transaction) {
                        $info['aris_id'] = $document['project_id'] . '-' . $document['DocumentID'];

                        // update document id in transaciont's aris id 
                        // field: DocumentID
                        $transaction_status = $this->M_transaction->update($info + $this->u_additional, $transaction['id']);

                        // $this->M_aris_document->update(array('last_date_synced' => NOW), $document['id']);
                        $this->db->where('id', $document['id']);
                        $this->db->update('aris_documents', array('last_date_synced' => NOW));


                        $this->db->trans_commit();
                        $response['status'] = 2;
                        $response['message'] = 'Transaction sucessfully updated! Record sucessfully synchronized!';
                        $response['id'] = $transaction['id'];
                    } else { // transaction doesn't exists
                        // Create transaction based on ARIS document
                        // Get customer detail in ARIS
                        unset($params);
                        $params['project_id'] = $project_id;
                        $params['CUSTOMERID'] = $document['CUSTOMERID'];
                        // $this->M_aris_document->update(array('last_date_synced' => NOW), $document['id']);
                        $this->db->where('id', $document['id']);
                        $this->db->update('aris_documents', array('last_date_synced' => NOW));


                        // Currrent implementation of this method fails because the customerid of the aris_customers is not unique. 
                        // TODO: sync customer method should be rewritten.

                        $customer = $this->M_aris_customer->where(array('project_id' => $project_id, 'CUSTOMERID' => $document['CUSTOMERID']))->get();
                        $sales_agent = $this->M_aris_sales_agent->where(array('project_id' => $project_id, 'SalesAgentID' => $document['DM']))->get();

                        if ($customer) {
                            $customer['LASTNAME'] = str_replace('?', 'Ñ', $customer['LASTNAME']);
                            $customer['FIRSTNAME'] = str_replace('?', 'Ñ', $customer['FIRSTNAME']);
                            $customer['EMAIL_ADDR'] = str_replace('?', 'Ñ', $customer['EMAIL_ADDR']);
                        }


                        if ($sales_agent) {
                            $sales_agent['Lastname'] = str_replace('?', 'Ñ', $sales_agent['Lastname']);
                            $sales_agent['Firstname'] = str_replace('?', 'Ñ', $sales_agent['Firstname']);
                        }

                        $buyer = $this->M_buyer->where(array('first_name' => $customer['FIRSTNAME'], 'last_name' => $customer['LASTNAME']))->get();
                        $seller = $this->M_seller->where(array('first_name' => $sales_agent['Firstname'], 'last_name' => $sales_agent['Lastname']))->get();
                        if (!$buyer) {
                            // create buyer in REMS    
                            $buyer_id = $this->create_buyer($customer);
                            if (!$buyer_id) {
                                $response['status'] = 0;
                                $response['message'] = "Oops! Something went wrong while creating the customer's account!";

                                $this->db->trans_rollback();
                                if ($multiple) {
                                    return $response;
                                } else {
                                    echo json_encode($response);
                                    exit();
                                }
                            }
                        } else {
                            $buyer_id = $buyer['id'];
                        }

                        if (!$seller) {
                            // create seller in REMS
                            $seller_id = $this->create_seller($sales_agent);

                            if (!$seller_id) {
                                $response['status'] = 0;
                                $response['message'] = "Oops! Something went wrong while creating the seller's account!";

                                $this->db->trans_rollback();
                                if ($multiple) {
                                    return $response;
                                } else {
                                    echo json_encode($response);
                                    exit();
                                }
                            }
                            // vdebug($seller_id);

                        } else {
                            $seller_id = $seller['id'];
                        }

                        $transaction_id = $this->create_transaction($document, $property, $buyer_id, $seller_id, $financing_id);

                        if (!$transaction_id) {
                            $response['status'] = 0;
                            $response['message'] = 'Failed to create transaction!';

                            $this->db->trans_rollback();
                            if ($multiple) {
                                return $response;
                            } else {
                                echo json_encode($response);
                                exit();
                            }
                        }

                        $discount_amount = floatval($document['DISCOUNT']) + floatval($document['DISCOUNT_P']);

                        // Create transaction property
                        $t_property = $this->process_property($property, $lot, $transaction_id, $discount_amount);

                        // vdebug($t_property);

                        if (!$t_property) {
                            $response['status'] = 0;
                            $response['message'] = 'Failed to create transaction property!';

                            $this->db->trans_rollback();
                            if ($multiple) {
                                return $response;
                            } else {
                                echo json_encode($response);
                                exit();
                            }
                        }

                        // Get Discount


                        // vdebug($document);
                        // Process terms
                        $this->process_terms($transaction_id, $financing_id, $document['RADATE'], $t_property, $document, $discount_amount, $special_sync, $special_dates);

                        if ($discount_amount) {
                            $this->process_promos($discount_amount, $transaction_id);
                        }

                        // Process seller
                        $this->process_seller($transaction_id, $seller_id);

                        // Process client
                        $this->process_client($transaction_id, $buyer_id);



                        // Process mortgage
                        $this->process_mortgage($transaction_id, 'save');

                        $this->M_aris_document->update(array('last_date_synced' => NOW), $document['id']);

                        $this->db->trans_commit();
                        $response['status'] = 1;
                        $response['message'] = 'Transaction successfully created! Record sucessfully synchronized from ' . "$project_id-$id" . '! Transaction ID: ' . $transaction_id;
                        if ($multiple) {
                            return $response;
                        } else {
                            echo json_encode($response);
                            exit();
                        }
                    }
                } else {
                    $this->db->trans_commit();
                    $response['status'] = 0;
                    $response['message'] = 'Cannot sync record. No property found!';
                    if ($multiple) {
                        return $response;
                    } else {
                        echo json_encode($response);
                        exit();
                    }
                }
            } else {
                $this->db->trans_commit();
                $response['status'] = 0;
                $response['message'] = 'Cannot sync record. No ARIS lot record found.';
                if ($multiple) {
                    return $response;
                } else {
                    echo json_encode($response);
                    exit();
                }
            }
        } else {
            show_404();
        }
    }

    public function process_promos($discount_amount, $transaction_id = 0)
    {
        $info['transaction_id'] = $transaction_id;
        $info['discount_id'] = '2';
        $info['discount_amount_rate'] = $discount_amount;
        $info['deduct_to'] = 'TCP';
        $info['remarks'] = 'Deductable to TCP';
        $this->M_transaction_discount->insert($info + $this->additional);
    }

    public function collectibles($project_id = FALSE, $id = FALSE)
    {
        // id = Document ID
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_collectible->where(array('project_id' => $project_id, 'DocumentID' => $id))->get_all();
            $this->view_data['project_id'] = $project_id;

            $this->template->build('view/_collectibles_detail', $this->view_data);
        } else {
            show_404();
        }
    }

    public function ar($project_id = FALSE, $id = FALSE)
    {
        // id = Document ID
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_aris_ar->where(array('project_id' => $project_id, 'DocumentID' => $id))->get_all();
            $this->view_data['project_id'] = $project_id;

            $this->template->build('view/_ar_detail', $this->view_data);
        } else {
            show_404();
        }
    }

    public function pastdue($project_id = FALSE, $id = FALSE)
    {
        // id = Document ID
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_aris_pastdue->where(array('project_id' => $project_id, 'DocumentID' => $id))->get_all();
            $this->view_data['project_id'] = $project_id;

            $this->template->build('view/_pastdue_detail', $this->view_data);
        } else {
            show_404();
        }
    }


    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        /* ==================== begin: Add model fields ==================== */
                        'SetupID' => $upload_data[0],
                        'ProjectName' => $upload_data[1],
                        'Address' => $upload_data[2],
                        'City' => $upload_data[3],
                        'Province' => $upload_data[4],
                        'PostalCode' => $upload_data[5],
                        'Country' => $upload_data[6],
                        'PhoneNumber' => $upload_data[7],
                        'FaxNumber' => $upload_data[8],
                        'Password' => $upload_data[9],
                        'Confirmation' => $upload_data[10],
                        'Remarks' => $upload_data[11],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $aris_id = $upload_data[0];
                    $aris = $this->M_Aris->get($aris_id);

                    if ($aris) {
                        $result = $this->M_Aris->update($fields, $aris_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'SetupID' => $upload_data[1],
                            'ProjectName' => $upload_data[2],
                            'Address' => $upload_data[3],
                            'City' => $upload_data[4],
                            'Province' => $upload_data[5],
                            'PostalCode' => $upload_data[6],
                            'Country' => $upload_data[7],
                            'PhoneNumber' => $upload_data[8],
                            'FaxNumber' => $upload_data[9],
                            'Password' => $upload_data[10],
                            'Confirmation' => $upload_data[11],
                            'Remarks' => $upload_data[12],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Aris->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'SetupID' => $upload_data[0],
                            'ProjectName' => $upload_data[1],
                            'Address' => $upload_data[2],
                            'City' => $upload_data[3],
                            'Province' => $upload_data[4],
                            'PostalCode' => $upload_data[5],
                            'Country' => $upload_data[6],
                            'PhoneNumber' => $upload_data[7],
                            'FaxNumber' => $upload_data[8],
                            'Password' => $upload_data[9],
                            'Confirmation' => $upload_data[10],
                            'Remarks' => $upload_data[11],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Aris->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'aris');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'aris');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued    =    $this->input->post('update_existing_data');

            $_is_update    =    $_ued === '1' ? TRUE : FALSE;

            $_alphas        =    [];
            $_datas            =    [];

            $_titles[]    =    'id';

            $_start    =    3;
            $_row        =    2;

            $_filename    =    'Aris CSV Template.csv';

            $_fillables    =    $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'aris');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[]    =    $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group    =    $this->M_Aris->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title]    =    isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas            =    $this->__get_excel_columns(count($_titles));
            $_xls_columns    =    array_combine($_alphas, $_titles);
            $_firstAlpha    =    reset($_alphas);
            $_lastAlpha        =    end($_alphas);

            $_objSheet    =    $this->excel->getActiveSheet();
            $_objSheet->setTitle('Aris');
            $_objSheet->setCellValue('A1', 'ARIS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style    =    array(
                'font'  => array(
                    'bold'    =>    TRUE,
                    'size'    =>    10,
                    'name'    =>    'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $arises    =    $this->M_Aris->as_array()->get_all();
        if ($arises) {

            foreach ($arises as $_lkey => $aris) {

                $_datas[$aris['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_arises_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'aris');
                }
            } else {

                $_filename    =    'list_of_arises_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'aris');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($arises as $_lkey => $aris) {

                                $_datas[$aris['id']][$_title]    =    isset($aris[$_column]) && ($aris[$_column] !== '') ? Dropdown::get_static('bool', $aris[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($arises as $_lkey => $aris) {

                                if ($_name === 'status') {

                                    $_datas[$aris['id']][$_title]    =    isset($aris[$_name]) && $aris[$_name] ? Dropdown::get_static('inventory_status', $aris[$_name], 'view') : '';
                                } else {

                                    $_datas[$aris['id']][$_title]    =    isset($aris[$_name]) && $aris[$_name] ? $aris[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Aris');
                $_objSheet->setCellValue('A1', 'LIST OF ARIS');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'aris');
            }
        } else {

            $this->notify->error('No Record Found', 'aris');
        }
    }

    function sync_buyers()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $customers = $this->M_aris_customer->as_array()->get_all();
        $counter = 0;

        foreach ($customers as $key => $value) {
            $params = [];

            // for some reason the import converts the Ñ symbol as question mark, replace it with 
            // the appropriate symbol

            $value['LASTNAME'] = str_replace('?', 'Ñ', $value['LASTNAME']);
            $value['EMAIL_ADDR'] = str_replace('?', 'ñ', $value['EMAIL_ADDR']);
            $value['FIRSTNAME'] = str_replace('?', 'Ñ', $value['FIRSTNAME']);
            $params['last_name'] = $value['LASTNAME'];
            $params['first_name'] = $value['FIRSTNAME'];

            $buyer = $this->M_buyer->where($params)->get();

            if ($buyer) {
                continue;
                $id = $buyer['id'];
                $info['aris_id'] = $value['CUSTOMERID'];

                $additional = [
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];

                $buyer_status = $this->M_buyer->update($info + $additional, $id);

                $counter += 1;
            } else {
                if (!empty($value['LASTNAME']) && !empty($value['FIRSTNAME'])) {
                    $this->create_buyer($value);
                    $counter += 1;
                }
            }
        }

        if ($counter) {
            $response['status'] = 1;
            $response['message'] = $counter . ' buyer record(s) successfully synced!';
        }

        echo json_encode($response);
        exit();
    }

    function sync_sellers()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $customers = $this->M_aris_sales_agent->as_array()->get_all();
        $counter = 0;

        foreach ($customers as $key => $value) {
            $params = [];

            // for some reason the import converts the Ñ symbol as question mark, replace it with 
            // the appropriate symbol

            $value['Lastname'] = str_replace('?', 'Ñ', $value['Lastname']);
            $value['Firstname'] = str_replace('?', 'Ñ', $value['Firstname']);
            $params['last_name'] = $value['Lastname'];
            $params['first_name'] = $value['Firstname'];
            $seller = $this->M_seller->where($params)->get();

            if ($seller) {
                continue;
                $id = $seller['id'];
                $info['aris_id'] = $value['SalesAgentID'];

                $additional = [
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];

                $seller_status = $this->M_seller->update($info + $additional, $id);

                $counter += 1;
            } else {
                if (!empty($value['Lastname']) && !empty($value['Firstname'])) {
                    $this->create_seller($value);
                    $counter += 1;
                }
            }
        }

        if ($counter) {
            $response['status'] = 1;
            $response['message'] = $counter . ' seller record(s) successfully synced!';
        } else {
            $response['status'] = 0;
            $response['message'] = 'No exact match found!';
        }

        echo json_encode($response);
        exit();
    }

    function ledger($document_id = 0, $project_id = 0, $lot_id = 0, $customer_id = 0, $ledger = 0)
    {
        // $params['where']['payments.DATE_PAID >='] = date('Y-m-d 00:00:00',strtotime($from));
        // $params['where']['payments.DATE_PAID <='] = date('Y-m-d 23:59:59',strtotime($to));

        $params['where']['aris_payments.project_id'] = $project_id;

        if ($customer_id) {
            $params['where']['aris_customers.id'] = $customer_id;
            $param_docs['where']['aris_customers.CUSTOMERID'] = $customer_id;
            $param_docs['where']['aris_customers.project_id'] = $project_id;
        }
        if ($lot_id) {
            $params['where']['aris_lots.id'] = $lot_id;
            $param_docs['where']['aris_lots.LotID'] = $lot_id;
        }

        $params['sort_by'] = "aris_payments.DATE_PAID";
        $params['sort_order'] = "ASC";
        $params['select'] = "aris_customers.LASTNAME, aris_customers.FIRSTNAME, aris_customers.MI, aris_payments.OR_NO, aris_lots.Lot_no, aris_documents.RESAMT_P, aris_payments.PRINCIPAL, aris_payments.INTEREST, aris_payments.PENALTY, aris_payments.PERIOD, aris_payments.AmountPaid, aris_payments.DATE_PAID, aris_payments.DATE_DUE, aris_payments.OR_DATE, aris_payments.REMARKS,aris_documents.PERCENTDPP";

        $params['not_where'] = 1;

        $data['results'] = $this->M_Aris->get_all_collections($params);

        // vdebug($data['results']);

        $param_docs['select'] = "aris_customers.customerID, aris_customers.FIRSTNAME, aris_customers.MI, aris_customers.LASTNAME, aris_lots.Lot_no, aris_lots.Lot_no, aris_lots.Area, aris_lots.Price, aris_lots.Price, aris_lots.Price, aris_documents.RA, aris_documents.RADATE, aris_documents.CTS, aris_documents.CTSDATE, aris_documents.MODE, aris_documents.AMORTAMT, aris_documents.DISCOUNT, aris_documents.INTEREST, aris_documents.PERCENTDP, aris_documents.DPDATE_P, aris_documents.PERCENTDPP";

        $param_docs['where']['aris_lots.project_id'] = $project_id;
        $param_docs['where']['aris_documents.project_id'] = $project_id;

        $param_docs['row'] = 1;
        $data['document'] = $this->M_Aris->get_all_documents($param_docs);

        $data['project_name'] = get_value_field($project_id, 'aris_project_information', 'ProjectName');
        $data['address'] = get_value_field($project_id, 'aris_project_information', 'Address');
        if (!$ledger) {
            # code...
            $data['fileTitle'] = "Customer Ledger";
            $filename = "customer_ledger";
        } else {
            $data['fileTitle'] = "Customer Ledger - Internal";
            $filename = "customer_ledger_internal";
        }

        $receiptHTML = $this->load->view('/reports/aris/' . $filename, $data, true);

        echo $receiptHTML;
        die();
    }



    public function aris_reports_print($id = 0, $project_id = 0, $print = false)
    {
        error_reporting(E_ALL);
        $customer_id = 0;
        $lot_id = 0;
        $report_type = 0;

        if (isset($_POST) && !empty($_POST)) {
            $post = $_POST;
            $project_id = $_POST['project_id'];
            $from = $_POST['from'];
            $to = $_POST['to'];
            $id = $_POST['standard_report_id'];
            $doc_type = $_POST['doc_type'];
            if (isset($_POST['customer_id'])) {
                $customer_id = $_POST['customer_id'];
            }
            if (isset($_POST['lot_id'])) {
                $lot_id = $_POST['lot_no'];
            }
            if (isset($_POST['report_type'])) {
                $report_type = $data['report_type'] = $_POST['report_type'];
            }
        } else {
            $from = date('Y-m-d', strtotime('-1 year'));
            $to = date('Y-m-d');
            $customer_id = 0;
        }
        $data['project_name'] = "ALL";

        if ($project_id) {
            $params['where']['documents.project_id'] = $project_id;
            $params['where']['lots.project_id'] = $project_id;
            $params['where']['customers.project_id'] = $project_id;
            $params['where']['LASTNAME !='] = "";

            $data['project_name'] = get_value_field_aris($project_id, 'project_information', 'ProjectName');
        }

        if ($id) {
            if ($id == 2) { // lot inventory by customer
                if ($lot_id) {
                    $params['where']['lots.id'] = $lot_id;
                }
                if ($customer_id) {
                    $params['where']['customers.id'] = $customer_id;
                }

                $params['select'] = "customers.customerID, customers.FIRSTNAME, customers.MI, customers.LASTNAME, lots.Lot_no, lots.Lot_no, lots.Area, lots.Price, lots.Price, lots.Price, documents.RA, documents.CTS";

                if ($report_type == 'd') { // customer
                    $params['sort_by'] = "customers.LASTNAME";
                    $params['sort_order'] = "ASC";
                    $data['fileTitle'] = 'Lot Inventory by Customer';
                } else if ($report_type == 'e') { // cts
                    $params['sort_by'] = "documents.CTS";
                    $params['sort_order'] = "DESC";
                    $params['where']['documents.CTS !='] = "";
                    $data['fileTitle'] = 'Lot Inventory by CTS';
                } else if ($report_type == 'f') { // lot
                    $params['sort_by'] = "lots.Lot_no";
                    $params['sort_order'] = "ASC";
                    $data['fileTitle'] = 'Lot Inventory by Lot No';
                } else if ($report_type == 'g') { // ra
                    $params['sort_by'] = "documents.RA";
                    $params['sort_order'] = "DESC";
                    $params['where']['documents.RA'] = NULL;
                    $data['fileTitle'] = 'Lot Inventory by RA No';
                }

                $filename = "lot_inventory_customer";
                $data['results'] = $this->Aris_report_model->get_all_documents($params);
            } else if ($id == 4) { // customer directory
                unset($params['where']['lots.project_id']);
                unset($params['where']['documents.project_id']);
                if ($customer_id) {
                    $params['where']['customers.id'] = $customer_id;
                }
                $params['sort_by'] = "customers.LASTNAME";
                $params['sort_order'] = "ASC";
                $params['select'] = "customers.ADDRESS, customers.CITY, customers.POSTALCODE, customers.LASTNAME, customers.FIRSTNAME, customers.MI, customers.POSTALCODE, customers.PROVINCE, customers.COUNTRY, customers.CEL_NO, customers.PHONE_NO, customers.FAX_NO, customers.EMAIL_ADDR";
                $data['results'] = $this->Aris_report_model->get_all_customers($params);
                $data['fileTitle'] = "Customer's Directory";
                $filename = "customer_directory";
            } else if ($id == 5) { // customer collections
                $params['where']['payments.DATE_PAID >='] = date('Y-m-d 00:00:00', strtotime($from));
                $params['where']['payments.DATE_PAID <='] = date('Y-m-d 23:59:59', strtotime($to));
                $params['where']['payments.project_id'] = $project_id;
                if ($customer_id) {
                    $params['where']['customers.id'] = $customer_id;
                }
                if ($lot_id) {
                    $params['where']['lots.id'] = $lot_id;
                }

                $params['sort_by'] = "payments.DATE_PAID";
                $params['sort_order'] = "ASC";
                $params['select'] = "customers.LASTNAME, customers.FIRSTNAME, customers.MI, payments.OR_NO, lots.Lot_no, documents.RESAMT_P, payments.PRINCIPAL, payments.INTEREST, payments.PENALTY, payments.PERIOD, payments.AmountPaid, payments.DATE_PAID";
                $data['results'] = $this->Aris_report_model->get_all_collections($params);

                $data['fileTitle'] = "Collection Report by Customer";
                $filename = "customer_payments";
            } else if ($id == 7) { // customer collectibles
                $params['where']['collectibles.DateDue >='] = date('Y-m-d 00:00:00', strtotime($from));
                $params['where']['collectibles.DateDue <='] = date('Y-m-d 23:59:59', strtotime($to));
                $params['where']['collectibles.project_id'] = $project_id;

                if ($customer_id) {
                    $params['where']['customers.id'] = $customer_id;
                }
                if ($lot_id) {
                    $params['where']['lots.id'] = $lot_id;
                }

                $params['sort_by'] =  "customers.LASTNAME";
                $params['sort_order'] = "ASC";
                $params['select'] = "customers.LASTNAME, customers.FIRSTNAME, customers.MI, lots.Lot_no, collectibles.DateDue, collectibles.Principal, collectibles.Interest, customers.ADDRESS, customers.CITY, customers.POSTALCODE, customers.PROVINCE, customers.COUNTRY";

                if ($report_type == 'h') { // customer
                    $data['fileTitle'] = 'Collectible Report by Customer';
                } else if ($report_type == 'i') { // cts
                    echo "ongoing";
                    die();
                    $data['fileTitle'] = 'Collectible Report by Month';
                } else if ($report_type == 'j') { // lot
                    echo "ongoing";
                    die();
                    $data['fileTitle'] = 'Collectible Report by Year';
                } else if ($report_type == 'k') { // ra
                    $params['sort_by'] = "collectibles.DateDue";
                    $data['fileTitle'] = 'Collectible Report by Date Due';
                }

                $data['results'] = $this->Aris_report_model->get_all_collectibles($params);
                $data['fileTitle'] = "Collectibles Report by Customer";
                $filename = "customer_collectibles";
            } else if ($id == 3) { // fully paid accounts
                $params['where']['payments.OR_DATE >='] = date('Y-m-d 00:00:00', strtotime($from));
                $params['where']['payments.OR_DATE <='] = date('Y-m-d 23:59:59', strtotime($to));
                $params['where']['payments.project_id'] = $project_id;
                $params['where']['payments.PaymentFor'] = 5;
                if ($customer_id) {
                    $params['where']['customers.id'] = $customer_id;
                }
                // if($lot_id){ $params['where']['lots.id'] = $lot_id; }

                $params['sort_by'] = "payments.OR_DATE";
                $params['sort_order'] = "ASC";
                $params['select'] = 'customers.LASTNAME, customers.FIRSTNAME, customers.MI, lots.Lot_no, payments.OR_DATE, payments.OR_NO, documents.RA, documents.CTS, documents.DAS, lots.Area, lots.Price';
                //, DATE_FORMAT(payments.OR_DATE, %M %Y) as group_date
                $to_date    = $to;
                $from_date  = $from;
                $start = $month = strtotime($to_date);
                $end = strtotime($from_date);
                while ($month < $end) {
                    $date = date('Y-m', $month);
                    $month = strtotime("+1 month", $month);
                    $params['like']['payments.OR_DATE'] = $date;
                    $result[$date] = $this->Aris_report_model->get_all_fully_paid($params);
                }
                $data['results'] = $result;
                $data['fileTitle'] = "Fully Paid Accounts by Customer";
                $filename = "customer_fully_paid";
            } else if ($id == 8) { // cancellation report by customer
                $params['where']['payments.project_id'] = $project_id;
                $params['where']['documents.DateCancelled !='] = "";

                if ($customer_id) {
                    $params['where']['customers.id'] = $customer_id;
                }
                if ($lot_id) {
                    $params['where']['lots.id'] = $lot_id;
                }

                $params['sort_order'] = "ASC";
                $params['group_by'] = "payments.DocumentID";
                $params['select'] = 'customers.LASTNAME, customers.FIRSTNAME, customers.MI, lots.Lot_no, documents.DateCancelled, payments.OR_NO, payments.PRINCIPAL, payments.INTEREST, payments.PENALTY, payments.AmountPaid, documents.RA, documents.CTS, documents.DAS, lots.Area, lots.Price';
                // echo "<pre>";print_r($params);print_r($post);die();
                $start = $month = strtotime($from);
                $end = strtotime($to);
                $result = array();
                while ($month < $end) {
                    $date = date('Y-m', $month);
                    $month = strtotime("+1 month", $month);
                    $params['like']['documents.DateCancelled'] = $date;
                    $result[$date] = $this->Aris_report_model->get_all_cancelled($params);
                }

                $data['results'] = $result;
                $data['fileTitle'] = "Cancellation Report by Customer";
                $filename = "cancelled_report_customer";
            } else if ($id == 1) { // pastdue report by customer
                $params['where']['collectibles.DateDue >='] = date('Y-m-d 00:00:00', strtotime($from));
                $params['where']['collectibles.DateDue <='] = date('Y-m-d 23:59:59', strtotime($to));
                $params['where']['collectibles.project_id'] = $project_id;
                if ($customer_id) {
                    $params['where']['customers.id'] = $customer_id;
                }
                if ($lot_id) {
                    $params['where']['lots.id'] = $lot_id;
                }
                $params['where']['documents.DateCancelled'] = NULL;
                $params['sort_order'] = "ASC";
                $filename = "pastdue_report_customer";
                $data['report_type'] = $report_type;

                $params['sort_by'] =  "collectibles.DateDue";
                $params['group_by'] = "collectibles.DocumentID";

                $params['select'] = 'documents.DocumentID,customers.ADDRESS,customers.PROVINCE,
                    customers.CITY,customers.LASTNAME, customers.FIRSTNAME, customers.MI, lots.Lot_no, collectibles.DateDue as col_DateDue,  collectibles.PRINCIPAL, collectibles.INTEREST';

                $params['select'] = 'documents.DocumentID,customers.ADDRESS,customers.PROVINCE,
                    customers.CITY,customers.LASTNAME, customers.FIRSTNAME, customers.MI, lots.Lot_no, collectibles.DateDue as col_DateDue,  count(collectibles.id) as count_c, sum(collectibles.PRINCIPAL) as sum_p, sum(collectibles.INTEREST) as sum_i, documents.AMORTAMT';

                $table = "collectibles";

                $data['fileTitle'] = "Past Due Accounts";
                if ($report_type == 'b') {
                    $data['fileTitle'] = "Past Due Accounts for Amortization";
                } else if ($report_type == 'c') { // Past Due for Legal Notice
                    $data['fileTitle'] = "Past Due Accounts for Legal Notice";
                }

                $data['results'] = $this->Aris_report_model->get_all_pastdue($params, '', $table);
            } else if ($id == 9) { // ar report by customer
                echo "On-Going";
                die();
            } else if ($id == 10) { // Customer Ledger
                $params['where']['payments.DATE_PAID >='] = date('Y-m-d 00:00:00', strtotime($from));
                $params['where']['payments.DATE_PAID <='] = date('Y-m-d 23:59:59', strtotime($to));
                $params['where']['payments.project_id'] = $project_id;

                if ($customer_id) {
                    $params['where']['customers.id'] = $customer_id;
                    $param_docs['where']['customers.id'] = $customer_id;
                }
                if ($lot_id) {
                    $params['where']['lots.id'] = $lot_id;
                    $param_docs['where']['lots.id'] = $lot_id;
                }

                $params['sort_by'] = "payments.DATE_PAID";
                $params['sort_order'] = "ASC";
                $params['select'] = "customers.LASTNAME, customers.FIRSTNAME, customers.MI, payments.OR_NO, lots.Lot_no, documents.RESAMT_P, payments.PRINCIPAL, payments.INTEREST, payments.PENALTY, payments.PERIOD, payments.AmountPaid, payments.DATE_PAID, payments.DATE_DUE, payments.OR_DATE, payments.REMARKS";
                $data['results'] = $this->Aris_report_model->get_all_collections($params);


                $param_docs['select'] = "customers.customerID, customers.FIRSTNAME, customers.MI, customers.LASTNAME, lots.Lot_no, lots.Lot_no, lots.Area, lots.Price, lots.Price, lots.Price, documents.RA, documents.RADATE, documents.CTS, documents.CTSDATE, documents.MODE, documents.AMORTAMT, documents.DISCOUNT, documents.INTEREST, documents.PERCENTDP, documents.DPDATE_P";
                $param_docs['where']['lots.project_id'] = $project_id;
                $param_docs['where']['documents.project_id'] = $project_id;

                $param_docs['row'] = 1;
                $data['document'] = $this->Aris_report_model->get_all_documents($param_docs);

                $data['fileTitle'] = "Customer Ledger";
                $filename = "customer_ledger";
            }

            if ($print) {
                echo "<pre>";
                print_r($data);
                die;
            }
            if ($doc_type == "csv") {
                $this->customer_ledger_csv($data);
            } else {
                $receiptHTML = $this->load->view('tables/reports/aris/' . $filename, $data, true);
                echo $receiptHTML;
                die();
                if (isset($_GET['download'])) {
                    pdf_create($receiptHTML, '' . $data['fileTitle'] . '.pdf', 4, array('legal', 'landscape'));
                } else {
                    pdf_create($receiptHTML, '' . $data['fileTitle'] . '.pdf', 5, array('legal', 'landscape'));
                }
            }
        }
    }

    function create_buyer($customer = array())
    {
        $info['type_id'] = 1;
        $info['last_name'] = ucwords($customer['LASTNAME']);
        $info['first_name'] = ucwords($customer['FIRSTNAME']);
        $info['civil_status_id'] = !empty($customer['SPOUSENAME']) ? 2 : 1;
        $info['mobile_no'] = $customer['CEL_NO'];
        $info['email'] = !empty($customer['EMAIL_ADDR']) ? $customer['EMAIL_ADDR'] : mb_strtolower($info['first_name'] . '.' . $info['last_name'] . '@shjdc.com.ph');
        $info['email'] = str_replace(' ', '', $info['email']);
        $info['present_address'] = $customer['ADDRESS'];
        $info['aris_id'] = $customer['CUSTOMERID'];
        $info['is_active'] = 1;
        $pwd = password_format($info['last_name'], $info['first_name']);
        $group_id = ['4'];

        $user_info = array(
            'first_name' => $info['first_name'],
            'last_name' => $info['last_name'],
            'active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW
        );

        $user = $this->M_user->where(array('username' => $info['email']))->get();



        if ($user) {

            $this->db->where('user_id', $user['id']);
            $buyer = $this->M_buyer->get();

            if ($buyer) {
                if (str_replace(' ', '', $buyer['first_name']) != str_replace(' ', '', $info['first_name']) || str_replace(' ', '', $buyer['last_name']) != str_replace(' ', '', $info['last_name'])) {
                    $info['email'] = mb_strtolower($info['first_name'] . '.' . $info['last_name'] . '@shjdc.com.ph');
                    $info['user_id'] = $this->ion_auth->register($info['email'], $pwd, $info['email'], $user_info, $group_id);
                    if (!$info['user_id']) {
                        $info['user_id'] = $user['id'];
                    }
                    $buyer['id'] = $this->M_buyer->insert($info + $this->additional);
                }
            } else {
                $info['user_id'] = $user['id'];
                $buyer['id'] = $this->M_buyer->insert($info + $this->additional);
            }
            return $buyer['id'];
        } else {
            $user_id = $this->ion_auth->register($info['email'], $pwd, $info['email'], $user_info, $group_id);
            if ($user_id) {
                $info['user_id'] = $user_id;
                $buyer_id = $this->M_buyer->insert($info + $this->additional);

                return $buyer_id;
            } else {
                return 0;
            }
        }
    }

    function create_seller($sales_agent = array())
    {

        if (!$sales_agent) {

            $default_seller = $this->M_user->where(array('username' => 'default.seller@shjdc.com.ph'))->get();
            if ($default_seller) {
                $this->db->where('user_id', $default_seller['id']);
                $seller = $this->M_seller->get();

                return $seller['id'];
            } else {
                $user_info = array(
                    'first_name' => 'default',
                    'last_name' => 'seller',
                    'active' => 1,
                    'created_by' => $this->user->id,
                    'created_at' => NOW
                );
                $pwd = password_format('seller', 'default');
                $group_id = ['3'];
                $email = 'default.seller@shjdc.com.ph';
                $user_id = $this->ion_auth->register($email, $pwd, $email, $user_info, $group_id);
                $info = array(
                    'user_id' => $user_id,
                    'last_name' => 'seller',
                    'first_name' => 'default',
                    'mobile_no' => '',
                    'email' => $email,
                    'seller_team_id' => 0,
                    'present_address' => 'Pueblo de Panay',
                    'aris_id' => 0,
                    'is_active' => 1,
                );

                $seller_id = $this->M_seller->insert($info + $this->additional);

                return $seller_id;
            }
        }

        $info['last_name'] = ucwords($sales_agent['Lastname']);
        $info['first_name'] = ucwords($sales_agent['Firstname']);
        $info['mobile_no'] = $sales_agent['Mobilephone'] ? $sales_agent['Mobilephone'] : 0;
        $info['email'] = mb_strtolower($sales_agent['Firstname'] . '.' . $sales_agent['Lastname'] . '@shjdc.com.ph');
        $info['email'] = str_replace(' ', '', $info['email']);
        $info['seller_team_id'] = 0;
        $info['present_address'] = $sales_agent['Address'] ? $sales_agent['Address'] : 'Roxas City';
        $info['aris_id'] = $sales_agent['SalesAgentID'];
        $info['is_active'] = 1;

        $pwd = password_format($info['last_name'], $info['first_name']);
        $group_id = ['3'];

        $user_info = array(
            'first_name' => $info['first_name'],
            'last_name' => $info['last_name'],
            'active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW
        );

        // vdebug($info);
        $user = $this->M_user->where(array('username' => $info['email']))->get();

        // Add a check if the user already exists in the system. 
        // If the user already exists, just return the seller id, otherwise create the id
        // vdebug($user);

        // if($sales_agent['Lastna'])



        if ($user) {
            $this->db->where('user_id', $user['id']);
            $seller = $this->M_seller->get();

            if ($seller) {

                if (str_replace(' ', '', $seller['first_name']) != str_replace(' ', '', $info['first_name']) || str_replace(' ', '', $seller['last_name']) != str_replace(' ', '', $info['last_name'])) {
                    $info['email'] = mb_strtolower($info['first_name'] . '.' . $info['last_name'] . '@shjdc.com.ph');
                    $info['user_id'] = $this->ion_auth->register($info['email'], $pwd, $info['email'], $user_info, $group_id);
                    if (!$info['user_id']) {
                        $info['user_id'] = $user['id'];
                    }
                    $seller['id'] = $this->M_seller->insert($info + $this->additional);
                }
            } else {
                $info['user_id'] = $user['id'];
                $seller['id'] = $this->M_seller->insert($info + $this->additional);
            }

            return $seller['id'];
        } else {
            $user_id = $this->ion_auth->register($info['email'], $pwd, $info['email'], $user_info, $group_id);
            if ($user_id) {
                $info['user_id'] = $user_id;
                $seller_id = $this->M_seller->insert($info + $this->additional);

                return $seller_id;
            } else {
                return 0;
            }
        }
    }

    function create_transaction($document = array(), $property = array(), $buyer_id = 0, $seller_id = 0, $financing_id = 0)
    {
        $commission_setup = $this->M_commission->with_commission_setup_values()->where("is_active", 1)->get();

        if (is_object($property)) {
            $property = get_object_vars($property);
        }

        // Fetch Date
        $reservation_date = $document['RADATE'];
        $date = new DateTime($reservation_date);
        $date->modify('+1 month');
        $expiration_date = $date->format('Y-m-d H:i:s');
        $date->modify('-1 day');
        $due_date = $date->format('Y-m-d H:i:s');

        $info['is_recognized'] = 0;
        $info['reference'] = $this->generate_reference($property['id']);
        $info['period_id'] = 1;
        $info['buyer_id'] = $buyer_id;
        $info['seller_id'] = $seller_id;
        $info['project_id'] = $property['project_id'];
        $info['property_id'] = $property['id'];
        $info['financing_scheme_id'] = $financing_id;
        $info['commission_setup_id'] = $commission_setup['id'];
        $info['reservation_date'] = $reservation_date;
        $info['expiration_date'] = $expiration_date;
        $info['due_date'] = $due_date;
        $info['remarks'] = "Created using ARIS sync transaction using document with id " . $document['DocumentID'];
        $info['penalty_type'] = 1;
        $info['aris_id'] = $document['project_id'] . '-' . $document['DocumentID'];
        $info['is_active'] = 1;
        $info['wht_rate'] = 0;
        $info['wht_amount'] = 0;
        $info['is_inclusive'] = 0;
        $info['vat_amount'] = 0;
        $transaction_id = $this->M_transaction->insert($info + $this->additional);

        return $transaction_id;
    }

    function process_property($property = array(), $lot = array(), $transaction_id = 0, $discount_amount)
    {
        if (is_object($property)) {
            $property = get_object_vars($property);
        }
        $total_lot_price = floatval($lot['Area']) * floatval($lot['Price']);
        $info['transaction_id'] = $transaction_id;
        $info['project_id'] = $property['project_id'];
        $info['property_id'] = $property['id'];
        $info['lot_area'] = $lot['Area'];
        $info['floor_area'] = 0;
        $info['lot_price_per_sqm'] = $lot['Price'];
        $info['house_model_id'] = 0;
        $info['package_type_id'] = 0;
        $info['house_model_interior_id'] = 0;
        $info['total_house_price'] = 0;
        $info['total_lot_price'] = $total_lot_price;
        $info['total_selling_price'] = $total_lot_price;
        $info['total_miscellaneous_amount'] = 0;
        $info['total_contract_price'] = $total_lot_price;
        $info['total_discount_price'] = 0;
        $info['collectible_price'] = $total_lot_price - abs($discount_amount);
        $info['commissionable_type_id'] = 0;
        $info['commissionable_amount'] = 0;
        $info['commissionable_amount_remarks'] = 0;
        $info['is_active'] = 1;

        $t_property = $this->M_transaction_property->insert($info + $this->additional);
        return $t_property;
    }

    function get_financing_scheme($document = array(), $mode, $test = 0)
    {

        // Period: 1 = Reservation, 2 = Downpayment, 3 = Loan, 4 

        $reservation_payment = $this->M_aris_payment->where(array('project_id' => $document['project_id'], 'DocumentID' => $document['DocumentID'], 'remarks' => 'Reservation'))->get();

        // Reservation values
        $reservation_terms = '1';
        $reservation_interest = '0';
        $reservation_percentage = '0';
        if ($reservation_payment) {
            $reservation_amount = $reservation_payment['AmountPaid'];
        } else {
            $reservation_amount = $document['RESAMT_P'];
        }
        // $reservation_amount = intval($document['RESAMT_P']);

        // Downpayment values
        $downpayment_terms = '1';
        $downpayment_interest = '0';
        $downpayment_percentage = $document['PERCENTDP'] || $document['PERCENTDP'] == '0' ? $document['PERCENTDP'] : $document['PERCENTDPP'];
        $downpayment_amount = '0';

        if ($downpayment_percentage[0] == '.' || $downpayment_percentage[0] == '0') {
            $downpayment_percentage = floatval($downpayment_percentage) * 100;
        }

        // Loan values
        $loan_terms = $document['MAPERIOD'] ? $document['MAPERIOD'] : $document['MAPERIOD_P'];
        $loan_interest = $document['INTEREST'] || $document['INTEREST'] == '0' ? $document['INTEREST'] : $document['INTEREST_P'];
        $loan_percentage =  100 - $downpayment_percentage;
        $loan_amount = '0';

        if ($loan_interest[0] == '.' || $loan_interest[0] == '0') {
            $loan_interest = floatval($loan_interest) * 100;
        }

        $categories = $this->M_financing_scheme_category->get_all();
        $category = 0;
        $category_id = 0;


        $aris_category = $this->M_financing_scheme_category->where('name', 'Aris')->get();

        if (!$aris_category) {
            $aris_category_info = array(
                'name' => 'Aris',
                'description' => 'Imported Aris Financing Scheme',
            );
            if ($mode == 'sync') {
                $aris_category_created = $this->M_financing_scheme_category->insert($aris_category_info + $this->additional);
                $category_id = $aris_category_created['id'];
            } else {
                $category_id = 0;
            }
        } else {
            $category_id = $aris_category['id'];
        }

        $financing_scheme_name = "ARIS (R-$reservation_amount; DP-$downpayment_percentage%($downpayment_terms); L-$loan_percentage%($loan_terms)) I-$loan_interest%";
        $this->db->where('name', $financing_scheme_name);
        $result = $this->M_financing_scheme->get();


        $aris_financing_scheme_values = array(
            'reservation_terms' => $reservation_terms,
            'reservation_interest' => $reservation_interest,
            'reservation_percentage' => number_format($reservation_percentage, 8, '.', ''),
            'reservation_amount' => $reservation_amount,
            'downpayment_terms' => $downpayment_terms,
            'downpayment_interest' => $downpayment_interest,
            'downpayment_percentage' => number_format($downpayment_percentage, 8, '.', ''),
            'downpayment_amount' => $downpayment_amount,
            'loan_terms' => $loan_terms,
            'loan_interest' => $loan_interest,
            'loan_percentage' => number_format($loan_percentage, 8, '.', ''),
            'loan_amount' => $loan_amount,
        );

        // vdebug($aris_financing_scheme_values);

        if ($result) {
            return $result['id'];
        } else {
            if ($mode == 'sync') {
                return $this->create_financing_scheme($aris_financing_scheme_values, $category_id, $financing_scheme_name);
            } else {
                return 0;
            }
        }
    }

    function create_financing_scheme($all_values, $category_id, $financing_scheme_name)
    {

        // We need to find out if the values that we need already exist in the financing_scheme_values_table

        $financing_scheme = array(
            'category_id' => $category_id,
            'name' => $financing_scheme_name,
            'description' => $financing_scheme_name,
            'scheme_type' => '1',
            'is_active' => '1'
        );

        $finance_scheme_id = $this->M_financing_scheme->insert($financing_scheme, $this->additional);

        $value_categories = array(
            '1' => 'reservation',
            '2' => 'downpayment',
            '3' => 'loan'
        );

        foreach ($value_categories as $key => $value) {

            $values = array(
                'financing_id' => $finance_scheme_id,
                'period_id' => $key,
                'period_term' => $all_values[$value . '_terms'],
                'period_rate_amount' => $all_values[$value . '_amount'] ? $all_values[$value . '_amount'] : 0,
                'period_rate_percentage' => $all_values[$value . '_percentage'],
                'period_interest_percentage' => $all_values[$value . '_interest']
            );
            $this->M_financing_scheme_value->insert($values + $this->additional);
        }

        return $finance_scheme_id;
    }

    function strpos_array($remarks, $arr = array())
    {
        $is_exist = 0;
        foreach ($arr as $value) {
            $result = strpos($remarks, $value);
            if ($result !== false) $is_exist = 1;
        }

        return $is_exist;
    }

    function process_terms($transaction_id = 0, $financing_id = 0, $reservation_date, $t_property = 0, $document, $discount, $special_sync = 0, $special_dates = 0)
    {
        $f_values = $this->M_financing_scheme_value->where('financing_id', $financing_id)->get_all();
        $t_property = $this->M_transaction_property->get($t_property);
        $total_contract_price = $t_property['total_contract_price'];



        // This is for those cases where the aris data have excess paid amount

        $this->db->where('project_id', $document['project_id']);
        $this->db->where('DocumentID', $document['DocumentID']);
        $this->db->where('PERIOD', '0');
        $this->db->where('REMARKS not like "Balloon Payment"');
        $this->db->where('(`REMARKS` LIKE "%DOWN%" OR `REMARKS` LIKE "%RESERVATION%")');
        $this->db->order_by('PaymentID');
        $payments = $this->M_aris_payment->fields('PRINCIPAL')->get_all();

        $this->db->where('project_id', $document['project_id']);
        $this->db->where('DocumentID', $document['DocumentID']);
        $this->db->where('PERIOD', '0');
        $this->db->where('(`REMARKS` LIKE "%DOWN PAYMENT")');
        $this->db->order_by('PaymentID');
        $dp_payment = $this->M_aris_payment->fields('PRINCIPAL')->get_all()[0]['PRINCIPAL'];

        if ($special_dates) {
            $this->db->where('project_id', $document['project_id']);
            $this->db->where('DocumentID', $document['DocumentID']);
            $this->db->where('PERIOD', '0');
            $this->db->where('(`REMARKS` LIKE "Balloon Payment")');
            $this->db->order_by('PaymentID');
            $ma_date = $this->M_aris_payment->fields('DATE_DUE')->get()['DATE_DUE'];
        }

        $initial_sum = 0.00;
        if ($payments) {

            foreach ($payments as $payment) {
                $initial_sum += floatval($payment['PRINCIPAL']);
            }
        }

        // for discount
        $total_contract_price = $total_contract_price - $discount;

        $period_amount = 0;
        $downpayment = 0;

        // vdebug($document);
        $res_days = $document['RESDAYS_P'];
        $count = 0;

        $excess = 0.00;

        // vdebug($document);

        foreach ($f_values as $key => $value) {
            switch ($value['period_id']) {
                case '1':
                    $effectivity_date = $reservation_date;
                    $downpayment = $value['period_rate_amount'];
                    break;
                case '2': // Downpayment Period
                    // Get effectivity date
                    // $effectivity_date = add_months_to_date($effectivity_date, 1);
                    $effectivity_date = date('Y-m-d', strtotime("$effectivity_date + $res_days days"));
                    $period_term = $value['period_term'];
                    break;
                case '3': // Loan Period
                    // Get effectivity date
                    $effectivity_date =  $document['FIRSTMA'] ? $document['FIRSTMA'] : $document['FIRSTMA_P'];

                    if ($special_dates) {
                        $effectivity_date = $ma_date;
                    }
                    break;
            }

            // $bool = $value['period_rate_amount'] == 0;
            // Calculate period amount
            if ($value['period_rate_amount'] == 0) {
                $period_amount = (floatval($total_contract_price) * floatval($value['period_rate_percentage'])) / 100;
            } else {
                $period_amount = $value['period_rate_amount'];
            }

            if ($value['period_id'] == '3') {
                // vdebug($period_amount);
                $period_amount = $period_amount - $excess;
                $excess = 0.00;
            }

            if ($value['period_id'] == '2') {
                if ($value['period_rate_amount'] == 0 && $value['period_rate_percentage'] == 0) {
                    $count++;
                } else {
                    if (floatval($initial_sum) > floatval($period_amount)) {
                        $excess = floatval($initial_sum) - floatval($period_amount);
                    }
                    $period_amount = $period_amount - $downpayment;
                }
            } else {
                if ($count) {
                    $period_amount = $period_amount - $downpayment;
                }
            }

            if ($special_sync) {
                if ($value['period_id'] == '2') {
                    $period_amount = $dp_payment;
                } elseif ($value['period_id'] == '3') {
                    $period_amount = $total_contract_price - $downpayment - $dp_payment;
                }
            }

            $info['transaction_id'] = $transaction_id;
            $info['period_id'] = $value['period_id'];
            $info['period_term'] = $value['period_term'];
            if ($excess) {
                $info['period_amount'] = $initial_sum - $downpayment;
            } else {
                $info['period_amount'] = $period_amount;
            }
            $info['interest_rate'] = $value['period_interest_percentage'];
            $info['effectivity_date'] = $effectivity_date;
            $info['excess'] = $excess;
            if (!$period_amount) {
                $info['period_amount'] = 0;
            }

            $this->M_transaction_billing->insert($info + $this->additional);
        }
    }

    function process_seller($transaction_id = 0, $seller_id = 0)
    {
        $seller = $this->M_seller->get($seller_id);

        if ($seller) {
            $info['transaction_id'] = $transaction_id;
            $info['seller_id'] = $seller_id;
            $info['seller_position_id'] = $seller['seller_position_id'];
            $info['sellers_rate'] = 0;
            $info['sellers_rate_amount'] = 0;
            $info['is_main'] = 0;
            $info['commission_amount_rate'] = 0;
            $info['is_active'] = 1;

            $this->M_transaction_seller->insert($info + $this->additional);
        }
    }

    function process_client($transaction_id = 0, $buyer_id = 0)
    {
        $buyer = $this->M_buyer->get($buyer_id);

        if ($buyer) {
            $info['transaction_id'] = $transaction_id;
            $info['client_id'] = $buyer_id;
            $info['client_type'] = $buyer['type_id'];
            $info['is_active'] = 1;

            $this->M_transaction_client->insert($info + $this->additional);
        }
    }

    public function process_mortgage($transaction_id = 0, $type = "", $process = "", $tb_id = "")
    {
        $this->transaction_library->initiate($transaction_id);

        $monthly_payment = 0;

        $breakdown = $this->transaction_library->process_mortgage($transaction_id, $type, $process, $this->user->id, $tb_id);
    }

    public function get_period_by_remarks($remarks, $period)
    {

        $reservation = strpos(strtolower($remarks), 'reservation');

        if ($reservation !== FALSE && $period == 0) {
            return 1;
        }

        $downpayment = strpos(strtolower($remarks), 'down');

        if ($downpayment !== FALSE  && $period == 0) {
            return 2;
        }

        if ($period == 1) {
            return 3;
        } else {
            return 1;
        }

        return 0;
    }

    public function generate_project_transactions()
    {
        // $centro_villas1_lots = ["1001","2006","2015","6012","7001","7004","7005","7025","7028","7029","8009","8010","1002008","1002009","1003019","1005010","1005016","1006003","1006005","1006006","1006010","1006011","1006013","1006016","1007001","1007004","1007005","1007007","1007009","1007010","1007012","1007013","1007021","1007022","1007023","1007024","1007027","1007028","1008001","1008002","1008003","1008009","1008010","2005013","2006005","2006006","2006013","2007021","2007022","2008013","2008014","2008016","2008017","1008006","2006024"];
        // $centro_villas1_radates = ["18-05-07 000000","21-02-02 000000","20-02-24 000000","18-09-18 000000","18-10-03 000000","20-07-06 000000","20-07-06 000000","19-03-05 000000","19-08-14 000000","19-11-04 000000","20-06-30 000000","20-06-25 000000","21-04-21 000000","20-01-09 000000","19-04-13 000000","19-07-26 000000","19-01-22 000000","19-01-22 000000","16-08-10 000000","16-08-10 000000","18-10-29 000000","18-10-24 000000","19-01-09 000000","14-07-21 000000","18-10-03 000000","21-02-02 000000","21-02-02 000000","18-12-10 000000","19-01-24 000000","18-12-14 000000","19-01-04 000000","19-01-04 000000","19-01-09 000000","19-01-09 000000","18-12-14 000000","18-12-14 000000","19-12-06 000000","19-08-14 000000","19-09-12 000000","14-09-18 000000","19-01-09 000000","20-12-02 000000","21-02-02 000000","20-10-21 000000","19-05-08 000000","16-08-10 000000","19-01-09 000000","19-01-09 000000","19-01-09 000000","20-05-29 000000","19-10-14 000000","14-10-31 000000","14-10-31 000000","18-12-20 000000","21-06-10 000000"];
        // $centro_villas2_lots = ["1010", "3019", "3021", "4019", "4023", "4024", "5005", "5019", "5020", "7007", "7008", "7021", "7022", "7023", "7024", "8020", "8026", "8030", "8031", "10011", "11015", "1001004", "1001009", "1001015", "1004001", "1004002", "1004011", "1004012", "1004025", "1004026", "1004027", "1004028", "1005004", "1005006", "1006007", "1006008", "1006009", "1007002", "1007030", "2001001", "2004016", "2004022", "2007005", "2007013", "2007014", "2007020", "2008006", "2008007", "2008008", "2008009", "2008027", "2009004", "2009008", "2009011", "2009013", "2010002", "2010003", "2010005", "2010006", "2010007", "2010012", "2010018", "2010019", "2011001", "2011009", "2011016", "2011017"];
        // $centro_villas2_radates =["18-04-20 000000","18-08-09 000000","18-08-15 000000","19-06-11 000000","19-11-07 000000","19-11-07 000000","18-08-15 000000","18-10-02 000000","18-10-02 000000","18-07-02 000000","18-07-02 000000","18-10-05 000000","18-10-05 000000","19-08-20 000000","19-06-07 000000","18-08-29 000000","18-08-01 000000","19-01-10 000000","19-01-10 000000","21-02-03 000000","18-08-24 000000","20-07-13 000000","21-06-29 000000","15-03-20 000000","14-11-27 000000","14-11-27 000000","19-09-03 000000","19-09-03 000000","20-12-29 000000","15-05-08 000000","15-05-08 000000","20-12-14 000000","15-04-06 000000","15-05-09 000000","15-01-16 000000","15-01-16 000000","15-02-07 000000","15-04-09 000000","15-05-15 000000","21-01-27 000000","19-02-15 000000","19-08-30 000000","20-01-28 000000","19-11-19 000000","18-10-31 000000","19-09-17 000000","15-12-01 000000","15-12-01 000000","15-11-27 000000","15-09-26 000000","15-11-17 000000","15-08-20 000000","15-08-26 000000","15-05-26 000000","20-02-28 000000","17-06-09 000000","19-01-21 000000","19-02-15 000000","17-04-22 000000","17-04-22 000000","21-06-24 000000","15-05-02 000000","15-05-02 000000","15-05-27 000000","16-01-08 000000","19-01-22 000000","15-08-14 000000"];
        // $costa_verde_lots = ["4020025"];
        // $costa_verde_radates = ['11-12-28 000000'];
        // $cvd_hl_lots = ["11010","11020","14015","16045","17022","19025","20006","20017","22006","22021","24002","25026","27024","28015","30007","30008","1009030","1011011","1014020","1022007","1023019","2010024","2010025","2017010","2026021","3009040","3014006","3025005"];
        // $cvd_hl_radates = ["18-01-16 000000","18-01-18 000000","21-05-26 000000","21-04-08 000000","19-12-03 000000","20-11-07 000000","20-11-24 000000","21-04-19 000000","19-08-30 000000","20-08-03 000000","18-09-11 000000","18-06-21 000000","18-04-26 000000","21-01-08 000000","20-10-07 000000","20-12-23 000000","17-12-01 000000","12-09-12 000000","12-02-15 000000","16-07-13 000000","13-04-10 000000","17-08-18 000000","16-10-14 000000","19-12-02 000000","11-11-22 000000","12-05-29 000000","17-09-12 000000","20-03-02 000000"];
        // $happy_homes_hhl_lots1 = ["7003","13002","14003","14004","14006","14010","14011","14022","16022","16023","16025","17010","17011","17017","23006","23015","24004","24010","1011003","2001005","2004007","2007006","2007009","2008001","2008005","2009001","2009007","2009009","2011002","2013020","2013027","2014001","2014005","2014019","2014020","2014023","2014025","2015002","2015009","2015011","2015015","2015018","2016001","2016003","2016011","2016018","2016019","2017002","2017003","2017009","2017020","2017021","2023001","2023004","2023008","2023009","2023012","2023014","2023017","2023018","2024001","2024002","2024003","2024005","2024006","2024008","2024012","2024014","2024015"];
        // $happy_homes_hhl_radates1 = ["21-01-11 000000","21-01-07 000000","18-05-30 000000","18-08-13 000000","20-08-18 000000","18-08-08 000000","18-06-26 000000","20-05-04 000000","20-03-16 000000","18-06-18 000000","20-03-11 000000","18-04-30 000000","20-04-30 000000","20-09-16 000000","20-05-04 000000","20-07-16 000000","20-09-10 000000","21-03-04 000000","11-08-22 000000","20-01-09 000000","19-06-20 000000","20-09-24 000000","20-05-12 000000","20-04-23 000000","20-08-20 000000","20-11-04 000000","20-10-30 000000","20-11-04 000000","12-09-24 000000","21-01-07 000000","20-12-23 000000","17-09-13 000000","20-02-12 000000","19-08-16 000000","18-04-12 000000","19-03-22 000000","19-09-03 000000","18-01-05 000000","17-10-19 000000","16-11-09 000000","17-12-01 000000","20-06-02 000000","17-06-16 000000","18-04-05 000000","20-01-03 000000","17-12-01 000000","20-12-14 000000","17-09-02 000000","20-08-17 000000","20-12-11 000000","18-09-24 000000","18-12-18 000000","20-01-09 000000","20-01-30 000000","20-10-22 000000","20-06-11 000000","20-06-26 000000","20-06-30 000000","21-01-07 000000","20-08-04 000000","20-08-28 000000","20-11-16 000000","20-09-22 000000","20-10-07 000000","20-09-24 000000","20-09-10 000000","20-09-30 000000","20-09-24 000000","20-10-08 000000"];
        // $happy_homes_2_lots = ["13014","13015","18003","18004","18008","18027","19026","2014015","2015025","3020018"];
        // $happy_homes_2_radates = ["20-12-11 000000","20-12-14 000000","21-05-04 000000","21-05-05 000000","21-05-04 000000","21-05-04 000000","21-05-11 000000","21-03-02 000000","21-02-24 000000","19-11-12 000000"];
        // $happy_homes_hhl_lots2 = ["14027","14029","16008","23002","23003","1014017","2013013","2015014","2015022","2015023","2016024","2023007","2023010","2023019","2024013"];
        // $happy_homes_hhl_radates2 = ["17-08-24 000000","18-08-03 000000","20-07-28 000000","21-06-30 000000","20-06-02 000000","21-03-23 000000","21-01-07 000000","20-11-04 000000","21-03-24 000000","17-11-03 000000","19-12-02 000000","20-11-04 000000","21-05-04 000000","20-07-17 000000","20-06-02 000000"];
        // $milibili_heights_lots = ["1","4","2000001","2000008"];
        // $milibili_heights_radates = ["14-05-08 000000","18-05-11 000000","14-05-08 000000","17-05-25 000000"];
        // $one_waterfront_village_lots = ["1003","1004","1006","1008","2001","2004","2008","2010","2011","2016","3003","3004","3005","3007","3008","4001","4002","4003","4004","4005","4006","5003","5007","5010","5011","6005","6007","6008","7006","7009","8001","8002","8004","8005","8006","8007","8014","8015","8016","8017","8019","9001","9002","9004","9005","9007","9010","9011","9012","9019","9020","10001","10002","10003","10005","10006","10007","10008","10009","10010","10016","10017","11001","11002","11003","11004","11005","11007","11009","11012","12001","12002","12003","12005","13006","13008","13011","14001","14002","14005","14007","14008","14013","14014","14016","14017","14018","14019","14020","14023","14026","15001","15002","15007","15008","15009","15010","15011","15015","15016","15017","15018","16001","16002","16006","16009","16010","16013","16014","16015","16016","16017","16018","16020","16021","16024","2001008","2002003","2002004","2002018","2003006","2004004","2004005","2005006","2007001","2007008","2007011","2009006","2010009","2011006","2015007"];
        // $one_waterfront_village_radates = ["16-12-09 000000","17-07-28 000000","19-10-01 000000","16-12-23 000000","17-10-10 000000","17-06-29 000000","18-04-20 000000","18-04-20 000000","18-02-23 000000","21-01-04 000000","17-01-07 000000","17-07-19 000000","20-03-23 000000","21-07-07 000000","20-11-03 000000","20-01-29 000000","21-06-08 000000","20-09-15 000000","17-09-04 000000","17-10-11 000000","17-10-17 000000","21-07-07 000000","20-07-13 000000","17-08-30 000000","17-08-24 000000","17-11-16 000000","20-09-15 000000","20-11-09 000000","17-07-05 000000","17-08-14 000000","20-10-12 000000","20-10-07 000000","21-07-07 000000","20-08-20 000000","17-10-10 000000","17-08-24 000000","20-06-02 000000","20-07-09 000000","20-08-20 000000","20-08-20 000000","20-08-10 000000","20-08-20 000000","17-08-18 000000","20-06-29 000000","17-09-22 000000","17-09-22 000000","18-01-05 000000","17-07-24 000000","17-06-29 000000","20-04-25 000000","20-09-15 000000","20-10-06 000000","17-07-31 000000","17-07-31 000000","17-07-29 000000","20-11-17 000000","17-07-29 000000","20-09-18 000000","17-07-29 000000","21-03-12 000000","20-09-30 000000","20-08-24 000000","21-07-19 000000","18-04-06 000000","18-04-20 000000","18-03-01 000000","20-06-11 000000","21-05-14 000000","21-03-15 000000","21-07-07 000000","20-09-18 000000","20-11-17 000000","19-11-19 000000","20-10-06 000000","20-10-01 000000","20-09-05 000000","17-01-20 000000","19-11-13 000000","19-11-13 000000","20-02-12 000000","20-09-25 000000","20-09-15 000000","17-07-28 000000","18-05-09 000000","17-08-31 000000","17-07-31 000000","18-04-27 000000","19-11-05 000000","17-08-04 000000","17-10-06 000000","17-12-01 000000","20-10-23 000000","16-12-09 000000","17-09-12 000000","21-07-22 000000","20-08-11 000000","20-08-11 000000","20-09-16 000000","21-01-04 000000","20-09-29 000000","17-10-06 000000","20-10-23 000000","17-09-13 000000","17-09-28 000000","17-09-15 000000","20-08-13 000000","18-02-06 000000","17-10-05 000000","20-11-26 000000","17-07-11 000000","17-07-11 000000","17-07-11 000000","17-07-11 000000","20-01-22 000000","20-01-22 000000","20-09-10 000000","21-01-22 000000","20-09-08 000000","20-09-08 000000","19-12-02 000000","19-10-08 000000","20-05-12 000000","20-08-06 000000","20-07-13 000000","20-02-12 000000","19-11-26 000000","20-02-12 000000","21-07-19 000000","20-02-07 000000","20-06-11 000000","20-03-23 000000"];
        // $orchard_lane_lots = ["3016","6022","6023","7010","9018","12021","1005024","1006024"];
        // $orchard_lane_radates = ["20-07-22 000000","18-12-10 000000","18-12-10 000000","21-01-04 000000","19-02-04 000000","21-03-12 000000","17-11-15 000000","19-01-04 000000"];
        // $pinesville_ss_lots = ['10'];
        // $pinesville_ss_radates = ['13-10-22 000000'];
        // $pdp_lots = ["1000003","1000004","1000013","1000017","1000018","1000022","1000027"];
        // $pdp_radates = ["10-03-09 000000","08-06-13 000000","08-06-14 000000","08-06-20 000000","08-06-28 000000","10-09-30 000000","11-02-21 000000"];
        // $pdp3_lots = ["3000001","3000002","3000005","3000006","3000010","3000012","3000013","3000015","3000016","3000022","3000026","3000029","3000033","3000035","3000040"];
        // $pdp3_radates = ["15-11-03 000000","12-12-11 000000","12-12-26 000000","13-04-10 000000","13-01-30 000000","13-03-27 000000","13-05-20 000000","13-07-03 000000","12-09-19 000000","13-07-30 000000","13-04-26 000000","13-04-25 000000","13-04-27 000000","13-01-21 000000","18-08-22 000000"];
        // $pdp4_lots = ["13","4000004","4000005","4000006","4000008","4000009"];
        // $pdp4_radates = ["21-02-04 000000","18-06-06 000000","18-06-06 000000","18-06-06 000000","21-01-06 000000","19-08-23 000000"];
        // $rio_grande_lots = ["4014","5012","6016","10018","2001002","2010014"];
        // $rio_grande_radates = ["21-01-13 000000","18-09-11 000000","21-03-02 000000","21-01-19 000000","17-03-23 000000","21-04-20 000000"];
        // $st_jude_lots = ["1021","2012","3020","3023","1002010","1002015","1002020","1002023","1003020","1003021","1003022","1002","1005","1007","1011","1013","1016","1018","1019","1020","1022","2013","2014","2017","2018","2019","2020","2023","2025","2026","2027","2028","2029","2030","2031","2032","2034","3011","3012","3013","3014","3015","3017","3018","3022","1001001","1001007","1002001","1002004","1002011","1002024","1002029","1003001","1003002","1003015"];
        // $st_jude_radates = ["21-04-28 000000","21-05-05 000000","16-02-19 000000","16-02-06 000000","21-03-25 000000","21-06-04 000000","21-03-25 000000","21-04-27 000000","21-07-08 000000","21-03-23 000000","21-03-23 000000","16-02-12 000000","16-02-15 000000","20-02-29 000000","20-03-10 000000","20-03-10 000000","20-03-10 000000","16-02-23 000000","16-02-23 000000","16-02-26 000000","16-02-22 000000","16-02-12 000000","16-02-12 000000","19-01-09 000000","20-04-06 000000","20-03-11 000000","16-02-29 000000","16-02-27 000000","20-02-27 000000","20-03-20 000000","16-02-29 000000","16-02-29 000000","16-02-29 000000","16-02-15 000000","16-02-15 000000","20-03-13 000000","18-03-07 000000","16-02-06 000000","16-02-06 000000","16-02-24 000000","16-02-22 000000","16-02-27 000000","16-02-27 000000","16-02-19 000000","16-02-19 000000","20-04-06 000000","20-05-19 000000","20-08-04 000000","20-12-15 000000","21-03-04 000000","20-07-24 000000","21-01-08 000000","20-11-05 000000","20-10-31 000000","21-03-04 000000"];
        // $sitio_no_lots = ["1000117","2000207","2000218","4000408","4000419","4000428","5000522","5000524","3324","2000202","2000204","2000225","2000228","3000320","3000325","3000326","3000328","4000403","4000418","4000425","4000426","4000430"];
        // $sitio_no_radates = ["21-06-21 000000","21-05-17 000000","21-06-01 000000","18-02-28 000000","17-03-20 000000","18-01-25 000000","21-04-12 000000","21-04-12 000000","18-06-09 000000","21-01-04 000000","21-02-26 000000","17-12-09 000000","21-01-04 000000","17-11-15 000000","17-07-14 000000","19-10-09 000000","19-12-11 000000","20-07-13 000000","21-02-19 000000","17-09-22 000000","18-07-16 000000","20-01-10 000000"];
        // $st_joseph_lots = ["2007","3009","4016","5008","5015","7011","9003","9006","9013","9016","1003003","1003005","1003010","1003013","1003017","1003018","1013004","1013006","1014005","2002001","2003016","2005009","2005010","3005003"];
        // $st_joseph_radates = ["20-09-01 000000","17-06-09 000000","17-06-02 000000","20-09-01 000000","21-01-14 000000","20-10-01 000000","21-01-19 000000","21-01-19 000000","21-01-15 000000","20-10-16 000000","21-04-19 000000","16-10-27 000000","16-07-30 000000","21-01-09 000000","17-05-02 000000","17-05-20 000000","16-09-24 000000","16-09-26 000000","16-10-20 000000","21-04-22 000000","21-02-01 000000","20-09-02 000000","20-09-02 000000","20-10-09 000000"];
        // $sta_maria_lots = ["2003","2009","3001","3006","3010","4009","4010","4013","5001","5002","5006","6001","6006","7013","7014","7015","8013","8018","9009","9014","9015","10015","1001002","1001005","1001006","1001011","1002002","1002012","1002013","1003004","1003006","1003008","1003009","1003011","1003012","1004003","1004004","1004005","1004006","1004007","1004009","1005001","1005011","1006004","1007006","1007014","1008005","1008011","1008014","1008015","1008018","1009001","1009014","1009020","1010011"];
        // $sta_maria_radates = ["18-10-29 000000","16-04-01 000000","16-05-17 000000","16-03-16 000000","17-03-23 000000","17-05-11 000000","20-01-29 000000","16-04-14 000000","16-03-21 000000","20-01-24 000000","17-10-17 000000","17-11-17 000000","19-02-20 000000","16-03-21 000000","16-03-21 000000","16-06-01 000000","17-07-07 000000","20-06-13 000000","20-01-24 000000","16-04-16 000000","16-04-08 000000","20-05-08 000000","21-06-21 000000","21-01-05 000000","21-01-05 000000","21-02-08 000000","19-03-21 000000","20-04-24 000000","19-12-18 000000","21-07-12 000000","21-05-19 000000","21-04-28 000000","21-02-23 000000","18-05-30 000000","21-06-28 000000","20-04-13 000000","21-03-29 000000","21-07-02 000000","20-11-16 000000","20-05-14 000000","19-07-01 000000","21-05-05 000000","21-07-08 000000","18-10-29 000000","21-04-19 000000","21-04-12 000000","20-08-04 000000","18-01-11 000000","16-10-17 000000","21-04-12 000000","20-09-14 000000","21-04-14 000000","21-03-18 000000","21-05-03 000000","20-05-13 000000"];
        // $twin_hearts_lots = ["1000104","1000108","1000110","1000114","1000116","1000118","1000119","1000120","1000121","1000129","1000131","1000137","2000002","2000201","2000203","2000205","2000208","2000209","2000211","2000212","2000215","2000216","2000217","2000219","2000223","2000224","2000226","2000227","2000229","2000230","2000231","2000233","2000235","2000236","2000237","2000239","3000135","3000301","3000303","3000304","3000305","3000306","3000308","3000309","3000310","3000311","3000312","3000314","3000315","3000317","3000318","3000319","3000321","3000322","3000323","3000324","3000327","3000329","3000331","3000333","3000337","3000339","4000401","4000404","4000405","4000407","4000409","4000410","4000411","4000412","4000414","4000415","4000417","4000422","4000423","4000427","4000429","4000437","4000439"];
        // $twin_hearts_radates = ["21-01-04 000000","21-03-06 000000","21-03-11 000000","20-08-28 000000","20-02-03 000000","19-12-28 000000","19-12-10 000000","19-12-28 000000","19-12-10 000000","21-03-17 000000","21-03-17 000000","19-12-11 000000","21-02-03 000000","20-03-10 000000","20-02-05 000000","20-03-02 000000","21-01-25 000000","20-02-07 000000","20-02-04 000000","20-02-04 000000","20-05-08 000000","20-01-14 000000","20-04-06 000000","19-12-09 000000","21-04-20 000000","20-06-02 000000","20-06-02 000000","20-01-06 000000","20-01-06 000000","21-01-09 000000","20-02-28 000000","20-03-05 000000","20-02-14 000000","21-02-03 000000","20-02-10 000000","19-12-19 000000","20-11-12 000000","19-12-23 000000","20-04-06 000000","20-05-18 000000","20-05-05 000000","20-08-04 000000","19-12-12 000000","19-12-09 000000","21-01-30 000000","19-12-09 000000","19-12-09 000000","21-01-08 000000","19-12-09 000000","19-12-09 000000","20-09-07 000000","19-12-10 000000","20-02-03 000000","21-05-04 000000","20-04-03 000000","21-04-29 000000","19-12-19 000000","19-12-09 000000","20-02-20 000000","19-12-18 000000","19-12-19 000000","19-12-19 000000","19-12-16 000000","20-02-15 000000","19-12-12 000000","19-12-09 000000","19-12-12 000000","21-02-01 000000","19-12-12 000000","19-12-13 000000","20-06-26 000000","19-12-13 000000","20-02-10 000000","20-12-02 000000","20-02-12 000000","20-03-16 000000","20-03-13 000000","20-02-03 000000","20-02-03 000000"];
        // $verdant_meadows_1a_lots = ["2033","2036","2037","2038","12022","14025","14031","14032","14033","14034","14035","17012","17013","14037"];
        // $verdant_meadows_1a_radates = ["19-10-30 000000","19-09-25 000000","19-10-23 000000","19-10-23 000000","19-09-13 000000","19-11-08 000000","19-09-03 000000","18-07-26 000000","20-07-13 000000","18-07-02 000000","18-07-17 000000","21-01-07 000000","21-01-07 000000","19-08-01 000000"];
        // $villa_dsruiz1_lots = ["5004","9008","1003014","1003016","1009003","1009004","1009007","2002021","2002025","2009005","10014"];
        // $villa_dsruiz1_radates = ["18-10-15 000000","19-09-03 000000","13-06-19 000000","16-10-24 000000","21-01-30 000000","17-08-23 000000","20-09-21 000000","16-08-10 000000","15-08-20 000000","18-02-09 000000","18-03-07 000000"];
        // $villa_dsruiz2_lots = ["14","15","20","22","25","42","1009","1012","1014","1015","2022","3002","4008","5013","5014","6003","7002","7012","7016","7018","7020","8021","8022","8023","10004","10012","1000043","2000012","2000041","2000075","2000076","2001004","2001006","2002005","2002007","2002008","2002009","2002012","2002013","2002016","2005001","2005002","2005005","2005007","2005011","2005012","2006001","2007007","2007017","2007024","2007025","2008003","2008004","2008023","2008025","2010011","2010017"];
        // $villa_dsruiz2_radates = ["14-11-07 000000","15-10-17 000000","14-08-22 000000","16-05-27 000000","16-02-06 000000","14-02-11 000000","18-03-01 000000","20-09-28 000000","20-11-19 000000","19-01-30 000000","19-01-10 000000","21-02-16 000000","18-06-05 000000","19-10-05 000000","19-07-15 000000","18-03-22 000000","19-04-08 000000","18-03-23 000000","19-09-09 000000","19-09-11 000000","20-03-13 000000","20-07-09 000000","20-06-11 000000","20-05-11 000000","20-08-15 000000","19-06-17 000000","15-04-11 000000","16-05-21 000000","18-03-07 000000","17-11-13 000000","17-12-05 000000","17-05-11 000000","18-01-09 000000","19-03-05 000000","20-08-20 000000","18-11-26 000000","19-10-30 000000","19-02-15 000000","19-02-01 000000","19-12-03 000000","18-11-14 000000","20-06-17 000000","20-09-28 000000","19-07-06 000000","18-11-20 000000","18-11-22 000000","19-03-11 000000","20-02-12 000000","19-09-26 000000","20-02-05 000000","19-01-04 000000","19-06-27 000000","17-10-11 000000","20-10-20 000000","20-10-05 000000","20-11-18 000000","19-10-01 000000"];
        // $villa_dsruiz2hl_lots = ["8011","8012","2001003","2001021","2002006","2002011","2005003","2005004","2008015","2010016"];
        // $villa_dsruiz2hl_radates = ["21-07-24 000000","21-07-19 000000","20-09-28 000000","21-06-10 000000","19-11-05 000000","19-02-15 000000","20-09-28 000000","19-01-18 000000","21-07-22 000000","19-06-27 000000"];
        // $villa_dsruiz3_lots = ["2002","2005","5009","5021","5022","5023","5032","6014","6015","6018","6019","6020","6021","6029","9017","12011","12014","13005","13007","13010","15019","15021","22022","1001003","1002003","1002006","1005002","1005003","1005005","1005007","1005008","1005017","1005025","1005028","1005029","1005033","1005034","1006001","1006025","1006026","1006027","1006029","1007026","1008004","1010003","1010004","1011004","1011010","1015023","1019001","1019007","1020003","1020004","1020006","2006002","2006014","2006020","2006021","2007004","2010008","2014016","2014017","2014018","3001015","3002004","3002006","3002007","3002009","3002013","3003002","3003003","3003007","3003008","3003009","3003015","3006009","3006019","3008015","3009012","3009013","3012017","3012020","3012024","3012025","3012026","3013004","3013006","3014002","3014003","3014019","3014020","3019008","3019022","3020002","3020003","3020004","3020005","3020006","3022008","3022023","3022024","3022027","3022029","4013003","4013005","300013006"];
        // $villa_dsruiz3_radates = ["17-12-22 000000","18-01-18 000000","18-05-04 000000","17-10-19 000000","17-10-04 000000","17-09-29 000000","18-01-03 000000","21-02-23 000000","21-02-23 000000","18-03-26 000000","18-03-26 000000","18-03-26 000000","18-03-24 000000","21-07-19 000000","21-07-08 000000","21-08-03 000000","21-08-03 000000","18-03-26 000000","18-03-26 000000","17-03-04 000000","18-02-15 000000","17-08-07 000000","21-03-25 000000","17-01-21 000000","15-06-02 000000","15-08-01 000000","14-10-23 000000","14-10-25 000000","14-11-10 000000","14-10-30 000000","14-10-30 000000","15-10-13 000000","15-09-29 000000","15-07-11 000000","16-06-17 000000","15-05-02 000000","17-02-24 000000","15-01-16 000000","15-02-16 000000","14-10-28 000000","14-12-03 000000","14-11-07 000000","15-04-25 000000","16-07-25 000000","15-01-10 000000","15-01-10 000000","16-02-22 000000","14-10-30 000000","16-11-11 000000","15-01-02 000000","16-11-02 000000","15-01-10 000000","15-01-10 000000","15-02-16 000000","15-12-11 000000","16-04-11 000000","21-02-15 000000","21-02-15 000000","16-02-13 000000","16-06-23 000000","17-02-10 000000","17-02-13 000000","17-02-13 000000","21-05-04 000000","16-01-12 000000","21-07-08 000000","15-12-14 000000","16-06-29 000000","15-07-18 000000","16-07-30 000000","15-08-01 000000","21-03-02 000000","21-03-02 000000","21-03-02 000000","21-05-04 000000","21-03-08 000000","21-02-18 000000","21-03-10 000000","21-03-02 000000","21-02-18 000000","21-03-02 000000","16-01-07 000000","21-04-27 000000","21-05-11 000000","21-07-27 000000","21-02-18 000000","21-03-06 000000","16-07-26 000000","16-07-26 000000","16-07-26 000000","16-07-26 000000","21-03-02 000000","21-05-28 000000","21-03-02 000000","21-03-24 000000","21-03-17 000000","21-03-17 000000","21-06-09 000000","21-03-02 000000","21-03-02 000000","21-04-07 000000","21-03-17 000000","21-03-10 000000","21-02-16 000000","21-02-16 000000","21-08-02 000000"];
        // $villa_dsruiz_lots = ['1009006'];
        // $villa_dsruiz_radates = ['21-04-27 000000'];
        // $vista_del_rio_lots = ["4015","6002","7019","12006","12007","12008","13001","13003","13004","13009","15012","15013","15022","17001","17002","17007","17014","17018","17023","17039","18005","20002","20007","20008","20009","20016","21003","21006","21007","1020005","2020004"];
        // $vista_del_rio_radates = ["19-04-29 000000","19-06-14 000000","19-06-01 000000","18-10-01 000000","18-10-01 000000","18-10-01 000000","20-06-17 000000","19-12-28 000000","20-01-30 000000","19-08-30 000000","18-10-12 000000","20-05-12 000000","21-07-30 000000","18-10-30 000000","18-10-17 000000","20-06-11 000000","19-09-13 000000","19-08-30 000000","20-05-08 000000","19-04-16 000000","19-06-11 000000","21-01-08 000000","20-10-21 000000","20-10-21 000000","19-07-22 000000","19-07-10 000000","20-04-14 000000","20-08-06 000000","20-09-22 000000","21-01-08 000000","19-06-21 000000"];
        // $vista_del_rio_hl_lots = ['17008'];
        // $vista_del_rio_hl_radates = ['19-10-01 000000'];
        $project_id = [
            // '1' => array($centro_villas1_lots, $centro_villas1_radates),
            // '2' => ["179","211","306","316","326","335","344"],
            // '3' => array($centro_villas2_lots, $centro_villas2_radates),
            // '5' => array($costa_verde_lots, $costa_verde_radates),
            // '7' => ["226","235","237","238"],
            // '9' => ["54","87","94","106","107","111","147","160","164","172","177","179","180","191","206","227","229","230","241","244","245","248","250","254","256","258","262","263","268","270","274","278","295","305","307","309","84","100","142","223","225","233","235","272","275","299","300","302","304","308"],
            // '11' => ["6","7","24"],
            // '14' => ["9","12","18","38","40","50","59","70","85","89","92","103","105","106","111","112","114","118","127","141","151","166","169","176","183","185","188","194","195","196","218","223","226","227","244","249","251","252","263","264","265","270","273","324","328","329","330","338","339","343","355","356","358","367","368","369","370","375","376","377","379","383","386","390","391","396","397","402","404","409","410","413","415","417","421","424","429","431","433","436","437","442","447","455","456","457","458","460","461","465","466","467","469","470","475"],
            // '16' => array($orchard_lane_lots, $orchard_lane_radates),
            // '18' => array($pdp_lots, $pdp_radates),
            // '20' => ["29","67"],
            // '21' => array($pdp4_lots, $pdp4_radates),
            // '22' => array($pinesville_ss_lots, $pinesville_ss_radates),
            // '24' => array($rio_grande_lots, $rio_grande_radates),
            // '25' => ["84"],
            // '27' => array($vista_del_rio_hl_lots, $vista_del_rio_hl_radates),
            // '29' => array($villa_dsruiz2_lots, $villa_dsruiz2_radates),
            // '30' => array($villa_dsruiz3_lots, $villa_dsruiz3_radates), //No properties
            // '30' => ["17","19","23","31","32","40","41","66","80","88","89","90","91","104","105","144","183","204","205","215","226","241","243","250","255","256","272","342","356","365","379","393","394","397","398","402"], //No properties
            // '31' => ["139"],
            // '32' => ["21","107","114","123","164","167","169","170","173","175","190","197","206","208","221","244","249","250","252","264","266","273","274","275","279","282","287","289","290","297","301","302","306","311","318","327","330","341","342","348","353","355","357","359","361","362","364","180","198","248","285","339","343","369","372","373","374"], //No properties
            // '33' => array($verdant_meadows_1a_lots, $verdant_meadows_1a_radates), //No properties
            // '36' => array($villa_dsruiz_lots, $villa_dsruiz_radates), 
            // '37' => ["209","223"],
            // '38' => ["132","193","206","215","222"],
            // '39' => array($sta_maria_lots, $sta_maria_radates),
            // '40' => ["27","31","155","167","198"],
            // '41' => array($twin_hearts_lots, $twin_hearts_radates)
            '5' => ["188", "244"]
        ];
        $documents = [];
        $count = 0;
        $results = [];
        foreach ($project_id as $project_key => $project) {
            // $documents = [];
            // foreach($project[0] as $lot_key => $value){
            //     $count_documents = count($documents);
            //     $this->db->where("Lot_no like '%$value'");
            //     $lots = $this->M_aris_lot->where(array('project_id' => $project_key))->get_all();
            //     if($lots){
            //         foreach($lots as $key => $lot){
            //             $selected_lot = $this->M_aris_document->where(array('project_id' => $project_key, 'LOTID' => $lot['LotID'], 'RADATE' => $project[1][$lot_key]))->get();
            //             if($selected_lot){
            //                 $documents[] = $selected_lot;
            //                 break;
            //             }
            //         }
            //     } else{
            //         continue;
            //     }
            // }
            // foreach($documents as $key => $document){
            //     $response = $this->sync_transaction($project_key, $document['DocumentID'],1);

            //     $count++;
            // }
            foreach ($project as $key => $document_id) {
                $response = $this->sync_transaction($project_key, $document_id);
                $results[] = $response;
            }
        }
        echo json_encode($results);
        exit();
    }

    public function resync_transaction($project_id, $document_id, $debug = 0, $special_sync = 0, $special_dates = 0)
    {

        if ($this->input->post() || $debug) {
            $failed_rollback = 1;
            $this->db->trans_begin();
            $transaction = $this->M_transaction->where('aris_id', "$project_id-$document_id")->get();
            $transaction_id = $transaction['id'];

            $this->db->delete('transactions', array('id' => $transaction_id));
            $this->db->delete('transaction_payments', array('transaction_id' => $transaction_id));
            $this->db->delete('transaction_official_payments', array('transaction_id' => $transaction_id));

            // $transaction_delete = $this->M_transaction->where('id', $transaction_id)->delete();
            // $transaction_payments_delete = $this->M_transaction_payment->where('transaction_id', $transaction_id)->delete();
            // $transaction_official_payments_delete = $this->M_Transaction_official_payment->where('transaction_id', $transaction_id)->delete();
            $aris_payments = $this->M_aris_payment->fields('id')->where(array('project_id' => $project_id, 'DocumentID' => $document_id))->get_all();
            if ($aris_payments) {
                foreach ($aris_payments as $key => $aris_payment) {

                    $this->db->where('id', $aris_payment['id']);
                    $this->db->update('aris_payments', array('official_payment_id' => '0'));
                    // $this->M_aris_payment->update(array('official_payment_id' => '0'),$aris_payment['id']);
                }
            }


            $accounting_entries = $this->M_accounting_entries->with_accounting_entry_items()->where(array('project_id' => $transaction['project_id'], 'property_id' => $transaction['property_id']))->get_all();
            if (is_array($accounting_entries)) {
                foreach ($accounting_entries as $key => $accounting_entry) {
                    if (is_array($accounting_entry['accounting_entry_items'])) {
                        $entry_items = $accounting_entry['accounting_entry_items'];
                        foreach ($entry_items as $key => $item) {
                            $this->db->delete('accounting_entry_items', array('id' => $item['id']));
                            // $this->M_accounting_entry_items->where('id', $item['id'])->delete();
                        }
                    }
                    $this->db->delete('accounting_entries', array('id' => $accounting_entry['id']));

                    // $this->M_accounting_entries->where('id', $accounting_entry['id'])->delete();
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
                $failed_rollback = 0;
            }

            if ($failed_rollback) {
                $response['status'] = 0;
                $response['message'] = 'Rollback failed!';
                echo json_encode($response);
                return;
            }

            $response = $this->sync_transaction($project_id, $document_id, 1, $special_sync, $special_dates);

            if($debug){
                return $response;
            } else{
                echo json_encode($response);
                return;
            }
        } else {
            show_403();
        }
    }

    public function generate_reference($property_id = 0, $update_existing = 0)
	{
		$cnt = 1;
		$this->db->where('property_id', $property_id);
		$this->db->order_by('created_at');
		$transactions = $this->M_transaction->get_all();
		$property_name = $this->M_property->fields('name')->get($property_id)['name'];

		if ($update_existing) {
			if ($transactions) {
				foreach ($transactions as $key => $transaction) {
					$reference = $property_name . '/' . sprintf('%03d', $cnt++);
					$new_id = ['reference' => $reference];
					$this->M_transaction->update($new_id + $this->u_additional, $transaction['id']);
				}
			}
		} else {

			if ($transactions) {
				$reference = $property_name . '/' . sprintf('%03d', count($transactions) + 1);
			} else {
				$reference = $property_name . '/' . sprintf('%03d', 1);
			}
			return $reference;
		}
	}
    public function get_old_aris_transactions($project_id)
    {
        $table = [];
        // $this->db->where('DateCancelled is null');
        // $this->db->where('last_date_synced is null');
        $this->db->where('project_id', $project_id);
        // $this->db->order_by('project_id', 'ASC');
        $this->db->order_by('DocumentID', 'ASC');
        $documents = $this->M_aris_document->get_all();

        foreach ($documents as $key => $document) {
            // $this->db->where('DATE_PAID > "2020-12-31 00:00:00"');
            // $payments = $this->M_aris_payment->where(array('project_id' => $document['project_id'], 'DocumentID' => $document['DocumentID']))->get_all();
            // if (!$payments) {
            //     $this->db->where('REMARKS like "%Full Payment%"');
            //     $f_payments = $this->M_aris_payment->where(array('project_id' => $document['project_id'], 'DocumentID' => $document['DocumentID']))->get_all();
            //     if (!$f_payments) {
                    $response = $this->resync_transaction($document['project_id'], $document['DocumentID'], 1);

                    $data = array(
                        'project_id' => $document['project_id'],
                        'DocumentID' => $document['DocumentID'],
                        // 'results' => $response
                    );

                    $table[] = $response;
            //     }
            // }
        }
        echo json_encode($table);
        exit();
    }
    public function update_aris_transaction_status($project_id=0){
        if($project_id){
            $count = 0;
            $this->db->where("project_id = $project_id");
            $this->db->where('DateCancelled is not null');
            $documents = $this->M_aris_document->get_all();
            foreach($documents as $document){
                $transaction = $this->M_transaction->where(['aris_id'=>$project_id . '-' . $document['DocumentID']])->get();
                if($transaction){
                    $this->transaction_library->initiate($transaction['id']);
                    $this->transaction_library->update_transaction_status('1','0','4','Cancelled');
                    $count++;
                }
            }
        }else{
            show_error('Provide project_id.', 403, 'Incomplete Information');
        }
        echo "Successfully updated $count transaction(s)";
        exit();
    }
    public function test_project_names(){
        $result = [];
        $project_list = $this->M_Aris->fields('ProjectName')->get_all();
        foreach($project_list as $project){
            $rems_project = $this->M_project->where(['name' => $project['ProjectName']])->get();
            if($rems_project){
                $message = 'Project match found!';
            }else{
                $message = 'No matching project found!';
            }
            $result[] = ['project' => $project['ProjectName'], 'result' => $message ];
        }
        echo json_encode($result);
        exit();
    }
}
