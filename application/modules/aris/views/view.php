<?php
    $id = isset($data['id']) && $data['id'] ? $data['id'] : 'N/A';
?>


<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Project Details</h3>
		</div>
	</div>
</div>

<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="aris_view" data-ktwizard-state="step-first">
	<div class="kt-portlet">
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-grid__item">

				<!--begin: Form Wizard Nav -->
				<div class="kt-wizard-v3__nav">
					<div class="kt-wizard-v3__nav-items">
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									General Information
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>

						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" id='document_information_click' data-project='<?= $id ?>'>
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Documents
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>

						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Lots
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>

						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									 Sales Agent								
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Customers
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
					</div>
				</div>
				<!--end: Form Wizard Nav -->
			</div>
		</div>
	</div>

	<div class="kt-grid__item kt-grid__item--fluid --kt-wizard-v3__wrapper">

		<!--begin: Form Wizard Step 1-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
			<?php $this->load->view('view/_general_information'); ?>
		</div>
		<!--end: Form Wizard Step 1-->

		<!--begin: Form Wizard Step 2-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" id='document_information'>
            <!-- <?php //$this->load->view('view/_document_information'); ?> -->
		</div>
		<!--end: Form Wizard Step 2-->

		<!-- begin: Form Wizard Step 3-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_lot_information.php'); ?>
		</div>
		<!--end: Form Wizard Step 3-->

		<!--begin: Form Wizard Step 4-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_sales_agent_information.php'); ?>
		</div>
		<!--end: Form Wizard Step 4-->

		<!--begin: Form Wizard Step 5-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_customer_information.php'); ?>
		</div>
		<!--end: Form Wizard Step 5-->
	</div>
</div>