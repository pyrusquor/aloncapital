<?php if( isset($constructions) && !empty($constructions) ) : ?>
	<?php
		if( $sort_order == 'ASC' ) { $sort_order = 'DESC'; } 
		else { $sort_order = 'ASC'; }
	?>
	<table class="table my_prop">
		<thead>
			<tr>
				<td style="width:12%"> <b> Project </b> </td>
				<td style="width:12%"> <b> Property </b> </td>
				<td> <b> Block / Lot/ Cluster </b> </td>
				<td> <b> Date Launched </b> </td>
				<td> <b> Start Date of Construction </b> </td>
				<td> <b> Progress </b> </td>
				<td> <b> Due Date </b> </td>
				<td> <b> Personnel </b> </td>
			</tr>
		</thead>
		<tbody>
		<?php $a = 0; ?>
			<?php foreach ($constructions as $key => $construction) { ?>
				<tr class="<?php if($a % 2 == 1): echo "unread"; endif; ?>">
					<?php 
						$property_id = $construction['property_id']; 
						$project_id = get_value_field($property_id,'cornerstone_properties','project_id'); 
					?>
					<td> 
						<?php echo $construction['project_name']; ?> 
					</td>
					<td> 
						<a target="_BLANK" href="<?php echo base_url(); ?>#properties?property_name=<?php echo $construction['property_name']; ?>">
							<?php echo clean($construction['name']); ?> 
						</a>
						<br> 
						<a target="_BLANK" href="<?php echo base_url(); ?>#clients?type=client&id=<?php echo $construction['client_id']; ?>" 
							style="color:#000">
							<?php echo $construction['client_name']; ?>
						</a>
					</td>
					<td> 
						<?php
							echo 'Blk'.get_value_field ($property_id,'cornerstone_properties','block'); 
							echo "/";
							echo 'Lot'.get_value_field ($property_id,'cornerstone_properties','lot'); 
							echo "/";
							echo 'C'.get_value_field ($property_id,'cornerstone_properties','cluster'); 
						?>
					</td>
					<td><?php echo $launched_date = format_date($construction['is_launched_date']); ?></td>
					<td> 
						<?php $start_date = format_date($construction['start_date_construction']); ?>

						<?php
							if (( $start_date == 'January 01 1970') || ($start_date == 'November 30 -0001')) {
								echo "Date of construction not defined";
							} else {
								echo $start_date;
							}
						?>
					</td>
					<td> <?php echo $construction['progress']; ?>% </td>					
					<td>
						<?php 
							// if (( $start_date == 'January 01 1970') || ($start_date == 'November 30 -0001')) {
							// 	echo "Not yet started";
							// } else if (date_diffe($start_date, $construction['duration']) > 0 ) {
							// 	echo abs(date_diffe($start_date, $construction['duration'])).' day(s) delay';
							// } else if (date_diffe($start_date, $construction['duration']) < 0 ){
							// 	echo abs(date_diffe($start_date, $construction['duration'])).' day(s) before due';
							// } else {
							// 	echo "0 Delay";
							// } 
						?>

						<?php
							$over_due = date_diffe($start_date, $construction['duration']);
							$months = floor($over_due / 30);
	                		$days = $over_due - ($months*30);
                		?>
						<?php 
							if (( $start_date == 'January 01 1970') || ($start_date == 'November 30 -0001')) {
								echo "Not yet started";
							} else if (date_diffe($start_date, $construction['duration']) > 0 ) {
								echo "<span class='red-lbl'>".abs($months)." mo(s) ".abs($days)." day(s) overdue</span>";
							} else if (date_diffe($start_date, $construction['duration']) < 0 ){ 
								echo "<span class='green-lbl'>".abs($months)." mo(s) ".abs($days)." day(s) to overdue</span>";
						 	} else {
								echo "0 Delay";
							} 
						?>
					</td>				
					<td>
						<?php 
							if ($construction['personnel_name']) {
								echo $construction['personnel_name'];
							} else {
								echo "No engr has been assigned to this property.";
							}; 
						?>
					</td>
				</tr>
			<?php $a++; ?>
		<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="8"> 
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12 text-right">
							<?php echo ( isset( $construction_pagination ) ? $construction_pagination : '' ); ?>
						</div>
					</div>				
				</td>			
			</tr>		
		</tfoot>
	</table>	
<?php else: ?>
	<div class="alert alert-warning" role="alert">
		<h6 class="text-center"> There are no completed constructions right now. </h6>
	</div>
<?php endif; ?>