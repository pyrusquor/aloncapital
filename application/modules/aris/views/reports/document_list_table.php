<?php //echo "<pre>";print_r($documents);echo "</pre>"; ?>
<?php if( $documents ): ?>
	<?php
		if( $sort_order == 'ASC' ) { $sort_order = 'DESC'; } 
		else { $sort_order = 'ASC'; }
	?>
	<table id="document_list_table" class="table my_prop table-responsive">
		<thead>
			<tr>
				<td class="col-md-1"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#document_list_table_container').fadeOut(); $('#document_list_table_container').load('<?php echo base_url('reports/documents_form_ajax/0/'.$code.'/cornerstone_transactions_v2.id/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Transaction Reference</b> </span></a> </td>
				<td class="col-md-1"><b> Project </b> </td>
				<td class="col-md-1"><b> Property </b> </td>
				<td class="col-md-1"><b> File Name </b> </td>
				<td class="col-md-1"><b> # of Days </b> </td>
				<td class="col-md-1"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#document_list_table_container').fadeOut(); $('#document_list_table_container').load('<?php echo base_url('reports/documents_form_ajax/0/'.$code.'/commission_period_types.id/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Period Stage</b> </span></a> </td>
				<td class="col-md-1"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#document_list_table_container').fadeOut(); $('#document_list_table_container').load('<?php echo base_url('reports/documents_form_ajax/0/'.$code.'/cornerstone_commission_documents.number_of_days/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Due Date</b> </span></a> </td>
				<td class="col-md-1"><b> Date Created </b> </td>
			</tr>
		</thead>
		<tbody>
			<?php $a = 0; ?>
			<?php foreach( $documents as $document ): 
				if ($document['commission_period_id'] == 1) {
					$effectivity_date = $document['reservation_date'];
				} else if ($document['commission_period_id'] == 2) {
					$effectivity_date = $document['downpayment_effectivity_date'];
				} else if ($document['commission_period_id'] == 3) {
					$effectivity_date = $document['loan_effectivity_date'];
				}
			?>
				<tr class="<?php if($a % 2 == 1): echo "unread"; endif; ?>">		
					<td> <a target="_blank" href="<?php echo base_url( 'dashboard#v2/transactions?id=' . $document['transaction_id']. '&process=view'  ); ?>"> <?php echo get_value_field($document['transaction_id'],'transactions_v2','reference'); ?> </a> </td>
					<td> <?php echo get_value_field($document['project_id'],'projects','name'); ?> </td>
					<td> <a target="_BLANK" href="<?php echo base_url(); ?>#properties?property_name=<?php echo $document['property_name']; ?>"> <?php echo $document['property_name']; ?> </a> 
						<br> 
						<?php echo get_value_field($document['client_id'],'clients','firstname').' '.get_value_field($document['client_id'],'clients','lastname'); ?>
					</td>
					<td> <?php echo $document['document_name']; ?> </td>					
					<td> <?php echo $document['number_of_days']; ?> day(s) starting <?php echo time_format($effectivity_date); ?></td>
					<td> 
						<?php echo get_value_field($document['commission_period_id'], 'cornerstone_document_staging', 'name'); ?>
					</td>					
					<td> 
						<?php
							$over_due = date_diffe($effectivity_date, $document['number_of_days']);
							$months = floor($over_due / 30);
	                		$days = $over_due - ($months*30);
                		?>
						<?php if (date_diffe($effectivity_date, $document['number_of_days']) > 0 ) {
							echo "<span class='red-lbl'>".abs($months)." mo(s) ".abs($days)." day(s) overdue</span>";
						} else if (date_diffe($effectivity_date, $document['number_of_days']) < 0 ){ 
							echo "<span class='green-lbl'>".abs($months)." mo(s) ".abs($days)." day(s) to overdue</span>";
						 } else {
							echo "0 Delay";
						} ?>
					</td>		
					<td> <?php echo format_date($document['transaction_date_created']); ?> </td>					
				</tr>
				<?php $a++; ?>
			<?php endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="8"> 
					<div class="row">
						<div class="col-sm-12 col-md-6 col-lg-6">
							<!-- <button type="submit" class="btn btn-danger btn-sm delete-selected-btn"> <i class="glyphicon glyphicon-trash"></i> Delete Selected </button> -->
						</div>
						<div class="col-sm-12 col-md-6 col-lg-6 text-right">
							<?php echo ( isset( $document_pagination ) ? $document_pagination : '' ); ?>
						</div>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
<?php else: ?>
	<div class="alert alert-warning" role="alert">
		<h6 class="text-center"> There are no documents right now. </h6>
	</div>
<?php endif; ?>