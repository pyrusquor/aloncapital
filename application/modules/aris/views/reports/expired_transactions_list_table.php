<?php if( $transactions ) : ?>
	<?php
		if( $sort_order == 'ASC' ) {
			$sort_order = 'DESC';
		} else {
			$sort_order = 'ASC';
		}
	?>
	<table class="table my_prop">
		<thead>
			<tr>
				<td class="col-md-2"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#expired_transactions_list_table_container').fadeOut(); $('#expired_transactions_list_table_container').load('<?php echo base_url('reports/expired_transactions_form_ajax/'.$code.'/cornerstone_expired_transactions_view.transaction_id/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Reference</b> </span></a> </td>
				<td  <a href="#" class="fld-name"><a href="#" class="fld-name"><span onclick="event.preventDefault(); $('#expired_transactions_list_table_container').fadeOut(); $('#expired_transactions_list_table_container').load('<?php echo base_url('reports/expired_transactions_form_ajax/'.$code.'/cornerstone_expired_transactions_view.project_name/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Project Name</b> </span></a></td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#expired_transactions_list_table_container').fadeOut(); $('#expired_transactions_list_table_container').load('<?php echo base_url('reports/expired_transactions_form_ajax/'.$code.'/cornerstone_expired_transactions_view.property_name/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Property Name</b> </span></a></td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#expired_transactions_list_table_container').fadeOut(); $('#expired_transactions_list_table_container').load('<?php echo base_url('reports/expired_transactions_form_ajax/'.$code.'/cornerstone_expired_transactions_view.client_name/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Client Name</b> </span></a></td>
				<td class="col-md-1"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#expired_transactions_list_table_container').fadeOut(); $('#expired_transactions_list_table_container').load('<?php echo base_url('reports/expired_transactions_form_ajax/'.$code.'/cornerstone_expired_transactions_view.ra_date/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>RA Date</b> </span></a></td>
				<td class="col-md-2"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#expired_transactions_list_table_container').fadeOut(); $('#expired_transactions_list_table_container').load('<?php echo base_url('reports/expired_transactions_form_ajax/'.$code.'/cornerstone_expired_transactions_view.expiration_date/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Expiration Date</b> </span></a></td>

				<td class="col-md-1"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#expired_transactions_list_table_container').fadeOut(); $('#expired_transactions_list_table_container').load('<?php echo base_url('reports/expired_transactions_form_ajax/'.$code.'/cornerstone_expired_transactions_view.num_days/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Days Left</b> </span></a></td>
			
			
				<td class="col-md-2"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#expired_transactions_list_table_container').fadeOut(); $('#expired_transactions_list_table_container').load('<?php echo base_url('reports/expired_transactions_form_ajax/'.$code.'/cornerstone_expired_transactions_view.period_id/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Period Type</b> </span></a></td>
			</tr>
		</thead>
		<tbody>
		<?php $a = 0; ?>
			<?php foreach ($transactions as $key => $payment) { ?>
				<tr class="<?php if($a % 2 == 1): echo "unread"; endif; ?>">
					<td> <a target="_blank" href="<?php echo base_url( 'dashboard#v2/transactions?id=' . $payment['transaction_id'] . '&process=view' ); ?>"> <?php echo get_value_field($payment['transaction_id'],'transactions_v2','reference'); ?> </a> </td>
					<td> <?php echo $payment['project_name']; ?> </td>
					<td> <a target="_BLANK" href="<?php echo base_url(); ?>#properties?property_name=<?php echo $payment['property_name']; ?>"> <?php echo $payment['property_name']; ?> </a> </td>
					<td> <?php echo $payment['client_name']; ?> </td>
					<td> <?php echo format_date($payment['ra_date']); ?> </td>
					<td> <?php echo format_date($payment['expiration_date']); ?> </td>
					<td> <?php echo $payment['num_days']; ?> </td>
					<td> <?php if($payment['period_id'] == '1'){ echo '<span class="period-lbl grn">'.$payment['period_name'].'</span>'; } else if($payment['period_id'] == '2') { echo '<span class="period-lbl ylw">'.$payment['period_name'].'</span>'; } else { echo '<span class="period-lbl pnk">'.$payment['period_name'].'</span>'; } ?> </td>
				</tr>
			<?php $a++; ?>
		<?php } ?>
		</tbody>
	</table>	
	<div class="col-md-12">
		<span class=" pull-right">
			<?php echo ( isset( $transaction_pagination ) ? $transaction_pagination : '' ); ?>
		</span>
	</div>
<?php else: ?>
	<div class="alert alert-warning" role="alert">
		<h6 class="text-center">  There are no for expired transactions right now. </h6>
	</div>
<?php endif; ?>