<div class="tab-content">


   <!-- Year 1 Data -->
    <div id="year_one" class="tab-pane fade in active">
    <h3>Year 1</h3>
    <?php
      $count = 1;
      $forecast = $forecasts['forecasts']['year_'.$count]; 
    ?>

    <!-- Chart Container for Units Sold -->
    <div id="units_sold_chart_container_year_one" class="forecast-chart" style="display: none">
    </div>
    <!-- End Chart Container -->

    <div class="panel panel-default panel-body forecast-table clearfix">
      <table class="table table-hover table-bordered table-responsive" id="units_sold_table_year_one">
        <thead>
          <tr>
            <th class="col-md-2"><strong>Units Sold (<?php echo $forecast['year']; ?>)</strong></th>
            <?php
              for ($i = 1; $i <= 12; $i++) { 

              $month = $i;
              if ($i < 10) {
                $month = "0" . $i;
              }

              $parse = $forecast['year'] . "-" . $month . "-01";

              $date = date('M-y', strtotime($parse));

              ?>

              <th class="text-center"><b><?php echo $date; ?></b></th>
          <?php } ?>
              <th class="text-center"><b>Total Units Sold</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($forecast['projects'] as $project) { ?>
            <tr>
              <th><?php echo $project['project_name']; ?></th>
              <?php foreach ($project['units_sold'] as $key => $value) { ?>

              <td class="text-center <?php echo decide_color($value); ?>"><?php echo $value; ?></td>
              <?php } ?>
              <td class="text-center <?php echo decide_color($project['total_units_sold']); ?>"><?php echo $project['total_units_sold']; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>


    <script>
    Highcharts.chart('units_sold_chart_container_year_one', {
      data: {
          table: 'units_sold_table_year_one',
          startColumn: 0,
          endColumn: 12,
          switchRowsAndColumns: true
      },
      chart: {
          type: 'spline'
      },
      title: {
          text: 'Units Sold (Year 1)'
      },
      yAxis: {
          allowDecimals: false,
          title: {
              text: 'Units Sold'
          }
      },
      tooltip: {
          formatter: function () {
              return '<b>' + this.series.name + '</b><br/>' +
                  this.point.y + ' ' + this.point.name.toLowerCase();
          }
      }
    });
    </script>


    <!--   /* Prices Table Year One */ -->
    <div class="panel panel-default panel-body forecast-table clearfix">
      <table class="table table-hover table-bordered table-responsive" id="prices_prices_prices">
        <thead>
          <tr>
            <th class="col-md-2"><strong>Prices (<?php echo $forecast['year']; ?>)</strong></th>
            <?php
              for ($i = 1; $i <= 12; $i++) { 

              $month = $i;
              if ($i < 10) {
                $month = "0" . $i;
              }

              $parse = $forecast['year'] . "-" . $month . "-01";

              $date = date('M-y', strtotime($parse));

              ?>

              <th class="text-center"><b><?php echo $date; ?></b></th>
          <?php } ?>
              <th class="text-center"><b>AVG Unit Price</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($forecast['projects'] as $project) { ?>
            <tr>
                <th><?php echo $project['project_name']; ?></th>
              <?php foreach ($project['prices'] as $key => $value) { ?>
                <td class="text-center <?php echo decide_color($value); ?>"><?php echo format_currency($value); ?></td>
                <?php } ?>
                <td class="text-center <?php echo decide_color($project['average_unit_price']); ?>"><?php echo format_currency($project['average_unit_price']); ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
  </div>
  <!-- End: Prices Table Year One -->


  <!-- Sales Growth Chart Container -->
  <div id="sales_growth_chart_container_year_one" class="forecast-chart" style="display: none">
  </div>

  <div class="panel panel-default panel-body forecast-table clearfix">
    <table class="table table-hover table-bordered table-responsive">
      <thead>
        <tr>
          <th class="col-md-2"><b>Sales Growth (<?php echo $forecast['year']; ?>)</b></th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>AVG Growth Rate</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['sales_growth_rates'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?>%</td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['average_growth_rate']); ?>"><b><?php echo round($project['average_growth_rate'], 2);?>%</b></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <script>
        Highcharts.chart('sales_growth_chart_container_year_one', {
          data: {
              table: 'sales_growth_table_year_one',
              startColumn: 0,
              endColumn: 12,
              switchRowsAndColumns: true
          },
          chart: {
              type: 'spline'
          },
          title: {
              text: 'Sales Growth'
          },
          yAxis: {
              allowDecimals: false,
              title: {
                  text: 'Sales Growth Percentage'
              }
          },
          tooltip: {
              formatter: function () {
                  return '<b>' + this.series.name + '</b><br/>' +
                      this.point.y + ' ' + this.point.name.toLowerCase();
              }
          }
        });
    </script>
  </div>

    <table class="table table-hover table-bordered table-responsive" id="sales_growth_table_year_one" style="display: none">
      <thead>
        <tr>
          <th class="col-md-2">Sales Growth (<?php echo $forecast['year']; ?>)</th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>AVG Growth Rate</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['sales_growth_rates'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?></td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['average_growth_rate']); ?>"><b><?php echo round($project['average_growth_rate'], 2);?></b></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>


  <div id="revenue_chart_container_year_one" class="forecast-chart" style="display: none">
  </div>

  <div class="panel panel-default panel-body forecast-table clearfix">
    <table class="table table-hover table-bordered table-responsive">
      <thead>
        <tr>
          <th class="col-md-2"><b>Revenue (<?php echo $forecast['year']; ?>)</b></th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>Total Revenue</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['revenues'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo format_currency($value); ?></td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['total_revenue']); ?>"><?php echo format_currency($project['total_revenue']); ?></td>
          </tr>
        <?php } ?>
        <tr>
          <td><b>Total Revenue</b></td>
          <?php for ($i = 0; $i < 12; $i++)
          { ?>
            <td class="text-center"><b><?php echo format_currency($forecast['monthly_total_revenues'][$i]); ?></b></td>
          <?php } ?>
            <td class="text-center"><b><?php echo format_currency($forecast['year_revenue']); ?></b></td>
        </tr>
      </tbody>
    </table>

        <script>
        Highcharts.chart('revenue_chart_container_year_one', {
          data: {
              table: 'revenue_table_year_one',
              startColumn: 0,
              endColumn: 12,
              switchRowsAndColumns: true
          },
          chart: {
              type: 'spline'
          },
          title: {
              text: 'Units Sold (Year 1)'
          },
          yAxis: {
              allowDecimals: false,
              title: {
                  text: 'Revenue (Year 1)'
              }
          },
          tooltip: {
              formatter: function () {
                  return '<b>' + this.series.name + '</b><br/>' +
                      this.point.y + ' ' + this.point.name.toLowerCase();
              }
          }
        });
    </script>
  </div>


    <table class="table table-hover table-bordered table-responsive" id="revenue_table_year_one" style="display: none">
      <thead>
        <tr>
          <th class="col-md-2">Sales Growth (<?php echo $forecast['year']; ?>)</th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['revenues'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?></td>
            <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>

  </div>
   <!-- End of Year 1 Data and Tables -->

  <!-- Year 2 Data -->
    <div id="year_two" class="tab-pane fade">
    <h3>Year 2</h3>
    <?php
      $count = 2;
      $forecast = $forecasts['forecasts']['year_'.$count]; 
    ?>

    <!-- Chart Container for Units Sold -->
    <div id="units_sold_chart_container_year_two" class="forecast-chart" style="display: none">
    </div>
    <!-- End Chart Container -->

    <div class="panel panel-default panel-body forecast-table clearfix">
      <table class="table table-hover table-bordered table-responsive" id="units_sold_table_year_two">
        <thead>
          <tr>
            <th class="col-md-2"><strong>Units Sold (<?php echo $forecast['year']; ?>)</strong></th>
            <?php
              for ($i = 1; $i <= 12; $i++) { 

              $month = $i;
              if ($i < 10) {
                $month = "0" . $i;
              }

              $parse = $forecast['year'] . "-" . $month . "-01";

              $date = date('M-y', strtotime($parse));

              ?>

              <th class="text-center"><b><?php echo $date; ?></b></th>
          <?php } ?>
              <th class="text-center"><b>Total Units Sold</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($forecast['projects'] as $project) { ?>
            <tr>
              <th><?php echo $project['project_name']; ?></th>
              <?php foreach ($project['units_sold'] as $key => $value) { ?>

              <td class="text-center <?php echo decide_color($value); ?>"><?php echo $value; ?></td>
              <?php } ?>
              <td class="text-center <?php echo decide_color($project['total_units_sold']); ?>"><?php echo $project['total_units_sold']; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>


    <script>
    Highcharts.chart('units_sold_chart_container_year_two', {
      data: {
          table: 'units_sold_table_year_two',
          startColumn: 0,
          endColumn: 12,
          switchRowsAndColumns: true
      },
      chart: {
          type: 'spline'
      },
      title: {
          text: 'Units Sold (Year 2)'
      },
      yAxis: {
          allowDecimals: false,
          title: {
              text: 'Units Sold'
          }
      },
      tooltip: {
          formatter: function () {
              return '<b>' + this.series.name + '</b><br/>' +
                  this.point.y + ' ' + this.point.name.toLowerCase();
          }
      }
    });
    </script>


    <!--   /* Prices Table Year One */ -->
    <div class="panel panel-default panel-body forecast-table clearfix">
      <table class="table table-hover table-bordered table-responsive" id="prices_prices_prices">
        <thead>
          <tr>
            <th class="col-md-2"><strong>Prices (<?php echo $forecast['year']; ?>)</strong></th>
            <?php
              for ($i = 1; $i <= 12; $i++) { 

              $month = $i;
              if ($i < 10) {
                $month = "0" . $i;
              }

              $parse = $forecast['year'] . "-" . $month . "-01";

              $date = date('M-y', strtotime($parse));

              ?>

              <th class="text-center"><b><?php echo $date; ?></b></th>
          <?php } ?>
              <th class="text-center"><b>AVG Unit Price</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($forecast['projects'] as $project) { ?>
            <tr>
                <th><?php echo $project['project_name']; ?></th>
              <?php foreach ($project['prices'] as $key => $value) { ?>
                <td class="text-center <?php echo decide_color($value); ?>"><?php echo format_currency($value); ?></td>
                <?php } ?>
                <td class="text-center <?php echo decide_color($project['average_unit_price']); ?>"><?php echo format_currency($project['average_unit_price']); ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
  </div>
  <!-- End: Prices Table Year One -->


  <!-- Sales Growth Chart Container -->
  <div id="sales_growth_chart_container_year_two" class="forecast-chart" style="display: none">
  </div>

  <div class="panel panel-default panel-body forecast-table clearfix">
    <table class="table table-hover table-bordered table-responsive">
      <thead>
        <tr>
          <th class="col-md-2"><b>Sales Growth (<?php echo $forecast['year']; ?>)</b></th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>AVG Growth Rate</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['sales_growth_rates'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?>%</td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['average_growth_rate']); ?>"><b><?php echo round($project['average_growth_rate'], 2);?>%</b></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <script>
        Highcharts.chart('sales_growth_chart_container_year_two', {
          data: {
              table: 'sales_growth_table_year_two',
              startColumn: 0,
              endColumn: 12,
              switchRowsAndColumns: true
          },
          chart: {
              type: 'spline'
          },
          title: {
              text: 'Sales Growth (Year 2)'
          },
          yAxis: {
              allowDecimals: false,
              title: {
                  text: 'Sales Growth Percentage'
              }
          },
          tooltip: {
              formatter: function () {
                  return '<b>' + this.series.name + '</b><br/>' +
                      this.point.y + ' ' + this.point.name.toLowerCase();
              }
          }
        });
    </script>
  </div>

    <table class="table table-hover table-bordered table-responsive" id="sales_growth_table_year_two" style="display: none">
      <thead>
        <tr>
          <th class="col-md-2">Sales Growth (<?php echo $forecast['year']; ?>)</th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>AVG Growth Rate</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['sales_growth_rates'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?></td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['average_growth_rate']); ?>"><b><?php echo round($project['average_growth_rate'], 2);?></b></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>


  <div id="revenue_chart_container_year_two" class="forecast-chart" style="display: none">
  </div>

  <div class="panel panel-default panel-body forecast-table clearfix">
    <table class="table table-hover table-bordered table-responsive">
      <thead>
        <tr>
          <th class="col-md-2"><b>Revenue (<?php echo $forecast['year']; ?>)</b></th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>Total Revenue</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['revenues'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo format_currency($value); ?></td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['total_revenue']); ?>"><?php echo format_currency($project['total_revenue']); ?></td>
          </tr>
        <?php } ?>
        <tr>
          <td><b>Total Revenue</b></td>
          <?php for ($i = 0; $i < 12; $i++)
          { ?>
            <td class="text-center"><b><?php echo format_currency($forecast['monthly_total_revenues'][$i]); ?></b></td>
          <?php } ?>
            <td class="text-center"><b><?php echo format_currency($forecast['year_revenue']); ?></b></td>
        </tr>
      </tbody>
    </table>

        <script>
        Highcharts.chart('revenue_chart_container_year_two', {
          data: {
              table: 'revenue_table_year_two',
              startColumn: 0,
              endColumn: 12,
              switchRowsAndColumns: true
          },
          chart: {
              type: 'spline'
          },
          title: {
              text: 'Revenues (Year 2)'
          },
          yAxis: {
              allowDecimals: false,
              title: {
                  text: 'Revenue (Year 2)'
              }
          },
          tooltip: {
              formatter: function () {
                  return '<b>' + this.series.name + '</b><br/>' +
                      this.point.y + ' ' + this.point.name.toLowerCase();
              }
          }
        });
    </script>
  </div>


    <table class="table table-hover table-bordered table-responsive" id="revenue_table_year_two" style="display: none">
      <thead>
        <tr>
          <th class="col-md-2">Sales Growth (<?php echo $forecast['year']; ?>)</th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['revenues'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?></td>
            <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>

  </div>
   <!-- End of Year 1 Data and Tables -->

      <!-- Year 3 Data -->
    <div id="year_three" class="tab-pane fade">
    <h3>Year 3</h3>
    <?php
      $count = 3;
      $forecast = $forecasts['forecasts']['year_'.$count]; 
    ?>

    <!-- Chart Container for Units Sold -->
    <div id="units_sold_chart_container_year_three" class="forecast-chart" style="display: none">
    </div>
    <!-- End Chart Container -->

    <div class="panel panel-default panel-body forecast-table clearfix">
      <table class="table table-hover table-bordered table-responsive" id="units_sold_table_year_three">
        <thead>
          <tr>
            <th class="col-md-2"><strong>Units Sold (<?php echo $forecast['year']; ?>)</strong></th>
            <?php
              for ($i = 1; $i <= 12; $i++) { 

              $month = $i;
              if ($i < 10) {
                $month = "0" . $i;
              }

              $parse = $forecast['year'] . "-" . $month . "-01";

              $date = date('M-y', strtotime($parse));

              ?>

              <th class="text-center"><b><?php echo $date; ?></b></th>
          <?php } ?>
              <th class="text-center"><b>Total Units Sold</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($forecast['projects'] as $project) { ?>
            <tr>
              <th><?php echo $project['project_name']; ?></th>
              <?php foreach ($project['units_sold'] as $key => $value) { ?>

              <td class="text-center <?php echo decide_color($value); ?>"><?php echo $value; ?></td>
              <?php } ?>
              <td class="text-center <?php echo decide_color($project['total_units_sold']); ?>"><?php echo $project['total_units_sold']; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>


    <script>
    Highcharts.chart('units_sold_chart_container_year_three', {
      data: {
          table: 'units_sold_table_year_three',
          startColumn: 0,
          endColumn: 12,
          switchRowsAndColumns: true
      },
      chart: {
          type: 'spline'
      },
      title: {
          text: 'Units Sold (Year 3)'
      },
      yAxis: {
          allowDecimals: false,
          title: {
              text: 'Units Sold'
          }
      },
      tooltip: {
          formatter: function () {
              return '<b>' + this.series.name + '</b><br/>' +
                  this.point.y + ' ' + this.point.name.toLowerCase();
          }
      }
    });
    </script>


    <!--   /* Prices Table Year One */ -->
    <div class="panel panel-default panel-body forecast-table clearfix">
      <table class="table table-hover table-bordered table-responsive" id="prices_prices_prices">
        <thead>
          <tr>
            <th class="col-md-2"><strong>Prices (<?php echo $forecast['year']; ?>)</strong></th>
            <?php
              for ($i = 1; $i <= 12; $i++) { 

              $month = $i;
              if ($i < 10) {
                $month = "0" . $i;
              }

              $parse = $forecast['year'] . "-" . $month . "-01";

              $date = date('M-y', strtotime($parse));

              ?>

              <th class="text-center"><b><?php echo $date; ?></b></th>
          <?php } ?>
              <th class="text-center"><b>AVG Unit Price</b></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($forecast['projects'] as $project) { ?>
            <tr>
                <th><?php echo $project['project_name']; ?></th>
              <?php foreach ($project['prices'] as $key => $value) { ?>
                <td class="text-center <?php echo decide_color($value); ?>"><?php echo format_currency($value); ?></td>
                <?php } ?>
                <td class="text-center <?php echo decide_color($project['average_unit_price']); ?>"><?php echo format_currency($project['average_unit_price']); ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
  </div>
  <!-- End: Prices Table Year One -->


  <!-- Sales Growth Chart Container -->
  <div id="sales_growth_chart_container_year_three" class="forecast-chart" style="display: none">
  </div>

  <div class="panel panel-default panel-body forecast-table clearfix">
    <table class="table table-hover table-bordered table-responsive">
      <thead>
        <tr>
          <th class="col-md-2"><b>Sales Growth (<?php echo $forecast['year']; ?>)</b></th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>AVG Growth Rate</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['sales_growth_rates'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?>%</td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['average_growth_rate']); ?>"><b><?php echo round($project['average_growth_rate'], 2);?>%</b></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <script>
        Highcharts.chart('sales_growth_chart_container_year_three', {
          data: {
              table: 'sales_growth_table_year_three',
              startColumn: 0,
              endColumn: 12,
              switchRowsAndColumns: true
          },
          chart: {
              type: 'spline'
          },
          title: {
              text: 'Sales Growth (Year 3)'
          },
          yAxis: {
              allowDecimals: false,
              title: {
                  text: 'Sales Growth Percentage'
              }
          },
          tooltip: {
              formatter: function () {
                  return '<b>' + this.series.name + '</b><br/>' +
                      this.point.y + ' ' + this.point.name.toLowerCase();
              }
          }
        });
    </script>
  </div>

    <table class="table table-hover table-bordered table-responsive" id="sales_growth_table_year_three" style="display: none">
      <thead>
        <tr>
          <th class="col-md-2">Sales Growth (<?php echo $forecast['year']; ?>)</th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>AVG Growth Rate</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['sales_growth_rates'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?></td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['average_growth_rate']); ?>"><b><?php echo round($project['average_growth_rate'], 2);?></b></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>


  <div id="revenue_chart_container_year_three" class="forecast-chart" style="display: none">
  </div>

  <div class="panel panel-default panel-body forecast-table clearfix">
    <table class="table table-hover table-bordered table-responsive">
      <thead>
        <tr>
          <th class="col-md-2"><b>Revenue (<?php echo $forecast['year']; ?>)</b></th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
            <th class="text-center"><b>Total Revenue</b></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['revenues'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo format_currency($value); ?></td>
            <?php } ?>
            <td class="text-center  <?php echo decide_color($project['total_revenue']); ?>"><?php echo format_currency($project['total_revenue']); ?></td>
          </tr>
        <?php } ?>
        <tr>
          <td><b>Total Revenue</b></td>
          <?php for ($i = 0; $i < 12; $i++)
          { ?>
            <td class="text-center"><b><?php echo format_currency($forecast['monthly_total_revenues'][$i]); ?></b></td>
          <?php } ?>
            <td class="text-center"><b><?php echo format_currency($forecast['year_revenue']); ?></b></td>
        </tr>
      </tbody>
    </table>

        <script>
        Highcharts.chart('revenue_chart_container_year_three', {
          data: {
              table: 'revenue_table_year_three',
              startColumn: 0,
              endColumn: 12,
              switchRowsAndColumns: true
          },
          chart: {
              type: 'spline'
          },
          title: {
              text: 'Revenues (Year 3)'
          },
          yAxis: {
              allowDecimals: false,
              title: {
                  text: 'Revenues'
              }
          },
          tooltip: {
              formatter: function () {
                  return '<b>' + this.series.name + '</b><br/>' +
                      this.point.y + ' ' + this.point.name.toLowerCase();
              }
          }
        });
    </script>
  </div>


    <table class="table table-hover table-bordered table-responsive" id="revenue_table_year_three" style="display: none">
      <thead>
        <tr>
          <th class="col-md-2">Sales Growth (<?php echo $forecast['year']; ?>)</th>
          <?php
            for ($i = 1; $i <= 12; $i++) { 

            $month = $i;
            if ($i < 10) {
              $month = "0" . $i;
            }

            $parse = $forecast['year'] . "-" . $month . "-01";

            $date = date('M-y', strtotime($parse));

            ?>

            <th class="text-center"><b><?php echo $date; ?></b></th>
        <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($forecast['projects'] as $project) { ?>
          <tr>
            <th><?php echo $project['project_name']; ?></th>
            <?php foreach ($project['revenues'] as $key => $value) { ?>

            <td class="text-center  <?php echo decide_color($value); ?>"><?php echo $value;?></td>
            <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>

  </div>
   <!-- End of Year 1 Data and Tables -->

<div id="comparison_link" class="tab-pane fade">
<h3>Comparison</h3>
<div class="panel panel-default panel-body clearfix forecast-table" id="prices_table">
  <table class="table table-hover table-bordered table-responsive">
    <thead>
      <tr>
        <th class="col-md-2">Units Sold</th>
        <th class="center">% Change (Year 2 vs Year 1)</th>
        <th class="center">% Change (Year 3 vs Year 2)</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($forecasts['comparison']['year_2']['sold_units'] as $key => $project) { ?>
        <tr>
          <th><?php echo $project['name']; ?></th>
          <td class="<?php echo decide_color($project['value']); ?>"><?php echo $project['value']; ?> %</td>
          <td class="<?php echo decide_color($forecasts['comparison']['year_3']['sold_units'][$key]['value']); ?>"><?php echo $forecasts['comparison']['year_3']['sold_units'][$key]['value']; ?> %</td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>

<div class="panel panel-default panel-body clearfix  forecast-table" id="prices_table">
  <table class="table table-hover table-bordered table-responsive">
    <thead>
      <tr>
        <th class="col-md-2">Sales Growth Rate</th>
        <th class="center">Difference (Year 2 vs Year 1)</th>
        <th class="center">Difference (Year 3 vs Year 2)</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($forecasts['comparison']['year_2']['sales_growth'] as $key => $project) { ?>
        <tr>
          <?php $value = $forecasts['comparison']['year_3']['sales_growth'][$key]['value']; ?>
          <th><?php echo $project['name']; ?></th>
          <td class="<?php echo decide_color($project['value']); ?>"><?php echo round($project['value'], 2); ?></td>
          <td class="<?php echo decide_color($value); ?>"> <?php echo round($value, 2); ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>

  <div id="summary_units_sold_chart" class="forecast-chart" style="display: none">
  </div>

  <table class="table table-hover table-bordered table-responsive" id="summary_units_sold_table" style="display: none">
    <thead>
      <tr>
        <th class="col-md-2"><strong></strong></th>

          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_1']['year']; ?></b></th>
          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_2']['year']; ?></b></th>
          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_3']['year']; ?></b></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($forecast['projects'] as $key => $project) { ?>
        <tr>
          <th><?php echo $project['project_name']; ?></th>
          <td><?php echo $forecasts['forecasts']['year_1']['projects'][$key]['total_units_sold']; ?></td>
          <td><?php echo $forecasts['forecasts']['year_2']['projects'][$key]['total_units_sold']; ?></td>
          <td><?php echo $forecasts['forecasts']['year_3']['projects'][$key]['total_units_sold']; ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

      <script>

      Highcharts.chart('summary_units_sold_chart', {
        data: {
            table: 'summary_units_sold_table',
            startColumn: 0,
            switchRowsAndColumns: true
        },
        chart: {
            type: 'spline'
        },
        title: {
            text: '3 Years Units Sold'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Units Sold'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });

    </script>

  <div id="summary_revenues_chart" class="forecast-chart" style="display: none">
  </div>

  <table class="table table-hover table-bordered table-responsive" id="summary_revenues_table" style="display: none">
    <thead>
      <tr>
        <th class="col-md-2"><strong></strong></th>

          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_1']['year']; ?></b></th>
          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_2']['year']; ?></b></th>
          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_3']['year']; ?></b></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($forecast['projects'] as $key => $project) { ?>
        <tr>
          <th><?php echo $project['project_name']; ?></th>
          <td><?php echo $forecasts['forecasts']['year_1']['projects'][$key]['total_revenue']; ?></td>
          <td><?php echo $forecasts['forecasts']['year_2']['projects'][$key]['total_revenue']; ?></td>
          <td><?php echo $forecasts['forecasts']['year_3']['projects'][$key]['total_revenue']; ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

        <script>

      Highcharts.chart('summary_revenues_chart', {
        data: {
            table: 'summary_revenues_table',
            startColumn: 0,
            switchRowsAndColumns: true
        },
        chart: {
            type: 'spline'
        },
        title: {
            text: '3 Years Gross Profit'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Units Sold'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });

    </script>

  <div id="summary_growth_rates_chart" class="forecast-chart" style="display: none">
  </div>


    <table class="table table-hover table-bordered table-responsive" id="summary_growth_rates_table" style="display: none">
    <thead>
      <tr>
        <th class="col-md-2"><strong></strong></th>

          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_1']['year']; ?></b></th>
          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_2']['year']; ?></b></th>
          <th class="text-center"><b><?php echo $forecasts['forecasts']['year_3']['year']; ?></b></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($forecast['projects'] as $key => $project) { ?>
        <tr>
          <th><?php echo $project['project_name']; ?></th>
          <td><?php echo $forecasts['forecasts']['year_1']['projects'][$key]['average_growth_rate']; ?></td>
          <td><?php echo $forecasts['forecasts']['year_2']['projects'][$key]['average_growth_rate']; ?></td>
          <td><?php echo $forecasts['forecasts']['year_3']['projects'][$key]['average_growth_rate']; ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

  <script>

      Highcharts.chart('summary_growth_rates_chart', {
        data: {
            table: 'summary_growth_rates_table',
            startColumn: 0,
            switchRowsAndColumns: true
        },
        chart: {
            type: 'spline'
        },
        title: {
            text: '3 Years Growth Rate'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Units Sold'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });

    </script>



<div class="panel panel-default panel-body clearfix  forecast-table" id="prices_table">
  <table class="table table-hover table-bordered table-responsive">
    <thead>
      <tr>
        <th class="col-md-2">Revenue</th>
        <th class="center">Difference (Year 2 vs Year 1)</th>
        <th class="center">Difference (Year 3 vs Year 2)</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($forecasts['comparison']['year_2']['revenues'] as $key => $project) { ?>
        <tr>
          <?php $value = $forecasts['comparison']['year_3']['revenues'][$key]['value']; ?>
          <th><?php echo $project['name']; ?></th>
          <td class="<?php echo decide_color($project['value']); ?>"><?php echo format_currency($project['value']); ?></td>
          <td class="<?php echo decide_color($value); ?>"><b><?php echo format_currency($value); ?></b></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
</div>
</div>