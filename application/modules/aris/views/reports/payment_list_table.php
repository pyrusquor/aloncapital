<?php if( $payments ) : ?>
	<?php
		if( $sort_order == 'ASC' ) {
			$sort_order = 'DESC';
		} else {
			$sort_order = 'ASC';
		}
	?>
	<table class="table my_prop">
		<thead>
			<tr>
				<td class="col-md-2"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#payment_list_table_container').fadeOut(); $('#payment_list_table_container').load('<?php echo base_url('reports/payments_form_ajax/0/'.$code.'/cornerstone_transaction_payments.transaction_id/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Transaction Reference</b> </span></a> </td>
				<td class="col-md-2"> <b>Project</b> </td>
				<td> <b>Property Name</b>  </td>
				<td> <b>Client Name</b>  </td>
				<td> <b>Payment Amount</b>  </a> </td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#payment_list_table_container').fadeOut(); $('#payment_list_table_container').load('<?php echo base_url('reports/payments_form_ajax/0/'.$code.'/cornerstone_transaction_payments.payment_date/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Payment Date</b> </span></a> </td>
				<td> <b>Period Type</b>  </td>
			</tr>
		</thead>
		<tbody>
		<?php $a = 0; ?>
			<?php foreach ($payments as $key => $payment) { ?>
				<tr class="<?php if($a % 2 == 1): echo "unread"; endif; ?>">
					<td> <a target="_blank" href="<?php echo base_url( 'dashboard#v2/transactions?id=' . $payment['transaction_id'] . '&process=view' ); ?>"> <?php echo get_value_field($payment['transaction_id'],'transactions_v2','reference'); ?> </a> </td>
					<td> <?php echo get_value_field($payment['project_id'],'projects','name'); ?> </td>
					<?php  $property_name = get_value_field($payment['property_id'],'properties','name'); ?>
					<td> <a target="_BLANK" href="<?php echo base_url(); ?>#properties?property_name=<?php echo $property_name; ?>"> <?php echo $property_name; ?> </a> </td>
					<td> <?php echo $payment['client_name']; ?> </td>
					<td> <?php echo format_currency($payment['total_amount']); ?> </td>
					<td> <?php echo format_date($payment['payment_date']); ?> </td>
					<td> <?php if($payment['period_id'] == '1'){ echo '<span class="period-lbl grn">'.$payment['period_type_name'].'</span>'; } else if($payment['period_id'] == '2') { echo '<span class="period-lbl ylw">'.$payment['period_type_name'].'</span>'; } else { echo '<span class="period-lbl pnk">'.$payment['period_type_name'].'</span>'; } ?> </td>
				</tr>
			<?php $a++; ?>
		<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7"> 
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12 text-right">
							<?php echo ( isset( $payment_pagination ) ? $payment_pagination : '' ); ?>
						</div>
					</div>				
				</td>			
			</tr>		
		</tfoot>
	</table>	
<?php else: ?>
	<div class="alert alert-warning" role="alert">
		<h6 class="text-center"> There are no completed payments right now. </h6>
	</div>
<?php endif; ?>