<div class="col-md-3">
<div class="offer offer-danger">
	<div class="shape">
		<div class="shape-text">
									
		</div>
	</div>
	<div class="offer-content">
		<h3>
			Start Year
		</h3>
		<div id="range-div">
            <div class="form-group">
				<label class="control-label col-sm-2" for="email">Year</label>
			    <div class="col-sm-10">
					<select class="form-control sales-forecast-year" style="margin-bottom: 0px">
                    	<?php for($i = date('Y')-2; $i > date('Y', strtotime(date('Y') . " -8 years")); $i--)
                    	{ ?>
                    		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    	<?php } ?>
                    </select>
			    </div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="col-md-6">
	<div class="offer offer-danger">
	<div class="shape">
		<div class="shape-text">
									
		</div>
	</div>
	<div class="offer-content">
		<h3>
			View
		</h3>
		<div id="range-div">

		
			
		
		<div class="row">
		<span class="col-md-6">

			<a class="btn btn-danger btn-block gray-btn" href="#year_one" data-toggle="tab" style="padding: 5px; color: #333;">Year 1 Forecast</a>
			<a class="btn btn-danger btn-block gray-btn" href="#year_two" data-toggle="tab" style="padding: 5px; color: #333;">Year 2 Forecast</a>
		

		</span>

		<span class="col-md-6">

			
			<a class="btn btn-danger btn-block gray-btn" href="#year_three" data-toggle="tab" style="padding: 5px; color: #333;">Year 3 Forecast</a>
			<a class="btn btn-danger btn-block gray-btn" href="#comparison_link" data-toggle="tab" style="padding: 5px; color: #333;">Comparison</a>
			
		</span>

		</div>
		</div>

	</div>
</div>
</div>

<div class="col-md-3">
<div class="offer offer-danger">
	<div class="shape">
		<div class="shape-text">
									
		</div>
	</div>
	<div class="offer-content">
		<h3>
			Render as
		</h3>
		<div id="range-div">
			<div class="radio radio-danger">
	            <input type="radio" name="render-forecast" id="render-table" value="table" checked>
	            <label for="render-table">
	               Table
	            </label>
	        </div>

	        <div class="radio radio-danger">
	            <input type="radio" name="render-forecast" id="render-chart" value="chart">
	            <label for="render-chart">
	                Chart
	            </label>
	        </div>
			</div>
	</div>
</div>
</div>




<div class="col-md-12">
	<div id="sales_forecast_table_container">
		<?php echo $this->load->view('tables/reports/forecast_tables.php'); ?>
	</div>
</div>


