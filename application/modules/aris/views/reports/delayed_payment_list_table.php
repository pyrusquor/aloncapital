<?php if ($delayed_payments) : ?>
	<?php
		if ($sort_order == 'ASC') { $sort_order = 'DESC'; } 
		else { $sort_order = 'ASC'; }
	?>
	<table class="table my_prop">
		<thead>
			<tr>
				<td class="col-md-2"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#delayed_payment_list_table_container').fadeOut(); $('#delayed_payment_list_table_container').load('<?php echo base_url('reports/delayed_payments_form_ajax_v2/'.$code.'/cornerstone_delayed_payments.reference/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Transaction Reference</b> </span></a> </td>
				<td class="col-md-2"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#delayed_payment_list_table_container').fadeOut(); $('#delayed_payment_list_table_container').load('<?php echo base_url('reports/delayed_payments_form_ajax_v2/'.$code.'/cornerstone_delayed_payments.project_name/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Project Name</b> </span></a> </td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#delayed_payment_list_table_container').fadeOut(); $('#delayed_payment_list_table_container').load('<?php echo base_url('reports/delayed_payments_form_ajax_v2/'.$code.'/cornerstone_delayed_payments.property_name/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Property Name</b> </span></a> </td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#delayed_payment_list_table_container').fadeOut(); $('#delayed_payment_list_table_container').load('<?php echo base_url('reports/delayed_payments_form_ajax_v2/'.$code.'/cornerstone_delayed_payments.client_name/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Client Name</b> </span></a> </td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#delayed_payment_list_table_container').fadeOut(); $('#delayed_payment_list_table_container').load('<?php echo base_url('reports/delayed_payments_form_ajax_v2/'.$code.'/cornerstone_delayed_payments.delayed_days/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Days Delayed</b> </span></a> </td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#delayed_payment_list_table_container').fadeOut(); $('#delayed_payment_list_table_container').load('<?php echo base_url('reports/delayed_payments_form_ajax_v2/'.$code.'/cornerstone_delayed_payments.payment_date/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Target Date</b> </span></a></td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#delayed_payment_list_table_container').fadeOut(); $('#delayed_payment_list_table_container').load('<?php echo base_url('reports/delayed_payments_form_ajax_v2/'.$code.'/cornerstone_delayed_payments.period_id/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Period Type</b> </span>	</a> </td>
			</tr>
		</thead>
		<tbody>
		<?php $a = 0; ?>
			<?php foreach ($delayed_payments as $key => $delayed_payment) { ?>
				<?php //if (empty($delayed_payment['overdue'])) { continue; } ?>
				<tr class="<?php if($a % 2 == 1): echo "unread"; endif; ?>">
					<td> <a target="_blank" href="<?php echo base_url( 'dashboard#v2/transactions?id=' . $delayed_payment['transaction_id'] . '&process=view'  ); ?>"> <?php echo $delayed_payment['reference'] ?> </a> </td>
					<td> <?php echo $delayed_payment['project_name']; ?> </td>
					<td> <a target="_BLANK" href="<?php echo base_url(); ?>#properties?property_name=<?php echo $delayed_payment['property_name']; ?>"> <?php echo $delayed_payment['property_name']; ?></a> </td>
					<td> <?php echo $delayed_payment['client_name']; ?> </td>
					<td> 
						<?php echo $delayed_payment['delayed_days']; ?> days
					</td>
					<td> <?php echo format_date($delayed_payment['payment_date']); ?> </td>
					<td> <?php if($delayed_payment['period_id'] == '1'){ echo '<span class="period-lbl grn">'.$delayed_payment['period_name'].'</span>'; } else if($delayed_payment['period_id'] == '2') { echo '<span class="period-lbl ylw">'.$delayed_payment['period_name'].'</span>'; } else { echo '<span class="period-lbl pnk">'.$delayed_payment['period_name'].'</span>'; } ?> </td>
				</tr>
			<?php $a++; ?>
		<?php } ?>
		</tbody>
		
	</table>
				<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12 text-right pull-right">
							<?php echo ( isset( $delayed_payment_pagination ) ? $delayed_payment_pagination : '' ); ?>
						</div>
					</div>	
<?php else: ?>
	<div class="alert alert-warning" role="alert">
		<h6 class="text-center"> There are no delayed payments right now. </h6>
	</div>
<?php endif; ?>