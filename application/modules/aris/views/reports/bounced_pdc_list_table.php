<?php if ($bounced_pdcs) : ?>
	<?php
		if ($sort_order == 'ASC') { $sort_order = 'DESC'; } 
		else { $sort_order = 'ASC'; }
	?>
	<table class="table my_prop">
		<thead>
			<tr>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#bounced_pdcs_list_table_container').fadeOut(); $('#bounced_pdcs_list_table_container').load('<?php echo base_url('reports/bounced_form_ajax/0/cornerstone_post_dated_checks_view.property_code/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Property Code</b> </span></a> </td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#bounced_pdcs_list_table_container').fadeOut(); $('#bounced_pdcs_list_table_container').load('<?php echo base_url('reports/bounced_form_ajax/0/cornerstone_post_dated_checks_view.cheque_no/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Cheque Number</b> </span></a>  </td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#bounced_pdcs_list_table_container').fadeOut(); $('#bounced_pdcs_list_table_container').load('<?php echo base_url('reports/bounced_form_ajax/0/cornerstone_post_dated_checks_view.cheque_amount/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Cheque Amount</b> </span></a> </td>
				<td> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#bounced_pdcs_list_table_container').fadeOut(); $('#bounced_pdcs_list_table_container').load('<?php echo base_url('reports/bounced_form_ajax/0/cornerstone_post_dated_checks_view.reference/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Reference</b> </span></a> </td>
				<td><a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#bounced_pdcs_list_table_container').fadeOut(); $('#bounced_pdcs_list_table_container').load('<?php echo base_url('reports/bounced_form_ajax/0/cornerstone_post_dated_checks_view.due_date/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Due Date</b> </span></a> </td>
				<td><a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#bounced_pdcs_list_table_container').fadeOut(); $('#bounced_pdcs_list_table_container').load('<?php echo base_url('reports/bounced_form_ajax/0/cornerstone_post_dated_checks_view.status_name/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Status</b> </span></a> </td>
			</tr>


		</thead>
		<tbody>
		<?php $a = 0; ?>
			<?php foreach ($bounced_pdcs as $pdc) { ?>
				<?php //if (empty($delayed_payment['overdue'])) { continue; } ?>
				<tr class="<?php if($a % 2 == 1): echo "unread"; endif; ?>">
					<td> <?php echo $pdc['property_code']; ?> </td>
					<td> <?php echo $pdc['cheque_no']; ?> </td>
					<td> <?php echo format_currency( $pdc['cheque_amount'] );?> </td>
					<td> <a target="_blank" href="<?php echo base_url( 'dashboard#v2/transactions?id=' . $pdc['transaction_id'] ); ?>"> <?php echo $pdc['reference']; ?></a> </td>
					<td> <?php echo date('M d, Y', strtotime($pdc['due_date'])); ?> </td>
					<td> <?php echo $pdc['status_name']; ?> </td>
				</tr>
			<?php $a++; ?>
		<?php } ?>
		</tbody>
		
	</table>
	<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 text-right pull-right">
				<?php echo ( isset( $bounced_pdc_pagination ) ? $bounced_pdc_pagination : '' ); ?>
			</div>
		</div>	
<?php else: ?>
	<div class="alert alert-warning" role="alert">
		<h6 class="text-center"> There are no bounced post dated checks right now. </h6>
	</div>
<?php endif; ?>
