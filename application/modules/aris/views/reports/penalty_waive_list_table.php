<?php if( $waive_penalties ) : ?>
	<?php
		if( $sort_order == 'ASC' ) {
			$sort_order = 'DESC';
		} else {
			$sort_order = 'ASC';
		}
	?>
	<table class="table my_prop">
		<thead>
			<tr>
				<td class="col-md-1"> <a href="#" class="fld-name"> <span onclick="event.preventDefault(); $('#penalty_waive_list_table_container').fadeOut(); $('#penalty_waive_list_table_container').load('<?php echo base_url('reports/penalty_waive_form_ajax/'.$code.'/cornerstone_transaction_waiving.date_created/'.$sort_order); ?>').fadeIn();" class="fld-name"> <b>Date</b> </span></a> </td>
				<td class="col-md-1"> <b>Reference</b></td>
				<td class="col-md-1"> <b>Property</b></td>
				<td class="col-md-1"> <b>Client</b></td>
				<td class="col-md-1"> <b>Penalty Amount</b></td>
				<td class="col-md-1"> <b>Created By</b></td>
				<td class="col-md-1"> <b>Actions</b></td>
			</tr>
		</thead>
		<tbody>
		<?php $a = 0; ?>
			<?php foreach ($waive_penalties as $key => $waive) { ?>
				<tr class="<?php if($a % 2 == 1): echo "unread"; endif; ?>">
					<td><?php echo date('F d, Y',strtotime($waive['date_created']));?> </td>
					<td> <a target="_blank" href="<?php echo base_url( 'dashboard#v2/transactions?id=' . $waive['transaction_id'] . '&process=view' ); ?>"> <?php echo $waive['reference'] ?> </a> </td>
					<td> <?php echo $waive['property_name']?> </td>
					<td> <a target="_blank" href="<?php echo base_url( 'dashboard#clients?type=client&id=' . $waive['client_id'] ); ?>"> <?php echo $waive['firstname'].' '.$waive['lastname'] ?> </a> </td>
					<td> <?php echo format_currency($waive['penalty_amount'],true)?> </td>
					<td>
						<?php 
							if ($waive['created_by'] != 0)
								echo get_value_field($waive['created_by'], 'cornerstone_personnels', 'firstname').' '.get_value_field($waive['created_by'], 'cornerstone_personnels', 'lastname'); 
							else
								echo "N/A";
						?>
					</td>
					<td>
						<a class="btn check-btn approve_waiving " title="Approve" data-id="<?php echo $waive['waiving_id']?>" data-transaction-id="<?php echo $waive['transaction_id'] ?>"></a>
						<a class="btn close-btn deny_waiving " title="Deny" data-id="<?php echo $waive['waiving_id']?>" data-transaction-id="<?php echo $waive['transaction_id'] ?>"></a>
						<a class="btn view-btn view_penalties" title="View Penalties" target="_blank" href="<?php echo base_url( 'dashboard#v2/transactions?transaction_penalty_id=' . $waive['transaction_id'] ); ?>"></a>
					</td>
				</tr>
			<?php $a++; ?>
		<?php } ?>
		</tbody>
	</table>	
	<div class="col-md-12">
		<span class=" pull-right">
			<?php echo ( isset( $waive_penalties_pagination ) ? $waive_penalties_pagination : '' ); ?>
		</span>
	</div>
<?php else: ?>
	<div class="alert alert-warning" role="alert">
		<h6 class="text-center">  There are no waiving requests right now. </h6>
	</div>
<?php endif; ?>