<html>
  <head>
    <title><?php echo $fileTitle ?></title>
    <style>
		html,body{
			font-size: 12px;
		}
		@page{
			margin: 20px 20px 20px 20px;
		}
		table{
			
			text-align:left;
			text-transform: capitalize;
		}

		
		table tbody tr td{padding: 5px;}
		table tbody tr td{text-transform: uppercase;}
		table tbody tr td,table tbody tr td:nth-child(2){
			text-align: left !important;
		}
		table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
		table thead tr th{text-align: left;vertical-align: bottom;}
		
		p{
			margin: 0;
		}
		p:last-child{
			margin-bottom: 10px;
		}
		.title{
			text-align: center;
			font-size: 16px
		}
		.divider-table td{
			border: 1px solid #fff;
			text-align: left;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
  </head>
  <body>
	<div class="title">
		<p>Pueblo de Panay Corporation Inc.<br><?php echo $project_name; ?><br><?php echo $fileTitle ?></p>
	</div>
	<table width="100%" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th style="width:20%">Customers' Name</th>
					<th style="width:20%">Lot No</th>
					<th style="width:5%">Date Due</th>
					<th style="width:5%">Principal</th>
					<th style="width:10%">Interest</th>
					<th style="width:10%">Total</th>
					<th style="width:30%">Complete Address</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					if ($results) {
						foreach ($results as $key => $result) { ?><tr>
							<td><?php echo $result['LASTNAME'].", ".$result['MI']." ".$result['FIRSTNAME']; ?></td>
							<td><?php echo format_aris_property($result['Lot_no']); ?></td>
							<td><?php echo db_date_format($result['DateDue'],true); ?></td>
							<td><?php echo format_currency($result['Principal']); ?></td>
							<td><?php echo format_currency($result['Interest']); ?></td>
							<td><?php echo format_currency($result['Principal'] + $result['Interest']); ?></td>
							<td><?php echo $result['ADDRESS']." ".$result['CITY']." ".$result['PROVINCE']." ".$result['COUNTRY']." ".$result['POSTALCODE'] ; ?></td>
						</tr><?php }
					} 
				?>
			</tbody>
	</table>
</body>
</html>