<html>
  <head>
    <title><?php echo $fileTitle ?></title>
    <style>
		/*html,body,table{*/
			/*font-size: 12px;*/
		/*}*/
		html, body { margin:10px 15px 0 10px; font-family:Helvetica, Arial, Calibri; font-size: 8pt}

		@page{
			margin: 20px 20px 20px 20px;
		}
		table{
			
			text-align:left;
			text-transform: capitalize;
		}

		table tbody tr td{padding: 5px;}
		table tbody tr td{text-transform: uppercase;}
		table tbody tr td,table tbody tr td:nth-child(2){
			text-align: left !important;
		}
		table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
		table thead tr th{text-align: left;vertical-align: bottom;}
		
		p{
			margin: 0;
		}
		p:last-child{
			margin-bottom: 10px;
		}
		.title{
			text-align: center;
			font-size: 16px
		}
		.divider-table td{
			border: 1px solid #fff;
			text-align: left;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
  </head>
  <body>

	<div class="title">
		<?php //vdebug($document); ?>
		<p>Pueblo de Panay Inc.<br><?php echo $address; ?><br><?php echo $project_name; ?><br><b><?php echo $fileTitle ?></b><br><br>Summary Information of: <?php echo $document['LASTNAME'].", ".$document['FIRSTNAME']." ".$document['MI']; ?></p>
	</div>
	<?php
		$tcp 			 = $document['Price'] * $document['Area'];
		$total_payment   = sum_array_value($results, 'AmountPaid');
		$total_principal = sum_array_value($results, 'PRINCIPAL');
		$total_penalty 	 = sum_array_value($results, 'PENALTY');
		$total_interest  = sum_array_value($results, 'INTEREST');
		$last_row 		 = end($results);
		$balance 	     = $tcp - $total_principal;
	?>
	<table width="100%" cellspacing="0" cellpadding="0" border="black">
			<tbody>
				<tr>
					<th style="width:10%">Lot No.:</th>
					<td style="width:12%"><?php echo format_aris_property($document['Lot_no']); ?></td>
					<th style="width:10%">Paid Principal:</th>
					<td style="width:12%"><?php echo format_currency($total_principal); ?></td>
					<th style="width:10%">Reservation No.:</th>
					<td style="width:12%"><?php echo $document['RA']; ?></td>
					<th style="width:10%">Contract to Sell No.:</th>
					<td style="width:12%"><?php echo $document['CTS']; ?></td>
				</tr>
				<tr>
					<th style="width:10%">Area (sqm):</th>
					<td style="width:12%"><?php echo $document['Area']; ?></td>
					<th style="width:10%">Paid Interest:</th>
					<td style="width:12%"><?php echo format_currency($total_interest); ?></td>
					<th style="width:10%">Reservation Date:</th>
					<td style="width:12%"><?php echo db_date_format($document['RADATE'],true); ?></td>
					<th style="width:10%">Contract to Sell Date:</th>
					<td style="width:12%"><?php echo db_date_format($document['CTSDATE'],true); ?></td>
				</tr>
				<tr>
					<th style="width:10%">Price/sqm:</th>
					<td style="width:12%"><?php echo format_currency($document['Price']); ?></td>
					<th style="width:10%">Paid Penalty:</th>
					<td style="width:12%"><?php echo format_currency($total_penalty); ?></td>
					<th style="width:10%">Down Payment:</th>
					<td style="width:12%"><?php echo ($document['PERCENTDPP'] * 100); ?> %</td>
					<th style="width:10%">Amortization Amt:</th>
					<td style="width:12%"><?php echo format_currency($document['AMORTAMT']); ?></td>
				</tr>
				<tr>
					<th style="width:10%">Contract Price:</th>
					<td style="width:12%"><?php echo format_currency($tcp); ?></td>
					<th style="width:10%">Total Payment:</th>
					<td style="width:12%"><?php echo format_currency($total_payment); ?></td>
					<th style="width:10%">Down Payment Amt:</th>
					<td style="width:12%"><?php echo format_currency($document['DPDATE_P']); ?></td>
					<th style="width:10%">Payment Mode:</th>
					<td style="width:12%"><?php echo $document['MODE']; ?></td>
				</tr>
				<tr>
					<th style="width:10%">Discount:</th>
					<td style="width:12%"><?php echo format_currency($document['DISCOUNT']); ?></td>
					<th style="width:10%">Last Payment Amt:</th>
					<td style="width:12%"><?php echo format_currency($last_row['AmountPaid']); ?></td>
					<th style="width:10%">DP Due Date:</th>
					<td style="width:12%"></td>
					<th style="width:10%">Interest/Annum</th>
					<td style="width:12%"><?php echo $document['INTEREST'] * 100; ?>%</td>
				</tr>
				<tr>
					<th style="width:10%">Outstanding Bal:</th>
					<td style="width:12%"><?php echo format_currency($balance); ?></td>
					<th style="width:10%">Last Payment Date:</th>
					<td style="width:12%"><?php echo db_date_format($last_row['DATE_PAID'],true); ?></td>
					<th style="width:10%">Down Payment Balance:</th>
					<td style="width:12%"></td>
					<th style="width:10%">Periods Left:</th>
					<td style="width:12%"></td>
				</tr>
				<tr>
					<th style="width:10%" colspan="1">Inhouse Balance:</th>
					<td style="width:12%"colspan="7"></td>
				</tr>
				

			</tbody>
	</table>

	<div class="title">
		<p><br><b><h3>Ammortization Schedule</h3></b></p>
	</div>
		<?php //echo "<pre>";print_r($results);echo "</pre>";?>
		<table width="100%" cellspacing="0" cellpadding="0" border="black">
			<thead>
				<tr>
					<th style="width:5%">Due Date</th>
					<th style="width:5%">Period</th>
					<th style="width:10%">Balance</th>
					<th style="width:10%">Amount Paid</th>
					<th style="width:5%">Receipt Date</th>
					<th style="width:5%">Receipt No.</th>
					<th style="width:15%">Remarks</th>

				</tr>
			</thead>
			<tbody>
				<?php $bal = $tcp; $total_amt_paid = 0;?>
				<?php 
					if ($results) {
						foreach ($results as $key => $result) { ?><tr>
							<td><?php echo db_date_format($result['DATE_DUE'],true); ?></td>
							<td><?php echo $result['PERIOD']; ?></td>
							<td><?php echo format_currency($bal); ?></td>							
							<td><?php echo format_currency($result['AmountPaid']); ?></td>
							<td><?php echo db_date_format($result['OR_DATE'],true); ?></td>
							<td><?php echo substr($result['OR_NO'], 2)  ; ?></td>
							<td><?php echo $result['REMARKS']; ?></td>
						</tr><?php $bal = $bal - $result['AmountPaid']; $total_amt_paid += $result['AmountPaid']; } 
					} 
				?>
				<tr>
					<td><?php echo db_date_format($result['DATE_DUE'],true); ?></td>
					<td>&nbsp;</td>
					<td><?php echo format_currency($tcp - $total_amt_paid); ?></td>
					<td><?php echo format_currency($total_amt_paid); ?></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		
	<br />
	<br />

	<table class="table">
		<tbody>
			<tr>
				<td ><strong>Date Printed:</strong></td>
				<td ><?=date('F j, Y'); ?></td>

				<?php if ( isset( $result['signatory'] ) && ( $result['signatory'] ) ): ?>
					<?php foreach ($result['signatory'] as $key => $signatory) { ?>
						<td><strong><?=ucwords(str_replace('_id', '', $key));?> By: </strong></td>
						<td> <?=get_person_name($signatory,'staff');?> </td>
					<?php } ?>
				<?php endif ?>

			</tr>
		</tbody>
	</table>

</body>
</html>