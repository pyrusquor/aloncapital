<html>
  <head>
    <title><?php echo $fileTitle ?></title>
    <style>
		html,body{
			font-size: 12px;
		}
		@page{
			margin: 20px 20px 20px 20px;
		}
		table{
			
			text-align:left;
			text-transform: capitalize;
		}

		
		table tbody tr td{padding: 5px;}
		table tbody tr td{text-transform: uppercase;}
		table tbody tr td,table tbody tr td:nth-child(2){
			text-align: left !important;
		}
		table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
		table thead tr th{text-align: left;vertical-align: bottom;}
		
		p{
			margin: 0;
		}
		p:last-child{
			margin-bottom: 10px;
		}
		.title{
			text-align: center;
			font-size: 16px
		}
		.divider-table td{
			border: 1px solid #fff;
			text-align: left;
			text-transform: capitalize;
			font-weight: 600;
		}
	</style>
  </head>
  <body>
	<div class="title">
		<p>Pueblo de Panay Corporation Inc.<br><?php echo $project_name; ?><br><?php echo $fileTitle ?></p>
	</div>
	<table width="100%" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th style="width:3%">No.</th>
					<th style="width:17%">Customers' Name</th>
					<th style="width:10%">Street/Brgy.</th>
					<th style="width:10%">City</th>
					<th style="width:5%">Postal</th>
					<th style="width:5%">Province</th>
					<th style="width:10%">Country</th>
					<th style="width:10%">Cel No.</th>
					<th style="width:10%">Phone No.</th>
					<th style="width:10%">Fax No.</th>
					<th style="width:5%">Email Address</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					if ($results) {
						foreach ($results as $key => $result) { ?><tr>
							<td><?php echo $key + 1; ?></td>
							<td><?php echo $result['LASTNAME'].", ".$result['MI']." ".$result['FIRSTNAME']; ?></td>
							<td><?php echo $result['ADDRESS']; ?></td>
							<td><?php echo $result['CITY']; ?></td>
							<td><?php echo $result['POSTALCODE']; ?></td>
							<td><?php echo $result['PROVINCE']; ?></td>
							<td><?php echo $result['COUNTRY']; ?></td>
							<td><?php echo $result['CEL_NO']; ?></td>
							<td><?php echo $result['PHONE_NO']; ?></td>
							<td><?php echo $result['FAX_NO']; ?></td>
							<td><?php echo $result['EMAIL_ADDR']; ?></td>
						</tr><?php }
					} 
				?>
			</tbody>
	</table>
</body>
</html>