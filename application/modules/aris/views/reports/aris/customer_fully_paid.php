<html>
  <head>
    <title><?php echo $fileTitle ?></title>
    <style>
		html,body{
			font-size: 12px;
		}
		@page{
			margin: 20px 20px 20px 20px;
		}
		table{
			
			text-align:left;
			text-transform: capitalize;
		}

		
		table tbody tr td{padding: 5px;}
		table tbody tr td{text-transform: uppercase;}
		table tbody tr td,table tbody tr td:nth-child(2){
			text-align: left !important;
		}
		table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
		table thead tr th{text-align: left;vertical-align: bottom;}
		
		p{
			margin: 0;
		}
		p:last-child{
			margin-bottom: 10px;
		}
		.title{
			text-align: center;
			font-size: 16px
		}
		.divider-table td{
			border: 1px solid #fff;
			text-align: left;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
  </head>
  <body>
	<div class="title">
		<p>Pueblo de Panay Corporation Inc.<br><?php echo $project_name; ?><br><?php echo $fileTitle ?></p>
	</div>
	<table width="100%" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th style="width:30%">Customer's Name</th>
					<th style="width:10%">Lot No.</th>
					<th style="width:10%">Date Fully Paid</th>
					<th style="width:10%">Last RNo.</th>
					<th style="width:10%">RA #</th>
					<th style="width:5%">CTS #</th>
					<th style="width:5%">DAS #</th>
					<th style="width:10%">Area (sqm)</th>
					<th style="width:10%">TCP</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					if ($results) {
						foreach ($results as $key => $result) { ?>
							
							<?php if ($result): ?>
								<tr>
									<td colspan="9">
										<center><u><b><?php echo date('F Y',strtotime($key)); ?></b></u></center>
									</td>
								<tr>
								<?php foreach ($result as $key => $res) { ?>
									<tr>
										<td><?php echo $res['LASTNAME'].", ".$res['MI']." ".$res['FIRSTNAME']; ?></td>
										<td><?php echo format_aris_property($res['Lot_no']); ?></td>
										<td><?php echo db_date_format($res['OR_DATE'],true); ?></td>
										<td><?php echo $res['OR_NO']; ?></td>
										<td><?php echo $res['RA']; ?></td>
										<td><?php echo $res['CTS']; ?></td>
										<td><?php echo $res['DAS']; ?></td>
										<td><?php echo $res['Area']; ?></td>
										<td><?php echo format_currency($res['Price'] *  $res['Area']); ?></td>
									</tr>
								<?php } ?>
							<?php endif ?>
					<?php }
					} 
				?>
			</tbody>
	</table>
</body>
</html>