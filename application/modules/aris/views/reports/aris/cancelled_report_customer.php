<html>
  <head>
    <title><?php echo $fileTitle ?></title>
    <style>
		html,body{
			font-size: 12px;
		}
		@page{
			margin: 20px 20px 20px 20px;
		}
		table{
			
			text-align:left;
			text-transform: capitalize;
		}

		
		table tbody tr td{padding: 5px;}
		table tbody tr td{text-transform: uppercase;}
		table tbody tr td,table tbody tr td:nth-child(2){
			text-align: left !important;
		}
		table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
		table thead tr th{text-align: left;vertical-align: bottom;}
		
		p{
			margin: 0;
		}
		p:last-child{
			margin-bottom: 10px;
		}
		.title{
			text-align: center;
			font-size: 16px
		}
		.divider-table td{
			border: 1px solid #fff;
			text-align: left;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
  </head>
  <body>
	<div class="title">
		<p>Pueblo de Panay Corporation Inc.<br><?php echo $project_name; ?><br><?php echo $fileTitle ?></p>
	</div>
	<table width="100%" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th style="width:15%">Customer's Name</th>
					<th style="width:5%">Lot No.</th>
					<th style="width:15%">Date Cancelled</th>
					<th style="width:5%">Area (sqm)</th>
					<th style="width:5%">RA #</th>
					<th style="width:5%">CTS #</th>
					<th style="width:5%">Principal</th>
					<th style="width:10%">Interest</th>
					<th style="width:10%">Penalty</th>
					<th style="width:10%">Total</th>
					<th style="width:15%">OB</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$totalprincipal=0;
				$totalinterest=0;
				$totalpenalty=0;
				$grandtotal=0;
				$TotalOB=0;

					if ($results) {
						foreach ($results as $key => $result) { 

							$initsumprincipal=0;
							$initsuminterest=0;
							$initsumpenalty=0;
							$initsumtotal=0;
							$initsumob=0;


							?>
								
							<?php if ($result): 
									foreach ($result as $tempkey=> $tempres) {

										$initsumprincipal =  $tempres['PRINCIPAL'] + $initsumprincipal;
										$initsuminterest =  $tempres['INTEREST'] + $initsuminterest;
										$initsumpenalty =  $tempres['PENALTY'] + $initsumpenalty;
										$initsumtotal =  $tempres['AmountPaid'] + $initsumtotal;
										$initsumob =  ($tempres['Price'] * $tempres['Area']) + $initsumob;
										
									}
									 //echo print_r(format_currency($subsumprincipal)); die();
								?>
								<tr>
									<td>
										<center><u><b><?php echo date('F Y',strtotime($key)); ?></b></u></center>
									</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><i><?php echo format_currency($initsumprincipal); ?></i></td>
									<td><i><?php echo format_currency($initsuminterest); ?></i></td>
									<td><i><?php echo format_currency($initsumpenalty); ?></i></td>
									<td><i><?php echo format_currency($initsumtotal); ?></i></td>
									<td><i><?php echo format_currency($initsumob); ?></i></td>
								<tr>
								<?php foreach ($result as $key => $res) { 


									//$Total =  $res['PRINCIPAL'] + $res['INTEREST'] + $res['PENALTY'] ;
									$totalprincipal =  $res['PRINCIPAL'] + $totalprincipal;
									$totalinterest =  $res['INTEREST'] + $totalinterest;
									$totalpenalty =  $res['PENALTY'] + $totalpenalty;
									$grandtotal =  $res['AmountPaid'] + $grandtotal;
									$TotalOB =  (($res['Price'] * $res['Area']) + $TotalOB);
									?>
									<tr>
										<td><?php echo $res['LASTNAME'].", ".$res['MI']." ".$res['FIRSTNAME']; ?></td>
										<td><?php echo format_aris_property($res['Lot_no']); ?></td>
										<td><?php echo db_date_format($res['DateCancelled'],true); ?></td>
										<td><?php echo $res['Area']; ?></td>
										<td><?php echo $res['RA']; ?></td>
										<td><?php echo $res['CTS']; ?></td>
										<td><?php echo number_format(($res['PRINCIPAL']),2); ?></td>
										<td><?php echo number_format(($res['INTEREST']),2); ?></td>
										<td><?php echo number_format(($res['PENALTY']),2); ?></td>
										<td><?php echo number_format(($res['AmountPaid']),2); ?></td>
										<td><?php echo number_format(($res['Price'] *  $res['Area']),2); ?></td>
									</tr>
								<?php } ?>
							<?php endif ?>
					<?php }
					} 
				?>
			</tbody>
			
	</table>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr >
					<?php echo "<br />";?>
				</tr>
				<tr>
					<th style="width:15%"></th>
					<th style="width:5%"></th>
					<th style="width:15%"></th>
					<th style="width:5%"></th>
					<th style="width:5%"></th>
					<th style="width:5%"></th>
					<th style="width:5%">Principal</th>
					<th style="width:10%">Interest</th>
					<th style="width:10%">Penalty</th>
					<th style="width:10%">Total</th>
					<th style="width:15%">OB</th>
				</tr>
				<tr >
					<th style="width:20%"><?php echo "<B>"."Grand Total ";?></th>
					<th style="width:0%"></th>
					<th style="width:15%"></th>
					<th style="width:5%"></th>
					<th style="width:5%"></th>
					<th style="width:5%"></th>
					<th style="width:5%"><?php echo "<B>".format_currency($totalprincipal);?></th>
					<th style="width:10%"><?php echo "<B>".format_currency($totalinterest);?></th>
					<th style="width:10%"><?php echo "<B>".format_currency($totalpenalty);?></th>
					<th style="width:10%"><?php echo "<B>".format_currency($grandtotal);?></th>
					<th style="width:15%"><?php echo "<B>".format_currency($TotalOB);?></th>

				</tr>
				<tr  style="height:120px; overflow:hidden;">
					<?php echo "<br />";?>
				</tr>
				<tr>
					<?php echo "<br />";?>
					
					<td colspan="5">
						<?php echo "Prepared by: __________________________";?>
					</td>
					<td>
					</td>
					
					
					<td colspan="5">
						<?php echo "Checked by: __________________________";?>
					</td>
					
				</tr>
			</table>
</body>
</html>