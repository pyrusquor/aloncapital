<?php 
    $project_id = isset($data['project_id']) && $data['project_id'] ? $data['project_id'] : '';
    $lot_id = isset($data['LotID']) && $data['LotID'] ? $data['LotID'] : '';
    $lot_no = isset($data['Lot_no']) && $data['Lot_no'] ? $data['Lot_no'] : '';
    $area = isset($data['Area']) && $data['Area'] ? $data['Area'] : '';
    $price = isset($data['Price']) && $data['Price'] ? $data['Price'] : '';
    $remarks = isset($data['Remarks']) && $data['Remarks'] ? $data['Remarks'] : '';
    $open = isset($data['Open']) && $data['Open'] ? $data['Open'] : '';
    
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
  <div class="kt-container kt-container--fluid">
    <div class="kt-subheader__main">
      <h3 class="kt-subheader__title">Lot Detail</h3>
    </div>
    <div class="kt-subheader__toolbar">
      <div class="kt-subheader__wrapper">
        <a href="<?php echo site_url('aris/view/' . $project_id);?>" class="btn btn-label-instagram btn-sm btn-elevate">
          <i class="fa fa-reply"></i> Back
        </a>
      </div>
    </div>
  </div>
</div>

<!-- begin:: Content -->
<div
  class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
  <div class="row">
    <div class="col-md-6">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                            General Information
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                    <!--begin::Form-->
                        <div class="kt-widget13">
                            <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Lot Number
                            </span>
                            <span
                                class="kt-widget13__text kt-widget13__text--bold"
                            ><?=$lot_no?></span>
                            </div>
                        <div class="kt-widget13">
                            <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Area
                            </span>
                            <span
                                class="kt-widget13__text kt-widget13__text--bold"
                            ><?=$area?></span>
                            </div>
                        </div>
                        <!-- ==================== begin: Add fields details  ==================== -->
                        <div class="kt-widget13">
                            <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Price
                            </span>
                            <span
                                class="kt-widget13__text kt-widget13__text--bold"
                            ><?=money_php($price)?></span>
                            </div>
                        </div>
                        <div class="kt-widget13">
                            <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Remarks
                            </span>
                            <span
                                class="kt-widget13__text kt-widget13__text--bold"
                            ><?=$remarks?></span>
                            </div>
                        </div>

                        <!-- ==================== end: Add model details ==================== -->
                    <!--end::Form-->
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>