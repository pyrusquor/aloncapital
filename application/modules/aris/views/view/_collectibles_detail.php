<?php 
    $collectibles = isset($data) && $data ? $data : 0;
    $project_id = isset($project_id) && $project_id ? $project_id : 0;
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
  <div class="kt-container kt-container--fluid">
    <div class="kt-subheader__main">
      <h3 class="kt-subheader__title">Collectble Detail</h3>
    </div>
    <div class="kt-subheader__toolbar">
      <div class="kt-subheader__wrapper">
        <a href="<?php echo site_url('aris/view/' . $project_id);?>" class="btn btn-label-instagram btn-sm btn-elevate">
          <i class="fa fa-reply"></i> Back
        </a>
      </div>
    </div>
  </div>
</div>

<!-- begin:: Content -->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group form-group-xs row">
                    <table class="table table-striped- table-bordered table-hover" id="collectible_table">
                        <thead>
                            <tr>
                                <th>Document ID</th>
                                <th>Date Due</th>
                                <th>Principal</th>
                                <th>Interest</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($collectibles): ?>
                                <?php foreach($collectibles as $key => $value): ?>
                                    <tr>
                                        <td><?=$value['DocumentID']; ?></td>
                                        <td><?=$value['DateDue']; ?></td>
                                        <td><?=$value['Principal']; ?></td>
                                        <td><?=money_php($value['Interest']); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                    <tr>
                                        <td colspan="4" class="text-center">No record found</td>
                                    </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
	.table#collectible_table td {
		padding: 0.75rem 0.25rem;
		vertical-align: top;
		border-top: 1px solid #ebedf2;
		font-size: 12px;
	}
</style>