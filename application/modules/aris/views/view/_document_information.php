<?php 
	// $documents = isset($data['documents']) && $data['documents'] ? $data['documents'] : 0;
	// vdebug($documents);
?>
<div class="row" id='kt_content'>
	<div class="col-sm-12">
		<!-- General Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_document_information">
				<div class="card">
					<div class="card-header" id="head_document_information">
						<div class="card-title" data-toggle="collapse" data-target="#document_information"
							aria-expanded="true" aria-controls="document_information">
							Documents <svg xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
								viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path
										d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
										id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path
										d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
										id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3"
										transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>

					</div>


					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="document_information" class="collapse show" aria-labelledby="head_document_information"
						data-parent="#accord_document_information">
						<div class="card-body">
							<div class="row">
								<div class="col-md-4 transaction-btn-color">
									<h5> Legend: </h5>
									<div class='btn btn-success'>Synced</div>
								</div>
							</div>
							<div class="form-group form-group-xs row m-3">
								<table class="table table-striped table-bordered table-hover" id="document_table">
									<thead>
										<tr>
											<th>ID</th>
											<th>Lot</th>
											<!-- <th>Customer</th> -->
											<!-- <th>Mode</th> -->
											<!-- <th>RA</th> -->
											<th>RA Date</th>
											<!-- <th>DP Percentage</th> -->
											<th title="Reservation Amount">RE Amount</th>
											<th title="Costumer Name"> Buyer Name</th>
											<!-- <th title="Reservation Terms">RE Terms</th> -->
											<!-- <th>Amortization</th> -->
											<!-- <th title="Monthly Amortization Period">MAP</th> -->
											<th title="First Monthly Amortization Date">FMA Date</th>
											<!-- <th>Discount</th> -->
											<!-- <th title="Walk-in">WI</th> -->
											<!-- <th>Date Created</th> -->
											<th>Actions</th>
											<th>Transaction</th>
											<th>Cancelled</th>
										</tr>
									</thead>
									<tbody>
                                        <?php if($documents): ?>
                                            <?php foreach($documents as $key => $value): ?>
												<?php
													$status = "";
													// $is_sync = isset($value['last_date_synced']) && $value['last_date_synced'] ? 1 : 0;
													$params['project_id'] = $value['project_id'];
													$params['LOTID'] = $value['LOTID'];

													$customer_first_name = get_value_field_multiple(array('project_id' => $value['project_id'], 'CUSTOMERID' => $value['CUSTOMERID']), 'aris_customers', 'FIRSTNAME');
													$customer_last_name = get_value_field_multiple(array('project_id' => $value['project_id'], 'CUSTOMERID' => $value['CUSTOMERID']), 'aris_customers', 'LASTNAME');
													$customer_full_name = $customer_first_name . " " . $customer_last_name;
													$lot_no = get_value_field_multiple($params, 'aris_lots', 'Lot_no');
													if($value['rems_transaction_id']) {
														$status = "table-success";
													}

												?>
                                                <tr class="<?php echo $status ?>">
                                                    <td><?=$value['DocumentID']; ?></td>
                                                    <td><?=$lot_no; ?></td>
                                                    <!-- <td><?//=$value['MODE']; ?></td> -->
                                                    <!-- <td><?//=$value['RA']; ?></td> -->
                                                    <td><?=date('m/d/Y', strtotime($value['RADATE'])); ?></td>
													<!-- <td><?//=$value['PERCENTDPP']; ?></td> -->
                                                    <td><?=money_php($value['RESAMT_P']); ?></td>
													<td><?= $customer_full_name ?></td>
                                                    <!-- <td><?//=$value['RESDAYS_P']; ?></td> -->
                                                    <!-- <td><?//=money_php($value['AMORTAMT']); ?></td> -->
                                                    <!-- <td><?//=$value['MAPERIOD_P']; ?></td> -->
                                                    <td><?=view_date($value['FIRSTMA']); ?></td>
                                                    <!-- <td><?//=money_php($value['DISCOUNT']); ?></td> -->
													<!-- <td>
														<?php// if($value['WALK-IN']): ?>
															Yes
														<?php// else: ?>
															No
														<?php// endif; ?>
													</td> -->
                                                    <!-- <td><?//=view_date($value['DateCancelled']); ?></td> -->
                                                    <td>
														<span class="dropdown">
															<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
																<i class="la la-ellipsis-h"></i>
															</a>
															<div class="dropdown-menu dropdown-menu-right">
																<!-- <?php if($is_sync): ?>
																	<a href="<?php echo site_url('transaction/view/' . $transaction['id']);?>" class="dropdown-item remove_aris" target="_blank">
																		<i class="la la-eye"></i> View Transaction
																	</a>
																<?php endif; ?> -->
																<a href="<?php echo site_url('aris/ar/' . $value['project_id'] . '/' . $value['DocumentID']);?>" class="dropdown-item remove_aris">
																	<i class="la la-eye"></i> View AR 
																</a>

																<a href="<?php echo site_url('aris/collectibles/' . $value['project_id'] . '/' . $value['DocumentID']);?>" class="dropdown-item remove_aris">
																	<i class="la la-eye"></i> View Collectibles 
																</a>

																<a href="<?php echo site_url('aris/pastdue/' . $value['project_id'] . '/' . $value['DocumentID']);?>" class="dropdown-item remove_aris">
																	<i class="la la-eye"></i> View Past Due 
																</a>

																<a href="<?php echo site_url('aris/lot/' . $value['project_id'] . '/' . $value['LOTID']);?>" class="dropdown-item remove_aris">
																	<i class="la la-eye"></i> View Lot 
																</a>

																<a href="<?php echo site_url('aris/ledger/' . $value['DocumentID'] . '/' . $value['project_id'] . '/' . $value['LOTID']. '/' . $value['CUSTOMERID']);?>" class="dropdown-item remove_aris">
																	<i class="la la-eye"></i> Check Customer Ledger 
																</a>

																<a href="<?php echo site_url('aris/ledger/' . $value['DocumentID'] . '/' . $value['project_id'] . '/' . $value['LOTID']. '/' . $value['CUSTOMERID'].'/1');?>" class="dropdown-item remove_aris">
																	<i class="la la-eye"></i> Check Customer Ledger Internal 
																</a>


																<a href="javascript: void(0)" class="dropdown-item remove_aris sync_transaction" data-project="<?=$value['project_id']?>" data-document="<?=$value['DocumentID']?>">
																	<i class="la la-refresh"></i> Sync Transaction 
																</a>
																<?php if($value['rems_transaction_id']): ?>
																	<a href="javascript: void(0)" class="dropdown-item remove_aris sync_collections" data-project="<?=$value['project_id']?>" data-document="<?=$value['DocumentID']?>">
																		<i class="la la-refresh"></i> Sync Collections 
																	</a>
																<?php endif; ?>
																<?php if($value['rems_transaction_id']): ?>
																	<a href="javascript: void(0)" class="dropdown-item remove_aris resync_collections" data-project="<?=$value['project_id']?>" data-document="<?=$value['DocumentID']?>">
																		<i class="la la-trash"></i> Resync Transaction 
																	</a>
																<?php endif; ?>
																
															</div>
														</span>
														<a href="<?php echo site_url('aris/document/' . $value['project_id'] . '/' . $value['DocumentID']);?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
															<i class="la la-eye"></i>
														</a>
													</td>
													<td>
													<?php if($value['rems_transaction_id']): ?>
													<a href="<?php echo site_url('transaction/view/' . $value['rems_transaction_id'] );?>"class='btn btn-success'>
														<i class="fa fa-paperclip"></i> View Transaction
													</a>
													<?php else: ?>
														<a href="javascript:void(0);"class='btn btn-danger'>
														<i class="fa fa-paperclip"></i> Transaction not Found!
													</a>
													<?php endif ?>
													</td>
													<td>
													<?php if($value['DateCancelled']): ?>
													<a href="<?php echo site_url('transaction/view/' . $value['rems_transaction_id'] );?>"class='btn btn-danger'>
														Yes
													</a>
													<?php else: ?>
														<a href="javascript:void(0);"class='btn btn-success'>
														No
													</a>
													<?php endif ?>
													</td>
                                                </tr>
                                            <?php endforeach; ?>
										<?php else: ?>
											<tr>
												<td colspan="12" class="text-center">No record found</td>
											</tr>
                                        <?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<style>
	.table#document_table td {
		padding: 0.75rem 0.25rem;
		vertical-align: top;
		border-top: 1px solid #ebedf2;
		font-size: 12px;
	}
</style>