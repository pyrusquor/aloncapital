<?php
    $id = isset($data['id']) && $data['id'] ? $data['id'] : 'N/A';
    $setup_id = isset($data['SetupID']) && $data['SetupID'] ? $data['SetupID'] : 'N/A';
    $project_name = isset($data['ProjectName']) && $data['ProjectName'] ? $data['ProjectName'] : 'N/A';
    $address = isset($data['Address']) && $data['Address'] ? $data['Address'] : 'N/A';
    $city = isset($data['City']) && $data['City'] ? $data['City'] : 'N/A';
    $postal_code = isset($data['PostalCode']) && $data['PostalCode'] ? $data['PostalCode'] : 'N/A';
    $country = isset($data['country']) && $data['country'] ? $data['country'] : 'N/A';
    $phone_number = isset($data['PhoneNumber']) && $data['PhoneNumber'] ? $data['PhoneNumber'] : 'N/A';
    $fax_number = isset($data['FaxNumber']) && $data['FaxNumber'] ? $data['FaxNumber'] : 'N/A';
    $remarks = isset($data['Remarks']) && $data['Remarks'] ? $data['Remarks'] : 'N/A';
    $date_created = isset($data['date_created']) && $data['date_created'] ? $data['date_created'] : '--';
    $date_updated = isset($data['date_updated']) && $data['date_updated'] ? $data['date_updated'] : '--';
    $pds = isset($data['pds']) && $data['pds'] ? $data['pds'] : 0;
    $collectibles = isset($data['collectibles']) && $data['collectibles'] ? $data['collectibles'] : '0';
    $lots = isset($data['aris_lots']) && $data['aris_lots'] ? $data['aris_lots'] : '0';
     
?>

<div class="row">
	<div class="col-sm-6">
		<!-- General Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
				<div class="card">
					<div class="card-header" id="head_general_information">
						<div class="card-title" data-toggle="collapse" data-target="#general_information" aria-expanded="true" aria-controls="general_information">
							General Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="general_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
						<div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Reference #</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $id.". ". $setup_id; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Date Created</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $date_created; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Project Name</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $project_name; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Address</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $address; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">City</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $city; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Province</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $province; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Postal Code</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $postal_code; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Phone Number</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $phone_number; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Fax Number</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $fax_number; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>

                                <div class="col-sm-6">
									<span href="#" class="kt-notification-v2__item">
										<div class="kt-notification-v2__itek-wrapper">
											<div class="kt-notification-v2__item-title">
												<h7 class="kt-portlet__head-title kt-font-primary">Remarks</h7>
											</div>
											<div class="kt-notification-v2__item-desc">
												<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo $remarks; ?>
												</h6>
											</div>
										</div>
									</span>
								</div>
                            </div>
						</div>
					</div>
                    
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-sm-6">
    <div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_pds_information">
				<div class="card">
					<div class="card-header" id="head_pds_information">
						<div class="card-title" data-toggle="collapse" data-target="#pds_information" aria-expanded="true" aria-controls="pds_information">
							PD Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="pds_information" class="collapse show" aria-labelledby="head_pds_information" data-parent="#accord_pds_information">
						<div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
									<table class="table table-striped- table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Due Date</th>
                                                <th>Principal</th>
                                                <th>Interest</th>
                                                <th>Penalty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($pds): ?>
                                                <?php foreach($pds as $key => $value): ?>
                                                    <tr>
                                                        <td><?=$value['DueDate'];?></td>
                                                        <td><?=money_php($value['Principal']);?></td>
                                                        <td><?=money_php($value['Interest']);?></td>
                                                        <td><?=money_php($value['Penalty']);?></td>
                                                    </tr>
                                                <?php endforeach; ?>
											<?php else: ?>
												<tr>
													<td colspan="4" class="text-center">No record found</td>
												</tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
								</div>

                            </div>
						</div>
					</div>
                    
				</div>
			</div>
		</div>
	</div>
</div>
