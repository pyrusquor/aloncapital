<?php 
    $project_id = isset($data['project_id']) && $data['project_id'] ? $data['project_id'] : 0;
    $document_id = isset($data['DocumentID']) && $data['DocumentID'] ? $data['DocumentID'] : 0;
    $lot_id = isset($data['LOTID']) && $data['LOTID'] ? $data['LOTID'] : 0;
    $customer_id = isset($data['CUSTOMERID']) && $data['CUSTOMERID'] ? $data['CUSTOMERID'] : 0;
    $mode = isset($data['MODE']) && $data['MODE'] ? $data['MODE'] : 'N/A';
    $status = isset($data['STATUS']) && $data['STATUS'] ? $data['STATUS'] : 'N/A';
    $reservation_application = isset($data['RA']) && $data['RA'] ? $data['RA'] : 'N/A';
    $reservation_application_date = isset($data['RADATE']) && $data['RADATE'] ? view_date($data['RADATE']) : 'N/A';
    $contract_to_sell = isset($data['CTS']) && $data['CTS'] ? $data['CTS'] : 'N/A';
    $contract_to_sell_date = isset($data['CTSDATE']) && $data['CTSDATE'] ? view_date($data['CTSDATE']) : 'N/A';
    $deed_of_sale = isset($data['DAS']) && $data['DAS'] ? $data['DAS'] : 'N/A';
    $deed_of_sale_date = isset($data['DASDATE']) && $data['DASDATE'] ? view_date($data['DASDATE']) : 'N/A';
    $smprice_p = isset($data['SMPRICE_P']) && $data['SMPRICE_P'] ? $data['SMPRICE_P'] : 0;
    $smprice = isset($data['SMPRICE']) && $data['SMPRICE'] ? $data['SMPRICE'] : 0;
    $downpayment_percentage_payment = isset($data['PERCENTDPP']) && $data['PERCENTDPP'] ? $data['PERCENTDPP'] : 0;
    $downpayment_percentage = isset($data['PERCENTDP']) && $data['PERCENTDP'] ? $data['PERCENTDP'] : 0;
    $downpayment_date_payment = isset($data['DPDATE_P']) && $data['DPDATE_P'] ? view_date($data['DPDATE_P']) : 'N/A';
    $downpayment_date = isset($data['DPDATE']) && $data['DPDATE'] ? view_date($data['DPDATE']) : 'N/A';
    $reservation_payment = isset($data['RESAMT_P']) && $data['RESAMT_P'] ? $data['RESAMT_P'] : 'N/A';
    $reservation_amount = isset($data['RESAMT']) && $data['RESAMT'] ? $data['RESAMT'] : 0;
    $reservation_day_payment = isset($data['RESDAYS_P']) && $data['RESDAYS_P'] ? $data['RESDAYS_P'] : 'N/A';
    $amortization_payment = isset($data['AMORTAMT_P']) && $data['AMORTAMT_P'] ? $data['AMORTAMT_P'] : 'N/A';
    $amortization_amount = isset($data['AMORTAMT']) && $data['AMORTAMT'] ? $data['AMORTAMT'] : 0;
    $monthly_amortization_period_payment = isset($data['MAPERIOD_P']) && $data['MAPERIOD_P'] ? $data['MAPERIOD_P'] : 'N/A';
    $monthly_amortization_period = isset($data['MAPERIOD']) && $data['MAPERIOD'] ? $data['MAPERIOD'] : 'N/A';
    $first_monthly_amortization_payment = isset($data['FIRSTMA_P']) && $data['FIRSTMA_P'] ? view_date($data['FIRSTMA_P']) : 'N/A';
    $first_monthly_amortization = isset($data['FIRSTMA']) && $data['FIRSTMA'] ? view_date($data['FIRSTMA']) : 'N/A';
    $interest_percentage = isset($data['INTEREST_P']) && $data['INTEREST_P'] ? $data['INTEREST_P'] : 'N/A';
    $interest = isset($data['INTEREST']) && $data['INTEREST'] ? $data['INTEREST'] : 'N/A';
    $discount_percentage = isset($data['DISCOUNT_P']) && $data['DISCOUNT_P'] ? $data['DISCOUNT_P'] : 'N/A';
    $discount = isset($data['DISCOUNT']) && $data['DISCOUNT'] ? $data['DISCOUNT'] : 'N/A';
    $division_manager_id = isset($data['DM']) && $data['DM'] ? $data['DM'] : 0;
    $unit_manager_id = isset($data['UM']) && $data['UM'] ? $data['UM'] : 0;
    $sales_executive_id = isset($data['SE']) && $data['SE'] ? $data['SE'] : 0;
    $sales_division_id = isset($data['DIVISION']) && $data['DIVISION'] ? $data['DIVISION'] : 0;
    $walk_in = isset($data['WALK_IN']) && $data['WALK_IN'] ? 'Yes' : 'No';
    $remarks = isset($data['REMARKS']) && $data['REMARKS'] ? $data['REMARKS'] : 'N/A';
    $date_cancelled = isset($data['DateCancelled']) && $data['DateCancelled'] ? view_date($data['DateCancelled']) : 'N/A';
    $date_created = isset($data['date_created']) && $data['date_created'] ? view_date($data['date_created']) : 'N/A';
    $payments = isset($payments) && $payments ? $payments : 0;
    

    $customer_first_name = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'FIRSTNAME');
    $customer_last_name = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'LASTNAME');
    $customer_full_name = $customer_first_name . " " . $customer_last_name;
    $customer_spouse_name = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'SPOUSENAME');
    $customer_address = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'ADDRESS');
    $customer_country = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'COUNTRY');
    $customer_phone_no = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'PHONE_NO');
    $customer_cell_no = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'CEL_NO');
    $customer_email = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'EMAIL_ADDR') ? get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'EMAIL_ADDR') : 'N/A';
    $customer_active = get_value_field_multiple(array('project_id' => $project_id, 'CUSTOMERID' => $customer_id), 'aris_customers', 'ACTIVE');
    $customer_status = isset($customer_active) && $customer_active ? 'Yes' : 'No';

    $division_manager_first_name = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $division_manager_id), 'aris_sales_agent', 'Firstname');
    $division_manager_last_name = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $division_manager_id), 'aris_sales_agent', 'Lastname');
    $division_manager_full_name = $division_manager_first_name ? $division_manager_first_name . " " . $division_manager_last_name : 'N/A';
    $division_manager_account_no = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $division_manager_id), 'aris_sales_agent', 'Acct_No') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $division_manager_id), 'aris_sales_agent', 'Acct_No') : 'N/A';
    $division_manager_address = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $division_manager_id), 'aris_sales_agent', 'Address') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $division_manager_id), 'aris_sales_agent', 'Address') : 'N/A';
    $division_manager_province = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $division_manager_id), 'aris_sales_agent', 'Province') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $division_manager_id), 'aris_sales_agent', 'Province') : 'N/A';

    $unit_manager_first_name = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $unit_manager_id), 'aris_sales_agent', 'Firstname');
    $unit_manager_last_name = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $unit_manager_id), 'aris_sales_agent', 'Lastname');
    $unit_manager_full_name = $unit_manager_first_name ? $unit_manager_first_name . " " . $unit_manager_last_name : 'N/A';
    $unit_manager_account_no = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $unit_manager_id), 'aris_sales_agent', 'Acct_No') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $unit_manager_id), 'aris_sales_agent', 'Acct_No') : 'N/A';
    $unit_manager_address = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $unit_manager_id), 'aris_sales_agent', 'Address') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $unit_manager_id), 'aris_sales_agent', 'Address') : 'N/A';
    $unit_manager_province = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $unit_manager_id), 'aris_sales_agent', 'Province') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $unit_manager_id), 'aris_sales_agent', 'Province') : 'N/A';
    
    $sales_executive_first_name = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_executive_id), 'aris_sales_agent', 'Firstname');
    $sales_executive_last_name = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_executive_id), 'aris_sales_agent', 'Lastname');
    $sales_executive_full_name = $sales_executive_first_name ? $sales_executive_first_name . " " . $sales_executive_last_name : 'N/A';
    $sales_executive_account_no = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_executive_id), 'aris_sales_agent', 'Acct_No') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_executive_id), 'aris_sales_agent', 'Acct_No') : 'N/A';
    $sales_executive_address = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_executive_id), 'aris_sales_agent', 'Address') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_executive_id), 'aris_sales_agent', 'Address') : 'N/A';
    $sales_executive_province = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_executive_id), 'aris_sales_agent', 'Province') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_executive_id), 'aris_sales_agent', 'Province') : 'N/A';

    $sales_division_first_name = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_division_id), 'aris_sales_agent', 'Firstname');
    $sales_division_last_name = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_division_id), 'aris_sales_agent', 'Lastname');
    $sales_division_full_name = $sales_division_first_name ? $sales_division_first_name . " " . $sales_division_last_name : 'N/A';
    $sales_division_account_no = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_division_id), 'aris_sales_agent', 'Acct_No') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_division_id), 'aris_sales_agent', 'Acct_No') : 'N/A';
    $sales_division_address = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_division_id), 'aris_sales_agent', 'Address') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_division_id), 'aris_sales_agent', 'Address') : 'N/A';
    $sales_division_province = get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_division_id), 'aris_sales_agent', 'Province') ? get_value_field_multiple(array('project_id' => $project_id, 'SalesAgentID' => $sales_division_id), 'aris_sales_agent', 'Province') : 'N/A';

    $lot_no = isset($lot['Lot_no']) && $lot['Lot_no'] ? $lot['Lot_no'] : 'N/A';
    $area = isset($lot['Area']) && $lot['Area'] ? $lot['Area'] : 'N/A';
    $price = isset($lot['Price']) && $lot['Price'] ? $lot['Price'] : 'N/A';
    $lot_remarks = isset($lot['Remarks']) && $lot['Remarks'] ? $lot['Remarks'] : 'N/A';
    $open = isset($lot['Open']) && $lot['Open'] ? $lot['Open'] : 'N/A';
?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Document Detail</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
            <a href="<?php echo site_url('aris/view/' . $project_id);?>" class="btn btn-label-instagram btn-sm btn-elevate">
                <i class="fa fa-reply"></i> Back
            </a>
			</div>
		</div>
	</div>
</div>

<div class="row">
    <div class="col-sm-6">
        <!-- Customer Information -->
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_customer_information">
                <div class="card">
                    <div class="card-header" id="head_customer_information">
                        <div class="card-title" data-toggle="collapse" data-target="#customer_information" aria-expanded="true" aria-controls="customer_information">
                            Customer Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div id="customer_information" class="collapse show" aria-labelledby="head_customer_information" data-parent="#accord_customer_information">
                        <div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Customer Name</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$customer_full_name?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Email</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$customer_email?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Phone Number</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$customer_phone_no?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Mobile Number</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$customer_cell_no?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Address</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$customer_address?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Country</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$customer_country?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Active</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$customer_status?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
    <div class="col-sm-6">
        <!-- Lot Information -->
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_lot_information">
                <div class="card">
                    <div class="card-header" id="head_lot_information">
                        <div class="card-title" data-toggle="collapse" data-target="#lot_information" aria-expanded="true" aria-controls="lot_information">
                            Lot Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div id="lot_information" class="collapse show" aria-labelledby="head_lot_information" data-parent="#accord_lot_information">
                        <div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Lot ID</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$lot_id?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Lot No</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$lot_no?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Area</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$area?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Price</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=money_php($price)?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Remarks</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$lot_remarks?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Open</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$open?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-sm-6">
        <!-- Document Information -->
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_document_information">
                <div class="card">
                    <div class="card-header" id="head_document_information">
                        <div class="card-title" data-toggle="collapse" data-target="#document_information" aria-expanded="true" aria-controls="document_information">
                            Document Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div id="document_information" class="collapse show" aria-labelledby="head_document_information" data-parent="#accord_document_information">
                        <div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Mode</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$mode?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Status</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$status?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Reservation Application</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$reservation_application?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Reservation Application Date</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$reservation_application_date?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Contract to Sell</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$contract_to_sell?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Contract To Sell Date</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$contract_to_sell_date?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Deed of Absolute Sale</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$deed_of_sale?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Deed of Absolute Sale Date</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$deed_of_sale_date?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">SM Price Payment</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$smprice_p?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">SM Price</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$smprice?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Downpayment Percentage</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$downpayment_percentage_payment?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Downpayment Date</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$downpayment_date_payment?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Downpayment Percentage</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$downpayment_percentage?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Downpayment Date</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$downpayment_date?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Reservation</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=money_php($reservation_payment)?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Reservation Amount</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=money_php($reservation_amount)?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Reservation Terms</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$reservation_day_payment?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Amortization</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=money_php($amortization_payment)?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Amortization Amount</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=money_php($amortization_amount)?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Monthly Amortization Period</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$monthly_amortization_period_payment?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Monthly Amortization Period</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$monthly_amortization_period?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">First Monthly Amortization Period</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$first_monthly_amortization_payment?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">First Monthly Amortization</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$first_monthly_amortization?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Interest Percentage</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$interest_percentage?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Interest</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$interest?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Discount Percentage</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$discount_percentage?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Discount</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$discount?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-sm-6">
        <!-- Sales Agent Information -->
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_sales_agent_information">
                <div class="card">
                    <div class="card-header" id="head_sales_agent_information">
                        <div class="card-title" data-toggle="collapse" data-target="#sales_agent_information" aria-expanded="true" aria-controls="sales_agent_information">
                            Sales Agent Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div id="sales_agent_information" class="collapse show" aria-labelledby="head_sales_agent_information" data-parent="#accord_sales_agent_information">
                        <div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Division Manager</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$division_manager_full_name?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Account No</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$division_manager_account_no?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Address</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$division_manager_address?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Province</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$division_manager_province?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                            
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Unit Manager</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$unit_manager_full_name?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Account No</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$unit_manager_account_no?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Address</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$unit_manager_address?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Province</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$unit_manager_province?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                            
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Sales Executive</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$sales_executive_full_name?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Account No</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$sales_executive_account_no?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Address</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$sales_executive_address?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Province</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$sales_executive_province?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            
                            <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>

                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Sales Division</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$sales_division_full_name?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Account No</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$sales_division_account_no?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Address</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$sales_division_address?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Province</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?=$sales_division_province?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-sm-12">
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_payment_information">
                <div class="card">
                    <div class="card-header" id="head_payment_information">
                        <div class="card-title" data-toggle="collapse" data-target="#payment_information" aria-expanded="true" aria-controls="payment_information">
                            Payments <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div id="payment_information" class="collapse show" aria-labelledby="head_payment_information" data-parent="#accord_payment_information">
                        <div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-12">
                                    <table class="table table-striped- table-bordered table-hover">
                                        <thead>
                                            <tr>
                                            <th>Period</th>
											<th>Date Due</th>
											<th>Date Paid</th>
											<th>OR No</th>
											<th>OR Date</th>
											<th>Principal</th>
											<th>Interest</th>
											<th>Penalty</th>
											<th>Penalty Rate</th>
											<th>Amount Paid</th>
											<th>Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($payments): ?>
                                                <?php foreach($payments as $key => $value): ?>
                                                    <tr>
                                                        <td><?=$value['PERIOD']; ?></td>
                                                        <td><?=view_date($value['DATE_DUE']); ?></td>
                                                        <td><?=view_date($value['DATE_PAID']); ?></td>
                                                        <td><?=$value['OR_NO']; ?></td>
                                                        <td><?=view_date($value['OR_DATE']); ?></td>
                                                        <td><?=money_php($value['PRINCIPAL']); ?></td>
                                                        <td><?=money_php($value['INTEREST']); ?></td>
                                                        <td><?=money_php($value['PENALTY']); ?></td>
                                                        <td><?=$value['PENALTY_RATE']; ?></td>
                                                        <td><?=$value['AmountPaid']; ?></td>
                                                        <td><?=$value['REMARKS']; ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <tr>
                                                    <td colspan="11" class="text-center">No record found</td>
                                                </tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                 
    </div>
</div>