<?php 
    $payments = isset($data) && $data ? $data : 0;
    $project_id = isset($project['project_id']) && $project['project_id'] ? $project['project_id'] : 0;
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
  <div class="kt-container kt-container--fluid">
    <div class="kt-subheader__main">
      <h3 class="kt-subheader__title">Transaction Detail</h3>
    </div>
    <div class="kt-subheader__toolbar">
      <div class="kt-subheader__wrapper">
        <a href="<?php echo site_url('aris/view/' . $project_id);?>" class="btn btn-label-instagram btn-sm btn-elevate">
          <i class="fa fa-reply"></i> Back
        </a>
      </div>
    </div>
  </div>
</div>

<!-- begin:: Content -->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group form-group-xs row">
                    <table class="table table-striped- table-bordered table-hover" id="payment_table">
                    <thead>
                        <tr>
                            <th>Period</th>
                            <th>Date Due</th>
                            <th>Date Paid</th>
                            <th>OR No</th>
                            <th>OR Date</th>
                            <th>Principal</th>
                            <th>Interest</th>
                            <th>Penalty</th>
                            <th>Penalty Rate</th>
                            <th>Amount Paid</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($payments): ?>
                            <?php foreach($payments as $key => $value): ?>
                                <tr>
                                    <td><?=$value['PERIOD']; ?></td>
                                    <td><?=view_date($value['DATE_DUE']); ?></td>
                                    <td><?=view_date($value['DATE_PAID']); ?></td>
                                    <td><?=$value['OR_NO']; ?></td>
                                    <td><?=view_date($value['OR_DATE']); ?></td>
                                    <td><?=money_php($value['PRINCIPAL']); ?></td>
                                    <td><?=money_php($value['INTEREST']); ?></td>
                                    <td><?=money_php($value['PENALTY']); ?></td>
                                    <td><?=$value['PENALTY_RATE']; ?></td>
                                    <td><?=$value['AmountPaid']; ?></td>
                                    <td><?=$value['REMARKS']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="11" class="text-center">No record found</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
	.table#payment_table td {
		padding: 0.75rem 0.25rem;
		vertical-align: top;
		border-top: 1px solid #ebedf2;
		font-size: 12px;
	}
</style>