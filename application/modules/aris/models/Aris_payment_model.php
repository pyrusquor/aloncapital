<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aris_payment_model extends MY_Model {
    public $table = 'aris_payments'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'project_id',
        'PaymentID',
        'DocumentID',
        'DATE_DUE',
        'PERIOD',
        'DATE_PAID',
        'OR_NO',
        'OR_DATE',
        'PRINCIPAL',
        'INTEREST',
        'PENALTY',
        'PENALTY_RATE',
        'REMARKS',
        'AmountPaid',
        'PaymentFor',
        'official_payment_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'project_id' => array(
            'field' => 'project_id',
            'label' => 'Projec ID',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }

    public function if_synced($document_id = 0, $project_id = 0){

        $this->db->select('count(id) as count');
        $this->db->where('project_id', $project_id);
        $this->db->where('DocumentID', $document_id);
        $this->db->where('official_payment_id!="0"');
        $this->db->where('deleted_at is null');
        $query = $this->db->get('aris_payments')->result_array();
        if($query[0]['count']){
            return 1;
        }else{
            return 0;
        }

    }

    public function get_new_payments($project_id,$document_id){
        $this->db->where('DATE_PAID > "2021-08-11 00:00:00"');
        $this->db->where('deleted_at is null');
        if($project_id){
            $this->db->where('project_id', $project_id);
        }
        if($document_id){
            $this->db->where('DocumentID', $project_id);
        }
        $this->db->order_by('project_id', 'ASC');
        $this->db->order_by('PaymentID', 'ASC');
        $payments = $this->db->get('aris_payments')->result_array();
        $mod_payments = [];
        $results = [];
        $ids = [];
        foreach($payments as $key => $payment ){
            $this->db->select('id');
            $this->db->where('deleted_at is null');
            $this->db->where('aris_id = "'. $payment['project_id'] . '-' . $payment['DocumentID'] .'"');
            $transaction = $this->db->get('transactions')->result_array();
            if($transaction){
                $ids[] = $transaction[0]['id'];
                $mod_payments[] = $payment + array('transaction_id' => $transaction[0]['id']);
            }
        }

        // foreach($ids as $key => $id){
        //     $filtered = array_filter($mod_payments, function ($var) use ($id) {
        //         return ($var['transaction_id'] == $id);
        //     });
        //     $results["$id"] = $filtered;
        // }
        return $ids;
    }

    public function get_sum($params = array()){
        $this->db->select_sum('AmountPaid');

        if( ! empty($params['project_id']))
        {
            $this->db->where('aris_payments.project_id', $params['project_id']);
        }

        $this->db->where('aris_payments.deleted_at IS NULL');

        return $this->db->get('aris_payments')->row_array();
    }
}