<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aris_customer_model extends MY_Model {
    public $table = 'aris_customers'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'project_id',
        'CUSTOMERID',
        'FIRSTNAME',
        'MI',
        'LASTNAME',
        'SPOUSENAME',
        'ADDRESS',
        'CITY',
        'POSTALCODE',
        'PROVINCE',
        'COUNTRY',
        'PHONE_NO',
        'CEL_NO',
        'FAX_NO',
        'EMAIL_ADDR',
        'REMARKS',
        'ACTIVE',
        'ExcludeLPD',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'project_id' => array(
            'field' => 'project_id',
            'label' => 'Projec ID',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}