<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aris_report_model extends MY_Model {
    public $table = 'aris_documents'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'project_id',
        'DocumentID',
        'DateDue',
        'Interest',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'project_id' => array(
            'field' => 'project_id',
            'label' => 'Projec ID',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->sort_by = 'id';
        $this->sort_order = "DESC";


        
        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
    public function get_all_documents( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( $this->table );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LOTID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = aris_documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_customers.FIRSTNAME', 
                    'aris_customers.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['where_in'] ) && !empty( $params['where_in'] ) ) {
            foreach ($params['where_in'] as $key => $where_in) {
                $this->db->where_in( $key, $ids );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_documents.' . $this->sort_by, $this->sort_order );
        }
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( ( isset( $params['id'] ) && !empty( $params['id'] ) ) || ( isset( $params['row'] ) && !empty( $params['row'] ) ) ){
                if( ( isset( $params['id'] ) && !empty( $params['id'] ) ) ){
                    $this->db->where( 'aris_documents.id', $params['id'] );
                }
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        
        return $return;
    }
    public function get_all_customers( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'aris_customers' );


        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_customers.FIRSTNAME', 
                    'aris_customers.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_customers.' . $this->sort_by, $this->sort_order );
        }
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_customers.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        // echo $this->db->last_query();
        return $return;
    }
    public function get_all_collections( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'aris_payments' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = aris_payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LotID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = aris_documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_payments.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'aris_customers.FIRSTNAME', 'ASC');
        $this->db->order_by( 'aris_payments.PERIOD', 'ASC');
        

        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }

    public function get_all_collections_v2( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        // if (empty($params['select']) && !isset($params['select'])) {
        //     $params['select'] = "*";
        // }
       

        $params['select'] = "aris_customers.LASTNAME, aris_customers.FIRSTNAME, aris_customers.MI, aris_lots.Lot_no, aris_documents.RADATE,aris_documents.DocumentID,aris_documents.project_id,
                        SUM(aris_payments.PRINCIPAL) as total_principal,
                        SUM(aris_payments.INTEREST) as total_interest,
                        SUM(aris_payments.PENALTY) as total_penalty,
                        SUM(aris_payments.AmountPaid) as total_paid";

        $this->db->select($params['select']);

        $this->db->from( 'aris_documents' );

        $this->db->join( 'aris_payments', 'aris_documents.DocumentID = aris_payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LotID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = aris_documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 
        
        $this->db->where('aris_documents.deleted_at IS NULL');
        // $this->db->where('aris_payments.deleted_at IS NULL');
        $this->db->where('aris_lots.deleted_at IS NULL');
        $this->db->where('aris_documents.DateCancelled IS NULL');

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_payments.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'aris_customers.FIRSTNAME', 'ASC');
        $this->db->order_by( 'aris_documents.RADATE', 'ASC');
        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');
        
        if ($params['group_by'] == "DocumentID") {
            $this->db->group_by("aris_payments.DocumentID");
        } else if ($params['group_by'] == "project_id") {
            $this->db->group_by("aris_payments.project_id");
        }

        // $this->db->group_by("aris_payments.DocumentID");

        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
    public function get_all_collectibles( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'aris_collectibles' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = aris_collectibles.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LOTID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = aris_documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 


        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_collectibles.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'aris_collectibles.DateDue', 'ASC');


        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_collectibles.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }

    public function get_all_collectibles_test( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'collectibles' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = collectibles.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 


        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'collectibles.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'collectibles.DateDue', 'ASC');


        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'collectibles.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }

    public function get_all_fully_paid( $params = array(), $count = false ){
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'aris_payments' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['like'] ) && !empty( $params['like'] ) ) {
            foreach ($params['like'] as $key => $like) {
                $this->db->like( $key, $like );
            }
        } 


        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_payments.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');


        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
    
    public function get_all_cancelled( $params = array(), $count = false ){
        error_reporting(E_ALL);
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'aris_payments' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = aris_payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LOTID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = aris_documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['like'] ) && !empty( $params['like'] ) ) {
            foreach ($params['like'] as $key => $like) {
                $this->db->like( $key, $like );
            }
        } 


        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_payments.' . $this->sort_by, $this->sort_order );
        }

        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');


        if( isset( $params['group_by'] ) && !empty( $params['group_by'] ) ) {
            $this->db->group_by($params['group_by']);
        } 
        
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }

    public function get_all_pastdue( $params = array(), $count = false , $table = ""){
        error_reporting(E_ALL);
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( $table );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = aris_collectibles.DocumentID', 'left' );
        $this->db->join( 'aris_customers', 'aris_documents.CUSTOMERID = aris_customers.CUSTOMERID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LOTID', 'left' );
        // $this->db->join( 'aris_payments', 'aris_payments.DocumentID = collectibles.DocumentID AND collectibles.DateDue != payments.DATE_DUE', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['like'] ) && !empty( $params['like'] ) ) {
            foreach ($params['like'] as $key => $like) {
                $this->db->like( $key, $like );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_payments.' . $this->sort_by, $this->sort_order );
        }

        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');


        if( isset( $params['group_by'] ) && !empty( $params['group_by'] ) ) {
            $this->db->group_by($params['group_by']);
        } 
        
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }

    public function get_all_customer_ledger( $params = array(), $count = false ) {
            error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'aris_payments' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LotID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = documents.CUSTOMERID', 'left' );
        $this->db->join( 'collectibles', 'collectibles.DocumentID = documents.DocumentID AND collectibles.project_id=customers.project_id AND collectibles.DateDue=payments.DATE_DUE', 'left' );


        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_payments.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'aris_customers.FIRSTNAME', 'ASC');
        $this->db->order_by( 'aris_payments.PERIOD', 'ASC');
        

        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        // echo $this->db->last_query();die();
        return $return;
    }


    public function get_all_ar( $params = array(), $count = false ){
        error_reporting(E_ALL);
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'collectibles' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = collectibles.DocumentID', 'left' );
        $this->db->join( 'aris_customers', 'aris_documents.CUSTOMERID = customers.CUSTOMERID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        // $this->db->join( 'aris_payments', 'aris_payments.DocumentID = collectibles.DocumentID AND collectibles.DateDue != payments.DATE_DUE', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['like'] ) && !empty( $params['like'] ) ) {
            foreach ($params['like'] as $key => $like) {
                $this->db->like( $key, $like );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_payments.' . $this->sort_by, $this->sort_order );
        }

        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');


        if( isset( $params['group_by'] ) && !empty( $params['group_by'] ) ) {
            $this->db->group_by($params['group_by']);
        } 
        
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
}