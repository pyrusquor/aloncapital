<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aris_document_model extends MY_Model {
    public $table = 'aris_documents'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'project_id',
        'DocumentID',
        'LOTID',
        'CUSTOMERID',
        'MODE',
        'STATUS',
        'RA',
        'RADATE',
        'CTS',
        'CTSDATE',
        'DAS',
        'DASDATE',
        'SMPRICE_P',
        'SMPRICE',
        'PERCENTDPP',
        'PERCENTDP',
        'DPDATE_P',
        'DPDATE',
        'RESAMT_P',
        'RESAMT',
        'RESDAYS_P',
        'RESDAYS',
        'AMORTAMT_P',
        'AMORTAMT',
        'MAPERIOD_P',
        'MAPERIOD',
        'FIRSTMA_P',
        'FIRSTMA',
        'INTEREST_P',
        'INTEREST',
        'DISCOUNT_P',
        'DISCOUNT',
        'DM',
        'UM',
        'SE',
        'DIVISION',
        'WALK_IN',
        'REMARKS',
        'DateCancelled',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        'last_date_synced',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'project_id' => array(
            'field' => 'project_id',
            'label' => 'Project ID',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }


    public function sales_report($params = [], $count = false)
    {   
        if ( isset($params['mod']) && ($params['mod'] == "get_properties")) {
            // code...
            $this->db->select('aris_lots.Lot_no as Lot_no');
        } else {
            $this->db->select('
                COUNT(aris_documents.id) as t_count,
                SUM(aris_lots.Area * aris_lots.Price) as total_selling_price,

            ', false);
        }
       
        // SUM(COALESCE(aris_documents.RESAMT_P,0) + COALESCE(aris_documents.AMORTAMT_P,0) + COALESCE(aris_documents.DISCOUNT_P,0)) as total_selling_price,

        $this->db->join('aris_lots', 'aris_lots.LotID = aris_documents.LOTID', 'left');


        if (!empty($params['date'])) {
            $this->db->where('DATE(aris_documents.RADATE) = ', $params['date']);
        }

        if (!empty($params['year'])) {
            $this->db->where('YEAR(aris_documents.RADATE) = ', $params['year']);
        }

        if (!empty($params['year_month'])) {
            $this->db->where('DATE_FORMAT(aris_documents.RADATE, "%Y-%m") = ', $params['year_month']);
        }
        if (!empty($params['year_month_range'])) {
            $this->db->where("DATE_FORMAT(aris_documents.RADATE, '%Y-%m') between '" .  $params['year_month_range'][0] . "' AND '" . $params['year_month_range'][1] . "'");
        }


        if (!empty($params['daterange'])) {
            $dates = explode('-', $params['daterange']);
            $f_date_from = db_date($dates[0]);
            $f_date_to  = db_date($dates[1]);
            $this->db->where('aris_documents.RADATE BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
        }

        if (!empty($params['project_id'])) {
            $this->db->where('aris_documents.project_id', $params['project_id']);
            $this->db->where('aris_lots.project_id', $params['project_id']);
        }

        $this->db->where('aris_documents.deleted_at IS NULL');
        $this->db->where('aris_documents.DateCancelled IS NULL');


        return $this->db->get('aris_documents')->result_array();
    }
}