<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aris_model extends MY_Model {
    public $table = 'aris_project_information'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'SetupID',
        'ProjectName',
        'Address',
        'City',
        'Province',
        'PostalCode',
        'Country',
        'PhoneNumber',
        'FaxNumber',
        'Password',
        'Confirmation',
        'Remarks',
        'date_created',
        'date_updated',
        'updated_by',
        'deleted_at',
        'deleted_by',
        'last_date_synced',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->sort_by = 'id';
        $this->sort_order = 'asc';
        $this->table_aris  = 'aris_documents';


        // for relationship tables
        $this->has_many['documents'] = array('foreign_model'=>'aris/aris_document_model','foreign_table'=>'aris_documents','foreign_key'=>'project_id','local_key'=>'id');
        $this->has_many['ars'] = array('foreign_model'=>'aris/aris_ar_model','foreign_table'=>'aris_ar','foreign_key'=>'project_id','local_key'=>'id');
        $this->has_many['collectibles'] = array('foreign_model'=>'aris/aris_collectible_model','foreign_table'=>'aris_collectibles','foreign_key'=>'project_id','local_key'=>'id');
        $this->has_many['customers'] = array('foreign_model'=>'aris/aris_customer_model','foreign_table'=>'aris_customers','foreign_key'=>'project_id','local_key'=>'id');
        $this->has_many['aris_lots'] = array('foreign_model'=>'aris/aris_lot_model','foreign_table'=>'aris_lots','foreign_key'=>'project_id','local_key'=>'id');
        $this->has_many['past_dues'] = array('foreign_model'=>'aris/aris_past_due_model','foreign_table'=>'aris_pastdue','foreign_key'=>'project_id','local_key'=>'id');
        $this->has_many['pds'] = array('foreign_model'=>'aris/aris_pd_model','foreign_table'=>'aris_pd','foreign_key'=>'project_id','local_key'=>'id');
        $this->has_many['sales_agent'] = array('foreign_model'=>'aris/aris_sales_agent_model','foreign_table'=>'aris_sales_agent','foreign_key'=>'project_id','local_key'=>'id');
        $this->has_many['payments'] = array('foreign_model'=>'aris/aris_payment_model','foreign_table'=>'aris_payments','foreign_key'=>'project_id','local_key'=>'id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table_aris, $data);

    }

    public function get_aris($table = "",$params = array()) {

        if( !empty($params['where']) && isset($params['where']) ){
            foreach($params['where'] as $key => $value){
                $this->db->where($key, $value);
            }
        } 
        if( !empty($params['limit']) && isset($params['limit']) ){
            $this->db->limit( $params['limit'], $params['offset'] );
        }

        if( !empty($params['sort_by']) && isset($params['sort_order']) ){
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        }

        $this->db->where('deleted_at is null');

        $query = $this->db->get($table);

        if (isset($params['row'])) {
            $result = $query->row_array();
        } else {
            $result = $query->result_array();
        }
     

        return $result;
        
    }

     public function save_aris($table = "",$params = array(),$batch = 0) {
        
        if ($batch == 1) {
            //insert
            $id = $this->db->insert_batch($table, $params);
        } else if ($batch == 2) {
            //update
            $id = $this->db->update_batch($table, $params, 'id');
            // echo $this->aris->last_query();die();
        }  else if (isset($params['id']) && !empty($params['id']))  {
            $this->db->where('id', $params['id']);
            $this->db->update($table, $params);
            $id = $params['id'];
        } else {
            $query = $this->db->insert($table, $params);
            $id = $this->db->insert_id();
        }
            
        return true;
    }

    public function get_all_project($project = "",$password = "",$selected = "",$last_id = 0) {
        error_reporting(E_ALL);
        $this->load_project_database($project,$password);
        $tables = $this->aris_tables($selected);
        $data[$project] = array();
        // echo "<pre>";print_r($tables);die();
        if ($tables) {
            foreach ($tables as $key => $table) {
                $where = array();
                $w = "";
                if ( ($table == "payments") && ($last_id) ) {
                    $where['PaymentID >'] = (int) $last_id;
                    $w = " WHERE PaymentID > ".$last_id;
                }
                if ( ($table == "collectibles") && ($last_id) ) {
                    $where['DateDue >'] =  "#".$last_id."#";
                    $w = " WHERE DateDue > #".$last_id."#";
                }
                if ( ($table == "ar") && ($last_id) ) {
                    $where['DocumentID >'] = (int) $last_id;
                    $w = " WHERE DocumentID > ".$last_id;
                }
                $q = $this->aris_project->query("SELECT * FROM ([".$table."])".$w);
                // $query = $this->aris_project->get_where('['.$table.']',$where);
                $data[$project][$table] =  $q->result_array();
                // echo $this->aris_project->last_query();die();
                // vdebug($q);

            }
        }
        // echo $this->aris_project->last_query();die();
        $this->aris_project->close();
        return $data;

    }
    public function load_project_database($project = "",$password = ""){
        // $this->load->database();

        // $main = "\\\\rebg-carmela\\CARM\\WORKING ARIS (MAIN)\\";
        $dbName  = $main.$project.".mdb";

        // The folder structure on the dev environment is different from the staging server.
        // Check the environment and set the appropriate path.

        if(ENVIRONMENT=='development'){
            $dbName  = $_SERVER["DOCUMENT_ROOT"] . "/aris_files/".$project.".mdb";
        } else{
            $dbName  = $_SERVER["DOCUMENT_ROOT"] . "/rems/aris_files/".$project.".mdb";
        }

        // $s = "//192.168.0.51/carm/WORKING ARIS (MAIN)/";
        // $s = "//192.168.0.195/WORKING_ARIS_MAIN/";
        // $dbName = $s.$project.".mdb";

        // This change should make sure that PHP use the 64bit odbc driver. Make sure that you are using 64bit PHP
        $db_conn = 'Driver={Microsoft Access Driver (*.mdb, *.accdb)}; DBQ='.$dbName;
        $db['aris_project']['hostname'] = $db_conn;
        $db['aris_project']['password'] = $password;
        $db['aris_project']['database'] = $db_conn;
        $db['aris_project']['username'] = "sa";
        $db['aris_project']['dbdriver'] = 'odbc';
        $db['aris_project']['dbprefix'] = '';
        $db['aris_project']['pconnect'] = TRUE;
        $db['aris_project']['db_debug'] = TRUE;
        $db['aris_project']['cache_on'] = FALSE;
        $db['aris_project']['cachedir'] = '';
        $db['aris_project']['char_set'] = 'utf8';
        $db['aris_project']['dbcollat'] = 'utf8_general_ci';
        $db['aris_project']['swap_pre'] = '';
        $db['aris_project']['autoinit'] = TRUE;
        $db['aris_project']['stricton'] = FALSE;
        // echo "<pre>";print_r($db);die();
        $this->aris_project = $this->load->database($db['aris_project'], TRUE);
        // vdebug($this->aris_project);
    }

    public function aris_tables($selected = ""){
        $data = array(
            'project information',
            'aris_lots',
            'aging',
            'amortization',
            'amortization calculator',
            'ar',
            'collectibles',
            'customers',
            'documents',
            'pastdue',
            'documents',
            'payments',
            'sales agent'
        );

        if ($selected) { $data =array(); $data[0] = 'project information'; $data[] = $selected; }
        return $data;
    }


    public function get_all_documents( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( $this->table_aris );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LOTID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = aris_documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_customers.FIRSTNAME', 
                    'aris_customers.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['where_in'] ) && !empty( $params['where_in'] ) ) {
            foreach ($params['where_in'] as $key => $where_in) {
                $this->db->where_in( $key, $ids );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_documents.' . $this->sort_by, $this->sort_order );
        }
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( ( isset( $params['id'] ) && !empty( $params['id'] ) ) || ( isset( $params['row'] ) && !empty( $params['row'] ) ) ){
                if( ( isset( $params['id'] ) && !empty( $params['id'] ) ) ){
                    $this->db->where( 'aris_documents.id', $params['id'] );
                }
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        
        return $return;
    }
    public function get_all_customers( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'customers' );


        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'customers.FIRSTNAME', 
                    'customers.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'customers.' . $this->sort_by, $this->sort_order );
        }
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'customers.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        // echo $this->db->last_query();
        return $return;
    }
    public function get_all_collections( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'aris_payments' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = aris_payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LotID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = aris_documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'aris_payments.FIRSTNAME', 
                    'aris_payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if ($params['not_where']) {
            $this->db->where( array('aris_payments.REMARKS !=' => 'OR Cancelled') );
        }

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'aris_payments.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'aris_customers.FIRSTNAME', 'ASC');
        $this->db->order_by( 'aris_payments.DATE_PAID', 'ASC');
        

        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'aris_payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
    public function get_all_collectibles( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'collectibles' );

        $this->db->join( 'documents', 'documents.DocumentID = collectibles.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        $this->db->join( 'customers', 'customers.CUSTOMERID = documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'payments.FIRSTNAME', 
                    'payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 


        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'collectibles.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'collectibles.DateDue', 'ASC');


        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'collectibles.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
    public function get_all_collectibles_test( $params = array(), $count = false ) {
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'collectibles' );

        $this->db->join( 'documents', 'documents.DocumentID = collectibles.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        $this->db->join( 'customers', 'customers.CUSTOMERID = documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'payments.FIRSTNAME', 
                    'payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 


        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'collectibles.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'collectibles.DateDue', 'ASC');


        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'collectibles.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
    public function get_all_fully_paid( $params = array(), $count = false ){
        error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'payments' );

        $this->db->join( 'documents', 'documents.DocumentID = payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        $this->db->join( 'customers', 'customers.CUSTOMERID = documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'payments.FIRSTNAME', 
                    'payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['like'] ) && !empty( $params['like'] ) ) {
            foreach ($params['like'] as $key => $like) {
                $this->db->like( $key, $like );
            }
        } 


        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'payments.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');


        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
    public function get_all_cancelled( $params = array(), $count = false ){
        error_reporting(E_ALL);
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'payments' );

        $this->db->join( 'documents', 'documents.DocumentID = payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        $this->db->join( 'customers', 'customers.CUSTOMERID = documents.CUSTOMERID', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'payments.FIRSTNAME', 
                    'payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['like'] ) && !empty( $params['like'] ) ) {
            foreach ($params['like'] as $key => $like) {
                $this->db->like( $key, $like );
            }
        } 

        

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'payments.' . $this->sort_by, $this->sort_order );
        }

        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');


        if( isset( $params['group_by'] ) && !empty( $params['group_by'] ) ) {
            $this->db->group_by($params['group_by']);
        } 
        
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
    public function get_all_pastdue( $params = array(), $count = false , $table = ""){
        error_reporting(E_ALL);
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( $table );

        $this->db->join( 'documents', 'documents.DocumentID = collectibles.DocumentID', 'left' );
        $this->db->join( 'customers', 'documents.CUSTOMERID = customers.CUSTOMERID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        // $this->db->join( 'payments', 'payments.DocumentID = collectibles.DocumentID AND collectibles.DateDue != payments.DATE_DUE', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'payments.FIRSTNAME', 
                    'payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['like'] ) && !empty( $params['like'] ) ) {
            foreach ($params['like'] as $key => $like) {
                $this->db->like( $key, $like );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'payments.' . $this->sort_by, $this->sort_order );
        }

        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');


        if( isset( $params['group_by'] ) && !empty( $params['group_by'] ) ) {
            $this->db->group_by($params['group_by']);
        } 
        
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }


    public function get_all_customer_ledger( $params = array(), $count = false ) {
            error_reporting(E_ALL);
        
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'aris_payments' );

        $this->db->join( 'aris_documents', 'aris_documents.DocumentID = aris_payments.DocumentID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = aris_documents.LotID', 'left' );
        $this->db->join( 'aris_customers', 'aris_customers.CUSTOMERID = aris_documents.CUSTOMERID', 'left' );
        $this->db->join( 'aris_collectibles', 'aris_collectibles.DocumentID = aris_documents.DocumentID AND aris_collectibles.project_id=aris_customers.project_id AND aris_collectibles.DateDue=aris_payments.DATE_DUE', 'left' );


        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'payments.FIRSTNAME', 
                    'payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'payments.' . $this->sort_by, $this->sort_order );
        }
        $this->db->order_by( 'customers.FIRSTNAME', 'ASC');
        $this->db->order_by( 'payments.PERIOD', 'ASC');
        

        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        // echo $this->db->last_query();die();
        return $return;
    }


    public function get_all_ar( $params = array(), $count = false ){
        error_reporting(E_ALL);
        // echo "<pre>";print_r($params);die();
        if (empty($params['select']) && !isset($params['select'])) {
            $params['select'] = "*";
        }
        $this->db->select($params['select']);
        $this->db->from( 'collectibles' );

        $this->db->join( 'documents', 'documents.DocumentID = collectibles.DocumentID', 'left' );
        $this->db->join( 'customers', 'documents.CUSTOMERID = customers.CUSTOMERID', 'left' );
        $this->db->join( 'aris_lots', 'aris_lots.LotID = documents.LOTID', 'left' );
        // $this->db->join( 'payments', 'payments.DocumentID = collectibles.DocumentID AND collectibles.DateDue != payments.DATE_DUE', 'left' );

        if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
                if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
                $search_fields = array( 
                    'payments.FIRSTNAME', 
                    'payments.LASTNAME', 
                );
                $term = explode(' ', $params['search']->term );
                foreach( $term as $t ) {
                    $not        = '';
                    $operator   = 'OR';
                    if( substr( $t, 0, 1 ) == '-' ) {
                        $not        = 'NOT ';
                        $operator   = 'AND';
                        $t      = substr( $t, 1,strlen( $t ) );
                    }
                    $index   = 0;
                    $like    = '';
                    $like   .= "(";
                    foreach ( $search_fields as $field ) {
                        $index++;
                        $like .= " ".$field." ".$not."LIKE '%".$t."%' ";
                        $like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
                    }
                    $like   .= ") ";
                    $this->db->where( $like );
                }
            }
        }

        if( isset( $params['where'] ) && !empty( $params['where'] ) ) {
            foreach ($params['where'] as $key => $where) {
                $this->db->where( $key, $where );
            }
        } 

        if( isset( $params['like'] ) && !empty( $params['like'] ) ) {
            foreach ($params['like'] as $key => $like) {
                $this->db->like( $key, $like );
            }
        } 

        //ORDER BY
        if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        } else {
            $this->db->order_by( 'payments.' . $this->sort_by, $this->sort_order );
        }

        $this->db->order_by( 'aris_lots.Lot_no', 'ASC');


        if( isset( $params['group_by'] ) && !empty( $params['group_by'] ) ) {
            $this->db->group_by($params['group_by']);
        } 
        
        //TO GET ALL RECORD
        if( $count ){
            //LIMIT
            if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                $this->db->limit( $params['limit'], $params['offset'] );
            } else {
                $this->db->limit( $params['limit'] );
            }
            $return = $this->db->count_all_results();
        } else {
            if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
                $this->db->where( 'payments.id', $params['id'] );
                $return = $this->db->get()->row_array();
            } else {
                //LIMIT
                if(  isset( $params['offset'] ) && isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
                    $this->db->limit( $params['limit'], $params['offset'] );
                } else {
                    if(!empty($params['limit']))
                        $this->db->limit( $params['limit'] );
                }
                $return = $this->db->get()->result_array();
            }
        }
        return $return;
    }
}