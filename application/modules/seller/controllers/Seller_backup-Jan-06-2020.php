<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seller extends MY_Controller
{

	private $fields = [
		array(
			'field' => 'info[last_name]',
			'label' => 'Last Name',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'info[first_name]',
			'label' => 'First Name',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'info[birth_date]',
			'label' => 'Birth Date',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'contact[email]',
			'label' => 'Email',
			'rules' => 'trim|required|is_unique[users.email]'
		),
	];

	public function __construct()
	{
		parent::__construct();

		// Load models
		$this->load->model('Seller_model', 'M_seller');
		$this->load->model('seller/Seller_reference_model', 'M_seller_ref');
		$this->load->model('seller/Seller_contact_info_model', 'M_seller_contact_info');
		$this->load->model('seller/Seller_source_info_model', 'M_seller_source_info');
		$this->load->model('seller/Seller_work_experience_model', 'M_seller_work_exp');
		$this->load->model('seller/Seller_education_model', 'M_seller_educ');
		$this->load->model('seller/Seller_training_model', 'M_seller_training');
		$this->load->model('seller/Seller_exam_model', 'M_seller_exam');
		$this->load->model('position/Seller_position_model', 'M_seller_position');
		$this->load->model('auth/Ion_auth_model', 'M_auth');
		$this->load->model('user/User_model', 'M_user');

		// Load pagination library 
		$this->load->library('ajax_pagination');

		// Per page limit 
		$this->perPage = 9;
	}

	public function index_old()
	{
		// Get record count 
		// $conditions['returnType'] = 'count'; 
		$this->view_data['totalRec'] = $totalRec = $this->M_seller->count_rows();

		// Pagination configuration 
		$config['target']      = '#sellerContent';
		$config['base_url']    = base_url('seller/generalSearch');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'SellerPagination';

		// Initialize pagination library 
		$this->ajax_pagination->initialize($config);

		// Get records 
		$this->view_data['records'] = $this->M_seller->fields(array('first_name', 'last_name', 'sales_group', 'seller_type'))->with_contact('fields: mobile_no, email')->with_position('fields: type')->limit($this->perPage, 0)->get_all();

		$this->template->build('index', $this->view_data);
	}

	public function index()
	{
		$_db_columns	=	$this->M_seller->get_columns();
		if ($_db_columns) {

			$_columns	=	[];
			foreach ($_db_columns as $key => $_dbclm) {

				$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

				if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

					if (strpos($_name, '_id') !== FALSE) {

						$_column	=	$_name;
						$_columns[$key]['value']	=	$_column;
						$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';
						$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
						$_columns[$key]['label']	=	ucwords(strtolower($_title));
					} elseif (strpos($_name, 'is_') !== FALSE) {

						$_column	=	$_name;
						$_columns[$key]['value']	=	$_column;
						$_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';
						$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
						$_columns[$key]['label']	=	ucwords(strtolower($_title));
					} else {

						$_columns[$key]['value']	=	$_name;
						$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
						$_columns[$key]['label']	=	ucwords(strtolower($_title));
					}
				} else {

					continue;
				}
			}

			$_ccount	=	count($_columns);
			$_cceil	=	ceil(($_ccount / 2));

			$this->view_data['_columns']	= array_chunk($_columns, $_cceil);

			$_documents =	$this->M_seller->get_all();
			if ($_documents) {

				$this->view_data['_total']	=	count($_documents);
			}
		}

		// Get record count 
		// $conditions['returnType'] = 'count'; 
		$this->view_data['totalRec'] = $totalRec = $this->M_seller->count_rows();

		// Pagination configuration 
		$config['target']      = '#sellerContent';
		$config['base_url']    = base_url('seller/generalSearch');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'SellerPagination';

		// Initialize pagination library 
		$this->ajax_pagination->initialize($config);

		// Get records 
		$this->view_data['records'] = $this->M_seller->fields(array('first_name', 'last_name', 'sales_group', 'seller_type'))->with_contact('fields: mobile_no, email')->with_position('fields: type')->limit($this->perPage, 0)->get_all();

		$this->template->build('index', $this->view_data);
	}

	public function generalSearch()
	{

		if ($this->input->is_ajax_request()) {
			$search_val = $this->input->post('keywords');

			$page = $this->input->post('page');

			if (!$page) {
				$offset = 0;
			} else {
				$offset = $page;
			}

			$totalRec = $this->M_seller->count_rows();

			// Pagination configuration 
			$config['target']      = '#sellerContent';
			$config['base_url']    = base_url('seller/generalSearch');
			$config['total_rows']  = $totalRec;
			$config['per_page']    = $this->perPage;
			$config['link_func']   = 'SellerPagination';

			// Initialize pagination library 
			$this->ajax_pagination->initialize($config);

			// Get records 
			$this->view_data['records'] = $this->M_seller->fields(array('first_name', 'last_name', 'sales_group', 'seller_type'))->with_contact('fields: mobile_no, email')->with_position('fields: type')->limit($this->perPage, $offset)->get_all();

			if (!empty($search_val)) {

				unset($config['total_rows']);
				
				$totalRec = $this->M_seller->fields(array('first_name', 'last_name', 'sales_group', 'seller_type'))
					->with_contact('fields: mobile_no, email')->with_position('fields: type')
					->where('last_name', 'like', $search_val, TRUE)
					->where('first_name', 'like', $search_val, TRUE)
					->where('middle_name', 'like', $search_val, TRUE)
					->count_rows();

				// Pagination configuration 
				$config['total_rows']  = $totalRec;

				// Initialize pagination library 
				$this->ajax_pagination->initialize($config);

				$this->view_data['records'] = $this->M_seller->fields(array('first_name', 'last_name', 'sales_group', 'seller_type'))
					->with_contact('fields: mobile_no, email')->with_position('fields: type')
					->where('last_name', 'like', $search_val, TRUE)
					->where('first_name', 'like', $search_val, TRUE)
					->where('middle_name', 'like', $search_val, TRUE)
					->limit($this->perPage, $offset)
					->get_all();
			}


			$this->load->view('seller/_filter', $this->view_data, false);
		}
	}

	public function create()
	{
		$this->load->helper('format');

		// Get Seller Position
		// $this->view_data['position'] = $this->M_seller_position->get_all();

		if ($this->input->post()) {

			$response['status']	= 0;
			$response['msg'] = 'Oops! Please refresh the page and try again.';

			$this->form_validation->set_rules($this->fields);

			if ($this->form_validation->run() === TRUE) {
				$post = $this->input->post();

				$seller_info = $post['info'];
				$seller_contact = $post['contact'];
				$seller_src_info = $post['source_info'];
				$seller_work_exp = $post['work_exp'];
				$seller_acad = $post['academic'];
				$seller_trainings = $post['training'];
				$seller_exam = $post['exam'];

				$sellerPwd = seller_password($seller_info['last_name'], $seller_info['birth_date']);
				$sellerEmail = $seller_contact['email'];

				// Seller Group ID
				$sellerGroupID = ['3'];

				$sellerAccAuth = array(
					'first_name' => $seller_info['first_name'],
					'last_name' => $seller_info['last_name'],
					'active' => 1,
					'created_by' => $this->user->id,
					'created_at' => NOW
				);

				// $SellerUserID = $this->M_user->insert($sellerAccAuth);

				$userID = $this->ion_auth->register($sellerEmail, $sellerPwd, $sellerEmail, $sellerAccAuth, $sellerGroupID);

				if ($userID !== FALSE) {

					$additional = [
						'is_active' => 1,
						'user_id'	=> $userID,
						'created_by' => $this->user->id,
						'created_at' => NOW
					];
					$sellerID = $this->M_seller->insert($seller_info + $additional);

					if ($sellerID !== FALSE) {

						// Insert Seller Contact Info
						$seller_contact['seller_id'] = $sellerID;
						$this->M_seller_contact_info->insert($seller_contact);

						// Insert Seller Source of Info
						$seller_src_info['seller_id'] = $sellerID;
						$this->M_seller_source_info->insert($seller_src_info);

						$refFields = array('ref_name', 'ref_address', 'ref_contact_no');
						$refData = [];
						foreach ($refFields as $field) {
							foreach ($this->input->post($field) as $key => $value) {
								if (!empty($value)) {
									$refData[$key]['seller_id'] = $sellerID;
									$refData[$key][$field] = $value;
								}
							}
						}
						if ($refData) {
							$this->M_seller_ref->insert($refData);
						}

						// Insert Seller Source work Experience
						$seller_work_exp['seller_id'] = $sellerID;
						$this->M_seller_work_exp->insert($seller_work_exp);

						// Insert Seller Academic History
						$acad_items = [];
						foreach ($seller_acad as $key => $educ) {

							if (!empty($educ['level'])) {
								$acad_items['seller_id'] = $sellerID;
								$acad_items['level'] = $educ['level'];
								$acad_items['course'] = $educ['course'];
								$acad_items['school'] = $educ['school'];
								$acad_items['year'] = $educ['year'];
								$acad_items['rating'] = $educ['rating'];
							}

							if (!empty($acad_items)) {
								$this->M_seller_educ->insert($acad_items);
							}
						}

						// Inser Seller Seminars and Trainings
						$training_items = [];
						foreach ($seller_trainings as $key => $training) {

							if (!empty($training['name'])) {
								$training_items['seller_id'] = $sellerID;
								$training_items['name'] = $training['name'];
								$training_items['trainor'] = $training['trainor'];
								$training_items['year'] = $training['year'];
							}

							if (!empty($training_items)) {
								$this->M_seller_training->insert($training_items);
							}
						}

						// Inser Government / Non Government Exams
						$exam_items = [];
						foreach ($seller_exam as $key => $exam) {

							if (!empty($exam['name'])) {
								$exam_items['seller_id'] = $sellerID;
								$exam_items['name'] = $exam['name'];
								$exam_items['date'] = $exam['date'];
								$exam_items['status'] = $exam['status'];
							}

							if (!empty($exam_items)) {
								$this->M_seller_exam->insert($exam_items);
							}
						}

						// Start emailing email and password to seller here



						$response['status']	= 1;
						$response['message'] = 'Seller Account Successfully Created!';
					}
				}
			} else {
				$response['status']	 =	0;
				$response['message']	= 'Oops! Something when wrong please try again.';
			}

			echo json_encode($response);
			exit();
		}

		$this->template->build('create', $this->view_data);
	}

	public function update($id = FALSE)
	{
		if ($id) {

			$this->load->helper('format');


			$this->view_data['info'] = $data = $this->M_seller->with_contact()->with_source()
				->with_reference()->with_work_experience()
				->with_academic()->with_exam()->with_training()
				->get($id);

			// vdebug($data);
			if ($data) {

				$response['status']	= 0;
				$response['msg'] = 'Oops! Please refresh the page and try again.';

				$this->view_data['position'] = $this->M_seller_position->get_all();

				if ($this->input->post()) {

					$post = $this->input->post();

					$seller_info = $post['info'];
					$seller_contact = $post['contact'];
					$seller_src_info = $post['source_info'];
					$seller_work_exp = $post['work_exp'];
					$seller_acad = $post['academic'];
					$seller_trainings = $post['training'];
					$seller_exam = $post['exam'];

					// OLD Password
					$oldPwd = seller_password($data['last_name'], $data['birth_date']);

					// NEW Password
					$newPwd = seller_password($seller_info['last_name'], $seller_info['birth_date']);

					if ($oldPwd !== $newPwd) {
						// Update User Password
						$updatePassword = $this->M_auth->change_password($data['contact']['email'], $oldPwd, $newPwd);
					}

					$oldEmail = $data['contact']['email'];
					$newEmail = $seller_contact['email'];

					if ($oldEmail !== $newEmail) {
						// Update User Email
						$mail = ['email' => $newEmail];
						$updateEmail = (bool) $this->M_user->update($mail, array('id' => $data['user_id']));
					}

					// Update Seller Info

					$additional = [
						'updated_by' => $this->user->id,
						'updated_at' => NOW
					];
					$this->M_seller->update($seller_info + $additional, $data['id']);

					// Update Seller Contact Info
					if (!empty($data['contact'])) {
						$this->M_seller_contact_info->update($seller_contact, array('seller_id' => $data['id']));
					} else {
						$seller_contact['seller_id'] = $data['id'];
						$this->M_seller_contact_info->insert($seller_contact);
					}

					// Update Seller Source of Info
					if (!empty($data['source'])) {
						$this->M_seller_source_info->update($seller_src_info, array('seller_id' => $data['id']));
					} else {
						$seller_src_info['seller_id'] = $data['id'];
						$this->M_seller_source_info->insert($seller_src_info);
					}


					// Delete all Reference of seller and Add again
					$this->M_seller_ref->delete(array('seller_id' => $data['id']));
					// Insert Reference
					$refFields = array('ref_name', 'ref_address', 'ref_contact_no');
					$refData = [];
					foreach ($refFields as $field) {
						foreach ($this->input->post($field) as $key => $value) {
							if (!empty($value)) {
								$refData[$key]['seller_id'] = $data['id'];
								$refData[$key][$field] = $value;
							}
						}
					}
					if ($refData) {
						$this->M_seller_ref->insert($refData);
					}

					// Update Seller Source work Experience
					if (!empty($data['work_experience'])) {
						$this->M_seller_work_exp->update($seller_work_exp, array('seller_id' => $data['id']));
					} else {
						$seller_work_exp['seller_id'] = $data['id'];
						$this->M_seller_work_exp->insert($seller_work_exp);
					}

					// Delete all Academic history of seller and Add again
					$this->M_seller_educ->delete(array('seller_id' => $data['id']));
					// Insert Seller Academic History
					$acad_items = [];
					foreach ($seller_acad as $key => $educ) {

						if (!empty($educ['level'])) {
							$acad_items['seller_id'] = $data['id'];
							$acad_items['level'] = $educ['level'];
							$acad_items['course'] = $educ['course'];
							$acad_items['school'] = $educ['school'];
							$acad_items['year'] = $educ['year'];
							$acad_items['rating'] = $educ['rating'];
						}

						if (!empty($acad_items)) {
							$this->M_seller_educ->insert($acad_items);
						}
					}

					// Delete all Seminars and training of seller and Add again
					$this->M_seller_training->delete(array('seller_id' => $data['id']));
					// Inser Seller Seminars and Trainings
					$training_items = [];
					foreach ($seller_trainings as $key => $training) {

						if (!empty($training['name'])) {
							$training_items['seller_id'] = $data['id'];
							$training_items['name'] = $training['name'];
							$training_items['trainor'] = $training['trainor'];
							$training_items['year'] = $training['year'];
						}

						if (!empty($training_items)) {
							$this->M_seller_training->insert($training_items);
						}
					}

					// Delete all Exam and Add again
					$this->M_seller_exam->delete(array('seller_id' => $data['id']));
					// Inser Government / Non Government Exams
					$exam_items = [];
					foreach ($seller_exam as $key => $exam) {

						if (!empty($exam['name'])) {
							$exam_items['seller_id'] = $data['id'];
							$exam_items['name'] = $exam['name'];
							$exam_items['date'] = $exam['date'];
							$exam_items['status'] = $exam['status'];
						}

						if (!empty($exam_items)) {
							$this->M_seller_exam->insert($exam_items);
						}
					}

					// Start emailing email and password to seller here
					// if($updatePassword OR $updateEmail){
					// 	// Send new email to notify seller to their new email or password
					// }

					$response['status']	= 1;
					$response['message'] = 'Seller Account Successfully Update!';

					echo json_encode($response);
					exit();
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function delete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_seller->get($id);
			if ($list) {

				$deleted = $this->M_seller->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Seller successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function view($id = FALSE)
	{
		$this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

		$this->js_loader->queue([
			'//www.amcharts.com/lib/3/amcharts.js',
			'//www.amcharts.com/lib/3/serial.js',
			'//www.amcharts.com/lib/3/radar.js',
			'//www.amcharts.com/lib/3/pie.js',
			'//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
			'//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
			'//www.amcharts.com/lib/3/plugins/export/export.min.js',
			'//www.amcharts.com/lib/3/themes/light.js'
		]);

		if ($id) {

			$this->view_data['info'] = $this->M_seller->with_contact()->with_source()
				->with_reference()->with_work_experience()
				->with_academic()->with_exam()->with_training()
				->with_position('fields:name, type')
				->get($id);

			if ($this->view_data['info']) {

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function checkMail()
	{
		$response	= FALSE;
		$key = $this->input->post('contact[key]');
		$email = $this->input->post('contact[email]');

		if (!empty($key)) {
			$existing_email = $this->M_user->where('email =', $key)->get();

			if ($email == $existing_email['email']) {
				$response = TRUE;
			} else {
				$check = $this->M_user->where('email', $email)->get();
				// Email doesnt exists / or not yet taken
				if ($check == FALSE) {
					$response = TRUE;
				}
			}
		} else {

			$check = $this->M_user->where('email', $email)->get();
			// Email doesnt exists / or not yet taken
			if ($check == FALSE) {
				$response = TRUE;
			}
		}
		echo json_encode($response);
	}

	public function fetchSellerType()
	{
		if ($this->input->is_ajax_request()) {
			$group_id = $this->input->post('id');
			if ($group_id == 1) {
				echo json_encode(Dropdown::get_static('THRG'));
			} elseif ($group_id == 2) {
				echo json_encode(Dropdown::get_static('SHJDC'));
			}
		}
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];
		$_extra_datas		=	[];
		$_adatas			=	[];

		$_titles[]	=	'#';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		// $sellers	=	$this->M_seller->as_array()->get_all();
		$sellers = $this->M_seller
			->with_contact('fields:present_address,home_address,living_quarters,mobile_no,email,landline')
			->with_source('fields:recruiter, supervisor, net_worth, to_report, to_attend, commission_based, is_member, group')
			->with_reference('fields: ref_name, ref_address, ref_contact_no')
			->with_work_experience('fields: employer, designation, emp_address, salary, emp_contact_no, supervisor, start_date, end_date, reason, to_contact')
			->with_academic('fields: level, course, school, year, rating')
			->with_exam()
			->with_training()
			->with_position('fields:name, type')
			->get_all();

		if ($sellers) {

			foreach ($sellers as $skey => $seller) {

				$_datas[$seller['id']]['#']	=	$_no;

				$_no++;
			}


			$_filename	=	'list_of_sellers' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				$_additional_column	=	$this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

						if ((strpos($_name, 'contact') !== FALSE) or (strpos($_name, 'source') !== FALSE) or (strpos($_name, 'reference') !== FALSE) or (strpos($_name, 'academic') !== FALSE)) {

							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($sellers as $skey => $seller) {

								$_extra_datas[$seller['id']][$_title]	=	isset($seller[$_name]) && $seller[$_name] ? $seller[$_name] : '';
							}
						} elseif ((strpos($_name, 'work_experience') !== FALSE)) {
							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? $_name : '';

							foreach ($sellers as $skey => $seller) {

								$_extra_datas[$seller['id']][$_title]	=	isset($seller[$_name]) && $seller[$_name] ? $seller[$_name] : '';
							}
						} else {

							$_column	=	$_name;

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($sellers as $skey => $seller) {

								if ( $_column === 'sales_group' ) {

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_static('group_type', $seller[$_column], 'view') : '';

								} elseif ( $_column === 'seller_type' ){

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? strtoupper($seller[$_column]) : '';

								} elseif ( $_column === 'birth_date' ){

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] && (strtotime($seller[$_column]) > 0) ? date_format(date_create($seller[$_column]), 'm/d/Y') : '';

								} elseif ( $_column === 'gender' ){

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_static('sex', $seller[$_column], 'view') : '';

								} elseif ( $_column === 'is_active' ){

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_static('bool', $seller[$_column], 'view') : '';

								}else{
									$_datas[$seller['id']][$_title]	=	isset($seller[$_name]) && $seller[$_name] ? $seller[$_name] : '';
								}

							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title	=	ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('List of Sellers');
				$_objSheet->setCellValue('A1', 'LIST OF SELLERS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($sellers as $key => $seller) {

					$seller_id	=	isset($seller['id']) && $seller['id'] ? $seller['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title	=	ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$seller_id][$_xls_columns[$_alpha]]) && $_datas[$seller_id][$_xls_columns[$_alpha]] ? $_datas[$seller_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns	=	$_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							if($_a_column === 'contact') {

								$ad_title	=	'Contact Informations';
							} elseif($_a_column === 'reference') {

								$ad_title	=	'References';
							} elseif($_a_column === 'source') {

								$ad_title	=	'Source of Informations';
							} elseif($_a_column === 'academic'){

								$ad_title	=	'Academic History';
							}else {

								$ad_title = $_a_column;
							}
							
							$a_title	=	ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$seller_id][$_a_column])) {

									foreach ($_extra_datas[$seller_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if($key === 'to_report' OR $key === 'to_attend' OR $key === 'commission_based' OR $key === 'is_member'){
												$xa_value	=	isset($_extra_datas[$seller_id][$_a_column][$key]) && ($_extra_datas[$seller_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$seller_id][$_a_column][$key], 'view') : '';
											}else{
												$xa_value	=	$_extra_datas[$seller_id][$_a_column][$key];
											}

											$xa_titles =	isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$seller_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$seller_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$seller_id][$_a_column])) {

									foreach ($_extra_datas[$seller_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					} 
					
					$_start += 1;

				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'seller');
			}
		} else {

			$this->notify->error('No Record Found', 'seller');
		}
	}
}
