<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seller extends MY_Controller
{

	private $fields = [
		array(
			'field' => 'info[last_name]',
			'label' => 'Last Name',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'info[first_name]',
			'label' => 'First Name',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'info[birth_date]',
			'label' => 'Birth Date',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'info[email]',
			'label' => 'Email',
			'rules' => 'trim|required|is_unique[users.email]'
		),
	];

	public function __construct()
	{
		parent::__construct();

		// Load models
		$this->load->model('Seller_model', 'M_seller');
		$this->load->model('seller/Seller_reference_model', 'M_seller_ref');
		// $this->load->model('seller/Seller_contact_info_model', 'M_seller_contact_info');
		$this->load->model('seller/Seller_source_info_model', 'M_seller_source_info');
		$this->load->model('seller/Seller_work_experience_model', 'M_seller_work_exp');
		$this->load->model('seller/Seller_education_model', 'M_seller_educ');
		$this->load->model('seller/Seller_training_model', 'M_seller_training');
		$this->load->model('seller/Seller_exam_model', 'M_seller_exam');
		$this->load->model('position/Seller_position_model', 'M_seller_position');
		$this->load->model('seller_teams/Seller_teams_model', 'M_seller_team');
		$this->load->model('auth/Ion_auth_model', 'M_auth');
		$this->load->model('user/User_model', 'M_user');
		$this->load->model('locations/Address_regions_model', 'M_region');
		$this->load->model('locations/Address_provinces_model', 'M_province');
		$this->load->model('locations/Address_city_municipalities_model', 'M_city');
		$this->load->model('locations/Address_barangays_model', 'M_barangay');
		$this->load->model('transaction/Transaction_model', 'M_transaction');
		$this->load->model('communication/Communication_model', 'M_coms');
		$this->load->model('aris/Aris_model', 'M_aris');
		$this->load->model('aris/Aris_sales_agent_model', 'M_aris_sales_agent');

		// Load pagination library 
		$this->load->library('ajax_pagination');

		// Format Helper
		$this->load->helper(['format', 'images']); // Load Helper

		// Per page limit 
		$this->perPage = 12;

		$this->_table_fillables		=	$this->M_seller->fillable;
		$this->_table_columns		=	$this->M_seller->__get_columns();
		$this->_table = 'sellers';
		$this->view_data['regions'] = $this->M_region->as_array()->get_all();
		$this->view_data['provinces'] = $this->M_province->as_array()->get_all();
		$this->view_data['cities'] = $this->M_city->as_array()->get_all();
		$this->view_data['barangays'] = $this->M_barangay->as_array()->get_all();
	}

	public function get_all()
	{
		$data['row'] = $this->M_seller->fields(array('id', 'first_name', 'last_name',  'mobile_no', 'email'))->get_all();

		echo json_encode($data);
	}

	public function get_seller_position($id)
	{
		$data = $this->M_seller->where('id', $id)->as_array()->get();

		echo json_encode($data);
	}

	public function index()
	{
		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns']		=	$this->__get_columns($_fills);

		// // Get record count 
		// // $conditions['returnType'] = 'count'; 
		// $this->view_data['totalRec'] = $totalRec = $this->M_seller->count_rows();

		// // Pagination configuration 
		// $config['target']      = '#sellerContent';
		// $config['base_url']    = base_url('seller/paginationData');
		// $config['total_rows']  = $totalRec;
		// $config['per_page']    = $this->perPage;
		// $config['link_func']   = 'SellerPagination';

		// // Initialize pagination library 
		// $this->ajax_pagination->initialize($config);

		// $sales_group_tiles = array();

		// $bg_colors = array(
		// 	'kt-portlet--solid-primary',
		// 	'kt-portlet--solid-warning',
		// 	'kt-portlet--solid-success',
		// 	'kt-portlet--solid-info',
		// 	'kt-portlet--solid-danger'
		// );

		// $bg_key = 0;

		// $seller_groups = Dropdown::get_dynamic('seller_group');

		// foreach ($seller_groups as $key => $value) {
		// 	if ($key) {
		// 		$this->db->select('COUNT(id) as count')->from('sellers')->where('sales_group_id', $key)->where('deleted_at', NULL);

		// 		$query = $this->db->get()->row_array();

		// 		// reset if $bg_key > $bg_colors
		// 		if (count($bg_colors) == $bg_key) {
		// 			$bg_key = 0;
		// 		}

		// 		array_push($sales_group_tiles, array(
		// 			'bg-color' => $bg_colors[$bg_key],
		// 			'sales_group' => $value,
		// 			'count' => $query['count']
		// 		));

		// 		$bg_key++;
		// 	}
		// }

		// $this->view_data['sales_group_tiles'] = $sales_group_tiles;

		// // Get records 
		// $this->view_data['records'] = $this->M_seller->fields(array('id', 'image', 'first_name', 'last_name', 'sales_group_id', 'seller_position_id',  'mobile_no', 'email', 'aris_id'))
		// 	->with_position('fields: name')
		// 	->limit($this->perPage, 0)
		// 	->order_by('id', 'DESC')
		// 	->get_all();

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function showItems()
	{
		$columnsDefault = [
			'id' => true,
			'first_name' => true,
			'seller_position_id' => true,
			'sales_group_id' => true,
			'mobile_no' => true,
			'email' => true,
			'last_login_time' => true,
			'created_by' => true,
			'updated_by' => true
		];
		$draw = $_REQUEST['draw'];
		$start = $_REQUEST['start'];
		$rowperpage = $_REQUEST['length'];
		$columnIndex = $_REQUEST['order'][0]['column'];
		$columnName = $_REQUEST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_REQUEST['order'][0]['dir'];
		$searchValue = $_REQUEST['search']['value'] ?? null;

		$filters = [];
		parse_str($_POST['filter'], $filters);
		$items = [];
		$totalRecordwithFilter = 0;
		$filteredItems = [];

		for ($query_loop = 0; $query_loop < 2; $query_loop++) {

			$query = $this->M_seller
				->with_position('fields:name')
				->with_group('fields:name')
				->order_by($columnName, $columnSortOrder)
				->as_array();

			// General Search
			if ($searchValue) {

				$query->or_where("(id like '%$searchValue%'");
				$query->or_where("first_name like '%$searchValue%'");
				$query->or_where("last_name like '%$searchValue%'");
				$query->or_where("mobile_no like '%$searchValue%'");
				$query->or_where("email like '%$searchValue%'");
				$query->or_where("present_address like '%$searchValue%')");
			}

			$advanceSearchValues = [
				'id' => [
					'data' => $filters['seller_id'] ?? null,
					'operator' => '=',
				],
				'seller_position_id' => [
					'data' => $filters['seller_position_id'] ?? null,
					'operator' => '=',
				],
				'sales_group_id' => [
					'data' => $filters['sales_group_id'] ?? null,
					'operator' => '=',
				],
				'seller_team_id' => [
					'data' => $filters['seller_team_id'] ?? null,
					'operator' => '=',
				],
				'gender' => [
					'data' => $filters['gender_id'] ?? null,
					'operator' => '=',
				],
				'civil_status_id' => [
					'data' =>  $filters['civil_status'] ?? null,
					'operator' => '=',
				],
			];

			// Advance Search
			foreach ($advanceSearchValues as $key => $value) {

				if ($value['data']) {

					$query->where($key, $value['operator'], $value['data']);
				}
			}

			if ($query_loop) {

				$totalRecordwithFilter = $query->count_rows();
			} else {

				$query->limit($rowperpage, $start);
				$items = $query->get_all();

				if (!!$items) {
					// Transform data
					foreach ($items as $key => $value) {
						$latest_login = $this->M_user->get_last_login_time($value['user_id']);
						$items[$key]['first_name'] = get_fname($value) ?? '';
						$items[$key]['seller_position_id'] = isset($value['position']) ? $value['position']['name'] : 'N/A';
						$items[$key]['sales_group_id'] = isset($value['group']) ? $value['group']['name'] : 'N/A';
						$items[$key]['last_login_time'] = $latest_login ? $latest_login : 'No Record Found';

						$items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

						$items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
					}

					foreach ($items as $item) {
						$filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
					}
				}
			}
		}

		$totalRecords = $this->M_seller->count_rows();

		$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"data" => $filteredItems,
			"request" => $_REQUEST
		);

		echo json_encode($response);
		exit();
	}

	public function paginationData()
	{
		if ($this->input->is_ajax_request()) {

			// Input from General Search
			$keyword = $this->input->post('keyword');

			// Input from Advanced Filter
			$name = $this->input->post('name');
			$sellerPosition = $this->input->post('sellerPosition');
			$sellerGroup = $this->input->post('sellerGroup');
			$sellerTeam = $this->input->post('sellerTeam');

			$page = $this->input->post('page');

			if (!$page) {
				$offset = 0;
			} else {
				$offset = $page;
			}

			$totalRec = $this->M_seller->count_rows();

			// Pagination configuration 
			$config['target']      = '#sellerContent';
			$config['base_url']    = base_url('seller/paginationData');
			$config['total_rows']  = $totalRec;
			$config['per_page']    = $this->perPage;
			$config['link_func']   = 'SellerPagination';

			// Query
			if (!empty($keyword)) :

				$this->db->group_start();
				$this->db->like('last_name', $keyword, 'both');
				$this->db->or_like('first_name', $keyword, 'both');
				$this->db->or_like('middle_name', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (!empty($name)) :

				$this->db->group_start();
				$this->db->like('last_name', $name, 'both');
				$this->db->or_like('first_name', $name, 'both');
				$this->db->or_like('middle_name', $name, 'both');
				$this->db->group_end();
			endif;

			if (!empty($sellerPosition)) :

				$this->db->group_start();
				$this->db->like('seller_position_id', $sellerPosition, 'both');
				$this->db->group_end();
			endif;

			if (!empty($sellerGroup) && !empty($sellerPosition)) :

				$this->db->where('sales_group_id', $sellerGroup);
				$this->db->where('seller_position_id', $sellerPosition);
			endif;

			$totalRec = $this->M_seller->count_rows();

			// Pagination configuration 
			$config['total_rows']  = $totalRec;

			// Initialize pagination library 
			$this->ajax_pagination->initialize($config);

			// Query
			if (!empty($keyword)) :

				$this->db->group_start();
				$this->db->like('last_name', $keyword, 'both');
				$this->db->or_like('first_name', $keyword, 'both');
				$this->db->or_like('middle_name', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (!empty($name)) :

				$this->db->group_start();
				$this->db->like('last_name', $name, 'both');
				$this->db->or_like('first_name', $name, 'both');
				$this->db->or_like('middle_name', $name, 'both');
				$this->db->group_end();
			endif;

			if (!empty($sellerPosition)) :

				$this->db->group_start();
				$this->db->like('seller_position_id', $sellerPosition, 'both');
				$this->db->group_end();
			endif;

			if (!empty($sellerGroup) && !empty($sellerPosition)) :

				$this->db->where('sales_group_id', $sellerGroup);
				$this->db->where('seller_position_id', $sellerPosition);
			endif;

			$this->view_data['records'] = $records = $this->M_seller->fields(array('id', 'image', 'first_name', 'last_name', 'sales_group_id', 'seller_position_id',  'mobile_no', 'email', 'aris_id'))
				->with_position('fields: name')
				->limit($this->perPage, $offset)
				->get_all();


			$this->load->view('seller/_filter', $this->view_data, false);
		}
	}

	public function view($id = FALSE, $debug = FALSE)
	{
		$this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

		$this->js_loader->queue([
			'//www.amcharts.com/lib/3/amcharts.js',
			'//www.amcharts.com/lib/3/serial.js',
			'//www.amcharts.com/lib/3/radar.js',
			'//www.amcharts.com/lib/3/pie.js',
			'//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
			'//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
			'//www.amcharts.com/lib/3/plugins/export/export.min.js',
			'//www.amcharts.com/lib/3/themes/light.js'
		]);

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		if ($id) {

			$this->view_data['info'] = $info = $this->M_seller
				->with_source()
				->with_region()
				->with_province()
				->with_city()
				->with_barangay()
				->with_reference()
				->with_work_experience()
				->with_academic()
				->with_exam()
				->with_training()
				->with_position('fields: name')
				->get($id);

			$this->view_data['audit'] = $this->__get_audit($id);

			$this->view_data['sales_logs'] = $this->M_transaction->with_seller()->with_property()->where("seller_id", $id)->get_all();
			$this->view_data['sales_agent'] = $agent = $this->M_aris_sales_agent->where('id', $info['aris_id'])->get();
			$this->view_data['aris_project'] = $this->M_aris->where('id', $agent['project_id'])->get();

			$coms_params['recipient_id'] = $id;
			$coms_params["recipient_table"] = "seller";

			$communications = $this->M_coms->with_seller()->where($coms_params)->get_all();

			$emails = array();
			$sms = array();

			if ($communications) {
				foreach ($communications as $key => $comm) {
					if ($comm["medium_type"] == "sms") {
						array_push($sms, $comm);
					} else if ($comm["medium_type"] == "email") {
						array_push($emails, $comm);
					}
				}
			}

			$this->view_data['emails'] = $emails;
			$this->view_data['sms'] = $sms;

			if ($this->view_data['info']) {

				if ($debug) {
					vdebug($this->view_data['info']);
				}

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function aris($id = FALSE, $debug = FALSE)
	{
		$this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

		$this->js_loader->queue([
			'//www.amcharts.com/lib/3/amcharts.js',
			'//www.amcharts.com/lib/3/serial.js',
			'//www.amcharts.com/lib/3/radar.js',
			'//www.amcharts.com/lib/3/pie.js',
			'//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
			'//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
			'//www.amcharts.com/lib/3/plugins/export/export.min.js',
			'//www.amcharts.com/lib/3/themes/light.js'
		]);

		if ($id) {

			$this->view_data['info'] = $info = $this->M_seller
				->with_source()
				->with_region()
				->with_province()
				->with_city()
				->with_barangay()
				->with_reference()
				->with_work_experience()
				->with_academic()
				->with_exam()
				->with_training()
				->with_position('fields: name')
				->where('aris_id', $id)
				->get();

			$this->view_data['audit'] = $this->__get_audit($info['id']);

			$this->view_data['sales_logs'] = $this->M_transaction->with_seller()->with_property()->where("seller_id", $info['id'])->get_all();
			$this->view_data['sales_agent'] = $agent = $this->M_aris_sales_agent->where('id', $id)->get();
			$this->view_data['aris_project'] = $this->M_aris->where('id', $agent['project_id'])->get();

			$coms_params['recipient_id'] = $info['id'];
			$coms_params["recipient_table"] = "seller";

			$communications = $this->M_coms->with_seller()->where($coms_params)->get_all();

			$emails = array();
			$sms = array();

			if ($communications) {
				foreach ($communications as $key => $comm) {
					if ($comm["medium_type"] == "sms") {
						array_push($sms, $comm);
					} else if ($comm["medium_type"] == "email") {
						array_push($emails, $comm);
					}
				}
			}

			$this->view_data['emails'] = $emails;
			$this->view_data['sms'] = $sms;

			if ($this->view_data['info']) {

				if ($debug) {
					vdebug($this->view_data['info']);
				}

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function create()
	{
		if ($this->input->post()) {

			$response['status']	= 0;
			$response['msg'] = 'Oops! Please refresh the page and try again.';

			$this->form_validation->set_rules($this->fields);

			if ($this->form_validation->run() === TRUE) {
				$post = $this->input->post();

				$seller_info = $post['info'];
				$seller_src_info = $post['source_info'];
				$seller_work_exp = $post['work_exp'];
				$seller_acad = $post['academic'];
				$seller_trainings = $post['training'];
				$seller_exam = $post['exam'];

				if (!$this->M_auth->email_check($seller_info['email'])) {

					$sellerPwd = password_format($seller_info['last_name'], $seller_info['birth_date']);
					$sellerEmail = $seller_info['email'];

					// Seller Group ID
					$sellerGroupID = ['3'];

					$sellerAccAuth = array(
						'first_name' => $seller_info['first_name'],
						'last_name' => $seller_info['last_name'],
						'active' => 1,
						'created_by' => $this->user->id,
						'created_at' => NOW
					);

					$userID = $this->ion_auth->register($sellerEmail, $sellerPwd, $sellerEmail, $sellerAccAuth, $sellerGroupID);

					if ($userID !== FALSE) {


						// Image Upload
						$upload_path = './assets/uploads/seller/images';
						$config = array();
						$config['upload_path'] = $upload_path;
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size'] = 5000;
						$config['encrypt_name'] = TRUE;
						$this->load->library('upload', $config, 'seller_image');
						$this->seller_image->initialize($config);

						if (!empty($_FILES['seller_image']['name'])) {

							if (!$this->seller_image->do_upload('seller_image')) {

								$this->notify->error($this->seller_image->display_errors(), 'seller/create');
							} else {

								$img_data = $this->seller_image->data();

								$seller_info['image'] = $img_data['file_name'];
							}
						}

						$additional = [
							'is_active' => 1,
							'user_id'	=> $userID,
							'created_by' => $this->user->id,
							'created_at' => NOW
						];
						$sellerID = $this->M_seller->insert($seller_info + $additional);

						if ($sellerID !== FALSE) {

							// Insert Seller Source of Info
							$seller_src_info['seller_id'] = $sellerID;
							$this->M_seller_source_info->insert($seller_src_info);

							$refFields = array('ref_name', 'ref_address', 'ref_contact_no');
							$refData = [];
							foreach ($refFields as $field) {
								foreach ($this->input->post($field) as $key => $value) {
									if (!empty($value)) {
										$refData[$key]['seller_id'] = $sellerID;
										$refData[$key][$field] = $value;
									}
								}
							}
							if ($refData) {
								$this->M_seller_ref->insert($refData);
							}

							// Insert Seller Source work Experience
							$seller_work_exp['seller_id'] = $sellerID;
							$this->M_seller_work_exp->insert($seller_work_exp);

							// Insert Seller Academic History
							$acad_items = [];
							foreach ($seller_acad as $key => $educ) {

								if (!empty($educ['level'])) {
									$acad_items['seller_id'] = $sellerID;
									$acad_items['level'] = $educ['level'];
									$acad_items['course'] = $educ['course'];
									$acad_items['school'] = $educ['school'];
									$acad_items['year'] = $educ['year'];
									$acad_items['rating'] = $educ['rating'];
								}

								if (!empty($acad_items)) {
									$this->M_seller_educ->insert($acad_items);
								}
							}

							// Inser Seller Seminars and Trainings
							$training_items = [];
							foreach ($seller_trainings as $key => $training) {

								if (!empty($training['name'])) {
									$training_items['seller_id'] = $sellerID;
									$training_items['name'] = $training['name'];
									$training_items['trainor'] = $training['trainor'];
									$training_items['year'] = $training['year'];
								}

								if (!empty($training_items)) {
									$this->M_seller_training->insert($training_items);
								}
							}

							// Inser Government / Non Government Exams
							$exam_items = [];
							foreach ($seller_exam as $key => $exam) {

								if (!empty($exam['name'])) {
									$exam_items['seller_id'] = $sellerID;
									$exam_items['name'] = $exam['name'];
									$exam_items['date'] = $exam['date'];
									$exam_items['status'] = $exam['status'];
								}

								if (!empty($exam_items)) {
									$this->M_seller_exam->insert($exam_items);
								}
							}

							// Start emailing email and password to seller here

							$response['status']	= 1;
							$response['message'] = 'Seller Account Successfully Created!';
						}
					}
				} else {
					$response['status']	 =	0;
					$response['message']	= 'Oops! Email Address already exists.';
				}
			} else {
				$response['status']	 =	0;
				$response['message']	= 'Oops! Something when wrong please try again.';
			}

			echo json_encode($response);
			exit();
		}

		$this->template->build('create', $this->view_data);
	}

	public function update($id = FALSE)
	{
		if ($id) {

			$this->view_data['info'] = $data = $this->M_seller
				->with_source()
				->with_reference()
				->with_work_experience()
				->with_academic()
				->with_exam()
				->with_training()
				->with_position('fields: id, name')
				->with_team('fields: id, name')
				->get($id);

			// vdebug($data);
			if ($data) {

				$response['status']	= 0;
				$response['msg'] = 'Oops! Please refresh the page and try again.';

				$this->view_data['position'] = $this->M_seller_position->get_all();

				if ($this->input->post()) {

					$post = $this->input->post();

					$seller_info = $post['info'];
					$seller_src_info = $post['source_info'];
					$seller_work_exp = $post['work_exp'];
					$seller_acad = $post['academic'];
					$seller_trainings = $post['training'];
					$seller_exam = $post['exam'];

					if ($this->M_auth->email_check($data['email'])) {

						$oldPwd = password_format($data['last_name'], $data['birth_date']);
						$newPwd = password_format($seller_info['last_name'], $seller_info['birth_date']);

						if ($oldPwd !== $newPwd) {
							// Update User Password
							$this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
						}

						$oldEmail = $data['email'];
						$newEmail = $seller_info['email'];

						if ($oldEmail !== $newEmail) {
							// Update User Email
							$_user_data = [
								'email' => $newEmail,
								'username' => $newEmail,
								'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
								'updated_at' => NOW
							];
							$this->M_user->update($_user_data, array('id' => $data['user_id']));
						}

						// Update Seller Info
						$additional = [
							'updated_by' => $this->user->id,
							'updated_at' => NOW
						];

						$upload_path = './assets/uploads/seller/images';
						$config = array();
						$config['upload_path'] = $upload_path;
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size'] = 5000;
						$config['encrypt_name'] = TRUE;
						$this->load->library('upload', $config, 'seller_image');
						$this->seller_image->initialize($config);

						if (!empty($_FILES['seller_image']['name'])) {
							if (!$this->seller_image->do_upload('seller_image')) {

								$this->notify->error($this->seller_image->display_errors(), 'seller/update');
							} else {

								if (file_exists($upload_path . '/' . $data['image'])) {

									unlink($upload_path . '/' . $data['image']);
								}

								$img_data = $this->seller_image->data();

								$seller_info['image'] = $img_data['file_name'];
							}
						} else {

							$seller_info['image'] = $data['image'];
						}

						$update = $this->M_seller->update($seller_info + $additional, $data['id']);

						if ($update !== FALSE) {
							$user_data = array(
								'first_name' => $seller_info['first_name'],
								'last_name' => $seller_info['last_name'],
								'active' => $data['is_active'],
								'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
								'updated_at' => NOW
							);

							$this->M_user->update($user_data, $data['user_id']);

							// Update Seller Source of Info
							if (!empty($data['source'])) {
								$this->M_seller_source_info->update($seller_src_info, array('seller_id' => $data['id']));
							} else {
								$seller_src_info['seller_id'] = $data['id'];
								$this->M_seller_source_info->insert($seller_src_info);
							}

							// Delete all Reference of seller and Add again
							$this->M_seller_ref->delete(array('seller_id' => $data['id']));

							// Insert Reference
							$refFields = array('ref_name', 'ref_address', 'ref_contact_no');
							$refData = [];
							foreach ($refFields as $field) {
								foreach ($this->input->post($field) as $key => $value) {
									if (!empty($value)) {
										$refData[$key]['seller_id'] = $data['id'];
										$refData[$key][$field] = $value;
									}
								}
							}
							if ($refData) {
								$this->M_seller_ref->insert($refData);
							}

							// Update Seller Source work Experience
							if (!empty($data['work_experience'])) {
								$this->M_seller_work_exp->update($seller_work_exp, array('seller_id' => $data['id']));
							} else {
								$seller_work_exp['seller_id'] = $data['id'];
								$this->M_seller_work_exp->insert($seller_work_exp);
							}

							// Delete all Academic history of seller and Add again
							$this->M_seller_educ->delete(array('seller_id' => $data['id']));
							// Insert Seller Academic History
							$acad_items = [];
							foreach ($seller_acad as $key => $educ) {

								if (!empty($educ['level'])) {
									$acad_items['seller_id'] = $data['id'];
									$acad_items['level'] = $educ['level'];
									$acad_items['course'] = $educ['course'];
									$acad_items['school'] = $educ['school'];
									$acad_items['year'] = $educ['year'];
									$acad_items['rating'] = $educ['rating'];
								}

								if (!empty($acad_items)) {
									$this->M_seller_educ->insert($acad_items);
								}
							}

							// Delete all Seminars and training of seller and Add again
							$this->M_seller_training->delete(array('seller_id' => $data['id']));
							// Inser Seller Seminars and Trainings
							$training_items = [];
							foreach ($seller_trainings as $key => $training) {

								if (!empty($training['name'])) {
									$training_items['seller_id'] = $data['id'];
									$training_items['name'] = $training['name'];
									$training_items['trainor'] = $training['trainor'];
									$training_items['year'] = $training['year'];
								}

								if (!empty($training_items)) {
									$this->M_seller_training->insert($training_items);
								}
							}

							// Delete all Exam and Add again
							$this->M_seller_exam->delete(array('seller_id' => $data['id']));
							// Inser Government / Non Government Exams
							$exam_items = [];
							foreach ($seller_exam as $key => $exam) {

								if (!empty($exam['name'])) {
									$exam_items['seller_id'] = $data['id'];
									$exam_items['name'] = $exam['name'];
									$exam_items['date'] = $exam['date'];
									$exam_items['status'] = $exam['status'];
								}

								if (!empty($exam_items)) {
									$this->M_seller_exam->insert($exam_items);
								}
							}

							// Start emailing email and password to seller here


							$response['status']	= 1;
							$response['message'] = 'Seller Account Successfully Update!';
						} else {

							$response['status']	= 0;
							$response['message'] = 'Oops! Something went wrong. Please refresh the page and try again.';
						}
					} else {

						$response['status']	= 0;
						$response['message'] = 'Oops! Email Address already exists.';
					}

					echo json_encode($response);
					exit();
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function delete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_seller->get($id);
			if ($list) {

				$deleted = $this->M_seller->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Seller successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function bulkDelete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_seller->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Seller successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	function checkMail()
	{
		if ($this->input->is_ajax_request()) {

			$response	= FALSE;
			$key = $this->input->post('info[key]');
			$email = $this->input->post('info[email]');

			if (!empty($key)) {
				$existing_email = $this->M_user->where('email =', $key)->get();

				if ($email == $existing_email['email']) {
					$response = TRUE;
				} else {
					$check = $this->M_user->where('email', $email)->get();
					// Email doesnt exists / or not yet taken
					if ($check == FALSE) {
						$response = TRUE;
					}
				}
			} else {

				$check = $this->M_user->where('email', $email)->get();
				// Email doesnt exists / or not yet taken
				if ($check == FALSE) {
					$response = TRUE;
				}
			}
			echo json_encode($response);
		} else {

			show_404();
		}
	}

	function fetchSellerPositions()
	{
		if ($this->input->is_ajax_request()) {
			$items	=	[];
			$sales_group_id = $this->input->post('id');

			$datas = $this->M_seller_position->fields('id, name')->where('sales_group_id', $sales_group_id)->as_object()->get_all();

			if ($datas) {
				foreach ($datas as $key => $data) {

					$items[$data->id]['id'] = $data->id;
					$items[$data->id]['name'] = $data->name;
				}

				echo json_encode($items);
				exit();
			}
		} else {

			show_404();
		}
	}

	function fetchSellerTeams()
	{
		if ($this->input->is_ajax_request()) {
			$items	=	[];
			$sales_group_id = $this->input->post('id');

			$datas = $this->M_seller_team->fields('id, name')->where('seller_group_id', $sales_group_id)->as_object()->get_all();

			if ($datas) {
				foreach ($datas as $key => $data) {

					$items[$data->id]['id'] = $data->id;
					$items[$data->id]['name'] = $data->name;
				}

				echo json_encode($items);
				exit();
			}
		} else {

			show_404();
		}
	}

	function export()
	{
		extract($this->input->post());
		$order = explode(',', $order);
		$_db_columns = [];
		$_alphas = [];
		$_datas = [];
		$_extra_datas = [];
		$_adatas = [];

		$_titles[] = '#';

		$_start = 3;
		$_row = 2;
		$_no = 1;

		if ($seller_id) {
			$this->db->where('id', $seller_id);
		}

		if ($seller_position_id) {
			$this->db->where('seller_position_id', $seller_position_id);
		}

		if ($sales_group_id) {
			$this->db->where('sales_group_id', $sales_group_id);
		}

		if ($seller_team_id) {
			$this->db->where('seller_team_id', $seller_team_id);
		}

		if ($gender_id) {
			$this->db->where('gender', $gender_id);
		}

		if ($civil_status) {
			$this->db->where('civil_status_id', $civil_status);
		}

		if ($order) {
			$this->db->order_by($order[0], $order[1]);
		}

		$sellers = $this->M_seller
			->with_source('fields:recruiter, supervisor, net_worth, to_report, to_attend, commission_based, is_member, group')
			->with_reference('fields: ref_name, ref_address, ref_contact_no')
			->with_work_experience('fields: employer, designation, emp_address, salary, emp_contact_no, supervisor, start_date, end_date, reason, to_contact')
			->with_academic('fields: level, course, school, year, rating')
			->with_exam()
			->with_training()
			->with_position('fields:name')
			->get_all();

		if ($sellers) {

			foreach ($sellers as $skey => $seller) {

				$_datas[$seller['id']]['#']	=	$_no;

				$_no++;
			}


			$_filename = 'list_of_sellers' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style = array(
				'font' => array(
					'bold' => TRUE,
					'size' => 10,
					'name' => 'Verdana'
				)
			);

			$_objSheet = $this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column = $this->input->post('_export_column');
				$_additional_column = $this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

						if ((strpos($_name, 'source') !== FALSE) or (strpos($_name, 'reference') !== FALSE) or (strpos($_name, 'academic') !== FALSE)) {

							$_extra_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($sellers as $skey => $seller) {

								$_extra_datas[$seller['id']][$_title] = isset($seller[$_name]) && $seller[$_name] ? $seller[$_name] : '';
							}
						} elseif ((strpos($_name, 'work_experience') !== FALSE)) {
							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? $_name : '';

							foreach ($sellers as $skey => $seller) {

								$_extra_datas[$seller['id']][$_title]	=	isset($seller[$_name]) && $seller[$_name] ? $seller[$_name] : '';
							}
						} else {

							$_column = $_name;

							$_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[] = $_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($sellers as $skey => $seller) {

								if ($_column === 'sales_group_id') {

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_static('group_type', $seller[$_column], 'view') : '';
								} elseif ($_column === 'seller_position_id') {

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_dynamic('seller_positions', $seller[$_column], 'name', 'id', 'view') : '';
								} elseif ($_column === 'birth_date') {

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] && (strtotime($seller[$_column]) > 0) ? date_format(date_create($seller[$_column]), 'm/d/Y') : '';
								} elseif ($_column === 'gender') {

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_static('sex', $seller[$_column], 'view') : '';
								} else if ($_column === 'regCode') {

									$_datas[$seller['id']][$_title] = isset($seller[$_column]) && $seller[$_column] ? get_value_field($seller[$_column], 'address_regions', 'regDesc', 'regCode') : '';
								} else if ($_column === 'provCode') {

									$_datas[$seller['id']][$_title] = isset($seller[$_column]) && $seller[$_column] ? get_value_field($seller[$_column], 'address_provinces', 'provDesc', 'provCode') : '';
								} else if ($_column === 'citymunCode') {

									$_datas[$seller['id']][$_title] = isset($seller[$_column]) && $seller[$_column] ? get_value_field($seller[$_column], 'address_city_municipalities', 'citymunDesc', 'citymunCode') : '';
								} else if ($_column === 'brgyCode') {

									$_datas[$seller['id']][$_title] = isset($seller[$_column]) && $seller[$_column] ? get_value_field($seller[$_column], 'address_barangays', 'brgyDesc', 'brgyCode') : '';
								} elseif ($_column === 'is_active') {

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_static('bool', $seller[$_column], 'view') : '';
								} elseif ($_column === 'is_exported') {

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_static('bool', $seller[$_column], 'view') : '';
								} elseif ($_column === 'civil_status') {

									$_datas[$seller['id']][$_title]	=	isset($seller[$_column]) && $seller[$_column] ? Dropdown::get_static('civil_status', $seller[$_column], 'view') : '';
								} elseif ($_column === 'seller_team_id') {

									$_datas[$seller['id']][$_title] = isset($seller[$_column]) && $seller[$_column] ? get_value_field($seller[$_column], 'seller_teams', 'name') : '';
								} else {
									$_datas[$seller['id']][$_title]	=	isset($seller[$_name]) && $seller[$_name] ? $seller[$_name] : '';
								}
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title	=	ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('List of Sellers');
				$_objSheet->setCellValue('A1', 'LIST OF SELLERS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($sellers as $key => $seller) {

					$seller_id	=	isset($seller['id']) && $seller['id'] ? $seller['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title	=	ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$seller_id][$_xls_columns[$_alpha]]) && $_datas[$seller_id][$_xls_columns[$_alpha]] ? $_datas[$seller_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns	=	$_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							if ($_a_column === 'contact') {

								$ad_title	=	'Contact Informations';
							} elseif ($_a_column === 'reference') {

								$ad_title	=	'References';
							} elseif ($_a_column === 'source') {

								$ad_title	=	'Source of Informations';
							} elseif ($_a_column === 'academic') {

								$ad_title	=	'Academic History';
							} else {

								$ad_title = $_a_column;
							}

							$a_title	=	ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$seller_id][$_a_column])) {

									foreach ($_extra_datas[$seller_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if ($key === 'to_report' or $key === 'to_attend' or $key === 'commission_based' or $key === 'is_member') {
												$xa_value	=	isset($_extra_datas[$seller_id][$_a_column][$key]) && ($_extra_datas[$seller_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$seller_id][$_a_column][$key], 'view') : '';
											} else {
												$xa_value	=	$_extra_datas[$seller_id][$_a_column][$key];
											}

											$xa_titles =	isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$seller_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$seller_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$seller_id][$_a_column])) {

									foreach ($_extra_datas[$seller_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					}

					$_start += 1;
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'seller');
			}
		} else {

			$this->notify->error('No Record Found', 'seller');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Seller CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'seller');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_seller->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Sellers');
			$_objSheet->setCellValue('A1', 'SELLERS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_failed_reasons = [];
				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'seller');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {
							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							$_data['birth_date']	=	isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
							$sellerGroupID = ['3'];

							if ($_id) {
								$data	=	$this->M_seller->get($_id);
								if ($data) {

									unset($_data['id']);

									$oldPwd = password_format($data['last_name'], $data['birth_date']);
									$newPwd = password_format($_data['last_name'], $_data['birth_date']);

									$oldEmail = $data['email'];
									$newEmail = $_data['email'];

									if ($this->M_auth->email_check($oldEmail)) {

										if ($oldPwd !== $newPwd) {
											// Update User Password
											$this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
										}

										if ($oldEmail !== $newEmail) {
											// Update User Email
											$_user_data = [
												'email' => $newEmail,
												'username' => $newEmail,
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											];
											$this->M_user->update($_user_data, array('id' => $data['user_id']));
										}

										// Update Seller Info
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_update	=	$this->M_seller->update($_data, $_id);
										if ($_update !== FALSE) {

											$user_data = array(
												'first_name' => $_data['first_name'],
												'last_name' => $_data['last_name'],
												'active' => $_data['is_active'],
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											);

											$this->M_user->update($user_data, $data['user_id']);

											$_updated++;
										} else {

											$_failed_reasons[$_data['id']][] = 'update not working.';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email already used';
										$_failed++;

										break;
									}
								} else {

									// Generate user password
									$sellerPwd = password_format($_data['last_name'], $_data['birth_date']);
									$sellerEmail = $_data['email'];

									if (!$this->M_auth->email_check($_data['email'])) {

										$user_data = array(
											'first_name' => $_data['first_name'],
											'last_name' => $_data['last_name'],
											'active' => $_data['is_active'],
											'created_by' => $this->user->id,
											'created_at' => NOW
										);

										$userID = $this->ion_auth->register($sellerEmail, $sellerPwd, $sellerEmail, $user_data, $sellerGroupID);

										if ($userID !== FALSE) {

											$_data['user_id']	= $userID;
											$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['created_at']	=	NOW;
											$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['updated_at']	=	NOW;

											$_insert	=	$this->M_seller->insert($_data);
											if ($_insert !== FALSE) {

												$_inserted++;
											} else {

												$_failed_reasons[$_data['id']][] = 'insert seller not working';
												$_failed++;

												break;
											}
										} else {

											$_failed_reasons[$_data['id']][] = 'insert ion auth not working';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email check already existing';
										$_failed++;

										break;
									}
								}
							} else {

								// Generate user password
								$sellerPwd = password_format($_data['last_name'], $_data['birth_date']);
								$sellerEmail = $_data['email'];

								if (!$this->M_auth->email_check($_data['email'])) {

									$user_data = array(
										'first_name' => $_data['first_name'],
										'last_name' => $_data['last_name'],
										'active' => $_data['is_active'],
										'created_by' => $this->user->id,
										'created_at' => NOW
									);

									$userID = $this->ion_auth->register($sellerEmail, $sellerPwd, $sellerEmail, $user_data, $sellerGroupID);

									if ($userID !== FALSE) {

										$_data['user_id']	= $userID;
										$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['created_at']	=	NOW;
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_insert	=	$this->M_seller->insert($_data);
										if ($_insert !== FALSE) {

											$_inserted++;
										} else {

											$_failed_reasons[$_dkey][] = 'insert seller not working';

											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_dkey][] = 'insert ion auth not working';

										$_failed++;

										break;
									}
								} else {

									$_failed_reasons[$_dkey][] = 'email check already existing';

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {
							$this->notify->error('Upload Failed! Please follow upload guide. ', 'seller');
						} else {

							$this->notify->success($_msg . '.', 'seller');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'seller');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'seller');
			}
		} else {

			$this->notify->error('Something went wrong!', 'seller');
		}
	}

	public function names_to_uppercase_words()
	{
		$this->db->query("UPDATE sellers SET first_name=CONCAT(UPPER(LEFT(first_name, 1)), LOWER(SUBSTRING(first_name, 2))), middle_name=CONCAT(UPPER(LEFT(middle_name, 1)), LOWER(SUBSTRING(middle_name, 2))), last_name=CONCAT(UPPER(LEFT(last_name, 1)), LOWER(SUBSTRING(last_name, 2)))");

		$next_word_queries = [
			"UPDATE sellers SET first_name = REPLACE(first_name,' a',' A'), middle_name = REPLACE(middle_name,' a',' A'), last_name = REPLACE(last_name,' a',' A')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' b',' B'), middle_name = REPLACE(middle_name,' b',' B'), last_name = REPLACE(last_name,' b',' B')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' c',' C'), middle_name = REPLACE(middle_name,' c',' C'), last_name = REPLACE(last_name,' c',' C')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' d',' D'), middle_name = REPLACE(middle_name,' d',' D'), last_name = REPLACE(last_name,' d',' D')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' e',' E'), middle_name = REPLACE(middle_name,' e',' E'), last_name = REPLACE(last_name,' e',' E')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' f',' F'), middle_name = REPLACE(middle_name,' f',' F'), last_name = REPLACE(last_name,' f',' F')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' g',' G'), middle_name = REPLACE(middle_name,' g',' G'), last_name = REPLACE(last_name,' g',' G')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' h',' H'), middle_name = REPLACE(middle_name,' h',' H'), last_name = REPLACE(last_name,' h',' H')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' i',' I'), middle_name = REPLACE(middle_name,' i',' I'), last_name = REPLACE(last_name,' i',' I')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' j',' J'), middle_name = REPLACE(middle_name,' j',' J'), last_name = REPLACE(last_name,' j',' J')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' k',' K'), middle_name = REPLACE(middle_name,' k',' K'), last_name = REPLACE(last_name,' k',' K')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' l',' L'), middle_name = REPLACE(middle_name,' l',' L'), last_name = REPLACE(last_name,' l',' L')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' m',' M'), middle_name = REPLACE(middle_name,' m',' M'), last_name = REPLACE(last_name,' m',' M')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' n',' N'), middle_name = REPLACE(middle_name,' n',' N'), last_name = REPLACE(last_name,' n',' N')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' o',' O'), middle_name = REPLACE(middle_name,' o',' O'), last_name = REPLACE(last_name,' o',' O')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' p',' P'), middle_name = REPLACE(middle_name,' p',' P'), last_name = REPLACE(last_name,' p',' P')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' q',' Q'), middle_name = REPLACE(middle_name,' q',' Q'), last_name = REPLACE(last_name,' q',' Q')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' r',' R'), middle_name = REPLACE(middle_name,' r',' R'), last_name = REPLACE(last_name,' r',' R')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' s',' S'), middle_name = REPLACE(middle_name,' s',' S'), last_name = REPLACE(last_name,' s',' S')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' t',' T'), middle_name = REPLACE(middle_name,' t',' T'), last_name = REPLACE(last_name,' t',' T')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' u',' U'), middle_name = REPLACE(middle_name,' u',' U'), last_name = REPLACE(last_name,' u',' U')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' v',' V'), middle_name = REPLACE(middle_name,' v',' V'), last_name = REPLACE(last_name,' v',' V')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' w',' W'), middle_name = REPLACE(middle_name,' w',' W'), last_name = REPLACE(last_name,' w',' W')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' x',' X'), middle_name = REPLACE(middle_name,' x',' X'), last_name = REPLACE(last_name,' x',' X')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' y',' Y'), middle_name = REPLACE(middle_name,' y',' Y'), last_name = REPLACE(last_name,' y',' Y')",
			"UPDATE sellers SET first_name = REPLACE(first_name,' z',' Z'), middle_name = REPLACE(middle_name,' z',' Z'), last_name = REPLACE(last_name,' z',' Z')",
		];

		foreach ($next_word_queries as $query) {
			$this->db->query($query);
		}
	}

	public function update_password($id, $mode = 0)
	{
		if ($id) {
			$results['status'] = 0;
			$results['message'] = 'Password not updated.';
			$additional['updated_by'] = $this->user->id;
			$additional['updated_at'] = NOW;
			$seller = $this->M_seller->get($id);
			$user = $this->M_user->get($seller['user_id']);
			$s_email = str_replace(' ', '', $seller['email']);
			$u_email = str_replace(' ', '', $user['email']);
			if ($s_email != $u_email) {
				$email = $u_email;
			} else {
				$email = $s_email;
			}
			$email = str_replace('..', '.', $email);
			$last_name = strtolower($seller['last_name']);
			$first_name = strtolower($seller['first_name']);
			$birthday = $seller['birth_date'];
			if ($birthday == '0000-00-00') {
				$password = password_format($last_name, $first_name, 1);
			} else {
				$password = password_format($last_name, $birthday);
			}
			if ($mode) {
				$info['username'] = $email;
				$info['password'] = $password;
				return $info;
			}
			$info['password'] = $this->M_auth->hash_password($password);
			$info['email'] = $email;
			$info['username'] = $email;
			$seller_update = $this->M_seller->update(['email' => $email] + $additional, $seller['id']);
			$user_update = $this->M_user->update($info + $additional, $seller['user_id']);
			if ($seller_update && $user_update) {
				$results['status'] = 1;
				$results['message'] = 'Password Successfully changed!';
			}
			echo json_encode($results);
			exit();
		}
	}

	public function update_all_passwords()
	{
		$results = [];
		if ($this->user->id == 6267) {
			$this->db->where('id > 2');
			$buyers = $this->M_seller->fields('id')->get_all();
			for ($i = 0; $i < count($buyers); $i++) {
				$result = $this->check_duplicates($buyers[$i]['id']);
				// $result = $this->update_password($buyers[$i]['id']);
				$results[] = $result;
			}
			vdebug(array_filter($results));
		}
		show_403('You have no permission to access this page.');
	}

	public function send_email($seller_id)
	{
		$response['status'] = 0;
		$response['message'] = 'Failed to send email!';
		$info = $this->update_password($seller_id, 1);
		$this->load->library('email');
		$this->email->from('johngaebriel.vargas@pueblodepanay.com', 'REMS Notification');
		$this->email->to('sekajii@gmail.com');

		$this->email->subject('REMS Access Credentials');
		$this->email->message("Username: " . $info['username'] . "\r\n Password: " . $info['password']);
		if ($this->email->send()) {
			$response['status'] = 1;
			$response['status'] = 'Email Sent!';
		}
		exit();
	}

	public function check_duplicates($seller_id)
	{
		$result = [];
		$source = $this->M_seller->where(['id' => $seller_id])->fields('last_name,first_name')->get();
		$name = str_replace(' ', '', $source['last_name']) . str_replace(' ', '', $source['first_name']);
		$this->db->where("concat(replace(last_name,' ',''), replace(first_name,' ','')) = \"$name\"");
		$sellers = array_column($this->M_seller->get_all(), 'id');
		foreach ($sellers as $seller) {
			$trans = $this->M_transaction->where(['buyer_id' => $seller])->get();
			if ($trans) {
				$result[] = $seller;
			}
		}
		return count($result) > 1 ? $result : '';
	}
}
