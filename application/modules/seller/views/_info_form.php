<?php
    // Seller Information
    $id = isset($info['id']) && $info['id'] ? $info['id'] : '';
    $sales_group_id = isset($info['sales_group_id']) && $info['sales_group_id'] ? $info['sales_group_id'] : '';
    $seller_position_id = isset($info['seller_position_id']) && $info['seller_position_id'] ? $info['seller_position_id'] : '';
    $image = isset($info['image']) && $info['image'] ? $info['image'] : '';
    $last_name = isset($info['last_name']) && $info['last_name'] ? $info['last_name'] : '';
    $first_name = isset($info['first_name']) && $info['first_name'] ? $info['first_name'] : '';
    $middle_name = isset($info['middle_name']) && $info['middle_name'] ? $info['middle_name'] : '';
    $seller_team_id = isset($info['seller_team_id']) && $info['seller_team_id'] ? $info['seller_team_id'] : '';
    $birth_place = isset($info['birth_place']) && $info['birth_place'] ? $info['birth_place'] : '';
    $birth_date = isset($info['birth_date']) && $info['birth_date'] ? $info['birth_date'] : '';
    $gender = isset($info['gender']) ? $info['gender'] : '';
    $height = isset($info['height']) && $info['height'] ? $info['height'] : '';
    $weight = isset($info['weight']) && $info['weight'] ? $info['weight'] : '';
    $spouse_name = isset($info['spouse_name']) && $info['spouse_name'] ? $info['spouse_name'] : '';
    $religion = isset($info['religion']) && $info['religion'] ? $info['religion'] : '';
    $tin = isset($info['tin']) && $info['tin'] ? $info['tin'] : '';
    $sss = isset($info['sss']) && $info['sss'] ? $info['sss'] : '';
    $philhealth = isset($info['philhealth']) && $info['philhealth'] ? $info['philhealth'] : '';
    $hdmf = isset($info['hdmf']) && $info['hdmf'] ? $info['hdmf'] : '';
    $citizenship = isset($info['citizenship']) && $info['citizenship'] ? $info['citizenship'] : '';
    $skill = isset($info['skill']) && $info['skill'] ? $info['skill'] : '';
    $hobby = isset($info['hobby']) && $info['hobby'] ? $info['hobby'] : '';
    $language = isset($info['language']) && $info['language'] ? $info['language'] : '';
    $commission_rate = isset($info['commission_rate']) && $info['commission_rate'] ? $info['commission_rate'] : '0';
    $civil_status_id = isset($info['civil_status_id']) && $info['civil_status_id'] ? $info['civil_status_id'] : '';
    

    $seller_position = isset($info['position']['name']) && $info['position']['name'] ? $info['position']['name'] : '';
    $seller_team = isset($info['team']['name']) && $info['team']['name'] ? $info['team']['name'] : '';

    if (isset($sales_group_id) && !empty($sales_group_id)) {
        $seller_position_name = $seller_position;
    }

    if (isset($seller_team_id) && !empty($seller_team_id)) {
        $seller_team_name = $seller_team;
    }

?>


<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>Sales Group <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[sales_group_id]', Dropdown::get_dynamic('seller_group'), set_value('info[sales_group_id]', @$sales_group_id), 'class="form-control kt-select2" id="sales_group"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="">Team </label>
            <select name="info[seller_team_id]" class="form-control suggests" id="sales_team" data-id="<?php echo $seller_team_id; ?>" data-text="<?php echo $seller_team; ?>">
                <?php if ($seller_team): ?>
                    <option value="<?php echo $seller_team_id; ?>" selected><?php echo $seller_team_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    
    <div class="col-sm-3">
        <div class="form-group">
            <label class="">Commission Rate <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Commission Rate" name="info[commission_rate]" value="<?php echo set_value('info[commission_rate]', @$commission_rate); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="">Seller Position <span class="kt-font-danger">*</span></label>
            <select name="info[seller_position_id]" class="form-control suggests" id="seller_type" data-id="<?php echo $seller_position_id; ?>" data-text="<?php echo $seller_position_name; ?>">
                <?php if ($seller_position): ?>
                    <option value="<?php echo $seller_position_id; ?>" selected><?php echo $seller_position_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
   
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Lastname <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Last Name" name="info[last_name]" value="<?php echo set_value('info[last_name]', @$last_name); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Firstname <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="First Name" name="info[first_name]"  value="<?php echo set_value('info[first_name]"', @$first_name); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Middlename </label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Middlename" name="info[middle_name]" value="<?php echo set_value('info[middle_name]"', @$middle_name); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>Birth Place</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Place of Birth" name="info[birth_place]" value="<?php echo set_value('info[birth_place]"', @$birth_place); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Birth Date <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control datePicker" placeholder="Birth Date" name="info[birth_date]" value="<?php echo set_value('info[birth_date]"', @$birth_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Gender </label>
            <?php echo form_dropdown('info[gender]', Dropdown::get_static('sex'), set_value('info[gender]', @$gender), 'class="form-control" id="gender"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>Civil Status <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[civil_status_id]', Dropdown::get_static('civil_status'), set_value('info[civil_status]', @$civil_status_id), 'class="form-control" id="civil_status_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Height</label>
            <div class="kt-input-icon">
                <input type="number" class="form-control" placeholder="Height in CM" name="info[height]" value="<?php echo set_value('info[height]"', @$height); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Weight</label>
            <div class="kt-input-icon">
                <input type="number" class="form-control" placeholder="Weight in KG" name="info[weight]" value="<?php echo set_value('info[weight]"', @$weight); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Religion </label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Religion" name="info[religion]" value="<?php echo set_value('info[religion]"', @$religion); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>TIN</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="TIN" name="info[tin]" value="<?php echo set_value('info[tin]"', @$tin); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>SSS</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="SSS" name="info[sss]" value="<?php echo set_value('info[sss]"', @$sss); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>PhilHeath</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="PhilHeath" name="info[philhealth]" value="<?php echo set_value('info[philhealth]"', @$philhealth); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>HDMF</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="HDMF" name="info[hdmf]" value="<?php echo set_value('info[hdmf]"', @$hdmf); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Hobbies & Interest</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Hobbies & Interest" name="info[hobby]" value="<?php echo set_value('info[hobby]"', @$hobby); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Citizenship</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Citizenship" name="info[citizenship]" value="<?php echo set_value('info[citizenship]"', @$citizenship); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Skills</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Skills" name="info[skill]" value="<?php echo set_value('info[skill]"', @$skill); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Language / Dialects</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Language / Dialects" name="info[language]" value="<?php echo set_value('info[language]"', @$language); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Spouse Name</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Spouse Name" name="info[spouse_name]" value="<?php echo set_value('info[spouse_name]"', @$spouse_name); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>Profile Photo</label>
            <div>
                <?php if(get_image('seller', 'images', $image)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('seller', 'images', $image)); ?>" width="90px" height="90px"/>
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="seller_image" class="" aria-invalid="false">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
