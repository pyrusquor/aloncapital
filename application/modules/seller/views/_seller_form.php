<?php
    // Billing Information
    $buyer_id = isset($info['buyer_id']) && $info['buyer_id'] ? $info['buyer_id'] : '';
?>

<div class="row">

    <div class="col-md-12">

         <div id="buyer_form_repeater">
            <div class="form-grouprow">
                <label>Buyer Name : 
                    <div data-repeater-create="" class="btn btn btn-primary">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Add</span>
                        </span>
                    </div>
                </label>

                <div data-repeater-list="" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10">
                        <div class="col-sm-2">
                            <div class="input-group">
                                <select class="form-control suggests" id="buyer_id" name="buyer_id[]">
                                    <option value="0">Select Buyer Name</option>
                                    <?php //if ($model_name): ?>
                                        <!-- <option value="<?php //echo $model_id; ?>" selected><?php //echo $model_name; ?></option> -->
                                    <?php //endif ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <?php echo form_dropdown('info[client_type]', Dropdown::get_static('client_transaction_type'), set_value('info[client_type]', @$client_type), 'class="form-control" id="client_type"'); ?>
                        </div>
                        <div class="col-sm-2">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                <i class="la la-remove"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>