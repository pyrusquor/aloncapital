<!--begin: Basic Seller Info-->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
    <?php $this->load->view('_info_form'); ?>
</div>
<!--end: Basic Seller Info-->

<!--begin: Seller Contact Form -->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_contact_form'); ?>
</div>
<!--end: Seller Contact Form -->

<!--begin: Seller Source of Info -->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_source_form'); ?>
</div>
<!--end: Seller Source of Info -->

<!--begin: Seller Work Experience -->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_work_exp_form'); ?>
</div>

<!--end: Seller Work Experience -->

<!--begin: Seller Educations-->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_academic_form'); ?>
</div>
<!--end: Seller Educations-->

<!--begin: Form Actions -->
<div class="kt-form__actions">
    <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-prev">
        Previous
    </div>
    <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-submit">
        Submit
    </div>
    <div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-next">
        Next Step
    </div>
</div>

<!--end: Form Actions -->