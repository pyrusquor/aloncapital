<?php if (!empty($records)) : ?>
    <div class="row" id="sellerList">
            <?php foreach ($records as $r) :
                $aris_id = isset($r['aris_id']) && $r['aris_id'] ? $r['aris_id'] : '';
                $first_name = isset($r['first_name']) && $r['first_name'] ? $r['first_name'] : '';
                $last_name = isset($r['last_name']) && $r['last_name'] ? $r['last_name'] : '';
                $image = isset($r['image']) && $r['image'] ? $r['image'] : '';
                $mobile_no =    isset($r['mobile_no']) && $r['mobile_no'] ? $r['mobile_no'] : '';
                $email =    isset($r['email']) && $r['email'] ? $r['email'] : '';

                $sales_group_id = isset($r['sales_group_id']) && $r['sales_group_id'] ? $r['sales_group_id'] : '';
                
                $seller_position = isset($r['position']['name']) && $r['position']['name'] ? $r['position']['name'] : '';

            ?>
                <div class="col-md-4 col-xl-3">
                    <!--Begin::Portlet-->
                    <div class="kt-portlet kt-portlet--height-fluid">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>" class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                                    <span></span>
                                </label>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                    <i class="flaticon-more-1 kt-font-brand"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <a href="<?php echo base_url('seller/view/' . $r['id']); ?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                <span class="kt-nav__link-text">View</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="<?php echo base_url('seller/update/' . $r['id']); ?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                <span class="kt-nav__link-text">Update</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="javascript: void(0)" class="kt-nav__link remove_seller" data-id="<?php echo $r['id']; ?>">
                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                <span class="kt-nav__link-text">Delete</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Widget -->
                            <div class="kt-widget kt-widget--user-profile-2">
                                <div class="kt-widget__head">
                                    <div class="kt-widget__media">
                                    <?php if (get_image('seller', 'images', $image)): ?>
                                        <img class="kt-widget__img" src="<?php echo base_url(get_image('seller', 'images', $image)); ?>" width="90px" height="90px"/>
                                        <?php else : ?>

                                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                            <?php echo get_initials($first_name . ' ' . $last_name); ?>
                                        </div>
                                    <?php endif; ?>
                                    </div>
                                    <div class="kt-widget__info">
                                        <a href="<?php echo base_url('seller/view/' . $r['id']); ?>" class="kt-widget__title kt-hidden-">
                                            <?php echo $r['id'].'. '.ucwords($first_name . ' ' . $last_name); ?> <?php if($aris_id): ?><i class="fa fa-check-circle text-success"></i><?php endif; ?>
                                        </a>
                                        <span class="kt-widget__desc">
                                            <?php echo ucwords($seller_position); ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="kt-widget__body">
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label" style="font-weight: 400">Sales Group:</span>
                                            <span class="kt-widget__data" style="font-weight: 600"><?php echo Dropdown::get_static('group_type', $sales_group_id, 'view'); ?></span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label" style="font-weight: 400">Number of Clients:</span>
                                            <span class="kt-widget__data" style="font-weight: 600">25</span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label" style="font-weight: 400">Mobile:</span>
                                            <span class="kt-widget__data" style="font-weight: 600"><?php echo $mobile_no; ?></span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label" style="font-weight: 400">Email:</span>
                                            <span class="kt-widget__data" style="font-weight: 600"><?php echo $email; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget__footer">
                                    <a href="<?php echo base_url('seller/view/' . $r['id']); ?>" class="btn btn-label-brand btn-lg btn-upper">View Profile</a>
                                </div>
                            </div>
                            <!--end::Widget -->
                        </div>
                    </div>
                    <!--End::Portlet-->
                </div>
            <?php endforeach; ?>
    </div>
    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found. Please adjust your search criteria and try again.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <button class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand" data-toggle="modal" data-target="#filterModal">Try Again</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>