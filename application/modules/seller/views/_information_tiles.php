<div class="row">
    <?php foreach ($sales_group_tiles as $sales_group) : ?>
        <div class="col-sm col-12">
            <div class="kt-portlet kt-portlet--fluid <?= @$sales_group['bg-color'] ?>">
                <div class="kt-portlet__head text-white">
                    <div class="kt-portlet__head-label">
                        <div class="kt-portlet__head-titles">
                            <h4 class="mb-0">
                                <?= strtoupper($sales_group['sales_group']) ?>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body text-white">
                    <span class="d-block font-weight-bold mb-7 text-dark-50 d-flex" style="align-items: baseline;">
                        <h1 class="mb-0"><?= $sales_group['count'] ?></h1>&nbsp;<h5 class="mb-0">Seller<?= $sales_group['count'] > 1 ? 's' : '' ?></h5>
                    </span>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>