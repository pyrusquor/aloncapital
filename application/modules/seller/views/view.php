<?php
// Seller Information
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$sales_group_id = isset($info['sales_group_id']) && $info['sales_group_id'] ? $info['sales_group_id'] : '';
$last_name = isset($info['last_name']) && $info['last_name'] ? $info['last_name'] : '';
$image = isset($info['image']) && $info['image'] ? $info['image'] : '';
$first_name = isset($info['first_name']) && $info['first_name'] ? $info['first_name'] : '';
$middle_name = isset($info['middle_name']) && $info['middle_name'] ? $info['middle_name'] : '';
$designation = isset($info['designation']) && $info['designation'] ? $info['designation'] : '';
$birth_place = isset($info['birth_place']) && $info['birth_place'] ? $info['birth_place'] : '';
$birth_date = isset($info['birth_date']) && $info['birth_date'] && (strtotime($info['birth_date']) > 0) ? date_format(date_create($info['birth_date']), 'F j, Y') : '';
$gender = isset($info['gender']) && $info['gender'] ? $info['gender'] : '';
$civil_status = isset($info['civil_status_id']) && $info['civil_status_id'] ? $info['civil_status_id'] : '';
$height = isset($info['height']) && $info['height'] ? $info['height'] : '';
$weight = isset($info['weight']) && $info['weight'] ? $info['weight'] : '';
$spouse_name = isset($info['spouse_name']) && $info['spouse_name'] ? $info['spouse_name'] : '';
$religion = isset($info['religion']) && $info['religion'] ? $info['religion'] : '';
$tin = isset($info['tin']) && $info['tin'] ? $info['tin'] : '';
$sss = isset($info['sss']) && $info['sss'] ? $info['sss'] : '';
$philhealth = isset($info['philhealth']) && $info['philhealth'] ? $info['philhealth'] : '';
$hdmf = isset($info['hdmf']) && $info['hdmf'] ? $info['hdmf'] : '';
$citizenship = isset($info['citizenship']) && $info['citizenship'] ? $info['citizenship'] : '';
$skill = isset($info['skill']) && $info['skill'] ? $info['skill'] : '';
$hobby = isset($info['hobby']) && $info['hobby'] ? $info['hobby'] : '';
$language = isset($info['language']) && $info['language'] ? $info['language'] : '';
$seller_rate = isset($info['seller_rate']) && $info['seller_rate'] ? $info['seller_rate'] : '0';

// Contact Info
$present_address = isset($info['present_address']) && $info['present_address'] ? $info['present_address'] : '';
$home_address = isset($info['home_address']) && $info['home_address'] ? $info['home_address'] : '';
$living_quarters = isset($info['living_quarters']) && $info['living_quarters'] ? $info['living_quarters'] : '';
$mobile_no = isset($info['mobile_no']) && $info['mobile_no'] ? $info['mobile_no'] : '';
$email = isset($info['email']) && $info['email'] ? $info['email'] : '';
$landline = isset($info['landline']) && $info['landline'] ? $info['landline'] : '';
$region = isset($info['region']->regDesc) && $info['region']->regDesc ? $info['region']->regDesc : '';
$province = isset($info['province']->provDesc) && $info['province']->provDesc ? $info['province']->provDesc : '';
$city = isset($info['city']->citymunDesc) && $info['city']->citymunDesc ? $info['city']->citymunDesc : '';
$barangay = isset($info['barangay']->brgyDesc) && $info['barangay']->brgyDesc ? $info['barangay']->brgyDesc : '';
$sitio = isset($info['sitio']) && $info['sitio'] ? $info['sitio'] : '';

// Source Info
$recruiter = isset($info['source']['recruiter']) && $info['source']['recruiter'] ? $info['source']['recruiter'] : '';
$supervisor = isset($info['source']['supervisor']) && $info['source']['supervisor'] ? $info['source']['supervisor'] : '';
$net_worth = isset($info['source']['net_worth']) && $info['source']['net_worth'] ? $info['source']['net_worth'] : '';
$to_report = isset($info['source']['to_report']) && $info['source']['to_report'] ? $info['source']['to_report'] : '';
$to_attend = isset($info['source']['to_attend']) && $info['source']['to_attend'] ? $info['source']['to_attend'] : '';
$commission_based = isset($info['source']['commission_based']) && $info['source']['commission_based'] ? $info['source']['commission_based'] : '';
$is_member = isset($info['source']['is_member']) && $info['source']['is_member'] ? $info['source']['is_member'] : '';
$group = isset($info['source']['group']) && $info['source']['group'] ? $info['source']['group'] : '';

// Work Experience
$employer = isset($info['work_experience']['employer']) && $info['work_experience']['employer'] ? $info['work_experience']['employer'] : '';
$designation_exp = isset($info['work_experience']['designation']) && $info['work_experience']['designation'] ? $info['work_experience']['designation'] : '';
$emp_address = isset($info['work_experience']['emp_address']) && $info['work_experience']['emp_address'] ? $info['work_experience']['emp_address'] : '';
$salary = isset($info['work_experience']['salary']) && $info['work_experience']['salary'] ? $info['work_experience']['salary'] : '';
$emp_contact_no = isset($info['work_experience']['emp_contact_no']) && $info['work_experience']['emp_contact_no'] ? $info['work_experience']['emp_contact_no'] : '';
$supervisor = isset($info['work_experience']['supervisor']) && $info['work_experience']['supervisor'] ? $info['work_experience']['supervisor'] : '';
$start_date = isset($info['work_experience']['start_date']) && $info['work_experience']['start_date'] && (strtotime($info['work_experience']['start_date']) > 0) ? date_format(date_create($info['work_experience']['start_date']), 'F j, Y') : '';
$end_date = isset($info['work_experience']['end_date']) && $info['work_experience']['end_date'] && (strtotime($info['work_experience']['end_date']) > 0) ? date_format(date_create($info['work_experience']['end_date']), 'F j, Y') : '';
$reason = isset($info['work_experience']['reason']) && $info['work_experience']['reason'] ? $info['work_experience']['reason'] : '';
$to_contact = isset($info['work_experience']['to_contact']) && $info['work_experience']['to_contact'] ? $info['work_experience']['to_contact'] : '';

$seller_position = isset($info['position']['name']) && $info['position']['name'] ? $info['position']['name'] : '';
$sales_agent = isset($sales_agent) && $sales_agent ? $sales_agent : 0;
$account_no = isset($sales_agent['Accnt_No']) && $sales_agent['Accnt_No'] ? $sales_agent['Accnt_No'] : 'N/A';
$mobile_phone = isset($sales_agent['Mobilephone']) && $sales_agent['Mobilephone'] ? $sales_agent['Mobilephone'] : 'N/A';
$tax_ex = isset($sales_agent['Tax_ex']) && $sales_agent['Tax_ex'] ? $sales_agent['Tax_ex'] : 'N/A';
$address = isset($sales_agent['Address']) && $sales_agent['Address'] ? $sales_agent['Address'] : 'N/A';
$aris_city = isset($sales_agent['City']) && $sales_agent['City'] ? $sales_agent['City'] : 'N/A';
$aris_province = isset($sales_agent['Province']) && $sales_agent['Province'] ? $sales_agent['Province'] : 'N/A';

$project = isset($aris_project) && $aris_project ? $aris_project : 0;
$project_name = isset($project['ProjectName']) && $project['ProjectName'] ? $project['ProjectName'] : 'N/A';
$project_phone = isset($project['PhoneNumber']) && $project['PhoneNumber'] ? $project['PhoneNumber'] : 'N/A';
$project_fax = isset($project['FaxNumber']) && $project['FaxNumber'] ? $project['PhoneNumber'] : 'N/A';
$project_address = isset($project['Address']) && $project['Address'] ? $project['Address'] : 'N/A';
$project_city = isset($project['City']) && $project['City'] ? $project['City'] : 'N/A';
$project_province = isset($project['Province']) && $project['Province'] ? $project['Province'] : 'N/A';
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Seller</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('seller/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('seller'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!--begin:: Portlet-->
    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <!-- <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                        <?php echo get_initials($first_name . ' ' . $last_name); ?>
                    </div> -->
                    <?php if (get_image('seller', 'images', $image)) : ?>
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<?php echo base_url(get_image('seller', 'images', $image)); ?>" width="110px" height="110px" />
                        </div>
                    <?php else : ?>

                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                            <?php echo get_initials($first_name . ' ' . $last_name); ?>
                        </div>
                    <?php endif; ?>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <span class="kt-widget__username"><?php echo ucwords($first_name . ' ' . $last_name); ?></span>
                        </div>

                        <div class="kt-widget__desc">
                            <?php echo ucwords($seller_position); ?>
                        </div>
                    </div>
                    <div class="kt-widget__stats kt-margin-t-20 hide">
                        <div class="kt-widget__icon">
                            <i class="flaticon-piggy-bank"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title kt-font-bold">Clients</span>
                            <span class="kt-widget__value">249</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab" aria-selected="true">
                            <i class="flaticon2-user-outline-symbol"></i> Basic Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#contact_information" role="tab" aria-selected="false">
                            <i class="flaticon-support"></i> Contact Info
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#source_information" role="tab" aria-selected="false">
                            <i class="flaticon-book"></i> Source of Info
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#work_experience" role="tab" aria-selected="false">
                            <i class="flaticon-trophy"></i> Work Experience
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#education" role="tab" aria-selected="false">
                            <i class="flaticon-medal"></i> Education
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payment_request" role="tab" aria-selected="false">
                            <i class="fa fa-hand-holding-usd"></i> Payment Request
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payment_voucher" role="tab" aria-selected="false">
                            <i class="flaticon2-list"></i> Payment Voucher
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#saleslog" role="tab" aria-selected="false">
                            <i class="flaticon-mail-1"></i> Sales Logs
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#communication" role="tab" aria-selected="false">
                            <i class="flaticon-mail-1"></i> Email & SMS
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#aris_detail" role="tab" aria-selected="false">
                            <i class="flaticon2-user-outline-symbol"></i> ARIS Detail
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#audittrail" role="tab" aria-selected="false">
                            <i class="flaticon2-note"></i> Audit
                        </a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content  kt-margin-t-20">
                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="basic_information" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row ">
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Sales Group:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('group_type', $sales_group_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Last Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($last_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Place of Birth:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($birth_place); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Height:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $height; ?>
                                                    cm</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Citizenship:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($citizenship); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">PhilHealth:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $philhealth; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Hobbies & Interest:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $hobby; ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Seller Type:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $seller_position; ?> - <?php echo $seller_rate; ?> %</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">First Name:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($first_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Birthdate:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $birth_date; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Weight:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $weight; ?>
                                                    kg</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">TIN:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $tin; ?></span>
                                            </div>
                                        </div>
                                        <label class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">HDMF:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $hdmf; ?></span>
                                            </div>
                                        </label>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Languages / Dialects
                                                Spoken:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($language); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Team:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($designation); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Middle Name:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($middle_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Gender:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ($gender == '1') ? 'Male' : 'Female'; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Civil Status:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $civil_status; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Religion:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($religion); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">SSS:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $sss; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Skills:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $skill; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Spouse Name:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($spouse_name); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="contact_information" role="tabpanel-2">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Home Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($home_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Present Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($present_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Region:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($region); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Province:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($province); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">City:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($city); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Barangay:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($barangay); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Sitio:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($sitio); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Mailing Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">N/A</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Living Quarters:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $living_quarters; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Landline:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $landline; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Mobile:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $mobile_no; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Email:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $email; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="source_information" role="tabpanel-3">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Name of Recruiter:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($recruiter); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Name of Supervisor:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($supervisor); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Property owned / Net
                                                worth:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ($net_worth == "1") ? 'No' : 'No'; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Report at Sales Office 3
                                                times a week:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ($to_report == "1") ? 'Yes' : 'No'; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Attend to weekly Sales
                                                Meeting:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ($to_attend == "1") ? 'Yes' : 'No'; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Work in a Commission Based
                                                Salary:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ($commission_based == "1") ? 'Yes' : 'No'; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Nember of any
                                                Group/Organization:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ($is_member == "1") ? 'Yes' : 'No'; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Group/Organization
                                                Membership:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $group; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <?php if (!empty($info['reference'])) :
                                            $count = 1;
                                        ?>
                                            <?php foreach ($info['reference'] as $ref => $r) : ?>
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2" class="kt-shape-bg-color-1">REFERENCE <?php echo $count++; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row" style="width: 30%;">Name</th>
                                                            <td><?php echo ucwords($r['ref_name']); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Address</th>
                                                            <td><?php echo ucwords($r['ref_address']); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Contact Number</th>
                                                            <td><?php echo ucwords($r['ref_contact_no']); ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="work_experience" role="tabpanel-4">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Employer:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($employer); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Address:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($emp_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Contact Number:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $emp_contact_no; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Supervisor:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($supervisor); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Date Start of
                                                Employment:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $start_date; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Date End of
                                                Employment:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $end_date; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Designation:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($designation_exp); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Salary:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $salary; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Reasons for leaving:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $reason; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">May we contact?:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ($to_contact == "1") ? 'Yes' : 'No'; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="education" role="tabpanel-5">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <?php if (!empty($info['academic'])) : ?>

                                        <div class="col-7">
                                            <span class="kt-section__title">
                                                Academic History
                                            </span>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Degree</th>
                                                        <th>Course</th>
                                                        <th>School/University</th>
                                                        <th>Year</th>
                                                        <th>Rating</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php foreach ($info['academic'] as $akey => $a) : ?>
                                                        <tr>
                                                            <td><?php echo $a['level']; ?></td>
                                                            <td><?php echo $a['course']; ?></td>
                                                            <td><?php echo $a['school']; ?></td>
                                                            <td><?php echo $a['year']; ?></td>
                                                            <td><?php echo $a['rating']; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="payment_request" role="tabpanel-5">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentrequest_table">
                                    <thead>
                                        <tr>
                                            <th width="1%">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Payee</th>
                                            <th>Payable Type</th>
                                            <th>Property</th>
                                            <th>Due Date</th>
                                            <th>Gross Amount</th>
                                            <th>Tax Rate (VAT - WHT)</th>
                                            <th>Paid Amount</th>
                                            <th>Remaining Amount</th>
                                            <th>Payment Status</th>
                                            <th>Status</th>
                                            <th>Particulars</th>
                                        </tr>
                                    </thead>
                                </table>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="payment_voucher" role="tabpanel-5">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentvoucher_table">
                                    <thead>
                                        <tr>
                                            <th width="1%">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Payment Type</th>
                                            <th>Payee</th>
                                            <th>Payee Type</th>
                                            <th>Paid Date</th>
                                            <th>Paid Amount</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                </table>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <div class="tab-pane" id="saleslog" role="tabpanel-6">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="kt-portlet__body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="kt_widget5_tab1_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <?php if ($sales_logs) : ?>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <td style="width:20%">Property</td>
                                                                    <td style="width:25%">Reference Number</td>
                                                                    <td style="width:20%">Reservation Date</td>
                                                                    <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                    <td class="kt-align-center" style="width:22%">Seller</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($sales_logs as $key => $sales_log) : ?>
                                                                    <?php
                                                                    $reference = $sales_log['reference'];
                                                                    $transaction_id = $sales_log['id'];
                                                                    $property_name = $sales_log['property']['name'];
                                                                    $property_id = $sales_log['property']['id'];
                                                                    $reservation_date = $sales_log['reservation_date'];
                                                                    $this->transaction_library->initiate($sales_log['id']);
                                                                    $tpm_p = $this->transaction_library->get_amount_paid(1, 0, 1);
                                                                    $payment_percentage = $tpm_p . "%";
                                                                    $seller_name = @$sales_log['seller']["first_name"] . " " . @$sales_log['seller']["middle_name"] . " " . @$sales_log['seller']["last_name"];
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="<?php echo base_url("property/view/" . $property_id) ?>"><?= $property_name ?></a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="<?php echo base_url("transaction/view/" . $transaction_id) ?>"><?= $reference ?></a>
                                                                        </td>
                                                                        <td><?= view_date($reservation_date) ?></td>
                                                                        <td class="kt-align-center"><?= $payment_percentage ?></td>
                                                                        <td class="kt-align-center"><?= $seller_name ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    <?php else : ?>
                                                        <h4 class="font-weight-italic text-center">
                                                            No records found
                                                        </h4>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_widget5_tab2_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td style="width:20%">Property</td>
                                                                <td style="width:25%">Reference Number</td>
                                                                <td style="width:20%">Reservation Date</td>
                                                                <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                <td class="kt-align-center" style="width:22%">Buyer</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_widget5_tab3_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td style="width:20%">Property</td>
                                                                <td style="width:25%">Reference Number</td>
                                                                <td style="width:20%">Reservation Date</td>
                                                                <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                <td class="kt-align-center" style="width:22%">Buyer</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="communication" role="tabpanel-7">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row row-no-padding row-col-separator-lg">
                                    <div class="col-12">
                                        <span class="kt-section__title">
                                            Email
                                        </span>
                                        <div class="kt-portlet__body">
                                            <div class="kt-widget3">
                                                <?php if ($emails) : ?>
                                                    <?php foreach ($emails as $key => $email) : //vdebug($email) 
                                                    ?>
                                                        <?php
                                                        $seller_name = @$email['seller']["first_name"] . " " . @$email['seller']["middle_name"] . " " . @$email['seller']["last_name"];
                                                        $date_sent = $email['date_sent'];
                                                        $content = isset($email['content']) && $email['content'] ? $email['content'] : 'N/A';;

                                                        ?>
                                                        <div class="kt-widget3__item">
                                                            <div class="kt-widget3__header">
                                                                <div class="kt-widget3__user-img">
                                                                    <img class="kt-widget3__img" src="http://localhost/rems-dev//assets/media/users/user1.jpg" alt="">
                                                                </div>
                                                                <div class="kt-widget3__info">
                                                                    <a href="#" class="kt-widget3__username">
                                                                        <?= $seller_name; ?>
                                                                    </a>
                                                                </div>
                                                                <span class="kt-widget3__status kt-font-success">
                                                                    <?= view_date($date_sent); ?>
                                                                </span>
                                                            </div>
                                                            <div class="kt-widget3__body">
                                                                <p class="kt-widget3__text">
                                                                    <?= $content ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php else : ?>
                                                    <h4 class="font-weight-italic text-center">
                                                        No emails found
                                                    </h4>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-no-padding row-col-separator-lg">
                                    <div class="col-12">
                                        <span class="kt-section__title">
                                            SMS
                                        </span>
                                        <div class="kt-portlet__body">
                                            <div class="kt-widget3">
                                                <?php if ($sms) : ?>
                                                    <?php foreach ($sms as $key => $sm) : ?>
                                                        <?php
                                                        $seller_name = @$email['seller']["first_name"] . " " . @$email['seller']["middle_name"] . " " . @$email['seller']["last_name"];
                                                        $date_sent = $email['date_sent'];
                                                        $content = $email['content'];
                                                        ?>
                                                        <div class="kt-widget3__item">
                                                            <div class="kt-widget3__header">
                                                                <span class="kt-userpic kt-userpic--circle kt-userpic--success">
                                                                    <span><?php echo get_initials(@$email['seller']['first_name'] . ' ' . @$email['seller']['last_name']); ?></span>
                                                                </span>
                                                                <div class="kt-widget3__info">
                                                                    <a href="#" class="kt-widget3__username">
                                                                        <?= $seller_name; ?>
                                                                    </a>
                                                                </div>
                                                                <span class="kt-widget3__status kt-font-success">
                                                                    <?= view_date($date_sent); ?>
                                                                </span>
                                                            </div>
                                                            <div class="kt-widget3__body">
                                                                <p class="kt-widget3__text">
                                                                    <?= $content ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php else : ?>
                                                    <h4 class="font-weight-italic text-center">
                                                        No sms found
                                                    </h4>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="aris_detail" role="tabpanel-8">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row row-no-padding row-col-separator-lg">
                                    <div class="col-12">
                                        <span class="kt-section__title">
                                            Sales Agent Information
                                        </span>
                                        <div class="kt-portlet__body">
                                            <div class="row ">
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Account Number:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $account_no ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Mobile Number:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $mobile_phone ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Tax:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $tax_ex ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row ">
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Address:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $address ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">City:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $aris_city ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Province:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $aris_province ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="kt-section__title">
                                            Project Information
                                        </span>
                                        <div class="kt-portlet__body">
                                            <div class="row ">
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Project:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $project_name ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Phone Number:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $project_phone ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Fax number:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $project_fax ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row ">
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Address:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $project_address ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">City:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $project_city ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group form-group-xs row">
                                                        <label class="col col-form-label text-right">Province:</label>
                                                        <div class="col">
                                                            <span class="form-control-plaintext kt-font-bolder"><?= $project_province ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="audittrail" role="tabpanel-9 ">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">Audit Trail</h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="table-responsive">
                                        <?php if ($audit) : ?>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <td style="width:10%">ID</td>
                                                        <td style="width:15%">Affected Table</td>
                                                        <td style="width:15%">Action</td>
                                                        <td class="kt-align-center" style="width:22%">Date</td>
                                                        <td class="kt-align-center" style="width:25%">Created By</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (isset($audit) && ($audit)) {
                                                        foreach ($audit as $key => $value) { ?>
                                                            <tr class="clickable" data-toggle="collapse" style="cursor: pointer;" id="row-<?php echo $value['id'] ?>" data-target="#hidden-row-<?php echo $value['id'] ?>">
                                                                <td><?php echo $value['id'] ?></td>
                                                                <td><?php echo $value['affected_table'] ?></td>
                                                                <td><?php echo $value['action'] ?></td>
                                                                <td><?php echo view_date($value['created_at']) ?></td>
                                                                <td><?php echo $value['user']['first_name'] . ' ' . $value['user']['last_name'] ?></td>
                                                            </tr>
                                                            <tr class="collapse" id="hidden-row-<?php echo $value['id'] ?>">
                                                                <td colspan="3">
                                                                    Previous Record: <code><?php echo $value['previous_record'] ?></code>
                                                                </td>
                                                                <td colspan="2">
                                                                    New Record: <code><?php echo $value['new_record'] ?></code>
                                                                </td>
                                                            </tr>
                                                    <?php }
                                                    } ?>
                                                </tbody>
                                            </table>
                                        <?php else : ?>
                                            <h4 class="font-weight-italic text-center">
                                                No records found
                                            </h4>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->
            </div>
        </div>
    </div>

    <div class="row hide">
        <div class="col-xl-12">
            <!--begin:: Widgets/Order Statistics-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Sales Report
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body  kt-portlet__body--fit">
                    <div class="row row-no-padding row-col-separator-lg">
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Commission Paid Out
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Commission Expenses
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Acquisition Commission
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Sale Commission
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                    </div>
                    <!-- To be Continued -->

                    <div class="row row-no-padding row-col-separator-lg">
                        <div class="col-lg-9">
                            <div id="kt_amcharts_1" style="height: 450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Order Statistics-->
        </div>
    </div>

    <div class="row hide">
        <div class="col-xl-12">
            <!--begin:: Widgets/Order Statistics-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Payables Overview
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body  kt-portlet__body--fit">
                    <div class="row row-no-padding row-col-separator-lg">
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Paid
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Open Payables
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Over Due Payables
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Payables
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                    </div>
                    <!-- To be Continued -->
                    <div class="row row-no-padding row-col-separator-lg">
                        <div class="col-lg-9">
                            <!-- <div id="kt_amcharts_1" style="height: 450px;"></div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Order Statistics-->
        </div>
    </div>
</div>