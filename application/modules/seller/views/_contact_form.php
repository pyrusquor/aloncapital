<?php
    // Contact Info
    $present_address = isset($info['present_address']) && $info['present_address'] ? $info['present_address'] : '';
    $home_address = isset($info['home_address']) && $info['home_address'] ? $info['home_address'] : '';
    $living_quarters = isset($info['living_quarters']) && $info['living_quarters'] ? $info['living_quarters'] : '';
    $mobile_no = isset($info['mobile_no']) && $info['mobile_no'] ? $info['mobile_no'] : '';
    $email = isset($info['email']) && $info['email'] ? $info['email'] : '';
    $landline = isset($info['landline']) && $info['landline'] ? $info['landline'] : '';
    $region_id = isset($info['regCode']) && $info['regCode'] ? $info['regCode'] : '';
    $region_id = isset($info['regCode']) && $info['regCode'] ? $info['regCode'] : '';
    $province_id = isset($info['provCode']) && $info['provCode'] ? $info['provCode'] : '';
    $city_id = isset($info['citymunCode']) && $info['citymunCode'] ? $info['citymunCode'] : '';
    $barangay_id = isset($info['brgyCode']) && $info['brgyCode'] ? $info['brgyCode'] : '';
    // vdebug($info);
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Mobile <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Mobile" name="info[mobile_no]" value="<?php echo set_value('info[mobile_no]"', @$mobile_no); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Email <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Email" id="seller_email" name="info[email]" value="<?php echo set_value('info[email]"', @$email); ?>" autocomplete="off">
                <input type="hidden" value="<?php echo $email; ?>" id="seller_email_key" name="key">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Living Quarters</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Living Quarters" name="info[living_quarters]" value="<?php echo set_value('info[living_quarters]"', @$living_quarters); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Landline</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Landline" name="info[landline]" value="<?php echo set_value('info[landline]"', @$landline); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Present Address: <span class="kt-font-danger">*</span></label>
            <textarea class="form-control" id="present_address" rows="3" spellcheck="false" name="info[present_address]"><?php echo set_value('info[present_address]"', @$present_address); ?></textarea> 
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Home Address:</label>
            <textarea class="form-control" id="home_address" rows="3" spellcheck="false" name="info[home_address]"><?php echo set_value('info[home_address]"', @$home_address); ?></textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label>Region:</label>
            <select class="form-control" id="region" name="info[regCode]" data-val="<?php echo $region_id ?>">
                <option value="">Select Region</option>
                <?php foreach ($regions as $value): ?>
                    <option value="<?php echo $value['regCode']; ?>"><?php echo $value['regDesc']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Province:</label>
            <select class="form-control" id="province" name="info[provCode]" data-val="<?php echo $province_id ?>" disabled>
                <option value="">Select Province</option>
                <?php foreach ($provinces as $value): ?>
                    <option value="<?php echo $value['provCode']; ?>"><?php echo $value['provDesc']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>City:</label>
            <select class="form-control" id="city" name="info[citymunCode]" data-val="<?php echo $city_id ?>" disabled>
                <option value="">Select City</option>
                <?php foreach ($cities as $value): ?>
                    <option value="<?php echo $value['citymunCode']; ?>"><?php echo $value['citymunDesc']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Barangay:</label>
            <select class="form-control" id="brgy" name="info[brgyCode]" data-val="<?php echo $barangay_id ?>" disabled>
                <option value="">Select Barangay</option>
            </select>
        </div>
    </div>
</div>