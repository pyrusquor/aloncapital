<?php
// Work Experience
$employer = isset($info['work_experience']['employer']) && $info['work_experience']['employer'] ? $info['work_experience']['employer'] : '';
$designation_exp = isset($info['work_experience']['designation']) && $info['work_experience']['designation'] ? $info['work_experience']['designation'] : '';
$emp_address = isset($info['work_experience']['emp_address']) && $info['work_experience']['emp_address'] ? $info['work_experience']['emp_address'] : '';
$salary = isset($info['work_experience']['salary']) && $info['work_experience']['salary'] ? $info['work_experience']['salary'] : '';
$emp_contact_no = isset($info['work_experience']['emp_contact_no']) && $info['work_experience']['emp_contact_no'] ? $info['work_experience']['emp_contact_no'] : '';
$supervisor = isset($info['work_experience']['supervisor']) && $info['work_experience']['supervisor'] ? $info['work_experience']['supervisor'] : '';
$start_date = isset($info['work_experience']['start_date']) && $info['work_experience']['start_date'] ? $info['work_experience']['start_date'] : '';
$end_date = isset($info['work_experience']['end_date']) && $info['work_experience']['end_date'] ? $info['work_experience']['end_date'] : '';
$reason = isset($info['work_experience']['reason']) && $info['work_experience']['reason'] ? $info['work_experience']['reason'] : '';
$to_contact = isset($info['work_experience']['to_contact']) && $info['work_experience']['to_contact'] ? $info['work_experience']['to_contact'] : '';
?>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label class="">Employer</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Employer" name="work_exp[employer]" value="<?php echo set_value('work_exp[employer]"', @$employer); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Address</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Address" name="work_exp[emp_address]" value="<?php echo set_value('work_exp[emp_address]"', @$emp_address); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Contact Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Contact Number" name="work_exp[emp_contact_no]" value="<?php echo set_value('work_exp[emp_contact_no]"', @$emp_contact_no); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Name of Immediate Supervisor</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Name of Immediate Supervisor" name="work_exp[supervisor]" value="<?php echo set_value('work_exp[supervisor]"', @$supervisor); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Date Start of Employment</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control datePicker" placeholder="Date Start of Employment" name="work_exp[start_date]" value="<?php echo set_value('work_exp[start_date]"', @$start_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Date End of Employment</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control datePicker" placeholder="Date End of Employment" name="work_exp[end_date]" value="<?php echo set_value('work_exp[end_date]"', @$end_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label class="">Designation</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Designation" name="work_exp[designation]" value="<?php echo set_value('work_exp[designation]"', @$designation); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Salary</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Salary" name="work_exp[salary]" value="<?php echo set_value('work_exp[salary]"', @$salary); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Reasons for leaving</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Reasons for leaving" name="work_exp[reason]" value="<?php echo set_value('work_exp[reason]"', @$reason); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">May we contact?</label>
            <?php echo form_dropdown('work_exp[to_contact]', Dropdown::get_static('bool'), set_value('work_exp[to_contact]', @$to_contact), 'class="form-control" id="to_report"'); ?>
        </div>
    </div>
</div>