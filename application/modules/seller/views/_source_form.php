<?php
    // Source Info
    $recruiter = isset($info['source']['recruiter']) && $info['source']['recruiter'] ? $info['source']['recruiter'] : '';
    $supervisor = isset($info['source']['supervisor']) && $info['source']['supervisor'] ? $info['source']['supervisor'] : '';
    $net_worth = isset($info['source']['net_worth']) && $info['source']['net_worth'] ? $info['source']['net_worth'] : '';
    $to_report = isset($info['source']['to_report']) ? $info['source']['to_report'] : '';
    $to_attend = isset($info['source']['to_attend']) ? $info['source']['to_attend'] : '';
    $commission_based = isset($info['source']['commission_based']) ? $info['source']['commission_based'] : '';
    $is_member = isset($info['source']['is_member']) ? $info['source']['is_member'] : '';
    $group = isset($info['source']['group']) ? $info['source']['group'] : '';

    $reference = isset($info['reference']) && $info['reference'] ? $info['reference'] : '';
?>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label class="">Name of Recruiter <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Name of Recruiter" name="source_info[recruiter]" value="<?php echo set_value('source_info[recruiter]"', @$recruiter); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label class="">Name of Sales Supervisor <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Name of Sales Supervisor" name="source_info[supervisor]" value="<?php echo set_value('source_info[supervisor]"', @$supervisor); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label>Property owned / Net worth:</label>
            <input type="text" class="form-control" placeholder="Property owned / Net worth:" name="source_info[net_worth]" value="<?php echo set_value('source_info[net_worth]"', @$net_worth); ?>">
        </div>
        <div class="form-group">
            <label>Report at Sales Office 3 times a week? <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('source_info[to_report]', Dropdown::get_static('bool'), set_value('source_info[to_report]', @$to_report), 'class="form-control" id="to_report"'); ?>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label>Attend to weekly Sales Meeting? <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('source_info[to_attend]', Dropdown::get_static('bool'), set_value('source_info[to_attend]', @$to_attend), 'class="form-control" id="to_report"'); ?>
            <span class="form-text text-muted"></span>
        </div>
        <div class="form-group">
            <label>Work in a commission based salary? <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('source_info[commission_based]', Dropdown::get_static('bool'), set_value('source_info[commission_based]', @$commission_based), 'class="form-control" id="to_report"'); ?>
        </div>
        <div class="form-group">
            <label>Member of any group / organization?</label>
            <?php echo form_dropdown('source_info[is_member]', Dropdown::get_static('bool'), set_value('source_info[is_member]', @$is_member), 'class="form-control" id="to_report"'); ?>
        </div>
        <div class="form-group">
            <label>Group / Organization Membership</label>
            <input type="text" class="form-control" placeholder="Group / Organization Membership" name="source_info[group]" value="<?php echo set_value('source_info[group]"', @$group); ?>">
        </div>
    </div>
    <div class="col-lg-6">
        <!--begin::Accordion-->
        <div class="accordion  accordion-toggle-arrow" id="accordionExample4">
            <?php if( ! empty($reference) ): ?>
                <?php $count = 1; foreach ($reference as $rkey => $r): ?>
                    <div class="card">
                        <div class="card-header" id="heading-<?php echo $count; ?>">
                            <div class="card-title" data-toggle="collapse" data-target="#collapse-<?php echo $count; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $count; ?>">
                                <i class="flaticon-user-add"></i> Reference <?php echo $count++; ?>
                            </div>
                        </div>
                        <div id="collapse-<?php echo $count; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample4">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="">Name</label>
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" placeholder="Name" name="ref_name[]" value="<?php echo $r['ref_name']; ?>">
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Address</label>
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" placeholder="Address" name="ref_address[]" value="<?php echo $r['ref_address']; ?>">
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Contact Number</label>
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" placeholder="Contact Number" name="ref_contact_no[]" value="<?php echo $r['ref_contact_no']; ?>">
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <?php for ($i=1; $i <= 3; $i++): ?>
                    <div class="card">
                        <div class="card-header" id="heading-<?php echo $i; ?>">
                            <div class="card-title <?php echo ($i == '1') ? '' : 'collapsed'; ?>" data-toggle="collapse" data-target="#collapse-<?php echo $i; ?>" aria-expanded="<?php echo ($i == '1') ? 'true' : 'false'; ?>" aria-controls="collapse-<?php echo $i; ?>">
                                <i class="flaticon-user-add"></i> Reference <?php echo $i; ?>
                            </div>
                        </div>
                        <div id="collapse-<?php echo $i; ?>" class="collapse <?php echo ($i == '1') ? 'show' : ''; ?>" aria-labelledby="headingTwo1" data-parent="#accordionExample4">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="">Name</label>
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" placeholder="Name" name="ref_name[]">
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Address</label>
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" placeholder="Address" name="ref_address[]">
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Contact Number</label>
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" placeholder="Contact Number" name="ref_contact_no[]">
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
                
            <?php endif; ?>

        </div>

        <!--end::Accordion-->

    </div>
</div>