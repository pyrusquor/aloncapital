<?php
    $academic = isset($info['academic']) && $info['academic'] ? $info['academic'] : '';
    $exam = isset($info['exam']) && $info['exam'] ? $info['exam'] : '';
    $training = isset($info['training']) && $info['training'] ? $info['training'] : '';
?>
<div class="form-group row">
    <div class="kt-section kt-section--first">
        <div class="col-lg-12">
            <h3 class="kt-section__title">Academic History</h3>
            <div class="kt-section__body">
                <div id="academic_form_repeater">
                    <div class="form-group form-group-last row" id="academic_form_repeater">
                        <div data-repeater-list="academic" class="col-lg-12">
                        <?php if( ! empty($academic) ): ?>
                            <?php foreach ($academic as $ackey => $ac): ?>
                                <div data-repeater-item class="form-group row align-items-center">
                                    <div class="col-md-2">
                                        <div class="kt-form__group--inline">
                                            <div class="kt-form__label">
                                                <label>Degree/Level:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <input type="text" class="form-control" placeholder="Degree" name="level" value="<?php echo $ac['level']; ?>">
                                            </div>
                                        </div>
                                        <div class="d-md-none kt-margin-b-10"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="kt-form__group--inline">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Course:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <input type="text" class="form-control" placeholder="Course" name="course" value="<?php echo $ac['course']; ?>">
                                            </div>
                                        </div>
                                        <div class="d-md-none kt-margin-b-10"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="kt-form__group--inline">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">School/University:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <input type="text" class="form-control" placeholder="School/University" name="school" value="<?php echo $ac['school']; ?>">
                                            </div>
                                        </div>
                                        <div class="d-md-none kt-margin-b-10"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="kt-form__group--inline">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Year:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <input type="text" class="form-control yearPicker" placeholder="Year" name="year" value="<?php echo $ac['year']; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="d-md-none kt-margin-b-10"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="kt-form__group--inline">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Rating:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <input type="text" class="form-control" placeholder="Rating" name="rating" value="<?php echo $ac['rating']; ?>">
                                            </div>
                                        </div>
                                        <div class="d-md-none kt-margin-b-10"></div>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                            <i class="la la-trash-o"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div data-repeater-item class="form-group row align-items-center">
                                <div class="col-md-2">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label>Degree/Level:</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <input type="text" class="form-control" placeholder="Degree" name="level">
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-2">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label class="kt-label m-label--single">Course:</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <input type="text" class="form-control" placeholder="Course" name="course">
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label class="kt-label m-label--single">School/University:</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <input type="text" class="form-control" placeholder="School/University" name="school">
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-2">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label class="kt-label m-label--single">Year:</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <input type="text" class="form-control yearPicker" placeholder="Year" name="year" readonly>
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-2">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label class="kt-label m-label--single">Rating:</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <input type="text" class="form-control" placeholder="Rating" name="rating">
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-1">
                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                        <i class="la la-trash-o"></i>
                                    </a>
                                </div>
                            </div>
                        <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group form-group-last row">
                        <div class="col-lg-2">
                            <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                <i class="la la-plus"></i> Add
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg">
            </div>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="kt-section__title">Seminars and Trainings</h3>
                    <div class="kt-section__body">
                        <div id="seminar_form_repeater">
                            <div class="form-group form-group-last row" id="seminar_form_repeater">
                                <div data-repeater-list="training" class="col-lg-12">
                                <?php if( ! empty( $training ) ): ?>
                                    <?php foreach ($training as $tkey => $t): ?>
                                        <div data-repeater-item class="form-group row align-items-center">
                                            <div class="col-md-4">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                        <label>Name:</label>
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $t['name']; ?>">
                                                    </div>
                                                </div>
                                                <div class="d-md-none kt-margin-b-10"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single">Trainor:</label>
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <input type="text" class="form-control" placeholder="Trainor" name="trainor" value="<?php echo $t['trainor']; ?>">
                                                    </div>
                                                </div>
                                                <div class="d-md-none kt-margin-b-10"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single">Year:</label>
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <input type="text" class="form-control yearPicker" placeholder="Year" name="year" value="<?php echo $t['year']; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="d-md-none kt-margin-b-10"></div>
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                    <i class="la la-trash-o"></i>
                                                </a>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <div data-repeater-item class="form-group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__label">
                                                    <label>Name:</label>
                                                </div>
                                                <div class="kt-form__control">
                                                    <input type="text" class="form-control" placeholder="Name" name="name">
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__label">
                                                    <label class="kt-label m-label--single">Trainor:</label>
                                                </div>
                                                <div class="kt-form__control">
                                                    <input type="text" class="form-control" placeholder="Trainor" name="trainor">
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__label">
                                                    <label class="kt-label m-label--single">Year:</label>
                                                </div>
                                                <div class="kt-form__control">
                                                    <input type="text" class="form-control yearPicker" placeholder="Year" name="year" readonly>
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                <i class="la la-trash-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group form-group-last row">
                                <div class="col-lg-4">
                                    <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                        <i class="la la-plus"></i> Add
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3 class="kt-section__title">Government / Non-government Licensure Exam
                        Taken</h3>
                    <div class="kt-section__body">
                        <div id="exam_form_repeater">
                            <div class="form-group form-group-last row" id="exam_form_repeater">
                                <div data-repeater-list="exam" class="col-lg-12">
                                <?php if( ! empty($exam) ): ?>
                                    <?php foreach ($exam as $ekey => $e): ?>
                                        <div data-repeater-item class="form-group row align-items-center">
                                            <div class="col-md-4">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                        <label>Name:</label>
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $e['name']; ?>">
                                                    </div>
                                                </div>
                                                <div class="d-md-none kt-margin-b-10"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single">Date:</label>
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <input type="text" class="form-control datePicker" placeholder="Date" name="date" value="<?php echo $e['date']; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="d-md-none kt-margin-b-10"></div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single">Status:</label>
                                                    </div>
                                                    <div class="kt-form__control">
                                                        <input type="text" class="form-control" placeholder="Status" name="status" value="<?php echo $e['status']; ?>">
                                                    </div>
                                                </div>
                                                <div class="d-md-none kt-margin-b-10"></div>
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                    <i class="la la-trash-o"></i>
                                                </a>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <div data-repeater-item class="form-group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__label">
                                                    <label>Name:</label>
                                                </div>
                                                <div class="kt-form__control">
                                                    <input type="text" class="form-control" placeholder="Name" name="name">
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__label">
                                                    <label class="kt-label m-label--single">Date:</label>
                                                </div>
                                                <div class="kt-form__control">
                                                    <input type="text" class="form-control datePicker" placeholder="Date" name="date" readonly>
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="kt-form__group--inline">
                                                <div class="kt-form__label">
                                                    <label class="kt-label m-label--single">Status:</label>
                                                </div>
                                                <div class="kt-form__control">
                                                    <input type="text" class="form-control" placeholder="Status" name="status">
                                                </div>
                                            </div>
                                            <div class="d-md-none kt-margin-b-10"></div>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                <i class="la la-trash-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group form-group-last row">
                                <div class="col-lg-4">
                                    <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                        <i class="la la-plus"></i> Add
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>