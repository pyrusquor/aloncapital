<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Seller_work_experience_model extends MY_Model {

	public $table = 'seller_work_experiences'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = ['seller_id', 'employer', 'designation', 'emp_address', 'salary', 'emp_contact_no', 'supervisor', 'start_date', 'end_date', 'reason', 'to_contact']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = FALSE;
        $this->timestamps = FALSE;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
	}
}