<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Seller_contact_info_model extends MY_Model {

	public $table = 'seller_contact_infos'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = ['seller_id', 'present_address', 'home_address', 'living_quarters', 'mobile_no', 'email', 'landline']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = FALSE;
        $this->timestamps = FALSE;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
	}
}