<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Seller_model extends MY_Model
{

	public $table = 'sellers'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'user_id',
		'sales_group_id',
		'seller_position_id',
		'civil_status_id',
		'seller_team_id',
		'image',
		'last_name',
		'first_name',
		'middle_name',
		'designation',
		'birth_place',
		'birth_date',
		'gender',
		'height',
		'weight',
		'spouse_name',
		'religion',
		'tin',
		'philhealth',
		'hdmf',
		'sss',
		'citizenship',
		'skill',
		'hobby',
		'language',
		'present_address',
		'home_address',
		'living_quarters',
		'mobile_no',
		'email',
		'landline',
		'upline_id',
		'commission_rate',
		'regCode',
		'provCode',
		'citymunCode',
		'brgyCode',
		'sitio',
		'SID',
		'acct_number',
		'is_exported',
		'aris_id',
		'other_mobile',
		'is_active',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = TRUE;
		$this->return_as = 'array';

		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		// $this->has_one['contact'] = array('foreign_model'=>'Seller_contact_info_model','foreign_table'=>'seller_contact_infos','foreign_key'=>'seller_id','local_key'=>'id');
		$this->has_one['source'] = array('foreign_model' => 'seller/Seller_source_info_model', 'foreign_table' => 'seller_source_infos', 'foreign_key' => 'seller_id', 'local_key' => 'id');
		$this->has_one['work_experience'] = array('foreign_model' => 'seller/Seller_work_experience_model', 'foreign_table' => 'seller_work_experiences', 'foreign_key' => 'seller_id', 'local_key' => 'id');
		$this->has_one['region'] = array('foreign_model' => 'locations/Address_regions_model', 'foreign_table' => 'address_regions', 'foreign_key' => 'regCode', 'local_key' => 'regCode');
		$this->has_one['province'] = array('foreign_model' => 'locations/Address_provinces_model', 'foreign_table' => 'address_provinces', 'foreign_key' => 'provCode', 'local_key' => 'provCode');
		$this->has_one['city'] = array('foreign_model' => 'locations/Address_city_municipalities_model', 'foreign_table' => 'address_city_municipalities', 'foreign_key' => 'citymunCode', 'local_key' => 'citymunCode');
		$this->has_one['barangay'] = array('foreign_model' => 'locations/Address_barangays_model', 'foreign_table' => 'address_barangays', 'foreign_key' => 'brgyCode', 'local_key' => 'brgyCode');

		$this->has_many['reference'] = array('foreign_model' => 'seller/Seller_reference_model', 'foreign_table' => 'seller_references', 'foreign_key' => 'seller_id', 'local_key' => 'id');
		$this->has_many['academic'] = array('foreign_model' => 'seller/Seller_education_model', 'foreign_table' => 'seller_educations', 'foreign_key' => 'seller_id', 'local_key' => 'id');
		$this->has_many['exam'] = array('foreign_model' => 'seller/Seller_exam_model', 'foreign_table' => 'seller_exams', 'foreign_key' => 'seller_id', 'local_key' => 'id');
		$this->has_many['training'] = array('foreign_model' => 'seller/Seller_training_model', 'foreign_table' => 'seller_trainings', 'foreign_key' => 'seller_id', 'local_key' => 'id');

		$this->has_one['position'] = array('foreign_model' => 'position/Seller_position_model', 'foreign_table' => 'seller_positions', 'foreign_key' => 'id', 'local_key' => 'seller_position_id');

		$this->has_one['group'] = array('foreign_model' => 'seller_group/Seller_group_model', 'foreign_table' => 'seller_group', 'foreign_key' => 'id', 'local_key' => 'sales_group_id');

		$this->has_one['team'] = array('foreign_model' => 'seller_teams/Seller_teams_model', 'foreign_table' => 'seller_teams', 'foreign_key' => 'id', 'local_key' => 'seller_team_id');
	}
}
