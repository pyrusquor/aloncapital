<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_history/Login_history_model', 'M_login_history');
        $this->load->model('locations/Address_regions_model', 'M_region');
        $this->load->model('locations/Address_provinces_model', 'M_province');
        $this->load->model('locations/Address_city_municipalities_model', 'M_city');
        $this->load->model('user_permission/User_permission_model', 'M_User_permission');
		$this->load->model('permission/Permission_model', 'M_Permission');
        $this->load->model('locations/Address_barangays_model', 'M_barangay');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('auth/Ion_auth_model', 'M_ion_auth');
        $this->load->model('staff/Staff_model', 'M_staff');
        $this->load->model('applicant/Applicant_model', 'M_applicant');
        $this->load->model('applicant/Applicant_employment_model', 'M_applicant_employment');
        $this->load->model('applicant/Applicant_identification_model', 'M_applicant_identification');
        $this->load->model('permission/Permission_model', 'M_permission');
        $this->load->model('auth/Ion_auth_model', 'M_auth');

        $this->load->model('loan/Loan_model', 'M_Loan');


        $this->admin = "admin";
        $this->password = "password";

        $this->load->library('communication_library');
        // print_r($this->session->userdata());
        // $this->session->sess_destroy();
        $this->ion_auth->logout();
        session_start();
        session_regenerate_id();
    }

    public function login()
    {
        // overwrite default theme and layout if needed
        $this->template->set_layout('login');
        $this->template->build('login');
    }

    public function registration()
    {
        $regions = $this->M_region->as_array()->get_all();
        $provinces = $this->M_province->as_array()->get_all();
        $cities = $this->M_city->as_array()->get_all();
        $barangays = $this->M_barangay->as_array()->get_all();
        $permissions = $this->M_permission->as_array()->get_all();

        $locations = array(
            $regions,
            $provinces,
            $cities,
            $barangays,
            $permissions
        );

        // overwrite default theme and layout if needed
        $this->template->set_layout('registration');
        $this->template->build('registration', $locations);
    }

    public function calculate_loan($loan_period = 0, $loan_amount = 0){

        $interest_rate = 3.5;

        // monthly_payment($loan_amount,$interest_rate,$loan_period);
        $monthly_payment = ( ( ($loan_amount * .035) * $loan_period ) + $loan_amount ) / $loan_period;

        $processing_fee =  round(($loan_amount * .035));

        $result['monthly_payment'] = money_php($monthly_payment);
        $result['processing_fee'] = money_php($processing_fee);

        echo json_encode($result);
    }

    public function privacy_policy()
    {
        $this->template->set_layout('privacy_policy');
        $this->template->build('privacy_policy');
    }

    public function validate()
    {

        $this->form_validation->set_rules("email", "Username", "trim|required", array("required" => "required"));
        $this->form_validation->set_rules("password", "Password", "trim|required", array("required" => "required"));

        if ($this->form_validation->run() === false) {

            $result = array('status' => false, 'msg' => 'Please input all required fields.');

        } else {

            $email = cleaner($this->input->post('email'), 'username', '_');
            $password = cleaner($this->input->post('password'));
            $remember = cleaner($this->input->post('remember'), 'alpha');
            $remember = (bool) $remember;

            // Save login history
            $user_id = $this->ion_auth->get_user_id_from_identity($email);
            $ip_address = $_SERVER['REMOTE_ADDR'];

            $history['user_id'] = $user_id;
            $history['ip_address'] = $ip_address;

            if ($this->ion_auth->login($email, $password, $remember)) {
                $result = array('status' => true, 'msg' => 'You have successfully logged in.');

                if ($user_id) {
                    $this->M_login_history->insert($history);
                }

            } else {
                $result = array('status' => false, 'msg' => 'The username or password you entered is incorrect.');
            }
        }

        echo json_encode($result);
    }

    public function logout()
    {
        // log the user out
        $this->ion_auth->logout();
        header('Location: ' . base_url('login'));

    }

    public function register() {

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $post = $this->input->post();


            // Image Upload
            $upload_path = './assets/uploads/applicant/images';
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 5000;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config, 'image');
            $this->image->initialize($config);
            
            if (!empty($_FILES['image']['name'])) {

                if (!$this->image->do_upload('image')) {
                    
                    vdebug($this->image->display_errors());

                    $this->notify->error($this->image->display_errors(), 'registration');
                } else {
                    $img_data = $this->image->data();

                }
            }


            if(!$this->M_auth->email_check($post['email'])) {

                $password = $post['password'] ? $post['password'] : password_format($post['last_name'], $post['birthday']);
            
                $user_data = array(
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'username' => $this->input->post('email'),
                    'password' => $password,
                    'email' => $this->input->post('email'),
                    'active' => 1,
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => '',
                    'phone' => $this->input->post('contact'),
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d')
                );
    
                $user_result =  $this->ion_auth->register($post['email'], $password, $post['email'], $user_data, ['4']);
    
                if($user_result)
                {    
                        $user_permission = array(
                            'user_id' => $user_result,
                            'permission_id' => 1,
                            'created_by' => 1,
                            'created_at' => date('Y-m-d')
                        );
    
                        // $user_group_result = $this->M_ion_auth->add_to_group(1, $user_result);
                        $user_permission_result = $this->M_User_permission->insert($user_permission);
                        
                        $additional_fields = array(
                            'created_by' => $this->session->userdata('user_id'),
                            'created_at' => date('Y-m-d')
                        );
        
                        $_u_data['user_id'] = $user_result;
                        $_u_data['first_name'] = $post['first_name'];
                        $_u_data['middle_name'] = $post['middle_name'];
                        $_u_data['last_name'] = $post['last_name'];
                        $_u_data['birth_date'] = $post['birthday'];
                        $_u_data['mobile_no'] = $post['contact'];
                        $_u_data['country_id'] = '169';
                        $_u_data['regCode'] = $post['regCode'];
                        $_u_data['regCode'] = $post['regCode'];
                        $_u_data['provCode'] = $post['provCode'];
                        $_u_data['citymunCode'] = $post['citymunCode'];
                        $_u_data['brgyCode'] = $post['brgyCode'];
                        $_u_data['email'] = $post['email'];
                        $_u_data['type_id'] = $post['type_id'];
                        $_u_data['gender'] = @$post['gender'];
                        $_u_data['source_referral'] = @$post['source_referral'];
                        $_u_data['civil_status_id'] = @$post['civil_status_id'];
                        $_u_data['nationality'] = @$post['nationality'];
                        $_u_data['image'] = @$img_data['file_name'];


                        $reg_result = $this->M_applicant->insert($_u_data + $additional_fields);

                        if ($_u_data['type_id'] == 3) {
                            $_e_data['employer'] = $post['employer'];
                            $_e_data['applicant_id'] = $reg_result;
                            $this->M_applicant_employment->insert($_e_data + $additional_fields);
                        }
                      
    
                        $_u_full_name = $post['first_name'] . " " .$post['middle_name'] . " " .$post['last_name'];
    
                        $recipient['subject'] = "Alon Capital Registration";
                        $recipient['name'] = $_u_full_name;
                        $recipient['email'] = $post['email'];
    
                        // $content = "You're successfully registered to Alon Capital.";
                        $content = $this->load->view('emails/registration',$recipient,true);
                        
                        // $this->communication_library->send_email($recipient, $content);
        
                        if ($reg_result === FALSE) {
                            // Validation
                            $response['status'] = 0;
                            $response['message'] = validation_errors();
                        } else {
        
                            // Success
                            $response['status'] = 1;
                            $response['message'] = 'Successfully registered, you can access your account now';
                        }
    
                }
                else
                {
                    $response['status'] = 0;
                    $response['message'] = validation_errors();
                }
            } else {
                $response['status'] = 0;
                $response['message'] ='Email already exists.';
            }


            echo json_encode($response);
            exit();

        }


    }

    public function user_verification_get() {

        if ($this->input->post()) {

            $response['status'] = 0;

            $post = $this->input->post();

            $recipient['subject'] = "Alon Capital Forgot Password";
            $recipient['name'] = "";
            $recipient['email'] = $post['email'];

            $content = "Click this link to reset your password:" . base_url("forgot_password?email=" . $post['email']);

            $res = $this->communication_library->send_email($recipient, $content);

             // Success
            $response['status'] = 1;
            
            echo json_encode($response);
            exit();

        }

    }

    public function test_email()
    {
        $recipient['email'] = "charleskennethhernandez@gmail.com";
        $recipient['subject'] = "test";
        $content = "test";
        $res = $this->communication_library->send_email($recipient, $content);
        vdebug($res);
    }

     public function send_loan_info($loan_id = 0, $view = 0, $debug = 0)
    {   

        $recipient['email'] = "charleskennethhernandez@gmail.com";
        $recipient['subject'] = "test";
        $content = "test";

        $data['loan'] = $this->M_Loan->with_applicant()->get($loan_id);

        if ($debug) {
            vdebug($data);
        }

        if ($view) {
            $this->load->view('emails/loan',$data);
        } else {
            $content = $this->load->view('emails/loan',$data,true);
            $this->communication_library->send_email($recipient, $content);
        }

    }

    public function test_sendgrid(){
        // require 'vendor/autoload.php'; // If you're using Composer (recommended)
        require "assets/vendors/general/sendgrid-php/sendgrid-php.php";
        // Comment out the above line if not using Composer
        // require("<PATH TO>/sendgrid-php.php");
        // If not using Composer, uncomment the above line and
        // download sendgrid-php.zip from the latest release here,
        // replacing <PATH TO> with the path to the sendgrid-php.php file,
        // which is included in the download:
        // https://github.com/sendgrid/sendgrid-php/releases

        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("test@example.com", "Example User");
        $email->setSubject("Sending with SendGrid is Fun");
        $email->addTo("test@example.com", "Example User");
        $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        $email->addContent(
            "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
        );
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }

    }
    public function forgot_password() {

        if($this->input->get('email')) {

            $user_email = $this->input->get('email');
            $data=$this->M_user->user_verification($user_email);
    
            if($data)
            {
                $data['message'] = 'Success.';
            }
            else
            {
                $data['message'] = 'Not Valid User.';
            }
            $this->template->set_layout('registration');
            $this->template->build('verify', $data);

        }


    }

    public function reset_password () {

        if($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $post = $this->input->post();
            $email = $post['email'];
            $password = $post['password'];

            $user_data = $this->M_user->where("email", $email)->get();
            $user_id = $user_data['id'];

            $_u_data['password'] = $this->M_ion_auth->hash_password($password);
            
            if ($user_id) {
                $res = $this->M_user->update($_u_data, $user_id);
            }

            if($res) {
                // Success
                $response['status'] = 1;
                $response['message'] = 'Password has been changed!';
            }

            
            echo json_encode($response);
            exit();

        }
    }


    public function save_aris_files(){
        $path = $_SERVER['DOCUMENT_ROOT'] . "/aris/";
        $files = $this->scan_aris_directory($path);
    }

    public function index(){
        $post = $this->input->post();
        $project_id = $post['project_id'];
        $this->aris($project_id);
    }

    public function sync_all($count = 0){
        for ($i=1; $i < $count; $i++) { 
            $this->aris($i);
        }
    }
    public function aris($project_id = ""){
        error_reporting(E_ALL);
        $where = array();

        $tables = array('customers','documents','sales_agent','lots','payments','collectibles','ar','pd','pastdue');
        // $tables = array('ar');
        $params['where']['id'] = $project_id;
        $project_files = $this->Aris_model->get_aris('project_information',$params);
        $drop_tables = array('pd','pastdue');
        $large_tables = array('ar','payments','collectibles');
        $final_tables = array();
        $result = array();
        if ($project_files) {
            foreach ($project_files as $key => $file) {
                if ($tables) {
                    foreach ($tables as $key => $table) {
                        $total_init_count = 0;

                        $table = str_replace('_', ' ', $table);

                        $params_init['where']['project_id'] = $project_id;
                        $params_init['sort_order'] = 'DESC';
                        $params_init['row'] = '1';
                        $params_init['limit'] = '1';
                        $params_init['offset'] = '0';

                        $last_id = 0;

                        if (in_array($table, $large_tables)) {
                            if ($table == "payments") { $params_init['sort_by'] = 'PaymentID'; }
                            if ($table == "collectibles") { $params_init['sort_by'] = 'DateDue'; }
                            if ($table == "ar") { $params_init['sort_by'] = 'DocumentID'; }

                            $init = $this->Aris_model->get_aris($table,$params_init);

                            // echo "<pre>";print_r($init);echo "</pre>";die();

                            if (($table == "payments")&&($init)) { $last_id = $init['PaymentID']; }
                            if (($table == "collectibles")&&($init)) { $last_id = date('m-d-Y',strtotime($init['DateDue'])); }
                            if (($table == "ar")&&($init)) { $last_id = $init['DocumentID']; }
                        }

                        $filename = get_value_field_aris($file['Password'],'project_files','filename','password');
                        $results = $this->Aris_model->get_all_project($filename,$file['Password'],$table,$last_id); // info from sql
                        // echo "<pre>";print_r($results);echo "</pre>";die();
                         
                        if ($results) {
                            $final_tables[$table] = $this->save_project_information($results,$table);
                        }
                    }
                }
            }
        }
        $result['results'] = $final_tables;
        $result['view'] = $this->load->view('tables/reports/aris/sync_result',$result,true);
        echo json_encode($result);
    }

    public function save_project_information($results = array(),$selected = ''){
        error_reporting(E_ALL);
        $project_id = 0;

        $drop_tables = array('ar','pd','pastdue');
        $large_tables = array('payments','collectibles');
        $continue = array('ar');
        $i = 0;$u = 0;
        // echo "<pre>";print_r($results);echo "</pre>";die();
        $info_insert = array();
        $info_update = array();

        if ( !empty($results) && isset($results) ) {
            foreach ($results as $key => $result) {
                $project_info = $result['project information'];

                if ($project_info) {
                    foreach ($project_info as $key => $info) {
                        $table = "project_information";
                        $where = array('where' => array('ProjectName' => $info['ProjectName']));
                        $data = $this->Aris_model->get_aris($table,$where);
                        $project_id = @$data[0]['id'];
                        if (empty($project_id)) {
                            $data['date_created'] = date('Y-m-d H:i:s');
                            $this->Aris_model->save_aris('project_information',$info);
                        } else {
                            $data['date_updated'] = date('Y-m-d H:i:s');
                            $data['last_date_synced'] = date('Y-m-d H:i:s');
                            $info['id'] = $project_id;
                            $this->Aris_model->save_aris('project_information',$info);
                        }
                    }
                }
                
                if ( !empty($result[$selected]) && isset($result[$selected]) ) {
                    $infos = $result[$selected];
                    foreach ($infos as $key => $info) {
                        $id = 0;
                        $where = array();
                        $selected = str_replace(' ', '_', $selected);
                        $info['project_id'] = $project_id;
                        $where['project_id'] = $project_id;

                        if ($selected == "lots") {
                            $where['LotID'] = $info['LotID'];
                        }
                        if ($selected == "customers") {
                            $where['CUSTOMERID'] = $info['CUSTOMERID'];
                        }
                        if ($selected == "sales_agent") {
                            $where['SalesAgentID'] = $info['SalesAgentID'];
                        }
                        
                        if ($selected == "payments") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DATE_DUE'] = $info['DATE_DUE'];
                            $where['PaymentID'] = $info['PaymentID'];
                        }
                        
                        if ($selected == "collectibles") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DateDue'] = $info['DateDue'];
                        }
                        if ($selected == "pastdue") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DateDue'] = $info['DateDue'];
                        }
                        if ($selected == "ar") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DateDue'] = $info['DateDue'];
                        }

                        if ($selected == "pd") {
                            $where['DocumentID'] = $info['DocumentID'];
                        }
                        if ($selected == "documents") {
                            $where['DocumentID'] = $info['DocumentID'];
                        }

                        $where_params['where'] = $where; 
                        if (!in_array($selected, $continue)) {
                            $data = $this->Aris_model->get_aris($selected,$where_params);
                            $id = @$data[0]['id'];
                        }
                        if (empty($id)) {
                            $info['date_created'] = date('Y-m-d H:i:s');
                            $info['date_updated'] = date('Y-m-d H:i:s');
                            $name = "insert";
                            $i++;
                            $info_insert[] = $info;
                        } else {
                            $info_update = array();
                            $info['id'] = $id;
                            $info['date_updated'] = date('Y-m-d H:i:s');
                            $name = "update";
                            $u++;
                            $info_update[] = $info;
                        }
                    }
                    
                    if ($info_insert) {
                        $this->Aris_model->save_aris($selected,$info_insert,1);
                    }
                    
                    if ($info_update) {
                        $this->Aris_model->save_aris($selected,$info_update,2);
                    } 
                    
                }
            }
        }
        $return['insert'] = $i;
        $return['update'] = $u;
        return $return;
    }

    public function scan_aris_directory($target) {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
            foreach( $files as $file ) {
                $directory = explode('/', $file);
                if ($directory) { 
                    $filename = $directory[4];
                    if ($filename == "Sta. Maria Village.mdb") {
                        continue;
                    }
                    $filename_ext = explode('.', $filename);
                    if ( ($filename_ext) && (isset($filename_ext[1])) ) {
                        $ext = $filename_ext[1];
                        $file_name = $filename_ext[0];
                        if ($ext == "mdb") {
                            $project_file = $this->Aris_model->get_aris('project_files',array('where' => array('filename' => $file_name)));

                            if (empty($project_file)) {
                                $data['filename'] = $file_name;
                                $data['date_created'] = date('Y-m-d H:i:s');
                                $this->Aris_model->save_aris('project_files',$data);
                                echo $file_name." - Success";echo "<br>";
                            } 

                        }
                    }
                }
                $this->scan_aris_directory( $file );
            }
        } 
    }

    public function api_export_applicants($admin = "", $password = "", $debug = 0) {

        if ( ($admin == $this->admin) || ($password == $this->password) ) {

            $results = $this->M_applicant
                ->with_employment('fields:employer')
                ->order_by('id', 'DESC')
                ->get_all();

            if ($debug) {
                vdebug($results);
            }


            if ($results) {
                foreach ($results as $key => $result) {
                    $return[$key]['id'] = $result['id'];
                    $return[$key]['company_name'] = $result['id'];
                    $return[$key]['last_name'] = $result['last_name'];
                    $return[$key]['first_name'] = $result['first_name'];
                    $return[$key]['middle_name'] = $result['middle_name'];
                    $return[$key]['spouse_name'] = $result['spouse_name'];
                    $return[$key]['birth_place'] = $result['birth_place'];
                    $return[$key]['birth_date'] = $result['birth_date'];
                    $return[$key]['gender'] = $result['gender'];
                    $return[$key]['nationality'] = $result['nationality'];
                    $return[$key]['civil_status_id'] = $result['civil_status_id'];
                    $return[$key]['present_address'] = $result['present_address'];
                    $return[$key]['mailing_address'] = $result['mailing_address'];
                    $return[$key]['business_address'] = $result['business_address'];
                    $return[$key]['mobile_no'] = $result['mobile_no'];
                    $return[$key]['other_mobile'] = $result['other_mobile'];
                    $return[$key]['email'] = $result['email'];
                    $return[$key]['landline'] = $result['landline'];
                    $return[$key]['social_media'] = $result['social_media'];
                    $return[$key]['regCode'] = $result['regCode'];
                    $return[$key]['provCode'] = $result['provCode'];
                    $return[$key]['citymunCode'] = $result['citymunCode'];
                    $return[$key]['brgyCode'] = $result['brgyCode'];
                    $return[$key]['sitio'] = $result['sitio'];
                }
            }

            if (count($results) == 0) {
                echo json_encode(array( 'success' => 0 ));
            } else {
                echo json_encode(array('data' => $return, 'count' => count($return), 'success' => 1));
            }

        } else {
            
            echo json_encode(array('message' => "wrong api credential", 'success' => 0));

        }
    }
    
    public function api_import_applicant() {

        $post = $_POST;

        if ( ($this->admin == $post['admin']) || ($this->password == $post['password']) ) {

            if(!$this->M_auth->email_check($post['email'])) {

                $password = $post['password'] ? $post['password'] : password_format($post['last_name'], $post['birthday']);
            
                $user_data = array(
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'username' => $this->input->post('email'),
                    'password' => $password,
                    'email' => $this->input->post('email'),
                    'active' => 1,
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => '',
                    'phone' => $this->input->post('contact'),
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d')
                );
    
                $user_result =  $this->ion_auth->register($post['email'], $password, $post['email'], $user_data, ['4']);
    
                if($user_result)
                {    
                        $user_permission = array(
                            'user_id' => $user_result,
                            'permission_id' => 1,
                            'created_by' => 1,
                            'created_at' => date('Y-m-d')
                        );
    
                        // $user_group_result = $this->M_ion_auth->add_to_group(1, $user_result);
                        $user_permission_result = $this->M_User_permission->insert($user_permission);
                        
                        $additional_fields = array(
                            'created_by' => $this->session->userdata('user_id'),
                            'created_at' => date('Y-m-d')
                        );
        
                        $_u_data['user_id'] = $user_result;
                        $_u_data['first_name'] = $post['first_name'];
                        $_u_data['middle_name'] = $post['middle_name'];
                        $_u_data['last_name'] = $post['last_name'];
                        $_u_data['birth_date'] = $post['birthday'];
                        $_u_data['mobile_no'] = $post['contact'];
                        $_u_data['regCode'] = $post['regCode'];
                        $_u_data['provCode'] = $post['provCode'];
                        $_u_data['citymunCode'] = $post['citymunCode'];
                        $_u_data['brgyCode'] = $post['brgyCode'];
                        $_u_data['email'] = $post['email'];
                        $_u_data['type_id'] = $post['type_id'];
                        $_u_data['gender'] = @$post['gender'];
                        $_u_data['source_referral'] = @$post['source_referral'];
                        $_u_data['civil_status_id'] = @$post['civil_status_id'];
                        $_u_data['nationality'] = @$post['nationality'];
                        $_u_data['image'] = @$img_data['file_name'];


                        $reg_result = $this->M_applicant->insert($_u_data + $additional_fields);

                        if ($_u_data['type_id'] == 3) {
                            $_e_data['employer'] = $post['employer'];
                            $_e_data['applicant_id'] = $reg_result;
                            $this->M_applicant_employment->insert($_e_data + $additional_fields);
                        }
                      
    
                        $_u_full_name = $post['first_name'] . " " .$post['middle_name'] . " " .$post['last_name'];
    
                        $recipient['subject'] = "Alon Capital Registration";
                        $recipient['name'] = $_u_full_name;
                        $recipient['email'] = $post['email'];
    
                        // $content = "You're successfully registered to Alon Capital.";
                        $content = $this->load->view('emails/registration',$recipient,true);
                        
                        $this->communication_library->send_email($recipient, $content);
        
                        if ($reg_result === FALSE) {
                            // Validation
                            $response['status'] = 0;
                            $response['message'] = validation_errors();
                        } else {
        
                            // Success
                            $response['status'] = 1;
                            $response['message'] = 'Successfully registered, you can access your account now';
                        }
    
                }
                else
                {
                    $response['status'] = 0;
                    $response['message'] = validation_errors();
                }
            } else {
                $response['status'] = 0;
                $response['message'] ='Email already exists.';
            }

            if ($reg_result === FALSE) {
                echo json_encode(array( 'success' => 0 ));
            } else {
                echo json_encode(array('data' => $response, 'count' => count($return), 'success' => 1));
            }

        } else {
            
            echo json_encode(array('message' => "wrong api credential", 'success' => 0));

        }
    }  

    public function api_import_loan_application() {

        $post = $_POST;

        if ( ($this->admin == $post['admin']) || ($this->password == $post['password']) ) {

            if(!$this->M_auth->email_check($post['email'])) {

                $password = $post['password'] ? $post['password'] : password_format($post['last_name'], $post['birthday']);
            
                $user_data = array(
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'username' => $this->input->post('email'),
                    'password' => $password,
                    'email' => $this->input->post('email'),
                    'active' => 1,
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => '',
                    'phone' => $this->input->post('contact'),
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d')
                );
    
                $user_result =  $this->ion_auth->register($post['email'], $password, $post['email'], $user_data, ['4']);
    
                if($user_result)
                {    
                        $user_permission = array(
                            'user_id' => $user_result,
                            'permission_id' => 1,
                            'created_by' => 1,
                            'created_at' => date('Y-m-d')
                        );
    
                        // $user_group_result = $this->M_ion_auth->add_to_group(1, $user_result);
                        $user_permission_result = $this->M_User_permission->insert($user_permission);
                        
                        $additional_fields = array(
                            'created_by' => $this->session->userdata('user_id'),
                            'created_at' => date('Y-m-d')
                        );
        
                        $_u_data['user_id'] = $user_result;
                        $_u_data['first_name'] = $post['first_name'];
                        $_u_data['middle_name'] = $post['middle_name'];
                        $_u_data['last_name'] = $post['last_name'];
                        $_u_data['birth_date'] = $post['birthday'];
                        $_u_data['mobile_no'] = $post['contact'];
                        $_u_data['regCode'] = $post['regCode'];
                        $_u_data['provCode'] = $post['provCode'];
                        $_u_data['citymunCode'] = $post['citymunCode'];
                        $_u_data['brgyCode'] = $post['brgyCode'];
                        $_u_data['email'] = $post['email'];
                        $_u_data['type_id'] = $post['type_id'];
                        $_u_data['gender'] = @$post['gender'];
                        $_u_data['source_referral'] = @$post['source_referral'];
                        $_u_data['civil_status_id'] = @$post['civil_status_id'];
                        $_u_data['nationality'] = @$post['nationality'];
                        $_u_data['image'] = @$img_data['file_name'];


                        $reg_result = $this->M_applicant->insert($_u_data + $additional_fields);

                        if ($_u_data['type_id'] == 3) {
                            $_e_data['employer'] = $post['employer'];
                            $_e_data['applicant_id'] = $reg_result;
                            $this->M_applicant_employment->insert($_e_data + $additional_fields);
                        }
                      
    
                        $_u_full_name = $post['first_name'] . " " .$post['middle_name'] . " " .$post['last_name'];
    
                        $recipient['subject'] = "Alon Capital Registration";
                        $recipient['name'] = $_u_full_name;
                        $recipient['email'] = $post['email'];
    
                        // $content = "You're successfully registered to Alon Capital.";
                        $content = $this->load->view('emails/registration',$recipient,true);
                        
                        $this->communication_library->send_email($recipient, $content);
        
                        if ($reg_result === FALSE) {
                            // Validation
                            $response['status'] = 0;
                            $response['message'] = validation_errors();
                        } else {
        
                            // Success
                            $response['status'] = 1;
                            $response['message'] = 'Successfully registered, you can access your account now';
                        }
    
                }
                else
                {
                    $response['status'] = 0;
                    $response['message'] = validation_errors();
                }
            } else {
                $response['status'] = 0;
                $response['message'] ='Email already exists.';
            }

            if ($reg_result === FALSE) {
                echo json_encode(array( 'success' => 0 ));
            } else {
                echo json_encode(array('data' => $response, 'count' => count($return), 'success' => 1));
            }

        } else {
            
            echo json_encode(array('message' => "wrong api credential", 'success' => 0));

        }
    }    

    //api_import_loan_application
    //api_export_loan_disbursement
    //api_import_loan_collection_application
}
