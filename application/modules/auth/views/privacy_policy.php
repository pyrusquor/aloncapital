<div class="content">
   <!-- BEGIN REGISTRATION FORM -->
      <div class="kt-login__head">
          <a href="<?=base_url();?>">
            <h3 class="kt-login__title"> 
              <img width="210px;" src="<?php echo base_url("assets/media/bg/alon_homepage_logo.svg"); ?>">
              Loan Management System
          </h3>
         </a>
      </div>

      <h3 class="font-green">Terms of Service & Privacy Policy</h3>

      <br>
      <div class="row">
         <div class="col-md-12">
            <h4 class="font-green">1. Privacy Policy</h4>

            <p>This Privacy Policy statement (this &ldquo;Policy&rdquo;) sets out the policy of Alon Capital Tech Lending Inc. (&ldquo;Alon Capital&rdquo; or we/us/our) in relation to the collection, storage, use, and protection of your Personal Information. We may have to update this Policy from time to time to comply with Applicable Law or to reflect changes in our company policy. We will promptly inform you of any changes by posting the updated Policy on our website.</p>

            <p>When you continue to maintain your account with us or when you continue to avail of our service, we take it to mean that you have read, understood, and agreed to the collection, storage, use and sharing of your Personal Information, in accordance with this Policy.</p>

            <p>Applicable Law means any and all law, rules and regulations of the Republic of the Philippines in respect of personal information, right to privacy, data privacy, information security and the like, including the Data Privacy Act of 2012 (Republic Act No. 10173) as may be amended from time to time.</p>

            <h4 class="font-green">2. Collection of Personal Information</h4>

            <p>We collect your Personal Information so we can be of service to you. Personal Information is any information from which the identity of an individual can be reasonably and directly ascertained by an entity holding the information, or when put together with other information would directly and certainly identify an individual, such as name, gender, date of birth, address, telephone number, mobile number, and email address, among others.</p>

            <p>We may request you to update your Personal Information as needed. Thus, we (Alon Capital) may be unable (and/or reserves the right) to provide you the full benefit of our services if we do not receive complete and updated Personal Information from you when so requested.</p>

            <p>What data do we collect?</p>

            <p>(Information processed and collected by Alon Capital)</p>

            <p>Alon Capital collects only data/information that Clients provide us. In this Privacy Policy, werefer to all of this as &ldquo;Information&rdquo;.</p>

            <p>As one of our Clients. Alon Capital requires/requests that the following information be provided:</p>

            <p>Full Name; <br>Date of Birth; <br>Contact Details; Address; <br>Credit information, such as bank account details, employment details (position, employment status, employee ID number, date hired), government ID number (SSS/GSIS/TIN), salary range; <br>Demographic information, such as gender; <br>Other information as required in our online platforms, relevant to our purpose. 

            <h4 class="font-green">3. Use of Personal Information</h4>

            <p>We use your Personal Information to provide you with a simple online platform that can be accessed anytime and anywhere, specifically to facilitate the pre-screening of borrowers. We also use your Personal Information to: (a) communicate relevant products and advisories to you; and (b) process information for statistical and research purposes.</p>

            <p>We may be required to disclose your Personal Information to comply with legal requirements or processes, such as a court order, or to comply with a legal obligation, or to prevent imminent harm to public security, safety or order. We also disclose or share your Personal Information to credit information or investigation companies, banks, credit bureaus (including the Credit Information Corporation created pursuant to Republic Act No. 9510), credit protection provider or guarantee institutions, for reports on, among others, basic credit data, payment history and defaulting accounts.</p>

            <p>We may share your Personal Information with our affiliates or outsource the processing of Personal Information to third parties, such as vendors, service providers, or partners, to fulfill any of the above purposes. They are only authorized to use Personal Information for such contracted purposes. They may have access to Personal Information for a limited time under reasonable contractual and technical safeguards to limit their use of such information.</p>

            <p>We assure you that we will ask for your consent before we use, process or share your Personal Information for any other purpose, subject to existing data protection policies, data sharing agreement, in accordance with the Data Privacy Act of 2012, its implementing rules and regulations.</p>

            <p>How does Alon Capital collect and process Information (Personal or otherwise) provided by the Client / Data Subject?</p>

            <p>Alon Capital requires its clients/data subjects to sign up / fill up an online form. Verificationis done by its technical support team to classify online applications as active/verified orinactive/unverified accounts/data subjects. After 365 days, unverified accounts are segregated and anonymized. The rest of the unverified applicants&rsquo; information aredeleted and disposed of from the database of Alon Capital.</p>

            <p>Collected, processed and stored information are then used to analyze the salary range, demographic information and other relevant information necessary for the approval andprocessing of loans of the applicant / client / data subject. No disclosure of informationare is made to any third party from the verification process to the execution, effectivity, and the whole duration of contract of loan with the applicant, client / data subject.</p>

            <p>Disclosure of personal information or sensitive personal information in accordance withany legitimate purpose as herein mentioned to partner banks and/or financial institutions shall be covered by a specific data sharing agreement in accordance with the requirements of the Data Privacy Act.</p>

            <p>Processing of personal data/information are is encrypted with our existing dataprotection tools/technologies. Personal data/information anonymized and stored are protected in the same way for a limited period of 5 years. Alon Capital may engage third party technology or service providers for this purpose, subject to existing requirements for data privacy and protection of personal data/information.</p>

            <h4 class="font-green">4. Protection of Personal Information</h4>

            <p>Your privacy and security are important to us. We secure and protect your Personal Information with appropriate safeguards to ensure privacy, prevent loss or unauthorized use, all in compliance with Applicable Law.</p>

            <p>We have put in place reasonable and appropriate security arrangements and measures that use physical, electronic, and procedural safeguards to protect Personal Information. We utilize a secured server with a firewall, encryption, and other appropriate security controls. All information supplied through our website is encrypted using Secure Sockets Layer (SSL) technology so that it cannot be intercepted, hacked or read as it is transmitted over the internet. We have an internal protocol mandating regular review of our information collection, storage, and processing practices, including physical security measures. These are all done to protect your Personal Information against unauthorized access, alteration, disclosure, or destruction.</p>

            <p>Your Personal Information can only be accessed or processed by certain authorized personnel who are under strict instructions to treat it with utmost confidentiality. These authorized personnel are required by contract to act appropriately and are subject to technical controls. They will be held responsible and accountable if they fail to observe these requirements and controls.</p>

            <p>We do not sell or trade your Personal Information to anyone for any purpose, other than those stated in this Policy. We will only share or disclose Personal Information in strict accordance with this Policy, the Data Privacy Act of 2012, its implementing rules, regulations and other relevant laws and issuances.</p>

            <p>Your Personal Information will be in our records or stored in our systems for as long as it is necessary to fulfill the purpose for which it was collected, or while it is needed by us for business, tax, or legal purposes. We take appropriate measures to protect your Personal Information when it is time to dispose of the same.</p>

            <h4 class="font-green">5. Privacy Principles We are guided by the following privacy principles:</h4>

            <p>(a) Transparency</p>

            <p>We are committed to keeping you informed in clear and plain language of the nature, purpose, and extent of the processing of your Personal Information, including the risks and safeguards involved, your rights as a data subject, and how these can be exercised. Your Personal Information will only be collected, stored, used and shared if you give your consent. You have the right to object or to withdraw your consent.</p>

            <p>(b) Legitimate purpose</p>

            <p>The processing of Personal Information will always be compatible with the declared and specified purposes as provided in this Policy.</p>

            <p>(c) Proportionality</p>

            <p>The processing of Personal Information will be adequate, relevant, suitable, necessary, and not excessive in relation to the declared and specified purposes as stated in this Policy.</p>

            <p>(d) Security</p>

            <p>We will protect your Personal Information, including the quality and accuracy thereof, using appropriate safeguards as required by Appropriate Law and as provided in this Policy.</p>

            <h4 class="font-green">6. What are the data subject&rsquo;s rights?</h4>

            <p>Alon Capital, as much as reasonably possible, would like to make sure you are fully aware of all of your data protection rights. Every client/data subject may exercise the following rights:</p>

            <p>The right to access - Clients/data subjects have the right to request from our DPO for copies of personal data they have provided and collected through this platform. The firm may charge you a small fee for this service.</p>

            <p>The right to rectification - Clients/data subjects have the right to request that Alon Capital correct any information believed to be inaccurate. You also have the right to request Alon Capital to complete information the Client/data subject believes to be incomplete.</p>

            <p>The right to erasure - Clients/data subjects have the right to request that Alon Capital erase Clients/data subject&rsquo;s personal data, under certain conditions.</p>

            <p>The right to restrict processing - Clients/data subjects have the right to request that Alon Capital restrict the processing of Clients/data subject&rsquo;s personal data, under certain conditions.</p>

            <p>The right to object to processing - Clients/data subjects have the right to object to Alon Capital's processing of Clients/data subject&rsquo;s personal data, under certain conditions.</p>

            <p>The right to data portability - Clients/data subjects have the right to request that Alon Capital transfer the data that we have collected to another organization or firm, or directly to Clients/data subjects, under certain conditions.</p>

            <h4 class="font-green">Amendments &amp; Revisions to this Policy</h4> Alon Capital Privacy Policy may be updated from time to time. Please periodically review this statement in our website and our principal place of business to be apprised of our latest privacy policy. This policy was last updated on October 2020.</p>

            <p>By visiting our website / online platform and/or by submitting the required information inour online forms, the client/data subject expressly consents to the processing,collection, controlled disclosure, and use of the User&rsquo;s personal information inaccordance with this privacy policy.</p>
         </div>
      </div>
   <!-- END REGISTRATION FORM -->
</div>

<style type="text/css">
   .form-group{
      margin-bottom: 0px;
   }
   .control-label{
      color: #51ca12 !important;
      font-weight: bold !important;
   }
   .login .content h3,.login .content h4{
      text-align: left !important;
      color : #0A284D !important;
      font-weight: bold !important;
   }
</style>