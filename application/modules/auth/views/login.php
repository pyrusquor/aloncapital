<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
        
        <div class="container"><div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style=""> <!-- background-image: url(<?php echo base_url("assets/media/bg/bg-2.jpg"); ?>); -->
                <div class="row">

                <div class="col-md-6 left-login">

                <div class="kt-login__logo">
                        <a href="#">
                            <img src="<?php //echo base_url("assets/media/logos/REMS-logo-green.png"); ?>">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title"> 
                                <img width="210px;" src="<?php echo base_url("assets/media/bg/alon_homepage_logo.png"); ?>">
                                
                            </h3>
                            <br>
                            <p class="">Loan Management System<br>Welcome Back, Login To Continue.</p>
                        </div>
                        <div class="divider"></div>
                        <form class="kt-form" action="">
                            <div class="kt-input-icon kt-input-icon--left input-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-envelope"></i></span>
                            </div>
                            <div class="kt-input-icon kt-input-icon--left input-group">
                                <input class="form-control" type="password" placeholder="Password" name="password">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-lock"></i></span>
                            </div>
                            <div class="row kt-login__extra">
                                <div class="col">
                                    <label class="kt-checkbox">
                                        <input class="form-control" type="checkbox" name="remember"> Remember me
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col kt-align-right">
                                    <a href="javascript:;" id="kt_login_forgot" class="kt-login__link">Forget Password ?</a>
                                </div>
                            </div>
                            <div class="kt-login__actions custom_login d-flex justify-content-between">
                                <button id="kt_login_signin_submit" class="btn btn-brand btn-pill kt-login__btn-primary">Sign In</button>
                                <a href="<?php echo base_url("registration") ?>" class="btn btn-brand btn-pill kt-login__account-link login__create-account">Register</a>
                            </div>
                        </form>
                    </div>
                    <!-- <div class="kt-login__signup">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Sign Up</h3>
                            <div class="kt-login__desc">Enter your details to create your account:</div>
                        </div>
                        <form class="kt-form" action="">
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Fullname" name="fullname">
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="password" placeholder="Password" name="password">
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="password" placeholder="Confirm Password" name="rpassword">
                            </div>
                            <div class="row kt-login__extra">
                                <div class="col kt-align-left">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="agree">I Agree the <a href="#" class="kt-link kt-login__link kt-font-bold">terms and conditions</a>.
                                        <span></span>
                                    </label>
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="kt-login__actions">
                                <button id="kt_login_signup_submit" class="btn btn-brand btn-pill kt-login__btn-primary">Sign Up</button>&nbsp;&nbsp;
                                <button id="kt_login_signup_cancel" class="btn btn-secondary btn-pill kt-login__btn-secondary">Cancel</button>
                            </div>
                        </form>
                    </div> -->
                    <div class="kt-login__forgot">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Forgotten Password ?</h3>
                            <div class="kt-login__desc">Enter your email to reset your password:</div>
                        </div>
                        <form class="kt-form" action="">
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
                            </div>
                            <div class="kt-login__actions">
                                <button id="kt_login_forgot_submit" class="btn btn-brand btn-pill kt-login__btn-primary">Request</button>&nbsp;&nbsp;
                                <button id="kt_login_forgot_cancel" class="btn btn-secondary btn-pill kt-login__btn-secondary">Cancel</button>
                            </div>
                        </form>
                    </div>

                    
                    <!-- <div class="kt-login__account">
                        <span class="kt-login__account-msg">
                            Don't have an account yet ?
                        </span>
                        &nbsp;&nbsp;
                        <a href="javascript:;" id="kt_login_signup" class="kt-login__account-link">Sign Up!</a>
                    </div> -->

                </div>
                    <div class="col-md-6 right-image">
                        <div class="login-bg-image">
                        <!-- <img src="<?php echo base_url("assets/media/bg/Login-BG.png"); ?>"> -->
                            <ul class="bxslider">
                                <li><img src="<?php echo base_url("assets/media/bg/alon1.jpg"); ?>"></li>
                                <li><img src="<?php echo base_url("assets/media/bg/alon2.jpg"); ?>"></li>
                                <li><img src="<?php echo base_url("assets/media/bg/alon3.jpg"); ?>"></li>             
                            </ul>
                        </div>
                    </div>
                </div>

            </div>


            <footer>
                <div>
                    <p>Copyright <?=date('Y');?> Loan Management System | All Rights Reserved</p>
                </div>
            </footer>


            

</div>
        
        </div>

    </div>
</div>
<!-- end:: Page -->

