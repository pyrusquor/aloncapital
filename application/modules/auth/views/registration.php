<!-- BEGIN LOGIN -->
<?php
    $regions = $this->_ci_cached_vars[0];
    $provinces = $this->_ci_cached_vars[1];
    $cities = $this->_ci_cached_vars[2];
    $barangays = $this->_ci_cached_vars[3];
    $permissions = $this->_ci_cached_vars[4];
?>

<?php
    $source_referral = ( isset($_GET['ref']) && !empty($_GET['ref']) ? 5 : 0 )
?>
<div class="content">
   <div class="kt-login__head">
       <h3 class="kt-login__title"> 
           <img width="210px;" src="<?php echo base_url("assets/media/bg/alon_homepage_logo.png"); ?>">
           Loan Management System
       </h3>
   </div>
   <!-- BEGIN REGISTRATION FORM -->
   <form id="registration_form" action="" method="post">
      <h3 class="font-green">Registration</h3>
      <p class="hint"> Welcome to Alon Capital! </p>
      <p>Start your journey with Alon by providing all required information below</p>
      
      <!-- <div class="form-group">
         <label class="control-label ">Username</label>
         <input class="form-control" type="text" placeholder="Username" name="username" /> 
      </div> -->
      
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                   <label class="control-label ">Application Loan Type *</label>
                   <select class="form-control" id="type_id" name="type_id">
                      <option value="1" selected>SME Loan Account</option>
                      <!-- <option value="2" >Personal Loan Account</option> -->
                      <!-- <option value="3">Salary Loan Account</option> -->
                  </select>
                </div>
            </div>

            <div class="col-md-6 hide">
                <div class="form-group for_salary_loan ">
                   <label class="control-label">Employer *</label>
                   <select class="form-control" name="employer">
                      <option>Select Employer</option>
                      <option value="1">Mother's Best</option>
                      <option value="2">KMC</option>
                   </select>
                </div>
            </div>

        </div>


        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                   <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                   <label class="control-label ">Email *</label>
                   <input class="form-control" type="email" placeholder="Email" name="email" /> 
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                   <label class="control-label ">Desired Password *</label>
                   <input class="form-control" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" /> 
                </div>
            </div>

        </div>


        <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                   <label class="control-label ">First Name *</label>
                   <input class="form-control" type="text" placeholder="First Name" name="first_name" /> 
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                   <label class="control-label ">Middle Name *</label>
                   <input class="form-control" type="text" placeholder="Middle Name" name="middle_name" /> 
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                   <label class="control-label ">Last Name *</label>
                   <input class="form-control" type="text" placeholder="Last Name" name="last_name" /> 
                </div>
            </div>

        </div>
           
            
        
        <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                   <label class="control-label">Birthday (yyyy-mm-dd) *</label>
                   <input class="form-control datePicker" type="text" placeholder="Birthday" name="birthday" /> 
                </div>
            </div>

            <div class="col-md-4">

                 <div class="form-group">
                   <label class="control-label">Gender *</label>
                   <?php echo form_dropdown('gender', Dropdown::get_static('sex'), set_value('gender', @$gender), 'class="form-control" id="gender"'); ?>
                </div>

            </div>

            <div class="col-md-4">

                  <div class="form-group">
                   <label class="control-label">Civil Status *</label>
                   <?php echo form_dropdown('civil_status_id', Dropdown::get_static('civil_status'), set_value('[civil_status]', @$civil_status_id), 'class="form-control" id="civil_status_id"'); ?>
                </div>
            </div>

        </div>
            
        
        <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                   <label class="control-label">Nationality *</label>
                   <?php echo form_dropdown('nationality', Dropdown::get_static('nationality'), set_value('nationality', 62), 'class="form-control" id="nationality"'); ?>
                </div>
            </div>


            <div class="col-md-4">
               <div class="form-group">
                   <label class="control-label ">Mobile Number *</label>
                   <input class="form-control form-control-solid" type="number" placeholder="Mobile Number" name="contact" /> 
                </div>
            </div> 

            <?php $src_hide = ""; if ($source_referral == 5): 
                $src_hide = "hide"; 
            endif ?>
            
            <div class="col-md-4 <?=$src_hide;?>">
                <div class="form-group">
                   <label class="control-label ">Referral Source</label>
                   <?php echo form_dropdown('source_referral', Dropdown::get_static('source_referral_type'), set_value('source_referral', @$source_referral), 'class="form-control" id="source_referral"'); ?>
                </div>
            </div>
        </div>
            
         
        <div class="row">
             <div class="col-md-4">
                <div class="form-group">
                   <label class="control-label ">Region</label>
                   <select class="form-control" id="region" name="regCode">
                      <option value="">Select Region</option>
                      <?php foreach ($regions as $value): ?>
                          <option value="<?php echo @$value['regCode']; ?>"><?php echo $value['regDesc']; ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
             </div>
             <div class="col-md-4">
                <div class="form-group">
                   <label class="control-label ">Province</label>
                   <select class="form-control" id="province" name="provCode">
                      <option value="">Select Province</option>
                      <?php foreach ($provinces as $value): ?>
                          <option value="<?php echo $value['provCode']; ?>"><?php echo $value['provDesc']; ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
             </div>
             <div class="col-md-4">
                 <div class="form-group">
                   <label class="control-label ">City</label>
                   <select class="form-control" id="city" name="citymunCode">
                      <option value="">Select City</option>
                      <?php foreach ($cities as $value): ?>
                          <option value="<?php echo $value['citymunCode']; ?>"><?php echo $value['citymunDesc']; ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
             </div>
             <div class="col-md-4">
                 <div class="form-group d-none">
                   <label class="control-label ">Barangay</label>
                   <input class="form-control" type="text" placeholder="Barangay" name="brgyCode" /> 
                </div>
             </div>
        </div>
            
        <div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label ">Profile Picture</label>
                    <input class="form-control" type="file" name="image" class="" aria-invalid="false">
                </div>
            </div>

        </div>
        <br>    
        <br>    
        <br>    
        <div class="row">
            <div class="col-md-12">
                <div class="kt-login__head">
                    <h3 class="kt-login__title"> 
                        Generate Loan Computation
                        <a href="javascript:void(0)" id="loan_modal_btn" data-target="#loanModal" class="btn btn-success uppercase pull-right">Click to view Loan Requirements</a>
                    </h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Loan Amount <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="number" class="form-control" id="loan_amount" placeholder="Amount" name="" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label>Loan Duration (Maximum of 6 mos only) <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="number" min="1" max="6" class="form-control" id="loan_period" name="" value="6"  max placeholder="(MOS)" autocomplete="off">
                    </div>
                    <span class="form-text text-muted"></span>
                    
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-3 hide">Monthly Interest: <div>3%</div></div>
            <div class="col-md-3 hide">One-Time Processing Fee : <div id="processing_fee"></div> </div>
            <div class="col-md-3 hide">Transaction Fee : <div>3%</div></div>
            <div class="col-md-3">Monthly Payable : <div id="monthly_payable"></div> </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <a href="javascript:void(0)" id="calculateloan" class="btn btn-success uppercase pull-right">Calculate</a>
            </div>

        </div>


      <div class="form-group margin-top-20 margin-bottom-20">
         <label class="mt-checkbox mt-checkbox-outline">
            <input type="checkbox" name="agree" /> I agree to the
            <a href="<?=base_url();?>privacy_policy" target="_BLANK">Terms of Service & Privacy Policy </a>
            <span></span>
         </label>
         <div id="register_tnc_error"> </div>
      </div>

      <div class="form-actions">
         <a href="<?php echo base_url("login") ?>" class="btn btn-light uppercase ">Back</a>
         <button type="submit" id="registration_form_btn" class="btn btn-success uppercase pull-right">Submit</button>
      </div>

   </form>
   <!-- END REGISTRATION FORM -->
</div>

<style type="text/css">
   .form-group{
      margin-bottom: 0px;
   }
   .control-label{
      color: #0A284D !important;
      font-weight: bold !important;
   }
   .login .content h3{
      text-align: left !important;
      color : #0A284D !important;
      font-weight: bold !important;
   }
</style>

<div class="modal fade" id="loanModal" tabindex="-1" role="dialog" aria-labelledby="loanModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Loan Requirements</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <h1>Requirements</h1>

                <ul>
                    <li>Must be at least 18 years of age</li>
                    <li>Must be a Filipino citizen</li>
                    <li>At least one (1) year in business operation</li>
                </ul>

                <h1>Please prepare the following documents:</h1>

                <h3>Primary Requirements:</h3>
                <ol>
                    <li>Last 3 months Bank Statements of the company (checking account) and the signatories (either savings or checking account)</li>
                    <li>Latest Audited Financial Statements and Latest Income Tax Returns (AFS/ITR)</li>
                    <li>Company Registration Documents (DTI/GIS/COR2303/SEC/Articles of Incorporation)</li>
                    <li>Business Permit</li>
                </ol>

                <h3>Secondary Requirements:</h3>
                <ol>
                        <li>Company Profile</li>
                        <li>1 Valid ID (w/ address) of the signatories / owner of the company</li>
                        <li>1 Proof of Billing</li>
                </ol>

                <h3>Additional Requirements for Co-Borrower:</h3>
                <ol>
                    <li>1. Latest 3 months bank statement</li>
                    <li>2. 1 Valid ID (w/ address)</li>
                    <li>3. 1 Proof of Billing</li>
                </ol>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>