<?php 
    $email = @$this->_ci_cached_vars['email']; 
    $email = @$_GET['email']; 
?>

<div class="content">
    <!-- BEGIN REGISTRATION FORM -->
    <form id="registration_form" action="" method="post">
        <h3 class="font-green mb-5">Reset Password</h3>
        <input type="text" name="email" class="d-none" value="<?php echo $email ?>">
        <div class="form-group">
            <label class="control-label ">New Password</label>
            <input class="form-control" id="password" type="password" placeholder="New Password" name="password" /> 
        </div>

        <div class="form-actions">
            <button type="submit" id="change_password_form_btn" class="btn btn-success uppercase pull-right">Submit</button>
        </div>
    </form>
    <!-- END REGISTRATION FORM -->
</div>