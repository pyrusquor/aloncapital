<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Initial_warehouse_inventory_logs extends CI_Migration
    {
        protected $tbl = "warehouse_inventory_logs";
        protected $fields = array(

            "id" => array(

                "type" => "INT",


                "constraint" => "11",


                "unsigned" => True,


                "auto_increment" => True,


                "NOT NULL" => True,


            ),

            "created_by" => array(

                "type" => "INT",


                "NOT NULL" => True,


                "unsigned" => True,


            ),

            "created_at" => array(

                "type" => "DATETIME",


                "NOT NULL" => True,


            ),

            "updated_by" => array(

                "type" => "INT",


                "NOT NULL" => True,


                "unsigned" => True,


            ),

            "updated_at" => array(

                "type" => "DATETIME",


                "NOT NULL" => True,


            ),

            "deleted_by" => array(

                "type" => "INT",


                "NOT NULL" => True,


                "unsigned" => True,


            ),

            "deleted_at" => array(

                "type" => "DATETIME",


                "NOT NULL" => True,


            ),

            "warehouse_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NULL" => True,


            ),

            "warehouse_inventory_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NULL" => True,


            ),

            "sub_warehouse_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NULL" => True,


            ),

            "sub_warehouse_inventory_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NULL" => True,


            ),

            "virtual_warehouse_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NULL" => True,


            ),

            "virtual_warehouse_inventory_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NULL" => True,


            ),

            "item_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NOT NULL" => True,


            ),

            "unit_of_measurement_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NOT NULL" => True,


            ),

            "actual_quantity_in" => array(

                "type" => "DOUBLE",


                "constraint" => "20,2",


                "NOT NULL" => True,


                "default" => "0",


            ),

            "actual_quantity_out" => array(

                "type" => "DOUBLE",


                "constraint" => "20,2",


                "NOT NULL" => True,


                "default" => "0",


            ),

            "available_quantity_in" => array(

                "type" => "DOUBLE",


                "constraint" => "20,2",


                "NOT NULL" => True,


                "default" => "0",


            ),

            "available_quantity_out" => array(

                "type" => "DOUBLE",


                "constraint" => "20,2",


                "NOT NULL" => True,


                "default" => "0",


            ),

            "unit_price" => array(

                "type" => "DECIMAL",


                "constraint" => "20,2",


                "NULL" => True,


                "default" => "0",


            ),

            "remarks" => array(

                "type" => "VARCHAR",


                "constraint" => "255",


                "NULL" => True,


            ),

            "transaction_type" => array(

                "type" => "INT",


                "constraint" => "2",


                "NOT NULL" => True,


                "default" => "1",


            ),

            "current_actual_quantity" => array(

                "type" => "DOUBLE",


                "constraint" => "20,2",


                "NOT NULL" => True,


            ),

            "current_available_quantity" => array(

                "type" => "DOUBLE",


                "constraint" => "20,2",


                "NOT NULL" => True,


            ),

        );

        public function up()
        {
            if (!$this->db->table_exists($this->tbl)) {
                $this->dbforge->add_field($this->fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->tbl, TRUE);
            }

        }

        public function down()
        {
            if ($this->db->table_exists($this->tbl)) {
                $this->dbforge->drop_table($this->tbl);
            }
        }
    }
