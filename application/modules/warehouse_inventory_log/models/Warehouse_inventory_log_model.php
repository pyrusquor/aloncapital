<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Warehouse_inventory_log_model extends MY_Model
    {
        public $table = 'warehouse_inventory_logs'; // you MUST mention the table name
        public $primary_key = 'id';
        public $fillable = [
            "id",
            "created_by",
            "created_at",
            "updated_by",
            "updated_at",
            "deleted_by",
            "deleted_at",
            "warehouse_id",
            "warehouse_inventory_id",
            "sub_warehouse_id",
            "sub_warehouse_inventory_id",
            "virtual_warehouse_id",
            "virtual_warehouse_inventory_id",
            "item_id",
            "unit_of_measurement_id",
            "actual_quantity_in",
            "actual_quantity_out",
            "available_quantity_in",
            "available_quantity_out",
            "unit_price",
            "remarks",
            "transaction_type",
            "current_actual_quantity",
            "current_available_quantity",

        ];
        public $form_fillables = [
            "id",


            "warehouse_id",
            "warehouse_inventory_id",
            "sub_warehouse_id",
            "sub_warehouse_inventory_id",
            "virtual_warehouse_id",
            "virtual_warehouse_inventory_id",
            "item_id",
            "unit_of_measurement_id",
            "actual_quantity_in",
            "actual_quantity_out",
            "available_quantity_in",
            "available_quantity_out",
            "unit_price",
            "remarks",
            "transaction_type",


        ];
        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [


            "warehouse_id" => array(
                "field" => "warehouse_id",
                "label" => "Warehouse",
                "rules" => "numeric"
            ),

            "warehouse_inventory_id" => array(
                "field" => "warehouse_inventory_id",
                "label" => "Warehouse Inventory",
                "rules" => "numeric"
            ),

            "sub_warehouse_id" => array(
                "field" => "sub_warehouse_id",
                "label" => "Sub Warehouse",
                "rules" => "numeric"
            ),

            "sub_warehouse_inventory_id" => array(
                "field" => "sub_warehouse_inventory_id",
                "label" => "Sub Warehouse Inventory",
                "rules" => "numeric"
            ),

            "virtual_warehouse_id" => array(
                "field" => "virtual_warehouse_id",
                "label" => "Virtual Warehouse",
                "rules" => "numeric"
            ),

            "virtual_warehouse_inventory_id" => array(
                "field" => "virtual_warehouse_inventory_id",
                "label" => "Virtual Warehouse Inventory",
                "rules" => "numeric"
            ),

            "item_id" => array(
                "field" => "item_id",
                "label" => "Item",
                "rules" => "numeric|required"
            ),

            "unit_of_measurement_id" => array(
                "field" => "unit_of_measurement_id",
                "label" => "Unit of Measurement",
                "rules" => "numeric|required"
            ),

            "actual_quantity_in" => array(
                "field" => "actual_quantity_in",
                "label" => "Actual Quantity (In)",
                "rules" => "required|numeric"
            ),

            "actual_quantity_out" => array(
                "field" => "actual_quantity_out",
                "label" => "Actual Quantity (Out)",
                "rules" => "required|numeric"
            ),

            "available_quantity_in" => array(
                "field" => "available_quantity_in",
                "label" => "Available Quantity (In)",
                "rules" => "required|numeric"
            ),

            "available_quantity_out" => array(
                "field" => "available_quantity_out",
                "label" => "Available Quantity (Out)",
                "rules" => "required|numeric"
            ),

            "unit_price" => array(
                "field" => "unit_price",
                "label" => "Unit Price",
                "rules" => "numeric"
            ),

            "remarks" => array(
                "field" => "remarks",
                "label" => "Remarks",
                "rules" => "None"
            ),

            "transaction_type" => array(
                "field" => "transaction_type",
                "label" => "Transaction Type",
                "rules" => "required|numeric"
            ),


        ];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = true;
            $this->return_as = 'array';

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;


            $this->has_one["warehouse"] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouses', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');
            $this->has_one["source"] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouses', 'foreign_key' => 'id', 'local_key' => 'source_warehouse_id');
            $this->has_one["destination"] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouses', 'foreign_key' => 'id', 'local_key' => 'destination_warehouse_id');
            $this->has_one["warehouse_inventory"] = array('foreign_model' => 'warehouse_inventory/warehouse_inventory_model', 'foreign_table' => 'warehouse_inventories', 'foreign_key' => 'id', 'local_key' => 'warehouse_inventory_id');
//            $this->has_one["sub_warehouse"] = array('foreign_model' => 'sub_warehouse/sub_warehouse_model', 'foreign_table' => 'sub_warehouses', 'foreign_key' => 'id', 'local_key' => 'sub_warehouse_id');
//            $this->has_one["sub_warehouse_inventory"] = array('foreign_model' => 'sub_warehouse_inventory/sub_warehouse_inventory_model', 'foreign_table' => 'sub_warehouse_inventories', 'foreign_key' => 'id', 'local_key' => 'sub_warehouse_inventory_id');
//            $this->has_one["virtual_warehouse"] = array('foreign_model' => 'virtual_warehouse/virtual_warehouse_model', 'foreign_table' => 'virtual_warehouses', 'foreign_key' => 'id', 'local_key' => 'virtual_warehouse_id');
//            $this->has_one["virtual_warehouse_inventory"] = array('foreign_model' => 'virtual_warehouse_inventory/virtual_warehouse_inventory_model', 'foreign_table' => 'virtual_warehouse_inventories', 'foreign_key' => 'id', 'local_key' => 'virtual_warehouse_inventory_id');
            $this->has_one["item"] = array('foreign_model' => 'item/item_model', 'foreign_table' => 'item', 'foreign_key' => 'id', 'local_key' => 'item_id');
            $this->has_one["unit_of_measurement"] = array('foreign_model' => 'inventory_settings_unit_of_measurement/inventory_settings_unit_of_measurement_model', 'foreign_table' => 'inventory_settings_unit_of_measurements', 'foreign_key' => 'id', 'local_key' => 'unit_of_measurement_id');
            $this->has_one["material_receiving"] = array('foreign_model' => 'material_receiving/material_receiving_model', 'foreign_table' => 'material_receivings', 'foreign_key' => 'id', 'local_key' => 'material_receiving_id');
            $this->has_one["material_issuance"] = array('foreign_model' => 'material_issuance/material_issuance_model', 'foreign_table' => 'material_issuances', 'foreign_key' => 'id', 'local_key' => 'material_issuance_id');

        }

        function get_columns()
        {
            $_return = FALSE;

            if ($this->fillable) {
                $_return = $this->fillable;
            }

            return $_return;
        }

        public function insert_dummy()
        {
            require APPPATH . '/third_party/faker/autoload.php';
            $faker = Faker\Factory::create();

            $data = [];

            for ($x = 0; $x < 10; $x++) {
                array_push($data, array(
                    'name' => $faker->word,
                ));
            }
            $this->db->insert_batch($this->table, $data);

        }

    }