<div class="row">








    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="warehouses" id="warehouse_id"
                            name="id">
                        <?php if ($warehouse_id): ?>
                            <option value="<?php echo $warehouse_id['id']; ?>"
                                    selected><?php echo $warehouse_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Warehouse Inventory <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="warehouse_inventories" id="warehouse_inventory_id"
                            name="id">
                        <?php if ($warehouse_inventory_id): ?>
                            <option value="<?php echo $warehouse_inventory_id['id']; ?>"
                                    selected><?php echo $warehouse_inventory_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Sub Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="sub_warehouses" id="sub_warehouse_id"
                            name="id">
                        <?php if ($sub_warehouse_id): ?>
                            <option value="<?php echo $sub_warehouse_id['id']; ?>"
                                    selected><?php echo $sub_warehouse_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Sub Warehouse Inventory <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="sub_warehouse_inventories" id="sub_warehouse_inventory_id"
                            name="id">
                        <?php if ($sub_warehouse_inventory_id): ?>
                            <option value="<?php echo $sub_warehouse_inventory_id['id']; ?>"
                                    selected><?php echo $sub_warehouse_inventory_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Virtual Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="virtual_warehouses" id="virtual_warehouse_id"
                            name="id">
                        <?php if ($virtual_warehouse_id): ?>
                            <option value="<?php echo $virtual_warehouse_id['id']; ?>"
                                    selected><?php echo $virtual_warehouse_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Virtual Warehouse Inventory <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="virtual_warehouse_inventories" id="virtual_warehouse_inventory_id"
                            name="id">
                        <?php if ($virtual_warehouse_inventory_id): ?>
                            <option value="<?php echo $virtual_warehouse_inventory_id['id']; ?>"
                                    selected><?php echo $virtual_warehouse_inventory_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Item <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="item" id="item_id"
                            name="id">
                        <?php if ($item_id): ?>
                            <option value="<?php echo $item_id['id']; ?>"
                                    selected><?php echo $item_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Unit of Measurement <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                    <select class="form-control suggests" data-module="inventory_settings_unit_of_measurements" id="unit_of_measurement_id"
                            name="id">
                        <?php if ($unit_of_measurement_id): ?>
                            <option value="<?php echo $unit_of_measurement_id['id']; ?>"
                                    selected><?php echo $unit_of_measurement_id['name']; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Actual Quantity (In) <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                    <input type="number" min="0" step="0.01" class="form-control" id="actual_quantity_in" name="actual_quantity_in" placeholder="Actual Quantity (In)" v-model="info.form.actual_quantity_in">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Actual Quantity (Out) <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                    <input type="number" min="0" step="0.01" class="form-control" id="actual_quantity_out" name="actual_quantity_out" placeholder="Actual Quantity (Out)" v-model="info.form.actual_quantity_out">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Available Quantity (In) <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                    <input type="number" min="0" step="0.01" class="form-control" id="available_quantity_in" name="available_quantity_in" placeholder="Available Quantity (In)" v-model="info.form.available_quantity_in">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Available Quantity (Out) <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                    <input type="number" min="0" step="0.01" class="form-control" id="available_quantity_out" name="available_quantity_out" placeholder="Available Quantity (Out)" v-model="info.form.available_quantity_out">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Unit Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                    <input type="number" min="0" step="0.01" class="form-control" id="unit_price" name="unit_price" placeholder="Unit Price" v-model="info.form.unit_price">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Remarks <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Transaction Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                
                
                
                    <?php echo form_dropdown('transaction_type', Dropdown::get_static('inventory_log_transaction_type'), null, 'class="form-control" v-model="info.form.transaction_type"'); ?>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    



</div>