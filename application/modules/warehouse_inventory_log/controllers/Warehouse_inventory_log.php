<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Warehouse_inventory_log extends MY_Controller
    {

        private $__errors = array(
            'create_master' => null,
            'create_slave' => null,
            'update_master' => null,
            'update_slave' => null,
        );

        public function __construct()
        {
            parent::__construct();

            $this->load->model('Warehouse_inventory_log_model', 'M_Warehouse_inventory_log');

            $this->load->helper('form');

            $this->_table_fillables = $this->M_Warehouse_inventory_log->fillable;
            $this->_form_fillables = $this->M_Warehouse_inventory_log->form_fillables;
            $this->_table_columns = $this->M_Warehouse_inventory_log->__get_columns();
        }

        #region private methods
        private function __get_obj($id, $with_relations = TRUE)
        {
            if ($with_relations) {
                $obj = $this->M_Warehouse_inventory_log
                    ->with_warehouse()
                    ->with_warehouse_inventory()
//                    ->with_sub_warehouse()
//                    ->with_sub_warehouse_inventory()
//                    ->with_virtual_warehouse()
//                    ->with_virtual_warehouse_inventory()
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->get($id);
            } else {
                $obj = $this->M_Warehouse_inventory_log->get($id);
            }

            return $obj;
        }

        private function __search($params)
        {
            $id = isset($params['id']) ? $params['id'] : null;
            $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
            $status_list = isset($params['status_list']) && !empty($params['status_list']) ? $params['status_list'] : null;

            if ($status_list) {
                $status_list_arr = explode(",", $status_list);
                $this->M_Warehouse_inventory_log->where('status', $status_list_arr);
            }

            if (!$id) {

                $id = isset($params['id']) && !empty($params['id']) ? $params['id'] : null;
                if ($id) {
                    $this->M_Warehouse_inventory_log->where('id', $id);
                }

                $created_by = isset($params['created_by']) && !empty($params['created_by']) ? $params['created_by'] : null;
                if ($created_by) {
                    $this->M_Warehouse_inventory_log->where('created_by', $created_by);
                }

                $created_at = isset($params['created_at']) && !empty($params['created_at']) ? $params['created_at'] : null;
                if ($created_at) {
                    $this->M_Warehouse_inventory_log->where('created_at', $created_at);
                }

                $updated_by = isset($params['updated_by']) && !empty($params['updated_by']) ? $params['updated_by'] : null;
                if ($updated_by) {
                    $this->M_Warehouse_inventory_log->where('updated_by', $updated_by);
                }

                $updated_at = isset($params['updated_at']) && !empty($params['updated_at']) ? $params['updated_at'] : null;
                if ($updated_at) {
                    $this->M_Warehouse_inventory_log->where('updated_at', $updated_at);
                }

                $deleted_by = isset($params['deleted_by']) && !empty($params['deleted_by']) ? $params['deleted_by'] : null;
                if ($deleted_by) {
                    $this->M_Warehouse_inventory_log->where('deleted_by', $deleted_by);
                }

                $deleted_at = isset($params['deleted_at']) && !empty($params['deleted_at']) ? $params['deleted_at'] : null;
                if ($deleted_at) {
                    $this->M_Warehouse_inventory_log->where('deleted_at', $deleted_at);
                }

                $warehouse_id = isset($params['warehouse_id']) && !empty($params['warehouse_id']) ? $params['warehouse_id'] : null;
                if ($warehouse_id) {
                    $this->M_Warehouse_inventory_log->where('warehouse_id', $warehouse_id);
                }

                $source_warehouse_id = isset($params['source_warehouse_id']) && !empty($params['source_warehouse_id']) ? $params['source_warehouse_id'] : null;
                if ($source_warehouse_id) {
                    $this->M_Warehouse_inventory_log->where('source_warehouse_id', $source_warehouse_id);
                }

                $destination_warehouse_id = isset($params['destination_warehouse_id']) && !empty($params['destination_warehouse_id']) ? $params['destination_warehouse_id'] : null;
                if ($destination_warehouse_id) {
                    $this->M_Warehouse_inventory_log->where('destination_warehouse_id', $destination_warehouse_id);
                }

                $warehouse_inventory_id = isset($params['warehouse_inventory_id']) && !empty($params['warehouse_inventory_id']) ? $params['warehouse_inventory_id'] : null;
                if ($warehouse_inventory_id) {
                    $this->M_Warehouse_inventory_log->where('warehouse_inventory_id', $warehouse_inventory_id);
                }

                $sub_warehouse_id = isset($params['sub_warehouse_id']) && !empty($params['sub_warehouse_id']) ? $params['sub_warehouse_id'] : null;
                if ($sub_warehouse_id) {
                    $this->M_Warehouse_inventory_log->where('sub_warehouse_id', $sub_warehouse_id);
                }

                $sub_warehouse_inventory_id = isset($params['sub_warehouse_inventory_id']) && !empty($params['sub_warehouse_inventory_id']) ? $params['sub_warehouse_inventory_id'] : null;
                if ($sub_warehouse_inventory_id) {
                    $this->M_Warehouse_inventory_log->where('sub_warehouse_inventory_id', $sub_warehouse_inventory_id);
                }

                $virtual_warehouse_id = isset($params['virtual_warehouse_id']) && !empty($params['virtual_warehouse_id']) ? $params['virtual_warehouse_id'] : null;
                if ($virtual_warehouse_id) {
                    $this->M_Warehouse_inventory_log->where('virtual_warehouse_id', $virtual_warehouse_id);
                }

                $virtual_warehouse_inventory_id = isset($params['virtual_warehouse_inventory_id']) && !empty($params['virtual_warehouse_inventory_id']) ? $params['virtual_warehouse_inventory_id'] : null;
                if ($virtual_warehouse_inventory_id) {
                    $this->M_Warehouse_inventory_log->where('virtual_warehouse_inventory_id', $virtual_warehouse_inventory_id);
                }

                $item_id = isset($params['item_id']) && !empty($params['item_id']) ? $params['item_id'] : null;
                if ($item_id) {
                    $this->M_Warehouse_inventory_log->where('item_id', $item_id);
                }

                $unit_of_measurement_id = isset($params['unit_of_measurement_id']) && !empty($params['unit_of_measurement_id']) ? $params['unit_of_measurement_id'] : null;
                if ($unit_of_measurement_id) {
                    $this->M_Warehouse_inventory_log->where('unit_of_measurement_id', $unit_of_measurement_id);
                }

                $actual_quantity_in = isset($params['actual_quantity_in']) && !empty($params['actual_quantity_in']) ? $params['actual_quantity_in'] : null;
                if ($actual_quantity_in) {
                    $this->M_Warehouse_inventory_log->where('actual_quantity_in', $actual_quantity_in);
                }

                $actual_quantity_out = isset($params['actual_quantity_out']) && !empty($params['actual_quantity_out']) ? $params['actual_quantity_out'] : null;
                if ($actual_quantity_out) {
                    $this->M_Warehouse_inventory_log->where('actual_quantity_out', $actual_quantity_out);
                }

                $available_quantity_in = isset($params['available_quantity_in']) && !empty($params['available_quantity_in']) ? $params['available_quantity_in'] : null;
                if ($available_quantity_in) {
                    $this->M_Warehouse_inventory_log->where('available_quantity_in', $available_quantity_in);
                }

                $available_quantity_out = isset($params['available_quantity_out']) && !empty($params['available_quantity_out']) ? $params['available_quantity_out'] : null;
                if ($available_quantity_out) {
                    $this->M_Warehouse_inventory_log->where('available_quantity_out', $available_quantity_out);
                }

                $unit_price = isset($params['unit_price']) && !empty($params['unit_price']) ? $params['unit_price'] : null;
                if ($unit_price) {
                    $this->M_Warehouse_inventory_log->where('unit_price', $unit_price);
                }

                $remarks = isset($params['remarks']) && !empty($params['remarks']) ? $params['remarks'] : null;
                if ($remarks) {
                    $this->M_Warehouse_inventory_log->where('remarks', $remarks);
                }

                $transaction_type = isset($params['transaction_type']) && !empty($params['transaction_type']) ? $params['transaction_type'] : null;
                if ($transaction_type) {
                    $this->M_Warehouse_inventory_log->where('transaction_type', $transaction_type);
                }

                $current_actual_quantity = isset($params['current_actual_quantity']) && !empty($params['current_actual_quantity']) ? $params['current_actual_quantity'] : null;
                if ($current_actual_quantity) {
                    $this->M_Warehouse_inventory_log->where('current_actual_quantity', $current_actual_quantity);
                }

                $current_available_quantity = isset($params['current_available_quantity']) && !empty($params['current_available_quantity']) ? $params['current_available_quantity'] : null;
                if ($current_available_quantity) {
                    $this->M_Warehouse_inventory_log->where('current_available_quantity', $current_available_quantity);
                }


            } else {
                $this->M_Warehouse_inventory_log->where('id', $id);
            }

            $this->M_Warehouse_inventory_log->order_by('id', 'DESC');

            if ($with_relations === 'yes') {
                $result = $this->M_Warehouse_inventory_log
                    ->with_warehouse()
                    ->with_source()
                    ->with_destination()
                    ->with_warehouse_inventory()
                    ->with_item()
                    ->with_unit_of_measurement()
                    ->get_all();
            } else {
                $result = $this->M_Warehouse_inventory_log->get_all();
            }

            return $result;
        }

        private function process_create_master($data, $additional)
        {
            $data['reference'] = uniqidWarehouse_inventory_log_model();

            $form_data = $data + additional;
            $this->form_validation->set_data($form_data);
            $this->form_validation->set_rules($this->M_Warehouse_inventory_log->fields);

            if ($this->form_validation->run() === true) {

                $request = $this->db->insert('warehouse_inventory_logs', $data + $additional);
                if ($request) {
                    return $this->db->insert_id();
                } else {
                    return false;
                }
            } else {
                $this->_form_errors['create_master'] = validation_errors();
                return false;
            }
        }

        private function process_create_slave($data, $references, $additional)
        {
            $this->load->model('warehouse_inventory_log_item/Warehouse_inventory_log_item_model', 'M_item');
            if (property_exists($this->M_item, 'form_fillables')) {
                $fillables = $this->M_item->form_fillables;
            } else {
                $fillables = $this->M_item->fillable;
            }

            $item = [];
            foreach ($fillables as $field) {
                if (key_exists($field, $data)) {
                    $item[$field] = $data[$field];
                } else {
                    if (key_exists($field, $references)) {
                        $item[$field] = $references[$field];
                    }
                }
            }

            return $this->M_item->insert($item);
        }

        private function process_update_master($id, $data, $additional)
        {
            $old_object = $this->M_Warehouse_inventory_log->get($id);
            return $this->M_Warehouse_inventory_log->update($data + $additional, $id);
        }

        function process_update_slave($data, $additional, $references)
        {
            $id = isset($data['id']) ? $data['id'] : null;
            $this->load->model('warehouse_inventory_log_item/Warehouse_inventory_log_item_model', 'M_item');

            $temp_data = $data + $additional;
            $update_data = [];
            if (property_exists($this->M_item, 'form_fillables')) {
                $fillables = $this->M_item->form_fillables;
            } else {
                $fillables = $this->M_item->fillable;
            }
            foreach ($fillables as $key) {
                if (array_key_exists($key, $temp_data)) {
                    $update_data[$key] = $temp_data[$key];
                }
            }

            if ($id) {
                // return $this->db->update('material_receiving_items');
                return $this->M_item->update($update_data, $id);
            } else {
                $additional = [
                    'created_by' => $additional['updated_by'],
                    'created_at' => $additional['updated_at']
                ];

                return $this->process_create_slave($data, $references, $additional);
            }
        }

        private function process_purge_deleted($items, $additional)
        {
            $this->load->model('warehouse_inventory_log_item/Warehouse_inventory_log_item_model', 'M_item');
            if ($items) {
                foreach ($items as $item) {
                    if ($item) {
                        $this->M_item->delete($item);
                    }
                }
            }
        }
        # endregion


        // API
        public function search()
        {
            $result = $this->__search($_GET);

            header('Content-Type: application/json');
            echo json_encode($result);

        }

        public function showWarehouseInventoryLogs()
        {
            header('Content-Type: application/json');
            $output = ['data' => ''];

            if (property_exists($this->M_Warehouse_inventory_log, 'form_fillables')) {
                $fillables = $this->M_Warehouse_inventory_log->form_fillables;
            } else {
                $fillables = $this->M_Warehouse_inventory_log->fillable;
            }

            $columnsDefault = [];
            foreach ($fillables as $field) {
                $columnsDefault[$field] = true;
            }
            $columnsDefault["material_receiving_id"] = true;
            $columnsDefault["material_issuance_id"] = true;
            $columnsDefault["transaction_type"] = true;
            $columnsDefault["current_actual_quantity"] = true;
            $columnsDefault["current_available_quantity"] = true;
            $columnsDefault["item"] = true;
            $columnsDefault["unit_of_measurement"] = true;
            $columnsDefault["created_at"] = true;
            $columnsDefault["created_by"] = true;
            $columnsDefault["source"] = true;
            $columnsDefault["destination"] = true;

            // optionally add relations

            if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
                $columnsDefault = [];
                foreach ($_REQUEST['columnsDef'] as $field) {
                    $columnsDefault[$field] = true;
                }
            }

            // get all raw data
            $objects = $this->__search($_GET);
            $data = [];
            $this->load->helper('inventory_log');
            if ($objects) {

                foreach ($objects as $d) {
                    $__d = $this->filterArray($d, $columnsDefault);
                    $__d['transaction_label'] = inventory_log_transaction_type_lookup($__d['transaction_type']);
                    $__d['actor'] = get_person_name($__d['created_by'], "staff");
                    array_push($data, $__d);
                }

                // count data
                $totalRecords = $totalDisplay = count($data);

                // filter by general search keyword
                if (isset($_REQUEST['search'])) {
                    $data = $this->filterKeyword($data, $_REQUEST['search']);
                    $totalDisplay = count($data);
                }

                if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                    foreach ($_REQUEST['columns'] as $column) {
                        if (isset($column['search'])) {
                            $data = $this->filterKeyword($data, $column['search'], $column['data']);
                            $totalDisplay = count($data);
                        }
                    }
                }

                // sort
                if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                    $column = $_REQUEST['order'][0]['column'];
                    $dir = $_REQUEST['order'][0]['dir'];
                    usort($data, function ($a, $b) use ($column, $dir) {
                        $a = array_slice($a, $column, 1);
                        $b = array_slice($b, $column, 1);
                        $a = array_pop($a);
                        $b = array_pop($b);

                        if ($dir === 'asc') {
                            return $a > $b ? true : false;
                        }

                        return $a < $b ? true : false;
                    });
                }

                // pagination length
                if (isset($_REQUEST['length'])) {
                    $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
                }

                // return array values only without the keys
                if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                    $tmp = $data;
                    $data = [];
                    foreach ($tmp as $d) {
                        $data[] = array_values($d);
                    }
                }
                $secho = 0;
                if (isset($_REQUEST['sEcho'])) {
                    $secho = intval($_REQUEST['sEcho']);
                }

                $output = array(
                    'sEcho' => $secho,
                    'sColumns' => '',
                    'iTotalRecords' => $totalRecords,
                    'iTotalDisplayRecords' => $totalDisplay,
                    'data' => $data,
                );

            }

            echo json_encode($output);
            exit();
        }

        public function process_create()
        {
            header('Content-Type: application/json');
            if ($this->input->post()) {
                $__request = $this->input->post('request');
                $request = [];
                $items = $this->input->post('items');

                if (property_exists($this->M_Warehouse_inventory_log, 'form_fillables')) {
                    $fillables = $this->M_Warehouse_inventory_log->form_fillables;
                } else {
                    $fillables = $this->M_Warehouse_inventory_log->fillable;
                }

                foreach ($fillables as $field) {
                    if (array_key_exists($field, $__request)) {
                        $request[$field] = $__request[$field];
                    }
                }

                $this->db->trans_start();
                $this->db->trans_strict(false);

                $additional = array(
                    'created_at' => NOW,
                    'created_by' => $this->user->id
                );

                $request_object = $this->process_create_master($request, $additional);
                if ($request_object) {
                    $references = array(
                        'warehouse_inventory_log_id' => $request_object,
                    );
                    foreach ($items as $item) {
                        $this->process_create_slave($item, $references, $additional);
                    }
                }

                $this->db->trans_complete(); # Completing request
                if ($this->db->trans_status() === false || !$request_object) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = $this->__errors['create_master'] + $this->__errors['create_slave'];
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();

                    $response['status'] = 1;
                    $response['message'] = 'Request Successfully saved!';
                }

                echo json_encode($response);
                exit();
            }
        }

        public function process_update($id)
        {
            header('Content-Type: application/json');
            $additional = [
                'updated_by' => $this->user->id,
                'updated_at' => NOW,
            ];
            if ($this->input->post()) {
                $__request = $this->input->post('request');
                $request = [];
                $items = $this->input->post('items');

                if (property_exists($this->M_Warehouse_inventory_log, 'form_fillables')) {
                    $fillables = $this->M_Warehouse_inventory_log->form_fillables;
                } else {
                    $fillables = $this->M_Warehouse_inventory_log->fillable;
                }

                foreach ($fillables as $key) {
                    if (array_key_exists($key, $__request)) {
                        $request[$key] = $__request[$key];
                    }
                }

                $this->db->trans_start();
                $this->db->trans_strict(false);
                $request_object = $this->process_update_master($id, $request, $additional);
                if ($request_object) {
                    $references = [
                        'warehouse_inventory_log_id' => $id,
                    ];
                    foreach ($items as $item) {
                        $this->process_update_slave($item, $additional, $references);
                    }
                }
                $deleted_items = $this->input->post('deleted_items');

                $this->db->trans_complete(); # Completing request
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $this->process_purge_deleted($deleted_items, $additional);

                    $response['status'] = 1;
                    $response['message'] = 'Successfully saved!';
                }

                echo json_encode($response);
                exit();

            }
        }

        // http

        public function index()
        {
            $_fills = $this->_table_fillables;
            $_colms = $this->_table_columns;

            $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
            $this->view_data['_columns'] = $this->__get_columns($_fills);

            $db_columns = $this->_table_columns;
            if ($db_columns) {
                $column = [];
                foreach ($db_columns as $key => $dbclm) {
                    $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                    if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                        if (strpos($name, '_id') !== false) {
                            $column = $name;
                            $column[$key]['value'] = $column;
                            $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                            $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                            $column[$key]['label'] = ucwords(strtolower($_title));
                        } elseif (strpos($name, 'is_') !== false) {
                            $column = $name;
                            $column[$key]['value'] = $column;
                            $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                            $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                            $column[$key]['label'] = ucwords(strtolower($_title));
                        } else {
                            $column[$key]['value'] = $name;
                            $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                            $column[$key]['label'] = ucwords(strtolower($_title));
                        }
                    } else {
                        continue;
                    }
                }

                $column_count = count($column);
                $cceil = ceil(($column_count / 2));

                $this->view_data['columns'] = array_chunk($column, $cceil);
                $column_group = $this->M_Warehouse_inventory_log->count_rows();
                if ($column_group) {
                    $this->view_data['total'] = $column_group;
                }

            }

            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue([
                'vendors/custom/datatables/datatables.bundle.js',
                'js/vue2.js',
                'js/axios.min.js',
                'js/utils.js'
            ]);

            $this->template->build('index', $this->view_data);
        }


        public function form($id = false)
        {
            if (property_exists($this->M_Warehouse_inventory_log, 'form_fillables')) {
                $fillables = $this->M_Warehouse_inventory_log->form_fillables;
            } else {
                $fillables = $this->M_Warehouse_inventory_log->fillable;
            }
            if ($id) {
                $method = "Update";
                $obj = $this->__get_obj($id);
                if (in_array('reference', $fillables)) {
                    $reference = $obj['reference'];
                } else {
                    $reference = null;
                }

                $form_data = fill_form_data($fillables, $obj);


                $this->view_data['warehouse'] = $obj['warehouse'];

                $this->view_data['warehouse_inventory'] = $obj['warehouse_inventory'];

                $this->view_data['sub_warehouse'] = $obj['sub_warehouse'];

                $this->view_data['sub_warehouse_inventory'] = $obj['sub_warehouse_inventory'];

                $this->view_data['virtual_warehouse'] = $obj['virtual_warehouse'];

                $this->view_data['virtual_warehouse_inventory'] = $obj['virtual_warehouse_inventory'];

                $this->view_data['item'] = $obj['item'];

                $this->view_data['unit_of_measurement'] = $obj['unit_of_measurement'];


            } else {
                $method = "Create";
                $obj = null;
                $reference = null;
                $form_data = fill_form_data($fillables);
            }
            $is_editable = 1;

            $this->view_data['fillables'] = $fillables;
            $this->view_data['current_user'] = $this->user->id;
            $this->view_data['method'] = $method;
            $this->view_data['obj'] = $obj;
            $this->view_data['id'] = $id ? $id : null;
            $this->view_data['form_data'] = $form_data;
            $this->view_data['reference'] = $reference;

            $this->js_loader->queue([
                'js/vue2.js',
                'js/axios.min.js',
                'js/utils.js'
            ]);

            $this->template->build('form', $this->view_data);


        }

        public function delete()
        {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $id = $this->input->post('id');
            if ($id) {

                $list = $this->M_Warehouse_inventory_log->get($id);
                if ($list) {

                    $deleted = $this->M_Warehouse_inventory_log->delete($list['id']);
                    if ($deleted !== false) {

                        $response['status'] = 1;
                        $response['message'] = 'Warehouse Inventory Log Successfully Deleted';
                    }
                }
            }

            echo json_encode($response);
            exit();
        }

        public function bulkDelete()
        {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            if ($this->input->is_ajax_request()) {
                $delete_ids = $this->input->post('deleteids_arr');

                if ($delete_ids) {
                    foreach ($delete_ids as $value) {
                        $data = [
                            'deleted_by' => $this->session->userdata['user_id']
                        ];
                        $this->db->update('item_group', $data, array('id' => $value));
                        $deleted = $this->M_Warehouse_inventory_log->delete($value);
                    }
                    if ($deleted !== false) {

                        $response['status'] = 1;
                        $response['message'] = 'Warehouse Inventory Log Successfully Deleted';
                    }
                }
            }

            echo json_encode($response);
            exit();
        }

        public function view($id = FALSE)
        {
            if ($id) {
                $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
                $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

                $this->view_data['data'] = $this->__get_obj($id);

                if ($this->view_data['data']) {

                    $this->template->build('view', $this->view_data);
                } else {

                    show_404();
                }
            } else {
                show_404();
            }
        }

        public function import()
        {

            $file = $_FILES['csv_file']['tmp_name'];
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);

            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $err = 0;


            foreach ($sheetData as $key => $upload_data) {

                if ($key > 0) {

                    if ($this->input->post('status') == '1') {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $warehouse_inventory_log_id = $upload_data[0];
                        $warehouse_inventory_log = $this->M_Warehouse_inventory_log->get($warehouse_inventory_log_id);

                        if ($warehouse_inventory_log) {
                            $result = $this->M_Warehouse_inventory_log->update($fields, $warehouse_inventory_log_id);
                        }

                    } else {

                        if (!is_numeric($upload_data[0])) {
                            $fields = array(
                                'name' => $upload_data[1],
                                /* ==================== begin: Add model fields ==================== */

                                /* ==================== end: Add model fields ==================== */
                            );

                            $result = $this->M_Warehouse_inventory_log->insert($fields);
                        } else {
                            $fields = array(
                                'name' => $upload_data[0],
                                /* ==================== begin: Add model fields ==================== */

                                /* ==================== end: Add model fields ==================== */
                            );

                            $result = $this->M_Warehouse_inventory_log->insert($fields);
                        }

                    }
                    if ($result === FALSE) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                        $err = 1;
                        break;
                    }
                }
            }

            if ($err == 0) {
                $this->notify->success('CSV successfully imported.', 'warehouse_inventory_log');
            } else {
                $this->notify->error('Oops something went wrong.');
            }

            header('Location: ' . base_url() . 'warehouse_inventory_log');
            die();
        }

        public function export_csv()
        {
            if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

                $_ued = $this->input->post('update_existing_data');

                $_is_update = $_ued === '1' ? TRUE : FALSE;

                $_alphas = [];
                $_datas = [];

                $_titles[] = 'id';

                $_start = 3;
                $_row = 2;

                $_filename = 'Warehouse Inventory Log CSV Template.csv';

                $_fillables = $this->_table_fillables;
                if (!$_fillables) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse_inventory_log');
                }

                foreach ($_fillables as $_fkey => $_fill) {

                    if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                        $_titles[] = $_fill;
                    } else {

                        continue;
                    }
                }

                if ($_is_update) {

                    $_group = $this->M_Warehouse_inventory_log->as_array()->get_all();
                    if ($_group) {

                        foreach ($_titles as $_tkey => $_title) {

                            foreach ($_group as $_dkey => $li) {

                                $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                            }
                        }
                    }
                } else {

                    if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                        unset($_titles[0]);
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));
                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                $_objSheet = $this->excel->getActiveSheet();
                $_objSheet->setTitle('Warehouse Inventory Log');
                $_objSheet->setCellValue('A1', 'WAREHOUSE INVENTORY LOG');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_objSheet->setCellValue($_xkey . $_row, $_column);
                }

                if ($_is_update) {

                    if (isset($_datas) && $_datas) {

                        foreach ($_datas as $_dkey => $_data) {

                            foreach ($_alphas as $_akey => $_alpha) {

                                $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                                $_objSheet->setCellValue($_alpha . $_start, $_value);
                            }

                            $_start++;
                        }
                    } else {

                        $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                        $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                        $_style = array(
                            'font' => array(
                                'bold' => FALSE,
                                'size' => 9,
                                'name' => 'Verdana'
                            )
                        );
                        $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                show_404();
            }
        }

        function export()
        {

            $_db_columns = [];
            $_alphas = [];
            $_datas = [];

            $_titles[] = '#';

            $_start = 3;
            $_row = 2;
            $_no = 1;

            $warehouse_inventory_logs = $this->M_Warehouse_inventory_log->as_array()->get_all();
            if ($warehouse_inventory_logs) {

                foreach ($warehouse_inventory_logs as $_lkey => $warehouse_inventory_log) {

                    $_datas[$warehouse_inventory_log['id']]['#'] = $_no;

                    $_no++;
                }

                $_filename = 'list_of_warehouse_inventory_logs_' . date('m_d_y_h-i-s', time()) . '.xls';

                $_objSheet = $this->excel->getActiveSheet();

                if ($this->input->post()) {

                    $_export_column = $this->input->post('_export_column');
                    if ($_export_column) {

                        foreach ($_export_column as $_ekey => $_column) {

                            $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                        }
                    } else {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse_inventory_log');
                    }
                } else {

                    $_filename = 'list_of_warehouse_inventory_logs_' . date('m_d_y_h-i-s', time()) . '.csv';

                    // $_db_columns	=	$this->M_land_inventory->fillable;
                    $_db_columns = $this->_table_fillables;
                    if (!$_db_columns) {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse_inventory_log');
                    }
                }

                if ($_db_columns) {

                    foreach ($_db_columns as $key => $_dbclm) {

                        $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                        if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                            if ((strpos($_name, '_id') !== FALSE)) {

                                $_column = $_name;

                                $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                                $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            } elseif ((strpos($_name, 'is_') !== FALSE)) {

                                $_column = $_name;

                                $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                                $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                                foreach ($warehouse_inventory_logs as $_lkey => $warehouse_inventory_log) {

                                    $_datas[$warehouse_inventory_log['id']][$_title] = isset($warehouse_inventory_log[$_column]) && ($warehouse_inventory_log[$_column] !== '') ? Dropdown::get_static('bool', $warehouse_inventory_log[$_column], 'view') : '';
                                }
                            } else {

                                $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                                foreach ($warehouse_inventory_logs as $_lkey => $warehouse_inventory_log) {

                                    if ($_name === 'status') {

                                        $_datas[$warehouse_inventory_log['id']][$_title] = isset($warehouse_inventory_log[$_name]) && $warehouse_inventory_log[$_name] ? Dropdown::get_static('inventory_status', $warehouse_inventory_log[$_name], 'view') : '';
                                    } else {

                                        $_datas[$warehouse_inventory_log['id']][$_title] = isset($warehouse_inventory_log[$_name]) && $warehouse_inventory_log[$_name] ? $warehouse_inventory_log[$_name] : '';
                                    }
                                }
                            }
                        } else {

                            continue;
                        }
                    }

                    $_alphas = $this->__get_excel_columns(count($_titles));

                    $_xls_columns = array_combine($_alphas, $_titles);
                    $_firstAlpha = reset($_alphas);
                    $_lastAlpha = end($_alphas);

                    foreach ($_xls_columns as $_xkey => $_column) {

                        $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                        $_objSheet->setCellValue($_xkey . $_row, $_title);
                    }

                    $_objSheet->setTitle('List of Warehouse Inventory Log');
                    $_objSheet->setCellValue('A1', 'LIST OF WAREHOUSE INVENTORY LOG');
                    $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                    if (isset($_datas) && $_datas) {

                        foreach ($_datas as $_dkey => $_data) {

                            foreach ($_alphas as $_akey => $_alpha) {

                                $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                                $_objSheet->setCellValue($_alpha . $_start, $_value);
                            }

                            $_start++;
                        }
                    } else {

                        $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                        $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                        $_style = array(
                            'font' => array(
                                'bold' => FALSE,
                                'size' => 9,
                                'name' => 'Verdana'
                            )
                        );
                        $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }

                    foreach ($_alphas as $_alpha) {

                        $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                    }

                    $_style = array(
                        'font' => array(
                            'bold' => TRUE,
                            'size' => 10,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment; filename="' . $_filename . '"');
                    header('Cache-Control: max-age=0');
                    $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                    @ob_end_clean();
                    $_objWriter->save('php://output');
                    @$_objSheet->disconnectWorksheets();
                    unset($_objSheet);
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'warehouse_inventory_log');
                }
            } else {

                $this->notify->error('No Record Found', 'warehouse_inventory_log');
            }
        }

    }
