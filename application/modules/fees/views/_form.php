<?php
    $name            =    isset($fee['name']) && $fee['name'] ? $fee['name'] : '';
    $description        =    isset($fee['description']) && $fee['description'] ? $fee['description'] : '';
    $value            =    isset($fee['value']) && $fee['value'] ? $fee['value'] : '';
    $value_type    =    isset($fee['value_type']) && $fee['value_type'] ? $fee['value_type'] : '';
    $add_to            =    isset($fee['add_to']) && $fee['add_to'] ? $fee['add_to'] : '';
    $fees_type    =    isset($fee['fees_type']) && $fee['fees_type'] ? $fee['fees_type'] : '';
    $default    =    isset($fee['default']) && $fee['default'] ? $fee['default'] : '';
    // to add more fees type
?>


<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Description</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-edit"></i></span>
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Value</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="value" value="<?php echo set_value('value', $value); ?>" placeholder="Value" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-money"></i></span>
            </div>
            <?php echo form_error('value'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Value Type</label>
            <div class="kt-input-icon kt-input-icon--left">
                <?php echo form_dropdown('value_type', Dropdown::get_static('value_type'), set_value('value_type', $value_type), 'class="form-control" id="value_type"'); ?>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-chain"></i></span>
            </div>
            <?php echo form_error('value_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Add to</label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('add_to', Dropdown::get_static('add_to'), set_value('add_to', $add_to), 'class="form-control" id="add_to"'); ?>
                
            </div>
            <?php echo form_error('add_to'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Fees Type</label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('fees_type', Dropdown::get_static('fees_type'), set_value('add_to', $add_to), 'class="form-control" id="fees_type"'); ?>
            </div>
            <?php echo form_error('fees_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Is Default</label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('default', Dropdown::get_static('bool'), set_value('default', $default), 'class="form-control" id="default"'); ?>
            </div>
            <?php echo form_error('default'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
