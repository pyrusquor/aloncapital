<?php
    $id            =    isset($fee['id']) && $fee['id'] ? $fee['id'] : '';
    $name            =    isset($fee['name']) && $fee['name'] ? $fee['name'] : '';
    $description        =    isset($fee['description']) && $fee['description'] ? $fee['description'] : '';
    $value            =    isset($fee['value']) && $fee['value'] ? $fee['value'] : '';
    $value_type    =    isset($fee['value_type']) && $fee['value_type'] ? Dropdown::get_static('value_type',$fee['value_type']) : '';
    $add_to            =    isset($fee['add_to']) && $fee['add_to'] ? Dropdown::get_static('add_to',$fee['add_to']) : '';
    $fees_type    =    isset($fee['fees_type']) && $fee['fees_type'] ? Dropdown::get_static('fees_type',$fee['fees_type']) : '';
    $default    =    isset($fee['default']) && $fee['default'] ? Dropdown::get_static('bool',$fee['default']) : '';
    $created_at    =    isset($fee['created_at']) && $fee['created_at'] ? nice_date($fee['created_at'],'F j, Y') : '';
    $updated_at    =    isset($fee['updated_at']) && $fee['updated_at'] ? nice_date($fee['updated_at'],'F j, Y') : '';
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">View Fee</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('fees/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<a href="<?php echo site_url('fees');?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-6">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							General Information
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<!--begin::Form-->
					<div class="kt-widget4">
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Name
							</span>
							<span class="kt-widget4__text kt-widget4__text--bold">
								<?php echo $name; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc kt-align-right">
								Description
							</span>
							<span class="kt-widget4__text">
								<?php echo $description; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Value
							</span>
							<span class="kt-widget4__text kt-widget4__text--bold">
								<?php echo $value; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Value Type
							</span>
							<span class="kt-widget4__text">
								<?php echo $value_type; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Add To:
							</span>
							<span class="kt-widget4__text">
								<?php echo $add_to; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Fees Type:
							</span>
							<span class="kt-widget4__text">
								<?php echo $fees_type; ?>
							</span>
						</div>

						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Is Default:
							</span>
							<span class="kt-widget4__text">
								<?php echo $default; ?>
							</span>
						</div>

						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Date Created:
							</span>
							<span class="kt-widget4__text">
								<?php echo $created_at; ?>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Date Updated:
							</span>
							<span class="kt-widget4__text">
								<?php echo $updated_at; ?>
							</span>
						</div>
					</div>

					<!--end::Form-->
				</div>
			</div>
			<!--end::Portlet-->

		</div>
	</div>
</div>
<!-- begin:: Footer -->