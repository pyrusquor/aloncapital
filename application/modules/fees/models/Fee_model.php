<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Fee_model extends MY_Model {

	public $table = 'fees'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = ['name', 'description', 'value', 'value_type', 'add_to', 'fees_type', 'default']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [
		'name' => array(
			'field'=>'name',
			'label'=>'Name',
			'rules'=>'trim|required'
		),
		'description' => array(
			'field'=>'description',
			'label'=>'Description',
			'rules'=>'trim'
		),
		'value' => array(
			'field'=>'value',
			'label'=>'Value',
			'rules'=>'trim'
		),
		'value_type' => array(
			'field'=>'value_type',
			'label'=>'Value Type',
			'rules'=>'trim'
		),
		'add_to' => array(
			'field'=>'add_to',
			'label'=>'Add To',
			'rules'=>'trim'
		),
		'fees_type' => array(
			'field'=>'fees_type',
			'label'=>'Fees Type',
			'rules'=>'trim'
		),
		'default' => array(
			'field'=>'default',
			'label'=>'Default',
			'rules'=>'trim'
		),
	];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
	}

	function get_columns () 
	{

		$_return	=	FALSE;

		if ( $this->fillable ) {

			$_return	=	$this->fillable;
		}

		return $_return;
	}

	public function insert_dummy()
	{
		require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

		$data = [];

        for($x = 0; $x < 20; $x++)
        {
            array_push($data,array(
				'name'=> $faker->name,
				'location' => $faker->address,
				'email' => $faker->email,
				'year_started' => $faker->year,
				'contact_number' => $faker->phoneNumber,
				'website' => $faker->url
            ));
		}
		$this->db->insert_batch($this->table, $data);

	}
}