<?php

$name = isset($warehouse['name']) && $warehouse['name'] ? $warehouse['name'] : '';
// ==================== begin: Add model fields ====================
$warehouse_code = isset($warehouse['warehouse_code']) && $warehouse['warehouse_code'] ? $warehouse['warehouse_code'] : '';
$sbu_id = isset($warehouse['sbu']) && $warehouse['sbu'] ? $warehouse['sbu']['name'] : '';
$address = isset($warehouse['address']) && $warehouse['address'] ? $warehouse['address'] : '';
$telephone_number = isset($warehouse['telephone_number']) && $warehouse['telephone_number'] ? $warehouse['telephone_number'] : '';
$fax_number = isset($warehouse['fax_number']) && $warehouse['fax_number'] ? $warehouse['fax_number'] : '';
$mobile_number = isset($warehouse['mobile_number']) && $warehouse['mobile_number'] ? $warehouse['mobile_number'] : '';
$email = isset($warehouse['email']) && $warehouse['email'] ? $warehouse['email'] : '';
$contact_person = isset($warehouse['contact_person']) && $warehouse['contact_person'] ? $warehouse['contact_person'] : '';
$is_active = isset($warehouse['is_active']) && $warehouse['is_active'] ? $warehouse['is_active'] : '';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label>Warehouse Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="warehouse_code" value="<?php echo set_value('warehouse_code', $warehouse_code); ?>" placeholder="Warehouse Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-barcode"></i></span>
            </div>
            <?php echo form_error('warehouse_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>SBU <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select class="form-control suggests" data-module="sbu"  id="sbu_id" name="sbu_id">
                    <option value="">Select SBU ID</option>
                    <?php if ($sbu_id): ?>
                        <option value="<?php echo @$sbu_id; ?>" selected><?php echo $sbu_id; ?></option>
                    <?php endif ?>
                </select>
                <!-- <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-asterisk"></i></span> -->
            </div>
            <?php echo form_error('sbu_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Address</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="address" value="<?php echo set_value('address', $address); ?>" placeholder="Address" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-pin"></i></span>
            </div>
            <?php echo form_error('address'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Telephone Number</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="telephone_number" value="<?php echo set_value('telephone_number', $telephone_number); ?>" placeholder="Telephone Number" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span>
            </div>
            <?php echo form_error('telephone_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Fax Number</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="fax_number" value="<?php echo set_value('fax_number', $fax_number); ?>" placeholder="Fax Number" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-fax"></i></span>
            </div>
            <?php echo form_error('fax_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Mobile Number</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="mobile_number" value="<?php echo set_value('mobile_number', $mobile_number); ?>" placeholder="Mobile Number" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span>
            </div>
            <?php echo form_error('mobile_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Email</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="email" value="<?php echo set_value('email', $email); ?>" placeholder="Email" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-envelope"></i></span>
            </div>
            <?php echo form_error('email'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Contact Person <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="contact_person" value="<?php echo set_value('contact_person', $contact_person); ?>" placeholder="Contact Person" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('contact_person'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Status <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), set_value('is_active', @$is_active), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>