<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
// ==================== begin: Add model fields ====================
$warehouse_code = isset($data['warehouse_code']) && $data['warehouse_code'] ? $data['warehouse_code'] : '';
$sbu_id = isset($data['sbu']) && $data['sbu'] ? $data['sbu']['name'] : '';
$address = isset($data['address']) && $data['address'] ? $data['address'] : '';
$telephone_number = isset($data['telephone_number']) && $data['telephone_number'] ? $data['telephone_number'] : '';
$fax_number = isset($data['fax_number']) && $data['fax_number'] ? $data['fax_number'] : '';
$mobile_number = isset($data['mobile_number']) && $data['mobile_number'] ? $data['mobile_number'] : '';
$email = isset($data['email']) && $data['email'] ? $data['email'] : '';
$contact_person = isset($data['contact_person']) && $data['contact_person'] ? $data['contact_person'] : '';
$is_active= isset($data['is_active']) && $data['is_active'] ? $data['is_active'] : '0';

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Warehouse</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('warehouse/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('warehouse');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $name; ?></span>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        SBU
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $sbu_id; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Address
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $address; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Telephone Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $telephone_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Fax Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $fax_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Mobile Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $mobile_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Email
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $email; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Contact Person
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $contact_person; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Status
                                    </span>
                                    <span class="kt-widget__data" style="font-weight: 500"><?php echo Dropdown::get_static('warehouse_status', $is_active, 'view'); ?></span>
                                </div>
                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
