<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Warehouse_model extends MY_Model {
    public $table = 'warehouse'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name',
        'warehouse_code',
        'sbu_id',
        'address',
        'telephone_number',
        'fax_number',
        'mobile_number',
        'email',
        'contact_person',
        'is_active',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */
        'warehouse_code' => array(
            'field' => 'warehouse_code',
            'label' => 'Warehouse Code',
            'rules' => 'trim|required'
        ),
        'sbu_id' => array(
            'field' => 'sbu_id',
            'label' => 'SBU ID',
            'rules' => 'trim|required'
        ),
        'address' => array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'trim'
        ),
        'telephone_number' => array(
            'field' => 'telephone_number',
            'label' => 'Telephone Number',
            'rules' => 'trim'
        ),
        'fax_number' => array(
            'field' => 'fax_number',
            'label' => 'Fax Number',
            'rules' => 'trim'
        ),
        'mobile_number' => array(
            'field' => 'mobile_number',
            'label' => 'Mobile Number',
            'rules' => 'trim'
        ),
        'email' => array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim'
        ),
        'contact_person' => array(
            'field' => 'contact_person',
            'label' => 'Contact Person',
            'rules' => 'trim|required'
        ),
        'is_active' => array(
            'field' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim|required'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['sbu'] = array('foreign_model' => 'sbu/Sbu_model', 'foreign_table' => 'sbu', 'foreign_key' => 'id', 'local_key' => 'sbu_id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
                'warehouse_code' => $faker->word,
                'sbu_id' => $faker->word,
                'address' => $faker->address,
                'telephone_number' => $faker->phoneNumber,
                'fax_number' => $faker->phoneNumber,
                'mobile_number' => $faker->phoneNumber,
                'email' => $faker->email,
                'contact_person' => $faker->name,
                'is_active' => $faker->numberBetween(0,1)
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}