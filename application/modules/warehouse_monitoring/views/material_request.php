<div>
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Material Request Monitoring</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc" id="total"></span>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                    <input type="text" class="form-control" placeholder="Search Material Request..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                        <span><i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
                        <i class="fa fa-upload"></i> Import
                    </button>
                    <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#_export_option">
                        <i class="fa fa-download"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="module__cta">
        <div class="kt-container  kt-container--fluid ">
            <div class="module__create">
                <a href="<?php echo site_url('material_request/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Create Material Request
                </a>
            </div>

            <div class="module__filter">
                <button class="btn btn-secondary btn-elevate btn-sm" id="resetFilters">
                    <i class="fa fa-refresh"></i> Reset
                </button>
                <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                    <i class="fa fa-filter"></i> Filter
                </button>
            </div>
        </div>
    </div> -->

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <!--begin: Advance Search -->
            <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
                <div class="row">
                    <div class="col-lg-12">
                        <form class="kt-form">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="form-control-label">ID</label>
                                    <div class="kt-input-icon  kt-input-icon--left">
                                        <input type="text" name="material_request_id" class="form-control form-control-sm _filter" placeholder="ID" id="_column_1" data-column="1">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-object-group"></i></span></span>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="form-control-label">Name</label>
                                    <div class="kt-input-icon  kt-input-icon--left">
                                        <input type="text" name="material_request_name" class="form-control form-control-sm _filter" placeholder="Name" id="_column_2" data-column="2">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-object-group"></i></span></span>
                                    </div>
                                </div>
                                <!-- ==================== begin: Add filter fields ==================== -->

                                <!-- ==================== end: Add filter fields ==================== -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end: Advance Search -->

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="material_request_table">
                <thead>
                    <tr>
                        <th width="1%">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                <span></span>
                            </label>
                        </th>
                        <th>ID</th>
                        <!-- ==================== begin: Add header fields ==================== -->
                        <th>Reference</th>
                        <th>Company</th>
                        <th>Request Type</th>
                        <th>Customer Type</th>
                        <th>Request Date</th>
                        <th>Request Status</th>
                        <th>Purchase Order Request(s)</th>
                        <!-- <th>Created By</th>
                        <th>Last Update By</th> -->
                        <!-- ==================== end: Add header fields ==================== -->
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->

        </div>
    </div>
</div>