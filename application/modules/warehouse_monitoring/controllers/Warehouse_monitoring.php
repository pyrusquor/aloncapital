<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Warehouse_monitoring extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model([
            'material_request/Material_request_model' => 'M_Material_request',
            'purchase_order_request/Purchase_order_request_model' => 'M_Purchase_order_request',
            'purchase_order_request_item/Purchase_order_request_item_model' => 'M_Purchase_order_request_item'
        ]);

        // specific models
        $this->load->model('material_request_item/Material_request_item_model', 'M_Material_request_item');

        // accounting entries?

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images', 'material_request_helper']); // Load Helper

        // Per page limit
        $this->perPage = 12;
    }

    public function get_all()
    {
        $data['row'] = $this->M_Material_request->get_all();

        echo json_encode($data);
    }

    private function __search($params)
    {

        $id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
        if (!$id) {
            $customer_type = isset($params['customer_type']) && !empty($params['customer_type']) ? $params['customer_type'] : null;
            $request_type = isset($params['request_type']) && !empty($params['request_type']) ? $params['request_type'] : null;
            $request_status = isset($params['request_status']) && !empty($params['request_status']) ? $params['request_status'] : null;

            $company_id = isset($params['company_id']) && !empty($params['company_id']) ? $params['company_id'] : null;
            $project_id = isset($params['project_id']) && !empty($params['project_id']) ? $params['project_id'] : null;
            $sub_project_id = isset($params['sub_project_id']) && !empty($params['sub_project_id']) ? $params['sub_project_id'] : null;
            $property_id = isset($params['property_id']) && !empty($params['property_id']) ? $params['property_id'] : null;
            $amenity_id = isset($params['amenity_id']) && !empty($params['amenity_id']) ? $params['amenity_id'] : null;
            $sub_amenity_id = isset($params['sub_amenity_id']) && !empty($params['sub_amenity_id']) ? $params['sub_amenity_id'] : null;
            $department_id = isset($params['department_id']) && !empty($params['department_id']) ? $params['department_id'] : null;
            $vehicle_equipment_id = isset($params['vehicle_equipment_id']) && !empty($params['vehicle_equipment_id']) ? $params['vehicle_equipment_id'] : null;
            $accounting_ledger_id = isset($params['accounting_ledger_id']) && !empty($params['accounting_ledger_id']) ? $params['accounting_ledger_id'] : null;
            $reference = isset($params['reference']) && !empty($params['reference']) ? $params['reference'] : null;
            $no_rpo = isset($params['no_rpo']) && @$params['no_rpo'] == 1 ? $params['no_rpo'] : null;

            if ($company_id) {
                $this->M_Material_request->where('company_id', $company_id);
            }

            if ($accounting_ledger_id) {
                $this->M_Material_request->where('accounting_ledger_id', $accounting_ledger_id);
            }

            if ($project_id !== null) {
                $this->M_Material_request->where('project_id', $project_id);
            }
            if ($sub_project_id !== null) {
                $this->M_Material_request->where('sub_project_id', $sub_project_id);
            }
            if ($property_id !== null) {
                $this->M_Material_request->where('property_id', $property_id);
            }
            if ($amenity_id !== null) {
                $this->M_Material_request->where('amenity_id', $amenity_id);
            }
            if ($sub_amenity_id !== null) {
                $this->M_Material_request->where('sub_amenity_id', $sub_amenity_id);
            }
            if ($department_id !== null) {
                $this->M_Material_request->where('department_id', $department_id);
            }
            if ($vehicle_equipment_id !== null) {
                $this->M_Material_request->where('vehicle_equipment_id', $vehicle_equipment_id);
            }
            if ($request_status !== null) {
                $this->M_Material_request->where('request_status', $request_status);
            }
            if ($request_type !== null) {
                $this->M_Material_request->where('request_type', $request_type);
            }
            if ($customer_type !== null) {
                $this->M_Material_request->where('customer_type', $customer_type);
            }
            if ($reference !== null) {
                $this->M_Material_request->like('reference', $reference, 'both');
            }
        } else {
            $this->M_Material_request->where('id', $id);
        }

        $this->M_Material_request->order_by('id', 'DESC');

        if ($with_relations === 'yes') {
            $result = $this->M_Material_request
                ->with_project()
                ->with_department()
                ->with_sub_project()
                ->with_company()
                ->with_amenity()
                ->with_property()
                ->with_sub_amenity()
                ->with_approving_staff()
                ->with_requesting_staff()
                ->with_accounting_ledger()
                ->get_all();
        } else {
            $result = $this->M_Material_request->get_all();
        }

        if ($no_rpo ?? null) {
            foreach ($result as $key => $r) {
                $rpo_items = $this->db->query("SELECT COUNT(`id`) AS `rpo_count` FROM `purchase_order_request_items` WHERE `material_request_id` = '" . $r['id'] . "'")->row_array();
                if ($rpo_items['rpo_count'] > 0) {
                    unset($result[$key]);
                }
            }
        }

        return $result;
    }

    public function search()
    {

        $result = $this->__search($_GET);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function material_request()
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('material_request');
    }

    public function showMaterialRequests()
    {
        header('Content-Type: application/json');
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'reference' => true,
            'company_id' => false,
            'company' => true,
            'customer_type' => true,
            'request_type' => true,
            'request_date' => true,
            'request_status' => true,
            'requesting_staff_id' => true,
            'approving_staff_id' => true,
            'purchase_order_requests' => true,
            'created_by' => true,
            'updated_by' => true,

            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data

        if (isset($_REQUEST['filter'])) {
            $_filters = $_GET;
            $material_requests = $this->__search($_filters);
        } else {
            $material_requests = $this->M_Material_request->with_company()->with_accounting_ledger()->order_by('id', 'DESC')->as_array()->get_all();
        }

        $data = [];

        if ($material_requests) {

            foreach ($material_requests as $key => $value) {

                $customer_ref = mr_customer_type_ref($value['customer_type']);

                $customer = isset($customer_ref['fk']) ? get_object_from_table($value[$customer_ref['fk']], $customer_ref['table'], false, 'name ASC', null, null, 'id, name') : null;

                $material_requests[$key]['customer_type'] = isset($value[$customer_ref['fk']]) ? "<a href='/" . $customer_ref['controller'] . '/view/' . $value[$customer_ref['fk']] . "'><span class='badge badge-secondary p-2 mr-2'>" . (ucfirst($value['customer_type']) ?? '') . "</span> " . $customer['name'] . "</a>" : '';

                $material_requests[$key]['request_date'] = view_date($value['request_date']);

                $material_requests[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $material_requests[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';

                $rpo_items = $this->M_Purchase_order_request_item
                    ->with_purchase_order_request()
                    ->where("material_request_id", $value['id'])
                    ->get_all();

                $purchase_order_requests = "";

                $ids = [];

                if (!!$rpo_items) {

                    foreach ($rpo_items as $item) {

                        if (isset($item['purchase_order_request']) && !in_array($item['purchase_order_request']['id'], $ids)) {

                            $purchase_order_requests .= "<a class='badge badge-primary mr-2' href='" . base_url() . "purchase_order_request/view/" . ($item['purchase_order_request']['id'] ?? '') . "'>" . ($item['purchase_order_request']['reference'] ?? '') . "</a>";

                            array_push($ids, $item['purchase_order_request']['id']);
                        }
                    }
                }

                $material_requests[$key]['purchase_order_requests'] = $purchase_order_requests;
            }

            foreach ($material_requests as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }
}
