<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_order_items_model extends MY_Model
{
    public $table = 'sales_order_items'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'sales_order_id',
        'item_code',
        'item_id',
        'unit_of_measure',
        'quantity',
        'unit_price',
        'subtotal',
        'grandtotal',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_one['payee'] = array('foreign_model' => 'user/User_model', 'foreign_table' => 'users', 'foreign_key' => 'id', 'local_key' => 'payee_type_id');
        // $this->has_one['accounting_entry'] = array('foreign_model' => 'accounting_entries/accounting_entries_model', 'foreign_table' => 'accounting_entries', 'foreign_key' => 'id', 'local_key' => 'accounting_entry_id');
        $this->has_one['item'] = array('foreign_model' => 'item/Item_model', 'foreign_table' => 'items', 'foreign_key' => 'id', 'local_key' => 'item_id');
    }

}
