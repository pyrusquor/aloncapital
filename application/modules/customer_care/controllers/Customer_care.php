<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_care extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $customer_care_models = array(
            'customer_care/Customer_care_model' => 'M_Customer_care',
            'complaint_tickets/Complaint_ticket_model' => 'M_Complaint_ticket',
            'complaint_tickets/Complaint_ticket_log_model' => 'M_Complaint_ticket_log',
            'complaint_categories/Complaint_category_model' => 'M_Complaint_category',
            'inquiry_tickets/Inquiry_ticket_model' => 'M_Inquiry_ticket',
            'inquiry_tickets/Inquiry_ticket_log_model' => 'M_Inquiry_ticket_log',
            'inquiry_categories/Inquiry_category_model' => 'M_Inquiry_category',
        );

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model($customer_care_models);

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper
        $this->load->helper('form');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Complaint_ticket->fillable;
        $this->_table_columns = $this->M_Complaint_ticket->__get_columns();
    }

    public function index()
    {
        $this->template->build('index', $this->view_data);
    }

    public function complaint_table()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Complaint_ticket->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Complaint_ticket->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('complaints_table', $this->view_data);
    }

    public function showComplaints()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'buyer_id' => true,
            'project_id' => true,
            'house_model_id' => true,
            'complaint_category_id' => true,
            'complaint_sub_category_id' => true,
            'year' => true,
            'month' => true,
            'reference' => true,
            'buyer_name' => true,
            'created_at' => true,
            'project_name' => true,
            'block_lot' => true,
            'turn_over_date' => true,
            'warranty' => true,
            'house_model_name' => true,
            'description' => true,
            'forwarded_date' => true,
            'closed_date' => true,
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $complaints = $this->M_Complaint_ticket
            ->with_project()
            ->with_house_model()
            ->with_property()
            ->with_buyer()
            ->with_complaint_category()
            ->with_complaint_sub_category()
            ->with_complaint_logs()
            ->order_by('id', 'DESC')->as_array()->get_all();
        $data = [];

        if ($complaints) {

            foreach ($complaints as $key => $value) {
                $complaints[$key]['buyer_name'] = $value['buyer']['first_name'] . ' ' . $value['buyer']['last_name'];
                $complaints[$key]['project_name'] = $value['project']['name'];
                $complaints[$key]['block_lot'] = $value['property']['block'] . '/' . $value['property']['lot'];
                $complaints[$key]['house_model_name'] = $value['house_model']['name'];
                $complaints[$key]['year'] = date('Y', strtotime($value['created_at']));
                $complaints[$key]['month'] = date('m', strtotime($value['created_at']));

                if (isset($complaints[$key]['complaint_logs'])) {
                    $complaint_logs = $complaints[$key]['complaint_logs'];
                    if ($complaint_logs) {
                        foreach ($complaint_logs as $id => $log) {
                            if ($log['status'] == 2) {
                                $complaints[$key]['forwarded_date'] = isset($log['created_at']) ? $log['created_at'] : '--';
                            }
                            if ($log['status'] == 6) {
                                $complaints[$key]['closed_date'] = isset($log['created_at']) ? $log['created_at'] : '--';
                            }
                        }
                    }
                } else {
                    $complaints[$key]['forwarded_date'] = '--';
                    $complaints[$key]['closed_date'] = '--';
                }

                if (!isset($complaints[$key]['forwarded_date'])) {
                    $complaints[$key]['forwarded_date'] = '--';
                }

                if (!isset($complaints[$key]['closed_date'])) {
                    $complaints[$key]['closed_date'] = '--';
                }
            }

            foreach ($complaints as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function inquiry_table()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Inquiry_ticket->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Inquiry_ticket->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('inquiries_table', $this->view_data);
    }

    public function showInquiries()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'buyer_id' => true,
            'project_id' => true,
            'house_model_id' => true,
            'inquiry_category_id' => true,
            'inquiry_sub_category_id' => true,
            'year' => true,
            'month' => true,
            'reference' => true,
            'buyer_name' => true,
            'created_at' => true,
            'project_name' => true,
            'block_lot' => true,
            'turn_over_date' => true,
            'warranty' => true,
            'house_model_name' => true,
            'description' => true,
            'forwarded_date' => true,
            'closed_date' => true,
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $inquiries = $this->M_Inquiry_ticket
            ->with_project()
            ->with_house_model()
            ->with_property()
            ->with_buyer()
            ->with_inquiry_category()
            ->with_inquiry_sub_category()
            ->with_inquiry_logs()
            ->order_by('id', 'DESC')->as_array()->get_all();
        $data = [];

        if ($inquiries) {

            foreach ($inquiries as $key => $value) {
                $inquiries[$key]['buyer_name'] = $value['buyer']['first_name'] . ' ' . $value['buyer']['last_name'];
                $inquiries[$key]['project_name'] = @$value['project']['name'];
                $inquiries[$key]['block_lot'] = $value['property']['block'] . '/' . $value['property']['lot'];
                $inquiries[$key]['house_model_name'] = isset($value['house_model']['name']) && $value['house_model']['name'] ? $value['house_model']['name'] : '';;
                $inquiries[$key]['year'] = date('Y', strtotime($value['created_at']));
                $inquiries[$key]['month'] = date('m', strtotime($value['created_at']));

                if (isset($inquiries[$key]['inquiry_logs'])) {
                    $inquiry_logs = $inquiries[$key]['inquiry_logs'];
                    if ($inquiry_logs) {
                        foreach ($inquiry_logs as $id => $log) {
                            if ($log['status'] == 2) {
                                $inquiries[$key]['forwarded_date'] = isset($log['created_at']) ? $log['created_at'] : '--';
                            }
                            if ($log['status'] == 6) {
                                $inquiries[$key]['closed_date'] = isset($log['created_at']) ? $log['created_at'] : '--';
                            }
                        }
                    }
                } else {
                    $inquiries[$key]['forwarded_date'] = '--';
                    $inquiries[$key]['closed_date'] = '--';
                }

                if (!isset($inquiries[$key]['forwarded_date'])) {
                    $inquiries[$key]['forwarded_date'] = '--';
                }

                if (!isset($inquiries[$key]['closed_date'])) {
                    $inquiries[$key]['closed_date'] = '--';
                }
            }

            foreach ($inquiries as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function complaint_annual_report()
    {
        $this->view_data['complaint_categories'] = $complaint_categories = $this->M_Complaint_category->as_array()->get_all();
        $data = [];
        $year = $this->input->post('year');

        if (!$year) {
            $year = date('Y');
        }

        $this->view_data['year'] = $year;

        // loop per month
        for ($month = 1; $month <= 12; $month++) {
            // Set date range by month
            $params['daterange'] = date($year . '/' . $month . '/01 00:00:00') . "-" . date($year . '/' . $month . '/31 23:59:59');

            // Get month name
            $dateObj   = DateTime::createFromFormat('!m', $month);
            $month_name = $dateObj->format('F');

            foreach ($complaint_categories as $key => $value) {
                $params['complaint_category_id'] = $value['id'];
                $count = $this->M_Customer_care->get_annual_report($params, 'complaint_tickets');

                $result[$value['name']] = $count['count'];
            }

            $data[$month_name] = $result;
        }

        if (!empty($data)) {
            $this->view_data['data'] = $data;
        }

        $this->template->build('complaints_annual_report', $this->view_data);
    }

    public function complaint_category_report()
    {
        $date_range = $this->input->post('date_range');
        $year = $this->input->post('year');

        if ($date_range) {
            $params['daterange'] = $date_range;
        }

        if (!$year) {
            $year = date('Y');
        }

        $this->view_data['year'] = $year;

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $data = $this->M_Customer_care->get_complaint_by_sub_category($params, 'complaint_tickets', 'complaint_categories', 'complaint_sub_categories');

        $this->view_data['data'] = $data;

        $this->template->build('complaints_category_report', $this->view_data);
    }

    public function complaint_house_model_report()
    {
        $date_range = $this->input->post('date_range');
        $year = $this->input->post('year');

        if ($date_range) {
            $params['daterange'] = $date_range;
        }

        if (!$year) {
            $year = date('Y');
        }

        $this->view_data['year'] = $year;

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $data = $this->M_Customer_care->get_house_model_report($params, 'complaint_tickets');

        $this->view_data['data'] = $data;

        $this->template->build('complaints_house_model_report', $this->view_data);
    }

    public function complaint_project_report()
    {
        $date_range = $this->input->post('date_range');
        $year = $this->input->post('year');

        if ($date_range) {
            $params['daterange'] = $date_range;
        }

        if (!$year) {
            $year = date('Y');
        }

        $this->view_data['year'] = $year;

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $data = $this->M_Customer_care->get_project_report($params, 'complaint_tickets');

        $this->view_data['data'] = $data;

        $this->template->build('complaints_project_report', $this->view_data);
    }

    public function inquiry_annual_report()
    {
        $this->view_data['inquiry_categories'] = $inquiry_categories = $this->M_Inquiry_category->as_array()->get_all();
        $data = [];
        $year = $this->input->post('year');

        if (!$year) {
            $year = date('Y');
        }

        $this->view_data['year'] = $year;

        if ($inquiry_categories) {
            // loop per month
            for ($month = 1; $month <= 12; $month++) {
                // Set date range by month
                $params['daterange'] = date($year . '/' . $month . '/01 00:00:00') . "-" . date($year . '/' . $month . '/31 23:59:59');

                // Get month name
                $dateObj   = DateTime::createFromFormat('!m', $month);
                $month_name = $dateObj->format('F');

                foreach ($inquiry_categories as $key => $value) {
                    $params['inquiry_category_id'] = $value['id'];
                    $count = $this->M_Customer_care->get_annual_report($params, 'inquiry_tickets');

                    $result[$value['name']] = $count['count'];
                }

                $data[$month_name] = $result;
            }

            if (!empty($data)) {
                $this->view_data['data'] = $data;
            }
        }

        $this->template->build('inquiries_annual_report', $this->view_data);
    }

    public function inquiry_category_report()
    {
        $date_range = $this->input->post('date_range');
        $year = $this->input->post('year');

        if ($date_range) {
            $params['daterange'] = $date_range;
        }

        if (!$year) {
            $year = date('Y');
        }

        $this->view_data['year'] = $year;

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $data = $this->M_Customer_care->get_complaint_by_sub_category($params, 'inquiry_tickets', 'inquiry_categories', 'inquiry_sub_categories');

        $this->view_data['data'] = $data;

        $this->template->build('inquiries_category_report', $this->view_data);
    }

    public function inquiry_house_model_report()
    {
        $date_range = $this->input->post('date_range');
        $year = $this->input->post('year');

        if ($date_range) {
            $params['daterange'] = $date_range;
        }

        if (!$year) {
            $year = date('Y');
        }

        $this->view_data['year'] = $year;

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $data = $this->M_Customer_care->get_house_model_report($params, 'inquiry_tickets');

        $this->view_data['data'] = $data;

        $this->template->build('inquiries_house_model_report', $this->view_data);
    }

    public function inquiry_project_report()
    {
        $date_range = $this->input->post('date_range');
        $year = $this->input->post('year');

        if ($date_range) {
            $params['daterange'] = $date_range;
        }

        if (!$year) {
            $year = date('Y');
        }

        $this->view_data['year'] = $year;

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $data = $this->M_Customer_care->get_project_report($params, 'inquiry_tickets');

        $this->view_data['data'] = $data;

        $this->template->build('inquiries_project_report', $this->view_data);
    }

    public function export_complaint()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $complaints    =    $this->M_Complaint_ticket
            ->with_project()
            ->with_house_model()
            ->with_property()
            ->with_buyer()
            ->with_department()
            ->with_complaint_category()
            ->with_complaint_sub_category()
            ->with_complaint_logs()
            ->with_transaction()
            ->as_array()
            ->get_all();

        // vdebug($complaints);

        if ($complaints) {

            foreach ($complaints as $_lkey => $complaint) {

                $_datas[$complaint['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_complaints_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
                }
            } else {

                $_filename    =    'list_of_complaints' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($complaints as $_lkey => $complaint) {

                                if ($_name == "buyer") {

                                    $_datas[$complaint['id']][$_title]    =    isset($complaint[$_name]) && ($complaint[$_name] !== '') ? $complaint[$_name]['first_name'] . " " . $complaint[$_name]['last_name'] : '';
                                } else if ($_name == "transaction") {

                                    $_datas[$complaint['id']][$_title]    =    isset($complaint[$_name]) && ($complaint[$_name] !== '') ? $complaint[$_name]['reference'] : '';
                                } else {

                                    $_datas[$complaint['id']][$_title]    =    isset($complaint[$_name]) && ($complaint[$_name] !== '') ? $complaint[$_name]['name'] : '';
                                }
                            }
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($complaints as $_lkey => $complaint) {

                                $_datas[$complaint['id']][$_title]    =    isset($complaint[$_column]) && ($complaint[$_column] !== '') ? Dropdown::get_static('bool', $complaint[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($complaints as $_lkey => $complaint) {

                                if ($_name === 'status') {

                                    $_datas[$complaint['id']][$_name] = isset($complaint[$_name]) && $complaint[$_name] ? Dropdown::get_static('customer_care_status', !empty($complaint['complaint_logs']) ? end($complaint['complaint_logs'])['status'] : $complaint[$_name], 'view') : '';
                                } else if ($_name === 'warranty') {

                                    $_datas[$complaint['id']][$_name] = Dropdown::get_static('bool', $complaint[$_name], 'view') ?? '';
                                } else if ($_name === 'turn_over_date') {

                                    $_datas[$complaint['id']][$_title] = $complaint[$_name] != "0000-00-00 00:00:00" ? (view_date(substr($complaint[$_name], 0, 10)) ?? '') : '';
                                } else {

                                    $_datas[$complaint['id']][$_title] = isset($complaint[$_name]) && $complaint[$_name] ? $complaint[$_name] : '';
                                }

                                $_datas[$complaint['id']]['started'] = $complaint['created_at'];

                                if ($complaint['complaint_logs']) {

                                    foreach ([2, 3, 5, 6] as $value) {

                                        $logs = array_filter($complaint['complaint_logs'], function ($item) use ($value) {
                                            return $item['status'] == $value;
                                        });

                                        $date = null;

                                        if ($logs) {

                                            if ($value == 2) {

                                                $current_log = $logs[0];

                                                $date = $current_log['created_at'];
                                            } else {

                                                foreach ($logs as $_key => $_value) {

                                                    if (!$date) {

                                                        $date = $_value['created_at'];
                                                    } else {

                                                        if ((new DateTime($_value['created_at'])) > (new DateTime($date))) {

                                                            $date = $_value['created_at'];
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if ($value == 2) {

                                            $_datas[$complaint['id']]['forwarded'] = $date ?? '';
                                        } else if ($value == 3) {

                                            $_datas[$complaint['id']]['work in progress'] = $date ?? '';
                                        } else if ($value == 5) {

                                            $_datas[$complaint['id']]['completed'] = $date ?? '';
                                        } else if ($value == 6) {

                                            $_datas[$complaint['id']]['closed'] = $date ?? '';
                                        }
                                    }
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                vdebug($_datas);

                array_push($_titles, 'started', 'forwarded', 'work in progress', 'completed', 'closed');

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of complaints');
                $_objSheet->setCellValue('A1', 'LIST OF complaints');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
            }
        } else {

            $this->notify->error('No Record Found', 'customer_care');
        }
    }

    public function export_inquiry()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $inquiries    =    $this->M_Inquiry_ticket
            ->with_project()
            ->with_house_model()
            ->with_property()
            ->with_buyer()
            ->with_department()
            ->with_complaint_category()
            ->with_complaint_sub_category()
            ->with_inquiry_logs()
            ->with_transaction()
            ->as_array()
            ->get_all();

        if ($inquiries) {

            foreach ($inquiries as $_lkey => $inquiry) {

                $_datas[$inquiry['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_inquiries_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
                }
            } else {

                $_filename    =    'list_of_inquiries' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($inquiries as $_lkey => $inquiry) {

                                if ($_name == "buyer") {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_name]) && ($inquiry[$_name] !== '') ? $inquiry[$_name]['first_name'] . " " . $inquiry[$_name]['last_name'] : '';
                                } else if ($_name == "transaction") {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_name]) && ($inquiry[$_name] !== '') ? $inquiry[$_name]['reference'] : '';
                                } else {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_name]) && ($inquiry[$_name] !== '') ? $inquiry[$_name]['name'] : '';
                                }
                            }
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($inquiries as $_lkey => $inquiry) {

                                $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_column]) && ($inquiry[$_column] !== '') ? Dropdown::get_static('bool', $inquiry[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($inquiries as $_lkey => $inquiry) {

                                if ($_name === 'status') {

                                    // $_datas[$inquiry['id']][$_name]    =    isset($inquiry[$_name]) && $inquiry[$_name] ? Dropdown::get_static('customer_care_status', $inquiry[$_name], 'view') : '';

                                    $_datas[$inquiry['id']][$_name] = isset($inquiry[$_name]) && $inquiry[$_name] ? Dropdown::get_static('customer_care_status', !empty($inquiry['inquiry_logs']) ? end($inquiry['inquiry_logs'])['status'] : $inquiry[$_name], 'view') : '';
                                } else if ($_name === 'warranty') {

                                    $_datas[$inquiry['id']][$_name] = Dropdown::get_static('bool', $inquiry[$_name], 'view') ?? '';
                                } else if ($_name === 'turn_over_date') {

                                    $_datas[$inquiry['id']][$_title] = $inquiry[$_name] != "0000-00-00 00:00:00" ? (view_date(substr($inquiry[$_name], 0, 10)) ?? '') : '';
                                } else {

                                    $_datas[$inquiry['id']][$_title]    =    isset($inquiry[$_name]) && $inquiry[$_name] ? $inquiry[$_name] : '';
                                }

                                $_datas[$inquiry['id']]['started'] = $inquiry['created_at'];

                                if ($inquiry['inquiry_logs']) {

                                    foreach ([2, 3, 5, 6] as $value) {

                                        $logs = array_filter($inquiry['inquiry_logs'], function ($item) use ($value) {
                                            return $item['status'] == $value;
                                        });

                                        $date = null;

                                        if ($logs) {

                                            if ($value == 2) {

                                                $current_log = $logs[0];

                                                $date = $current_log['created_at'];
                                            } else {

                                                foreach ($logs as $_key => $_value) {

                                                    if (!$date) {

                                                        $date = $_value['created_at'];
                                                    } else {

                                                        if ((new DateTime($_value['created_at'])) > (new DateTime($date))) {

                                                            $date = $_value['created_at'];
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if ($value == 2) {

                                            $_datas[$inquiry['id']]['forwarded'] = $date ?? '';
                                        } else if ($value == 3) {

                                            $_datas[$inquiry['id']]['work in progress'] = $date ?? '';
                                        } else if ($value == 5) {

                                            $_datas[$inquiry['id']]['completed'] = $date ?? '';
                                        } else if ($value == 6) {

                                            $_datas[$inquiry['id']]['closed'] = $date ?? '';
                                        }
                                    }
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                array_push($_titles, 'started', 'forwarded', 'work in progress', 'completed', 'closed');

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of inquiries');
                $_objSheet->setCellValue('A1', 'LIST OF inquiries');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'customer_care');
            }
        } else {

            $this->notify->error('No Record Found', 'customer_care');
        }
    }

    public function export_annual_complaints()
    {
        $_alphas = [];
        $_datas = [];

        $_titles[] = 'month';

        $_start = 3;
        $_row = 2;

        $year = $this->input->post('year_export') ?? date('Y');

        $complaint_categories = $this->M_Complaint_category->as_array()->get_all();

        foreach (range(1, 12) as $month) {

            // Set date range by month
            $params['daterange'] = date($year . '/' . $month . '/01 00:00:00') . "-" . date($year . '/' . $month . '/31 23:59:59');

            // Get month name
            $dateObj   = DateTime::createFromFormat('!m', $month);
            $month_name = $dateObj->format('F');

            $result['month'] = $month_name;

            foreach ($complaint_categories as $key => $value) {
                $params['complaint_category_id'] = $value['id'];
                $count = $this->M_Customer_care->get_annual_report($params, 'complaint_tickets');

                $result[$value['name']] = $count['count'];
            }

            $_datas[] = $result;
        }

        foreach ($complaint_categories as $key => $value) {

            array_push($_titles, $value['name']);
        }

        if ($_datas) {

            $_filename = $year . '_annual_complaint_statistics_reports_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            $_alphas = $this->__get_excel_columns(count($_titles));

            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            foreach ($_xls_columns as $_xkey => $_column) {

                $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                $_objSheet->setCellValue($_xkey . $_row, $_title);
            }

            $_objSheet->setTitle($year . ' ANNUAL COMPLAINT STATISTICS REPORTS');
            $_objSheet->setCellValue('A1', $year . ' ANNUAL COMPLAINT STATISTICS REPORTS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            if (isset($_datas) && $_datas) {

                foreach ($_datas as $_dkey => $_data) {

                    foreach ($_alphas as $_akey => $_alpha) {

                        $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start++;
                }
            } else {

                $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                $_style = array(
                    'font'  => array(
                        'bold' => FALSE,
                        'size' => 9,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            $this->notify->error('No Record Found For Year ' . $year, 'customer_care/complaint_annual_report');
        }
    }

    public function export_complaint_category()
    {
        $_alphas = [];
        $_datas = [];

        $_titles = [];

        $_start = 3;
        $_row = 2;

        $year = $this->input->post('year_export') ?? date('Y');

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $_datas = $this->M_Customer_care->get_complaint_by_sub_category($params, 'complaint_tickets', 'complaint_categories', 'complaint_sub_categories');

        array_push($_titles, 'category', 'sub_category', 'count');

        foreach ($_datas as $key => $value) {

            unset($_datas[$key]['sub_category_id']);
        }

        // vdebug($_datas);

        if ($_datas) {

            $_filename = $year . '_complaint_category_report_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            $_alphas = $this->__get_excel_columns(count($_titles));

            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            foreach ($_xls_columns as $_xkey => $_column) {

                if ($_column == "count") {
                    $_column = "no._of_complaints";
                }

                $_title = ($_column !== 'ID') ? ucwords(strtolower(str_replace('_', ' ', $_column))) : $_column;

                $_objSheet->setCellValue($_xkey . $_row, $_title);
            }

            $_objSheet->setTitle($year . ' COMPLAINT CATEGORY REPORTS');
            $_objSheet->setCellValue('A1', $year . ' COMPLAINT CATEGORY REPORTS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            if (isset($_datas) && $_datas) {

                foreach ($_datas as $_dkey => $_data) {

                    foreach ($_alphas as $_akey => $_alpha) {

                        $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start++;
                }
            } else {

                $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                $_style = array(
                    'font'  => array(
                        'bold' => FALSE,
                        'size' => 9,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            $this->notify->error('No Record Found For Year ' . $year, 'customer_care/complaint_category_report');
        }
    }

    public function export_house_model_complaints()
    {
        $_alphas = [];
        $_datas = [];

        $_titles = [];

        $_start = 3;
        $_row = 2;

        $year = $this->input->post('year_export') ?? date('Y');

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $_datas = $this->M_Customer_care->get_house_model_report($params, 'complaint_tickets');

        array_push($_titles, 'name', 'count');

        foreach ($_datas as $key => $value) {

            unset($_datas[$key]['sub_category_id']);
        }

        if ($_datas) {

            $_filename = $year . '_complaints_per_house_model_report_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            $_alphas = $this->__get_excel_columns(count($_titles));

            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            foreach ($_xls_columns as $_xkey => $_column) {

                if ($_column == "name") {
                    $_column = "house_model";
                } else if ($_column == "count") {
                    $_column = "no._of_complaints";
                }

                $_title = ($_column !== 'ID') ? ucwords(strtolower(str_replace('_', ' ', $_column))) : $_column;

                $_objSheet->setCellValue($_xkey . $_row, $_title);
            }

            $_objSheet->setTitle($year . ' COMPLAINTS PER HOUSE MODEL REPORTS');
            $_objSheet->setCellValue('A1', $year . ' COMPLAINTS PER HOUSE MODEL REPORTS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            if (isset($_datas) && $_datas) {

                foreach ($_datas as $_dkey => $_data) {

                    foreach ($_alphas as $_akey => $_alpha) {

                        $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start++;
                }
            } else {

                $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                $_style = array(
                    'font'  => array(
                        'bold' => FALSE,
                        'size' => 9,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            $this->notify->error('No Record Found For Year ' . $year, 'customer_care/complaint_house_model_report');
        }
    }

    public function export_complaint_project()
    {
        $_alphas = [];
        $_datas = [];

        $_titles = [];

        $_start = 3;
        $_row = 2;

        $year = $this->input->post('year_export') ?? date('Y');

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $_datas = $this->M_Customer_care->get_project_report($params, 'complaint_tickets');

        array_push($_titles, 'name', 'count');

        foreach ($_datas as $key => $value) {

            unset($_datas[$key]['project_id']);
        }

        if ($_datas) {

            $_filename = $year . '_complaints_per_project_report_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            $_alphas = $this->__get_excel_columns(count($_titles));

            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            foreach ($_xls_columns as $_xkey => $_column) {

                if ($_column == "name") {
                    $_column = "project";
                } else if ($_column == "count") {
                    $_column = "no._of_complaints";
                }

                $_title = ($_column !== 'ID') ? ucwords(strtolower(str_replace('_', ' ', $_column))) : $_column;

                $_objSheet->setCellValue($_xkey . $_row, $_title);
            }

            $_objSheet->setTitle($year . ' COMPLAINTS PER PROJECT REPORTS');
            $_objSheet->setCellValue('A1', $year . ' COMPLAINTS PER PROJECT REPORTS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            if (isset($_datas) && $_datas) {

                foreach ($_datas as $_dkey => $_data) {

                    foreach ($_alphas as $_akey => $_alpha) {

                        $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start++;
                }
            } else {

                $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                $_style = array(
                    'font'  => array(
                        'bold' => FALSE,
                        'size' => 9,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            $this->notify->error('No Record Found For Year ' . $year, 'customer_care/complaint_project_report');
        }
    }

    public function export_annual_inquiry()
    {
        $_alphas = [];
        $_datas = [];

        $_titles[] = 'month';

        $_start = 3;
        $_row = 2;

        $year = $this->input->post('year_export') ?? date('Y');

        $inquiry_categories = $this->M_Inquiry_category->as_array()->get_all();

        foreach (range(1, 12) as $month) {

            // Set date range by month
            $params['daterange'] = date($year . '/' . $month . '/01 00:00:00') . "-" . date($year . '/' . $month . '/31 23:59:59');

            // Get month name
            $dateObj   = DateTime::createFromFormat('!m', $month);
            $month_name = $dateObj->format('F');

            $result['month'] = $month_name;

            foreach ($inquiry_categories as $key => $value) {
                $params['inquiry_category_id'] = $value['id'];
                $count =  $this->M_Customer_care->get_annual_report($params, 'inquiry_tickets');

                $result[$value['name']] = $count['count'];
            }

            $_datas[] = $result;
        }

        foreach ($inquiry_categories as $key => $value) {

            array_push($_titles, $value['name']);
        }

        if ($_datas) {

            $_filename = $year . '_annual_inquiry_statistics_reports_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            $_alphas = $this->__get_excel_columns(count($_titles));

            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            foreach ($_xls_columns as $_xkey => $_column) {

                $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                $_objSheet->setCellValue($_xkey . $_row, $_title);
            }

            $_objSheet->setTitle($year . ' ANNUAL INQUIRY STATISTICS REPORTS');
            $_objSheet->setCellValue('A1', $year . ' ANNUAL INQUIRY STATISTICS REPORTS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            if (isset($_datas) && $_datas) {

                foreach ($_datas as $_dkey => $_data) {

                    foreach ($_alphas as $_akey => $_alpha) {

                        $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start++;
                }
            } else {

                $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                $_style = array(
                    'font'  => array(
                        'bold' => FALSE,
                        'size' => 9,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            $this->notify->error('No Record Found For Year ' . $year, 'customer_care/complaint_annual_report');
        }
    }

    public function export_inquiry_category()
    {
        $_alphas = [];
        $_datas = [];

        $_titles = [];

        $_start = 3;
        $_row = 2;

        $year = $this->input->post('year_export') ?? date('Y');

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $_datas = $this->M_Customer_care->get_complaint_by_sub_category($params, 'inquiry_tickets', 'inquiry_categories', 'inquiry_sub_categories');

        array_push($_titles, 'category', 'sub_category', 'count');

        foreach ($_datas as $key => $value) {

            unset($_datas[$key]['sub_category_id']);
        }

        // vdebug($_datas);

        if ($_datas) {

            $_filename = $year . '_inquiry_category_report_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            $_alphas = $this->__get_excel_columns(count($_titles));

            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            foreach ($_xls_columns as $_xkey => $_column) {

                if ($_column == "count") {
                    $_column = "no._of_inquiries";
                }

                $_title = ($_column !== 'ID') ? ucwords(strtolower(str_replace('_', ' ', $_column))) : $_column;

                $_objSheet->setCellValue($_xkey . $_row, $_title);
            }

            $_objSheet->setTitle($year . ' INQUIRY CATEGORY REPORTS');
            $_objSheet->setCellValue('A1', $year . ' INQUIRY CATEGORY REPORTS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            if (isset($_datas) && $_datas) {

                foreach ($_datas as $_dkey => $_data) {

                    foreach ($_alphas as $_akey => $_alpha) {

                        $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start++;
                }
            } else {

                $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                $_style = array(
                    'font'  => array(
                        'bold' => FALSE,
                        'size' => 9,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            $this->notify->error('No Record Found For Year ' . $year, 'customer_care/inquiry_category_report');
        }
    }

    public function export_house_model_inquiries()
    {
        $_alphas = [];
        $_datas = [];

        $_titles = [];

        $_start = 3;
        $_row = 2;

        $year = $this->input->post('year_export') ?? date('Y');

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $_datas = $this->M_Customer_care->get_house_model_report($params, 'inquiry_tickets');

        array_push($_titles, 'name', 'count');

        foreach ($_datas as $key => $value) {

            unset($_datas[$key]['sub_category_id']);
        }

        if ($_datas) {

            $_filename = $year . '_inquiry_per_house_model_report_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            $_alphas = $this->__get_excel_columns(count($_titles));

            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            foreach ($_xls_columns as $_xkey => $_column) {

                if ($_column == "name") {
                    $_column = "house_model";
                } else if ($_column == "count") {
                    $_column = "no._of_inquiries";
                }

                $_title = ($_column !== 'ID') ? ucwords(strtolower(str_replace('_', ' ', $_column))) : $_column;

                $_objSheet->setCellValue($_xkey . $_row, $_title);
            }

            $_objSheet->setTitle($year . ' INQUIRIES PER HOUSE MODEL REPORTS');
            $_objSheet->setCellValue('A1', $year . ' INQUIRIES PER HOUSE MODEL REPORTS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            if (isset($_datas) && $_datas) {

                foreach ($_datas as $_dkey => $_data) {

                    foreach ($_alphas as $_akey => $_alpha) {

                        $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start++;
                }
            } else {

                $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                $_style = array(
                    'font'  => array(
                        'bold' => FALSE,
                        'size' => 9,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            $this->notify->error('No Record Found For Year ' . $year, 'customer_care/complaint_house_model_report');
        }
    }

    public function export_inquiry_project()
    {
        $_alphas = [];
        $_datas = [];

        $_titles = [];

        $_start = 3;
        $_row = 2;

        $year = $this->input->post('year_export') ?? date('Y');

        $params['daterange'] = date($year . '/01/01 00:00:00') . "-" . date($year . '/12/31 23:59:59');

        $_datas = $this->M_Customer_care->get_project_report($params, 'inquiry_tickets');

        array_push($_titles, 'name', 'count');

        foreach ($_datas as $key => $value) {

            unset($_datas[$key]['project_id']);
        }

        if ($_datas) {

            $_filename = $year . '_inquiry_per_project_report_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            $_alphas = $this->__get_excel_columns(count($_titles));

            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            foreach ($_xls_columns as $_xkey => $_column) {

                if ($_column == "name") {
                    $_column = "project";
                } else if ($_column == "count") {
                    $_column = "no._of_inquiries";
                }

                $_title = ($_column !== 'ID') ? ucwords(strtolower(str_replace('_', ' ', $_column))) : $_column;

                $_objSheet->setCellValue($_xkey . $_row, $_title);
            }

            $_objSheet->setTitle($year . ' INQUIRIES PER PROJECT REPORTS');
            $_objSheet->setCellValue('A1', $year . ' INQUIRIES PER PROJECT REPORTS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            if (isset($_datas) && $_datas) {

                foreach ($_datas as $_dkey => $_data) {

                    foreach ($_alphas as $_akey => $_alpha) {

                        $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    $_start++;
                }
            } else {

                $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                $_style = array(
                    'font'  => array(
                        'bold' => FALSE,
                        'size' => 9,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            $this->notify->error('No Record Found For Year ' . $year, 'customer_care/complaint_project_report');
        }
    }
}
