<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_care_model extends MY_Model
{
    public function get_annual_report($params = array(), $table)
    {
        $this->db->select('COUNT(' . $table . '.id) as count,');

        $this->db->from($table);

        if (!empty($params['daterange'])) {
            $dates = explode('-', $params['daterange']);
            $f_date_from = db_date($dates[0]);
            $f_date_to = db_date($dates[1]);

            $this->db->where($table . '.created_at BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
        }

        if (!empty($params['complaint_category_id'])) {
            $this->db->where('complaint_tickets.complaint_category_id', $params['complaint_category_id']);
        }

        if (!empty($params['inquiry_category_id'])) {
            $this->db->where('inquiry_tickets.inquiry_category_id', $params['inquiry_category_id']);
        }

        $this->db->where($table . '.deleted_at IS NULL');

        $query = $this->db->get()->row_array();

        return $query;
    }

    public function get_complaint_by_sub_category($params = array(), $table, $category_table, $sub_category_table)
    {
        $type = explode("_", $table)[0];

        $this->db->select($table . '.' . $type . '_sub_category_id as sub_category_id' . ', ' . $category_table . '.name as category, ' . $sub_category_table . '.name as sub_category, COUNT(' . $table . '.id) as count, ');

        $this->db->from($table);

        $this->db->join($category_table, $table . '.' . $type . '_category_id = ' . $category_table . '.id', 'left');

        $this->db->join($sub_category_table, $table . '.' . $type . '_sub_category_id = ' . $sub_category_table . '.id', 'left');

        if (!empty($params['daterange'])) {
            $dates = explode('-', $params['daterange']);
            $f_date_from = db_date($dates[0]);
            $f_date_to = db_date($dates[1]);

            $this->db->where($table . '.created_at BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
        }

        $this->db->where($table . '.deleted_at IS NULL');
        $this->db->group_by('sub_category_id');
        $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function get_house_model_report($params = array(), $table)
    {

        // $this->db->select($table.'.house_model_id, house_models.name, COUNT('.$table.'.id) as count,');
        $this->db->select('house_models.name, COUNT(house_models.id) as count');

        $this->db->from($table);

        $this->db->join('house_models', $table . '.house_model_id = house_models.id', 'left');

        if (!empty($params['daterange'])) {
            $dates = explode('-', $params['daterange']);
            $f_date_from = db_date($dates[0]);
            $f_date_to = db_date($dates[1]);

            $this->db->where($table . '.created_at BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
        }

        $this->db->where($table . '.deleted_at IS NULL');

        // $this->db->group_by($table.'.house_model_id');
        $this->db->group_by('house_models.name');

        $query = $this->db->get()->result_array();

        return $query;
    }

    public function get_project_report($params = array(), $table)
    {
        $this->db->select($table . '.project_id, projects.name, COUNT(' . $table . '.id) as count,');

        $this->db->from($table);

        $this->db->join('projects', $table . '.project_id = projects.id', 'left');

        if (!empty($params['daterange'])) {
            $dates = explode('-', $params['daterange']);
            $f_date_from = db_date($dates[0]);
            $f_date_to = db_date($dates[1]);

            $this->db->where($table . '.created_at BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
        }

        $this->db->where($table . '.deleted_at IS NULL');
        $this->db->group_by($table . '.project_id');
        $query = $this->db->get()->result_array();

        return $query;
    }
}
