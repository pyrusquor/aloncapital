<?php

$years = [
    '2022',
    '2021',
    '2020',
    '2019',
    '2018',
];

?>

<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__toolbar ml-auto">
            <div class="kt-subheader__wrapper">
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#_export_option">
                    <i class="fa fa-download"></i> Export
                </button>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Top Nature of Complaints
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="my-3">
            <form method="post" id="filter_report" class="form-inline">
                <div class="form-group">
                    <label for="year" class="mr-3">Year: </label>
                    <select class="form-control" id="year" name="year" class="mr-3">
                        <?php foreach ($years as $value) : ?>
                            <option value="<?= $value ?>" <?php if ($value == strval($year)) : ?> selected <?php endif ?>>
                                <?= $value ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-sm ml-3">Generate</button>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="container">
                    <table class="table table-striped- table-bordered table-hover table-checkable">
                        <thead>
                            <tr class="text-center">
                                <th style="width: 40%;">Category</th>
                                <th style="width: 40%;">Sub Category</th>
                                <th style="width: 20%;">No. of Complaint</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data as $key => $value) : ?>
                                <tr>
                                    <td><?= $value['category'] ?></td>
                                    <td><?= $value['sub_category'] ?></td>
                                    <td class="text-center"><?= $value['count'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<!--begin: Export Modal-->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="export_form" target="_blank" action="<?php echo site_url('customer_care/export_inquiry_category'); ?>" method="POST">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="year_export" class="mr-3">Select Year: </label>
                                <select class="form-control" id="year_export" name="year_export">
                                    <?php foreach ($years as $value) : ?>
                                        <option value="<?= $value ?>">
                                            <?= $value ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" form="export_form">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end: Export Modal-->