<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Ticket Reports
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>

        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Complaint Reports
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-12">
                <div id="container">
                    <table class="table table-striped- table-bordered table-hover table-checkable">
                        <thead>
                            <tr>
                                <th style="width: 30%;">Report Name</th>
                                <th>Description</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Complaints</td>
                                <td>Compaint table.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/complaint_table">View</a></td>
                            </tr>
                            <tr>
                                <td>Annual Complaint Statistics</td>
                                <td>Count of complaints per category per year.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/complaint_annual_report">View</a></td>
                            </tr>
                            <tr>
                                <td>Complaints per Category</td>
                                <td>Count of complaints per category.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/complaint_category_report">View</a></td>
                            </tr>
                            <tr>
                                <td>Complaints per House Model</td>
                                <td>Count of complaints per house model.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/complaint_house_model_report">View</a></td>
                            </tr>
                            <tr>
                                <td>Complaints per Project</td>
                                <td>Count of complaints per project.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/complaint_project_report">View</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Inquiry Reports
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-12">
                <div id="container">
                    <table class="table table-striped- table-bordered table-hover table-checkable">
                        <thead>
                            <tr>
                                <th style="width: 30%;">Report Name</th>
                                <th>Description</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Inquiries</td>
                                <td>Inquiry table.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/inquiry_table">View</a></td>
                            </tr>
                            <tr>
                                <td>Annual Inquiry Statistics</td>
                                <td>Count of complaints per category per year.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/inquiry_annual_report">View</a></td>
                            </tr>
                            <tr>
                                <td>Complaints per Category</td>
                                <td>Count of complaints per category.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/inquiry_category_report">View</a></td>
                            </tr>
                            <tr>
                                <td>Complaints per House Model</td>
                                <td>Count of complaints per house model.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/inquiry_house_model_report">View</a></td>
                            </tr>
                            <tr>
                                <td>Complaints per Project</td>
                                <td>Count of complaints per project.</td>
                                <td><a href="<?php echo base_url(); ?>customer_care/inquiry_project_report">View</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>