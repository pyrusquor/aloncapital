<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Inquiries</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="total"></span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search Ticket..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm"
                        data-toggle="collapse" data-target="#_advance_search" aria-expanded="true"
                        aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal"
                        data-target="#_export_option">
                    <i class="fa fa-download"></i> Export
                </button>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!--begin: Advance Search -->
        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                            <!-- ==================== begin: Add filter fields ==================== -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="form-control-label">Buyer</label>
                                    <select class="form-control suggests _filter" id="_column_1" data-column="1"  data-module='buyers' data-type='person' name="buyer_id">
                                        <option value="">Select Buyer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="form-control-label">Project</label>
                                    <select class="form-control suggests _filter" id="_column_2" data-column="2"  data-module='projects' name="project_id">
                                        <option value="">Select Project</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 d-none">
                                <div class="form-group">
                                    <label class="form-control-label">Project ID</label>
                                    <input type="text" class="form-control _filter" name="project_id" id="project_id"
                                           placeholder="Project ID"
                                           autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="form-control-label">House Model</label>
                                    <select class="form-control suggests _filter" id="_column_3" data-column="3" data-module="house_models" name="house_model_id" data-param="project_id">
                                        <option value="">Select House Model</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="form-control-label">Category</label>
                                    <select class="form-control suggests _filter" id="_column_4" data-column="4" data-module="inquiry_categories" name="inquiry_category_id">
                                        <option value="">Select Category</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 d-none">
                                <div class="form-group">
                                    <label class="form-control-label">Category ID</label>
                                    <input type="text" class="form-control _filter" name="inquiry_category_id" id="inquiry_category_id"
                                           placeholder="Category ID"
                                           autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="form-control-label">Sub Category</label>
                                    <select class="form-control suggests _filter" id="_column_5" data-column="5" data-module="inquiry_sub_categories" name="inquiry_sub_category_id" data-param="inquiry_category_id">
                                        <option value="">Select Sub Category</option>
                                    </select>
                                </div>
                            </div>
                            <!-- ==================== end: Add filter fields ==================== -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end: Advance Search -->

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="inquiry_table">
            <thead>
            <tr>
                <th>ID</th>
                <!-- ==================== begin: Add header fields ==================== -->
                <th>Buyer ID</th>
                <th>Project ID</th>
                <th>House Model ID</th>
                <th>Inquiry ID</th>
                <th>Sub Inquiry ID</th>
                <th>Year</th>
                <th>Month</th>
                <th>CF ID No.</th>
                <th>Client</th>
                <th>Date of Inquiry</th>
                <th>Project</th>
                <th>BLK/LOT</th>
                <th>Date of Turn Over</th>
                <th>Warranty</th>
                <th>House Model</th>
                <th>Inquiry</th>
                <th>Date Forwarded</th>
                <th>Date Completed</th>
                <!-- ==================== end: Add header fields ==================== -->
            </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>

<!--begin::Modal-->
<!--begin: Export Modal-->
<!-- <div class="modal fade show" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true" style="padding-right: 15px; display: block;"> -->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form" target="_blank"
                      action="<?php echo site_url('customer_care/export_inquiry'); ?>" method="POST">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group form-group-last kt-hide">
                                <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                    <div class="alert-text">
                                        Oh snap! You need select at least one.
                                    </div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($_columns) && $_columns): ?>

                            <div class="col-lg-11 offset-lg-1">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" id="_export_select_all"> Field
                                        <span></span>
                                    </label>
                                    <label class="kt-checkbox kt-checkbox--bold"></label>
                                </div>
                            </div>

                            <?php foreach ($_columns as $key => $_column): ?>

                                <?php if ($_column): ?>

                                    <?php
                                    $_offset = '';
                                    if ($_column === reset($_columns)) {

                                        $_offset = 'offset-lg-1';
                                    }
                                    ?>

                                    <div class="col-lg-5 <?php echo isset($_offset) && $_offset ? $_offset : ''; ?>">
                                        <div class="kt-checkbox-list">
                                            <?php foreach ($_column as $_ckey => $_clm): ?>

                                                <?php
                                                $_label = isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
                                                $_value = isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
                                                ?>

                                                <label class="kt-checkbox kt-checkbox--bold">
                                                    <input type="checkbox" name="_export_column[]"
                                                           class="_export_column"
                                                           value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
                                                    <span></span>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>

                            <div class="col-lg-10 offset-lg-1">
                                <div class="form-group form-group-last">
                                    <div class="alert alert-solid-danger alert-bold fade show" role="alert"
                                         id="form_msg">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            Something went wrong. Please contact your system administrator.
                                        </div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left"
                            data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit"
                            class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right"
                            form="_export_form">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end: Export Modal-->
