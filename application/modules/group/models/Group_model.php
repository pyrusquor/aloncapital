<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Group_model extends MY_Model {
	public $table = 'groups'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = []; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];
}