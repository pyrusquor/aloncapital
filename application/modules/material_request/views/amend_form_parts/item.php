<div class="row">
    <div class="container">
        <h4>Filter Items</h4>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Group <span class="kt-font-danger">*</span></label>
                    <select class="form-control" v-model="request_items.cart.item_group_id">
                        <option></option>
                        <option v-for="group in request_items.data.item_groups" :value="group.id">
                            {{ group.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control" v-model="request_items.cart.item_type_id" autocomplete="off" id="type_selection">
                        <option></option>
                        <option v-for="type in request_items.data.item_types" :value="type.id">
                            {{ type.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Class Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="class_selection" v-model="request_items.cart.item_class_id" placeholder="Select Class" autocomplete="off" id="class_selection">
                        <option></option>
                        <option v-for="item_class in request_items.data.item_classes" :value="item_class.id">
                            {{ item_class.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Brand Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="brand_selection" v-model="request_items.cart.item_brand_id" placeholder="Select brand" autocomplete="off" id="brand_selection">
                        <option></option>
                        <option v-for="brand in request_items.data.item_brands" :value="brand.id">
                            {{ brand.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Item Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="item_selection" v-model="request_items.cart.item_id" placeholder="Select Item" autocomplete="off" id="item_selection">
                        <option></option>
                        <option v-for="item in request_items.data.items" :value="item.id">
                            {{ item.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="">Unit of Measurement <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="unit_selection" v-model="request_items.cart.unit_of_measurement_id" autocomplete="off" id="unit_selection">
                        <option></option>
                        <option v-for="unit in request_items.data.units_of_measurement" :value="unit.id">
                            {{ unit.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="form-group">
                    <label class="">Quantity</label>
                    <input type="number" step="1" min="0" class="form-control" v-model="request_items.cart.quantity">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Unit Cost</label>
                    <span class="form-control">
                        {{ request_items.cart.unit_cost }}
                    </span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Sub Total Cost</label>
                    <span class="form-control">
                        {{ request_items.cart.total_cost }}
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <a href="#!" class="btn btn-sm btn-primary" @click.prevent="addItemToRequest('cart')" v-if="request_items.cart_is_valid">
                Add to Material Request
            </a>
        </div>
    </div>
    <hr>
    <div class="container">
        <h4>Items Requested</h4>
        <h5>Total: {{ info.form.request_amount }}</h5>

        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Group</th>
                        <th>Type</th>
                        <th>Brand</th>
                        <th>Class</th>
                        <th>Unit</th>
                        <th>Qty</th>
                        <th>SubTotal</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody v-if="request_items.form">
                    <tr v-for="(ri, key) in request_items.form">
                        <td>{{ ri.item.name }}</td>
                        <td>{{ ri.item_group.name }}</td>
                        <td>{{ ri.item_type.name }}</td>
                        <td>{{ ri.item_brand.name }}</td>
                        <td>{{ ri.item_class.name }}</td>
                        <td>{{ ri.unit_of_measurement.name }}</td>
                        <td>{{ ri.quantity }}</td>
                        <td>{{ ri.total_cost }}</td>
                        <td>
                            <a href="#!" class="text-warning" @click.prevent="loadItem(key)">Edit</a>
                            <a href="#!" class="text-danger" @click.prevent="removeItem(key)">Delete</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>