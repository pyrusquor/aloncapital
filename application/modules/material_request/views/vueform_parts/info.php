<?php
if ($id) {
    $approving_staff_info = get_person($form_data['approving_staff_id'], 'staff');
    $approving_staff_name = get_fname($approving_staff_info);

    $requesting_staff_id_info = get_person($form_data['requesting_staff_id'], 'staff');
    $requesting_staff_name = get_fname($requesting_staff_id_info);

    $company_data = get_object_from_table($form_data['company_id'], 'companies');
    $company_name = $company_data->name;

    $ledger_data = get_object_from_table($form_data['accounting_ledger_id'], 'accounting_ledgers', 'name ASC', null, null, 'id, name');
    $ledger_name = $ledger_data->name;
} else {
    $approving_staff_info = null;
    $approving_staff_name = '';
    $requesting_staff_id_info = null;
    $requesting_staff_name = '';
    $company_data = null;
    $company_name = '';
    $ledger_data = null;
    $ledger_name = '';
    if(isset($staff)){
        $form_data['requesting_staff_id'] = $staff['id'];
        $requesting_staff_name = $staff['last_name'] . ' ' . $staff['first_name'];
    }
    if(isset($approver)){
        $form_data['approving_staff_id'] = $approver['staff']['id'];
        $approving_staff_name = $approver['staff']['last_name'] . ' ' . $approver['staff']['first_name'];
    }
}
?>
<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label class="">Type of Request<span class="kt-font-danger"></span></label>
                    <?php echo form_dropdown('request_type', Dropdown::get_static("mr_type"), null, 'class="form-control form-control-sm _filter" v-model="info.form.request_type"') ?>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label class="">Customer Type<span class="kt-font-danger"></span></label>
                    <?php echo form_dropdown('customer_type', Dropdown::get_static("mr_customer_type"), null, 'class="form-control form-control-sm _filter" v-model="info.form.customer_type"') ?>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-6" v-if="info.ui.show_project">
                <div class="form-group">
                    <label class="">Project Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" v-model="info.form.project_id" @change="projectChanged()">
                        <option></option>
                        <option v-for="(obj, key) in info.data.projects" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6" v-if="info.ui.show_sub_project">
                <div class="form-group">
                    <label class="">Sub_project Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" v-model="info.form.sub_project_id">
                        <option></option>
                        <option v-for="(obj, key) in info.data.sub_projects" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6" v-if="info.ui.show_amenity">
                <div class="form-group">
                    <label class="">Amenity Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" v-model="info.form.amenity_id" @change="amenityChanged()">
                        <option></option>
                        <option v-for="(obj, key) in info.data.amenities" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6" v-if="info.ui.show_sub_amenity">
                <div class="form-group">
                    <label class="">Sub_amenity Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" v-model="info.form.sub_amenity_id">
                        <option></option>
                        <option v-for="(obj, key) in info.data.sub_amenities" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6" v-if="info.ui.show_block_and_lot">
                <div class="form-group">
                    <label class="">Property Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" v-model="info.form.property_id">
                        <option></option>
                        <option v-for="(obj, key) in info.data.properties" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6" v-if="info.ui.show_department">
                <div class="form-group">
                    <label class="">Department Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" v-model="info.form.department_id">
                        <option></option>
                        <option v-for="(obj, key) in info.data.departments" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6" v-if="info.ui.show_vehicle_equipment">
                <div class="form-group">
                    <label class="">Vehicle or Equipment <span class="kt-font-danger"></span></label>
                    <select class="form-control" v-model="info.form.vehicle_equipment_id">
                        <option></option>
                        <option v-for="(obj, key) in info.data.vehicle_equipments" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date of Request <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control kt_datepicker" placeholder="Date of Request" name="request[request_date]" id="request_date" v-model="info.form.request_date" readonly>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Expense Account</label>
                    <select class="form-control" id="expense_account" name="request[accounting_ledger_id" v-model="info.form.accounting_ledger_id">
                        <option>Select Accounting Ledger</option>
                        <?php foreach ($accounting_ledgers as $ledger) : ?>
                            <option value="<?= $ledger->id ?>" <?php if ($ledger_data) : ?> <?php if ($ledger_data['id'] == $ledger->id) : ?> selected="selected" <?php endif; ?> <?php endif; ?>>
                                <?php echo $ledger->name; ?>
                            </option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <!--
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Days Lapsed <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="number" min="0" class="form-control" placeholder="Days Lapsed"
                               v-model="info.form.days_lapsed"
                               name="request[days_lapsed]">
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Company <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="companies" id="company_id" name="request[company_id]">
                        <?php if ($company_name) : ?>
                            <option value="<?php echo $form_data['company_id']; ?>" selected><?php echo $company_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Particulars</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" placeholder="Particulars" name="request[particulars]" v-model="info.form.particulars">
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id" name="request[approving_staff_id]">
                        <?php if ($approving_staff_name) : ?>
                            <option value="<?php echo $form_data['approving_staff_id']; ?>" selected>
                                <?php echo $approving_staff_name; ?></option>
                        <?php endif ?>
                    </select>

                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person" id="requesting_staff_id" name="request[requesting_staff_id]">
                        <?php if ($requesting_staff_name) : ?>
                            <option value="<?php echo $form_data['requesting_staff_id']; ?>" selected><?php echo $requesting_staff_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>
</div>