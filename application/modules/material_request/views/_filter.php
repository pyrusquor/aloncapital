<?php if (!empty($records)) :
    $this->load->model('project/project_model');
    $this->load->model('company/company_model');

    $this->load->view('material_request/modals/password_confirmation');
    ?>
    <div class="row" id="kt_content">
        <?php foreach ($records as $r) : ?>
            <?php
            $id = ($r['id'] ? $r['id'] : "");
            $reference = ($r['reference'] ? $r['reference'] : "N/A");

            $particulars = ($r['particulars'] ? $r['particulars'] : "");
            $project_id = ($r['project_id'] ? $r['project_id'] : "");
            $company_id = ($r['company_id'] ? $r['company_id'] : "");
            $approving_staff_id = ($r['approving_staff_id'] ? $r['approving_staff_id'] : "");
            $requesting_staff_id = ($r['requesting_staff_id'] ? $r['requesting_staff_id'] : "");

            $days_lapsed = ($r['days_lapsed'] ? $r['days_lapsed'] : "");
            $particulars = ($r['particulars'] ? $r['particulars'] : "");
            $request_date = ($r['request_date'] ? $r['request_date'] : "");
            $request_amount = ($r['request_amount'] ? $r['request_amount'] : "");
            $request_reason = ($r['request_reason'] ? $r['request_reason'] : "");

            $project = $this->project_model->get($project_id);
            $company = $this->company_model->get($company_id);


            ?>
            <!--begin:: Portlet-->
            <div class="kt-portlet ">
                <div class="kt-portlet__body custom-transaction_materials">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden">
                                <img src="./assets/media/project-logos/3.png" alt="image">
                            </div>
                            <div
                                    class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                                <?php // echo get_initials($payable_type_id); ?>
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="<?php echo base_url(); ?>material_request/view/<?php echo $id ?>"
                                       class="kt-widget__username">
                                        Reference : <?php echo $reference; ?>
                                        <i class="flaticon2-correct"></i>
                                        <br>
                                        Customer Type: <?= mr_customer_type_lookup($r['customer_type']); ?>
                                    </a>

                                    <div class="kt-widget__action custom_portlet_header">

                                        <div class="kt-portlet__head kt-portlet__head--noborder"
                                             style="min-height: 0px;">
                                            <div class="kt-portlet__head-label">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>"
                                                           class="m-checkable delete_check"
                                                           data-id="<?php echo $r['id']; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                    <i class="flaticon-more-1 kt-font-brand"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('material_request/view/' . $id); ?>"
                                                               class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon-eye"></i>
                                                                <span class="kt-nav__link-text">View</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('material_request/form/' . $id); ?>"
                                                               class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-pen"></i>
                                                                <span class="kt-nav__link-text">Update</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="#!"
                                                               class="kt-nav__link remove_material_request"
                                                               data-id="<?= $id; ?>"
                                                            >
                                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                                <span class="kt-nav__link-text">Delete</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <hr>
                                                            <span class="kt-nav__link">
                                                                <span class="kt-nav__link-text">SET STATUS TO:</span>
                                                             </span>
                                                        </li>
                                                        <?php foreach (mr_status_array() as $key => $mr_status): ?>
                                                            <?php if (current_user_id() === $r['approving_staff_id'] && $r['request_status'] != $key): ?>
                                                                <li class="kt-nav__item">
                                                                    <a href="#!"
                                                                       class="kt-nav__link process-action"
                                                                       data-id="<?= $r['id'] ?>"
                                                                       data-action="<?= $key; ?>"
                                                                       data-table="material_requests"
                                                                       data-field="request_status"
                                                                       data-value="<?= $key; ?>"
                                                                       data-actor="approving_staff_id"
                                                                    >
                                                                        <i class="kt-nav__link-icon flaticon2-cancel"></i>
                                                                        <span class="kt-nav__link-text"><?php echo $mr_status; ?></span>
                                                                    </a>
                                                                </li>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>

                                                        <li class="kt-nav-item">
                                                            <hr>
                                                        </li>
                                                        <?php if (current_user_id() == $r['requesting_staff_id'] && $r['request_status'] != 4): ?>
                                                            <li class="kt-nav__item">
                                                                <a href="#!"
                                                                   class="kt-nav__link process-action-with-supervision"
                                                                   data-id="<?= $r['id'] ?>"
                                                                   data-action="4"
                                                                   data-table="material_requests"
                                                                   data-field="request_status"
                                                                   data-value="4"
                                                                   data-actor="requesting_staff_id"
                                                                   data-validator="<?= $r['approving_staff_id'] ?>"
                                                                >
                                                                    <i class="kt-nav__link-icon flaticon2-cancel"></i>
                                                                    <span class="kt-nav__link-text">Cancel Own Request</span>
                                                                </a>
                                                            </li>
                                                        <?php endif; ?>
                                                        <!--                                                        <li class="kt-nav__item">-->
                                                        <!--                                                            <a href="#!"-->
                                                        <!--                                                               class="kt-nav__link cancel_request" data-id="-->
                                                        <? //=$id?><!--">-->
                                                        <!--                                                                <i class="kt-nav__link-icon flaticon2-cancel"></i>-->
                                                        <!--                                                                <span class="kt-nav__link-text">Cancel</span>-->
                                                        <!--                                                            </a>-->
                                                        <!--                                                        </li>-->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget__info">
                                    <?php
                                        $customer_type_data = mr_customer_type_data_display($r['id']);
                                        if ($customer_type_data):
                                            foreach ($customer_type_data as $ctd):
                                                ?>
                                                <div class="kt-widget__desc">
                                                    <a href="<?php echo base_url(); ?><?php echo $ctd['url']; ?>/view/<?php echo $ctd['data']->id; ?>"
                                                       target="_BLANK" class="kt-widget__username">
                                                        <?= $ctd['name'] ?>: <?= $ctd['data']->name; ?>
                                                    </a>
                                                </div>
                                            <?php endforeach; endif; ?>
                                    <div class="kt-widget__desc">
                                        <a href="<?php echo base_url(); ?>company/view/<?php echo $company_id; ?>"
                                           target="_BLANK" class="kt-widget__username">
                                            Company : <?= $company['name'] ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__bottom">


                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Request Date</span>
                                    <span class="kt-widget__value"><?php echo view_date($request_date); ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Request Amount</span>
                                    <span class="kt-widget__value"><?php echo money_php($request_amount); ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Days Lapsed</span>
                                    <span class="kt-widget__value"><?php echo $days_lapsed; ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Request Status</span>
                                    <span class="kt-widget__value"><?php echo mr_status_lookup($r['request_status']); ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Request Reason</span>
                                    <span class="kt-widget__value"><?php echo $request_reason; ?></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>

    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="<?php echo base_url('material_request/form'); ?>"
                               class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>