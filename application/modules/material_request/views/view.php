<?php
    // Material Request Information
    $id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
    /* ==================== begin: Add model fields ==================== */
    $approving_staff_info = get_person($info['approving_staff_id'], 'staff');
    $approving_staff_name = get_fname($approving_staff_info);

    $requesting_staff_id_info = get_person($info['requesting_staff_id'], 'staff');
    $requesting_staff_name = get_fname($requesting_staff_id_info);

    /* ==================== end: Add model fields ==================== */
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Material Request Details</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <?php if ($this->ion_auth->is_admin()): ?>
                    <a href="<?php echo site_url('material_request/form/' . $id); ?>"
                       class="btn btn-label-success btn-elevate btn-sm">
                        <i class="fa fa-edit"></i> Edit
                    </a>
                <?php endif; ?>
                <a href="<?php echo site_url('material_request'); ?>"
                   class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!--begin:: Portlet-->
    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <?php if (get_image('material_request', 'images', $image)) : ?>
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<?php echo base_url(get_image('material_request', 'images', $image)); ?>"
                                 width="110px" height="110px"/>
                        </div>
                    <?php endif; ?>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <span class="kt-widget__username"><?php echo ucwords($info['reference']); ?></span>
                        </div>

                        <div class="kt-widget__desc">
                            <?php echo view_date($info['request_date']); ?>
                        </div>
                        <div class="kt-widget__desc">
                            <?php echo mr_status_lookup($info['request_status']); ?>
                        </div>
                        <div class="kt-widget__desc">
                            <?php $this->load->view('modals/document_attachment')?>
                        </div>
                    </div>
                    <div class="kt-widget__stats kt-margin-t-20">
                        <div class="kt-widget__icon">
                            <i class="flaticon-piggy-bank"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title kt-font-bold">Requested Amount</span><br>
                            <span class="kt-widget__value"><?= money_php($info['request_amount']) ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab"
                           aria-selected="true">
                            <i class="flaticon2-user-outline-symbol"></i> Basic Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#material_request_items" role="tab"
                           aria-selected="false">
                            <i class="flaticon-support"></i> Items
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content kt-margin-t-20">
                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="basic_information" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <p>Customer Type: <?= mr_customer_type_lookup($info['customer_type']); ?></p>
                                <div class="row ">
                                    <div class="col">
                                        <?php
                                            $customer_type_data = mr_customer_type_data_display($id);
                                            foreach ($customer_type_data as $ctd):
                                                ?>
                                                <div class="form-group form-group-xs row">
                                                    <label class="col col-form-label text-right">
                                                        <?= $ctd['name'] ?> :
                                                    </label>
                                                    <div class="col">
                                                        <span class="form-control-plaintext kt-font-bolder">
                                                            <a href="<?php echo base_url(); ?><?php echo $ctd['url']; ?>/view/<?php echo $ctd['data']->id; ?>">
                                                                <?= $ctd['data']->name; ?>
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>

                                            <?php endforeach; ?>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Company:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder">
                                                    <a href="<?php echo base_url(); ?>company/view/<?php echo $company['id']; ?>">
                                                        <?= $company['name'] ?>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Particulars:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder">
                                                    <?= $info['particulars']; ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Reason:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder">
                                                    <?= $info['request_reason']; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Requesting Staff:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder">
                                                    <?= $requesting_staff_name; ?>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Approving Staff:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder">
                                                    <?= $approving_staff_name; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="material_request_items" role="tabpanel-2">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col">
                                        <table class="table table-borderless">
                                            <thead>
                                            <tr>
                                                <th>Group</th>
                                                <th>Type</th>
                                                <th>Brand</th>
                                                <th>Class</th>
                                                <th>Item</th>
                                                <th>Unit</th>
                                                <th>Quantity</th>
                                                <th>Cost</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                $this->load->model('item/item_model');
                                                $this->load->model('item_brand/item_brand_model');
                                                $this->load->model('item_class/item_class_model');
                                                $this->load->model('item_type/item_type_model');
                                                $this->load->model('item_group/item_group_model');
                                                $this->load->model('inventory_settings_unit_of_measurement/inventory_settings_unit_of_measurement_model', 'unit_of_measurement_model');
                                                foreach ($items as $item):
                                                    $item_group = $this->item_group_model->get($item['item_group_id']);
                                                    $item_type = $this->item_type_model->get($item['item_type_id']);
                                                    $item_brand = $this->item_brand_model->get($item['item_brand_id']);
                                                    $item_class = $this->item_class_model->get($item['item_class_id']);
                                                    $__item = $this->item_model->get($item['item_id']);
                                                    $unit_of_measurement = $this->unit_of_measurement_model->get($item['unit_of_measurement_id']);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?= $item_group['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $item_type['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $item_brand['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $item_class['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $__item['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $unit_of_measurement['name'] ?>
                                                        </td>
                                                        <td>
                                                            <?= $item['quantity']; ?>
                                                        </td>
                                                        <td>
                                                            <?= money_php($item['unit_cost']) ?> /
                                                            <?= money_php($item['total_cost']) ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->
            </div>
        </div>
    </div>

</div>