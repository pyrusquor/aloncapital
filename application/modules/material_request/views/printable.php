<!DOCTYPE html>
<html>

<head>
    <title><?php echo "PV-" . $info['reference']; ?></title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 15px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th {
            padding: 5px 8px;
        }

        .table>thead>tr>th {
            border-bottom: 2px solid #00529c;
        }

        .table>tfoot>tr>td {
            border-top: 2px solid #00529c;
        }

        .table>tfoot {
            border-top: 2px solid #ddd;
        }

        .table>tbody {
            border-bottom: 1px solid #fff;
        }

        .table.table-striped>thead>tr>th {
            background: #00529c;
            color: #fff;
            border: 0;
            padding: 12px 8px;
            text-transform: uppercase;
        }

        .table.table-striped>thead>tr>th a {
            color: #fff;
            font-weight: 400;
        }

        .table.table-striped>thead>tr:nth-child(2)>th {
            background: #0075de;
        }

        .table.table-striped td {
            border: 0;
            vertical-align: middle;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background: #fff;
        }

        .table-striped>tbody>tr:nth-of-type(even) {
            background: #f1f1f1;
        }

        .color-bluegreen {
            color: #169f98;
        }

        .color-white {
            color: #fff;
        }

        .peso_currency {
            font-family: DejaVu Sans;
        }

        .bg-bluegreen {
            background-color: #169f98;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .margin0 {
            margin: 0;
        }

        .padding10 {
            padding: 10px;
        }

        p {
            margin: 0 0 15px;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-warning {
            background-color: #fcf8e3;
            border-color: #faebcc;
            color: #8a6d3b;
        }

        .d-grid {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            gap: 1rem 3rem;
        }

        input[type='text'],
        select.form-control {
            background: transparent;
            border: none;
            border-bottom: 1px solid #000000;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        input[type='text']:focus,
        select.form-control:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>

<body>
    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body m-5 p-5">
                <?php
                // vdebug($info);
                $id = isset($info['id']) && $info['id'] ? $info['id'] : "";

                $request_type = isset($info['request_type']) && $info['request_type'] ? $info['request_type'] : "";

                $customer_type = isset($info['customer_type']) && $info['customer_type'] ? mr_customer_type_lookup($info['customer_type']) : "";

                $company = isset($info['company']['name']) && $info['company']['name'] ? $info['company']['name'] : "";

                $company_address = isset($info['company']['location']) && $info['company']['location'] ? $info['company']['location'] : "";

                $particulars = isset($info['particulars']) && $info['particulars'] ? $info['particulars'] : "";

                $requst_reason = isset($info['requst_reason']) && $info['requst_reason'] ? $info['requst_reason'] : "";

                $requesting_staff = isset($info['requesting_staff']) && $info['requesting_staff'] ? get_fname($info['requesting_staff']) : "";

                $approving_staff = isset($info['approving_staff']) && $info['approving_staff'] ? get_fname($info['approving_staff']) : "";

                $request_amount = isset($info['request_amount']) && $info['request_amount'] ? $info['request_amount'] : "";

                $request_items = isset($request_items) && $request_items ? $request_items : [];

                ?>

                <div class="top-header text-center">
                    <h4 class="text-uppercase"><?= $company ?></h4>
                    <p>
                        <?= $company_address ?>
                    </p>

                    <div class="row">
                        <div class="col-lg-4">
                            <p>
                                Print Date:
                                <?= view_date(NOW) ?>
                                &nbsp;
                                <?= date("h:i:sa") ?>
                            </p>
                        </div>
                        <div class="col-lg-4">
                            <h4 class="font-weight-bold text-uppercase">
                                Material Request Slip
                            </h4>
                        </div>
                        <div class="offset-4"></div>
                    </div>
                </div>

                <div class="d-flex justify-content-between">
                    <p class="font-weight-bold">
                        Document#:
                        <?= $info['reference'] ?>
                    </p>
                    <p>
                        <span class="font-weight-bold">Date:</span>
                        <span><?= view_date(NOW) ?></span>
                    </p>
                </div>

                <div class="d-flex">
                    <p class="mr-3 text-uppercase font-weight-bold text-nowrap">
                        Request Type:
                    </p>
                    <p class="border-bottom w-100">
                        <?= Dropdown::get_static('mr_type', $request_type, 'view') ?>
                    </p>
                </div>

                <div class="d-flex">
                    <p class="mr-3 text-uppercase font-weight-bold text-nowrap">
                        Customer Type:
                    </p>
                    <p class="border-bottom w-100">
                        <?= $customer_type; ?>
                    </p>
                </div>

                <?php
                $customer_type_data = mr_customer_type_data_display($id);
                foreach ($customer_type_data as $ctd) :
                ?>

                    <div class="d-flex">
                        <p class="mr-3 text-uppercase font-weight-bold">
                            <?= $ctd['name'] ?>:
                        </p>
                        <p class="border-bottom w-100">
                            <?= $ctd['data']->name ?>
                        </p>
                    </div>

                <?php endforeach; ?>

                <div class="d-flex">
                    <p class="mr-3 text-uppercase font-weight-bold">
                        Company:
                    </p>
                    <p class="border-bottom w-100">
                        <?= $company; ?>
                    </p>
                </div>

                <div class="d-flex">
                    <p class="mr-3 text-uppercase font-weight-bold">
                        Particulars:
                    </p>
                    <p class="border-bottom w-100">
                        <?= $particulars; ?>
                    </p>
                </div>

                <div class="d-flex">
                    <p class="mr-3 text-uppercase font-weight-bold">
                        Reason:
                    </p>
                    <p class="border-bottom w-100">
                        <?= $requst_reason; ?>
                    </p>
                </div>

                <div class="d-flex">
                    <p class="mr-3 text-uppercase font-weight-bold">
                        Amount:
                    </p>
                    <p class="border-bottom w-100 font-weight-bold">
                        ***
                        <?= number_to_words($info['request_amount']); ?>
                        PESOS ONLY***
                    </p>
                </div>

                <div class="d-flex justify-content-around">
                    <p>
                        (
                        <?= money_php($info['request_amount']) ?>
                        )
                    </p>
                    <p class="font-weight-bold">Charging:</p>
                </div>

                <div class="border-bottom py-5">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th class="font-weight-bold" scope="col">
                                    Group
                                </th>
                                <th class="font-weight-bold" scope="col">
                                    Type
                                </th>
                                <th class="font-weight-bold" scope="col">
                                    Brand
                                </th>
                                <th class="font-weight-bold" scope="col">
                                    Class
                                </th>
                                <th class="font-weight-bold" scope="col">
                                    Item
                                </th>
                                <th class="font-weight-bold" scope="col">
                                    Item Code
                                </th>
                                <th class="font-weight-bold" scope="col">
                                    Quantity
                                </th>
                                <th class="font-weight-bold" scope="col">
                                    Unit of Measurement
                                </th>
                                <th class="font-weight-bold" scope="col">
                                    Cost
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($request_items) : ?>
                                <?php foreach ($request_items as $key =>
                                    $item) : ?>
                                    <?php
                                    $account_id = get_value_field($item['ledger_id'], 'accounting_ledgers', 'account_id');
                                    $account_title = get_value_field($item['ledger_id'], 'accounting_ledgers', 'name');
                                    ?>
                                    <tr>
                                        <td>
                                            <?= $item['item_group']['name'] ?>
                                        </td>
                                        <td>
                                            <?= $item['item_type']['name'] ?>
                                        </td>
                                        <td>
                                            <?= $item['item_brand']['name'] ?>
                                        </td>
                                        <td>
                                            <?= $item['item_class']['name'] ?>
                                        </td>
                                        <td>
                                            <?= $item['item']['name'] ?>
                                        </td>
                                        <td>
                                            <?= $item['item']['code'] ?>
                                        </td>
                                        <td>
                                            <?= $item['quantity'] ?>
                                        </td>
                                        <td>
                                            <?= $item['unit_of_measurement']['name'] ?>
                                        </td>
                                        <td>
                                            <?= money_php($item['total_cost']) ?>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                            <?php else : ?>
                                <p class="text-center font-italic">
                                    ------ Nothing Follows ------
                                </p>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <div class="d-grid my-5">
                    <p>
                        <span class="font-weight-bold">Requested by:</span>
                    </p>
                    <p>
                        <span class="font-weight-bold">Approved by:</span>
                    </p>
                </div>

                <div class="d-grid">
                    <p class="border-bottom w-100 text-center">
                        <span class="font-weight-bold">
                            <?= $requesting_staff ?>
                        </span>
                    </p>
                    <p class="border-bottom w-100 text-center">
                        <span class="font-weight-bold">
                            <?= $approving_staff ?>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>