<div class="row">
    <div class="container">
        <div class="row">

            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Customer Type<span class="kt-font-danger"></span></label>
                    <?php echo form_dropdown('customer_type', Dropdown::get_static("mr_customer_type"), $customer_type, 'class="form-control form-control-sm _filter" v-model="customer_type"')?>
                    <input type="hidden" name="request[customer_type]" v-model="customer_type">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6" v-if="show_project">
                <div class="form-group">
                    <label class="">Project Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" data-module="projects" id="project_id"
                            name="request[project_id]"
                            v-model="info_form.project"
                            @change="projectChanged(); checkInfoForm()"
                    >
                        <option value="">Select Project</option>
                        <?php foreach($project_list as $p):?>
                            <option value="<?=$p->id;?>"
                                    <?php if($p->id == $form_data['project_id']) echo 'selected';?>>
                                <?=$p->name;?>
                            </option>
                        <?php endforeach;?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6" v-if="info_form.project && show_sub_project">
                <div class="form-group">
                    <label class="">Sub Project Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" id="sub_project_id"
                            name="request[sub_project_id]"
                            v-model="info_form.sub_project"
                            @change="checkInfoForm()"
                    >
                        <option v-for="sp in info_data.sub_projects" :value="sp.id">
                            {{ sp.name }}
                        </option>

                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6" v-if="show_amenity">
                <div class="form-group">
                    <label class="">Amenity Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" data-module="amenities" id="amenity_id"
                            name="request[amenity_id]"
                            v-model="info_form.amenity"
                            @change="amenityChanged(); checkInfoForm()"
                    >
                        <option value="">Select amenity</option>
                        <?php foreach($amenity_list as $p):?>
                            <option value="<?=$p->id;?>"
                                <?php if($p->id == $form_data['amenity_id']) echo 'selected';?>>
                                <?=$p->name;?>
                            </option>
                        <?php endforeach;?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6" v-if="show_sub_amenity && info_form.amenity">
                <div class="form-group">
                    <label class="">Sub amenity Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" id="sub_amenity_id"
                            name="request[sub_amenity_id]"
                            v-model="info_form.sub_amenity"
                            @change="checkInfoForm()"
                    >
                        <option v-for="sp in info_data.sub_amenities" :value="sp.id">
                            {{ sp.name }}
                        </option>

                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6" v-if="show_property && info_form.project">
                <div class="form-group">
                    <label class="">Property <span class="kt-font-danger"></span></label>
                    <select class="form-control" id="property_id"
                            name="request[property_id]"
                            v-model="info_form.property"
                            @change="checkInfoForm()"
                    >
                        <option v-for="sp in info_data.properties" :value="sp.id">
                            {{ sp.name }}
                        </option>

                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6" v-if="show_department">
                <div class="form-group">
                    <label class="">Department Name <span class="kt-font-danger"></span></label>
                    <select class="form-control" data-module="departments" id="department_id"
                            name="request[department_id]"
                            v-model="info_form.department"
                            @change="checkInfoForm()"
                    >
                        <option value="">Select department</option>
                        <?php foreach($department_list as $p):?>
                            <option value="<?=$p->id;?>"
                                <?php if($p->id == $form_data['department_id']) echo 'selected';?>>
                                <?=$p->name;?>
                            </option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>

            <div class="col-sm-6" v-if="show_vehicle_equipment">
                <div class="form-group">
                    <label class="">Vehicle or Equipment <span class="kt-font-danger"></span></label>

                    <input type="text" class="form-control" id="equipment_id" name="request[equipment_id]" v-model="info_form.vehicle_equipment" @change="checkInfoForm()">
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date of Request <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control kt_datepicker" placeholder="Date of Request"
                               name="request[request_date]"
                               value="<?php echo set_value('request_date"', @$form_data['request_date']); ?>" readonly>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Days Lapsed <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="number" min="0" class="form-control" placeholder="Days Lapsed"
                               name="request[days_lapsed]" value="<?php echo $form_data['days_lapsed'] ?>">
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Company <span class="kt-font-danger"></span></label>
                    <select class="form-control suggests" data-module="companies" id="company_id"
                            name="request[company_id]">
                        <option value="">Select Company</option>
                        <?php if ($company_name): ?>
                            <option value="<?php echo $form_data['company_id']; ?>"
                                    selected><?php echo $company_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Particulars</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" placeholder="Particulars" name="request[particulars]"
                               value="<?php echo $form_data['particulars'] ?>">
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <!--
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Requested Amount <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="number" min="0" step="0.1" class="form-control" placeholder="Requested Amount"
                               name="request[request_amount]" value="<?php echo (float)$form_data['request_amount'] ?>">
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            -->
        </div>
        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id"
                            name="request[approving_staff_id]">
                        <option value="">Select Property</option>
                        <?php if ($approving_staff_name): ?>
                            <option value="<?php echo $form_data['approving_staff_id']; ?>"
                                    selected><?php echo $approving_staff_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person"
                            id="requesting_staff_id" name="request[requesting_staff_id]">
                        <option value="">Select Requesting Staff</option>
                        <?php if ($requesting_staff_name): ?>
                            <option value="<?php echo $form_data['requesting_staff_id']; ?>"
                                    selected><?php echo $requesting_staff_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>

</div>
