<script type="text/javascript">
    window.object_id = "<?=$id?>";
</script>
<button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#document_attachment_modal">
    <i class="fa fa-upload"></i> Document Attachments
</button>
<div class="modal modal-lg fade" id="document_attachment_modal" tabindex="-1" role="dialog"
     aria-labelledby="document_attachment_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <h4>Documents</h4>
                    <div class="spinner-border text-success" role="status" v-if="loading">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <div class="table-responsive" v-else>
                        <table class="table table-condensed table-striped">
                            <thead>
                            <tr>
                                <th>Created</th>
                                <th>File</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="doc in documents">
                                <td><small>{{ doc.created_at }}</small></td>
                                <td><small><a :href="doc.path" target="_new">{{ doc.filename }}</a></small></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <form method="post" action="<?=site_url('document_attachment/upload')?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="form-group-label">File</div>
                            <input type="file" class="form-control form-control-file" name="document">
                            <input type="hidden" name="object_type" value="material_requests">
                            <input type="hidden" name="object_type_id" value="<?=$id?>">
                            <input type="hidden" name="return_url" value="<?=site_url('material_request/view/' . $id)?>">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Upload</button>
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary">Close</button>
            </div>
        </div>
    </div>
</div>
