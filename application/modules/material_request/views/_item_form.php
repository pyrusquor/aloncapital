<?php
    foreach ($child_fields as $cf) {
        $form_data[$cf] = isset($info[$cf]) && $info[$cf] ? $info[$cf] : '';
    }
?>
<input type="hidden" id="material_request_id" value="<?php echo $id;?>" v-bind="material_request_id">
<div class="row">
    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Group <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="group_selection"
                            v-model="form_data.item_group"
                            @change="getItemTypes()"
                            placeholder="Select group"
                            autocomplete="off" id="group_selection">
                        <option v-for="group in item_groups" :value="group.id">
                            {{ group.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="type_selection"
                            v-model="form_data.item_type"
                            placeholder="Select type"
                            @change="getItems()"
                            autocomplete="off" id="type_selection">
                        <option v-for="type in item_types" :value="type.id">
                            {{ type.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Brand Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="brand_selection"
                            v-model="form_data.item_brand"
                            @change="getItems()"
                            placeholder="Select brand"
                            autocomplete="off" id="brand_selection">
                        <option v-for="brand in item_brands" :value="brand.id">
                            {{ brand.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label class="">Class Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="class_selection"
                            v-model="form_data.item_class"
                            placeholder="Select Class"
                            autocomplete="off" id="class_selection">
                        <option v-for="item_class in item_classes"
                                :value="item_class.id">
                            {{ item_class.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="">Item Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="item_selection"
                            v-if="formItemCheck('item_type')"
                            v-model="form_data.item"
                            placeholder="Select Item"
                            @change="populateItemData()"
                            autocomplete="off" id="item_selection">
                        <option v-for="item in items"
                                :value="item.id">
                            {{ item.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="">Unit of Measurement <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="unit_selection"
                            v-model="form_data.units_of_measurement"
                            placeholder="Select Item"
                            autocomplete="off" id="unit_selection">
                        <option v-for="unit in units_of_measurement" :value="unit.id">
                            {{ unit.name }}
                        </option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="">Quantity</label>
                    <input v-if="formItemCheck('item')" type="number" step="1" min="0" class="form-control" v-model="form_data.quantity" @change="calculateSubTotal()">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="">Unit/Total Cost</label>
                    <span class="form-control">
                        {{ form_data.unit_cost }}/{{ form_data.unit_cost * form_data.quantity }}
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group form-group-last row">
            <div class="offset-10"></div>
            <div class="col-lg-2">
                <a href="#!" @click.prevent="addItem()" class="btn btn-bold btn-sm btn-label-brand">
                    <i class="la la-plus"></i> {{ form_action.label }}
                </a>
            </div>
        </div>

    </div>

    <div class="container my-3">
        <div class="row">
            <div class="col-sm-1">
                Group
            </div>
            <div class="col-sm-1">
                Type
            </div>
            <div class="col-sm-1">
                Brand
            </div>
            <div class="col-sm-1">
                Class
            </div>
            <div class="col-sm-2">
                Item
            </div>
            <div class="col-sm-1">
                Unit
            </div>
            <div class="col-sm-1">
                Qty
            </div>
            <div class="col-sm-2">
                Unit Cost/Total Cost
            </div>
        </div>
        <hr>
        <input type="hidden" name="deleted_items" v-model="deleted_items">
        <div class="row" v-for="(el, key) in item_data">
            <input type="hidden" :name="getItemDataKey('id', key, '')" v-model="el.id">
            <div class="col-sm-1">
                {{ getFormData('item_group', key).name }}
                <input type="hidden" :name="getItemDataKey('item_group', key, '_id')" v-model="el.item_group">
            </div>
            <div class="col-sm-1">
                {{ getFormData('item_type', key).name }}
                <input type="hidden" :name="getItemDataKey('item_type', key, '_id')" v-model="el.item_type">
            </div>
            <div class="col-sm-1">
                {{ getFormData('item_brand', key).name }}
                <input type="hidden" :name="getItemDataKey('item_brand', key, '_id')" v-model="el.item_brand">
            </div>
            <div class="col-sm-1">
                {{ getFormData('item_class', key).name }}
                <input type="hidden" :name="getItemDataKey('item_class', key, '_id')" v-model="el.item_class">
            </div>
            <div class="col-sm-2">
                {{ getFormData('item', key).name }}
                <input type="hidden" :name="getItemDataKey('item', key, '_id')" v-model="el.item">
            </div>
            <div class="col-sm-1">
                {{ getFormData('units_of_measurement', key).name }}
                <input type="hidden" :name="getItemDataKey('unit_of_measurement', key, '_id')" v-model="el.units_of_measurement">
            </div>
            <div class="col-sm-1">
                {{ el.quantity }}
                <input type="hidden" :name="getItemDataKey('quantity', key, '')" v-model="el.quantity">
            </div>
            <div class="col-sm-2">
                {{ el.unit_cost }}/{{ el.total_cost }}
                <input type="hidden" :name="getItemDataKey('unit_cost', key, '')" v-model="el.unit_cost">
                <input type="hidden" :name="getItemDataKey('total_cost', key, '')" v-model="el.total_cost">
            </div>
            <div class="col-sm-2">
                <a href="#!" class="btn-sm btn btn-label-warning btn-bold" @click.prevent="editItem(key)">
                    <i class="la la-pencil"></i>

                </a>
                <a href="#!"
                   class="btn-sm btn btn-label-danger btn-bold" @click.prevent="removeItem(key)">
                    <i class="la la-trash-o"></i>
                </a>
            </div>
        </div>
    </div>

</div>
