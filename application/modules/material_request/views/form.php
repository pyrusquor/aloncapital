<?php

    $form_data = array();
    foreach ($master_fields as $mf) {
        $form_data[$mf] = isset($info[$mf]) && $info[$mf] ? $info[$mf] : '';
    }
    if ($id) {
        $approving_staff_info = get_person($form_data['approving_staff_id'], 'staff');
        $approving_staff_name = get_fname($approving_staff_info);

        $requesting_staff_id_info = get_person($form_data['requesting_staff_id'], 'staff');
        $requesting_staff_name = get_fname($requesting_staff_id_info);

        $company_data = get_object_from_table($form_data['company_id'], 'companies');
        $company_name = $company_data->name;

        $project_data = get_object_from_table($form_data['project_id'], 'projects');
        $project_name = $project_data->name;

        $sub_project_data = get_object_from_table($form_data['sub_project_id'], 'sub_projects');
        $amenity_data = get_object_from_table($form_data['amenity_id'], 'amenities');
        $sub_amenity_data = get_object_from_table($form_data['sub_amenity_id'], 'sub_amenities');
        $property_data = get_object_from_table($form_data['property_id'], 'properties');
        $department_data = get_object_from_table($form_data['department_id'], 'departments');

        $sub_project_name = $sub_project_data->name;
        $amenity_name = $amenity_data->name;
        $sub_amenity_name = $sub_amenity_data->name;
        $property_name = $property_data->name;
        $department_name = $department_data->name;

        $customer_type = $form_data['customer_type'];

        $equipment_data = null; // get_object_from_table($form_data['equipment_id'], 'equipment');
        $equipment_name = ''; // $equipment_data->name;
    } else {
        $approving_staff_info = null;
        $approving_staff_name = '';
        $requesting_staff_id_info = null;
        $requesting_staff_name = '';
        $company_data = null;
        $company_name = '';
        $project_data = null;
        $project_name = '';

        $sub_project_data = null;
        $sub_project_name = '';
        $amenity_data = null;
        $amenity_name = '';
        $sub_amenity_data = null;
        $sub_amenity_name = '';
        $property_data = null;
        $property_name = '';
        $department_data = null;
        $department_name = '';
        $equipment_data = null;
        $equipment_name = '';

        $customer_type = '';
    }

?>
<script type="text/javascript">
    window.customer_type = "<?php echo $customer_type;?>";
    window.info_form_data = {
        project: "<?php echo $form_data['project_id'];?>",
        sub_project: "<?php echo $form_data['sub_project_id'];?>",
        amenity: "<?php echo $form_data['amenity_id'];?>",
        sub_amenity: "<?php echo $form_data['sub_amenity_id'];?>",
        property: "<?php echo $form_data['property_id'];?>",
        department: "<?php echo $form_data['department_id'];?>",
        vehicle_equipment: "<?php echo $form_data['vehicle_equipment_id'];?>",
    }
</script>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Create Material Request</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo base_url('material_request'); ?>" class="btn btn-label-instagram"><i class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>
<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                <div class="kt-grid__item">

                    <!--begin: Form Wizard Nav -->
                    <div class="kt-wizard-v3__nav">
                        <div class="kt-wizard-v3__nav-items">
                            <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label">
                                        <span>1</span> General Information
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>
                            <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label">
                                        <span>2</span> List of Items
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!--end: Form Wizard Nav -->
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper" id="form_app">
                    <!--begin: Form Wizard Form-->
                    <form class="kt-form" method="POST" action="<?php form_open('material_request/form/'.@$info['id']); ?>" id="form_material_request"  enctype="multipart/form-data">
                        <?php $this->load->view('_form'); ?>
                    </form>

                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
</div>