<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Material_request extends MY_Controller
{
    private $fields = [
        array(
            'field' => 'request[reference]',
            'label' => 'Reference',
            'rules' => 'trim',
        ),
        /* ==================== begin: Add model fields ==================== */
        // relation fields
        array(
            'field' => 'request[project_id]',
            'label' => 'Customer Type',
            'rules' => 'trim|required',
        ), array(
            'field' => 'request[company_id]',
            'label' => 'Company',
            'rules' => 'trim|required',
        ), array(
            'field' => 'request[approving_staff_id]',
            'label' => 'Approved By',
            'rules' => 'trim|required',
        ), array(
            'field' => 'request[requesting_staff_id]',
            'label' => 'Requested By',
            'rules' => 'trim|required',
        ),
        // other fields
        //            array(
        //                'field' => 'request[days_lapsed]',
        //                'label' => 'Days Lapsed',
        //                'rules' => 'trim|required',
        //            ),
        array(
            'field' => 'request[request_type]',
            'label' => 'Type of Request',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'request[particulars]',
            'label' => 'Particulars',
            'rules' => 'trim',
        ), array(
            'field' => 'request[request_date]',
            'label' => 'Request Date',
            'rules' => 'trim|required',
        ), array(
            'field' => 'request[request_amount]',
            'label' => 'Request Amount',
            'rules' => 'trim|required',
        ), array(
            'field' => 'request[request_reason]',
            'label' => 'Reason for Request',
            'rules' => 'trim',
        ),
        /* ==================== end: Add model fields ==================== */

    ];

    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Material_request_model', 'M_Material_request');
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('staff/Staff_model', 'M_staff');
        $this->load->model('budgeting/Budgeting_setup_model','M_budget_setup');
        $this->load->model('budgeting/Budgeting_logs_model','M_budget_logs');

        // specific models
        $this->load->model('material_request_item/Material_request_item_model', 'M_Material_request_item');

        // accounting entries?

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images', 'material_request_helper']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Material_request->fillable;
        $this->_table_columns = $this->M_Material_request->__get_columns();
        $this->_table = 'material_requests';

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'is_active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $this->module_name = $this->uri->segment(1, '');
    }

    public function get_all()
    {
        $data['row'] = $this->M_Material_request->get_all();

        echo json_encode($data);
    }

    private function __search($params)
    {

        $id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';
        if (!$id) {
            $customer_type = isset($params['customer_type']) && !empty($params['customer_type']) ? $params['customer_type'] : null;
            $request_type = isset($params['request_type']) && !empty($params['request_type']) ? $params['request_type'] : null;
            $request_status = isset($params['request_status']) && !empty($params['request_status']) ? $params['request_status'] : null;

            $company_id = isset($params['company_id']) && !empty($params['company_id']) ? $params['company_id'] : null;
            $project_id = isset($params['project_id']) && !empty($params['project_id']) ? $params['project_id'] : null;
            $sub_project_id = isset($params['sub_project_id']) && !empty($params['sub_project_id']) ? $params['sub_project_id'] : null;
            $property_id = isset($params['property_id']) && !empty($params['property_id']) ? $params['property_id'] : null;
            $amenity_id = isset($params['amenity_id']) && !empty($params['amenity_id']) ? $params['amenity_id'] : null;
            $sub_amenity_id = isset($params['sub_amenity_id']) && !empty($params['sub_amenity_id']) ? $params['sub_amenity_id'] : null;
            $department_id = isset($params['department_id']) && !empty($params['department_id']) ? $params['department_id'] : null;
            $vehicle_equipment_id = isset($params['vehicle_equipment_id']) && !empty($params['vehicle_equipment_id']) ? $params['vehicle_equipment_id'] : null;
            $accounting_ledger_id = isset($params['accounting_ledger_id']) && !empty($params['accounting_ledger_id']) ? $params['accounting_ledger_id'] : null;
            $reference = isset($params['reference']) && !empty($params['reference']) ? $params['reference'] : null;
            $no_rpo = isset($params['no_rpo']) && @$params['no_rpo'] == 1 ? $params['no_rpo'] : null;

            if ($company_id) {
                $this->M_Material_request->where('company_id', $company_id);
            }

            if ($accounting_ledger_id) {
                $this->M_Material_request->where('accounting_ledger_id', $accounting_ledger_id);
            }

            if ($project_id !== null) {
                $this->M_Material_request->where('project_id', $project_id);
            }
            if ($sub_project_id !== null) {
                $this->M_Material_request->where('sub_project_id', $sub_project_id);
            }
            if ($property_id !== null) {
                $this->M_Material_request->where('property_id', $property_id);
            }
            if ($amenity_id !== null) {
                $this->M_Material_request->where('amenity_id', $amenity_id);
            }
            if ($sub_amenity_id !== null) {
                $this->M_Material_request->where('sub_amenity_id', $sub_amenity_id);
            }
            if ($department_id !== null) {
                $this->M_Material_request->where('department_id', $department_id);
            }
            if ($vehicle_equipment_id !== null) {
                $this->M_Material_request->where('vehicle_equipment_id', $vehicle_equipment_id);
            }
            if ($request_status !== null) {
                $this->M_Material_request->where('request_status', $request_status);
            }
            if ($request_type !== null) {
                $this->M_Material_request->where('request_type', $request_type);
            }
            if ($customer_type !== null) {
                $this->M_Material_request->where('customer_type', $customer_type);
            }
            if ($reference !== null) {
                $this->M_Material_request->like('reference', $reference, 'both');
            }
        } else {
            $this->M_Material_request->where('id', $id);
        }

        $this->M_Material_request->order_by('id', 'DESC');

        if ($with_relations === 'yes') {
            $result = $this->M_Material_request
                ->with_project()
                ->with_department()
                ->with_sub_project()
                ->with_company()
                ->with_amenity()
                ->with_property()
                ->with_sub_amenity()
                ->with_approving_staff()
                ->with_requesting_staff()
                ->with_accounting_ledger()
                ->get_all();
        } else {
            $result = $this->M_Material_request->get_all();
        }

        if ($no_rpo ?? null) {
            foreach ($result as $key => $r) {
                $rpo_items = $this->db->query("SELECT COUNT(`id`) AS `rpo_count` FROM `purchase_order_request_items` WHERE `material_request_id` = '" . $r['id'] . "'")->row_array();
                if ($rpo_items['rpo_count'] > 0) {
                    unset($result[$key]);
                }
            }
        }

        return $result;
    }

    public function search()
    {

        $result = $this->__search($_GET);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function deprecated()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // Get record count
        // $conditions['returnType'] = 'count';
        $this->view_data['totalRec'] = $totalRec = $this->M_Material_request->count_rows();

        // Pagination configuration
        $config['target'] = '#material_request_content';
        $config['base_url'] = base_url('material_request/paginationData');
        $config['total_rows'] = $totalRec;
        $config['per_page'] = $this->perPage;
        $config['link_func'] = 'MaterialRequestPagination';

        // Initialize pagination library
        $this->ajax_pagination->initialize($config);

        // Get records
        $this->view_data['records'] = $this->M_Material_request
            ->limit($this->perPage, 0)
            ->get_all();
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue([
            'vendors/custom/datatables/datatables.bundle.js',
            'js/vue2.js',
            'js/axios.min.js'
        ]);

        $this->template->build('__index', $this->view_data);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Material_request->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Material_request->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue([
            'vendors/custom/datatables/datatables.bundle.js',
            'js/vue2.js',
            'js/axios.min.js'
        ]);

        $this->template->build('index', $this->view_data);
    }
    function filterKeyword($data, $search, $field = '')
    {
        $filter = '';

        if (isset($search['value'])) {
            $filter = $search['value'];
        }

        if (!empty($filter)) {
            if (!empty($field)) {
                if (strpos(strtolower($field), 'date') !== false) {
                    // filter by date range
                    $data = $this->filterByDateRange($data, $filter, $field);
                } else {
                    // filter by column
                    $data = array_filter($data, function ($a) use ($field, $filter) {

                        if ($a[$field] == $filter) {
                            return 1;
                        } else {
                            return 0;
                        }

                        // return (boolean) preg_match( "/$filter/i", $a[ $field ] );

                    });
                }
            } else {
                // general filter
                $data = array_filter($data, function ($a) use ($filter) {
                    // print_r($a); die();
                    // return (boolean) preg_grep( "/$filter/i", (array) $a );
                    foreach ($a as $key => $row) {
                        if (is_array($a[$key])) {
                            if (key_exists('name', $a[$key])) {
                                if ($a[$key]['name'] == $filter) {
                                    return 1;
                                }
                            }
                        } else {
                            if ($key == 'request_status') {
                                $this->load->helper('material_request');
                                if (mr_status_reverse_lookup($filter) === 1) {
                                    return 1;
                                }
                            } else {
                                if ($a[$key] == $filter) {
                                    return 1;
                                }
                            }
                        }
                    }
                    return 0;
                });
            }
        }

        return $data;
    }

    public function showMaterialRequests()
    {
        header('Content-Type: application/json');
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'reference' => true,
            'company_id' => false,
            'company' => true,
            'customer_type' => true,
            'request_type' => true,
            'request_date' => true,
            'request_status' => true,
            'requesting_staff_id' => true,
            'approving_staff_id' => true,
            'status' => true,
            'created_by' => true,
            'updated_by' => true

            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data

        if (isset($_REQUEST['filter'])) {
            $_filters = $_GET;
            $material_requests = $this->__search($_filters);
        } else {
            $material_requests = $this->M_Material_request->with_company()->with_accounting_ledger()->order_by('id', 'DESC')->as_array()->get_all();
        }

        $data = [];

        if ($material_requests) {

            foreach ($material_requests as $key => $value) {

                $customer_ref = mr_customer_type_ref($value['customer_type']);

                $customer = isset($customer_ref['fk']) ? get_object_from_table($value[$customer_ref['fk']], $customer_ref['table'], false, 'name ASC', null, null, 'id, name') : null;

                $material_requests[$key]['customer_type'] = isset($value[$customer_ref['fk']]) ? "<a href='/" . $customer_ref['controller'] . '/view/' . $value[$customer_ref['fk']] . "'><span class='badge badge-secondary p-2 mr-2'>" . (ucfirst($value['customer_type']) ?? '') . "</span> " . $customer['name'] . "</a>" : '';

                $material_requests[$key]['request_date'] = view_date($value['request_date']);

                $material_requests[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $material_requests[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($material_requests as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public
    function paginationData()
    {
        if ($this->input->is_ajax_request()) {


            // Input from General Search
            $keyword = $this->input->post('keyword');


            // Input from Advanced Filter
            //                $name = $this->input->post('name');
            /* ==================== begin: Add model fields ==================== */

            $keys = [];
            foreach ($_POST as $search_key => $search_value) {
                if (substr($search_key, 0, strlen("filter_")) === "filter_") {
                    if (!empty($search_value)) {
                        $__key = array(
                            "field" => str_replace("filter_", "", $search_key),
                            "value" => $search_value
                        );
                        array_push($keys, $__key);
                    }
                }
            }


            /* ==================== end: Add model fields ==================== */

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_Material_request->count_rows();
            $where = array();

            // Pagination configuration
            $config['target'] = '#material_request_content';
            $config['base_url'] = base_url('material_request/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'MaterialRequestPagination';

            // Query


            $totalRec = $this->M_Material_request->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            //                $this->view_data['records'] = $records = $this->M_Material_request
            //                    ->limit($this->perPage, $offset)
            //                    ->get_all();


            if (!empty($keyword)) :
                $this->M_Material_request->like('reference', $keyword, 'both');
            endif;
            foreach ($keys as $key) {

                $this->M_Material_request->where($key['field'], $key['value']);
            }
            $this->M_Material_request->limit($this->perPage, $offset);
            $this->view_data['records'] = $this->M_Material_request->get_all();

            $this->load->view('material_request/_filter', $this->view_data, false);
        }
    }

    private
    function process_create_master($data, $additional)
    {
        $data['reference'] = uniqidMR();
        $data['request_amount'] = remove_commas($data['request_amount']);

        $material_request = $this->db->insert('material_requests', array_merge($data, $additional));

        if ($material_request) {
            $this->db->where('ledger_id', $data['accounting_ledger_id']);
            $budget = $this->M_budget_setup->get();
            if($budget){
                $id = $budget['id'];
                $init_amount = $budget['amount'];
                $requested_amount = $data['request_amount'];
                $budget_info['amount'] = $init_amount - $requested_amount;
                $budget_result = $this->M_budget_setup->update($budget_info, $id);
                if($budget_result){
                    $budget_log_info = [
                        'ledger_id' => $data['accounting_ledger_id'],
                        'budgeting_setup_id' => $id,
                        'material_request_id' => $this->db->insert_id(),
                        'amount' => $requested_amount
                    ];
                }
            }
            return $this->db->insert_id();
        } else {
            return false;
        }
        



    }

    private
    function process_create_slave($data, $additional, $amended = null)
    {
        $material_request_item = $this->M_Material_request_item->insert($data, $additional);

        $desc = $amended ? "Amended: Item created" : "Item created";

        $this->log_item($material_request_item, null, $desc);

        return $material_request_item;
    }

    private
    function process_update_master($data, $additional, $id)
    {
        return $this->M_Material_request->update($data + $additional, $id);
    }

    private
    function process_update_slave($data, $additional, $request_id, $amended = null)
    {
        /*
             * 1. Get all children of master
             * 2. If current item is not listed, create new
             * 3. Else, update
             */
        $id = isset($data['id']) ? $data['id'] : null;

        if ($id) {

            $desc = $amended ? "Amended: Item updated" : "Item updated";

            $this->log_item($id, null, $desc);

            return $this->M_Material_request_item->update($data + $additional, $id);
        } else {
            $data['material_request_id'] = $request_id;
            $additional = [
                'created_by' => $additional['updated_by'],
                'created_at' => $additional['updated_at']
            ];
            return $this->process_create_slave($data, $additional, $amended);
        }
    }

    private
    function process_purge_deleted($items, $additional, $amended = null)
    {
        if ($items) {
            foreach ($items as $item) {
                if ($item) {

                    $desc = $amended ? "Amended: Item deleted" : "Item deleted";

                    $this->log_item($item, null, $desc);

                    $this->M_Material_request_item->delete($item);
                }
            }
        }
    }

    private function log_item($id, $data, $description)
    {
        if (!!$id) {
            
            $log = $this->M_Material_request_item->get($id);
            unset($log['id']);
            $log['material_request_item_id'] = $id;
        } else {

            if (!!$data) {

                $log = $data;
            }
        }

        $log['description'] = $description;
        $log['log_time'] = NOW;
        $this->db->insert('material_request_item_logs', $log);
    }

    public
    function form($id = null)
    {
        $this->js_loader->queue([
            'js/vue2.js',
            // 'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js',
            'js/axios.min.js'
        ]);

        $this->load->helper('accounting_db');
        $this->load->helper('approver');

        $approver_setting = get_approver($this->module_name);
        $this->db->where('user_id = ' . $this->user->id);
        $staff = $this->M_staff->get();

        $this->view_data['id'] = $id;
        $this->view_data['fillables'] = $this->M_Material_request->form_fillables;

        if ($id) {
            $method = "Update";
            $form_data = $this->M_Material_request->get($id);
        } else {
            $method = "Create";
            $form_data = $this->M_Material_request->fillable;
            $this->view_data['approver'] = $approver_setting;
            $this->view_data['staff'] = $staff;
        }

        $this->view_data['form_data'] = $form_data;
        $this->view_data['accounting_ledgers'] = get_expense_ledgers();

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            $request = $this->input->post('request');
            $items = $this->input->post('items');
            $deleted_items = $this->input->post('deleted_items');

            if ($id) {

                $additional = [
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];

                $this->db->trans_start(); # Starting Transaction
                $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                $result = $this->process_update_master($request, $additional, $id);
                if ($result) {
                    foreach ($items as $item) {
                        $this->process_update_slave($item, $additional, $id);
                    }
                }
                $this->db->trans_complete(); # Completing request
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $this->process_purge_deleted($deleted_items, $additional);

                    $response['status'] = 1;
                    $response['message'] = 'Material Request Successfully ' . $method . 'd!';
                }
            } else {
                $additional = [
                    'created_by' => $this->user->id,
                    'created_at' => NOW,
                ];
                $this->db->trans_start(); # Starting Transaction
                $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
                $result = $this->process_create_master($request, $additional);
                if ($result) {
                    foreach ($items as $item) {
                        $item['material_request_id'] = $result;
                        $item_id = $this->process_create_slave($item, $additional);
                    }
                }
                $this->db->trans_complete(); # Completing request
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();

                    $response['status'] = 1;
                    $response['message'] = 'Material Request Successfully ' . $method . 'd!';
                }
            }

            //            if ($this->form_validation->run() === true) {
            //
            //            } else {
            //                $response['status'] = 0;
            //                $response['message'] = validation_errors();
            //            }
            echo json_encode($response);
            exit();
        } else {

            $this->template->build('vueform', $this->view_data);
        }
    }

    public
    function amend($id = null)
    {
        $this->js_loader->queue([
            // 'js/vue2.js',
            'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js',
            'js/axios.min.js'
        ]);

        if ($id) {
            
            $form_data = $this->M_Material_request->get($id);
        }

        $this->view_data['form_data'] = $form_data;

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            $request_amount = $this->input->post('request_amount');
            $items = $this->input->post('items');
            $deleted_items = $this->input->post('deleted_items');

            if ($id) {

                $additional = [
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];

                $this->db->trans_start(); # Starting Transaction
                $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                $this->M_Material_request->update([
                    'request_amount' => $request_amount,
                    'updated_at' => NOW
                ], $id);

                if (!!$items) {

                    foreach ($items as $item) {
                        $this->process_update_slave($item, $additional, $id, true);
                    }
                }

                $this->db->trans_complete(); # Completing request
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $this->process_purge_deleted($deleted_items, $additional, true);

                    $response['status'] = 1;
                    $response['message'] = 'Material Request Successfully Amended';
                }
            }

            echo json_encode($response);
            exit();
        } else {

            $this->template->build('amend_form', $this->view_data);
        }
    }

    public
    function __form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            if ($this->form_validation->run() === true) {
                $request = $this->input->post('request');
                $items = $this->input->post('item_data');
                $deleted_items = $this->input->post('deleted_items');

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    $result = $this->process_update_master($request, $additional, $id);

                    if ($result) {
                        foreach ($items as $item) {
                            $this->process_update_slave($item, $additional, $id);
                        }
                    }
                    $this->db->trans_complete(); # Completing request
                    if ($this->db->trans_status() === false) {
                        # Something went wrong.
                        $this->db->trans_rollback();
                        $response['status'] = 0;
                        $response['message'] = 'Error!';
                    } else {
                        # Everything is Perfect.
                        # Committing data to the database.
                        $this->db->trans_commit();
                        $this->process_purge_deleted($deleted_items, $additional);

                        $response['status'] = 1;
                        $response['message'] = 'Material Request Successfully ' . $method . 'd!';
                    }
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];
                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
                    $result = $this->process_create_master($request, $additional);
                    if ($result) {
                        foreach ($items as $item) {
                            $item['material_request_id'] = $result;
                            $this->process_create_slave($item, $additional);
                        }
                    }
                    $this->db->trans_complete(); # Completing request
                    if ($this->db->trans_status() === false) {
                        # Something went wrong.
                        $this->db->trans_rollback();
                        $response['status'] = 0;
                        $response['message'] = 'Error!';
                    } else {
                        # Everything is Perfect.
                        # Committing data to the database.
                        $this->db->trans_commit();

                        $response['status'] = 1;
                        $response['message'] = 'Material Request Successfully ' . $method . 'd!';
                    }
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;
        $this->view_data['master_fields'] = array(
            'id',
            'project_id',
            'company_id',
            'approving_staff_id',
            'requesting_staff_id',
            'reference',
            'days_lapsed',
            'particulars',
            'request_date',
            'request_amount',
            'request_reason',
            'customer_type',
            'sub_project_id',
            'amenity_id',
            'sub_amenity_id',
            'property_id',
            'department_id',
            'vehicle_equipment_id',
        );
        $this->view_data['child_fields'] = array(
            'id',
            'item_brand_id',
            'item_class_id',
            'item_id',
            'unit_of_measurement_id',
            'material_request_id',
            'quantity',
            'unit_cost',
            'request_reason',
            'total',
        );

        $this->view_data['method'] = $method;

        if ($id) {
            $this->view_data['info'] = $this->M_Material_request->with_material_request_items()->get($id);
            $this->view_data['items'] = $this->M_Material_request_item->get_all(array('material_request_id' => $id));
            $this->view_data['id'] = $id;
        } else {
            $this->view_data['id'] = null;
        }

        $this->js_loader->queue([
            //                'js/vue2.js',
            'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js',
            'js/axios.min.js'
        ]);

        $this->view_data['project_list'] = get_objects_from_table_by_field('projects', null, null, 'name ASC')->result();
        $this->view_data['amenity_list'] = get_objects_from_table_by_field('amenities', null, null, 'name ASC')->result();
        $this->view_data['department_list'] = get_objects_from_table_by_field('departments', null, null, 'name ASC')->result();
        $this->view_data['current_user'] = $this->user->id;

        $this->template->build('form', $this->view_data);
    }

    public
    function view($id = FALSE)
    {
        //        $this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');
        //
        //        $this->js_loader->queue([
        //            '//www.amcharts.com/lib/3/amcharts.js',
        //            '//www.amcharts.com/lib/3/serial.js',
        //            '//www.amcharts.com/lib/3/radar.js',
        //            '//www.amcharts.com/lib/3/pie.js',
        //            '//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
        //            '//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
        //            '//www.amcharts.com/lib/3/plugins/export/export.min.js',
        //            '//www.amcharts.com/lib/3/themes/light.js'
        //        ]);
        $this->js_loader->queue([
            'js/vue2.js',
            'js/axios.min.js'
        ]);

        if ($id) {
            $material_request = $this->M_Material_request
                ->get($id);

            $this->view_data['info'] = $material_request;

            $this->load->model('project/project_model');
            $this->load->model('company/company_model');
            $this->load->model('material_request_item/material_request_item_model');

            $this->view_data['project'] = $this->project_model->get($material_request['project_id']);
            $this->view_data['company'] = $this->company_model->get($material_request['company_id']);
            $this->view_data['id'] = $id;
            $this->view_data['items'] = $this->M_Material_request_item->where(
                'material_request_id',
                $id
            )->get_all();

            if ($this->view_data['info']) {
                $this->template->build('view', $this->view_data);
            } else {
                show_404();
            }
        } else {

            show_404();
        }
    }

    public
    function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();
            $_input['created_by'] = $this->session->userdata['user_id'];

            $result = $this->M_Material_request->from_form()->insert($_input);

            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Material Request successfully created.', 'material_request');
            }
        }

        $this->template->build('create');
    }

    public
    function update($id = false)
    {
        if ($id) {

            $this->view_data['material_request'] = $data = $this->M_Material_request->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();
                    $_input['updated_by'] = $this->session->userdata['user_id'];

                    $result = $this->M_Material_request->from_form()->update($_input, $data['id']);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'material_request');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public
    function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Material_request->get($id);
            if ($list) {

                $deleted = $this->M_Material_request->delete($list['id']);
                if ($deleted !== FALSE) {

                    $response['status'] = 1;
                    $response['message'] = 'Material Request successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public
    function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Material_request->delete($value);
                }
                if ($deleted !== FALSE) {

                    $response['status'] = 1;
                    $response['message'] = 'Material Request successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $material_requests = $this->M_Material_request->as_array()->get_all();
        if ($material_requests) {

            foreach ($material_requests as $_lkey => $material_request) {

                $_datas[$material_request['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_material_requests_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'material_request');
                }
            } else {

                $_filename = 'list_of_material_requests_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'material_request');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($material_requests as $_lkey => $material_request) {

                                $_datas[$material_request['id']][$_title] = isset($material_request[$_column]) && ($material_request[$_column] !== '') ? Dropdown::get_static('bool', $material_request[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($material_requests as $_lkey => $material_request) {

                                if ($_name === 'status') {

                                    $_datas[$material_request['id']][$_title] = isset($material_request[$_name]) && $material_request[$_name] ? Dropdown::get_static('inventory_status', $material_request[$_name], 'view') : '';
                                } else {

                                    $_datas[$material_request['id']][$_title] = isset($material_request[$_name]) && $material_request[$_name] ? $material_request[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Material Request');
                $_objSheet->setCellValue('A1', 'LIST OF MATERIAL REQUEST');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'material_request');
            }
        } else {

            $this->notify->error('No Record Found', 'material_request');
        }
    }

    function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? TRUE : FALSE;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Material Request CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'material_request');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Material_request->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Material Request');
            $_objSheet->setCellValue('A1', 'MATERIAL REQUEST');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public
    function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'name' => $upload_data[1],
                        /* ==================== begin: Add model fields ==================== */

                        /* ==================== end: Add model fields ==================== */
                    );

                    $material_request_id = $upload_data[0];
                    $material_request = $this->M_Material_request->get($material_request_id);

                    if ($material_request) {
                        $result = $this->M_Material_request->update($fields, $material_request_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Material_request->insert($fields);
                    } else {
                        $fields = array(
                            'name' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Material_request->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'material_request');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'material_request');
        die();
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            $this->view_data['info'] = $info = $this->M_Material_request
                ->with_project()
                ->with_department()
                ->with_sub_project()
                ->with_company()
                ->with_amenity()
                ->with_property()
                ->with_sub_amenity()
                ->with_approving_staff()
                ->with_requesting_staff()
                ->get($id);

            $request_items = $this->M_Material_request_item
                ->where('material_request_id', $id)
                ->with_item_group()
                ->with_item_type()
                ->with_item_brand()
                ->with_item_class()
                ->with_item()
                ->with_unit_of_measurement()
                ->get_all();

            $this->view_data['request_items'] = $request_items;

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }
}
