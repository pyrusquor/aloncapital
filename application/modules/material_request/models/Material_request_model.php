<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Material_request_model extends MY_Model
    {

        public $table = 'material_requests'; // you MUST mention the table name
        public $primary_key = 'id'; // you MUST mention the primary key
        public $fillable = [
            'id',
            /* ==================== begin: Add model fields ==================== */
            /* ==================== begin: Relation fields ==================== */
            'project_id',
            'company_id',
            'approving_staff_id',
            'requesting_staff_id',
            // adds
            'sub_project_id',
            'amenity_id',
            'sub_amenity_id',
            'property_id',
            'department_id',
            'vehicle_equipment_id',
            'accounting_ledger_id',
            // customer
            'customer_type',
            /* ==================== end: Relation fields ==================== */
            'reference',
            'days_lapsed',
            'particulars',
            'request_date',
            'request_amount',
            'request_reason',
            /* ==================== State ==================== */
            /* Request Status
                - New Request
                - For RPO
                - For Issuance
                - Issued
                - Cancelled
             * Dropdown::get_static('material_request_status')
             */
            'request_status',
            'request_type',
            /* ==================== end: Add model fields ==================== */
            'created_by',
            'created_at',
            'updated_by',
            'updated_at'
        ]; // If you want, you can set an array with the fields that can be filled by insert/update

        public $form_exclude = [
            'created_by',
            'created_at',
            'updated_by',
            'updated_at'
        ];

        public $form_fillables = [
            'project_id',
            'company_id',
            'approving_staff_id',
            'requesting_staff_id',
            'sub_project_id',
            'amenity_id',
            'sub_amenity_id',
            'property_id',
            'department_id',
            'vehicle_equipment_id',
            'customer_type',
            'reference',
            'days_lapsed',
            'particulars',
            'request_date',
            'request_amount',
            'request_reason',
            'request_status',
            'request_type',
            'accounting_ledger_id',
        ];

        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = TRUE;
            $this->return_as = 'array';

            // Pagination
            $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
            $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;

            // for relationship tables
            $this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
            $this->has_one['department'] = array('foreign_model' => 'department/department_model', 'foreign_table' => 'departments', 'foreign_key' => 'id', 'local_key' => 'department_id');
            $this->has_one['sub_project'] = array('foreign_model' => 'sub_project/sub_project_model', 'foreign_table' => 'sub_projects', 'foreign_key' => 'id', 'local_key' => 'sub_project_id');
            $this->has_one['company'] = array('foreign_model' => 'company/company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
            $this->has_one['amenity'] = array('foreign_model' => 'amenity/amenity_model', 'foreign_table' => 'amenities', 'foreign_key' => 'id', 'local_key' => 'amenity_id');
            $this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');
            $this->has_one['sub_amenity'] = array('foreign_model' => 'sub_amenity/sub_amenity_model', 'foreign_table' => 'sub_amenities', 'foreign_key' => 'id', 'local_key' => 'sub_amenity_id');
            $this->has_one['approving_staff'] = array('foreign_model' => 'staff/staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'approving_staff_id');
            $this->has_one['requesting_staff'] = array('foreign_model' => 'staff/staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'requesting_staff_id');
            $this->has_one['accounting_ledger'] = array('foreign_model' => 'accounting_ledgers/accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'accounting_ledger_id');

            $this->has_many['request_items'] = array('foreign_model' => 'material_request_item/material_request_item_model', 'foreign_table' => 'material_request_items', 'foreign_key' => 'material_request_id', 'local_key' => 'id', 'get_relate' => TRUE);


        }

        public function get_columns()
        {
            $_return = false;

            if ($this->fillable) {
                $_return = $this->fillable;
            }

            return $_return;
        }

    }
