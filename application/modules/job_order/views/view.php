<?php

// vdebug($job_order);

$number = isset($job_order['number']) && $job_order['number'] ? $job_order['number'] : '';
$company = isset($job_order['company']) && $job_order['company'] ? $job_order['company']['name'] : '';
$project = isset($job_order['project']) && $job_order['project'] ? $job_order['project']['name'] : '';
$description = isset($job_order['description']) && $job_order['description'] ? $job_order['description'] : '';
$staff = isset($job_order['staff']) && $job_order['staff'] ? $job_order['staff']['first_name'] . " " . $job_order['staff']['last_name'] : '';
$status = isset($job_order['status']) && $job_order['status'] ? $job_order['status'] : '';
$date_requested = isset($job_order['created_at']) && $job_order['created_at'] ? view_date($job_order['created_at']) : '';

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Job Order</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('job_order'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Job Order
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Job Order No.:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $number; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Sub-Project:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Justification:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        SBU:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Sub-Amenity:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Company:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $company; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Amenity:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Approved By:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Project:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $project; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Order Description:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $description; ?></p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Project Category:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Equipment:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Date Ordered:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $date_requested; ?></p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Property:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold">N/A</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Ordered By:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $staff; ?></p>
                                </div>
                            </div>
                        </div>
                            <!--end::Form-->
                        </div>
                    
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>
    </div>
</div>
<!-- begin:: Footer -->
