<?php
    $number            =    isset($info['number']) && $info['number'] ? $info['number'] : '';
    $project_id            =    isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
    $company_id            =    isset($info['company_id']) && $info['company_id'] ? $info['company_id'] : '';
    $request_description            =    isset($info['description']) && $info['description'] ? $info['description'] : '';
    $staff_id            =    isset($info['staff_id']) && $info['staff_id'] ? $info['staff_id'] : '';

    $company = $info['company']['name'];
    $staff_name = $info['staff']['first_name'] . " " . $info['staff']['last_name'];
?>


<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Job Order No. <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="number" value="<?php echo set_value('number', $number); ?>" placeholder="Job Order No.">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
            </div>
            <?php echo form_error('number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Project <span class="kt-font-danger">*</span></label>
            <select class="form-control kt-select2" id="project_id" name="project_id">
                <?php foreach ($project as $p): ?>
                        <option value="<?php echo $p['id'] ?>" <?php echo ($project_id == $p['id']) ? 'selected' : '' ?>><?php echo ucwords($p['name']); ?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="companies" id="company_id" name="company_id" required>
                    <option value="">Select Company</option>
                    <?php if ($company): ?>
                        <option value="<?php echo $company_id; ?>" selected><?php echo $company; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('or_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Request Description <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="request_description" value="<?php echo set_value('request_description', $request_description); ?>" placeholder="Request Description">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-columns"></i></span></span>
            </div>
            <?php echo form_error('request_description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Requested By <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <select class="form-control suggests" data-module="staff"  data-type="person" id="staff_id" name="staff_id" required>
                <option value="">Select Staff</option>
                <?php if ($staff_name): ?>
                    <option value="<?php echo $staff_id; ?>" selected><?php echo $staff_name; ?></option>
                <?php endif?>
            </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="fab la-adn"></i></span></span>
            </div>
            <?php echo form_error('staff_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>