<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Job_order_model extends MY_Model
{
    public $table = 'job_orders'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'number', 
        'payment_type',
        'job_order_date',
        'staff_id',
        'priority_level',
        'approved_by',
        'job_request_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['staff'] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'staff_id');

        $this->has_one['approver'] = array('foreign_model' => 'staff/Staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'approved_by');

        $this->has_one['job_request'] = array('foreign_model' => 'job_request/Job_request_model', 'foreign_table' => 'job_requests', 'foreign_key' => 'id', 'local_key' => 'job_request_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
