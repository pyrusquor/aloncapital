<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
$description = isset($data['description']) && $data['description'] ? $data['description'] : '';
$code = isset($data['code']) && $data['code'] ? $data['code'] : '';
$rate = isset($data['rate']) && $data['rate'] ? $data['rate'] : '';
$ledger_id = isset($data['ledger_id']) && $data['ledger_id'] ? $data['ledger_id'] : '';

$ledger_name = isset($data['ledgers']) && $data['ledgers'] ? $data['ledgers']['name'] : '';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Tax Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Tax Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-money"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Tax Description <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Tax Description" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-edit"></i></span>
            </div>
            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Tax Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="code" value="<?php echo set_value('code', $code); ?>" placeholder="Tax Code" autocomplete="off">
            </div>
            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Tax Rate <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="rate" value="<?php echo set_value('rate', $rate); ?>" placeholder="Tax Rate" autocomplete="off">
            </div>
            <?php echo form_error('rate'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Ledger <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="accounting_ledgers"  id="ledger_id" name="ledger_id">
                    <option value="">Select Ledgers</option>
                    <?php if ($ledger_name): ?>
                    <option value="<?php echo $ledger_id; ?>" selected><?php echo $ledger_name; ?></option>
                <?php endif?>
                </select>
            </div>
            <?php echo form_error('ledger_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>