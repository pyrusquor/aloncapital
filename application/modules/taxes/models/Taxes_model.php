<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Taxes_model extends MY_Model
{
    public $table = 'taxes'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name',
        'description',
        'code',
        'rate',
        'ledger_id',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required',
        ),
        'code' => array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim|required',
        ),
        'rate' => array(
            'field' => 'rate',
            'label' => 'Rate',
            'rules' => 'trim|required',
        ),
        'ledger_id' => array(
            'field' => 'ledger_id',
            'label' => 'Ledger',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['ledgers'] = array('foreign_model' => 'chart_of_accounts/Chart_of_accounts_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'ledger_id');
        
        $this->has_one['tax'] = array('foreign_model' => 'taxes/Taxes_model', 'foreign_table' => 'taxes', 'foreign_key' => 'id', 'local_key' => 'tax_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    // public function insert_dummy()
    // {
    //     require APPPATH . '/third_party/faker/autoload.php';
    //     $faker = Faker\Factory::create();

    //     $data = [];

    //     for ($x = 0; $x < 10; $x++) {
    //         array_push($data, array(
    //             'company' => $faker->word,
    //             'sbu' => $faker->word,
    //             'warehouse' => $faker->word,
    //             'item_code' => $faker->word,
    //             'item_name' => $faker->word,
    //             'current_physical_count' => $faker->word,
    //             'is_active' => $faker->numberBetween(0, 1),
    //         ));
    //     }
    //     $this->db->insert_batch($this->table, $data);

    // }
}
