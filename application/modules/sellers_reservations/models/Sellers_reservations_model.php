<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sellers_reservations_model extends MY_Model
{

    public $table = 'sellers_reservations'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'prospect_id',
        'property_id',
        'seller_id',
        'buyer_id',
        'reservation_fee',
        'seller_group',
        'payment_status',
        'remarks',
        'expected_reservation_date',
        'expiration_date',
        'is_active',
        'reservation_status',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        array(
            'field' => 'prospect_id',
            'label' => 'Prospect',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'property_id',
            'label' => 'Property',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'seller_id',
            'label' => 'Seller',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'expected_reservation_date',
            'label' => 'Expected Reservation Date',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'expiration_date',
            'label' => 'Expiration Date',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['seller'] = array('foreign_model' => 'seller/Seller_model', 'foreign_table' => 'sellers', 'foreign_key' => 'id', 'local_key' => 'seller_id');

        $this->has_one['property'] = array('foreign_model' => 'property/Property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');

        $this->has_one['prospect'] = array('foreign_model' => 'prospect/Prospect_model', 'foreign_table' => 'prospect', 'foreign_key' => 'id', 'local_key' => 'prospect_id');

        $this->has_one['buyer'] = array('foreign_model' => 'buyer/Buyer_model', 'foreign_table' => 'buyers', 'foreign_key' => 'id', 'local_key' => 'buyer_id');
        
        $this->has_one['payment'] = array('foreign_model' => 'sellers_reservations/Sellers_reservation_payment_model', 'foreign_table' => 'sellers_reservation_payments', 'foreign_key' => 'sellers_reservation_id', 'local_key' => 'id');
        
        $this->has_one['seller_groups'] = array('foreign_model' => 'seller_group/Seller_group_model', 'foreign_table' => 'seller_group', 'foreign_key' => 'id', 'local_key' => 'seller_group');
        
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
