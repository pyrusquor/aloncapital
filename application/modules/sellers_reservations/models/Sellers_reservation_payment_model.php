<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sellers_reservation_payment_model extends MY_Model
{

    public $table = 'sellers_reservation_payments'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'sellers_reservation_id',
        'receipt_type',
        'or_number',
        'accounting_entry_id',
        'amount',
        'date_paid',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;
    }

}
