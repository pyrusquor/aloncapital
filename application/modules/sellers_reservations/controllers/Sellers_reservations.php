<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sellers_reservations extends MY_Controller
{
    private $fields = [
        array(
            'field' => 'prospect_id',
            'label' => 'Prospect',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'property_id',
            'label' => 'Property',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'seller_id',
            'label' => 'Seller',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'expected_reservation_date',
            'label' => 'Expected Reservation Date',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'expiration_date',
            'label' => 'Expiration Date',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('sellers_reservations/Sellers_reservations_model', 'M_Sellers_reservations');
        $this->load->model('sellers_reservations/Sellers_reservation_payment_model', 'M_Sellers_reservation_payments');
        $this->load->model('property/Property_model', 'M_Property');
        $this->load->model('accounting_entries/Accounting_entries_model', 'M_Accounting_entry');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');
        $this->load->model('accounting_settings/Accounting_settings_model', 'M_Accounting_settings');
        $this->load->model('accounting_setting_items/Accounting_setting_items_model', 'M_Accounting_setting_items');
        $this->_table_fillables = $this->M_Sellers_reservations->fillable;
        $this->_table_columns = $this->M_Sellers_reservations->__get_columns();
        $this->additional = ['created_by' => $this->user->id, 'created_at' => NOW];
        $this->u_additional = ['updated_by' => $this->user->id, 'updated_at' => NOW];
    }

    public function index()
    {

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'reservation_status' => true,
            'buyer_id' => true,
            'property_id' => true,
            'seller_id' => true,
            'expected_reservation_date' => true,
            'expiration_date' => true,
            'payment_status' => true,
            'is_active' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);
        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];
        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Sellers_reservations
                ->with_buyer()
                ->with_seller()
                ->with_property()
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

            //     $query->or_where("(amount like '%$searchValue%'");
            //     $query->or_where("month like '%$searchValue%'");
            //     $query->or_where("year like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'buyer_id' => [
                    'data' => $filters['buyer_id'] ?? null,
                    'operator' => '=',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'seller_id' => [
                    'data' => $filters['seller_id'] ?? null,
                    'operator' => '=',
                ],
                'expected_reservation_date' => [
                    'data' => $filters['expected_reservation_date'] ?? null,
                    'operator' => '=',
                ],
                'expiration_date' => [
                    'data' => $filters['expiration_date'] ?? null,
                    'operator' => '=',
                ],
                'reservation_status' => [
                    'data' => $filters['reservation_status'] ?? null,
                    'operator' => '=',
                ],
                'payment_status' => [
                    'data' => $filters['payment_status'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {
                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {
                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();
                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {
                        $items[$key]['buyer_id'] = @$value['buyer']['first_name'] . " " .  @$value['buyer']['last_name'];

                        $items[$key]['property_id'] = @$value['property']['name'];
        
                        $items[$key]['seller_id'] =  @$value['seller']['first_name'] . " " .  @$value['seller']['last_name'];
        
                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';
        
                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    
                        $items[$key]['payment_status'] = Dropdown::get_static('sellers_reservation_payment_status',$value['payment_status'],'view');
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Sellers_reservations->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form()
    {

        $data = $this->input->get();
        if ($data) {
            $project_id = $data['project_id'];
            $property_id = $data['property_id'];
            $property_data = $this->M_Property->fields('id,name,status')->with_project(['fields' => 'id,name'])->get($property_id);
            $this->view_data['property'] = $property_data;
            $property_status = $property_data['status'];
            if ($property_status != '1') {
                $this->notify->error('Property not Available!', 'sellers_reservations');
            }
        } else {
            redirect(base_url('sellers_reservations'));
        }

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            // PROPERTY STATUS
            //  '1' => 'Available', 
            //  '2' => 'Reserved', 
            //  '3' => 'Sold', 
            //  '4' => 'Hold', 
            //  '5' => 'Not For Sale'

            // RESERVATION STATUS
            // 1 - Reserved
            // 2 - Completed
            // 3 - Cancelled
            // 4 - Expired
            $post_data = $this->input->post();
            $post_data['expected_reservation_date'] = $post_data['expected_reservation_date'] . ' 08:00:00';
            $post_data['expiration_date'] = date('Y-m-d H:i:s', strtotime($post_data['expected_reservation_date'] . ' + 3 days'));
            $post_data['is_active'] = 1;

            if ($post_data) {
                $response['status'] = 0;
                $response['message'] = 'Oops! Please refresh the page and try again.';
                $result = $this->M_Sellers_reservations->insert($post_data);
                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                } else {
                    $form = $this->input->post();
                    $property_id = $form['property_id'];
                    $property_data = ["status" => 4];
                    if ($property_id) {
                        $this->M_Property->update($property_data, $property_id);
                    } else {
                        $response['status'] = 0;
                        $response['message'] = 'No property id!';
                        echo json_encode($response);
                        exit();
                    }

                    $reservation["is_active"] = 0;
                    $reservation["reservation_status"] = 1;
                    $form_status = $this->M_Sellers_reservations->update(
                        $reservation,
                        $result
                    );
                    if ($form_status) {
                        $response['status'] = 1;
                        $response['message'] = 'Reserved Successfully!';
                    }
                }
            }
            echo json_encode($response);
            exit();
        }
        $this->template->build('form', $this->view_data);
    }

    public function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();

            $result = $this->M_Sellers_reservations->from_form()->insert($_input);

            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Seller Reservation successfully created.', 'sellers_reservationss');
            }
        }

        $this->template->build('create');
    }

    public function update($id = false)
    {
        if ($id) {

            $this->view_data['sellers_reservationss'] = $data = $this->M_Sellers_reservations->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();

                    $result = $this->M_Sellers_reservations->from_form()->update($_input, $data['id']);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'sellers_reservationss');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Sellers_reservations->get($id);
            if ($list) {

                $deleted = $this->M_Sellers_reservations->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Sellers_reservations->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Type Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['sellers_reservationss'] = $this->M_Sellers_reservations->get($id);

            if ($this->view_data['sellers_reservationss']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;

        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'company' => $upload_data[1],
                        /* ==================== begin: Add model fields ==================== */
                        'sbu' => $upload_data[2],
                        'warehouse' => $upload_data[3],
                        'item_code' => $upload_data[4],
                        'item_name' => $upload_data[5],
                        'in' => $upload_data[6],
                        'out' => $upload_data[7],
                        'balance' => $upload_data[8],
                        'remarks' => $upload_data[9],
                        'is_active' => $upload_data[10],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $sellers_reservationss_id = $upload_data[0];
                    $sellers_reservationss = $this->M_Sellers_reservations->get($sellers_reservationss_id);

                    if ($sellers_reservationss) {
                        $result = $this->M_Sellers_reservations->update($fields, $sellers_reservationss_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'company' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */
                            'sbu' => $upload_data[1],
                            'warehouse' => $upload_data[2],
                            'item_code' => $upload_data[3],
                            'item_name' => $upload_data[4],
                            'in' => $upload_data[5],
                            'out' => $upload_data[6],
                            'balance' => $upload_data[7],
                            'remarks' => $upload_data[8],
                            'is_active' => $upload_data[9],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Sellers_reservations->insert($fields);
                    } else {
                        $fields = array(
                            'company' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */
                            'sbu' => $upload_data[2],
                            'warehouse' => $upload_data[3],
                            'item_code' => $upload_data[4],
                            'item_name' => $upload_data[5],
                            'in' => $upload_data[6],
                            'out' => $upload_data[7],
                            'balance' => $upload_data[8],
                            'remarks' => $upload_data[9],
                            'is_active' => $upload_data[10],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Sellers_reservations->insert($fields);
                    }
                }
                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'sellers_reservationss');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'sellers_reservationss');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Sub Warehouse Inventory Logs CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'sellers_reservationss');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Sellers_reservations->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Sub Warehouse Inventory Logs');
            $_objSheet->setCellValue('A1', 'SUB WAREHOUSE INVENTORY LOGS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $sellers_reservationsss = $this->M_Sellers_reservations->as_array()->get_all();
        if ($sellers_reservationsss) {

            foreach ($sellers_reservationsss as $_lkey => $sellers_reservationss) {

                $_datas[$sellers_reservationss['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_sellers_reservationsss_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'sellers_reservationss');
                }
            } else {

                $_filename = 'list_of_sellers_reservationsss_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns    =    $this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'sellers_reservationss');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($sellers_reservationsss as $_lkey => $sellers_reservationss) {

                                $_datas[$sellers_reservationss['id']][$_title] = isset($sellers_reservationss[$_column]) && ($sellers_reservationss[$_column] !== '') ? Dropdown::get_static('bool', $sellers_reservationss[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($sellers_reservationsss as $_lkey => $sellers_reservationss) {

                                if ($_name === 'status') {

                                    $_datas[$sellers_reservationss['id']][$_title] = isset($sellers_reservationss[$_name]) && $sellers_reservationss[$_name] ? Dropdown::get_static('inventory_status', $sellers_reservationss[$_name], 'view') : '';
                                } else {

                                    $_datas[$sellers_reservationss['id']][$_title] = isset($sellers_reservationss[$_name]) && $sellers_reservationss[$_name] ? $sellers_reservationss[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Sub Warehouse Inventory Logs');
                $_objSheet->setCellValue('A1', 'LIST OF SUB WAREHOUSE INVENTORY LOGS');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_style = array(
                    'font' => array(
                        'bold' => true,
                        'size' => 10,
                        'name' => 'Verdana',
                    ),
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'sellers_reservationss');
            }
        } else {

            $this->notify->error('No Record Found', 'sellers_reservationss');
        }
    }

    public function cancel_reservation($id = false)
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($id) {
            $reservation_is_active = ['is_active' => 1];
            $reservation_status = ['reservation_status' => 3];
            $property_status = ['status' => 1];

            $result = $this->M_Sellers_reservations->update($reservation_is_active, $id);


            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {
                $property = $this->M_Sellers_reservations->get($id);

                $this->M_Sellers_reservations->update($reservation_status, $id);


                $this->M_Property->update($property_status, $property['property_id']);

                $response['status'] = 1;
                $response['message'] = 'Reservation Cancelled';
                // Success
                // $this->notify->success('Reservation Cancelled.', 'sellers_reservations');

            }
            echo json_encode($response);
            exit();
        } else {
            show_404();
        }
    }
    public function payment($id = 0)
    {
        $post_data = $this->input->post();
        if ($id || $post_data) {
            if ($post_data) {
                $response['status'] = 0;
                $response['message'] = 'Something went wrong';

                $accounting_setting = $this->get_entries($post_data['company_id'], $post_data['project_id'], 1, 2, 1, 6);
                if (!$accounting_setting) {
                    $response['message'] = 'No appropriate accounting setting found!';
                } else {
                    $accounting_items = $accounting_setting['accounting_setting_item'];
                    $this->db->trans_begin();



                    $entry_data = [
                        'company_id' => $post_data['company_id'],
                        'or_number' => $post_data['or_number'],
                        'invoice_number' => $post_data['or_number'],
                        'journal_type' => 3,
                        'payment_date' => $post_data['date_paid'],
                        'payee_type' => 'buyers',
                        'payee_type_id' => $post_data['buyer_id'],
                        'remarks' => 'Seller Reservation',
                        'cr_total' => $post_data['amount'],
                        'dr_total' => $post_data['amount'],
                        'is_approve' => 1,
                        'project_id' => $post_data['project_id'],
                        'property_id' => $post_data['property_id']
                    ];
                    
                    $accounting_entry_id = $this->M_Accounting_entry->insert($entry_data+$this->additional);
                    foreach($accounting_items as $item_key=>$item){
                        $entry_item_data = [
                            'accounting_entry_id' => $accounting_entry_id,
                            'ledger_id' => $item['accounting_entry'],
                            'amount' => $post_data['amount'],
                            'dc' => $item['accounting_entry_type'],
                            'payee_type' => 'buyers',
                            'payee_type_id' => $post_data['buyer_id'],
                            'description' => 'Seller Reservation'
                        ];
                        $this->M_Accounting_entry_items->insert($entry_item_data + $this->additional);
                    }
                    $payment_data = [
                        'sellers_reservation_id' => $post_data['sellers_reservation_id'],
                        'receipt_type' => $post_data['receipt_type'],
                        'or_number' => $post_data['or_number'],
                        'amount' => $post_data['amount'],
                        'date_paid' => $post_data['date_paid'],
                        'accounting_entry_id' => $accounting_entry_id
                    ];
                    $this->M_Sellers_reservations->update(['payment_status' => 2] + $this->u_additional, $post_data['sellers_reservation_id']);
                    $this->M_Sellers_reservation_payments->insert($payment_data + $this->additional);
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                        $response['status'] = 1;
                        $response['message'] = 'Payment processed successfully!';
                    }
                }

                echo json_encode($response);
                exit();
            } else {
                $reservation = $this->M_Sellers_reservations->with_property(['with' => ['relation' => 'project', 'fields' => 'company_id'], 'fields' => 'id,name'])->get($id);
                if ($reservation['reservation_status'] != '1') {
                    $this->notify->error('Invalid reservation!', 'sellers_reservations');
                }
                $this->view_data['reservation'] = $reservation;
                $this->template->build('payment', $this->view_data);
            }
        } else {
            $this->notify->error('Please provide a reservation id!', 'sellers_reservations');
        }
    }
    public function get_entries($company_id = 0, $project_id = 0, $period_id, $category_type, $entry_types_id, $module_types_id)
    {
        $filter = [
            'company_id' => $company_id,
            'project_id' => $project_id,
            'period_id' => $period_id,
            'category_type' => $category_type,
            'entry_types_id' => $entry_types_id,
            'module_types_id' => $module_types_id
        ];
        $accounting_setting = $this->M_Accounting_settings->with_accounting_setting_item()->get($filter);
        if (!$accounting_setting) {
            unset($filter['project_id']);
            $accounting_setting = $this->M_Accounting_settings->with_accounting_setting_item()->get($filter);
        }
        return $accounting_setting;
    }
}
