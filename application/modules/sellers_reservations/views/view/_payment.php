<?php
// vdebug($reservation);
?>
<div class='row'>
    <input type='hidden' name='project_id' value='<?= @$reservation['property']['project']['id'] ?>'>
    <input type='hidden' name='company_id' value='<?= @$reservation['property']['project']['company_id'] ?>'>
    <input type='hidden' name='property_id' value='<?= @$reservation['property_id'] ?>'>
    <input type='hidden' name='sellers_reservation_id' value='<?= @$reservation['id'] ?>'>
    <input type='hidden' name='buyer_id' value='<?= @$reservation['buyer_id'] ?>'>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Reservation Fee<span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <input class='form-control' name='amount' value='<?= @$reservation['reservation_fee'] ?>' type="text" readonly>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Date Paid<span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <input type="text" class="form-control kt_datepicker" name="date_paid" id="datepicker" value="<?= date('Y-m-d', strtotime(NOW));  ?>" readonly placeholder="" autocomplete="off">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Receipt Type <span class="kt-font-danger">*</span></label>
            <select class="form-control" name="receipt_type" id="receipt_type">
                <option value="">Select Type</option>
                <option value="1">Collection Receipt</option>
                <option value="2">Acknowledgement Receipt</option>
                <option value="3">Provisionary Receipt</option>
                <option value="4">Official Receipt</option>
                <option value="5">Manual Receipt</option>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Receipt Number</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" id="OR_number" placeholder="OR Number" name="or_number" value="">
            </div>
        </div>
    </div>
</div>