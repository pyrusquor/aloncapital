<?php
// ==================== begin: Add model fields ====================

// vdebug($property);
// ==================== end: Add model fields ====================
?>

<div class="row">
    <input type='hidden' name='property_id' value='<?= @$property['id'] ?>'>

    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Project</label>
            <div class="kt-input-icon">
                <input class="form-control" type='text' value='<?= @$property['project']['name']; ?>' disabled>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Property</label>
            <div class="kt-input-icon">
                <input class="form-control" type='text' value='<?= @$property['name']; ?>' disabled>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Seller</label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="sellers" data-type='person' id="seller_id" name="seller_id">
                </select>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Seller Group</label>
            <div class="kt-input-icon">
                <select class="form-control suggests-show-options" data-module="seller_group" name="seller_group">
                </select>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Buyer</label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="buyers" data-type='person' id="buyer_id" name="buyer_id">
                </select>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Expected Reservation Date<span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <input type="text" class="form-control kt_datepicker" name="expected_reservation_date" id="datepicker" value="<?= date('Y-m-d', strtotime(NOW));  ?>" readonly placeholder="" autocomplete="off">
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Reservation Fee<span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="reservation_fee" id="datepicker" value="" placeholder="" autocomplete="off" min='0' step='any'>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Notes<span class="kt-font-danger">*</span></label>
            <div class="row">
                <div class="col-sm-12">
                    <textarea class="form-control" value='test' name="remarks" rows='15'></textarea>
                </div>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>