<?php

$name = isset($item_group['name']) && $item_group['name'] ? $item_group['name'] : '';
$group_code = isset($item_group['group_code']) && $item_group['group_code'] ? $item_group['group_code'] : '';
$account_number = isset($item_group['account_number']) && $item_group['account_number'] ? $item_group['account_number'] : '';
$status = isset($item_group['is_active']) && $item_group['is_active'] ? $item_group['is_active'] : '';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
     <div class="col-sm-6">
        <div class="form-group">
            <label>Group Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="group_code" value="<?php echo set_value('group_code', $group_code); ?>" placeholder="Group Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('group_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Account Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" maxlength="8" name="account_number" value="<?php echo set_value('account_number', $account_number); ?>" placeholder="Account Number" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-credit-card"></i></span>
            </div>
            <?php echo form_error('account_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
     <div class="col-sm-6">
        <div class="form-group">
            <label>Status</label>
            <div class="kt-input-icon kt-input-icon--left">

                <?php echo form_dropdown('is_active', Dropdown::get_static('inventory_status'), set_value('is_active', @$status), 'class="form-control"'); ?>
                <?php echo form_error('is_active'); ?>

                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-check-square"></i></span>
            </div>

            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>