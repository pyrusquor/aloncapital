<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item_group_model extends MY_Model {
    public $table = 'item_group'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = ['name', 'group_code', 'account_number', 'is_active', 'created_by', 'updated_by', 'deleted_by'];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        'group_code' => array(
            'field' => 'group_code',
            'label' => 'Group Code',
            'rules' => 'trim'
        ),
        'account_number' => array(
            'name' => 'account_number',
            'label' => 'Account Number',
            'rules' => 'trim'
        ),
        'is_active' => array(
            'name' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim'
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
                'group_code' => $faker->word,
                'account_number' => $faker->numberBetween(10000000,99999999),
                'status' => $faker->numberBetween(0, 1)
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}