<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
// ==================== begin: Add model fields ====================
$bank_id = isset($data['bank_id']) && $data['bank_id'] ? $data['bank_id'] : '';
$bank = isset($data['accounting_ledger']['name']) && $data['accounting_ledger']['name'] ? $data['accounting_ledger']['name'] : '';
$branch = isset($data['branch']) && $data['branch'] ? $data['branch'] : '';
$amount = isset($data['amount']) && $data['amount'] ? $data['amount'] : '';
$unique_number = isset($data['unique_number']) && $data['unique_number'] ? $data['unique_number'] : '';
$due_date = isset($data['due_date']) && $data['due_date'] ? $data['due_date'] : '';
$particulars = isset($data['particulars']) && $data['particulars'] ? $data['particulars'] : '';
$status = isset($data['status']) && $data['status'] ? 'Active' : 'Inactive';
$is_active = isset($data['is_active']) && $data['is_active'] ? 'Active' : 'Inactive';
// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Cheque</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('cheque/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('cheque');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Bank ID
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $bank_id; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Bank
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $bank; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Branch
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $branch; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Amount
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo money_php($amount); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Unique Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $unique_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Due Date
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $due_date; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Particulars
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $particulars; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Check Status
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $status; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Status
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $is_active; ?></span>
                                </div>
                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
