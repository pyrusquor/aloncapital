<?php
// ==================== begin: Add model fields ====================
$bank_id = isset($cheque['bank_id']) && $cheque['bank_id'] ? $cheque['bank_id'] : '';
$branch = isset($cheque['branch']) && $cheque['branch'] ? $cheque['branch'] : '';
$amount = isset($cheque['amount']) && $cheque['amount'] ? $cheque['amount'] : '';
$unique_number = isset($cheque['unique_number']) && $cheque['unique_number'] ? $cheque['unique_number'] : '';
$due_date = isset($cheque['due_date']) && $cheque['due_date'] ? $cheque['due_date'] : date('Y-m-d');
$particulars = isset($cheque['particulars']) && $cheque['particulars'] ? $cheque['particulars'] : '';
$status = isset($cheque['status']) && $cheque['status'] ? $cheque['status'] : '';
$is_active = isset($cheque['is_active']) && $cheque['is_active'] ? $cheque['is_active'] : '';

$bank_name = isset($cheque['accounting_ledger']['name']) && $cheque['accounting_ledger']['name'] ? $cheque['accounting_ledger']['name'] : '';

// ==================== end: Add model fields ====================

?>
<!-- ==================== begin: Add form model fields ==================== -->
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Bank <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="banks" data-type="banks"  id="bank_id" name="bank_id">
                <option value="">Select Bank</option>
                <?php if ($bank_name): ?>
                    <option value="<?php echo $bank_id; ?>" selected><?php echo $bank_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Branch <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="branch" value="<?php echo set_value('branch', $branch); ?>" placeholder="Branch" autocomplete="off">
            </div>
            <?php echo form_error('branch'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Amount <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="number" class="form-control" id="amount" placeholder="Amount"
                           name="amount"
                           value="<?php echo set_value('amount', @$amount); ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Unique Number <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="unique_number" value="<?php echo set_value('unique_number', $unique_number); ?>" placeholder="Unique Number" autocomplete="off">
            </div>
            <?php echo form_error('unique_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Due Date <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control compDatepicker" placeholder="Due Date" name="due_date" value="<?php echo set_value('due_date"', @$due_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Status <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('status', Dropdown::get_static('cheque_status'), set_value('status', @$status), 'class="form-control"'); ?>
                
            </div>
            <?php echo form_error('status'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <div class="form-group">
                <label for="message">Particulars</label>
                <textarea type="text" class="form-control" id="particulars" name="particulars" placeholder="Particulars" rows="7"><?php echo set_value('particulars', @$particulars); ?></textarea>
            </div>
        </div>
    </div>
</div>
<!-- ==================== end: Add form model fields ==================== -->

