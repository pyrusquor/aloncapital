<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_demography_model extends MY_Model
{

	public function get_demographics($params = array())
	{
		$this->db->from('transaction_properties');

		$this->db->join('transactions', 'transaction_properties.transaction_id = transactions.id', 'left');

		$this->db->join('buyers', 'transactions.buyer_id = buyers.id', 'left');

		$this->db->join('buyer_employment_table', 'buyer_employment_table.buyer_id = buyers.id', 'left');

		if (isset($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('transactions.reservation_date BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}

		if (isset($params['age'])) {
			$this->db->select('
				COUNT(DISTINCT(buyers.id)) as count,
			');

			$ages = explode('-', $params['age']);
			$age_from = $ages[0];
			$age_to  = $ages[1];
			$this->db->where('(year(curdate()) - year(`buyers`.`birth_date`)) BETWEEN "' . $age_from . '" AND "' . $age_to . '"');

			$query = $this->db->get()->row_array();

			return $query;
		}

		if (isset($params['project_id'])) {
			$this->db->where('transaction_properties.project_id', $params['project_id']);
		}

		if (isset($params['gender'])) {
			$this->db->select('buyers.gender, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.gender');
		}

		if (isset($params['civil_status'])) {
			$this->db->select('buyers.civil_status_id, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.civil_status_id');
		}

		if (isset($params['housing_membership'])) {
			$this->db->select('buyers.housing_membership_id, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.housing_membership_id');
		}

		if (isset($params['country_id'])) {
			$this->db->select('buyers.country_id, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.country_id');
		}

		if (isset($params['region_id'])) {
			$this->db->select('buyers.regCode, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.regCode');
		}

		if (isset($params['province_id'])) {
			$this->db->select('buyers.provCode, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.provCode');
		}

		if (isset($params['city_id'])) {
			$this->db->select('buyers.citymunCode, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.citymunCode');
		}

		if (isset($params['barangay_id'])) {
			$this->db->select('buyers.brgyCode, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.brgyCode');
		}

		if (isset($params['sitio'])) {
			$this->db->select('
				COUNT(DISTINCT(buyers.id)) as count,
			');

			$this->db->select('buyers.sitio as name');
			$this->db->group_by('buyers.sitio')->having('count(buyers.sitio) > 1');

			$query = $this->db->get()->row_array();

			return $query;
		}

		if (isset($params['e_type'])) {
			$this->db->select('buyer_employment_table.occupation_type_id, COUNT(DISTINCT(buyer_employment_table.id)) as count');
			$this->db->group_by('buyer_employment_table.occupation_type_id');
		}

		if (isset($params['industry_type'])) {
			$this->db->select('buyer_employment_table.industry_id, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyer_employment_table.industry_id');
		}

		if (isset($params['occupation_type'])) {
			$this->db->select('buyer_employment_table.occupation_id, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyer_employment_table.occupation_id');
		}

		if (isset($params['source_referral_type'])) {
			$this->db->select('buyers.source_referral, COUNT(DISTINCT(buyers.id)) as count');
			$this->db->group_by('buyers.source_referral');
		}

		$this->db->where('transactions.deleted_at IS NULL');
		$this->db->where('transaction_properties.deleted_at IS NULL');
		$this->db->where('buyers.deleted_at IS NULL');

		$query = $this->db->get()->result_array();

		return $query;
	}


	public function get_seller_demographics($params = array())
	{

		$this->db->select('
			COUNT(sellers.id) as count,
        ');

		$this->db->from('sellers');

		$this->db->join('seller_positions', 'seller_positions.id = sellers.seller_position_id', 'left');

		if (!empty($params['age'])) {
			$ages = explode('-', $params['age']);
			$age_from = $ages[0];
			$age_to  = $ages[1];
			$this->db->where('(year(curdate()) - year(`sellers`.`birth_date`)) BETWEEN "' . $age_from . '" AND "' . $age_to . '"');
		}

		if (!empty($params['sales_group'])) {
			$this->db->where('sellers.sales_group_id', $params['sales_group']);
		}

		if (!empty($params['seller_position'])) {
			$this->db->where('sellers.seller_position_id', $params['seller_position']);
		}

		if (!empty($params['position_type'])) {
			$this->db->where('seller_positions.position_type_id', $params['position_type']);
		}

		if (!empty($params['region_id'])) {
			$this->db->where('sellers.regCode', $params['region_id']);
		}

		if (!empty($params['province_id'])) {
			$this->db->where('sellers.provCode', $params['province_id']);
		}

		if (!empty($params['city_id'])) {
			$this->db->where('sellers.citymunCode', $params['city_id']);
		}

		if (!empty($params['barangay_id'])) {
			$this->db->where('sellers.brgyCode', $params['barangay_id']);
		}


		if (!empty($params['daterange'])) {
			$dates = explode('-', $params['daterange']);
			$f_date_from = db_date($dates[0]);
			$f_date_to  = db_date($dates[1]);
			$this->db->where('sellers.created_at BETWEEN "' . $f_date_from . '" AND "' . $f_date_to . '"');
		}


		if (!empty($params['gender'])) {
			$this->db->where('sellers.gender', $params['gender']);
		}

		if (!empty($params['civil_status'])) {
			$this->db->where('sellers.civil_status_id', $params['civil_status']);
		}

		$this->db->where('sellers.deleted_at IS NULL');

		$query = $this->db->get()->row_array();
		return $query;
	}
}
