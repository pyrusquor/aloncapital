<?php 
    $CI =& get_instance();
    $CI->load->model('menu/Menu_model','menu');
    $CI->load->model('auth/Ion_auth_model','auth');
    $menu = $CI->menu->load_menu();
?>
<link rel="stylesheet" href="<?=base_url();?>assets/css/dashboard/stackpath.css">
<link rel="stylesheet" href="<?=base_url();?>assets/css/dashboard/custom-style.css">
<link href="<?=base_url();?>assets/css/dashboard/hover-min.css" rel="stylesheet" media="all">
<link href="<?=base_url();?>assets/css/dashboard/hover.css" rel="stylesheet" media="all">

<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

<div class="dashboard-wrapper">

        <!-- Dashboard Main -->
        <div class="dashboard-main">
            <div class="container">

                    <div class="row">
                        
                          <div class="col-md-4 c-p">
                            <div class="dashboard-cards">
                                <a href="<?=base_url();?>applicant" class="marketing">
                                    <div class="dashboard-cards-inner">
                                        <img class="dashboard-icon hvr-float marketing" src="<?=base_url();?>assets/img/dashboard/img/Alon/Applicants.png" alt=""><br>
                                        Applicants
                                    </div>
                                </a>
                            </div>
                        </div>

                          <div class="col-md-4 c-p">
                            <div class="dashboard-cards">
                                <a href="<?=base_url();?>loan" class="marketing">
                                    <div class="dashboard-cards-inner">
                                        <img class="dashboard-icon hvr-float marketing" src="<?=base_url();?>assets/img/dashboard/img/Alon/Loans.png" alt=""><br>
                                        Loans
                                    </div>
                                </a>
                            </div>
                        </div>

                          <div class="col-md-4 c-p">
                            <div class="dashboard-cards">
                                <a href="<?=base_url();?>disbursements" class="marketing">
                                    <div class="dashboard-cards-inner">
                                        <img class="dashboard-icon hvr-float marketing" src="<?=base_url();?>assets/img/dashboard/img/Alon/Disbursements.png" alt=""><br>
                                        Disbursements
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>

                   
            </div>
        </div>

        <!-- Legal -->
        <div class="dashboard-legal-submain hidden">
            <div class="container">

                    <div class="row">   
                        <div class="col-md-3">
                            
                        </div>       
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Legal/png/Company.png" alt=""><br>
                                    <a href="<?=base_url();?>company">Company</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Legal/png/Sub-Business-Unit.png" alt=""><br>
                                    <a href="<?=base_url();?>sbu">Sub-Business Unit</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Legal/png/Land-Inventory.png" alt=""><br>
                                    <a href="<?=base_url();?>land">Land Inventory</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            
                        </div>

                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Marketing -->
        <div class="dashboard-marketing-submain hidden">
            <div class="container">

                    <div class="row">
                        <div class="col-md-2">
                          
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Marketing Logo/png/Prospects.png" alt=""><br>
                                    <a href="<?=base_url();?>prospect">Prospects</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Marketing Logo/png/Marketing.png" alt=""><br>
                                    <a href="<?=base_url();?>seller_reservations">Seller Reservations</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Marketing Logo/png/Communications.png" alt=""><br>
                                    <a href="<?=base_url();?>communication">Communications</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Marketing Logo/png/Sales-Support.png" alt=""><br>
                                    <a href="<?=base_url();?>ticketing">Sales Support</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            
                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Sales Admin -->
        <div class="dashboard-sales_admin-submain hidden">
            <div class="container">

                    <div class="row">
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Alon/png/Projects.png" alt=""><br>
                                    <a href="<?=base_url();?>project" class="">Projects</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Alon/png/Properties.png" alt=""><br>
                                    <a href="<?=base_url();?>property">Properties</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Alon/png/Sellers.png" alt=""><br>
                                    <a href="<?=base_url();?>seller">Sellers</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Alon/png/Buyers.png" alt=""><br>
                                    <a href="<?=base_url();?>buyer">Buyers</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Alon/png/Contracts.png" alt=""><br>
                                    <a href="<?=base_url();?>transaction">Contracts</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">

                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Documentation -->
        <div class="dashboard-documentation-submain hidden">
            <div class="container">

                    <div class="row">
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Documentation Logo/png/List.png" alt=""><br>
                                    <a href="<?=base_url();?>document" class="">List</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Documentation Logo/png/Checklist.png" alt=""><br>
                                    <a href="<?=base_url();?>checklist">Checklist</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Documentation Logo/png/Form-Generator.png" alt=""><br>
                                    <a href="<?=base_url();?>form_generator">Form Generator</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Documentation Logo/png/File-Manager.png" alt=""><br>
                                    <a href="<?=base_url();?>storage">File Manager</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Documentation Logo/png/Contracts-Documents.png" alt=""><br>
                                    <a href="<?=base_url();?>transaction_document">Contracts Documents</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">

                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Credit & Collection -->
        <div class="dashboard-credit_collection-submain hidden">
            <div class="container">

                    <div class="row">
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Credit and Collections/png/Collections.png" alt=""><br>
                                    <a href="<?=base_url();?>transaction_payment" class="">Collections</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Credit and Collections/png/Commissions.png" alt=""><br>
                                    <a href="<?=base_url();?>transaction_commission">Commissions</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Credit and Collections/png/Post-Dated-Cheque.png" alt=""><br>
                                    <a href="<?=base_url();?>transaction_post_dated_checks">Post Dated Cheque</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Credit and Collections/png/AR-Clearing.png" alt=""><br>
                                    <a href="<?=base_url();?>ar_clearing">AR Clearing</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Credit and Collections/png/Letter-Request.png" alt=""><br>
                                    <a href="<?=base_url();?>transaction_letter_request">Letter Request</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Credit and Collections/png/Past-Due-Notices.png" alt=""><br>
                                    <a href="<?=base_url();?>transaction_past_due_notices">Post Due Notices</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Property Management -->
        <div class="dashboard-property_management-submain hidden">
            <div class="container">

                    <div class="row">
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-2">
 
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Property Management/png/Customer-Care.png" alt=""><br>
                                    <a href="<?=base_url();?>complaint_tickets">Customer Care</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Property Management/png/Inquiry-Tickets.png" alt=""><br>
                                    <a href="<?=base_url();?>inquiry_tickets">Inquiry Tickets</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                           
                        </div>
                        <div class="col-md-2">
                         
                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Accounting -->
        <div class="dashboard-accounting-submain hidden">
            <div class="container">

                    <div class="row">
                        <div class="col-md-1">
                         
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Accounting/png/Entries.png" alt=""><br>
                                    <a href="<?=base_url();?>accounting_entries">Entries</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Accounting/png/Regular-Cheques.png" alt=""><br>
                                    <a href="<?=base_url();?>cheque">Regular Cheques</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Accounting/png/Payment-Voucher.png" alt=""><br>
                                    <a href="<?=base_url();?>payment_request">Payment Voucher</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Accounting/png/Journal-Voucher.png" alt=""><br>
                                    <a href="<?=base_url();?>journal_voucher">Journal Voucher</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Accounting/png/Reconcilliation.png" alt=""><br>
                                    <a href="<?=base_url();?>reconciliation">Reconcilliation</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1">
                         
                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Taxation -->
        <div class="dashboard-taxation-submain hidden">
            <div class="container">

                    <div class="row">
                        <div class="col-md-5">
                           
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Taxation/png/Sales-Recognition.png" alt=""><br>
                                    <a href="<?=base_url();?>sales_recognize">Sales Recognition</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                           
                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Warehouse -->
        <div class="dashboard-warehouse-submain hidden">
            <div class="container">

                    <div class="row">
                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Warehouse/png/Warehouse.png" alt=""><br>
                                    <a href="javascript:void(0);">Warehouse</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Warehouse/png/Sub-Warehouse.png" alt=""><br>
                                    <a href="javascript:void(0);">Sub Warehouse</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Warehouse/png/Virtual-Warehouse.png" alt=""><br>
                                    <a href="javascript:void(0);">Virtual Warehouse</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Warehouse/png/Inventory.png" alt=""><br>
                                    <a href="javascript:void(0);">Inventory</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Warehouse/png/Material-Receiving.png" alt=""><br>
                                    <a href="javascript:void(0);">Material Receiving</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Warehouse/png/Material-Issuance.png" alt=""><br>
                                    <a href="javascript:void(0);">Material Issuance</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Construction -->
        <div class="dashboard-construction-submain hidden">
            <div class="container">

                    <div class="row">

                        <div class="col-md-3">

                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Construction/png/Job-Request.png" alt=""><br>
                                    <a href="javascript:void(0);">Job Request</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Construction/png/Construction-Template.png" alt=""><br>
                                    <a href="javascript:void(0);">Construction Template</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Construction/png/Construction-Order.png" alt=""><br>
                                    <a href="javascript:void(0);">Construction Order</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">

                        </div>
                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

        <!-- Purchasing -->
        <div class="dashboard-purchasing-submain hidden">
            <div class="container">

                    <div class="row">                 

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Purchasing/png/Suppliers.png" alt=""><br>
                                    <a href="javascript:void(0);">Suppliers</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Purchasing/png/Contractors.png" alt=""><br>
                                    <a href="javascript:void(0);">Contractors</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Purchasing/png/Material-Request.png" alt=""><br>
                                    <a href="javascript:void(0);">Material Request</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Purchasing/png/Purchase-Order-Request.png" alt=""><br>
                                    <a href="javascript:void(0);">Purchase Order Request</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Purchasing/png/Purchase-Order.png" alt=""><br>
                                    <a href="javascript:void(0);">Purchase Order</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="dashboard-cards">
                                <div class="dashboard-cards-inner">
                                    <img class="dashboard-icon hvr-float" src="<?=base_url();?>assets/img/dashboard/img/Purchasing/png/Payment-Request.png" alt=""><br>
                                    <a href="javascript:void(0);">Payment Request</a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <button class="btn btn-success back-button"style="text-align: center;"><</button>
            </div>
        </div>

    </div>

<script src="<?=base_url();?>assets/js/dashboard/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="<?=base_url();?>assets/js/dashboard/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="<?=base_url();?>assets/js/dashboard/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="<?=base_url();?>assets/js/dashboard/jquery.flip.min.js"></script>

<script src="<?=base_url();?>assets/js/custom.js"></script>

<style type="text/css">
    .dashboard-icon{
        height: 100%;
        width: 90px;
        color: white;
        background-color: #0A284D;
        border-radius: 50%;
        display: inline-block;
    }
</style>