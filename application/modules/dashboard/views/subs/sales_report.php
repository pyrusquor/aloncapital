<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Sales Report Graph <small>All Projects (Consolidated)</small>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-12">
                <div id="container"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div id="container_1"></div>
            </div>
            <div class="col-md-6">
                <div id="container_2"></div>
            </div>
        </div>
    </div>
</div>


<!-- <div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Sales Report Table <small>All Projects (Consolidated)</small>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <table class="table table-striped- table-bordered table-hover">
               <thead>
                   <th>Property Category</th>
                   <?php if ($months): ?>
                       <?php foreach ($months as $key => $month) { ?>
                            <th><?=view_date($month,"month_year");?></th>
                      <?php } ?>
                   <?php endif ?>
               </thead>
               <tbody>
                   <?php foreach ($types as $key => $type) { ?>
                        <tr>
                            <td><?=Dropdown::get_static('sub_project_type',$type,'view');?></td>
                        </tr>
                    <?php } ?>
               </tbody>
           </table>  
        </div>
    </div>
</div> -->