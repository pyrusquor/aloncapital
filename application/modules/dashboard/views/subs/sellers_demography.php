<!-- INSERT SALES REPORT GRAPH HERE -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Sellers Demography <small>All Projects (Consolidated)</small>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">


        <div class="row">
            <div class="col-md-6"><div id="container"></div> </div>
            <div class="col-md-6"><div id="container2"></div> </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12"><div id="container7"></div> </div>
        </div>

        <br>
        <br>

        <div class="row">
            <div class="col-md-6"><div id="container3"></div> </div>
            <div class="col-md-6"><div id="container4"></div> </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6"><div id="container5"></div> </div>
            <div class="col-md-6"><div id="container6"></div> </div>

        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6"><div id="container8"></div> </div>
            <div class="col-md-6"><div id="container9"></div> </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6"><div id="container10"></div> </div>
            <div class="col-md-6"><div id="container11"></div> </div>
        </div>

        
    </div>
</div>