<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Sellers Graph <small>All Projects (Consolidated)</small>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div id="container"></div>
        <br>
        <br>
        <div id="container_2"></div>
    </div>
</div>


<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Top Sellers <small>All Projects (Consolidated)</small>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <table class="table table-striped- table-bordered table-hover">
               <thead>
                   <td>Rank</td>
                   <td>Seller Name</td>
                   <td>Group</td>
                   <td>Position</td>
                   <td>Total Selling Price</td>
                   <td>Total Property Count</td>
               </thead>
               <tbody>
                    <?php foreach ($tops as $key => $top) { $key++; ?>
                        <tr>
                            <td><?=$key;?></td>
                            <td><?=$top['first_name']." ".$top['last_name'];?></td>
                            <td><?=Dropdown::get_static('group_type',$top['sales_group_id'],'view');?></td>
                            <td><?=$top['sp_name'];?></td>
                            <td><?=money_php($top['total_selling_price']);?></td>
                            <td><?=$top['t_count'];?></td>
                        </tr>
                    <?php } ?>
               </tbody>
           </table>  
        </div>
    </div>
</div>