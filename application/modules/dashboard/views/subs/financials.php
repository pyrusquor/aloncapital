<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Financials Graph <small>All Projects (Consolidated)</small>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div id="container"></div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Collection Efficiency Rate <small>All Projects (Consolidated)</small>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <table class="table table-striped- table-bordered table-hover">
               <thead>
                   <td>Period</td>
                   <td>Receivables</td>
                   <td>Actual Collection</td>
                   <td>Percentage</td>
               </thead>
               <tbody>
                    <?php foreach ($f_report['dates'] as $key => $date) { ?>
                        <tr>
                            <td><?=view_date($date,$month_opt);?></td>
                            <td><?=money_php($r = $f_report['rows'][$date]['receivable']['t_amount']);?></td>
                            <td><?=money_php($c = $f_report['rows'][$date]['collection']['t_amount']);?></td>
                            <td><?=round(get_percentage($r,$c));?>%</td>
                       </tr>
                    <?php } ?>
              </tbody>
            </table>
        </div>
    </div>
</div>