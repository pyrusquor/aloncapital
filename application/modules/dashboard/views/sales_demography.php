<style>
    .highcharts-figure, .highcharts-data-table table {
        min-width: 360px; 
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
</style>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Buyers Demography
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>

        </div>
        <div class="kt-subheader__toolbar">
            <!-- <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                <i class="fa fa-filter"></i> Filter
            </button> -->
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
                <!-- <a href="<?php echo site_url('complaint_sub_categories/form'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Create Complaint Sub Category
                </a>              -->
        </div>

        <div class="module__filter">
                <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn"
                data-toggle="modal" data-target="#filterModal" aria-expanded="true" >
                    <i class="fa fa-filter"></i> Filter
                </button>
        </div>
    </div>
</div>


<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="g_container">
    
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="form-control-label">Project Name:</label>
                            <select class="form-control" name="project_id" id="project_id">
                                <option value="0">Select Project</option>
                                <?php if ($projects): ?>
                                    <?php foreach ($projects as $key => $project) { ?>
                                      <option value="<?=$project['id'];?>">
                                          <?=$project['name'];?>
                                      </option>
                                    <?php } ?>
                                <?php endif ?>
                            </select>
                        </div>


                        <div class="form-group col-md-6">
                            <label class="">Date Range <span class="kt-font-danger">*</span></label>
                            <input type="text" class="form-control" readonly name="daterange" id="daterangepicker">
                            <span class="form-text text-muted"></span>
                        </div>
                       
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>