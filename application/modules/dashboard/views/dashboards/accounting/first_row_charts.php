<div class="row">

    <div class="col-md-36 col-lg-36 col-xl-12">
        <div class="kt-portlet kt-portlet--head-noborder" id='payment_requests_line_chart_portlet'>
            <div class="kt-portlet__head kt-portlet__head--fuid">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title">
                        <a href="javascript:void(0);" data-target='payment_request' id='accounting_pr_click'><h2 class="kt-font-dark" style='margin-top:25px;'>PAYMENT REQUESTS <span id="payment_requests_range" class="text-muted"></span></h2></a>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--space-x">
                <canvas id="payment_requests_line_chart" style="display: flex; min-height: 450px; max-height: 450px; padding: 0px; margin: 0px;" class="chartjs-render-monitor"></canvas>
            </div>
            <input type='hidden' id='accounting_selected_date_range'>
        </div>
    </div>
</div>