<div class="row">

    <div class="col-md-36 col-lg-36 col-xl-12">
        <div class="kt-portlet kt-portlet--head-noborder">
            <div class="kt-portlet__body kt-portlet__body--fit">

                <div class="row" style="margin-left:10px;">
                    <div class="col-md-9 col-lg-9 col-xl-3">
                        <h3 class="kt-subheader__title kt-font-boldest kt-font-transform-u">
                            Project
                        </h3>
                        <div style="width:300px;padding-bottom:20px;">
                            <select class="form-control suggests" data-module="projects" id="accounting_project_id">
                                <option value="">ALL PROJECTS</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-2">
                        <h3 class="kt-subheader__title kt-font-boldest kt-font-transform-u">
                            Start Date
                        </h3>
                        <div class="kt-input-icon  kt-input-icon--left kt-subheader__search" style="overflow:visible;">
                            <input type="text" id="accounting_start_month" class="form-control kt_datepicker" placeholder="START DATE" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>


                        </div>
                        <div id="accounting_start_month_modal" style="position: absolute;left: 165px;top: 50px;"></div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-2">
                        <h3 class="kt-subheader__title kt-font-boldest kt-font-transform-u">
                            End Date
                        </h3>
                        <div class="kt-input-icon  kt-input-icon--left kt-subheader__search" id="accounting_chart_ends_month_parent" style="overflow:visible;">
                            <input type="text" id="accounting_end_month" class="form-control kt_datepicker" placeholder="END DATE" readonly>
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>

                        </div>
                        <div id="accounting_end_month_modal" style="position:absolute;right:135px;"></div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-2">
                        <button type="button" id="accounting_reset_date" class="btn btn-danger btn-elevate" style="position: absolute;margin-top: 15px;"><i class="la la-trash"></i> Reset Date</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>