<div class="row">

    <div class="col-md-27 col-lg-27 col-xl-9" id='yearly_sales_chart_portlet'>
        <div class="kt-portlet kt-portlet--solid-light" style="background-color: #29D3B1">
            <div class="kt-portlet__head" style="margin-top:20px;">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title">
                        <h1 class="kt-font-light">TOTAL SALES OF THE YEAR</h1>
                        <h5 id="yearly_date_range" class="kt-font-light"></h5>
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar" style="margin-top:-20px;">
                <a  href="<?php echo base_url() . "dashboard/sales_report" ?>" class="btn btn-light kt-font-dark">View Full Report</a>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
            <a href='javascript:void(0);' id="total_yearly_sales_amount" class="kt-font-boldest kt-font-light display-4" style="text-align: right;padding-right:25px;"></a>
                <canvas id="yearly_sales_chart" style="display: flex; min-height: 600px; max-height: 600px; padding: 0px; margin: 0px;"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">

        <div class="kt-portlet kt-portlet--solid-light kt-portlet--height-fluid-half">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title text-muted">
                        Login History
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="260" data-scrollbar-shown="true" style="height: 260px; overflow: hidden;" style="padding-left:50px;">
                    <div class="kt-notification-v2" id="dashboard_login_history"> </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet kt-portlet--solid-light kt-portlet--height-fluid-half">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title text-muted">
                        Audit Trail
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="260" data-scrollbar-shown="true" style="height: 260px; overflow: hidden;" style="padding-left:50px;">
                    <div class="kt-notification-v2" id="dashboard_audit_trail"> </div>
                </div>
            </div>
        </div>
    </div>

</div>