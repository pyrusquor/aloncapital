<div class="row">

    <div class="col-md-36 col-lg-36 col-xl-12">
        <div class="kt-portlet kt-portlet--head-noborder">
            <div class="kt-portlet__body kt-portlet__body--fit">

                <div class="row" style="margin-left:10px;">
                    <div class="col-md-9 col-lg-9 col-xl-3">
                        <h3 class="kt-subheader__title kt-font-boldest kt-font-transform-u">
                            Project
                        </h3>
                        <div style="width:300px;padding-bottom:20px;">
                            <select class="form-control suggests" data-module="projects" id="sales_project_id">
                                <option value="">ALL PROJECTS</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>