<div class=row>
    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id="sales_month_total_portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget20">
                    <div class="kt-widget20__content kt-portlet__space-x">
                    <a href='javascript:void(0);' id='sales_month_total' style="text-align:right;padding-top:-20px;" class="kt-widget20__number kt-font-dark">₱ 0.00</a>
                        <h6 class="text-muted" style="text-align:right;padding-top:10px;">Sales for this month</h6>
                    </div>
                    <div class="kt-widget20__chart" style="height: 130px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="total_sales_month_chart" width="516" height="130" style="display: block; width: 516px; height: 130px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id='total_collections_month_portlet'>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget20">
                    <div class="kt-widget20__content kt-portlet__space-x">
                    <a href='javascript:void(0);'  id='total_collections_month' style="text-align:right;padding-top:-20px;" class="kt-widget20__number kt-font-dark">₱ 0.00</a>
                        <h6 class="text-muted" style="text-align:right;padding-top:10px;">Collections for this month</h6>
                    </div>
                    <div class="kt-widget20__chart" style="height: 130px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="total_collections_month_chart" width="516" height="130" style="display: block; width: 516px; height: 130px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id='property_sales_month_portlet'>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget20">
                    <div class="kt-widget20__content kt-portlet__space-x">
                        <a href='javascript:void(0);' id='property_sales_month' style="text-align:right;padding-top:-20px;" class="kt-widget20__number kt-font-dark"><span id="total_properties_month"></span> property(s)</a>
                        <h6 style="text-align:right;padding-top:10px;" class="text-muted">Properties sold for this month</h6>
                    </div>
                    <div class="kt-widget20__chart" style="height: 130px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="properties_month_chart" width="516" height="130" style="display: block; width: 516px; height: 130px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id='clients_month_portlet'>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget20">
                    <div class="kt-widget20__content kt-portlet__space-x">
                        <span style="text-align:right;padding-top:-20px;" class="kt-widget20__number kt-font-dark"><span id="total_clients_month"></span> client(s)</span>
                        <h6 style="text-align:right;padding-top:10px;" class="text-muted">New client(s) for this month</h6>
                    </div>
                    <div class="kt-widget20__chart" style="height: 130px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="clients_month_chart" width="516" height="130" style="display: block; width: 516px; height: 130px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>