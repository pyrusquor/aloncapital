<?php if ($top) :
    $rank = 1;
    foreach ($top as $key => $data) : ?>
        <tr>
            <th scope="row" style="vertical-align: middle;">
                <?php echo $rank++; ?>
            </th>
            <td style="vertical-align: middle;">
            <a href='javascript:void(0);' class='top-supplier-item kt-font-bold' data-name='<?= $data['name']?>' data-target='purchase_order' data-status='2' data-id='<?= $data['id'] ?>'><?php echo $data['name']; ?>
            </td>
            <td>
                <?php echo $data['location']; ?>
            </td>
            <td>
                <?php if($type=='count'): ?>
                    <?php echo $data['stat_count']; ?>
                <?php else: ?>
                    <?php echo money_php($data['stat_count']); ?>
                <?php endif ?>
            </td>
        </tr>
    <?php endforeach ?>
<?php endif ?>