<div class=row>
    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--bordered" id='po_this_month_amount_portlet'>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <a href="javascript:void(0)" data-target='purchase_order' data-status='0' class='purchases-click' data-type='month'><h3 class="kt-portlet__head-title">
                        Total PO for this month
                    </h3></a>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <span id="po_this_month" class="kt-widget1__desc"></span>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget1">
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total Amount</h3>
                            <span id="po_this_month_amount" class="kt-widget1__number kt-font-brand"></span>
                        </div>
                    </div>
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total No. of PO</h3>
                            <span id="po_this_month_count" class="kt-widget1__number kt-font-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--bordered" id='new_po_this_month_amount_portlet'>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                <a href="javascript:void(0)" data-target='purchase_order' data-status='1' class='purchases-click' data-type='month'><h3 class="kt-portlet__head-title">
                        New PO for this month
                    </h3></a>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <span id="new_po_this_month" class="kt-widget1__desc"></span>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget1">
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total Amount</h3>
                            <span id="new_po_this_month_amount" class="kt-widget1__number kt-font-brand"></span>
                        </div>
                    </div>
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total No. of Requests</h3>
                            <span id="new_po_this_month_count" class="kt-widget1__number kt-font-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--bordered" id='app_po_this_month_amount_portlet'>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                <a href="javascript:void(0)" data-target='purchase_order' data-status='2' class='purchases-click' data-type='month'><h3 class="kt-portlet__head-title">
                        Approved PO for this month
                    </h3></a>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <span id="app_po_this_month" class="kt-widget1__desc"></span>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget1">
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total Amount</h3>
                            <span id="app_po_this_month_amount" class="kt-widget1__number kt-font-brand"></span>
                        </div>
                    </div>
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total No. of Requests</h3>
                            <span id="app_po_this_month_count" class="kt-widget1__number kt-font-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>