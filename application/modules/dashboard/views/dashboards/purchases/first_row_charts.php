<div class="row">

    <div class="col-md-36 col-lg-36 col-xl-12">
        <div class="kt-portlet kt-portlet--head-noborder" id='po_bar_chart_portlet'>
            <div class="kt-portlet__head kt-portlet__head--fuid">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title">
                        <a id='monthly_po_click' href="javascript:void(0);" data-target='purchase_order' data-status='2' class='purchases-click' data-type='specific'><h2 class="kt-font-dark" style='margin-top:25px;'>MONTHLY PURCHASE ORDERS <span id="po_range" class="text-muted"></span></h2></a>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--space-x">
                <canvas id="po_bar_chart" style="display: flex; min-height: 450px; max-height: 450px; padding: 0px; margin: 0px;" class="chartjs-render-monitor"></canvas>
            </div>
        </div>
    </div>
</div>