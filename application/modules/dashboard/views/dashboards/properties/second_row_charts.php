<div class="row">

    <div class="col-md-36 col-lg-36 col-xl-12">
        <div class="kt-portlet kt-portlet--head-noborder" id='projects_bar_chart_portlet'>
            <div class="kt-portlet__head kt-portlet__head--fuid">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title">
                        <h2 class="kt-font-dark" style='margin-top:25px;'>PROJECT ANALYSIS <span id="project_analysis_range" class="text-muted"></span></h2>
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar" style='margin-top:25px;'>
                    <div class="dropdown" id="project_selector">
                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span id="selected_text2">ALL PROJECTS</span>
                        </button>
                        <div class="dropdown-menu" id="project_dropdown_container" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" data-id='1' data-project="hey1" href="javascript:void(0)">Action</a>
                            <a class="dropdown-item" data-id='2' data-project="hey2" href="javascript:void(0)">Another action</a>
                            <a class="dropdown-item" data-id='3' data-project="hey3" href="javascript:void(0)">Something else here</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--space-x">
                <canvas id="projects_bar_chart" style="display: flex; min-height: 450px; max-height: 450px; padding: 0px; margin: 0px;" class="chartjs-render-monitor"></canvas>
            </div>
            <div class="kt-portlet__foot kt-portlet__foot--fluid">

           <a href="javascript:void(0);" id='properties_click' class='kt-font-dark'><center><h3 id="selected_text1">ALL PROJECTS</h3></center></a>

            </div>
            <input type='hidden' id='properties_selected_project_name'>
            <input type='hidden' id='properties_selected_project_id'>
            <input type='hidden' id='properties_selected_date_range'>
        </div>
    </div>
</div>