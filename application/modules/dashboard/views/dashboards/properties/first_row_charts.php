<div class="row">

    <div class="col-md-36 col-lg-36 col-xl-12">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id='inventory_report_portlet'>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title">
                        <h2 class="kt-font-dark" style='margin-top:25px;'>Inventory Report</h2>
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar" style='margin-top:25px;'>
                </div>
            </div>
            <div class="kt-portlet__body">
            <table class="table">
                        <colgroup>
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 5%;">
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 14%;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>Project</th>
                                <th>Total</th>
                                <th>Available</th>
                                <th>Reserved</th>
                                <th>Sold</th>
                                <th>Hold</th>
                                <th>Not for sale</th>
                            </tr>
                        </thead>
            </table>
            <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="600" data-scrollbar-shown="true" style="height: 600px; overflow: hidden;">
                    <table class="table">
                        <colgroup>
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 5%;">
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 14%;">
                            <col span="1" style="width: 14%;">
                        </colgroup>

                        <tbody id="inventory_report">

                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>