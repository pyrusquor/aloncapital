<?php if ($projects): ?>
        <a class="dropdown-item" data-id='0' data-project="ALL PROJECTS" href="javascript:void(0)">ALL PROJECTS</a>
        <div class="dropdown-divider"></div>
    <?php foreach ($projects as $project): ?>

        <a class="dropdown-item" data-id='<?php echo $project['id'] ?>' data-project="<?php echo $project['name'] ?>" href="javascript:void(0)"><?php echo $project['name'] ?></a>

    <?php endforeach ?>
<?php endif ?>