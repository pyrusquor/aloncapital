<div class="row">
    <div class="col-xl-8 col-lg-12 order-lg-3 order-xl-1">
        <div class="kt-portlet kt-portlet--height-mobile" id="best_sellers_general_portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Best Sellers
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                </div>
            </div>
            <div class="kt-portlet__body" style="height: 390px;">
                <div class="kt-widget5" id="top_three_sellers_table">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">
        <!--begin:: Widgets/New Users-->
        <div class="kt-portlet" id="three_latest_buyers_portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        New Buyers
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                </div>
            </div>
            <div class="kt-portlet__body" style="height: 390px;">
            <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="390" data-scrollbar-shown="true" style="height: 390px; overflow: hidden;">

                <div class="kt-widget4" id="three_latest_buyers">
                </div>
            </div>
            </div>
        </div>
    </div>
</div>