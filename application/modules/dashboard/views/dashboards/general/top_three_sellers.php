<?php if ($top) :
    $rank = 1;
    foreach ($top as $key => $data) : ?>

        <div class="kt-widget5__item">
            <div class="kt-widget5__content">
                <div class="kt-widget5__pic">
                    <?= $rank++ ?>
                </div>
                <div class="kt-widget5__section">
                    <a href="javascript:void(0)" class="kt-widget5__title top-three-seller-item" data-name='<?= $data['last_name'] . ' ' . $data['first_name'] ?>' data-id='<?= $data['id'] ?>'>
                        <?php echo $data['first_name'] . ' ' . $data['last_name']; ?>
                    </a>
                    <p class="kt-widget5__desc">
                        <?php echo $data['sp_name']; ?>
                    </p>
                    <div class="kt-widget5__info">
                        <span>GROUP:</span>
                        <span class="kt-font-info"><?php echo Dropdown::get_static('group_type', $data['sales_group_id'], 'view'); ?></span>
                    </div>
                </div>
            </div>
            <div class="kt-widget5__content">
                <div class="kt-widget5__stats">
                    <span class="kt-widget5__number"><?php echo $data['t_count']; ?></span>
                    <span class="kt-widget5__votes">Properties</span>
                </div>
                <div class="kt-widget5__stats">
                    <span class="kt-widget5__number"><?php echo money_php($data['total_selling_price']); ?></span>
                    <span class="kt-widget5__sales">Price</span>
                </div>

            </div>
        </div>
    <?php endforeach ?>
<?php endif ?>