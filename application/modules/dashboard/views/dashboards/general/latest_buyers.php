<?php if ($buyers) :
    foreach ($buyers as $key => $buyer) :
        $project_object = get_person($buyer['project_id'], 'projects');
        $project_name = get_name($project_object) ? get_name($project_object) : '';
        $buyer_object = get_person($buyer['buyer_id'], 'buyers');
        $buyer_name = get_fname($buyer_object) ? get_fname($buyer_object) : '';
?>
        <div class="kt-widget4__item">
            <div class="kt-widget4__pic kt-widget4__pic--pic">
                <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                    <?php echo get_initials($buyer_name); ?>
                </div>
            </div>
            <div class="kt-widget4__info">
                <a href="#" class="kt-widget4__username">
                    <?= $buyer_name ?>
                </a>
                
                <p class="kt-widget4__text">
                    <?= $project_name ?>
                </p>
            </div>
            <a href="<?php echo base_url() . 'transaction/view/' .$buyer['id'] ?>" class="btn btn-sm btn-label-success btn-bold">VIEW TRANSACTION</a>
        </div>
    <?php endforeach ?>
<?php endif ?>