<?php if ($raw_activities) :
    $date_watcher = '';
    foreach ($raw_activities as $key => $activity) :
        if ($date_watcher != date('F d, Y', strtotime($activity['date']))) : ?>
            <?php $date_watcher = date('F d, Y', strtotime($activity['date'])) ?>
            <div class="kt-timeline-v2__item">
                <span class="kt-timeline-v2__item-time"></span>

                <div class="kt-timeline-v2__item-cricle">
                    <i class="fa fa-genderless kt-font-danger"></i>
                </div>

                <div class="kt-timeline-v2__item-text kt-padding-top-5">
                <h5><a href="#" class="kt-link kt-link--danger kt-font-boldest kt-font-transform-u"><?= $date_watcher  ?></a></h5>

                </div>
            </div>
        <?php endif ?>
        <div class="kt-timeline-v2__item">
            <span class="kt-timeline-v2__item-time"><?= date('G:i', strtotime($activity['date'])) ?></span>

            <div class="kt-timeline-v2__item-cricle">
                <?php if ($activity['type']=='login') : ?>
                    <i class="fa fa-genderless kt-font-success"></i>
                <?php else : ?>
                    <i class="fa fa-genderless kt-font-info"></i>
                <?php endif ?>
            </div>

            <div class="kt-timeline-v2__item-text kt-padding-top-5">
                <?php if ($activity['type']=='login') : ?>
                    <a href="javascript:void(0);" class="kt-link kt-link--success kt-font-bolder"><?= $activity['name'] ?></a> logged in from <a target="_BLANK" href="https://whatismyipaddress.com/ip/<?=$activity['detail'] ?>" class="kt-link kt-link--warning kt-font-bolder"> <?= $activity['detail'] ?> </a>
                <?php else : ?>
                    <a href="javascript:void(0);" class="kt-link kt-link--brand kt-font-bolder"><?= $activity['detail'] ?></a>&nbsp;<?= $activity['detail_2'] ?> by <?= $activity['name'] ?>
                <?php endif ?>


            </div>
        </div>

    <?php endforeach ?>
<?php endif ?>