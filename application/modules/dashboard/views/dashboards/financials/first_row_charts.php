<div class=row>
    <div class="col-md-9 col-lg-9 col-xl-3" id='collect_today_amount_portlet'>
        <div class="kt-portlet kt-portlet--bordered">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <a href='javascript:void(0);' id="financials_collections_today_click">  <h3 class="kt-portlet__head-title">
                        Collections for Today
                    </h3></a>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <span id="collect_today_date" class="kt-widget1__desc"></span>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget1">
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total Amount</h3>
                            <span id="collect_today_amount" class="kt-widget1__number kt-font-brand"></span>
                        </div>
                    </div>
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total No. of Collections</h3>
                            <span id="collect_today_count" class="kt-widget1__number kt-font-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--bordered" id='collect_past_due_amount_portlet'>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Past Due Collections
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget1">
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total Amount</h3>
                            <span id="collect_past_due_amount" class="kt-widget1__number kt-font-brand"></span>
                        </div>
                    </div>
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total No. of Collections</h3>
                            <span id="collect_past_due_count" class="kt-widget1__number kt-font-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--bordered" id='pdc_today_amount_portlet'>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                <a href='javascript:void(0);' id="financials_pdc_today_click">  <h3 class="kt-portlet__head-title">
                        PDC for Clearing Today
                    </h3> </a>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <span id="pdc_today_date" class="kt-widget1__desc"></span>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget1">
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total Amount</h3>
                            <span id="pdc_today_amount" class="kt-widget1__number kt-font-brand"></span>
                        </div>
                    </div>
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total No. of Cheques</h3>
                            <span id="pdc_today_count" class="kt-widget1__number kt-font-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--bordered" id='pdc_past_due_amount_portlet'>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Past Due PDC
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget1">
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total Amount</h3>
                            <span id="pdc_past_due_amount" class="kt-widget1__number kt-font-brand"></span>
                        </div>
                    </div>
                    <div class="kt-widget1__item">
                        <div class="kt-widget1__info">
                            <h3 class="kt-widget1__title">Total No. of Cheques</h3>
                            <span id="pdc_past_due_count" class="kt-widget1__number kt-font-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>