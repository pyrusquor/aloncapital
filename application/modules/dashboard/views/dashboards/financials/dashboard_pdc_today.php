<?php if ($today_pdc && count($today_pdc)>0) :
    foreach ($today_pdc as $key => $value) :?>
        <a href="javascript: void(0)" class="kt-notification-v2__item">
            <div class="kt-notification-v2__item-icon kt-font-success">
                <i class="fa fa-money-check-alt"></i>
            </div>
            <div class="kt-notification-v2__itek-wrapper">
                <div class="kt-notification-v2__item-title">
                    <?= money_php($value['amount']); ?><br>
                    
                </div>
                <div class="kt-notification-v2__item-desc">
                <?= $value['bank_name']; ?>&nbsp;&nbsp;-&nbsp;&nbsp;<span class="text-muted"><?= $value['unique_number']; ?></span>
                </div>
            </div>
        </a>
    <?php endforeach ?>
<?php else: ?>
    <a href="javascript: void(0)" class="kt-notification-v2__item">
            <div class="kt-notification-v2__item-icon kt-font-success">
                <i class="fa fa-check"></i>
            </div>
            <div class="kt-notification-v2__itek-wrapper">
            <h3 class="kt-font-success">NO PDC FOR CLEARING TODAY!</h3>
            </div>
        </a>
<?php endif ?>