<?php if ($today_collections) :
    foreach ($today_collections as $key => $value) :
        $fullname = $value['first_name'] . ' ' . $value['last_name']; ?>
        <a href="<?php echo base_url() . "transaction_payment/form/" . $value['transactions_id'] ?>" class="kt-notification-v2__item">
            <div class="kt-notification-v2__itek-wrapper">
                <div class="kt-notification-v2__item-title">
                    <?= $value['reference']; ?>
                </div>
                <div class="kt-notification-v2__item-desc">
                    <span class="text-muted"><?= $fullname; ?></span>
                </div>
            </div>
            <div class="kt-notification__item-icon kt-font-success">
                <button class="btn btn-success" type="button">Collect <?= money_php($value['total_amount']); ?></button>
            </div>
        </a>
    <?php endforeach ?>
<?php else : ?>
    <a href="javascript: void(0)" class="kt-notification-v2__item">
        <div class="kt-notification-v2__item-icon kt-font-success">
            <i class="fa fa-check"></i>
        </div>
        <div class="kt-notification-v2__itek-wrapper">
            <h3 class="kt-font-success">NO UNPAID COLLECTIONS TODAY!</h3>
        </div>
    </a>
<?php endif ?>