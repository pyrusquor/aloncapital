<div class="row">

    <div class="col-md-27 col-lg-27 col-xl-9">
        <div class="kt-portlet kt-portlet--solid-light" style="background-color:#561571" id='yearly_c_r_chart_portlet'>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title">
                        <h2 class="kt-font-light" style='margin-top:25px;'>RECEIVABLE AND COLLECTION REPORT</h2>
                        <h4 id="yearly_c_r_range" class="kt-font-light"></h4>
                        </div>
                    </div>
                    <div class="kt-portlet__head-toolbar" style='margin-top:20px;'>
                    <a href="<?php echo base_url() . "dashboard/financials_graph" ?>" class="btn btn-light "><span class="kt-font-dark">View Full Report</span></a>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit">

                    <canvas id="yearly_c_r_chart" style="display: block; min-height: 660px; max-height: 660px;" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
        </div>


        <div class="col-md-9 col-lg-9 col-xl-3">

            <div class="kt-portlet kt-portlet--solid-light kt-portlet--height-fluid-half" id='dashboard_for_collection_today_portlet'>
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <div class="kt-portlet__head-title text-muted">
                            For Collection Today
                        </div>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-icon btn-sm btn-icon-md " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon2-down-arrow"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-149px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <a class="dropdown-item" href="<?php echo base_url() . 'alerts/payments' ?>"><i class="flaticon-eye"></i> See All</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="260" data-scrollbar-shown="true" style="height: 260px; overflow: hidden;" style="padding-left:50px;">
                        <div class="kt-notification-v2" id="dashboard_for_collection_today"> </div>
                        <div class="ps__rail-x" style="left: 0px; bottom: -28px;">
                            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                        </div>
                        <div class="ps__rail-y" style="top: 28px; height: 200px; right: 0px;">
                            <div class="ps__thumb-y" tabindex="0" style="top: 25px; height: 175px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet kt-portlet--solid-light kt-portlet--height-fluid-half" id='for_pdc_clearing_today_portlet'>
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <div class="kt-portlet__head-title text-muted">
                            For PDC Clearing Today
                        </div>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-icon btn-sm btn-icon-md " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon2-down-arrow"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-149px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <a class="dropdown-item" href="<?php echo base_url() . 'alerts/post_dated_checks' ?>"><i class="flaticon-eye"></i> See All</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="260" data-scrollbar-shown="true" style="height: 260px; overflow: hidden;" style="padding-left:50px;">
                        <div class="kt-notification-v2" id="for_pdc_clearing_today"> </div>
                        <div class="ps__rail-x" style="left: 0px; bottom: -28px;">
                            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                        </div>
                        <div class="ps__rail-y" style="top: 28px; height: 200px; right: 0px;">
                            <div class="ps__thumb-y" tabindex="0" style="top: 25px; height: 175px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>