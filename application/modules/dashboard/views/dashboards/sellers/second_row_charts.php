<div class=row>
    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id='total_commissions_month_chart_portlet'>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget20">
                    <div class="kt-widget20__content kt-portlet__space-x">
                        <span id='total_commissions_month' style="text-align:right;padding-top:-20px;" class="kt-widget20__number kt-font-dark">₱ 0.00</span>
                        <h6 class="text-muted" style="text-align:right;padding-top:10px;">Total Commissions for this month</h6>
                    </div>
                    <div class="kt-widget20__chart" style="height: 130px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="total_commissions_month_chart" width="516" height="130" style="display: block; width: 516px; height: 130px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id='pending_commissions_month_chart_portlet'>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget20">
                    <div class="kt-widget20__content kt-portlet__space-x">
                        <span id='pending_commissions_month' style="text-align:right;padding-top:-20px;" class="kt-widget20__number kt-font-">₱ 0.00</span>
                        <h6 class="text-muted" style="text-align:right;padding-top:10px;">Pending Commissions for this month</h6>
                    </div>
                    <div class="kt-widget20__chart" style="height: 130px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="pending_commissions_month_chart" width="516" height="130" style="display: block; width: 516px; height: 130px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id='processing_commissions_month_chart_portlet'>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget20">
                    <div class="kt-widget20__content kt-portlet__space-x">
                    <span id='processing_commissions_month' style="text-align:right;padding-top:-20px;" class="kt-widget20__number kt-font-">₱ 0.00</span>
                        <h6 style="text-align:right;padding-top:10px;" class="text-muted">Processing Commissions for this month</h6>
                    </div>
                    <div class="kt-widget20__chart" style="height: 130px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="processing_commissions_month_chart" width="516" height="130" style="display: block; width: 516px; height: 130px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid" id='released_commissions_month_chart_portlet'>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-widget20">
                    <div class="kt-widget20__content kt-portlet__space-x">
                    <span id='released_commissions_month' style="text-align:right;padding-top:-20px;" class="kt-widget20__number kt-font-">₱ 0.00</span>
                        <h6 style="text-align:right;padding-top:10px;" class="text-muted">Released Commissions for this month</h6>
                    </div>
                    <div class="kt-widget20__chart" style="height: 130px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="released_commissions_month_chart" width="516" height="130" style="display: block; width: 516px; height: 130px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>