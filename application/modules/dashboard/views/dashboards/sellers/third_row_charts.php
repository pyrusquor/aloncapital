<div class="row">

    <div class="col-md-36 col-lg-36 col-xl-12">
        <div class="kt-portlet kt-portlet--head-noborder" id='top_sellers_table_portlet'>
            <div class="kt-portlet__head kt-portlet__head--fuid">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title">
                        <h2 class="kt-font-dark" style='margin-top:25px;'>TOP SELLERS<span class="text-muted">&nbsp; All Projects(Consolidated)</span> </h2>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--space-x">
                <table class="table">
                    <colgroup>
                        <col span="1" style="width: 5%;">
                        <col span="1" style="width: 16%;">
                        <col span="1" style="width: 16%;">
                        <col span="1" style="width: 16%;">
                        <col span="1" style="width: 16%;">
                        <col span="1" style="width: 9%;">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Seller Name</th>
                            <th>Group</th>
                            <th>Position</th>
                            <th>Total Selling Price</th>
                            <th>Total Property Count</th>
                        </tr>
                    </thead>
                </table>
                <div class="kt-scroll ps ps--active-y" data-scroll="true" data-height="600" data-scrollbar-shown="true" style="height: 600px; overflow: hidden;">
                    <table class="table">
                        <colgroup>
                            <col span="1" style="width: 5%;">
                            <col span="1" style="width: 16%;">
                            <col span="1" style="width: 16%;">
                            <col span="1" style="width: 16%;">
                            <col span="1" style="width: 16%;">
                            <col span="1" style="width: 9%;">
                        </colgroup>

                        <tbody id="top_sellers_table">

                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>