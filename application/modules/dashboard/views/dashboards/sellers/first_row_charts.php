<div class="row">

    <div class="col-md-36 col-lg-36 col-xl-12">
        <div class="kt-portlet kt-portlet--head-noborder" id='commissions_bar_chart_portlet'>
            <div class="kt-portlet__head kt-portlet__head--fuid">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-title">
                        <h2 class="kt-font-dark" style='margin-top:25px;'>MONTHLY COMMISSIONS <span id="commissions_range" class="text-muted"></span></h2>
                    </div>

                </div>
                <div class="kt-portlet__head-toolbar">
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--space-x">
                <canvas id="commissions_bar_chart" style="display: flex; min-height: 450px; max-height: 450px; padding: 0px; margin: 0px;" class="chartjs-render-monitor"></canvas>
            </div>
        </div>
    </div>
</div>