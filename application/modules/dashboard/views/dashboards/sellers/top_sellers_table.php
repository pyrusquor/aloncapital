<?php if ($top) :
    $rank = 1;
    foreach ($top as $key => $data) : ?>
        <tr>
            <th scope="row" style="vertical-align: middle;">
                <?php echo $rank++; ?>
            </th>
            <td style="vertical-align: middle;">
                <a href='javascript:void(0);' class='top-seller-item kt-font-bold' data-name='<?= $data['last_name'] . ' ' . $data['first_name'] ?>' data-id='<?= $data['id'] ?>'><?php echo $data['first_name'] . ' ' . $data['last_name']; ?> </a>
            </td>
            <td>
                <?php echo Dropdown::get_static('group_type',$data['sales_group_id'],'view'); ?>
            </td>
            <td>
                <?php echo $data['sp_name']; ?>
            </td>
            <td>
                <?php echo money_php($data['total_selling_price']); ?>
            </td>
            <td>
                <?php echo $data['t_count']; ?>
            </td>
        </tr>
    <?php endforeach ?>
<?php endif ?>