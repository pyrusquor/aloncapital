<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main" >
            <h3 class="kt-subheader__title">
                Dashboard </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Features </a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Dashboard </a>
            </div>
        </div>


        <div class="kt-subheader__toolbar">
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            &nbsp;&nbsp;
            <!-- Sales Filter -->

            <!-- Financials Filter -->

            <ul class="nav nav-pills nav-fill" role="tablist" id="dashboard_nav">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" data-target="#general" role="tab">
                        <i class="fas fa-tablet-alt"></i>
                        General
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-target="#sales" role="tab">
                        <i class="fas fa-money-bill"></i>
                        Sales
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-target="#financials" role="tab">
                        <i class="fas fa-chart-bar"></i>
                        Financials
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-target="#properties" role="tab">
                        <i class="fas fa-box"></i>
                        Properties
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-target="#sellers" role="tab">
                        <i class="fas fa-hand-holding-usd"></i>
                        Seller
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-target="#warehouses" role="tab">
                        <i class="fas fa-warehouse"></i>
                        Warehouse
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-target="#purchases" role="tab">
                        <i class="fas fa-wallet"></i>
                        Purchase
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" data-target="#accounting" role="tab">
                        <i class="fas fa-paperclip"></i>
                        Accounting
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php

$CI = &get_instance();
$CI->load->model('menu/Menu_model', 'menu');
$CI->load->model('auth/Ion_auth_model', 'auth');

$parent_page = $CI->uri->segment(1);
$current_page = $CI->uri->segment(1) . (($CI->uri->segment(2)) ? '/' . $CI->uri->segment(2) : '');
$group = json_decode(json_encode($CI->auth->get_users_groups()->result()[0]), true);

$submenu = [];
if ($module) {
    $module_link = "dashboard/index/" . $module;
    $mod_id = get_value_field($module_link, 'modules', 'id', 'link');
    $submenu = $CI->menu->load_submenu($mod_id);
} else {
    $submenu = $CI->menu->load_menu();
}

// vdebug($submenu);

?>



<div class="tab-content">
    <div class="tab-pane fade show active" id="general" role="tabpanel">
        <?php $this->load->view('dashboards/general/first_row_charts'); ?>
        <?php $this->load->view('dashboards/general/second_row_charts'); ?>
    </div>
    <div class="tab-pane fade" id="sales" role="tabpanel">
        <?php $this->load->view('dashboards/sales/top_filter'); ?>
        <?php $this->load->view('dashboards/sales/first_row_charts'); ?>
        <?php $this->load->view('dashboards/sales/second_row_charts'); ?>
    </div>
    <div class="tab-pane fade" id="financials" role="tabpanel">
        <?php $this->load->view('dashboards/financials/top_filter'); ?>
        <?php $this->load->view('dashboards/financials/first_row_charts'); ?>
        <?php $this->load->view('dashboards/financials/second_row_charts'); ?>
    </div>
    <div class="tab-pane fade" id="properties" role="tabpanel">
        <?php $this->load->view('dashboards/properties/first_row_charts'); ?>
        <?php $this->load->view('dashboards/properties/bottom_filter'); ?>
        <?php $this->load->view('dashboards/properties/second_row_charts'); ?>
    </div>
    <div class="tab-pane fade" id="sellers" role="tabpanel">

        <?php $this->load->view('dashboards/sellers/top_filter'); ?>
        <?php $this->load->view('dashboards/sellers/first_row_charts'); ?>
        <?php $this->load->view('dashboards/sellers/second_row_charts'); ?>
        <?php $this->load->view('dashboards/sellers/third_row_charts'); ?>
    </div>
    <div class="tab-pane fade" id="warehouses" role="tabpanel">
    </div>
    <div class="tab-pane fade" id="purchases" role="tabpanel">
        <?php $this->load->view('dashboards/purchases/top_filter'); ?>
        <?php $this->load->view('dashboards/purchases/first_row_charts'); ?>
        <?php $this->load->view('dashboards/purchases/second_row_charts'); ?>
        <?php $this->load->view('dashboards/purchases/third_row_charts'); ?>
    </div>
    <div class="tab-pane fade" id="accounting" role="tabpanel">
        <?php $this->load->view('dashboards/accounting/top_filter'); ?>
        <?php $this->load->view('dashboards/accounting/first_row_charts'); ?>
        <?php $this->load->view('dashboards/accounting/second_row_charts'); ?>
        <?php $this->load->view('dashboards/accounting/third_row_charts'); ?>
    </div>
</div>


<style type="text/css">
    .dropdown-menu {
        left: 314px !important;
    }

    #dashboard_nav .rems-ico {
        font-size: 70px !important;
    }
</style>