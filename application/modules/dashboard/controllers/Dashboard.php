<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('transaction/Transaction_model', 'M_Transaction');
        $this->load->model('transaction_commission/Transaction_commission_model', 'M_Transaction_commission');
        $this->load->model('transaction/Transaction_property_model', 'M_Transaction_property');
        $this->load->model('Sales_demography_model', 'M_sales_demography');
        $this->load->model('Sales_demography_model', 'M_sales_demography');
        $this->load->model('project/Project_model', 'M_project');
        $this->load->model('house/House_model', 'M_house_model');
        $this->load->model('position/Seller_position_model', 'M_position');
        $this->load->model('standard_report/Standard_report_model', 'M_report');
        $this->load->model('locations/Address_countries_model', 'M_country');
        $this->load->model('locations/Address_regions_model', 'M_region');
        $this->load->model('locations/Address_provinces_model', 'M_province');
        $this->load->model('locations/Address_city_municipalities_model', 'M_city');
        $this->load->model('locations/Address_barangays_model', 'M_barangay');
        $this->load->model('communication/Communication_group_model', 'M_comm_group');
    }

    public function index($module = FALSE)
    {
        // overwrite default theme and layout if needed
        $this->template->set_theme('default');
        $this->template->set_layout('default');

        $data['module'] = $module;

        // vdebug($data['module']);

        if ($this->ion_auth->is_admin()) {
            $this->template->build('main', $data);
        } else if ($this->ion_auth->is_seller()) {
            redirect('loan');
        } else if ($this->ion_auth->is_buyer()) {
            redirect('loan');
        } else {
            redirect('login', 'refresh');
        }
    }


    public function main($module = FALSE)
    {
        // overwrite default theme and layout if needed
        $this->template->set_theme('default');
        $this->template->set_layout('default');

        $data['module'] = $module;

        // vdebug($data['module']);
        $this->template->build('dashboard', $data);
    }

    public function sales_report()
    {

        $this->js_loader->queue(['vendors/custom/highcharts/code/highcharts.js']);

        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/offline-exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/export-data.js']);

        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/series-label.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/accessibility.js']);
        $this->js_loader->queue(['modules/dashboard/hc-themes.js']);

        if (isset($_GET['date_option_range']) && !empty($_GET['date_option_range'])) {

            $g_dates = $this->get_dates(array('date_option_range' => $_GET['date_option_range'], 'daterange' => $_GET['daterange']));

            $months = $g_dates['months'];
            $month_opt = $g_dates['month_opt'];
            $date_from = $g_dates['date_from'];
            $date_to = $g_dates['date_to'];

            $daterange = $_GET['daterange'];
        } else {

            $date = NOW;
            $date_from = date("Y/m/d", strtotime($date . " -1 year"));
            $date_to  = date("Y/m/d", strtotime($date));

            $date_option_range = "Y-m";

            $period = $this->M_report->get_periods($date_from, $date_to);

            $months = array();
            $month_opt = "year_month";

            foreach ($period as $key => $dt) {
                $months[] = $dt->format("Y-m");
            }

            $daterange = view_date($date_from) . " - " . view_date($date_to);
        }

        $types = [1, 2, 3, 4];


        $data['months'] = $months;
        $j = [];

        if ((isset($_GET['sub_type_id']) && !empty($_GET['sub_type_id'])) || (isset($_GET['project_id']) && !empty($_GET['project_id']))) {

            $type = $_GET['sub_type_id'];

            $projects = $this->M_report->get_projects(array('sub_type_id' => $_GET['sub_type_id'], 'project_id' => $_GET['project_id']));

            //per project
            foreach ($projects as $key => $project) {

                $rows = [];

                foreach ($months as $key2 => $month) {
                    // if(!$key){continue;}
                    $params[$month_opt] = $month;
                    $params['project_id'] = $project['id'];
                    $params['sub_type_id'] = $type;
                    $data['rows'][$project['id']][$month] = $this->M_report->sales_report($params);

                    $rows[] =  ($data['rows'][$project['id']][$month]['0']['total_selling_price'] ? (float) $data['rows'][$project['id']][$month]['0']['total_selling_price'] : (float) 0);
                }

                $j[$key]['name'] = $project['name'];
                $j[$key]['data'] = $rows;
            }
        } else {
            //per project
            foreach ($types as $key => $type) {

                $rows = [];
                foreach ($months as $key2 => $month) {
                    // if(!$key){continue;}
                    $params[$month_opt] = $month;
                    $params['sub_type_id'] = $type;
                    $data['rows'][$type][$month] = $this->M_report->sales_report($params);
                    $rows[] =  ($data['rows'][$type][$month]['0']['total_selling_price'] ? (float) $data['rows'][$type][$month]['0']['total_selling_price'] : (float) 0);
                }

                $j[$key]['name'] = Dropdown::get_static('sub_project_type', $type, 'view');
                $j[$key]['data'] = $rows;
            }
        }

        unset($params['project_id']);
        unset($params['sub_type_id']);

        $property_status = Dropdown::get_static('property_status');

        $series_1 = [];
        foreach ($property_status as $key => $p) {
            if (!$key) {
                continue;
            }
            $params['status'] = $key;
            $p_count = $this->M_report->get_property_demography($params);

            $s['name'] = $p;
            $s['y'] = (int)$p_count['count'];
            array_push($series_1, $s);
        }

        $series_2 = [];
        $transactions = $this->M_Transaction->as_array()->get_all();
        $t_count = 0;
        $f_count = 0;

        foreach ($transactions as $key => $t) {
            if (!$key) {
                continue;
            }
            $result = $this->M_report->get_recognized_sales($t['id']);

            if ($result) {
                $t_count += 1;
            } else {
                $f_count += 1;
            }
        }
        $s['name'] = 'Recognized Sales';
        $s['y'] = (int)$t_count;
        array_push($series_2, $s);

        $s['name'] = 'Unrecognized Sales';
        $s['y'] = (int)$f_count;
        array_push($series_2, $s);

        $this->view_data['title'] =  "Company Sales Report by Property Category";
        $this->view_data['title_1'] =  "Company Sales Report by Property Status";
        $this->view_data['title_2'] =  "Company Sales Report by Recognized Sales";

        $this->view_data['subtitle']  = "For the date range " . $daterange;
        $this->view_data['yAxisTitle'] = "Philippine Peso (Php)";
        $this->view_data['dates'] = json_encode($months);
        $this->view_data['json_data'] = json_encode($data);
        $this->view_data['types'] = $types;
        $this->view_data['months'] = $months;
        $this->view_data['projects'] = $this->M_project->get_all();
        $this->view_data['month_opt'] = $month_opt;

        $this->view_data['series'] = json_encode($j);
        $this->view_data['series_1'] = json_encode($series_1);
        $this->view_data['series_2'] = json_encode($series_2);
        // $this->output->enable_profiler(TRUE); 
        if (isset($_GET) && !empty($_GET)) {
            $return['html'] = $this->load->view('dashboard/subs/sales_report', $this->view_data, true);
            $return['data'] = $this->view_data;
            $return['daterange'] =  $daterange;
            echo json_encode($return);
        } else {
            $this->template->build('sales_report', $this->view_data);
        }
    }

    public function sales_demography()
    {
        $this->js_loader->queue(['vendors/custom/highcharts/code/highcharts.js']);

        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/offline-exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/export-data.js']);


        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/series-label.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/accessibility.js']);
        $this->js_loader->queue(['modules/dashboard/hc-themes.js']);


        if (isset($_GET) && !empty($_GET)) {

            if (isset($_GET['daterange']) && !empty($_GET['daterange'])) {

                $daterange = $_GET['daterange'];
            } else {

                $date = NOW;
                $date_from = date("Y/m/d", strtotime($date . " -1 year"));
                $date_to  = date("Y/m/d", strtotime($date));

                $daterange = view_date($date_from) . " - " . view_date($date_to);
            }

            if (isset($_GET['project_id']) && !empty($_GET['project_id'])) {

                $params['project_id'] = $_GET['project_id'];
            }

            $params['daterange'] = $daterange;

            $civil_status = Dropdown::get_static('civil_status');
            $housing_membership = Dropdown::get_static('housing_membership');
            $occupation_type = Dropdown::get_static('occupation_type');
            $industry_type = Dropdown::get_static('industry');
            $occupation = Dropdown::get_static('occupation');
            $source_referral = Dropdown::get_static('source_referral_type');
            $countries = $this->M_country->fields(array('id', 'name'))->get_all();
            $regions = $this->M_region->fields(array('regCode', 'regDesc'))->get_all();
            $provinces = $this->M_province->fields(array('provCode', 'provDesc'))->get_all();
            $cities = $this->M_city->fields(array('citymunCode', 'citymunDesc'))->get_all();
            $barangays = $this->M_barangay->fields(array('brgyCode', 'brgyDesc'))->get_all();

            $params['gender'] = true;
            $gender_count = $this->M_sales_demography->get_demographics($params);

            // vdebug($gender_count);

            $this->view_data['unaccounted_gender'] = 0;
            $this->view_data['male'] = 0;
            $this->view_data['female'] = 0;
            
            foreach ($gender_count as $key => $value) {
                if ($value['gender'] == 0) {

                    $this->view_data['unaccounted_gender'] = (int) $value['count'];
                } else if ($value['gender'] == 1) {
                    $this->view_data['male'] = (int) $value['count'];
                } else if ($value['gender'] == 2) {
                    $this->view_data['female'] = (int) $value['count'];
                }
            }

            unset($params['gender']);
            $series_2 = [];

            $params['civil_status'] = true;
            $civil_status_count = $this->M_sales_demography->get_demographics($params);
            foreach ($civil_status_count as $count) {

                if ($count['civil_status_id'] == 0) {
                    $s['name'] = 'Unaccounted';
                    $s['y'] = (int) $count['count'];
                    array_push($series_2, $s);
                } else {
                    $s['name'] = $civil_status[$count['civil_status_id']];
                    $s['y'] = (int) $count['count'];
                    array_push($series_2, $s);
                }
            }

            unset($params['civil_status']);
            $series_3 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];

            // Employment Type
            $params['e_type'] = true;
            $employment_count = $this->M_sales_demography->get_demographics($params);
            $unaccounted_occupation_count = 0;

            foreach ($employment_count as $count) {

                if (!$count['occupation_type_id']) {

                    $unaccounted_occupation_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_occupation_count;
                    $series_3[0] = $s;
                } else {
                    $s['name'] = $occupation_type[$count['occupation_type_id']];
                    $s['y'] = (int) $count['count'];
                    array_push($series_3, $s);
                }
            }

            unset($params['e_type']);
            $series_5 = [];

            $params['housing_membership'] = true;
            $housing_membership_count = $this->M_sales_demography->get_demographics($params);

            foreach ($housing_membership_count as $count) {

                if ($count['housing_membership_id'] == 0) {
                    $s['name'] = 'Unaccounted';
                    $s['y'] = (int) $count['count'];
                    array_push($series_5, $s);
                } else {
                    $s['name'] = $housing_membership[$count['housing_membership_id']];
                    $s['y'] = (int) $count['count'];
                    array_push($series_5, $s);
                }
            }

            unset($params['housing_membership']);

            $ages = ["18-23", "24-29", "30-35", "36-41", "42-47", "48-53", "54-59", "60-65", "66-100"];

            $series_4 = [];
            $v = 0;
            foreach ($ages as $key => $age) {

                $params['age'] = $age;
                $ac = $this->M_sales_demography->get_demographics($params);

                $x = (int) $ac['count'];
                array_push($series_4, $x);
                if ($x) {
                    $v = 1;
                }
            }
            if (empty($v)) {
                $series_4 = [];
            }

            unset($params['age']);

            $series_6 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];
            $params['region_id'] = true;
            $region_count = $this->M_sales_demography->get_demographics($params);
            $unaccounted_region_count = 0;

            foreach ($region_count as $key => $count) {

                $region_array = json_decode(json_encode($regions), true);
                $region = array_values(array_filter($region_array, function ($item) use ($count) {
                    return $item['regCode'] == $count['regCode'];
                }));

                if (!$count['regCode']) {

                    $unaccounted_region_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_region_count;
                    $series_6[0] = $s;
                } else {

                    $s['name'] = $region[0]['regDesc'];
                    $s['y'] = (int) $count['count'];
                    array_push($series_6, $s);
                }
            }

            unset($params['region_id']);

            $series_7 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];
            $params['province_id'] = true;
            $province_count = $this->M_sales_demography->get_demographics($params);
            $unaccounted_province_count = 0;

            foreach ($province_count as $key => $count) {

                $province_array = json_decode(json_encode($provinces), true);
                $province = array_values(array_filter($province_array, function ($item) use ($count) {
                    return $item['provCode'] == $count['provCode'];
                }));

                if (!$count['provCode']) {

                    $unaccounted_province_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_province_count;
                    $series_7[0] = $s;
                } else {

                    $s['name'] = $province[0]['provDesc'];
                    $s['y'] = (int) $count['count'];
                    array_push($series_7, $s);
                }
            }

            unset($params['province_id']);

            $series_8 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];
            $params['city_id'] = true;
            $city_count = $this->M_sales_demography->get_demographics($params);
            $unaccounted_city_count = 0;

            foreach ($city_count as $key => $count) {

                $city_array = json_decode(json_encode($cities), true);
                $city = array_values(array_filter($city_array, function ($item) use ($count) {
                    return $item['citymunCode'] == $count['citymunCode'];
                }));

                if (!$count['citymunCode']) {

                    $unaccounted_city_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_city_count;
                    $series_8[0] = $s;
                } else {

                    $s['name'] = $city[0]['citymunDesc'];
                    $s['y'] = (int) $count['count'];
                    array_push($series_8, $s);
                }
            }
            unset($params['city_id']);

            $series_9 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];
            $params['barangay_id'] = true;
            $barangay_count = $this->M_sales_demography->get_demographics($params);
            $unaccounted_barangay_count = 0;

            foreach ($barangay_count as $key => $count) {

                $barangay_array = json_decode(json_encode($barangays), true);
                $barangay = array_values(array_filter($barangay_array, function ($item) use ($count) {
                    return $item['brgyCode'] == $count['brgyCode'];
                }));

                if (!$count['brgyCode']) {

                    $unaccounted_barangay_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_barangay_count;
                    $series_9[0] = $s;
                } else {

                    $s['name'] = $barangay[0]['brgyDesc'];
                    $s['y'] = (int) $count['count'];
                    array_push($series_9, $s);
                }
            }

            unset($params['barangay_id']);

            $series_10 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];
            $params['country_id'] = true;
            $country_count = $this->M_sales_demography->get_demographics($params);
            $unaccounted_country_count = 0;

            foreach ($country_count as $key => $count) {

                $country_array = json_decode(json_encode($countries), true);

                $country = array_values(array_filter($country_array, function ($item) use ($count) {
                    return $item['id'] == $count['country_id'];
                }));

                if (!$count['country_id']) {

                    $unaccounted_country_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_country_count;
                    $series_10[0] = $s;
                } else {

                    $s['name'] = $country[0]['name'];
                    $s['y'] = (int) $count['count'];
                    array_push($series_10, $s);
                }
            }

            unset($params['country_id']);

            $series_11 = [];

            $params['sitio'] = 'sitio';

            $sitio_count = $this->M_sales_demography->get_demographics($params);
            // if ($sitio_count['count'] != 0) {
            // $s['name'] = $sitio_count['name'];
            $s['name'] = 'Unaccounted';
            $s['y'] = (int) $sitio_count['count'];
            array_push($series_11, $s);
            // }

            unset($params['sitio']);

            $series_12 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];
            $unaccounted_industry_count = 0;

            $params['industry_type'] = true;
            $industry_type_count = $this->M_sales_demography->get_demographics($params);

            foreach ($industry_type_count as $count) {

                if (!$count['industry_id']) {

                    $unaccounted_industry_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_industry_count;
                    $series_12[0] = $s;
                } else {
                    $s['name'] = $industry_type[$count['industry_id']];
                    $s['y'] = (int) $count['count'];
                    array_push($series_12, $s);
                }
            }

            unset($params['industry_type']);

            $series_13 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];
            $unaccounted_occupation_count = 0;

            $params['occupation_type'] = true;
            $occupation_type_count = $this->M_sales_demography->get_demographics($params);

            foreach ($occupation_type_count as $count) {

                if (!$count['occupation_id']) {

                    $unaccounted_occupation_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_occupation_count;
                    $series_13[0] = $s;
                } else {
                    $s['name'] = $occupation[$count['occupation_id']];
                    $s['y'] = (int) $count['count'];
                    array_push($series_13, $s);
                }
            }

            unset($params['occupation_type']);

            $series_14 = [[
                'name' => 'Unaccounted',
                'y' => 0
            ]];
            $unaccounted_source_referral_count = 0;

            $params['source_referral_type'] = true;
            $source_referral_type_count = $this->M_sales_demography->get_demographics($params);

            foreach ($source_referral_type_count as $count) {

                if (!$count['source_referral']) {

                    $unaccounted_source_referral_count += $count['count'];

                    $s['name'] = 'Unaccounted';
                    $s['y'] = $unaccounted_source_referral_count;
                    $series_14[0] = $s;
                } else {
                    $s['name'] = $source_referral[$count['source_referral']];
                    $s['y'] =  (int) $count['count'];
                    array_push($series_14, $s);
                }
            }

            unset($params['source_referral_type']);

            $this->view_data['title_1'] =  "Gender Demographic Chart";
            $this->view_data['title_2'] =  "Civil Status Demographic Chart";
            $this->view_data['title_3'] =  "Employment Type Demographic Chart";
            $this->view_data['title_4'] =  "Age Demographic Chart";
            $this->view_data['title_5'] =  "Buyer Housing Membership Chart";
            $this->view_data['title_6'] =  "Buyer by Regions";
            $this->view_data['title_7'] =  "Buyer by Provinces";
            $this->view_data['title_8'] =  "Buyer by Cities/Municipalities";
            $this->view_data['title_9'] =  "Buyer by Barangays";
            $this->view_data['title_10'] =  "Buyer by Countries";
            $this->view_data['title_11'] =  "Buyer by Sitio";
            $this->view_data['title_12'] =  "Buyer by Industry";
            $this->view_data['title_13'] =  "Buyer by Occupation";
            $this->view_data['title_14'] =  "Buyer by Source Referral";
            $this->view_data['subtitle']  = "Buyers from " . $daterange;

            $this->view_data['series_2'] = json_encode($series_2);
            $this->view_data['series_3'] = json_encode($series_3);
            $this->view_data['series_4'] = json_encode($series_4);
            $this->view_data['series_5'] = json_encode($series_5);
            $this->view_data['series_6'] = json_encode($series_6);
            $this->view_data['series_7'] = json_encode($series_7);
            $this->view_data['series_8'] = json_encode($series_8);
            $this->view_data['series_9'] = json_encode($series_9);
            $this->view_data['series_10'] = json_encode($series_10);
            $this->view_data['series_11'] = json_encode($series_11);
            $this->view_data['series_12'] = json_encode($series_12);
            $this->view_data['series_13'] = json_encode($series_13);
            $this->view_data['series_14'] = json_encode($series_14);

            $return['html'] = $this->load->view('dashboard/subs/sales_demography', $this->view_data, true);
            $return['data'] = $this->view_data;
            $return['daterange'] =  $daterange;
            echo json_encode($return);
        } else {

            $this->view_data['projects'] = $this->M_project->get_all();

            $this->template->build('sales_demography', $this->view_data);
        }
    }

    public function sellers_demography()
    {
        $this->js_loader->queue(['vendors/custom/highcharts/code/highcharts.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/series-label.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/accessibility.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/offline-exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/export-data.js']);
        $this->js_loader->queue(['modules/dashboard/hc-themes.js']);


        $sales_group = 0;

        if (isset($_GET['daterange']) && !empty($_GET['daterange'])) {

            $daterange = $_GET['daterange'];
        } else {

            $date = NOW;
            $date_from = date("Y/m/d", strtotime($date . " -10 year"));
            $date_to  = date("Y/m/d", strtotime($date));

            $daterange = view_date($date_from) . " - " . view_date($date_to);
        }

        if (isset($_GET['sales_group']) && !empty($_GET['sales_group'])) {

            $sales_group = $_GET['sales_group'];
        }

        $data['projects'] = $projects = $this->M_project->get_all();

        $civil_status = Dropdown::get_static('civil_status');
        $groups = Dropdown::get_static('group_type');
        $position_types = Dropdown::get_static('position_type');
        $regions = $this->M_region->fields(array('regCode', 'regDesc'))->get_all();
        $provinces = $this->M_province->fields(array('provCode', 'provDesc'))->get_all();
        $cities = $this->M_city->fields(array('citymunCode', 'citymunDesc'))->get_all();
        $barangays = $this->M_barangay->fields(array('brgyCode', 'brgyDesc'))->where('citymunCode', 61914)->get_all();
        $s1_positions = $this->M_position->get_all(array('position_type_id' => 1));
        $s2_positions = $this->M_position->get_all(array('position_type_id' => 2));

        $params['daterange'] = $daterange;

        $params['gender'] = 1;
        $params['sales_group'] = $sales_group;
        $male = $this->M_sales_demography->get_seller_demographics($params);

        $params['gender'] = 2;
        $female = $this->M_sales_demography->get_seller_demographics($params);

        $this->view_data['male']  = (int) $male['count'];
        $this->view_data['female']  = (int) $female['count'];

        $series_2 = [];
        foreach ($civil_status as $key => $c) {
            if (!$key) {
                continue;
            }
            unset($params['gender']);
            $params['civil_status'] = $key;
            $c_count = $this->M_sales_demography->get_seller_demographics($params);

            $s['name'] = $c;
            $s['y'] = (int) $c_count['count'];
            array_push($series_2, $s);
        }

        $series_3 = [];
        foreach ($groups as $key => $g) {
            if (!$key) {
                continue;
            }

            unset($params['civil_status']);
            $params['sales_group'] = $key;

            $g_count = $this->M_sales_demography->get_seller_demographics($params);

            $s['name'] = $g;
            $s['y'] = (int) $g_count['count'];
            array_push($series_3, $s);
        }

        $series_4 = [];
        foreach ($position_types as $key => $pt) {
            if (!$key) {
                continue;
            }

            $params['position_type'] = $key;
            $params['sales_group'] = $sales_group;

            $pt_count = $this->M_sales_demography->get_seller_demographics($params);

            $s['name'] = $pt;
            $s['y'] = (int) $pt_count['count'];
            array_push($series_4, $s);
        }
        unset($params['position_type']);
        unset($params['sales_group']);


        $series_5 = [];
        $series_6 = [];

        $ages = ["18-23", "24-29", "30-35", "36-41", "42-47", "48-53", "54-59", "60-65", "66-100"];
        $series_7 = [];

        foreach ($ages as $key => $age) {

            $params['age'] = $age;
            $params['sales_group'] = $sales_group;

            $ac = $this->M_sales_demography->get_seller_demographics($params);
            $x = (int) $ac['count'];
            array_push($series_7, $x);
        }


        unset($params['sales_group']);
        //positions 
        $series_s1 = [];
        foreach ($s1_positions as $key => $s1) {

            unset($params['age']);
            $params['seller_position'] = $s1['id'];

            $s1_count = $this->M_sales_demography->get_seller_demographics($params);

            $s['name'] = $s1['name'];
            $s['y'] = (int) $s1_count['count'];
            array_push($series_s1, $s);
        }

        $series_s2 = [];
        foreach ($s2_positions as $key => $s2) {

            $params['seller_position'] = $s2['id'];

            $s2_count = $this->M_sales_demography->get_seller_demographics($params);

            $s['name'] = $s2['name'];
            $s['y'] = (int) $s2_count['count'];
            array_push($series_s2, $s);
        }

        unset($params['seller_position']);

        $series_8 = [];
        foreach ($regions as $key => $reg) {
            if (!$key) {
                continue;
            }
            $params['region_id'] = $reg->regCode;

            $reg_count = $this->M_sales_demography->get_seller_demographics($params);

            if ($reg_count['count'] != 0) {
                $s['name'] = $reg->regDesc;
                $s['y'] = (int) $reg_count['count'];
                array_push($series_8, $s);
            }
        }

        unset($params['region_id']);

        $series_9 = [];
        foreach ($provinces as $key => $pr) {
            if (!$key) {
                continue;
            }
            $params['province_id'] = $pr->provCode;

            $pr_count = $this->M_sales_demography->get_seller_demographics($params);

            if ($pr_count['count'] != 0) {
                $s['name'] = $pr->provDesc;
                $s['y'] = (int) $pr_count['count'];
                array_push($series_9, $s);
            }
        }

        unset($params['province_id']);

        $series_10 = [];
        foreach ($cities as $key => $ct) {
            if (!$key) {
                continue;
            }
            $params['city_id'] = $ct->citymunCode;

            $ct_count = $this->M_sales_demography->get_seller_demographics($params);

            if ($ct_count['count'] != 0) {
                $s['name'] = $ct->citymunDesc;
                $s['y'] = (int) $ct_count['count'];
                array_push($series_10, $s);
            }
        }

        unset($params['city_id']);

        $series_11 = [];
        foreach ($barangays as $key => $brgy) {
            if (!$key) {
                continue;
            }
            $params['barangay_id'] = $brgy->brgyCode;

            $brgy_count = $this->M_sales_demography->get_seller_demographics($params);

            if ($brgy_count['count'] != 0) {
                $s['name'] = $brgy->brgyDesc;
                $s['y'] = (int) $brgy_count['count'];
                array_push($series_11, $s);
            }
        }

        $this->view_data['series_s1'] =  "Seller Position Chart";
        $this->view_data['series_s2'] =  "Seller Position Chart";
        $this->view_data['title_1'] =  "Gender Demographic Chart";
        $this->view_data['title_2'] =  "Civil Status Demographic Chart";
        $this->view_data['title_3'] =  "Sales Group Chart";
        $this->view_data['title_4'] =  "Agent/Broker Chart";
        $this->view_data['title_7'] =  "Age Demographic Chart";
        $this->view_data['title_8']  = "Seller by Regions";
        $this->view_data['title_9']  = "Seller by Provinces";
        $this->view_data['title_10']  = "Seller by Cities/Municipalities";
        $this->view_data['title_11']  = "Seller by Barangays";

        $this->view_data['subtitle']  = "Sellers encoded in the system ";

        $this->view_data['series_s1'] = json_encode($series_s1);
        $this->view_data['series_s2'] = json_encode($series_s2);
        $this->view_data['series_2'] = json_encode($series_2);
        $this->view_data['series_3'] = json_encode($series_3);
        $this->view_data['series_4'] = json_encode($series_4);
        $this->view_data['series_5'] = json_encode($series_5);
        $this->view_data['series_6'] = json_encode($series_6);
        $this->view_data['series_7'] = json_encode($series_7);
        $this->view_data['series_8'] = json_encode($series_8);
        $this->view_data['series_9'] = json_encode($series_9);
        $this->view_data['series_10'] = json_encode($series_10);
        $this->view_data['series_11'] = json_encode($series_11);

        if (isset($_GET) && !empty($_GET)) {
            $return['html'] = $this->load->view('dashboard/subs/sellers_demography', $this->view_data, true);
            $return['data'] = $this->view_data;
            $return['daterange'] =  $daterange;
            echo json_encode($return);
        } else {
            $this->template->build('sellers_demography', $this->view_data);
        }
    }

    public function get_project($id = false)
    {
        if ($id) {
            $data = $this->M_project->get($id);

            $output = array(
                'data' => $data,
            );
            echo json_encode($output);
        }
    }

    public function financials_graph()
    {

        $this->js_loader->queue(['vendors/custom/highcharts/code/highcharts.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/series-label.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/accessibility.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/offline-exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/export-data.js']);
        $this->js_loader->queue(['modules/dashboard/hc-themes.js']);

        if (isset($_GET['date_option_range']) && !empty($_GET['date_option_range'])) {

            $g_dates = $this->get_dates(array('date_option_range' => $_GET['date_option_range'], 'daterange' => $_GET['daterange']));

            $months = $g_dates['months'];
            $month_opt = $g_dates['month_opt'];
            $date_from = $g_dates['date_from'];
            $date_to = $g_dates['date_to'];

            $daterange = $_GET['daterange'];
        } else {

            $date = NOW;
            $date_from = date("Y/m/d", strtotime($date . " -1 year"));
            $date_to  = date("Y/m/d", strtotime($date));

            $date_option_range = "Y-m";

            $period = $this->M_report->get_periods($date_from, $date_to);

            $months = array();
            $month_opt = "year_month";

            foreach ($period as $key => $dt) {
                $months[] = $dt->format("Y-m");
            }

            $daterange = $date_from . " - " . $date_to;
        }

        $project_id = 0;
        $sub_type_id = 0;

        if (isset($_GET['project_id']) && !empty($_GET['project_id'])) {
            $project_id = $_GET['project_id'];
        }

        $data['dates'] = $months;

        $data['projects'] = $projects = $this->M_project->get_all();

        //consolidated all projects
        foreach ($months as $key => $month) {

            $params[$month_opt] = $month;
            $data['rows'][$month]['collection'] = $collection = $this->M_report->collection($params);
            $data['rows'][$month]['receivable'] = $receivable = $this->M_report->receivable($params);

            $cols[] = ($collection['t_amount'] ? (float) $collection['t_amount'] : (float) 0);
            $recs[] = ($receivable['t_amount'] ? (float) $receivable['t_amount'] : (float) 0);
        }

        $series = [];

        $col['name'] = 'Collections';
        $col['data'] = $cols;
        array_push($series, $col);

        $rec['name'] = 'Receivables';
        $rec['data'] = $recs;
        array_push($series, $rec);

        // vdebug($series);
        // vdebug($data);
        $this->view_data['f_report'] = $data;
        $this->view_data['series'] = json_encode($series);
        $this->view_data['dates'] = json_encode($months);
        $this->view_data['title'] =  "Receivable and Collection Report";
        $this->view_data['subtitle']  = "For the date range " . $daterange;
        $this->view_data['yAxisTitle'] = "Philippine Peso (Php)";
        $this->view_data['month_opt'] = $month_opt;
        $this->view_data['projects'] = $this->M_project->get_all();


        if (isset($_GET) && !empty($_GET)) {
            $return['html'] = $this->load->view('dashboard/subs/financials', $this->view_data, true);
            $return['data'] = $this->view_data;
            $return['daterange'] = $daterange;
            echo json_encode($return);
        } else {
            $this->template->build('financials', $this->view_data);
        }
    }

    public function sellers_graph($debug = 0)
    {

        $this->js_loader->queue(['vendors/custom/highcharts/code/highcharts.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/series-label.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/accessibility.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/offline-exporting.js']);
        $this->js_loader->queue(['vendors/custom/highcharts/code/modules/export-data.js']);
        $this->js_loader->queue(['modules/dashboard/hc-themes.js']);


        if (isset($_GET['date_option_range']) && !empty($_GET['date_option_range'])) {

            $g_dates = $this->get_dates(array('date_option_range' => $_GET['date_option_range'], 'daterange' => $_GET['daterange']));

            $months = $g_dates['months'];
            $month_opt = $g_dates['month_opt'];
            $date_from = $g_dates['date_from'];
            $date_to = $g_dates['date_to'];

            $daterange = $_GET['daterange'];
        } else {

            $date = NOW;
            $date_from = date("Y/m/d", strtotime($date . " -1 year"));
            $date_to  = date("Y/m/d", strtotime($date));

            $date_option_range = "Y-m";

            $period = $this->M_report->get_periods($date_from, $date_to);

            $months = array();
            $month_opt = "year_month";

            foreach ($period as $key => $dt) {
                $months[] = $dt->format("Y-m");
            }

            $daterange = $date_from . " - " . $date_to;
        }

        $project_id = 0;
        $sub_type_id = 0;

        if (isset($_GET['project_id']) && !empty($_GET['project_id'])) {
            $project_id = $_GET['project_id'];
        }

        if (isset($_GET['sub_type_id']) && !empty($_GET['sub_type_id'])) {
            $sub_type_id = $_GET['sub_type_id'];
        }

        $data['dates'] = $months;

        $data['projects'] = $projects = $this->M_project->get_all();

        $data['groups'] = $groups = Dropdown::get_static('group_type');

        $positions = $this->M_position->get_all();

        $series = [];

        foreach ($groups as $key2 => $group) {

            if (!$key2) {
                continue;
            }

            $amts = [];
            foreach ($months as $key => $month) {

                $params[$month_opt] = $month;
                $params['group'] = $key2;
                $params['project_id'] = $project_id;
                $params['sub_type_id'] = $sub_type_id;

                $ds = $this->M_report->sales_report($params);

                $ds_t = ($ds[0]['total_selling_price']  ? $ds[0]['total_selling_price'] : 0);

                $amts[] = (float) $ds_t;
            }


            $s['name'] = $group;
            $s['data'] = $amts;

            array_push($series, $s);
        }


        $group = 0;
        if (isset($_GET['group']) && !empty($_GET['group'])) {
            $group = $_GET['group'];
        }
        $series_2 = [];
        foreach ($positions as $key2 => $position) {

            $amts = [];
            foreach ($months as $key => $month) {

                $params[$month_opt] = $month;
                $params['position'] = $position['id'];
                $params['project_id'] = $project_id;
                $params['sub_type_id'] = $sub_type_id;
                $params['group'] = $group;

                $ds = $this->M_report->sales_report($params);

                $ds_t = ($ds[0]['total_selling_price']  ? $ds[0]['total_selling_price'] : 0);

                $amts[] = (float) $ds_t;
            }


            $s['name'] = $position['name'];
            $s['data'] = $amts;

            array_push($series_2, $s);
        }

        $prm['tops'] = 1;
        $prm['daterange'] = $daterange;
        $prm['sub_type_id'] = $sub_type_id;
        $prm['group'] = $group;
        $prm['project_id'] = $project_id;

        $data['tops'] = $this->M_report->sales_report($prm);
        if ($debug) {
            vdebug($data['tops']);
        }
        $this->view_data['groups'] = $data['groups'];
        $this->view_data['tops'] = $data['tops'];

        $this->view_data['dates'] = json_encode($months);

        $this->view_data['series'] = json_encode($series);
        $this->view_data['series_2'] = json_encode($series_2);

        $this->view_data['title'] =  "Sales Report by Sales Group";
        $this->view_data['title_2'] =  "Sales Report by Seller Position";

        $this->view_data['subtitle']  = "For the date range " . $daterange;
        $this->view_data['yAxisTitle'] = "Philippine Peso (Php)";

        $this->view_data['projects'] = $this->M_project->get_all();

        if (isset($_GET) && !empty($_GET)) {
            $return['html'] = $this->load->view('dashboard/subs/sellers', $this->view_data, true);
            $return['data'] = $this->view_data;
            $return['daterange'] = $daterange;
            echo json_encode($return);
        } else {
            $this->template->build('sellers', $this->view_data);
        }
    }

    public function inventory()
    {

        $this->template->build('dashboard');
    }

    public function get_dates($params = [])
    {
        $dates = explode('-', $params['daterange']);
        $date_from = db_date2($dates[0]);
        $date_to  = db_date2($dates[1]);

        if ($params['date_option_range'] == "daily") {

            $month_opt = "date";

            $months = $this->M_report->get_day_periods($date_from, $date_to);
        } else if ($params['date_option_range'] == "weekly") {

            $month_opt = "daterange";

            $months = $this->M_report->get_week_periods($date_from, $date_to);
        } else if ($params['date_option_range'] == "monthly") {

            $month_opt = "year_month";

            $date_option_range = "Y-m";

            $period = $this->M_report->get_periods($date_from, $date_to);

            $months = array();

            foreach ($period as $key => $dt) {

                $months[] = $dt->format("Y-m");
            }
        } else if ($params['date_option_range'] == "yearly") {

            $month_opt = "year";

            $date_option_range = "Y";

            $period = $this->M_report->get_year_periods($date_from, $date_to);

            $months = array();

            foreach ($period as $key => $dt) {
                $months[] = $dt->format("Y");
            }
        }

        $data['months'] = $months;
        $data['month_opt'] = $month_opt;
        $data['date_from'] = $date_from;
        $data['date_to'] = $date_to;

        return $data;
    }

    public function for_collection($debug = 0)
    {
        if ($this->input->post()) {
            $type = $this->input->post('type');
            $project_id = $this->input->post('project_id');
            $collections = $this->M_report->sum_collections($type, $project_id);
        }


        $result['date'] = date('F d, Y');
        $result['amount'] = money_php($collections['sum']);
        $result['count'] = $collections['count'];
        echo json_encode($result);
        exit();
    }

    public function for_pdc($debug = 0)
    {
        if ($this->input->post() || $debug) {
            $type = $this->input->post('type');
            $project_id = $this->input->post('project_id');
            if ($debug) {
                $type = 'today';
                $project_id = 0;
            }
            $pdc = $this->M_report->sum_pdc($type, $project_id);
        }

        $result['date'] = date('F d, Y');
        $result['amount'] = money_php($pdc['sum']);
        $result['count'] = $pdc['count'];
        echo json_encode($result);
        exit();
    }
    public function yearly_collections_receivables($debug = 0)
    {

        if ($this->input->post() || $debug) {
            $range = '-1 year';
            $date = NOW;

            $date_from = date("Y/m/d", strtotime($date . $range));
            $date_to  = date("Y/m/d", strtotime($date));
            $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));
            $result['range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));
            $project_id = $this->input->post('project_id');

            foreach ($months['months'] as $month => $attrib) {
                $temp_date = explode('-', $attrib);
                $results['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                $results['values_c'][] = $this->M_report->get_monthly_cr($project_id, $attrib, 'collections');
                $results['values_r'][] = $this->M_report->get_monthly_cr($project_id, $attrib, 'receivables');
            }
        }
        echo json_encode($results);
        exit();
    }

    public function for_today_collections_list($debug = 0)
    {
        if ($this->input->post() || $debug) {
            $type = $this->input->post('type');
            $project_id = $this->input->post('project_id');
            $data['today_collections'] = $this->M_report->collection_list($type, $project_id);
        }

        $result['html'] = $this->load->view('dashboards/financials/dashboard_collections_today', $data, true);
        echo json_encode($result);
        exit();
    }

    public function for_today_pdc_list($debug = 0)
    {
        if ($this->input->post() || $debug) {
            $type = $this->input->post('type');
            $project_id = $this->input->post('project_id');
            if ($debug) {
                $type = 'today';
                $project_id = 0;
            }
            $data['today_pdc'] = $this->M_report->pdc_list($type, $project_id);
        }

        $result['html'] = $this->load->view('dashboards/financials/dashboard_pdc_today', $data, true);
        echo json_encode($result);
        exit();
    }

    public function sold_projects_list($debug = '')
    {

        if ($this->input->post() || $debug) {
            $data['projects'] = $this->db->query(
                "SELECT projects.name AS name,
                projects.id
                FROM transactions
                inner join transaction_properties
                    ON transaction_properties.transaction_id = transactions.id
                    AND transaction_properties.deleted_at IS NULL
                    AND transaction_properties.house_model_id != 0
                inner join projects
                    ON transaction_properties.project_id = projects.id
                    AND projects.deleted_at IS NULL
                WHERE  ( Year(transactions.reservation_date) = Year(Curdate())
                    AND Month(transactions.reservation_date) <= Month(Curdate()) )
                    OR ( Year(transactions.reservation_date) = Year(Curdate() - interval 1 year)
                    AND Month(transactions.reservation_date) >= Month(Curdate()) )
                GROUP  BY transaction_properties.project_id
                order by projects.name 
            "
            )->result_array();
        }
        $data['html'] = $this->load->view('dashboards/properties/projects-dropdown', $data, true);
        echo json_encode($data);
        exit();
    }

    public function per_project_summary()
    {

        if ($this->input->post()) {
            $date_range = $this->input->post('date');
            $data_sets = $this->input->post('data_sets');
            $range = $this->input->post('time_frame');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            if ($data_sets == 0) {

                if ($range == 'specific') {
                    $date_from = date("Y/m/d", strtotime($start_date));
                    $date_to  = date("Y/m/d", strtotime($end_date));
                } else {
                    $date = NOW;
                    $date_from = date("Y/m/d", strtotime($date . " -1 year"));
                    $date_to  = date("Y/m/d", strtotime($date));
                }



                $months = $this->get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));

                $q_projects = $this->M_Transaction->get_projects($date_from, $date_to);

                $results['labels'] = [];
                $results['values'] = [];
                foreach ($q_projects as $project) {
                    array_push($results['labels'], $project['label']);
                    array_push($results['values'], $project['count']);
                }
                // vdebug($results);
                $start_date = explode('-', $months['months'][0]);
                $end_date = explode('-', end($months['months']));
                $results['date_range'] = date("F", mktime(0, 0, 0, $start_date[1], 10)) . ' ' . $start_date[0] . " - " . date("F", mktime(0, 0, 0, $end_date[1], 10)) . ' ' . $end_date[0];
            } else {

                $date = NOW;
                if ($range == 'specific') {
                    $date_from = date("Y/m/d", strtotime($start_date));
                    $date_to  = date("Y/m/d", strtotime($end_date));
                } else {
                    $date = NOW;
                    $date_from = date("Y/m/d", strtotime($date . " -1 year"));
                    $date_to  = date("Y/m/d", strtotime($date));
                }
                $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));
                $results = [];
                $results['labels'] = [];
                $results['values'] = [];
                $models = [];

                foreach ($months['months'] as $key => $value) {
                    $temp_date = explode('-', $value);
                    $results['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                    $models_arr = $this->M_Transaction->get_models_from_project($value, $data_sets);
                    foreach ($models_arr as $model) {
                        $models[] = $model;
                    }
                }
                foreach ($months['months'] as $key => $value) {
                    foreach (array_unique($models) as $model) {
                        $model_name = $this->M_house_model->get_name($model);
                        $results['values'][$model_name][] = $this->M_Transaction->get_models_from_project($value, $data_sets, $model);
                    }
                }
                $start_date = explode('-', $months['months'][0]);
                $end_date = explode('-', end($months['months']));
                $results['date_range'] = date("F", mktime(0, 0, 0, $start_date[1], 10)) . ' ' . $start_date[0] . " - " . date("F", mktime(0, 0, 0, $end_date[1], 10)) . ' ' . $end_date[0];
            }
        }
        echo json_encode($results);
    }
    public function dashboard_seller_graph($debug = 0)
    {
        $results = [];

        if ($this->input->post() || $debug == 1) {

            $commission_status = $this->input->post('commission_status');
            $range = $this->input->post('time_frame');
            $project_id = $this->input->post('project_id');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');



            if ($range == 'specific') {
                $date_from = date("Y/m/d", strtotime($start_date));
                $date_to = date("Y/m/d", strtotime($end_date));
            } else {
                if ($range == 'year') {
                    $data_attrib = "-1 year";
                } elseif ($range == '6months') {
                    $data_attrib = "-6 month";
                }
                $date = NOW;
                $date_from = date("Y/m/d", strtotime($date . $data_attrib));
                $date_to  = date("Y/m/d", strtotime($date));
            }
            //          This part is for the future when we set dynamic dates.
            $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));

            //          Loop through every month and call the values from that month.
            foreach ($months['months'] as $month => $attrib) {
                $temp_date = explode('-', $attrib);
                $results['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                $results['values'][] = $this->M_Transaction_commission->get_monthly_commissions($attrib, $commission_status, $project_id);
            }
            //          Change the numeric months to word names.
            $results['date_range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));
            $results['current_month'] = money_php(end($results['values']));
        }

        echo json_encode($results);
    }

    public function dashboard_seller_top_sellers($debug = 0)
    {

        $params['tops'] = 1;
        if ($debug) {
            // vdebug($this->M_report->sales_report($params));

        }
        $results = [];
        if ($this->input->post() || $debug) {
            $params['project_id'] = $this->input->post('project_id');
            $range = $this->input->post('time_frame');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            $params['general_limit'] = $this->input->post('limit');
            if ($debug) {
                $params['project_id'] = 0;
                $range = 'year';
            }

            if ($range == 'year') {
                $date = NOW;
                $date_from = date("Y/m/d", strtotime($date . " -1 year"));
                $date_to  = date("Y/m/d", strtotime($date));

                $daterange = $date_from . " - " . $date_to;

                $params['daterange'] = $daterange;
            } else {
                $date_from = date("Y-m", strtotime($start_date));
                $date_to  = date("Y-m", strtotime($end_date));
                $params['year_month_range'] = [$date_from, $date_to];
            }

            $results['top'] = $this->M_report->sales_report($params);
            if ($params['general_limit']) {
                $results['html'] = $this->load->view('dashboards/general/top_three_sellers', $results, true);
            } else {
                $results['html'] = $this->load->view('dashboards/sellers/top_sellers_table', $results, true);
            }
        }


        echo json_encode($results);
    }

    public function purchase_charts($debug = 0)
    {
        $results = [];
        $date = NOW;
        if ($this->input->post() || $debug) {

            $range = $this->input->post('time_frame');
            $status = $this->input->post('status');
            $get_stat = $this->input->post('get_stat');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');

            if ($range == 'specific') {

                $date_from = date("Y/m/d", strtotime($start_date));
                $date_to  = date("Y/m/d", strtotime($end_date));
            } else {

                $date_from = date("Y/m/d", strtotime($date . $range));
                $date_to  = date("Y/m/d", strtotime($date));
            }

            $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));

            $results['date_range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));
            foreach ($months['months'] as $month => $attrib) {
                $temp_date = explode('-', $attrib);
                $results['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                $results['values'][] = $this->M_report->get_monthly_po($attrib, $status, $get_stat);
            }
        }
        echo json_encode($results);
    }

    public function top_po_suppliers($debug = 0)
    {

        $results = [];
        if ($this->input->post() || $debug) {
            $get_stat = $this->input->post('get_stat');
            $status = $this->input->post('status');
            $range = $this->input->post('time_frame');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');

            $date = NOW;
            if ($range == 'specific') {
                $date_from = date("Y/m", strtotime($start_date));
                $date_to  = date("Y/m", strtotime($end_date));
                $date_range_raw = date("F Y", strtotime($start_date)) . ' - ' . date("F Y", strtotime($end_date));
            } else {
                $date_from = date("Y/m", strtotime($date . '-1 year'));
                $date_to  = date("Y/m", strtotime($date));
                $date_range_raw = date("F Y", strtotime($date . '-1 year')) . ' - ' . date("F Y", strtotime($date));
            }
            $range = [$date_from, $date_to];
            $results['top'] = $this->M_report->top_suppliers($range, $status, $get_stat);
            $results['type'] = $get_stat;

            $results['date_range'] = $date_range_raw;
            $results['html'] = $this->load->view('dashboards/purchases/top_suppliers_table', $results, true);
        }
        echo json_encode($results);
    }

    public function accounting_payment_rv($debug = 0)
    {
        $date = NOW;
        $results = [];
        if ($this->input->post() || $debug) {
            $range = $this->input->post('time_frame');
            $type = $this->input->post('type');
            $project_id = $this->input->post('project_id');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            if ($debug) {
                $range = '-1 year';
            }
            if ($range == 'specific') {
                $date_from = date("Y/m/d", strtotime($start_date));
                $date_to  = date("Y/m/d", strtotime($end_date));
                $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));
            } else {
                $date_from = date("Y/m/d", strtotime($date . $range));
                $date_to  = date("Y/m/d", strtotime($date));
                $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));
            }

            $results['date_range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));
            foreach ($months['months'] as $month => $attrib) {
                $temp_date = explode('-', $attrib);
                $results['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                $results['values'][] = $this->M_report->payment_rv_monthly($attrib, $type, $project_id);
            }
        }

        echo json_encode($results);
    }

    public function accounting_payment_requests_pie($debug = 0)
    {

        $results = [];

        if ($this->input->post() || $debug) {
            $range = $this->input->post('time_frame');
            $project_id = $this->input->post('project_id');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');

            if ($debug) {
                $range = '-1 year';
            }

            if ($range == 'specific') {
                $date_from = date("Y-m", strtotime($start_date));
                $date_to  = date("Y-m", strtotime($end_date));
            } else {
                $date = NOW;
                $date_from = date("Y-m", strtotime($date . $range));
                $date_to  = date("Y-m", strtotime($date));
            }

            $query = $this->M_report->payment_request_pie($date_from, $date_to, $project_id);
            $total = $query['total'];
            $results['labels'] = $query['ids'];
            $results['date_range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));
            foreach ($query['count'] as $key => $value) {
                $results['values'][] = round(($value / $total) * 100);
            }
        }

        echo json_encode($results);
    }

    public function month_sales($debug = 0)
    {

        if ($this->input->post() || $debug) {
            $filter = $this->input->post('filter');
            $range = $this->input->post('date_range');
            $project_id = $this->input->post('project_id');
            $select = $this->input->post('select');
            if ($debug) {
                $range = '-6 month';
                $project_id = 0;
            }

            $date = NOW;

            $date_from = date("Y/m/d", strtotime($date . $range));
            $date_to  = date("Y/m/d", strtotime($date));
            $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));
            $result['date_range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));


            foreach ($months['months'] as $month => $attrib) {
                $temp_date = explode('-', $attrib);
                $result['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                $result['values'][] = $this->M_report->sales_monthly($attrib, $project_id, $select);
            }
        }
        $result['amount'] = money_php(end($result['values']));
        $result['total'] = money_php(array_sum($result['values']));
        echo json_encode($result);
        exit();
    }

    public function month_collections($debug = 0)
    {
        if ($this->input->post() || $debug) {
            $filter = $this->input->post('filter');
            $range = $this->input->post('date_range');
            $project_id = $this->input->post('project_id');
            if ($debug) {
                $range = '-6 month';
                $project_id = 0;
            }

            $date = NOW;

            $date_from = date("Y/m/d", strtotime($date . $range));
            $date_to  = date("Y/m/d", strtotime($date));
            $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));
            $result['date_range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));


            foreach ($months['months'] as $month => $attrib) {
                $temp_date = explode('-', $attrib);
                $result['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                $result['values'][] = $this->M_report->collections_monthly($attrib, $project_id);
            }
        }
        $result['amount'] = money_php(end($result['values']));
        echo json_encode($result);
        exit();
    }

    public function month_property_count($debug = 0)
    {
        if ($this->input->post() || $debug) {
            $filter = $this->input->post('filter');
            $range = $this->input->post('date_range');
            $project_id = $this->input->post('project_id');
            $select = $this->input->post('select');
            if ($debug) {
                $range = '-6 month';
                $project_id = 0;
            }

            $date = NOW;

            $date_from = date("Y/m/d", strtotime($date . $range));
            $date_to  = date("Y/m/d", strtotime($date));
            $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));
            $result['date_range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));


            foreach ($months['months'] as $month => $attrib) {
                $temp_date = explode('-', $attrib);
                $result['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                $result['values'][] = $this->M_report->sales_monthly($attrib, $project_id, $select);
            }
        }
        $result['amount'] = end($result['values']);
        echo json_encode($result);
        exit();
    }

    public function month_buyer_count($debug = 0)
    {
        if ($this->input->post() || $debug) {
            $filter = $this->input->post('filter');
            $range = $this->input->post('date_range');
            $project_id = $this->input->post('project_id');
            $select = $this->input->post('select');
            if ($debug) {
                $range = '-6 month';
                $project_id = 0;
            }

            $date = NOW;

            $date_from = date("Y/m/d", strtotime($date . $range));
            $date_to  = date("Y/m/d", strtotime($date));
            $months = get_dates(array('daterange' => $date_from . ' - ' . $date_to, 'date_option_range' => 'monthly'));
            $result['date_range'] = date("F Y", strtotime($date_from)) . " - " . date("F Y", strtotime($date_to));


            foreach ($months['months'] as $month => $attrib) {
                $temp_date = explode('-', $attrib);
                $result['labels'][] = date("F", mktime(0, 0, 0, $temp_date[1], 10)) . ' ' . $temp_date[0];
                $result['values'][] = $this->M_report->buyer_count($attrib, $project_id, $select);
            }
        }
        $result['amount'] = end($result['values']);
        echo json_encode($result);
        exit();
    }

    public function get_count_for_week($debug = 0)
    {
        if ($this->input->post() || $debug) {

            $date = date("Y/m/d", strtotime(NOW));
            $week = get_dates(array('daterange' => $date . ' - ', 'date_option_range' => 'weekdays'));
            foreach ($week['months'] as $day => $attrib) {

                $result['labels'][] = date('F d, Y', strtotime($attrib));
                $query = $this->M_comm_group->with_communication('fields:*count*')->where(array('status' => 1, "DATE_FORMAT(send_schedule_date, '%Y/%m/%d')=" => $attrib))->as_array()->get_all();
                $result['past_due_notices'][] = $pdn_count = $this->M_report->get_count_past_due_notices($attrib);
                $result['letter_request'][] = $lr_count = $this->M_report->get_count_transaction_letter_request($attrib);



                $sms_count = 0;
                $email_count = 0;
                if ($query) {
                    foreach ($query as $comm_group => $comm) {
                        if ($comm['medium_type'] == 'sms') {
                            $sms_count += $comm['communication'][0]['counted_rows'];
                        } elseif ($comm['medium_type'] == 'email') {
                            $email_count += $comm['communication'][0]['counted_rows'];
                        }
                    }
                }
                $result['email'][] = $email_count;
                $result['sms'][] = $sms_count;
                $result['totals'][]  = $email_count + $sms_count + $pdn_count + $lr_count;
            }
            $result['total_email'] = array_sum($result['email']);
            $result['total_sms'] = array_sum($result['sms']);
            $result['total_past_due_notices'] = array_sum($result['past_due_notices']);
            $result['total_letter_request'] = array_sum($result['letter_request']);
        }
        echo json_encode($result);
        exit();
    }

    public function get_recent_activities($debug = 0)
    {
        $result = '';
        if ($this->input->post() || $debug) {

            $page_data['raw_activities'] = $this->M_report->get_recent_activity();
            $result = $this->load->view('dashboards/general/recent_activities', $page_data, true);
        }
        echo json_encode($result);
        exit();
    }
    public function get_recent_tickets($debug = 0)
    {
        $result = '';
        if ($this->input->post() || $debug) {

            $page_data['tickets'] = $this->M_report->get_recent_tickets();
            $result = $this->load->view('dashboards/general/inquiry_tickets', $page_data, true);
        }
        echo json_encode($result);
        exit();
    }
    public function get_latest_buyers($debug = 0)
    {
        $result = '';
        if ($this->input->post() || $debug) {

            $page_data['buyers'] = $this->M_report->get_latest_buyers();
            $result = $this->load->view('dashboards/general/latest_buyers', $page_data, true);
        }
        echo json_encode($result);
        exit();
    }
}
