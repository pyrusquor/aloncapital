<?php
// ==================== begin: Add model fields ====================
$company_id = isset($info['company_id']) && $info['company_id'] ? $info['company_id'] : '';
$completion_certificate = isset($info['completion_certificate']) && $info['completion_certificate'] ? $info['completion_certificate'] : '';
$customer_code = isset($info['customer_code']) && $info['customer_code'] ? $info['customer_code'] : '';
$instruction_number = isset($info['instruction_number']) && $info['instruction_number'] ? $info['instruction_number'] : '';
$filed_date = isset($info['filed_date']) && $info['filed_date'] ? $info['filed_date'] : '';
$agent = isset($info['agent']) && $info['agent'] ? $info['agent'] : '';
$warehouse = isset($info['warehouse']) && $info['warehouse'] ? $info['warehouse'] : '';
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
$sub_project_id = isset($info['sub_project_id']) && $info['sub_project_id'] ? $info['sub_project_id'] : '';

$construction_method = isset($info['construction_method']) && $info['construction_method'] ? $info['construction_method'] : '';
$furnishing_type = isset($info['furnishing_type']) && $info['furnishing_type'] ? $info['furnishing_type'] : '';
$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';

$proposed_start_date = isset($info['proposed_start_date']) && $info['proposed_start_date'] ? $info['proposed_start_date'] : '';
$proposed_finish_date = isset($info['proposed_finish_date']) && $info['proposed_finish_date'] ? $info['proposed_finish_date'] : '';
$actual_start_date = isset($info['actual_start_date']) && $info['actual_start_date'] ? $info['actual_start_date'] : '';
$actual_finish_date = isset($info['actual_finish_date']) && $info['actual_finish_date'] ? $info['actual_finish_date'] : '';


// ==================== end: Add model fields ====================


// Add the following fields
// 1. Proposed Start Date
// 2. Proposed Date to Finish
// 3. Actual Start Date
// 4. Actual Date Finished
// 5. Construction Method Dropdown (Cast-in-place, Precast, Hybrid)
// 6. Furnishing Type Dropdown (Bare, Finished, Fully Furnished)
// 7. Remarks

// Remove the following fields
// 1. Turnover Date
// 2. Waranty Date

?>

<div class="row">
    <div class="col-md-12">

        <div class="row">

            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label>Status <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('status', Dropdown::get_static("construction_order_status"), 1, 'class="form-control"') ?>
                </div>
            </div>

            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label class="">Customer Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('customer_type', Dropdown::get_static("construction_order_customer_type"), null, 'class="form-control"') ?>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">Company <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="company_id" id="company_id" class="suggests form-control" data-module="companies">
                            <option value="">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Completion Certificate <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="completion_certificate" value="<?php echo set_value('completion_certificate', $completion_certificate); ?>" placeholder="Completion Certificate" autocomplete="off">
                    </div>
                    <?php echo form_error('completion_certificate'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">Project</label>
                    <div class="kt-input-icon">
                        <select name="project_id" id="project_id" class="suggests form-control" data-module="projects">
                            <option value="0">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">Sub Project</label>
                    <div class="kt-input-icon">
                        <select name="sub_project_id" id="sub_project_id" class="suggests form-control" data-module="sub_projects">
                            <option value="0">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Property <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <select name="property_id" id="property_id" class="suggests form-control" data-module="properties">
                            <option value="0">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label class="">Furnishing Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('furnishing_type', Dropdown::get_static("option_unit_type"), null, 'class="form-control"') ?>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Customer Code <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="customer_code" value="<?php echo set_value('customer_code', $customer_code); ?>" placeholder="Customer Code" autocomplete="off">
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <?php echo form_error('customer_code'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Agent <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="agent" value="<?php echo set_value('agent', $agent); ?>" placeholder="Agent" autocomplete="off">
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <?php echo form_error('agent'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">Warehouse</label>
                    <div class="kt-input-icon">
                        <select name="warehouse_id" id="warehouse_id" class="suggests form-control" data-module="warehouses">
                            <option value="0">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label class="">Construction Method <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('construction_method', Dropdown::get_static("construction_method"), null, 'class="form-control"') ?>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Filled Date <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Filed Date" name="filed_date" value="<?php echo set_value('filed_date"', @$filed_date); ?>" readonly>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Instruction Number <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="number" class="form-control" name="instruction_number" value="<?php echo set_value('instruction_number', $instruction_number); ?>" placeholder="Instruction Number" autocomplete="off">
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <?php echo form_error('instruction_number'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>


        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Proposed Start Date<span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Proposed Start" name="proposed_start_date" value="<?php echo set_value('proposed_start_date"', @$proposed_start_date); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Proposed End Date<span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Proposed End" name="proposed_finish_date" value="<?php echo set_value('proposed_finish_date"', @$proposed_finish_date); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>


        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Actual Start Date<span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Actual Start" name="actual_start_date" value="<?php echo set_value('actual_start_date"', @$actual_start_date); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-Actual">
                    <label>Actual End Date<span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Actual End" name="actual_finish_date" value="<?php echo set_value('actual_finish_date"', @$actual_finish_date); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Note <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" name="note" value="<?php echo set_value('note', $note); ?>" placeholder="Note" autocomplete="off">
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <?php echo form_error('note'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>


        <!-- <div class="row">

            <div class="col-sm-4 hide">
                <div class="form-group">
                    <label>Turnover Date</label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Turnover Date" name="turnover_date" value="<?php echo set_value('turnover_date"', @$turnover_date); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>


        <div class="row hide">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Warranty Start <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Warranty Start" name="warranty_start" value="<?php echo set_value('warranty_start"', @$warranty_start); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Warrant End <span class="kt-font-danger"></span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Warrant End" name="warranty_end" value="<?php echo set_value('warranty_end"', @$warranty_end); ?>" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div> -->



    </div>
</div>