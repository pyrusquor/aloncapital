<div class="row">
    <div class="col-md-12">
        <input type="hidden" id="construction_order_id" value="<?php echo $id;?>">
        <div class="row">
        <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Template</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="template_id"  id="template_id" class="suggests form-control" data-module="construction_templates"><option value="0">Select Option</option></select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="">Group</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="item_group_id"  id="item_group_id" class="form-control">
                            <option value="0">Select Option</option>
                            <?php foreach($item_group as $k => $v): ?>
                                <option value="<?=$v['id']?>"><?=$v['name']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="">Type</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="item_type_id"  id="item_type_id" class="form-control">
                            <option value="0">Select Option</option>
                            <?php foreach($item_type as $k => $v): ?>
                                <option value="<?=$v['id']?>"><?=$v['name']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="">Brand Name</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="item_brand_id"  id="item_brand_id" class="suggests form-control" data-module="item_brand">
                            <option value="0">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="">Class Name</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="item_class_id"  id="item_class_id" class="suggests form-control" data-module="item_class">
                            <option value="0">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label class="">Item Name</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="item"  id="item" class="form-control">
                            <option value="0">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="">Unit of Measurement</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select name="unit_of_measurement"  id="unit_of_measurement" class="suggests form-control" data-module="inventory_settings_unit_of_measurements">
                            <option value="0">Select Option</option>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="">Quantity</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="number" class="form-control" placeholder="0" id="quantity" name="quantity" value="<?=$quantity;?>">
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="">Unit Cost</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="number" class="form-control" placeholder="0" id="amenity" name="amenity" value="<?=$amenity;?>" readonly>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="">Sub Total Cost</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="number" class="form-control" placeholder="0" id="sub_amenity" name="sub_amenity" value="<?=$sub_amenity;?>" readonly>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-2 align-self-center">
                <?php if (empty($items)): ?>
                    <a id="add_entry_btn" href="javascript:;" data-repeater-create=""
                        class="btn btn-bold btn-sm btn-label-brand">
                        <i class="la la-plus"></i> Add Entry Item
                    </a>
                <?php endif ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <!-- Construction Order Items -->
                <?php if (!empty($items)): ?>
                <?php foreach ($items as $key => $item): ?>
                <?php
                        $construction_order_id = isset($item['construction_order_id']) && $item['construction_order_id'] ? $item['construction_order_id'] : '';
                        $item_id = isset($item['item_id']) && $item['item_id'] ? $item['item_id'] : '';
                        $item_name = isset($item['name']) && $item['name'] ? $item['name'] : '';
                        $item_code = isset($item['item_code']) && $item['item_code'] ? $item['item_code'] : '';
                        $quantity = isset($item['quantity']) && $item['quantity'] ? $item['quantity'] : '';
                        $tax_id = isset($item['tax_id']) && $item['tax_id'] ? $item['tax_id'] : '';
                        $tax_name = get_value_field($tax_id,'taxes','name');
                        $rate = isset($item['rate']) && $item['rate'] ? $item['rate'] : '';
                        $tax_amount = isset($item['tax_amount']) && $item['tax_amount'] ? $item['tax_amount'] : '';
                        $total_amount = isset($item['total_amount']) && $item['total_amount'] ? $item['total_amount'] : '';
                        ?>
                <div id="construction_order_item_form_repeater">
                    <div data-repeater-list="item[<?php echo $item_id; ?>]">
                        <div data-repeater-item="item[<?php echo $item_id; ?>]" class="row">
                            <div class="row">
                                <input type="hidden" name="item[<?php echo $item_id ?>][item_id]" class="form-control item_id" value="<?php echo set_value('item_id', $item_id);?>" id="item[<?php echo $item_id ?>][item_id]" readonly>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Item Name <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control item_name"
                                                name="item[<?php echo $item_id ?>][item_name]"
                                                value="<?php echo set_value('item_name', $item_name); ?>"
                                                placeholder="Item Name" autocomplete="off" id="item[<?php echo $item_id ?>][item_name]" readonly>

                                        </div>
                                        <?php echo form_error('item_name'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Item Code <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control item_code"
                                                name="item[<?php echo $item_id ?>][item_code]"
                                                value="<?php echo set_value('item_code', $item_code); ?>"
                                                placeholder="Item Code" autocomplete="off" id="item[<?php echo $item_id ?>][item_code]" readonly>

                                        </div>
                                        <?php echo form_error('item_code'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Unit of Measurement <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control unit_of_measurement"
                                                name="item[<?php echo $item_id ?>][unit_of_measurement]"
                                                value="<?php echo set_value('unit_of_measurement', $unit_of_measurement); ?>"
                                                placeholder="Unit of Measurement" autocomplete="off" id="item[<?php echo $item_id ?>][unit_of_measurement]" readonly>

                                        </div>
                                        <?php echo form_error('unit_of_measurement'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control quantity" id="item[<?php echo $item_id ?>][quantity]"
                                                name="item[<?php echo $item_id ?>][quantity]"
                                                value="<?php echo set_value('quantity', $quantity); ?>" placeholder="Quantity"
                                                autocomplete="off" readonly>
                                        </div>
                                        <?php echo form_error('quantity'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Unit Cost</label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control amenity" id="item[<?php echo $item_id ?>][amenity]"
                                                name="item[<?php echo $item_id ?>][amenity]"
                                                value="<?php echo set_value('amenity', $amenity); ?>" placeholder="Unit Cost"
                                                autocomplete="off" readonly>
                                        </div>
                                        <?php echo form_error('amenity'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>


                                <div class="col-sm-2">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label>Sub Total <span class="kt-font-danger">*</span></label>
                                                <div class="kt-input-icon kt-input-icon--left">
                                                    <input type="text" class="form-control sub_amenity"
                                                        name="item[<?php echo $item_id ?>][sub_amenity]"
                                                        value="<?php echo set_value('sub_amenity', $sub_amenity); ?>"
                                                        placeholder="Total Amount" autocomplete="off"
                                                        id="item[<?php echo $item_id ?>][sub_amenity]" readonly>
                                                </div>
                                                <?php echo form_error('sub_amenity'); ?>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <a href="javascript:;" data-repeater-delete=""
                                                    class="btn-sm btn btn-label-danger btn-bold">
                                                    <i class="la la-trash-o"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php else: ?>
                <div id="construction_order_item_form_repeater">
                    <div data-repeater-list="items">
                        <div data-repeater-item="items">
                            <div class="row">
                            <input type="hidden" name="item[item_id]" class="form-control item_id" value="<?php echo set_value('item_id', $item_id);?>" id="item[item_id]" readonly>
                                <div class="col-sm-2">
                                        <label>Item Name <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control item_name"
                                                name="item[item_name]"
                                                value="<?php echo set_value('item_name', $item_name); ?>"
                                                placeholder="Item Name" autocomplete="off" id="item[item_name]" readonly>

                                        </div>
                                        <?php echo form_error('item_name'); ?>
                                        <span class="form-text text-muted"></span>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Item Code <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control item_code"
                                                name="item[item_code]"
                                                value="<?php echo set_value('item_code', $item_code); ?>"
                                                placeholder="Item Code" autocomplete="off" id="item[item_code]" readonly>

                                        </div>
                                        <?php echo form_error('item_code'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Unit of Measurement <span class="kt-font-danger">*</span></label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control unit_of_measurement"
                                                name="item[unit_of_measurement]"
                                                value="<?php echo set_value('unit_of_measurement', $unit_of_measurement); ?>"
                                                placeholder="Unit of Measurement" autocomplete="off" id="item[unit_of_measurement]" readonly>

                                        </div>
                                        <?php echo form_error('unit_of_measurement'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control quantity" id="item[quantity]"
                                                name="item[quantity]"
                                                value="<?php echo set_value('quantity', $quantity); ?>" placeholder="Quantity"
                                                autocomplete="off" readonly>
                                        </div>
                                        <?php echo form_error('quantity'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Unit Cost</label>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control amenity" id="item[amenity]"
                                                name="item[amenity]"
                                                value="<?php echo set_value('amenity', $amenity); ?>" placeholder="Unit Cost"
                                                autocomplete="off" readonly>
                                        </div>
                                        <?php echo form_error('amenity'); ?>
                                        <span class="form-text text-muted"></span>
                                    </div>
                                </div>


                                <div class="col-sm-2">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label>Sub Total <span class="kt-font-danger">*</span></label>
                                                <div class="kt-input-icon kt-input-icon--left">
                                                    <input type="text" class="form-control sub_amenity"
                                                        name="item[sub_amenity]"
                                                        value="<?php echo set_value('sub_amenity', $sub_amenity); ?>"
                                                        placeholder="Sub Total" autocomplete="off"
                                                        id="item[sub_amenity]" readonly>
                                                </div>
                                                <?php echo form_error('sub_amenity'); ?>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <a href="javascript:;" data-repeater-delete=""
                                                    class="btn-sm btn btn-label-danger btn-bold">
                                                    <i class="la la-trash-o"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <!-- /Construction Order  Items -->

            </div>
        </div>
    </div>
</div>