<!-- CONTENT HEADER -->
<?php
$info ? extract($info) : '';
?>
<script type='text/javascript'>
    var timeNow = <?= date("Y-m-d", strtotime(NOW)) ?>
</script>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Overhead Cost</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a onclick="goBack()" class="btn btn-label-instagram"><i class="fas fa-reply"></i>
                    Back</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                <form class="kt-form" method="POST" action="" id="overhead_cost_form" enctype="multipart/form-data">
                    <input type='hidden' value='<?= $id ?>' id='construction_order_id' name='order[construction_order_id]'>
                    <input type='hidden' value='<?= $original_ids ?>' name='original_ids'>
                    <div class="kt-section kt-section--first">
                        <h3 class="kt-section__title">List of Costs</h3>
                        <div class='repeater'>
                            <div data-repeater-list="cost">
                                <?php if ($overhead_cost) :
                                    foreach ($overhead_cost as $key => $cost) :
                                        extract($cost);
                                ?>
                                        <div data-repeater-item class='row'>
                                            <div class="col-sm-8">
                                                <input type='hidden' name='cost[id]' value='<?= $id ?>'>
                                                <div class="form-group">
                                                    <label>Description <span class="kt-font-danger">*</span></label>
                                                    <div class="kt-input-icon">
                                                        <input class="form-control" name="cost[description]" value="<?= $cost['description'] ?>" placeholder='Cost Description' required>
                                                        <span class="kt-input-icon__icon"></span>
                                                    </div>
                                                    <span class="form-text text-muted"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Amount<span class="kt-font-danger">*</span></label>
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="kt-input-icon">
                                                                <input class="form-control" min="0" step="0.25" type="number" value="<?= $cost['amount'] ?>" name="cost[amount]" placeholder="Amount" required />
                                                                <span class="kt-input-icon__icon"></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-danger btn-bold">
                                                                <i class="la la-trash-o"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <span class="form-text text-muted"></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                <?php else : ?>
                                    <div data-repeater-item class='row'>
                                        <input type='hidden' name='cost[id]' val=''>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label>Description <span class="kt-font-danger">*</span></label>
                                                <div class="kt-input-icon">
                                                    <input class="form-control" name="cost[description]" placeholder='Cost Description' required>
                                                    <span class="kt-input-icon__icon"></span>
                                                </div>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Amount<span class="kt-font-danger">*</span></label>
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="kt-input-icon">
                                                            <input class="form-control" min="0" step="0.25" type="number" name="cost[amount]" placeholder="Amount" required />
                                                            <span class="kt-input-icon__icon"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-danger btn-bold">
                                                            <i class="la la-trash-o"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="form-group form-group-last row">
                                <div class="offset-10"></div>
                                <div class="col-lg-2">
                                    <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                        <i class="la la-plus"></i> Cost
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class='col-lg-12 kt-align-right'>
                            <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
                                Submit
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>