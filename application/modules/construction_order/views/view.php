<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-portlet__head-title">
                <small>Order No: </small><?= $data['cor_number'] ?: 'N/A' ?></h6>
            </h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('construction_order/overhead_cost_form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-folder-open"></i> Update Overhead Costs
                </a>
                <a href="<?php echo site_url('construction_order/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('construction_order'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class='row'>
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">

                <ul class="nav nav-pills nav-justified" role="tablist" id="dashboard_nav">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" data-target="#info" role="tab">
                            <i class="fas fa-tablet-alt"></i>
                            Order Info
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" data-target="#items" role="tab">
                            <i class="fas fa-boxes"></i>
                            Items
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" data-target="#issued_items" role="tab">
                            <i class="fas fa-box-open"></i>
                            Issued Items
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" data-target="#laborers" role="tab">
                            <i class="fas fa-users"></i>
                            Laborers
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" data-target="#equipment" role="tab">
                            <i class="fas fa-tools"></i>
                            Equipment
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <div class="tab-content">
        <div class="tab-pane fade show active" id="info" role="tabpanel">
            <?php $this->load->view('view/info') ?>
        </div>
        <div class="tab-pane fade" id="items" role="tabpanel">
            <?php $this->load->view('view/items') ?>
        </div>
        <div class="tab-pane fade" id="issued_items" role="tabpanel">
            <?php $this->load->view('view/issued_items') ?>
        </div>
        <div class="tab-pane fade" id="laborers" role="tabpanel">
            <?php $this->load->view('view/laborers') ?>
        </div>
        <div class="tab-pane fade" id="equipment" role="tabpanel">
            <?php $this->load->view('view/equipment') ?>
        </div>
    </div>
</div>
<!-- begin:: Footer -->