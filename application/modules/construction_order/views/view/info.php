<?php
extract($data);
?>
<div class="row">
    <div class="col-md-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h5 class="display-"><?= $data['status'] ?></h5>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                    <div class="row">
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Company</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark">
                                            <?= $company['name'] ?: 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Project</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark">
                                            <?= $project['name'] ?: 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Property</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark">
                                            <?= $property['name'] ?: 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style='margin-top:15px'>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Customer Code</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark">
                                            <?= $customer_code ?: 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Agent</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark">
                                            <?= $agent ?: 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h5 class="display-">Cost</h5>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                    <div class="row">
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Labor Cost</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $laborer_total ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Overhead Cost</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $overhead_cost_total ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h5 class="display-">Dates</h5>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                    <div class="row">
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Proposed Start Date</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $proposed_start_date ? date('Y-m-d', strtotime($proposed_start_date)) : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Proposed End Date</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $proposed_end_date ? date('Y-m-d', strtotime($proposed_end_date)) : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Actual Start Date</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $actual_start_date ? date('Y-m-d', strtotime($actual_start_date)) : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style='margin-top:15px'>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Actual End Date</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $actual_end_date ? date('Y-m-d', strtotime($actual_end_date)) : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Filed Date</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $filed_date ? date('Y-m-d', strtotime($filed_date)) : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Complete Date</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $complete_date ? date('Y-m-d', strtotime($complete_date)) : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h5 class="display-">Construction Information</h5>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                    <div class="row">
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Construction Method</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $construction_method ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Furnishing Type</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $furnishing_type ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Warehouse</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $warehouse_id ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style='margin-top:15px'>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Order</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $order_to_construct ? : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Completion Certificate</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $completion_certificate ? : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div href="#" class="kt-notification-v2__item">
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        <h6 class="kt-portlet__head-title kt-font-primary">Instruction Number</h6>
                                    </div>
                                    <div class="kt-notification-v2__item-desc">
                                        <h6 class="kt-portlet__head-title kt-font-dark"><?= $instruction_number ? : 'N/A' ?> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>