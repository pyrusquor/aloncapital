<?php
$equipment_info = $data['equipment_info'] ? $data['equipment_info'] : [];
?>
<div class="row">
    <div class="col-md-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Equipment Info
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                        <a class="btn btn-label-primary btn-elevate btn-sm btn-filter" href='<?= base_url('construction_order/equipment_info_form/' . $data['id']) ?>' aria-expanded="true">
                            <i class="fas fa-snowplow"></i> Edit Equipment Info
</a>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div class="kt-widget13">
                        <?php if ($equipment_info) : ?>
                            <table class="table table-striped- table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Hours
                                        </th>
                                    </tr>
                                    <?php foreach ($equipment_info as $key => $info) :?>
                                        <tr>
                                            <td>
                                                <?= $info['fixed_asset']['name'] ?>
                                            </td>
                                            <td>
                                                <?=$info['date'] ?>
                                            </td>
                                            <td>
                                                <?= $info['hours'] ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <h3 class="display-1">No Equipment Found!</h3>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-6">

    </div>
</div>