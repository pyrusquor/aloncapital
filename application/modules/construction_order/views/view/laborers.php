<?php
$labor_info = $data['labor_info'] ? $data['labor_info'] : [];
?>
<div class="row">
    <div class="col-md-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Labor Info
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-actions">
                        <a class="btn btn-label-primary btn-elevate btn-sm btn-filter" href='<?= base_url('construction_order/labor_info_form/' . $data['id']) ?>' aria-expanded="true">
                            <i class="fas fa-users"></i> Edit Laborer Info
</a>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div class="kt-widget13">
                        <?php if ($labor_info) : ?>
                            <table class="table table-striped- table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Laborer Type
                                        </th>
                                        <th>
                                            Task
                                        </th>
                                        <th>
                                            Daily Pay
                                        </th>
                                    </tr>
                                    <?php foreach ($labor_info as $key => $info) :
                                        $time_in = strtotime($info['time_in']);
                                        $time_out = strtotime($info['time_out']);
                                        $difference = round(abs($time_out - $time_in) / 3600, 2);
                                        $daily_pay = $difference * $info['laborer']['hour_rate'];
                                    ?>
                                        <tr>
                                            <td>
                                                <?= $info['laborer']['first_name'] . ' ' . $info['laborer']['last_name'] ?>
                                            </td>
                                            <td>
                                                <?= Dropdown::get_static('laborer_type', $info['laborer']['laborer_type'], 'view') ?>
                                            </td>
                                            <td>
                                                <?= $info['task'] ?>
                                            </td>
                                            <td>
                                                <?= money_php($daily_pay) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <h3 class="display-1">No Laborers Found!</h3>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-6">

    </div>
</div>