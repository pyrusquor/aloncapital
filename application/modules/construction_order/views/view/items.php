<div class="row">
    <div class="col-md-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        General Information
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div class="kt-widget13">
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Names
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $name; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-6">

    </div>
</div>