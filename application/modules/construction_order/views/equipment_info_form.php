<!-- CONTENT HEADER -->
<?php
$info ? extract($info) : '';
?>
<script type='text/javascript'>
    var timeNow = <?= date("Y-m-d", strtotime(NOW)) ?>
</script>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Equipment Info</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a onclick="goBack()" class="btn btn-label-instagram"><i class="fas fa-reply"></i>
                    Back</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                <form class="kt-form" method="POST" action="" id="equipment_info_form" enctype="multipart/form-data">
                    <input type='hidden' value='<?= $id ?>' id='construction_order_id' name='order[construction_order_id]'>
                    <input type='hidden' value='<?= $original_ids ?>' name='original_ids'>
                    <div class='row'>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Project<span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon">
                                    <select class="form-control suggests" data-module="projects" name="order[project_id]" required>
                                        <option value="">Select Project</option>
                                        <?php if ($project) : ?>
                                            <option value="<?= $project['id'] ?>" selected><?= $project['name']; ?></option>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Property</label>
                                <div class="kt-input-icon">
                                    <select class="form-control suggests" data-module="properties" name="order[property_id]">
                                        <option value="">Select Property</option>
                                        <?php if ($property) : ?>
                                            <option value="<?= $property['id'] ?>" selected><?= $property['name']; ?></option>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                    <div class="kt-section kt-section--first">
                        <h3 class="kt-section__title">Equipment</h3>
                        <div class='repeater'>
                            <div data-repeater-list="equipment">
                                <?php if($equipment_info): 
                                    foreach($equipment_info as $key => $equipment):
                                        extract($equipment);
                                        ?>
                                <div data-repeater-item class='row'>
                                    <div class="col-sm-5">
                                    <input type='hidden' name='equipment[id]' value ='<?= $id ?>'>

                                        <div class="form-group">
                                            <label>Fixed Asset Name <span class="kt-font-danger">*</span></label>
                                            <div class="kt-input-icon">
                                                <select class="form-control suggests" data-module="fixed_assets" name="equipment[fixed_asset_id]" required>
                                                    <option value="<?= $fixed_asset['id']; ?>" selected><?= $fixed_asset['name']; ?></option>
                                                </select>
                                                <span class="kt-input-icon__icon"></span>

                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Date<span class="kt-font-danger">*</span></label>
                                            <div class="kt-input-icon">
                                                <input type="text" class="form-control kt_datepicker" placeholder="Date of Request" name="equipment[date]" value="<?= $date ?>" readonly>
                                                <span class="kt-input-icon__icon"></span>
                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Hours<span class="kt-font-danger">*</span></label>
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <div class="kt-input-icon">
                                                        <input type="number" class="form-control" placeholder="Hours" name="equipment[hours]" value="<?= $hours ?>" required>
                                                        <span class="kt-input-icon__icon"></span>
                                                    </div>
                                                    <span class="form-text text-muted"></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach ?>
                                <?php else: ?>
                                    <div data-repeater-item class='row'>
                                    <input type='hidden' name='equipment[id]' val =''>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Name <span class="kt-font-danger">*</span></label>
                                            <div class="kt-input-icon">
                                                <select class="form-control suggests" data-module="fixed_assets" name="equipment[fixed_asset_id]" required></select>
                                                <span class="kt-input-icon__icon"></span>

                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Date<span class="kt-font-danger">*</span></label>
                                            <div class="kt-input-icon">
                                                <input type="text" class="form-control kt_datepicker" placeholder="Date of Request" name="equipment[date]" value="<?= date("Y-m-d", strtotime(NOW)) ?>" readonly>
                                                <span class="kt-input-icon__icon"></span>
                                            </div>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Hours<span class="kt-font-danger">*</span></label>
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <div class="kt-input-icon">
                                                        <input type="number" class="form-control" placeholder="Hours" name="equipment[hours]" value="" required>
                                                        <span class="kt-input-icon__icon"></span>
                                                    </div>
                                                    <span class="form-text text-muted"></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif ?>
                            </div>
                            <div class="form-group form-group-last row">
                                <div class="offset-10"></div>
                                <div class="col-lg-2">
                                    <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                        <i class="la la-plus"></i> Equipment
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class='col-lg-12 kt-align-right'>
                            <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
                                Submit
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>