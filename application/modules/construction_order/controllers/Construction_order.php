<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Construction_order extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Construction_order_model', 'M_Construction_order');
        $this->load->model('Construction_order_labor_info_model', 'M_Construction_order_labor_info');
        $this->load->model('Construction_order_equipment_info_model', 'M_Construction_order_equipment_info');
        $this->load->model('Construction_order_overhead_cost_model', 'M_Construction_order_overhead_cost');
        $this->load->model('construction_order_item/Construction_order_item_model', 'M_Construction_order_item');
        $this->load->model('company/company_model', 'M_company');
        $this->load->model('warehouse/warehouse_model', 'M_warehouse');
        $this->load->model('item/item_model', 'M_item');
        $this->load->model('item_abc/item_abc_model', 'M_item_abc');
        $this->load->model('item_brand/item_brand_model', 'M_item_brand');
        $this->load->model('item_class/item_class_model', 'M_item_class');
        $this->load->model('item_group/item_group_model', 'M_item_group');
        $this->load->model('item_type/item_type_model', 'M_item_type');

        $this->load->helper('form');

        $this->_table_fillables = $this->M_Construction_order->fillable;
        $this->_table_columns = $this->M_Construction_order->__get_columns();
        $this->create_info = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $this->update_info = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Construction_order->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Construction_order->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'cor_number' => true,
            'company' => true,
            'project' => true,
            'property' => true,
            'filed_date' => true,
            'proposed_start_date' => true,
            'proposed_finish_date' => true,
            'actual_start_date' => true,
            'actual_finish_date' => true,
            'status' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Construction_order
                ->with_company(['fields'=>'name'])
                ->with_project(['fields'=>'name'])
                ->with_property(['fields'=>'name'])
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("reference like '%$searchValue%'");
                $query->or_where("total_amount like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'reference' => [
                    'data' => $filters['reference'] ?? null,
                    'operator' => 'like',
                ],
                'total_amount' => [
                    'data' => $filters['total_amount'] ?? null,
                    'operator' => 'like',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();


                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {
                        $items[$key]['company'] = isset($value['company']) ? '<div><a href="/company/view/' . $value['company']['id'] . '" target="_blank">' . $value['company']['name'] . '</a><div></div></div>' : 'N/A';
                        $items[$key]['project'] = isset($value['project']) ? '<div><a href="/project/view/' . $value['project']['id'] . '" target="_blank">' . $value['project']['name'] . '</a><div></div></div>' : 'N/A';
                        $items[$key]['property'] = isset($value['property']) ? '<div><a href="/company/view/' . $value['property']['id'] . '" target="_blank">' . $value['property']['name'] . '</a><div></div></div>' : 'N/A';

                        $items[$key]['filed_date'] = $value['filed_date'] ? date("Y-m-d",strtotime($value['filed_date'])) : 'N/A' ;
                        $items[$key]['proposed_start_date'] = $value['proposed_start_date'] ? date("Y-m-d",strtotime($value['proposed_start_date'])) : 'N/A' ;
                        $items[$key]['proposed_finish_date'] = $value['proposed_finish_date'] ? date("Y-m-d",strtotime($value['proposed_finish_date'])) : 'N/A' ;
                        $items[$key]['actual_start_date'] = $value['actual_start_date'] ? date("Y-m-d",strtotime($value['actual_start_date'])) : 'N/A' ;
                        $items[$key]['actual_finish_date'] = $value['actual_finish_date'] ? date("Y-m-d",strtotime($value['actual_finish_date'])) : 'N/A' ;

                        $items[$key]['status'] = Dropdown::get_static('construction_order_status',$value['status'],'view');
                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    } 
                }
            }
        }

        $totalRecords = $this->M_Construction_order->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {
            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Construction_order->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                $data['company_id'] = $info['company_id'];
                $data['status'] = $info['status'];
                $data['customer_code'] = $info['customer_code'];
                $data['cor_number'] = uniqidReal();
                $data['completion_certificate'] = $info['completion_certificate'];
                $data['customer_code'] = $info['customer_code'];
                $data['instruction_number'] = $info['instruction_number'];
                $data['filed_date'] = $info['filed_date'];
                $data['agent'] = $info['agent'];
                $data['warehouse_id'] = $info['warehouse_id'];
                $data['project_id'] = $info['project_id'];
                $data['sub_project_id'] = $info['sub_project_id'];
                $data['property_id'] = $info['property_id'];
                $data['note'] = $info['note'];

                $data['furnishing_type'] = $info['furnishing_type'];
                $data['construction_method'] = $info['construction_method'];
                $data['actual_finish_date'] = $info['actual_finish_date'];
                $data['actual_start_date'] = $info['actual_start_date'];
                $data['proposed_finish_date'] = $info['proposed_finish_date'];
                $data['proposed_start_date'] = $info['proposed_start_date'];

                $item = $info['item'];

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $construction_order_status = $this->M_Construction_order->update($info + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $construction_order_status = $this->M_Construction_order->insert($data + $additional);

                    if ($item) {
                        foreach ($item as $key => $value) {
                            $items['order_id'] = $construction_order_status;
                            $items['item_id'] = $value['item_id'];
                            $items['item_name'] = $value['item_name'];
                            $items['item_code'] = $value['item_code'];
                            $items['unit_of_measurement'] = $value['unit_of_measurement'];
                            $items['quantity'] = $value['quantity'];
                            $items['amenity'] = $value['amenity'];
                            $items['sub_amenity'] = $value['sub_amenity'];

                            $construction_order_item = $this->M_Construction_order_item->insert($items + $additional);
                        }
                    }
                }

                if ($construction_order_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Construction Order Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_Construction_order->get($id);
        }

        $this->view_data['item_abc'] = $this->M_item_abc->get_all();
        $this->view_data['item_group'] = $this->M_item_group->get_all();
        $this->view_data['item_type'] = $this->M_item_type->get_all();

        $this->view_data['method'] = $method;
        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Construction_order->get($id);
            if ($list) {

                $deleted = $this->M_Construction_order->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Construction Order Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_Construction_order->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Construction Order Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = FALSE)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $data =  $this->M_Construction_order
                                        ->with_company(['fields'=>'name'])
                                        ->with_project(['fields'=>'name'])
                                        ->with_property(['fields'=>'name'])
                                        ->with_labor_info(['with'=>['relation'=>'laborer','fields'=>'last_name,first_name,hour_rate,laborer_type']])
                                        ->with_equipment_info(['with'=>['relation'=>'fixed_asset','fields'=>'name']])
                                        ->with_overhead_cost(['fields'=>'amount'])
                                        ->get($id);

            if ($data) {
                $data['status'] = $data['status'] ? Dropdown::get_static('construction_order_status',$data['status'],'view') :'N/A';
                $data['construction_method'] = $data['construction_method'] ? Dropdown::get_static('construction_method', $data['construction_method'], 'view') : 'N/A';
                $data['furnishing_type'] = $data['furnishing_type'] ? Dropdown::get_static('option_unit_type', $data['furnishing_type'], 'view') : 'N/A';
                $data['warehouse_id'] = $data['warehouse_id'] ? Dropdown::get_dynamic('warehouses', $data['warehouse_id'],'name','id','view'): 'N/A';

                $labor_total = 0;
                $cost_total = 0;

                if($data['labor_info']){
                    foreach($data['labor_info'] as $l_key => $labor){
                        $time_in = strtotime($labor['time_in']);
                        $time_out = strtotime($labor['time_out']);
                        $difference = round(abs($time_out - $time_in) / 3600, 2);
                        $daily_pay = $difference * $labor['laborer']['hour_rate'];
                        $labor_total += $daily_pay;
                    }
                }

                $data['laborer_total'] = money_php($labor_total);

                if($data['overhead_cost']){
                    foreach($data['overhead_cost'] as $o_key => $cost){
                        $cost_total += $cost['amount'];
                    }
                }

                $data['overhead_cost_total'] = money_php($cost_total);
                $data['laborer_total'] = money_php($labor_total);


                $this->view_data['data'] = $data;
                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        /* ==================== begin: Add model fields ==================== */
                        'company_id' => $upload_data[0],
                        'cor_number' => $upload_data[1],
                        'customer_code' => $upload_data[2],
                        'order_to_construct' => $upload_data[3],
                        'completion_certificate' => $upload_data[4],
                        'instruction_number' => $upload_data[5],
                        'filed_date' => $upload_data[6],
                        'delivery_date' => $upload_data[7],
                        'agent' => $upload_data[8],
                        'warehouse_id' => $upload_data[9],
                        'note' => $upload_data[10],
                        'complete_date' => $upload_data[11],
                        'turnover_date' => $upload_data[12],
                        'warranty_start' => $upload_data[13],
                        'warranty_end' => $upload_data[14],
                        'project_id' => $upload_data[15],
                        'sub_project_id' => $upload_data[16],
                        'property_id' => $upload_data[17],
                        'status' => $upload_data[18],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $construction_order_id = $upload_data[0];
                    $construction_order = $this->M_Construction_order->get($construction_order_id);

                    if ($construction_order) {
                        $result = $this->M_Construction_order->update($fields, $construction_order_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'company_id' => $upload_data[1],
                            'cor_number' => $upload_data[2],
                            'customer_code' => $upload_data[3],
                            'order_to_construct' => $upload_data[4],
                            'completion_certificate' => $upload_data[5],
                            'instruction_number' => $upload_data[6],
                            'filed_date' => $upload_data[7],
                            'delivery_date' => $upload_data[8],
                            'agent' => $upload_data[8],
                            'warehouse_id' => $upload_data[10],
                            'note' => $upload_data[11],
                            'complete_date' => $upload_data[12],
                            'turnover_date' => $upload_data[13],
                            'warranty_start' => $upload_data[14],
                            'warranty_end' => $upload_data[15],
                            'project_id' => $upload_data[16],
                            'sub_project_id' => $upload_data[17],
                            'property_id' => $upload_data[18],
                            'status' => $upload_data[19],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Construction_order->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'company_id' => $upload_data[0],
                            'cor_number' => $upload_data[1],
                            'customer_code' => $upload_data[2],
                            'order_to_construct' => $upload_data[3],
                            'completion_certificate' => $upload_data[4],
                            'instruction_number' => $upload_data[5],
                            'filed_date' => $upload_data[6],
                            'delivery_date' => $upload_data[7],
                            'agent' => $upload_data[8],
                            'warehouse_id' => $upload_data[9],
                            'note' => $upload_data[10],
                            'complete_date' => $upload_data[11],
                            'turnover_date' => $upload_data[12],
                            'warranty_start' => $upload_data[13],
                            'warranty_end' => $upload_data[14],
                            'project_id' => $upload_data[15],
                            'sub_project_id' => $upload_data[16],
                            'property_id' => $upload_data[17],
                            'status' => $upload_data[18],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Construction_order->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'construction_order');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'construction_order');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued    =    $this->input->post('update_existing_data');

            $_is_update    =    $_ued === '1' ? TRUE : FALSE;

            $_alphas        =    [];
            $_datas            =    [];

            $_titles[]    =    'id';

            $_start    =    3;
            $_row        =    2;

            $_filename    =    'Construction Order CSV Template.csv';

            $_fillables    =    $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'construction_order');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[]    =    $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group    =    $this->M_Construction_order->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title]    =    isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas            =    $this->__get_excel_columns(count($_titles));
            $_xls_columns    =    array_combine($_alphas, $_titles);
            $_firstAlpha    =    reset($_alphas);
            $_lastAlpha        =    end($_alphas);

            $_objSheet    =    $this->excel->getActiveSheet();
            $_objSheet->setTitle('Construction Order');
            $_objSheet->setCellValue('A1', 'CONSTRUCTION ORDER');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style    =    array(
                'font'  => array(
                    'bold'    =>    TRUE,
                    'size'    =>    10,
                    'name'    =>    'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $construction_orders    =    $this->M_Construction_order->as_array()->get_all();
        if ($construction_orders) {

            foreach ($construction_orders as $_lkey => $construction_order) {

                $_datas[$construction_order['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_construction_orders_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'construction_order');
                }
            } else {

                $_filename    =    'list_of_construction_orders_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'construction_order');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($construction_orders as $_lkey => $construction_order) {

                                $_datas[$construction_order['id']][$_title]    =    isset($construction_order[$_column]) && ($construction_order[$_column] !== '') ? Dropdown::get_static('bool', $construction_order[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($construction_orders as $_lkey => $construction_order) {

                                if ($_name === 'status') {

                                    $_datas[$construction_order['id']][$_title]    =    isset($construction_order[$_name]) && $construction_order[$_name] ? Dropdown::get_static('inventory_status', $construction_order[$_name], 'view') : '';
                                } else {

                                    $_datas[$construction_order['id']][$_title]    =    isset($construction_order[$_name]) && $construction_order[$_name] ? $construction_order[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Construction Order');
                $_objSheet->setCellValue('A1', 'LIST OF CONSTRUCTION ORDER');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'construction_order');
            }
        } else {

            $this->notify->error('No Record Found', 'construction_order');
        }
    }

    public function labor_info_form($construction_order_id = 0)
    {

        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $operation = 'Create';

        $post_data = $this->input->post();
        if($post_data){
                $info_data = $post_data['order'];
                $laborers = isset($post_data['laborer_info']) ? $post_data['laborer_info'] : [];
                $original_ids = $post_data['original_ids'] ? explode(',',$post_data['original_ids']) : [];
                $co_labor_info_status = '';
                if($laborers){

                    foreach($laborers as $key => $laborer){
                        $id = $laborer['id'];
                        unset($laborer['id']);
                        if($id){
                            // check if this id was part of the $original_ids. remove it from the array once updated.
                            $operation = 'Update';
                            $co_labor_info_status = $this->M_Construction_order_labor_info->update($info_data+$laborer+$this->update_info, $id);
                            $index = array_search($id, $original_ids);
                            if ($index !== FALSE) {
                                unset($original_ids[$index]);
                            }
                        }else{
                            $co_labor_info_status = $this->M_Construction_order_labor_info->insert($info_data+$laborer+$this->create_info);
                        }
                    }

                }
                // Since the update statement removed the ids that was updated, logically we are left with the deleted ids. Delete these rows one by one from the database.
                
                foreach($original_ids as $oi_key  => $original_id){
                    $co_labor_info_status = $this->M_Construction_order_labor_info->delete($original_id);
                }
                if($co_labor_info_status){
                    $response['status'] = 1;
                    $response['message'] = $operation . 'd Labor Info!';
                }
            echo json_encode($response);
            exit();
        }

        $construction_order = $this->M_Construction_order->with_project(['fields' => 'name'])->with_property(['fields' => 'name'])->with_labor_info(['with' => ['relation' => 'laborer', 'fields' => 'last_name, first_name']])->get($construction_order_id);
        $construction_order ?: redirect(base_url('construction_order'));

        // store the labor info ids of existing labor_info for this construction_order
        //This will be used to determine if a particular row was deleted from the form
        $original_ids = [];
        if ($construction_order['labor_info']) {
            foreach ($construction_order['labor_info'] as $order_key => $labor_info) {
                $original_ids[] = $labor_info['id'];
            }
        }
        $this->view_data['original_ids'] = implode(',', $original_ids);
        $this->view_data['operation'] = $operation;
        $this->view_data['info'] = $construction_order;
        $this->template->build('labor_info_form', $this->view_data);
    }
    public function equipment_info_form($construction_order_id = 0){

        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $operation = 'Create';

        $post_data = $this->input->post();
        if($post_data){
            $info_data = $post_data['order'];
            $equipment_data = isset($post_data['equipment']) ? $post_data['equipment'] : [];
            $original_ids = $post_data['original_ids'] ? explode(',',$post_data['original_ids']) : [];
            $co_equipment_info_status = '';
            if($equipment_data){

                foreach($equipment_data as $key => $equipment){
                    $id = $equipment['id'];
                    unset($equipment['id']);
                    if($id){
                        // check if this id was part of the $original_ids. remove it from the array once updated.
                        $operation = 'Update';
                        $co_equipment_info_status = $this->M_Construction_order_equipment_info->update($info_data+$equipment+$this->update_info, $id);
                        $index = array_search($id, $original_ids);
                        if ($index !== FALSE) {
                            unset($original_ids[$index]);
                        }
                    }else{
                        $co_equipment_info_status = $this->M_Construction_order_equipment_info->insert($info_data+$equipment+$this->create_info);
                    }
                }

            }
            if($original_ids){
                foreach($original_ids as $oi_key  => $original_id){
                    $co_equipment_info_status = $this->M_Construction_order_equipment_info->delete($original_id);
                }
            }

            if($co_equipment_info_status){
                $response['status'] = 1;
                $response['message'] = $operation . 'd Equipment Info!';
            }
            echo json_encode($response);
            exit();
        }

        $construction_order = $this->M_Construction_order->with_project(['fields'=>'name'])->with_property(['fields'=>'name'])->with_equipment_info(['with'=>['relation'=>'fixed_asset','fields'=>'name']])->get($construction_order_id);
        $construction_order ? : redirect(base_url('construction_order'));
        // store the labor info ids of existing labor_info for this construction_order
        //This will be used to determine if a particular row was deleted from the form
        $original_ids = [];
        if($construction_order['equipment_info']){
            foreach($construction_order['equipment_info'] as $order_key => $labor_info){
                $original_ids[] = $labor_info['id'];
            }
        }
        $this->view_data['original_ids'] = implode(',',$original_ids);
        $this->view_data['info'] = $construction_order;
        $this->template->build('equipment_info_form', $this->view_data);
    }

    public function overhead_cost_form($construction_order_id = 0){
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $operation = 'Create';

        $post_data = $this->input->post();
        if($post_data){
            $info_data = $post_data['order'];
            $cost_data = isset($post_data['cost']) ? $post_data['cost'] : [];
            $original_ids = $post_data['original_ids'] ? explode(',',$post_data['original_ids']) : [];
            $co_overhead_cost_status = '';
            if($cost_data){

                foreach($cost_data as $key => $cost){
                    $id = $cost['id'];
                    unset($cost['id']);
                    if($id){
                        // check if this id was part of the $original_ids. remove it from the array once updated.
                        $operation = 'Update';
                        $co_overhead_cost_status = $this->M_Construction_order_overhead_cost->update($info_data+$cost+$this->update_info, $id);
                        $index = array_search($id, $original_ids);
                        if ($index !== FALSE) {
                            unset($original_ids[$index]);
                        }
                    }else{
                        $co_overhead_cost_status = $this->M_Construction_order_overhead_cost->insert($info_data+$cost+$this->create_info);
                    }
                }

            }
            if($original_ids){
                foreach($original_ids as $oi_key  => $original_id){
                    $co_overhead_cost_status = $this->M_Construction_order_overhead_cost->delete($original_id);
                }
            }

            if($co_overhead_cost_status){
                $response['status'] = 1;
                $response['message'] = $operation . 'd Overhead Cost!';
            }
            echo json_encode($response);
            exit();
        }

        $construction_order = $this->M_Construction_order->with_project(['fields'=>'name'])->with_property(['fields'=>'name'])->with_overhead_cost()->get($construction_order_id);
        $construction_order ? : redirect(base_url('construction_order'));
        // store the labor info ids of existing labor_info for this construction_order
        //This will be used to determine if a particular row was deleted from the form
        $original_ids = [];
        if($construction_order['overhead_cost']){
            foreach($construction_order['overhead_cost'] as $order_key => $labor_info){
                $original_ids[] = $labor_info['id'];
            }
        }
        $this->view_data['original_ids'] = implode(',',$original_ids);
        $this->view_data['info'] = $construction_order;
        $this->template->build('overhead_cost_form', $this->view_data);
    }
}
