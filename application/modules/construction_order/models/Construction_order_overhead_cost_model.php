<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Construction_order_overhead_cost_model extends MY_Model {
    public $table = 'construction_order_overhead_cost'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'construction_order_id',
        'amount',
        'description',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
		$this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
		$this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}