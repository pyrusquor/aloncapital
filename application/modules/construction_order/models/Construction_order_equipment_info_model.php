<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Construction_order_equipment_info_model extends MY_Model {
    public $table = 'construction_order_equipment_info'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'construction_order_id',
        'date',
        'fixed_asset_id',
        'project_id',
        'property_id',
        'hours',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
		$this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
		$this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');
		$this->has_one['fixed_asset'] = array('foreign_model' => 'fixed_assets/fixed_assets_model', 'foreign_table' => 'fixed_assets', 'foreign_key' => 'id', 'local_key' => 'fixed_asset_id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}