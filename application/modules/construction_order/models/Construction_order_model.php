<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Construction_order_model extends MY_Model
{
    public $table = 'construction_orders'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'company_id',
        'cor_number',
        'customer_code',
        'order_to_construct',
        'completion_certificate',
        'instruction_number',
        'filed_date',
        'delivery_date',
        'agent',
        'warehouse_id',
        'note',
        'complete_date',
        'proposed_start_date',
        'proposed_finish_date',
        'actual_start_date',
        'actual_finish_date',
        'construction_method',
        'furnishing_type',
        'project_id',
        'sub_project_id',
        'property_id',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'company_id' => array(
            'field' => 'company_id',
            'label' => 'Company ID',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
		$this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
		$this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');
		$this->has_one['company'] = array('foreign_model' => 'company/company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
		$this->has_many['labor_info'] = array('foreign_model' => 'construction_order/Construction_order_labor_info_model', 'foreign_table' => 'construction_order_labor_info', 'foreign_key' => 'construction_order_id', 'local_key' => 'id');
		$this->has_many['equipment_info'] = array('foreign_model' => 'construction_order/Construction_order_equipment_info_model', 'foreign_table' => 'construction_order_equipment_info', 'foreign_key' => 'construction_order_id', 'local_key' => 'id');
		$this->has_many['overhead_cost'] = array('foreign_model' => 'construction_order/Construction_order_overhead_cost_model', 'foreign_table' => 'construction_order_overhead_cost', 'foreign_key' => 'construction_order_id', 'local_key' => 'id');

    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
