<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Material_request_item_model extends MY_Model
    {

        public $table = 'material_request_items'; // you MUST mention the table name
        public $primary_key = 'id'; // you MUST mention the primary key
        public $fillable = [
            'id',
            /* ==================== begin: Add model fields ==================== */
            // relational
            'item_brand_id',
            'item_class_id',
            'item_id',
            'unit_of_measurement_id',
            'material_request_id',
            // additional
            'item_group_id',
            'item_type_id',
            // other fields
            'quantity',
            'unit_cost',
            'request_reason',
            'total_cost',
            /* ==================== end: Add model fields ==================== */
            'created_by',
            'created_at',
            'updated_by',
            'updated_at'
        ]; // If you want, you can set an array with the fields that can be filled by insert/update

        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = TRUE;
            $this->return_as = 'array';

            // Pagination
            $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
            $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;

            // for relationship tables
            // $this->has_many['table_name'] = array();
            $this->has_one['material_request'] = array('foreign_model' => 'material_request/material_request_model', 'foreign_table' => 'material_request', 'foreign_key' => 'id', 'local_key' => 'material_request_id');
            $this->has_one['item_brand'] = array('foreign_model' => 'item_brand/item_brand_model', 'foreign_table' => 'item_brand', 'foreign_key' => 'id', 'local_key' => 'item_brand_id');
            $this->has_one['item_group'] = array('foreign_model' => 'item_group/item_group_model', 'foreign_table' => 'item_group', 'foreign_key' => 'id', 'local_key' => 'item_group_id');
            $this->has_one['item_type'] = array('foreign_model' => 'item_type/item_type_model', 'foreign_table' => 'item_type', 'foreign_key' => 'id', 'local_key' => 'item_type_id');
            $this->has_one['item_class'] = array('foreign_model' => 'item_class/item_class_model', 'foreign_table' => 'item_class', 'foreign_key' => 'id', 'local_key' => 'item_class_id');
            $this->has_one['item'] = array('foreign_model' => 'item/item_model', 'foreign_table' => 'item', 'foreign_key' => 'id', 'local_key' => 'item_id');
            $this->has_one['unit_of_measurement'] = array('foreign_model' => 'inventory_settings_unit_of_measurement/inventory_settings_unit_of_measurement_model', 'foreign_table' => 'inventory_settings_unit_of_measurements', 'foreign_key' => 'id', 'local_key' => 'unit_of_measurement_id');
        }

    }
