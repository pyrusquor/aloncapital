<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Material_request_item extends MY_Controller
{
    private $fields = [
        /* ==================== begin: Add model fields ==================== */
        array(
            'field' => 'request[item_brand_id]',
            'label' => 'Brand',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'request[item_class_id]',
            'label' => 'Class',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'request[item_id]',
            'label' => 'Item',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'request[unit_of_measurement_id]',
            'label' => 'Unit of Measurement',
            'rules' => 'trim|required'
        ),

        array(
            'field' => 'request[quantity]',
            'label' => 'Quantity',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'request[unit_cost]',
            'label' => 'Unit Cost',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'request[request_reason]',
            'label' => 'Reason for Request',
            'rules' => 'trim|required'
        ),

        /* ==================== end: Add model fields ==================== */

    ];

    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Material_request_item_model', 'M_Material_request_item');
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');


        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Material_request_item->fillable;
        $this->_table_columns = $this->M_Material_request_item->__get_columns();
        $this->_table = 'material_request_items';
    }

    public function get_all()
    {
        $data['row'] = $this->M_Material_request_item->get_all();

        echo json_encode($data);
    }

    public function get_all_by_request($id)
    {
        $this->load->model('purchase_order_request_item/Purchase_order_request_item_model', 'M_Purchase_order_request_item');
        $this->load->model('material_receiving_item/Material_receiving_item_model', 'M_Material_receiving_item');
        $this->load->model('material_issuance_item/Material_issuance_item_model', 'M_Material_issuance_item');

        $with_relations = $this->input->get('with_relations');
        $with_relations = $with_relations ? $with_relations : 'yes';
        $with_current_quantity = $this->input->get('with_current_quantity');
        $with_quantity_issued = $this->input->get('with_quantity_issued');

        if ($with_relations === 'yes') {
            $data = $this->M_Material_request_item->where(array('material_request_id' => $id))->with_item_brand()->with_item_group()->with_item_type()->with_item_class()->with_item()->with_unit_of_measurement()->get_all();
        } else {
            $data = $this->M_Material_request_item->where(array('material_request_id' => $id))->get_all();
        }

        if (isset($with_current_quantity) && $with_current_quantity === 'true' && !!$data) {

            foreach ($data as $key => $value) {

                $rpo_items = $this->M_Purchase_order_request_item->where("material_request_id", $value["material_request_id"])->where("item_id", $value["item_id"])->get_all();

                if (!!$rpo_items) {

                    $rpo_quantity_total = 0;

                    foreach ($rpo_items as $_key => $_value) {

                        $rpo_quantity_total += $_value['quantity'];
                    }

                    $quantity = ($value['quantity'] - $rpo_quantity_total);

                    $data[$key]["quantity"] = $quantity < 0 ? 0 : $quantity;
                }
            }
        }

        if (isset($with_quantity_issued) && $with_quantity_issued === "true" && !!$data) {

            foreach ($data as $key => $value) {

                // $mrr_items = $this->M_Material_receiving_item->where("material_request_id", $value["material_request_id"])->where("item_id", $value["item_id"])->get_all();
                $mrr_items = $this->M_Material_receiving_item->where("item_id", $value["item_id"])->get_all();

                if (!!$mrr_items) {

                    $mi_quantity_total = 0;

                    foreach ($mrr_items as $item) {

                        $mi_item = $this->M_Material_issuance_item->where("material_request_id", $value["material_request_id"])->where("material_receiving_item_id", $item["id"])->get();

                        if (!!$mi_item) {

                            $mi_quantity_total += $mi_item["quantity"];
                        }
                    }

                    $data[$key]["quantity_issued"] = $mi_quantity_total < 0 ? 0 : $mi_quantity_total;
                }
            }
        }



        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get_item($mrs_id = null, $item_id = null)
    {
        $this->load->model('purchase_order_request_item/Purchase_order_request_item_model', 'M_Purchase_order_request_item');

        $item = $this->M_Material_request_item->where("material_request_id", $mrs_id)->where("item_id", $item_id)->get();

        $rpo_items = $this->M_Purchase_order_request_item->where("material_request_id", $mrs_id)->where("item_id", $item_id)->get_all();

        if (!!$rpo_items && !!$item) {

            $rpo_quantity_total = 0;

            foreach ($rpo_items as $_key => $_value) {

                $rpo_quantity_total += $_value['quantity'];
            }

            $quantity = ($item['quantity'] - $rpo_quantity_total);

            $item["quantity"] = $quantity < 0 ? 0 : $quantity;
        }

        echo json_encode($item);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // Get record count
        // $conditions['returnType'] = 'count';
        $this->view_data['totalRec'] = $totalRec = $this->M_Material_request_item->count_rows();

        // Pagination configuration
        $config['target'] = '#material_request_item_content';
        $config['base_url'] = base_url('material_request_item/paginationData');
        $config['total_rows'] = $totalRec;
        $config['per_page'] = $this->perPage;
        $config['link_func'] = 'MaterialRequestItemPagination';

        // Initialize pagination library
        $this->ajax_pagination->initialize($config);

        // Get records
        $this->view_data['records'] = $this->material_request_item
            ->limit($this->perPage, 0)
            ->get_all();
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            // Input from Advanced Filter
            $name = $this->input->post('name');
            /* ==================== begin: Add model fields ==================== */

            /* ==================== end: Add model fields ==================== */

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->material_request_item->count_rows();
            $where = array();

            // Pagination configuration
            $config['target'] = '#material_request_item_content';
            $config['base_url'] = base_url('material_request_item/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'MaterialRequestItemPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('last_name', $keyword, 'both');
                $this->db->or_like('first_name', $keyword, 'both');
                $this->db->or_like('middle_name', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($name)) :
                $this->db->group_start();
                $this->db->like('last_name', $name, 'both');
                $this->db->or_like('first_name', $name, 'both');
                $this->db->or_like('middle_name', $name, 'both');
                $this->db->group_end();
            endif;

            $totalRec = $this->material_request_item->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('last_name', $keyword, 'both');
                $this->db->or_like('first_name', $keyword, 'both');
                $this->db->or_like('middle_name', $keyword, 'both');
                $this->db->group_end();
            endif;


            if (!empty($civil_status_id) && !empty($civil_status_id)) :
                $this->db->where('civil_status_id', $civil_status_id);
                $where['civil_status_id'] = $civil_status_id;
            endif;

            $this->view_data['records'] = $records = $this->material_request_item
                ->limit($this->perPage, $offset)
                ->get_all();


            $this->load->view('material_request_item/_filter', $this->view_data, false);
        }
    }

    public function view($id = FALSE)
    {
        $this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

        $this->js_loader->queue([
            '//www.amcharts.com/lib/3/amcharts.js',
            '//www.amcharts.com/lib/3/serial.js',
            '//www.amcharts.com/lib/3/radar.js',
            '//www.amcharts.com/lib/3/pie.js',
            '//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
            '//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
            '//www.amcharts.com/lib/3/plugins/export/export.min.js',
            '//www.amcharts.com/lib/3/themes/light.js'
        ]);

        if ($id) {

            $this->view_data['info'] = $this->material_request_item
                ->get($id);

            if ($this->view_data['info']) {
                $this->template->build('view', $this->view_data);
            } else {
                show_404();
            }
        } else {

            show_404();
        }
    }

    public function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();
            $_input['created_by'] = $this->session->userdata['user_id'];

            $result = $this->material_request_item->from_form()->insert($_input);

            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Material Request Item successfully created.', 'material_request_item');
            }
        }

        $this->template->build('create');
    }

    public function update($id = false)
    {
        if ($id) {

            $this->view_data['material_request_item'] = $data = $this->material_request_item->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();
                    $_input['updated_by'] = $this->session->userdata['user_id'];

                    $result = $this->material_request_item->from_form()->update($_input, $data['id']);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'material_request_item');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Material_request_item->get($id);
            if ($list) {

                $deleted = $this->M_Material_request_item->delete($list['id']);
                if ($deleted !== FALSE) {

                    $response['status'] = 1;
                    $response['message'] = 'Material Request Item successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Material_request_item->delete($value);
                }
                if ($deleted !== FALSE) {

                    $response['status'] = 1;
                    $response['message'] = 'Material Request Item successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $material_request_items = $this->M_Material_request_item->as_array()->get_all();
        if ($material_request_items) {

            foreach ($material_request_items as $_lkey => $material_request_item) {

                $_datas[$material_request_item['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_material_request_items_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'material_request_item');
                }
            } else {

                $_filename = 'list_of_material_request_items_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'material_request_item');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($material_request_items as $_lkey => $material_request_item) {

                                $_datas[$material_request_item['id']][$_title] = isset($material_request_item[$_column]) && ($material_request_item[$_column] !== '') ? Dropdown::get_static('bool', $material_request_item[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($material_request_items as $_lkey => $material_request_item) {

                                if ($_name === 'status') {

                                    $_datas[$material_request_item['id']][$_title] = isset($material_request_item[$_name]) && $material_request_item[$_name] ? Dropdown::get_static('inventory_status', $material_request_item[$_name], 'view') : '';
                                } else {

                                    $_datas[$material_request_item['id']][$_title] = isset($material_request_item[$_name]) && $material_request_item[$_name] ? $material_request_item[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Material Request Item');
                $_objSheet->setCellValue('A1', 'LIST OF MATERIAL REQUEST ITEM');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'material_request_item');
            }
        } else {

            $this->notify->error('No Record Found', 'material_request_item');
        }
    }

    function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? TRUE : FALSE;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Material Request Item CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'material_request_item');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Material_request_item->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Material Request Item');
            $_objSheet->setCellValue('A1', 'MATERIAL REQUEST ITEM');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'name' => $upload_data[1],
                        /* ==================== begin: Add model fields ==================== */

                        /* ==================== end: Add model fields ==================== */
                    );

                    $material_request_item_id = $upload_data[0];
                    $material_request_item = $this->M_Material_request_item->get($material_request_item_id);

                    if ($material_request_item) {
                        $result = $this->M_Material_request_item->update($fields, $material_request_item_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Material_request_item->insert($fields);
                    } else {
                        $fields = array(
                            'name' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Material_request_item->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'material_request_item');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'material_request_item');
        die();
    }
}
