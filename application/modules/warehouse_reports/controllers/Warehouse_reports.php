<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Warehouse_reports extends MY_Controller
{
    public function print($printable_name)
    {
        // $this->template->build('printable/' . $printable_name, $this->view_data);

        $generateHTML = $this->load->view('printable/' . $printable_name, $this->view_data, true);

        echo $generateHTML;
        die();
    }
}