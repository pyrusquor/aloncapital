<?php

$id = isset($sub_warehouse_inventory_logs['id']) && $sub_warehouse_inventory_logs['id'] ? $sub_warehouse_inventory_logs['id'] : '';
$company = isset($sub_warehouse_inventory_logs['company']) && $sub_warehouse_inventory_logs['company'] ? $sub_warehouse_inventory_logs['company'] : '';
$sbu = isset($sub_warehouse_inventory_logs['sbu']) && $sub_warehouse_inventory_logs['sbu'] ? $sub_warehouse_inventory_logs['sbu'] : '';
$warehouse = isset($sub_warehouse_inventory_logs['warehouse']) && $sub_warehouse_inventory_logs['warehouse'] ? $sub_warehouse_inventory_logs['warehouse'] : '';
$item_code = isset($sub_warehouse_inventory_logs['item_code']) && $sub_warehouse_inventory_logs['item_code'] ? $sub_warehouse_inventory_logs['item_code'] : '';
$item_name = isset($sub_warehouse_inventory_logs['item_name']) && $sub_warehouse_inventory_logs['item_name'] ? $sub_warehouse_inventory_logs['item_name'] : '';
$in = isset($sub_warehouse_inventory_logs['in']) && $sub_warehouse_inventory_logs['in'] ? $sub_warehouse_inventory_logs['in'] : '';
$out = isset($sub_warehouse_inventory_logs['out']) && $sub_warehouse_inventory_logs['out'] ? $sub_warehouse_inventory_logs['out'] : '';
$balance = isset($sub_warehouse_inventory_logs['balance']) && $sub_warehouse_inventory_logs['balance'] ? $sub_warehouse_inventory_logs['balance'] : '';
$remarks = isset($sub_warehouse_inventory_logs['remarks']) && $sub_warehouse_inventory_logs['remarks'] ? $sub_warehouse_inventory_logs['remarks'] : '';
$is_active = isset($sub_warehouse_inventory_logs['is_active']) && $sub_warehouse_inventory_logs['is_active'] ? $sub_warehouse_inventory_logs['is_active'] : '';

// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Sub Warehouse Inventory Logs</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('sub_warehouse_inventory_logs/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('sub_warehouse_inventory_logs'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                            <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Company
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $company; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        SBU
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $sbu; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Warehouse
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $warehouse; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Item Code
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $item_code; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Item Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $item_name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        In
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $in; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Out
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $out; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Balance
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $balance; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Remarks
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $remarks; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Is Active
                                    </span>
                                    <span class="kt-widget__data" style="font-weight: 500"><?php echo Dropdown::get_static('warehouse_status', $is_active, 'view'); ?></span>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->

                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
