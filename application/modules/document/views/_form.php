<?php
	$_security_classification_id	=	isset($record['security_classification_id']) && $record['security_classification_id'] ? $record['security_classification_id'] : '';
	$_classification_id						=	isset($record['classification_id']) && $record['classification_id'] ? $record['classification_id'] : '';
	$_relevant_policies						=	isset($record['relevant_policies']) && $record['relevant_policies'] ? $record['relevant_policies'] : '';
	$_is_need_hard_copy						=	isset($record['is_need_hard_copy']) && ($record['is_need_hard_copy'] !== '') ? $record['is_need_hard_copy'] : '';
	$_issuing_party_id						=	isset($record['issuing_party_id']) && $record['issuing_party_id'] ? $record['issuing_party_id'] : '';
	$_access_right_id							=	isset($record['access_right_id']) && $record['access_right_id'] ? explode(',', $record['access_right_id']) : '';
	$_is_retention								=	isset($record['is_retention']) && ($record['is_retention'] !== '') ? $record['is_retention'] : '';
	$_description									=	isset($record['description']) && $record['description'] ? $record['description'] : '';
	$_processor										=	isset($record['processor']) && $record['processor'] ? $record['processor'] : '';
	$_lead_time										=	isset($record['lead_time']) && $record['lead_time'] ? $record['lead_time'] : '';
	$_owner_id										=	isset($record['owner_id']) && $record['owner_id'] ? $record['owner_id'] : '';
	$_name												=	isset($record['name']) && $record['name'] ? $record['name'] : '';
?>

<div class="kt-section kt-section--first">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Document Name <code>*</code></label>
				<div class="kt-input-icon">
					<input type="text" name="name" class="form-control" id="name" placeholder="Document Name" value="<?php echo set_value('name', @$_name);?>" autocomplete="off">
				</div>
				<?php echo form_error('name'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Need hard copy</label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('is_need_hard_copy', Dropdown::get_static('bool'), set_value('is_need_hard_copy', @$_is_need_hard_copy), 'class="form-control" id="is_need_hard_copy"'); ?>
				</div>
				<?php echo form_error('is_need_hard_copy'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Description <code>*</code></label>
				<div class="kt-input-icon">
					<input type="text" name="description" class="form-control" id="description" placeholder="Description" value="<?php echo set_value('description', @$_description);?>" autocomplete="off">
				</div>
				<?php echo form_error('description'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Access Right</label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('access_right_id[]', Dropdown::get_static('access_right'), set_value('access_right_id', @$_access_right_id), 'class="form-control kt-select2" id="access_right_id" name="param" multiple="multiple"'); ?>
				</div>
				<?php echo form_error('access_right_id[]'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Document Classification</label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('classification_id', Dropdown::get_static('classification'), set_value('classification_id', @$_classification_id), 'class="form-control" id="classification_id"'); ?>
				</div>
				<?php echo form_error('classification_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Security Classification</label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('security_classification_id', Dropdown::get_static('security'), set_value('security_classification_id', @$_security_classification_id), 'class="form-control" id="security_classification_id"'); ?>
				</div>
				<?php echo form_error('security_classification_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Lead Time</label>
				<div class="kt-input-icon">
					<div class="input-group">
						<input type="text" name="lead_time" class="form-control" placeholder="Lead Time" value="<?php echo set_value('lead_time', @$_lead_time);?>" autocomplete="off">
						<div class="input-group-append"><span class="input-group-text">Days</span></div>
					</div>										
				</div>
				<?php echo form_error('lead_time'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Issuing Party</label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('issuing_party_id', Dropdown::get_static('parties'), set_value('issuing_party_id', @$_issuing_party_id), 'class="form-control" id="issuing_party_id"'); ?>
				</div>
				<?php echo form_error('issuing_party_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Owner</label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('owner_id', Dropdown::get_static('document_owner'), set_value('owner_id', @$_owner_id), 'class="form-control" id="owner_id"'); ?>
				</div>
				<?php echo form_error('owner_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Relevant Policies</label>
				<div class="kt-input-icon">
					<input type="text" name="relevant_policies" class="form-control" placeholder="Relevant Policies" value="<?php echo set_value('relevant_policies', @$_relevant_policies);?>" autocomplete="off">
				</div>
				<?php echo form_error('relevant_policies'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Processor</label>
				<div class="kt-input-icon">
					<input type="text" name="processor" class="form-control" placeholder="Processor" value="<?php echo set_value('processor', @$_processor);?>" autocomplete="off">
				</div>
				<?php echo form_error('processor'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Retention</label>
				<div class="kt-input-icon">
					<?php echo form_dropdown('is_retention', Dropdown::get_static('bool'), set_value('is_retention', @$_is_retention), 'class="form-control" id="is_retention"'); ?>
				</div>
				<?php echo form_error('is_retention'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<?php if ( FALSE ): ?>

			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-control-label">Formats: </label>
					<label class="form-control-label text-muted"> .doc, .pdf, .jpeg</label>
					<div>
						<span class="btn btn-sm">
							<input type="file" name="" class="">
						</span>
					</div>
					<?php echo form_error('form_file'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ( FALSE ): ?>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-control-label">Retention</label>
					<div class="kt-input-icon">
						<?php echo form_dropdown('is_retention', Dropdown::get_static('bool'), set_value('is_retention', @$_is_retention), 'class="form-control" id="is_retention"'); ?>
					</div>
					<?php echo form_error('is_retention'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>

<?php if ( FALSE ): ?>
	
	<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
	<h3 class="kt-section__title">Additional Custom Fields</h3>
	<div class="kt-section__body">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label class="form-control-label">First Name <code>*</code></label>
					<div class="kt-input-icon">
						<input type="text" name="first_name" class="form-control" placeholder="First Name" value="<?php echo set_value('name', @$_name);?>" autocomplete="off">
					</div>
					<?php echo form_error('name'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label class="form-control-label">Last Name <code>*</code></label>
					<div class="kt-input-icon">
						<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?php echo set_value('name', @$_name);?>" autocomplete="off">
					</div>
					<?php echo form_error('name'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>