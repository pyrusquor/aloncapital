<?php #echo ud($_docx);
	$_id													=	isset($record['id']) && $record['id'] ? $record['id'] : '';
	$_security_classification_id	=	isset($record['security_classification_id']) && $record['security_classification_id'] ? $record['security_classification_id'] : '';
	$_classification_id						=	isset($record['classification_id']) && $record['classification_id'] ? $record['classification_id'] : '';
	$_relevant_policies						=	isset($record['relevant_policies']) && $record['relevant_policies'] ? $record['relevant_policies'] : '';
	$_is_need_hard_copy						=	isset($record['is_need_hard_copy']) && ($record['is_need_hard_copy'] !== '') ? $record['is_need_hard_copy'] : '';
	$_access_right_id							=	isset($record['access_right_id']) && $record['access_right_id'] ? $record['access_right_id'] : '';
	$_issuing_party_id						=	isset($record['issuing_party_id']) && $record['issuing_party_id'] ? $record['issuing_party_id'] : '';
	$_is_retention								=	isset($record['is_retention']) && ($record['is_retention'] !== '') ? $record['is_retention'] : '';
	$_description									=	isset($record['description']) && $record['description'] ? $record['description'] : '';
	$_processor										=	isset($record['processor']) && $record['processor'] ? $record['processor'] : '';
	$_lead_time										=	isset($record['lead_time']) && $record['lead_time'] ? $record['lead_time'] : '';
	$_owner_id										=	isset($record['owner_id']) && $record['owner_id'] ? $record['owner_id'] : '';
	$_name												=	isset($record['name']) && $record['name'] ? $record['name'] : '';
	$_created_at									=	isset($record['created_at']) && $record['created_at'] ? date_format(date_create($record['created_at']), 'F j, Y') : '';
	$_updated_at									=	isset($record['updated_at']) && $record['updated_at'] ? date_format(date_create($record['updated_at']), 'F j, Y') : '';
?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Document</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('document/update/'.$_id);?>" class="btn btn-label-success btn-elevate btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<a href="<?php echo site_url('document');?>" class="btn btn-label-instagram btn-elevate btn-sm">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<!-- begin::Portlet-->
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				GENERAL INFORMATION
			</h3>
		</div>
	</div>

	<div class="kt-portlet__body">
		<div class="kt-section kt-section--first">
			<div class="kt-section__body">				
				<div class="kt-portlet kt-portlet--bordered">
					<div class="kt-section__body">
						<div class="row">
							<div class="col-sm-6">
								<div class="d-flex flex-column bd-highlight mb-3">
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Document Name</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo isset($_name) && $_name ? $_name : '';?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Document Classification</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo Dropdown::get_static('classification', @$_classification_id, 'view');?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Owner</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo Dropdown::get_static('document_owner', @$_owner_id, 'view');?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Retention</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo Dropdown::get_static('bool', @$_is_retention, 'view');?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Access Right</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo Dropdown::get_static('access_right', @$_access_right_id, 'view');?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Issuing Party</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo Dropdown::get_static('parties', @$_issuing_party_id, 'view');?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Created by</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo isset($_created_at) && $_created_at ? $_created_at : '';?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="d-flex flex-column bd-highlight mb-3">
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Description</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo isset($_description) && $_description ? $_description : '';?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Lead Time</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo isset($_lead_time) && $_lead_time ? $_lead_time : '';?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Processor</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo isset($_processor) && $_processor ? $_processor : '';?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Need Hard Copy</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo Dropdown::get_static('bool', @$_is_need_hard_copy, 'view');?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Security Classification</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo Dropdown::get_static('security', @$_security_classification_id, 'view');?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Relevant Policies</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo isset($_relevant_policies) && $_relevant_policies ? $_relevant_policies : '';?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								  <div class="p-3 bd-highlight">
								  	<div class="row">
								  		<div class="col-sm-5">
								  			<h6 class="kt-font-primary">Last Update by</h6>
								  		</div>
								  		<div class="col-sm-7">
								  			<h6 class="kt-portlet__head-title kt-font-dark">
													<?php echo isset($_updated_at) && $_updated_at ? $_updated_at : '';?>
												</h6>
								  		</div>
								  	</div>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end::Portlet