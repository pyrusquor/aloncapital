<div class="form-group row text-center">
	<div class="col-lg-12 col-xl-12">
		<?php if ($ext == '.jpg' || $ext == '.jpeg' || $ext == '.png') : ?>
			<div class="kt-avatar kt-avatar--outline kt-avatar--circle-">
				<?php if (isset($_src) && $_src) : ?>

					<div class="kt-avatar__holder" style="background-image: url(<?php echo $_src; ?>);"></div>
				<?php else : ?>

					<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/img/default/_img_404.jpeg'); ?>);"></div>
				<?php endif; ?>
			</div>
		<?php else : ?>
			<div class="kt-avatar kt-avatar--outline kt-avatar--circle- d-flex align-items-center justify-content-center">
				<b><?php echo $name ?></b>
			</div>
		<?php endif; ?>
	</div>
</div>