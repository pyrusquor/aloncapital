<?php?>

<div class="kt-section kt-section--first">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Document Name <span class="kt-font-danger">*</span></label>
				<div class="kt-input-icon">
					<input type="text" name="name" class="form-control" id="name" placeholder="Document Name" value="" autocomplete="off">
				</div>
				<?php echo form_error('name'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Need hard copy</label>
				<div class="kt-input-icon">
					<select class="form-control" name="is_need_hard_copy">
						<option value="">Yes / No</option>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
				</div>
				<?php echo form_error('is_need_hard_copy'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Description <span class="kt-font-danger">*</span></label>
				<div class="kt-input-icon">
					<input type="text" name="description" class="form-control" id="description" placeholder="Description" value="" autocomplete="off">
				</div>
				<?php echo form_error('description'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Access Right</label>
				<div class="kt-input-icon">
					<select class="form-control kt-selectpicker kt-select2" id="access_right_id" multiple name="access_right_id">
						<option value="">Select Access Right</option>
						<option value="1">Legal Staff</option>
						<option value="2">Legal Head</option>
						<option value="3">Agent</option>
						<option value="4">Sales Admin</option>
						<option value="5">AR Staff</option>
						<option value="6">AR Head</option>
						<option value="7">Titling Officer</option>
						<option value="8">Tax</option>
						<option value="9">Engineering</option>
						<option value="10">Cashier</option>
						<option value="11">Sales Coordinator</option>
						<option value="12">Client</option>
						<option value="13">Credit and Collection</option>
					</select>
				</div>
				<?php echo form_error('access_right_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Document Classification</label>
				<div class="kt-input-icon">
					<select class="form-control" name="classification_id">
						<option value="">Select Document Classification</option>
						<option value="1">Client Document</option>
						<option value="2">Company Document</option>
						<option value="3">Government Document</option>
					</select>
				</div>
				<?php echo form_error('classification_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Security Classification</label>
				<div class="kt-input-icon">
					<select class="form-control" name="security_classification">
						<option value="">Select Security Type</option>
						<option value="1">Confidential</option>
						<option value="2">Highly Confidential</option>
						<option value="3">Restricted</option>
					</select>
				</div>
				<?php echo form_error('security_classification'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Lead Time</label>
				<div class="kt-input-icon">
					<div class="input-group">
						<input type="text" name="lead_time" class="form-control" placeholder="Lead Time" value="" autocomplete="off">
						<div class="input-group-append"><span class="input-group-text">Days</span></div>
					</div>										
				</div>
				<?php echo form_error('lead_time'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Issuing Party</label>
				<div class="kt-input-icon">
					<select class="form-control" name="issuing_party">
						<option value="">Select Issuing Party</option>
						<option value="1">Company</option>
						<option value="2">Client</option>
						<option value="3">City Hall</option>
						<option value="4">Registrar</option>
						<option value="5">Engineer's Office</option>
						<option value="6">Planning Office</option>
						<option value="7">City Assessors</option>
						<option value="8">Capelco</option>
						<option value="9">MRWD</option>
						<option value="10">Bureau of Lands</option>
						<option value="11">Registry of Deeds</option>
					</select>
				</div>
				<?php echo form_error('issuing_party'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Owner</label>
				<div class="kt-input-icon">
					<select class="form-control" name="owner_id">
						<option value="">Select Owner</option>
						<option value="1">Legal Staff</option>
						<option value="2">Legal Head</option>
						<option value="3">Agent</option>
						<option value="4">Sales Admin</option>
						<option value="5">AR Staff</option>
						<option value="6">AR Head</option>
						<option value="7">Titling Officer</option>
						<option value="8">Tax</option>
						<option value="9">Engineering</option>
						<option value="10">Cashier</option>
						<option value="11">Sales Coordinator</option>
						<option value="12">Client</option>
						<option value="13">Credit and Collection</option>
					</select>
				</div>
				<?php echo form_error('owner_id'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Relevant Policies</label>
				<div class="kt-input-icon">
					<input type="text" name="relevant_policies" class="form-control" placeholder="Relevant Policies" value="" autocomplete="off">
				</div>
				<?php echo form_error('relevant_policies'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Processor</label>
				<div class="kt-input-icon">
					<input type="text" name="processor" class="form-control" placeholder="Processor" value="" autocomplete="off">
				</div>
				<?php echo form_error('processor'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Formats: </label>
				<label class="form-control-label text-muted">.doc, .pdf, .jpeg</label>
				<div>
					<span class="btn btn-sm">
						<input type="file" name="form_file" class="">
					</span>
				</div>
				<?php echo form_error('form_file'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="form-control-label">Retention</label>
				<div class="kt-input-icon">
					<select class="form-control" name="retention">
						<option value="">Yes / No</option>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
				</div>
				<?php echo form_error('retention'); ?>
				<span class="form-text text-muted"></span>
			</div>
		</div>
	</div>
</div>

<?php if ( FALSE ): ?>
	
	<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
	<h3 class="kt-section__title">Additional Custom Fields</h3>
	<div class="kt-section__body">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label class="form-control-label">First Name <span class="kt-font-danger">*</span></label>
					<div class="kt-input-icon">
						<input type="text" name="first_name" class="form-control" placeholder="First Name" value="" autocomplete="off">
					</div>
					<?php echo form_error('name'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label class="form-control-label">Last Name <span class="kt-font-danger">*</span></label>
					<div class="kt-input-icon">
						<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="" autocomplete="off">
					</div>
					<?php echo form_error('name'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>