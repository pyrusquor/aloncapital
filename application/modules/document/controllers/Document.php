<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 
 */
class Document extends MY_Controller
{

	function __construct()
	{

		parent::__construct();

		$this->load->model('document/Document_model', 'M_document');

		$this->_table_fillables	=	$this->M_document->fillable;
		$this->_table_columns		=	$this->M_document->__get_columns();
	}

	function index()
	{

		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills); #pd($this->view_data['_fillables']);
		$this->view_data['_columns']		=	$this->__get_columns($_fills); #ud($this->view_data['_columns']);

		$_documents =	$this->M_document->get_all();
		if ($_documents) {

			$this->view_data['_total']	=	count($_documents);
		}

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	function get_documents()
	{

		$_response	=	[
			'iTotalDisplayRecords'	=>	'',
			'iTotalRecords'					=>	'',
			'sColumns'							=>	'',
			'sEcho'									=>	'',
			'data'									=>	'',
		];

		$_total['_displays']	=	0;
		$_total['_records']		=	0;

		$_columns	=	[
			'id'	=>	TRUE,
			'name'	=>	TRUE,
			'description'	=>	TRUE,
			'owner_id'	=>	TRUE,
			'classification_id'	=>	TRUE,
			'created_by' => true,
			'updated_by' => true
			// 'Actions'	=>	FALSE
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {

			$_columns	=	[];

			foreach ($_REQUEST['columnsDef'] as $_dkey => $_def) {

				$_columns[$_def]	=	TRUE;
			}
		}

		$_documents	=	$this->M_document->as_array()->get_all();
		if ($_documents) {

			foreach ($_documents as $key => $value) {

				$_documents[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

				$_documents[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
			}

			$_datas	=	[];

			foreach ($_documents as $_ckkey => $_ck) {

				$_datas[]	=	$this->filterArray($_ck, $_columns);
			}

			$_total['_displays']	=	$_total['_records']	=	count($_datas);

			if (isset($_REQUEST['search'])) {

				$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

				$_total['_displays']	=	$_datas ? count($_datas) : 0;
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {

				foreach ($_REQUEST['columns'] as $_ckey => $_clm) {

					if ($_clm['search']) {

						$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

						$_total['_displays']	=	$_datas ? count($_datas) : 0;
					}
				}
			}

			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

				$_column	=	$_REQUEST['order'][0]['column'];
				$_dir			=	$_REQUEST['order'][0]['dir'];

				$_column = $_column - 1;

				usort($_datas, function ($x, $y) use ($_column, $_dir) {

					$x	=	array_slice($x, $_column, 1);
					$x	=	array_pop($x);

					$y	=	array_slice($y, $_column, 1);
					$y	=	array_pop($y);

					if ($_dir === 'asc') {

						return $x > $y ? TRUE : FALSE;
					} else {

						return $x < $y ? TRUE : FALSE;
					}
				});
			}

			if (isset($_REQUEST['length'])) {

				$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
			}

			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {

				$_temp	=	$_datas;
				$_datas	=	[];

				foreach ($_temp as $key => $_tmp) {

					$_datas[]	=	array_values($_tmp);
				}
			}

			$_secho	=	0;
			if (isset($_REQUEST['sEcho'])) {

				$_secho	=	intval($_REQUEST['sEcho']);
			}

			$_response	=	[
				'iTotalDisplayRecords'	=>	$_total['_displays'],
				'iTotalRecords'					=>	$_total['_records'],
				'sColumns'							=>	'',
				'sEcho'									=>	$_secho,
				'data'									=>	$_datas
			];
		}

		// ud($_response);

		echo json_encode($_response);

		exit();
	}

	function view($_id = FALSE)
	{

		if ($_id) {

			$this->view_data['record']	=	$this->M_document->get($_id);
			if ($this->view_data['record']) {

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function create()
	{

		$_datas	=	[];

		if ($this->input->post()) {

			$_input	=	$this->input->post();

			foreach ($_input as $_ikey => $_int) {

				if (is_array($_int)) {

					$_value	=	implode(',', $_int);

					$_datas[$_ikey]	=	$_value;
				} else {

					$_datas[$_ikey]	=	$_int;
				}
			}

			$_insert	=	$this->M_document->insert($_datas); #lqq(); ud($_insert);
			if ($_insert !== FALSE) {

				// $_did	=	$this->db->insert_id();

				// $this->notify->success('Document Template successfully created.', 'document/update/'.$_did);

				$this->notify->success('Document Template successfully created.', 'document');
			} else {

				$this->notify->error('Oh snap! Please refresh the page and try again.');
			}
		}

		$this->template->build('create', $this->view_data);
	}

	function update($_id = FALSE)
	{

		$_datas	=	[];

		if ($_id) {

			$record	=	$this->M_document->get($_id);
			if ($record) {

				$this->view_data['record']	=	$record;

				if ($this->input->post()) {

					$_input	=	$this->input->post();

					foreach ($_input as $_ikey => $_int) {

						if (is_array($_int)) {

							$_value	=	implode(',', $_int);

							$_datas[$_ikey]	=	$_value;
						} else {

							$_datas[$_ikey]	=	$_int;
						}
					}

					// $_updated	=	$this->M_document->from_form()->update(NULL, $record['id']);
					$_updated	=	$this->M_document->update($_datas, $record['id']); #lqq(); ud($_updated);
					if ($_updated !== FALSE) {

						// $this->notify->success('Document Template successfully updated.');

						$this->notify->success('Document Template successfully updated.', 'document');
					} else {

						$this->notify->error('Oh snap! Please refresh the page and try again.');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function delete()
	{

		$_response['_status']	=	0;
		$_response['_msg']		=	'Oops! Please refresh the page and try again.';

		$_id	=	$this->input->post('id');
		if ($_id) {

			$record	=	$this->M_document->get($_id);
			if ($record) {

				$_deleted	=	$this->M_document->delete($record['id']);
				if ($_deleted !== FALSE) {

					$_response['_status']	=	1;
					$_response['_msg']		=	'Record successfully deleted';
				}
			}
		}

		echo json_encode($_response);

		exit();
	}

	function bulkDelete()
	{

		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_document->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Record/s successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];

		$_titles[]	=	'#';
		// $_titles[]	=	'ID';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		$_documents	=	$this->M_document->as_array()->get_all();
		if ($_documents) {

			foreach ($_documents as $_dkey => $_docx) {

				$_datas[$_docx['id']]['#']	=	$_no;
				// $_datas[$_docx['id']]['ID']	=	isset($_docx['id']) && $_docx['id'] ? $_docx['id'] : $_no;

				$_no++;
			}

			$_filename	=	'list_of_documents_' . date('m_d_y_h-i-s', time()) . '.xls';

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				if ($_export_column) {

					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			} else {

				$_filename	=	'list_of_documents_' . date('m_d_y_h-i-s', time()) . '.csv';

				// $_db_columns	=	$this->M_document->fillable;
				$_db_columns	=	$this->_table_fillables;
				if (!$_db_columns) {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

						if ((strpos($_name, '_id') !== FALSE)) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($_documents as $_dkey => $_docx) {

								if ($_column === 'classification_id') {

									$_datas[$_docx['id']][$_title]	=	isset($_docx[$_column]) && $_docx[$_column] ? Dropdown::get_static('classification', $_docx[$_column], 'view') : '';
								} elseif ($_column === 'owner_id') {

									$_datas[$_docx['id']][$_title]	=	isset($_docx[$_column]) && $_docx[$_column] ? Dropdown::get_static('document_owner', $_docx[$_column], 'view') : '';
								} elseif ($_column === 'security_classification_id') {

									$_datas[$_docx['id']][$_title]	=	isset($_docx[$_column]) && $_docx[$_column] ? Dropdown::get_static('security', $_docx[$_column], 'view') : '';
								} elseif ($_column === 'issuing_party_id') {

									$_datas[$_docx['id']][$_title]	=	isset($_docx[$_column]) && $_docx[$_column] ? Dropdown::get_static('parties', $_docx[$_column], 'view') : '';
								} elseif ($_column === 'access_right_id') {

									$_datas[$_docx['id']][$_title]	=	isset($_docx[$_column]) && $_docx[$_column] ? Dropdown::get_static('access_right', $_docx[$_column], 'view') : '';
								} else {

									$_datas[$_docx['id']][$_title]	=	isset($_docx[$_column]) && $_docx[$_column] ? $_docx[$_column] : '';
								}
							}
						} elseif ((strpos($_name, 'is_') !== FALSE)) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($_documents as $_dkey => $_docx) {

								$_datas[$_docx['id']][$_title]	=	isset($_docx[$_column]) && ($_docx[$_column] !== '') ? Dropdown::get_static('bool', $_docx[$_column], 'view') : '';
							}
						} else {

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($_documents as $_dkey => $_docx) {

								$_datas[$_docx['id']][$_title]	=	isset($_docx[$_name]) && $_docx[$_name] ? $_docx[$_name] : '';
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_firstAlpha	=	reset($_alphas);
				$_lastAlpha		=	end($_alphas);

				foreach ($_xls_columns as $_xkey => $_column) {

					$_title	=	($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

					$_objSheet->setCellValue($_xkey . $_row, $_title);
				}

				$_objSheet->setTitle('List of Documents');
				$_objSheet->setCellValue('A1', 'LIST OF DOCUMENTS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}

				$_style	=	array(
					'font'  => array(
						'bold'	=>	TRUE,
						'size'	=>	10,
						'name'	=>	'Verdana'
					)
				);
				$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
			}
		} else {

			$this->notify->error('No Record Found', 'document');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Document CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$_documents	=	$this->M_document->as_array()->get_all(); #up($_documents);
				if ($_documents) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($_documents as $_dkey => $_docx) {

							$_datas[$_docx['id']][$_title]	=	isset($_docx[$_title]) && ($_docx[$_title] !== '') ? $_docx[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Documents');
			$_objSheet->setCellValue('A1', 'DOCUMENTS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {

							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							if ($_id) {

								$_docx	=	$this->M_document->get($_id);
								if ($_docx) {

									unset($_data['id']);

									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_update	=	$this->M_document->update($_data, $_id);
									if ($_update !== FALSE) {

										$_updated++;
									} else {

										$_failed++;

										break;
									}
								} else {

									$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['created_at']	=	NOW;
									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_insert	=	$this->M_document->insert($_data);
									if ($_insert !== FALSE) {

										$_inserted++;
									} else {

										$_failed++;

										break;
									}
								}
							} else {

								$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['created_at']	=	NOW;
								$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['updated_at']	=	NOW;

								$_insert	=	$this->M_document->insert($_data);
								if ($_insert !== FALSE) {

									$_inserted++;
								} else {

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {

							$this->notify->error('Upload Failde! Please follow upload guide.', 'document');
						} else {

							$this->notify->success($_msg . '.', 'document');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'document');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'document');
			}
		} else {

			$this->notify->error('Something went wrong!', 'document');
		}
	}

	function view_land_document_file()
	{

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'Oops! Please refresh the page and try again.';

		$_id	=	$this->input->post('id');
		if ($_id) {

			$_respo['_status']	=	1;

			$this->view_data['_src']	=	'';

			$this->load->model('document/Land_document_upload_model', 'M_land_uploaded_document');

			$_uploaded_document	=	$this->M_land_uploaded_document->find_all($_id, FALSE, TRUE);
			if ($_uploaded_document) {

				if (file_exists($_uploaded_document->file_src)) {

					$this->view_data['_src']	=	base_url($_uploaded_document->file_src);
				}
			}

			$_respo['_html']	=	$this->load->view('document/_modal/_view_land_process_document_file', $this->view_data, TRUE); #vd($_response);
		}

		echo json_encode($_respo);

		exit();
	}

	function view_transaction_document_file()
	{

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'Oops! Please refresh the page and try again.';

		$_id	=	$this->input->post('id');
		if ($_id) {

			$_respo['_status']	=	1;

			$this->view_data['_src']	=	'';

			$this->load->model('document/Transaction_document_upload_model', 'M_transaction_uploaded_document');

			$_uploaded_document	=	$this->M_transaction_uploaded_document->find_all($_id, FALSE, TRUE);

			if ($_uploaded_document) {

				if (file_exists($_uploaded_document->file_src)) {

					$this->view_data['_src']	=	base_url($_uploaded_document->file_src);
					$this->view_data['name'] = $_uploaded_document->client_name;
					$this->view_data['ext'] = $_uploaded_document->file_ext;

					$_respo['name'] = $_uploaded_document->client_name;
					$_respo['src'] = base_url($_uploaded_document->file_src);
					$_respo['ext'] = $_uploaded_document->file_ext;
					$_respo['_html']	=	$this->load->view('document/_modal/_view_transaction_process_document_file', $this->view_data, TRUE);
				}
			}
		}

		echo json_encode($_respo);

		exit();
	}



	function view_project_document_file()
	{

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'Oops! Please refresh the page and try again.';

		$_id	=	$this->input->post('id');
		if ($_id) {

			$_respo['_status']	=	1;

			$this->view_data['_src']	=	'';

			$this->load->model('document/Project_document_upload_model', 'M_project_uploaded_document');

			$_uploaded_document	=	$this->M_project_uploaded_document->find_all($_id, FALSE, TRUE);
			if ($_uploaded_document) {

				if (file_exists($_uploaded_document->file_src)) {

					$this->view_data['_src']	=	base_url($_uploaded_document->file_src);
				}
			}

			$_respo['_html']	=	$this->load->view('document/_modal/_view_project_process_document_file', $this->view_data, TRUE);
		}

		echo json_encode($_respo);

		exit();
	}
}
