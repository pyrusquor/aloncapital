<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Document_model extends MY_Model {

	public $primary_key	=	'id'; // you MUST mention the primary key
	public $protected		=	['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $fillable		=	[
													'name',
													'description',
													'classification_id',
													'lead_time',
													'owner_id',
													'processor',
													'is_retention',
													'is_need_hard_copy',
													'access_right_id',
													'security_classification_id',
													'issuing_party_id',
													'relevant_policies',
													'created_by',
													'created_at',
													'updated_by',
													'updated_at',
													'deleted_by',
													'deleted_at'
												]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $table				=	'documents'; // you MUST mention the table name
	public $rules				=	[];
	public $_fields			=	[
													'name'	=>	array(
														'field'	=>	'name',
														'label'	=>	'Document Name',
														'rules'	=>	'trim|required|min_length[2]'
													),
													'description'	=>	array(
														'field'	=>	'description',
														'label'	=>	'Document Description',
														'rules'	=>	'trim|required|min_length[2]'
													),
													'classification_id'	=>	array(
														'field'	=>	'classification_id',
														'label'	=>	'Document Classification',
														'rules'	=>	'trim',
													),
													'lead_time'	=>	array(
														'field'	=>	'lead_time',
														'label'	=>	'Lead Time',
														'rules'	=>	'trim|is_natural_no_zero',
													),
													'owner_id'	=>	array(
														'field'	=>	'owner_id',
														'label'	=>	'Owner',
														'rules'	=>	'trim',
													),
													'processor'	=>	array(
														'field'	=>	'processor',
														'label'	=>	'Processor',
														'rules'	=>	'trim|min_length[2]'
													),
													'is_retention'	=>	array(
														'field'	=>	'is_retention',
														'label'	=>	'Option',
														'rules'	=>	'trim',
													),
													'is_need_hard_copy'	=>	array(
														'field'	=>	'is_need_hard_copy',
														'label'	=>	'Option',
														'rules'	=>	'trim',
													),
													'security_classification'	=>	array(
														'field'	=>	'security_classification',
														'label'	=>	'Security Classification',
														'rules'	=>	'trim',
													),
													'issuing_party'	=>	array(
														'field'	=>	'issuing_party',
														'label'	=>	'Issuing Party',
														'rules'	=>	'trim',
													),
													'relevant_policies'	=>	array(
														'field'	=>	'relevant_policies',
														'label'	=>	'Relevant Policies',
														'rules'	=>	'trim',
													)
												];

	function __construct () {

		parent::__construct();

		$this->soft_deletes = TRUE;

		$this->return_as = 'array';

		$this->rules['insert']	=	$this->_fields;
		$this->rules['update']	=	$this->_fields;
	}

	function insert_dummy () {

		require APPPATH.'/third_party/faker/autoload.php';

		$_faker	=	Faker\Factory::create();

		$_datas	=	[];

		for ( $i = 1; $i <= 5; $i++ ) {

			array_push($_datas, [
				'name'				=>	$_faker->name,
				'description'	=>	$_faker->address,
				'classification_id'	=>	rand(1,3),
				'lead_time'	=>	rand(1,365),
				'owner_id'	=>	rand(1,13),
				'processor'	=>	$_faker->lastName,
				'is_retention'	=>	rand(0,1),
				'is_need_hard_copy'	=>	rand(0,1),
				'access_right_id'	=>	rand(1,13),
				'security_classification'	=>	rand(1,3),
				'issuing_party'	=>	rand(1,11),
				'relevant_policies'	=>	$_faker->lastName,
				'created_by'	=>	rand(1,6),
				'created_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'updated_by'	=>	rand(1,6),
				'updated_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time()))
			]);
		}

		$this->db->insert_batch($this->table, $_datas);
	}
}