<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Communication_setting_model extends MY_Model
{
    public $table = 'communication_settings'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'name',
        'days_to_send',
        'days_to_send_type',
        'period_id',
        'setting_slug',
        'sms',
        'sms_template_id',
        'email',
        'email_template_id',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
    public $fields = [
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'days_to_send',
            'label' => 'Days to Send',
            'rules' => 'trim',
        ),
        array(
            'field' => 'days_to_send_type',
            'label' => 'Days to Send Type',
            'rules' => 'trim',
        ),
        array(
            'field' => 'period_id',
            'label' => 'Period ID',
            'rules' => 'trim',
        ),
        array(
            'field' => 'setting_slug',
            'label' => 'Setting Slug',
            'rules' => 'trim',
        ),
        array(
            'field' => 'sms',
            'label' => 'sms',
            'rules' => 'trim',
        ),
        array(
            'field' => 'sms_template_id',
            'label' => 'SMS Template ID',
            'rules' => 'trim',
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim',
        ),
        array(
            'field' => 'email_template_id',
            'label' => 'Email Template ID',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert']	=	$this->fields;
        $this->rules['update']	=	$this->fields;
        
        $this->has_one['sms_template'] = array('foreign_model'=>'communication_templates/communication_template_model', 'foreign_table'=>'communication_templates', 'foreign_key'=>'id', 'local_key'=>'sms_template_id');
        $this->has_one['email_template'] = array('foreign_model'=>'communication_templates/communication_template_model', 'foreign_table'=>'communication_templates', 'foreign_key'=>'id', 'local_key'=>'email_template_id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}