<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Communication_model extends MY_Model {

	public $table = 'communications'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'communication_group_id',
		'date_sent',
		'communication_title',
		'recipient_id',
		'recipient_table',
		'recipient_address',
		'recipient_mobile',
		'medium_type',
		'is_sent',
		'content',
		'template_id',
		'status',
		'sent_type',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update


	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		// $this->has_one['setting'] = array('foreign_model'=>'communication/communication_setting_model','foreign_table'=>'communication_settings','foreign_key'=>'id','local_key'=>'setting_id');
		
		$this->has_one['template'] = array('foreign_model'=>'communication/communication_template_model','foreign_table'=>'communication_templates','foreign_key'=>'id','local_key'=>'template_id');

		$this->has_one['buyer'] = array('foreign_model'=>'buyer/Buyer_model','foreign_table'=>'buyers','foreign_key'=>'id','local_key'=>'recipient_id');
		$this->has_one['seller'] = array('foreign_model'=>'seller/Seller_model','foreign_table'=>'sellers','foreign_key'=>'id','local_key'=>'recipient_id');

		$this->has_one['send'] = array('foreign_model'=>'communication/Communication_group_model','foreign_table'=>'communication_groups','foreign_key'=>'id','local_key'=>'communication_group_id');

	}

	public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
