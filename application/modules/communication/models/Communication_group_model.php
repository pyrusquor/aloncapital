<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Communication_group_model extends MY_Model {

	public $table = 'communication_groups'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'communication_setting_id',
		'communication_template_id',
		'send_schedule_date',
		'medium_type',
		'sent_type',
		'status',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['setting'] = array('foreign_model'=>'communication/communication_setting_model','foreign_table'=>'communication_settings','foreign_key'=>'id','local_key'=>'communication_setting_id');

		$this->has_one['template'] = array('foreign_model'=>'communication/communication_template_model','foreign_table'=>'communication_templates','foreign_key'=>'id','local_key'=>'communication_template_id');

		$this->has_many['communication'] = array('foreign_model'=>'communication/communication_model','foreign_table'=>'communications','foreign_key'=>'communication_group_id','local_key'=>'id');

	}

	public function get_search_communication_group($date_range=[],$medium_type=''){
		$result = [];
        if(count($date_range)>1){
            $this->db->where("communication_groups.send_schedule_date between '$date_range[0]' and '$date_range[1]'");
        }
		if($medium_type){
            $this->db->where("communication_groups.medium_type",$medium_type);
		}
        $this->db->select('*');
        $this->db->select('communication_templates.name as template_name');
        $this->db->select('communication_groups.id as id');
        $this->db->join('communication_templates','communication_groups.communication_template_id=communication_templates.id','inner');
        $this->db->where('communication_groups.deleted_at is null');
        $query = $this->db->get('communication_groups')->result_array();

        if($query){
            foreach($query as $key=>$value){
				$comms = $this->db->query(
					'SELECT count(id) as count
					from communications
					where deleted_at is null and
					communication_group_id='. $value['id']
				)->result_array();
				$query[$key]['comm_count'] =  $comms[0]['count'];
			}
			$result = $query;
        }
        return $result;
	}

}
