<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Communication extends MY_Controller
{

	private $fields = [
		array(
			'field' => 'info[send_schedule_date]',
			'label' => 'Schedule Date',
			'rules' => 'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();

		$models = array(
			'Communication_group_model' => 'M_comm_group',
			'Communication_model' => 'M_communication',
			'Communication_templates/Communication_template_model' => 'M_comm_template',
			'communication_settings/Communication_setting_model' => 'M_communication_setting',
			'app_global/app_global_model' => 'M_app_global'
		);

		// Load models
		$this->load->model('user/User_model', 'M_user');
		$this->load->model($models);

		// Load pagination library 
		$this->load->library('ajax_pagination');

		// Format Helper
		$this->load->helper(['format', 'images']); // Load Helper

		$this->load->helper('inflector');
		// Per page limit 
		$this->perPage = 12;

		$this->_table_fillables		=	$this->M_communication->fillable;
		$this->_table_columns		=	$this->M_communication->__get_columns();

		$this->u_additional = [
			'updated_by' => $this->user->id,
			'updated_at' => NOW
		];

		$this->additional = [
			'created_by' => $this->user->id,
			'created_at' => NOW
		];
	}

	public function balance()
	{
		$a = $this->communication_library->sms_account();
		vdebug($a);
	}

	public function index($type = '')
	{
		// $_fills	=	$this->_table_fillables;
		// $_colms	=	$this->_table_columns;

		// $this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
		// $this->view_data['_columns']		=	$this->__get_columns($_fills);

		// // Get record count 
		// // $conditions['returnType'] = 'count'; 
		// $this->view_data['totalRec'] = $totalRec = $this->M_comm_group->count_rows();

		// // Pagination configuration 
		// $config['target']      = '#communicationContent';
		// $config['base_url']    = base_url('communication/paginationData');
		// $config['total_rows']  = $totalRec;
		// $config['per_page']    = $this->perPage;
		// $config['link_func']   = 'CommunicationPagination';

		// // Initialize pagination library 
		// $this->ajax_pagination->initialize($config);




		// // Get records 
		// $this->view_data['records'] = $this->M_comm_group->with_setting()->with_template()
		// 								->with_communication('fields:*count*')->order_by('id','DESC')
		// 								->limit($this->perPage, 0)
		// 								->get_all();

		// $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		// $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		// $this->template->build('index', $this->view_data);

		// 		Get record count 
		// $conditions['returnType'] = 'count'; 

		// SMS Account
		$sms_account = $this->get_sms_account();
		$account = json_decode($sms_account);
		$balance = 0;

		if ($account) {
			$this->view_data['sms_credits'] = $account->credit_balance;
		} else {
			$this->view_data['sms_credits'] = $balance;
		}

		$_fills = $this->_table_fillables;
		$_colms = $this->_table_columns;
		$this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns'] = $this->__get_columns($_fills);
		$db_columns = $this->M_communication->get_columns();
		if ($db_columns) {
			$column = [];
			foreach ($db_columns as $key => $dbclm) {
				$name = isset($dbclmn) && $dbclmn ? $dbclmn : '';
				if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
					if (strpos($name, '_id') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('_id', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} elseif (strpos($name, 'is_') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('is_', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} else {
						$column[$key]['value'] = $name;
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					}
				} else {
					continue;
				}
			}
			$column_count = count($column);
			$cceil = ceil(($column_count / 2));
			$this->view_data['columns'] = array_chunk($column, $cceil);
			$column_group = $this->M_communication->count_rows();
			if ($column_group) {
				$this->view_data['total'] = $column_group;
			}
		}
		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);
		$this->template->build('index', $this->view_data);
	}

	public function showCommunications()
	{

		$output = ['data' => ''];

		$columnsDefault = [
			'id' => true,
			'communication_title' => true,
			'medium_type' => true,
			'sent_type' => true,
			'date_sent' => true,
			'recipient_id' => true,
			'status' => true,
			'created_by' => true,
			'updated_by' => true
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}
		$send_date = $this->input->post('scheduled_date');
		$communication_type = $this->input->post('communication_type');
		$date_range = [];

		if (!empty($send_date)) {
			$date_range = explode('-', $send_date);

			$from_str   = strtotime($date_range[0]);
			$from       = date('Y-m-d H:i:s', $from_str);

			$to_str     = strtotime($date_range[1] . '23:59:59');
			$to         = date('Y-m-d H:i:s', $to_str);
			$date_range = array($from, $to);
		}

		// get all raw data
		// $communication = $this->M_comm_group->with_communication('fields:*count*')->with_template()->as_array()->get_all();
		$communication = $this->M_comm_group->get_search_communication_group($date_range, $communication_type);
		$data = [];

		if ($communication) {

			foreach ($communication as $key => $entry) {

				// $date_sent = isset($entry['send']) && $entry['send'] ? $entry['send']['send_schedule_date'] : 'N/A';

				$communication[$key]['communication_title'] = isset($entry['template_name']) && $entry['template_name'] ? $entry['template_name'] : 'N/A';
				$communication[$key]['date_sent'] = view_date($entry['send_schedule_date']);

				$communication[$key]['recipient_id'] = isset($entry['comm_count']) && $entry['comm_count'] ? $entry['comm_count'] : 'N/A';
				// $communication[$key]['recipient_id'] = isset($entry['communication']) && $entry['communication'] ? $entry['communication'][0]['counted_rows'] : 'N/A';

				$communication[$key]['created_by'] = '<div><a href="/user/view/' . $entry['created_by'] . '" target="_blank">' . get_person_name($entry['created_by'], "users") . '</a><div>' . ($entry['created_at'] ? view_date($entry['created_at']) : '') . '</div></div>';

				$communication[$key]['updated_by'] = '<div><a href="/user/view/' . $entry['updated_by'] . '" target="_blank">' . get_person_name($entry['updated_by'], "users") . '</a><div>' . ($entry['updated_at'] ? view_date($entry['updated_at']) : '') . '</div></div>';
			}

			foreach ($communication as $d) {
				$data[] = $this->filterArray($d, $columnsDefault);
			}

			// count data
			$totalRecords = $totalDisplay = count($data);

			// filter by general search keyword
			if (isset($_REQUEST['search'])) {
				$data = $this->filterKeyword($data, $_REQUEST['search']);
				$totalDisplay = count($data);
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
				foreach ($_REQUEST['columns'] as $column) {
					if (isset($column['search'])) {
						$data = $this->filterKeyword($data, $column['search'], $column['data']);
						$totalDisplay = count($data);
					}
				}
			}

			// sort
			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
				$column = $_REQUEST['order'][0]['column'] - 1;
				$dir = $_REQUEST['order'][0]['dir'];

				usort($data, function ($a, $b) use ($column, $dir) {
					$a = array_slice($a, $column, 1);
					$b = array_slice($b, $column, 1);
					$a = array_pop($a);
					$b = array_pop($b);

					if ($dir === 'asc') {
						return $a > $b ? true : false;
					}

					return $a < $b ? true : false;
				});
			}

			// pagination length
			if (isset($_REQUEST['length'])) {
				$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
			}

			// return array values only without the keys
			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
				$tmp = $data;
				$data = [];
				foreach ($tmp as $d) {
					$data[] = array_values($d);
				}
			}
			$secho = 0;
			if (isset($_REQUEST['sEcho'])) {
				$secho = intval($_REQUEST['sEcho']);
			}

			$output = array(
				'sEcho' => $secho,
				'sColumns' => '',
				'iTotalRecords' => $totalRecords,
				'iTotalDisplayRecords' => $totalDisplay,
				'data' => $data,
			);
		}


		echo json_encode($output);
		exit();
	}

	public function paginationData()
	{
		if ($this->input->is_ajax_request()) {

			// Input from General Search
			$keyword = $this->input->get('keyword');
			$page = $this->input->get('page');

			// Input from Advanced Filter
			$values = $this->input->get();
			$offset = 0;


			if ($page) {
				$offset = $page;
			}

			$totalRec = $this->M_comm_group->count_rows();
			$where = array();

			// Pagination configuration 
			$config['target']      = '#communicationContent';
			$config['base_url']    = base_url('communication/paginationData');
			$config['total_rows']  = $totalRec;
			$config['per_page']    = $this->perPage;
			$config['link_func']   = 'CommunicationPagination';

			unset($values['page']);
			unset($values['keyword']);

			// Query
			if ($values) {
				foreach ($values as $key => $n_value) {
					if ($n_value)
						$this->db->where($key, $n_value);
				}
			}

			$totalRec = $this->M_comm_group->count_rows();

			// Pagination configuration 
			$config['total_rows']  = $totalRec;

			// Initialize pagination library 
			$this->ajax_pagination->initialize($config);

			// Query
			if ($values) {
				foreach ($values as $key => $n_value) {
					if ($n_value)
						$this->db->where($key, $n_value);
				}
			}
			$this->view_data['records'] = $records = $this->M_comm_group->with_setting()->with_template()
				->with_communication('fields:*count*')->order_by('id', 'DESC')
				->limit($this->perPage, 0)
				->get_all();

			$this->load->view('transaction/_filter', $this->view_data, false);
		}
	}

	public function view($id = FALSE, $type = '')
	{

		if ($id) {

			$this->view_data['info'] =  $this->M_comm_group
				->with_setting()
				->with_template()
				->with_communication()->order_by('id', 'DESC')
				->limit($this->perPage, 0)
				->get($id);

			if ($this->view_data['info']) {

				if ($type == "debug") {

					vdebug($this->view_data['info']);
				}

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			redirect('transaction');
		}
	}

	public function form($id = FALSE, $type = "")
	{
		$this->js_loader->queue(['tinymce/tinymce.min.js']);

		$method = "Create";
		if ($id) {
			$method = "Update";
		}

		if ($this->input->post()) {

			$response['status']	= 0;
			$response['msg'] = 'Oops! Please refresh the page and try again.';

			$this->form_validation->set_rules($this->fields);

			if ($this->form_validation->run() === TRUE) {

				$post = $this->input->post();

				$this->save($post, $id);

				$this->db->trans_complete(); # Completing transaction

				/*Optional*/
				if ($this->db->trans_status() === FALSE) {
					# Something went wrong.
					$this->db->trans_rollback();
					$response['status']	= 0;
					$response['message'] = 'Error!';
				} else {
					# Everything is Perfect. 
					# Committing data to the database.
					$this->db->trans_commit();
					$response['status']	= 1;
					$response['message'] = 'Communication Successfully ' . $method . 'd!';
				}
			} else {
				$response['status']	 =	0;
				$response['message'] = validation_errors();
			}

			echo json_encode($response);
			exit();
		}

		$this->view_data['method'] = $method;
		$this->view_data['type'] = $type;

		if ($id) {
			$this->view_data['info'] = $this->M_comm_group
				->with_setting()
				->with_template()
				->with_communication()
				->order_by('id', 'DESC')
				->limit($this->perPage, 0)
				->get($id);
		}

		$this->template->build('form', $this->view_data);
	}

	public function save($post = array(), $id)
	{
		$info = $post['info'];
		$recipients = isset($info['all_recipients']) ? $info['all_recipients'] : [];

		$group['communication_template_id'] = $info['template_id'];
		$group['communication_setting_id'] = 0;
		$group['send_schedule_date'] = $info['send_schedule_date'];
		$group['medium_type'] = $info['medium_type'];
		$group['sent_type'] = 0;
		$group['status'] = 0;

		if ($id) {
			// Update

			// Update Communication Group
			$this->M_comm_group->update($group + $this->u_additional, $id);

			$communication_group = $this->M_comm_group->with_communication()->get($id);

			$current_recipients = isset($communication_group['communication']) ? $communication_group['communication'] : [];

			$current_recipient_table = "";

			if (count($current_recipients)) {
				// First item's recipient table
				$current_recipient_table = $current_recipients[0]['recipient_table'];
			}

			// If same recipient table
			if ($current_recipient_table == $info['recipient_type']) {
				foreach ($recipients as $key => $recipient) {
					// If new recipient id is in current recipients ids
					if (in_array($recipient, array_column($current_recipients, 'recipient_id'))) {

						// Update recipient
						$recipient_to_update = $this->M_communication->where(array('communication_group_id' => $id, 'recipient_id' => $recipient))->get_all();

						if ($recipient_to_update) {
							$this->M_communication->update(array(
								'medium_type' => $info['medium_type'],
								'content' => $info['content'],
								'template_id' => $info['template_id']
							), $recipient_to_update[0]['id']);
						}
					} else {
						$this->create_recipient_item($id, $recipient, $info);
					}
				}

				// Delete old recipients that are not in new recipients
				foreach (array_column($current_recipients, 'recipient_id') as $recipient_id) {
					if (!in_array($recipient_id, $recipients)) {

						$recipients_to_delete = $this->M_communication->where(array('communication_group_id' => $id, 'recipient_id' => $recipient_id))->get_all();

						if ($recipients_to_delete) {
							foreach ($recipients_to_delete as $recipient_to_delete) {
								$this->M_communication->delete($recipient_to_delete['id']);
							}
						}
					}
				}
			} else {
				// Delete all current recipients
				foreach ($current_recipients as $recipient) {
					$this->M_communication->delete($recipient['id']);
				}

				// Create new recipients
				foreach ($recipients as $key => $recipient) {
					$this->create_recipient_item($id, $recipient, $info);
				}
			}
		} else {
			// Create

			$group_id = $this->M_comm_group->insert($group + $this->additional);

			if (count($recipients)) {
				foreach ($recipients as $key => $recipient) {
					$this->create_recipient_item($group_id, $recipient, $info);
				}
			}
		}
	}

	public function create_recipient_item($communication_group_id, $recipient_id, $info)
	{
		$comm['communication_group_id'] = $communication_group_id;
		$comm['communication_title'] = $communication_group_id;
		$comm['recipient_id'] = $recipient_id;
		$comm['recipient_table'] = $info['recipient_type'];
		$comm['recipient_address'] = get_value_field($recipient_id, plural($info['recipient_type']), 'email');
		$comm['recipient_mobile'] = get_value_field($recipient_id, plural($info['recipient_type']), 'mobile_no');
		$comm['medium_type'] = $info['medium_type'];
		$comm['is_sent'] = 0;
		$comm['content'] = $info['content'];
		$comm['template_id'] = $info['template_id'];
		$comm['status'] = 0;
		$comm['sent_type'] = 0;

		$this->M_communication->insert($comm + $this->additional);
	}

	public function send($group_id = 0)
	{
		$response['status'] = 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($group_id) {

			$result = $this->communication_library->send_group($group_id);

			$response['status'] = $result;
			$response['message'] = 'Message successfully sent!';

			echo json_encode($response);
		} else {
			echo json_encode($response);
		}
	}

	public function get_sms_account()
	{
		$result = $this->communication_library->sms_account();
		return $result;
	}

	public function delete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_comm_group->get($id);
			if ($list) {

				$deleted = $this->M_comm_group->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Communication successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function bulkDelete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_communication->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Communication successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];
		$_extra_datas		=	[];
		$_adatas			=	[];

		$_titles[]	=	'#';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		//$transactions	=	$this->M_communication->as_array()->get_all();
		$transactions = $this->M_communication->as_array()->get_all();
		// ->with_source('fields:recruiter, supervisor, net_worth, to_report, to_attend, commission_based, is_member, group')
		// ->with_reference('fields: ref_name, ref_address, ref_contact_no')
		// ->with_work_experience('fields: employer, designation, emp_address, salary, emp_contact_no, supervisor, start_date, end_date, reason, to_contact')
		// ->with_academic('fields: level, course, school, year, rating')
		// ->with_exam()
		// ->with_training()
		// ->with_position('fields:name')
		// ->get_all();

		if ($transactions) {

			foreach ($transactions as $skey => $transaction) {

				$_datas[$transaction['id']]['#']	=	$_no;

				$_no++;
			}


			$_filename	=	'list_of_transactions' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				$_additional_column	=	$this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {



						$_column	=	$_name;

						$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

						$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

						foreach ($transactions as $skey => $transaction) {

							if ($_column === 'period_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? $transaction[$_column] : '';
							} elseif ($_column === 'buyer_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('buyers', $transaction[$_column], 'last_name', 'id', 'view') : '';
							} elseif ($_column === 'seller_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('sellers', $transaction[$_column], 'last_name', 'id', 'view') : '';
							} elseif ($_column === 'project_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('projects', $transaction[$_column], 'name', 'id', 'view') : '';
							} elseif ($_column === 'property_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ?  Dropdown::get_dynamic('properties', $transaction[$_column], 'name', 'id', 'view') : '';
							} elseif ($_column === 'financing_scheme_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('financing_schemes', $transaction[$_column], 'name', 'id', 'view') : '';
							} else {
								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title	=	ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('List of Communications');
				$_objSheet->setCellValue('A1', 'LIST OF SELLERS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($transactions as $key => $transaction) {

					$transaction_id	=	isset($transaction['id']) && $transaction['id'] ? $transaction['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title	=	ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$transaction_id][$_xls_columns[$_alpha]]) && $_datas[$transaction_id][$_xls_columns[$_alpha]] ? $_datas[$transaction_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns	=	$_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							// if($_a_column === 'contact') {

							// 	$ad_title	=	'Contact Informations';
							// } elseif($_a_column === 'reference') {

							// 	$ad_title	=	'References';
							// } elseif($_a_column === 'source') {

							// 	$ad_title	=	'Source of Informations';
							// } elseif($_a_column === 'academic'){

							// 	$ad_title	=	'Academic History';
							// }else {

							// 	$ad_title = $_a_column;
							// }

							$a_title	=	ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if ($key === 'to_report' or $key === 'to_attend' or $key === 'commission_based' or $key === 'is_member') {
												$xa_value	=	isset($_extra_datas[$transaction_id][$_a_column][$key]) && ($_extra_datas[$transaction_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$transaction_id][$_a_column][$key], 'view') : '';
											} else {
												$xa_value	=	$_extra_datas[$transaction_id][$_a_column][$key];
											}

											$xa_titles =	isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$transaction_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$transaction_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					}

					$_start += 1;
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}
		} else {

			$this->notify->error('No Record Found', 'transaction');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Communication CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_communication->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Communications');
			$_objSheet->setCellValue('A1', 'SELLERS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_failed_reasons = [];
				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {
							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							$_data['birth_date']	=	isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
							$transactionGroupID = ['3'];

							if ($_id) {
								$data	=	$this->M_communication->get($_id);
								if ($data) {

									unset($_data['id']);

									$oldPwd = password_format($data['last_name'], $data['birth_date']);
									$newPwd = password_format($_data['last_name'], $_data['birth_date']);

									$oldEmail = $data['email'];
									$newEmail = $_data['email'];

									if ($this->M_auth->email_check($oldEmail)) {

										if ($oldPwd !== $newPwd) {
											// Update User Password
											$this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
										}

										if ($oldEmail !== $newEmail) {
											// Update User Email
											$_user_data = [
												'email' => $newEmail,
												'username' => $newEmail,
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											];
											$this->M_user->update($_user_data, array('id' => $data['user_id']));
										}

										// Update Communication Info
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_update	=	$this->M_communication->update($_data, $_id);
										if ($_update !== FALSE) {

											$user_data = array(
												'first_name' => $_data['first_name'],
												'last_name' => $_data['last_name'],
												'active' => $_data['is_active'],
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											);

											$this->M_user->update($user_data, $data['user_id']);

											$_updated++;
										} else {

											$_failed_reasons[$_data['id']][] = 'update not working.';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email already used';
										$_failed++;

										break;
									}
								} else {

									// Generate user password
									$transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
									$transactionEmail = $_data['email'];

									if (!$this->M_auth->email_check($_data['email'])) {

										$user_data = array(
											'first_name' => $_data['first_name'],
											'last_name' => $_data['last_name'],
											'active' => $_data['is_active'],
											'created_by' => $this->user->id,
											'created_at' => NOW
										);

										$userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

										if ($userID !== FALSE) {

											$_data['user_id']	= $userID;
											$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['created_at']	=	NOW;
											$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['updated_at']	=	NOW;

											$_insert	=	$this->M_communication->insert($_data);
											if ($_insert !== FALSE) {

												$_inserted++;
											} else {

												$_failed_reasons[$_data['id']][] = 'insert transaction not working';
												$_failed++;

												break;
											}
										} else {

											$_failed_reasons[$_data['id']][] = 'insert ion auth not working';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email check already existing';
										$_failed++;

										break;
									}
								}
							} else {

								// Generate user password
								$transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
								$transactionEmail = $_data['email'];

								if (!$this->M_auth->email_check($_data['email'])) {

									$user_data = array(
										'first_name' => $_data['first_name'],
										'last_name' => $_data['last_name'],
										'active' => $_data['is_active'],
										'created_by' => $this->user->id,
										'created_at' => NOW
									);

									$userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

									if ($userID !== FALSE) {

										$_data['user_id']	= $userID;
										$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['created_at']	=	NOW;
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_insert	=	$this->M_communication->insert($_data);
										if ($_insert !== FALSE) {

											$_inserted++;
										} else {

											$_failed_reasons[$_dkey][] = 'insert transaction not working';

											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_dkey][] = 'insert ion auth not working';

										$_failed++;

										break;
									}
								} else {

									$_failed_reasons[$_dkey][] = 'email check already existing';

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {
							$this->notify->error('Upload Failed! Please follow upload guide. ', 'transaction');
						} else {

							$this->notify->success($_msg . '.', 'transaction');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'transaction');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'transaction');
			}
		} else {

			$this->notify->error('Something went wrong!', 'transaction');
		}
	}

	public function update($type = 0, $post = array())
	{
		$response['status'] = 0;
		$response['msg'] = 'Oops! Please refresh the page and try again.';
		if (empty($post)) {
			$post = $this->input->post();
		}

		if ($post) {
			if ($this->input->post()) {
				$additional = [
					'updated_by' => $this->user->id,
					'updated_at' => NOW,
				];

				if ($post['type'] == "communication_setting") {
					foreach ($post as $key => $category) {
						if ($category == 'communication_setting') {
							continue;
						};

						foreach ($category as $key => $value) {
							$data['days_to_send'] = $value['days_to_send'];
							$data['sms'] = isset($value['sms']) ? 1 : 0;
							$data['sms_template_id'] = !empty($value['sms_template_id']) ? $value['sms_template_id'] : 0;
							$data['email'] = isset($value['email']) ? 1 : 0;
							$data['email_template_id'] = !empty($value['email_template_id']) ? $value['email_template_id'] : 0;

							$result = $this->M_communication_setting->update($data + $additional, $value['id']);

							if ($result) {
								$response['status'] = 1;
								$response['msg'] = 'Settings successfully updated!';
							} else {
								$response['status'] = 1;
								$response['msg'] = 'Oops! Failed to updated!';
							}
						}
					}

					echo json_encode($response);
					exit();
				} else if ($post['type'] == "app_global") {
					foreach ($post as $key => $value) {
						if ($value == 'app_global') {
							continue;
						};

						$result = $this->M_app_global->update($value + $additional, $key);

						if ($result) {
							$response['status'] = 1;
							$response['msg'] = 'Settings successfully updated!';
						} else {
							$response['status'] = 1;
							$response['msg'] = 'Oops! Failed to updated!';
						}
					}

					echo json_encode($response);
					exit();
				}
			} else {
				echo json_encode($response);
				exit();
			}
		} else {
			$return['modal'] = $type;
			$return['type'] = $type;

			// Overdue Payments
			$return['overdue_payments'] = $this->M_communication_setting->as_array()->where('setting_slug', 'overdue_payments')->get_all();

			// Upcoming Payments
			$return['upcoming_payments'] = $this->M_communication_setting->as_array()->where('setting_slug', 'upcoming_payments')->get_all();

			// Update buyer Details
			$return['update_buyer_details'] = $this->M_communication_setting->as_array()->where('setting_slug', 'update_buyer_details')->get_all();

			// Document Followup
			$return['document_followup'] = $this->M_communication_setting->as_array()->where('setting_slug', 'document_followup')->get_all();

			// Reservation
			$return['reservation'] = $this->M_communication_setting->as_array()->where('setting_slug', 'reservation')->get_all();

			// Global Application
			$return['app_global'] = $this->M_app_global->as_array()->where('value_type', 1)->get_all();

			$return['html'] = $this->load->view('communication/modals/' . $type, $return, true);

			echo json_encode($return);
		}
	}
}
