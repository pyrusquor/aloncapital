<div class="modal-header">
	<h5 class="modal-title" id="apiModalLabel">API Settings</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	<form class="kt-form" id="form_modal" action="<?php echo base_url('communication/update'); ?>" method="POST">
		<input type="hidden" id="type" value="<?=$type;?>" name="type">
		<div class="container">
			<?php if($app_global): ?>
				<?php foreach ($app_global as $key => $value): ?>
					<div class="row mb-3"> 
						<div class="col-md-2">
							<input type="hidden" name="<?=$value['id']?>[name]" value="<?=$value['name']?>">
							<p><?=$value['name']?></p>
						</div>
						<div class="col-md-10">
							<input type="text" class="form-control form-control-md" name="<?=$value['id']?>[value]" value="<?=$value['value']?>"> 
						</div>
					</div>
				<?php endforeach; ?>
			<?php else: ?>
				<div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
					<div class="alert-icon"><i class="flaticon-warning"></i></div>
					<div class="alert-text">
						Something went wrong. Please contact your system administrator.
					</div>
					<div class="alert-close">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="la la-close"></i></span>
						</button>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</form>
</div>
<div class="modal-footer">
	<div class="kt-form__actions btn-block">
		<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
			<i class="fa fa-times"></i> Close
		</button>
		<button type="submit" id="_settings_form" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" data-action="action-submit">
			<i class="fa fa-save"></i> Save
		</button>
	</div>
</div>