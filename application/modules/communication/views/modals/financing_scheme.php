
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Change Financing Scheme Form</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	<form id="form_modal" action="<?=base_url();?>transaction/process_transaction" method="post">
			<input type="hidden" id="transaction_id" value="<?=$info['id'];?>" name="transaction_id">
			<input type="hidden" id="type" value="<?=$type;?>" name="type">
			
			<div class="row">
			    <div class="col-md-6">

			        <div class="row">
			            <div class="col-sm-12">
			                <div class="form-group">
			                    <label class="">Financing Scheme <span class="kt-font-danger">*</span></label>
			                    <select class="form-control kt-select2" id="financing_scheme_id" name="financing_scheme_id" <?php //echo ($this->router->fetch_method() === 'form' && !empty($this->uri->segment(3)) ) ? 'disabled' : ''; ?>>
			                        <option value="">Select Option</option>
			                        <?php foreach ($schemes as $scheme): ?>
			                                <option value="<?php echo $scheme['id'] ?>" <?php echo ($info['financing_scheme_id'] == $scheme['id']) ? 'selected' : '' ?>><?php echo ucwords($scheme['name']); ?></option>
			                        <?php endforeach;?>
			                    </select>
			                    <span class="form-text text-muted"></span>
			                </div>
			            </div>
			        </div>

			    </div>
			</div>

	</form>
	
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_billing_information',$info,true); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_account_information',$info,true); ?>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>



