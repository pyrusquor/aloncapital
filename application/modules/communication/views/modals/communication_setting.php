<div class="modal-header">
    <h5 class="modal-title" id="_settings_label">Settings - Automatic Communication To Buyers</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body">
    <form class="kt-form" id="form_modal" action="<?php echo base_url('communication/update'); ?>" method="POST">
        <input type="hidden" id="type" value="<?=$type;?>" name="type">
        <div class="container">
            <div class="row">                
                <?php if($overdue_payments && $upcoming_payments && $update_buyer_details && $document_followup && $reservation): ?>
                    <!-- start: Overdue Payments -->
                        <h5 class="mr-3">Overdue Payments</h5>
                        <div class="nav nav-tabs" role="tablist">
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-secondary active">
                                    <input type="radio" data-target="#overdue_payment_reservation" id="overdue_period" value="1" autocomplete="off" checked> Reservation
                                </label>
                                <label class="btn btn-secondary">
                                    <input type="radio" data-target="#overdue_payment_downpayment" id="overdue_period" value="2" autocomplete="off"> Downpayment
                                </label>
                                <label class="btn btn-secondary">
                                    <input type="radio" data-target="#overdue_payment_loan" id="overdue_period" value="3" autocomplete="off"> Loan Takeout
                                </label>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div id="overdue_payment_reservation" class="tab-pane active">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Date to Send</th>
                                            <th></th>
                                            <th>SMS Communication</th>
                                            <th></th>
                                            <th>Email Communication</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($overdue_payments as $key => $value): if($value['period_id'] != "1"): continue; endif; ?>
                                            <tr>
                                                <td style="width: 10%;">
                                                    <input type="hidden" name="overdue_payments[<?=$key?>][id]" value="<?=$value['id']?>">
                                                    <input type="hidden" name="overdue_payments[<?=$key?>][period_id]" value="<?=$value['period_id']?>">
                                                    <?=$value['name']?></td>
                                                <td  style="width: 30%;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="overdue_payments[<?=$key?>][days_to_send]" value="<?=$value['days_to_send']?>"> 
                                                    </div>
                                                    <div class="col-md-8">
                                                        Days <?=$value['days_to_send_type']?> Due Date
                                                    </div>
                                                </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="overdue_payments[<?=$key?>][sms]" class="btn-switch" <?php if($value['sms']){ ?> checked <?php }?>>
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="overdue_payments[<?=$key?>][sms_template_id]" data-template="<?=$value['sms_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="sms_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="overdue_payments[<?=$key?>][email]" class="btn-switch" <?php if($value['email']){ ?> checked <?php }?>>    
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="overdue_payments[<?=$key?>][email_template_id]" data-template="<?=$value['email_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="email_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="overdue_payment_downpayment" class="tab-pane">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Date to Send</th>
                                            <th></th>
                                            <th>SMS Communication</th>
                                            <th></th>
                                            <th>Email Communication</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($overdue_payments as $key => $value): if($value['period_id'] != "2"): continue; endif; ?>
                                        <tr>
                                                <td style="width: 10%;">
                                                    <input type="hidden" name="overdue_payments[<?=$key?>][id]" value="<?=$value['id']?>">
                                                    <input type="hidden" name="overdue_payments[<?=$key?>][period_id]" value="<?=$value['period_id']?>">
                                                    <?=$value['name']?></td>
                                                <td  style="width: 30%;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="overdue_payments[<?=$key?>][days_to_send]" value="<?=$value['days_to_send']?>"> 
                                                    </div>
                                                    <div class="col-md-8">
                                                        Days <?=$value['days_to_send_type']?> Due Date
                                                    </div>
                                                </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="overdue_payments[<?=$key?>][sms]" class="btn-switch" <?php if($value['sms']){ ?> checked <?php }?>>
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="overdue_payments[<?=$key?>][sms_template_id]" data-template="<?=$value['sms_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="sms_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="overdue_payments[<?=$key?>][email]" class="btn-switch" <?php if($value['email']){ ?> checked <?php }?>>    
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="overdue_payments[<?=$key?>][email_template_id]" data-template="<?=$value['email_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="email_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="overdue_payment_loan" class="tab-pane">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Date to Send</th>
                                            <th></th>
                                            <th>SMS Communication</th>
                                            <th></th>
                                            <th>Email Communication</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($overdue_payments as $key => $value): if($value['period_id'] != "3"): continue; endif; ?>
                                        <tr>
                                                <td style="width: 10%;">
                                                    <input type="hidden" name="overdue_payments[<?=$key?>][id]" value="<?=$value['id']?>">
                                                    <input type="hidden" name="overdue_payments[<?=$key?>][period_id]" value="<?=$value['period_id']?>">
                                                    <?=$value['name']?></td>
                                                <td  style="width: 30%;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="overdue_payments[<?=$key?>][days_to_send]" value="<?=$value['days_to_send']?>"> 
                                                    </div>
                                                    <div class="col-md-8">
                                                        Days <?=$value['days_to_send_type']?> Due Date
                                                    </div>
                                                </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="overdue_payments[<?=$key?>][sms]" class="btn-switch" <?php if($value['sms']){ ?> checked <?php }?>>
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="overdue_payments[<?=$key?>][sms_template_id]" data-template="<?=$value['sms_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="sms_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="overdue_payments[<?=$key?>][email]" class="btn-switch" <?php if($value['email']){ ?> checked <?php }?>>    
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="overdue_payments[<?=$key?>][email_template_id]" data-template="<?=$value['email_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="email_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <!-- end: Upcoming Payments -->
                    <!-- start: Upcoming Payments -->
                        <h5 class="mr-3">Upcoming Payments</h5>
                        <div class="nav nav-tabs" role="tablist">
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-secondary">
                                    <input type="radio" data-target="#upcoming_payment_downpayment" id="upcoming_period" value="2" autocomplete="off"> Downpayment
                                </label>
                                <label class="btn btn-secondary">
                                    <input type="radio" data-target="#upcoming_payment_loan" id="upcoming_period" value="3" autocomplete="off"> Loan Takeout
                                </label>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div id="upcoming_payment_downpayment" class="tab-pane active">
                                <table class="table table-borderless">
                                    <tbody>
                                        <?php foreach($upcoming_payments as $key => $value): if($value['period_id'] != "2"): continue; endif; ?>
                                            <tr>
                                                <td style="width: 10%;">
                                                    <input type="hidden" name="upcoming_payments[<?=$key?>][id]" value="<?=$value['id']?>">
                                                    <?=$value['name']?></td>
                                                <td  style="width: 30%;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="upcoming_payments[<?=$key?>][days_to_send]" value="<?=$value['days_to_send']?>"> 
                                                    </div>
                                                    <div class="col-md-8">
                                                        Days <?=$value['days_to_send_type']?> Due Date
                                                    </div>
                                                </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="upcoming_payments[<?=$key?>][sms]" class="btn-switch" <?php if($value['sms']){ ?> checked <?php }?>>
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="upcoming_payments[<?=$key?>][sms_template_id]" data-template="<?=$value['sms_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="sms_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="upcoming_payments[<?=$key?>][email]" class="btn-switch" <?php if($value['email']){ ?> checked <?php }?>>    
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="upcoming_payments[<?=$key?>][email_template_id]" data-template="<?=$value['email_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="email_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="upcoming_payment_loan" class="tab-pane">
                                <table class="table table-borderless">
                                    <tbody>
                                        <?php foreach($upcoming_payments as $key => $value): if($value['period_id'] != "3"): continue; endif; ?>
                                            <tr>
                                                <td style="width: 10%;">
                                                    <input type="hidden" name="upcoming_payments[<?=$key?>][id]" value="<?=$value['id']?>">
                                                    <?=$value['name']?></td>
                                                <td  style="width: 30%;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="upcoming_payments[<?=$key?>][days_to_send]" value="<?=$value['days_to_send']?>"> 
                                                    </div>
                                                    <div class="col-md-8">
                                                        Days <?=$value['days_to_send_type']?> Due Date
                                                    </div>
                                                </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="upcoming_payments[<?=$key?>][sms]" class="btn-switch" <?php if($value['sms']){ ?> checked <?php }?>>
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="upcoming_payments[<?=$key?>][sms_template_id]" data-template="<?=$value['sms_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="sms_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                                <td  style="width: 10%;">
                                                    <input type="checkbox" name="upcoming_payments[<?=$key?>][email]" class="btn-switch" <?php if($value['email']){ ?> checked <?php }?>>    
                                                </td>
                                                <td  style="width: 20%;">
                                                    <div class="form-group">
                                                        <select class="form-control" name="upcoming_payments[<?=$key?>][email_template_id]" data-template="<?=$value['email_template_id']?>"
                                                                placeholder="Type"
                                                                autocomplete="off" id="email_template_id">
                                                            <option value="">Select template</option>
                                                        </select>
                                                        <span class="form-text text-muted"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <!-- end: Upcoming Payments -->
                    <!-- start: Update Buyer Details -->       
                        <h5 class="mr-3">Update of Details</h5>
                        <table class="table table-borderless">
                            <tbody>
                                <?php foreach($update_buyer_details as $key => $value): ?>
                                    <tr>
                                        <td style="width: 10%;">
                                            <input type="hidden" name="update_buyer_details[<?=$key?>][id]" value="<?=$value['id']?>">
                                            <?=$value['name']?></td>
                                        <td  style="width: 30%;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="update_buyer_details[<?=$key?>][days_to_send]" value="<?=$value['days_to_send']?>"> 
                                            </div>
                                            <div class="col-md-8">
                                                Days <?=$value['days_to_send_type']?> Due Date
                                            </div>
                                        </div>
                                        </td>
                                        <td  style="width: 10%;">
                                            <input type="checkbox" name="update_buyer_details[<?=$key?>][sms]" class="btn-switch" <?php if($value['sms']){ ?> checked <?php }?>>
                                        </td>
                                        <td  style="width: 20%;">
                                            <div class="form-group">
                                                <select class="form-control" name="update_buyer_details[<?=$key?>][sms_template_id]" data-template="<?=$value['sms_template_id']?>"
                                                        placeholder="Type"
                                                        autocomplete="off" id="sms_template_id">
                                                    <option value="">Select template</option>
                                                </select>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </td>
                                        <td  style="width: 10%;">
                                            <input type="checkbox" name="update_buyer_details[<?=$key?>][email]" class="btn-switch" <?php if($value['email']){ ?> checked <?php }?>>    
                                        </td>
                                        <td  style="width: 20%;">
                                            <div class="form-group">
                                                <select class="form-control" name="update_buyer_details[<?=$key?>][email_template_id]" data-template="<?=$value['email_template_id']?>"
                                                        placeholder="Type"
                                                        autocomplete="off" id="email_template_id">
                                                    <option value="">Select template</option>
                                                </select>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>   
                    <!-- end: Update Buyer Details -->  
                    <!-- start: Follow Up on Documents -->       
                        <h5 class="mr-3">Follow Up on Documents</h5>
                        <table class="table table-borderless">
                            <tbody>
                                <?php foreach($document_followup as $key => $value): ?>
                                    <tr>
                                        <td style="width: 10%;">
                                            <input type="hidden" name="document_followup[<?=$key?>][id]" value="<?=$value['id']?>">
                                            <?=$value['name']?></td>
                                        <td  style="width: 30%;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="document_followup[<?=$key?>][days_to_send]" value="<?=$value['days_to_send']?>"> 
                                            </div>
                                            <div class="col-md-8">
                                                Days <?=$value['days_to_send_type']?> Due Date
                                            </div>
                                        </div>
                                        </td>
                                        <td  style="width: 10%;">
                                            <input type="checkbox" name="document_followup[<?=$key?>][sms]" class="btn-switch" <?php if($value['sms']){ ?> checked <?php }?>>
                                        </td>
                                        <td  style="width: 20%;">
                                            <div class="form-group">
                                                <select class="form-control" name="document_followup[<?=$key?>][sms_template_id]" data-template="<?=$value['sms_template_id']?>"
                                                        placeholder="Type"
                                                        autocomplete="off" id="sms_template_id">
                                                    <option value="">Select template</option>
                                                </select>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </td>
                                        <td  style="width: 10%;">
                                            <input type="checkbox" name="document_followup[<?=$key?>][email]" class="btn-switch" <?php if($value['email']){ ?> checked <?php }?>>    
                                        </td>
                                        <td  style="width: 20%;">
                                            <div class="form-group">
                                                <select class="form-control" name="document_followup[<?=$key?>][email_template_id]" data-template="<?=$value['email_template_id']?>"
                                                        placeholder="Type"
                                                        autocomplete="off" id="email_template_id">
                                                    <option value="">Select template</option>
                                                </select>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>   
                    <!-- end: Follow Up on Documents -->  
                    <!-- start: Reservation -->       
                        <h5 class="mr-3">Reservation</h5>
                        <table class="table table-borderless">
                            <tbody>
                                <?php foreach($reservation as $key => $value): ?>
                                    <tr>
                                        <td style="width: 10%;">
                                            <input type="hidden" name="reservation[<?=$key?>][id]" value="<?=$value['id']?>">
                                            <?=$value['name']?></td>
                                        <td  style="width: 30%;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="reservation[<?=$key?>][days_to_send]" value="<?=$value['days_to_send']?>"> 
                                            </div>
                                            <div class="col-md-8">
                                                Days <?=$value['days_to_send_type']?> Due Date
                                            </div>
                                        </div>
                                        </td>
                                        <td  style="width: 10%;">
                                            <input type="checkbox" name="reservation[<?=$key?>][sms]" class="btn-switch" <?php if($value['sms']){ ?> checked <?php }?>>
                                        </td>
                                        <td  style="width: 20%;">
                                            <div class="form-group">
                                                <select class="form-control" name="reservation[<?=$key?>][sms_template_id]" data-template="<?=$value['sms_template_id']?>"
                                                        placeholder="Type"
                                                        autocomplete="off" id="sms_template_id">
                                                    <option value="">Select template</option>
                                                </select>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </td>
                                        <td  style="width: 10%;">
                                            <input type="checkbox" name="reservation[<?=$key?>][email]" class="btn-switch" <?php if($value['email']){ ?> checked <?php }?>>    
                                        </td>
                                        <td  style="width: 20%;">
                                            <div class="form-group">
                                                <select class="form-control" name="reservation[<?=$key?>][email_template_id]" data-template="<?=$value['email_template_id']?>"
                                                        placeholder="Type"
                                                        autocomplete="off" id="email_template_id">
                                                    <option value="">Select template</option>
                                                </select>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>   
                    <!-- end: Reservation -->                   
                <?php else : ?>
                    <div class="col-lg-10 offset-lg-1">
                        <div class="form-group form-group-last">
                            <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    Something went wrong. Please contact your system administrator.
                                </div>
                                <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <div class="kt-form__actions btn-block">
        <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
            <i class="fa fa-times"></i> Close
        </button>
        <button type="submit" id="_settings_form" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" data-action="action-submit">
            <i class="fa fa-save"></i> Save
        </button>
    </div>
</div>