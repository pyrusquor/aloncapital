<?php
    $buyers = isset($info['buyers']) && $info['buyers'] ? $info['buyers'] : '';
    $client_type = isset($buyers['buyers']) && $buyers['buyers'] ? $buyers['buyers'] : '';
?>

<div class="row">
    <div class="col-md-12">

        <a href="<?php echo base_url(); ?>buyer/create" target="_blank" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Buyer Record Here</a>

    </div>
</div>
<br>    
<br>    
<div class="row">

    <div class="col-md-12">

        <?php echo $this->load->view('form/_buyer_details_form'); ?>

    </div>

</div>