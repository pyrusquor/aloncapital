<?php
	$reservation_date = view_date($info['reservation_date']);
	$expiration_date = view_date($info['expiration_date']);
	$scheme_name = $info['scheme']['name'];
	$billings = $info['billings'];
	$tbillings = $info['tbillings'];
?>
<div class="row">
	<div class="col-sm-12">
		<!-- Billing Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_billing_information">
				<div class="card">
					<div class="card-header" id="head_billing_information">
						<div class="card-title" data-toggle="collapse" data-target="#billing_information" aria-expanded="true" aria-controls="billing_information">
							Billing Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="billing_information" class="collapse show" aria-labelledby="head_billing_information" data-parent="#accord_billing_information">
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group form-group-xs row">
										<div class="col-sm-4">
											<span href="#" class="kt-notification-v2__item">
												<div class="kt-notification-v2__itek-wrapper">
													<div class="kt-notification-v2__item-title">
														<h7 class="kt-portlet__head-title kt-font-primary">Payment Scheme</h7>
													</div>
													<div class="kt-notification-v2__item-desc">
														<h6 class="kt-portlet__head-title kt-font-dark">
															<?php echo $scheme_name; ?>
														</h6>
													</div>
												</div>
											</span>
										</div>
										<div class="col-sm-4">
											<span href="#" class="kt-notification-v2__item">
												<div class="kt-notification-v2__itek-wrapper">
													<div class="kt-notification-v2__item-title">
														<h7 class="kt-portlet__head-title kt-font-primary">Reservation Date</h7>
													</div>
													<div class="kt-notification-v2__item-desc">
														<h6 class="kt-portlet__head-title kt-font-dark">
															<?php echo $reservation_date; ?>
														</h6>
													</div>
												</div>
											</span>
										</div>
										<div class="col-sm-4">
											<span href="#" class="kt-notification-v2__item">
												<div class="kt-notification-v2__itek-wrapper">
													<div class="kt-notification-v2__item-title">
														<h7 class="kt-portlet__head-title kt-font-primary">Expires On</h7>
													</div>
													<div class="kt-notification-v2__item-desc">
														<h6 class="kt-portlet__head-title kt-font-dark">
															<?php echo $expiration_date; ?>
														</h6>
													</div>
												</div>
											</span>
										</div>
									</div>
								</div>
							</div>


							<div class="row">
								<div class="col-md-12">
									<table class="table table-striped- table-bordered table-hover">
										<tr>
											<th colspan="6"> <center>ORIGINAL BILLING TERMS</center></th>
										</tr>
										<tr>
											<th>Period</th>
											<th>Effectivity Date</th>
											<th>Terms</th>
											<th>Interest Rate</th>
											<th>Total Amount</th> 
											<th>Monthly Payment</th> 
										</tr>
										<tr>
											<?php if ($billings) { ?>
												<?php foreach ($billings as $key => $billing) { if(!$billing['period_term']){ continue; }?>
													<tr>
														<td><?php echo Dropdown::get_static('periods', $billing['period_id'], 'view'); ?></td>
														<td><?php echo view_date($billing['effectivity_date']); ?></td>
														<td><?php echo $billing['period_term']." mo(s)"; ?></td>
														<td><?php echo $billing['interest_rate']."%"; ?></td>
														<td><?php echo money_php($billing['period_amount']); ?></td>
														<td><?php echo money_php(monthly_payment($billing['period_amount'],$billing['interest_rate'],$billing['period_term'])); ?></td>
													</tr>
												<?php } ?>
											<?php } ?>
										</tr>
									</table>

								</div>
							</div>
							
							<?php if ($tbillings): ?>
								
								<div class="row">
									<div class="col-md-12">
										<table class="table table-striped- table-bordered table-hover">
											<tr>
												<th colspan="6"> <center>TRANSACTION LOGS</center></th>
											</tr>
											<tr>
												<th>Period</th>
												<th>Effectivity Date</th>
												<th>Terms</th>
												<th>Interest Rate</th>
												<th>Total Amount</th> 
												<th>Monthly Payment</th> 
											</tr>
											<tr>
												<?php foreach ($tbillings as $key => $billing) { if(!$billing['period_term']){ continue; }?>
													<tr>
														<td>
															<?php echo Dropdown::get_static('periods', $billing['period_id'], 'view'); ?>
															<br>
															<?php echo $billing['slug']; ?>
														</td>
														<td><?php echo view_date($billing['effectivity_date']); ?></td>
														<td><?php echo $billing['period_term']." mo(s)"; ?></td>
														<td><?php echo $billing['interest_rate']."%"; ?></td>
														<td><?php echo money_php($billing['period_amount']); ?></td>
														<td><?php echo money_php(monthly_payment($billing['period_amount'],$billing['interest_rate'],$billing['period_term'])); ?><br>
															<?php echo $billing['remarks']; ?>
														</td>
													</tr>
												<?php } ?>
											</tr>
										</table>
									</div>
								</div>

							<?php endif ?>

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>