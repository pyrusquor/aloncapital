<?php if (!empty($records)) : ?>
    <div class="row" id="communicationList">
        <?php foreach ($records as $r) : ?>
            <?php
                $id = $r['id'];
            ?>
        <!--begin:: Portlet-->
        <div class="kt-portlet ">
            <div class="kt-portlet__body">

                <div class="kt-widget kt-widget--user-profile-3">

                    <div class="kt-widget__top">

                        <div class="kt-widget__content">
                            <div class="kt-widget__head">

                                <a href="<?php echo base_url(); ?>communication/view/<?php echo $id; ?>" class="kt-widget__username">
                                    Group : <?php echo $id; ?>
                                </a>

                                <div class="kt-widget__action">
                                    <div class="kt-portlet__head kt-portlet__head--noborder" style="min-height: 0px;">
                                        <div class="kt-portlet__head-label">
                                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>" class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                                                <span></span>
                                            </label>
                                        </div>

                                        <div class="kt-portlet__head-toolbar">


                                            <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                <i class="flaticon-more-1 kt-font-brand"></i>
                                            </a>


                                            <div class="dropdown-menu dropdown-menu-right">
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <a href="<?php echo base_url('communication/view/' . $r['id']); ?>" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                            <span class="kt-nav__link-text">View</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="javascript: void(0)" class="kt-nav__link send_message" data-id="<?php echo $r['id']?>">
                                                            <i class="kt-nav__link-icon fa fa-money-bill"></i>
                                                            <span class="kt-nav__link-text">Send</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
        
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    <a href="#" class="kt-widget__username">
                                        Scheduled Date : <?php echo view_hrdate($r['send_schedule_date']); ?>
                                    </a>
                                    <br>
                                    <?php $s = ($r['status']) ? "success": "danger"; ?>
                                    <?php $sa = ($r['status']) ? "Delivered": "Pending"; ?>

                                   
                                    <a href="#" class="kt-widget__username">
                                        Status : <span class="kt-badge kt-badge--<?=$s;?> kt-badge--inline kt-badge--pill kt-badge--rounded"><?=$sa;?></span>
                                    </a>
                                    <br>

                                    <a href="#" class="kt-widget__username">
                                        Send Type : <?php echo ($r['sent_type'] == 0) ? "Manual": "Automatic"; ?> <!--  automatic or manual -->
                                    </a>
                                </div>
                            </div>


                        </div>
                    </div>


                    <div class="kt-widget__bottom">
                        
                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Medium</span>
                                <span class="kt-widget__value">
                                   <?php echo strtoupper($r['medium_type']); ?>
                                </span>
                            </div>
                        </div>
                        
                        <?php if (isset($r['setting']['name'])): ?>
                            
                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Setting Name</span>
                                <span class="kt-widget__value">
                                   <?php echo $r['setting']['name']; ?>
                                </span>
                            </div>
                        </div>
                        
                        <?php endif ?>


                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Template Title</span>
                                <span class="kt-widget__value">
                                   <?php echo $r['template']['name']; ?>
                                </span>
                            </div>
                        </div>
                        
                            
                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Number of Recipients</span>
                                <span class="kt-widget__value">
                                   <?php echo @$r['communication'][0]['counted_rows']; ?>
                                </span>
                            </div>
                        </div>


                        
                    </div>
                </div>

            </div>
        </div>

        <?php endforeach; ?>
    </div>

    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="<?php echo base_url('communication/form'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<style>
    .hide{
        display: none !important;
    }
</style>
<script>
    $(document).ready(function(){
        $('[data-toggle="kt-popover"]').popover();
    });
</script>