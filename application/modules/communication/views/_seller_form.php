<?php
    $sellers = isset($info['sellers']) && $info['sellers'] ? $info['sellers'] : '';

    $t_property = isset($info['t_property']) && $info['t_property'] ? $info['t_property'] : '';

    $commissionable_type_id = isset($t_property['commissionable_type_id']) && $t_property['commissionable_type_id'] ? $t_property['commissionable_type_id'] : '';
    $commissionable_amount = isset($t_property['commissionable_amount']) && $t_property['commissionable_amount'] ? $t_property['commissionable_amount'] : '';
    $commissionable_amount_remarks = isset($t_property['commissionable_amount_remarks']) && $t_property['commissionable_amount_remarks'] ? $t_property['commissionable_amount_remarks'] : '';

?>

<div class="row">
    <div class="col-md-12">

        <a href="<?php echo base_url(); ?>seller/create" target="_blank" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Seller Record Here</a>

    </div>
</div>

<br>    
<br>  

<div class="row">
    <div class="col-md-12">

        <label>Sales Force Information : </label>

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">Commissionable Amount <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('property[commissionable_type_id]', Dropdown::get_static('commissionable_types'), set_value('commissionable_type_id', @$commissionable_type_id), 'class="form-control sellerComp" id="commissionable_type_id"'); ?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">&nbsp;</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="commissionable_amount" placeholder="" name="property[commissionable_amount]" value="<?php echo set_value('property[commissionable_amount]"', @$commissionable_amount); ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">Remarks</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <textarea class="form-control" name="property[commissionable_amount_remarks]" id="commissionable_amount_remarks"><?php echo @$commissionable_amount_remarks; ?>
                        </textarea>
                    </div>
                </div>
            </div>

        </div>
       
        
    </div>
</div>

<div class="row">

    <div class="col-md-12">
        
        <?php echo $this->load->view('form/_seller_details_form'); ?>

    </div>

</div>