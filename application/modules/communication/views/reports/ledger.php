<html>
  	<head>
    	<title><?php echo $fileTitle ?></title>
	    <style>
			html, body { margin:10px 15px 0 10px; font-family:Helvetica, Arial, Calibri; font-size: 11pt}

			/*@page{
				margin: 20px 20px 20px 20px;
			}*/

			table{
				text-align:left;
				text-transform: capitalize;
			}

			table tbody tr td{padding: 5px; font-size: 8pt}
			table tbody tr td{text-transform: uppercase;}
			table tbody tr td,table tbody tr td:nth-child(2){
				text-align: left !important;
			}
			table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
			table thead tr th{text-align: left;vertical-align: bottom; font-size: 11pt}
			table tbody tr th{text-align: left;vertical-align: bottom; font-size: 11pt;padding: 5px}
			
			p {
				margin: 0;
			}

			p:last-child{
				margin-bottom: 10px;
			}
			.title{
				text-align: center;
				font-size: 12pt
			}
			.divider-table td{
				border: 1px solid #fff;
				text-align: left;
				text-transform: uppercase;
				font-weight: 600;
			}
		</style>
  	</head>
  	<body>

		<div class="title">
			<p>
				<?php echo $company_name; ?>
				<br>
				<?php echo $location; ?>
				<br><br>
				<?php echo $project_name; ?>
				<br>
				<b><?php echo $fileTitle ?></b>
				<br><br>
				SUMMARY INFORMATION OF: <b><?php echo strtoupper(get_fname($info['buyer'])); ?></b>
			</p>
		</div>


		<?php
            
            $this->transaction_library->initiate( $transaction_id );
            
            $total_paid_amount = $this->transaction_library->get_amount_paid(1, 0);
            $total_paid_principal = $this->transaction_library->get_amount_paid(2, 0);
            $total_paid_interest = $this->transaction_library->get_amount_paid(3, 0);
            $total_paid_penalty = $this->transaction_library->get_amount_paid(4, 0);

            $dp_balance = $this->transaction_library->get_remaining_balance(2, 2);
            $lv_balance = $this->transaction_library->get_remaining_balance(2, 3);

            $dp = $this->transaction_library->get_amount(2);
            $dp_pp = $this->transaction_library->get_amount(2,1);
            $dp_date = $this->transaction_library->effectivity_dates(2);

            $lv_int = $this->transaction_library->interests(3);

            $last = $this->transaction_library->last_payment();
            $last_amount_paid = $last['amount_paid'];
            $last_payment_date = $last['payment_date'];

			$property = $info['property'];
			$t_property = $info['t_property'];
			$billings = $info['billings'];
	
			$tcp 			 = $t_property['total_contract_price'];

			$dp_monthly = $this->transaction_library->monthly_ammort(2);
			$lv_monthly = $this->transaction_library->monthly_ammort(3);

			$cts_number = isset($property['cts_number']) && $property['cts_number'] ? $property['cts_number'] : 'N/A';

			$period_left = $this->transaction_library->period_left();
			$amt_due = $this->transaction_library->total_amount_due();

			$discount = $this->transaction_library->total_discounts();
		?>

		<table width="100%" cellspacing="0" cellpadding="0" border="black">
			<tbody>
				<tr>
					<th style="width:10%">Lot No.:</th>
					<td style="width:12%"><?php echo $property['lot']; ?></td>
					<th style="width:10%">Paid Principal:</th>
					<td style="width:12%"><?php echo money_php($total_paid_principal); ?></td>
					<th style="width:10%">Reservation No.:</th>
					<td style="width:12%"><?php echo $info['reference']; ?></td>
					<th style="width:10%">Contract to Sell No.:</th>
					<td style="width:12%"><?php echo $cts_number; ?></td>
				</tr>
				<tr>
					<th style="width:10%">Area (sqm):</th>
					<td style="width:12%"><?php echo $property['lot_area']; ?></td>
					<th style="width:10%">Paid Interest:</th>
					<td style="width:12%"><?php echo money_php($total_paid_interest); ?></td>
					<th style="width:10%">Reservation Date:</th>
					<td style="width:12%"><?php echo view_date($info['reservation_date']); ?></td>
					<th style="width:10%">Contract to Sell Date:</th>
					<td style="width:12%"><?php echo view_date($property['cts_notarized_date']); ?></td>
				</tr>
				<tr>
					<th style="width:10%">Price / SQM:</th>
					<td style="width:12%"><?php echo money_php($property['lot_price_per_sqm']); ?></td>
					<th style="width:10%">Paid Penalty:</th>
					<td style="width:12%"><?php echo money_php($total_paid_penalty); ?></td>
					<th style="width:10%">Down Payment (%):</th>
					<td style="width:12%"><?php echo convertPercentage($dp_pp); ?>%</td>
					<th style="width:10%">Amortization Amt:</th>
					<td style="width:12%"><?php echo money_php($lv_monthly); ?></td>
				</tr>
				<tr>
					<th style="width:10%">Contract Price:</th>
					<td style="width:12%"><?php echo money_php($tcp); ?></td>
					<th style="width:10%">Total Payment:</th>
					<td style="width:12%"><?php echo money_php($total_paid_amount); ?></td>
					<th style="width:10%">Down Payment Amt:</th>
					<td style="width:12%"><?php echo money_php($dp); ?></td>
					<th style="width:10%">Payment Mode:</th>
					<td style="width:12%">Monthly</td>
				</tr>
				<tr>
					<th style="width:10%">Discount:</th>
					<td style="width:12%"><?php echo money_php($discount); ?></td>
					<th style="width:10%">Last Payment Amt:</th>
					<td style="width:12%"><?php echo money_php($last_amount_paid); ?></td>
					<th style="width:10%">DP Due Date:</th>
					<td style="width:12%"><?php echo view_date($dp_date); ?></td>
					<th style="width:10%">Interest/Annum</th>
					<td style="width:12%"><?php echo $lv_int; ?>%</td>
				</tr>
				<tr>
					<th style="width:10%">Outstanding Bal:</th>
					<td style="width:12%"><?php echo money_php($amt_due); ?></td>
					<th style="width:10%">Last Payment Date:</th>
					<td style="width:12%">
						<?php 
							if ($last_payment_date) {
								echo view_date($last_payment_date); 
							} else {
								echo "N/A";
							}
						?>
					</td>
					<th style="width:10%">Down Payment Balance:</th>
					<td style="width:12%"><?php echo money_php($dp_balance); ?></td>
					<th style="width:10%">Periods Left:</th>
					<td style="width:12%"><?php echo $period_left; ?></td>
				</tr>
				<tr>
					<th style="width:10%" colspan="1">Inhouse Balance:</th>
					<td style="width:12%"colspan="7"><?php echo money_php($lv_balance); ?></td>
				</tr>
			</tbody>
		</table>
		


		<div class="title">
			<p><br><b><h3>AMORTIZATION SCHEDULE</h3></b></p>
		</div>

		<table width="100%" cellspacing="0" cellpadding="0" border="black">
			<thead>
				<tr>
					<th style="width:5%">Due Date</th>
					<th style="width:5%">Period</th>
					<th style="width:10%">Balance</th>
					<th style="width:10%">Amount Paid</th>
					<th style="width:5%">Receipt Date</th>
					<th style="width:5%">Receipt No.</th>
					<th style="width:15%">Remarks</th>

				</tr>
			</thead>
			<tbody>
				<?php 
					if ($payments) {
						foreach ($payments as $key => $payment) { ?><tr>
							<?php $bal = $tcp; ?>
							<td><?php echo view_date($payment['due_date']); ?></td>
							<td><?php echo $key++; ?></td>
							<td><?php echo money_php($payment['beginning_balance']); ?></td>							
							<td>
	            				<?php if (isset($payment['official_payments'])) { ?>
	            					<?php foreach ($payment['official_payments'] as $key => $official_payment) { ?>
	            						<?php echo $official_payment['ar_number'].' - '.money_php($official_payment['amount_paid'])."<br>"; ?>
	            					<?php } ?>
	            				<?php } ?>
							</td>
							<td>
								<?php if (isset($payment['official_payments'])) { ?>
	            					<?php foreach ($payment['official_payments'] as $key => $official_payment) { ?>
	            						<?php echo view_date($official_payment['payment_date'])."<br>"; ?>
	            					<?php } ?>
	            				<?php } ?>
							</td>
							<td>
								<?php if (isset($payment['official_payments'])) { ?>
	            					<?php foreach ($payment['official_payments'] as $key => $official_payment) { ?>
	            						<?php echo $official_payment['or_number']."<br>"; ?>
	            					<?php } ?>
	            				<?php } ?>
							</td>
							<td><?php echo $payment['particulars']; ?></td>
						</tr><?php }
					} 
				?>
			</tbody>
		</table>

	</body>
</html>