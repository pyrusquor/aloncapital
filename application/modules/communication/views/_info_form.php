<?php

$id = isset($info['id']) && $info['id'] ? $info['id'] : "";

$communication_template = isset($info['template']) && $info['template'] ? $info['template'] : "";

$send_schedule_date = isset($info['send_schedule_date']) && $info['send_schedule_date'] ? $info['send_schedule_date'] : "";

$medium_type = isset($info['medium_type']) && $info['medium_type'] ? $info['medium_type'] : "";

$template_content = isset($info['template']['content']) && $info['template']['content'] ? $info['template']['content'] : "";

$recipients = isset($info['communication']) && $info['communication'] ? $info['communication'] : [];

?>

<script type="text/javascript">
    window.current_recipients = []

    window.recipient_table = ""

    <?php foreach ($recipients as $key => $recipient) : ?>
        <?php if (!$key) : ?>
            window.recipient_table = <?= json_encode($recipient['recipient_table']) ?>
        <?php endif ?>

        window.current_recipients.push(<?= json_encode($recipient['recipient_id']) ?>)
    <?php endforeach; ?>
</script>

<div class="row">
    <div class="col-md-4">
        <input type="hidden" name="id" value="<?php echo $id; ?>">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Template Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="communication_templates" id="template_id" name="info[template_id]" required>
                        <option value="">Select Template</option>
                        <?php if ($communication_template) : ?>
                            <option value="<?php echo $communication_template['id']; ?>" selected><?= $communication_template['name'] ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Schedule Date <span class="kt-font-danger">*</span></label>
                    <input type="text" id="send_schedule_date" readonly name="info[send_schedule_date]" class="form-control datetimePicker" value="<?= $send_schedule_date ?>">
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Medium Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="medium_type" name="info[medium_type]">
                        <option value="sms" <?php if ($medium_type === "sms") : ?>selected<?php endif ?>>SMS</option>
                        <option value="email" <?php if ($medium_type === "email") : ?>selected<?php endif ?>>E-mail</option>
                        <option value="both" <?php if ($medium_type === "both") : ?>selected<?php endif ?>>Both</option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

    </div>


    <div class="col-md-8">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Message Content <span class="kt-font-danger">*</span></label>
                    <textarea class="form-control" id="content" name="info[content]">
                        <?php echo set_value('info[content]"', $template_content); ?>
                    </textarea>
                </div>
            </div>
        </div>
    </div>
</div>