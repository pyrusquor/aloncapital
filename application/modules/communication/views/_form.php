<!--begin: Basic Communication Info-->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
    <?php $this->load->view('_info_form'); ?>
</div>

<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="">Recipient Type <span class="kt-font-danger">*</span></label>
                        <select class="form-control" id="recipient_type" name="info[recipient_type]">
                            <option value="">Select Option</option>
                            <option value="buyer">Buyers</option>
                            <option value="seller">Sellers</option>
                            <option value="contacts">Contacts</option>
                        </select>
                        <span class="form-text text-muted"></span>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="">Filter Name</label>
                                <input type="text" id="search_name" placeholder="Name" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label class="">Filter By</label>
                                <select class="form-control" id="filter_by">
                                    <option>Select Option</option>
                                </select>
                            </div>
                        </div>
                        <div class="kt-margin-t-15" id="tblFields" style="height: 250px; overflow-y: scroll;">
                            <table class="table table-striped" id="names_table">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Names</th>
                                        <th class="kt-align-left">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="row">
                <div class="col-sm-12" style="max-height: 350px; overflow-y: scroll;">
                    <div class="form-group">
                        <label>All Recipients <span class="kt-font-danger">*</span></label>
                        <select name="info[all_recipients][]" id="all_recipients" class="form-control" multiple="multiple">

                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--begin: Form Actions -->
<div class="kt-form__actions">
    <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
        Previous
    </div>
    <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
        Submit
    </div>
    <div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
        Next Step
    </div>
</div>

<!--end: Form Actions -->