<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends MY_Controller
{

	private $fields = [
		array(
			'field' => 'property[property_id]',
			'label' => 'Property Name',
			'rules' => 'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();

		$transaction_models = array('Transaction_billing_model' => 'M_transaction_billing', 'Transaction_client_model' => 'M_transaction_client', 'Transaction_discount_model' => 'M_transaction_discount', 'Transaction_fee_model' => 'M_transaction_fee', 'Transaction_payment_model' => 'M_transaction_payment', 'Transaction_property_model' => 'M_transaction_property', 'Transaction_seller_model' => 'M_transaction_seller', 'transaction_commission/Transaction_commission_model' => 'M_Transaction_commission', 'ticketing/Ticketing_model' => 'M_Transaction_ticketing', 'transaction_post_dated_check/Transaction_post_dated_check_model' => 'M_transaction_post_dated_check', 'transaction_payment/Transaction_official_payment_model' => 'M_Transaction_official_payment', 'seller/Seller_model' => 'M_seller', 'buyer/Buyer_model' => 'M_buyer', 'transaction/Transaction_billing_logs_model' => 'M_Tbilling_logs', 'transaction_payment/Transaction_penalty_logs_model' => 'M_tpenalty_logs', 'transaction/Transaction_refund_logs_model' => 'M_refund_logs', 'transaction/Transaction_financing_scheme_logs_model' => 'M_Transaction_financing_scheme_log', 'transaction/Transaction_adhoc_fee_model' => 'M_Transaction_adhoc_fee', 'transaction/Transaction_adhoc_fee_schedule_model' => 'M_Transaction_adhoc_fee_schedule', 'house/house_model' => 'M_House', 'transaction_past_due_notices/Transaction_past_due_notice_model' => 'M_Transaction_past_due_notice', 'standard_report/Standard_report_model' => 'M_report');

		// Load models
		$this->load->model('aris/aris_model', 'M_aris');
		$this->load->model('aris/aris_lot_model', 'M_aris_lot');
		$this->load->model('aris/aris_document_model', 'M_aris_document');
		$this->load->model('aris/aris_payment_model', 'M_aris_payment');
		$this->load->model('Transaction_model', 'M_transaction');
		$this->load->model('property/Property_model', 'M_property');
		$this->load->model('form_generator/Form_generator_model', 'M_printable');
		$this->load->model('project/Project_model', 'M_project');
		$this->load->model('fees/Fee_model', 'M_fees');
		$this->load->model('promos/Promo_model', 'M_promo');
		$this->load->model('financing_scheme/Financing_scheme_model', 'M_scheme');
		$this->load->model('financing_scheme/Financing_scheme_commission_values_model', 'M_scheme_commission');
		$this->load->model('commissions/commissions_model', 'M_commission');
		$this->load->model('auth/Ion_auth_model', 'M_auth');
		$this->load->model('user/User_model', 'M_user');
		$this->load->model('transaction_letter_request/Transaction_letter_request_model', 'M_letter_request');

		$this->load->model('document/Document_model', 'M_documents');
		$this->load->model('checklist/Checklist_model', 'M_checklist');
		$this->load->model('checklist/Document_checklist_model', 'M_document_checklist');

		$this->load->model('house/House_model', 'M_house');
		$this->load->model('transaction_personnel_logs/Transaction_personnel_logs_model', 'M_transaction_personnel_logs');

		$this->load->model('transaction_document/Transaction_document_model', 'M_transaction_document');
		$this->load->model('transaction_document_stages/Transaction_document_stages_model', 'M_transaction_document_stage');
		$this->load->model('transaction_document_stage_logs/Transaction_document_stage_logs_model', 'M_transaction_document_stage_logs');

		$this->load->model('transaction_loan_document/Transaction_loan_document_model', 'M_transaction_loan_document');
		$this->load->model('transaction_loan_document_stages/Transaction_loan_document_stages_model', 'M_transaction_loan_document_stage');
		$this->load->model('transaction_loan_document_stage_logs/Transaction_loan_document_stage_logs_model', 'M_transaction_loan_document_stage_logs');

		$this->load->model('accounting_entries/Accounting_entries_model', 'M_Accounting_entries');
		$this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');

        $this->load->model('sellers_reservations/Sellers_reservations_model', 'M_Sellers_reservations');

		$this->load->model('aris/Aris_document_model', 'M_aris_document');
		$this->load->model('aris/Aris_model', 'M_aris');

		$this->load->model('seller_group/Seller_group_model', 'M_Seller_group');
		$this->load->model($transaction_models);


		// Load pagination library 
		$this->load->library('ajax_pagination');

		$this->load->helper('transaction_helper');

		// Format Helper
		$this->load->helper(['format', 'images']); // Load Helper
		// $this->load->helper('cleaner_helper');// Load Helper

		$this->load->library('mortgage_computation');
		$this->load->library('adhoc_computation');

		// Per page limit 
		$this->perPage = 6;

		$this->_table_fillables		=	$this->M_transaction->fillable;
		$this->_table_columns		=	$this->M_transaction->__get_columns();

		$this->u_additional = [
			'updated_by' => $this->user->id,
			'updated_at' => NOW
		];

		$this->additional = [
			'is_active' => 1,
			'created_by' => $this->user->id,
			'created_at' => NOW
		];

		$this->c_additional = [
			'created_by' => $this->user->id,
			'created_at' => NOW
		];

		$this->commission_setup = $this->M_commission->with_commission_setup_values()->where("is_active", 1)->get();
	}

	public function test($transaction_id){

	}

	public function recompute_monthly_payment()
	{
		$return = monthly_payment($this->input->post('full_amount'), $this->input->post('interest_rate'), $this->input->post('term'));
		echo json_encode($return);
	}

	public function _filter_links()
	{

		$links = [];
		$r['id'] = 0;

		$links = array(
			array(
				'url' => base_url('ticketing/form/' . $r['id']),
				'name' => 'Create a Ticket',
				'target' => '_BLANK',
				'icon' => 'fa fa-sticky-note',
			),
			array(
				'url' => base_url('transaction_post_dated_check/form/' . $r['id']),
				'name' => 'Create a PDC',
				'target' => '_BLANK',
				'icon' => 'fa fa-money-check',
			),
			array(
				'url' => base_url('transaction_payment/form/' . $r['id']),
				'name' => 'Collect a Payment',
				'target' => 'fa-money-bill',
				'icon' => 'fa-money-bill',
			),
			array(
				'url' => base_url('transaction/view/' . $r['id']),
				'name' => 'View Transaction',
				'target' => '',
				'icon' => 'flaticon2-checking',
			),
			array(
				'url' => base_url('transaction/form/' . $r['id']),
				'name' => 'Edit Details',
				'target' => '',
				'icon' => 'flaticon2-edit',
			),
			array(
				'url' => 'javascript: void(0)',
				'name' => 'Delete Transaction',
				'target' => '',
				'icon' => 'flaticon2-edit',
				'class' => 'remove_transaction'
			),
			array(
				'url' => base_url('transaction/process_mortgage/' . $r['id'] . '/save/recompute'),
				'name' => 'Recompute Schedule',
				'target' => '',
				'icon' => 'flaticon2-edit',
				'data' => ['id'],
			),
		);
	}

	public function template($page = "")
	{

		$this->template->build($page);
	}

	public function index($type = "")
	{
		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns']	=	$this->__get_columns($_fills);
		$totalRec = 0;
		if ($this->ion_auth->is_seller()) {
			$seller_id = get_value_field($this->session->userdata['user_id'], 'buyers', 'id', 'user_id');

			// Get records
			$this->view_data['records'] = $this->M_transaction->order_by('id', 'DESC')
				->with_buyer()
				->with_seller(sprintf('where: user_id = %s', $this->session->userdata['user_id']))
				->with_project()->with_property()->with_scheme()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_refunds()->with_tbillings()
				->where('seller_id', $seller_id)
				->limit($this->perPage, 0)
				->get_all();

			if ($this->view_data['records']) {
				$totalRec = count($this->view_data['records']);
			}
			// vdebug($this->view_data);
		} else if ($this->ion_auth->is_buyer()) {

			$buyer_id = get_value_field($this->session->userdata['user_id'], 'buyers', 'id', 'user_id');
			// Get records
			$this->view_data['records'] = $this->M_transaction->order_by('id', 'DESC')
				->with_buyer()
				->with_seller()
				->with_project()->with_property()->with_scheme()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_refunds()->with_tbillings()
				->where('buyer_id', $buyer_id)
				->limit($this->perPage, 0)
				->get_all();

			if ($this->view_data['records']) {
				$totalRec = count($this->view_data['records']);
			}
		} else {

			$this->get_transaction_due_date();

			$totalRec = $this->M_transaction->count_rows();

			// Get records
			$this->view_data['records'] = $this->M_transaction->order_by('reservation_date', 'DESC')
				->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_refunds()->with_tbillings()
				->limit($this->perPage, 0)
				->get_all();

			// $this->view_data['records'] = [];
		}

		$this->view_data['totalRec'] = $totalRec;
		$this->view_data['document_stages'] = $this->M_transaction_document_stage->order_by('category_id', 'ASC')->order_by('order_by', 'ASC')->get_all();
		$this->view_data['loan_document_stages'] = $this->M_transaction_loan_document_stage->order_by('category_id', 'ASC')->order_by('order_by', 'ASC')->get_all();


		// Pagination configuration
		$config['target']      = '#transactionContent';
		$config['base_url']    = base_url('transaction/paginationData');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'TransactionPagination';

		// Initialize pagination library
		$this->ajax_pagination->initialize($config);

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		if ($type == "debug") {
			vdebug($this->view_data);
		}
		$this->template->build('index', $this->view_data);
	}

	public function paginationData()
	{
		if ($this->input->is_ajax_request()) {

			// Input from General Search
			$keyword = $this->input->post('keyword');

			$general_status = $this->input->post('general_status');
			$collection_status = $this->input->post('collection_status');
			$documentation_status = $this->input->post('documentation_status');
			$reference = $this->input->post('reference');
			$project_id = $this->input->post('project_id');
			$property_id = $this->input->post('property_id');
			$period_id = $this->input->post('period');
			$buyer_id = $this->input->post('buyer_id');
			$seller_id = $this->input->post('seller_id');
			$reservation_date = $this->input->post('reservation_date');
			$due_for_payment_date = $this->input->post('due_for_payment_date');
			$personnel_id = $this->input->post('personnel_id');
			$day = $this->input->post('day');
			$month = $this->input->post('month');
			$year = $this->input->post('year');
			$page = $this->input->post('page');

			$greater_than = $this->input->post('greater_than');
			$equal_to = $this->input->post('equal_to');
			$lower_than = $this->input->post('lower_than');

			$dp_greater_than = $this->input->post('dp_greater_than');
			$dp_equal_to = $this->input->post('dp_equal_to');
			$dp_lower_than = $this->input->post('dp_lower_than');

			if (!empty($reservation_date)) {
				$date_range = explode('-', $reservation_date);

				$from_str   = strtotime($date_range[0]);
				$from       = date('Y-m-d H:i:s', $from_str);

				$to_str     = strtotime($date_range[1] . '23:59:59');
				$to         = date('Y-m-d H:i:s', $to_str);
			}

			if (!empty($due_for_payment_date)) {
				$date_range = explode('-', $due_for_payment_date);

				$from_str   = strtotime($date_range[0]);
				$from_due       = date('Y-m-d H:i:s', $from_str);

				$to_str     = strtotime($date_range[1] . '23:59:59');
				$to_due         = date('Y-m-d H:i:s', $to_str);
			}

			if (!$page) {
				$offset = 0;
			} else {
				$offset = $page;
			}

			$totalRec = $this->M_transaction->count_rows();
			$where = array();

			// Pagination configuration
			$config['target'] = '#transactionContent';
			$config['base_url'] = base_url('transaction/paginationData');
			$config['total_rows'] = $totalRec;
			$config['per_page'] = $this->perPage;
			$config['link_func'] = 'TransactionPagination';

			if ($this->ion_auth->is_seller()) {
				$logged_seller_id = get_value_field($this->session->userdata['user_id'], 'sellers', 'id', 'user_id');
			}  else if ($this->ion_auth->is_buyer()) {
				$logged_buyer_id = get_value_field($this->session->userdata['user_id'], 'buyers', 'id', 'user_id');
			} 

			// Query
			if (!empty($keyword)) :
				$this->db->group_start();
				$this->db->like('reference', $keyword, 'both');
				$this->db->like('ra_number', $keyword, 'both');
				$this->db->or_like('general_status', $keyword, 'both');
				$this->db->or_like('collection_status', $keyword, 'both');
				$this->db->or_like('documentation_status', $keyword, 'both');
				$this->db->or_like('period_id', $keyword, 'both');
				$this->db->or_like('project_id', $keyword, 'both');
				$this->db->or_like('property_id', $keyword, 'both');
				$this->db->or_like('buyer_id', $keyword, 'both');
				$this->db->or_like('seller_id', $keyword, 'both');
				$this->db->or_like('id', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (isset($logged_seller_id) && !empty($logged_seller_id)) :
				$this->db->where('seller_id', $logged_seller_id);
			endif;

			if (isset($logged_buyer_id) && !empty($logged_buyer_id)) :
				$this->db->where('buyer_id', $logged_buyer_id);
			endif;

			if (!empty($general_status) && !empty($general_status)) :
				$this->db->where('general_status', $general_status);
			endif;

			if (!empty($collection_status) && !empty($collection_status)) :
				$this->db->where('collection_status', $collection_status);
			endif;

			if (!empty($documentation_status) && !empty($documentation_status)) :
				$this->db->where('documentation_status', $documentation_status);
			endif;

			if (!empty($reference) && !empty($reference)) :
				$this->db->where('reference', $reference);
			endif;

			if (!empty($project_id) && !empty($project_id)) :
				$this->db->where('project_id', $project_id);
			endif;

			if (!empty($property_id) && !empty($property_id)) :
				$this->db->where('property_id', $property_id);
			endif;

			if (!empty($period_id) && !empty($period_id)) :
				$this->db->where('period_id', $period_id);
			endif;

			if (!empty($buyer_id) && !empty($buyer_id)) :
				$this->db->where('buyer_id', $buyer_id);
			endif;

			if (!empty($seller_id) && !empty($seller_id)) :
				$this->db->where('seller_id', $seller_id);
			endif;

			if (!empty($day) && !empty($day)) :
				$this->db->like('reservation_date', $day, "both");
			endif;

			if (!empty($month) && !empty($month)) :
				$this->db->like('reservation_date', $month, "both");
			endif;

			if (!empty($year) && !empty($year)) :
				$this->db->like('reservation_date', $year, "both");
			endif;

			if (!empty($reservation_date) && !empty($reservation_date)) :
				$this->db->where('reservation_date BETWEEN "' . $from . '" AND "' .  $to . '"');
			endif;

			if (!empty($due_for_payment_date) && !empty($due_for_payment_date)) :
				$this->db->where('due_date BETWEEN "' . $from_due . '" AND "' .  $to_due . '"');
			endif;

			if (!empty($greater_than) && empty($lower_than) && empty($equal_to)) :
				$this->db->where('tpm_p > "' . $greater_than.'"');
			endif;

			if (!empty($lower_than) && empty($greater_than) && empty($equal_to)) :
				$this->db->where('tpm_p < "' . $lower_than.'"');
			endif;

			if (!empty($greater_than) && empty($lower_than) && !empty($equal_to)) :
				$this->db->where('tpm_p >= "' . $greater_than.'"');
			endif;

			if (!empty($lower_than) && empty($greater_than) && !empty($equal_to)) :
				$this->db->where('tpm_p <= "' . $lower_than.'"');
			endif;

			if (!empty($equal_to) && empty($greater_than) && empty($lower_than)) :
				$this->db->where('tpm_p = "' . $equal_to.'"');
			endif;

			if (empty($equal_to) && !empty($greater_than) && !empty($lower_than)) :
				$this->db->where('tpm_p < "' . $lower_than.'" AND tpm_p > "' . $greater_than.'"');
			endif;


			if (!empty($dp_greater_than) && empty($dp_lower_than) && empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp > "' . $dp_greater_than.'"');
			endif;

			if (!empty($dp_lower_than) && empty($dp_greater_than) && empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp < "' . $dp_lower_than.'"');
			endif;

			if (!empty($dp_greater_than) && empty($dp_lower_than) && !empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp >= "' . $dp_greater_than.'"');
			endif;

			if (!empty($dp_lower_than) && empty($dp_greater_than) && !empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp <= "' . $dp_lower_than.'"');
			endif;

			if (!empty($dp_equal_to) && empty($dp_greater_than) && empty($dp_lower_than)) :
				$this->db->where('tpm_p_dp = "' . $dp_equal_to.'"');
			endif;

			if (empty($dp_equal_to) && !empty($dp_greater_than) && !empty($dp_lower_than)) :
				$this->db->where('tpm_p_dp < "' . $dp_lower_than.'" AND tpm_p_dp > "' . $dp_greater_than.'"');
			endif;


			if (!empty($personnel_id) && !empty($personnel_id)) :
				$this->db->where('personnel_id', $personnel_id);
			endif;

			$totalRec = $this->M_transaction->count_rows();

			// Pagination configuration
			$config['total_rows'] = $totalRec;

			// Initialize pagination library
			$this->ajax_pagination->initialize($config);

			// Query
			if (!empty($keyword)) :
				$this->db->group_start();
				$this->db->like('reference', $keyword, 'both');
				$this->db->or_like('general_status', $keyword, 'both');
				$this->db->or_like('collection_status', $keyword, 'both');
				$this->db->or_like('documentation_status', $keyword, 'both');
				$this->db->or_like('period_id', $keyword, 'both');
				$this->db->or_like('project_id', $keyword, 'both');
				$this->db->or_like('property_id', $keyword, 'both');
				$this->db->or_like('buyer_id', $keyword, 'both');
				$this->db->or_like('seller_id', $keyword, 'both');
				$this->db->or_like('id', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (isset($logged_seller_id) && !empty($logged_seller_id)) :
				$this->db->where('seller_id', $logged_seller_id);
			endif;

			if (isset($logged_buyer_id) && !empty($logged_buyer_id)) :
				$this->db->where('buyer_id', $logged_buyer_id);
			endif;


			if (!empty($general_status) && !empty($general_status)) :
				$this->db->where('general_status', $general_status);
			endif;

			if (!empty($collection_status) && !empty($collection_status)) :
				$this->db->where('collection_status', $collection_status);
			endif;

			if (!empty($documentation_status) && !empty($documentation_status)) :
				$this->db->where('documentation_status', $documentation_status);
			endif;

			if (!empty($reference) && !empty($reference)) :
				$this->db->where('reference', $reference);
			endif;

			if (!empty($project_id) && !empty($project_id)) :
				$this->db->where('project_id', $project_id);
			endif;

			if (!empty($property_id) && !empty($property_id)) :
				$this->db->where('property_id', $property_id);
			endif;

			if (!empty($period_id) && !empty($period_id)) :
				$this->db->where('period_id', $period_id);
			endif;

			if (!empty($buyer_id) && !empty($buyer_id)) :
				$this->db->where('buyer_id', $buyer_id);
			endif;

			if (!empty($seller_id) && !empty($seller_id)) :
				$this->db->where('seller_id', $seller_id);
			endif;

			if (!empty($day) && !empty($day)) :
				$this->db->like('reservation_date', $day, "both");
			endif;

			if (!empty($month) && !empty($month)) :
				$this->db->like('reservation_date', $month, "both");
			endif;

			if (!empty($year) && !empty($year)) :
				$this->db->like('reservation_date', $year, "both");
			endif;

			if (!empty($reservation_date) && !empty($reservation_date)) :
				$this->db->where('reservation_date BETWEEN "' . $from . '" AND "' .  $to . '"');
			endif;

			if (!empty($due_for_payment_date) && !empty($due_for_payment_date)) :
				$this->db->where('due_date BETWEEN "' . $from_due . '" AND "' .  $to_due . '"');
			endif;

			if (!empty($personnel_id) && !empty($personnel_id)) :
				$this->db->where('personnel_id', $personnel_id);
			endif;

			if (!empty($greater_than) && empty($lower_than) && empty($equal_to)) :
				$this->db->where('tpm_p > "' . $greater_than.'"');
			endif;

			if (!empty($lower_than) && empty($greater_than) && empty($equal_to)) :
				$this->db->where('tpm_p < "' . $lower_than.'"');
			endif;

			if (!empty($equal_to) && empty($greater_than) && empty($lower_than)) :
				$this->db->where('tpm_p = "' . $equal_to.'"');
			endif;

			if (!empty($greater_than) && empty($lower_than) && !empty($equal_to)) :
				$this->db->where('tpm_p >= "' . $greater_than.'"');
			endif;

			if (!empty($lower_than) && empty($greater_than) && !empty($equal_to)) :
				$this->db->where('tpm_p <= "' . $lower_than.'"');
			endif;

			if (empty($equal_to) && !empty($greater_than) && !empty($lower_than)) :
				$this->db->where('tpm_p < "' . $lower_than.'" AND tpm_p > "' . $greater_than.'"');
			endif;

			if (!empty($dp_greater_than) && empty($dp_lower_than) && empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp > "' . $dp_greater_than.'"');
			endif;

			if (!empty($dp_lower_than) && empty($dp_greater_than) && empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp < "' . $dp_lower_than.'"');
			endif;

			if (!empty($dp_greater_than) && empty($dp_lower_than) && !empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp >= "' . $dp_greater_than.'"');
			endif;

			if (!empty($dp_lower_than) && empty($dp_greater_than) && !empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp <= "' . $dp_lower_than.'"');
			endif;

			if (!empty($dp_equal_to) && empty($dp_greater_than) && empty($dp_lower_than)) :
				$this->db->where('tpm_p_dp = "' . $dp_equal_to.'"');
			endif;

			if (empty($dp_equal_to) && !empty($dp_greater_than) && !empty($dp_lower_than)) :
				$this->db->where('tpm_p_dp < "' . $dp_lower_than.'" AND tpm_p_dp > "' . $dp_greater_than.'"');
			endif;
		
			
			$this->view_data['records'] = $this->M_transaction->order_by('reservation_date', 'DESC')
				->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_refunds()->with_tbillings()
				->limit($this->perPage, $offset)
				->get_all();
			
			// if ($this->ion_auth->is_seller()) {
			// 	vdebug($this->view_data['records']);
			// }
			$this->load->view('transaction/_filter', $this->view_data, false);
		}
	}

	public function view($id = FALSE, $type = '')
	{

		if ($id) {

			$this->view_data['info'] = $transaction = $this->M_transaction
				->with_buyer()->with_seller()->with_project()->with_property()->with_personnel()->with_scheme()->with_commission_setup()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_tbillings()
				->with_adhoc_fee()->with_adhoc_fee_schedule()->with_commission()->with_aris_payments('order_by:PaymentID,asc')
				->get($id);

			$order['period_id'] = 'ASC';
			$order['id'] = 'ASC';

			$t_official_where['transaction_id'] = $id;
			$t_official_where['deleted_at'] = NULL;

			$this->db->where('transaction_payment_id>0');
			$this->view_data['official_payments'] = $this->M_Transaction_official_payment->where($t_official_where)->get_all();

			$this->view_data['sellers'] = $this->M_transaction_seller->where('transaction_id', $id)
				->with_seller()->get_all();

			$this->view_data['printables'] = $this->M_printable->as_array()->get_all();

			$this->view_data['tickets'] = $this->M_Transaction_ticketing->where('transaction_id', $id)->get_all();

			$this->view_data['post_dated_checks'] = $this->M_transaction_post_dated_check->with_bank()->where('transaction_id', $id)->get_all();

			$this->view_data['transaction_id'] = $id;

			// $this->view_data['commissions'] = $_commissions = $this->M_Transaction_commission->where( array('transaction_id' => $id, 'seller_id' => $seller_id) )->get_all();



			if ($this->view_data['info']) {

				if ($type == "debug") {

					vdebug($this->view_data['info']);
				}

				$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
				$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			redirect('transaction');
		}
	}

	public function load_mortgage_schedule($debug = 0)
	{
		$results = "";
		if ($this->input->post() || $debug) {

			$order['period_id'] = 'ASC';
			$order['id'] = 'ASC';

			$id = $this->input->post('id');
			$cp = $this->input->post('cp');
			$penalty_type = $this->input->post('pt');
			$adhoc_fee = $this->input->post('af');

			$data['cp'] = $cp;
			$data['penalty_type'] = $penalty_type;
			$data['adhoc_fee'] = $adhoc_fee;
			$data['transaction_id'] = $id;
			$data['info'] =  $this->M_transaction
				->with_buyer()->with_seller()->with_project()->with_property()->with_personnel()->with_scheme()->with_commission_setup()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_tbillings()
				->with_adhoc_fee()->with_adhoc_fee_schedule()->with_commission()
				->get($id);
			if ($debug) {
				$id = 306;
				$data['transaction_id'] = 306;
			}
			// $data['transaction_payments'] = $this->M_transaction_payment->where('transaction_id',$id)->order_by($order)->with_official_payments()->get_all();

			$data['payments'] = $this->M_transaction_payment->where('transaction_id', $id)->order_by($order)->with_official_payments()->get_all();

			$results = $this->load->view('view/_mortgage_schedule_information', $data, true);
		}

		echo $results;
	}

	public function form($id = FALSE, $type = "", $reservation_id = '')
	{
		$method = "Create";
		if ($id) {
			$method = "Update";
		}

		if ($this->input->post()) {

			$response['status']	= 0;
			$response['msg'] = 'Oops! Please refresh the page and try again.';

			$this->form_validation->set_rules($this->fields);

			if ($this->form_validation->run() === TRUE) {

				$post = $this->input->post();

				if (isset($post['type']) && ($post['type'] == "billing_setting")) {

					$this->process_transaction($post['type'], $post['id'], $post);
				} else {

					$transaction_id = $this->save_transaction($post);

					$this->insert_transaction_documents($transaction_id);
				}
				$this->db->trans_complete(); # Completing transaction

				/*Optional*/
				if ($this->db->trans_status() === FALSE) {
					# Something went wrong.
					$this->db->trans_rollback();
					$response['status']	= 0;
					$response['message'] = 'Error!';
				} else {
					# Everything is Perfect. 
					# Committing data to the database.
					$this->db->trans_commit();
					$response['status']	= 1;
					$response['message'] = 'Transaction Successfully ' . $method . 'd!';
				}
			} else {
				$response['status']	 =	0;
				$response['message'] = validation_errors();
			}

			echo json_encode($response);
			exit();
		}

		$this->view_data['projects'] = $projects = $this->M_project->as_array()->get_all();

		$project_id = @$projects['id'];

		$this->view_data['house'] = $this->M_house->where("project_id", $project_id)->get();
		$this->view_data['fees'] = $this->M_fees->as_array()->get_all();
		$this->view_data['sales_groups'] = $this->M_Seller_group->as_array()->get_all();

		$this->view_data['default_fees'] = $this->M_fees->where('default', 1)->as_array()->get_all();
		$this->view_data['promos'] = $this->M_promo->as_array()->get_all();
		$this->view_data['schemes'] = $this->M_scheme->as_array()->get_all();
		$this->view_data['method'] = $method;
		$this->view_data['type'] = $type;

		if($reservation_id){

			$reservation = $this->M_Sellers_reservations->with_payment()->with_property(['with'=>[['relation'=>'project','with'=>['relation'=>'company']], ['relation'=>'model']]])->with_buyer()->with_seller()->with_seller_groups()->get($reservation_id);
			$reservation ? : $this->notify->error('No reservation found!', 'sellers_reservations');
			($reservation['reservation_status']<2 && $reservation['payment_status']=='2') ? : $this->notify->error('Invalid Reservation!', 'sellers_reservations');
			$reservation['payment'] ? : $this->notify->error('No payments made!', 'sellers_reservations') ;
			$this->view_data['reservation'] = $reservation;
		}

		if ($this->input->get()) {
			$project_id = $this->input->get('project_id');
			$property_id = $this->input->get('property_id');
			$this->view_data['info']['project']['id'] = $project_id;
			$property = $this->M_property->fields(array('name', 'model_id'))->get($property_id);
			$this->view_data['info']['property']['id'] = $property_id;
			$this->view_data['info']['property']['name'] = $property['name'];
			$this->view_data['info']['property']['model_id'] = $property['model_id'];
			$this->view_data['house'] = $this->M_house->where("id", $this->view_data['info']['property']['model_id'])->get();
		}

		if ($this->view_data['projects']) {
			if ($id) {
				$this->view_data['info'] =  $this->M_transaction
					->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()->with_scheme_values()
					->with_t_property()->with_billings()->with_buyers()
					->with_sellers()->with_fees()->with_promos()->with_tbillings()
					->get($id);

				$this->view_data['house'] = $this->M_house->where("id", $this->view_data['info']['property']['model_id'])->get();

				// vdebug($this->view_data['info']);
			}

			if ($type == "debug") {
				vdebug($this->view_data);
			}
			$this->template->build('form', $this->view_data);
		} else {
			show_404();
		}
	}

	public function save_transaction($post = array())
	{

		$info 	= $post['info'];
		$billings 	= $post['billing'];
		$property 	= $post['property'];
		$sellers 	= $post['seller'];
		$clients 	= $post['client'];
		$fees 		= $post['fees'];
		$promos 	= $post['promos'];

		$id = @$post['id'];
		$t_property_id = $property['transaction_property_id'];

		$transaction['buyer_id'] = $clients[0]['client_id'];
		$transaction['seller_id'] = $sellers[0]['seller_id'];
		$transaction['project_id'] = $property['project_id'];
		$transaction['property_id'] = $property['property_id'];
		$transaction['financing_scheme_id'] = $billings['financing_scheme_id'];
		// $transaction['commission_setup_id'] = $this->commission_setup['id'];
		$transaction['commission_setup_id'] = $billings['commission_setup_id'];
		$transaction['expiration_date'] = $billings['expiration_date'];
		$transaction['reservation_date'] = $billings['reservation_date'];
		$transaction['penalty_type'] = $billings['penalty_type'];
		$transaction['documentation_status'] = 1;
		$transaction['transaction_loan_document_stage_id'] = 0;
		$transaction['documentation_category'] = get_documentation_category($property);
		$transaction['loan_documentation_category'] = 0;
		// $transaction['tax_rate'] = $post['tax_rate'];
		$transaction['vat_amount'] = $post['vat_amount'];
		$transaction['wht_rate'] = $post['wht_rate'];
		$transaction['is_inclusive'] = $post['is_inclusive'];
		$transaction['wht_amount'] = $post['wht_amount'];
		$transaction['vat_amount'] = $post['vat_amount'];
		$transaction['ra_number'] = $post['ra_number'];
		$transaction['sales_group_id'] = $post['sales_group_id'];

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

		$transaction_id = $this->process_m_transaction($transaction, $id);



		$this->process_property($property, $transaction_id, $t_property_id);

		$this->process_terms($post, $transaction_id);

		$this->process_fees($fees, $transaction_id);

		$this->process_promos($promos, $transaction_id);

		$this->process_sellers($sellers, $transaction_id);

		$this->process_clients($clients, $transaction_id);

		// M_transaction_payment
		$this->process_mortgage($transaction_id, 'save');

		// M_transaction_commission
		$this->process_commission($transaction_id, 'save');

		$this->generate_reference($t_property_id, 1);

		if($post['reservation'] && $transaction_id ){
			$this->process_reservation($post['reservation'], $transaction_id);
		}

		return $transaction_id;
	}

	public function process_reservation($reservation_id, $transaction_id){

		$this->load->library('../modules/transaction_payment/controllers/Transaction_payment');
		$response['status'] = 0;
		$response['message'] = 'Check accounting settings for reservation!';

		$reservation = $this->M_Sellers_reservations
			->with_payment()
			->with_property(['with'=>['relation'=>'project']])
			->get($reservation_id);

		$this->M_Sellers_reservations->update([
			'reservation_status' => '2'
		] + $this->u_additional,$reservation_id);
		
		if(!isset($reservation['property']['project'])){
			$response['message'] = "Project doesn't exist!";
			echo json_encode($response);
			exit();
		}

		$accounting_setting = $this->get_entries($reservation['property']['project']['company_id'], $reservation['property']['project']['id'], 1, 1, 1, 6);
		
		if(!$accounting_setting){
			echo json_encode($response);
			exit();
		}

		$accounting_items = $accounting_setting['accounting_setting_item'];

		$payment = $reservation['payment'];

		$entry_data = [
			'company_id' => $reservation['property']['project']['company_id'],
			'or_number' => $payment['or_number'],
			'invoice_number' => $payment['or_number'],
			'journal_type' => 3,
			'payment_date' => $payment['date_paid'],
			'payee_type' => 'buyers',
			'payee_type_id' => $reservation['buyer_id'],
			'remarks' => 'Seller Reservation',
			'cr_total' => $payment['amount'],
			'dr_total' => $payment['amount'],
			'is_approve' => 1,
			'project_id' => $reservation['property']['project']['id'],
			'property_id' => $reservation['property_id']
		];
		
		$accounting_entry_id = $this->M_Accounting_entries->insert($entry_data+$this->c_additional);
		foreach($accounting_items as $item_key=>$item){
			$entry_item_data = [
				'accounting_entry_id' => $accounting_entry_id,
				'ledger_id' => $item['accounting_entry'],
				'amount' => $payment['amount'],
				'dc' => $item['accounting_entry_type'],
				'payee_type' => 'buyers',
				'payee_type_id' => $reservation['buyer_id'],
				'description' => 'Seller Reservation'
			];
			$this->M_Accounting_entry_items->insert($entry_item_data + $this->c_additional);
		}
		// vdebug('test1');
		$payment_info = [
			'transaction_id' => $transaction_id,
			'info' => [
				'period_count' => 0,
				'period_id' => 1,
				'payment_date' => $payment['date_paid'],
				'or_date' => $payment['date_paid'],
				'is_waived' => 1,
				'penalty_amount' => 0,
				'rebate_amount' => 0,
				'amount_paid' => $payment['amount'],
				'payment_type_id' => '1',
				'post_dated_check_id' => '0',
				'rc_bank_name' => '',
				'rc_check_number' => '0',
				'cash_amount' => 0,
				'check_deposit_amount' => '0',
				'receipt_type' => $payment['receipt_type'],
				'OR_number' => $payment['or_number'],
				'adhoc_payment' => '0',
				'grand_total' => $payment['amount'],
				'remarks' => 'Reservation',
			],
			'transaction_payment_id' => '',
			'payment_application' => '0'
		];
		$this->transaction_payment->insert_payment($payment_info);

	}

	public function get_entries($company_id = 0, $project_id = 0, $period_id, $category_type, $entry_types_id, $module_types_id)
    {
        $filter = [
            'company_id' => $company_id,
            'project_id' => $project_id,
            'period_id' => $period_id,
            'category_type' => $category_type,
            'entry_types_id' => $entry_types_id,
            'module_types_id' => $module_types_id
        ];
        $accounting_setting = $this->M_Accounting_settings->with_accounting_setting_item()->get($filter);
        if (!$accounting_setting) {
            unset($filter['project_id']);
            $accounting_setting = $this->M_Accounting_settings->with_accounting_setting_item()->get($filter);
        }
        return $accounting_setting;
    }

	//Use this function to update the transaction references ------------------->

	public function update_transaction_references($debug = 0)
	{
		$transactions = $this->M_transaction->get_all();
		foreach ($transactions as $key => $value) {
			if ($debug) {
				$reference[$key] = $this->generate_reference($value['property_id'], 1);
			} else {
				$reference = $this->generate_reference($value['property_id'], 1);
			}
		}
		if ($debug) {
			vdebug($reference);
		}
	}

	//<---------------------------------------------------------------------------

	public function generate_reference($property_id = 0, $update_existing = 0)
	{
		$cnt = 1;
		$this->db->where('property_id', $property_id);
		$this->db->order_by('created_at');
		$transactions = $this->M_transaction->get_all();
		$property_name = $this->M_property->fields('name')->get($property_id)['name'];

		if ($update_existing) {
			if ($transactions) {
				foreach ($transactions as $key => $transaction) {
					$reference = $property_name . '/' . sprintf('%03d', $cnt++);
					$new_id = ['reference' => $reference];
					$this->M_transaction->update($new_id + $this->u_additional, $transaction['id']);
				}
			}
		} else {

			if ($transactions) {
				$reference = $property_name . '/' . sprintf('%03d', count($transactions) + 1);
			} else {
				$reference = $property_name . '/' . sprintf('%03d', 1);
			}
			return $reference;
		}
	}

	public function process_m_transaction($transaction = array(), $id = 0)
	{

		if ($id) {
			# code...
			$transaction_id = $this->M_transaction->update($transaction + $this->u_additional, $id);
		} else {
			$transaction['period_id'] = 1;
			$transaction['reference'] = uniqidReal();
			$transaction_id = $this->M_transaction->insert($transaction + $this->additional);
		}

		return $transaction_id;
	}

	public function process_property($property = array(), $transaction_id = 0, $t_property_id = 0)
	{
		// M_transaction_property
		$property['total_miscellaneous_amount'] = 0;
		$property['total_discount_price'] = 0;
		$property['transaction_id'] = $transaction_id;
		unset($property['transaction_property_id']);

		// $general_category_id = 1;
		// if($property['property_types'] > 2){ $general_category_id = 2; }
		// $property_id = get_value_field($transaction_id,'transaction_properties','property_id','transaction_id');
		// general_category_id
		// 1 = Residential Lot
		// 2 = Commercial Lot
		// 3 = House and Lot
		// 4 = Condominium

		$general_category_id = get_documentation_category($property);

		$this->process_documents($transaction_id, $general_category_id);

		if ($t_property_id) {
			$transaction['project_id'] = $property['project_id'];
			$transaction['property_id'] = $property['property_id'];
			$this->M_transaction->update($transaction + $this->u_additional, $transaction_id);

			$this->M_transaction_property->update($property + $this->u_additional, $t_property_id);
		} else {
			$this->M_transaction_property->insert($property + $this->additional);

			$original_property['status'] = 2;
			$this->M_property->update($original_property + $this->u_additional, $property['property_id']);
		}
	}

	public function process_terms($post = array(), $transaction_id = 0)
	{

		$terms 		= $post['period_term'];
		$interest_rate 		= $post['interest_rate'];
		$percentage_rate	= $post['percentage_rate'];
		$period_amount 		= $post['period_amount'];
		$effectivity_date 	= $post['effectivity_date'];
		$period_id 			= $post['period_id'];

		if ($terms) {
			$order_id = 0;
			foreach ($terms as $key => $term) {
				$order_id++;
				// M_transaction_billing
				$billing['period_term'] = $term;
				$billing['order_id'] = $order_id;
				$billing['period_amount'] = $period_amount[$key];
				$billing['interest_rate'] = $interest_rate[$key];
				$billing['effectivity_date'] = $effectivity_date[$key];
				$billing['period_id'] = $period_id[$key];
				$billing['transaction_id'] = $transaction_id;

				$billing_id = @$billing_id[$key];

				if ($billing_id) {
					# code...
					$this->M_transaction_billing->update($billing + $this->u_additional, $billing_id);
				} else {
					$this->M_transaction_billing->insert($billing + $this->additional, $billing_id);
				}
			}
		}
	}

	public function process_fees($fees = array(), $transaction_id = 0)
	{
		if ($fees) {
			foreach ($fees as $key => $fee) {
				$fee['transaction_id'] = $transaction_id;

				if ($fee['fees_id']) {
					// M_transaction_fee
					$this->M_transaction_fee->insert($fee + $this->additional);
				}
			}
		}
	}

	public function process_promos($promos = array(), $transaction_id = 0)
	{
		if ($promos) {
			foreach ($promos as $key => $promo) {

				if (isset($promo['discount_id'])) {
					$info['transaction_id'] = $transaction_id;
					$info['discount_id'] = $promo['discount_id'];
					$info['discount_amount_rate'] = $promo['promo_amount'];
					$info['deduct_to'] = $promo['discount_id'] == 1 ? 'Loan' : 'TCP';
					$info['remarks'] = $promo['discount_id'] == 1 ? 'Deductable to Loan' : 'Deductable to TCP';
					// M_transaction_discount
					$this->M_transaction_discount->insert($info + $this->additional);
				}
			}
		}
	}

	public function process_sellers($sellers = array(), $transaction_id = 0)
	{

		if ($sellers) {
			foreach ($sellers as $key => $seller) {
				// M_transaction_seller
				$seller['transaction_id'] = $transaction_id;
				$this->M_transaction_seller->insert($seller + $this->additional);
			}
		}
	}

	public function process_clients($clients = array(), $transaction_id = 0)
	{

		if ($clients) {
			foreach ($clients as $key => $client) {
				// M_transaction_client
				$client['transaction_id'] = $transaction_id;
				$this->M_transaction_client->insert($client + $this->additional);
			}
		}
	}

	public function process_documents($transaction_id = 0, $general_category_id = 0, $loan_category_id = 0, $debug = 0)
	{
		//loan_category_id = 1; HDMF Window 2 
		//loan_category_id = 2; HDMF Retail Window 
		//loan_category_id = 3; Bank


		//transaction_id = 1; Lots Only
		//transaction_id = 2; Downpayment
		//transaction_id = 3; TCT

		if ($general_category_id) {
			# code...
			$stages = $this->M_transaction_document_stage->order_by('order_by', 'ASC')->where('category_id', $general_category_id)->or_where('category_id', '3')->get_all();

			foreach ($stages as $key => $stage) {
				# code...
				$checklist = $this->M_checklist->where('transaction_document_stage_id', $stage['id'])->get();

				$documents = $this->M_document_checklist->with_document()->where('checklist_id', $checklist['id'])->get_all();

				if ($documents) {

					foreach ($documents as $key => $document) {
						# code...
						$doc = $document['document'];

						// Loop through document checklist
						$data['transaction_id'] = $transaction_id;
						$data['document_id'] = $doc['id'];
						$data['checklist'] = $checklist['id'];
						$data['category_id'] = 4; //sales
						$data['owner'] = $this->get_document_owner($doc['id']);
						$data['checklist'] = $checklist['id'];
						$data['start_date'] = NOW;
						$data['due_date'] = NOW;
						$data['status'] = 0;
						$data['timeline'] = 0;
						$data['transaction_document_stage_id'] = $stage['id'];

						// Insert documents
						$this->M_transaction_document->insert($data);
					}
				}
			}
		}

		if ($loan_category_id) {
			# code...
			$stages = $this->M_transaction_loan_document_stage->order_by('order_by', 'ASC')->where('category_id', $loan_category_id)->get_all();

			if ($stages) {
				foreach ($stages as $key => $stage) {
					# code...
					$checklist = $this->M_checklist->where('loan_document_stage_id', $stage['id'])->get();

					$documents = $this->M_document_checklist->with_document()->where('checklist_id', $checklist['id'])->get_all();

					if ($debug) {
						vdebug($checklist);
					}
					if ($documents) {

						foreach ($documents as $key => $document) {
							# code...
							$doc = $document['document'];

							// Loop through document checklist
							$data['transaction_id'] = $transaction_id;
							$data['document_id'] = $doc['id'];
							$data['checklist'] = $checklist['id'];
							$data['category_id'] = 4; //sales
							$data['owner'] = $this->get_document_owner($doc['id']);
							$data['checklist'] = $checklist['id'];
							$data['start_date'] = NOW;
							$data['due_date'] = NOW;
							$data['status'] = 0;
							$data['timeline'] = 0;
							$data['transaction_loan_document_stage_id'] = $stage['id'];

							// Insert documents
							$this->M_transaction_loan_document->insert($data);
						}
					}
				}
			}
		}
	}

	public function delete($transaction_id = 0)
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if (!$transaction_id) {
			$id	= $this->input->post('id');
		} else {
			$id = $transaction_id;
		}
		if ($id) {

			$list['id'] = $id;

			if ($list) {

				$deleted = $this->M_transaction->delete($list['id']);

				$x = $this->M_transaction_billing->delete(array('transaction_id' => $list['id']));

				$this->M_transaction_client->delete(array('transaction_id' => $list['id']));
				$this->M_transaction_discount->delete(array('transaction_id' => $list['id']));
				$this->M_transaction_fee->delete(array('transaction_id' => $list['id']));
				$this->M_transaction_payment->delete(array('transaction_id' => $list['id']));

				$this->M_transaction_property->delete(array('transaction_id' => $list['id']));
				$this->M_transaction_seller->delete(array('transaction_id' => $list['id']));
				$this->M_Transaction_commission->delete(array('transaction_id' => $list['id']));
				$this->M_Transaction_ticketing->delete(array('transaction_id' => $list['id']));
				$this->M_transaction_post_dated_check->delete(array('transaction_id' => $list['id']));

				$this->M_Tbilling_logs->delete(array('transaction_id' => $list['id']));
				$this->M_tpenalty_logs->delete(array('transaction_id' => $list['id']));
				$this->M_refund_logs->delete(array('transaction_id' => $list['id']));

				$this->M_Transaction_financing_scheme_log->delete(array('transaction_id' => $list['id']));
				$this->M_Transaction_adhoc_fee->delete(array('transaction_id' => $list['id']));
				$this->M_Transaction_adhoc_fee_schedule->delete(array('transaction_id' => $list['id']));

				$this->M_Transaction_past_due_notice->delete(array('transaction_id' => $list['id']));
				$this->M_letter_request->delete(array('transaction_id' => $list['id']));

				$this->M_transaction_personnel_logs->delete(array('transaction_id' => $list['id']));
				$this->M_transaction_document->delete(array('transaction_id' => $list['id']));

				$this->M_transaction_loan_document->delete(array('transaction_id' => $list['id']));

				$official_payments = $this->M_Transaction_official_payment->where('transaction_id', $list['id'])->get_all();

				if ($official_payments) {
					foreach ($official_payments as $key => $official_payment) {

						$entry_id = $official_payment['entry_id'];
						$this->M_Accounting_entries->delete(array('id' => $entry_id));
						$this->M_Accounting_entry_items->delete(array('accounting_entry_id' => $entry_id));
					}
				}

				$this->M_Transaction_official_payment->delete(array('transaction_id' => $list['id']));

				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Transaction successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function bulkDelete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_transaction->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Transaction successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}


	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];
		$_extra_datas		=	[];
		$_adatas			=	[];

		$_titles[]	=	'#';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		//$transactions	=	$this->M_transaction->as_array()->get_all();

		// vdebug($this->input->post());

		$general_status = $this->input->post('general_status');
		$collection_status = $this->input->post('collection_status');
		$documentation_status = $this->input->post('documentation_status');
		$reference = $this->input->post('reference');
		$project_id = $this->input->post('project_id');
		$property_id = $this->input->post('property_id');
		$period_id = $this->input->post('period');
		$buyer_id = $this->input->post('buyer_id');
		$seller_id = $this->input->post('seller_id');
		$reservation_date = $this->input->post('reservation_date');
		$due_for_payment_date = $this->input->post('due_for_payment_date');
		$personnel_id = $this->input->post('personnel_id');
		$day = $this->input->post('day');
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$page = $this->input->post('page');

		$greater_than = $this->input->post('greater_than');
			$equal_to = $this->input->post('equal_to');
			$lower_than = $this->input->post('lower_than');


			$dp_greater_than = $this->input->post('dp_greater_than');
			$dp_equal_to = $this->input->post('dp_equal_to');
			$dp_lower_than = $this->input->post('dp_lower_than');

		if (!empty($general_status) && !empty($general_status)) :
			$this->db->where('general_status', $general_status);
		endif;

		if (!empty($collection_status) && !empty($collection_status)) :
			$this->db->where('collection_status', $collection_status);
		endif;

		if (!empty($documentation_status) && !empty($documentation_status)) :
			$this->db->where('documentation_status', $documentation_status);
		endif;

		if (!empty($reference) && !empty($reference)) :
			$this->db->where('reference', $reference);
		endif;

		if (!empty($project_id) && !empty($project_id)) :
			$this->db->where('project_id', $project_id);
		endif;

		if (!empty($property_id) && !empty($property_id)) :
			$this->db->where('property_id', $property_id);
		endif;

		if (!empty($period_id) && !empty($period_id)) :
			$this->db->where('period_id', $period_id);
		endif;

		if (!empty($buyer_id) && !empty($buyer_id)) :
			$this->db->where('buyer_id', $buyer_id);
		endif;

		if (!empty($seller_id) && !empty($seller_id)) :
			$this->db->where('seller_id', $seller_id);
		endif;

		if (!empty($day) && !empty($day)) :
			$this->db->like('reservation_date', $day, "both");
		endif;

		if (!empty($month) && !empty($month)) :
			$this->db->like('reservation_date', $month, "both");
		endif;

		if (!empty($year) && !empty($year)) :
			$this->db->like('reservation_date', $year, "both");
		endif;

		if (!empty($reservation_date) && !empty($reservation_date)) :
			$this->db->where('reservation_date BETWEEN "' . $from . '" AND "' .  $to . '"');
		endif;

		if (!empty($due_for_payment_date) && !empty($due_for_payment_date)) :
			$this->db->where('due_date BETWEEN "' . $from_due . '" AND "' .  $to_due . '"');
		endif;

		if (!empty($personnel_id) && !empty($personnel_id)) :
			$this->db->where('personnel_id', $personnel_id);
		endif;

		if (!empty($greater_than) && empty($lower_than) && empty($equal_to)) :
				$this->db->where('tpm_p > "' . $greater_than.'"');
			endif;

			if (!empty($lower_than) && empty($greater_than) && empty($equal_to)) :
				$this->db->where('tpm_p < "' . $lower_than.'"');
			endif;

			if (!empty($equal_to) && empty($greater_than) && empty($lower_than)) :
				$this->db->where('tpm_p = "' . $equal_to.'"');
			endif;

			if (!empty($greater_than) && empty($lower_than) && !empty($equal_to)) :
				$this->db->where('tpm_p >= "' . $greater_than.'"');
			endif;

			if (!empty($lower_than) && empty($greater_than) && !empty($equal_to)) :
				$this->db->where('tpm_p <= "' . $lower_than.'"');
			endif;

			if (empty($equal_to) && !empty($greater_than) && !empty($lower_than)) :
				$this->db->where('tpm_p < "' . $lower_than.'" AND tpm_p> "' . $greater_than.'"');
			endif;
			
		if (!empty($dp_greater_than) && empty($dp_lower_than) && empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp > "' . $dp_greater_than.'"');
			endif;

			if (!empty($dp_lower_than) && empty($dp_greater_than) && empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp < "' . $dp_lower_than.'"');
			endif;

			if (!empty($dp_greater_than) && empty($dp_lower_than) && !empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp >= "' . $dp_greater_than.'"');
			endif;

			if (!empty($dp_lower_than) && empty($dp_greater_than) && !empty($dp_equal_to)) :
				$this->db->where('tpm_p_dp <= "' . $dp_lower_than.'"');
			endif;

			if (!empty($dp_equal_to) && empty($dp_greater_than) && empty($dp_lower_than)) :
				$this->db->where('tpm_p_dp = "' . $dp_equal_to.'"');
			endif;

		if (empty($dp_equal_to) && !empty($dp_greater_than) && !empty($dp_lower_than)) :
				$this->db->where('tpm_p_dp < "' . $dp_lower_than.'" AND tpm_p_dp > "' . $dp_greater_than.'"');
			endif;

		$transactions = $this->M_transaction->order_by('reservation_date', 'DESC')->as_array()->get_all();

		if ($transactions) {

			foreach ($transactions as $skey => $transaction) {

				$_datas[$transaction['id']]['#']	=	$_no;

				$_no++;
			}


			$_filename	=	'list_of_transactions' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				$_additional_column	=	$this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {



						$_column	=	$_name;

						$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

						$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

						foreach ($transactions as $skey => $transaction) {

							// $documentation_status =  Dropdown::get_dynamic('transaction_document_stages', $r['documentation_status'], 'name', 'id', 'value');

							if ($_column === 'period_id') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? $transaction[$_column] : '';
							} elseif ($_column === 'buyer_id') {

								$buyer_last_name	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('buyers', $transaction[$_column], 'last_name', 'id', 'view') : '';
								$buyer_first_name	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('buyers', $transaction[$_column], 'first_name', 'id', 'view') : '';
								$_datas[$transaction['id']][$_title] = $buyer_first_name . ' ' . $buyer_last_name;
							} elseif ($_column === 'seller_id') {

								$seller_last_name	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('sellers', $transaction[$_column], 'last_name', 'id', 'view') : '';
								$seller_first_name	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('sellers', $transaction[$_column], 'first_name', 'id', 'view') : '';
								$_datas[$transaction['id']][$_title] = $seller_first_name . ' ' . $seller_last_name;
							} elseif ($_column === 'project_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('projects', $transaction[$_column], 'name', 'id', 'view') : '';
							} elseif ($_column === 'property_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ?  Dropdown::get_dynamic('properties', $transaction[$_column], 'name', 'id', 'view') : '';
							} elseif ($_column === 'financing_scheme_id') {

								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('financing_schemes', $transaction[$_column], 'name', 'id', 'view') : '';
							} elseif ($_column === 'day') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction["reservation_date"]) && $transaction["reservation_date"] ? view_date($transaction['reservation_date'], "day") : '';
							} elseif ($_column === 'month') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction["reservation_date"]) && $transaction["reservation_date"] ? view_date($transaction['reservation_date'], "month") : '';
							} elseif ($_column === 'year') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction["reservation_date"]) && $transaction["reservation_date"] ? view_date($transaction['reservation_date'], "year") : '';
							} elseif ($_column == 'general_status') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ?  Dropdown::get_static('general_status', $transaction[$_column]) : '';
							} elseif ($_column == 'collection_status') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ?  Dropdown::get_static('collection_status', $transaction[$_column]) : '';
							} elseif ($_column == 'is_recognized') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ?  'Recognized' : 'Not Recognized';
							} elseif ($_column == 'transaction_loan_document_stage_id') {
								$loan_documentation_status =  Dropdown::get_dynamic('transaction_loan_document_stages', $transaction[$_column], 'name', 'id', 'value');
								$_datas[$transaction['id']][$_title] = ($loan_documentation_status) ?: "No Loan Documents Assigned";
							} elseif ($_column == 'documentation_status') {
								$documentation_status =  Dropdown::get_dynamic('transaction_document_stages', $transaction[$_column], 'name', 'id', 'value');
								$_datas[$transaction['id']][$_title] = ($documentation_status) ?: "No Documents Assigned";
							} elseif ($_column == 'penalty_type') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ?  Dropdown::get_static('penalty_types', $transaction[$_column]) : '';
							} elseif ($_column == 'loan_documentation_category' || $_column == 'documentation_category') {
								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? $transaction[$_column] : 'No Category';
							} elseif ($_column == 'vat_amount') {
								$_datas[$transaction['id']][$_title]	=	number_format(floatval($transaction[$_column]), 2, '.', '');
							} elseif ($_column == 'wht_amount') {
								$_datas[$transaction['id']][$_title]	=	number_format(floatval($transaction[$_column]), 2, '.', '');
							} elseif ($_column == 'wht_rate') {
								$_datas[$transaction['id']][$_title]	=	number_format(floatval($transaction[$_column]), 2, '.', '');
							} elseif ($_column == 'is_active') {
								$_datas[$transaction['id']][$_title]	=	$transaction[$_column] ? 'Active' : 'Inactive';
							} elseif ($_column == 'is_inclusive') {
								$_datas[$transaction['id']][$_title]	=	$transaction[$_column] ? 'Inclusive' : 'Non-inclusive';
							} else {
								$_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title	=	ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('List of Transactions');
				$_objSheet->setCellValue('A1', 'LIST OF TRANSACTIONS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($transactions as $key => $transaction) {

					$transaction_id	=	isset($transaction['id']) && $transaction['id'] ? $transaction['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title	=	ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$transaction_id][$_xls_columns[$_alpha]]) && $_datas[$transaction_id][$_xls_columns[$_alpha]] ? $_datas[$transaction_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns	=	$_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							// if($_a_column === 'contact') {

							// 	$ad_title	=	'Contact Informations';
							// } elseif($_a_column === 'reference') {

							// 	$ad_title	=	'References';
							// } elseif($_a_column === 'source') {

							// 	$ad_title	=	'Source of Informations';
							// } elseif($_a_column === 'academic'){

							// 	$ad_title	=	'Academic History';
							// }else {

							// 	$ad_title = $_a_column;
							// }

							$a_title	=	ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if ($key === 'to_report' or $key === 'to_attend' or $key === 'commission_based' or $key === 'is_member') {
												$xa_value	=	isset($_extra_datas[$transaction_id][$_a_column][$key]) && ($_extra_datas[$transaction_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$transaction_id][$_a_column][$key], 'view') : '';
											} else {
												$xa_value	=	$_extra_datas[$transaction_id][$_a_column][$key];
											}

											$xa_titles =	isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$transaction_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$transaction_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					}

					$_start += 1;
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}
		} else {

			$this->notify->error('No Record Found', 'transaction');
		}
	}

	function export_csv_transactions()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Transaction CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_transaction->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Transactions');
			$_objSheet->setCellValue('A1', 'TRANSACTIONS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function export_csv_transaction_properties()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Transaction Properties CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			// $_fillables	=	$this->_table_fillables;
			$_fillables = $this->M_transaction_property->fillable;
			// $_fillables = $this->M_transaction_billing->fillable;
			// $_fillables = $this->M_transaction_client->fillable;
			// $_fillables = $this->M_transaction_seller->fillable;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_transaction_property->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Transactions');
			$_objSheet->setCellValue('A1', 'TRANSACTION PROPERTIES');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}
	function export_csv_transaction_billings()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Transaction Billings CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			// $_fillables	=	$this->_table_fillables;
			// $_fillables = $this->M_transaction_property->fillable;
			$_fillables = $this->M_transaction_billing->fillable;
			// $_fillables = $this->M_transaction_client->fillable;
			// $_fillables = $this->M_transaction_seller->fillable;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_transaction_billing->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Transactions');
			$_objSheet->setCellValue('A1', 'TRANSACTION BILLINGS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}
	function export_csv_transaction_clients()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Transaction Clients CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			// $_fillables	=	$this->_table_fillables;
			// $_fillables = $this->M_transaction_property->fillable;
			// $_fillables = $this->M_transaction_billing->fillable;
			$_fillables = $this->M_transaction_client->fillable;
			// $_fillables = $this->M_transaction_seller->fillable;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_transaction_client->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Transactions');
			$_objSheet->setCellValue('A1', 'TRANSACTION CLIENTS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}
	function export_csv_transaction_sellers()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Transaction Sellers CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			// $_fillables	=	$this->_table_fillables;
			// $_fillables = $this->M_transaction_property->fillable;
			// $_fillables = $this->M_transaction_billing->fillable;
			// $_fillables = $this->M_transaction_client->fillable;
			$_fillables = $this->M_transaction_seller->fillable;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_transaction_seller->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Transactions');
			$_objSheet->setCellValue('A1', 'TRANSACTION SELLERS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {
				// vdebug($this->input->post());
				$post = $this->input->post();

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_failed_reasons = [];
				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				$doc_type = $post['doc_type'];

				$_fillables = array();

				switch ($doc_type) {

					case 'transactions':
						$_fillables = $this->M_transaction->fillable;
						break;

					case 'transaction_properties':
						$_fillables = $this->M_transaction_property->fillable;
						break;

					case 'transaction_billings':
						$_fillables = $this->M_transaction_billing->fillable;
						break;

					case 'transaction_clients':
						$_fillables = $this->M_transaction_client->fillable;
						break;

					case 'transaction_sellers':
						$_fillables = $this->M_transaction_seller->fillable;
						break;

					default:
						$response['status']	 =	0;
						$response['message'] = 'Please select a document type.';
						echo json_encode($response);
						exit();
						break;
				}

				// vdebug($_fillables);

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					// $this->notify->error($_msg, 'document');
					$response['status']	 =	0;
					$response['message'] = $_msg;
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					// $_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						// $this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
						$response['status']	 =	0;
						$response['message'] = 'Something went wrong. Please refresh the page and try again.';
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {
						foreach ($_datas as $_dkey => $_data) {
							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							// vdebug($_data);
							$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_data['created_at']	=	NOW;
							$_data['reference'] = uniqidReal();
							$_data['remarks'] = '';

							$_insert	=	$this->db->insert($doc_type, $_data);
						}
						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {
							$response['status']	 =	0;
							$response['message'] = 'Upload Failed! Please follow upload guide.';
						} else {

							$response['status']	 =	1;
							$response['message'] = $_msg;
						}
					}
				} else {

					// $this->notify->warning('CSV was empty.', 'transaction');
					$response['status']	 =	0;
					$response['message'] = 'CSV was empty.';
				}
			} else {

				// $this->notify->warning('Not a CSV file!', 'transaction');
				$response['status']	 =	0;
				$response['message'] = 'Not a CSV file!';
			}
		} else {

			// $this->notify->error('Something went wrong!', 'transaction');
			$response['status']	 =	0;
			$response['message'] = 'Something went wrong!';
		}

		echo json_encode($response);
		exit();
	}

	public function process_mortgage($transaction_id = 0, $type = "", $process = "", $tb_id = "")
	{
		// function to recalculate billing schedule

		$this->transaction_library->initiate($transaction_id);

		$monthly_payment = 0;

		$breakdown = $this->transaction_library->process_mortgage($transaction_id, $type, $process, $this->user->id, $tb_id);

		if ($type != "save") {

			vdebug($breakdown);
		}

		if ($process == "recompute") {

			$this->M_Transaction_commission->delete(array('transaction_id' => $transaction_id));

			$u_additional = [
				'updated_by' => $this->user->id,
				'updated_at' => NOW
			];
			$this->db->reset_query();
			$this->M_transaction->update(array('period_id' => 1) + $u_additional, $transaction_id);

			$this->process_commission($transaction_id, 'save');


			redirect('transaction/view/' . $transaction_id);
		} else if ($process == "recompute_remainings") {

			redirect('transaction/view/' . $transaction_id);
		}
	}


	public function process_commission($transaction_id = 0, $type = "")
	{
		// function to recalculate commission schedule

		$transaction = $this->M_transaction->with_t_property()->with_sellers()->get($transaction_id);
		$billing = $this->M_transaction_billing->fields('period_amount')->where('period_id', 1)->where('transaction_id', $transaction_id)->get();

		$reservation_rate = $billing['period_amount'];
		$collectible_price = $transaction['t_property']['collectible_price'];
		$commissionable_amount = $transaction['t_property']['commissionable_amount'];

		$financing_scheme_id = $transaction['financing_scheme_id'];

		$sellers = $transaction['sellers'];

		// $commission_values = $this->M_scheme_commission->where('financing_id',$financing_scheme_id)->get_all();
		$commission_setup = $this->M_commission->with_commission_setup_values()->where("id", $transaction['commission_setup_id'])->get();

		// $commission_setup = $this->commission_setup;
		$commission_values = $commission_setup['commission_setup_values'];
		$commissionable_amount = $transaction['t_property']['commissionable_amount'];

		$additional = [
			'created_by' => $this->user->id,
			'created_at' => NOW
		];

		$u_additional = [
			'updated_by' => $this->user->id,
			'updated_at' => NOW
		];

		if ($sellers) {

			// Delete current transaction commissions
			if ($type == "update") {

				$current_transaction_commissions = $this->M_Transaction_commission->where('transaction_id', $transaction_id)->get_all();

				if ($current_transaction_commissions) {
					// code...
					foreach ($current_transaction_commissions as $transaction_commission) {
						$this->M_Transaction_commission->delete($transaction_commission['id']);
					}
				}
			}

			foreach ($sellers as $key => $seller) {

				$sellers_rate_amount = $seller['sellers_rate_amount'];
				$commission_percentage_rate = $seller['sellers_rate'];
				$commission_amount_rate = get_percentage_amount($seller['sellers_rate_amount'], $seller['sellers_rate']);

				if ($type == "update") {
					# code...
					$ts['commission_amount_rate'] = $commission_amount_rate;

					$y['transaction_id'] = $transaction_id;
					$y['seller_id'] = $seller['seller_id'];
					$y['deleted_at'] = NULL;

					$id = get_value_field_multiple($y, 'transaction_sellers', 'id');

					$uts = $this->M_transaction_seller->update($ts + $this->u_additional, $id);
				}

				$first_release_amount = 0;

				if ($commission_values) {

					foreach ($commission_values as $key => $c_val) {

						$period_id = $c_val['period_id'];

						$percentage_rate_to_collect = $c_val['percentage_to_finish'];

						if ($period_id == 1) {

							$amount_rate_to_collect = ($c_val['percentage_to_finish'] / 100) * $reservation_rate;
							$remarks = "Pay reservation fee to release cash advance";

							$commission_percentage_rate = 0;

							if ($seller['commission_amount_rate']) {
								$commission_amount_rate = $seller['commission_amount_rate'];
							} else {
								$commission_amount_rate = $c_val['commission_rate_amount'];
							}

							$sellers_rate_amount = $sellers_rate_amount - $commission_amount_rate;
						} else {

							// new commission schedule
							$sellers_rate_amount = $sellers_rate_amount - $first_release_amount;

							$wht_amount = $sellers_rate_amount * $commission_setup['wht_percentage'];
							$commission_percentage_rate = $c_val['commission_rate_percentage'];

							$amount_rate_to_collect = ($c_val['percentage_to_finish'] / 100) * $collectible_price;
							$remarks = "Pay " . $c_val['percentage_to_finish'] . "% of collectible price fee to release " . $commission_percentage_rate . "% of commission";

							$commission_amount_rate = $sellers_rate_amount * ($c_val['commission_rate_percentage'] / 100);

							// old process, deduct the advance comm to the next comm, remove this
							// if ($key == 1) {
							// 	$commission_amount_rate = $commission_amount_rate - $first_release_amount;
							// }
						}

						$wht_rate = $c_val['commission_vat'];
						$status = 1;

						$i['transaction_id'] = $transaction_id;
						$i['seller_id'] = $seller['seller_id'];
						$i['period_type_id'] = $period_id;
						$i['percentage_rate_to_collect'] = $percentage_rate_to_collect;
						$i['amount_rate_to_collect'] = $amount_rate_to_collect;
						$i['commission_percentage_rate'] = $commission_percentage_rate;
						$i['commission_amount_rate'] = $commission_amount_rate;
						$i['wht_rate'] = $wht_rate;
						$i['status'] = $status;
						$i['remarks'] = $remarks;
						$i['target_date'] = $this->M_Transaction_commission->get_target_date($period_id, $transaction_id, $amount_rate_to_collect);

						if ($type == "save") {

							$this->M_Transaction_commission->insert($i + $additional);
						} else if ($type == "update") {

							// $w['transaction_id'] = $transaction_id;
							// $w['seller_id'] = $seller['seller_id'];
							// $w['period_type_id'] = $period_id;
							// $w['deleted_at'] = NULL;

							// $id = get_value_field_multiple($w, 'transaction_commissions', 'id');

							// $u = $this->M_Transaction_commission->update($i + $this->u_additional, $id);

							$this->M_Transaction_commission->insert($i + $additional);
						} else {

							$breakdown[] = $i;
						}
					}
				}
			}
		}


		if ($type == "debug") {

			vdebug($breakdown);
		}
	}

	public function view_mortgage($transaction_id = 0, $type = "")
	{

		$this->view_data['transaction'] = $this->M_transaction->with_payments()->get($transaction_id);

		$this->view_data['payments'] = $this->view_data['transaction']['payments'];

		if ($type == "view") {

			$this->template->build('view/_mortgage_schedule_information', $this->view_data);
		} else {
			$return['html'] = $this->load->view('transaction/view/_mortgage_schedule_information', $this->view_data, true);

			echo json_encode($return);
		}
	}


	public function view_commission($transaction_id = 0, $type = "")
	{

		$this->view_data['sellers'] = $this->M_transaction_seller->where('transaction_id', $transaction_id)
			->with_seller()->with_commissions()->get_all();
		vdebug($this->view_data['sellers']);
		$this->template->build('view/_commission_schedule_information', $this->view_data);
	}


	public function view_ledger($transaction_id = 0, $type = 0, $debug = 0)
	{


		if (!$transaction_id) {
			redirect('transaction');
		}

		$this->view_data['info'] =  $this->M_transaction
			->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
			->with_t_property()->with_billings()->with_buyers()
			->with_sellers()->with_fees()->with_promos()
			->get($transaction_id);

		$order['period_id'] = 'ASC';
		$order['id'] = 'ASC';

		$this->view_data['payments'] = $this->M_transaction_payment->where('transaction_id', $transaction_id)->order_by($order)->with_official_payments()->get_all();

		$this->view_data['official_payments'] = $this->M_Transaction_official_payment->where('transaction_id', $transaction_id)->get_all();

		$this->view_data['sellers'] = $this->M_transaction_seller->where('transaction_id', $transaction_id)
			->with_seller()->with_commissions()->get_all();

		$this->view_data['project'] = $this->M_project->where('id', $this->view_data['info']['project_id'])->with_company()->get();

		$this->view_data['project_name'] = $this->view_data['project']['name'];
		$this->view_data['company_name'] = $this->view_data['project']['company']['name'];
		$this->view_data['location'] = $this->view_data['project']['company']['location'];

		$this->view_data['transaction_id'] = $transaction_id;

		if ($type == 1) {
			$this->view_data['fileTitle'] = "Customer Ledger";
			$file = "ledger";
		} else if ($type == 2) {
			$this->view_data['fileTitle'] = "Customer Ledger - Internal";
			$file = "internal_ledger";
		}

		if ($debug) {
			vdebug($this->view_data);
		}

		$this->load->view('transaction/reports/' . $file, $this->view_data);
	}

	public function process_transaction($type = 0, $id = 0, $post = array())
	{

		if (empty($post)) {
			$post = $this->input->post();

			if (!isset($id) || ($id)) {
				$transaction_id = $id;
			} elseif (isset($post['info'])) {
				if ($post['type'] != 'change_property') {
					$post = (array) json_decode($post['info']);
					$transaction_id = isset($post['transaction_id']) && $post['transaction_id'] ? $post['transaction_id'] : $post['transaction_id'];
				} else {
					$transaction_id = $post['transaction_id'];
				}
			} else {
				$transaction_id = $post['transaction_id'];
			}
		} else {
			$transaction_id = $post['id'];
		}

		if ($post) {
			// saving modal
			$this->transaction_library->initiate($transaction_id);
			$return['data'] = $post;

			$additional = [
				'created_by' => $this->user->id,
				'created_at' => NOW
			];

			$u_additional = [
				'updated_by' => $this->user->id,
				'updated_at' => NOW
			];

			if ($post['type'] == "compute_refund") {

				$rf['transaction_id'] = $post['transaction_id'];
				$rf['refund_amount'] = remove_commas($post['refund']);
				$rf['refund_percentage'] = $post['refund_percentage'];
				$rf['processing_fee'] = $post['processing_fee'];
				$rf['misc_fee'] = $post['misc_fee'];
				$rf['penalty'] = $post['penalty'];
				$rf['remarks'] =  trim($post['reason']);
				$rf['date_cancellation'] = $post['date_cancellation'];
				$rf['status'] = 0;

				$this->M_refund_logs->insert($rf + $additional);

				$return['message'] = 'Transaction Successfully Canceled!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;

				$t = $this->M_transaction->get($transaction_id);

				$p_data['status'] = 1;

				$this->M_property->update($p_data, $t['property_id']);

				$this->transaction_library->update_transaction_status($this->user->id, '0', 4, "Soft deleted");

				$return['message'] = 'Transaction Successfully Canceled!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;

				echo json_encode($return);
			} else if ($post['type'] == "confirm") {

				$t = $this->M_transaction->get($transaction_id);

				$p_data['status'] = 1;

				$this->M_property->update($p_data, $t['property_id']);

				$this->transaction_library->update_transaction_status($this->user->id, '0', 4, "Soft deleted");

				$return['message'] = 'Transaction Successfully Canceled!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;

				echo json_encode($return);
			} else if ($post['type'] == "open") {
				$transaction_id = $post['transaction_id'];

				$t = $this->M_transaction->get($transaction_id);

				$refund_log = $this->M_refund_logs->get(['transaction_id' => $transaction_id]);

				$this->M_refund_logs->delete($refund_log['id']);

				$p_data['status'] = 2;

				$this->M_property->update($p_data, $t['property_id']);

				$this->transaction_library->update_transaction_status($this->user->id, '0', 2, $post['reason']);
				$return['message'] = 'Transaction Successfully Opened!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;

				echo json_encode($return);
			} else if ($post['type'] == "financing_scheme") {
				$fs['financing_scheme_id'] = $post['financing_scheme_id'];
				$transaction_id = $post['transaction_id'];
				$transaction = $this->M_transaction->get($transaction_id);

				if (isset($post['fs_values'])) {
					if (!empty('fs_discount_id')) {
						$discount_id = $post['fs_discount_id'];

						$discount = $this->M_promo->get($discount_id);
						$info['transaction_id'] = $transaction_id;
						$info['discount_id'] = $discount_id;
						if ($discount['value_type'] == 1) {
							$info['discount_percentage_rate'] = $discount['value'];
							$info['discount_amount_rate'] = 0;
						} else {
							$info['discount_percentage_rate'] = 0;
							$info['discount_amount_rate'] = $discount['value'];
						}

						$this->M_transaction_discount->insert($info + $additional);
					}
					$total_payment = $this->M_transaction_payment->total_payment($post['transaction_id']);
					$tb_data = json_decode(stripslashes($post['tb_values']), true);
					$fs_data = json_decode(stripslashes($post['fs_values']), true);
					// Update transaction billings per period *note(Equity not included)					
					$this->transaction_library->recalculate_mortgage($transaction_id, $tb_data, $fs_data, $this->user->id);

					$this->M_transaction->update($fs + $u_additional, $post['transaction_id']);
				}

				$return['message'] = 'Financing Scheme Successfully Changed!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;
				echo json_encode($return);
			} else if ($post['type'] == 'discount') {
				$return['status'] = 0;
				$return['message'] = 'Oops! Please refresh the page and try again.';

				$transaction_id = $post['transaction_id'];
				$transaction = $this->M_transaction->with_t_property()->get($transaction_id);
				$t_property_id = $transaction['t_property']['id'];

				// Previous discount using Promos table
				// if(isset($post['fs_values'])) {
				// 	if(!empty($post['fs_discount_id'])) {
				// 		$discount_id = $post['fs_discount_id'];

				// 		$discount = $this->M_promo->get($discount_id);
				// 		$info['transaction_id'] = $transaction_id;
				// 		$info['discount_id'] = $discount_id;
				// 		if($discount['value_type'] == 1) {
				// 			$info['discount_percentage_rate'] = $discount['value'];
				// 			$info['discount_amount_rate'] = 0;
				// 		} else {
				// 			$info['discount_percentage_rate'] = 0;
				// 			$info['discount_amount_rate'] = $discount['value'];
				// 		}

				// 		$this->M_transaction_discount->insert($info + $additional);

				// 		$total_payment = $this->M_transaction_payment->total_payment($post['transaction_id']);
				// 		$tb_data = json_decode(stripslashes($post['tb_values']), true);
				// 		$fs_data = json_decode(stripslashes($post['fs_values']), true);
				// 		// Update transaction billings per period *note(Equity not included)					
				// 		$this->transaction_library->recalculate_mortgage($transaction_id, $tb_data, $fs_data, $this->user->id);

				// 		$return['message'] = 'Discount sucessfully added!';
				// 		$return['status'] = 1;
				// 		echo json_encode($return);
				// 	} else {
				// 		$return['message'] = 'An error occurred! Please contact your administrator';
				// 		$return['status'] = 0;
				// 		echo json_encode($return);
				// 	}
				// } 
				$discounts = $post['promos'];
				$property = $post['property'];

				$tb_data = $this->M_transaction_billing->where('transaction_id', $transaction_id)->get_all();
				$financing_scheme = $this->M_scheme->with_values()->get($transaction['financing_scheme_id']);
				$financing_scheme_values = $financing_scheme['values'];
				$fs_data = array();

				foreach ($financing_scheme_values as $key => $value) {
					$fs_data[] = array(
						'period_id' => $value['period_id'],
						'period_term' => $value['period_term'],
						'period_amount' => $value['period_rate_amount'],
					);
				}

				if (is_object($property)) {
					$property = get_object_vars($property);
				}

				if (!empty($property['collectible_price'])) {
					$t_property['total_contract_price'] = $property['collectible_price'];
					// $t_property = $this->M_transaction_property->update($t_property + $u_additional, $t_property_id);
					$tcp = 0;

					if ($t_property) {
						// Check scheme type
						switch ($financing_scheme['scheme_type']) {
							case '1': // Standard
								$tcp = $property['collectible_price'];
								$dp_amount = floatval($tcp) - floatval($tb_data[0]['period_amount']);
								$tb_data[1]['period_amount'] = (floatval($dp_amount) * floatval($financing_scheme_values[1]['period_rate_percentage'])) / 100;
								$tb_data[2]['period_amount'] = (floatval($tcp) * floatval($financing_scheme_values[2]['period_rate_percentage'])) / 100;
								break;
							case '2': // Zero Downpayment
								$tcp = $property['collectible_price'];
								$tcp -= floatval($tb_data[0]['period_amount']);
								$tb_data[1]['period_amount'] = 0;
								$tb_data[2]['period_amount'] = (floatval($tcp) * floatval($financing_scheme_values[2]['period_rate_percentage'])) / 100;
								break;
							case '3': // Fixed Loan Amount
								$tcp = $property['collectible_price'];
								$dp_amount = floatval($tcp) - (floatval($tb_data[2]['period_amount']) + floatval($tb_data[0]['period_amount']));
								$tb_data[1]['period_amount'] = $dp_amount;
								break;
							case '4': // Fixed Downpayment
								$tcp = $property['collectible_price'];
								$loan_amount = floatval($tcp) - (floatval($tb_data[1]['period_amount']) + floatval($tb_data[0]['period_amount']));
								$tb_data[2]['period_amount'] = $loan_amount;
								break;
							case '5': // Cash Payment
								$tcp = $property['collectible_price'];
								$loan_amount = floatval($tcp) - floatval($tb_data[0]['period_amount']);
								break;
						}
					}
				}

				// echo "<pre>";
				// print_r($financing_scheme_values);
				// echo "<br>";
				// vdebug($tb_data);

				foreach ($discounts as $key => $value) {
					if (is_object($value)) {
						$value = get_object_vars($value);
						if (!empty($value['discount_amount_rate']) || $value['discount_amount_rate'] > 0) {
							$info['transaction_id'] = $transaction_id;
							$info['discount_id'] = $value['discount_id'];
							$info['discount_amount_rate'] = $value['discount_amount_rate'];
							$info['deduct_to'] = $value['discount_id'] == 1 ? 'Loan' : 'TCP';
							$info['remarks'] = $value['discount_id'] == 1 ? 'Deductable to Loan' : 'Deductable to TCP';

							$this->M_transaction_discount->insert($info + $additional);
						}
					} else {
						if (!empty($value['discount_amount_rate'])  || $value['discount_amount_rate'] > 0) {
							$info['transaction_id'] = $transaction_id;
							$info['discount_id'] = $value['discount_id'];
							$info['discount_amount_rate'] = $value['discount_amount_rate'];
							$info['deduct_to'] = $value['discount_id'] == 1 ? 'Loan' : 'TCP';
							$info['remarks'] = $value['discount_id'] == 1 ? 'Deductable to Loan' : 'Deductable to TCP';

							$this->M_transaction_discount->insert($info + $additional);
						}
					}

					// Deduct to loan
					if ($value['discount_id'] == 1) {
						$tb_data[2]['period_amount'] = floatval($tb_data[2]['period_amount']) - floatval($value['discount_amount_rate']);
					}

					$return['status'] = 1;
					$return['message'] = 'Discount sucessfully added!';
					$return['transaction_id'] = $transaction_id;
				}

				$this->transaction_library->recalculate_mortgage($transaction_id, $tb_data, $fs_data, $this->user->id);

				$return['status'] = 1;
				$return['message'] = 'Discount sucessfully added!';
				$return['transaction_id'] = $transaction_id;


				echo json_encode($return);
				exit();
			} else if ($post['type'] == "due_date") {


				$transaction_id = $post['transaction_id'];
				$periods = $post['period_id'];

				$effectivity_dates = $post['effectivity_date'];
				$original_effectivity_dates = $post['original_effectivity_date'];

				$period_terms = $post['period_term'];
				$original_period_terms = $post['original_period_term'];

				$interest_rates = $post['interest_rate'];
				$original_interest_rates = $post['original_interest_rate'];

				$period_amounts = $post['period_amount'];
				$original_period_amounts = $post['original_period_amount'];

				if (is_object($effectivity_dates)) {
					$effectivity_dates = get_object_vars($effectivity_dates);
				}

				if (is_object($original_effectivity_dates)) {
					$original_effectivity_dates = get_object_vars($original_effectivity_dates);
				}

				if (is_object($period_terms)) {
					$period_terms = get_object_vars($period_terms);
				}

				if (is_object($original_period_terms)) {
					$original_period_terms = get_object_vars($original_period_terms);
				}

				if (is_object($interest_rates)) {
					$interest_rates = get_object_vars($interest_rates);
				}

				if (is_object($original_interest_rates)) {
					$original_interest_rates = get_object_vars($original_interest_rates);
				}

				if (is_object($period_amounts)) {
					$period_amounts = get_object_vars($period_amounts);
				}

				if (is_object($original_period_amounts)) {
					$original_period_amounts = get_object_vars($original_period_amounts);
				}

				if ($periods) {
					foreach ($periods as $key => $period) {
						if (($effectivity_dates[$period] != $original_effectivity_dates[$period])) {
							# code...
							$params['transaction_id'] = $transaction_id;
							$params['period_id'] = $period;
							$params['is_paid'] = 0;
							$payments = $this->M_transaction_payment->order_by('id', 'ASC')->where($params)->get_all();

							if ($payments) {
								foreach ($payments as $key => $payment) {
									# code...
									$effectivity_date = $effectivity_dates[$period];

									$p['due_date'] = add_months_to_date($effectivity_date, $key);
									$p['next_payment_date'] = add_months_to_date($p['due_date'], 1);
									$a = $this->M_transaction_payment->update($p, $payment['id']);
									// 
								}
							}
						}
					}
				}

				$return['message'] = 'Billing Setting Successfully Changed!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;
				echo json_encode($return);
			} else if ($post['type'] == "interest_rates") {

				$transaction_id = $post['transaction_id'];
				$periods = $post['period_id'];

				$effectivity_dates = $post['effectivity_date'];
				$original_effectivity_dates = $post['original_effectivity_date'];

				$period_terms = $post['period_term'];
				$original_period_terms = $post['original_period_term'];

				$interest_rates = $post['interest_rate'];
				$original_interest_rates = $post['original_interest_rate'];

				$period_amounts = $post['period_amount'];
				$original_period_amounts = $post['original_period_amount'];

				if (is_object($effectivity_dates)) {
					$effectivity_dates = get_object_vars($effectivity_dates);
				}

				if (is_object($original_effectivity_dates)) {
					$original_effectivity_dates = get_object_vars($original_effectivity_dates);
				}

				if (is_object($period_terms)) {
					$period_terms = get_object_vars($period_terms);
				}

				if (is_object($original_period_terms)) {
					$original_period_terms = get_object_vars($original_period_terms);
				}

				if (is_object($interest_rates)) {
					$interest_rates = get_object_vars($interest_rates);
				}

				if (is_object($original_interest_rates)) {
					$original_interest_rates = get_object_vars($original_interest_rates);
				}

				if (is_object($period_amounts)) {
					$period_amounts = get_object_vars($period_amounts);
				}

				if (is_object($original_period_amounts)) {
					$original_period_amounts = get_object_vars($original_period_amounts);
				}

				$fs_data = false;

				if ($periods) {
					foreach ($periods as $key => $period) {
						# code...
						$tb_data[$key]['period_amount'] = $period_amounts[$period];
						$tb_data[$key]['interest_rate'] = $interest_rates[$period];
						$tb_data[$key]['period_term'] = $period_terms[$period];
						$tb_data[$key]['effectivity_date'] = $effectivity_dates[$period];
						$tb_data[$key]['period_id'] = $period;
					}
				}

				// vdebug($tb_data);
				$type = $post['type'];

				if ($tb_data) {
					# code...
					$this->transaction_library->recalculate_mortgage($transaction_id, $tb_data, $fs_data, $this->user->id, $type);
				}

				$return['message'] = 'Interest Rates Successfully Changed!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;
				echo json_encode($return);
			} else if ($post['type'] == "adhoc_fee") {
				$fees = $post['fees'];
				$tcp = $post['tcp'];
				$additional = array(
					'created_by' => $this->user->id,
					'created_at' => NOW
				);

				$this->M_Transaction_adhoc_fee->delete('transaction_id', $transaction_id);

				foreach ($fees as $key => $value) {
					if (is_object($value)) {
						$value = get_object_vars($value);
					}

					$percentage = ($tcp / 100) * $value['fees_amount_rate'];
					$info = array(
						'transaction_id' => $post['transaction_id'],
						'fee_id' => $value['fees_id'],
						'amount' => $value['fees_amount_rate'],
						'percentage'  => $percentage,
						'terms' => $post['terms'],
						'effectivity_date' => $post['effectivity_date']
					);

					$adhoc_fee_result = $this->M_Transaction_adhoc_fee->insert($info + $additional);
				}

				$data = array(
					'transaction_id' => $post['transaction_id'],
					'terms' => $post['terms'],
					'effectivity_date' => $post['effectivity_date'],
					'amount' => $post['total_fee'],
					'tcp' => $tcp
				);

				$steps = $this->transaction_library->process_adhoc_fee($data);

				if (isset($steps['monthly']) && ($steps['monthly'])) {
					foreach ($steps['monthly'] as $key => $step) {
						$countterm = $step['month'];

						$adhoc['transaction_id'] = $transaction_id;
						$adhoc['total_amount'] = $step['amount'];
						$adhoc['beginning_balance'] = $step['beginning_balance'];
						$adhoc['ending_balance'] = $step['ending_balance'];
						$adhoc['due_date'] = $step['due_date'];
						$adhoc['next_payment_date'] = $step['next_payment_date'];
						$adhoc['is_paid'] = 0;
						$adhoc['is_complete'] = 0;

						$status = $this->M_Transaction_adhoc_fee_schedule->insert($adhoc + $additional);
					}

					$return['message'] = 'Adhoc Fee successfully created!';
					$return['status'] = 1;
					$return['transaction_id'] = $transaction_id;
					echo json_encode($return);
				}
			} else if ($post['type'] == 'move_in') {
				$return['message'] = 'Oops! Something went wrong';
				$return['status'] = 0;
				// get transaction detail
				$t = $this->M_transaction->get($transaction_id);
				$period_id = $t['period_id'];

				if ($t) {
					if ($period_id === '3') {
						// transaction financing scheme
						$financing_scheme = $this->M_scheme->with_values()->get($t['financing_scheme_id']);

						// financing scheme values
						$fs_values = $financing_scheme['values'];

						$return = $this->transaction_library->movein_mortgage($transaction_id, $fs_values, $this->user->id);
					} else {
						$return['message'] = 'Reservation and downpayment period must be completed before moving in. ';
						$return['status'] = 0;
					}
				}

				echo json_encode($return);
			} else if ($post['type'] == "change_property" || $post['type'] == "update_transaction" || $post['type'] == "delete_transaction" || $post['type'] == "recompute_schedule") {
				$t = $this->M_transaction->get($transaction_id);
				$return['message'] = 'Confirmed! Please click ok to continue...';
				$return['type'] =  $post['type'];
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;

				echo json_encode($return);
			} else if ($post['type'] == "change_buyer") {

				$transaction = $this->M_transaction
					->with_buyer()
					->with_buyers()
					->get($post['transaction_id']);

				$new_buyers = $post['client'];

				$principal_buyer_array = array_filter($new_buyers, function ($buyer) {

					if ($buyer->client_type == 1) {

						return $buyer;
					}
				});

				$principal_buyer = array_values($principal_buyer_array)[0];

				if ($transaction['buyer_id'] != $principal_buyer->client_id) {
					$this->M_transaction->update(['buyer_id' => $principal_buyer->client_id], $post['transaction_id']);
				}

				$current_buyers = $transaction['buyers'];

				foreach ($current_buyers as $value) {

					$this->M_transaction_client->delete($value['id']);
				}

				foreach ($new_buyers as $value) {

					$this->M_transaction_client->insert([
						'transaction_id' => $transaction['id'],
						'client_id' => $value->client_id,
						'client_type' => $value->client_type,
					]);
				}

				$return['message'] = 'Buyers Successfully Changed!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;
				echo json_encode($return);
			} else if ($post['type'] == "change_seller") {

				$transaction_property = $this->M_transaction_property->where('transaction_id', $post['transaction_id'])->get();

				$this->M_transaction_property->update($post['property'], $transaction_property['id']);

				$transaction = $this->M_transaction
					->with_seller()
					->with_sellers()
					->get($post['transaction_id']);

				$new_sellers = $post['seller'];

				$main_seller = $new_sellers[0];

				if ($transaction['seller_id'] != $main_seller->seller_id) {
					$this->M_transaction->update(['seller_id' => $main_seller->seller_id], $post['transaction_id']);
				}

				$current_sellers = $transaction['sellers'];

				foreach ($current_sellers as $seller) {

					$this->M_transaction_seller->delete($seller['id']);
				}

				foreach ($new_sellers as $seller) {

					$seller->transaction_id = $post['transaction_id'];

					$this->M_transaction_seller->insert(json_decode(json_encode($seller), true) + $this->additional);
				}

				$this->process_commission($post['transaction_id'], 'update');

				$return['message'] = 'Sellers Successfully Changed!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;
				echo json_encode($return);
			} else if ($post['type'] == "change_ra_number") {

				$this->M_transaction->update(['ra_number' => $post['ra_number']], $post['transaction_id']);

				$return['message'] = 'RA Number Successfully Changed!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;
				echo json_encode($return);
			} else if ($post['type'] == "change_commission_setup") {

				$array_post = json_decode(json_encode($post['transaction_property']), true);

				$this->db->trans_start();

				$this->M_transaction->update(['commission_setup_id' => $post['commission_setup_id']], $post['transaction_id']);

				$this->M_transaction_property->where('transaction_id', $post['transaction_id'])->update($array_post);

				$this->process_commission($transaction_id, 'update');

				$this->db->trans_complete();

				$return['message'] = 'Commission Setup Successfully Changed!';
				$return['status'] = 1;
				$return['transaction_id'] = $transaction_id;
				echo json_encode($return);
			} else {
				$type = $post['type'];
				$property = $post['property'];
				$t_property_id = $property['transaction_property_id'];

				$this->process_property($property, $transaction_id, $t_property_id);

				$periods = $post['period_id'];

				$effectivity_dates = $post['effectivity_date'];
				$original_effectivity_dates = $post['original_effectivity_date'];

				$period_terms = $post['period_term'];
				$original_period_terms = $post['original_period_term'];

				$interest_rates = $post['interest_rate'];
				$original_interest_rates = $post['original_interest_rate'];

				$period_amounts = $post['period_amount'];
				$original_period_amounts = $post['original_period_amount'];

				$tb_data = [];
				if ($periods) {
					foreach ($periods as $key => $period) {
						if (($period_amounts[$period] != $original_period_amounts[$period])) {

							$beginning_balance = $period_amounts[$period];
							$terms = $period_terms[$period];
							$effectivity_date = $effectivity_dates[$period];
							$interest_rate = $interest_rates[$period];

							$tb_logs['transaction_id'] = $transaction_id;
							$tb_logs['transaction_payment_id'] = 0;
							$tb_logs['period_amount'] = $beginning_balance;
							$tb_logs['period_term'] = $terms;
							$tb_logs['effectivity_date'] = $effectivity_date;
							$tb_logs['starting_balance'] = $beginning_balance;
							$tb_logs['ending_balance'] = "";
							$tb_logs['interest_rate'] = $interest_rate;
							$tb_logs['period_id'] = $period;
							$tb_logs['slug'] = 'restructure';
							$tb_logs['remarks'] = "Change in Billing Setting";

							$additional = [
								'created_by' => $this->user->id,
								'created_at' => NOW
							];

							$tb_id = $this->M_Tbilling_logs->insert($tb_logs + $additional);

							$tb_data[$key]['period_term'] = $post['period_term'][$period];
							$tb_data[$key]['period_amount'] = $post['period_amount'][$period];
							$tb_data[$key]['interest_rate'] = $post['percentage_rate'][$period] ? $post['percentage_rate'][$period] : 0;
							$tb_data[$key]['period_id'] = $period;
							$tb_data[$key]['effectivity_date'] = $post['effectivity_date'][$period];
							$tb_data[$key]['monthly_payment'] = floatval($post['period_amount'][$period]) / intval($post['period_term'][$period]);
						}
					}

					$this->transaction_library->recalculate_mortgage($transaction_id, $tb_data, false, $this->user->id);
				}

				return true;
			}
		} else {
			// initializing modal
			$this->transaction_library->initiate($id);
			$return['modal'] = $type;
			$return['refund_amount'] = $this->transaction_library->get_refund_details() > 0 ? $this->transaction_library->get_refund_details() : 0;

			$return['info'] = $info = $this->M_transaction->with_buyer()->with_seller()
				->with_project()->with_property()->with_scheme()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_commission()
				->get($id);
			$return['tpm'] = $this->transaction_library->get_amount_paid(1, 0);
			// For transaction cancellation

			if ($info['project']) {
				$return['info']['house_model'] = $house_model = $this->M_House->where('project_id', $info['project']['id'])->get();
				$sub_type_id = $house_model['sub_type_id'];

				switch ($sub_type_id) {
					case "1":
						$return['processing_fee'] = 20000.00;
						break;
					case "2":
						$return['processing_fee'] = 10000.00;
						break;
					case "3":
						$return['processing_fee'] = 20000.00;
						break;
					case "4":
						$return['processing_fee'] = 30000.00;
						break;
					default:
						$return['processing_fee'] = 0;
						break;
				}
			}

			$return['schemes'] = $this->M_scheme->as_array()->get_all();
			$return['fees'] = $this->M_fees->as_array()->get_all();


			$return['info']['billings'] = $return['info']['billings'];
			$return['type'] = $type;

			if ($type == 'change_property' || $type == 'update_transaction' || $type == 'delete_transaction' || $type == 'recompute_schedule') {
				$return['modal'] = 'confirm';
			} elseif ($type == 'move_in') {
				$completed_terms = $this->M_transaction_payment->completed_terms_count($transaction_id, 3);
				$completed_terms = $completed_terms[0]['count'];
				$remaining_terms = intval($return['info']['billings'][2]['period_term']) - intval($completed_terms);

				$return['loan']['period_term'] = $return['info']['billings'][2]['period_term'];
				$return['loan']['completed_terms'] = $completed_terms;
				$return['loan']['remaining_terms'] = $remaining_terms;
			} else if ($type == 'change_buyer') {
				$return['all_buyers'] = $this->M_buyer->get_all();
			} elseif ($type == 'change_seller') {

				$return['all_sellers'] = $this->M_seller->order_by('first_name')->get_all();
			} elseif ($type == 'change_ra_number') {

				$return['ra_number'] = $this->M_transaction->get($transaction_id = $id)['ra_number'];
			} elseif ($type == 'change_commission_setup') {

				$return['commission_setup_id'] = $info['commission_setup_id'];
			}

			$return['html'] = $this->load->view('transaction/modals/' . $return['modal'], $return, true);
			echo json_encode($return);
		}
	}

	// For change financing scheme function
	// param:: $data: new transaction billing details
	public function create_financing_scheme_log($transaction_id, $financing_scheme_id, $data)
	{
		$transaction = $this->M_transaction->get($transaction_id);
		$prev_financing_scheme_id = $transaction['financing_scheme_id'];
		$new_financing_scheme_id = $financing_scheme_id;
		$remarks = array(
			'RE',
			'DP',
			'LOAN',
			'EQ',
		);

		$additional = [
			'created_by' => $this->user->id,
			'created_at' => NOW
		];

		foreach ($data as $key => $value) {
			$info = [
				'transaction_id' => $transaction_id,
				'prev_financing_scheme_id' => $prev_financing_scheme_id,
				'new_financing_scheme_id' => $new_financing_scheme_id,
				'period_amount' => $value['period_amount'],
				'interest_rate' => $value['interest_rate'],
				'period_term' => $value['period_term'],
				'effectivity_date' => $value['effectivity_date'],
				'period_id' => $value['period_id'],
				'remarks' => $remarks[$key],
			];

			$this->M_Transaction_financing_scheme_log->insert($info + $additional);
		}
	}

	public function get_transaction_due_date()
	{
		// Get all transactions
		$transactions = $this->M_transaction->get_all();


		if ($transactions) {
			// Filter transaction with no due date
			$filtered_transactions = array_filter($transactions, function ($t) {
				return $t['due_date'] == NULL;
			});

			// Loop through filtered array
			foreach ($filtered_transactions as $key => $transaction) {
				$this->transaction_library->initiate($transaction['id']);
				$due_date = $this->transaction_library->due_date();

				// Update transaction due date
				$this->M_transaction->update(array('due_date' => $due_date), $transaction['id']);
			}
		}
	}

	public function letter_request($transaction_id = false)
	{
		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		if ($this->input->post()) {

			// Image Upload
			$upload_path = './assets/uploads/letter_request/images';
			$config = array();
			$config['upload_path'] = $upload_path;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 5000;
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config, 'scanned_file');
			$this->scanned_file->initialize($config);

			$_input	=	$this->input->post();
			$_input['approve_date'] = 'NULL';

			if (!empty($_FILES['scanned_file']['name'])) {

				if (!$this->scanned_file->do_upload('scanned_file')) {
					$this->notify->error($this->scanned_file->display_errors(), 'transaction/letter_request/' . $transaction_id);
				} else {
					$img_data = $this->scanned_file->data();
				}
			}

			$additional_fields = array(
				'image' => $img_data['file_name'],
				'created_by' => $this->session->userdata('user_id'),
				'created_at' => date('Y-m-d'),
				'reference' => uniqidDLR(),
			);

			$_insert = $this->M_letter_request->insert($_input + $additional_fields);

			if ($_insert !== FALSE) {

				$this->notify->success('Letter Request successfully created.', 'transaction_letter_request');
			} else {

				$this->notify->error('Oh snap! Please refresh the page and try again.');
			}
		}

		$this->view_data['transaction_id']	=	$transaction_id;


		$this->template->build('letter_request', $this->view_data);
	}

	public function past_due_notice($transaction_id = false)
	{

		if ($this->input->post()) {

			if ($transaction_id) {

				$_input	=	$this->input->post();

				$additional_fields = array(
					'transaction_id' => $transaction_id,
					'created_by' => $this->session->userdata('user_id'),
					'created_at' => date('Y-m-d'),
				);

				$_insert = $this->M_Transaction_past_due_notice->insert($_input + $additional_fields);

				if ($_insert !== FALSE) {

					$this->notify->success('Letter Request successfully created.', 'transaction');
				} else {

					$this->notify->error('Oh snap! Please refresh the page and try again.');
				}
			}
		}

		// $this->view_data['transaction_id']	=	$transaction_id;


		$this->template->build('past_due_notice');
	}

	function process_checklist($transaction_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		if ($transaction_id) {

			$this->view_data['transaction_id']	=	$transaction_id;

			$this->template->build('process_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function process_loan_checklist($transaction_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		if ($transaction_id) {

			$this->view_data['transaction_id']	=	$transaction_id;

			$this->template->build('process_loan_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function process_checklist_upload($_li_id = FALSE, $_doc_id = FALSE)
	{

		if ($_li_id && $_doc_id && isset($_FILES['_file']['name']) && ($_FILES['_file']['name'] !== '')) {

			$this->load->model('transaction_document/Transaction_document_model', 'M_transaction_document');

			unset($_where);
			$_where['transaction_id']	=	$_li_id;
			$_where['document_id']				=	$_doc_id;

			$_li_document	=	$this->M_transaction_document->find_all($_where, ['id'], TRUE);
			if ($_li_document && isset($_li_document->id) && $_li_document->id) {

				$_ext	=	['.jpg', '.jpeg', '.png'];

				set_time_limit(0);

				unset($_config);
				$_config['upload_path']	=	$_location	=	'assets/img/_transaction/_documents/_process';
				$_config['allowed_types'] = 'jpeg|jpg|png';
				$_config['overwrite'] = TRUE;
				$_config['max_size'] = '1000000';
				$_config['file_name'] = $_filename = $_doc_id . '_thumb';

				if (isset($_ext) && $_ext) {

					$_image	=	$_location . '/' . $_filename;

					foreach ($_ext as $key => $x) {

						if (file_exists($_image . $x)) {

							unlink($_image . $x);
						}
					}
				}

				$this->load->library('upload', $_config);

				if (!$this->upload->do_upload('_file')) {

					$this->notify->error($this->upload->displaY_errors(), 'transaction/process_checklist/' . $_li_id);
				} else {

					$_uploaded_document_id	=	FALSE;

					$this->load->model('document/Transaction_document_upload_model', 'M_transaction_uploaded_document');

					$_udatas	=	$this->upload->data();
					if ($_udatas) {

						unset($_where);
						$_where['transaction_id']	=	$_li_id;
						$_where['document_id']				=	$_doc_id;

						$_uploaded_document	=	$this->M_transaction_uploaded_document->find_all($_where, ['id'], TRUE); #lqq(); ud($_uploaded_document);
						if ($_uploaded_document && isset($_uploaded_document->id) && $_uploaded_document->id) {

							$_uploaded_document_id	=	$_uploaded_document->id;

							unset($_update);
							$_update['transaction_id']	=	$_li_id;
							$_update['document_id']				=	$_doc_id;
							$_update['file_name']					=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_update['file_type']					=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_update['file_src']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_update['file_path']					=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_update['full_path']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_update['raw_name']					=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_update['orig_name']					=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_update['client_name']				=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_update['file_ext']					=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_update['file_size']					=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_update['updated_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']				=	NOW;

							$_updated	=	$this->M_transaction_uploaded_document->update($_update, $_uploaded_document_id);
							if (!$_updated) {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'transaction/process_checklist/' . $_li_id);
							}
						} else {

							unset($_insert);
							$_insert['transaction_id']	=	$_li_id;
							$_insert['document_id']				=	$_doc_id;
							$_insert['file_name']					=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_insert['file_type']					=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_insert['file_src']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_insert['file_path']					=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_insert['full_path']					=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_insert['raw_name']					=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_insert['orig_name']					=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_insert['client_name']				=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_insert['file_ext']					=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_insert['file_size']					=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_insert['created_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['created_at']				=	NOW;
							$_insert['updated_by']				=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['updated_at']				=	NOW;

							$_inserted	=	$this->M_transaction_uploaded_document->insert($_insert);
							if ($_inserted) {

								$_uploaded_document_id	=	$this->db->insert_id();
							} else {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'transaction/process_checklist/' . $_li_id);
							}
						}

						if ($_uploaded_document_id) {

							unset($_update);
							$_update['uploaded_document_id']	=	$_uploaded_document_id;
							$_update['has_file']	=	1;
							$_update['updated_by']						=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']						=	NOW;

							$_updated	=	$this->M_transaction_document->update($_update, $_li_document->id);
							if ($_updated) {
								$this->transaction_library->check_commission_status($_li_id, $this->user->id);
								$this->notify->success('Success! Image uploaded.', 'transaction/process_checklist/' . $_li_id);
							} else {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'transaction/process_checklist/' . $_li_id);
							}
						} else {

							$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction/process_checklist/' . $_li_id);
						}
					}
				}
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function documents($_li_id = FALSE)
	{

		$this->view_data['_largescreen']	=	TRUE;

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		// $this->load->model('land/Land_inventory_document_model', 'M_transaction_document');
		$this->load->model('checklist/Checklist_model', 'M_checklist');

		if ($_li_id) {

			$this->view_data['_li_id']	=	$_li_id;

			$_checklists	=	$this->M_checklist->find_all(FALSE, ['id', 'name']);
			if ($_checklists) {

				$this->view_data['_checklists']	=	$_checklists; #ud($_checklists);
			}

			$this->template->build('documents', $this->view_data);
		} else {

			show_404();
		}
	}

	function loan_documents($_li_id = FALSE)
	{

		$this->view_data['_largescreen']	=	TRUE;

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		// $this->load->model('land/Land_inventory_document_model', 'M_transaction_document');
		$this->load->model('checklist/Checklist_model', 'M_checklist');

		if ($_li_id) {

			$this->view_data['_li_id']	=	$_li_id;

			$_checklists = $this->M_checklist->find_all(FALSE, ['id', 'name']);
			if ($_checklists) {

				$this->view_data['_checklists']	=	$_checklists; #ud($_checklists);
			}

			$this->template->build('loan_documents', $this->view_data);
		} else {

			show_404();
		}
	}

	function document_checklist($_li_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('transaction_document/Transaction_document_model', 'M_transaction_document');

		if ($_li_id) {

			$this->view_data['_li_id']	=	$_li_id;

			$_documents	=	$this->M_transaction_document->_get_document_checklist($_li_id);
			if ($_documents) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('document_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function loan_document_checklist($_li_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);


		if ($_li_id) {

			$this->view_data['_li_id']	=	$_li_id;

			$_documents	=	$this->M_transaction_loan_document->_get_loan_document_checklist($_li_id);
			if ($_documents) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('loan_document_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function insert_document_checklist($_li_id = FALSE)
	{

		if ($_li_id) {

			$_dcs	=	[];

			$_lid_ids	=	[];

			$_inserted	=	0;
			$_updated		=	0;
			$_removed		=	0;
			$_failed		=	0;

			$_inputs	=	$this->input->post();

			// vdebug($_inputs);

			if ($_inputs) {

				foreach ($_inputs as $key => $_input) {

					if ($key !== '_document_checklist_length') {

						$_dcs[$key]['transaction_id']	=	$_li_id;
						$_dcs[$key]['document_id']	=	$key;

						// up($_input);

						foreach ($_input as $_ikey => $_int) {

							if ($_ikey === 'start_date') {

								$_dcs[$key][$_ikey]	=	date_format(date_create($_int), 'Y-m-d H:i:s');
							} else {

								$_dcs[$key][$_ikey]	=	$_int;
							}
						}
					} else {

						continue;
					}
				}

				$this->load->model('transaction_document/Transaction_document_model', 'M_transaction_document');

				if ($_dcs && !empty($_dcs)) {

					foreach ($_dcs as $_dkey => $_dc) {

						unset($_where);
						$_where['transaction_id']	=	$_dc['transaction_id'];
						$_where['document_id']				=	$_dc['document_id'];

						$_lid	=	$this->M_transaction_document->find_all($_where, FALSE, TRUE); #lqq(); up($_lid);
						if ($_lid) {

							unset($_datas);
							foreach ($_dc as $_dkey => $_d) {

								if (($_dkey !== 'transaction_id') && ($_dkey !== 'document_id')) {

									$_datas[$_dkey]	=	$_d;

									$_datas[$_dkey]['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_datas[$_dkey]['updated_at']	=	NOW;
								}
							}

							$_update	=	$this->M_transaction_document->update($_datas, $_lid->id);
							if ($_update) {

								$_updated++;
							} else {

								$_failed++;

								break;
							}
						} else {

							unset($_datas);
							$_datas	=	$_dc;

							$_datas['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['created_at']	=	NOW;
							$_datas['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['updated_at']	=	NOW;

							$_insert	=	$this->M_transaction_document->insert($_datas);
							if ($_insert) {

								$_inserted++;
							} else {

								$_failed++;

								break;
							}
						}
					}

					$_msg	=	'';
					if ($_inserted > 0) {

						$_msg	=	$_inserted . ' document/s was successfuly inserted';
					}

					if ($_updated > 0) {

						$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' document/s was successfuly updated';
					}

					if ($_failed > 0) {

						$this->notify->error('Something went wrong! Please refresh the page and try again.', 'transaction/document_checklist/' . $_li_id);
					} else {

						$this->notify->success($_msg . '.', 'transaction/document_checklist/' . $_li_id);
					}
				}
			}
		} else {

			show_404();
		}
	}

	function insert_loan_document_checklist($_li_id = FALSE)
	{

		if ($_li_id) {

			$_dcs	=	[];

			$_lid_ids	=	[];

			$_inserted	=	0;
			$_updated		=	0;
			$_removed		=	0;
			$_failed		=	0;

			$_inputs	=	$this->input->post();

			// vdebug($_inputs);

			if ($_inputs) {

				foreach ($_inputs as $key => $_input) {

					if ($key !== '_document_checklist_length') {

						$_dcs[$key]['transaction_id']	=	$_li_id;
						$_dcs[$key]['document_id']	=	$key;

						// up($_input);

						foreach ($_input as $_ikey => $_int) {

							if ($_ikey === 'start_date') {

								$_dcs[$key][$_ikey]	=	date_format(date_create($_int), 'Y-m-d H:i:s');
							} else {

								$_dcs[$key][$_ikey]	=	$_int;
							}
						}
					} else {

						continue;
					}
				}


				if ($_dcs && !empty($_dcs)) {

					foreach ($_dcs as $_dkey => $_dc) {

						unset($_where);
						$_where['transaction_id'] = $_dc['transaction_id'];
						$_where['document_id'] = $_dc['document_id'];

						$_lid = $this->M_transaction_loan_document->find_all($_where, FALSE, TRUE); #lqq(); up($_lid);
						if ($_lid) {

							unset($_datas);
							foreach ($_dc as $_dkey => $_d) {

								if (($_dkey !== 'transaction_id') && ($_dkey !== 'document_id')) {

									$_datas[$_dkey]	=	$_d;

									$_datas[$_dkey]['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_datas[$_dkey]['updated_at'] = NOW;
								}
							}

							$_update = $this->M_transaction_loan_document->update($_datas, $_lid->id);
							if ($_update) {

								$_updated++;
							} else {

								$_failed++;

								break;
							}
						} else {

							unset($_datas);
							$_datas	=	$_dc;

							$_datas['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['created_at'] = NOW;
							$_datas['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['updated_at'] = NOW;

							$_insert = $this->M_transaction_loan_document->insert($_datas);
							if ($_insert) {

								$_inserted++;
							} else {

								$_failed++;

								break;
							}
						}
					}

					$_msg = '';
					if ($_inserted > 0) {

						$_msg = $_inserted . ' document/s was successfuly inserted';
					}

					if ($_updated > 0) {

						$_msg .= ($_inserted ? ' and ' : '') . $_updated . ' document/s was successfuly updated';
					}

					if ($_failed > 0) {

						$this->notify->error('Something went wrong! Please refresh the page and try again.', 'transaction/loan_document_checklist/' . $_li_id);
					} else {

						$this->notify->success($_msg . '.', 'transaction/loan_document_checklist/' . $_li_id);
					}
				}
			}
		} else {

			show_404();
		}
	}

	function get_filtered_document_checklist()
	{

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'';
		$_view_data	=	[];

		$_checklist_id	=	$this->input->post('value');
		$_li_id					=	$this->input->post('li_id');
		if ($_checklist_id && $_li_id) {

			$_view_data['_li_id']	=	$_li_id;

			$this->load->model('checklist/Document_checklist_model', 'M_document_checklist');

			$_documents	=	$this->M_document_checklist->_get_documents($_checklist_id);
			if ($_documents) {

				$_view_data['_documents']	=	$_documents;
			}

			$_respo['_status']	=	1;

			$_respo['_html']	=	$this->load->view('_checklist/documents', $_view_data, TRUE);
		} else {

			$_respo['_msg']	=	'Oops! Please refresh the page and try again.';
		}

		echo json_encode($_respo);

		exit();
	}

	function get_filtered_loan_document_checklist()
	{

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'';
		$_view_data	=	[];

		$_checklist_id	=	$this->input->post('value');
		$_li_id					=	$this->input->post('li_id');
		if ($_checklist_id && $_li_id) {

			$_view_data['_li_id']	=	$_li_id;

			$this->load->model('checklist/Document_checklist_model', 'M_document_checklist');

			$_documents	=	$this->M_document_checklist->_get_documents($_checklist_id);
			if ($_documents) {

				$_view_data['_documents']	=	$_documents;
			}

			$_respo['_status']	=	1;

			$_respo['_html']	=	$this->load->view('_checklist/loan_documents', $_view_data, TRUE);
		} else {

			$_respo['_msg']	=	'Oops! Please refresh the page and try again.';
		}

		echo json_encode($_respo);

		exit();
	}

	function insert_transaction_documents($transaction_id = 0)
	{
		$additional = [
			'created_by' => $this->user->id,
			'created_at' => NOW
		];

		if ($transaction_id) {
			// Get checklist id
			$checklist = $this->M_checklist->get(["is_reservation_document" => 1]);

			// Check if it exist
			if ($checklist) {
				$checklist_id = $checklist['id'];

				// Get documents using checklist id
				$_document_checklists	=	$this->M_document_checklist->where('checklist_id', $checklist_id)->get_all();

				// Loop through document checklist
				foreach ($_document_checklists as $key => $doc) {
					$data['transaction_id'] = $transaction_id;
					$data['document_id'] = $doc['document_id'];
					$data['checklist'] = $doc['checklist_id'];
					$data['category_id'] = 4; //sales
					$data['owner'] = $this->get_document_owner($doc['document_id']);
					$data['checklist'] = $doc['checklist_id'];
					$data['start_date'] = NOW;

					// Insert documents
					$this->M_transaction_document->insert($data + $additional);
				}
			}
		}
	}

	function get_document_owner($document_id = 0)
	{

		if ($document_id) {

			$document = $this->M_documents->get($document_id);

			$owner_id = $document['owner_id'];

			return $owner_id;
		}
	}

	function update_document_stage($id = false)
	{

		$additional = [
			'updated_by' => $this->user->id,
			'updated_at' => NOW,
		];

		if ($id) {
			$info = $this->input->post();
			// New Status/Stage
			$documentation_status = $info['documentation_status'];
			// Transaction Documents table

			// Get all documents in this transaction
			$transaction_documents = $this->M_transaction_document->get_all(['transaction_id' => $id]);

			$td_status = 1; // 1 = Pending 2 = Completed
			$filtered_docs = [];

			if ($transaction_documents) {
				// Filter documents that has files
				$filtered_docs = array_filter($transaction_documents, function ($var) {
					return $var['has_file'] == 1;
				});
			} else {
				$transaction_documents = [];
			}

			$total_docs = count($transaction_documents);
			$total_docs_with_files = count($filtered_docs);

			// Check if total documents in this transaction is equal to the number of documents that has file
			if ($total_docs == $total_docs_with_files) {

				// Insert to Transaction Document Stage Logs
				foreach ($transaction_documents as $key => $t_document) {
					$td_stage_log['transaction_id'] = $id;
					$td_stage_log['previous_id'] = $t_document['transaction_document_stage_id'];
					$td_stage_log['new_id'] = $documentation_status;

					$this->M_transaction_document_stage_logs->insert($td_stage_log + $additional);
				}

				$data = array(
					array(
						'transaction_id' => $id,
						'transaction_status' => $td_status,
						'transaction_document_stage_id' => $documentation_status,
					),
				);

				// Update document status
				$result = $this->db->update_batch('transaction_documents', $data, 'transaction_id');

				$transaction = $this->M_transaction->get($id);

				$res = $this->M_transaction->update(['documentation_status' => $documentation_status] + $additional, $id);

				if ($res) {
					$response['status'] = 1;
					$response['message'] = 'Updated Transaction Stage';
				} else {
					$response['status'] = 0;
					$response['message'] = 'Error!';
				}
			} else {

				$response['status'] = 0;
				$response['message'] = 'Cannot complete this stage. Please upload all required files first. ' . $total_docs . " " . $total_docs_with_files;
			}


			echo json_encode($response);
			exit();
		}
	}

	function update_loan_document_stage($id = false)
	{

		$additional = [
			'updated_by' => $this->user->id,
			'updated_at' => NOW,
		];

		if ($id) {
			$info = $this->input->post();
			// New Status/Stage
			$transaction_loan_document_stage_id = $info['transaction_loan_document_stage_id'];
			// Transaction Documents table

			// Get all documents in this transaction
			$transaction_loan_documents = $this->M_transaction_loan_document->get_all(['transaction_id' => $id]);

			$td_status = 1; // 1 = Pending 2 = Completed
			$filtered_docs = [];

			if ($transaction_loan_documents) {
				// Filter documents that has files
				$filtered_docs = array_filter($transaction_loan_documents, function ($var) {
					return $var['has_file'] == 1;
				});
			} else {
				$transaction_loan_documents = [];
			}

			$total_docs = count($transaction_loan_documents);
			$total_docs_with_files = count($filtered_docs);

			// Check if total documents in this transaction is equal to the number of documents that has file
			if ($total_docs == $total_docs_with_files) {

				// Insert to Transaction Document Stage Logs
				foreach ($transaction_loan_documents as $key => $t_document) {
					$td_stage_log['transaction_id'] = $id;
					$td_stage_log['previous_id'] = $t_document['transaction_loan_document_stage_id'];
					$td_stage_log['new_id'] = $transaction_loan_document_stage_id;

					$this->M_transaction_loan_document_stage_logs->insert($td_stage_log + $additional);
				}

				$data = array(
					array(
						'transaction_id' => $id,
						'transaction_status' => $td_status,
						'transaction_loan_document_stage_id' => $transaction_loan_document_stage_id,
					),
				);

				// Update document status
				$result = $this->db->update_batch('transaction_loan_documents', $data, 'transaction_id');

				$transaction = $this->M_transaction->get($id);

				$res = $this->M_transaction->update(['transaction_loan_document_stage_id' => $transaction_loan_document_stage_id] + $additional, $id);

				if ($res) {
					$response['status'] = 1;
					$response['message'] = 'Updated Transaction Stage';
				} else {
					$response['status'] = 0;
					$response['message'] = 'Error!';
				}
			} else {

				$response['status'] = 0;
				$response['message'] = 'Cannot complete this stage. Please upload all required files first. ' . $total_docs . " " . $total_docs_with_files;
			}


			echo json_encode($response);
			exit();
		}
	}

	function update_loan_documents($id = false)
	{

		$additional = [
			'updated_by' => $this->user->id,
			'updated_at' => NOW,
		];

		if ($id) {

			$info = $this->input->post();
			// New Status/Stage
			$loan_documentation_category = $info['transaction_loan_document_category_id'];
			// Transaction Documents table

			$transaction = $this->M_transaction->get($id);
			$res = $this->M_transaction->update(['loan_documentation_category' => $loan_documentation_category] + $additional, $id);

			$this->process_documents($id, 0, $loan_documentation_category);

			// loan_documentation_category

			if ($res) {
				$response['status'] = 1;
				$response['message'] = 'Updated Loan Document Category';
			} else {
				$response['status'] = 0;
				$response['message'] = 'Error!';
			}
		}


		echo json_encode($response);
		exit();
	}

	function add_personnel($transaction_id = false)
	{
		if ($transaction_id) {

			$post = $this->input->post();

			$personnel_id = $post['personnel_id'];
			$post['is_active'] = 1;


			$all_personnels = $this->M_transaction_personnel_logs->get_all(["transaction_id" => $transaction_id]);

			if (!empty($all_personnels)) {

				foreach ($all_personnels as $key => $personnel) {
					$data['is_active'] = 0;

					$this->M_transaction_personnel_logs->update($data, $personnel['id']);
				}
			}


			$res = $this->M_transaction_personnel_logs->insert($post);

			$t_personnel_log = $this->M_transaction_personnel_logs->get($res);

			if ($res) {
				$response['status'] = 1;
				$response['message'] = 'Added Personnel successfully';

				$t_data['personnel_id'] = $t_personnel_log['personnel_id'];
				$this->M_transaction->update($t_data, $transaction_id);
			} else {
				$response['status'] = 0;
				$response['message'] = 'Error!';
			}

			echo json_encode($response);
			exit();
		}
	}

	function view_transaction_document_stage_log($td_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('transaction_document_stage_logs/Transaction_document_stage_logs_model', 'M_transaction_document_stage_logs');

		if ($td_id) {

			$this->view_data['td_id']	=	$td_id;

			$_document_stage_log	=	$this->M_transaction_document_stage_logs->get_all(["transaction_id" => $td_id]);

			$transaction = $this->M_transaction->get($td_id);

			$this->view_data['transaction_reference']	=	$transaction['reference'];

			if ($_document_stage_log) {
				$this->view_data['_document_stage_log']	=	$_document_stage_log;
				$this->view_data['_total']			=	count($_document_stage_log);
			}

			$this->template->build('transaction_document_stage_log', $this->view_data);
		} else {

			show_404();
		}
	}

	function view_transaction_document_loan_stage_log($td_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('transaction_document_stage_logs/Transaction_document_stage_logs_model', 'M_transaction_document_stage_logs');

		if ($td_id) {

			$this->view_data['td_id']	=	$td_id;

			$_document_stage_log	=	$this->M_transaction_document_stage_logs->get_all(["transaction_id" => $td_id]);

			$transaction = $this->M_transaction->get($td_id);

			$this->view_data['transaction_reference']	=	$transaction['reference'];

			if ($_document_stage_log) {
				$this->view_data['_document_stage_log']	=	$_document_stage_log;
				$this->view_data['_total']			=	count($_document_stage_log);
			}

			$this->template->build('transaction_document_stage_log', $this->view_data);
		} else {

			show_404();
		}
	}

	function process_adhoc_schedule($terms = 0, $effectivity_date, $amount = 0, $tcp = 0)
	{
		$fees = array(
			'terms' => $terms,
			'effectivity_date' => $effectivity_date,
			'amount' => $amount,
			'tcp' => $tcp,
		);

		$result = $this->transaction_library->process_adhoc_fee($fees);
		echo json_encode($result['monthly']);
	}

	function get_refund_amount($id = 0, $percentage = 0)
	{
		$this->transaction_library->initiate($id);
		$this->transaction_library->set_refund_percentage($percentage);
		$return['refund_amount'] = $this->transaction_library->get_refund_details() > 0 ? $this->transaction_library->get_refund_details() : 0;

		echo json_encode($return['refund_amount']);
	}

	function update_table_fillables($type_id = "")
	{

		// $post = $this->input->post();

		// $type_id = $post['type_id'];

		if ($type_id) {
			switch ($type_id) {
				case 'transactions':
					$_fills	= $this->M_transaction->fillable;
					$_colms	= $this->_table_columns = $this->M_transaction->__get_columns();;
					break;
				case 'transaction_properties':
					$_fills = $this->M_transaction_property->fillable;
					$_colms = $this->M_transaction_property->__get_columns();
					break;
				case 'transaction_billings':
					$_fills = $this->M_transaction_billing->fillable;
					$_colms = $this->M_transaction_billing->__get_columns();
					break;
				case 'transaction_clients':
					$_fills = $this->M_transaction_client->fillable;
					$_colms = $this->M_transaction_client->__get_columns();
					break;
				case 'transaction_sellers':
					$_fills = $this->M_transaction_seller->fillable;
					$_colms = $this->M_transaction_seller->__get_columns();
					break;
				default:
					return;
					break;
			}

			$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
			$this->view_data['_columns']	=	$this->__get_columns($_fills);

			$return["html"] = $this->load->view('transaction/_batch_upload', $this->view_data, true);

			echo json_encode($return);
		}
	}

public function get_properties()
	{

		$post = $this->input->post();


		if ($post) {
			$params["status"] = 1;
			$params["project_id"] = $post['project_id'];
			$output['data'] = [];

			$properties = $this->M_property->where($params)->get_all();

			if ($properties) {
				$output = array(
					'data' => $properties,
				);
			}


			echo json_encode($output);
		}
	}

	public function print_refund($id = 0, $refund_percentage = 0, $processing_fee = 0, $miscellaneous_fee = 0, $penaly = 0)
	{
		if ($id) {
			$this->transaction_library->initiate($id);

			$this->view_data['info'] = $info = $this->M_transaction->with_buyer()->with_seller()
				->with_project()->with_property()->with_scheme()
				->with_t_property()->with_billings()->with_buyers()
				->with_sellers()->with_fees()->with_promos()->with_commission()
				->get($id);

			$this->view_data['tpm'] = $this->transaction_library->get_amount_paid(1, 0);
			$this->view_data['refund_percentage'] = $refund_percentage;
			$this->view_data['processing_fee'] = $processing_fee;
			$this->view_data['miscellaneous_fee'] = $miscellaneous_fee;
			$this->view_data['penaly'] = $penaly;

			$this->load->view('transaction/view/_refund_computation', $this->view_data);
		}
	}

	public function get_house_model()
	{
		$post = $this->input->post();

		$project_id = $post['project_id'];

		$where["project_id"] = $project_id;

		$data = $this->M_house->where($where)->as_array()->get();

		if ($data) {
			$output = array(
				'data' => $data,
			);
			echo json_encode($output);
		} else {
			echo json_encode(0);
		}
	}
	public function total_day_sales()
	{

		$today = date("Y-m-d");

		$sales_today = $this->M_transaction->where('reservation_date', $today)->with_t_property()->get_all();
		$total_sales = 0;
		$sales_count = 0;

		if ($this->input->post()) {

			if ($sales_today) {

				foreach ($sales_today as $sales) {

					$total_sales += $sales['t_property']['collectible_price'];
					$sales_count += 1;
				}
			}
		}
		$result['amount'] = money_php($total_sales);
		$result['count'] = $sales_count;
		echo json_encode($result);
	}

	public function total_week_sales()
	{
		$total_sales = 0;
		$sales_count = 0;
		$filter = 22;

		if ($this->input->post()) {
			$filter = $this->input->post('filter');
			$weekly_sales = $this->db->query('SELECT * FROM  transactions INNER JOIN transaction_properties ON transaction_properties.transaction_id=transactions.id WHERE  YEARWEEK(reservation_date, 0) = YEARWEEK(CURDATE() - INTERVAL ' . $filter . ' WEEK, 0)');
			$result['week'] = get_date_range("week", $filter);
			if ($weekly_sales) {

				foreach ($weekly_sales->result_array() as $row) {
					$total_sales += $row['collectible_price'];
					$sales_count += 1;
				}
			}
		}
		$result['amount'] = money_php($total_sales);
		$result['count'] = $sales_count;
		$result['raw_amount'] = $total_sales;

		echo json_encode($result);
		exit();
	}

	public function total_month_sales()
	{
		$total_sales = 0;
		$sales_count = 0;

		if ($this->input->post()) {
			$filter = $this->input->post('filter');
			$monthly_sales = $this->db->query('SELECT * FROM  transactions INNER JOIN transaction_properties ON transaction_properties.transaction_id=transactions.id WHERE YEAR(reservation_date) = YEAR(CURDATE() - INTERVAL ' . $filter . ' MONTH) AND MONTH(reservation_date) = MONTH(CURDATE() - INTERVAL ' . $filter . ' MONTH) ORDER BY reservation_date');
			$result['month'] = get_date_range("month", $filter);
			$result['dates'] = [];
			$result['indiv_amounts'] = [];

			if ($monthly_sales) {

				foreach ($monthly_sales->result_array() as $row) {
					$total_sales += $row['collectible_price'];
					$sales_count += 1;
					array_push($result['dates'], date('F-d', strtotime($row['reservation_date'])));
					array_push($result['indiv_amounts'], $row['collectible_price']);
				}
			}
		}
		$result['amount'] = money_php($total_sales);
		$result['count'] = $sales_count;
		echo json_encode($result);
		exit();
	}

	public function total_year_sales()
	{
		$total_sales = 0;
		$sales_count = 0;

		if ($this->input->post()) {

			$yearly_sales = $this->db->query(
				"SELECT y, m, coalesce(sum(transaction_properties.collectible_price),0) as sum, count(transaction_properties.collectible_price) as count
				FROM (
				  SELECT y, m
				  FROM
					(SELECT YEAR(CURDATE()) y UNION ALL SELECT YEAR(CURDATE())-1) years,
					(SELECT 1 m UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4
					  UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8
					  UNION ALL SELECT 9 UNION ALL SELECT 10 UNION ALL SELECT 11 UNION ALL SELECT 12) months) ym
				  LEFT JOIN transactions
				  ON ym.y = YEAR(transactions.reservation_date)
					 AND ym.m = MONTH(transactions.reservation_date)
					 AND (transactions.deleted_at IS NULL and transactions.deleted_by IS NULL)
				 left join transaction_properties on
				 transaction_properties.transaction_id=transactions.id
				WHERE
				  ((y=YEAR(CURDATE()) AND m<=MONTH(CURDATE()))
				  OR
				  (y<YEAR(CURDATE()) AND m>=MONTH(CURDATE())))
				 AND
				(transaction_properties.deleted_at IS NULL and transaction_properties.deleted_by IS NULL)
				group by 
				y,m
				"
			)->result_array();

			$date_from = date("F Y", strtotime(NOW . " -1 year"));
			$date_to  = date("F Y", strtotime(NOW));
			$result['range'] = "$date_from - $date_to";
			$result['dates'] = [];
			$result['indiv_amounts'] = [];
			if ($yearly_sales) {

				foreach ($yearly_sales as $row) {
					$total_sales += $row['sum'];
					$sales_count += $row['count'];
					array_push($result['dates'], date("F", mktime(0, 0, 0, $row['m'], 10)) . ' ' . $row['y']);
					array_push($result['indiv_amounts'], $row['sum']);
				}
			}
		}
		$result['amount'] = money_php($total_sales);
		$result['count'] = $sales_count;
		echo json_encode($result);
		exit();
	}

	public function update_commission_target_date($force = 0, $transaction_id = 0)
	{
		if ($force) {
			if ($transaction_id) {
				$this->db->where('transaction_id', $transaction_id);
			}
			$commissions = $this->M_Transaction_commission->with_transaction()->get_all();
			foreach ($commissions as $commission => $attribs) {
				if (array_key_exists('transaction', $attribs)) {
					$data = [];
					$id = $attribs['id'];
					$period_id = $attribs['period_type_id'];
					$transaction_id = $attribs['transaction_id'];
					$amount_rate_to_collect = $attribs['amount_rate_to_collect'];
					$data['target_date'] = $this->M_Transaction_commission->get_target_date($period_id, $transaction_id, $amount_rate_to_collect);
					$data['updated_by'] = $this->user->id;
					$data['updated_at'] = NOW;
					$this->M_Transaction_commission->update($data, $id);
				}
			}
		} else {
			echo 'NOT ALLOWED!';
		}
	}

	public function get_tax_amount($amount = 0, $is_lot = 0)
	{

		$threshold = 0;

		if ($is_lot == 1) {
			$threshold = 1500000;
		}
		if ($is_lot == 2) {
			$threshold = 0;
		} else {
			$threshold = 2500000;
		}

		if ($threshold < $amount) {
			$amount = $amount * .12;
		}

		$result['amount'] = $amount;

		echo json_encode($result);
	}


	public function breakdown($period_term = 0, $interest_rate = 0, $period_amount = 0)
	{
		$effectivity_date = date('Y-m-d');

		$value['period_term'] = $period_term;
		$value['interest_rate'] = $interest_rate;
		$value['period_amount'] = $period_amount;
		$value['effectivity_date'] = $effectivity_date;

		$this->mortgage_computation->reconstruct($value, $period_amount);

		$steps = $this->mortgage_computation->breakdown();

		$a = "<table border='1'><tr><td>Month</td><td>Principal</td><td>Interest</td><td>Total</td><td>Due Date</td></tr>";

		foreach ($steps['monthly'] as $key => $step) {
			$x = $step['principal'] + $step['interest'];
			$a .= "<tr><td>" . $step['month'] . "</td><td>" . $step['principal'] . "</td><td>" . $step['interest'] . "</td><td>" . $x . "</td><td>" . $step['due_date'] . "</td></tr>";
		}

		$a .= "</table>";

		echo $a;
	}

	public function dashboard($debug = "")
	{


		$date = NOW;
		$date_from = date("Y/m/d", strtotime($date . " -1 year"));
		$date_to  = date("Y/m/d", strtotime($date));

		$date_option_range = "Y-m";

		$period = $this->M_report->get_periods($date_from, $date_to);

		$months = array();
		$month_opt = "year_month";

		foreach ($period as $key => $dt) {
			$months[] = $dt->format("Y-m");
		}

		$daterange = view_date($date_from) . " - " . view_date($date_to);

		$projects = $this->M_project->order_by('name', 'ASC')->as_array()->get_all();

        $aris_projects = $this->M_aris->order_by('ProjectName', 'ASC')->as_array()->get_all();

         if ($aris_projects) {

			$rows = [];

            foreach ($aris_projects as $key => $project) {
                # code...
                $p['project_id'] =  $project['id'];
              
                foreach ($months as $key2 => $month) {

					$params[$month_opt] = $month;
					$params['project_id'] = $project['id'];

					$data['rows'][$project['id']][$month] = $this->M_aris_document->sales_report($params);

					$this->view_data['aris_projects'][$project['ProjectName']][$month]['count'] = ($data['rows'][$project['id']][$month]['0']['t_count'] ? (float) $data['rows'][$project['id']][$month]['0']['t_count'] : (float) 0);
					
					$this->view_data['aris_projects'][$project['ProjectName']][$month]['total_selling_price'] = ($data['rows'][$project['id']][$month]['0']['total_selling_price'] ? (float) $data['rows'][$project['id']][$month]['0']['total_selling_price'] : (float) 0);
				}

				$j[$key]['name'] = $project['ProjectName'];
				$j[$key]['data'] = $rows;

            }
        }


		//per project
		foreach ($projects as $key => $project) {

			$rows = [];

			foreach ($months as $key2 => $month) {

				$params[$month_opt] = $month;
				$params['project_id'] = $project['id'];

				$data['rows'][$project['id']][$month] = $this->M_report->sales_report($params);

				// $rows[] =  ($data['rows'][$project['id']][$month]['0']['total_selling_price'] ? (float) $data['rows'][$project['id']][$month]['0']['total_selling_price'] : (float) 0);

				$this->view_data['projects'][$project['name']][$month]['total_selling_price'] = ($data['rows'][$project['id']][$month]['0']['total_selling_price'] ? (float) $data['rows'][$project['id']][$month]['0']['total_selling_price'] : (float) 0);

				$this->view_data['projects'][$project['name']][$month]['count'] = ($data['rows'][$project['id']][$month]['0']['t_count'] ? (float) $data['rows'][$project['id']][$month]['0']['t_count'] : (float) 0);
			}

			$j[$key]['name'] = $project['name'];
			$j[$key]['data'] = $rows;
		}

		$this->view_data['months'] = $months;

		// $this->view_data = $j;

		// if ($Projects) {
		//     # code...
		//     foreach ($Projects as $key => $project) {
		//         # code...
		//         $params['project_id'] =  $project['id'];
		//         // $params['date'] = date('Y-m-d');
		//         $r = $this->M_Transaction_official_payment->collections_dashboard($params);

		//         $this->view_data['projects'][$project['name']]['total'] = $r['amount_paid'] ? $r['amount_paid'] : 0;

		//         if ($status) {
		//             # code...
		//             foreach ($status as $key => $stat) {

		//                 if ($key) {
		//                     # code...
		//                     $w['project_id'] = $project['id'];
		//                     // $w['date'] = date('Y-m-d');
		//                     $w['payment_type_id'] = $key;

		//                     $rr = $this->M_Transaction_official_payment->collections_dashboard($w);
		//                     $this->view_data['projects'][$project['name']][$stat] =  $rr['amount_paid'] ? $rr['amount_paid'] : 0;
		//                 }
		//             }
		//         }
		//     }
		// }


		if ($debug == 1) {
			# code...
			vdebug($this->view_data);
		}

		$this->template->build('dashboard', $this->view_data);
	}

	public function update_existing_sales_group($transaction_id)
	{
		$transaction = $this->M_transaction->get($transaction_id);

		$transaction_seller_id = $transaction['seller_id'];

		$property_id = $transaction['property_id'];

		$seller_id = $this->M_transaction_seller->get($transaction_seller_id)['seller_id'];

		$sales_group_id = $this->M_seller->get($seller_id)['sales_group_id'];

		$update = ['sales_arm_id' => $sales_group_id];

		$this->M_property->update($update, $property_id);
	}

	public function get_properties_dashboard() {

		$month = $this->input->post('month');
		$project_name = $this->input->post('project_name');
		$mod = $this->input->post('mod');
		
		if ($mod == "ARIS") {
			$project_id = get_value_field($project_name,'aris_project_information','id','ProjectName');

			$params["year_month"] = $month;
			$params['project_id'] = $project_id;
			$params['mod'] = "get_properties";

			$res = $this->M_aris_document->sales_report($params);
			
			$result['lots'] = $res;

			$a = "";
			if ($res) {
				// code...
				foreach ($res as $key => $re) {
					// code...
					$a .= $re['Lot_no']."<br>";
				}
			}
			
			$result['view'] = $a;
		} else if ($mod == "REMS") {
			$project_id = get_value_field($project_name,'projects','id','name');

			$params["year_month"] = $month;
			$params['project_id'] = $project_id;
			$params['select'] = "property_name";

			$res = $this->M_report->sales_report($params);

			$a = "";
			if ($res) {
				// code...
				foreach ($res as $key => $re) {
					// code...
					$a .= $re['name']."<br>";
				}
			}
			
			$result['view'] = $a;
		}
		

		echo json_encode($result);
	}

	public function recompute_tpm_p($debug = 0) {

        $transactions = $this->M_transaction->get_all();

		foreach ($transactions as $key => $value) {
			if ($debug) {
				$this->transaction_library->initiate($value['id']);
       			$tpm_p = $this->transaction_library->get_amount_paid(2, 0, 1);
				$reference[$key] = $tpm_p;
			} else {
				$this->transaction_library->initiate($value['id']);
       			$tpm_p = $this->transaction_library->get_amount_paid(2, 0, 1);

				$new_id = ['tpm_p' => $tpm_p];
				$a = $this->M_transaction->update($new_id + $this->u_additional, $value['id']);
				$reference[$value['id']] = $tpm_p;
			}
		}

		// if ($debug) {
			vdebug($reference);
		// }
	}

	public function recompute_billing_dp($debug = 0) {

        $transactions = $this->M_transaction->get_all();

		foreach ($transactions as $key => $value) {
			if ($debug) {
				$this->transaction_library->initiate($value['id']);
       			$dp_p = $this->transaction_library->get_amount(2, 1);

				$reference[$value['id']] = round($dp_p * 100,2);
			} else {
				$this->transaction_library->initiate($value['id']);
       			$dp_p = $this->transaction_library->get_amount(2, 1);

				$reference[$value['id']] = round($dp_p * 100,2);

				$new_id = ['tpm_p_dp' => round($dp_p * 100,2)];
				$a = $this->M_transaction->update($new_id + $this->u_additional, $value['id']);
				$reference[$value['id']] = round($dp_p * 100,2);
			}
		}

		// if ($debug) {
			vdebug($reference);
		// }
	}

	public function get_sales_group_per_transaction($transaction_id = 0,$debug = 0) {

        $transactions = $this->M_transaction->get_all();

		foreach ($transactions as $key => $value) {
			$sales_group_id = get_value_field($value['seller_id'],'sellers','sales_group_id');

			$new_id = ['sales_group_id' => $sales_group_id];
			$a = $this->M_transaction->update($new_id + $this->u_additional, $value['id']);
			$reference[$value['id']] = $sales_group_id;
		}

		// if ($debug) {
			vdebug($reference);
		// }
	}


	public function get_properties_per_transaction($transaction_id = 0,$debug = 0) {

        $transactions = $this->M_transaction->with_t_property()->get_all();

		foreach ($transactions as $key => $transaction) {

			if (isset($transaction['t_property'])) {
				if ($transaction['property_id'] != $transaction['t_property']['property_id']) {
					$reference[$transaction['id']] = $transaction['property_id'] ."-". $transaction['t_property']['property_id'];
				}
			}

		}

		// if ($debug) {
			vdebug($reference);
		// }
	}
}
