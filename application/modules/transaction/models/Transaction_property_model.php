<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_property_model extends MY_Model {

	public $table = 'transaction_properties'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
		'project_id',
		'property_id',
		'lot_area',
		'floor_area',
		'lot_price_per_sqm',
		'house_model_id',
		'package_type_id',
		'house_model_interior_id',
		'total_house_price',
		'total_lot_price',
		'total_selling_price',
		'total_miscellaneous_amount',
		'total_contract_price',
		'total_discount_price',
		'collectible_price',
		'commissionable_type_id',
		'commissionable_amount',
		'commissionable_amount_remarks',
		'is_lot',
		'is_inclusive',
		'vat_amount',
		'is_active',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
		$this->has_one['project'] = array('foreign_model'=>'project/project_model','foreign_table'=>'projects','foreign_key'=>'id','local_key'=>'project_id');
		$this->has_one['transaction'] = array('foreign_model'=>'transaction/transaction_model','foreign_table'=>'transactions','foreign_key'=>'id','local_key'=>'transaction_id');
		$this->has_one['house_model'] = array('foreign_model'=>'house/house_model','foreign_table'=>'house_models','foreign_key'=>'id','local_key'=>'house_model_id');

	}

}
