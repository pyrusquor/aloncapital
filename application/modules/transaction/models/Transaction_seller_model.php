<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_seller_model extends MY_Model {

	public $table = 'transaction_sellers'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
		'seller_id',
		'seller_position_id',
		'sellers_rate',
		'sellers_rate_amount',
		'is_main',
		'commission_amount_rate',
		'is_active',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['seller'] = array('foreign_model'=>'seller/seller_model','foreign_table'=>'sellers','foreign_key'=>'id','local_key'=>'seller_id');
		
		$this->has_many['commissions'] = array('foreign_model'=>'transaction_commission/transaction_commission_model','foreign_table'=>'transaction_commissions','foreign_key'=>'seller_id','local_key'=>'seller_id');

		$this->has_one['transaction'] = array('foreign_model'=>'transaction/transaction_model','foreign_table'=>'transactions','foreign_key'=>'id','local_key'=>'transaction_id');

	}

}
