<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_payment_model extends MY_Model {

	public $table = 'transaction_payments'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
		'total_amount',
		'principal_amount',
		'interest_amount',
		'beginning_balance',
		'ending_balance',
		'period_id',
		'particulars',
		'due_date',
		'next_payment_date',
		'is_paid',
		'is_complete',
		'process',
		'deleted_reason',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update


	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['transaction'] = array('foreign_model' => 'transaction/Transaction_model','foreign_table' => 'transactions', 'foreign_key' => 'id','local_key'=>'transaction_id');
		
		$this->has_many['official_payments']  = array('foreign_model'=>'transaction_payment/transaction_official_payment_model','foreign_table'=>'transaction_official_payments','foreign_key'=>'transaction_payment_id','local_key'=>'id');

		$this->has_many['entry_items'] = array('foreign_model' => 'accounting_entry_items/accounting_entry_items_model', 'foreign_table' => 'accounting_entry_items', 'foreign_key' => 'accounting_entry_id', 'local_key' => 'accounting_entry_id');

	}

	public function total_payment($transaction_id) {
		$this->db->select_sum("total_amount");
		$this->db->from("transaction_payments");
		$this->db->where("transaction_id", $transaction_id);
		$this->db->where("is_complete = 1");
		$this->db->where("deleted_at IS NULL");
		$query = $this->db->get();

		$result = $query->result_array();
		
		return $result[0]['total_amount'] ? $result[0]['total_amount'] : 0;
	}

	public function completed_terms_count($transaction_id, $period_id) {
		$this->db->select("COUNT(id) AS count, period_id");
		$this->db->from("transaction_payments");
		$this->db->where("transaction_id", $transaction_id);
		$this->db->where("period_id", $period_id);
		$this->db->where("is_complete = 1");
		$this->db->where("deleted_at IS NULL");
		$query = $this->db->get();

		$result = $query->result_array();
		
		return $result;
	}

	public function mortgage_term_count($transaction_id, $period_id) {
		$this->db->select("COUNT(id) AS count");
		$this->db->from("transaction_payments");
		$this->db->where("transaction_id", $transaction_id);
		$this->db->where("period_id", $period_id);
		$this->db->where("is_paid = 1");
		$this->db->where("deleted_at IS NULL");
		$query = $this->db->get();

		$result = $query->result_array();
		
		return $result[0]['count'] ? $result[0]['count'] : 0;
	}

	public function check_partial_payments($transaction_id, $period_id) {
		$this->db->select("COUNT(id) AS count");
		$this->db->from("transaction_payments");
		$this->db->where("transaction_id", $transaction_id);
		$this->db->where("period_id", $period_id);
		$this->db->where("is_paid = 1");
		$this->db->where("is_complete = 0");
		$this->db->where("deleted_at IS NULL");
		$query = $this->db->get();

		$result = $query->result_array();

		return $result[0]['count'] ? $result[0]['count'] : 0;
	}

	public function get_due_payments_today($project_id=0){
		$result = [];
		$this->db->join('transactions',' transaction_payments.transaction_id = transactions.id','inner');
		$this->db->where("transactions.deleted_at IS NULL");
		$this->db->where("transaction_payments.deleted_at IS NULL");
		$this->db->where("transaction_payments.due_date = curdate()");
		if($project_id){
			$this->db->where("transactions.project_id = $project_id");
		}
		$query = $this->db->get('transaction_payments')->result_array();
		if($query){
			$result = $query;
		}

		return $result;
	}

}
