<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_adhoc_fee_model extends MY_Model {

	public $table = 'transaction_adhoc_fees'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
		'fee_id',
		'value_type',
		'is_complete',
		'amount',
		'percentage',
		'terms',
		'effectivity_date',
		'is_active',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_many['schedules']  = array('foreign_model'=>'transaction/transaction_adhoc_fee_schedule_model','foreign_table'=>'transaction_adhoc_fee_schedules','foreign_key'=>'adhoc_fee_id','local_key'=>'id');
		$this->has_many['payments']  = array('foreign_model'=>'transaction_payment/Transaction_adhoc_fee_payments_model','foreign_table'=>'transaction_adhoc_fee_payments','foreign_key'=>'adhoc_fee','local_key'=>'id');
	}

}
