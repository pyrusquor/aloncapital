<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaction_model extends MY_Model
{

	public $table = 'transactions'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'reference',
		'ra_number',
		'is_recognized',
		'tax_rate',
		'period_id',
		'buyer_id',
		'seller_id',
		'project_id',
		'property_id',
		'financing_scheme_id',
		'commission_setup_id',
		'reservation_date',
		'expiration_date',
		'due_date',
		'remarks',
		'general_status',
		'collection_status',
		'documentation_status',
		'transaction_loan_document_stage_id',
		'documentation_category',
		'loan_documentation_category',
		'penalty_type',
		'sales_recognize_threshold_date',
		'is_active',
		'personnel_id',
		'wht_rate',
		'wht_amount',
		'is_inclusive',
		'vat_amount',
		'aris_id',
		'sales_recognize_entry_id',
		'tpm_p',
		'tpm_p_dp',
		'sales_group_id',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = TRUE;
		$this->return_as = 'array';

		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		$this->has_one['buyer'] = array('foreign_model' => 'buyer/buyer_model', 'foreign_table' => 'buyers', 'foreign_key' => 'id', 'local_key' => 'buyer_id');
		$this->has_one['seller'] = array('foreign_model' => 'seller/seller_model', 'foreign_table' => 'sellers', 'foreign_key' => 'id', 'local_key' => 'seller_id');
		$this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
		$this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');
		$this->has_one['scheme'] = array('foreign_model' => 'financing_scheme/financing_scheme_model', 'foreign_table' => 'financing_scheme', 'foreign_key' => 'id', 'local_key' => 'financing_scheme_id');

		$this->has_one['t_property'] = array('foreign_model' => 'transaction/transaction_property_model', 'foreign_table' => 'transaction_properties', 'foreign_key' => 'transaction_id', 'local_key' => 'id');

		$this->has_one['refunds'] = array('foreign_model' => 'transaction/transaction_refund_logs_model', 'foreign_table' => 'transaction_refund_logs', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_one['commission_setup'] = array('foreign_model' => 'commissions/commissions_model', 'foreign_table' => 'commission_setup', 'foreign_key' => 'id', 'local_key' => 'commission_setup_id');
		$this->has_many['adhoc_fee'] = array('foreign_model' => 'transaction/transaction_adhoc_fee_model', 'foreign_table' => 'transaction_adhoc_fees', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['adhoc_fee_schedule'] = array('foreign_model' => 'transaction/transaction_adhoc_fee_schedule_model', 'foreign_table' => 'transaction_adhoc_fee_schedules', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['adhoc_fee_payments'] = array('foreign_model' => 'transaction_payment/transaction_adhoc_fee_payments_model', 'foreign_table' => 'transaction_adhoc_fee_payments', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['commission'] = array('foreign_model' => 'transaction_commission/transaction_commission_model', 'foreign_table' => 'transaction_commissions', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['billings'] = array('foreign_model' => 'transaction/transaction_billing_model', 'foreign_table' => 'transaction_billings', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['discounts'] = array('foreign_model' => 'transaction/transaction_discount_model', 'foreign_table' => 'transaction_discounts', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['aris_payments'] = array('foreign_model' => 'aris/aris_payment_model', 'foreign_table' => 'transaction_discounts', 'foreign_key' => 'official_payment_id', 'local_key' => 'id');



		$this->has_many['payments']  = array('foreign_model' => 'transaction/transaction_payment_model', 'foreign_table' => 'transaction_payments', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['official_payments']  = array('foreign_model' => 'transaction_payment/transaction_official_payment_model', 'foreign_table' => 'transaction_official_payments', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['buyers'] = array('foreign_model' => 'transaction/transaction_client_model', 'foreign_table' => 'transaction_clients', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['sellers'] = array('foreign_model' => 'transaction/transaction_seller_model', 'foreign_table' => 'transaction_sellers', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['fees']    = array('foreign_model' => 'transaction/transaction_fee_model', 'foreign_table' => 'transaction_fees', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['promos']  = array('foreign_model' => 'transaction/transaction_discount_model', 'foreign_table' => 'transaction_discounts', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
		$this->has_many['scheme_values'] = array('foreign_model' => 'financing_scheme/financing_scheme_values_model', 'foreign_table' => 'financing_scheme_values', 'foreign_key' => 'financing_id', 'local_key' => 'financing_scheme_id');


		$this->has_many['tbillings'] = array('foreign_model' => 'transaction/transaction_billing_logs_model', 'foreign_table' => 'transaction_billing_logs', 'foreign_key' => 'transaction_id', 'local_key' => 'id');

		$this->has_many['personnel'] = array('foreign_model' => 'transaction_personnel_logs/transaction_personnel_logs_model', 'foreign_table' => 'transaction_personnel_logs', 'foreign_key' => 'transaction_id', 'local_key' => 'id');
	}

	public function get_models_from_project($date = "", $project_id = "", $model_id = "")
	{
		$date = "$date-01";

		if ($model_id == '') {
			$results = [];
			$this->db->select('transaction_properties.house_model_id');
			$this->db->join('transaction_properties', 'transaction_properties.transaction_id=transactions.id', 'inner');
			$this->db->where("year(transactions.reservation_date)=year('$date')");
			$this->db->where("month(transactions.reservation_date)=month('$date')");
			$this->db->where("transactions.general_status>0");
			$this->db->where("transactions.general_status<4");
			$this->db->where("transactions.project_id=$project_id");
			$this->db->where("transaction_properties.house_model_id!=0");
			$this->db->where("transactions.deleted_at is NULL");
			$this->db->where("transaction_properties.deleted_at is NULL");
			$query = $this->db->get('transactions')->result_array();
			$raw_model_list = [];

			foreach ($query as $item) {
				$this->db->select("id");
				$this->db->where('id', $item['house_model_id']);
				$model = $this->db->get('house_models')->result_array();
				array_push($raw_model_list, $model[0]['id']);
			}
			$results = array_unique($raw_model_list);
		} else {
			$this->db->select('transaction_properties.house_model_id');
			$this->db->join('transaction_properties', 'transaction_properties.transaction_id=transactions.id', 'inner');
			$this->db->where("year(transactions.reservation_date)=year('$date')");
			$this->db->where("month(transactions.reservation_date)=month('$date')");
			$this->db->where("transactions.general_status>0");
			$this->db->where("transactions.general_status<4");
			$this->db->where("transactions.project_id=$project_id");
			$this->db->where("transaction_properties.house_model_id=$model_id");
			$this->db->where("transactions.deleted_at is NULL");
			$this->db->where("transaction_properties.deleted_at is NULL");
			$query = $this->db->get('transactions')->result_array();

			if ($query) {
				$results = count($query);
			} else {
				$results = 0;
			}
		}


		return $results;
	}
	public function get_projects($start_date, $end_date)
	{
		$result = [];
		$this->db->select('Count(transactions.id) AS count');
		$this->db->select('projects.name AS label');
		$this->db->select('projects.id');
		$this->db->join('projects', 'transactions.project_id = projects.id', 'inner');
		$this->db->join('transaction_properties', 'transaction_properties.transaction_id=transactions.id', 'inner');
		$this->db->where("transaction_properties.deleted_at is NULL");
		$this->db->where("transactions.deleted_at is NULL");
		$this->db->where("transaction_properties.house_model_id !=0");
		$this->db->where("transactions.general_status>0");
		$this->db->where("transactions.general_status<4");
		$this->db->where("transactions.general_status<4");
		$this->db->group_by("transactions.project_id");
		$this->db->order_by('projects.name');
		$this->db->where("DATE_FORMAT(transactions.reservation_date, '%Y/%m') between '$start_date' and '$end_date'");
		$query = $this->db->get('transactions')->result_array();
		if ($query) {
			$result = $query;
		}

		return $result;
	}
}
