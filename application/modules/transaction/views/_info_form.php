<?php
if($reservation){
    // vdebug($reservation);
    $collectible_price = $reservation['property']['total_selling_price'];
    $total_contract_price = $reservation['property']['total_selling_price'];
}else{
    $id = isset($info['id']) && $info['id'] ? $info['id'] : '';

    $t_property = isset($info['t_property']) && $info['t_property'] ? $info['t_property'] : '';

    
   
    $total_contract_price = isset($t_property['total_contract_price']) && $t_property['total_contract_price'] ? $t_property['total_contract_price'] : '';
    $collectible_price = isset($t_property['collectible_price']) && $t_property['collectible_price'] ? $t_property['collectible_price'] : '';

    $total_discount_price = isset($t_property['total_discount_price']) && $t_property['total_discount_price'] ? $t_property['total_discount_price'] : '';
    $total_miscellaneous_amount = isset($t_property['total_miscellaneous_amount']) && $t_property['total_miscellaneous_amount'] ? $t_property['total_miscellaneous_amount'] : '';
    
    $i['info'] = isset($info) && $info ? $info : '';;  
    $i['fees'] = isset($fees) && $fees ? $fees : '';;  
}

?>
<input type='hidden' name='reservation' value='<?= $reservation['id'] ?>'>
<div class="row">
    <div class="col-md-6">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <?php echo $this->load->view('form/_property_form',$i); ?>
    </div>

    <div class="col-md-6">
         <div class="row hide">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>VAT </label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control" id="vat" placeholder="VAT" name="" value="<?php echo set_value('property[vat]"', @$vat); ?>" readonly>
                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                        </div>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            
            <div class="col-sm-6">
                <div class="form-group">
                    <label>VAT Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="vat_amount" placeholder="VAT Amount" name="" value="<?php echo set_value('property[vat_amount]"', @$total_lot_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Formula =  Total Selling Price * VAT</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
               <?php echo $this->load->view('form/_fees_form',$i); ?>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Total Property Price <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="total_contract_price" placeholder="Total Contract Price" name="property[total_contract_price]" value="<?php echo set_value('property[total_contract_price]"', $total_contract_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Total Selling Price + Total Additional Fees</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php echo $this->load->view('form/_promos_form',$i); ?>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Loan Discount <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="loan_amount_deduction" placeholder="Loan Discount" value="0" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Total Contract Price - Promos & Discounts Loan Amount Type</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Total Contract Price <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="collectible_price" placeholder="Collectible Price" name="property[collectible_price]" value="<?php echo set_value('property[collectible_price]"', $collectible_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Total Contract Price - Promos & Discounts</span>
                </div>
            </div>
        </div>
    </div>
</div>
