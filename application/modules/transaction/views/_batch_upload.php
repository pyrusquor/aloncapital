<!--  Batch Upload Guide -->

    <div class="modal-dialog modal-xl" role="document" id="batchuploadContent">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upload_guide_label">Transaction Upload Guide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-head-bg-brand table-striped table-hover table-bordered table-condensed table-checkable" id="_upload_guide_modal">
                    <thead>
                        <tr class="text-center">
                            <th width="1%" scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Type</th>
                            <th scope="col">Format</th>
                            <th scope="col">Option</th>
                            <th scope="col">Required</th>
                        </tr> 
                    </thead>
                    <?php if (isset($_fillables) && $_fillables) : ?>

                        <tbody>
                            <?php foreach ($_fillables as $_fkey => $_fill) : ?>

                                <?php
                                $_no                =    isset($_fill['no']) && $_fill['no'] ? $_fill['no'] : '';
                                $_name            =    isset($_fill['name']) && $_fill['name'] ? $_fill['name'] : '';
                                $_type            =    isset($_fill['type']) && $_fill['type'] ? $_fill['type'] : '';
                                $_required    =    isset($_fill['required']) && $_fill['required'] ? $_fill['required'] : '';
                                $_dropdown    =    isset($_fill['dropdown']) && $_fill['dropdown'] && !empty($_fill['dropdown']) ? $_fill['dropdown'] : '';
                                ?>

                                <tr>
                                    <td><?php echo @$_no; ?></td>
                                    <td class="_ug_name"><?php echo @$_name; ?></td>
                                    <td><?php echo @$_type; ?></td>
                                    <td>
                                        <?php if ($_name === 'birth_date') : ?>
                                            yyyy-mm-dd (e.g. 2020-01-28)
                                        <?php endif; ?>
                                    </td>
                                    <td class="_ug_option">
                                        <?php if ($_dropdown) : ?>

                                            <ul class="_ul">
                                                <?php foreach ($_dropdown as $_dkey => $_drop) : ?>

                                                    <li><?php echo @$_drop; ?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <!-- <ul class="_ul">
									    <li>1 = Legal Staff</li>
											<li>2 = Legal Head</li>
											<li>3 = Agent</li>
									   </ul> -->
                                    </td>
                                    <td>
                                        <?php if ($_required === 'Yes') : ?>

                                            <span class="kt-font-danger">Yes</span>
                                        <?php elseif ($_required === 'No') : ?>

                                            No
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm btn-icon-sm" data-dismiss="modal">
                    <i class="la la-close"></i> Close
                </button>
            </div>
        </div>
    </div>
