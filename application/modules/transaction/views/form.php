
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><?=$method;?> Transaction</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo base_url('transaction'); ?>" class="btn btn-label-instagram"><i class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div id="_advance_search" class="collapse kt-margin-b-25 kt-margin-t-25 kt-margin-r-60 kt-margin-l-60">
                <div class="row">
                    <div class="col-lg-12">
                        <form class="kt-form" id="financing_scheme_filter">
                            <div class="form-group row">
                                <div class="col-sm-3 mb-3">
                                    <label class="form-control-label">Reservation Amount</label>
                                    <select class="form-control form-control-sm" id="reservation_amount">
                                        <option value="">Select option</option>
                                        <option value="5000">5,000.00</option>
                                        <option value="7000">7,000.00</option>
                                        <option value="10000">10,000.00</option>
                                        <option value="20000">20,000.00</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mb-3">
                                    <label class="form-control-label">Downpayment</label>
                                    <select class="form-control form-control-sm" id="downpayment">
                                        <option value="">Select option</option>
                                        <option value="50">50</option>
                                        <option value="20">20</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mb-3">
                                    <label class="form-control-label">Downpayment Terms</label>
                                    <select class="form-control form-control-sm" id="downpayment_terms">
                                        <option value="">Select option</option>
                                        <option value="60">60</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mb-3">
                                    <label class="form-control-label">Downpayment Interest Rate</label>
                                    <select class="form-control form-control-sm" id="downpayment_interest_rate">
                                        <option value="">Select option</option>
                                        <option value="0">0</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mb-3">
                                    <label class="form-control-label">Loan</label>
                                    <select class="form-control form-control-sm" id="loan">
                                        <option value="">Select option</option>
                                        <option value="50">50</option>
                                        <option value="80">80</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mb-3">
                                    <label class="form-control-label">Loan Terms</label>
                                    <select class="form-control form-control-sm" id="loan_terms">
                                        <option value="">Select option</option>
                                        <option value="60">60</option>
                                        <option value="240">240</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mb-3">
                                    <label class="form-control-label">Loan Interest Rate</label>
                                    <select class="form-control form-control-sm" id="loan_interest_rate">
                                        <option value="">Select option</option>
                                        <option value="6.985">6.985%</option>
                                        <option value="15">15%</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                <div class="kt-grid__item">

                    <!--begin: Form Wizard Nav -->
                    <div class="kt-wizard-v3__nav">
                        <div class="kt-wizard-v3__nav-items">
                            <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label">
                                        <span>1</span> Property Information
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>
                            <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label">
                                        <span>2</span> Billing Information
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>
                            
                            <?php if ($type != "property"): ?>
                                
                            <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label">
                                        <span>3</span> Seller and Commission Information
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>
                            <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label">
                                        <span>4</span> Buyer Information
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>

                            <?php endif ?>

                        </div>
                    </div>
                    <!--end: Form Wizard Nav -->
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                    <!--begin: Form Wizard Form-->
                    <form class="kt-form" method="POST" action="<?php echo base_url('transaction/form'); ?>" id="form_transaction"  enctype="multipart/form-data">
                        <?php $this->load->view('_form'); ?>
                    </form>
                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
</div>