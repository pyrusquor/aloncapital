<?php if (!empty($records)) : ?>
    <div class="row" id="kt_content">
        <?php foreach ($records as $r) : ?>
            <?php
            $id = $r['id'];
            $ul_readonly = "";
            $li_readonly = "";
            $cs_hide = "";
            $ob_hide = "";
            $ndd_hide = "";
            $refund = array();
            $reference = $r['reference'];
            $ra_number = $r['ra_number'];

            $reservation_date = view_date_custom($r['reservation_date']);
            $expiration_date = view_date_custom($r['expiration_date']);
            $created_date = view_date_custom($r['created_at']);
            $sales_group = get_value_field($r['sales_group_id'],'seller_group','name');

            $sales_group = $sales_group ? $sales_group : "N/A";
            // vdebug($r);
            $cp = money_php(@$r['t_property']['collectible_price']);
            $tpm = money_php('0.00');

            $client_id = @$r['buyer_id'];
            $client = get_person($client_id, 'buyers');
            $client_name = get_fname($client);

            $seller_id = @$r['seller_id'];
            $seller_name = @$r['seller']['first_name'] . ' ' . @$r['seller']['last_name'];

            $project_id = $r['project_id'];
            $project_name = $r['project']['name'];

            $property_id = $r['property_id'];
            $property_name = $r['property']['name'];

            $financing_scheme_name = @$r['scheme']['name'];

            $period = Dropdown::get_static('period_names', $r['period_id'], 'view');

            $this->transaction_library->initiate($id);

            $dp_billing_percentage = $r['tpm_p_dp'];

            $tpm = $this->transaction_library->get_amount_paid(2, 0);
            $trb = $this->transaction_library->get_remaining_balance(2, 0);

            $tpm_p = $this->transaction_library->get_amount_paid(2, 0, 1);
            $trb_p = $this->transaction_library->get_remaining_balance(2, 0, 1);

            $due_date = $this->transaction_library->due_date();
            $amount_due = $this->transaction_library->total_amount_due();

            $last_payment = $this->transaction_library->last_payment();

            
            $payment_status = $this->transaction_library->payment_status();

            $encoded_by = get_person_name($r['created_by'], "users");

            $general_status = Dropdown::get_static('general_status', $r['general_status']);
            $collection_status = $r['collection_status'];

            $documentation_status =  Dropdown::get_dynamic('transaction_document_stages', $r['documentation_status'], 'name', 'id', 'value');
            $loan_documentation_status =  Dropdown::get_dynamic('transaction_loan_document_stages', $r['transaction_loan_document_stage_id'], 'name', 'id', 'value');

            $loan_documentation_status = ($loan_documentation_status) ?: "No Loan Documents Assigned";

            $w = array('category_id' => 3);

            $general_stages = implode('<br>', Dropdown::get_dynamic('transaction_document_stages', false, 'name', 'id', 'form', 'order_by', $w));
            $loan_stages =  implode('<br>', Dropdown::get_dynamic('transaction_loan_document_stages', false, 'name', 'id', 'form', 'order_by', array('category_id' => 1)));

            // put this to library get_viewed_tickets($transaction_id)
            $ticket_id = ["transaction_id" => $id];
            $tickets = $this->M_Transaction_ticketing->get_all($ticket_id);

            $tickets_viewed = 0;

            if ($tickets) {

                $viewed_tickets = array_filter($tickets, function ($v) {
                    return $v['is_viewed'] != 1;
                });

                $tickets_viewed = count($viewed_tickets);
            }
            // put this to library get_viewed_tickets($transaction_id)


            if ($r['general_status'] == 4) {
                $ul_readonly = "hide";
                $li_readonly = "hide";
                $cs_hide = "hide";
                $ob_hide = "hide";
                $ndd_hide = "hide";
                $refund = isset($r['refunds']) && $r['refunds'] ? $r['refunds'] : 0;
            }

            $admin_only = "hide";
            $seller_only_links = 'disabled-link';
            if ($this->ion_auth->is_admin()) {
                $admin_only = "";
                $seller_only_links = '';
            }
            $aris = explode('-', $r['aris_id'])
            ?>
            <!--begin:: Portlet-->
            <div class="kt-portlet ">
                <div class="kt-portlet__body custom-transaction_payments">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden">
                                <img src="./assets/media/project-logos/3.png" alt="image">
                            </div>
                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                                <?php echo get_initials(@$client['first_name'] . ' ' . @$client['last_name']); ?>
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <div>
                                        <a href="<?php echo base_url(); ?>transaction/view/<?php echo $id; ?>" class="kt-widget__username">
                                            Reference : <?php echo $r['id'] . ". " . $reference; ?>
                                            <i class="flaticon2-correct"></i>
                                        </a>
                                        <a class="<?= $admin_only; ?> modal_view view_ar_note ml-5 text-danger" href="javascript: void(0)" class="modal_view" data-url="ticketing/view/0/<?php echo $id; ?>/ar_notes" data-transactionid="<?php echo $id; ?>" id="view_ar_notes">
                                            <b> View AR Notes </b> <i class="fa fa-bell"></i> <span class="badge badge-secondary ml-3" id="tickets_count"><?php echo $tickets_viewed ?></span>
                                        </a>
                                    </div>

                                    <div class="kt-widget__action <?= $admin_only; ?>">

                                        <div class="kt-portlet__head" style="min-height: 0px;">
                                            <div class="kt-portlet__head-label">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>" class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                                                    <span></span>
                                                </label>
                                            </div>

                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                    <i class="flaticon-more-1 kt-font-brand"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('transaction/view/' . $r['id']); ?>" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon-visible"></i>
                                                                <span class="kt-nav__link-text">View Contract</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item <?php echo $li_readonly; ?>">
                                                            <a href="<?php echo base_url('transaction_payment/form/' . $r['id']); ?>" class="kt-nav__link" target="_BLANk">
                                                                <i class="kt-nav__link-icon flaticon-coins"></i>
                                                                <span class="kt-nav__link-text">Collect Payment</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item <?php echo $li_readonly; ?>">
                                                            <a href="<?php echo base_url('ticketing/form/' . $r['id']); ?>" class="kt-nav__link" target="_BLANk">
                                                                <i class="kt-nav__link-icon fa fa-sticky-note"></i>
                                                                <span class="kt-nav__link-text">Create Ticket</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item <?php echo $li_readonly; ?>">
                                                            <a href="<?php echo base_url('transaction_post_dated_check/form/' . $r['id']); ?>" class="kt-nav__link" target="_BLANk">
                                                                <i class="kt-nav__link-icon fa fa-money-check"></i>
                                                                <span class="kt-nav__link-text">Create PDC</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link create_arnote" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-plus"></i>
                                                                <span class="kt-nav__link-text">Create AR Note</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('transaction/letter_request/' . $r['id']); ?>" class="kt-nav__link letter_request">
                                                                <i class="kt-nav__link-icon flaticon2-paper"></i>
                                                                <span class="kt-nav__link-text">Create Letter Request</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('transaction/past_due_notice/' . $r['id']); ?>" class="kt-nav__link letter_request">
                                                                <i class="kt-nav__link-icon flaticon2-paper"></i>
                                                                <span class="kt-nav__link-text">Create Past Due Notice</span>
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                    <i class="flaticon2-document kt-font-brand"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">

                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('transaction/view_transaction_document_stage_log/' . $r['id']); ?>" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-paper"></i>
                                                                <span class="kt-nav__link-text">View General Documents Stage Logs</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('transaction/view_transaction_document_loan_stage_log/' . $r['id']); ?>" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-paper"></i>
                                                                <span class="kt-nav__link-text">View Loan Documents Stage Logs</span>
                                                            </a>
                                                        </li>


                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('transaction/process_checklist/' . $r['id']); ?>" class="kt-nav__link process_checklist">
                                                                <i class="kt-nav__link-icon flaticon2-paper"></i>
                                                                <span class="kt-nav__link-text">Upload General Documents</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo base_url('transaction/process_loan_checklist/' . $r['id']); ?>" class="kt-nav__link process_checklist">
                                                                <i class="kt-nav__link-icon flaticon2-paper"></i>
                                                                <span class="kt-nav__link-text">Upload Loan Documents</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link update_transaction_stage" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-file-1"></i>
                                                                <span class="kt-nav__link-text">Update General Document Stage</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link update_transaction_loan_stage" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-file-1"></i>
                                                                <span class="kt-nav__link-text">Update Loan Document Stage</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                    <i class="flaticon-cogwheel-2 kt-font-brand"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav <?php echo $r['general_status'] == 4 ? '' : 'hide'; ?>">
                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="open" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-checkmark"></i>
                                                                <span class="kt-nav__link-text">Re-open Transaction</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="compute_refund" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-cancel"></i>
                                                                <span class="kt-nav__link-text">Compute Refund Amount</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <ul class="kt-nav <?php echo $ul_readonly; ?>">
                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="change_ra_number" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-information"></i>
                                                                <span class="kt-nav__link-text">Change RA Number</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="interest_rates" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-percentage"></i>
                                                                <span class="kt-nav__link-text">Change Loan Interest Rates</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="due_date" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-event-calendar-symbol"></i>
                                                                <span class="kt-nav__link-text">Change Billing Setting</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="financing_scheme" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-open-text-book"></i>
                                                                <span class="kt-nav__link-text">Change Financing Scheme</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="change_commission_setup" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon fa fa-money-bill-wave"></i>
                                                                <span class="kt-nav__link-text">Change Commission Setup</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="change_property" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon fa fa-hotel"></i>
                                                                <span class="kt-nav__link-text">Change Property Information</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="change_buyer" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon fa fa-user-check"></i>
                                                                <span class="kt-nav__link-text">Change Buyer Information</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="change_seller" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon fa fa-user-check"></i>
                                                                <span class="kt-nav__link-text">Change Seller Information</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" id="adhoc_modal_btn" class="kt-nav__link process_transaction" data-type="adhoc_fee" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-open-text-book"></i>
                                                                <span class="kt-nav__link-text">Add Adhoc Fee</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" id="discount_modal_btn" class="kt-nav__link process_transaction" data-type="discount" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-coins"></i>
                                                                <span class="kt-nav__link-text">Add Discount</span>
                                                            </a>
                                                        </li>

                                                        <?php if ($period === 'Loan') : ?>
                                                            <li class="kt-nav__item">
                                                                <a href="javascript: void(0)" id="discount_modal_btn" class="kt-nav__link process_transaction" data-type="move_in" data-id="<?php echo $r['id']; ?>">
                                                                    <i class="kt-nav__link-icon flaticon-buildings"></i>
                                                                    <span class="kt-nav__link-text">Move In</span>
                                                                </a>
                                                            </li>
                                                        <?php endif; ?>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="compute_refund" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-cancel"></i>
                                                                <span class="kt-nav__link-text">Cancel Transaction</span>
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                    <i class="flaticon-user-ok kt-font-brand"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav <?php echo $ul_readonly; ?>">
                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link add_personnel" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-users"></i>
                                                                <span class="kt-nav__link-text">Add Personnel</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item <?php echo $li_readonly; ?>">
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="update_transaction" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon fa fa-edit"></i>
                                                                <span class="kt-nav__link-text">Update</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <!-- <a href="javascript: void(0)" class="kt-nav__link remove_transaction" data-id="<?php echo $r['id']; ?>"> -->
                                                            <a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="delete_transaction" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon-delete-1"></i>
                                                                <span class="kt-nav__link-text">Delete</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" id="adhoc_modal_btn" class="kt-nav__link process_transaction" data-type="recompute_schedule" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-refresh"></i>
                                                                <span class="kt-nav__link-text">Recompute Schedule</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="<?= base_url(); ?>transaction/process_commission/<?= $r['id']; ?>/update" class="kt-nav__link" data-type="process_commission" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-refresh"></i>
                                                                <span class="kt-nav__link-text">Process Commission</span>
                                                            </a>
                                                        </li>

                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link update_transaction_loan_documents" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-refresh"></i>
                                                                <span class="kt-nav__link-text">Process Loan Documents</span>
                                                            </a>
                                                        </li>
                                                        <?php if ($r['aris_id']) : ?>
                                                            <li class="kt-nav__item">
                                                                <a href="javascript: void(0)" class="kt-nav__link sync_aris_payment" data-id="<?php echo $aris[0]; ?>" data-documentid="<?= $aris[1] ?>">
                                                                    <i class="kt-nav__link-icon flaticon2-refresh"></i>
                                                                    <span class="kt-nav__link-text">Sync Aris Payments</span>
                                                                </a>
                                                            </li>
                                                        <?php endif ?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget__info mt-3">
                                    <div class="kt-widget__desc">
                                        <a class="kt-widget__username" class="kt-widget__username">
                                            Reservation Agreement Number : <strong><?php echo $ra_number; ?></strong>
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>project/view/<?php echo $project_id; ?>" target="_BLANK" class="kt-widget__username <?= $seller_only_links ?>">
                                            Project : <strong><?php echo $project_name; ?></strong>
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>property/view/<?php echo $property_id; ?>" target="_BLANK" class="kt-widget__username <?= $seller_only_links ?>">
                                            Property : <strong><?php echo $property_name; ?></strong>
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>buyer/view/<?php echo $client_id; ?>" target="_BLANK" class="kt-widget__username <?= $seller_only_links ?>">
                                            Buyer : <strong><?php echo $client_name; ?></strong>
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>seller/view/<?php echo $seller_id; ?>" target="_BLANK" class="kt-widget__username <?= $seller_only_links ?>">
                                            Seller : <strong><?php echo $seller_name; ?></strong><br>
                                            Sales Group : <strong><?php echo $sales_group; ?></strong>
                                        </a>
                                    </div>
                                    <div class="kt-widget__desc">
                                        <a href="#" class="kt-widget__username">
                                            Encoded By / Date : <strong><?php echo $encoded_by . " / " . view_date_custom($r['created_at']); ?></strong>
                                        </a>
                                        <br>
                                        <a href="#" class="kt-widget__username">
                                            Reserved Date : <strong><?php echo $reservation_date; ?></strong>
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>transaction/view_ledger/<?php echo $id; ?>/1" target="_BLANK" class="kt-widget__username">
                                            <i class="fa fa-print"></i> Customer Ledger (Customer's Copy)
                                        </a>
                                        <br>
                                        <a href="<?php echo base_url(); ?>transaction/view_ledger/<?php echo $id; ?>/2" target="_BLANK" class="kt-widget__username <?= $admin_only; ?>">
                                            <i class="fa fa-print"></i> Customer Ledger (Internal)
                                        </a>
                                        <br>
                                        <span class="kt-widget__username">
                                            Remarks : <strong><?php echo $r['remarks']; ?></strong>
                                        </span>
                                        <!-- /aris/ledger/208/1/218/174 -->
                                        <!-- $document_id = 0,$project_id = 0,$lot_id = 0,$customer_id = 0,$ledger = 0 -->
                                        <?php
                                        // $aris_ids = get_aris_ids($r);
                                        $aris_ids = 0;

                                        if (isset($aris_ids['document_id']) && ($aris_ids['document_id'])) { ?>

                                            <a href="<?php echo base_url(); ?>aris/ledger/<?php echo $aris_ids['document_id']; ?>/<?php echo $aris_ids['project_id']; ?>/<?php echo $aris_ids['lot_id']; ?>/<?php echo $aris_ids['buyer_id']; ?>/0" target="_BLANK" class="kt-widget__username">
                                                <i class="fa fa-print"></i> ARIS Customer Ledger (Customer's Copy)
                                            </a>
                                            <br>

                                            <!-- aris/ledger/208/1/218/174/1 -->
                                            <a href="<?php echo base_url(); ?>aris/ledger/<?php echo $aris_ids['document_id']; ?>/<?php echo $aris_ids['project_id']; ?>/<?php echo $aris_ids['lot_id']; ?>/<?php echo $aris_ids['buyer_id']; ?>/1" target="_BLANK" class="kt-widget__username">
                                                <i class="fa fa-print"></i> ARIS Customer Ledger (Internal)
                                            </a>
                                            <br>
                                        <?php } ?>

                                    </div>
                                    <div class="kt-widget__desc">
                                        <span class="kt-widget__username">
                                            Financing Scheme : <strong><?php echo $financing_scheme_name; ?></strong>
                                        </span>
                                        <br>
                                        <span class="kt-widget__username">
                                            DP Billing Percentage : <strong><?php echo $dp_billing_percentage; ?></strong>
                                        </span>
                                        <br>
                                        <span class="kt-widget__username">
                                            Total Collectible Price : <strong><?php echo $cp; ?></strong>
                                        </span>
                                        <br>
                                        <span class="kt-widget__username">
                                            Total Payments made : <strong><?php echo money_php($tpm); ?> (<?php echo $tpm_p; ?>%)</strong>
                                        </span>
                                        <br>
                                        <span class="kt-widget__username">
                                            Total Remaining Balance : <strong><?php echo money_php($trb); ?> (<?php echo $trb_p; ?>%)</strong>
                                            <button type="button" class="btn" data-placement="top" data-toggle="kt-popover" data-trigger="focus" title="" data-html="true" data-content="a. Principal" data-original-title="Information"><i class="flaticon2-information"></i></button>
                                        </span>
                                        <br>

                                        <span class="kt-widget__username">
                                            Last Collection Date : <strong><?=( isset($last_payment['payment_date']) ? view_date($last_payment['payment_date']) : "---");?></strong>
                                        </span>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__bottom mt-0">
                            <div class="kt-widget__item p-0">

                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">General Stage
                                        <button type="button" class="btn" data-placement="top" data-toggle="kt-popover" data-trigger="focus" title="" data-html="true" data-content="<b>GENERAL STAGE</b><br>a. Reserved<br>b. On - Going<br>c. Completed<br>d. Cancelled<br>e. Rebooked<br><b>PERIOD</b><br>a. Reservation<br>b. Downpayment<br>c. Loan<br>d. Equity" data-original-title="General Stages and Period"><i class="flaticon2-information"></i></button>

                                    </span>
                                    <span class="kt-widget__value">
                                        <?php echo get_color($r['general_status'], 'general', $general_status, $period); ?>
                                    </span>
                                </div>
                            </div>

                            <?php if (isset($refund) && ($refund)) : ?>
                                <div class="kt-widget__item p-0">
                                    <div class="kt-widget__details">
                                        <span class="kt-widget__title">
                                            Cancellation Amount
                                            <button type="button" class="btn" data-placement="top" data-toggle="kt-popover" data-trigger="focus" title="" data-html="true" data-content="Date : <?php echo view_date_custom($refund['date_cancellation']); ?><br>Reason : <?php echo trim($refund['remarks']); ?>" data-original-title="Cancellation Details"><i class="flaticon2-information"></i></button>
                                        </span>
                                        <span class="kt-widget__value">
                                            <?php echo money_php($refund['refund_amount']); ?>
                                        </span>
                                    </div>
                                </div>
                            <?php endif ?>

                            <div class="kt-widget__item p-0 <?php echo $cs_hide; ?>">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Collection Status <button type="button" class="btn" data-placement="top" data-toggle="kt-popover" data-trigger="focus" title="" data-html="true" data-content="a. Pre - Reserved<br>b. Reserved<br>c. On - Time<br>d. Past Due<br>e. Fully Paid" data-original-title="Collection Status"><i class="flaticon2-information"></i></button> </span>
                                    <span class="kt-widget__value"><?php echo $payment_status; ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item p-0">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Documentation Stage <button type="button" class="btn" data-placement="top" data-toggle="kt-popover" data-trigger="focus" title="" data-html="true" data-content="<?= $general_stages; ?>" data-original-title="General Document Stages"><i class="flaticon2-information"></i></button></span>
                                    <span class="kt-widget__value"><?php echo $documentation_status; ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item p-0">
                                <div class="kt-widget__details">

                                    <span class="kt-widget__title">Loan Stage <button type="button" class="btn" data-placement="top" data-toggle="kt-popover" data-trigger="focus" title="" data-html="true" data-content="<?= $loan_stages; ?>" data-original-title="Loan Document Stages"><i class="flaticon2-information"></i></button></span>

                                    <span class="kt-widget__value"><?php echo $loan_documentation_status; ?></span>

                                </div>
                            </div>

                            <!-- 
                            <div class="kt-widget__item p-0">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Financing Scheme</span>
                                    <span class="kt-widget__value"><?php echo $financing_scheme_name; ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item p-0">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Total Collectible Price</span>
                                    <span class="kt-widget__value"><?php echo $cp; ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item p-0">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Total Payments Made <button type="button" class="btn" data-placement="top" data-toggle="kt-popover" data-trigger="focus" title="" data-html="true" data-content="a. Principal" data-original-title="Information"><i class="flaticon2-information"></i></button> </span>
                                    <span class="kt-widget__value"><?php echo money_php($tpm); ?> (<?php echo $tpm_p; ?>%) </span>
                                </div>
                            </div>

                             <div class="kt-widget__item p-0">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Total Remaining Balance <button type="button" class="btn" data-placement="top" data-toggle="kt-popover" data-trigger="focus" title="" data-html="true" data-content="a. Principal" data-original-title="Information"><i class="flaticon2-information"></i></button></span>
                                    <span class="kt-widget__value"><?php echo money_php($trb); ?> (<?php echo $trb_p; ?>%)</span>
                                </div>
                            </div> -->

                            <div class="kt-widget__item p-0 <?php echo $ob_hide; ?>">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Past Due Balance</span>
                                    <span class="kt-widget__value"><?php echo money_php($amount_due); ?></span>
                                </div>
                            </div>

                            <div class="kt-widget__item p-0 <?php echo $ndd_hide; ?>">
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Next Due Date</span>
                                    <span class="kt-widget__value"><?php echo view_date_custom($due_date); ?></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>

    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">Search for Sales Record</h3>
                            <p class="kt-callout__desc">
                                Enter buyer / property detail on advance search.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <button id="_advance_search_btn" data-toggle="modal" data-target="#filterModal" aria-expanded="true" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Advance Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<style>
    .hide {
        display: none !important;
    }

    .dropdown-menu {
        min-width: 18rem;
    }

    .kt-widget.kt-widget--user-profile-3 .kt-widget__top .kt-widget__content .kt-widget__info .kt-widget__desc a,
    .kt-widget.kt-widget--user-profile-3 .kt-widget__top .kt-widget__content .kt-widget__info .kt-widget__desc {
        color: black !important;
    }
</style>
<script>
    $(document).ready(function() {
        $('[data-toggle="kt-popover"]').popover();
    });
</script>