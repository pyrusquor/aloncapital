<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Total Sales Dashboard 
            </h3>
        </div>
    </div>
</div>

<?php 
    $total_s = 0;
?>

<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="propertyContent">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <td>Project Name</td>
                                <?php if ($months): ?>
                                    <?php foreach ($months as $key => $month) { //if(empty($key)){ continue; } ?>
                                         <td><?php echo $month; ?></td>
                                    <?php } ?>
                                <?php endif ?>
                                <td>Total</td>
                            </tr>
                        </thead>
                        <tbody>

                        <?php if ($projects): ?>
                            <?php foreach ($projects as $key => $project) { $total_c = 0; $total_tsp = 0; $a_total_c = 0; $a_total_tsp = 0; ?>
                                <tr>
                                    <td><?=$key; ?></td>

                                    <?php if ($months): ?>
                                        <?php foreach ($months as $key2 => $month) { //if(empty($key)){ continue; } ?>

                                            <?php $a_total_c += $aris_projects[$key][$month]['count']; $a_total_tsp += $aris_projects[$key][$month]['total_selling_price']; ?>
                                            <?php $total_c += $project[$month]['count']; $total_tsp += $project[$month]['total_selling_price']; ?>

                                            <?php @$total_aris_count_monthly[$month]['count'] += $aris_projects[$key][$month]['count']; @$total_aris_tsp_monthly[$month]['tsp'] += $aris_projects[$key][$month]['total_selling_price']; ?>
                                            
                                            <?php @$total_rems_count_monthly[$month]['count'] += $project[$month]['count']; @$total_rems_tsp_monthly[$month]['tsp'] += $project[$month]['total_selling_price']; ?>

                                            <td>
                                                <?php if($aris_projects[$key][$month]['count'] ==  $project[$month]['count']){ echo '<i class="flaticon2-correct"></i> '; } else { echo '<i class="flaticon2-cross"></i> '; }  echo "<b>".round((get_percentage($aris_projects[$key][$month]['count'],$project[$month]['count']) * 100),2)."%</b>" ?>

                                                <br>
                                                <a class="view_properties_transaction_dashboard" data-month="<?=$month;?>" data-project-id="<?=$key; ?>" data-mod="ARIS">ARIS</a> : <?php echo $aris_projects[$key][$month]['count']; ?> / <?php echo money_php($aris_projects[$key][$month]['total_selling_price']); ?><br>
                                                <a class="view_properties_transaction_dashboard" data-month="<?=$month;?>" data-project-id="<?=$key; ?>" data-mod="REMS">REMS</a> : <?php echo $project[$month]['count']; ?> / <?php echo money_php($project[$month]['total_selling_price']); ?> <br>

                                            </td>

                                        <?php } ?>

                                    <?php endif ?>

                                    <td> 
                                        <?php 
                                            if($total_c == $a_total_c){ 
                                                echo '<i class="flaticon2-correct"></i> ';
                                            } else { 
                                                echo '<i class="flaticon2-cross"></i> '; 
                                            } 
                                            echo "<b>".round((get_percentage($a_total_c,$total_c) * 100),2)."%</b>";
                                        ?>
                                        <br>
                                        ARIS : <?=$a_total_c; ?> / <?=money_php($a_total_tsp); ?>
                                        <br>
                                        REMS : <?=$total_c; ?> / <?=money_php($total_tsp); ?>
                                    </td>

                                </tr>
                            <?php } ?>
                        <?php endif ?>
                            <tr>
                                <td>Total</td>

                                <?php if ($months): ?>
                                    <?php foreach ($months as $key2 => $month) { //if(empty($key)){ continue; } ?>

                                        <?php $a_total_c += $total_aris_count_monthly[$month]['count']; $a_total_tsp += $total_aris_tsp_monthly[$month]['tsp']; ?>
                                        <?php $total_c += $total_rems_count_monthly[$month]['count']; $total_tsp += $total_rems_tsp_monthly[$month]['tsp']; ?>

                                        <td>
                                            <?php if($total_aris_count_monthly[$month]['count'] ==  $total_rems_count_monthly[$month]['count']){ echo '<i class="flaticon2-correct"></i> '; } else { echo '<i class="flaticon2-cross"></i> '; }  echo "<b>".round((get_percentage($total_aris_count_monthly[$month]['count'],$total_rems_count_monthly[$month]['count']) * 100),2)."%</b>" ?>

                                            <br>

                                            ARIS : <?php echo $total_aris_count_monthly[$month]['count']; ?> / <?php echo money_php($total_aris_tsp_monthly[$month]['tsp']); ?><br>
                                            REMS : <?php echo $total_rems_count_monthly[$month]['count']; ?> / <?php echo money_php($total_rems_tsp_monthly[$month]['tsp']); ?> <br>

                                        </td>

                                    <?php } ?>

                                <?php endif ?>

                                <td> 
                                    <?php 
                                        if($total_c == $a_total_c){ 
                                            echo '<i class="flaticon2-correct"></i> ';
                                        } else { 
                                            echo '<i class="flaticon2-cross"></i> '; 
                                        } 
                                        echo "<b>".round((get_percentage($a_total_c,$total_c) * 100),2)."%</b>";
                                    ?>
                                    <br>
                                    ARIS : <?=$a_total_c; ?> / <?=money_php($a_total_tsp); ?>
                                    <br>
                                    REMS : <?=$total_c; ?> / <?=money_php($total_tsp); ?>
                                </td>

                            </tr>

                        </tbody>

                     
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .flaticon2-correct{
        color: green;
    }
     .flaticon2-cross{
        color: red;
    }
    .view_properties_transaction_dashboard{
        cursor: pointer;
        color: blue !important;
        font-weight: bold;
    }
</style>
