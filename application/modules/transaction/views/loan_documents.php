<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Transaction Loan Documents</h3>
			<?php if ( isset($_total) && $_total ): ?>

			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<span class="kt-subheader__desc" id="_total"><?php echo $_total;?> TOTAL</span>
			<?php endif; ?>
			<!-- <span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
				<input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
				<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
			</div> -->
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<?php if ( isset($_li_id) && $_li_id ): ?>

				<button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="_loan_document_checklist_form">
					<i class="fa fa-plus-circle"></i> Submit
				</button>
				<a href="<?php echo site_url('transaction/document_checklist/'.$_li_id);?>"
					class="btn btn-label-instagram btn-elevate btn-sm">
					<i class="fa fa-reply"></i> Cancel
				</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__body">

		<div class="kt-margin-b-35 kt-margin-t-10">
			<div class="row">
				<div class="col-lg-12">
					<form class="kt-form">
						<div class="form-group row">
							<div class="col-lg-4 mb-3">
								<label class="form-control-label">Checklist Template <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select class="form-control kt-select2" name="checklist" id="checklist"
										data-li_id="<?php echo @$_li_id;?>">
										<?php if ( isset($_checklists) && $_checklists ): ?>

										<option value=""></option>

										<?php foreach ( $_checklists as $key => $_checklist ): ?>

										<?php
													$_name	=	isset($_checklist->name) && $_checklist->name ? $_checklist->name : '';
													$_id		=	isset($_checklist->id) && $_checklist->id ? $_checklist->id : '';
												?>

										<option value="<?php echo @$_id;?>">
											<?php echo @$_name;?>
										</option>
										<?php endforeach; ?>
										<?php else: ?>

										<option value="">-- No record found --</option>
										<?php endif; ?>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
												class="la la-book"></i></span></span>
								</div>
							</div>
							<div class="col-lg-4 mb-3">
								<label class="form-control-label">
									Start Date <code>*</code></label>
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control kt_datepicker _start_date"
										placeholder="Start Date" name="start_date" id="start_date">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
												class="la la-calendar"></i></span>
								</div>

							</div>
							<div class="col-lg-4 mb-3">
								<label class="form-control-label">
									Category <code>*</code></label>
								<div class="kt-input-icon kt-input-icon--left">
									<?php echo form_dropdown('category', Dropdown::get_static('document_checklist_category'), "", 'class="form-control" id="category"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
												class="la la-calendar"></i></span>
								</div>

							</div>
							<div class="col-lg-4 mb-3">
								<label class="form-control-label">
									Loan Document Stage <code>*</code></label>
								<div class="kt-input-icon kt-input-icon--left">
									<select class="form-control suggests" data-module="transaction_loan_document_stages"
										id="transaction_loan_document_stage_id" name="transaction_loan_document_stage_id">
										<?php if ($td_stages_id): ?>
										<option value="<?php echo $td_stages_id; ?>">
											<?php echo @$td_stages_name; ?>
										</option>
										<?php else: ?>
										<option value="0">Select Transaction Loan Document Stages</option>
										<?php endif; ?>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
												class="la la-calendar"></i></span>
								</div>

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<form class="kt-form" action="<?php echo site_url('transaction/insert_loan_document_checklist/'.@$_li_id);?>" method="POST" id="_loan_document_checklist_form">
			<div id="_filtered_loan_document_checklist"></div>
		</form>

	</div>
</div>

<script type="text/javascript">
	var _li_id = '<?php echo isset($_li_id) && $_li_id ? $_li_id : '
	';?>';
</script>