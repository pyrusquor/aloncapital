<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Transaction
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc"><?php echo (!empty($totalRec)) ? $totalRec : 0; ?> TOTAL</span>
                <?php if(!$this->ion_auth->is_buyer()): ?> 
                <form class="kt-margin-l-20" id="kt_subheader_search_form">
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        <input type="text" class="form-control" placeholder="Enter RA No." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"></path>
                                    </g>
                                </svg>

                                <!--<i class="flaticon2-search-1"></i>-->
                            </span>
                        </span>
                    </div>
                </form>
                <?php endif ?>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <?php if($this->ion_auth->is_admin()): ?>
                <!-- <a href="<?php echo site_url('transaction/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Add Transaction
                </a>
                <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                    <i class="fa fa-filter"></i> Filter
                </button> -->
                <a href="<?php echo site_url('transaction/dashboard'); ?>" class="btn btn-label-success btn-elevate btn-sm" target="_BLANK">
                    <i class="fa fa-chart-bar"></i> Dashboard
                </a>
                <a href="<?php echo site_url('fees'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    Transaction Fees
                </a>
                <a href="<?php echo site_url('promos'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    Promos & Discounts
                </a>
                <a href="<?php echo site_url('taxes'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    Taxes
                </a>
                <a href="<?php echo site_url('transaction_confirmation'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    Transaction Confirmation
                </a>
                <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
                    <i class="fa fa-upload"></i> Import
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#_export_option">
                    <i class="fa fa-download"></i> Export
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" id="bulkDelete" disabled>
                    <i class="fa fa-trash"></i> Delete Selected
                </button>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if($this->ion_auth->is_admin()): ?>
<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
                <a href="<?php echo site_url('transaction/form'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Add Transaction
                </a>             
        </div>

        <div class="module__filter">
                <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn"
                data-toggle="modal" data-target="#filterModal" aria-expanded="true" >
                    <i class="fa fa-filter"></i> Filter
                </button>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if($this->ion_auth->is_admin()): ?>
    <?php $this->load->view('view/information_tiles'); ?>
<?php endif ?>

<!-- Batch Upload -->
<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <form class="kt-form" id="_export_csv" action="<?php echo site_url('transaction/export_csv_transactions'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Document Type</label>
                            <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control form-control-sm" name="update_existing_data" id="_doc_type">
                                    <option value=""> -- Select Document Type -- </option>
                                    <option value="transactions">Transactions</option>
                                    <option value="transaction_properties">Transaction Properties</option>
                                    <option value="transaction_billings">Transaction Billing</option>
                                    <option value="transaction_clients">Transaction Clients</option>
                                    <option value="transaction_sellers">Transaction Sellers</option>
                                </select>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
                            </div>
                            <?php echo form_error('update_existing_data'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <form class="kt-form" id="_upload_form" action="<?php echo site_url('transaction/import'); ?>" method="POST">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Upload CSV file:</label>
                            <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
                            <input type="file" name="csv_file" class="" size="1000" accept="*.csv">
                            <input type="text" name="doc_type" class="d-none" id="_doc_type_val">
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group form-group-last row custom_import_style">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm" form="_upload_form" id="_upload_form_btn">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                        <div class="col-lg-6 kt-align-right">
                            <!-- <a href="<?php echo site_url('document/export') ?>" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm">
								<i class="fa fa-file-csv"></i>
							</a> -->
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" form="_export_csv">
                                <i class="fa fa-file-csv"></i>
                            </button>
                            <button id="_upload_guide_btn" type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
<!-- Batch upload here -->
<?php $this->load->view('_batch_upload'); ?>
<!-- Batch upload here -->
</div>

<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="transactionContent">
    <?php $this->load->view('_filter'); ?>
</div>

<!-- EXPORT -->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form" target="_blank" action="javascript:void(0);<?php //echo site_url('transaction/export'); ?>">
                    <div class="row">
                        <?php if (isset($_columns) && $_columns) : ?>

                            <div class="col-lg-12">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" id="_export_select_all"> Field
                                        <span></span>
                                    </label>
                                    <label class="kt-checkbox kt-checkbox--bold"></label>
                                </div>
                            </div>

                            <?php foreach ($_columns as $key => $_column) : ?>

                                <?php if ($_column) : ?>

                                    <?php
                                    $_offset    =    '';
                                    if ($_column === reset($_columns)) {

                                        $_offset    =    'offset-lg-1';
                                    }
                                    ?>

                                    <div class="col-lg-4">
                                        <div class="kt-checkbox-list">
                                            <?php foreach ($_column as $_ckey => $_clm) : ?>

                                                <?php
                                                $_label    =    isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
                                                $_value    =    isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
                                                ?>

                                                <label class="kt-checkbox kt-checkbox--bold">
                                                    <input type="checkbox" name="_export_column[]" class="_export_column" value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
                                                    <span></span>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <!-- Additional Fields -->
                            <div class="col-lg-4">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" name="_export_column[]" class="_export_column" value="day"> Day
                                        <span></span>
                                    </label>
                                </div>
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" name="_export_column[]" class="_export_column" value="month"> Month
                                        <span></span>
                                    </label>
                                </div>
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" name="_export_column[]" class="_export_column" value="year"> Year
                                        <span></span>
                                    </label>
                                </div>
                            </div>

                        <?php else : ?>

                            <div class="col-lg-10 offset-lg-1">
                                <div class="form-group form-group-last">
                                    <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            Something went wrong. Please contact your system administrator.
                                        </div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" id='_export_form_btn'>
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">General Status:</label>
                            <?php echo form_dropdown('general_status', Dropdown::get_static('general_status'), set_value('general_status', @$general_status), 'class="form-control" id="general_status"'); ?>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Collection Status:</label>
                            <?php echo form_dropdown('collection_status', Dropdown::get_static('collection_status'), set_value('collection_status', @$collection_status), 'class="form-control" id="collection_status"'); ?>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="form-control-label">Documentation Stage:</label>
                            <select class="form-control" style="width: 100%" id="documentation_status" name="documentation_status">
                                <option value="">Select Option</option>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Reference:</label>
                            <select class="suggests_modal" style="width: 100%" data-module="transactions" id="reference" name="reference" data-select="reference">
                                <option value="">Select Reference</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Project:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-module="projects" id="project_id" name="project_id">
                                <option value="">Select Project</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="form-control-label">Property:</label>
                            <select class="form-control suggests_modal" style="width: 100%" data-param="project_id" data-module="properties" id="property_id" name="property_id">
                                <option value="">Select Property</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Period:</label>
                            <?php echo form_dropdown('period', Dropdown::get_static('period_names'), set_value('period', @$period), 'class="form-control" id="period"'); ?>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Buyer:</label>
                            <select class="form-control suggests_modal" data-type="person" style="width: 100%" data-module="buyers" id="buyer_id" name="buyer_id">
                                <option value="">Select Buyer</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="form-control-label">Seller:</label>
                            <select class="form-control suggests_modal" data-type="person" style="width: 100%" data-module="sellers" id="seller_id" name="seller_id">
                                <option value="">Select Seller</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                            <div class="form-group col-md-4">
                            <label class="form-control-label">Reservation Date:</label>
                            <input type="text" class="form-control" readonly name="reservation_date" id="kt_transaction_daterangepicker">
                            </div>
                    <div class="form-group col-md-4">
                            <label class="form-control-label">Due for Payment:</label>
                            <input type="text" class="form-control" readonly name="due_for_payment_date" id="kt_transaction_daterangepicker2" >
                        </div>
                    <div class="form-group col-md-4">
                            <label class="form-control-label">Personnel:</label>
                            <select class="form-control suggests_modal" data-type="person" style="width: 100%" data-module="staff" id="personnel_id" name="personnel_id">
                                <option value="">Select Personnel</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="form-control-label">Total Payments Made (%) Greater Than (>):</label>
                            <input type="text" class="form-control" name="greater_than" id="greater_than">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Total Payments Made (%) Equal To (=):</label>
                            <input type="text" class="form-control" name="equal_to" id="equal_to">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Total Payments Made (%) Lower Than (<):</label>
                            <input type="text" class="form-control" name="lower_than" id="lower_than">
                        </div>

                         <div class="form-group col-md-4">
                            <label class="form-control-label">Downpayment Percentage (%) Greater Than (>):</label>
                            <input type="text" class="form-control" name="dp_greater_than" id="dp_greater_than">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Downpayment Percentage (%) Equal To (=):</label>
                            <input type="text" class="form-control" name="dp_equal_to" id="dp_equal_to">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">Downpayment Percentage (%) Lower Than (<):</label>
                            <input type="text" class="form-control" name="dp_lower_than" id="dp_lower_than">
                        </div>

                            <div class="form-group col-md-4">
                            <label class="form-control-label">Day:</label>
                            <input type="text" class="form-control dayPicker" name="day" id="day" readonly>
                            </div>
                            <div class="form-group col-md-4">
                            <label class="form-control-label">Month:</label>
                            <input type="text" class="form-control monthPicker" name="month" id="month" readonly>
                            </div>
                            <div class="form-group col-md-4">
                            <label class="form-control-label">Year:</label>
                            <input type="text" class="form-control yearPicker" name="year" id="year" readonly>
                            </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('modals/create_ar_note');?>
<?php $this->load->view('modals/update_transaction_stage');?>
<?php $this->load->view('modals/update_transaction_loan_stage');?>
<?php $this->load->view('modals/update_transaction_loan_documents');?>
<?php $this->load->view('modals/add_personnel');?>

<input type="hidden" id="search_by_transaction_id" value="<?php echo @$_GET['transaction_id']; ?>">