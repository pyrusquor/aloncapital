\<?php
	$commission_setup = isset($info['commission_setup']) && $info['commission_setup'] ? $info['commission_setup'] : 'N/A';
?>
<div class="row">

	<div class="col-sm-12">
		<!-- Commission Schedule Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_commission_schedule_information">
				<div class="card">
					<div class="card-header" id="head_commission_schedule_information">
						<div class="card-title" data-toggle="collapse" data-target="#commission_schedule_information" aria-expanded="true" aria-controls="commission_schedule_information">
								Commission Schedule <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>
					</div>
					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="commission_schedule_information" class="collapse show" aria-labelledby="head_commission_schedule_information" data-parent="#accord_commission_schedule_information">
						<div class="card-body">
							<div class="form-group form-group-xs row">

									<div class="col-md-12">

										<div class="accordion accordion-light  accordion-toggle-arrow" id="accordionExample2">

											<?php if ($sellers): ?>
												<?php foreach ($sellers as $key => $seller) { ?>
													<?php 
														$sellers_rate_amount = $seller['sellers_rate_amount'];
														$commission_amount_rate = $seller['commission_amount_rate'];
														$wht_amount = $sellers_rate_amount * ( $commission_setup['wht_percentage'] / 100 );
													?>

													<div class="card">
														<div class="card-header" id="headingOne<?=$key;?>">
															<div class="card-title" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
																<?php echo get_fname($seller['seller']); ?> <?php money_php($seller['sellers_rate_amount']) ;?>
															</div>
														</div>



														<div id="collapseOne2" class="collapse show" aria-labelledby="headingOne<?=$key;?>" data-parent="#accordionExample2" style="">

															<div class="card-body">
																<?php 
																	$this->load->model('Transaction_commission/transaction_commission_model', 'M_transaction_commission');
																	$commissions = $this->M_transaction_commission->where( array('transaction_id' => $transaction_id, 'seller_id' => $seller['seller_id']) )->get_all();
																	$total_amt_to_release = 0;
																	$first_release_amount = 0;

																	// if($commissions) {
																	// 	foreach($commissions as $key => $value) {
																	// 		$total_amt_to_release += $commission_amount_rate;
																	// 	}
																	// }
																	// $total_amt_to_release = $total_amt_to_release;
																?>
																<div class="row">
																	<div class="col-md-8">
																		<div class="form-group form-group-xs row">
																			<div class="col-sm-4">
																				<span href="#" class="kt-notification-v2__item">
																					<div class="kt-notification-v2__itek-wrapper">
																						<div class="kt-notification-v2__item-title">
																							<h7 class="kt-portlet__head-title kt-font-primary">Gross Commission Amount</h7>
																						</div>
																						<div class="kt-notification-v2__item-desc">
																							<h6 class="kt-portlet__head-title kt-font-dark">
																								<?php echo money_php($sellers_rate_amount);?>
																							</h6>
																						</div>
																					</div>
																				</span>
																			</div>
																			<div class="col-sm-4">
																				<span href="#" class="kt-notification-v2__item">
																					<div class="kt-notification-v2__itek-wrapper">
																						<div class="kt-notification-v2__item-title">
																							<h7 class="kt-portlet__head-title kt-font-primary">Tax Withholding level</h7>
																						</div>
																						<div class="kt-notification-v2__item-desc">
																							<h6 class="kt-portlet__head-title kt-font-dark">
																								<?=$commission_setup['wht_percentage']?>%
																							</h6>
																						</div>
																					</div>
																				</span>
																			</div>
																			<div class="col-md-4">
																				<span href="#" class="kt-notification-v2__item">
																					<div class="kt-notification-v2__itek-wrapper">
																						<div class="kt-notification-v2__item-title">
																							<h7 class="kt-portlet__head-title kt-font-primary">Withholding Tax Amount:</h7>
																						</div>
																						<div class="kt-notification-v2__item-desc">
																							<h6 class="kt-portlet__head-title kt-font-dark">
																								<?php echo money_php($wht_amount); ?>
																							</h6>
																						</div>
																					</div>
																				</span>
																			</div>
																		</div>
																		
																	</div>
																	
																</div>

																<div class="row">
																	<div class="col-md-12">
																		<?php if ($commissions) { ?>

																			<table class="table table-striped- table-bordered table-hover">
																				<tr>
																					<th>Percentage to Collect</th>
																					<th>Amount to Collect</th>
																					<th>Percentage to Release</th>
																					<th>Amount to Release</th>
																					<th>Net Amount</th>
																					<th>Target Date</th>
																					<th>Remarks</th>
																					<th>Commission Notes</th>
																					<th>Status</th>
																				</tr>

																				<?php foreach ($commissions as $key => $commission) { ?>
																					<?php 
																						if (!$key):
																							$first_release_amount = $commission['commission_amount_rate'];
																						endif;
																					?>
																					<tr>
																						<td><?php echo $commission['percentage_rate_to_collect']; ?>%</td>
																						<td><?php echo money_php($commission['amount_rate_to_collect']); ?></td>
																						<td><?php echo $commission['commission_percentage_rate']; ?>%</td>

																						<td><?php echo money_php($commission['commission_amount_rate']); ?></td>

																						<!-- <td><?php //echo money_php(get_net_amount($sellers_rate_amount, $commission_setup['wht_percentage'], $first_release_amount, $commission['period_type_id'])); ?></td> -->

																						<td><?php echo money_php(get_net_amount($commission['commission_amount_rate'], $commission_setup['wht_percentage'], $first_release_amount, $commission['period_type_id'],$key)); ?></td>

																						<td><?php echo date('F d, Y',strtotime($commission['target_date'])); ?></td>
																						<td><?php echo $commission['remarks']; ?></td>
																						<td><?php echo "N/A"; ?></td>
																						<td><?php echo Dropdown::get_static('commission_status', $commission['status'], 'view'); ?></td>
																					</tr>

																				<?php } ?>


																			</table>

																		<?php } else { ?>

																			<div class="alert alert-outline-danger fade show" role="alert">
																					<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
																					<div class="alert-text"> No commission schedule for this seller. </div>
																					<div class="alert-close">
																						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																							<span aria-hidden="true"><i class="la la-close"></i></span>
																						</button>
																					</div>
																				</div>


																		<?php } ?>

																	</div>
																</div>
															</div>
														</div>
													</div>


												<?php } ?>
											<?php endif ?>

										</div>


									</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	
	
</div>