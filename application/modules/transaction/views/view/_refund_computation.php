<?php 
    $reservation_fee = isset($info['billings'][0]['period_amount']) && $info['billings'][0]['period_amount'] ? $info['billings'][0]['period_amount'] : 0;
    $refund_percentage = isset($refund_percentage) && $refund_percentage ? $refund_percentage : 0;
    $processing_fee = isset($processing_fee) && $processing_fee ? $processing_fee : 0;
    $miscellaneous_fee = isset($miscellaneous_fee) && $miscellaneous_fee ? $miscellaneous_fee : 0;
    $penalty = isset($penalty) && $penalty ? $penalty : 0;

    $tpm_rf = floatval($tpm) - floatval($reservation_fee);
    $fee_sum = floatval($processing_fee) + floatval($miscellaneous_fee) + floatval($penalty);
    $tpm_sub_total = $tpm_rf * $refund_percentage;
    $tpm_total = $tpm_sub_total - $fee_sum;

?>
<html>
    <head>
        <style>
            html { margin:10px 15px 0 10px; font-family:Helvetica; }

            .header p, .header h3 { text-align: center; }
            .account-info { margin: 4rem 25rem }
            .account-info p, .account-info h3 { text-align: left; }

        </style>
    </head>
    <body>
        <div class="row">
            <div class="col-md-12">
                <div class="header">
                    <h3 style="margin-top:-5px">SACRED HEART OF JESUS DEVELOPMENT CORPORATION</h3>
                    <p style="font-size:8pt; line-height:18px; margin-top:-15px">
                        <span style="font-size:10pt">Punta Dulog Commercial Complex St. Joseph Avenue, Pueblo De Panay Township, Brgy. Lawaan, Roxas City</span><br/>
                        (036) 6212 - 806 * 6215 - 844<br/>
                        Fax No. 000 - 0000; Email Address: shjdc@gmail.com; Website: https://www.shjdc.com<br/>
                        VAT Reg TIN #: <u>002 - 240 - 754 - 000</u> 
                    </p>
                    <p style="font-size:8pt; line-height:18px; margin-top:-15px">&nbsp;</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="account-info">
                    <p>
                        <strong>Refund Amount</strong> = ((Total Payment Made - Reservation Fee) * Refund Percentage) - (Processing Fee + Miscellaneous Fee + Penalty)
                    </p>
                    <p>= ((<?=$tpm?> - <?=$reservation_fee?>) * <?=$refund_percentage?>) - (<?=$processing_fee?> + <?=$miscellaneous_fee?> + <?=$penalty?>) </p>
                    <p>= (<?=$tpm_rf?> * <?=$refund_percentage?>) - (<?=$processing_fee?> + <?=$miscellaneous_fee?> + <?=$penalty?>) </p>
                    <p>= <?=$tpm_sub_total?> - <?=$fee_sum?>  </p>
                    <p>= <?=money_php($tpm_total)?></p>
                </div>
            </div>
        </div>
    </body>

</html>