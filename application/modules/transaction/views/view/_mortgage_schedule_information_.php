<?php
$this->transaction_library->initiate($transaction_id);
$transaction_payments = isset($payments) && $payments ? $payments : 0;
$adhoc_fee_total = 0;
$adhoc_fee_balance = 0;
$adhoc_fee_payment = 0;

// Get adhoc fee details 
if ($adhoc_fee) {
	foreach ($adhoc_fee as $key => $value) {
		$adhoc_fee_total += floatval($value['amount']);
	}
}
// Get adhoc fee official payments
if ($transaction_payments) {
	foreach ($transaction_payments as $key => $value) {
		if (isset($value['official_payments'])) {
			foreach ($value['official_payments'] as $id => $data) {
				$adhoc_fee_payment += floatval($data['adhoc_payment']);
			}
		}
	}
}
$adhoc_fee_balance = $adhoc_fee_total - $adhoc_fee_payment;
?>

<div class="row">
	<div class="col-sm-12">
		<!-- General Information -->
		<div class="kt-portlet">
			<div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
				<div class="card">
					<div class="card-header" id="head_general_information">
						<div class="card-title" data-toggle="collapse" data-target="#general_information" aria-expanded="true" aria-controls="general_information">
							Mortgage Schedule <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
									<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
								</g>
							</svg>
						</div>

					</div>


					<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
					<div id="general_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
						<div class="card-body">

							<div class="row">
								<div class="col-md-4 transaction-btn-color">
									<h5> Legend: </h5>
									<div class='btn btn-success'>Paid / Complete Payment</div>
									<div class='btn btn-warning'>Paid / Partial Payment</div>
									<div class='btn btn-danger'>Delayed Payment</div><br><br>
								</div>
								<div class="col-md-2">
									<h5>Total Contract Price: </h5>
									<h6><?php echo $cp ?></h6>
									<br><br>
								</div>
								<div class="col-md-2">
									<h5>Adhoc Fees Payments: </h5>
									<h6><?php echo money_php($adhoc_fee_payment); ?></h6>
									<br><br>
								</div>
								<div class="col-md-2">
									<h5>Penalty Type: </h5>
									<h6><?php echo $penalty_type ?></h6>
									<br><br>
								</div>
								<div class="col-md-2">
									<h5>Adhoc Fee Balance: </h5>
									<h6><?php echo money_php($adhoc_fee_balance); ?></h6>
									<br><br>
								</div>
							</div>




							<div class="form-group form-group-xs row">
								<table class="table table-striped- table-bordered table-hover" id="m_schedule">
									<thead>
										<tr>
											<th>Particulars</th>
											<th>Due Date</th>
											<th>Beginning Balance</th>
											<th>Ending Balance</th>
											<th>Principal</th>
											<th>Interest</th>
											<th>Penalty</th>
											<th>Amount Due</th>
											<th>Payment Date</th>
											<th>Amount Paid</th>
											<th>Receipt #</th>
											<th>Remarks</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ($payments) :
											$previous_official_id = 0;
											$previous_balance = 0;
											$prev_paids = 0;
											$prev_or = 0;
											$prev_period_id = 0;
											$or_number = '';
											$previous_period = 0;
											$previous_payment = 0;
											$previous_amount_due = 0;
											foreach ($payments as $key => $payment) { ?>
												<?php
												$e = 0;
												$status = "";
												$penalty = 0;
												$prev_key = $key > 0 ? $key - 1 : 0;

												if (is_due($payment['due_date'])) {
													$status = "table-danger";
												}

												if (($payment['is_paid']) && ($payment['is_complete'])) {
													$status = "table-success";
												} else if (($payment['is_paid']) && ($payment['is_complete'] == 0)) {
													$status = "table-warning";
												}
												if (($payment['is_paid']) && ($payment['is_complete']) && !isset($payment['official_payments']) && ($previous_official_id)) {

													$CI = &get_instance();

													$p['transaction_id'] = $transaction_id;
													$p['id >'] = $previous_official_id;
													$p['period_id'] = $payment['period_id'];
													$p['deleted_at'] = NULL;
													$CI->db->where($p);
													$CI->db->limit(1);
													$r = $CI->db->get('transaction_official_payments')->result_array();
													$payment['official_payments'] = $r;
													$e = 1;
												}

												$beginning_balance = $payment['beginning_balance'];
												$ending_balance = $payment['ending_balance'];
												$total_amount_paid = [];
												$interest = $payment['interest_amount'];
												$principal = $payment['principal_amount'];
												$og_principal = $payment['principal_amount'];
												$og_total_amount = $payment['total_amount'];
												$period_count = 0;
												$amount_paid = 0;

												// set beginning balance based on the previous ending balance
												if ($previous_period == $payment['period_id']) {
													if ($previous_balance) {
														$beginning_balance = $previous_balance;
													}
												} else {
													$previous_balance = 0;
												}

												$previous_period = $payment['period_id'];

												if ($payment['period_id'] == "3" && $payments[$prev_key]['period_id'] == "1" && $payments[$prev_key]['is_complete']) {
													$beginning_balance -= floatval($payments[$prev_key]['principal_amount']);
												}

												if ($beginning_balance <= 0) {
													continue;
												}
												$id_w_penalty = 0;
												// if payments were made, get total payment for each period and calculate the penalty if not waived
												if (isset($payment['official_payments'])) {
													foreach ($payment['official_payments'] as $key => $official_payment) {
														
														if (empty($official_payment['deleted_at'])) {
															$period_count = intval($official_payment['period_count']);
															$total_principal = $official_payment['principal_amount'];

															$diff = $official_payment['interest_amount'] + $official_payment['penalty_amount'];
															$interest = $official_payment['interest_amount'];

															$amt_paid = $official_payment['principal_amount'] + $official_payment['interest_amount'] + $official_payment['penalty_amount'];

															if (!$e) { //paid amount
																if ($prev_paids) {
																	$amount_paid = $amt_paid - $prev_paids;
																	$prev_paids = 0;
																} else {
																	$amount_paid = $amt_paid;
																}
															} else { // advance payment
																$prev_paids += $amt_due;
																$amount_paid = $amt_due;
															}

															array_push($total_amount_paid, $official_payment['amount_paid']); // get amount paid

															// get penalty amount
															// param: $penalty_log['is_waived]
															// value: 0 = false, 1 = true

															$penalty_log = $this->M_tpenalty_logs->where(['transaction_official_payment_id' => $official_payment['id'], 'is_waived' => 0, 'transaction_payment_id' => $payment['id']])->get();

															// if ($penalty_log) {
															// 	vdebug($penalty_log);
															// }

															$t = $payment['total_amount'];
															$official_penalty_amount = $official_payment['penalty_amount'];

															$days = date_diffs($official_payment['payment_date'], $payment['due_date']);
															$penalty = $this->transaction_library->get_penalty_normal($days, $official_payment['period_id'], $t);
															$up = 0;

															if ((isset($penalty_log['is_waived'])) && ($penalty_log['is_waived'] != "1")) {

																$uphold_penalty = 0;

																if ($penalty_log) {
																	if ($period_count > 1) {
																		if ($payment['id'] == $penalty_log['transaction_payment_id']) {
																			$uphold_penalty =  $penalty_log['amount'];
																			$up = 1;
																		} else {
																			$uphold_penalty = 0;
																			$up = 2;
																		}
																	} else {
																		if ($payment['id'] == $penalty_log['transaction_payment_id']) {
																			$uphold_penalty =  $penalty_log['amount'];
																			$up = 3;
																		} else {
																			$uphold_penalty = 0;
																			$up = 4;
																		}
																	}
																}

																if ($penalty_log['amount'] > $penalty) {
																	$uphold_penalty = $penalty;
																}
															} else {
																if ($period_count > 1) {
																	$uphold_penalty = $penalty;
																	$up = 5;
																} else {
																	$uphold_penalty = 0;
																	$up = 6;
																}
															}

															if (($payment['period_id'] == 3) && ($uphold_penalty != 0)) {
																// echo "<pre>";
																// vdebug($penalty);
																// die();
															}

															if ($uphold_penalty) {
																$id_w_penalty = $official_payment['id'];
															}

															if (!$e) {
																$previous_official_id = $official_payment['id'];

																if ($official_payment['period_count'] <= 1) {
																	$or_count = 0;
																}
															}
														}
													}
												}

												// principal = Amount Paid - (Penalty + Interest)
												// ending balance = Beginning Balance - Principal
												$amount_paid = 0;
												foreach ($total_amount_paid as $amount) {
													$amount_paid += $amount;
												}

												$completed_terms_count = $this->M_transaction_payment->completed_terms_count($transaction_id, $payment['period_id']);

												$completed_terms = $completed_terms_count[0]['count'];

												// total amount paid divided by number of period
												if ($period_count > 1) {
													if ($previous_payment != $previous_amount_due && $prev_or != $official_payment['or_number']) {

														$period_count -= 1;

														$amount_paid = (($amount_paid - ($principal - $previous_payment)) / $period_count) + ($principal - $previous_payment);

														$ctr = 'a';
													} elseif ($prev_or == $official_payment['or_number'] && $amount_paid > $principal) {

														$amount_paid = $principal;
														$ctr = 'b';
													} else {
														// first

														$period_count -= 1;

														if ($period_count > 1) {
															$amount_paid = ($amount_paid / $period_count) - $official_payment['penalty_amount'];
														}

														$amount_paid = $principal + $interest;

														$ctr = 'c ' . $principal . ' ' . $interest;
													}
												}

												// Calculate principal and interest based on official payment type
												// Parameter: $official_payment['payment_application']
												// 0 = Regular Payment
												// 1 = Advance Payment
												// 2 = Balloon Payment
												// 3 = Continuous Payment
												if ($amount_paid != 0) {
													if ($official_payment['payment_application'] == "2") {
														$principal = $amount_paid - $diff;
													} else if ($official_payment['payment_application'] == "3") {
														$principal = $amount_paid;
														$interest = 0;
													} else {
														$principal = $payment['principal_amount'];
														$interest = $payment['interest_amount'];
													}
												} else {
													if (!isset($payment['official_payments'])) {
														$principal = $payment['principal_amount'];
														$interest = $payment['interest_amount'];
													} else {
														$principal = 0;
													}
												}

												// $ending_balance = $beginning_balance - $amount_paid;

												if (!$payment['is_paid']) {
													$ending_balance = $beginning_balance - $og_principal;
												} else if ($payment['is_paid'] && $payment['is_complete']) {
													if ($official_payment['payment_application'] == "3") {
														$ending_balance = $beginning_balance - $principal;
													} else {
														$ending_balance = $beginning_balance - $og_principal;
													}
												} else {
													// $ending_balance = ($beginning_balance - $amount_paid) + ( $interest + $penalty);
													if ($official_payment['payment_application'] == "2") {
														$principal = $amount_paid - $diff;
													} else {
														$ending_balance = $beginning_balance - $og_principal;
													}
												}

												$ending_balance = 1 > $ending_balance ? 0 : $ending_balance;

												$previous_balance = $ending_balance;

												$previous_amount_due = $payment['total_amount'];
												?>
												<tr class="<?php echo $status; ?>">
													<td><?php echo $payment['particulars']; ?></td>
													<td><?php echo view_date_custom($payment['due_date']); ?></td>
													<td data-previous="<?= $previous_balance; ?>"><?php echo money_php($beginning_balance); ?></td>

													<td data-beginning="<?= $beginning_balance; ?>" data-amtpaid="<?= $amount_paid; ?>" data-tpm='<?= json_encode($total_amount_paid); ?>' data-ctr='<?= $ctr; ?>' data-principal='<?= $principal; ?>' data-interest='<?= $interest; ?>' data-previous='<?= $previous_payment; ?>'>
														<?php echo money_php($ending_balance); ?>
													</td>

													<td><?php echo money_php($principal) . "<br>";  ?></td>

													<td><?php echo money_php($interest) . "<br>"; ?></td>

													<!-- penalty -->
													<td data-penalty-log="<?= isset($penalty_log['is_waived']) ? $penalty_log['is_waived'] : ''; ?>" data-up="<?= isset($up) ? $up: ''; ?>">
														<?php
														echo $payment['is_paid'] || $payment['is_complete'] ? money_php($uphold_penalty) . "<br>" : money_php($uphold_penalty = 0) . "<br>";
														?>
													</td>

													<!-- amount due -->
													<td data-principal_amount="<?= $payment['principal_amount']; ?>" data-interest_amount="<?= $payment['interest_amount']; ?>" data-uphold_penalty="<?= $uphold_penalty; ?>">

														<?php //echo money_php($amt_due = $payment['principal_amount'] + $payment['interest_amount'] + $uphold_penalty) . "<br>";
														?>

														<?php echo money_php($amt_due = $og_total_amount + $uphold_penalty) . "<br>"; ?>

														<!-- payment date -->
													<td>
														<?php if (isset($payment['official_payments'])) { ?>
															<?php foreach ($payment['official_payments'] as $key => $official_payment) { ?>
																<?php echo view_date_custom($official_payment['payment_date']) . "<br>"; ?>
															<?php } ?>
														<?php } ?>
													</td>

													<!-- amount paid -->
													<td>
														<?php
														if (isset($payment['official_payments'])) {
															foreach ($payment['official_payments'] as $key => $official_payment) {
																// if(empty($official_payment['deleted_at'])) {

																$period_count = $official_payment['period_count'];

																if (($uphold_penalty) && ($id_w_penalty == $official_payment['id'])) {
																	$amt_paid = $official_payment['principal_amount'] + $official_payment['interest_amount'] + $uphold_penalty;
																} else {
																	$amt_paid = $official_payment['principal_amount'] + $official_payment['interest_amount'] + $official_payment['penalty_amount'];
																}


																// check if payment method if advance payment
																if (!$e) { //paid amount
																	// echo "<span style='color:transparent'>a</span>";
																	if ($prev_paids) {
																		// echo "<span style='color:transparent'>aa</span>";
																		echo money_php($amt_paid - $prev_paids) . " <span style='color:transparent'>1</span><br>";
																		$prev_paids = 0;
																	} else {
																		// echo "<span style='color:transparent'>ab</span>";
																		// if payment is for single period
																		if ($period_count == 1) {
																			// echo "<span style='color:transparent'>aba</span>";
																			// check payment application type
																			// 0 = Regular Payment
																			// 1 = Advance Payment (Principal & Interest) 
																			// 2 = Balloon Payment (Add to  principal all surplus)
																			// 3 = Continuous Payment (Principal only)
																			if ($official_payment['payment_application'] == 1) {
																				$excess_amount = $official_payment['amount_paid'] - $amt_due;
																				if ($excess_amount > 0) {
																					echo money_php($excess_amount) . " <span style='color:transparent'>2</span><br>";
																				} else {
																					echo money_php($amt_due) . " <span style='color:transparent'>3</span><br>";
																				}
																			} else {
																				$excess_amount = $official_payment['amount_paid'] - $amt_due;
																				if ($excess_amount > 0) {
																					echo money_php($excess_amount) . " <span style='color:transparent'>4</span><br>";
																				} else {
																					echo money_php($amt_paid) . " <span style='color:transparent'>5</span><br>";
																				}
																			}
																		} elseif ($period_count > 1) {
																			// echo "<span style='color:transparent'>abb</span>";

																			// check payment application type
																			// 0 = Regular Payment
																			// 1 = Advance Payment (Principal & Interest) 
																			// 2 = Balloon Payment (Add to  principal all surplus)
																			// 3 = Continuous Payment (Principal only)
																			if ($official_payment['payment_application'] == 2) { // Balloon Type
																				// echo "<span style='color:transparent'>abba</span>";
																				echo money_php($amt_paid) . " <span style='color:transparent'>6</span><br>";
																			} elseif ($official_payment['payment_application'] == 3) { // Continuous Payment
																				// echo "<span style='color:transparent'>abbb</span>";

																				$amt_paid = floatval($official_payment['amount_paid']) / intval($period_count);
																				echo money_php($amt_paid) . " <span style='color:transparent'>7</span><br>";
																			} else {
																				// echo "<span style='color:transparent'>abbc</span>";

																				if ($previous_payment != $previous_amount_due && $prev_or != $official_payment['or_number']) {
																					$period_count -= 1;
																					$amt_paid = ($amt_paid - ($principal - $previous_payment)) / $period_count;
																				}

																				if ($period_count == 1) {
																					$excess_amount = $amt_paid - $amt_due;
																					$amt_paid -= $excess_amount;

																					echo money_php($excess_amount) . " (Underpaid amount) <span style='color:transparent'>8</span><br>";
																					echo money_php($amt_paid) . " <span style='color:transparent'>9</span><br>";
																				} else {
																					if ($prev_or == $official_payment['or_number']) {
																						$or_amt_due = $amt_due;
																						$or_amt_paid = $amt_paid;

																						$amt_due = $amt_due * $period_count;
																						$amt_paid = $amt_paid - $amt_due;
																						$rem_payment = $or_amt_paid - $previous_payment;

																						if ($rem_payment < $or_amt_due) {
																							$amt_paid = $rem_payment;
																							echo money_php($amt_paid) . " <span style='color:transparent'data-pv='" . $previous_payment . "' data-paid='" . $amt_paid . "' data-due='" . $amt_due . "'>10y</span><br>";
																						} else if ($amt_paid < $amt_due) {
																							echo money_php($amt_paid) . " <span style='color:transparent'data-pv='" . $previous_payment . "' data-paid='" . $amt_paid . "' data-due='" . $amt_due . "'>10</span><br>";
																						} else {
																							// echo "<span style='color:' data-paid='".$amt_paid."' data-due='".$amt_due."' >".$amt_paid."</span><br>";
																							// echo "<span style='color:' data-paid='".$amt_paid."' data-due='".$amt_due."' >".money_php($amt_due)."</span><br>";
																							echo money_php($amt_due) . " <span style='color:transparent' data-paid='" . $amt_paid . "' data-due='" . $amt_due . "'>10x</span><br>";
																							// echo "xx";
																						}
																					} else {
																						echo money_php($amt_paid) . " <span style='color:transparent'>11</span><br>";
																					}
																				}
																			}
																		} else {
																			// echo "<span style='color:transparent'>abc</span>";
																			echo money_php($amt_paid) . " <span style='color:transparent'>12</span><br>";
																		}
																	}
																} else { // advance payment
																	// echo "<span style='color:transparent'>b</span>";
																	if ($period_count == 1) {
																		$excess_amount = $official_payment['amount_paid'] - $amt_due;
																		if ($excess_amount > 0) {
																			echo money_php($amt_due) . " <span style='color:transparent'>13</span><br>";
																		} else {
																			echo money_php($amt_paid) . " <span style='color:transparent'>14</span><br>";
																		}
																	} else {
																		// same official receipt number from previous payment
																		if ($previous_payment != $previous_amount_due && $prev_or != $official_payment['or_number']) {
																			// for the same period 
																			// amount paid = amount paid - (principal + previous payment) / period count
																			if ($official_payment['period_id'] == $prev_period_id) {
																				if ($prev_or == $official_payment['or_number'] && $amt_paid > $principal) {
																					$period_count -= 1;
																					$amt_paid = (($amt_paid - ($principal - $previous_payment)) / $period_count);
																					echo money_php($principal - $previous_payment) . " (Underpaid amount) <br>";
																					echo money_php($amt_paid) . " <span style='color:transparent'>15</span><br>";
																				} else {
																					echo money_php($amt_due) . " <span style='color:transparent'>16</span><br>";
																				}
																			} else {
																				$period_count -= 1;
																				$amt_paid = (($amt_paid - ($principal - $previous_payment)) / $period_count);
																				echo money_php($amt_paid) . " <span style='color:transparent'>17</span><br>";
																			}
																		} else {
																			$amt_paid = $principal + $interest;
																			echo money_php($amt_paid + $uphold_penalty) . " <span style='color:transparent'>18</span><br>";
																		}
																	}
																}
																// }
															}
														} else {
															echo " <span style='color:transparent'>19</span><br>";
														}
														$previous_payment = isset($amt_paid) ? $amt_paid : '';;
														?>
													</td>

													<!-- receipt # -->
													<td data-prev="<?= $previous_official_id; ?>" data-e="<?= $e; ?>">
														<?php if (isset($payment['official_payments'])) { //echo "<pre>"; print_r($payment['official_payments']); echo "</pre>"; 
														?>
															<?php $or_key = 'A'; ?>
															<?php foreach ($payment['official_payments'] as $key => $official_payment) : ?>
																<?php
																if (empty($official_payment['deleted_at'])) {
																	$or_number = $official_payment['or_number'];

																	if ($official_payment['period_id'] == 1) {
																		echo $or_number . " <span style='color:transparent'>1</span><br>";
																	} elseif (count($payment['official_payments']) > 1) {
																		//  multiple payments
																		//  add key after or number if partial payments made
																		if ($or_number == $prev_or) {
																			$or_number .= "-" . $or_key;
																			echo $or_number . " <span style='color:transparent'>2</span><br>";
																			$or_key++;
																		} else {
																			echo $or_number . " <span style='color:transparent'>3</span><br>";
																		}
																	} else {

																		if ($e) {

																			// if (!$or_count) {
																			// 	$or_count++;
																			// 	echo $or_number."-".$or_count;
																			// } else {
																			// 	$or_count++;
																			// 	echo $or_number."-".$or_count;
																			// }

																			$or_count++;
																			echo $or_number . "-" . $or_count . " <span style='color:transparent'>4</span><br>";;
																		} else {
																			if ($official_payment['period_count'] > 1) {
																				// last row
																				$or_count++;
																				echo $or_number . "-" . $or_count . " <span style='color:transparent'>5</span><br>";
																			} else {
																				// single row
																				echo $or_number . " <span style='color:transparent'>6</span><br>";;
																			}
																		}
																	}

																	$prev_or = $official_payment['or_number'];
																	$prev_or_key = substr($or_number, -1);
																}
																?>
															<?php endforeach; ?>
														<?php } ?>
													</td>

													<!-- remarks # -->
													<td>
														<?php if (isset($payment['official_payments'])) { ?>
															<?php foreach ($payment['official_payments'] as $key => $official_payment) { ?>
																<?php if (empty($official_payment['deleted_at'])) : ?>
																	<?php echo $official_payment['remarks'] . "<br>"; ?>
																<?php endif; ?>
															<?php } ?>
														<?php } ?>
													</td>

												</tr>
											<?php $prev_period_id = $payment['period_id'];
											} ?>
										<?php endif ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<style>
	.table#m_schedule td {
		padding: 0.75rem 0.25rem;
		vertical-align: top;
		border-top: 1px solid #ebedf2;
		font-size: 12px;
	}
</style>