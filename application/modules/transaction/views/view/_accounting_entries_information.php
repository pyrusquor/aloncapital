<div class="row">
    <div class="col-sm-12">
        <!-- General Information -->
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
                <div class="card">
                    <div id="accounting_entries" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
                        <div class="card-body">
                            <!-- CONTENT -->
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__body">
                                    <!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="accounting_entries_table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Company</th>
                                                <th>Project</th>
                                                <th>Payee</th>
                                                <th>Payee Name</th>
                                                <th>OR Number</th>
                                                <th>Invoice Number</th>
                                                <th>Journal Type</th>
                                                <th>Payment Date</th>
                                                <th>Total</th>
                                                <th>Remarks</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <!--end: Datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>