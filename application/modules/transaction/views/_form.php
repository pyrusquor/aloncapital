<!--begin: Basic Transaction Info-->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
    <?php $this->load->view('_info_form'); ?>
</div>

<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
<?php if ($type != "property"): ?>
    <?php $this->load->view('_billing_form'); ?>
<?php else: ?>
    <?php $this->load->view('modals/_financing_scheme_form'); ?>
<?php endif; ?>
</div>

<?php if ($type != "property"): ?>

<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_seller_form'); ?>
</div>

<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_buyer_form'); ?>
</div>

<?php endif; ?>

<!--begin: Form Actions -->
<div class="kt-form__actions">
    <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-prev">
        Previous
    </div>
    <div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-submit">
        Submit
    </div>
    <div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-next">
        Next
    </div>
</div>

<!--end: Form Actions -->