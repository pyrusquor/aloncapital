<html>
  	<head>
    	<title><?php echo $fileTitle ?></title>
	    <style>
			html, body { margin:10px 15px 0 10px; font-family:Helvetica, Arial, Calibri; font-size: 11pt}

			/*@page{
				margin: 20px 20px 20px 20px;
			}*/

			table{
				text-align:left;
				text-transform: capitalize;
			}

			table tbody tr td{padding: 5px; font-size: 8pt}
			table tbody tr td{text-transform: uppercase;}
			table tbody tr td,table tbody tr td:nth-child(2){
				text-align: left !important;
			}
			table thead tr th{border-bottom: 2px solid black !important; padding: 5px;}
			table thead tr th{text-align: left;vertical-align: bottom; font-size: 11pt}
			table tbody tr th{text-align: left;vertical-align: bottom; font-size: 11pt;padding: 5px}
			
			p {
				margin: 0;
			}

			p:last-child{
				margin-bottom: 10px;
			}
			.title{
				text-align: center;
				font-size: 12pt
			}
			.divider-table td{
				border: 1px solid #fff;
				text-align: left;
				text-transform: uppercase;
				font-weight: 600;
			}
		</style>
  	</head>
  	<body>

		<div class="title">
			<p>
				<?php echo $company_name; ?>
				<br>
				<?php echo $location; ?>
				<br><br>
				<?php echo $project_name; ?>
				<br>
				<b><?php echo $fileTitle ?></b>
				<br><br>
				SUMMARY INFORMATION OF: <b><?php echo strtoupper(get_fname($info['buyer'])); ?></b>
			</p>
		</div>


		<?php
            
            $this->transaction_library->initiate( $transaction_id );
            
            $total_paid_amount = $this->transaction_library->get_amount_paid(1, 0);
            $total_paid_principal = $this->transaction_library->get_amount_paid(2, 0);
            $total_paid_interest = $this->transaction_library->get_amount_paid(3, 0);
            $total_paid_penalty = $this->transaction_library->get_amount_paid(4, 0);

            $dp_balance = $this->transaction_library->get_remaining_balance(2, 2);
            $lv_balance = $this->transaction_library->get_remaining_balance(2, 3);

            $dp = $this->transaction_library->get_amount(2);
            $dp_pp = $this->transaction_library->get_amount(2,1);
            $dp_date = $this->transaction_library->effectivity_dates(2);

            $lv_int = $this->transaction_library->interests(3);

            $last = $this->transaction_library->last_payment();
            $last_amount_paid = $last['amount_paid'];
            $last_payment_date = $last['payment_date'];

			$property = $info['property'];
			$t_property = $info['t_property'];
			$billings = $info['billings'];
	
			$tcp 			 = $t_property['total_contract_price'];

			$dp_monthly = $this->transaction_library->monthly_ammort(2);
			$lv_monthly = $this->transaction_library->monthly_ammort(3);

			$cts_number = isset($property['cts_number']) && $property['cts_number'] ? $property['cts_number'] : 'N/A';

			$period_left = $this->transaction_library->period_left();
			$amt_due = $this->transaction_library->total_amount_due();

			$discount = $this->transaction_library->total_discounts();
		?>

		<table width="100%" cellspacing="0" cellpadding="0" border="black">
			<tbody>
				<tr>
					<th style="width:10%">Lot No.:</th>
					<td style="width:12%"><?php echo $property['lot']; ?></td>
					<th style="width:10%">Paid Principal:</th>
					<td style="width:12%"><?php echo money_php($total_paid_principal); ?></td>
					<th style="width:10%">Reservation No.:</th>
					<td style="width:12%"><?php echo $info['reference']; ?></td>
					<th style="width:10%">Contract to Sell No.:</th>
					<td style="width:12%"><?php echo $cts_number; ?></td>
				</tr>
				<tr>
					<th style="width:10%">Area (sqm):</th>
					<td style="width:12%"><?php echo $property['lot_area']; ?></td>
					<th style="width:10%">Paid Interest:</th>
					<td style="width:12%"><?php echo money_php($total_paid_interest); ?></td>
					<th style="width:10%">Reservation Date:</th>
					<td style="width:12%"><?php echo view_date($info['reservation_date']); ?></td>
					<th style="width:10%">Contract to Sell Date:</th>
					<td style="width:12%"><?php echo view_date($property['cts_notarized_date']); ?></td>
				</tr>
				<tr>
					<th style="width:10%">Price / SQM:</th>
					<td style="width:12%"><?php echo money_php($property['lot_price_per_sqm']); ?></td>
					<th style="width:10%">Paid Penalty:</th>
					<td style="width:12%"><?php echo money_php($total_paid_penalty); ?></td>
					<th style="width:10%">Down Payment (%):</th>
					<td style="width:12%"><?php echo convertPercentage($dp_pp); ?>%</td>
					<th style="width:10%">Amortization Amt:</th>
					<td style="width:12%"><?php echo money_php($lv_monthly); ?></td>
				</tr>
				<tr>
					<th style="width:10%">Contract Price:</th>
					<td style="width:12%"><?php echo money_php($tcp); ?></td>
					<th style="width:10%">Total Payment:</th>
					<td style="width:12%"><?php echo money_php($total_paid_amount); ?></td>
					<th style="width:10%">Down Payment Amt:</th>
					<td style="width:12%"><?php echo money_php($dp); ?></td>
					<th style="width:10%">Payment Mode:</th>
					<td style="width:12%">Monthly</td>
				</tr>
				<tr>
					<th style="width:10%">Discount:</th>
					<td style="width:12%"><?php echo money_php($discount); ?></td>
					<th style="width:10%">Last Payment Amt:</th>
					<td style="width:12%"><?php echo money_php($last_amount_paid); ?></td>
					<th style="width:10%">DP Due Date:</th>
					<td style="width:12%"><?php echo view_date($dp_date); ?></td>
					<th style="width:10%">Interest/Annum</th>
					<td style="width:12%"><?php echo $lv_int; ?>%</td>
				</tr>
				<tr>
					<th style="width:10%">Outstanding Bal:</th>
					<td style="width:12%"><?php echo money_php($amt_due); ?></td>
					<th style="width:10%">Last Payment Date:</th>
					<td style="width:12%">
						<?php 
							if ($last_payment_date) {
								echo view_date($last_payment_date); 
							} else {
								echo "N/A";
							}
						?>
					</td>
					<th style="width:10%">Down Payment Balance:</th>
					<td style="width:12%"><?php echo money_php($dp_balance); ?></td>
					<th style="width:10%">Periods Left:</th>
					<td style="width:12%"><?php echo $period_left; ?></td>
				</tr>
				<tr>
					<th style="width:10%" colspan="1">Inhouse Balance:</th>
					<td style="width:12%"colspan="7"><?php echo money_php($lv_balance); ?></td>
				</tr>
			</tbody>
		</table>
		


		<div class="title">
			<p><br><b><h3>AMORTIZATION SCHEDULE</h3></b></p>
		</div>

		<table width="100%" cellspacing="0" cellpadding="0" border="black">
			<thead>
				<tr>
					<th style="width:5%">Due Date</th>
					<th style="width:5%">Period</th>
					<th style="width:10%">Balance</th>
					<th style="width:10%">Principal</th>
					<th style="width:10%">Interest</th>
					<th style="width:10%">Penalty</th>
					<th style="width:10%">Amt. Paid</th>
					<th style="width:10%">Date. Paid</th>
					<th style="width:5%">Receipt Date</th>
					<th style="width:5%">Receipt No.</th>
					<th style="width:15%">Remarks</th>

				</tr>
			</thead>
			<tbody>
				<?php
					if ($payments) :

						$previous_official_id = 0;
						$receipt_count = 0;$end_receipt =0;
						$previous_balance = 0;
						$prev_paids = 0;
						$prev_or = 0;
						$prev_period_id = 0;
						$or_number = '';
						$previous_period = 0;
						$previous_payment = 0;
						$previous_amount_due = 0;
						$t_amt_due = 0;
						$payment_to_complete = 0;
						

						foreach ($payments as $key => $payment) { 
							$e = 0; $end_receipt = 0; 
							$status = "";
							$penalty = 0;
							$prev_key = $key > 0 ? $key - 1 : 0;
							

							if (is_due($payment['due_date'])) {
								$status = "table-danger";
							}

							if (($payment['is_paid']) && ($payment['is_complete'])) {
								$status = "table-success";
							} else if (($payment['is_paid']) && ($payment['is_complete'] == 0)) {
								$status = "table-warning";
							}


							if ( ($payment['is_paid']) && ($payment['is_complete']) && !isset($payment['official_payments']) && ($previous_official_id)) {

								$CI = &get_instance();

								$p['transaction_id'] = $transaction_id;
								$p['id >'] = $previous_official_id;
								$p['period_id'] = $payment['period_id'];
								$p['deleted_at'] = NULL;
								$CI->db->where($p);
								$CI->db->limit(1);
								$r = $CI->db->get('transaction_official_payments')->result_array();
								$payment['official_payments'] = $r;
								$e = 1;

								if (!$receipt_count) {
									$receipt_count = 1;
								}
							}

							$total_amount_paid = [];
							$period_count = 0;

							$penalty =  "";
							$amount_due = "";
							$payment_date = "";
							$amount_paid = "";
							$receipt = "";
							$remarks = "";
							$paid_principal = '';
							$paid_interest = '';

							$beginning_balance = $payment['beginning_balance'];
							$ending_balance = $payment['ending_balance'];
							$principal = $payment['principal_amount'];
							$interest  = $payment['interest_amount'];
							$og_total_amount = $payment['total_amount'];
							

							if (isset($payment['official_payments'])) {
								foreach ($payment['official_payments'] as $key => $official_payment) {
									if (empty($official_payment['deleted_at'])) {
										if ($official_payment['payment_application'] == 1) {
											//Advance Payment
											$remarks = "Advance Payment";
											$receipt = $official_payment['or_number']."-".$receipt_count;
											$payment_date = view_date_custom($official_payment['payment_date']);

											$days = date_diffs($official_payment['payment_date'], $payment['due_date']);
											$penalty = 0; 

											if ($official_payment['penalty_amount']) {
												$penalty = $this->transaction_library->get_penalty_normal($days, $official_payment['period_id'], $og_total_amount);
											}

											if ($e) {

												$principal = $payment['principal_amount'];
												$interest  = $payment['interest_amount'];

												$paid_principal =  money_php($payment['principal_amount']);
												$paid_interest =  money_php($payment['interest_amount']);

												$amount_due = money_php($a = $og_total_amount + $penalty);
												$penalty = money_php($penalty);
												$amount_paid = $amount_due;

												$t_amt_due += $a;

											} else {

												if ($payment['is_complete']) {

													$penalty = money_php($penalty);
													$amount_due = money_php($principal + $interest);
													$amount_paid = $amount_due;

													$paid_principal = money_php($payment['principal_amount']);

													$paid_interest =  money_php($payment['interest_amount']);

													if( count($payment['official_payments']) >= 2 ){

														$payment_to_complete = 1;
														
														//advance with partial, partial already complete
														$remaining = $official_payment['amount_paid'] - $t_amt_due;

														$amount_due = money_php($remaining);

														$amount_paid = $amount_due;

														if ($remaining >= $payment['interest_amount']) {
															# code...
															$paid_interest =  money_php($payment['interest_amount']);
															$remaining = $remaining - $payment['interest_amount'];
														}

														$paid_principal = money_php($remaining);
														
													}
													

												} else {

													//advance with partial is not complete yet
													$penalty = money_php($penalty);

													$remaining = $official_payment['amount_paid'] - $t_amt_due;

													$amount_due = money_php($remaining);

													$amount_paid = $amount_due;

													
													if ($remaining >= $payment['interest_amount']) {
														# code...
														$paid_interest =  money_php($payment['interest_amount']);
														$remaining = $remaining - $payment['interest_amount'];
													}

													$paid_principal = money_php($remaining);
												}

												$end_receipt = 1;
											}
											
											$receipt_count++;
										
										} else if ($official_payment['payment_application'] == 2) {
											//Balloon Payment
											$remarks = "Balloon Payment";

											$paid_principal =  money_php($payment['principal_amount']);
											$paid_interest =  money_php($payment['interest_amount']);

											$penalty = money_php($official_payment['penalty_amount']);
											$amount_due = money_php($official_payment['amount_paid']);
											$payment_date = view_date_custom($official_payment['payment_date']);
											$amount_paid = money_php($official_payment['amount_paid']);
											$receipt = $official_payment['or_number'];

										} else if ($official_payment['payment_application'] == 3) {
											//Continuous Payment
											$remarks = "Continuous Payment";

											$interest = 0;

											$paid_principal =  money_php($payment['principal_amount']);
											$paid_interest =  money_php($payment['interest_amount']);

											$penalty = money_php($official_payment['penalty_amount']);
											$amount_due = money_php($official_payment['amount_paid']);
											$payment_date = view_date_custom($official_payment['payment_date']);
											$amount_paid = money_php($official_payment['amount_paid']);
											$receipt = $official_payment['or_number'];

										} else if ($official_payment['payment_application'] == 4) {
											//Partial Payment

											if ($payment_to_complete) {
												# code...

												//Advance Partial Payment Completion
												$remarks .= "<br>"."Partial Payment";

												$payment_date .= "<br>".view_date_custom($official_payment['payment_date']);
												$principal = $payment['principal_amount'];
												$interest  = $payment['interest_amount'];
												$penalty .= "<br>".money_php($official_payment['penalty_amount']);
												$amount_paid .= "<br>".money_php($official_payment['amount_paid']);

												$amount_due .= "<br>".money_php($official_payment['amount_paid']);
												$receipt .= "<br>".$official_payment['or_number'];

												$paid_principal .= "<br>".money_php($official_payment['principal_amount']);
												$paid_interest .= "<br>".money_php($official_payment['interest_amount']);

												$payment_to_complete = 0;

											} else {
												//Partial Payment
												$remarks .= "Partial Payment"."<br>";

												$payment_date .= view_date_custom($official_payment['payment_date'])."<br>";
												$penalty .= money_php($official_payment['penalty_amount'])."<br>";
												$amount_paid .= money_php($official_payment['amount_paid'])."<br>";

												$paid_principal .= money_php($official_payment['principal_amount'])."<br>";
												$paid_interest .= money_php($official_payment['interest_amount'])."<br>";

												$amount_due .= money_php($official_payment['amount_paid'])."<br>";
												$receipt .= $official_payment['or_number']."<br>";
											}

										} else {
											//regular payment

											$paid_principal =  money_php($payment['principal_amount']);
											$paid_interest =  money_php($payment['interest_amount']);

											$penalty = money_php($official_payment['penalty_amount']);
											$amount_due = money_php($official_payment['amount_paid']);
											$payment_date = view_date_custom($official_payment['payment_date']);
											$amount_paid = money_php($official_payment['amount_paid']);
											$receipt = $official_payment['or_number'];
											$remarks = "Regular Payment";
										}

										if(!$e){
											$previous_official_id = $official_payment['id']; $t_amt_due = 0;
										}
									}
								}
							}
				?>
						<tr class="<?php echo $status; ?>">

							<td><?php echo view_date_custom($payment['due_date']); ?></td>

							<td><?php echo Dropdown::get_static('period_names',$payment['period_id'],'view'); ?></td>

							<td><?php echo money_php($beginning_balance); ?></td>

							<!-- <td><?php //echo money_php($ending_balance); ?> </td> -->

							<td><?php echo money_php($principal);  ?></td>

							<td><?php echo money_php($interest); ?></td>

							<!-- <td><?php //echo money_php($og_total_amount); ?> </td> -->

							<!-- <td><?php //echo ($paid_principal); ?> </td> -->

							<!-- <td><?php //echo ($paid_interest); ?> </td> -->

							<!-- penalty -->
							<td><?php echo ($penalty); ?> </td>

							<!-- amount due -->
							<!-- <td><?php //echo ($amount_due); ?> </td> -->

							

							<!-- amount paid -->
							<td><?php echo ($amount_paid); ?> </td>

							<!-- payment date -->
							<td><?php echo ($payment_date); ?> </td>

							<!-- payment date -->
							<td><?php echo ($payment_date); ?> </td>

							<!-- receipt # -->
							<td> <?php echo $receipt; ?> </td>

							<!-- remarks # -->
							<td> <?php echo $payment['particulars']; ?> - <?php echo $remarks; ?> </td>
						</tr>
					<?php $prev_period_id = $payment['period_id'];  if($end_receipt){ $receipt_count = 0; }
					} ?>
				<?php endif ?>
			</tbody>
		</table>

	</body>
</html>