<?php

// vdebug($records);

?>

<div class="modal fade" id="update_transaction_loan_documents_modal" tabindex="-1" role="dialog"
    aria-labelledby="update_transaction_loan_documents_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Update Loan Documents Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="updateLoanTransactionCategoryForm" method="POST" method="post" action="<?php echo base_url("transaction/update_loan_documents")?>">
                    <input type="text" name="td_transaction_id" class="form-control d-none" id="td_transaction_id">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Transaction Loan Document Category <span class="kt-font-danger"></span></label>

                                <select name="td_transaction_loan_document_category_id" class="form-control" id="td_transaction_loan_document_category_id">
                                    <option value="0">Select Loan Category</option>
                                    <option value="1">HDMF Window 2</option>
                                    <option value="2">HDMF Retail Window</option>
                                    <option value="3">Bank</option>
                                </select>

                                <?php //echo form_dropdown('transaction_loan_document_stage_id', Dropdown::get_dynamic('transaction_loan_document_stages',false,'name','id','form','order_by'), '', 'class="form-control" id="td_transaction_loan_document_stage_id"'); ?>
                            </div>
                        </div>

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="updateLoanTransactionCategoryForm">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>