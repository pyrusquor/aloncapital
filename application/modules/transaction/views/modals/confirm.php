
<?php 
	$transaction_id = isset($info['id']) && $info['id'] ? $info['id'] : 0;
	$validation_info = isset($validation_info) && $validation_info ? $validation_info : '';
?>
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Confirm Transaction Form</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	
	<form id="form_modal" action="<?=base_url();?>transaction/process_transaction" method="post">
		<div class="form-group">
			<input type="hidden" id="transaction_id" name="transaction_id" value="<?=$transaction_id;?>">
			<input type="hidden" id="type" name="type" value="<?=$type;?>">
			<textarea class="d-none" id="info" name="info"><?=$validation_info;?></textarea>
		</div>
		<div class="form-group">
            <label class="">Username <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="username"
                   placeholder="Username" autocomplete="off" id="username">
            <span class="form-text text-muted"></span>
        </div>
		<div class="form-group">
            <label class="">Password <span class="kt-font-danger">*</span></label>
            <input type="password" class="form-control" name="password"
                   placeholder="Password"
                   autocomplete="off" id="password">
            <span class="form-text text-muted"></span>
        </div>
	</form>
	
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" id="confirmation" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>



