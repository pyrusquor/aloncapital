
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Discount Form</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	<form id="form_modal" action="<?=base_url();?>transaction/process_transaction" method="post">
			<input type="hidden" id="transactionID" value="<?=$info['id'];?>" name="transaction_id">
			<input type="hidden" id="type" value="<?=$type;?>" name="type">
			<!-- <input type="hidden" id="fs_values" name="fs_values">
			<input type="hidden" id="tb_values" name="tb_values">
			<input type="hidden" id="official_payments" name="official_payments">
			<input type="hidden" id="completed_terms" name="completed_terms">
			<input type="hidden" id="dp_prp" name="dp_prp">
			<input type="hidden" id="lo_prp" name="lo_prp">
			<input type="hidden" id="eq_prp" name="eq_prp">
			<input type="hidden" id="dp_pip" name="dp_pip">
			<input type="hidden" id="lo_pip" name="lo_pip">
			<input type="hidden" id="eq_pip" name="eq_pip">
			<input type="hidden" id="ra_amount" name="ra_amount">
			<input type="hidden" id="dp_amount" name="dp_amount">
			<input type="hidden" id="lo_amount" name="lo_amount">
			<input type="hidden" id="eq_amount" name="eq_amount">
			<input type="hidden" id="ra_term" name="ra_term">
			<input type="hidden" id="dp_term" name="dp_term">
			<input type="hidden" id="lo_term" name="lo_term">
			<input type="hidden" id="eq_term" name="eq_term">
			<input type="hidden" id="dp_partial_payment" name="dp_partial_payment">
			<input type="hidden" id="lo_partial_payment" name="lo_partial_payment"> -->

			<div class="row">
				<div class="col-md-6 d-none">
					<!-- <div class="form-group">
						<label class="form-control-label">Discount</label>
						<select class="form-control kt-select2" id="fs_discount" name="fs_discount_id">
							<option value="">Select Option</option>
						</select>
						<span class="form-text text-muted"></span>
					</div> -->
				</div>
				<!-- <div class="col-md-6">
					<div class="form-group">
						<label>Total Selling Price</label>
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" id="total_selling_price" value="<?=$info['t_property']['total_selling_price']?>" readonly>
						</div>
					</div>
				</div> -->
				<div class="col-md-6">
					<div class="form-group">
						<label>Current Contract Price</label>
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" id="total_selling_price" value="<?=$info['t_property']['total_contract_price']?>" readonly>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
				<div id="promos_modal_form_repeater">
					<div class="form-group">
						<label>Promos & Discounts : 
							<div data-repeater-create="" class="btn btn btn-primary">
								<span>
									<i class="la la-plus"></i>
									<span>Add</span>
								</span>
							</div>
						</label>

						<div data-repeater-list="promos" class="col-lg-12">
							<div data-repeater-item class="row kt-margin-b-10">

								<div class="col-sm-4">
									<div class="input-group">
										<div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
										<input type="text" class="form-control promoPrice" id="discount_amount_rate" placeholder="Amount" name="discount_amount_rate" value="<?php echo set_value('promos[price]"', @$discount_amount_rate); ?>">
									</div>
								</div>

								<div class="col-sm-4">
									<div class="input-group">
										<select class="form-control promoID" id="discount_id" name="discount_id">
											<option value="0">Select Option</option>
											<option value="1">Deductable to Loan</option>
											<option value="2">Deductable to TCP</option>
										</select>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="input-group">
										<input type="text" class="form-control" id="discount_remarks" placeholder="Remarks" name="discount_remarks" >
										
										<a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon ml-2">
											<i class="la la-remove"></i>
										</a>
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
       		</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label>Loan Discount <span class="kt-font-danger">*</span></label>
						<div class="kt-input-icon kt-input-icon--left">
							<div class="input-group">
								<div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
								<input type="text" class="form-control" id="loan_amount_deduction" placeholder="Loan Discount" value="0" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label>Total Contract Price <span class="kt-font-danger">*</span></label>
						<div class="kt-input-icon kt-input-icon--left">
							<div class="input-group">
								<div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
								<input type="text" class="form-control" id="collectible_price" placeholder="Collectible Price" name="property[collectible_price]" readonly>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			    <div class="col-md-6">
			        <div class="row">
			            <div class="col-sm-12">
			                <div class="form-group">
			                    <label class="">Financing Scheme </label>
			                    <select class="form-control kt-select2" id="financing_scheme_id" name="financing_scheme_id" disabled>
			                        <option value="">Select Option</option>
									<?php foreach($schemes as $scheme):?>
										<option value="<?php echo $scheme['id'] ?>" <?php echo ($info['financing_scheme_id'] == $scheme['id']) ? 'selected':''?> data-scheme="<?php echo $scheme['scheme_type']?>"><?php echo ucwords($scheme['name'])?></option>
									<?php endforeach;?>

			                    </select>
			                    <span class="form-text text-muted"></span>
			                </div>
			            </div>
			        </div>

			    </div>
			</div>

	</form>
	
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_billing_information',$info,true); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_account_information',$info,true); ?>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>



