
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Adhoc Fee Form</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	<form id="form_modal" action="<?=base_url();?>transaction/process_transaction" method="post">
        <input type="hidden" id="transactionID" value="<?=$info['id'];?>" name="transaction_id">
        <input type="hidden" id="type" value="<?=$type;?>" name="type">
        <div class="container">
			<div class="row">
				<div class="alert alert-custom alert-white alert-shadow fade show gutter-b" role="alert">
					<div class="alert-text">
					<i class="flaticon-danger text-danger"></i> Balloon Payments will be deducted in the remaining period term.
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-md-12">
                    <label>Document Stage</label>
					<div class="modal-text">
                        <h4 class="modal-title"><?=Dropdown::get_static('period_names', $info['period_id'], 'view');?></h4>
                    <div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
						<label>Period Term</label>
					<div class="modal-text">
						<h4><?=$loan['period_term']?></h4>
					</div>
				</div>
				<div class="col-md-4">
						<label>Completed Terms</label>
					<div class="modal-text">
						<h4><?=$loan['completed_terms']?></h4>
					</div>
				</div>
				<div class="col-md-4">
						<label>Remaining Terms</label>
					<div class="modal-text">
						<h4><?=$loan['remaining_terms']?></h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?php echo $this->load->view('view/_account_information'); ?>
				</div>
			</div>
        </div>
	</form>
	
	
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>



