<?php

// vdebug($records);

?>

<div class="modal fade" id="update_transaction_stage_modal" tabindex="-1" role="dialog"
    aria-labelledby="update_transaction_stage_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Update Transaction Stage</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="updateTransactionStageForm" method="POST" method="post" action="<?php echo base_url("transaction/update_document_stage")?>">
                    <input type="text" name="td_transaction_id" class="form-control d-none" id="td_transaction_id">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Transaction Document Stages <span class="kt-font-danger"></span></label>

                                <select class="form-control" id="td_documentation_status" name="documentation_status">
                                    <?php if ($document_stages): ?>
                                        <?php foreach ($document_stages as $key => $document_stage) {

                                            if ($document_stage['category_id'] == "1") {
                                                $document_stage_category_name = "Lots Only";
                                            } else if ($document_stage['category_id'] == "2") {
                                                $document_stage_category_name = "H&L / Condo";
                                            } else {
                                                $document_stage_category_name = "Both";
                                            }

                                         ?>
                                            <option value="<?=$document_stage['id'];?>">
                                                <?=$document_stage['order_by'].". ".$document_stage_category_name." - ".$document_stage['name'];?>
                                            </option>
                                        <?php } ?>
                                    <?php endif ?>
                                </select>

                                <?php //echo form_dropdown('documentation_status', Dropdown::get_dynamic('transaction_document_stages',false,'name','id','form','order_by'), '', 'class="form-control" id="td_documentation_status"'); ?>
                            </div>
                        </div>

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="updateTransactionStageForm">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>