<?php

// vdebug($records);

?>

<div class="modal fade" id="create_ar_note_modal" tabindex="-1" role="dialog"
    aria-labelledby="create_ar_note_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Create AR Note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="createArNoteForm" method="POST" method="post">
                    <input type="text" name="transaction_id" class="form-control d-none" id="transaction_id">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Remarks <span class="kt-font-danger"></span></label>
                                <textarea class="form-control" id="remarks"
                                    name="remarks"></textarea>
                            </div>
                        </div>

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="createArNoteForm">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>