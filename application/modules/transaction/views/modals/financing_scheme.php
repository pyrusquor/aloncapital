
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Change Financing Scheme Form</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	<form id="form_modal" action="<?=base_url();?>transaction/process_transaction" method="post">
			<input type="hidden" id="transactionID" value="<?=$info['id'];?>" name="transaction_id">
			<input type="hidden" id="type" value="<?=$type;?>" name="type">
			<input type="hidden" id="fs_values" name="fs_values">
			<input type="hidden" id="tb_values" name="tb_values">
			<input type="hidden" id="official_payments" name="official_payments">
			<input type="hidden" id="completed_terms" name="completed_terms">
			<input type="hidden" id="dp_prp" name="dp_prp">
			<input type="hidden" id="lo_prp" name="lo_prp">
			<input type="hidden" id="eq_prp" name="eq_prp">
			<input type="hidden" id="dp_pip" name="dp_pip">
			<input type="hidden" id="lo_pip" name="lo_pip">
			<input type="hidden" id="eq_pip" name="eq_pip">
			<input type="hidden" id="ra_amount" name="ra_amount">
			<input type="hidden" id="dp_amount" name="dp_amount">
			<input type="hidden" id="lo_amount" name="lo_amount">
			<input type="hidden" id="eq_amount" name="eq_amount">
			<input type="hidden" id="ra_term" name="ra_term">
			<input type="hidden" id="dp_term" name="dp_term">
			<input type="hidden" id="lo_term" name="lo_term">
			<input type="hidden" id="eq_term" name="eq_term">
			<input type="hidden" id="dp_partial_payment" name="dp_partial_payment">
			<input type="hidden" id="lo_partial_payment" name="lo_partial_payment">

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="form-control-label">Category</label>
						<div class="kt-input-icon kt-input-icon--left">
							<select class="form-control" name="category_id"
									placeholder="Type"
									autocomplete="off" id="financing_scheme_categories">
								<option>Select option</option>
							</select>
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
										class="la la-briefcase"></i></span></span>
						</div>
						<?php echo form_error('category_id'); ?>
						<span class="form-text text-muted"></span>
					</div>
				</div>
				<div class="col-md-6 hide">
					<div class="form-group">
						<label class="form-control-label">Discount</label>
						<select class="form-control kt-select2" id="fs_discount" name="fs_discount_id">
							<option value="">Select Option</option>
						</select>
						<span class="form-text text-muted"></span>
					</div>
				</div>
			</div>
			<div class="row">
			    <div class="col-md-6">
			        <div class="row">
			            <div class="col-sm-12">
			                <div class="form-group">
			                    <label class="">Financing Scheme <span class="kt-font-danger">*</span></label>
			                    <select class="form-control kt-select2" id="financing_scheme_id" name="financing_scheme_id" disabled>
			                        <option value="">Select Option</option>
									<?php foreach($schemes as $scheme):?>
										<option value="<?php echo $scheme['id'] ?>" <?php echo ($info['financing_scheme_id'] == $scheme['id']) ? 'selected':''?> data-scheme="<?php echo $scheme['scheme_type']?>"><?php echo ucwords($scheme['name'])?></option>
									<?php endforeach;?>

			                    </select>
			                    <span class="form-text text-muted"></span>
			                </div>
			            </div>
			        </div>

			    </div>
			</div>

	</form>
	
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_billing_information',$info,true); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_account_information',$info,true); ?>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>



