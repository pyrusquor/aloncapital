<?php

$current_buyers = isset($info['buyers']) && $info['buyers'] ? $info['buyers'] : '';

$t_property = isset($info['t_property']) && $info['t_property'] ? $info['t_property'] : '';

$commission_setup_id = $commission_setup_id ?? null;

$commissionable_type_id = $t_property['commissionable_type_id'] ?? null;
$commissionable_amount = $t_property['commissionable_amount'] ?? null;
$commissionable_amount_remarks = $t_property['commissionable_amount_remarks'] ?? null;

$total_selling_price = $t_property['total_selling_price'] ?? 0;
$total_contract_price = $t_property['total_contract_price'] ?? 0;
$collectible_price = $t_property['collectible_price'] ?? 0;

?>

<script>
    var window_total_selling_price_commission = "<?= $total_selling_price ?>"
    var window_total_contract_price_commission = "<?= $total_contract_price ?>"
    var window_collectible_price_commission = "<?= $collectible_price ?>"
</script>

<div class="modal-header">
    <h5 class="modal-title" id="changeBuyerModalLabel">Change Commission Setup</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body">
    <form id="form_modal">
        <input type="hidden" id="transactionID" value="<?= $info['id']; ?>" name="transaction_id">
        <input type="hidden" id="type" value="<?= $type; ?>" name="type">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Commission Setup <span class="kt-font-danger">*</span></label>
                    <?= form_dropdown('commission_setup_id', Dropdown::get_dynamic('commission_setup'), $commission_setup_id, 'class="form-control kt-select2"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Sales Force Information : </label>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="">Commissionable Amount <span class="kt-font-danger">*</span></label>
                            <?= form_dropdown('transaction_property[commissionable_type_id]', Dropdown::get_static('commissionable_types'), set_value('commissionable_type_id', $commissionable_type_id), 'class="form-control sellerComp" id="change_commissionable_type_id"'); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="">&nbsp;</label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                                    <input type="text" class="form-control" id="change_commissionable_amount" placeholder="" name="transaction_property[commissionable_amount]" value="<?= set_value('commissionable_amount', $commissionable_amount); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="">Remarks</label>
                            <div class="kt-input-icon kt-input-icon">
                                <textarea rows="3" cols="50" class="form-control" name="transaction_property[commissionable_amount_remarks]" id="change_commissionable_amount_remarks"><?= $commissionable_amount_remarks; ?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>