<?php

// vdebug($records);

?>

<div class="modal fade" id="update_transaction_loan_stage_modal" tabindex="-1" role="dialog"
    aria-labelledby="update_loan_transaction_stage_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Update Loan Stage</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="updateLoanTransactionStageForm" method="POST" method="post" action="<?php echo base_url("transaction/update_loan_document_stage")?>">
                    <input type="text" name="td_transaction_id" class="form-control d-none" id="td_transaction_id">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Transaction Loan Document Stages <span class="kt-font-danger"></span></label>

                                <select class="form-control" id="td_transaction_loan_document_stage_id" name="transaction_loan_document_stage_id">

                                    <?php if ($loan_document_stages): ?>
                                        <?php foreach ($loan_document_stages as $key => $loan_document_stage) {
                                            
                                            if ($loan_document_stage['category_id'] == "1") {
                                                $loan_document_stage_category_name = "HDMF Window 2";
                                            } else if ($loan_document_stage['category_id'] == "2") {
                                                $loan_document_stage_category_name = "HDMF Retail Window";
                                            } else {
                                                $loan_document_stage_category_name = "Bank";
                                            }

                                         ?>
                                            <option value="<?=$loan_document_stage['id'];?>">
                                                <?=$loan_document_stage['order_by'].". ".$loan_document_stage_category_name." - ".$loan_document_stage['name'];?>
                                            </option>
                                        <?php } ?>
                                    <?php endif ?>

                                </select>


                                <?php // echo form_dropdown('transaction_loan_document_stage_id', Dropdown::get_dynamic('transaction_loan_document_stages',false,'name','id','form','order_by'), '', 'class="form-control" id="td_transaction_loan_document_stage_id"'); ?>
                            </div>
                        </div>

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="updateLoanTransactionStageForm">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>