<?php
    $financing_scheme = isset($_financing_scheme) && $_financing_scheme ? $_financing_scheme : '';
    $financing_scheme_values = isset($_financing_scheme['values']) && $_financing_scheme['values'] ? $_financing_scheme['values'] : '';
    $billings = isset($billings) && $billings ? $billings : '';
    $p_id = "";
    $size = 3;

    if (empty($billings)) {
        $transaction_id = $info['id'];
        $t_property = $info['t_property'];
        $total_contract_price = $t_property['total_contract_price'];

        $billings = isset($info['billings']) && $info['billings'] ? $info['billings'] : '';
        $this->transaction_library->initiate($transaction_id);

        $p_id = 1;
        $size = 2;
    }

?>

<div class="row" id="financing_scheme_form">
    <input type="hidden" id="type" value="<?=$type?>">
    <?php if ($p_id): ?>
        
        <input type="hidden" name="type" value="billing_setting">
        <input type="hidden" id="re_paid_amount" value="<?=$this->transaction_library->get_amount_paid(2, 1);?>">
        <input type="hidden" id="dp_paid_amount" value="<?=$this->transaction_library->get_amount_paid(2, 2);?>">
        <input type="hidden" id="lo_paid_amount" value="<?=$this->transaction_library->get_amount_paid(2, 3);?>">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped- table-bordered table-hover">

                    <tr>
                        <th colspan="4">ADDITIONAL INFORMATION</th>
                    </tr>

                    <tr>
                        <th>Total Paid Principal</th>
                        <th>Old Contract Price</th>
                        <th>New Contract Price</th>
                        <th>Remaining Balance to Pay</th>
                    </tr>

                    <tr>
                        <td>
                            <span id="total_paid_principal" data-value="<?=$this->transaction_library->get_amount_paid( 2, 0 );?>">
                            <?=money_php($this->transaction_library->get_amount_paid( 2, 0 ));?>
                            </span>
                        </td>
                        <td><span id="old_contract_price"> <?=money_php($total_contract_price);?> </span></td>
                        <td><span id="new_contact_price"></span></td>
                        <td><span id="remaining_balance_to_pay" data-value=""></span></td>
                   </tr>

                </table>
            </div>    
        </div>
    <?php endif ?>
    
    <div class="col-sm-12">
        
        <div data-repeater-list="" class="col-lg-12">
            <div data-repeater-item="" class="form-group row align-items-center">

                <div class="col-md-1">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label>&nbsp;</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-<?=$size;?>">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Latest Date</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-<?=$size;?>">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Remaining Terms</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Interest</label>
                        </div>
                       
                    </div>
                </div>
                
                <?php if ($p_id): ?>
                    
                <div class="col-md-2 d-none">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Percentage</label>
                        </div>
                       
                    </div>
                </div>

                <?php endif ?>

                <div class="col-md-3">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Remaining Amount</label>
                        </div>
                       
                    </div>
                </div>

            </div>
        </div>

        <?php if ($billings): ?>
           <?php foreach ($billings as $key => $billing) { ?>
                
                <?php 

                    $period_id = isset($billing['period_id']) && $billing['period_id'] ? $billing['period_id'] : '0';

                    $effectivity_date = isset($billing['effectivity_date']) && $billing['effectivity_date'] ? $billing['effectivity_date'] : '0';

                    $period_term = isset($billing['period_term']) && $billing['period_term'] ? $billing['period_term'] : '0';

                    $period_interest_percentage =isset($billing['interest_rate']) && $billing['interest_rate'] ? $billing['interest_rate'] : '0';

                    $period_rate_percentage = isset($billing['period_rate_percentage']) && $billing['period_rate_percentage'] ? $billing['period_rate_percentage'] : '0';
                    $f_scheme_period_rate_percentage = isset($info['scheme_values'][$key]['period_rate_percentage']) && $info['scheme_values'][$key]['period_rate_percentage'] ? $info['scheme_values'][$key]['period_rate_percentage'] : 0;

                    $period_amount = isset($billing['period_amount']) && $billing['period_amount'] ? $billing['period_amount'] : '0';

                    $remaining_amount = $this->transaction_library->get_remaining_balance( 2, $billing['period_id'] );

                    if ($period_term) {
                        $progress = $this->transaction_library->get_amount_paid( 2, $billing['period_id'], 1 );
                        if ($progress >= 100) {
                            continue;
                        }
                    } else {
                        continue;
                    }

                    $ed_readonly = "readonly";
                    $pt_readonly = "readonly";
                    $ir_readonly = "readonly";
                    $pa_readonly = "readonly";
                    $pr_readonly = "readonly";
                    $autocompute = "";

                    $remaining_terms = $this->transaction_library->get_remaining_terms($billing['period_id']);
                    $last_payment = $this->transaction_library->last_payment();
                    $last_payment_by_period = $this->transaction_library->last_payment_by_period($period_id);
                    $last_payment_date = $last_payment_by_period['next_payment_date'];

                    if ($p_id) {
                        $pt_readonly = "";
                        $ir_readonly = "";
                        $pa_readonly = "";
                        $pr_readonly = "";
                        $autocompute = "computeSchemeAmount";
                    }

                    if ($type == "interest_rates") {
                        $ir_readonly = "";
                        $pt_readonly = "";

                        if ($period_id != 3) {
                            continue;
                        }

                    } else {
                        if ($period_id == 1) {
                            continue;
                        }
                    }

                    
                ?>
                <div data-repeater-list="" class="col-lg-12">
                    <div data-repeater-item="" class="form-group row align-items-center">

                        <div class="col-md-1">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">
                                    <?php echo Dropdown::get_static('periods', $period_id, 'view'); ?>
                                    <input type="hidden" id="period_id[]"  name="period_id[]" value="<?php echo $period_id; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-<?=$size;?>">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                     <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control datePicker effectivity_dates" id="effectivity_date[]" placeholder="" name="effectivity_date[<?=$period_id?>]" value="<?php echo empty($last_payment_date) == 1 ? db_date($billing['effectivity_date']) : db_date($last_payment_date); ?>" <?=$ed_readonly; ?> readonly>
                                        <input type="hidden" id="original_effectivity_date" name="original_effectivity_date[<?=$period_id?>]" value="<?php echo empty($last_payment_date) == 1 ? db_date($billing['effectivity_date']) : db_date($last_payment_date); ?>">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-<?=$size;?>">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Mo(s)</span></div>
                                            <input type="text" <?=$pt_readonly; ?> class="form-control terms" id="period_term" placeholder="" name="period_term[<?=$period_id?>]" value="<?php echo $remaining_terms; ?>">
                                            <input type="hidden" id="original_terms" name="original_period_term[<?=$period_id?>]" value="<?php echo $remaining_terms; ?>">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="interest_rate" placeholder="Interest Rate" name="interest_rate[<?=$period_id?>]" <?=$ir_readonly; ?> value="<?php echo $period_interest_percentage; ?>">

                                            <input type="hidden" id="original_interest_rate" name="original_interest_rate[<?=$period_id?>]" value="<?php echo $period_interest_percentage; ?>">

                                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                                        </div>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <?php if ($p_id): ?>
                        
                            <div class="col-md-2 d-none">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__control">

                                        <div class="kt-input-icon kt-input-icon--left">
                                            <div class="input-group">

                                                <input type="text" class="form-control percentage_rate <?=$autocompute;?>" id="percentage_rate" placeholder="Percentage Rate" name="percentage_rate[<?=$period_id?>]" <?=$pr_readonly; ?> value="">

                                                <input type="hidden" id="original_percentage_rate" name="original_percentage_rate[<?=$period_id?>]" value="">

                                                <div class="input-group-append"><span class="input-group-text">%</span></div>

                                            </div>

                                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>

                        <div class="col-md-3">
                            <div class="kt-form__group--inline">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <div class="input-group">

                                        <?php if ($type == "interest_rates"): ?>

                                            <?=money_php($remaining_amount);?>

                                            <input type="hidden" class="form-control period_amount <?=$autocompute;?>" value="<?=$remaining_amount;?>" id="period_amount" placeholder="Period Amount" name="period_amount[<?=$period_id?>]" <?=$pa_readonly; ?>>

                                            
                                        <?php else: ?>

                                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>

                                            <input type="text" class="form-control period_amount <?=$autocompute;?>" id="period_amount" placeholder="Period Amount" name="period_amount[<?=$period_id?>]" <?=$pa_readonly; ?>>

                                        <?php endif; ?>

                                        <input type="hidden" id="original_period_amount" name="original_period_amount[<?=$period_id?>]" value="<?php echo $remaining_amount; ?>">

                                        <input type="hidden" id="period_rate_p" value="<?php echo $f_scheme_period_rate_percentage; ?>">

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

            <?php } ?>

        <?php endif; ?>
     
    </div>

</div>