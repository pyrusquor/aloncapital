
<?php
	$misc = 0;
	$commission = isset($info['commission']) && $info['commission'] ? $info['commission'] : 0;
	$reservation_fee = isset($info['billings'][0]['period_amount']) && $info['billings'][0]['period_amount'] ? $info['billings'][0]['period_amount'] : 0;
	if ($commission) {
		foreach ($commission as $key => $value) {
			if($value['status'] == "4") {
				$misc += floatval($value['commission_amount_rate']);
			}			
		}
	}
?>
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Transaction Cancellation Form</h5>
	<form id="refund_form" onsubmit="setRefundDetail()" target="_blank">
		<input type="submit" class="btn btn-label-primary btn-elevate btn-sm ml-5" value="Print Sample" >
	</form>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	
	

	<form id="form_modal" action="<?=base_url();?>transaction/process_transaction" method="post">
		
		<div class="form-group">
			<label for="date_cancellation" class="form-control-label">Date Cancellation:</label>
			<input type="text" class="form-control datePicker" readonly id="date_cancellation" value="<?=date('Y-m-d');?>" name="date_cancellation">
			<input type="hidden" id="transaction_id" value="<?=$info['id'];?>" name="transaction_id">
			<input type="hidden" id="t_id" value="<?=$info['id'];?>">
			<input type="hidden" id="ra" value="<?=$refund_amount;?>">
			<input type="hidden" id="reservation_fee" value="<?=$reservation_fee;?>">
			<input type="hidden" id="type" value="<?=$type;?>" name="type">
		</div>

		<div class="form-group">
			<label for="tpm" class="form-control-label">Total Payments Made:</label>
			<input readonly type="text" class="form-control" id="tpm" name="tpm" value="<?=$tpm;?>">
		</div>

		<div class="form-group">
			<label for="refund" class="form-control-label">Refund Amount: <small>Refund Amount = (Total Amount Paid - Reservation Fee) * Refund Percentage</small></label>
			<input type="text" class="form-control" id="refund" name="refund" value="<?=$refund_amount;?>">
		</div>

		<hr>
		<h5>Fees</h5>

		<div class="form-group">
			<label for="refund_category" class="form-control-label">Refund Category:</label>
			<?php echo form_dropdown('refund_percentage', Dropdown::get_static('refund_categories'), '', 'class="form-control popField" id="refund_percentage"'); ?>
		</div>

		<div class="form-group">
			<label for="processing_fee" class="form-control-label">Processing Fee:</label>
			<input type="text" class="form-control" id="processing_fee" name="processing_fee" value="<?=$processing_fee?>">
		</div>

		<div class="form-group">
			<label for="misc_fee" class="form-control-label">Miscellaneous Fee:</label>
			<input type="text" class="form-control" id="misc_fee" name="misc_fee" value="<?=$misc?>">
		</div>
		
		<div class="form-group">
			<label for="penalty" class="form-control-label">Penalty:</label>
			<input type="text" class="form-control" id="penalty" name="penalty" value="0">
		</div>

		<div class="form-group">
			<label for="reason" class="form-control-label">Cancellation Reason: <span class="text-danger">*</span></label>
			<textarea class="form-control" id="reason" name="reason" required></textarea>
		</div>

	</form>
	
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_buyer_information',$info,true); ?>
		</div>
	</div>	
	
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_account_information',$info,true); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_property_information',$info,true); ?>
		</div>
	</div>	

	
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>



