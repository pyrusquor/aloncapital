<div class="modal-header">
    <h5 class="modal-title" id="changeSellerModalLabel">Change RA Number Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body">
    <form id="form_modal">
        <input type="hidden" id="transactionID" value="<?= $info['id']; ?>" name="transaction_id">
        <input type="hidden" id="type" value="<?= $type; ?>" name="type">

        <div class="col-sm-6">
            <div class="form-group">
                <label class="">RA Number</label>
                <input type="text" class="form-control" placeholder="RA Number" name="ra_number" value="<?php echo $ra_number; ?>">
            </div>
        </div>
    </form>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>