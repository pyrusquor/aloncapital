
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Adhoc Fee Form</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	<form id="form_modal" action="<?=base_url();?>transaction/process_transaction" method="post">
        <input type="hidden" id="transactionID" value="<?=$info['id'];?>" name="transaction_id">
        <input type="hidden" id="type" value="<?=$type;?>" name="type">
        <input type="hidden" id="tcp" value="<?=$info['property']['total_selling_price']?>" name="tcp">
        <div class="container">
            <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="form-control-label">Terms</label>
						<input class="form-control" id="terms" name="terms" placeholder="Enter number of months"> 
						<span class="form-text text-muted"></span>
					</div>
				</div>
			</div>
			<div class="row">
			    <div class="col-md-6">
			        <div class="form-group">
                        <label class="form-control-label">Effectivity Date:</label>
                        <input type="text" class="form-control kt_datepicker" id="effectivity_date" name="effectivity_date" readonly>
                    </div>
			    </div>
			</div>
            <div class="row" id="fees_modal_form_repeater">
                <label>Select Fees :
                    <div data-repeater-create="" class="btn btn btn-primary">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Add</span>
                        </span>
                    </div>
                </label>
                <div data-repeater-list="fees" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <select class="form-control feesID" id="fees_id" name="fees_id">
                                    <option value="0">Select Fees</option>
                                    <?php if ($fees): ?>
                                        <?php foreach ($fees as $key => $fee) { ?>
                                            <option data-value-type="<?php echo $fee['value_type']; ?>" data-value="<?php echo $fee['value']; ?>" value="<?php echo $fee['id']; ?>"><?php echo $fee['name']; ?></option>
                                        <?php } ?>
                                    <?php endif ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                                <input type="text" class="form-control feesAmount" id="fees_amount_rate" placeholder="Fee Amount" name="fees_amount_rate" value="<?php echo set_value('fees[price]"', @$fees_amount_rate); ?>" readonly>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                <i class="la la-remove"></i>
                            </a>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="row">
				<div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                        <input type="text" class="form-control" id="total_fee" placeholder="Total Amount" name="total_fee" readonly>
                    </div>
				</div>
			</div>
        </div>
	</form>
	
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_adhoc_fee_information'); ?>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>



