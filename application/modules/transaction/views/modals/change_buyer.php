<?php

$current_buyers = isset($info['buyers']) && $info['buyers'] ? $info['buyers'] : '';

?>

<div class="modal-header">
    <h5 class="modal-title" id="changeBuyerModalLabel">Change Buyer Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body">
    <form id="form_modal">
        <input type="hidden" id="transactionID" value="<?= $info['id']; ?>" name="transaction_id">
        <input type="hidden" id="type" value="<?= $type; ?>" name="type">
        <div id="buyer_form_repeater">
            <div class="form-grouprow">
                <label>Buyer Information :
                    <div data-repeater-create="" class="btn btn-sm btn-primary">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Add</span>
                        </span>
                    </div>
                </label>
                <div data-repeater-list="client" class="col-lg-12">
                    <?php if ($current_buyers) : ?>

                        <?php foreach ($current_buyers as $key => $current_buyer) {
                            $CI = &get_instance();
                            $current_buyer_id = isset($current_buyer['client_id']) && $current_buyer['client_id'] ? $current_buyer['client_id'] : '';
                            $current_buyer_info = $CI->M_buyer->get($current_buyer_id);
                            $current_buyer_name = get_fname($current_buyer_info);
                            $current_client_type = isset($current_buyer['client_type']) && $current_buyer['client_type'] ? $current_buyer['client_type'] : '';
                        ?>
                            <div data-repeater-item class="row kt-margin-b-10">
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <select class="form-control suggests_on_load_modal" name="client_id">
                                            <option value="">Select Buyer</option>
                                            <?php foreach ($all_buyers as $buyer) : ?>
                                                <option value="<?= $buyer['id'] ?>" <?php if ($buyer['id'] == $current_buyer_id) : ?>selected<?php endif; ?>>
                                                    <?= $buyer['first_name'] . ' ' . $buyer['last_name'] ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <?php echo form_dropdown('client_type', Dropdown::get_static('client_transaction_type'), set_value('client_type', @$current_client_type), 'class="form-control client_type"'); ?>
                                </div>

                                <div class="col-sm-2">
                                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                        <i class="la la-remove"></i>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php else : ?>
                        <?php for ($i = 0; $i < 1; $i++) { ?>
                            <div data-repeater-item class="row kt-margin-b-10">

                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <select class="form-control suggests" data-module="buyers" data-type="person" id="client_id" name="client_id">
                                            <option value="0">Select Buyer Name</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <?php echo form_dropdown('client_type', Dropdown::get_static('client_transaction_type'), set_value('client_type', @$client_type), 'class="form-control" id="client_type"'); ?>
                                </div>

                                <div class="col-sm-2">
                                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                        <i class="la la-remove"></i>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php endif; ?>
                </div>
            </div>
    </form>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>