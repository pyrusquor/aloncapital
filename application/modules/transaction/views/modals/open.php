
<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Transaction Opening Form</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	
	

	<form id="form_modal" action="<?=base_url();?>transaction/process_transaction" method="post">
		
		<div class="form-group">
			<input type="hidden" id="transaction_id" value="<?=$info['id'];?>" name="transaction_id">
			<input type="hidden" id="type" value="<?=$type;?>" name="type">
		</div>

		<div class="form-group">
			<label for="reason" class="form-control-label">Opening Reason:</label>
			<textarea class="form-control" id="reason" name="reason">
				
			</textarea>
		</div>

	</form>
	
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_buyer_information',$info,true); ?>
		</div>
	</div>	
	
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_account_information',$info,true); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php echo $this->load->view('view/_property_information',$info,true); ?>
		</div>
	</div>	

	
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>



