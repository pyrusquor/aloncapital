<?php

// vdebug($records);

?>

<div class="modal fade" id="add_personnel_modal" tabindex="-1" role="dialog"
    aria-labelledby="add_personnel_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Add Personnel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="addPersonnelForm" method="POST" method="post" action="<?php echo base_url("transaction/add_personnel")?>">
                    <input type="text" name="ap_transaction_id" class="form-control d-none" id="ap_transaction_id">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Personnel <span class="kt-font-danger"></span></label>
                                <select class="form-control" name="personnel_id" id="ap_personnel_id">
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Assigned At <span class="kt-font-danger"></span></label>
                                <input type="text" class="form-control kt_datepicker datePicker" name="assigned_at" id="ap_assigned_at"
                                    placeholder="Assigned At"
                                    autocomplete="off">
                            </div>
                        </div>

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="addPersonnelForm">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>