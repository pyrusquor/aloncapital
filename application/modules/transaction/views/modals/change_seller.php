<?php

$sellers = isset($info['sellers']) && $info['sellers'] ? $info['sellers'] : '';

$t_property = isset($info['t_property']) && $info['t_property'] ? $info['t_property'] : '';

$commissionable_type_id = isset($t_property['commissionable_type_id']) && $t_property['commissionable_type_id'] ? $t_property['commissionable_type_id'] : '';
$commissionable_amount = isset($t_property['commissionable_amount']) && $t_property['commissionable_amount'] ? $t_property['commissionable_amount'] : '';
$commissionable_amount_remarks = isset($t_property['commissionable_amount_remarks']) && $t_property['commissionable_amount_remarks'] ? $t_property['commissionable_amount_remarks'] : '';

$total_selling_price = $t_property['total_selling_price'] ?? 0;
$total_contract_price = $t_property['total_contract_price'] ?? 0;
$collectible_price = $t_property['collectible_price'] ?? 0;

?>

<script>
    var window_total_selling_price = "<?= $total_selling_price ?>"
    var window_total_contract_price = "<?= $total_contract_price ?>"
    var window_collectible_price = "<?= $collectible_price ?>"
</script>

<div class="modal-header">
    <h5 class="modal-title" id="changeSellerModalLabel">Change Seller Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body">
    <form id="form_modal">
        <input type="hidden" id="transactionID" value="<?= $info['id']; ?>" name="transaction_id">
        <input type="hidden" id="type" value="<?= $type; ?>" name="type">

        <div class="row">
            <div class="col-md-6">

                <a href="<?php echo base_url(); ?>seller/create" target="_blank" class="btn-light btn-sm btn-bold btn-upper btn-font-sm btn-custom btn-brand" style="padding: .5rem .5rem !important">Add Seller Record Here</a>

            </div>
        </div>

        <br>
        <br>

        <div class="row">
            <div class="col-md-12">

                <label>Sales Force Information : </label>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="">Commissionable Amount <span class="kt-font-danger">*</span></label>
                            <?php echo form_dropdown('property[commissionable_type_id]', Dropdown::get_static('commissionable_types'), set_value('commissionable_type_id', @$commissionable_type_id), 'class="form-control sellerComp" id="commissionable_type_id"'); ?>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="">&nbsp;</label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                                    <input type="text" class="form-control" id="commissionable_amount" placeholder="" name="property[commissionable_amount]" value="<?php echo set_value('property[commissionable_amount]"', @$commissionable_amount); ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="">Remarks</label>
                            <div class="kt-input-icon kt-input-icon">
                                <textarea rows="3" cols="50" class="form-control" name="property[commissionable_amount_remarks]" id="commissionable_amount_remarks"><?php echo @$commissionable_amount_remarks; ?>
                                </textarea>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="seller_form_repeater">
                    <div class="form-grouprow">
                        <label>Sales Force Information :
                            <div id="add_seller_btn" data-repeater-create="" class="btn btn-sm btn-primary">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add</span>
                                </span>
                            </div>
                        </label>

                        <?php if ($sellers) : ?>

                            <div data-repeater-list="seller" class="col-lg-12">

                                <?php foreach ($sellers as $key => $seller) {
                                    $CI = &get_instance();
                                    $seller_id = isset($seller['seller_id']) && $seller['seller_id'] ? $seller['seller_id'] : '';
                                    $seller_info = $CI->M_seller->get($seller_id);
                                    $seller_name = get_fname($seller_info);
                                    $seller_position_id = isset($seller['seller_position_id']) && $seller['seller_position_id'] ? $seller['seller_position_id'] : '';
                                    $sellers_rate = isset($seller['sellers_rate']) && $seller['sellers_rate'] ? $seller['sellers_rate'] : '';
                                    $sellers_rate_amount = isset($seller['sellers_rate_amount']) && $seller['sellers_rate_amount'] ? $seller['sellers_rate_amount'] : '';
                                    $commission_amount_rate = isset($seller['commission_amount_rate']) && $seller['commission_amount_rate'] ? $seller['commission_amount_rate'] : '';
                                ?>

                                    <div data-repeater-item class="row kt-margin-b-10 seller_row">
                                        <div class="col">

                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Seller's Name:</label>
                                            </div>

                                            <div class="input-group">
                                                <select class="form-control suggests_on_load_modal" name="seller_id">
                                                    <option value="">Select Seller</option>
                                                    <?php foreach ($all_sellers as $seller_item) : ?>
                                                        <option value="<?= $seller_item['id'] ?>" <?php if ($seller_item['id'] == $seller_id) : ?>selected<?php endif; ?>>
                                                            <?= $seller_item['first_name'] . ' ' . $seller_item['last_name'] ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Seller Position:</label>
                                            </div>
                                            <?php echo form_dropdown('seller_position_id', Dropdown::get_dynamic('seller_positions'), set_value('seller_position_id', @$seller_position_id), 'class="form-control sellerComp seller_position_id"'); ?>
                                        </div>

                                        <div class="col">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Seller's Percentage Rate:</label>
                                            </div>
                                            <input type="text" class="form-control sellerComp sellers_rate" name="sellers_rate" value="<?php echo $sellers_rate; ?>">
                                        </div>

                                        <div class="col">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Seller's Rate Amount:</label>
                                            </div>
                                            <input type="text" class="form-control sellers_rate_amount" name="sellers_rate_amount" readonly="" value="<?php echo $sellers_rate_amount; ?>">
                                        </div>

                                        <div class="col">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Advance Commission Amount:</label>
                                            </div>
                                            <input type="number" class="form-control sellerComp commission_amount_rate" name="commission_amount_rate" value="<?php echo $commission_amount_rate; ?>">
                                        </div>

                                        <div class="col-sm-1">
                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon mt-4">
                                                <i class="la la-remove"></i>
                                            </a>
                                        </div>

                                    </div>
                                <?php } ?>

                            </div>

                        <?php else : ?>
                            <?php for ($i = 0; $i < 1; $i++) { ?>
                                <div data-repeater-list="seller" class="col-lg-12">
                                    <div data-repeater-item class="row kt-margin-b-10 seller_row">
                                        <div class="col">

                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Seller's Name:</label>
                                            </div>

                                            <div class="input-group">
                                                <select class="form-control suggests sellerComp" data-module="sellers" data-parent="seller_row" data-needed="seller_position_id" data-target="seller_position_id" data-type="person" name="seller_id">
                                                    <option value="0">Select Seller Name</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Seller Position:</label>
                                            </div>
                                            <?php echo form_dropdown('seller_position_id', Dropdown::get_dynamic('seller_positions'), set_value('seller_position_id', @$seller_position_id), 'class="form-control sellerComp seller_position_id"'); ?>
                                        </div>

                                        <div class="col">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Seller's Percentage Rate:</label>
                                            </div>
                                            <input type="text" class="form-control sellerComp sellers_rate" name="sellers_rate" value="0">
                                        </div>

                                        <div class="col">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Seller's Rate Amount:</label>
                                            </div>
                                            <input type="text" class="form-control sellers_rate_amount" name="sellers_rate_amount" readonly="" value="0.00">
                                        </div>

                                        <div class="col">
                                            <div class="kt-form__label">
                                                <label class="kt-label m-label--single">Advance Commission Amount:</label>
                                            </div>
                                            <input type="number" class="form-control sellerComp commission_amount_rate" id="commission_amount_rate" name="commission_amount_rate" value="0">
                                        </div>

                                        <div class="col-sm-1">
                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                                <i class="la la-remove"></i>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            <?php } ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" data-action="action-submit">Submit</button>
</div>