<?php
$id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
$this->transaction_library->initiate($id);

$cp = money_php($info['t_property']['collectible_price']);
$penalty_type = Dropdown::get_static('penalty_types', $info['penalty_type'], 'view');
$adhoc_fee = isset($info['adhoc_fee']) && $info['adhoc_fee'] ? $info['adhoc_fee'] : 0;

$property_id = isset($info['property_id']) && $info['property_id'] ? $info['property_id'] : '';
$payee_type_id = isset($info['buyer']['id']) && $info['buyer']['id'] ? $info['buyer']['id'] : '';
$if_buyer = $this->ion_auth->is_buyer();
$pass['if_buyer'] = $if_buyer;

?>

<script type="text/javascript">
	window.property_id = '<?= $property_id; ?>'
	window.payee_type_id = '<?= $payee_type_id; ?>'
</script>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Transactions</h3>
		</div>
		<div class="kt-subheader__toolbar">
		<?php if(!$this->ion_auth->is_buyer()): ?>
			<div class="kt-subheader__wrapper">
				<a href="javascript:void(0);" class="btn btn-label-success btn-elevate btn-sm sync_aris_payments" data-id='<?= $id ?>'>
					<i class="fa fa-sync-alt"></i> Sync Aris Payments
				</a>
				<a href="<?php echo site_url('transaction_payment/form/' . $id); ?>" target="_BLANK" class="btn btn-label-info btn-elevate btn-sm <?php echo ($info['general_status'] == 4 ? "disabled" : ""); ?>">
					<i class="fa fa-money-bill"></i> Collect Payment
				</a>
				<a href="<?php echo site_url('transaction_payment/adhoc_fee/' . $id); ?>" target="_BLANK" class="btn btn-label-info btn-elevate btn-sm <?php echo ($info['general_status'] == 4 ? "disabled" : ""); ?>">
					<i class="fa fa-money-bill"></i> Collect Adhoc Fee
				</a>
			</div>
			<div class="kt-subheader__wrapper">

				<a href="javascript:void(0);" class="btn btn-label-info btn-elevate btn-sm" data-toggle="dropdown">
					<i class="fa fa-tasks"></i>Tasks
				</a>
				<div class="dropdown-menu dropdown-menu-right">
					<ul class="kt-nav">
						<li class="kt-nav__item">
							<a href="<?php echo base_url('ticketing/form/' . $id); ?>" class="kt-nav__link" target="_BLANk">
								<i class="kt-nav__link-icon fa fa-sticky-note"></i>
								<span class="kt-nav__link-text">Create Ticket</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="<?php echo base_url('transaction_post_dated_check/form/' . $id); ?>" class="kt-nav__link" target="_BLANk">
								<i class="kt-nav__link-icon fa fa-money-check"></i>
								<span class="kt-nav__link-text">Create PDC</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link create_arnote" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon-plus"></i>
								<span class="kt-nav__link-text">Create AR Note</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="<?php echo base_url('transaction/letter_request/' . $id); ?>" class="kt-nav__link letter_request">
								<i class="kt-nav__link-icon flaticon2-paper"></i>
								<span class="kt-nav__link-text">Create Letter Request</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="<?php echo base_url('transaction/past_due_notice/' . $id); ?>" class="kt-nav__link letter_request">
								<i class="kt-nav__link-icon flaticon2-paper"></i>
								<span class="kt-nav__link-text">Create Past Due Notice</span>
							</a>
						</li>

					</ul>
				</div>
			</div>

			<div class="kt-subheader__wrapper">
				<a href="javascript:void(0);" class="btn btn-label-info btn-elevate btn-sm" data-toggle="dropdown">
					<i class="flaticon2-document kt-font-brand"></i> Documents
				</a>
				<div class="dropdown-menu dropdown-menu-right">
					<ul class="kt-nav">
						<li class="kt-nav__item">
							<a href="<?php echo base_url('transaction/view_transaction_document_stage_log/' . $id); ?>" class="kt-nav__link">
								<i class="kt-nav__link-icon flaticon2-paper"></i>
								<span class="kt-nav__link-text">View General Documents Stage Logs</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="<?php echo base_url('transaction/view_transaction_document_loan_stage_log/' . $id); ?>" class="kt-nav__link">
								<i class="kt-nav__link-icon flaticon2-paper"></i>
								<span class="kt-nav__link-text">View Loan Documents Stage Logs</span>
							</a>
						</li>


						<li class="kt-nav__item">
							<a href="<?php echo base_url('transaction/process_checklist/' . $id); ?>" class="kt-nav__link process_checklist">
								<i class="kt-nav__link-icon flaticon2-paper"></i>
								<span class="kt-nav__link-text">Upload General Documents</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="<?php echo base_url('transaction/process_loan_checklist/' . $id); ?>" class="kt-nav__link process_checklist">
								<i class="kt-nav__link-icon flaticon2-paper"></i>
								<span class="kt-nav__link-text">Upload Loan Documents</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link update_transaction_stage" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon-file-1"></i>
								<span class="kt-nav__link-text">Update General Document Stage</span>
							</a>
						</li>
						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link update_transaction_loan_stage" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon-file-1"></i>
								<span class="kt-nav__link-text">Update Loan Document Stage</span>
							</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="kt-subheader__wrapper">

				<a href="javascript:void(0);" class="btn btn-label-info btn-elevate btn-sm" data-toggle="dropdown">
					<i class="flaticon-cogwheel-2 kt-font-brand"></i>Process
				</a>
				<div class="dropdown-menu dropdown-menu-right">
					<ul class="kt-nav <?php echo $info['general_status'] == 4 ? '' : 'hide'; ?>">
						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="open" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon2-checkmark"></i>
								<span class="kt-nav__link-text">Re-open Transaction</span>
							</a>
						</li>
						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="compute_refund" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon-cancel"></i>
								<span class="kt-nav__link-text">Compute Refund Amount</span>
							</a>
						</li>
					</ul>
					<ul class="kt-nav <?php echo $ul_readonly; ?>">
						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="interest_rates" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon2-percentage"></i>
								<span class="kt-nav__link-text">Change Loan Interest Rates</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="due_date" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon-event-calendar-symbol"></i>
								<span class="kt-nav__link-text">Change Billing Setting</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="financing_scheme" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon2-open-text-book"></i>
								<span class="kt-nav__link-text">Change Financing Scheme</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="change_property" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon fa fa-hotel"></i>
								<span class="kt-nav__link-text">Change Property Information</span>
							</a>
						</li>


						<li class="kt-nav__item">
							<a href="javascript: void(0)" id="adhoc_modal_btn" class="kt-nav__link process_transaction" data-type="adhoc_fee" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon2-open-text-book"></i>
								<span class="kt-nav__link-text">Add Adhoc Fee</span>
							</a>
						</li>

						<li class="kt-nav__item">
							<a href="javascript: void(0)" id="discount_modal_btn" class="kt-nav__link process_transaction" data-type="discount" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon-coins"></i>
								<span class="kt-nav__link-text">Add Discount</span>
							</a>
						</li>

						<?php if ($period === 'Loan') : ?>
							<li class="kt-nav__item">
								<a href="javascript: void(0)" id="discount_modal_btn" class="kt-nav__link process_transaction" data-type="move_in" data-id="<?php echo $id; ?>">
									<i class="kt-nav__link-icon flaticon-buildings"></i>
									<span class="kt-nav__link-text">Move In</span>
								</a>
							</li>
						<?php endif; ?>

						<li class="kt-nav__item">
							<a href="javascript: void(0)" class="kt-nav__link process_transaction" data-type="confirm" data-id="<?php echo $id; ?>">
								<i class="kt-nav__link-icon flaticon-cancel"></i>
								<span class="kt-nav__link-text">Cancel Transaction</span>
							</a>
						</li>

					</ul>
				</div>
			</div>
			

			<a href="<?php echo site_url('transaction/form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
				<i class="fa fa-edit"></i> Edit
			</a>
			
		<?php endif ?>
			<a href="<?php echo base_url('transaction?transaction_id='.$id); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
				<i class="fa fa-reply"></i> Back
			</a>
		</div>
	</div>
</div>

<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="transaction_view" data-ktwizard-state="step-first">
	<div class="kt-portlet">
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-grid__item">

				<!--begin: Form Wizard Nav -->
				<div class="kt-wizard-v3__nav">
					<div class="kt-wizard-v3__nav-items">
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									General Information
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-id="<?php echo $id; ?>" data-cp="<?php echo $cp; ?>" data-pt="<?php echo $penalty_type; ?>" data-af="<?php echo $adhoc_fee; ?>" id="mortgage_schedule_nav">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Mortgage Schedule
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Adhoc Fees
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Collections
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<?php if(!$if_buyer): ?>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Post Dated Checks
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>

						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Commission Schedule
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>

						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Printable Forms
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Tickets and Concerns
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
							<div class="kt-wizard-v3__nav-body">
								<div class="kt-wizard-v3__nav-label">
									Accounting Entries
								</div>
								<div class="kt-wizard-v3__nav-bar"></div>
							</div>
						</a>
						<?php endif ?>
					</div>
				</div>
				<!--end: Form Wizard Nav -->
			</div>
		</div>
	</div>

	<div class="kt-grid__item kt-grid__item--fluid --kt-wizard-v3__wrapper">

		<!--begin: Form Wizard Step 1-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
			<?php $this->load->view('view/_general_information',$pass); ?>
		</div>
		<!--end: Form Wizard Step 1-->

		<!--begin: Form Wizard Step 2-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" id="mortgage_schedule_view">
		</div>
		<!--end: Form Wizard Step 2-->

		<!--begin: Form Wizard Step 3-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_adhoc_fee_information'); ?>
		</div>
		<!--end: Form Wizard Step 3-->

		<!--begin: Form Wizard Step 4-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_collection_information'); ?>
		</div>
		<!--end: Form Wizard Step 4-->
		<?php if(!$if_buyer): ?>
		<!--begin: Form Wizard Step 5-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_post_dated_check_information'); ?>
		</div>
		<!--end: Form Wizard Step 5-->

		<!--begin: Form Wizard Step 6-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_commission_schedule_information'); ?>
		</div>
		<!--end: Form Wizard Step 6-->

		<!--begin: Form Wizard Step 7-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_document_information'); ?>
		</div>
		<!--end: Form Wizard Step 7-->

		<!--begin: Form Wizard Step 8-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_tickets_information'); ?>
		</div>
		<!--end: Form Wizard Step 8-->

		<!--begin: Form Wizard Step 9-->
		<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
			<?php $this->load->view('view/_accounting_entries_information'); ?>
		</div>
		<!--end: Form Wizard Step 9-->
		<?php endif ?>

		<?php if (FALSE) : ?>

			<!--begin: Form Actions -->
			<div class="kt-form__actions">
				<div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
					Previous
				</div>
				<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
					Submit
				</div>
				<div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
					Next Step
				</div>
			</div>
			<!--end: Form Actions -->
		<?php endif; ?>
	</div>
</div>
<?php $this->load->view('modals/create_ar_note'); ?>
<?php $this->load->view('modals/update_transaction_stage'); ?>
<?php $this->load->view('modals/update_transaction_loan_stage'); ?>