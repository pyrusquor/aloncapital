<?php
if($reservation){
    $buyers = $reservation['buyer'] ? [$reservation['buyer']] : [] ;
}else{
    $buyers = isset($info['buyers']) && $info['buyers'] ? $info['buyers'] : '';
}
?>


<div id="buyer_form_repeater">
    <div class="form-grouprow">
        <label>Buyer Information : 
            <div data-repeater-create="" class="btn btn-sm btn-primary">
                <span>
                    <i class="la la-plus"></i>
                    <span>Add</span>
                </span>
            </div>
        </label>
    

        <?php if ($buyers): ?>

            <?php foreach ($buyers as $key => $buyer) { 
                    if($reservation){
                        $buyer_id = $buyer['id'];
                        $buyer_name = $buyer['last_name'] . ' ' . $buyer['first_name'];
                    }else{
                        $CI = &get_instance();
                        $buyer_id = isset($buyer['client_id']) && $buyer['client_id'] ? $buyer['client_id'] : '';
                        $buyer_info = $CI->M_buyer->get($buyer_id);
                        $buyer_name = get_fname($buyer_info);
                        $client_type = isset($buyer['client_type']) && $buyer['client_type'] ? $buyer['client_type'] : '';
                    }

                ?>
                 <div data-repeater-list="client" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10">
                        
                        <div class="col-sm-3">
                            <div class="input-group">
                                <select class="form-control suggests" data-module="buyers" data-type="person" id="client_id" name="client_id">
                                    <?php if ($buyer_id): ?>
                                        <option value="<?php echo $buyer_id; ?>">
                                            <?php echo @$buyer_name; ?>
                                        </option>
                                    <?php else: ?>
                                        <option value="0">Select Buyer Name</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <?php echo form_dropdown('client_type', Dropdown::get_static('client_transaction_type'), set_value('client_type', @$client_type), 'class="form-control" id="client_type"'); ?>
                        </div>

                        <div class="col-sm-2">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                <i class="la la-remove"></i>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>

        <?php else : ?>
            <?php for ($i=0; $i < 1; $i++) { ?>
                <div data-repeater-list="client" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10">
                        
                        <div class="col-sm-3">
                            <div class="input-group">
                                <select class="form-control suggests" data-module="buyers" data-type="person" id="client_id" name="client_id">
                                    <option value="0">Select Buyer Name</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <?php echo form_dropdown('client_type', Dropdown::get_static('client_transaction_type'), set_value('client_type', @$client_type), 'class="form-control" id="client_type"'); ?>
                        </div>

                        <div class="col-sm-2">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                <i class="la la-remove"></i>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php endif; ?>
    </div>
</div>