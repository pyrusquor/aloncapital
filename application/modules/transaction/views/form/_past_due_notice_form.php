<div class="kt-wizard-v3__content">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Past Due Date <span class="kt-font-danger">*</span></label>
                <input type="text" class="form-control kt_datepicker datePicker" name="past_due_date" value="<?php echo set_value('past_due_date', @$past_due_date); ?>" placeholder="Past Due Date" autocomplete="off" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="">Notice <span class="kt-font-danger">*</span></label>
                <?php echo form_dropdown('notice', Dropdown::get_static("past_due_notice_type"), @$notice, 'class="form-control" required') ?>
            </div>
        </div>
    </div>
</div>