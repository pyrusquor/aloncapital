<div id="promos_form_repeater">
    <div class="form-grouprow">
        <label>Promos & Discounts : 
            <div data-repeater-create="" class="btn btn btn-primary">
                <span>
                    <i class="la la-plus"></i>
                    <span>Add</span>
                </span>
            </div>
        </label>

        <div data-repeater-list="promos" class="col-lg-12">
            <div data-repeater-item class="row kt-margin-b-10">

                <div class="col-sm-4">
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                        <input type="text" class="form-control promoPrice" id="promo_amount" placeholder="Amount" name="promo_amount" value="<?php echo set_value('promos[price]"', @$promo_amount); ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="input-group">
                        <select class="form-control promoID" id="discount_id" name="discount_id">
                            <option value="0">Select Option</option>
                            <option value="1">Deductable to Loan</option>
                            <option value="2" selected>Deductable to TCP</option>
                            <!-- <?php if ($promos): ?>
                                <?php foreach ($promos as $key => $promo) { ?>
                                    <option data-value="<?php echo $promo['value']; ?>" data-value-type="<?php echo $promo['value_type']; ?>" data-promo-type="<?php echo $promo['promo_type']; ?>" data-deduct-to="<?php echo $promo['deduct_to']; ?>" value="<?php echo $promo['id']; ?>"><?php echo $promo['name']; ?></option>
                                <?php } ?>
                            <?php endif ?> -->
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" class="form-control" id="discount_remarks" placeholder="Remarks" name="discount_remarks" value="<?php echo set_value('promos[remarks]"', @$discount_remarks); ?>">
                        
                        <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon ml-2">
                            <i class="la la-remove"></i>
                        </a>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>