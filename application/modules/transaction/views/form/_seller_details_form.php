<?php
if ($reservation) {
    $sellers = $reservation['seller'] ? [$reservation['seller']] : [];
} else {
    $sellers = isset($info['sellers']) && $info['sellers'] ? $info['sellers'] : '';
}
?>

<div id="seller_form_repeater">
    <div class="form-grouprow">
        <label>Sales Force Information :
            <div id="add_seller_btn" data-repeater-create="" class="btn btn-sm btn-primary">
                <span>
                    <i class="la la-plus"></i>
                    <span>Add</span>
                </span>
            </div>
        </label>

        <?php if ($sellers) : ?>


            <?php foreach ($sellers as $key => $seller) {
                if ($reservation) {
                    $seller_id = $seller['id'];
                    $seller_name = $seller['last_name'] . ' ' . $seller['first_name'];
                    $seller_position_id = $seller['seller_position_id'];
                } else {
                    $CI = &get_instance();
                    $seller_id = isset($seller['seller_id']) && $seller['seller_id'] ? $seller['seller_id'] : '';
                    $seller_info = $CI->M_seller->get($seller_id);
                    $seller_name = get_fname($seller_info);
                    $seller_position_id = isset($seller['seller_position_id']) && $seller['seller_position_id'] ? $seller['seller_position_id'] : '';
                    $sellers_rate = isset($seller['sellers_rate']) && $seller['sellers_rate'] ? $seller['sellers_rate'] : '';
                    $sellers_rate_amount = isset($seller['sellers_rate_amount']) && $seller['sellers_rate_amount'] ? $seller['sellers_rate_amount'] : '';
                }



            ?>
                <div data-repeater-list="seller" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10 seller_row">
                        <div class="col">

                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Name:</label>
                            </div>

                            <div class="input-group">
                                <select class="form-control suggests sellerComp" data-module="sellers" data-parent="seller_row" data-needed="seller_position_id" data-target="seller_position_id" data-type="person" id="seller_id" name="seller_id">
                                    <?php if ($seller_id) : ?>
                                        <option value="<?php echo $seller_id; ?>">
                                            <?php echo @$seller_name; ?>
                                        </option>
                                    <?php else : ?>
                                        <option value="0">Select Seller Name</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller Position:</label>
                            </div>
                            <?php echo form_dropdown('seller_position_id', Dropdown::get_dynamic('seller_positions'), set_value('seller_position_id', @$seller_position_id), 'class="form-control sellerComp seller_position_id" id="seller_position_id"'); ?>
                        </div>

                        <div class="col">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Percentage Rate:</label>
                            </div>
                            <input type="text" class="form-control sellerComp sellers_rate" id="sellers_rate" name="sellers_rate" value="<?php echo $sellers_rate; ?>">
                        </div>

                        <div class="col">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Rate Amount:</label>
                            </div>
                            <input type="text" class="form-control sellers_rate_amount" id="sellers_rate_amount" name="sellers_rate_amount" readonly="" value="<?php echo $sellers_rate_amount; ?>">
                        </div>

                        <div class="col">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Advance Commission Amount:</label>
                            </div>
                            <input type="number" class="form-control sellerComp commission_amount_rate" id="commission_amount_rate" name="commission_amount_rate" value="<?php echo $commission_amount_rate; ?>">
                        </div>

                        <div class="col-sm-1">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                <i class="la la-remove"></i>
                            </a>
                        </div>

                    </div>
                </div>
            <?php } ?>

        <?php else : ?>
            <?php for ($i = 0; $i < 1; $i++) { ?>
                <div data-repeater-list="seller" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10 seller_row">
                        <div class="col">

                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Name:</label>
                            </div>

                            <div class="input-group">
                                <select class="form-control suggests sellerComp" data-module="sellers" data-parent="seller_row" data-needed="seller_position_id" data-target="seller_position_id" data-type="person" id="seller_id" name="seller_id">
                                    <option value="0">Select Seller Name</option>
                                </select>
                            </div>
                        </div>

                        <div class="col">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller Position:</label>
                            </div>
                            <?php echo form_dropdown('seller_position_id', Dropdown::get_dynamic('seller_positions'), set_value('seller_position_id', @$seller_position_id), 'class="form-control sellerComp seller_position_id" id="seller_position_id"'); ?>
                        </div>

                        <div class="col">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Percentage Rate:</label>
                            </div>
                            <input type="text" class="form-control sellerComp sellers_rate" id="sellers_rate" name="sellers_rate" value="0">
                        </div>

                        <div class="col">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Rate Amount:</label>
                            </div>
                            <input type="text" class="form-control sellers_rate_amount" id="sellers_rate_amount" name="sellers_rate_amount" readonly="" value="0.00">
                        </div>

                        <div class="col">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Advance Commission Amount:</label>
                            </div>
                            <input type="number" class="form-control sellerComp commission_amount_rate" id="commission_amount_rate" name="commission_amount_rate" value="0">
                        </div>

                        <div class="col-sm-1">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                <i class="la la-remove"></i>
                            </a>
                        </div>

                    </div>
                </div>
            <?php } ?>
        <?php endif; ?>

    </div>
</div>