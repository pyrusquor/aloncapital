<?php
    $fees =  isset($fees) && $fees ? $fees : '';
    $selected_fees =  isset($info['fees']) && $info['fees'] ? $info['fees'] : '';
    if ($selected_fees) { $default_fees = $selected_fees; }

    // vdebug($fees);
    // vdebug($default_fees);
?>
<div id="fees_form_repeater">
    <div class="form-grouprow">
        <label>Additional Fees :
            <div data-repeater-create="" class="btn btn btn-primary">
                <span>
                    <i class="la la-plus"></i>
                    <span>Add</span>
                </span>
            </div>
        </label>

        <div data-repeater-list="fees" class="col-lg-12">
            <?php if( ($default_fees) && (isset($default_fees)) ){ ?>
                <?php foreach ($default_fees as $key => $default) { ?>
                <div data-repeater-item="" class="row kt-margin-b-10">
                    <div class="col-sm-5">
                        <div class="input-group">
                            <select class="form-control feesID" id="fees_id" name="fees[<?php echo $key; ?>][fees_id]">
                                <option value="0">Select Fees</option>
                                <?php if ($fees): ?>
                                    <?php foreach ($fees as $k => $fee) { $f_id = $default['id'];  ?>
                                        <option <?php if($fee['id'] == $f_id){ echo "selected"; };?> data-value="<?php echo $fee['value']; ?>" data-value-type="<?php echo $fee['value_type']; ?>" data-fees-type="<?php echo $fee['fees_type']; ?>" data-add-to="<?php echo $fee['add_to']; ?>" value="<?php echo $fee['id']; ?>">
                                            <?php echo $fee['name']; ?>
                                        </option>
                                    <?php } ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control feesPrice" id="fees_amount_rate" placeholder="Fee Amount" name="fees[<?php echo $key; ?>][fees_amount_rate]" value="<?php echo @$default['fees_amount_rate'] ?>">
                        </div>
                        <span class="form-text text-muted">auto-generated - Formula = <span class="formula"></span> </span>
                    </div>

                    <div class="col-sm-2">
                        <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                            <i class="la la-remove"></i>
                        </a>
                    </div>
                </div>

                <?php } ?>
            <?php } else { ?>
                <div data-repeater-item class="row kt-margin-b-10">
                    <div class="col-sm-5">
                        <div class="input-group">
                            <select class="form-control feesID" id="fees_id" name="fees_id">
                                <option value="0">Select Fees</option>
                                <?php if ($fees): ?>
                                    <?php foreach ($fees as $key => $fee) { ?>
                                        <option data-value="<?php echo $fee['value']; ?>" data-value-type="<?php echo $fee['value_type']; ?>" data-fees-type="<?php echo $fee['fees_type']; ?>" data-add-to="<?php echo $fee['add_to']; ?>" value="<?php echo $fee['id']; ?>"><?php echo $fee['name']; ?></option>
                                    <?php } ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control feesPrice" id="fees_amount_rate" placeholder="Fee Amount" name="fees_amount_rate" value="<?php echo set_value('fees[price]"', @$fees_amount_rate); ?>">
                        </div>
                        <span class="form-text text-muted">auto-generated - Formula = <span class="formula"></span> </span>
                    </div>

                    <div class="col-sm-2">
                        <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                            <i class="la la-remove"></i>
                        </a>
                    </div>
                </div> 
            <?php } ?>
        </div>
    </div>
</div>

<!-- 
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Total Misc Fees <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control" id="total_misc_fees" placeholder="Collectible Price" name="property[total_misc_fees]" value="<?php //echo set_value('property[total_misc_fees]"', $total_misc_fees); ?>" readonly>
                </div>
            </div>
            <span class="form-text text-muted">auto-generated - Sum of all fees </span>
        </div>
    </div>
</div> -->