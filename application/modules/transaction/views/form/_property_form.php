<?php
if($reservation){
    $project_id = $reservation['property']['project_id'];
    $property_name = $reservation['property']['name'];
    $property_id = $reservation['property']['id'];
    $lot_area = $reservation['property']['lot_area'];
    $floor_area = $reservation['property']['floor_area'];
    $lot_price_per_sqm = $reservation['property']['lot_price_per_sqm'];
    $total_lot_price = $reservation['property']['total_lot_price'];
    $package_type_id = $reservation['property']['package_type_id'];
    $total_selling_price = $reservation['property']['total_selling_price'];
    $total_selling_price = $reservation['property']['total_selling_price'];

    // vdebug($reservation);
} else{
    $p_id = "readonly";
    $id = isset($info['id']) && $info['id'] ? $info['id'] : '';
    $project = isset($info['project']) && $info['project'] ? $info['project'] : '';
    $project_name = isset($project['name']) && $project['name'] ? $project['name'] : '';
    $project_id = isset($project['id']) && $project['id'] ? $project['id'] : '';

    $property = isset($info['property']) && $info['property'] ? $info['property'] : '';
    $property_name = isset($property['name']) && $property['name'] ? $property['name'] : '';
    $property_id = isset($property['id']) && $property['id'] ? $property['id'] : '';
    $promo_amount = isset($property['promo_amount']) && $property['promo_amount'] ? $property['promo_amount'] : 0;


    $t_property = isset($info['t_property']) && $info['t_property'] ? $info['t_property'] : '';
    $t_property_id = isset($t_property['id']) && $t_property['id'] ? $t_property['id'] : '';
    
    $house_model_id = isset($t_property['house_model_id']) && $t_property['house_model_id'] ? $t_property['house_model_id'] : '';
    $house_model = get_value_field($house_model_id,'house_models','name');

    $house_model_interior_id = isset($t_property['house_model_interior_id']) && $t_property['house_model_interior_id'] ? $t_property['house_model_interior_id'] : '';
    $house_model_interior = get_value_field($house_model_interior_id,'house_model_interiors','name');


    $floor_area = isset($t_property['floor_area']) && $t_property['floor_area'] ? $t_property['floor_area'] : '';
    $lot_area = isset($t_property['lot_area']) && $t_property['lot_area'] ? $t_property['lot_area'] : '';
    $total_lot_price = isset($t_property['total_lot_price']) && $t_property['total_lot_price'] ? $t_property['total_lot_price'] : '';
    $lot_price_per_sqm = isset($t_property['lot_price_per_sqm']) && $t_property['lot_price_per_sqm'] ? $t_property['lot_price_per_sqm'] : '';
    $total_house_price = isset($t_property['total_house_price']) && $t_property['total_house_price'] ? $t_property['total_house_price'] : '';

    $package_type_id = isset($t_property['package_type_id']) && $t_property['package_type_id'] ? $t_property['package_type_id'] : '';

    $total_selling_price = isset($t_property['total_selling_price']) && $t_property['total_selling_price'] ? $t_property['total_selling_price'] : '';
    $adjusted_price = isset($t_property['adjusted_price']) && $t_property['adjusted_price'] ? $t_property['adjusted_price'] : '';

    $remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';

    $property_types = isset($house['sub_type_id']) && $house['sub_type_id'] ? $house['sub_type_id'] : '';

    if ( isset($type) && ($type == "property")) {
        # code...
        $p_id = "";
    }
}


?>
<input type="hidden" name="property[transaction_property_id]" value="<?=$t_property_id;?>">

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="">Reservation Agreement Number <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" id="ra_number" name="ra_number" placeholder="Reservation Agreement Number">
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="">Project Name <span class="kt-font-danger">*</span></label>
            <select class="form-control kt-select2" id="project_id" name="property[project_id]" <?php //echo ($this->router->fetch_method() === 'form' && !empty($this->uri->segment(3)) ) ? 'disabled' : ''; ?>>
                <?php foreach ($projects as $p): ?>
                        <option value="<?php echo $p['id'] ?>" <?php echo ($project_id == $p['id']) ? 'selected' : '' ?>>
                            <?php echo ucwords($p['name']); ?>
                        </option>
                <?php endforeach;?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="">Property Name <span class="kt-font-danger">*</span></label>
            <select class="form-control" id="property_id" name="property[property_id]">
                <option value="">Select Property</option>
                <?php if ($property_name): ?>
                    <option value="<?php echo $property_id; ?>" selected><?php echo $property_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Property Types <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[property_types]', Dropdown::get_static('property_types'), set_value('info[property_types]', @$property_types), 'class="form-control" id="property_types"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">House Model <?php echo $house_model_id; ?><span class="kt-font-danger">*</span></label>
            <select class="form-control popField" id="house_model_id" name="info[model_id]">
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Lot Type / Orientiation <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests popField" data-module="house_model_interiors" data-param="house_model_id" id="interior_id" name="property[house_model_interior_id]">
                <option value="">Select Lot Type / Orientiation</option>
                <?php if ($house_model_interior): ?>
                    <option value="<?php echo $house_model_interior_id; ?>" selected><?php echo $house_model_interior; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Lot Area <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <input type="text" class="form-control popField" id="lot_area" placeholder="Lot Area" name="property[lot_area]" data-original="<?=$lot_area;?>"  value="<?php echo set_value('property[lot_area]"', $lot_area); ?>" <?=$p_id;?>>

                    <input type="hidden" class="popField" name="is_lot" value="" id="is_lot">
                    <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                </div>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Floor Area <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <input type="text" class="form-control popField" id="floor_area" placeholder="Floor Area" name="property[floor_area]" data-original="<?=$floor_area;?>" value="<?php echo set_value('property[floor_area]"', $floor_area); ?>" <?=$p_id;?>>
                    <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                </div>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Lot Price / SQM<span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control popField" id="lot_price_per_sqm" placeholder="Lot Price / SQM" name="property[lot_price_per_sqm]" value="<?php echo set_value('property[lot_price_per_sqm]"', $lot_price_per_sqm); ?>" readonly>
                </div>
            </div>
            <span class="form-text text-muted">price in this field is fetch from project base lot of the chosen property</span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Total Lot Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control popField" id="total_lot_price" placeholder="Total Lot Price" name="property[total_lot_price]" value="<?php echo set_value('property[total_lot_price]"', $total_lot_price); ?>" readonly>
                </div>
            </div>
            <span class="form-text text-muted">auto-generated - Formula = Lot Price per SQM * Lot Area</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Package Type </label>
            <div class="kt-input-icon kt-input-icon--left">
                <?php echo form_dropdown('property[package_type_id]', Dropdown::get_static('package_types'), set_value('property[package_type_id]', @$package_type_id), 'class="form-control popField" id="package_type_id"'); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Property Model Price </label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control popField" placeholder="Property Model Price"  id="total_house_price" name="property[total_house_price]" value="<?php echo set_value('property[total_house_price]"', $total_house_price); ?>" readonly>
                </div>
            </div>
            <span class="form-text text-muted">auto-generated - price in this field is fetch from house interior latest price</span>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-sm-4">
        <div class="form-group">
            <label>Adjusted Price <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon kt-input-icon--left">
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                        <input type="number" class="form-control compute" id="adjusted_price" placeholder="Adjusted Price" name="info[adjusted_price]" value="<?php echo set_value('info[adjusted_price]"', @$adjusted_price); ?>" readonly>
                    </div>
                </div>
            <span class="form-text text-muted">Adjusted Price = New Property Price - Old Property Price</span>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Total Selling Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                    <input type="text" class="form-control popField" id="total_selling_price" placeholder="Total Selling Price" name="property[total_selling_price]" value="<?php echo set_value('property[total_selling_price]"', $total_selling_price); ?>" readonly>
                </div>
            </div>
            <span class="form-text text-muted">auto-generated - Formula = Total Lot Price + Property Model Price</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Witholding Tax Rate: <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('wht_rate', Dropdown::get_static('tax_rates'), set_value('wht_rate', @$wht_rate), 'class="form-control" id="wht_rate"'); ?>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Tax Description:</label>
            <textarea id="tax_description" class="form-control" cols="30" rows="3"></textarea>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Witholding Tax Amount:</label>
            <input type="number" readonly="" class="form-control" id="wht_amount" name="wht_amount">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
            <div class="form-group">
                <label>VAT Type: <span class="kt-font-danger">*</span> </label>
                <select name="is_inclusive" id="is_inclusive" class="form-control">
                    <option value="0">Select VAT Type</option>
                    <option value="1">Exclusive</option>
                    <option value="2">Inclusive</option>
                </select>
                    <!-- <input type="checkbox" name="is_inclusive" id="is_inclusive" checked=""> -->
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>VAT Amount <span class="kt-font-danger">*</span> </label>
            <div class="kt-input-icon kt-input-icon--left">

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">PHP</span>
                    </div>
                    <input type="text" readonly="" class="form-control" id="vat_amount" name="vat_amount">
                </div>
            </div>
        </div>
    </div>
    
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Remarks </label>
            <textarea class="form-control" id="remarks" name="info[remarks]">
                <?php echo set_value('info[remarks]"', $remarks); ?>
            </textarea>
        </div>
    </div>
</div>