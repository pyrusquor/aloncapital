<?php

$image = "";
$subject_id = 0;
?>

<div class="kt-wizard-v3__content">
    <div class="row">

        <div class="col-sm-6 hide">
            <div class="form-group">
                <label>Transaction ID <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon kt-input-icon--left">
                    <div class="input-group">
                        <input type="text" class="form-control" id="transaction_id"
                            value="<?php echo $transaction_id ?>" placeholder="Total Contract Price"
                            name="transaction_id">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Request Date <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon kt-input-icon--left">
                    <div class="input-group">
                        <input type="text" class="form-control kt_datepicker datePicker" name="request_date"
                            value="<?php echo set_value('request_date', @$request_date); ?>" placeholder="Request Date"
                            autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Subject <span class="kt-font-danger">*</span></label>
                <div class="kt-input-icon kt-input-icon--left">
                    <div class="input-group">
                        <select class="form-control suggests" data-module="letter_request_subjects" id="subject"
                            name="subject">
                            <?php if ($subject_id): ?>
                            <option value="<?php echo $subject_id; ?>">
                                <?php echo @$subject_name; ?>
                            </option>
                            <?php else: ?>
                            <option value="0">Select Subject</option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Request Body <span class="kt-font-danger">*</span></label>
                <textarea id="request_body" name="request_body" class="form-control" cols="30" rows="3"></textarea>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Upload Scanned Copy of the Request:</label>
                <div>
                    <?php if (get_image('transaction_letter_request', 'image', $image)): ?>
                    <img class="kt-widget__img"
                        src="<?php echo base_url(get_image('transaction_letter_request', 'image', $image)); ?>"
                        width="90px" height="90px" />
                    <?php endif; ?>
                    <span class="btn btn-sm">
                        <input type="file" name="scanned_file" class="" aria-invalid="false">
                    </span>
                </div>
                <span class="form-text text-muted"></span>
            </div>
        </div>
    </div>
</div>