<?php

// vdebug($_document_stage_log);

?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Transaction Document Stage Log</h3>
            <?php if ( isset($_total) && $_total ): ?>

            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="_total"><?php echo $_total;?> TOTAL</span>
            <?php endif; ?>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
                <input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
                <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                            class="la la-search"></i></span></span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="button" id="_advance_search_btn"
                    class="btn btn-label-primary btn-elevate btn-icon-sm btn-sm" data-toggle="collapse"
                    data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button>
                <a href="<?php echo site_url('transaction');?>" class="btn btn-label-primary btn-elevate btn-icon-sm btn-sm">
                    <i class="fa fa-reply"></i> Cancel
                </a>
            </div>
        </div>
    </div>
</div>


<div class="kt-portlet kt-portlet--mobile">

    <div class="kt-portlet__body">
        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Previous Stage</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control suggests" data-module="transaction_document_stages" id="_column_3" data-column="3"  name="td_previous_stage">
                                    <option value="">Select Stages</option>
                                </select>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-book"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">New  Stage</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <select class="form-control suggests" data-module="transaction_document_stages" id="_column_4" data-column="4"  name="td_new_stage">
                                    <option value="">Select Stages</option>
                                </select>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-book"></i></span></span>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-condensed table-checkable"
            id="_transaction_document_stage_log_table">
            <thead>
                <tr class="text-center">
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Transaction Reference</th>
                    <th>Previous Stage</th>
                    <th>New Stage</th>
                </tr>
            </thead>
            <?php if ( isset($_document_stage_log) && $_document_stage_log ): ?>

            <tbody>
                <?php foreach ( $_document_stage_log as $key => $td_stage_log ): //vdebug($td_stage_log) ?>

                <?php
                            $_id										=	isset($td_stage_log['id']) && $td_stage_log['id'] ? $td_stage_log['id'] : '';
                        
                            $_transaction_id										=	isset($td_stage_log['transaction_id']) && $td_stage_log['transaction_id'] ? $td_stage_log['transaction_id'] : '';

                            $_previous_id										=	isset($td_stage_log['previous_id']) && $td_stage_log['previous_id'] ? $td_stage_log['previous_id'] : 'N/A';

                            $prev_stage = get_person($_previous_id, 'transaction_document_stages');

                            $prev_stage_name = get_name($prev_stage);

                            $_new_id										=	isset($td_stage_log['new_id']) && $td_stage_log['new_id'] ? $td_stage_log['new_id'] : 'N/A';

                            $new_stage = get_person($_new_id, 'transaction_document_stages');

                            $new_stage_name = get_name($new_stage);
						?>

                <tr>
                    <td>
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" name="id[]" value="<?php echo @$_id;?>" class="m-checkable _select"
                                data-id="<?php echo @$_id;?>">
                            <span></span>
                        </label>
                    </td>
                    <td><?php echo @$_id;?></td>
                    <td>
                        <?php echo @$transaction_reference; ?>
                    </td>
                    <td>
                        <?php echo @$prev_stage_name;?>
                    </td>
                    <td>
                        <?php echo @$new_stage_name;?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <?php endif; ?>
        </table>
        <!--end: Datatable -->
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4 offset-lg-1">
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Field
                                <span></span>
                            </label>
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Name
                                <span></span>
                            </label>
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Description
                                <span></span>
                            </label>
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Owner
                                <span></span>
                            </label>
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Classification
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm">
                    <i class="fa fa-file-export"></i> Export
                </button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->

<!-- Start Upload Modal-->
<div class="modal fade" id="_upload_modal" tabindex="-1" role="dialog" aria-labelledby="_upload_modal_label"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_upload_modal_label">Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" method="POST" id="_upload_form" enctype="multipart/form-data">
                    <div class="form-group row text-center">
                        <div class="col-lg-12 col-xl-12">
                            <div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar">
                                <div class="kt-avatar__holder"
                                    style="background-image: url(<?php echo base_url('assets/img/default/_img.png');?>);">
                                </div>
                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                    data-original-title="Change avatar">
                                    <i class="fa fa-pen"></i>
                                    <!-- <input type="file" id="process_checklist_file" name="process_image" accept=".png, .jpg, .jpeg"> -->
                                    <input type="file" class="process_checklist_file" id="process_checklist_file"
                                        name="_file" accept=".png, .jpg, .jpeg">
                                </label>
                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                    data-original-title="Cancel avatar">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left"
                        data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right"
                        form="_upload_form">
                        <i class="fa fa-plus-circle"></i> Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Upload Modal-->

<!-- Start Upload Modal-->
<div class="modal fade" id="_view_file_modal" tabindex="-1" role="dialog" aria-labelledby="_view_file_modal_label"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_view_file_modal_label">View</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="_display_process_document_image"></div>
                <!-- <div class="form-group row text-center">
					<div class="col-lg-12 col-xl-12">
						<div class="kt-avatar kt-avatar--outline kt-avatar--circle-">
							<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/img/default/_img.png');?>);"></div>
						</div>
					</div>
				</div> -->
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left"
                        data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <!-- <button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_upload_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Upload Modal-->