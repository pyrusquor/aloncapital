<?php
// Billing Information
$financing_scheme_id = isset($info['financing_scheme_id']) && $info['financing_scheme_id'] ? $info['financing_scheme_id'] : '';
$reservation_date = isset($info['reservation_date']) && $info['reservation_date'] ? date('Y-m-d', strtotime($info['reservation_date'])) : '';
$expiration_date = isset($info['expiration_date']) && $info['expiration_date'] ? $info['expiration_date'] : '';
$billings = isset($info['billings']) && $info['billings'] ? $info['billings'] : '';

sort($schemes);

$scheme_id = $info['scheme']['id'];
$scheme_type = Dropdown::get_static('scheme_types', $info['scheme']['scheme_type']);
$scheme_name = $info['scheme']['name'];
$scheme_info = $scheme_name . " - " . $scheme_type;

?>



<div class="row">

    <div class="col-md-6">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Reservation Date <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="reservation_date" placeholder="Reservation Date" name="billing[reservation_date]" value="<?php echo $reservation_date ? set_value('billing[reservation_date]"', $reservation_date) : date("Y-m-d") ?>">
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-6">

        <div class="row">
            <div class="col-sm-12 d-none">
                <label class="">Expiration Date <span class="kt-font-danger">*</span></label>
                <div class="form-group">
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="expiration_date" placeholder="Expiration Date" name="billing[expiration_date]" value="<?php echo set_value('billing[expiration_date]"', $expiration_date); ?>" readonly>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="row align-items-center">
            <div class="col-sm-8">
                <div class="form-group">
                    <label class="form-control-label">Category</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select class="form-control" name="category_id" placeholder="Type" autocomplete="off" id="financing_scheme_categories">
                            <option>Select option</option>
                        </select>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-briefcase"></i></span></span>
                    </div>
                    <?php echo form_error('category_id'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-2">
                <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="form-control-label">Financing Scheme Type</label>
                    <div>
                        <?php echo form_dropdown('financing_scheme_type', Dropdown::get_static('scheme_types'), '', 'class="form-control form-control" id="financing_scheme_type"'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group d-none">
                    <label class="">Scheme Type <span class="kt-font-danger"></span></label>
                    <input type="text" class="form-control" id="scheme_type" name="scheme_type" value="<?php echo set_value('description', $description); ?>" placeholder="Scheme Type" autocomplete="off">
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Financing Scheme <span class="kt-font-danger">*</span></label>
                    <select class="form-control kt-select2" id="financing_scheme_id" name="billing[financing_scheme_id]" <?php //echo ($this->router->fetch_method() === 'form' && !empty($this->uri->segment(3)) ) ? 'disabled' : ''; 
                                                                                                                            ?>>
                        <option value="">Select Option</option>
                        <?php if ($scheme_name) : ?>
                            <option value="<?php echo $scheme_id ?>" selected><?php echo $scheme_info ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Penalty Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control kt-select2" id="penalty_type" name="billing[penalty_type]">
                        <option value="1">Standard Computation</option>
                        <option value="2">On or Before Due Date</option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">

    <div class="col-sm-12 financing_scheme_steps">
        <?php if ($billings) : $i['billings'] = $billings; ?>
            <?php echo $this->load->view('_financing_scheme_form', $i); ?>
        <?php endif ?>
    </div>

</div>