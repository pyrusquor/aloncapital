<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Letter Request</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="form_letter_request">
					<i class="fa fa-plus-circle"></i> Submit
				</button>
                <a href="<?php echo base_url('transaction'); ?>" class="btn btn-label-instagram"><i
                        class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body">
            
                    <form class="kt-form" method="POST" action="<?php echo base_url('transaction/letter_request/' . $transaction_id); ?>"
                        id="form_letter_request" enctype="multipart/form-data">
                        <?php $this->load->view('form/_letter_request_form'); ?>
                    </form>
                
        </div>
    </div>
</div>
