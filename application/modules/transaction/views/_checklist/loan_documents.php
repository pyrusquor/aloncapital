<style type="text/css">
	th, td {
		vertical-align: middle !important;
	}
</style>

<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_loan_document_checklist">
	<thead>
		<tr class="text-center">
			<th width="1%">
				<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
					<input type="checkbox" value="all" class="m-checkable" id="select-all">
					<span></span>
				</label>
			</th>
			<th>ID</th>
			<th>Name</th>
			<th>Description</th>
			<th>Category</th>
			<th width="12%">Start Date</th>
			<th>Owner</th>
			<th>Group</th>
			<th>Dependency</th>
			<th>Document Stage</th>
		</tr>
	</thead>
	<tbody>
		<?php if ( isset($_documents) && $_documents ):; ?>

			<?php foreach ( $_documents as $_dkey => $_docx ): ?>

				<?php
					$_description	=	isset($_docx->description) && $_docx->description ? $_docx->description : '';
					$_name				=	isset($_docx->name) && $_docx->name ? $_docx->name : '';
					$_id					=	isset($_docx->id) && $_docx->id ? $_docx->id : '';
					$_owner_id = $_docx->owner_id;
				?>

				<tr>
					<td width="1%">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<!-- <input type="checkbox" name="id[]" value="<?php echo @$_id;?>" class="m-checkable"> -->
							<input type="checkbox" name="<?php echo @$_id;?>[document_id]" value="<?php echo @$_id;?>" class="m-checkable">
							<span></span>
						</label>
					</td>
					<td class="text-center" width="1%">
						<?php echo @$_id;?>
					</td>
					<td>
						<?php echo @$_name;?>
					</td>
					<td>
						<?php echo @$_description;?>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[category_id]', Dropdown::get_static('document_checklist_category'), '', 'class="form-control form-control-sm"' . 'id="category_' . $_id . '"' ); ?>
					</td>
					<td class="text-center">
						<input type="text" class="form-control form-control-sm _start_date" id="date_<?php echo @$_id;?>" name="<?php echo @$_id;?>[start_date]" placeholder="mm/dd/yyyy" readonly>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[owner]', Dropdown::get_static('document_owner'), set_value('owner', @$_owner_id), 'class="form-control form-control-sm"'); ?>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[group_id]', Dropdown::get_static('group'), '', 'class="form-control form-control-sm"'); ?>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[dependency_id]', Dropdown::get_static('group'), '', 'class="form-control form-control-sm"'); ?>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[transaction_loan_document_stage_id]', Dropdown::get_dynamic('transaction_loan_document_stages'), '', 'class="form-control form-control-sm"' . 'id="td_stage_id_' . $_id . '"'); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php else: ?>

			<!-- <tr>
				<td class="text-center" colspan="9">
					No record Found
				</td>
			</tr> -->
		<?php endif; ?>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready( function () {

		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": true,
		  "progressBar": false,
		  "positionClass": "toast-top-right",
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "0",
		  "extendedTimeOut": "0",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}
	
		$('._start_date').datepicker({
    	orientation: "bottom left",
    	todayBtn: "linked",
    	clearBtn: true,
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});
		
	});
</script>