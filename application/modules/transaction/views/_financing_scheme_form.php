<?php
    $financing_scheme = isset($_financing_scheme) && $_financing_scheme ? $_financing_scheme : '';
    $financing_scheme_values = isset($_financing_scheme['values']) && $_financing_scheme['values'] ? $_financing_scheme['values'] : '';
    $billings = isset($billings) && $billings ? $billings : '';
    // vdebug($financing_scheme_values);
?>

<div class="row" id="financing_scheme_form">
    
    <div class="col-sm-12">
        
        <div data-repeater-list="" class="col-lg-12">
            <div data-repeater-item="" class="form-group row align-items-center">

                <div class="col-md-1">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label>&nbsp;</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Effectivity Date</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Terms</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Interest</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Percentage</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Amount</label>
                        </div>
                       
                    </div>
                </div>

            </div>
        </div>

        <?php if ($billings): ?>
           <?php foreach ($billings as $key => $billing) { ?>
                
                <?php 
                    $period_id = isset($billing['period_id']) && $billing['period_id'] ? $billing['period_id'] : '0';
                    $effectivity_date = isset($billing['effectivity_date']) && $billing['effectivity_date'] ? $billing['effectivity_date'] : '0';
                    $period_term = isset($billing['period_term']) && $billing['period_term'] ? $billing['period_term'] : '0';
                    $period_interest_percentage =isset($billing['period_interest_percentage']) && $billing['period_interest_percentage'] ? $billing['period_interest_percentage'] : '0';
                    $period_rate_percentage = isset($billing['period_rate_percentage']) && $billing['period_rate_percentage'] ? $billing['period_rate_percentage'] : '0';
                    $period_amount = isset($billing['period_amount']) && $billing['period_amount'] ? $billing['period_amount'] : '0';
                ?>
                <div data-repeater-list="" class="col-lg-12">
                    <div data-repeater-item="" class="form-group row align-items-center">

                        <div class="col-md-1">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">
                                    <?php echo Dropdown::get_static('periods', $period_id, 'view'); ?>
                                    <input type="hidden" id="period_id[]"  name="period_id[]" value="<?php echo $period_id; ?>">
                                    <input type="hidden" id="billing_id[]"  name="billing_id[]" value="<?php echo $billing['id']; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                     <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control datePicker effectivity_dates" id="effectivity_date[]" placeholder="" name="effectivity_date[]" value="<?php echo $effectivity_date; ?>" readonly>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Mo(s)</span></div>
                                            <input type="text" class="form-control terms" id="period_term" placeholder="" name="period_term[]" value="<?php echo $period_term; ?>">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="interest_rate" placeholder="Interest Rate" name="interest_rate[]" value="<?php echo $period_interest_percentage; ?>">
                                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                                        </div>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="percentage_rate" placeholder="Percentage Rate" name="percentage_rate[]" value="<?php echo $period_rate_percentage; ?>" readonly>
                                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                                        </div>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                               
                                <div class="kt-input-icon kt-input-icon--left">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                                        <input type="text" class="form-control" id="period_amount" placeholder="Period Amount" name="period_amount[]" value="<?php echo $period_amount; ?>" readonly>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>

            <?php } ?>

        <?php else: ?>
        
            <?php if ($financing_scheme_values): $reservation_rate = 0; $dp_period_amount = 0;?>
                <?php foreach ($financing_scheme_values as $key => $value) {

                    $period_id = $value['period_id'];
                    $scheme_type = $financing_scheme['scheme_type'];
                    $readonly = '';
                    $readonly_pct = '';
                    $readonly_amt = '';

                    if($period_id == '4') {
                        continue;
                    }

                    if ($period_id == '1') {
                        $reservation_rate = $value['period_rate_amount'];
                        $effectivity_date = $reservation_date;
                        $period_term = $value['period_term'];
                        $readonly = 'readonly';
                        $readonly_pct = 'readonly';
                        $readonly_amt = '';
                    } else {
                        $effectivity_date = add_months_to_date($effectivity_date,$period_term);
                        $period_term = $value['period_term'];
                        $readonly_pct = '';
                        $readonly_amt = 'readonly';
                    }

                    if ($value['period_rate_amount'] != "0.00") {
                        $period_amount = $value['period_rate_amount'];
                        $original_period_amount = get_percentage_amount($collectible_price, $value['period_rate_percentage']);
                    } else {
                        
                        $period_amount = get_percentage_amount($collectible_price, $value['period_rate_percentage']);
                        $original_period_amount = get_percentage_amount($collectible_price, $value['period_rate_percentage']);

                        if ( ($period_id == 2)&&($key == 1) ) { // deduct the reservation rate to the period amount of dp

                            if($period_amount != 0) {
                                $dp_period_amount = $period_amount;
                                $period_amount = $period_amount - $reservation_rate;
                            }

                        }
                    }

                    switch($scheme_type) {
                        case '2' :  // Zero Downpayment
                            if ($period_id == 3) $period_amount = $collectible_price - $reservation_rate;
                            break;
                        
                        case '3' : // Fixed Loan Amount
                            if ($period_id == 2) {
                                $loan_amount = $financing_scheme_values[2]['period_rate_amount'];
                                $period_amount = $collectible_price - ($loan_amount + $reservation_rate);
                            }
                            break;

                        case '4': // Fixed Downpayment
                            if ($period_id == 3) {
                                $downpayment_amount = $financing_scheme_values[1]['period_rate_amount'];
                                $period_amount = $collectible_price - ($downpayment_amount + $reservation_rate);
                            }
                            break;

                        case '5': // Cash Sale
                            if ($period_id == 3) {
                                $period_amount = $collectible_price - $dp_period_amount;
                            }

                    }
                ?>

                    <div data-repeater-list="" class="col-lg-12">
                        <div data-repeater-item="" class="form-group row align-items-center">

                            <div class="col-md-1">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__control">
                                        <?php echo Dropdown::get_static('periods', $period_id, 'view'); ?>
                                        <input type="hidden" id="period_id[]"  name="period_id[]" value="<?php echo $period_id; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__control">

                                         <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control <?php echo $period_id != 1 ? "datePicker" : "" ?> effectivity_dates" id="effectivity_date[]" placeholder="" data-period-id="<?php echo $period_id; ?>" name="effectivity_date[]" value="<?php echo $effectivity_date; ?>" <?=$readonly; ?>>
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__control">

                                        <div class="kt-input-icon kt-input-icon--left">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">Mo(s)</span></div>
                                                <input type="text" <?=$readonly; ?> class="form-control terms" id="period_term" placeholder="" name="period_term[]" value="<?php echo $value['period_term']; ?>">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__control">

                                        <div class="kt-input-icon kt-input-icon--left">
                                            <div class="input-group">
                                                <input type="   text" class="form-control" id="interest_rate" placeholder="Interest Rate" name="interest_rate[]" <?=$readonly; ?> value="<?php echo $value['period_interest_percentage']; ?>">
                                                <div class="input-group-append"><span class="input-group-text">%</span></div>
                                            </div>
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="kt-form__group--inline">
                                    <div class="kt-form__control">

                                        <div class="kt-input-icon kt-input-icon--left">
                                            <div class="input-group">
                                                <input type="text" class="form-control percentage_rate computeSchemeAmount" id="percentage_rate" placeholder="Percentage Rate" name="percentage_rate[]" data-period-id="<?php echo $period_id; ?>" value="<?php echo $value['period_rate_percentage']; ?>" <?=$readonly_pct; ?>>
                                                <div class="input-group-append"><span class="input-group-text">%</span></div>
                                            </div>
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="kt-form__group--inline">
                                   
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                                            <input type="text"  data-scheme-type="<?=$scheme_type;?>"  data-value-period-amount="<?=$value['period_rate_amount'];?>" data-original-period-amount="<?=$original_period_amount;?>"  class="form-control period_amount computeSchemeAmount" id="period_amount_<?=$period_id?>" placeholder="Period Amount" name="period_amount[]" data-period-id="<?php echo $period_id; ?>" value="<?php echo $period_amount; ?>" <?=$readonly_amt; ?>>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        </div>
                    </div>

                <?php } ?>
            <?php endif ?>

        <?php endif; ?>
     
    </div>

</div>