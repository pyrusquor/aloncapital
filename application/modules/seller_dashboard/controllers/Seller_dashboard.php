<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Seller_dashboard extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // overwrite default theme and layout if needed
        $this->template->set_theme('default');
        $this->template->set_layout('default');

        $this->template->build('dashboard');

    }
}