<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Suppliers extends MY_Controller
{

    private $fields = [
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim',
        ),
        array(
            'field' => 'code',
            'label' => 'Supplier Code',
            'rules' => 'trim',
        ),
        array(
            'field' => 'type',
            'label' => 'Supplier Type',
            'rules' => 'trim',
        ),
        array(
            'field' => 'payment_terms',
            'label' => 'Payment Terms',
            'rules' => 'trim',
        ),
        array(
            'field' => 'delivery_terms',
            'label' => 'Delivery Terms',
            'rules' => 'trim',
        ),
        array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'trim',
        ),
        array(
            'field' => 'mobile_number',
            'label' => 'Mobile Number',
            'rules' => 'trim',
        ),
        array(
            'field' => 'email_address',
            'label' => 'Email Address',
            'rules' => 'trim',
        ),
        array(
            'field' => 'tin_number',
            'label' => 'TIN Number',
            'rules' => 'trim',
        ),
    ];


    public function __construct()
    {
        parent::__construct();

        $this->load->model('suppliers/Suppliers_model', 'M_Suppliers');
        $this->load->model('user/User_model', 'M_user');

        $this->_table_fillables = $this->M_Suppliers->fillable;
        $this->_table_columns = $this->M_Suppliers->__get_columns();
        $this->_table = 'suppliers';
        // Format Helper
        $this->load->helper(['format', 'images']);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Suppliers->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Suppliers->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showSupplierss()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'name' => true,
            'code' => true,
            'type' => true,
            'payment_terms' => true,
            'delivery_terms' => true,
            'address' => true,
            'alternate_address' => true,
            'mobile_number' => true,
            'alternate_mobile_number' => true,
            'landline_number' => true,
            'fax_number' => true,
            'email_address' => true,
            'tin_number' => true,
            'sales_contact_person' => true,
            'sales_email_address' => true,
            'sales_mobile_number' => true,
            'finance_contact_person' => true,
            'finance_email_address' => true,
            'finance_mobile_number' => true,
            'image' => true,
            'payment_type' => true,
            'vat_type' => true,
            'tax_type' => true,
            'bank_id' => true,
            'bank_account_number' => true,
            'cor_image' => true,
            'auth_payee' => true,
            'is_blacklisted' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $suppliers = $this->M_Suppliers->as_array()->get_all();
        $data = [];

        if ($suppliers) {

            foreach ($suppliers as $key => $value) {
                // vdebug($value['is_blacklisted']);
                $suppliers[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                $suppliers[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';

                $suppliers[$key]['is_blacklisted'] = Dropdown::get_static('bool', $value['is_blacklisted'], "asd");
            }

            foreach ($suppliers as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    // echo "<pre>";
                    // print_r($x) ;echo "<br>";
                    // echo $_column;echo "<br>";

                    $x = array_slice($x, $_column, 1);

                    // vdebug($x);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function create()
    {
        if ($this->input->post()) {

            $response['status']    = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            // Image Upload
            $upload_path = './assets/uploads/supplier/images';
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 5000;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config, 'image');
            $this->load->library('upload', $config, 'cor_image');
            $this->image->initialize($config);
            $this->cor_image->initialize($config);

            if (!empty($_FILES['image']['name'])) {

                if (!$this->image->do_upload('image')) {
                    $this->notify->error($this->image->display_errors(), 'suppliers/create');
                } else {
                    $img_data = $this->image->data();
                    $_input['image'] = $img_data['file_name'];
                }
            }

            if (!empty($_FILES['cor_image']['name'])) {

                if (!$this->cor_image->do_upload('cor_image')) {
                    $this->notify->error($this->cor_image->display_errors(), 'suppliers/create');
                } else {
                    $cor_img_data = $this->cor_image->data();
                    $_input['cor_image'] = $cor_img_data['file_name'];
                }
            }

            $_input = $this->input->post();
            $_input['created_by'] = $this->session->userdata['user_id'];
            $_input['code'] = uniqidSL();

            $result = $this->M_Suppliers->from_form()->insert($_input);

            if ($result) {
                $response['status']    = 1;
                $response['message'] = 'Supplier Successfully Created!';
            }

            echo json_encode($response);
            exit();
        }

        $this->template->build('create');
    }

    public function update($id = false)
    {
        if ($id) {

            $this->view_data['data'] = $data = $this->M_Suppliers->with_bank()->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $response['status']    = 0;
                    $response['message'] = 'Oops! Please refresh the page and try again.';

                    // Image Upload
                    $upload_path = './assets/uploads/supplier/images';
                    $config = array();
                    $config['upload_path'] = $upload_path;
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = 5000;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config, 'image');
                    $this->load->library('upload', $config, 'cor_image');
                    $this->image->initialize($config);
                    $this->cor_image->initialize($config);

                    if (!empty($_FILES['image']['name'])) {

                        if (!$this->image->do_upload('image')) {
                            $this->notify->error($this->image->display_errors(), 'supplier/create');
                        } else {
                            $img_data = $this->image->data();
                            $_input['image'] = $img_data['file_name'];
                        }
                    }

                    if (!empty($_FILES['cor_image']['name'])) {

                        if (!$this->cor_image->do_upload('cor_image')) {
                            $this->notify->error($this->cor_image->display_errors(), 'supplier/create');
                        } else {
                            $cor_img_data = $this->cor_image->data();
                            $_input['cor_image'] = $cor_img_data['file_name'];
                        }
                    }

                    $_input = $this->input->post();
                    $_input['updated_by'] = $this->session->userdata['user_id'];

                    $result = $this->M_Suppliers->from_form()->update($_input, $data['id']);

                    if ($result) {
                        $response['status']    = 1;
                        $response['message'] = 'Supplier Successfully Updated!';
                    }

                    echo json_encode($response);
                    exit();
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Suppliers->get($id);
            if ($list) {

                $deleted = $this->M_Suppliers->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Suppliers->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Type Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_Suppliers->with_bank()->get($id);

            $this->view_data['audit'] = $this->__get_audit($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;

        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(

                        /* ==================== begin: Add model fields ==================== */
                        'name' => $upload_data[1],
                        'code' => $upload_data[2],
                        'type' => $upload_data[3],
                        'payment_terms' => $upload_data[4],
                        'delivery_terms' => $upload_data[5],
                        'address' => $upload_data[6],
                        'alternate_address' => $upload_data[7],
                        'mobile_number' => $upload_data[8],
                        'alternate_mobile_number' => $upload_data[9],
                        'landline_number' => $upload_data[10],
                        'fax_number' => $upload_data[11],
                        'email_address' => $upload_data[12],
                        'tin_number' => $upload_data[13],
                        'sales_contact_person' => $upload_data[14],
                        'sales_email_address' => $upload_data[15],
                        'sales_mobile_number' => $upload_data[16],
                        'finance_contact_person' => $upload_data[17],
                        'finance_email_address' => $upload_data[18],
                        'finance_mobile_number' => $upload_data[19],
                        'image' => $upload_data[20],
                        'payment_type' => $upload_data[21],
                        'vat_type' => $upload_data[22],
                        'tax_type' => $upload_data[23],
                        'bank_id' => $upload_data[24],
                        'bank_account_number' => $upload_data[25],
                        'cor_image' => $upload_data[26],
                        'auth_payee' => $upload_data[27],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $suppliers_id = $upload_data[0];
                    $suppliers = $this->M_Suppliers->get($suppliers_id);

                    if ($suppliers) {
                        $result = $this->M_Suppliers->update($fields, $suppliers_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[0],
                            'code' => $upload_data[1],
                            'type' => $upload_data[2],
                            'payment_terms' => $upload_data[3],
                            'delivery_terms' => $upload_data[4],
                            'address' => $upload_data[5],
                            'alternate_address' => $upload_data[6],
                            'mobile_number' => $upload_data[7],
                            'alternate_mobile_number' => $upload_data[8],
                            'landline_number' => $upload_data[9],
                            'fax_number' => $upload_data[10],
                            'email_address' => $upload_data[11],
                            'tin_number' => $upload_data[12],
                            'sales_contact_person' => $upload_data[13],
                            'sales_email_address' => $upload_data[14],
                            'sales_mobile_number' => $upload_data[15],
                            'finance_contact_person' => $upload_data[16],
                            'finance_email_address' => $upload_data[17],
                            'finance_mobile_number' => $upload_data[18],
                            'image' => $upload_data[19],
                            'payment_type' => $upload_data[20],
                            'vat_type' => $upload_data[21],
                            'tax_type' => $upload_data[22],
                            'bank_id' => $upload_data[23],
                            'bank_account_number' => $upload_data[24],
                            'cor_image' => $upload_data[25],
                            'auth_payee' => $upload_data[26],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Suppliers->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[1],
                            'code' => $upload_data[2],
                            'type' => $upload_data[3],
                            'payment_terms' => $upload_data[4],
                            'delivery_terms' => $upload_data[5],
                            'address' => $upload_data[6],
                            'alternate_address' => $upload_data[7],
                            'mobile_number' => $upload_data[8],
                            'alternate_mobile_number' => $upload_data[9],
                            'landline_number' => $upload_data[10],
                            'fax_number' => $upload_data[11],
                            'email_address' => $upload_data[12],
                            'tin_number' => $upload_data[13],
                            'sales_contact_person' => $upload_data[14],
                            'sales_email_address' => $upload_data[15],
                            'sales_mobile_number' => $upload_data[16],
                            'finance_contact_person' => $upload_data[17],
                            'finance_email_address' => $upload_data[18],
                            'finance_mobile_number' => $upload_data[19],
                            'image' => $upload_data[20],
                            'payment_type' => $upload_data[21],
                            'vat_type' => $upload_data[22],
                            'tax_type' => $upload_data[23],
                            'bank_id' => $upload_data[24],
                            'bank_account_number' => $upload_data[25],
                            'cor_image' => $upload_data[26],
                            'auth_payee' => $upload_data[27],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Suppliers->insert($fields);
                    }
                }
                if ($result === false) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'suppliers');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'suppliers_inventory');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'suppliers CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'suppliers');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Suppliers->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('suppliers');
            $_objSheet->setCellValue('A1', 'suppliers');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $supplierss = $this->M_Suppliers->as_array()->get_all();
        if ($supplierss) {

            foreach ($supplierss as $_lkey => $suppliers) {

                $_datas[$suppliers['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_supplierss_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'suppliers_inventory');
                }
            } else {

                $_filename = 'list_of_supplierss_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns    =    $this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'suppliers_inventory');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($supplierss as $_lkey => $suppliers) {

                                $_datas[$suppliers['id']][$_title] = isset($suppliers[$_column]) && ($suppliers[$_column] !== '') ? Dropdown::get_static('bool', $suppliers[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($supplierss as $_lkey => $suppliers) {

                                if ($_name === 'status') {

                                    $_datas[$suppliers['id']][$_title] = isset($suppliers[$_name]) && $suppliers[$_name] ? Dropdown::get_static('inventory_status', $suppliers[$_name], 'view') : '';
                                } else {

                                    $_datas[$suppliers['id']][$_title] = isset($suppliers[$_name]) && $suppliers[$_name] ? $suppliers[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of suppliers Inventory');
                $_objSheet->setCellValue('A1', 'LIST OF suppliers INVENTORY');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_style = array(
                    'font' => array(
                        'bold' => true,
                        'size' => 10,
                        'name' => 'Verdana',
                    ),
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'suppliers');
            }
        } else {

            $this->notify->error('No Record Found', 'suppliers_inventory');
        }
    }

    public function blacklist($id)
    {
        $response['status'] = 0;
        $response['message'] = "Supplier blacklisted";

        if ($id) {

            $supplier = $this->M_Suppliers->get($id);

            $this->db->trans_begin();

            if ($supplier['is_blacklisted'] == 0) {

                $this->M_Suppliers->update([
                    "is_blacklisted" => 1
                ], $id);
            } else {

                $this->M_Suppliers->update([
                    "is_blacklisted" => 0
                ], $id);
                $response['message'] = "Supplier removed from blacklist";
            }

            if ($this->db->trans_status() === FALSE) {

                $this->db->trans_rollback();
            } else {

                $this->db->trans_commit();
                $response['status'] = 1;
            }
        }

        echo json_encode($response);
        exit();
    }
}
