<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Document_checklist_model extends MY_Model {

	public $primary_key	=	'id'; // you MUST mention the primary key
	public $protected		=	['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $fillable		=	['checklist_id', 'document_id', 'is_active']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $table				=	'document_checklists'; // you MUST mention the table name
	public $rules				=	[];
	public $_fields			=	[];

	function __construct () {

		parent::__construct();

		$this->soft_deletes = TRUE;

		$this->return_as = 'array';

		$this->rules['insert']	=	$this->_fields;
		$this->rules['update']	=	$this->_fields;
	}

	function _get_documents ( $_id = FALSE ) {

		$_return	=	FALSE;

		if ( $_id ) {

			$_sql	=	"
								SELECT d.*
								FROM document_checklists AS dc
								LEFT JOIN documents AS d ON d.`id` = dc.`document_id`
								LEFT JOIN checklists AS c ON c.`id` = dc.`checklist_id`
								WHERE dc.`deleted_at` IS NULL
									AND d.`deleted_at` IS NULL
									AND c.`deleted_at` IS NULL
									AND c.`id` = ?
							";

			$_query	=	$this->db->query($_sql, [$_id]);

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}

	function insert_dummy () {

		require APPPATH.'/third_party/faker/autoload.php';

		$_faker	=	Faker\Factory::create();

		$_datas	=	[];

		for ( $i = 1; $i <= 5; $i++ ) {

			array_push($_datas, [
				'checklist_id'	=>	rand(1,40),
				'document_id'	=>	rand(1,25),
				'created_by'	=>	rand(1,6),
				'created_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time())),
				'updated_by'	=>	rand(1,6),
				'updated_at'	=>	date('Y-m-d H:i:s', mt_rand(1, time()))
			]);
		}

		$this->db->insert_batch($this->table, $_datas);
	}
}