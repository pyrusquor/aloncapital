<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Sales Checklists</h3>
			<?php if ( isset($_total) && $_total ): ?>

				<span class="kt-subheader__separator kt-subheader__separator--v"></span>
				<span class="kt-subheader__desc" id="_total"><?php echo $_total;?> TOTAL</span>
			<?php endif; ?>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
				<input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
				<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('sales_checklist/create');?>" class="btn btn-label-primary btn-elevate btn-icon-sm">
					<i class="fa fa-plus"></i> Create
				</a>
				<button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
					<i class="fa fa-filter"></i> Filter
				</button>
				<?php if ( FALSE ): ?>

					<button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
						<i class="fa fa-upload"></i> Import
					</button>
				<?php endif; ?>
				<button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#_export_option">
					<i class="fa fa-download"></i> Export
				</button>
			</div>
		</div>
	</div>
</div>


<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__body">
		<div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
			<div class="row">
				<div class="col-lg-12">
					<form class="kt-form">
						<div class="form-group row">
							<div class="col-sm-3">
								<label class="form-control-label">Checklist Name</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<input type="text" class="form-control form-control-sm _filter" placeholder="Checklist Name"  id="_column_2"  data-column="2" autocomplete="off">
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-book"></i></span></span>
								</div>
							</div>
							<div class="col-sm-3">
								<label class="form-control-label">Category Name</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<?php echo form_dropdown('category_id', Dropdown::get_static('document_checklist_category'), set_value('category_id', @$_category_id), 'class="form-control form-control-sm _filter" id="_column_0" data-column="0"'); ?>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sitemap"></i></span></span>
								</div>
							</div>
							<!-- <div class="col-sm-4">
								<label class="form-control-label">Date</label>
								<div class="input-daterange input-group" id="kt_datepicker">
									<input type="text" class="form-control form-control-sm kt-input" name="start" placeholder="From" data-col-index="5" />
									<div class="input-group-append">
										<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
									</div>
									<input type="text" class="form-control form-control-sm kt-input" name="end" placeholder="To" data-col-index="5" />
								</div>
							</div> -->
						</div>
					</form>
				</div>
			</div>
		</div>
		<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
			<form class="kt-form">
				<div class="form-group row">
					<div class="col-lg-3">
						<label class="form-control-label">Document Type</label>
						<div class="kt-input-icon  kt-input-icon--left">
							<select class="form-control form-control-sm" name="status">
								<option value=""> -- Update Existing Data -- </option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-lg-3">
						<label class="form-control-label">Upload CSV file:</label>
						<label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
						<input type="file" name="" class="">
					</div>
				</div>
				<div class="form-group form-group-last row">
					<div class="col-lg-3">
						<div class="row">
							<div class="col-lg-6">
								<button type="button" class="btn btn-brand btn-success btn-elevate btn-sm">
									<i class="fa fa-upload"></i> Upload
								</button>
							</div>
							<div class="col-lg-6 kt-align-right">
								<a href="<?php echo site_url('sales_checklist/export')?>" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm">
									<i class="fa fa-file-csv"></i>
								</a>
								<button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" data-toggle="modal" data-target="#upload_guide">
									<i class="fa fa-info-circle"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_checklist_table">
			<thead>
				<tr class="text-center">
					<th width="1%">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<input type="checkbox" value="all" class="m-checkable" id="select-all">
							<span></span>
						</label>
					</th>
					<th>Category</th>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>No. of Documents</th>
					<th>Last Update</th>
					<th>Actions</th>
				</tr>
			</thead>
		</table>
		<!--end: Datatable -->
	</div>
</div>

<!--begin::Modal-->
<!-- <div class="modal fade show" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true" style="padding-right: 15px; display: block;"> -->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="_export_option_label">Export Options</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="_export_form" target="_blank" action="<?php echo site_url('sales_checklist/export');?>" method="POST">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group form-group-last kt-hide">
								<div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
									<div class="alert-icon"><i class="flaticon-warning"></i></div>
									<div class="alert-text">
										Oh snap! You need select at least one.
									</div>
									<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 offset-lg-1">
							<div class="kt-checkbox-list">
								<label class="kt-checkbox kt-checkbox--bold">
									<input type="checkbox" id="_export_select_all"> Field
									<span></span>
								</label>
								<label class="kt-checkbox kt-checkbox--bold">
									<input type="checkbox" name="_export_column[]" class="_export_column" value="name"> Name
									<span></span>
								</label>
								<label class="kt-checkbox kt-checkbox--bold">
									<input type="checkbox" name="_export_column[]" class="_export_column" value="description"> Description
									<span></span>
								</label>
								<label class="kt-checkbox kt-checkbox--bold">
									<input type="checkbox" name="_export_column[]" class="_export_column" value="number_of_documents"> Number of Documents
									<span></span>
								</label>
								<label class="kt-checkbox kt-checkbox--bold">
									<input type="checkbox" name="_export_column[]" class="_export_column" value="category"> Category
									<span></span>
								</label>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="kt-form__actions btn-block">
					<button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
						<i class="fa fa-times"></i> Close
					</button>
					<button type="submit" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" form="_export_form">
					<i class="fa fa-file-export"></i> Export
				</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="upload_guide_label">Checklist</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<h2>UPLOAD GUIDE</h2>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->