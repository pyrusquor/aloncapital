<style type="text/css">
	th, td {
		vertical-align: middle !important;
	}
</style>


<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_document_checklist">
	<thead>
		<tr class="text-center">
			<th width="1%">
				<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
					<input type="checkbox" value="all" class="m-checkable" id="select-all">
					<span></span>
				</label>
			</th>
			<th>ID</th>
			<th width="15%">Name</th>
			<th width="15%">Description</th>
			<th>Owner</th>
			<th>Required Copies</th>
			<th>Days to Finish</th>
			<th>Days to Expire</th>
			<th>Required</th>
		</tr>
	</thead>
	<tbody>
		<?php if ( isset($_documents) && $_documents ): ?>

			<?php foreach ( $_documents as $_dkey => $_docx ): ?>

				<?php
					$_description	=	isset($_docx->description) && $_docx->description ? $_docx->description : '';
					$_name			=	isset($_docx->name) && $_docx->name ? $_docx->name : '';
					$_id			=	isset($_docx->id) && $_docx->id ? $_docx->id : '';
				?>

				<tr>
					<td width="1%">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
							<input type="checkbox" name="<?php echo @$_id;?>[document_id]" value="<?php echo @$_id;?>" class="m-checkable">
							<span></span>
						</label>
					</td>
					<td class="text-center" width="1%">
						<?php echo @$_id;?>
					</td>
					<td>
						<?php echo @$_name;?>
					</td>
					<td>
						<?php echo @$_description;?>
					</td>
					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[owner_id]', Dropdown::get_static('document_owner'), '', 'class="form-control form-control-sm"'); ?>
					</td>

					<td class="text-center">
						<input type="text" value="0" class="form-control form-control-sm" name="<?=@$_id.'[required_copies]';?>">
					</td>

					<td class="text-center">
						<input type="text" value="0" class="form-control form-control-sm" name="<?=@$_id.'[days_to_finish]';?>">
					</td>

					<td class="text-center">
						<input type="text" value="0" class="form-control form-control-sm" name="<?=@$_id.'[days_to_expire]';?>">
					</td>

					<td class="text-center">
						<?php echo form_dropdown(@$_id.'[is_required]', Dropdown::get_static('bool'), '', 'class="form-control form-control-sm"'); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready( function () {

		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": true,
		  "progressBar": false,
		  "positionClass": "toast-top-right",
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "0",
		  "extendedTimeOut": "0",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		$('._start_date').datepicker({
    	orientation: "bottom left",
    	todayBtn: "linked",
    	clearBtn: true,
    	autoclose: true,
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});
	});
</script>