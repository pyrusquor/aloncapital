<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Stages extends MY_Controller {

	function __construct () {

		parent::__construct();

		$this->load->model('stages/Stage_model', 'M_stage');

		// $this->view_data['_largescreen']	=	TRUE;
	}

	function index () {

		redirect('sales_checklist');
			
	}

	function create ($_proj_id = FALSE) {

		$this->view_data['sales_checklist_id'] = $_proj_id;

		if ( $this->input->post() ) {

			$_input	=	$this->input->post();
			$_insert	=	$this->M_stage->insert($_input); #lqq(); ud($_insert);

			if ( $_insert !== FALSE ) {

				// $_cid	=	$this->db->insert_id();
				// $this->notify->success('Checklist Template successfully created.', 'stages/update/'.$_cid);

				$this->notify->success('Stage successfully created.', 'checklist');
			} else {

				$this->notify->error('Oh snap! Please refresh the page and try again.');
			}
		}

		$this->template->build('create', $this->view_data);
	}

	function update ( $_id = FALSE ) {

		if ( $_id ) {

			$_list	=	$this->M_stage->get($_id);
			if ( $_list ) {

				$this->view_data['_list']	=	$_list;

				if ( $this->input->post() ) {

					$_input	=	$this->input->post();

					// $_updated	=	$this->M_stage->from_form()->update(NULL, $_list['id']);
					$_updated	=	$this->M_stage->update($_input, $_list['id']);
					if ( $_updated !== FALSE ) {

						// $this->notify->success('Checklist Template successfully updated.');

						$this->notify->success('Checklist Template successfully updated.', 'checklist');
					} else {

						$this->notify->error('Oh snap! Please refresh the page and try again.');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function view($_proj_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('sales_checklist/Sales_checklist_stages_document_model', 'M_cs_document');

		if ($_proj_id) {

			$this->view_data['_proj_id']	=	$_proj_id;

			$_documents	=	$this->M_cs_document->_get_document_checklist($_proj_id);
			if ($_documents) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('view', $this->view_data);
		} else {

			show_404();
		}
	}

	

	function delete () {

		$_response['_status']	=	0;
		$_response['_msg']		=	'Oops! Please refresh the page and try again.';

		$_id	=	$this->input->post('id');
		if ( $_id ) {

			$_list	=	$this->M_stage->get($_id);
			if ( $_list ) {

				$_deleted	=	$this->M_stage->delete($_list['id']);
				if ( $_deleted !== FALSE ) {

					$_response['_status']	=	1;
					$_response['_msg']		=	'Checklist successfully deleted';
				}
			}
		}

		echo json_encode($_response);

		exit();
	}

	function bulkDelete () {

		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if($this->input->is_ajax_request())
		{
			$delete_ids = $this->input->post('deleteids_arr');
		
			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_stage->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Record/s successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	function export () {

		$_titles[]	=	'#';
		$_titles[]	=	'name';
		$_titles[]	=	'description';
		$_titles[]	=	'number of documents';
		$_titles[]	=	'category';

		$_alphas	=	range('A', 'E');

		$_checklists	=	$this->M_stage->_get_checklist_info();
		if ( $_checklists ) {

			$_filename	=	'list_of_checklists'.date('m_d_y_h-i-s',time()).'.xls';

			$_start	=	3;
			$_row		=	2;
			$_no		=	1;

			$_objSheet	=	$this->excel->getActiveSheet();

			if ( $this->input->post() ) {

				unset($_titles);
				$_titles[]	=	'#';

				$_export_columns	=	$this->input->post('_export_column');

				if ( $_export_columns ) {

					foreach ( $_export_columns as $_ekey => $_clm ) {

						$_titles[]	=	isset($_clm) && $_clm ? str_replace('_', ' ', $_clm) : '';
					}

					$_alphas	=	[];

					$_alphas	=	$this->__get_excel_columns(count($_titles));

					$_xls_columns	=	array_combine($_alphas, $_titles);
					$_lastAlpha		=	end($_alphas);

	        foreach ( $_xls_columns as $_xkey => $_column ) {

	        	$_title	=	ucwords(strtolower($_column));

	        	$_objSheet->setCellValue($_xkey.$_row, $_title);
	        }
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'checklist');
				}
			} else {

				$_filename	=	'list_of_checklists'.date('m_d_y_h-i-s',time()).'.csv';

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

        foreach ( $_xls_columns as $_xkey => $_column ) {

        	$_title	=	ucwords(strtolower($_column));

        	$_objSheet->setCellValue($_xkey.$_row, $_title);
        }
			}

			$_objSheet->setTitle('List of Checklists');
			$_objSheet->setCellValue('A1', 'LIST OF CHECKLISTS');
			$_objSheet->mergeCells('A1:'.$_lastAlpha.'1');

			foreach ( $_checklists as $key => $_checklist ) {

				$_data['number of documents']	=	isset($_checklist['number_of_documents']) && $_checklist['number_of_documents'] ? $_checklist['number_of_documents'] : '';
      	$_data['description']					=	isset($_checklist['description']) && $_checklist['description'] ? $_checklist['description'] : '';
      	$_category_id									=	isset($_checklist['category_id']) && $_checklist['category_id'] ? $_checklist['category_id'] : '';
      	$_data['category']						=	isset($_category_id) && $_category_id ? Dropdown::get_static('document_checklist_category', $_category_id, 'view') : '';
      	$_data['name']								=	isset($_checklist['name']) && $_checklist['name'] ? $_checklist['name'] : '';
      	$_data['#']										=	isset($_no) && $_no ? $_no : '';

      	foreach ( $_alphas as $_akey => $_alpha ) {

      		$_value	=	isset($_data[$_xls_columns[$_alpha]]) && $_data[$_xls_columns[$_alpha]] ? $_data[$_xls_columns[$_alpha]] : '';

        	$_objSheet->setCellValue($_alpha.$_start, $_value);
        }

				$_start++;
				$_no++;
      }

			foreach ( $_alphas as $_alpha ) {

      	$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
      }

      $_style	=	array(
      						'font'  => array(
	                	'bold'	=>	TRUE,
	                	'size'	=>	10,
	                	'name'	=>	'Verdana'
	                )
      					);
      $_objSheet->getStyle('A1:'.$_lastAlpha.$_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="'.$_filename.'"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			$this->notify->error('No Record Found', 'checklist');
		}
	}
	
	// ENDOF

	function documents($id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('checklist/Checklist_model', 'M_checklist');

		if ($id) {

			$this->view_data['_proj_id']	=	$id;

			$_checklists	=	$this->M_checklist->find_all(FALSE, ['id', 'name']);

			if ($_checklists) {
				$this->view_data['_checklists']	=	$_checklists; #ud($_checklists);
			}

			$this->template->build('documents', $this->view_data);
		} else {

			show_404();
		}
	}

	function get_filtered_document_checklist()
	{
		$_respo['_status']	=	0;
		$_respo['_msg']			=	'';
		$_view_data	=	[];

		$_checklist_id	=	$this->input->post('value');
		$_proj_id		=	$this->input->post('proj_id');
		if ($_checklist_id && $_proj_id) {

			$_view_data['_proj_id']	=	$_proj_id;

			$this->load->model('checklist/Document_checklist_model', 'M_document_checklist');

			$_documents	=	$this->M_document_checklist->_get_documents($_checklist_id);

			if ($_documents) {
				$_view_data['_documents']	=	$_documents;
			}

			$_respo['_status']	=	1;

			$_respo['_html']	=	$this->load->view('_checklist/documents', $_view_data, TRUE);
		} else {

			$_respo['_msg']	=	'Oops! Please refresh the page and try again.';
		}

		echo json_encode($_respo);

		exit();
	}
	

	function update_document_checklist () {

		$_response['_status']	=	0;
		$_response['_msg']		=	'Oops! Please refresh the page and try again.';

		$_checklist_id	=	$this->input->post('id');
		$_docx_ids			=	$this->input->post('ids');
		if ( $_checklist_id ) {

			$this->load->model('stages/Document_checklist_model', 'M_document_checklist');

			$_selected_ids	=	[];

			if ( $_docx_ids ) {

				$_inserted	=	0;
				$_removed		=	0;

				$_document_checklists	=	$this->M_document_checklist->where('checklist_id', $_checklist_id)->get_all();
				if ( $_document_checklists ) {

					foreach ( $_document_checklists as $key => $_lts ) {

						$_selected_ids[$_lts['id']]	=	$_lts['document_id'];
					}
				}

				if ( count($_docx_ids) >= count($_selected_ids) ) {

					foreach ( $_docx_ids as $key => $_id ) {

						if ( !in_array($_id, $_selected_ids) ) {

							unset($_data);
							$_data['checklist_id']	=	$_checklist_id;
							$_data['document_id']		=	$_id;

							$_insert	=	$this->M_document_checklist->insert($_data);
							if ( $_insert ) {

								$_inserted++;
							}
						} else { continue; }
					}
				} else {

					$_must_delete	=	array_diff($_selected_ids, $_docx_ids);
					if ( $_must_delete ) {

						foreach ( $_must_delete as $_key => $_del_id ) {

							unset($_where);
							$_where['checklist_id']	=	$_checklist_id;
							$_where['document_id']	=	$_del_id;

							$_deleted	=	$this->M_document_checklist->where($_where)->delete();
							if ( $_deleted ) {

								$_removed++;
							}
						}
					}
				}				 

				if ( count($_docx_ids) == count($_selected_ids) ) {

					$_response['_status']	=	2;
					$_response['_msg']		=	'Oops! Nothings change.';
				} else {

					if ( $_inserted ) {

						$_response['_status']	=	1;
						$_response['_msg']		=	$_inserted.' document/s was added.';
					} elseif ( $_removed && ($_removed <=  count($_selected_ids)) ) {

						$_response['_status']	=	1;
						$_response['_msg']		=	$_removed.' document/s was removed.';
					}
				}
			} else {

				$_deleted	=	$this->M_document_checklist->where('checklist_id', $_checklist_id)->delete();
				if ( $_deleted ) {

					$_response['_status']	=	1;
					$_response['_msg']		=	'Checklist was updated.';
				} else {

					$_response['_status']	=	2;
					$_response['_msg']		=	'Oops! Nothings change.';
				}
			}
		}

		echo json_encode($_response);

		exit();
	}

	function get_document_checklist ( $_li_id = FALSE ) {

		$_docuemnt_ids	=	[];

		$this->load->model('land/Land_inventory_document_model', 'M_li_document');
		$this->load->model('document/Document_model', 'M_document');

		$_response	=	[];

		$_total['_displays']	=	0;
		$_total['_records']		=	0;
		$_datas	=	[];
		$_secho	=	0;

		if ( $_li_id ) {

			unset($_where);
			$_where['land_inventory_id']	=	$_li_id;

			$_land_inventory_documents	=	$this->M_li_document->find_all($_where, ['document_id']); #vd($_land_inventory_documents);
			if ( $_land_inventory_documents ) {

				foreach ( $_land_inventory_documents as $key => $_lid ) {

					if ( isset($_lid->document_id) && $_lid->document_id ) {

						$_docuemnt_ids[]	=	$_lid->document_id;
					}
				}
			}

			if ( isset($_docuemnt_ids) && !empty($_docuemnt_ids) ) {

				$_columns	=	[
											'id'	=>	TRUE,
											'name'	=>	TRUE,
											'description'	=>	TRUE,
											'owner_id'	=>	TRUE,
											'classification_id'	=>	TRUE,
											'updated_at'	=>	TRUE,
											// 'Actions'	=>	FALSE
										];

				if ( isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef']) ) {

					$_columns	=	[];

					foreach ( $_REQUEST['columnsDef'] as $_dkey => $_def ) {

						$_columns[$_def]	=	TRUE;
					}
				}

				$_documents	=	$this->M_document->where('id', $_docuemnt_ids)->as_array()->get_all(); #vd($_documents);
				if ( $_documents ) {

					foreach ( $_documents as $_ckkey => $_ck ) {

						$_datas[]	=	$this->filterArray($_ck, $_columns);
					}

					$_total['_displays']	=	$_total['_records']	=	count($_datas);

					if ( isset($_REQUEST['search']) ) {

						$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

						$_total['_displays']	=	$_datas ? count($_datas) : 0;
					}

					if ( isset($_REQUEST['columns']) && is_array($_REQUEST['columns']) ) {

						foreach ( $_REQUEST['columns'] as $_ckey => $_clm ) {

							if ( $_clm['search'] ) {

								$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

								$_total['_displays']	=	$_datas ? count($_datas) : 0;
							}
						}
					}

					if ( isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir'] ) {

						$_column	=	$_REQUEST['order'][0]['column'];
						$_dir			=	$_REQUEST['order'][0]['dir'];

						usort( $_datas, function ( $x, $y ) use ( $_column, $_dir ) {

							$x	=	array_slice($x, $_column, 1);
							$x	=	array_pop($x);

							$y	=	array_slice($y, $_column, 1);
							$y	=	array_pop($y);

							if ( $_dir === 'asc' ) {

								return $x > $y ? TRUE : FALSE;
							} else {

								return $x < $y ? TRUE : FALSE;
							}
						});
					}

					if ( isset($_REQUEST['length']) ) {

						$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
					}

					if ( isset($_REQUEST['array_values']) && $_REQUEST['array_values'] ) {

						$_temp	=	$_datas;
						$_datas	=	[];

						foreach ( $_temp as $key => $_tmp ) {

							$_datas[]	=	array_values($_tmp);
						}
					}

					if ( isset($_REQUEST['sEcho']) ) {

						$_secho	=	intval($_REQUEST['sEcho']);
					}
				}
			}
		}

		$_response	=	[
										'iTotalDisplayRecords'	=>	$_total['_displays'],
										'iTotalRecords'					=>	$_total['_records'],
										'sColumns'							=>	'',
										'sEcho'									=>	$_secho,
										'data'									=>	$_datas
									];

		echo json_encode($_response);

		exit();
	}

	function insert_document_checklist($_proj_id = FALSE)
	{
		if ($_proj_id) {

			$_dcs	=	[];

			$_inserted	=	0;
			$_updated		=	0;
			$_removed		=	0;
			$_failed		=	0;

			$_inputs	=	$this->input->post();

			if ($_inputs) {

				foreach ($_inputs as $key => $_input) {

					if ($key !== '_document_checklist_length') {

						$_dcs[$key]['sales_checklist_stage_id']	=	$_proj_id;
						$_dcs[$key]['document_id']	=	$key;

						foreach ($_input as $_ikey => $_int) {
							$_dcs[$key][$_ikey]	=	$_int;
						}
					} else {

						continue;
					}
				}

				$this->load->model('sales_checklist/Sales_checklist_stages_document_model', 'M_p_document');

				if ($_dcs && !empty($_dcs)) {

					foreach ($_dcs as $_dkey => $_dc) {

						unset($_where);
						$_where['sales_checklist_stage_id'] =	$_dc['project_id'];
						$_where['document_id']	=	$_dc['document_id'];

						$_pd	=	$this->M_p_document->find_all($_where, FALSE, TRUE); #lqq(); up($_pd);
						if ($_pd) {

							unset($_datas);
							foreach ($_dc as $_dkey => $_d) {

								if (($_dkey !== 'sales_checklist_stage_id') && ($_dkey !== 'document_id')) {
									$_datas[$_dkey]	=	$_d;

									$_datas[$_dkey]['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_datas[$_dkey]['updated_at']	=	NOW;
								}
							}

							$_update	=	$this->M_p_document->update($_datas, $_pd->id);

							if ($_update) {
								$_updated++;
							} else {
								$_failed++;
								break;
							}

						} else {

							unset($_datas);
							$_datas	=	$_dc;

							$_datas['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['created_at']	=	NOW;
							$_datas['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['updated_at']	=	NOW;

							$_insert	=	$this->M_p_document->insert($_datas);
							if ($_insert) {

								$_inserted++;
							} else {

								$_failed++;

								break;
							}
						}
					}

					$_msg	=	'';
					if ($_inserted > 0) {

						$_msg	=	$_inserted . ' document/s was successfuly inserted';
					}

					if ($_updated > 0) {

						$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' document/s was successfuly updated';
					}

					if ($_failed > 0) {
						$this->notify->error('Something went wrong! Please refresh the page and try again.', 'stages/view/' . $_proj_id);
					} else {
						$this->notify->success($_msg . '.', 'stages/view/' . $_proj_id);
					}
				}
			}
		} else {

			show_404();
		}
	}

	function process () {

		$this->load->model('land/Land_inventory_document_model', 'M_li_document');

		$_documents =	$this->M_li_document->get_all(); #ud($_documents);
		if ( $_documents ) {

			$this->view_data['_total']	=	count($_documents);
		}

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('process', $this->view_data);
	}

	function get_checklists () {

		$_response	=	[
			'iTotalDisplayRecords'	=>	'',
			'iTotalRecords'					=>	'',
			'sColumns'							=>	'',
			'sEcho'									=>	'',
			'data'									=>	'',
		];

		$_total['_displays']	=	0;
		$_total['_records']		=	0;

		$_columns	=	[
			'id'					=>	TRUE,
			'name'				=>	TRUE,
			'description'	=>	TRUE,
			'number_of_documents'		=>	TRUE,
			'order'		=>	TRUE,
			'created_at'	=>	TRUE,
			'updated_at'	=>	TRUE,
			'category_id'	=>	TRUE
		];

		if ( isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef']) ) {

			$_columns	=	[];

			foreach ( $_REQUEST['columnsDef'] as $_dkey => $_def ) {

				$_columns[$_def]	=	TRUE;
			}
		}

		// $_checklists	=	$this->M_stage->as_array()->get_all(); vp($this->db->last_query()); vd($_checklists);

		$_checklists	=	$this->M_stage->_get_checklist_info(); #vp($this->db->last_query()); vd($_checklists);

		if ( $_checklists ) {

			$_datas	=	[];

			foreach ( $_checklists as $_ckkey => $_ck ) {

				$_datas[]	=	$this->filterArray($_ck, $_columns);
			}

			$_total['_displays']	=	$_total['_records']	=	count($_datas);

			if ( isset($_REQUEST['search']) ) {

				$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

				$_total['_displays']	=	$_datas ? count($_datas) : 0;
			}

			if ( isset($_REQUEST['columns']) && is_array($_REQUEST['columns']) ) {

				foreach ( $_REQUEST['columns'] as $_ckey => $_clm ) {

					if ( $_clm['search'] ) {

						$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

						$_total['_displays']	=	$_datas ? count($_datas) : 0;
					}
				}
			}

			if ( isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir'] ) {

				$_column	=	$_REQUEST['order'][0]['column'];
				$_dir			=	$_REQUEST['order'][0]['dir'];

				usort( $_datas, function ( $x, $y ) use ( $_column, $_dir ) {

					$x	=	array_slice($x, $_column, 1);
					$x	=	array_pop($x);

					$y	=	array_slice($y, $_column, 1);
					$y	=	array_pop($y);

					if ( $_dir === 'asc' ) {

						return $x > $y ? TRUE : FALSE;
					} else {

						return $x < $y ? TRUE : FALSE;
					}
				});
			}

			if ( isset($_REQUEST['length']) ) {

				$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
			}

			if ( isset($_REQUEST['array_values']) && $_REQUEST['array_values'] ) {

				$_temp	=	$_datas;
				$_datas	=	[];

				foreach ( $_temp as $key => $_tmp ) {

					$_datas[]	=	array_values($_tmp);
				}
			}

			$_secho	=	0;
			if ( isset($_REQUEST['sEcho']) ) {

				$_secho	=	intval($_REQUEST['sEcho']);
			}

			$_response	=	[
											'iTotalDisplayRecords'	=>	$_total['_displays'],
											'iTotalRecords'					=>	$_total['_records'],
											'sColumns'							=>	'',
											'sEcho'									=>	$_secho,
											'data'									=>	$_datas
										];
		}

		// ud($_response);

		echo json_encode($_response);

		exit();
	}

	function get_process () {

		$_docuemnt_ids	=	[];

		$this->load->model('land/Land_inventory_document_model', 'M_li_document');
		$this->load->model('document/Document_model', 'M_document');

		$_land_inventory_documents	=	$this->M_li_document->find_all(FALSE, ['document_id']); #vd($_land_inventory_documents);
		if ( $_land_inventory_documents ) {

			foreach ( $_land_inventory_documents as $key => $_lid ) {

				if ( isset($_lid->document_id) && $_lid->document_id ) {

					$_docuemnt_ids[]	=	$_lid->document_id;
				}
			}
		}

		$_response	=	[];

		if ( isset($_docuemnt_ids) && !empty($_docuemnt_ids) ) {

			$_total['_displays']	=	0;
			$_total['_records']		=	0;

			$_columns	=	[
										'id'	=>	TRUE,
										'name'	=>	TRUE,
										'description'	=>	TRUE,
										'owner_id'	=>	TRUE,
										'created_at'	=>	TRUE,
										'status_id'	=>	TRUE,
									];

			if ( isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef']) ) {

				$_columns	=	[];

				foreach ( $_REQUEST['columnsDef'] as $_dkey => $_def ) {

					$_columns[$_def]	=	TRUE;
				}
			}

			$_process_checklists	=	$this->M_document->where('id', $_docuemnt_ids)->as_array()->get_all(); #vd($_process_checklists);
			if ( $_process_checklists ) {

				$_datas	=	[];

				foreach ( $_process_checklists as $_ckkey => $_ck ) {

					$_datas[]	=	$this->filterArray($_ck, $_columns);
				}

				$_total['_displays']	=	$_total['_records']	=	count($_datas);

				if ( isset($_REQUEST['search']) ) {

					$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

					$_total['_displays']	=	$_datas ? count($_datas) : 0;
				}

				if ( isset($_REQUEST['columns']) && is_array($_REQUEST['columns']) ) {

					foreach ( $_REQUEST['columns'] as $_ckey => $_clm ) {

						if ( $_clm['search'] ) {

							$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

							$_total['_displays']	=	$_datas ? count($_datas) : 0;
						}
					}
				}

				if ( isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir'] ) {

					$_column	=	$_REQUEST['order'][0]['column'];
					$_dir			=	$_REQUEST['order'][0]['dir'];

					usort( $_datas, function ( $x, $y ) use ( $_column, $_dir ) {

						$x	=	array_slice($x, $_column, 1);
						$x	=	array_pop($x);

						$y	=	array_slice($y, $_column, 1);
						$y	=	array_pop($y);

						if ( $_dir === 'asc' ) {

							return $x > $y ? TRUE : FALSE;
						} else {

							return $x < $y ? TRUE : FALSE;
						}
					});
				}

				if ( isset($_REQUEST['length']) ) {

					$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
				}

				if ( isset($_REQUEST['array_values']) && $_REQUEST['array_values'] ) {

					$_temp	=	$_datas;
					$_datas	=	[];

					foreach ( $_temp as $key => $_tmp ) {

						$_datas[]	=	array_values($_tmp);
					}
				}

				$_secho	=	0;
				if ( isset($_REQUEST['sEcho']) ) {

					$_secho	=	intval($_REQUEST['sEcho']);
				}

				$_response	=	[
												'iTotalDisplayRecords'	=>	$_total['_displays'],
												'iTotalRecords'					=>	$_total['_records'],
												'sColumns'							=>	'',
												'sEcho'									=>	$_secho,
												'data'									=>	$_datas
											];
			}
		}

		echo json_encode($_response);

		exit();
	}

	function process_upload () {

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'Oops! Please refresh the page and try again.';

		sleep(1);

		$_docx_id	=	$this->input->post('_id');
		if ( $_docx_id && isset($_FILES['_file']['name']) && ($_FILES['_file']['name'] !== '') ) {

			$_ext	=	['.jpg', '.jpeg', '.png'];

			set_time_limit(0);

			unset($_config);
			$_config['upload_path']	=	$_location	=	'assets/img/_documents/_process';
			$_config['allowed_types'] = 'jpeg|jpg|png';
			$_config['overwrite'] = TRUE;
			$_config['max_size'] = '1000000';
			$_config['file_name'] = $_filename = $_docx_id.'_thumb';

			if ( isset($_ext) && $_ext ) {

				$_image	=	$_location.'/'.$_filename;

				foreach ( $_ext as $key => $x ) {

					if ( file_exists($_image.$x) ) {

						unlink($_image.$x);
					}
				}
			}

			$this->load->library('upload', $_config);

			if ( !$this->upload->do_upload('_file') ) {

				$_respo['_msg']	=	$this->upload->displaY_errors();
			} else {

				// vd($this->upload->data());

				$_respo['_status']	=	1;
				$_respo['_msg']			=	'Success';
			}
		}

		echo json_encode($_respo);

		exit();
	}
	
}