<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
$is_admin = isset($data['is_admin']) && $data['is_admin'] ? 'Yes' : 'No';
$module_permissions = isset($data['module_permissions']) && $data['module_permissions'] ? $data['module_permissions'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
  <div class="kt-container kt-container--fluid">
    <div class="kt-subheader__main">
      <h3 class="kt-subheader__title">Permissions</h3>
    </div>
    <div class="kt-subheader__toolbar">
      <div class="kt-subheader__wrapper">
        <a href="<?php echo site_url('permission/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
          <i class="fa fa-edit"></i> Edit Permission
        </a>   
        <a href="<?php echo site_url('module_permissions/form/'.$id.'/1');?>" class="btn btn-label-success btn-elevate btn-sm">
            <i class="fa fa-edit"></i> Edit Modules
        </a>
        <a href="<?php echo site_url('permission');?>" class="btn btn-label-instagram btn-sm btn-elevate">
          <i class="fa fa-reply"></i> Back
        </a>
      </div>
    </div>
  </div>
</div>

<!-- begin:: Content -->
<div
  class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
  <div class="row">
    <div class="col-md-6">
      <!--begin::Portlet-->
      <div class="kt-portlet">
        <div class="kt-portlet__body">
          <!--begin::Portlet-->
          <div class="kt-portlet">
            <div class="kt-portlet__head">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                  General Information
                </h3>
              </div>
            </div>
            <div class="kt-portlet__body">
              <!--begin::Form-->
              <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Name
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$name?></span>
                </div>
              <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Is admin
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$is_admin?></span>
                </div>
                <!-- ==================== begin: Add fields details  ==================== -->

                <!-- ==================== end: Add model details ==================== -->
              </div>
              <!--end::Form-->
            </div>
          </div>
          <!--end::Portlet-->
        </div>
      </div>
      <!--end::Portlet-->

      <div class="kt-portlet">
        <div class="kt-portlet__body">
          <!--begin::Portlet-->
          <div class="kt-portlet">
            <div class="kt-portlet__head">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Module Permissions </h3>
              </div>
            </div>
            <div class="kt-portlet__body">
              <!--begin::Table-->
              <table
                class="table table-striped- table-bordered table-hover table-checkable"
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Create</th>
                    <th>Read</th>
                    <th>Update</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if($module_permissions):?>
                        <?php foreach($module_permissions as $key => $value): ?>
                            <tr>
                                <td><?=$value['module_id']?></td>
                                <td><?=$value['module_name']?></td>
                                <td><?=$value['is_create'] ? '<span class="text-success"><i class="flaticon2-check-mark"></i></span>' : '<span class="text-danger"><i class="flaticon2-cross"></i></span>'?></td>
                                <td><?=$value['is_read'] ? '<span class="text-success"><i class="flaticon2-check-mark"></i></span>' : '<span class="text-danger"><i class="flaticon2-cross"></i></span>'?></td>
                                <td><?=$value['is_update'] ? '<span class="text-success"><i class="flaticon2-check-mark"></i></span>' : '<span class="text-danger"><i class="flaticon2-cross"></i></span>'?></td>
                                <td><?=$value['is_delete'] ? '<span class="text-success"><i class="flaticon2-check-mark"></i></span>' : '<span class="text-danger"><i class="flaticon2-cross"></i></span>'?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <tr>
                        <td colspan="6">
                            <div class="kt-portlet kt-callout">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title text-center">No Records Found</h3>
                                            <p class="kt-callout__desc text-center">
                                                Sorry no record were found.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endif;?>
                </tbody>
                </tbody>
              </table>

              <!--end::Table-->
            </div>
          </div>
          <!--end::Portlet-->
        </div>
      </div>
    </div>

    <div class="col-md-6"></div>
  </div>
</div>
<!-- begin:: Footer -->
