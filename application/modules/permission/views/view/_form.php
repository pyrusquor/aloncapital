<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$is_admin = isset($info['is_admin']) && $info['is_admin'] ? $info['is_admin'] : '';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="name"
                   value="<?php echo set_value('name', $name); ?>" placeholder="Name"
                   autocomplete="off" id="name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Administrator <span class="kt-font-danger">*</span></label>
            <select class="form-control" name="is_admin"
                    value="<?php echo $is_admin; ?>" placeholder="Type" autocomplete="off"
                    id="is_admin">
                <option value="">Select option</option>
                <option value="0" <?php if($is_admin === "0"): ?>selected<?php endif;?>>No</option>
                <option value="1" <?php if($is_admin === "1"): ?>selected<?php endif;?>>Yes</option>
            </select>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>