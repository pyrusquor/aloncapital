<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Permission_model extends MY_Model
{
    public $table = 'permissions'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'name',
        'is_admin',
        'is_active',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
    public $fields = [
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'is_admin',
            'label' => 'Admin',
            'rules' => 'trim',
        ),
        array(
            'field' => 'is_active',
            'label' => 'Active',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert']	=	$this->fields;
        $this->rules['update']	=	$this->fields;

        $this->has_many['module_permissions'] = array('foreign_model' => 'module_permissions/module_permissions_model', 'foreign_table' => 'model_permissions', 'foreign_key' => 'permission_id', 'local_key' => 'id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}