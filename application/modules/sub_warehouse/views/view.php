<script type="text/javascript">
    window.current_user_id = "<?php echo current_user_id();?>";
    window.object_id = "<?php echo $data['id'];?>";
    window.parent_id = "<?php echo $data['warehouse_id'];?>";

</script>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Sub Warehouse</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="
                    <?php echo site_url('sub_warehouse/form/'.$data['id']);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('sub_warehouse'); ?>"
                   class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__body">

            <!--begin::Portlet-->
            <div class="kt-widget13 row">

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Name
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['name']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Warehouse Code
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['warehouse_code']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Main Warehouse
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['warehouse']['name']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Telephone Number
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['telephone_number']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Fax Number
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['fax_number']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Mobile Number
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['mobile_number']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Address
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['address']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Contact Person
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['contact_person']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Email
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['email']; ?>
                    </span>
                </div>

                <div class="kt-widget13__item col-sm-6 col-md-3">
                    <span class="kt-widget13__desc">
                        Status
                    </span>
                    <span class="kt-widget13__text kt-widget13__text--bold">
                        <?php echo $data['is_active']; ?>
                    </span>
                </div>


                <!-- ==================== end: Add model details ==================== -->
            </div>
            <!--end::Portlet-->
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-widget13__item col-sm-6 col-md-3">
                            <span class="kt-widget13__desc">
                                Created
                            </span>
                <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['created_at']; ?> by <?php echo get_person_name($data['created_by'], 'staff');?></span>
            </div>
            <div class="kt-widget13__item col-sm-6 col-md-3">
                            <span class="kt-widget13__desc">
                                Updated
                            </span>
                <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['updated_at']; ?> by <?php echo get_person_name($data['updated_by'], 'staff');?></span>
            </div>
        </div>
    </div>
    <!--end::Portlet-->


</div>
<!-- begin:: Footer -->
<div id="sub_warehouse_inventory_content">
    <div class="kt-container my-3  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

        <div class="module__cta">
            <div class="kt-container  kt-container--fluid ">
                <div class="module__create">
                    <a href="<?php echo site_url('sub_warehouse_inventory/form?warehouse_id=' . $data['id']); ?>"
                       class="btn btn-label-primary btn-elevate btn-sm">
                        <i class="fa fa-plus"></i> Create Warehouse Inventory
                    </a>
                </div>

                <div class="module__filter">
                    <button class="btn btn-secondary btn-elevate btn-sm" id="resetFilters">
                        <i class="fa fa-refresh"></i> Reset
                    </button>
                    <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                        <i class="fa fa-filter"></i> Filter
                    </button>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <div v-if="warehouse_items.loading">
            <div class="spinner-border text-success" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <table class="table table-striped- table-bordered table-hover" id="warehouse_inventory_table">
            <thead>
            <tr>
                <!-- ==================== begin: Add header fields ==================== -->
                <th>Item</th>
                <th>Unit of Measurement</th>
                <th>Actual Quantity</th>
                <th>Available Quantity</th>
                <th>Unit Price</th>
                <th>Actions</th>
                <!-- ==================== end: Add header fields ==================== -->
            </tr>
            </thead>
            <tbody>
            <tr v-for="(wi, idx) in warehouse_items.data">
                <td>{{ wi.item.name }}</td>
                <td>{{ wi.unit_of_measurement.name }}</td>
                <td>{{ wi.actual_quantity }}</td>
                <td>{{ wi.available_quantity }}</td>
                <td>{{ wi.unit_price }}</td>
                <td>
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                                <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#!" @click.prevent="prepTransfer(wi.id, wi.item_id, 'warehouse_transfer')">Return to Parent Warehouse</a>
                            </div>
                        </span>
                </td>
            </tr>
            </tbody>
        </table>
        <!--end: Datatable -->

        <!--begin: Modals -->
        <?php $this->load->view('modals/warehouse_inventory/filter');?>
        <?php $this->load->view('modals/warehouse_inventory/transfer__other_warehouse');?>
        <?php $this->load->view('modals/warehouse_inventory/transfer__sub_warehouse');?>
        <!--end: Modals -->
    </div>

</div>