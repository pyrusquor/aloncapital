<div class="row">


    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">

                <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                       v-model="info.form.name">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Warehouse Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">

                <input type="text" class="form-control" id="warehouse_code" name="warehouse_code"
                       placeholder="Warehouse Code" v-model="info.form.warehouse_code">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Main Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <select class="form-control suggests" data-module="warehouses" id="warehouse_id"
                        name="id"">
                    <?php if ($warehouse): ?>
                        <option value="<?php echo $warehouse['id']; ?>"
                                selected><?php echo $warehouse['name']; ?></option>
                    <?php endif ?>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Telephone Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <input type="number" min="0" step="0.01" class="form-control" id="telephone_number"
                       name="telephone_number" placeholder="Telephone Number" v-model="info.form.telephone_number">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Fax Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <input type="number" min="0" step="0.01" class="form-control" id="fax_number" name="fax_number"
                       placeholder="Fax Number" v-model="info.form.fax_number">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Mobile Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <input type="number" min="0" step="0.01" class="form-control" id="mobile_number" name="mobile_number"
                       placeholder="Mobile Number" v-model="info.form.mobile_number">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Address <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">

                <input type="text" class="form-control" id="address" name="address" placeholder="Address"
                       v-model="info.form.address">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Contact Person <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">

                <input type="text" class="form-control" id="contact_person" name="contact_person"
                       placeholder="Contact Person" v-model="info.form.contact_person">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Email <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                       v-model="info.form.email">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Status <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), null, 'class="form-control" v-model="info.form.is_active"'); ?>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


</div>