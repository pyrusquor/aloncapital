<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Initial_sub_warehouses extends CI_Migration
    {
        protected $tbl = "sub_warehouses";
        protected $fields = array(

            "id" => array(

                "type" => "INT",


                "constraint" => "11",


                "unsigned" => True,


                "auto_increment" => True,


                "NOT NULL" => True,


            ),

            "created_by" => array(

                "type" => "INT",


                "NOT NULL" => True,


                "unsigned" => True,


            ),

            "created_at" => array(

                "type" => "DATETIME",


                "NOT NULL" => True,


            ),

            "updated_by" => array(

                "type" => "INT",


                "NOT NULL" => True,


                "unsigned" => True,


            ),

            "updated_at" => array(

                "type" => "DATETIME",


                "NOT NULL" => True,


            ),

            "deleted_by" => array(

                "type" => "INT",


                "NOT NULL" => True,


                "unsigned" => True,


            ),

            "deleted_at" => array(

                "type" => "DATETIME",


                "NOT NULL" => True,


            ),

            "name" => array(

                "type" => "VARCHAR",


                "constraint" => "255",


                "NOT NULL" => True,


            ),

            "warehouse_code" => array(

                "type" => "VARCHAR",


                "constraint" => "32",


                "NOT NULL" => True,


            ),

            "warehouse_id" => array(

                "type" => "INT",


                "constraint" => "11",


                "NOT NULL" => True,


            ),

            "telephone_number" => array(

                "type" => "VARCHAR",


                "constraint" => "15",


                "NOT NULL" => True,


            ),

            "fax_number" => array(

                "type" => "VARCHAR",


                "constraint" => "15",


                "NOT NULL" => True,


            ),

            "mobile_number" => array(

                "type" => "VARCHAR",


                "constraint" => "15",


                "NOT NULL" => True,


            ),

            "address" => array(

                "type" => "VARCHAR",


                "constraint" => "512",


                "NOT NULL" => True,


            ),

            "contact_person" => array(

                "type" => "VARCHAR",


                "constraint" => "255",


                "NOT NULL" => True,


            ),

            "email" => array(

                "type" => "VARCHAR",


                "constraint" => "512",


                "NOT NULL" => True,


            ),

            "is_active" => array(

                "type" => "BOOL",


                "NOT NULL" => True,


                "default" => True,


            ),

        );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }

    }

    public function down()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
