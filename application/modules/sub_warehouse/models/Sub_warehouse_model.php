<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Sub_warehouse_model extends MY_Model
    {
        public $table = 'sub_warehouses'; // you MUST mention the table name
        public $primary_key = 'id';
        public $fillable = [
            "address",
            "contact_person",
            "created_at",
            "created_by",
            "deleted_at",
            "deleted_by",
            "email",
            "fax_number",
            "id",
            "is_active",
            "mobile_number",
            "name",
            "telephone_number",
            "updated_at",
            "updated_by",
            "warehouse_code",
            "warehouse_id",

        ];
        public $form_fillables = [
            "id",
            "address",
            "contact_person",
            "email",
            "fax_number",
            "is_active",
            "mobile_number",
            "name",
            "telephone_number",
            "warehouse_code",
            "warehouse_id",

        ];
        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [

            "address" => array(
                "field" => "address",
                "label" => "Address",
                "rules" => "trim|required"
            ),

            "contact_person" => array(
                "field" => "contact_person",
                "label" => "Contact Person",
                "rules" => "trim|required"
            ),


            "email" => array(
                "field" => "email",
                "label" => "Email",
                "rules" => "trim|required|valid_email"
            ),

            "fax_number" => array(
                "field" => "fax_number",
                "label" => "Fax Number",
                "rules" => "trim|required|is_natural"
            ),


            "is_active" => array(
                "field" => "is_active",
                "label" => "Status",
                "rules" => "required"
            ),

            "mobile_number" => array(
                "field" => "mobile_number",
                "label" => "Mobile Number",
                "rules" => "trim|required|is_natural"
            ),

            "name" => array(
                "field" => "name",
                "label" => "Name",
                "rules" => "required"
            ),

            "telephone_number" => array(
                "field" => "telephone_number",
                "label" => "Telephone Number",
                "rules" => "trim|required|is_natural"
            ),


            "warehouse_code" => array(
                "field" => "warehouse_code",
                "label" => "Warehouse Code",
                "rules" => "required"
            ),

            "warehouse_id" => array(
                "field" => "warehouse_id",
                "label" => "Main Warehouse",
                "rules" => "required"
            ),

        ];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = true;
            $this->return_as = 'array';

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;

            $this->has_one["warehouse"] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouses', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');


        }

        function get_columns()
        {
            $_return = FALSE;

            if ($this->fillable) {
                $_return = $this->fillable;
            }

            return $_return;
        }

        public function insert_dummy()
        {
            require APPPATH . '/third_party/faker/autoload.php';
            $faker = Faker\Factory::create();

            $data = [];

            for ($x = 0; $x < 10; $x++) {
                array_push($data, array(
                    'name' => $faker->word,
                ));
            }
            $this->db->insert_batch($this->table, $data);

        }

    }