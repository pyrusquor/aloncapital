<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Material_receiving_item_model extends MY_Model
{
    public $table = 'material_receiving_items'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'id',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
        'supplier_id',
        'material_receiving_id',
        'material_request_id',
        'purchase_order_id',
        'purchase_order_item_id',
        'purchase_order_request_item_id',
        'purchase_order_request_id',
        'warehouse_id',
        // items
        'item_group_id',
        'item_type_id',
        'item_brand_id',
        'item_class_id',
        'item_id',
        'unit_of_measurement_id',
        'item_brand_name',
        'item_class_name',
        'item_name',
        'unit_of_measurement_name',
        'item_group_name',
        'item_type_name',
        'unit_cost',
        'total_cost',
        'freight_cost',
        'quantity',
        'status',
        'expiration_date'
    ];

    public $form_fillables = [
        'id',
        'supplier_id',
        'material_receiving_id',
        'material_request_id',
        'purchase_order_id',
        'purchase_order_item_id',
        'purchase_order_request_item_id',
        'purchase_order_request_id',
        'warehouse_id',
        // items
        'item_group_id',
        'item_type_id',
        'item_brand_id',
        'item_class_id',
        'item_id',
        'unit_of_measurement_id',
        'item_brand_name',
        'item_class_name',
        'item_name',
        'unit_of_measurement_name',
        'item_group_name',
        'item_type_name',
        'unit_cost',
        'total_cost',
        'freight_cost',
        'quantity',
        'status',
        'expiration_date'
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
        $this->has_one['item_group'] = array('foreign_model' => 'item_group/item_group_model', 'foreign_table' => 'item_groups', 'foreign_key' => 'id', 'local_key' => 'item_group_id');
        $this->has_one['item_type'] = array('foreign_model' => 'item_type/item_type_model', 'foreign_table' => 'item_types', 'foreign_key' => 'id', 'local_key' => 'item_type_id');
        $this->has_one['item_brand'] = array('foreign_model' => 'item_brand/item_brand_model', 'foreign_table' => 'item_brands', 'foreign_key' => 'id', 'local_key' => 'item_brand_id');
        $this->has_one['item_class'] = array('foreign_model' => 'item_class/item_class_model', 'foreign_table' => 'item_classs', 'foreign_key' => 'id', 'local_key' => 'item_class_id');
        $this->has_one['item'] = array('foreign_model' => 'item/item_model', 'foreign_table' => 'items', 'foreign_key' => 'id', 'local_key' => 'item_id');
        $this->has_one['unit_of_measurement'] = array('foreign_model' => 'inventory_settings_unit_of_measurement/inventory_settings_unit_of_measurement_model', 'foreign_table' => 'inventory_settings_units_of_measurement', 'foreign_key' => 'id', 'local_key' => 'unit_of_measurement_id');
        $this->has_one['supplier'] = array('foreign_model' => 'suppliers/suppliers_model', 'foreign_table' => 'suppliers', 'foreign_key' => 'id', 'local_key' => 'supplier_id');
        $this->has_one['material_request'] = array('foreign_model' => 'material_request/material_request_model', 'foreign_table' => 'material_requests', 'foreign_key' => 'id', 'local_key' => 'material_request_id');
        $this->has_one['material_receiving'] = array('foreign_model' => 'material_receiving/material_receiving_model', 'foreign_table' => 'material_receivings', 'foreign_key' => 'id', 'local_key' => 'material_receiving_id');
        $this->has_one['purchase_order'] = array('foreign_model' => 'purchase_order/purchase_order_model', 'foreign_table' => 'purchase_orders', 'foreign_key' => 'id', 'local_key' => 'purchase_order_id');
        $this->has_one['purchase_order_request_item'] = array('foreign_model' => 'purchase_order_request_item/purchase_order_request_item_model', 'foreign_table' => 'purchase_order_request_items', 'foreign_key' => 'id', 'local_key' => 'purchase_order_request_item_id');
        $this->has_one['purchase_order_request'] = array('foreign_model' => 'purchase_order_request/purchase_order_request_model', 'foreign_table' => 'purchase_order_requests', 'foreign_key' => 'id', 'local_key' => 'purchase_order_request_id');
        $this->has_one['warehouse'] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouse', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
