<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Communication_settings extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Communication_setting_model', 'M_communication_setting');

        $this->load->helper('form');

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']);// Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_communication_setting->fillable;
        $this->_table_columns = $this->M_communication_setting->__get_columns();
    }

    public function get_all() {
        $data = $this->M_communication_setting
            ->fields(['name', 'days_to_send', 'days_to_send_type', 'period_id', 'setting_slug', 'sms', 'email'])
            ->with_sms_template('fields: name')
            ->with_email_template('fields: name')
            ->as_array()
            ->get_all();
        
        foreach($data as $key => $value) {            
            $result[$key]['name'] = $value['name'];
            $result[$key]['days_to_send'] = $value['days_to_send']; 
            $result[$key]['days_to_send_type'] = $value['days_to_send_type'];
            $result[$key]['period_id'] = $value['period_id'];
            $result[$key]['setting_slug'] = $value['setting_slug'];
            $result[$key]['sms'] = $value['sms'];
            $result[$key]['sms_template'] = isset($value['sms_template']['name']) && $value['sms_template']['name'] ? $value['sms_template']['name'] : 'N/A';
            $result[$key]['email'] = $value['email'];
            $result[$key]['email_template'] = isset($value['email_template']['name']) && $value['email_template']['name'] ? $value['email_template']['name'] : 'N/A';
        }

        $output = array(
            'data' => $result,
        );
        echo json_encode($output);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // Get record count
        // $conditions['returnType'] = 'count';
        $this->view_data['totalRec'] = $totalRec = $this->M_communication_setting->count_rows();

        // Update Buyer Details
        $this->view_data['update_buyer_details'] = $this->M_communication_setting
            ->with_sms_template()
            ->with_email_template()
            ->where(['setting_slug' => 'update_buyer_details', 'period_id' => '0'])->get_all();
        
        // Document Follow Up
        $this->view_data['document_followup'] = $this->M_communication_setting
            ->with_sms_template()
            ->with_email_template()
            ->where(['setting_slug' => 'document_followup', 'period_id' => '0'])->get_all();
        
        // reservation
        $this->view_data['reservation'] = $this->M_communication_setting
            ->with_sms_template()
            ->with_email_template()
            ->where(['setting_slug' => 'reservation', 'period_id' => '1'])->get_all();

        // Get records
        $this->view_data['records'] = $this->M_communication_setting
            ->limit($this->perPage, 0)
            ->get_all();
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showCommunicationSettings()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'name' => true,
            'days_to_send' => true,
            'days_to_send_type' => true,
            'period_id' => true,
            'setting_slug' => true,
            'sms' => true,
            'sms_template_id' => true,
            'email' => true,
            'email_template_id' => true,
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $communication_settings = $this->M_communication_setting->order_by('id', 'DESC')->as_array()->get_all();
        $data = [];

        if ($communication_settings) {

            foreach ($communication_settings as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );

        }

        echo json_encode($output);
        exit();
    }

    public function form($id = false,$slug = '')
    {
        $method = "Create";
        if ($id) {$method = "Update";}

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_communication_setting->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                if ($id) {
                    // TODO: Update communication settings by id and setting_slug
                    // Create an array
                    // Loop through each data and update
                    
                    vdebug($this->input->post());

                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    // $info['setting_slug'] = slugify($info['name']);
                    $communication_setting_status = $this->M_communication_setting->update($additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    // $info['setting_slug'] = slugify($info['name']);
                    $communication_setting_status = $this->M_communication_setting->insert($additional);
                }

                if($communication_setting_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Settings Successfully ' . $method . 'd!';
                }                
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $w['period_id'] = $id;
            $w['setting_slug'] = $slug;
            $this->view_data['results'] = $this->M_communication_setting->where($w)->get_all();
            $this->view_data['period_id'] = $id;

            // vdebug($this->view_data['info']);
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_communication_setting->get($id);
            if ($list) {

                $deleted = $this->M_communication_setting->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Settings Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('communication_settings', $data, array('id' => $value));
                    $deleted = $this->M_communication_setting->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Settings Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false, $setting_slug = "")
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_communication_setting->where('setting_slug', $setting_slug)->get($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import() {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach($sheetData as $key => $upload_data)
        {

            if($key > 0)
            {

                if ($this->input->post('status') == '1'){
                    $fields = array(
                        /* ==================== begin: Add model fields ==================== */
                        'name' => $upload_data[0],
                        'days_to_send' => $upload_data[1],
                        'days_to_send_type' => $upload_data[2],
                        'period_id' => $upload_data[3],
                        'setting_slug' => $upload_data[4],
                        'sms' => $upload_data[5],
                        'sms_template_id' => $upload_data[6],
                        'email' => $upload_data[7],
                        'email_template_id' => $upload_data[8],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $communication_setting_id = $upload_data[0];
                    $communication_setting = $this->M_communication_setting->get($communication_setting_id);

                    if($communication_setting){
                        $result = $this->M_communication_setting->update($fields, $communication_setting_id);
                    }

                } else {

                    if( ! is_numeric($upload_data[0]))
                    {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[1],
                            'days_to_send' => $upload_data[2],
                            'days_to_send_type' => $upload_data[3],
                            'period_id' => $upload_data[4],
                            'setting_slug' => $upload_data[5],
                            'sms' => $upload_data[6],
                            'sms_template_id' => $upload_data[7],
                            'email' => $upload_data[8],
                            'email_template_id' => $upload_data[9],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_communication_setting->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'name' => $upload_data[0],
                            'days_to_send' => $upload_data[1],
                            'days_to_send_type' => $upload_data[2],
                            'period_id' => $upload_data[3],
                            'setting_slug' => $upload_data[4],
                            'sms' => $upload_data[5],
                            'sms_template_id' => $upload_data[6],
                            'email' => $upload_data[7],
                            'email_template_id' => $upload_data[8],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_communication_setting->insert($fields);
                    }

                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if($err == 0)
        {
            $this->notify->success('CSV successfully imported.', 'communication_settings');
        }
        else{
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: '. base_url().'communication_settings');
        die();
    }

    public function export_csv()
    {
        if ( $this->input->post() && ($this->input->post('update_existing_data') !== '') ) {

            $_ued	=	$this->input->post('update_existing_data');

            $_is_update	=	$_ued === '1' ? TRUE : FALSE;

            $_alphas		=	[];
            $_datas			=	[];

            $_titles[]	=	'id';

            $_start	=	3;
            $_row		=	2;

            $_filename	=	'Communication Settings CSV Template.csv';

            $_fillables	=	$this->_table_fillables;
            if ( !$_fillables ) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'communication_settings');
            }

            foreach ( $_fillables as $_fkey => $_fill ) {

                if ( (strpos( $_fill, 'created_') === FALSE) && (strpos( $_fill, 'updated_') === FALSE) && (strpos( $_fill, 'deleted_') === FALSE) ) {

                    $_titles[]	=	$_fill;
                } else {

                    continue;
                }
            }

            if ( $_is_update ) {

                $_group	=	$this->M_communication_setting->as_array()->get_all();
                if ( $_group ) {

                    foreach ( $_titles as $_tkey => $_title ) {

                        foreach ( $_group as $_dkey => $li ) {

                            $_datas[$li['id']][$_title]	=	isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if ( isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id' ) {

                    unset($_titles[0]);
                }
            }

            $_alphas			=	$this->__get_excel_columns(count($_titles));
            $_xls_columns	=	array_combine($_alphas, $_titles);
            $_firstAlpha	=	reset($_alphas);
            $_lastAlpha		=	end($_alphas);

            $_objSheet	=	$this->excel->getActiveSheet();
            $_objSheet->setTitle('Communication Settings');
            $_objSheet->setCellValue('A1', 'Communication Settings');
            $_objSheet->mergeCells('A1:'.$_lastAlpha.'1');

            foreach ( $_xls_columns as $_xkey => $_column ) {

                $_objSheet->setCellValue($_xkey.$_row, $_column);
            }

            if ( $_is_update ) {

                if ( isset($_datas) && $_datas ) {

                    foreach ( $_datas as $_dkey => $_data ) {

                        foreach ( $_alphas as $_akey => $_alpha ) {

                            $_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha.$_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha.$_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha.$_start.':'.$_lastAlpha.$_start);

                    $_style	=	array(
                        'font'  => array(
                            'bold'	=>	FALSE,
                            'size'	=>	9,
                            'name'	=>	'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha.$_start.':'.$_lastAlpha.$_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ( $_alphas as $_alpha ) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style	=	array(
                'font'  => array(
                    'bold'	=>	TRUE,
                    'size'	=>	10,
                    'name'	=>	'Verdana'
                )
            );
            $_objSheet->getStyle('A1:'.$_lastAlpha.$_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="'.$_filename.'"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export() {

        $_db_columns	=	[];
        $_alphas			=	[];
        $_datas				=	[];

        $_titles[]	=	'#';

        $_start	=	3;
        $_row		=	2;
        $_no		=	1;

        $communication_settings	=	$this->M_communication_setting->as_array()->get_all();
        if ( $communication_settings ) {

            foreach ( $communication_settings as $_lkey => $communication_setting ) {

                $_datas[$communication_setting['id']]['#']	=	$_no;

                $_no++;
            }

            $_filename	=	'list_of_communication_settings_'.date('m_d_y_h-i-s',time()).'.xls';

            $_objSheet	=	$this->excel->getActiveSheet();

            if ( $this->input->post() ) {

                $_export_column	=	$this->input->post('_export_column');
                if ( $_export_column ) {

                    foreach ( $_export_column as $_ekey => $_column ) {

                        $_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'communication_settings');
                }
            } else {

                $_filename	=	'list_of_communication_settings'.date('m_d_y_h-i-s',time()).'.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns	=	$this->_table_fillables;
                if ( !$_db_columns ) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'communication_settings');
                }
            }

            if ( $_db_columns ) {

                foreach ( $_db_columns as $key => $_dbclm ) {

                    $_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ( (strpos( $_name, 'created_') === FALSE) && (strpos( $_name, 'updated_') === FALSE) && (strpos( $_name, 'deleted_') === FALSE) && ($_name !== 'id') ) {

                        if ( (strpos( $_name, '_id') !== FALSE) ) {

                            $_column	=	$_name;

                            $_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        } elseif ( (strpos( $_name, 'is_') !== FALSE) ) {

                            $_column	=	$_name;

                            $_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ( $communication_settings as $_lkey => $communication_setting ) {

                                $_datas[$communication_setting['id']][$_title]	=	isset($communication_setting[$_column]) && ($communication_setting[$_column] !== '') ? Dropdown::get_static('bool', $communication_setting[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ( $communication_settings as $_lkey => $communication_setting ) {

                                if ( $_name === 'status' ) {

                                    $_datas[$communication_setting['id']][$_title]	=	isset($communication_setting[$_name]) && $communication_setting[$_name] ? Dropdown::get_static('inventory_status', $communication_setting[$_name], 'view') : '';
                                } else {

                                    $_datas[$communication_setting['id']][$_title]	=	isset($communication_setting[$_name]) && $communication_setting[$_name] ? $communication_setting[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas	=	$this->__get_excel_columns(count($_titles));

                $_xls_columns	=	array_combine($_alphas, $_titles);
                $_firstAlpha	=	reset($_alphas);
                $_lastAlpha		=	end($_alphas);

                foreach ( $_xls_columns as $_xkey => $_column ) {

                    $_title	=	($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey.$_row, $_title);
                }

                $_objSheet->setTitle('List of Communication Settings');
                $_objSheet->setCellValue('A1', 'LIST OF Communication Settings');
                $_objSheet->mergeCells('A1:'.$_lastAlpha.'1');

                if ( isset($_datas) && $_datas ) {

                    foreach ( $_datas as $_dkey => $_data ) {

                        foreach ( $_alphas as $_akey => $_alpha ) {

                            $_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha.$_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha.$_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha.$_start.':'.$_lastAlpha.$_start);

                    $_style	=	array(
                        'font'  => array(
                            'bold'	=>	FALSE,
                            'size'	=>	9,
                            'name'	=>	'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha.$_start.':'.$_lastAlpha.$_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ( $_alphas as $_alpha ) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style	=	array(
                    'font'  => array(
                        'bold'	=>	TRUE,
                        'size'	=>	10,
                        'name'	=>	'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:'.$_lastAlpha.$_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="'.$_filename.'"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'communication_settings');
            }
        } else {

            $this->notify->error('No Record Found', 'communication_settings');
        }
    }

}