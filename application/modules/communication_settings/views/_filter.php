<?php if (!empty($records)) : ?>
    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Overdue Payments 
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                    <i class="flaticon-more-1 kt-font-brand"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <ul class="kt-nav">
                        <?php if($this->ion_auth->is_admin()): ?>
                            <li class="kt-nav__item">
                                <a href="<?php echo base_url('communication_settings/form/1/overdue_payments')?>" id="overdue_payment_update" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-edit"></i>
                                    <span class="kt-nav__link-text">Update</span>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="kt-nav__item">
                                <a href="#" id="overdue_payment_view" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                    <span class="kt-nav__link-text">View</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-widget13">
                <div class="kt-widget13__item">
                    <input type="hidden" id="communication_settings_data">
                    <div class="kt-widget13__text kt-widget13__text--bold mr-3">Period of Payment </div>
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-secondary active">
                            <input type="radio" name="overdue_payment_period" id="overdue_period" value="1" autocomplete="off" checked> Reservation
                        </label>
                        <label class="btn btn-secondary">
                            <input type="radio" name="overdue_payment_period" id="overdue_period" value="2" autocomplete="off"> Downpayment
                        </label>
                        <label class="btn btn-secondary">
                            <input type="radio" name="overdue_payment_period" id="overdue_period" value="3" autocomplete="off"> Loan Takeout
                        </label>
                    </div>
                    
                </div>
                <div class="kt-widget13__item">
                    <table id="overdue_payment_table" class="table table-borderless">

                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->

    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Upcoming Payments
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                    <i class="flaticon-more-1 kt-font-brand"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <ul class="kt-nav">
                        <?php if($this->ion_auth->is_admin()): ?>
                            <li class="kt-nav__item">
                                <a href="<?php echo base_url('communication_settings/form/1/upcoming_payments')?>" id="upcoming_payment_update" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-edit"></i>
                                    <span class="kt-nav__link-text">Update</span>
                                </a>
                            </li>
                        <?php else: ?>
                            <li class="kt-nav__item">
                                <a href="#" id="upcoming_payment_view" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                    <span class="kt-nav__link-text">View</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-widget13">
                <div class="kt-widget13__item">
                    <div class="kt-widget13__text kt-widget13__text--bold mr-3">Period of Payment </div>
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-secondary active">
                            <input type="radio" name="upcoming_payment" id="upcoming_payment" value="2" autocomplete="off" checked> Downpayment
                        </label>
                        <label class="btn btn-secondary">
                            <input type="radio" name="upcoming_payment" id="upcoming_payment" value="3" autocomplete="off"> Loan Takeout
                        </label>
                    </div>
                </div>
                <div class="kt-widget13__item">
                    <table id="upcoming_payment_table" class="table table-borderless">

                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->

    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Update of Details
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-widget13__item">
                <table class="table table-borderless">
                    <tbody>
                        <?php foreach($update_buyer_details as $key => $value): ?>
                        <tr>
                            <td style="width: 15%;"><?=$value['name']?></td>
                            <td style="width: 15%;"><?=$value['days_to_send']?> Day(s) <?=$value['days_to_send_type']?> Due Date</td>
                            <td style="width: 10%;">
                                <input type="checkbox" name="sms_switch" class="btn-switch" <?php if($value['email']): ?>checked <?php endif;?>>
                            </td>
                            <td style="width: 25%;"><?=@$value['sms_template']['title']?></td>
                            <td style="width: 10%;">
                                <input type="checkbox" name="email_switch" class="btn-switch" <?php if($value['email']): ?>checked <?php endif;?>>
                            </td>
                            <td style="width: 25%;"><?=@$value['email_template']['title']?></td>
                        </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->

    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Follow Up on Documents
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-widget13">
                <div class="kt-widget13__item">
                    <table class="table table-borderless">
                        <tbody>
                            <?php foreach($document_followup as $key => $value): ?>
                            <tr>
                                <td style="width: 15%;"><?=$value['name']?></td>
                                <td style="width: 15%;"><?=$value['days_to_send']?> Day(s) <?=$value['days_to_send_type']?> Due Date</td>
                                <td style="width: 10%;">
                                    <input type="checkbox" name="sms_switch" class="btn-switch" <?php if($value['email']): ?>checked <?php endif;?>>
                                </td>
                                <td style="width: 25%;"><?=@$value['sms_template']['title']?></td>
                                <td style="width: 10%;">
                                    <input type="checkbox" name="email_switch" class="btn-switch" <?php if($value['email']): ?>checked <?php endif;?>>
                                </td>
                                <td style="width: 25%;"><?=@$value['email_template']['title']?></td>
                            </tr>

                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>