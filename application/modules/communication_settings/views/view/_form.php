<div class="kt-portlet">
    <div class="kt-portlet__body">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Overdue Payments <?php switch($period_id) { case 1: echo "(Reservation)"; break; case 2: echo "(Downpayment)"; break; case 3: echo "(Loan Takeout)"; break; default: "";}?>
                    </h3>
                </div>
            </div>
        <div class="kt-portlet__body">
            <div class="kt-widget13">
                <div class="kt-widget13__item">
                    <div class="kt-widget13__text kt-widget13__text--bold mr-3">Period of Payment </div>
                </div>
                <div class="kt-widget13__item">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="kt-widget13__text kt-widget13__text--bold">Date to Send</th>
                                <th></th>
                                <th class="kt-widget13__text kt-widget13__text--bold">SMS Communication</th>
                                <th></th>
                                <th class="kt-widget13__text kt-widget13__text--bold">Email Communication</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($results): ?>
                                <?php foreach ($results as $key => $info) { 

                                    // ==================== begin: Add model fields ====================
                                    $id = isset($info['id']) && $info['id'] ? $info['id'] : '';
                                    $name = isset($info['name']) && $info['name'] ? $info['name'] : '';
                                    $days_to_send =  isset($info['days_to_send']) && $info['days_to_send'] ? $info['days_to_send'] : '';
                                    $days_to_send_type =  isset($info['days_to_send_type']) && $info['days_to_send_type'] ? $info['days_to_send_type'] : '';
                                    $period_id =  isset($info['period_id']) && $info['period_id'] ? $info['period_id'] : '';
                                    $setting_slug =  isset($info['setting_slug']) && $info['setting_slug'] ? $info['setting_slug'] : '';
                                    $sms =  isset($info['sms']) && $info['sms'] ? $info['sms'] : 0;
                                    $sms_template_id =  isset($info['sms_template_id']) && $info['sms_template_id'] ? $info['sms_template_id'] : '';
                                    $email =  isset($info['email']) && $info['email'] ? $info['email'] : 0;
                                    $email_template_id =  isset($info['email_template_id']) && $info['email_template_id'] ? $info['email_template_id'] : '';
                                    // ==================== end: Add model fields ====================

                                    ?>
                                     <tr>
                                        <td style="width: 15%;">
                                            <input type="hidden" class="form-control " name="name[<?=$id;?>]" value="<?=$name?>"> 
                                            <input type="hidden" class="form-control " name="days_to_send_type[<?=$id;?>]" value="<?=$days_to_send_type?>"> 
                                            <input type="hidden" class="form-control " name="period_id[<?=$id;?>]" value="<?=$period_id?>"> 
                                            <input type="hidden" class="form-control " name="setting_slug[<?=$id;?>]" value="<?=$setting_slug?>"> 
                                            <?=$name?>
                                        </td>
                                        <td style="width: 20%;">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="days_to_send[<?=$id;?>]" value="<?=$days_to_send?>"> 
                                                </div>
                                                <div class="col-md-8">
                                                    Days <?=$days_to_send_type?> Due Date
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 10%;">
                                            <input type="checkbox" name="sms[<?=$id;?>]" class="btn-switch" value="<?=$sms?>" <?php if($sms){ ?> checked <?php }?>>
                                        </td>
                                        <td style="width: 25%;">
                                            <div class="form-group">
                                                <input type="hidden" id="sms_template" value="<?=$sms_template_id?>">

                                                <select class="form-control sms_template_id suggests" name="sms_template_id[<?=$id;?>]"
                                                        placeholder="Type"
                                                        autocomplete="off" id="sms_template_id">
                                                    <option value="">Select template</option>
                                                </select>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </td>
                                        <td style="width: 10%;">
                                            <input type="checkbox" name="email" class="btn-switch" value="<?=$email?>" <?php if($email){ ?> checked <?php }?>>
                                        </td>
                                        <td style="width: 25%;">
                                            <div class="form-group">
                                                <input type="hidden" id="email_template" value="<?=$email_template_id?>">
                                                <select class="form-control email_template_id" name="email_template_id[<?=$id;?>]"
                                                        placeholder="Type"
                                                        autocomplete="off" id="email_template_id">
                                                    <option value="">Select template</option>
                                                </select>
                                                <span class="form-text text-muted"></span>
                                            </div>
                                        </td>
                                    </tr>

                                <?php } ?>
                            <?php endif ?>
                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--end::Portlet-->