<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
$days_to_send =  isset($data['days_to_send']) && $data['days_to_send'] ? $data['days_to_send'] : '';
$days_to_send_type =  isset($data['days_to_send_type']) && $data['days_to_send_type'] ? $data['days_to_send_type'] : '';
$period_id =  isset($data['period_id']) && $data['period_id'] ? $data['period_id'] : '';
$setting_slug =  isset($data['setting_slug']) && $data['setting_slug'] ? $data['setting_slug'] : '';
$sms =  isset($data['sms']) && $data['sms'] ? $data['sms'] : '';
$sms_template_id =  isset($data['sms_template_id']) && $data['sms_template_id'] ? $data['sms_template_id'] : '';
$email =  isset($data['email']) && $data['email'] ? $data['email'] : '';
$email_template_id =  isset($data['email_template_id']) && $data['email_template_id'] ? $data['email_template_id'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
  <div class="kt-container kt-container--fluid">
    <div class="kt-subheader__main">
      <h3 class="kt-subheader__title">Communication Settings</h3>
    </div>
    <div class="kt-subheader__toolbar">
      <div class="kt-subheader__wrapper">
        <a href="<?php echo site_url('communication_settings/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
          <i class="fa fa-edit"></i> Edit Communication Settings
        </a>   
        <a href="<?php echo site_url('communication_settings');?>" class="btn btn-label-instagram btn-sm btn-elevate">
          <i class="fa fa-reply"></i> Back
        </a>
      </div>
    </div>
  </div>
</div>

<!-- begin:: Content -->
<div
  class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
  <div class="row">
    <div class="col-md-6">
      <!--begin::Portlet-->
      <div class="kt-portlet">
        <div class="kt-portlet__body">
          <!--begin::Portlet-->
          <div class="kt-portlet">
            <div class="kt-portlet__head">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                  General Information
                </h3>
              </div>
            </div>
            <div class="kt-portlet__body">
              <!--begin::Form-->
              <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Name
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$name?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Days to Send
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$days_to_send?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Days to Send Type
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$days_to_send_type?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Period ID
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$period_id?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Settings Slug
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$setting_slug?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    SMS
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$sms?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    SMS Template
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$sms_template_id?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Email
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$email?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Email Template
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$email_template_id?></span>
                </div>
                <!-- ==================== begin: Add fields details  ==================== -->

                <!-- ==================== end: Add model details ==================== -->
              </div>
              <!--end::Form-->
            </div>
          </div>
          <!--end::Portlet-->
        </div>
      </div>
      <!--end::Portlet-->

    </div>

    <div class="col-md-6"></div>
  </div>
</div>
<!-- begin:: Footer -->
