<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Accounting Entries</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="total"></span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search Accounting Entries..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <div class="kt-input-icon  kt-input-icon--left kt-subheader__search">
                    <input type="text" class="form-control kt_datepicker" id="filter_start_date" name="filter_start_date" value="<?php echo date('Y-m-d') ?>" placeholder="Filter Start Date" autocomplete="off" readonly>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                </div>



                <div class="kt-input-icon  kt-input-icon--left kt-subheader__search">
                    <input type="text" class="form-control kt_datepicker" id="filter_end_date" name="filter_end_date" value="<?php echo date('Y-m-d') ?>" placeholder="Filter End Date" autocomplete="off" readonly>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                </div>

                <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                <a href="<?php echo site_url('accounting_entries/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Create Accounting Entries
                </a>

                <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('view/_information_tiles'); ?>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">

    </div>
</div>


<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!--begin: Advance Search -->
        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form" id="advance_search">
                        <div class="form-group row">
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Journal Type</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('journal_type', Dropdown::get_static('accounting_entries_journal_type'), '', 'class="form-control form-control-sm _filter" id="_column_5" data-column="5"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Payee Type</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('payee_type', Dropdown::get_static('payee_type'), '', 'class="form-control form-control-sm _filter" id="_column_7" data-column="7"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Payee</label>
                                <select class="form-control suggests _filter" id="_column_8" data-column="8" name="payee_type_id">
                                    <option value="">Select Payee</option>
                                </select>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Payment Date:</label>
                                <input type="text" class="form-control kt_transaction_daterangepicker _filter" name="date_range" id="_column_6" data-column="6" readonly>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Company</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <select class="form-control suggests _filter" id="_column_9" data-column="9" name="company_id" data-module="companies">
                                        <option value="">Select Company</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end: Advance Search -->


        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="accounting_entries_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Company</th>
                    <th>Project</th>
                    <th>Payee</th>
                    <th>Payee Name</th>
                    <th>OR Number</th>
                    <th>Invoice Number</th>
                    <th>Journal Type</th>
                    <th>Payment Date</th>
                    <th>Total</th>
                    <th>Remarks</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Last Update By</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>

<!--begin::Modal-->
<!--begin: Export Modal-->
<!-- <div class="modal fade show" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true" style="padding-right: 15px; display: block;"> -->