<?php

$id = isset($accounting_entries['id']) && $accounting_entries['id'] ? $accounting_entries['id'] : '';
$company_id = isset($accounting_entries['company_id']) && $accounting_entries['company_id'] ? $accounting_entries['company_id'] : '';
$or_number = isset($accounting_entries['or_number']) && $accounting_entries['or_number'] ? $accounting_entries['or_number'] : '';
$invoice_number = isset($accounting_entries['invoice_number']) && $accounting_entries['invoice_number'] ? $accounting_entries['invoice_number'] : '';
$journal_type = isset($accounting_entries['journal_type']) && $accounting_entries['journal_type'] ? $accounting_entries['journal_type'] : '';
$payment_date = isset($accounting_entries['payment_date']) && $accounting_entries['payment_date'] ? $accounting_entries['payment_date'] : '';
$payee_type = isset($accounting_entries['payee_type']) && $accounting_entries['payee_type'] ? $accounting_entries['payee_type'] : '';
$payee_type_id = isset($accounting_entries['payee_type_id']) && $accounting_entries['payee_type_id'] ? $accounting_entries['payee_type_id'] : '';
$remarks = isset($accounting_entries['remarks']) && $accounting_entries['remarks'] ? $accounting_entries['remarks'] : 'N/A';
$cr_total = isset($accounting_entries['cr_total']) && $accounting_entries['cr_total'] ? $accounting_entries['cr_total'] : '';
$dr_total = isset($accounting_entries['dr_total']) && $accounting_entries['dr_total'] ? $accounting_entries['dr_total'] : '';

$payee_data = get_person($payee_type_id, $payee_type);
$company = isset($accounting_entries['company']['name']) && $accounting_entries['company']['name'] ? $accounting_entries['company']['name'] : '';
$payee_name = get_fname($payee_data) ? get_fname($payee_data) : "N/A";

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Accounting Entries</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('accounting_entries/update/' . $id); ?>"
                    class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('accounting_entries'); ?>"
                    class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            OR Number
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $or_number; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Company
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $company; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Invoice Number
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $invoice_number; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Journal Type
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo Dropdown::get_static('accounting_entries_journal_type', $journal_type, 'view'); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Payment Date
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo date_format(date_create($payment_date), 'F j, Y'); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Payee Type
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $payee_type; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Payee
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $payee_name ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Remarks
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?php echo $remarks; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            CR Total
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo money_php($cr_total); ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            DR Total
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo money_php($dr_total); ?></p>
                                    </div>

                                </div>
                                <!--end::Form-->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                    
                </div>
                <!--end::Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
        
                                    <!--begin::Portlet-->
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Entry Items
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <?php if ($entry_items): //vdebug($entry_items); ?>
                                            <?php foreach ($entry_items as $key =>  $item): ?>
                                            <?php
                                    $dc = isset($item['dc']) && $item['dc'] ? $item['dc'] : "N/A";
                                    $ledger_id = isset($item['ledger_id']) && $item['ledger_id'] ? $item['ledger_id'] : "N/A";
                                    $amount = isset($item['amount']) && $item['amount'] ? $item['amount'] : "0";
                                    $description = isset($item['description']) && $item['description'] ? $item['description'] : "N/A";
                                    $ledger_ref_id = get_person($ledger_id, 'accounting_ledgers');
                                    $ledger_name = get_name($ledger_ref_id) ? get_name($ledger_ref_id) : "N/A";
                                ?>
                                            <div class="form-group form-group-xs row border-bottom pb-3 mb-3">
                                                <div class="col">
                                                    <div href="#" class="kt-notification-v2__item">
                                                        <div class="kt-notification-v2__itek-wrapper">
                                                            <div class="kt-notification-v2__item-title">
                                                                <h6 class="kt-portlet__head-title kt-font-primary">
                                                                    Type
                                                                </h6>
                                                            </div>
                                                            <div class="kt-notification-v2__item-desc">
                                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                                    <?php echo $dc == "d" ? "Debit" : "Credit"; ?>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div href="#" class="kt-notification-v2__item">
                                                        <div class="kt-notification-v2__itek-wrapper">
                                                            <div class="kt-notification-v2__item-title">
                                                                <h6 class="kt-portlet__head-title kt-font-primary">
                                                                    Ledger
                                                                </h6>
                                                            </div>
                                                            <div class="kt-notification-v2__item-desc">
                                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                                    <?php echo $ledger_name; ?>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div href="#" class="kt-notification-v2__item">
                                                        <div class="kt-notification-v2__itek-wrapper">
                                                            <div class="kt-notification-v2__item-title">
                                                                <h6 class="kt-portlet__head-title kt-font-primary">
                                                                    Dr Amount
                                                                </h6>
                                                            </div>
                                                            <div class="kt-notification-v2__item-desc">
                                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                                    <?php echo $dc == 'd' ? money_php($amount) : money_php(0); ?>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div href="#" class="kt-notification-v2__item">
                                                        <div class="kt-notification-v2__itek-wrapper">
                                                            <div class="kt-notification-v2__item-title">
                                                                <h6 class="kt-portlet__head-title kt-font-primary">
                                                                    Cr Amount
                                                                </h6>
                                                            </div>
                                                            <div class="kt-notification-v2__item-desc">
                                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                                    <?php echo $dc == 'c' ? money_php($amount) : money_php(0); ?>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div href="#" class="kt-notification-v2__item">
                                                        <div class="kt-notification-v2__itek-wrapper">
                                                            <div class="kt-notification-v2__item-title">
                                                                <h6 class="kt-portlet__head-title kt-font-primary">
                                                                    Description
                                                                </h6>
                                                            </div>
                                                            <div class="kt-notification-v2__item-desc">
                                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                                    <?php echo $description; ?>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach;?>
                                            <?php else: ?>
                                            <h4 class="text-center font-italic">No entry items found</h4>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>

            </div>


            <!--end::Portlet-->
        </div>
    </div>

</div>
<!-- begin:: Footer -->