<?php
$id = isset($accounting_entries['id']) && $accounting_entries['id'] ? $accounting_entries['id'] : '';
$company_id = isset($accounting_entries['company_id']) && $accounting_entries['company_id'] ? $accounting_entries['company_id'] : '';

$project_id = isset($accounting_entries['project_id']) && $accounting_entries['project_id'] ? $accounting_entries['project_id'] : '';
$property_id = isset($accounting_entries['property_id']) && $accounting_entries['property_id'] ? $accounting_entries['property_id'] : '';
$land_inventory_id = isset($accounting_entries['land_inventory_id']) && $accounting_entries['land_inventory_id'] ? $accounting_entries['land_inventory_id'] : '';

$or_number = isset($accounting_entries['or_number']) && $accounting_entries['or_number'] ? $accounting_entries['or_number'] : '';
$invoice_number = isset($accounting_entries['invoice_number']) && $accounting_entries['invoice_number'] ? $accounting_entries['invoice_number'] : '';
$journal_type = isset($accounting_entries['journal_type']) && $accounting_entries['journal_type'] ? $accounting_entries['journal_type'] : '';
$payment_date = isset($accounting_entries['payment_date']) && $accounting_entries['payment_date'] ? $accounting_entries['payment_date'] : '';
$payee_type = isset($accounting_entries['payee_type']) && $accounting_entries['payee_type'] ? $accounting_entries['payee_type'] : '';
$payee_type_id = isset($accounting_entries['payee_type_id']) && $accounting_entries['payee_type_id'] ? $accounting_entries['payee_type_id'] : '';
$remarks = isset($accounting_entries['remarks']) && $accounting_entries['remarks'] ? $accounting_entries['remarks'] : '';
$cr_total = isset($accounting_entries['cr_total']) && $accounting_entries['cr_total'] ? $accounting_entries['cr_total'] : '';
$dr_total = isset($accounting_entries['dr_total']) && $accounting_entries['dr_total'] ? $accounting_entries['dr_total'] : '';
$item_id = isset($entry_item['id']) && $entry_item['id'] ? $entry_item['id'] : '';
$ledger_id = isset($entry_item['ledger_id']) && $entry_item['ledger_id'] ? $entry_item['ledger_id'] : '';
$dc = isset($entry_item['dc']) && $entry_item['dc'] ? $entry_item['dc'] : '';
$description = isset($entry_item['description']) && $entry_item['description'] ? $entry_item['description'] : '';
$amount = isset($entry_item['amount']) && $entry_item['amount'] ? $entry_item['amount'] : '';

// $payee_type = isset($entry_item['payee_type']) && $entry_item['payee_type'] ? $entry_item['payee_type'] : '';
// $payee_type_id = isset($entry_item['payee_type_id']) && $entry_item['payee_type_id'] ? $entry_item['payee_type_id'] : '';
// print_r($accounting_entries['payee_type']);
$company = isset($accounting_entries['company']['name']) && $accounting_entries['company']['name'] ? $accounting_entries['company']['name'] : '';

$project = isset($accounting_entries['project']['name']) && $accounting_entries['project']['name'] ? $accounting_entries['project']['name'] : '';
$property = isset($accounting_entries['property']['name']) && $accounting_entries['property']['name'] ? $accounting_entries['property']['name'] : '';
$land_inventory = isset($accounting_entries['land_inventory']['name']) && $accounting_entries['land_inventory']['name'] ? $accounting_entries['land_inventory']['name'] : '';

$payee_name = isset($accounting_entries[$payee_type]) && $accounting_entries[$payee_type] ? $accounting_entries[$payee_type]['last_name'] . " " . $accounting_entries[$payee_type]['first_name'] : '';

?>

<!-- Accounting Entries Form -->
<div class="row">

    <div class="col-sm-6">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="companies" id="company_id" name="company_id" required>
                    <option value="">Select Company</option>
                    <?php if ($company) : ?>
                        <option value="<?php echo $company_id; ?>" selected><?php echo $company; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('company'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Project </label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="projects" id="project_id" name="project_id">
                    <option value="">Select Project</option>
                    <?php if ($project) : ?>
                        <option value="<?php echo $project_id; ?>" selected><?php echo $project; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('project'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            <label>Property </label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="properties" id="property_id" name="property_id">
                    <option value="">Select Property</option>
                    <?php if ($property) : ?>
                        <option value="<?php echo $property_id; ?>" selected><?php echo $property; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('property'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Land Inventory </label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="land" id="land_inventory_id" name="land_inventory_id">
                    <option value="">Select Property</option>
                    <?php if ($land_inventory) : ?>
                        <option value="<?php echo $land_inventory_id; ?>" selected><?php echo $land_inventory; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('land_inventory'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>OR Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="or_number" value="<?php echo set_value('or_number', $or_number); ?>" placeholder="OR Number" autocomplete="off">

            </div>
            <?php echo form_error('or_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Invoice Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="invoice_number" value="<?php echo set_value('invoice_number', $invoice_number); ?>" placeholder="Invoice Number" autocomplete="off">

            </div>
            <?php echo form_error('invoice_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Journal Type</label>
            <div class="kt-input-icon">

                <?php echo form_dropdown('journal_type', Dropdown::get_static('accounting_entries_journal_type'), set_value('journal_type', @$journal_type), 'class="form-control"'); ?>
                <?php echo form_error('journal_type'); ?>


            </div>
            <?php echo form_error('journal_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Payment Date</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker datePicker" name="payment_date" value="<?php echo set_value('payment_date', @$payment_date); ?>" placeholder="Date" autocomplete="off">

            </div>
            <?php echo form_error('payment_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Payee Type</label>
            <?php echo form_dropdown('payee_type', Dropdown::get_static('payee_type'), set_value('$payee_type', @$payee_type), 'class="form-control" id="payee_type"'); ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Payee</label>
            <div class="row">
                <div class="col-sm-12">
                    <select class="form-control suggests" data-type="person" data-module="<?php echo $payee_type ? $payee_type : "buyers" ?>" id="payee_type_id" name="payee_type_id">
                        <option value="">Select Payee</option>
                        <?php if ($payee_name) : ?>
                            <option value="<?php echo $payee_type_id; ?>" selected><?php echo $payee_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>



    <div class="col-sm-6">
        <div class="form-group">
            <label>Remarks</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="remarks" value="<?php echo set_value('remarks', $remarks); ?>" placeholder="Remarks" autocomplete="off">

            </div>
            <?php echo form_error('remarks'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">

    </div>
    <div class="col-sm-6">

    </div>
</div>
<!-- /Accounting Entries Form -->

<!-- Accounting Entry Items -->
<?php if (!empty($entry_items)) : ?>
    <?php foreach ($entry_items as $key => $entry_item) : ?>
        <?php
        $item_id = isset($entry_item['id']) && $entry_item['id'] ? $entry_item['id'] : '';
        $ledger_id = isset($entry_item['ledger_id']) && $entry_item['ledger_id'] ? $entry_item['ledger_id'] : '';
        $dc = isset($entry_item['dc']) && $entry_item['dc'] ? $entry_item['dc'] : '';
        $description = isset($entry_item['description']) && $entry_item['description'] ? $entry_item['description'] : '';
        $amount = isset($entry_item['amount']) && $entry_item['amount'] ? $entry_item['amount'] : '';
        $payee_type = isset($entry_item['payee_type']) && $entry_item['payee_type'] ? $entry_item['payee_type'] : '';
        $payee_type_id = isset($entry_item['payee_type_id']) && $entry_item['payee_type_id'] ? $entry_item['payee_type_id'] : '';

        ?>
        <div id="entry_item_form_repeater">
            <div data-repeater-list="entry_item[<?php echo $item_id; ?>]">
                <div data-repeater-item="entry_item[<?php echo $item_id; ?>]" class="row">
                    <div class="col-sm-2 d-none">
                        <div class="form-group">
                            <label>ID <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input class="form-control" name="entry_item[<?php echo $item_id ?>][id]" value="<?php echo set_value('item_id', $item_id); ?>" placeholder="Type" autocomplete="off" />

                            </div>
                            <?php echo form_error('dc'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Type <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <select class="form-control" name="entry_item[<?php echo $item_id ?>][dc]" value="<?php echo set_value('dc', $dc); ?>" placeholder="Type" autocomplete="off" id="entry_item_dc" data-type="<?php echo $dc; ?>">
                                    <option value="">Select option</option>
                                    <option value="d">d</option>
                                    <option value="c">c</option>
                                </select>

                            </div>
                            <?php echo form_error('dc'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Ledger <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">

                                <select name="entry_item[<?php echo $item_id ?>][ledger_id]" id="entry_item_ledger" class="form-control suggests" data-param="company_id" data-module="accounting_ledgers">
                                    <option value="">Select Ledger</option>
                                </select>

                                <!-- <select class="form-control" name="entry_item[<?php echo $item_id ?>][ledger_id]"
                            value="<?php echo set_value('ledger_id', $ledger_id); ?>" placeholder="Type"
                            autocomplete="off" id="entry_item_ledger" data-ledger="<?php echo $ledger_id; ?>">
                            <option>Select option</option>
                        </select> -->

                            </div>
                            <?php echo form_error('ledger_id'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Dr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="number" class="form-control" name="entry_item[<?php echo $item_id ?>][amount]" value="<?php echo set_value('amount', $dc === "d" ? $amount : ""); ?>" placeholder="DR Amount" autocomplete="off" id="dr_amount_input" disabled>

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Cr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="number" class="form-control" name="entry_item[<?php echo $item_id ?>][amount]" value="<?php echo set_value('amount', $dc === "c" ? $amount : ""); ?>" placeholder="CR Amount" autocomplete="off" id="cr_amount_input" disabled>

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Description</label>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" name="entry_item[<?php echo $item_id ?>][description]" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">

                                    </div>
                                    <?php echo form_error('description'); ?>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="col-sm-3">

                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                        <i class="la la-trash-o"></i>
                                    </a>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <div id="entry_item_form_repeater">
            <div data-repeater-list="entry_item">
                <div data-repeater-item="entry_item" class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Type <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <select class="form-control" name="entry_item[dc]" value="<?php echo set_value('dc', $dc); ?>" placeholder="Type" autocomplete="off" id="entry_item_dc">
                                    <option value="">Select option</option>
                                    <option value="d">D</option>
                                    <option value="c">C</option>
                                </select>

                            </div>
                            <?php echo form_error('dc'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Ledger <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <!-- <select class="form-control" name="entry_item[ledger_id]"
                                value="<?php echo set_value('ledger_id', $ledger_id); ?>" placeholder="Type"
                                autocomplete="off" id="entry_item_ledger">
                                <option>Select option</option>
                            </select> -->

                                <select name="entry_item[ledger_id]" id="entry_item_ledger" class="form-control suggests" data-param="company_id" data-module="accounting_ledgers">
                                    <option value="">Select Ledger</option>
                                </select>

                            </div>
                            <?php echo form_error('ledger_id'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Dr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="text" class="form-control" name="entry_item[amount]" value="<?php echo set_value('amount', $amount); ?>" placeholder="DR Amount" autocomplete="off" id="dr_amount_input" disabled>

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Cr Amount <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="text" class="form-control" name="entry_item[amount]" value="<?php echo set_value('amount', $amount); ?>" placeholder="CR Amount" autocomplete="off" id="cr_amount_input" disabled>

                            </div>
                            <?php echo form_error('amount'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Description</label>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control" name="entry_item[description]" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off">

                                    </div>
                                    <?php echo form_error('description'); ?>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="col-sm-3">

                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                        <i class="la la-trash-o"></i>
                                    </a>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        <?php endif; ?>

        <?php if (empty($entry_items)) : ?>
            <div class="form-group form-group-last row">
                <div class="offset-10"></div>
                <div class="col-lg-2">
                    <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                        <i class="la la-plus"></i> Add Entry Item
                    </a>
                </div>
            </div>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="offset-3"></div>
        <div class="col-sm-3">
            <div class="form-group row">
                <label for="dTotal" class="col-sm-4 col-form-label">Dr Total</label>
                <div class="col-sm-8">
                    <input class="form-control" name="dr_total" type="text" placeholder="D Total" id="dTotal" value="<?php echo set_value('dr_total', $dr_total); ?>" readonly>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group row">
                <label for="cTotal" class="col-sm-4 col-form-label">Cr Total</label>
                <div class="col-sm-8">
                    <input class="form-control" name="cr_total" type="text" placeholder="C Total" id="cTotal" value="<?php echo set_value('cr_total', $cr_total); ?>" readonly>
                </div>
            </div>
        </div>

        <div class="offset-3"></div>
    </div>


    <!-- /Accounting Entry Items -->