<div class=row>
    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--solid-brand kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        PENDING
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="pending_count" class="kt-font-boldest">254</h4>&nbsp;entry(s)
                </div>
            </div>
            <div class="kt-portlet__body">
            <span class="kt-widget20__number display-4">
                <h4 id="pending_amount"></h4>
            </span>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--solid-success kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        APPROVED
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="approved_count" class="kt-font-boldest">254</h4>&nbsp;entry(s)
                </div>
            </div>
            <div class="kt-portlet__body">
            <span class="kt-widget20__number display-4">
                <h4 id="approved_amount"></h4>
            </span>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12 col-xl-4">
        <div class="kt-portlet kt-portlet--solid-danger kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check kt-font-light"></i>
                    </span>
                    <div class="kt-portlet__head-title kt-font-light">
                        DISAPPROVED
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="disapproved_count" class="kt-font-boldest">254</h4>&nbsp;entry(s)
                </div>
            </div>
            <div class="kt-portlet__body">
                <span class="kt-widget20__number display-4 kt-font-light">
                <h4 id="disapproved_amount"></h4>
                </span>
            </div>
        </div>
    </div>
</div>