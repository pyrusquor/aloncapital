<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_entries extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('accounting_entries/Accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');

        $this->load->model('payment_voucher/Payment_voucher_model', 'M_pv');
        $this->load->model('payable_items/payable_items_model', 'M_pv_items');

        $this->load->model('accounting_entries/Accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');


        $this->load->model('buyer/Buyer_model', 'M_buyer');
        $this->load->model('seller/Seller_model', 'M_seller');
        $this->load->model('suppliers/Suppliers_model', 'M_supplier');
        $this->load->model('item/Item_model', 'M_item');
        $this->load->model('cheque/Cheque_model', 'M_cheques');

        $this->load->model('chart_of_accounts/Chart_of_accounts_model', 'M_accounting_ledgers');
        $this->_table_fillables = $this->M_Accounting_entries->fillable;
        $this->_table_columns = $this->M_Accounting_entries->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
    }

    public function get_all_buyers()
    {
        $data = $this->M_buyer->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }
    public function get_all_sellers()
    {
        $data = $this->M_seller->as_array()->get_all();
        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function get_all_ledgers()
    {
        $company_id = isset($_GET['company_id']) && $_GET['company_id'] ? $_GET['company_id'] : 0;

        $data = $this->M_accounting_ledgers->as_array()->get_all(['company_id' => $company_id]);

        $output = array(
            'data' => $data,
        );
        echo json_encode($output);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // $db_columns = $this->M_Accounting_entries->get_columns();
        // if ($db_columns) {
        //     $column = [];
        //     foreach ($db_columns as $key => $dbclm) {
        //         $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

        //         if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
        //             if (strpos($name, '_id') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } elseif (strpos($name, 'is_') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } else {
        //                 $column[$key]['value'] = $name;
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             }
        //         } else {
        //             continue;
        //         }
        //     }

        //     $column_count = count($column);
        //     $cceil = ceil(($column_count / 2));

        //     $this->view_data['columns'] = array_chunk($column, $cceil);
        //     $column_group = $this->M_Accounting_entries->count_rows();
        //     if ($column_group) {
        //         $this->view_data['total'] = $column_group;
        //     }
        // }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'project' => true,
            'or_number' => true,
            'company' => true,
            'company_id' => true,
            'invoice_number' => true,
            'journal_type' => true,
            'payment_date' => true,
            'payee_type' => true,
            'payee_type_id' => true,
            'company_id' => true,
            'payee_name' => true,
            'remarks' => true,
            'cr_total' => true,
            'payee' => true,
            'cancelled_at' => true,
            'is_approve' => true,
            'payee_type_id' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Accounting_entries
                ->with_company('fields:name')
                ->with_project('fields:name')
                ->with_property('fields:name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where(" (id like '%$searchValue%'");
                $query->or_where("or_number like '%$searchValue%'");
                $query->or_where("invoice_number like '%$searchValue%'");
                $query->or_where("payment_date like '%$searchValue%'");
                $query->or_where("cr_total like '%$searchValue%'");
                $query->or_where("remarks like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'journal_type' => [
                    'data' => $filters['journal_type'] ?? null,
                    'operator' => '=',
                ],
                'payee_type' => [
                    'data' => $filters['payee_type'] ?? null,
                    'operator' => 'like',
                ],
                'payee_type_id' => [
                    'data' => $filters['payee_type_id'] ?? null,
                    'operator' => '=',
                ],
                'date_range' => [
                    'data' => $filters['date_range'] ?? null,
                    'operator' => 'range',
                    'column' => 'payment_date',
                ],
                'company_id' => [
                    'data' => $filters['company_id'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range') {

                        $query->where($value['column'], '>=', explode(' - ', $value['data'])[0]);
                        $query->where($value['column'], '<=', explode(' - ', $value['data'])[1]);

                        continue;
                    }

                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $payee = get_payee($value);

                        $items[$key]['company_id'] = isset($value['company']['name']) && $value['company']['name'] ? $value['company']['name'] : 'N/A';
                        $items[$key]['project'] = isset($value['project']['name']) && $value['project']['name'] ? $value['project']['name'] : 'N/A';
                        $items[$key]['property'] = isset($value['property']['name']) && $value['property']['name'] ? $value['property']['name'] : 'N/A';

                        $items[$key]['payee_type_id'] = ($value['payee_type_id'] ? @$payee['name'] . " " . @$payee['first_name'] . " " . @$payee['last_name'] : 'N/A');

                        $items[$key]['cr_total'] = money_php($value['cr_total']);
                        $items[$key]['dr_total'] = money_php($value['dr_total']);
                        $items[$key]['payee_type'] = ucwords($value['payee_type']);
                        $items[$key]['payment_date'] = date('Y-m-d', strtotime($value['payment_date']));
                        $items[$key]['is_approve'] = $value['is_approve'];

                        $project = "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['project_id'] . "'>" . $items[$key]['project'] . "</a>";
                        $property = "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['property_id'] . "'>" . $items[$key]['property'] . "</a>";

                        if (empty($value['project_id']) || empty($value['property_id'])) {
                            $items[$key]['project'] = "General";
                        } else {
                            $items[$key]['project'] = $project . " <br> " . $property;
                        }

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Accounting_entries->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
        );

        echo json_encode($response);
        exit();
    }

     public function showAccountingEntries()
    {
        $where = $this->input->post('where') ? $this->input->post('where') : [];
        
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'project' => true,
            'or_number' => true,
            'company' => true,
            'invoice_number' => true,
            'journal_type' => true,
            'payment_date' => true,
            'payee_type' => true,
            'payee_type_id' => true,
            'company_id' => true,
            'payee_name' => true,
            'remarks' => true,
            'cr_total' => true,
            'payee' => true,
            'cancelled_at' => true,
            'is_approve' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        // $accounting_entries = $this->M_Accounting_entries->with_accounting_entry_items()->as_array()->get_all();
        $accounting_entries = $this->M_Accounting_entries->where($where)->with_company()->with_accounting_entry_items()->with_accounting_ledger()->as_array()->get_all();

        $data = [];

        if ($accounting_entries) {

            foreach ($accounting_entries as $key => $entry) {
                # code...
                $r = get_payee($entry);

                $accounting_entries[$key]['company'] = isset($entry['company']) && $entry['company'] ? $entry['company']['name'] : 'N/A';
                $accounting_entries[$key]['payee_name'] = ( $entry['payee_type_id'] ? @$r['name']." ".@$r['first_name']." ". @$r['last_name'] : 'N/A');
                
                $accounting_entries[$key]['cr_total'] = money_php($entry['cr_total']);
                $accounting_entries[$key]['dr_total'] = money_php($entry['dr_total']);

                $accounting_entries[$key]['payee_type'] = ucwords($entry['payee_type']);

                $accounting_entries[$key]['payment_date'] = date('Y-m-d', strtotime($entry['payment_date']));

                $accounting_entries[$key]['is_approve'] = $entry['is_approve'];

                $project = get_value_field($entry['project_id'],'projects','name');
                $property = get_value_field($entry['property_id'],'properties','name');

                // $transaction_id = get_value_field($entry['id'],'transaction_official_payments','transaction_id','entry_id');

                if ($transaction_id) {
                    $reference = get_value_field($transaction_id,'transactions','reference');
                }
                // $reference = "";
                // $transaction_id = "";

                $project = "<a target='_BLANK' href='".base_url()."project/view/".$entry['project_id']."'>".$project."</a>";
                $property = "<a target='_BLANK' href='".base_url()."property/view/".$entry['property_id']."'>".$property."</a>";
                $transaction = "<a target='_BLANK' href='".base_url()."transaction/view/".$transaction_id."'>".$reference."</a>";

                $accounting_entries[$key]['payee_name'] .= "<br>". $transaction;

                if (empty($entry['project_id']) || empty($entry['property_id'])) {
                    $accounting_entries[$key]['project'] = "General";
                } else {
                    $accounting_entries[$key]['project'] = $project." <br> ". $property;
                }
                
            }

            foreach ($accounting_entries as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'] - 1;
                $dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );

        

        }
        

        // $this->output->enable_profiler(TRUE);
        // vdebug($accounting_entries);

        echo json_encode($output);
        exit();
    }
    
    public function create()
    {

        if ($this->input->post()) {
            $_input = $this->input->post();

            // Entries
            $entries['or_number'] = $_input['or_number'];
            $entries['company_id'] = $_input['company_id'];
            $entries['invoice_number'] = $_input['invoice_number'];
            $entries['journal_type'] = $_input['journal_type'];
            $entries['payment_date'] = $_input['payment_date'];
            $entries['payee_type'] = $_input['payee_type'];
            $entries['payee_type_id'] = $_input['payee_type_id'];
            $entries['remarks'] = $_input['remarks'];
            $entries['cr_total'] = $_input['cr_total'];
            $entries['dr_total'] = $_input['dr_total'];

            $entry_id = $this->M_Accounting_entries->insert($entries + $this->additional);

            // Entry Items

            $entry_items = $_input['entry_item'];
            foreach ($entry_items as $key => $entry) {
                $entry_item['accounting_entry_id'] = $entry_id;
                $entry_item['ledger_id'] = $entry['ledger_id'];
                $entry_item['amount'] = $entry['amount'];
                $entry_item['dc'] = $entry['dc'];
                $entry_item['payee_type'] = $_input['payee_type'];
                $entry_item['payee_type_id'] = $_input['payee_type_id'];
                $entry_item['is_reconciled'] = 0;
                $entry_item['description'] = $entry['description'];

                $this->M_Accounting_entry_items->insert($entry_item + $this->additional);
            }

            if ($entry_id === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Successfully Updated.', 'accounting_entries');
            }
        }

        $this->template->build('create');
    }

    public function update($id = false, $paid_amount = false, $principal_amount = false, $interest_amount = 0, $penalty_amount = 0)
    {
        if ($id) {

            $this->view_data['accounting_entries'] = $data = $this->M_Accounting_entries->with_sellers()->with_buyers()->with_accounting_entry_items()->get($id);

            $accounting_entry_id = ["accounting_entry_id" => $id];
            $this->view_data['entry_items'] = $this->M_Accounting_entry_items->get_all($accounting_entry_id);

            if ($paid_amount) {
                $this->view_data['info']['paid_amount'] = $paid_amount;
            }
            if ($principal_amount) {
                $this->view_data['info']['principal_amount'] = $principal_amount;
            }
            if ($interest_amount) {
                $this->view_data['info']['interest_amount'] = $interest_amount;
            }
            if ($penalty_amount) {
                $this->view_data['info']['penalty_amount'] = $penalty_amount;
            }

            if ($data) {

                if ($this->input->post()) {
                    $_input = $this->input->post();

                    // Entries
                    $entries['or_number'] = $_input['or_number'];
                    $entries['invoice_number'] = $_input['invoice_number'];
                    $entries['journal_type'] = $_input['journal_type'];
                    $entries['payment_date'] = $_input['payment_date'];
                    $entries['payee_type'] = $_input['payee_type'];
                    $entries['payee_type_id'] = $_input['payee_type_id'];
                    $entries['remarks'] = $_input['remarks'];
                    $entries['cr_total'] = $_input['cr_total'];
                    $entries['dr_total'] = $_input['dr_total'];

                    $entries_id = $this->M_Accounting_entries->update($entries + $this->additional, $id);

                    // Entry Items
                    $entry_items = $_input['entry_item'];
                    $entry_id = $entry_items['id'];

                    foreach ($entry_items as $key => $entry) {
                        $entry_id = $entry['id'];
                        $entry_item['accounting_entry_id'] = $id;
                        $entry_item['ledger_id'] = $entry['ledger_id'];
                        $entry_item['amount'] = $entry['amount'];
                        $entry_item['dc'] = $entry['dc'];
                        $entry_item['payee_type'] = $_input['payee_type'];
                        $entry_item['payee_type_id'] = $_input['payee_type_id'];
                        $entry_item['is_reconciled'] = 0;
                        $entry_item['description'] = $entry['description'];

                        $this->M_Accounting_entry_items->update($entry_item + $this->additional, $entry_id);
                    }

                    if ($entries_id === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'accounting_entries');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Accounting_entries->get($id);
            if ($list) {

                $deleted = $this->M_Accounting_entries->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Accounting_entries->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Type Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['accounting_entries'] = $this->M_Accounting_entries->with_company()->get($id);

            $accounting_entry_id = ["accounting_entry_id" => $id];
            $this->view_data['entry_items'] = $this->M_Accounting_entry_items->get_all($accounting_entry_id);

            if ($this->view_data['accounting_entries']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function transfer($debug = 0)
    {
        $entries = $this->M_Accounting_entries_2->get_all();
        $entry_items = $this->M_Accounting_entry_items_2->get_all();

        // $entries = $this->db->get('cornerstone_entries')->result_array();
        // $entry_items = $this->db->get('cornerstone_entry_items')->result_array();

        $suppliers = $this->db->get('cornerstone_suppliers')->result_array();
        $items = $this->db->get('cornerstone_accounting_items_payable')->result_array();
        $p_vouchers = $this->db->get('cornerstone_payment_voucher')->result_array();
        $pv_items = $this->db->get('cornerstone_payment_voucher_items')->result_array();
        $cheques = $this->db->get('cornerstone_cheques')->result_array();


        if ($debug == "entries") {
            if ($entries) {
                foreach ($entries as $key => $entry) {

                    // entry_types
                    // 2 = cash payment journal :5
                    // 1 = cash receipt journal : 3

                    // journal_types
                    // 1 = purchase jounal : 4
                    // 0 = sales journal : 2

                    // invoice_type = journal_voucher : 1

                    // 1: { type: "Journal Voucher" },
                    // 2: { type: "Sales Journal" },
                    // 3: { type: "Cash Receipt Journal" },
                    // 4: { type: "Purchase Journal" },
                    // 5: { type: "Cash Payment Journal" },

                    if ($entry['invoice_type'] == "journal_voucher") {
                        $journal_type = 1;
                    } else if ($entry['entry_type'] == 1) {
                        $journal_type = 3;
                    } else if ($entry['entry_type'] == 2) {
                        $journal_type = 5;
                    } else if ($entry['journal_type'] == 1) {
                        $journal_type = 4;
                    } else if ($entry['journal_type'] == 0) {
                        $journal_type = 2;
                    }

                    $entries_data['journal_type'] = $journal_type;
                    $entries_data['payment_date'] = $entry['payment_date'];
                    $entries_data['dr_total'] = $entry['dr_total'];
                    $entries_data['cr_total'] = $entry['cr_total'];

                    if ($entry['agent']) {
                        $payee_type = 2;
                        $payee_type_id = $entry['agent'];
                    } elseif ($entry['broker']) {
                        $payee_type = 2;
                        $payee_type_id = $entry['broker'];
                    } elseif ($entry['client']) {
                        $payee_type = 5;
                        $payee_type_id = $entry['client'];
                    } elseif ($entry['personnel']) {
                        $payee_type = 3;
                        $payee_type_id = $entry['personnel'];
                    } elseif ($entry['employee']) {
                        $payee_type = 3;
                        $payee_type_id = $entry['employee'];
                    } elseif ($entry['supplier']) {
                        $payee_type = 1;
                        $payee_type_id = $entry['supplier'];
                    }

                    if ($entry['agent']) {
                        $payee_type = 2;
                        $payee_type_id = $entry['agent'];
                    }

                    $entries_data['payee_type'] = $payee_type;
                    $entries_data['payee_type_id'] = $payee_type_id;
                    $entries_data['is_approve'] = $entry['is_approve'];
                    $entries_data['or_number'] = $entry['or_number'];
                    $entries_data['invoice_number'] = $entry['or_number'];
                    $entries_data['company_id'] = 1;
                    $entries_data['remarks'] = $entry['narration'];

                    $entry_id = $this->M_Accounting_entries->insert($entries_data);

                    $this->db->where('cornerstone_payment_voucher.id', $entry['invoice_id']);
                    $p_vouchers = $this->db->get('cornerstone_payment_voucher')->result_array();

                    if ($p_vouchers) {
                        foreach ($p_vouchers as $key => $voucher) {
                            // $voucher_data['id'] = $voucher['id'];
                            $voucher_data['reference'] = ($voucher['voucher_number'] ? $voucher['voucher_number'] : '');
                            $voucher_data['payment_request_id'] = 0;
                            $voucher_data['payee_type'] = $payee_type;
                            $voucher_data['payee_type_id'] = $payee_type_id;
                            $voucher_data['gross_amount'] = $voucher['paid_amount'];
                            $voucher_data['net_amount'] = $voucher['paid_amount'];
                            $voucher_data['payment_type_id'] = $voucher['payment_type'];
                            $voucher_data['check_id'] = '';
                            $voucher_data['paid_amount'] = $voucher['paid_amount'];
                            $voucher_data['paid_date'] = $voucher['date_created'];
                            $voucher_data['particulars'] = ($voucher['particulars'] ? $voucher['particulars'] : '');
                            $voucher_data['accounting_entry_id'] = $entry_id;

                            $voucher_data['deleted_at'] = ($voucher['is_deleted'] ? date('Y-m-d H:i:s') : NULL);
                            $voucher_data['deleted_by'] = ($voucher['is_deleted'] ? 1 : NULL);

                            $voucher_data_id = $this->M_pv->insert($voucher_data);

                            $this->db->where('cornerstone_payment_voucher_items.id', $voucher['id']);
                            $pv_items = $this->db->get('cornerstone_payment_voucher_items')->result_array();

                            if ($pv_items) {

                                foreach ($pv_items as $key => $pv_item) {
                                    $pv_item_data['payment_request_id'] = 0;
                                    $pv_item_data['payment_voucher_id'] = $voucher_data_id;
                                    $pv_item_data['item_id'] = $pv_item['item_payable_id'];
                                    $pv_item_data['name'] = "";
                                    $pv_item_data['payable_type'] = "PV";
                                    $pv_item_data['quantity'] = $pv_item['item_payable_quantity'];
                                    $pv_item_data['unit_price'] = $pv_item['item_payable_price'];
                                    $pv_item_data['rate'] = $pv_item['tax_rate'];
                                    $pv_item_data['tax_id'] = $pv_item['tax_id'];
                                    $pv_item_data['tax_amount'] = ($pv_item['item_payable_price'] * $pv_item['item_payable_quantity']) * ($pv_item['tax_rate'] / 100);
                                    $pv_item_data['total_amount'] =  $pv_item_data['tax_amount'] + ($pv_item['item_payable_price'] * $pv_item['item_payable_quantity']);

                                    $pv_item_data['deleted_at'] = ($pv_item['is_deleted'] ? date('Y-m-d H:i:s') : NULL);
                                    $pv_item_data['deleted_by'] = ($pv_item['is_deleted'] ? 1 : NULL);

                                    $this->M_pv_items->insert($pv_item_data);
                                }
                            }
                        }
                    }

                    $entry_items = $this->M_Accounting_entry_items_2->get_all(array('accounting_entry_id' => $entry['id']));

                    if ($entry_items) {
                        foreach ($entry_items as $key => $entry_item) {

                            $payee_type = 0;
                            $payee_type_id = 0;

                            if ($entry_item['agent_id']) {
                                $payee_type = 2;
                                $payee_type_id = $entry_item['agent_id'];
                            } elseif ($entry_item['broker_id']) {
                                $payee_type = 2;
                                $payee_type_id = $entry_item['broker_id'];
                            } elseif ($entry_item['client_id']) {
                                $payee_type = 5;
                                $payee_type_id = $entry_item['client_id'];
                            } elseif ($entry_item['personnel_id']) {
                                $payee_type = 3;
                                $payee_type_id = $entry_item['personnel_id'];
                            } elseif ($entry_item['contractor_id']) {
                                $payee_type = 4;
                                $payee_type_id = $entry_item['contractor_id'];
                            } elseif ($entry_item['supplier_id']) {
                                $payee_type = 1;
                                $payee_type_id = $entry_item['supplier_id'];
                            }

                            $entry_items_data['accounting_entry_id'] = $entry_id;
                            $entry_items_data['ledger_id'] = $entry_item['ledger_id'];
                            $entry_items_data['amount'] = $entry_item['amount'];
                            $entry_items_data['dc'] = $entry_item['dc'];
                            $entry_items_data['payee_type'] = $payee_type;
                            $entry_items_data['payee_type_id'] = $payee_type_id;
                            $entry_items_data['is_reconciled'] = $entry_item['is_reconciled'];
                            $entry_items_data['description'] = $entry_item['description'];

                            $this->M_Accounting_entry_items->insert($entry_items_data);
                        }
                    }
                }
            }
        }

        if ($debug == 1) {
            vdebug($entries);
        }

        if ($debug == 2) {
            vdebug($entry_items);
        }

        if ($debug == 3) {
            vdebug($p_vouchers);
        }

        // vdebug($debug);

        if ($debug == "suppliers") {

            if ($suppliers) {
                foreach ($suppliers as $key => $supplier) {

                    $supplier_data['id'] = $supplier['id'];
                    $supplier_data['name'] = $supplier['name'];
                    $supplier_data['address'] = $supplier['address'];
                    $supplier_data['tin_number'] = $supplier['TIN'];
                    $supplier_data['email_address'] = $supplier['email'];
                    $supplier_data['mobile_number'] = $supplier['phone'];
                    // $supplier_data['is_accredited'] = $supplier['is_accredited'];
                    $supplier_data['deleted_at'] = ($supplier['is_deleted'] ? date('Y-m-d H:i:s') : NULL);
                    $supplier_data['deleted_by'] = ($supplier['is_deleted'] ? 1 : NULL);

                    $this->M_supplier->insert($supplier_data);
                }
            }

            // vdebug($suppliers);
        }


        if ($debug == "cheques") {

            if ($cheques) {
                foreach ($cheques as $key => $cheque) {

                    $check_data['bank_id'] = 0;
                    $check_data['branch'] = 0;
                    $check_data['amount'] = $cheque['amount'];
                    $check_data['unique_number'] = $cheque['cheque_no'];
                    $check_data['due_date'] = $cheque['date_released'];
                    $check_data['particulars']  = $cheque['description'];
                    $check_data['status'] = $cheque['is_active'];
                    $check_data['voucher_id'] = $cheque['voucher_id'];

                    $this->M_cheques->insert($check_data);
                }
            }

            // vdebug($suppliers);
        }

        if ($debug == "items") {

            if ($items) {
                foreach ($items as $key => $item) {

                    $item_data['id'] = $item['id'];
                    $item_data['name'] = $item['item_name'];
                    $item_data['code'] = $item['item_code'];
                    $item_data['description'] = $item['item_description'];
                    $item_data['total_price'] = $item['item_price'];
                    $item_data['deleted_at'] = ($item['is_deleted'] ? date('Y-m-d H:i:s') : NULL);
                    $item_data['deleted_by'] = ($item['is_deleted'] ? 1 : NULL);

                    $this->M_item->insert($item_data);
                }
            }

            // vdebug($suppliers);
        }
    }

    public function approve_entry()
    {

        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';
        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $id = $this->input->post('id');
        if ($id) {

            $entry = $this->M_Accounting_entries->get($id);

            if ($entry) {


                $data = array("is_approve" => '1');

                $deleted = $this->M_Accounting_entries->update($data + $additional, $id);
                if ($deleted) {

                    $response['status'] = 1;
                    $response['message'] = 'Accountry Entry Approved!';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulk_approve_entry()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';
        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $ids = $this->input->post('approve_array');

        if (count($ids)) {

            foreach ($ids as $id) {
                $entry = $this->M_Accounting_entries->where('is_approve !=', 1)->where('is_approve !=', 2)->get($id);

                if ($entry) {

                    $data = array("is_approve" => '1');

                    $this->M_Accounting_entries->update($data + $additional, $id);
                }
            }

            $response['status'] = 1;
            $response['message'] = 'Accountry Entries Approved!';
        }

        echo json_encode($response);
        exit();
    }

    public function disapprove_entry()
    {

        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';
        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $id = $this->input->post('id');
        if ($id) {

            $entry = $this->M_Accounting_entries->get($id);

            if ($entry) {


                $data = array("is_approve" => '2');

                $deleted = $this->M_Accounting_entries->update($data + $additional, $id);
                if ($deleted) {

                    $response['status'] = 1;
                    $response['message'] = 'Accountry Entry Disapproved!';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulk_disapprove_entry()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';
        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $ids = $this->input->post('disapprove_array');

        if (count($ids)) {

            foreach ($ids as $id) {
                $entry = $this->M_Accounting_entries->where('is_approve !=', 2)->get($id);

                if ($entry) {

                    $data = array("is_approve" => '2');

                    $this->M_Accounting_entries->update($data + $additional, $id);
                }
            }

            $response['status'] = 1;
            $response['message'] = 'Accountry Entries Disapproved!';
        }

        echo json_encode($response);
        exit();
    }

    public function get_accounting_entry_information()
    {

        $pending_count = 0;
        $approved_count = 0;
        $disapproved_count = 0;
        $pending_amount = 0;
        $approved_amount = 0;
        $disapproved_amount = 0;

        if ($this->input->post()) {

            $start = $this->input->post('start');
            $end = $this->input->post('end');

            if ($start == '' && $end == '') {
                $accounting_entry_query = $this->db->query('SELECT * FROM accounting_entries WHERE payment_date = CURDATE()');
                $accounting_entry_array = $accounting_entry_query->result_array();
            } else {
                $accounting_entry_query = $this->db->query("SELECT * FROM accounting_entries WHERE (payment_date BETWEEN CAST('$start' AS DATE)  AND CAST('$end' AS DATE))");
                $accounting_entry_array = $accounting_entry_query->result_array();
            }
            if ($accounting_entry_array) {

                foreach ($accounting_entry_array as $row) {
                    switch ($row['is_approve']) {
                        case '0':
                            $pending_count += 1;
                            $pending_amount += $row['cr_total'];
                            break;
                        case '1':
                            $approved_count += 1;
                            $approved_amount += $row['cr_total'];
                            break;
                        case '2':
                            $disapproved_count += 1;
                            $disapproved_amount += $row['cr_total'];
                            break;
                    }
                }
            }
        }
        $result['pending_count'] = $pending_count;
        $result['approved_count'] = $approved_count;
        $result['disapproved_count'] = $disapproved_count;
        $result['pending_amount'] = money_php($pending_amount);
        $result['approved_amount'] = money_php($approved_amount);
        $result['disapproved_amount'] = money_php($disapproved_amount);
        echo json_encode($result);
        exit();
    }

    public function showAccountingEntryItems()
    {
        $where = $this->input->post('where') ? $this->input->post('where') : [];

        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'dc' => true,
            'ledger_id' => true,
            'amount' => true,
            'description' => true,
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        // $accounting_entries = $this->M_Accounting_entries->with_accounting_entry_items()->as_array()->get_all();

        $accounting_entry_items = $this->M_Accounting_entry_items->where($where)->get_all();

        $data = [];

        if ($accounting_entry_items) {

            // foreach ($accounting_entry_items as $key => $entry) {
            // }

            foreach ($accounting_entry_items as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'] - 1;
                $dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }
}
