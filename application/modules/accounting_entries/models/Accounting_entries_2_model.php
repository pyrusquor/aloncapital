<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_entries_2_model extends MY_Model
{
    public $table = 'accounting_entries_2'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'or_number',
        'company_id',
        'invoice_number',
        'journal_type',
        'payment_date',
        'payee_type',
        'payee_type_id',
        'remarks',
        'cr_total',
        'dr_total',
        'is_approve',
        'cancelled_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'or_number' => array(
            'field' => 'or_number',
            'label' => 'OR Number',
            'rules' => 'trim|required',
        ),
        'company_id' => array(
            'field' => 'company_id',
            'label' => 'Company ID',
            'rules' => 'trim|required',
        ),
        'invoice_number' => array(
            'field' => 'invoice_number',
            'label' => 'Invoice Number',
            'rules' => 'trim|required',
        ),
        'journal_type' => array(
            'field' => 'journal_type',
            'label' => 'Journal Type',
            'rules' => 'trim|required',
        ),
        'payment_date' => array(
            'field' => 'payment_date',
            'label' => 'Payment Date',
            'rules' => 'trim|required',
        ),
        'payee_type' => array(
            'field' => 'payee_type',
            'label' => 'Payee Type',
            'rules' => 'trim|required',
        ),
        'payee_type_id' => array(
            'field' => 'payee_type_id',
            'label' => 'Payee Type ID',
            'rules' => 'trim|required',
        ),
        'remarks' => array(
            'field' => 'remarks',
            'label' => 'Remarks',
            'rules' => 'trim',
        ),
        'cr_total' => array(
            'field' => 'cr_total',
            'label' => 'CR Total',
            'rules' => 'trim|required',
        ),
        'dr_total' => array(
            'field' => 'dr_total',
            'label' => 'DR Total',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // $this->has_many['table_name'] = array();
        $this->has_many['accounting_entry_items'] = array('foreign_model' => 'Accounting_entry_items_model', 'foreign_table' => 'accounting_entry_items', 'foreign_key' => 'accounting_entry_id', 'local_key' => 'id');
        $this->has_one['buyers'] = array('foreign_model' => 'Buyer_model', 'foreign_table' => 'buyers', 'foreign_key' => 'id', 'local_key' => 'payee_type_id');
        $this->has_one['sellers'] = array('foreign_model' => 'Seller_model', 'foreign_table' => 'sellers', 'foreign_key' => 'id', 'local_key' => 'payee_type_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'or_number' => $faker->randomNumber($nbDigits = null, $strict = false),
                'invoice_number' => $faker->randomNumber($nbDigits = null, $strict = false),
                'journal_type' => $faker->numberBetween($min = 1, $max = 5),
                'payment_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'payee_type' => $faker->numberBetween($min = 1, $max = 2),
                'payee_type_id' => $faker->numberBetween($min = 1, $max = 9999),
                'remarks' => $faker->sentence(),
                'cr_total' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
                'dr_total' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}
