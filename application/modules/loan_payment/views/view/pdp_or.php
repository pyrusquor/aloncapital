<?php
	$id = $r['id'];

	$transaction = $r['transaction'];
	$transaction_id = $transaction['id'];
	$reference = $transaction['reference'];

	$project_name = $transaction['project_id'];
	$property_name = $transaction['property_id'];

	$buyer_id = $transaction['buyer_id'];

	$buyer = $this->M_buyer->get($buyer_id);

	$buyer_name = get_fname($buyer);

	$payment_method = @$r['payment_type_id'];
	$period_type_id = @$r['period_id'];

	$project_id = $transaction['project_id'];
	$property_id = $transaction['property_id'];

	$payment_date = view_date($r['payment_date']);
	$amount_paid =  money_php($r['amount_paid']);
	$principal_amount =  money_php($r['principal_amount']);
?>
<style type="text/css">
    html {
        margin: 10px 15px 0 10px;
        font-family: Helvetica;
    }

    table {
        width: 100%;
    }

    table#account-form tr td {
        width: 26%;
        padding: 4px;
        font-size: 9pt;
    }

    table#account-form tr td:nth-child(2) {
        width: 30%;
    }

    table#account-form tr td:first-child {
        width: 15%;
    }

    table#account-settle tr td {
        padding: 3px;
        font-size: 9pt;
    }

    .account-info p,
    .account-info h3 {
        text-align: center;
    }

    .text-center {
        text-align: center;
    }

    .centered {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }
</style>

<html>

<body>
    <table>
        <tr>
            <td width="20%">
                <table border="1" cellspacing="0" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <center><b>In settlement of the following</b></center>
                        </td>
                    </tr>
                    <tr>
                        <td>Billing Invoice No.</td>
                        <td>Amount</td>
                    </tr>
                    <tr>
                        <td>Total Sales (VAT Inclusive)</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Less: VAT</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>TOTAL</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Less: Withholding Tax</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Amount Due</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>VATABLE Sales</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>VAT-Exempt Sales</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>VAT Amount</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Total Sales</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center><b>FORM OF PAYMENT</b></center>
                        </td>
                    </tr>
                    <tr>
                        <td>Cash</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Check</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Bank</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Balance</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>TOTAL</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="80%" style="display: block">
                <table>
                    <tr style="text-align:center">
                        <td>
                            <h3 style="margin-top:-5px">SACRED HEART OF JESUS DEVELOPMENT CORPORATION</h3>
                            <p>Punta Dulog Commercial Complex, St. Joseph Ave.,</p>
                            <p>Pueblo de Panay Township, Brgy. Lawaan, Roxas City</p>
                            <p>( 036 ) 6212-806 * 6210-681 Fax No. #: (036) 6212-808</p>
                            <p>VAT Reg. <u>TIN: 002 - 240 - 754 - 000</u></p>
                        </td>
                        <td>
                            <h1>No. 5529</h1>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3>OFFICIAL RECEIPT</h3>
                        </td>
                        <td>
                            Date: ___________________,20____
                        </td>
                    </tr>
                    <tr style="text-align:left">
						<td colspan="1">
							<i>Received
								from</i> <u class="pl-5"><?=$buyer_name?></u>
								with TIN #: _______________________________ with Address at <u><?=$buyer['present_address']?></u> engaged in the business style of ____ the sum of
							<?=$amount_paid_in_words?>
							pesos ( <?=$amount_paid?> ) 	In partial / Full Payment of <u><?=$principal_amount?></u>
						</td>
					</tr>
                    <tr>
                        <td colspan="3">
                            Sr. Citizen TIN#: _____________ ISCA / PWD ID. NO. ______________________ Signature
                            ______________________
                        </td>
                    </tr>
                </table>
                <hr style="height: 2px; background-color: #000;">
                <table>
                    <tr>
                        <td>200 Bklts ( 50 x 3 ) 13.001 - 23,000 * B.I.R Authority to Print No. OCN2AU00001420253</td>

                    </tr>
                    <tr>
                        <td>Date Issued 22 June, 2015 * Valid until 22 June 2020</td>
                    </tr>
                    <tr>
                        <td>
                            <h3>BON XEROX AND COPY PRINTER</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>BON NONVAT Reg. TIN #: 184 - 805 - 955 - 000</td>
                    </tr>
                    <tr>
                        <td>Printer's Accreditation No. 072MP2013230000000 Date Issued December, 2013</td>
                        <td>
                            <div class="centered">
                                <span>By: ________________________</span>
                                <span class="text-center">Authorized
                                    Signature</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3>THIS OFFICIAL RECEIPT SHALL BE VALID FOR FIVE (5) YEARS FROM THE OF ATP</h3>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>