<?php

$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
$amount_paid = isset($info['amount_paid']) && $info['amount_paid'] ? $info['amount_paid'] : '';

$check_deposit_amount = isset($info['check_deposit_amount']) && $info['check_deposit_amount'] ? $info['check_deposit_amount'] : '';
$cash_amount = isset($info['cash_amount']) && $info['cash_amount'] ? $info['cash_amount'] : '';

$payment_date = isset($info['payment_date']) && $info['payment_date'] ? $info['payment_date'] : '';
$or_date = isset($info['or_date']) && $info['or_date'] ? $info['or_date'] : '';
$OR_number = isset($info['OR_number']) && $info['OR_number'] ? $info['OR_number'] : '';

$transaction = isset($transaction) && $transaction ? $transaction : array();
$reference = isset($transaction['reference']) && $transaction['reference'] ? $transaction['reference'] : '';

$buyer_id = isset($transaction['buyer_id']) && $transaction['buyer_id'] ? $transaction['buyer_id'] : '';
$buyer_name = isset($transaction['buyer']) && $transaction['buyer'] ? get_fname($transaction['buyer']) : '';

$property_id = isset($transaction['property_id']) && $transaction['property_id'] ? $transaction['property_id'] : '';
$property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : '';

$this->transaction_library->initiate($transaction_id);
$period_id = $this->transaction_library->current_period(1);
$amount_due = $this->transaction_library->total_amount_due();
$principal_due = $this->transaction_library->total_amount_due(1);
$interest_due = $this->transaction_library->total_amount_due(2);
$overdue = $this->transaction_library->payment_status(1);

$penalty_due = round($this->transaction_library->get_penalty(), 2);

$data['payments'] = $this->transaction_library->get_overdue_table(date('Y-m-d'));

$is_waived = 1;

?>

<div class="row">
    <div class="col-md-5">
        <input type="hidden" id="transaction_id" name="transaction_id" value="<?php echo $transaction_id; ?>">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Buyer Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" data-module="buyers" id="buyer_id">
                        <?php if ($buyer_id): ?>
                        <option value="<?=$buyer_id;?>" selected><?php echo $buyer_name; ?></option>
                        <?php endif?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Reference <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="reference">
                        <?php if ($reference): ?>
                        <option selected><?php echo $reference; ?></option>
                        <?php endif?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Property <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="property_id">
                        <?php if ($property_id): ?>
                        <option value="<?=$property_id;?> " selected><?php echo $property_name; ?></option>
                        <?php endif?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Period Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('info[period_id]', Dropdown::get_static('period_names'), set_value('info[period_id]', @$period_id), 'class="form-control" id="period_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date of Payment <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="payment_date"
                            placeholder="Date of Payment" name="info[payment_date]"
                            value="<?php echo date("Y-m-d"); ?>" readonly>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Receipt Date</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="or_date" placeholder="OR Date"
                            name="info[or_date]" value="<?php echo date("Y-m-d"); ?>" readonly>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Waive Penalty</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <?php echo form_dropdown('info[is_waived]', Dropdown::get_static('bool'), set_value('info[is_waived]', @$is_waived), 'class="form-control" id="is_waived"'); ?>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Penalty Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="number" class="form-control" id="penalty_amount" placeholder="Amount Paid"
                            name="info[penalty_amount]"
                            value="<?php echo set_value('info[penalty_amount]"', $penalty_due); ?>">
                    </div>
                </div>
            </div>


        </div>

        <div class="row">


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Payment Application <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <select class="form-control" name="payment_application">
                            <option value="0">Select Option</option>
                            <option value="2">Balloon Payment (Add to principal all surplus)</option>
                            <option value="1">Advance Payment (Principal & Interest)</option>
                            <!-- <option value="3">Fully Paid (Pay all remaining principal)</option> -->
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Amount Paid <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="number" class="form-control" id="amount_paid" placeholder="Amount Paid"
                            name="info[amount_paid]"
                            value="<?php echo set_value('info[amount_paid]"', $amount_due); ?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Payment Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('info[payment_type_id]', Dropdown::get_static('payment_types'), set_value('info[payment_type_id]', @$payment_type_id), 'class="form-control" id="payment_type_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Checks </label>
                    <select class="form-control" name="info[post_dated_check_id]" id="post_dated_check_id">
                        <option value="0">Select Checks</option>
                        <?php if ($checks): ?>
                        <?php foreach ($checks as $key => $check) {?>
                        <option value="<?php echo $check['id']; ?>">
                            <?php echo $check['unique_number']; ?>
                        </option>
                        <?php }?>
                        <?php endif?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Cash Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input  type="text" class="form-control" id="cash_amount" placeholder="Cash Amount"
                            name="info[cash_amount]"
                            value="<?php echo set_value('info[cash_amount]"', $cash_amount); ?>" readonly>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Check / Deposit Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input  type="text" class="form-control" id="check_deposit_amount"
                            placeholder="Check / Deposit Amt" name="info[check_deposit_amount]"
                            value="<?php echo set_value('info[check_deposit_amount]"', $check_deposit_amount); ?>" readonly>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Receipt Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="info[receipt_type]" id="receipt_type">
                        <option value="0">Select Type</option>
                        <option value="1">Collection Receipt</option>
                        <option value="2">Acknowledgement Receipt</option>
                        <option value="3">Provisionary Receipt</option>
                        <option value="4">Official Receipt</option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Receipt Number</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="OR_number" placeholder="OR Number"
                            name="info[OR_number]" value="<?php echo set_value('info[OR_number]"', $OR_number); ?>">
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Remarks <span class="kt-font-danger"></span></label>
                    <textarea class="form-control" id="remarks"
                        name="info[remarks]"><?php echo set_value('info[remarks]"', $remarks); ?></textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit-arnote"
                    >
                    Save AR Note
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-7">

        <div class="row">
            <div class="col-sm-3">
                <a class="btn btn-info modal_view view_ar_note" href="javascript: void(0)" class="modal_view"
                    data-url="ticketing/view/0/<?php echo $transaction_id; ?>/ar_notes">VIEW AR NOTES</a>

            </div>

            <div class="col-sm-6">
                <a class="btn btn-success" target="_BLANK"
                    href="<?php echo base_url(); ?>transaction/view/<?php echo $transaction_id; ?>">VIEW MORTGAGE AND
                    COLLECTIONS</a>

            </div>
        </div>

        <div id="detailed_due">
            <?php echo $this->load->view('transaction/view/_detailed_due_information', $data); ?>
        </div>

        <?php echo $this->load->view('transaction/view/_account_information'); ?>

        <?php echo $this->load->view('transaction/view/_billing_information'); ?>
    </div>
</div>