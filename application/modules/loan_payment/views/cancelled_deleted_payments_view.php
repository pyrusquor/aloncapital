<?php


?>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="_document_checklist">
	<thead>
		<tr class="text-center">
			<th>ID</th>
			<th width="15%">Transaction</th>
			<th width="15%">Period</th>
			<th>OR Number</th>
			<th>Payment Date</th>
			<th>Remarks</th>
			<th>Amount Paid</th>
		</tr>
	</thead>
	<tbody>
		<?php if ( isset($records) && $records ): ?>

			<?php foreach ( $records as $key => $r ): ?>

				<?php
                $id = $r['id'];
                $transaction_id = $r['transaction_id'];
                $period_id = $r['period_id'];
                $or_number = $r['or_number'];
                $payment_date = $r['payment_date'];
                $remarks = $r['remarks'];
                $amount_paid = $r['amount_paid'];
                $receipt_type = $r['receipt_type'];

                $transaction = $r['transaction'];

                $reference = $transaction['reference'];
				
				?>

				<tr>
                <td class="text-center" width="1%">
						<?php echo @$id;?>
					</td>
					<td class="text-center" width="1%">
						<?php echo @$reference;?>
					</td>
					<td>
                    <?php echo Dropdown::get_static('period_names', $period_id, 'view'); ?>
					</td>
					<td>
						<?php echo @$or_number;?>
					</td>
					<td class="text-center">
						<?=view_date($payment_date)?>
					</td>

					<td class="text-center">
						<?=$remarks?>
					</td>

					<td class="text-center">
						<?=money_php($amount_paid)?>
					</td>

				
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>
</div>
</div>