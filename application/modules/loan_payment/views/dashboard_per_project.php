<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Collections Dashboard per Project
            </h3>
        </div>
    </div>
</div>

<?php 
    $total_s = 0;
    $total_cp = 0;
?>

<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="propertyContent">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <!-- <button class="btn btn-success" onclick="exportTableToExcel('rems-per-project', 'rems-per-project')">Export Table Data To Excel File</button> -->

            <div class="row">
                <div class="col-md-6 col-sm-6">

                    <table class="table table-striped"  id="rems-per-project">
                        <thead class="thead-dark">
                            <tr>
                                <td>Reference</td>
                                <td>Reservation Date</td>
                                <td>Project</td>
                                <td>Property</td>
                                <td>Seller</td>
                                <td>Buyer</td>
                                <td>Total Collectible Price</td>
                                <td>Total Payments Made</td>
                            </tr>
                        </thead>
                        <tbody>

                        <?php if ($projects): ?>
                            <?php foreach ($projects as $key => $project) { //if(empty($project['total'])){continue;}?>
                                <tr>
                                    <td><a target="_BLANK" href="<?=base_url();?>transaction/view/<?=$project['id'];?>"><?=$project['reference']; ?></td>
                                    <td><?=view_date($project['reservation_date']); ?></td>
                                    <td><?=get_value_field($project['project_id'],'projects','name'); ?></td>
                                    <td><?=get_value_field($project['property_id'],'properties','name'); ?></td>
                                    <td><?=(get_person_name($project['seller_id'],'sellers')); ?></td>

                                    <td><?=(get_person_name($project['buyer_id'],'buyers')); ?></td>
                                    <td>
                                        <?php $total_cp += $project['collectible_price'];  ?>
                                        <b><?=money_php($project['collectible_price']);?></b>
                                    </td>
                                    <td>
                                        <?php $total_s += $project['total_payments_made'];  ?>
                                        <b><?=money_php($project['total_payments_made']);?></b>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php endif ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6">Total</td>
                                <td><?php echo money_php($total_cp); ?></td>
                                <td><?php echo money_php($total_s); ?></td>
                            </tr>
                        </tfoot>
                    </table>

                </div>

                <?php 
                    $total_s = 0;
                    $total_cp = 0;
                ?>
                 <div class="col-md-6 col-sm-6">

                    <table class="table table-striped"  id="rems-per-project">
                        <thead class="thead-dark">
                            <tr>
                                <!-- <td>Reference</td> -->
                                <td>Reservation<br>Date</td>
                                <!-- <td>Project</td> -->
                                <td>Property</td>
                                <td>Buyer</td>
                                <!-- <td>Seller</td> -->
                                <!-- <td>Total Collectible Price</td> -->
                                <td>Total Payments Made</td>
                            </tr>
                        </thead>
                        <tbody>

                        <?php if ($aris_projects): $a = 0;?>
                            <?php foreach ($aris_projects as $key => $project) { if( cleanString(trim(strtolower($projects[$a]['last_name']))) != cleanString(trim(mb_strtolower($project['LASTNAME']))) ){ echo "<tr><td colspan='4'><center>No Transactions<br>".trim(strtolower($projects[$a]['last_name']))." ". trim(mb_strtolower($project['LASTNAME'])) ."</center></td>"; $a++; }  ?>
                                <tr>
                                    <td><?=$project['project_id']." ".$project['DocumentID']." ".view_date($project['RADATE']); ?><br>&nbsp;</td>
                                    <td><?=($project['Lot_no']); ?></td>
                                    <td><?=(($project['FIRSTNAME'])); ?> <?=(($project['LASTNAME'])); ?></td>
                                    <td>
                                        <?php //$total_s += $project['total_principal'] + $project['total_interest'];  ?>

                                        <?php $total_s += $project['total_principal'];  ?>

                                        <!-- <b><?//=money_php($aris_tpm = $project['total_principal'] + $project['total_interest']);?></b> -->

                                        <b><?=money_php($aris_tpm = $project['total_principal']);?></b>

                                        <?php if(number_format($projects[$a]['total_payments_made']) == number_format($aris_tpm)){ echo '<i class="flaticon2-correct"></i>'; } else { echo '<i class="flaticon2-cross" data-rems="'.$projects[$a]['total_payments_made'].'" data-aris="'.$aris_tpm.'"></i>'; } ?>
                                    </td>
                                </tr>
                            <?php $a++; } ?>
                        <?php endif ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">Total</td>
                                <td><?php echo money_php($total_s); ?></td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
               
            </div>
        </div>
    </div>
</div>



<style type="text/css">
    .flaticon2-correct{
        color: green;
    }
     .flaticon2-cross{
        color: red;
    }
</style>