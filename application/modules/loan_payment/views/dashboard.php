<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Total Daily Collections Dashboard
            </h3>
        </div>
    </div>
</div>

<?php 
    $total_s = 0;
?>

<!-- CONTENT -->
<div class="kt-container--fluid kt-grid__item kt-grid__item--fluid" id="propertyContent">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <td>Project Name</td>
                                <td>Total</td>
                                <?php if ($status): ?>
                                    <?php foreach ($status as $key => $stat) { if(empty($key)){ continue; } ?>
                                         <td><?php echo $stat; ?></td>
                                    <?php } ?>
                                <?php endif ?>
                            </tr>
                        </thead>
                        <tbody>

                        <?php if ($projects): ?>
                            <?php foreach ($projects as $key => $project) { if(empty($project['total'])){continue;}?>
                                <tr>
                                    <td><?=$key; ?></td>
                                    <td>
                                        <?php   //echo $project['total']; 
                                                $total_s += $project['total']; 
                                        ?>
                                        <!-- <div class="progress progress-lg"> -->
                                            <!-- <div class="progress-bar progress-bar-striped progress-lg" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"> -->
                                                <b><?=money_php($project['total']);?></b>
                                            <!-- </div> -->
                                        <!-- </div>    -->
                                    </td>
                                    <?php if ($status): ?>
                                        <?php foreach ($status as $key => $stat) { if(empty($key) || ($project == 'total')){ continue; } ?>
                                            <?php $p = round(($project[$stat] / $project['total']) * 100) ;?>
                                             <td>
                                                <!-- <div class="progress progress-lg"> -->
                                                    <!-- <div class="progress-bar progress-bar-striped progress-bar-animated  bg-success" role="progressbar" style="width: <?=$p;?>%;" aria-valuenow="<?=$p;?>" aria-valuemin="0" aria-valuemax="100"> -->
                                                        <b><?=money_php($project[$stat]);?></b></div>
                                                <!-- </div>    -->
                                                <?//=$project[$stat];  //$total_s += $project[$stat] ?>
                                            </td>
                                        <?php } ?>
                                    <?php endif ?>
                                </tr>
                            <?php } ?>
                        <?php endif ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td><?php echo money_php($total_s); ?></td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
