<?php

$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
$amount_paid = isset($info['amount_paid']) && $info['amount_paid'] ? $info['amount_paid'] : 0;

$interest_amount = isset($info['interest_amount']) && $info['interest_amount'] ? $info['interest_amount'] : 0;
$principal_amount = isset($info['principal_amount']) && $info['principal_amount'] ? $info['principal_amount'] : 0;

$rc_bank_name = isset($info['rc_bank_name']) && $info['rc_bank_name'] ? $info['rc_bank_name'] : "";
$rc_check_number = isset($info['rc_check_number']) && $info['rc_check_number'] ? $info['rc_check_number'] : 0;

$check_deposit_amount = isset($info['check_deposit_amount']) && $info['check_deposit_amount'] ? $info['check_deposit_amount'] : 0;
$cash_amount = isset($info['cash_amount']) && $info['cash_amount'] ? $info['cash_amount'] : 0;
$adhoc_payment = isset($info['adhoc_payment']) && $info['adhoc_payment'] ? $info['adhoc_payment'] : 0;
$grand_total = isset($info['grand_total']) && $info['grand_total'] ? $info['grand_total'] : 0;
$rebate_amount = isset($info['rebate_amount']) && $info['rebate_amount'] ? $info['rebate_amount'] : 0;

$payment_date = isset($info['payment_date']) && $info['payment_date'] ? $info['payment_date'] : '';
$or_date = isset($info['or_date']) && $info['or_date'] ? $info['or_date'] : '';
$OR_number = isset($info['OR_number']) && $info['OR_number'] ? $info['OR_number'] : 'AR-';
$AR_number = isset($info['AR_number']) && $info['AR_number'] ? $info['AR_number'] : '';

$loan = isset($loan) && $loan ? $loan : array();
$reference = isset($loan['reference']) && $loan['reference'] ? $loan['reference'] : '';

$applicant_id = isset($loan['applicant_id']) && $loan['applicant_id'] ? $loan['applicant_id'] : '';

$applicant = get_person($applicant_id, 'applicants');
$applicant_name = get_fname($applicant);


                                                       
$loan_amount = isset($loan['loan_amount']) && $loan['loan_amount'] ? ($loan['loan_amount']) : '';
$loan_period = isset($loan['loan_period']) && $loan['loan_period'] ? $loan['loan_period']." Mo(s)" : '';
$loan_interest = isset($loan['loan_interest']) && $loan['loan_interest'] ? $loan['loan_interest']." %" : '';
$terms = isset($loan['terms']) && $loan['terms'] ? Dropdown::get_static('loan_terms',$loan['terms'],'view') : '';

$this->loan_library->initiate($loan['id']);

$total_interest_amount = $this->loan_library->get_amount(0,8);
$total_paid_interest = $this->loan_library->get_amount_paid(3,1,0);
$total_paid_principal = $this->loan_library->get_amount_paid(2,1,0);

$monthly_principal_amount = $loan_amount / $loan['loan_period'];

$payments = isset($loan['payments']) && $loan['payments'] ? ($loan['payments']) : '';

?>

<div class="row">
    <div class="col-md-5">
        <input type="hidden" id="loan_id" name="loan_id" value="<?php echo $loan_id; ?>">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Applicant / Borrower Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control" data-module="applicants" id="applicant_id">
                        <?php if ($applicant_id) : ?>
                            <option value="<?= $applicant_id; ?>" selected><?php echo $applicant_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Reference <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="reference">
                        <?php if ($reference) : ?>
                            <option selected><?php echo $reference; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
           
        </div>

        <div class="row hide">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Period Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('info[period_id]', Dropdown::get_static('period_names'), set_value('info[period_id]', 1), 'class="form-control" id="period_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date of Payment <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="payment_date" placeholder="Date of Payment" name="info[payment_date]" value="<?php echo date("Y-m-d"); ?>" readonly>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Receipt Date</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="or_date" placeholder="OR Date" name="info[or_date]" value="<?php echo date("Y-m-d"); ?>" readonly>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Principal Amount<span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="principal_amount" placeholder="Principal Amount" name="info[principal_amount]" value="<?php echo set_value('info[principal_amount]"', $principal_amount); ?>" data-total-amount="<?php echo $principal_amount; ?>">
                    </div>
                </div>
            </div>
             <div class="col-sm-6">
                <div class="form-group">
                    <label>Interest Amount<span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="interest_amount" placeholder="Interest Amount" name="info[amount_paid]" value="<?php echo set_value('info[interest_amount]"', $interest_amount); ?>" data-total-amount="<?php echo $interest_amount; ?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 hide">
                <div class="form-group">
                    <label>Payment Application <span class="kt-font-danger">*</span></label>
                    <div class="">
                        <select class="form-control" name="payment_application" id="payment_application">
                            <option selected value="0">Regular Payment (Exact amount only, with interest or penalty)</option>
                            <option value="2">Balloon Payment (Interest and apply all surplus to Principal)</option>
                            <option value="1">Advance Payment / Multiple Months (Principal & Interest)</option>
                            <option value="3">Continuous Payment / Multiple Months (Apply all surplus to Principal)</option>
                            <option value="4">Partial Payment</option>
                            <option value="5">Special Payment</option>
                            <!-- <option value="9">TEST</option> -->
                            <!-- <option value="3">Fully Paid (Pay all remaining principal)</option> -->
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Amount Paid <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="amount_paid" placeholder="Amount Paid" name="info[amount_paid]" value="<?php echo set_value('info[amount_paid]"', $amount_paid); ?>" data-total-amount="<?php echo $amount_paid; ?>">
                    </div>
                </div>
            </div>
        </div>


        <div class="row hide">

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Payment Type <span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('info[payment_type_id]', Dropdown::get_static('payment_types'), set_value('info[payment_type_id]', 1), 'class="form-control" id="payment_type_id"'); ?>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

            <div class="col-sm-6 hide">
                <div class="form-group">
                    <label class="">Checks </label>
                    <select class="form-control" name="info[post_dated_check_id]" id="post_dated_check_id">
                        <option value="0">Select Checks</option>
                        <?php if ($checks) : ?>
                            <?php foreach ($checks as $key => $check) { ?>
                                <option value="<?php echo $check['id']; ?>">
                                    <?php echo $check['unique_number']; ?>
                                </option>
                            <?php } ?>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>

        </div>

        <div class="row hide" data-field-type="regular_cheque">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Bank Name</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="rc_bank_name" placeholder="Bank Name" name="info[rc_bank_name]" value="<?php echo set_value('info[rc_bank_name]"', $rc_bank_name); ?>">
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Check Number</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="rc_check_number" placeholder="Check Number" name="info[rc_check_number]" value="<?php echo set_value('info[rc_check_number]"', $rc_check_number); ?>">
                    </div>
                </div>
            </div>

        </div>

        <div class="row hide">

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Cash Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input readonly type="text" class="form-control" id="cash_amount" placeholder="Cash Amount" name="info[cash_amount]" value="<?php echo set_value('info[cash_amount]"', $cash_amount); ?>">
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Check / Deposit Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input readonly type="text" class="form-control" id="check_deposit_amount" placeholder="Check / Deposit Amt" name="info[check_deposit_amount]" value="<?php echo set_value('info[check_deposit_amount]"', $check_deposit_amount); ?>">
                    </div>
                </div>
            </div>

        </div>



        <div class="row">
            <!--  <div class="col-sm-6">
                <div class="form-group">
                    <label>AR # <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control popField" id="AR_number" placeholder="AR Number" name="info[AR_number]" value="<?php echo set_value('info[AR_number]"', $AR_number); ?>">
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Receipt Type <span class="kt-font-danger">*</span></label>
                    <select class="form-control" name="info[receipt_type]" id="receipt_type">
                        <option value="0">Select Type</option>
                        <option value="1">Collection Receipt</option>
                        <option value="2" selected>Acknowledgement Receipt</option>
                        <option value="3">Provisionary Receipt</option>
                        <option value="4">Official Receipt</option>
                        <option value="5">Manual Receipt</option>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    <label>Receipt Number</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control" id="OR_number" placeholder="OR Number" name="info[OR_number]" value="<?php echo set_value('info[OR_number]"', $OR_number); ?>">
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="row hide">
            <div class="col-sm-6">
                <label>Adhoc Fee Payment </label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input type="text" class="form-control" id="adhoc_payment" placeholder="Amount" name="info[adhoc_payment]" value="<?php echo set_value('info[adhoc_payment]"', $adhoc_payment); ?>">
                </div>
            </div>
            <div class="col-sm-6">
                <label>Grand Total </label>
                <div class="kt-input-icon kt-input-icon--left">
                    <input readonly type="text" class="form-control" id="grand_total" placeholder="Amount" name="info[grand_total]" value="<?php echo set_value('info[grand_total]"', $grand_total); ?>">
                </div>
            </div>
        </div>

        <div class="row mt-3 ">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Remarks <span class="kt-font-danger"></span></label>
                    <textarea class="form-control" id="remarks" name="info[remarks]"></textarea>
                </div>
            </div>
        </div>



    </div>

    <div class="col-md-7 ">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Billing Information -->
                    <div class="kt-portlet">
                        <div class="accordion accordion-solid accordion-toggle-svg" id="accord_account_information">
                            <div class="card">
                                <div class="card-header" id="head_account_information">
                                    <div class="card-title" data-toggle="collapse" data-target="#account_information" aria-expanded="true" aria-controls="account_information">
                                        Account Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                                <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                                <div id="account_information" class="collapse show" aria-labelledby="head_account_information" data-parent="#accord_account_information">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-xs row">
                                                    
                                                    <div class="col-sm-4">
                                                        <span href="#" class="kt-notification-v2__item">
                                                            <div class="kt-notification-v2__itek-wrapper">
                                                                <div class="kt-notification-v2__item-title">
                                                                    <h7 class="kt-portlet__head-title kt-font-primary">Loan Application Type</h7>
                                                                </div>
                                                                <div class="kt-notification-v2__item-desc">
                                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                                        <?php echo $type = Dropdown::get_static('applicant_type',$loan['applicant']['type_id'],"view"); ?>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        &nbsp;
                                                    </div>
                                                    
                                                </div>

                                                <div class="form-group form-group-xs row">
                                                
                                                    <div class="col-sm-4">
                                                        <span href="#" class="kt-notification-v2__item">
                                                            <div class="kt-notification-v2__itek-wrapper">
                                                                <div class="kt-notification-v2__item-title">
                                                                    <h7 class="kt-portlet__head-title kt-font-primary">Principal Loan Amount :</h7>
                                                                </div>
                                                                <div class="kt-notification-v2__item-desc">
                                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                                        <?php echo money_php($loan_amount); ?>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <span href="#" class="kt-notification-v2__item">
                                                            <div class="kt-notification-v2__itek-wrapper">
                                                                <div class="kt-notification-v2__item-title">
                                                                    <h7 class="kt-portlet__head-title kt-font-primary"> Loan Period (Mos) :</h7>
                                                                </div>
                                                                <div class="kt-notification-v2__item-desc">
                                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                                        <?php echo ($loan_period); ?>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    
                                                     <div class="col-sm-4">
                                                        <span href="#" class="kt-notification-v2__item">
                                                            <div class="kt-notification-v2__itek-wrapper">
                                                                <div class="kt-notification-v2__item-title">
                                                                    <h7 class="kt-portlet__head-title kt-font-primary"> Loan Interest Rate (%) :</h7>
                                                                </div>
                                                                <div class="kt-notification-v2__item-desc">
                                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                                        <?php echo money_php($total_interest_amount); ?> - <?php echo ($loan_interest); ?>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>


                                                  <div class="form-group form-group-xs row">
                                                
                                                    <div class="col-sm-4">
                                                        <span href="#" class="kt-notification-v2__item">
                                                            <div class="kt-notification-v2__itek-wrapper">
                                                                <div class="kt-notification-v2__item-title">
                                                                    <h7 class="kt-portlet__head-title kt-font-primary">Monthly Principal Amount :</h7>
                                                                </div>
                                                                <div class="kt-notification-v2__item-desc">
                                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                                        <?php echo money_php($monthly_principal_amount); ?>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>

                                                   
                                                </div>


                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">

                                                <table class="table table-striped- table-bordered table-hover">
                                                    <tr>
                                                        <td> Paid Principal </td>
                                                        <td> Remaining Principal </td>
                                                        <td> Paid Interest </td>
                                                        <td> Remaining Interest </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo money_php($paid_principal); ?></td>
                                                        <td><?php echo money_php($loan_amount - $paid_principal); ?></td>
                                                        <td><?php echo money_php($paid_interest); ?></td>
                                                        <td><?php echo money_php($total_interest_amount - $paid_interest); ?></td>
                                                    </tr>
                                                </table>
                                                
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- General Information -->
                                                <div class="kt-portlet">
                                                    <div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
                                                        <div class="card">
                                                            <div class="card-header" id="head_general_information">
                                                                <div class="card-title" data-toggle="collapse" data-target="#general_information" aria-expanded="true" aria-controls="general_information">
                                                                    Loan Schedule <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                                                            <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                                                            <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                                                        </g>
                                                                    </svg>
                                                                </div>

                                                            </div>


                                                            <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                                                            <div id="general_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
                                                                <div class="card-body">

                                                                    <div class="form-group form-group-xs row">
                                                                        <table class="table table-striped- table-bordered table-hover" id="m_schedule">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Particulars</th>
                                                                                    <th>Due Date</th>
                                                                                    <th>Beginning Balance</th>
                                                                                    <th>Ending Balance</th>
                                                                                    <th>Principal</th>
                                                                                    <th>Interest</th>
                                                                                    <th>Initial Amount Due</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                    if ($payments) :

                                                                                        $previous_official_id = 0;
                                                                                        $receipt_count = 0;$end_receipt =0;
                                                                                        $previous_balance = 0;
                                                                                        $prev_paids = 0;
                                                                                        $prev_or = 0;
                                                                                        $prev_period_id = 0;
                                                                                        $or_number = '';
                                                                                        $previous_period = 0;
                                                                                        $previous_payment = 0;
                                                                                        $previous_amount_due = 0;
                                                                                        $t_amt_due = 0;
                                                                                        $payment_to_complete = 0;
                                                                                        

                                                                                        foreach ($payments as $key => $payment) { 
                                                                                            $e = 0; $end_receipt = 0; 
                                                                                            $status = "";
                                                                                            $penalty = 0;
                                                                                            $rebate = "";

                                                                                            $prev_key = $key > 0 ? $key - 1 : 0;
                                                                                            $gcash = "";

                                                                                            

                                                                                            if (is_due($payment['due_date'])) {
                                                                                                $status = "table-danger";
                                                                                            }

                                                                                            if (($payment['is_paid']) && ($payment['is_complete'])) {
                                                                                                $status = "table-success";
                                                                                                $gcash = "hide";
                                                                                            } else if (($payment['is_paid']) && ($payment['is_complete'] == 0)) {
                                                                                                $status = "table-warning";
                                                                                            }

                                                                                            $total_amount_paid = [];
                                                                                            $period_count = 0;

                                                                                            $penalty =  "";
                                                                                            $amount_due = "";
                                                                                            $payment_date = "";
                                                                                            $amount_paid = "";
                                                                                            $receipt = "";
                                                                                            $remarks = "";
                                                                                            $paid_principal = '';
                                                                                            $paid_interest = '';

                                                                                            $beginning_balance = $payment['beginning_balance'];
                                                                                            $ending_balance = $payment['ending_balance'];
                                                                                            $principal = $payment['principal_amount'];
                                                                                            $interest  = $payment['interest_amount'];
                                                                                            $og_total_amount = $payment['total_amount'];
                                                                                            
                                                                                ?>
                                                                                        <tr class="<?php echo $status; ?>">
                                                                                            <td><?php echo $payment['particulars']; ?></td>

                                                                                            <td><?php echo view_date_custom($payment['due_date']); ?></td>

                                                                                            <td><?php echo money_php($beginning_balance); ?></td>

                                                                                            <td><?php echo money_php($ending_balance); ?> </td>

                                                                                            <td><?php echo money_php($principal);  ?></td>

                                                                                            <td><?php echo money_php($interest); ?></td>

                                                                                            <td><?php echo money_php($og_total_amount); ?> </td>

                                                                                        </tr>
                                                                                    <?php $prev_period_id = $payment['period_id'];  if($end_receipt){ $receipt_count = 0; }
                                                                                    } ?>
                                                                                <?php endif ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

