<!--begin: Basic Transaction Info-->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
    <?php $this->load->view('_info_form'); ?>
</div>

<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" id="schedule">
    <div class="row">
        <div class="col-12">
            
        </div>
    </div>
</div>

<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" id="collection">
   
</div>

<!--begin: Form Actions -->
<div class="kt-form__actions">
    
    <button class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u mx-3"
        data-ktwizard-type="action-submit" id="submit_btn">
        Submit
    </button>
   
</div>

<!--end: Form Actions -->