<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Collect Adhoc Fee</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?= isset($transaction_id) ? base_url("transaction/view/$transaction_id") : base_url('transaction') ?>" class="btn btn-label-instagram"><i class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--">
            <div class="kt-grid__item">
                <form class="kt-form" method="POST" action="<?php echo base_url("transaction_payment/adhoc_fee/$transaction_id"); ?>" id="form" enctype="multipart/form-data">
                    <?php $this->load->view('adhoc_fee/_form'); ?>
                </form>
            </div>
        </div>
    </div>
</div>