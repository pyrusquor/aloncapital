<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loan_payment extends MY_Controller
{   

    private $fields = [
        array(
            'field' => 'info[period_id]',
            'label' => 'Period Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[payment_type_id]',
            'label' => 'Payment Type',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[amount_paid]',
            'label' => 'Amount Paid',
            'rules' => 'trim|required',
        ),

        array(
            'field' => 'info[payment_date]',
            'label' => 'Payment Date',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('loan/Loan_model', 'M_Loan');
        $this->load->model('loan/Loan_payment_model', 'M_Loan_payment');
        $this->load->model('loan/Loan_official_payment_model', 'M_Loan_official_payment');
        $this->_table_fillables = $this->M_Loan->fillable;
        $this->_table_columns = $this->M_Loan->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Loan->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Loan->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showLoans()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'reference' => true,
            'applicant_id' => true,
            'application_at' => true,
            'loan_amount' => true,
            'loan_period' => true,
            'status' => true,
            'status_id' => true,
            'terms' => true,
            'created_by' => true,
            'updated_by' => true
            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        if ($this->ion_auth->is_buyer()) {
            $buyer_id = get_value_field($this->session->userdata['user_id'], 'applicants', 'id', 'user_id');
            // $this->M_Loan->where("created_by", $buyer_id);
            $this->M_Loan->where("applicant_id", $buyer_id);
        }
        $loan = $this->M_Loan->with_applicant()->order_by('id', 'DESC')->as_array()->get_all();
        // vdebug($loan);
        $data = [];

        if ($loan) {

            foreach ($loan as $key => $value) {
                $type = Dropdown::get_static('applicant_type',$value['applicant']['type_id'],"view");

                $loan[$key]['applicant_id'] = '<div><a href="/applicant/view/' . $value['applicant_id'] . '" target="_blank">' . get_person_name($value['applicant_id'], "applicants") . '</a><br>'.$type.'</div>';

                $loan[$key]['application_at'] = '<div>' . ($value['application_at'] ? view_date($value['application_at']) : '') . '</div>';

                $loan[$key]['loan_amount'] = money_php($value['loan_amount']);

                $loan[$key]['loan_period'] = ($value['loan_period']." Mo(s)");

                $loan[$key]['status'] = Dropdown::get_static('loan_status',$value['status'],"view")."<br>".$value['status_remarks'];

                $loan[$key]['status_id'] = $value['status'];

                $loan[$key]['terms'] = Dropdown::get_static('loan_terms',$value['terms'],"view");

                $loan[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                $loan[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
            }

            foreach ($loan as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }

        echo json_encode($output);
        exit();
    }

    public function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();

             // Image Upload
            $upload_path = './assets/uploads/applicant/files';
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png|pdf|docx|zip|jpeg';
            $config['max_size'] = 5000;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config, 'applicant_files');
            $this->applicant_files->initialize($config);

            // vdebug($_FILES);

            if (!empty($_FILES['coe_bir_certificate']['name'])) {
                if (!$this->applicant_files->do_upload('coe_bir_certificate')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['coe_bir_certificate'] = $img_data['file_name'];
                    // vdebug($img_data);
                }
            }

            if (!empty($_FILES['latest_financial']['name'])) {
                if (!$this->applicant_files->do_upload('latest_financial')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['latest_financial'] = $img_data['file_name'];
                }
            }

            
            if (!empty($_FILES['latest_payslip_itr_company']['name'])) {
                if (!$this->applicant_files->do_upload('latest_payslip_itr_company')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['latest_payslip_itr_company'] = $img_data['file_name'];
                }
            }

            if (!empty($_FILES['latest_annual_proprietor_borrower']['name'])) {
                if (!$this->applicant_files->do_upload('latest_annual_proprietor_borrower')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['latest_annual_proprietor_borrower'] = $img_data['file_name'];
                }
            }

             if (!empty($_FILES['latest_bank_statement']['name'])) {
                if (!$this->applicant_files->do_upload('latest_bank_statement')) {
                    $this->notify->error($this->applicant_files->display_errors(), 'loan/create');
                    // vdebug($this->applicant_files->display_errors());
                } else {
                    $img_data = $this->applicant_files->data();
                    $_input['latest_bank_statement'] = $img_data['file_name'];
                }
            }

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW
            ];
            $this->M_Loan->insert($_input + $additional);



            // $_u_full_name = $post['first_name'] . " " .$post['middle_name'] . " " .$post['last_name'];
    
            // $recipient['subject'] = "Alon Capital Registration";
            // $recipient['name'] = $_u_full_name;
            // $recipient['email'] = $post['email'];

            // $content = "You're successfully registered to Alon Capital.";

            // $this->communication_library->send_email($recipient, $content);


            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Loan successfully created.', 'loan');
            }
        }

        $this->template->build('create');
    }


    public function form($loan_id = 0, $id = 0)
    {   

        if ($this->input->post()) {

            $loan_id = $this->input->post('loan_id');

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);

            if ($this->form_validation->run() === true) {

                $post = $this->input->post();

                $response = $this->insert_payment($post);

            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

       if ($loan_id) {

            $this->view_data['loan_id'] = $loan_id;

            $this->view_data['loan'] = $this->M_Loan->with_applicant()->with_payments()->get($loan_id);

            $this->view_data['info'] = $loan = $this->M_Loan->with_applicant()->with_payments()->get($loan_id);

            if ($loan['status'] != 4) {
                $response['status'] = 0;
                $response['msg'] = 'Oops! Please refresh the page and try again.';

                $this->notify->error("Sorry! The current loan is not approved! Please contact the administration if you want to proceed with this loan.", 'loan');
            }

            $this->template->build('form/form', $this->view_data);
        } else {

            redirect('loan');
        }

    }
    

     public function insert_payment($post = array(), $internal = 0, $debug = 0, $array_payment = 0)
    {
        // vdebug($post);

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        $loan_id = $post['loan_id'];

        $this->db->trans_start(); # Starting Loan
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

        $loan_id = $this->M_Loan->get($loan_id);

        $is_waived = 0;
        $payment_application = $post['payment_application']; // 1 = normal, 2 = balloon, 3 continuous payment

        // remove commas from the $post data so that the ar clearing will display the correct decimal places

        $post['info']['amount_paid'] = remove_commas($post['info']['amount_paid']);
        $post['info']['adhoc_payment'] = 0;
        $post['info']['grand_total'] = remove_commas($post['info']['amount_paid']);
        $post['info']['check_deposit_amount'] = 0;

        // unset($post['info']['is_waived']);
        // unset($post['info']'payment_application']);

        $info = $post['info'];

        // $off_p['entry_id'] = 0;
        // $off_p['ar_number']     = $info['AR_number'];1q
        $off_p['loan_id'] = $post['loan_id'];
        $off_p['or_number'] = $info['OR_number'];
        $off_p['receipt_type']     = $info['receipt_type'];
        $off_p['or_date'] = $info['or_date'];
        $off_p['payment_date'] = $info['payment_date'];
        $off_p['amount_paid'] = $info['amount_paid'];
        $off_p['adhoc_payment'] = $info['adhoc_payment'];
        $off_p['rebate_amount'] = 0;
        $off_p['grand_total'] = $info['grand_total'];
        $off_p['remarks'] = $info['remarks'];
        $off_p['payment_type_id'] = $info['payment_type_id'];
        $off_p['period_id'] = $info['period_id'];
        $off_p['period_count'] = 1;
        $off_p['rc_check_number'] = $info['rc_check_number'];
        $off_p['rc_bank_name'] = $info['rc_bank_name'];
        $off_p['payment_application'] = $payment_application;
        $period_id = $info['period_id'];

        $amount_paid = $off_p['amount_paid'];

        $relative = $this->get_relative_amount($amount_paid, $off_p['loan_id'], $off_p['period_id'], $payment_application, $debug, $array_payment);
        $u_payments = $relative['payments'];

        if ($u_payments) {

            if ($payment_application == "3") {
                $off_p['principal_amount'] = floatval($relative['principal']) + floatval($relative['interest']);
                $off_p['interest_amount'] = 0;
                $off_p['post_dated_check_id'] = $info['post_dated_check_id'];

                $relative['principal'] = $relative['principal'] + $relative['interest'];
                $relative['interest'] = 0;
            } else {
                $off_p['principal_amount'] = $relative['principal'];
                $off_p['interest_amount'] = $relative['interest'];
                $off_p['post_dated_check_id'] = $info['post_dated_check_id'];
            }

            $off_p['period_count'] = $relative['period_count'];

            $off_payment_id = $this->M_Loan_official_payment->insert($off_p + $additional);


            if ($u_payments) {
                foreach ($u_payments as $key => $u_payment) {
                    $u_additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $_u_payment['is_paid'] = $u_payment['is_paid'];
                    $_u_payment['is_complete'] = $u_payment['is_complete'];
                    $this->M_Loan_payment->update($_u_payment + $u_additional, array('id' => $u_payment['id']));

                    $off_p_update['loan_payment_id'] = $u_payment['id'];
                    $this->M_Loan_official_payment->update($off_p_update, array('id' => $off_payment_id));

                    $period_id = $u_payment['period_id'];
                }
            }

            if ($info['period_id'] == 1) {
                $period_id = 2;
            }

            if ((($payment_application == 3) || ($payment_application == 2)) && ($relative['amount_paid'])) {
                $b_amount_paid = $relative['amount_paid'];
                $b_period_id = $info['period_id'];
                $b_loan_payment_id = $relative['loan_payment_id'];

                $this->balloon($b_loan_payment_id, $relative, 1);

                if ($array_payment) {
                    $this->M_loan_payment->update(array('interest_amount' => $array_payment[1]), $b_loan_payment_id);
                }
            }

            if (($payment_application == 5) && ($relative['amount_paid'])) {
                $b_amount_paid = $relative['amount_paid'];
                $b_period_id = $info['period_id'];
                $b_loan_payment_id = $relative['loan_payment_id'];
                $relative['aris_is_paid'] = 1;

                $this->balloon($b_loan_payment_id, $relative, 1);
            }

            // do this as library
            // $_loan_data['period_id'] = $period_id;
            // $this->M_Loan->update($_loan_data + $u_additional, array('id' => $off_p['loan_id']));
            // $this->loan_library->initiate($loan_id);

            // $this->loan_library->update_period_status();

            // $_property_data['status'] = 3;
            // $this->M_property->update($_property_data + $u_additional, array('id' => $loan['property_id']));
        } elseif ($payment_application == 9) {

            $off_p['loan_payment_id'] = $post['loan_payment_id'];
            $off_p['amount_paid'] = $off_p['penalty_amount'];
            $off_p['remarks'] = 'Penalty Only';

            $off_payment_id = $this->M_Loan_official_payment->insert($off_p + $additional);

            $this->loan_library->generate_entries($off_payment_id, false, true, $post);

            if (!empty($off_p['penalty_amount'])) {
                $this->insert_penalty_logs($off_payment_id, $loan_id, $post);
            }
            $this->loan_library->initiate($loan_id);

            $this->loan_library->update_period_status();
        } else {
            $response['status'] = 0;
            $response['message'] = 'Oops! Something went wrong. Please check your payment details';

            return $response;
        }

        $this->db->trans_complete(); # Completing loan

        /*Optional*/
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $this->db->trans_commit();
            $response['status'] = 1;
            $response['message'] = 'Payment Successfully Created!';
        }
        return $response;
    }

     public function get_relative_amount($amount_paid = 0, $loan_id = 0, $period_id = 0, $payment_application = 0, $debug = 0, $array_payment = 0)
    {
        // http://13.251.190.131/rems-sales/loan_payment/get_relative_amount/100000/30/3/0/1
        // http://localhost/rems/loan_payment/get_relative_amount/56800/46/2/2/1
        $original_amount_paid = $amount_paid;
        $params['loan_id'] = $loan_id;
        $params['period_id'] = $period_id;
        $params['is_complete'] = 0;

        $order['period_id'] = 'ASC';
        $order['id'] = 'ASC';

        $payments = $this->M_Loan_payment->where($params)->order_by($order)->get_all();

        $return['payments'] = array();

        $no = 0;

        if ($debug == 1) {
            vdebug($payments);
        }

        if ($debug == 2) {
            vdebug($amount_paid);
        }
        $stop = 0;
        $return['principal'] = 0;
        $return['interest'] = 0;
        $f_principal = 0;
        $f_interest = 0;

        if ($payments) {
            foreach ($payments as $key => $payment) {

                if ((($payment_application == 3)  || ($payment_application == 2) || $payment_application == 5 || $payment_application == 9)  && ($stop)) { // advance payment go to balloon payment function
                    $return['principal'] = $amount_paid + $return['principal'];
                    break;
                }
                $p_id = $payment['id'];
                $stop = 1;

                $is_complete = 0;

                $interest = $payment['interest_amount'];
                $principal = $payment['principal_amount'];
                $total = $interest + $principal;

                if ($payment['is_paid'] == 1) {
                    $existing_principal = 0;
                    $existing_interest = 0;
                    $param['loan_id'] = $loan_id;
                    $param['loan_payment_id'] = $payment['id'];
                    $official_payments = $this->M_Loan_official_payment->where($param)->get_all();
                    $f_op_id = 0;

                    if ($official_payments) {
                        foreach ($official_payments as $key => $official_payment) {
                            $existing_principal = $official_payment['principal_amount'] + $existing_principal;
                            $existing_interest = $official_payment['interest_amount'] + $existing_interest;


                            if ($payment_application == 4) {
                                # code...
                                $paramsx['loan_id'] = $loan_id;
                                $paramsx['period_id'] = $period_id;
                                $paramsx['is_complete'] = 1;
                                $paramsx['id <'] = $official_payment['loan_payment_id'];

                                $order['period_id'] = 'DESC';
                                $order['id'] = 'DESC';

                                $f_payments = $this->M_Loan_payment->with_official_payments()->where($paramsx)->order_by($order)->get_all();

                                if ($f_payments) {
                                    $f_principal = 0;
                                    $f_interest = 0;
                                    foreach ($f_payments as $key => $f_payment) {

                                        if (isset($f_payment['official_payments']) && !empty($f_payment['official_payments'])) {
                                            break;
                                        }

                                        $f_principal = $f_payment['principal_amount'] + $f_principal;
                                        $f_interest = $f_payment['interest_amount'] + $f_interest;
                                    }
                                }

                                $f_op_id = $official_payment['loan_payment_id'];
                            }
                        }
                    }


                    // vdebug($official_payments);

                    if ($payment['id'] == $f_op_id) {
                        $payment['principal_amount'] = $f_principal +  $payment['principal_amount'];
                        $payment['interest_amount'] = $f_interest + $payment['interest_amount'];
                    }

                    $principal = number_format($payment['principal_amount'], 2, '.', '') - number_format($existing_principal, 2, '.', '');
                    $interest = number_format($payment['interest_amount'], 2, '.', '') - number_format($existing_interest, 2, '.', ''); // 4385.24 - 9311.77
                    $total = $interest + $principal;
                    $stop = 0;
                }

                if ($debug == 6) {
                    echo "<pre>";
                    echo "amount_paid: " . $amount_paid;
                    echo "<br>";

                    echo "existing_principal: " . $existing_principal;
                    echo "<br>";
                    echo "existing_interest: " . $existing_interest;
                    echo "<br>";

                    echo "f_principal: " . $f_principal;
                    echo "<br>";
                    echo "f_interest: " . $f_interest;
                    echo "<br>";

                    echo "p_principal: " . $payment['principal_amount'];
                    echo "<br>";
                    echo "p_interest: " . $payment['interest_amount'];
                    echo "<br>";

                    echo "principal: " . $principal;
                    echo "<br>";
                    echo "interest: " . $interest;
                    echo "<br>";
                    echo "<br>";
                    vdebug($f_payments);
                }


                if (($amount_paid == 0) || ($amount_paid <= .1)) {
                    break;
                }

                if ($interest) {

                    if (trim($amount_paid) >= trim($interest)) {
                        $return['interest'] = @$return['interest'] + $interest;
                        $amount_paid = $amount_paid - $interest;
                    } else {
                        $return['interest'] = @$return['interest'] + $amount_paid;
                        $amount_paid = 0;
                    }
                }

                // $rem_p = 0;

                if ($principal) {

                    // $rem_p = trim($amount_paid) - trim($principal);

                    if (trim($amount_paid) >= trim($principal)) {

                        $return['principal'] = @$return['principal'] + $principal;
                        $amount_paid = $amount_paid - $principal;
                        $is_complete = 1;
                    } else {

                        $return['principal'] = @$return['principal'] + $amount_paid;
                        $amount_paid = 0;
                    }
                }

                $return['payments'][$key]['id'] = $payment['id'];
                $return['payments'][$key]['is_paid'] = 1;

                // Temporarily force payment application 3 to is_complete, remove after syncing.
                if ($payment_application == 3) {
                    $return['payments'][$key]['is_complete'] = 1;
                } else {
                    $return['payments'][$key]['is_complete'] = $is_complete;
                }
                $return['payments'][$key]['period_id'] = $payment['period_id'];

                // if($payment_application==9){
                //     $return['payments'][$key]['amount_paid'] = 0;
                //     $return['payments'][$key]['principal'] = 0;
                //     $return['payments'][$key]['interest'] = 0;
                //     $total_amount = 0;

                // } else{
                $return['payments'][$key]['amount_paid'] = $amount_paid;
                $return['payments'][$key]['principal'] = $principal;
                $return['payments'][$key]['interest'] = $interest;
                $total_amount = $payment['total_amount'];
                // }
            }

            // if ( ($amount_paid < $return['principal']) && ($payment_application == 2) ) {
            //     $amount_paid = $return['principal'];
            //     $no = 1;
            // }
            if ($payment_application == 9) {
                $original_amount_paid = 0;
                $total_amount = 0;
            }

            $return['loan_payment_id'] = $p_id;
            $return['amount_paid'] = $original_amount_paid;
            $return['total_amount'] = $total_amount;
            $return['period_count'] = $payment_application == 9 ? 0 : is_nan(round(($total_amount / $original_amount_paid))) ? 1 : round(($total_amount / $original_amount_paid));
            $return['exact'] = $no;
        }

        if ($array_payment) {
            $return['payments']['0']['principal'] = $array_payment[0];
            $return['payments']['0']['interest'] = $array_payment[1];
            $return['principal'] = $array_payment[0];
            $return['interest'] = $array_payment[1];
        }

        if ($debug == 3) {
            vdebug($amount_paid);
        }

        if ($debug == 4) {
            vdebug($principal);
        }

        if ($debug == 5) {
            vdebug($return);
        }
        return $return;
    }

    public function process_loan_schedule($loan_id = 0,$debug = 0){

        $this->M_Loan_payment->delete(array('loan_id' => $loan_id));

        $loan = $this->M_Loan->get($loan_id);
        $loan_period = $loan['loan_period'];
        $loan_amount = $loan['loan_amount'];
        $loan_interest = $loan['loan_interest'];
        $date = $loan['loan_effectivity_date'];
        $monthly_payment = ( $loan['loan_amount'] / $loan_period );
        $factor_rate = 2;
        $sum_amount = 0;

        for( $i = 1; $i <= ( $loan_period ); $i++) {
            $countterm = $i;

            /* MONTHLY BREAKDOWN START */
            $breakdown['monthly'][$i]['month'] = $i;
        
            if( $i == 1 ) {
                $breakdown['monthly'][$i]['beginning_balance'] = round( $loan_amount, $factor_rate );
            } else {
                $breakdown['monthly'][$i]['beginning_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], $factor_rate );
            }

            $interest = round( $breakdown['monthly'][$i]['beginning_balance'] * $this->compute_percentage( $loan_interest ) , $factor_rate );
            
            $breakdown['monthly'][$i]['interest'] = $interest;

            $breakdown['monthly'][$i]['principal'] = $monthly_payment;

            $breakdown['monthly'][$i]['total_amount'] = $monthly_payment + $interest;
                
            $breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beginning_balance'] - $monthly_payment, $factor_rate );

            $due_date = add_months_to_date($date, ($i-1));
            $breakdown['monthly'][$i]['due_date'] = $due_date;

            $next_payment_date = add_months_to_date($due_date, 1);
            $breakdown['monthly'][$i]['next_payment_date'] = $next_payment_date;
            /* MONTHLY BREAKDOWN END */

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            if (!$debug) {

                $payment['loan_id'] = $loan_id;
                $payment['total_amount'] =  $monthly_payment + $interest;
                $payment['principal_amount'] =$monthly_payment;
                $payment['interest_amount'] = $interest;
                $payment['beginning_balance'] =  $breakdown['monthly'][$i]['beginning_balance'];
                $payment['ending_balance'] =  $breakdown['monthly'][$i]['ending_balance'] ;
                $payment['period_id'] = 1;
                $payment['particulars'] = ordinal($countterm) . ' Payment';
                $payment['is_paid'] = 0;
                $payment['is_complete'] = 0;
                $payment['due_date'] = $due_date;
                $payment['next_payment_date'] = $next_payment_date;
                $this->M_Loan_payment->insert($payment + $additional);
            }

            if ($breakdown['monthly'][$i]['ending_balance'] < 0) {
                break;
            }
        }

        return true;

        if ($debug) {
           vdebug($breakdown);
        }
    }
    public function update($id = false)
    {
        if ($id) {

            $this->view_data['loan'] = $data = $this->M_Loan->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();
                    $_input['updated_by'] = $this->session->userdata['user_id'];

                    $result = $this->M_Loan->from_form()->update($_input, $data['id']);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'loan');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function update_status()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';
        $loan_id = $this->input->post('loan_id');
        $status = $this->input->post('status_id');
        $remarks = $this->input->post('remarks');

        if ($loan_id) {

            $update = [
                'status' => $status,
                'status_remarks' => $remarks,
                'updated_by' => $this->user->id,
                'updated_at' => NOW,
            ];

            $id = $this->M_Loan->update($update, $loan_id);

            if ($id) {
                $response['status'] = 1;
                $response['message'] = $remarks;
            } else {
                $response['status'] = 0;
                $response['message'] = 'Error!';
            }
        } else {
            $response['status'] = 0;
            $response['message'] = 'Error!';
        }

        echo json_encode($response);
        exit();
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Loan->get($id);
            if ($list) {

                $deleted = $this->M_Loan->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_Loan->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Type Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = FALSE,$debug = FALSE)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['loan'] = $this->M_Loan->with_applicant()->with_payments()->get($id);

            if ($this->view_data['loan']) {
                if ($debug) {
                    vdebug($this->view_data);
                }
                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        /* ==================== begin: Add model fields ==================== */
                        'bank_id' =>  $upload_data[1],
                        'bank' =>  $upload_data[2],
                        'branch' =>  $upload_data[3],
                        'amount' =>  $upload_data[4],
                        'unique_number' =>  $upload_data[5],
                        'due_date' =>  $upload_data[6],
                        'particulars' =>  $upload_data[7],
                        'status' =>  $upload_data[8],
                        'is_active' =>  $upload_data[9],
                        /* ==================== end: Add model fields ==================== */
                    );

                    $loan_id = $upload_data[0];
                    $loan = $this->M_Loan->get($loan_id);

                    if ($loan) {
                        $result = $this->M_Loan->update($fields, $loan_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'bank_id' =>  $upload_data[0],
                            'bank' =>  $upload_data[1],
                            'branch' =>  $upload_data[2],
                            'amount' =>  $upload_data[3],
                            'unique_number' =>  $upload_data[4],
                            'due_date' =>  $upload_data[5],
                            'particulars' =>  $upload_data[6],
                            'status' =>  $upload_data[7],
                            'is_active' =>  $upload_data[8],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Loan->insert($fields);
                    } else {
                        $fields = array(
                            /* ==================== begin: Add model fields ==================== */
                            'bank_id' =>  $upload_data[1],
                            'bank' =>  $upload_data[2],
                            'branch' =>  $upload_data[3],
                            'amount' =>  $upload_data[4],
                            'unique_number' =>  $upload_data[5],
                            'due_date' =>  $upload_data[6],
                            'particulars' =>  $upload_data[7],
                            'status' =>  $upload_data[8],
                            'is_active' =>  $upload_data[9],
                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Loan->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'loan');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'loan');
        die();
    }

    public function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued    =    $this->input->post('update_existing_data');

            $_is_update    =    $_ued === '1' ? TRUE : FALSE;

            $_alphas        =    [];
            $_datas            =    [];

            $_titles[]    =    'id';

            $_start    =    3;
            $_row        =    2;

            $_filename    =    'Loan CSV Template.csv';

            $_fillables    =    $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'loan');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[]    =    $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group    =    $this->M_Loan->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title]    =    isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas            =    $this->__get_excel_columns(count($_titles));
            $_xls_columns    =    array_combine($_alphas, $_titles);
            $_firstAlpha    =    reset($_alphas);
            $_lastAlpha        =    end($_alphas);

            $_objSheet    =    $this->excel->getActiveSheet();
            $_objSheet->setTitle('Loan');
            $_objSheet->setCellValue('A1', 'CHEQUE');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style    =    array(
                'font'  => array(
                    'bold'    =>    TRUE,
                    'size'    =>    10,
                    'name'    =>    'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function export()
    {

        $_db_columns    =    [];
        $_alphas            =    [];
        $_datas                =    [];

        $_titles[]    =    '#';

        $_start    =    3;
        $_row        =    2;
        $_no        =    1;

        $loans    =    $this->M_Loan->as_array()->get_all();
        if ($loans) {

            foreach ($loans as $_lkey => $loan) {

                $_datas[$loan['id']]['#']    =    $_no;

                $_no++;
            }

            $_filename    =    'list_of_loans_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet    =    $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column    =    $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey]    =    isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'loan');
                }
            } else {

                $_filename    =    'list_of_loans_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns =   $this->M_land_inventory->fillable;
                $_db_columns    =    $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'loan');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name    =    isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column    =    $_name;

                            $_name    =    isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($loans as $_lkey => $loan) {

                                $_datas[$loan['id']][$_title]    =    isset($loan[$_column]) && ($loan[$_column] !== '') ? Dropdown::get_static('bool', $loan[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]    =    $_title =    isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($loans as $_lkey => $loan) {

                                if ($_name === 'status') {

                                    $_datas[$loan['id']][$_title]    =    isset($loan[$_name]) && $loan[$_name] ? Dropdown::get_static('inventory_status', $loan[$_name], 'view') : '';
                                } else {

                                    $_datas[$loan['id']][$_title]    =    isset($loan[$_name]) && $loan[$_name] ? $loan[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas    =    $this->__get_excel_columns(count($_titles));

                $_xls_columns    =    array_combine($_alphas, $_titles);
                $_firstAlpha    =    reset($_alphas);
                $_lastAlpha        =    end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title    =    ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Loan');
                $_objSheet->setCellValue('A1', 'LIST OF CHEQUE');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value    =    isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style    =    array(
                        'font'  => array(
                            'bold'    =>    FALSE,
                            'size'    =>    9,
                            'name'    =>    'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style    =    array(
                    'font'  => array(
                        'bold'    =>    TRUE,
                        'size'    =>    10,
                        'name'    =>    'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'loan');
            }
        } else {

            $this->notify->error('No Record Found', 'loan');
        }
    }

    public function compute_percentage( $value = 0 ) {
    
        if( $value ) {

            return $value / 100;

        }

    }
}
