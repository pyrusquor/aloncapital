<?php
//print_r($icon); exit;
$id = isset($nav_menu['id']) && $nav_menu['id'] ? $nav_menu['id'] : '';
$name = isset($nav_menu['name']) && $nav_menu['name'] ? $nav_menu['name'] : '';
// ==================== begin: Add model fields ====================
$description = isset($nav_menu['description']) && $nav_menu['description'] ? $nav_menu['description'] : '';
$parent_id = isset($nav_menu['parent_id']) && $nav_menu['parent_id'] ? $nav_menu['parent_id'] : '';
$link = isset($nav_menu['link']) && $nav_menu['link'] ? $nav_menu['link'] : '';
$icon = isset($nav_menu['icon']) && $nav_menu['icon'] ? $nav_menu['icon'] : '';
$ctr = isset($nav_menu['ctr']) && $nav_menu['ctr'] ? $nav_menu['ctr'] : '';

$group_id = isset($nav_menu['group_access']['group_id']) && $nav_menu['group_access']['group_id'] ? $nav_menu['group_access']['group_id'] : '';
$module_id = isset($nav_menu['group_access']['module_id']) && $nav_menu['group_access']['module_id'] ? $nav_menu['group_access']['module_id'] : '';
$permission = isset($nav_menu['group_access']['permission']) && $nav_menu['group_access']['permission'] ? $nav_menu['group_access']['permission'] : '';

// ==================== end: Add model fields ====================

?>

<div class="kt-portlet__head kt-portlet__head--noborder">
    <div class="kt-portlet__head-label">
        <h5>Navigation Menu</h5>
    </div>
    <div class="kt-portlet__head-toolbar"></div>
</div>
<div class="kt-portlet__body">
    
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-section__body">
                <!-- <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                    <div class="col-lg-9 col-xl-6">
                        <div class="kt-avatar kt-avatar--outline kt-avatar--circle--" id="kt_apps_user_add_avatar">
                            <div class="kt-avatar__holder" style="background-image: url(<?php echo base_url(); ?>assets/media/users/default.jpg)"></div>
                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                <i class="fa fa-pen"></i>
                                <input type="file" name="kt_apps_contacts_add_avatar">
                            </label>
                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                <i class="fa fa-times"></i>
                            </span>
                        </div>
                    </div>
                </div> -->
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Menu Name</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="<?php echo set_value('name', $name); ?>" name="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Description</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="<?php echo set_value('description', $description); ?>" name="description">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Parent ID</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control datePicker" type="text" value="<?php echo set_value('parent_id', $parent_id); ?>" name="parent_id">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Link</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="<?php echo set_value('link', $link); ?>" name="link">
                        <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Icon</label>
                    <div class="col-lg-9 col-xl-9">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                            <input type="text" class="form-control" value="<?php echo set_value('icon', $icon); ?>" name="icon" placeholder="" aria-describedby="basic-addon1">
                        </div>
                        <!--<span class="form-text text-muted">We'll never share your email with anyone else.</span>-->
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">CTR</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="<?php echo set_value('ctr', $ctr); ?>" name="ctr">
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>