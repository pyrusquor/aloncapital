<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nav_menu_model extends MY_Model
{
    public $table = 'modules'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'name',
        'description',
        'parent_id',
        'link',
        'icon',
        'ctr',
        'created_by',
        'created_at',
        'updated_by',
        'updated_date',
        'is_active',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        ),
        'parent_id' => array(
            'field' => 'parent_id',
            'label' => 'Parent ID',
            'rules' => 'trim'
        ),
        'link' => array(
            'field' => 'link',
            'label' => 'Link',
            'rules' => 'trim'
        ),
        'icon' => array(
            'field' => 'icon',
            'label' => 'Icon',
            'rules' => 'trim'
        ),
        'ctr' => array(
            'field' => 'ctr',
            'label' => 'CTR',
            'rules' => 'trim'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        //$this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['group_access'] = array('foreign_table'=>'group_access','foreign_key'=>'id');

    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
