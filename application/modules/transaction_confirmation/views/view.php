<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$department_id = isset($data['department_id']) && $data['department_id'] ? $data['department_id'] : '';
$category = isset($data['category']) && $data['category'] ? $data['category'] : '';
$username = isset($data['username']) && $data['username'] ? $data['username'] : '';
$password = isset($data['password']) && $data['password'] ? $data['password'] : '';
$last_name = isset($data['last_name']) && $data['last_name'] ? $data['last_name'] : '';
$first_name = isset($data['first_name']) && $data['first_name'] ? $data['first_name'] : '';
$position = isset($data['position']) && $data['position'] ? $data['position'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
  <div class="kt-container kt-container--fluid">
    <div class="kt-subheader__main">
      <h3 class="kt-subheader__title">Transaction Confirmation</h3>
    </div>
    <div class="kt-subheader__toolbar">
      <div class="kt-subheader__wrapper">
        <a href="<?php echo site_url('transaction_confirmation/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
          <i class="fa fa-edit"></i> Edit Transaction Confirmation
        </a>   
        <a href="<?php echo site_url('transaction_confirmation');?>" class="btn btn-label-instagram btn-sm btn-elevate">
          <i class="fa fa-reply"></i> Back
        </a>
      </div>
    </div>
  </div>
</div>

<!-- begin:: Content -->
<div
  class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
  <div class="row">
    <div class="col-md-6">
      <!--begin::Portlet-->
      <div class="kt-portlet">
        <div class="kt-portlet__body">
          <!--begin::Portlet-->
          <div class="kt-portlet">
            <div class="kt-portlet__head">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                  General Information
                </h3>
              </div>
            </div>
            <div class="kt-portlet__body">
              <!--begin::Form-->
              <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Department
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$department_id?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Category
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$category?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Username
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$username?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Password
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$password?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Last Name
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$last_name?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    First Name
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$first_name?></span>
                </div>
                <div class="kt-widget13__item">
                  <span pan class="kt-widget13__desc">
                    Position
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$position?></span>
                </div>
                <!-- ==================== begin: Add fields details  ==================== -->

                <!-- ==================== end: Add model details ==================== -->
              </div>
              <!--end::Form-->
            </div>
          </div>
          <!--end::Portlet-->
        </div>
      </div>
      <!--end::Portlet-->

    </div>

    <div class="col-md-6"></div>
  </div>
</div>
<!-- begin:: Footer -->
