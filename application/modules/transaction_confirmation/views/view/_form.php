<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$department_id = isset($info['department_id']) && $info['department_id'] ? $info['department_id'] : '';
$department = get_value_field($department_id,'departments','name');
$category = isset($info['category']) && $info['category'] ? $info['category'] : '';
$username = isset($info['username']) && $info['username'] ? $info['username'] : '';
$password = isset($info['password']) && $info['password'] ? $info['password'] : '';
$last_name = isset($info['last_name']) && $info['last_name'] ? $info['last_name'] : '';
$first_name = isset($info['first_name']) && $info['first_name'] ? $info['first_name'] : '';
$position = isset($info['position']) && $info['position'] ? $info['position'] : '';
// ==================== end: Add model fields ====================
$category_arr = json_decode($category);
?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="">Add Category <span class="kt-font-danger"></span><i class="small">(adhoc_fee, cancel, compute_refund, confirm, discount, due_date, financing_scheme, open)</i></label>
                    <input type="text" class="form-control"
                        placeholder="Category"
                        autocomplete="off" id="category_value">
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-md-4 align-self-center">
                <button type="button" class="btn btn-primary" id="add_category">Insert</button>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <textarea class="d-none" id="category" name="category"><?=$category?></textarea>
            <label>Category <span class="kt-font-danger">*</span></label>
            <select id="categories" class="suggests form-control" multiple="">
                <?php foreach($category_arr as $key => $value): ?>
                    <option value="<?=$value?>" selected><?=$value?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Username <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="username"
                   value="<?php echo set_value('username', $username); ?>" placeholder="Username"
                   autocomplete="off" id="username">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group <?php echo !$id ? 'd-none' : ''; ?>">
            <label class="">Current Password <span class="kt-font-danger">*</span></label>
            <input type="password" class="form-control" name="old_password"
                   placeholder="Password"
                   autocomplete="off" id="old_password">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Password <span class="kt-font-danger">*</span></label>
            <input type="password" class="form-control" name="password"
                   placeholder="Password"
                   autocomplete="off" id="password">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Confirm Password <span class="kt-font-danger">*</span></label>
            <input type="password" class="form-control"
                   placeholder="Confirm Password"
                   autocomplete="off" id="confirm_password">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Last Name <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="last_name"
                   value="<?php echo set_value('last_name', $last_name); ?>" placeholder="Last Name"
                   autocomplete="off" id="last_name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">First Name <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="first_name"
                   value="<?php echo set_value('first_name', $first_name); ?>" placeholder="First Name"
                   autocomplete="off" id="first_name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Department <span class="kt-font-danger"></span></label>
            <select class="form-control suggests popField" data-module="departments" id="department_id" name="department_id">
                <option value="">Select Department</option>
                <?php if ($department_id): ?>
                    <option value="<?php echo $department_id; ?>" selected><?php echo $department; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Position <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="position"
                   value="<?php echo set_value('position', $position); ?>" placeholder="Position"
                   autocomplete="off" id="position">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>