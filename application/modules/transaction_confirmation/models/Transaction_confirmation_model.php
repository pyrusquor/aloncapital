<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_confirmation_model extends MY_Model
{
    public $table = 'transaction_confirmation'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'department_id',
        'category',
        'username',
        'password',
        'last_name',
        'first_name',
        'position',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
    public $fields = [
        array(
            'field' => 'department_id',
            'label' => 'Department ID',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'category',
            'label' => 'Category',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'trim',
        ),
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'trim',
        ),
        array(
            'field' => 'position',
            'label' => 'Position',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert']	=	$this->fields;
        $this->rules['update']	=	$this->fields;

        $this->has_one['department'] = array('foreign_model'=>'department/department_model','foreign_table'=>'departments','foreign_key'=>'id','local_key'=>'department_id');

    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}