<div class="row">








    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Content <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                
                
                
                
                    <textarea class="form-control" id="content" name="content" v-model="info.form.content" rows="5"></textarea>
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Object Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                
                
                
                
                
                    <input type="text" class="form-control" id="object_type" name="object_type" placeholder="Object Type" v-model="info.form.object_type">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Object Type ID <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                
                
                    <input type="number" min="0" step="0.01" class="form-control" id="object_type_id" name="object_type_id" placeholder="Object Type ID" v-model="info.form.object_type_id">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

</div>