<?php
    $name = isset($info['name']) && $info['name'] ? $info['name'] : '';

    $description = isset($info['description']) && $info['description'] ? $info['description'] : '';

    $is_active = isset($info['is_active']) && $info['is_active'] ? $info['is_active'] : '0';
?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="name" value="<?php echo set_value('name', @$name); ?>" placeholder="Name">

            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Description <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="description" value="<?php echo set_value('description', @$description); ?>" placeholder="Description">

            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Status <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('is_active', Dropdown::get_static('production_status'), set_value('is_active', @$is_active), 'class="form-control" required'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>