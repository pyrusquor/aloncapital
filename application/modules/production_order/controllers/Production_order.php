<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Production_order extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$production_order_model = array(
			'production_order/Production_order_model' => 'M_production_order',
		);

		// Load models
		$this->load->model($production_order_model);


		// Load pagination library
		$this->load->library('ajax_pagination');

		// Format Helper
		$this->load->helper(['format', 'images']); // Load Helper

		// Per page limit
		$this->perPage = 12;

		$this->_table_fillables = $this->M_production_order->fillable;
		$this->_table_columns = $this->M_production_order->__get_columns();

		$this->u_additional = [
			'updated_by' => $this->user->id,
			'updated_at' => NOW,
		];

		$this->additional = [
			'is_active' => 1,
			'created_by' => $this->user->id,
			'created_at' => NOW,
		];

		$this->load->helper('format');
	}

	public function index()
	{
		$_fills = $this->_table_fillables;
		$_colms = $this->_table_columns;

		$this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns'] = $this->__get_columns($_fills);

		$db_columns = $this->M_production_order->get_columns();
		if ($db_columns) {
			$column = [];
			foreach ($db_columns as $key => $dbclm) {
				$name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

				if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
					if (strpos($name, '_id') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('_id', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} elseif (strpos($name, 'is_') !== false) {
						$column = $name;
						$column[$key]['value'] = $column;
						$name = isset($name) && $name ? str_replace('is_', '', $name) : '';
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					} else {
						$column[$key]['value'] = $name;
						$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
						$column[$key]['label'] = ucwords(strtolower($_title));
					}
				} else {
					continue;
				}
			}

			$column_count = count($column);
			$cceil = ceil(($column_count / 2));

			$this->view_data['columns'] = array_chunk($column, $cceil);
			$column_group = $this->M_production_order->count_rows();
			if ($column_group) {
				$this->view_data['total'] = $column_group;
			}
		}

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function showProductionOrders()
	{
		$output = ['data' => ''];

		$columnsDefault = [
			'id' => true,
			'name' => true,
			'description' => true,
			'is_active' => true,
			'created_by' => true,
			'updated_by' => true
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}

		// get all raw data
		$production_orders = $this->M_production_order->as_array()->get_all();

		$data = [];

		if ($production_orders) {

			foreach ($production_orders as $key => $value) {

				$production_orders[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

				$production_orders[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
			}

			foreach ($production_orders as $d) {
				$data[] = $this->filterArray($d, $columnsDefault);
			}

			// count data
			$totalRecords = $totalDisplay = count($data);

			// filter by general search keyword
			if (isset($_REQUEST['search'])) {
				$data = $this->filterKeyword($data, $_REQUEST['search']);
				$totalDisplay = count($data);
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
				foreach ($_REQUEST['columns'] as $column) {
					if (isset($column['search'])) {
						$data = $this->filterKeyword($data, $column['search'], $column['data']);
						$totalDisplay = count($data);
					}
				}
			}

			// sort
			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

				$_column = $_REQUEST['order'][0]['column'] - 1;
				$_dir = $_REQUEST['order'][0]['dir'];

				usort($data, function ($x, $y) use ($_column, $_dir) {

					$x = array_slice($x, $_column, 1);

					$x = array_pop($x);

					$y = array_slice($y, $_column, 1);
					$y = array_pop($y);

					if ($_dir === 'asc') {

						return $x > $y ? true : false;
					} else {

						return $x < $y ? true : false;
					}
				});
			}

			// pagination length
			if (isset($_REQUEST['length'])) {
				$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
			}

			// return array values only without the keys
			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
				$tmp = $data;
				$data = [];
				foreach ($tmp as $d) {
					$data[] = array_values($d);
				}
			}
			$secho = 0;
			if (isset($_REQUEST['sEcho'])) {
				$secho = intval($_REQUEST['sEcho']);
			}

			$output = array(
				'sEcho' => $secho,
				'sColumns' => '',
				'iTotalRecords' => $totalRecords,
				'iTotalDisplayRecords' => $totalDisplay,
				'data' => $data,
			);
		}

		echo json_encode($output);
		exit();
	}

	public function form($id = false)
	{

		$method = "Create";
		if ($id) {
			$method = "Update";
		}

		if ($this->input->post()) {

			$response['status'] = 0;
			$response['msg'] = 'Oops! Please refresh the page and try again.';

			$info = $this->input->post();

			if ($id) {

				$result = $this->M_production_order->update($info, $id);
			} else {

				$result = $this->M_production_order->insert($info);
			}


			if ($result === false) {

				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Production Order successfully' . " " . $method, 'production_order');
			}
		}

		if ($id) {
			$this->view_data['info'] = $info = $this->M_production_order->get($id);
		}

		$this->view_data['method'] = $method;

		$this->template->build('form', $this->view_data);
	}

	public function view($id = false)
	{
		if ($id) {
			$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
			$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

			$this->view_data['production_order'] = $this->M_production_order->get($id);

			if ($this->view_data['production_order']) {

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {
			show_404();
		}
	}

	public function delete()
	{
		$response['status'] = 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id = $this->input->post('id');
		if ($id) {

			$list = $this->M_production_order->get($id);
			if ($list) {

				$deleted = $this->M_production_order->delete($list['id']);
				if ($deleted !== false) {

					$response['status'] = 1;
					$response['message'] = 'Production Order Successfully Deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function bulkDelete()
	{
		$response['status'] = 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_production_order->delete($value);
				}
				if ($deleted !== false) {

					$response['status'] = 1;
					$response['message'] = 'Production Order/s Successfully Deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	function export()
	{

		$_db_columns = [];
		$_alphas = [];
		$_datas = [];
		$_extra_datas = [];
		$_adatas = [];

		$_titles[] = '#';

		$_start = 3;
		$_row = 2;
		$_no = 1;

		$production_orders = $this->M_production_order->as_array()->get_all();


		if ($production_orders) {

			foreach ($production_orders as $skey => $production_order) {

				$_datas[$production_order['id']]['#'] = $_no;

				$_no++;
			}

			$_filename = 'list_of_production_orders' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style = array(
				'font'  => array(
					'bold' => TRUE,
					'size' => 10,
					'name' => 'Verdana'
				)
			);

			$_objSheet = $this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column = $this->input->post('_export_column');
				$_additional_column = $this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

						$_column = $_name;

						$_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

						$_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

						foreach ($production_orders as $skey => $production_order) {

							if ($_column === 'is_active') {

								$_datas[$production_order['id']][$_title] = isset($production_order[$_column]) && $production_order[$_column] ? Dropdown::get_static('production_status', $production_order[$_column], 'view') : 'Inactive';
							} else {

								$_datas[$production_order['id']][$_title] = isset($production_order[$_name]) && $production_order[$_name] ? $production_order[$_name] : '';
							}
						}
					} else {

						continue;
					}
				}

				$_alphas = $this->__get_excel_columns(count($_titles));

				$_xls_columns = array_combine($_alphas, $_titles);
				$_lastAlpha	 = end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title = ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('List of Production Orders');
				$_objSheet->setCellValue('A1', 'LIST OF PRODUCTION ORDERS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($production_orders as $key => $production_order) {

					$production_order_id = isset($production_order['id']) && $production_order['id'] ? $production_order['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title = ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$production_order_id][$_xls_columns[$_alpha]]) && $_datas[$production_order_id][$_xls_columns[$_alpha]] ? $_datas[$production_order_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns = $_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							if ($_a_column === 'contact') {

								$ad_title = 'Contact Informations';
							} elseif ($_a_column === 'reference') {

								$ad_title = 'References';
							} elseif ($_a_column === 'source') {

								$ad_title = 'Source of Informations';
							} elseif ($_a_column === 'academic') {

								$ad_title = 'Academic History';
							} else {

								$ad_title = $_a_column;
							}

							$a_title = ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$production_order_id][$_a_column])) {

									foreach ($_extra_datas[$production_order_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if ($key === 'to_report' or $key === 'to_attend' or $key === 'commission_based' or $key === 'is_member') {
												$xa_value =	isset($_extra_datas[$production_order_id][$_a_column][$key]) && ($_extra_datas[$production_order_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$production_order_id][$_a_column][$key], 'view') : '';
											} else {
												$xa_value = $_extra_datas[$production_order_id][$_a_column][$key];
											}

											$xa_titles = isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$production_order_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$production_order_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$production_order_id][$_a_column])) {

									foreach ($_extra_datas[$production_order_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					}

					$_start += 1;
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'production_order');
			}
		} else {

			$this->notify->error('No Record Found', 'production_order');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued =	$this->input->post('update_existing_data');

			$_is_update = $_ued === '1' ? TRUE : FALSE;

			$_alphas = [];
			$_datas = [];

			$_titles[] = 'id';

			$_start = 3;
			$_row =	2;

			$_filename = 'Production Order CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables = $this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'production_order');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[] = $_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records = $this->M_production_order->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas = $this->__get_excel_columns(count($_titles));
			$_xls_columns =	array_combine($_alphas, $_titles);
			$_firstAlpha =	reset($_alphas);
			$_lastAlpha = end($_alphas);

			$_objSheet = $this->excel->getActiveSheet();
			$_objSheet->setTitle('Production Orders');
			$_objSheet->setCellValue('A1', 'PRODUCTION ORDERS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style = array(
						'font' => array(
							'bold' => FALSE,
							'size' => 9,
							'name' => 'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font' => array(
					'bold' => TRUE,
					'size' => 10,
					'name' => 'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	public function import()
	{

		$file = $_FILES['csv_file']['tmp_name'];
		$inputFileType = PHPExcel_IOFactory::identify($file);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($file);

		$sheetData = $objPHPExcel->getActiveSheet()->toArray();
		// print_r($sheetData); exit;
		$err = 0;

		foreach ($sheetData as $key => $upload_data) {

			if ($key > 0) {

				if ($this->input->post('status') == '1') {
					$fields = array(
						'name' => $upload_data[1],
						'description' => $upload_data[2],
						'is_active' => $upload_data[3],
					);

					$production_order_id = $upload_data[0];
					$production_order = $this->M_production_order->get($production_order_id);

					if ($production_order) {
						$result = $this->M_production_order->update($fields, $production_order_id);
					}
				} else {

					if (!is_numeric($upload_data[0])) {
						$fields = array(
							'name' => $upload_data[0],
							'description' => $upload_data[1],
							'is_active' => $upload_data[2],
						);

						$result = $this->M_production_order->insert($fields);
					} else {
						$fields = array(
							'name' => $upload_data[1],
							'description' => $upload_data[2],
							'is_active' => $upload_data[3],
						);

						$result = $this->M_production_order->insert($fields);
					}
				}
				if ($result === false) {

					// Validation
					$this->notify->error('Oops something went wrong.');
					$err = 1;
					break;
				}
			}
		}

		if ($err == 0) {
			$this->notify->success('CSV successfully imported.', 'production_order');
		} else {
			$this->notify->error('Oops something went wrong.');
		}

		header('Location: ' . base_url() . 'production_order');
		die();
	}
}
