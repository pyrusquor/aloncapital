<?php
	$id =	isset($staff['id']) && $staff['id'] ? $staff['id'] : '';
	$user_id =	isset($staff['user_id']) && $staff['user_id'] ? $staff['user_id'] : '';
	$first_name =	isset($staff['first_name']) && $staff['first_name'] ? $staff['first_name'] : '';
	$middle_name =	isset($staff['middle_name']) && $staff['middle_name'] ? $staff['middle_name'] : '';
	$last_name =	isset($staff['last_name']) && $staff['last_name'] ? $staff['last_name'] : '';
	$position =	isset($staff['position']) && $staff['position'] ? $staff['position'] : '';
	$birthday =	isset($staff['birthday']) && $staff['birthday'] ? $staff['birthday'] : '';
	$contact =	isset($staff['contact']) && $staff['contact'] ? $staff['contact'] : '';
	$area =	isset($staff['area']) && $staff['area'] ? $staff['area'] : '';
	$city =	isset($staff['city']) && $staff['city'] ? $staff['city'] : '';

	$regCode =	isset($staff['regCode']) && $staff['regCode'] ? get_value_field($staff['regCode'],"address_regions","regDesc","regCode") : '';
	$provCode =	isset($staff['provCode']) && $staff['provCode'] ? get_value_field($staff['provCode'],"address_provinces","provDesc","provCode") : '';
	$citymunCode =	isset($staff['citymunCode']) && $staff['citymunCode'] ? get_value_field($staff['citymunCode'],"address_city_municipalities","citymunDesc","citymunCode") : '';
	$brgyCode =	isset($staff['brgyCode']) && $staff['brgyCode'] ? get_value_field($staff['brgyCode'],"address_barangays","brgyDesc","brgyCode") : '';

	$address = $brgyCode." ".$citymunCode." ".$provCode." ".$regCode;

	$username =	isset($staff['user']['username']) && $staff['user']['username'] ? $staff['user']['username'] : '';
	$email =	isset($staff['user']['email']) && $staff['user']['email'] ? $staff['user']['email'] : '';
	$active =	isset($staff['user']['active']) && $staff['user']['active'] ? $staff['user']['active'] : '';
	$group_name =	isset($staff['user_group']->description) && $staff['user_group']->description ? $staff['user_group']->description : '';
	$fullname = ucfirst($first_name) .' '.ucfirst($last_name);
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Staff's Profile</h3>
		</div>
		<div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo base_url('staff/update/'.$id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                	<i class="fa fa-edit"></i>Edit
                </a>
				<?php if($active == 1): ?>
					<a href="<?php echo base_url('staff/deactivate/'.$user_id); ?>" class="btn btn-label-primary">
						<i class="fa fa-power-off"></i>Deactivate
					</a>
				<?php endif; ?>
				<?php if($active == 0): ?>
					<a href="<?php echo base_url('staff/activate/'.$user_id); ?>" class="btn btn-label-primary">
						<i class="fa fa-power-off"></i>Activate
					</a>
				<?php endif; ?>
				<a href="<?php echo base_url('staff'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>&nbsp;
            </div>
        </div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
		<div class="col-md-12 col-xl-12">
			<!--Begin:: Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__body">
					<div class="kt-widget kt-widget--user-profile-3">
						<div class="kt-widget__top">
							<div class="kt-widget__media">
								<?php $img = get_value_field($user_id,'staff','image','user_id'); //vdebug($img); ?>
								<?php if (get_image('staff', 'images', @$img)) : ?>
									<img alt="image" src="<?php echo base_url(get_image('staff', 'images', @$img)); ?>" />
                    			<?php endif; ?>
							</div>
							<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-bolder kt-font-light kt-hidden">
								JM
							</div>
							<div class="kt-widget__content">
								<div class="kt-widget__head">
									<div class="kt-widget__user">
										<a href="#" class="kt-widget__username">
											<?php echo $fullname; ?>
										</a>
										&nbsp;&nbsp;
										<span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success"><?php echo $group_name; ?></span>
									</div>
									<div class="kt-widget__action">
										
									</div>
								</div>
								<div class="kt-widget__subhead">
									<a href="javascript: void(0)"><i class="flaticon2-user"></i><?php echo $username; ?></a>
									<a href="javascript: void(0)"><i class="flaticon2-new-email"></i><?php echo $email; ?></a>
									<a href="javascript: void(0)"><i class="flaticon2-rectangular"></i><?php echo $position; ?></a>
									<a href="javascript: void(0)"><i class="flaticon2-shelter"></i>REMS</a>
									<a href="javascript: void(0)"><i class="flaticon2-information"></i><?php echo ($active == 1 ? 'Active' : 'Inactive'); ?></a>
								</div>
								<div class="kt-widget__info">
									<div class="kt-widget__desc">
										<!-- <strong>PROJECTS:</strong> Project 1, Project 2, Project 3, Project 4
										<br> 
										<strong>PROPERTIES:</strong> Property 1, Property 2, Property 3, Property 4 -->
									</div>
									<!--<div class="kt-widget__progress">
										<div class="kt-widget__text">
											Goals
										</div>
										<div class="progress" style="height: 5px;width: 100%;">
											<div class="progress-bar kt-bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<div class="kt-widget__stats">
											45%
										</div>
									</div>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="col-md-4 col-xl-4">
			<!--Begin:: Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Personal Information
						</h3>
					</div>
				</div>
				<div class="kt-form kt-form--label-right">
					<div class="kt-portlet__body">
						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Name:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder"><?php echo $fullname; ?></span>
							</div>
						</div>

						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Address:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder"><?php echo $address; ?></span>
							</div>
						</div>

						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Position:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder"><?php echo $position; ?></span>
							</div>
						</div>

						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Birthdate:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder"><?=view_date($birthday);?></span>
							</div>
						</div>

						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Email:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder">
									<a href="mailto:<?=($email);?>" target="_blank"><?=($email);?></a>
								</span>
							</div>
						</div>

						<div class="form-group form-group-xs row">
							<label class="col-4 col-form-label">Contact Number:</label>
							<div class="col-8">
								<span class="form-control-plaintext kt-font-bolder"><?=$contact;?></span>
							</div>
						</div>
						
						

					</div>
					<!--<div class="kt-portlet__foot"></div>-->
				</div>
			</div>

			<!--End:: Portlet-->
		</div>
		
		<div class="col-md-8 col-xl-8">
			<div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">User Activities</h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="height: 700px;overflow: scroll;">
					<table class="table table-striped- table-bordered table-hover table-checkable" id="audit_trail_table">
						<thead>
							<tr>
			                    <!-- ==================== begin: Add header fields ==================== -->
			                    <th>Affected ID</th>
			                    <th>Affected Table</th>
			                    <th>Action</th>
			                    <th>Remarks</th>
			                    <!-- ==================== end: Add header fields ==================== -->
							</tr>
						</thead>
						<tbody>
							<?php if ($audit_trail): ?>
								<?php foreach ($audit_trail as $key => $audit) { ?>
								<tr>
									<td><?=$audit['affected_id'];?></td>
									<td><?=$audit['affected_table'];?></td>
									<td><?=$audit['action'];?></td>
									<td><?=$audit['remarks'];?></td>
								</tr>
								<?php } ?>
							<?php endif ?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Login History</h3>
                    </div>
                </div>
                <div class="kt-portlet__body" style="height: 700px;overflow: scroll;">
					<table class="table table-striped- table-bordered table-hover table-checkable" id="login_history_table">
						<thead>
							<tr>
				                <th>IP Address</th>
				                <th>Login Date</th>
							</tr>
						</thead>
						<tbody>
							<?php if ($login_history): ?>
								<?php foreach ($login_history as $key => $history) { ?>
								<tr>
									<td><?=$history['ip_address'];?></td>
									<td><?=view_hrdate($history['created_at']);?></td>
								</tr>
								<?php } ?>
							<?php endif ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>