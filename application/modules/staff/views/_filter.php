<div class="row">
    <?php if (!empty($record)) : ?>
        <?php foreach ($record as $r): ?>
        <?php 
            if(isset($r['user_permission'])) {
                // $permission_data = get_person($r['user_permission']['permission_id'], 'permissions');
                // $permission = get_name($permission_data);

                $permission = get_value_field($r['user_permission']['permission_id'],'permissions','name');
            } else {
                $permission = 'N/A';
            }
        ?>
            <div class="col-md-4 col-xl-3">
                <!--Begin::Portlet-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>"
                                       class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                                <span></span>
                            </label>
                        </div>

                        <div class="kt-portlet__head-toolbar">

                            <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                <i class="flaticon-more-1 kt-font-brand"></i>
                            </a>
                            <?php if (isset($r['user'])) { ?>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="<?php echo base_url('staff/update/' . $r['id']); ?>"
                                           class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                            <span class="kt-nav__link-text">Update</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <?php if ($r['user']['active'] == 1): ?>
                                            <a href="<?php echo base_url('staff/deactivate/' . $r['user']['id']); ?>"
                                               class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-delete"></i>
                                                <span class="kt-nav__link-text">Deactivate</span>
                                            </a>
                                        <?php endif; ?>
                                        <?php if ($r['user']['active'] == 0): ?>
                                            <a href="<?php echo base_url('staff/activate/' . $r['user']['id']); ?>"
                                               class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-check-mark"></i>
                                                <span class="kt-nav__link-text">Activate</span>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="javascript: void(0)" class="kt-nav__link remove_staff"
                                           data-id="<?php echo $r['id']; ?>">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Delete</span>
                                        </a>
                                    </li>
                                    <!-- <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-send"></i>
                                            <span class="kt-nav__link-text">Reset Password</span>
                                        </a>
                                    </li>-->
                                </ul>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="kt-portlet__body cards__custom">

                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-2">
                            <div class="kt-widget__head card__head">
                                <div class="kt-widget__media">
                                    <?php if (get_image('staff', 'images', @$r['image'])) : ?>

                                        <img class="kt-widget__img" src="<?php echo base_url(get_image('staff', 'images', @$r['image'])); ?>" width="90px" height="90px" />
                                    <?php else : ?>

                                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                            <?php echo get_initials($r['first_name']) . '' . get_initials($r['last_name']); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="card__info">
                                    <a href="<?php echo base_url('staff/view/' . $r['id']); ?>"
                                       class="kt-widget__username">
                                        <?php echo ucfirst($r['first_name']) . ' ' . ucfirst($r['last_name']); ?>
                                    </a><br>
                                    <span class="kt-widget__desc">
                                        <?php echo ucfirst($permission); ?>
                                    </span>
                            </div>

                            <div class="kt-widget__body">
                                <!-- <div class="kt-widget__section">
                                    I distinguish three <a href="#" class="kt-font-brand kt-link kt-font-transform-u kt-font-bold">#xrs-54pq</a> objectsves First
                                    merely firsr <b>USD249/Annual</b> your been to giant
                                    esetablished and nice coocked rice
                                </div> -->
                                <div class="kt-widget__item">
                                    <?php if (isset($r['user'])) { ?>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label">Username:</span>
                                            <span class="kt-widget__data"><?php echo $r['user']['username']; ?></span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label">Email:</span>
                                            <a href="#" class="kt-widget__data"><?php echo $r['user']['email']; ?></a>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label">Group:</span>
                                            <span class="kt-widget__data"><?= isset($r['user_group']) && $r['user_group'] ? ucfirst($r['user_group']->description) : 'None'; ?></span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label">Permission:</span>
                                            <span class="kt-widget__data"><?php echo ucfirst(@$permission); ?></span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__label">Status:</span>
                                            <span class="kt-widget__data"><?php echo($r['user']['active'] == 1 ? 'Active' : 'Inactive'); ?></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="kt-widget__footer staff_footer">
                                <a href="<?php echo base_url('staff/view/' . $r['id']); ?>"
                                   class="btn btn-label-primary btn-label-brand btn-lg btn-upper staff_button">View Profile</a>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                </div>
                <!--End::Portlet-->
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>