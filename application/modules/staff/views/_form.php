<?php
$id = isset($staff['id']) && $staff['id'] ? $staff['id'] : '';
$user_id = isset($staff['user_id']) && $staff['user_id'] ? $staff['user_id'] : '';
$first_name = isset($staff['first_name']) && $staff['first_name'] ? $staff['first_name'] : '';
$middle_name = isset($staff['middle_name']) && $staff['middle_name'] ? $staff['middle_name'] : '';
$last_name = isset($staff['last_name']) && $staff['last_name'] ? $staff['last_name'] : '';
$position = isset($staff['position']) && $staff['position'] ? $staff['position'] : '';
$image = isset($staff['image']) && $staff['image'] ? $staff['image'] : '';
$birthday = isset($staff['birthday']) && $staff['birthday'] ? $staff['birthday'] : '';
$contact = isset($staff['contact']) && $staff['contact'] ? $staff['contact'] : '';
$area = isset($staff['area']) && $staff['area'] ? $staff['area'] : '';
$city = isset($staff['city']) && $staff['city'] ? $staff['city'] : '';
$username = isset($staff['user']['username']) && $staff['user']['username'] ? $staff['user']['username'] : '';
$email = isset($staff['user']['email']) && $staff['user']['email'] ? $staff['user']['email'] : '';
$active = isset($staff['user']['active']) && $staff['user']['active'] ? $staff['user']['active'] : '';
$group_id = isset($staff['group_id']) && $staff['group_id'] ? $staff['group_id'] : '';
$group_name = isset($staff['user_group']->description) && $staff['user_group']->description ? $staff['user_group']->description : '';
$fullname = ucfirst($first_name) . ' ' . ucfirst($last_name);
$regCode = isset($staff['regCode']) && $staff['regCode'] ? $staff['regCode'] : '';
$provCode = isset($staff['provCode']) && $staff['provCode'] ? $staff['provCode'] : '';
$citymunCode = isset($staff['citymunCode']) && $staff['citymunCode'] ? $staff['citymunCode'] : '';
$brgyCode = isset($staff['brgyCode']) && $staff['brgyCode'] ? $staff['brgyCode'] : '';
$permission_id = isset($staff['user_permission']['permission_id']) && $staff['user_permission']['permission_id'] ? $staff['user_permission']['permission_id'] : '';
$permission_name = isset($permission['name']) && $permission['name'] ? $permission['name'] : '';
?>

<div class="kt-portlet__head kt-portlet__head--noborder">
    <div class="kt-portlet__head-label">
        <h5>Staff's Profile Details</h5>
    </div>
    <div class="kt-portlet__head-toolbar"></div>
</div>
<div class="kt-portlet__body">

    <div class="row">
        <div class="col-xl-12">
            <div class="kt-section__body">
                <!-- <div class="form-group row">
					<label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
					<div class="col-lg-9 col-xl-6">
						<div class="kt-avatar kt-avatar--outline kt-avatar--circle--" id="kt_apps_user_add_avatar">
							<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url(); ?>assets/media/users/default.jpg)"></div>
							<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
								<i class="fa fa-pen"></i>
								<input type="file" name="kt_apps_contacts_add_avatar">
							</label>
							<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
								<i class="fa fa-times"></i>
							</span>
						</div>
					</div>
				</div> -->
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text"
                               value="<?php echo set_value('first_name', $first_name); ?>" name="first_name" placeholder="First Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text"
                               value="<?php echo set_value('last_name', $last_name); ?>" name="last_name" placeholder="Last Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Date of Birth</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control datePicker" type="text"
                               value="<?php echo set_value('birthday', $birthday); ?>" name="birthday">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Position</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="<?php echo set_value('position', $position); ?>"
                               name="position" placeholder="Position">
                        <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                    <div class="col-lg-9 col-xl-9">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span>
                            </div>
                            <input type="text" class="form-control"
                                   value="<?php echo set_value('contact', $contact); ?>" name="contact" placeholder="Contact Phone"
                                   aria-describedby="basic-addon1">
                        </div>
                        <!--<span class="form-text text-muted">We'll never share your email with anyone else.</span>-->
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="<?php echo set_value('city', $city); ?>"
                               name="city" placeholder="City">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Area</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="<?php echo set_value('area', $area); ?>"
                               name="area" placeholder="Area">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Region</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control" id="region" name="regCode">
                            <option value="">Select Region</option>
                            <?php foreach ($regions as $value): ?>

                                <option value="<?php echo $value['regCode']; ?>"<?php if($regCode === $value['regCode']): ?> selected<?php endif; ?>><?php echo $value['regDesc']; ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Province</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control" id="province" name="provCode" disabled>
                            <option value="">Select Province</option>
                            <?php foreach ($provinces as $value): ?>
                                <option value="<?php echo $value['provCode']; ?>"<?php if($provCode === $value['provCode']): ?> selected<?php endif; ?>><?php echo $value['provDesc']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control" id="city" name="citymunCode" disabled>
                            <option value="">Select City</option>
                            <?php foreach ($cities as $value): ?>
                                <option value="<?php echo $value['citymunCode']; ?>"<?php if($citymunCode === $value['citymunCode']): ?> selected<?php endif; ?>><?php echo $value['citymunDesc']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Barangay</label>
                    <div class="col-lg-9 col-xl-9">
                        <select class="form-control" id="brgy" name="brgyCode" disabled>
                            <option value="">Select Barangay</option>
                            <?php if($brgyCode): ?>
                                <option value="<?php echo $brgyCode; ?>" selected><?php echo $staff['barangay']->brgyDesc; ?></option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
                <h5>Staff's Account Details</h5>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                    <div class="col-lg-9 col-xl-9">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text"><i
                                            class="la la-at"></i></span></div>
                            <input type="text" class="form-control" value="<?php echo set_value('email', $email); ?>"
                                   name="email" placeholder="Email Address" aria-describedby="basic-addon1">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Username</label>
                    <div class="col-lg-9 col-xl-9">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text"><i
                                            class="la la-user"></i></span></div>
                            <input type="text" class="form-control" placeholder="Username"
                                   value="<?php echo set_value('username', $username); ?>" name="username">

                        </div>
                    </div>
                </div>
                <div class="form-group row hide">
                    <label class="col-xl-3 col-lg-3 col-form-label">Group</label>
                    <div class="col-lg-9 col-xl-9">
                        <select name="group_id"  id="group_id" class="hide suggests form-control" data-module="groups">
                            <option value="">Select Option</option>
                            <option value="1" selected>Admin</option>
                        </select>
                        <!-- <input class="form-control" type="text" value=""> -->
                        <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Permission</label>
                    <div class="col-lg-9 col-xl-9">
                        <select name="permission_id"  id="permission_id" class="suggests form-control" data-module="permissions">
                            <option value="">Select Option</option>
                            <?php if($permission_id): ?>
                                <option value="<?=$permission_id?>" selected><?=$permission_name?></option>
                            <?php endif; ?>
                        </select>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Projects</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="" name="project" placeholder="Projects">
                        <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Properties</label>
                    <div class="col-lg-9 col-xl-9">
                        <input class="form-control" type="text" value="" name="property" placeholder="Properties">
                        <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                    </div>
                </div>
                <div class="form-group form-group-last row">
                    <div class="col-md-6 col-xl-6">
                        <div class="form-group">
                            <label>Profile Photo</label>
                            <div>
                                <?php if (get_image('staff', 'images', $image)): ?>
                                    <img class="kt-widget__img" src="<?php echo base_url(get_image('staff', 'images', $image)); ?>" width="90px" height="90px"/>
                                <?php endif; ?>
                                <span class="btn btn-sm">
                                    <input type="file" name="staff_image" class="" aria-invalid="false">
                                </span>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
