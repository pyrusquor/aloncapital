<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Staff_model extends MY_Model {
	public $table = 'staff'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = ['user_id', 'first_name', 'middle_name', 'last_name', 'birthday', 'position', 'image', 'contact', 'area', 'city', 'regCode', 'provCode', 'citymunCode', 'brgyCode', 'group_id', 'created_by',
	'created_at',
	'updated_by',
	'updated_at']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];
	
	public $fields = [
		'user_id' => array(
			'field'=>'user_id',
			'label'=>'User ID',
			'rules'=>'trim'
		),
		'first_name' => array(
			'field'=>'first_name',
			'label'=>'First Name',
			'rules'=>'trim|required'
		),
		'middle_name' => array(
			'field'=>'middle_name',
			'label'=>'Middle Name',
			'rules'=>'trim'
		),
		'last_name' => array(
			'field'=>'last_name',
			'label'=>'Last Name',
			'rules'=>'trim|required'
		),
		'birthday' => array(
			'field'=>'birthday',
			'label'=>'Birthday',
			'rules'=>'trim|required'
		),
		'position' => array(
			'field'=>'position',
			'label'=>'Position',
			'rules'=>'trim'
		),
		'image' => array(
			'field'=>'image',
			'label'=>'Image',
			'rules'=>'trim'
		),
		'contact' => array(
			'field'=>'contact',
			'label'=>'Contact',
			'rules'=>'trim'
		),
		'area' => array(
			'field'=>'area',
			'label'=>'Area',
			'rules'=>'trim'
		),
		'city' => array(
			'field'=>'city',
			'label'=>'City',
			'rules'=>'trim'
		),
		'regCode' => array(
			'field'=>'regCode',
			'label'=>'Region',
			'rules'=>'trim'
		),
		'provCode' => array(
			'field'=>'provCode',
			'label'=>'Province',
			'rules'=>'trim'
		),
		'citymunCode' => array(
			'field'=>'citymunCode',
			'label'=>'City',
			'rules'=>'trim'
		),
		'brgyCode' => array(
			'field'=>'brgyCode',
			'label'=>'Barangay',
			'rules'=>'trim'
		),
		'group_id' => array(
			'field'=>'group_id',
			'label'=>'Group ID',
			'rules'=>'trim|required'
		)
	];
	
	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
		
		$this->has_one['user'] = array('foreign_model'=>'User_model','foreign_table'=>'users','foreign_key'=>'id','local_key'=>'user_id');
		$this->has_one['user_group'] = array('foreign_model'=>'Group_model','foreign_table'=>'groups','foreign_key'=>'id','local_key'=>'group_id');
		$this->has_one['user_permission'] = array('foreign_model'=>'user_permission/User_permission_model','foreign_table'=>'user_permissions','foreign_key'=>'user_id','local_key'=>'user_id');
		$this->has_one['barangay'] = array('foreign_model'=>'location/Address_barangays_model','foreign_table'=>'address_barangays','foreign_key'=>'brgyCode','local_key'=>'brgyCode');
	}
}