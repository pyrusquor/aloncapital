<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();

		$this->load->model('staff/Staff_model', 'M_staff');
		$this->load->model('user/User_model', 'M_user');
		$this->load->model('auth/Ion_auth_model', 'M_ion_auth');
		$this->load->model('group/Group_model', 'M_group');
		$this->load->model('user_permission/User_permission_model', 'M_User_permission');
		$this->load->model('permission/Permission_model', 'M_Permission');
        $this->load->model('locations/Address_regions_model', 'M_region');
        $this->load->model('locations/Address_provinces_model', 'M_province');
        $this->load->model('locations/Address_city_municipalities_model', 'M_city');
        $this->load->model('locations/Address_barangays_model', 'M_barangay');
        $this->load->model('login_history/Login_history_model', 'M_login_history');
        $this->load->model('audit_trail/Audit_trail_model', 'M_Audit_trail');

		// Load pagination library 
		$this->load->library('ajax_pagination');

		// Format Helper
        $this->load->helper(['format', 'images']);

		// Per page limit 
		$this->perPage = 150;

		$this->_table_fillables		=	$this->M_staff->fillable;
		$this->_table_columns		=	$this->M_staff->__get_columns();
        $this->_table = 'staff';

        $this->view_data['regions'] = $this->M_region->as_array()->get_all();
        $this->view_data['provinces'] = $this->M_province->as_array()->get_all();
        $this->view_data['cities'] = $this->M_city->as_array()->get_all();
        $this->view_data['barangays'] = $this->M_barangay->as_array()->get_all();
	}
	 
	public function index()
	{
		$this->template->title('REMS', 'Staff');

		// overwrite default theme and layout if needed
		$this->template->set_theme('default');
		$this->template->set_layout('default');

		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns']		=	$this->__get_columns($_fills);

		// Get record count 
		$this->view_data['totalRec'] = $totalRec = $this->M_staff->count_rows();

		// Pagination configuration 
		$config['target']      = '#staffContent';
		$config['base_url']    = base_url('staff/paginationData');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'StaffPagination';

		// Initialize pagination library 
		$this->ajax_pagination->initialize($config);
		
		// get all raw data
		$alldata = $this->M_staff->with_user('fields:username,email,active')->with_user_group('fields:description')->with_user_permission('fields:permission_id')->as_array()->limit($this->perPage, 0)->order_by('id','DESC')->get_all();
		
		// print_r($alldata); exit;

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);
		
		$this->view_data['record'] = $alldata; 

		$this->template->build('index', $this->view_data);

	}

	public function paginationData()
	{
		if ($this->input->is_ajax_request()) {

			// Input from General Search
			$keyword = $this->input->post('keyword');
			
			// Input from Advanced Filter
			$name = $this->input->post('name');
			$userGroup = $this->input->post('userGroup');
			$position = $this->input->post('position');

			$page = $this->input->post('page');

			if (!$page) {
				$offset = 0;
			} else {
				$offset = $page;
			}

			$totalRec = $this->M_staff->count_rows();
			$where = array();

			// Pagination configuration 
			$config['target']      = '#staffContent';
			$config['base_url']    = base_url('staff/paginationData');
			$config['total_rows']  = $totalRec;
			$config['per_page']    = $this->perPage;
			$config['link_func']   = 'StaffPagination';

			// Query
			if ( ! empty($keyword) ):
				$this->db->group_start();
					$this->db->like('last_name', $keyword, 'both');
					$this->db->or_like('first_name', $keyword, 'both');
				$this->db->group_end();
			endif;

			if ( ! empty($name) ):
				$this->db->group_start();
				$this->db->like('last_name', $name, 'both');
				$this->db->or_like('first_name', $name, 'both');
				$this->db->group_end();
			endif;

			if ( ! empty($userGroup) ):
				$this->db->group_start();
				$this->db->like('group_id', $userGroup, 'both');
				$this->db->group_end();
			endif;

			if ( ! empty($position) ):
				$this->db->group_start();
				$this->db->like('position', $position, 'both');
				$this->db->group_end();
			endif;

			// if ( ! empty($occupation_type_id) && ! empty($occupation_type_id) ):
			// 	$this->db->where('occupation_type_id',$occupation_type_id);
			// endif;

			// if ( ! empty($occupation_location_id) && ! empty($occupation_location_id) ):
			// 	$this->db->where('location_id',$occupation_location_id);
			// endif;

			// if ( ! empty($civil_status_id) && ! empty($civil_status_id) ):
			// 	$this->db->where('civil_status_id',$civil_status_id);
			// endif;

			$totalRec = $this->M_staff->count_rows();
			
			// Pagination configuration 
			$config['total_rows']  = $totalRec;

			// Initialize pagination library 
			$this->ajax_pagination->initialize($config);

			// Query
			if ( ! empty($keyword) ):
				$this->db->group_start();
					$this->db->like('last_name', $keyword, 'both');
					$this->db->or_like('first_name', $keyword, 'both');
				$this->db->group_end();
			endif;

			if ( ! empty($name) ):
				$this->db->group_start();
				$this->db->like('last_name', $name, 'both');
				$this->db->or_like('first_name', $name, 'both');
				$this->db->group_end();
			endif;

			if ( ! empty($position) ):
				$this->db->group_start();
				$this->db->like('position', $position, 'both');
				$this->db->group_end();
			endif;

			if ( ! empty($userGroup) ):
				$this->db->group_start();
				$this->db->like('group_id', $userGroup, 'both');
				$this->db->group_end();
			endif;

			// if ( ! empty($buyer_type_id) && ! empty($buyer_type_id) ):
			// 	$this->db->where('type_id',$buyer_type_id);$where['type_id'] = $buyer_type_id;
			// endif;

			/*if ( ! empty($occupation_type_id) && ! empty($occupation_type_id) ):
				$this->db->where('occupation_type_id',$occupation_type_id); $where['occupation_type_id'] = $occupation_type_id;
			endif;

			if ( ! empty($occupation_location_id) && ! empty($occupation_location_id) ):
				$this->db->where('location_id',$occupation_location_id); $where['location_id'] = $location_id;
			endif;*/
			
			// if ( ! empty($civil_status_id) && ! empty($civil_status_id) ):
			// 	$this->db->where('civil_status_id',$civil_status_id); $where['civil_status_id'] = $civil_status_id;
			// endif;
			
			$this->view_data['record'] = $this->M_staff->fields(array('id', 'first_name', 'last_name',  'position', 'group_id'))->with_user('fields:username,email,active')->with_user_group('fields:description')->with_user_permission('fields:permission_id')
				->limit($this->perPage, $offset)
				->get_all();

			
			$this->load->view('staff/_filter', $this->view_data, false);
		}
	}

	
	public function view($id = FALSE,$type = "")
	{
		if ($id) {

			$this->view_data['staff'] = $data['staff'] = $this->M_staff->with_user('fields:username,email,active')->with_user_group('fields:description')->with_user_permission('fields:permission_id')->as_array()->get($id);

			$user_id = $data['staff']['user_id'];

			$this->view_data['login_history'] = $data['login_history'] = $this->M_login_history->where(array('user_id'=>$user_id))->limit(25, 0)->order_by('id','DESC')->get_all();
			$this->view_data['audit_trail'] = $data['audit_trail'] = $this->M_Audit_trail->where(array('created_by'=>$user_id))->limit(25, 0)->order_by('id','DESC')->get_all();

			if ($type) {
				vdebug($data);
			}

			if ($this->view_data['staff']) {

				$this->template->build('view', $this->view_data);

			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}
	
	public function create()
	{
		$this->css_loader->queue("vendors/general/select2/dist/css/select2.css");
		$this->js_loader->queue("vendors/general/select2/dist/js/select2.full.js");

		if ($post = $this->input->post()) 
		{
            // Image Upload
            $upload_path = './assets/uploads/staff/images';
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 5000;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config, 'staff_image');
            $this->staff_image->initialize($config);

            if (!empty($_FILES['staff_image']['name'])) {

                if (!$this->staff_image->do_upload('staff_image')) {
                    $this->notify->error($this->staff_image->display_errors(), 'staff/create');
                } else {
                    $img_data = $this->staff_image->data();
                }
            }

			
			$password = password_format($post['last_name'], $post['birthday']);
		
            $user_data = array(
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'username' => $this->input->post('username'),
                'password' => $password,
                'email' => $this->input->post('email'),
                'active' => 1,
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => '',
                'phone' => $this->input->post('contact'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d')
            );

            // $user_result = $this->M_user->insert($user_data);
            $user_result =  $this->ion_auth->register($post['email'], $password, $post['email'], $user_data, ['1']);
            
			if($user_result)
			{
				$user_permission = array(
					'user_id' => $user_result,
					'permission_id' => $this->input->post('permission_id'),
					'created_by' => $this->session->userdata('user_id'),
					'created_at' => date('Y-m-d')
				);

				$user_group_result = $this->M_ion_auth->add_to_group($this->input->post('group_id'), $user_result);
				$user_permission_result = $this->M_User_permission->insert($user_permission);
				if($user_group_result)
				{	
					$additional_fields = array(
					    'image' => $img_data['file_name'],
						'user_id' => $user_result,
						'created_by' => $this->session->userdata('user_id'),
						'created_at' => date('Y-m-d')
					);

					$staff_result = $this->M_staff->from_form(NULL, $additional_fields)->insert();

					if ($staff_result === FALSE) {

						// Validation
						$this->notify->error('Oops something went wrong.');
					} else {

						// Success
						$this->notify->success('Staff successfully created.', 'staff');
					}
				}
				else{
					$this->notify->error('Oops something went wrong.');
				}
			}
			else
			{
				$this->notify->error('Oops something went wrong.');
			}
		}

		$this->template->build('create', $this->view_data);
	}
	
	public function update($id = FALSE)
	{
		$this->css_loader->queue("vendors/general/select2/dist/css/select2.css");
		$this->js_loader->queue("vendors/general/select2/dist/js/select2.full.js");
		
		if ($id) {

			$this->view_data['staff'] = $data = $this->M_staff->with_user('fields:id,username,email,active')->with_user_group('fields:description')->with_user_permission('fields:permission_id,id')->with_barangay()->as_array()->get($id);

			// vdebug($this->view_data['staff']);

			if ($data) 
			{
				if(isset($this->view_data['staff']['user_permission'])) {
					$permission_id = $this->view_data['staff']['user_permission']['permission_id'];
					$user_permission_id = $this->view_data['staff']['user_permission']['id'];
					
					$this->view_data['permission'] = $this->M_Permission->get($permission_id);
				}

				if ($post = $this->input->post()) {
					// print_r($this->input->post()); exit;

					$user_group_result = $this->M_ion_auth->remove_from_group($data['group_id'], $data['user']['id']);
					$user_group_result = $this->M_ion_auth->add_to_group($this->input->post('group_id'), $data['user']['id']);
				
					if($user_group_result)
					{
                        // Image Upload
                        $upload_path = './assets/uploads/staff/images';
                        $config = array();
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = 5000;
                        $config['encrypt_name'] = TRUE;
                        $this->load->library('upload', $config, 'staff_image');
                        $this->staff_image->initialize($config);

                        if (!empty($_FILES['staff_image']['name'])) {
                            if (!$this->staff_image->do_upload('staff_image')) {
                                $this->notify->error($this->staff_image->display_errors(), 'staff/create');
                            } else {
                                $img_data = $this->staff_image->data();
                            }
                        }

						$additional_fields = array(
						    'image' => @$img_data['file_name'],
							'user_id' => $data['user']['id'],
							'updated_by' => $this->session->userdata('user_id'),
							'updated_at' => date('Y-m-d'),
							// 'password' => $this->M_ion_auth->hash_password(strtolower($this->input->post('first_name').$this->input->post('last_name'))),
							// 'password' => password_format($this->input->post('last_name'), $this->input->post('birth_date')),
						);

						if(isset($this->view_data['staff']['user_permission'])) {
							$user_permission = array(
								'user_id' => $data['user']['id'],
								'permission_id' => $this->input->post('permission_id'),
								'updated_by' => $this->session->userdata('user_id'),
								'updated_at' => date('Y-m-d')
							);
							$user_permission_result = $this->M_User_permission->update($user_permission, $user_permission_id);
						} else {
							$user_permission = array(
								'user_id' => $data['user']['id'],
								'permission_id' => $this->input->post('permission_id'),
								'created_by' => $this->session->userdata('user_id'),
								'created_at' => date('Y-m-d')
							);
	
							$user_permission_result = $this->M_User_permission->insert($user_permission);
						}
						
						$staff_result = $this->M_staff->from_form(NULL, $additional_fields)->update(NULL, $data['id']);


						$oldPwd = password_format($data['last_name'], $data['birthday']);
                        $newPwd = password_format($post['last_name'], $post['birthday']);
                       	$oldPwd = "password";

                        if ($oldPwd !== $newPwd) {
                            // Update User Password
                            $this->M_ion_auth->change_password($post['email'], $oldPwd, $newPwd);
                        }

						// $user_data = array(
							// 'password' => password_format($this->input->post('last_name'), $this->input->post('birth_date')),
						// );

						// $this->M_user->update($user_data, $data['user']['id']);

						if ($staff_result === FALSE) {

							// Validation
							$this->notify->error('Oops something went wrong.');
						} else {

							// Success
							$this->notify->success('Staff successfully updated.', 'staff');
						}
					}
					else{
						$this->notify->error('Oops something went wrong.');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function delete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_staff->get($id);
			if ($list) {

				$deleted = $this->M_staff->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Buyer successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function bulkDelete()
	{	
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if($this->input->is_ajax_request())
		{
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_staff->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Buyer successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}
	
	public function deactivate($id = FALSE)
	{
		if ($id) {
			$data = array(
				'active' => 0,
				'updated_by' => $this->session->userdata('user_id'),
				'updated_at' => date('Y-m-d')
			);
			
			$staff_result = $this->M_user->update($data, $id);

			if ($staff_result === FALSE) {

				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Staff successfully deactivated.', 'staff');
			}
		} else {

			show_404();
		}
	}
	
	public function activate($id = FALSE)
	{
		if ($id) {
			$data = array(
				'active' => 1,
				'updated_by' => $this->session->userdata('user_id'),
				'updated_at' => date('Y-m-d')
			);
			
			$staff_result = $this->M_user->update($data, $id);

			if ($staff_result === FALSE) {

				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Staff successfully activated.', 'staff');
			}
		} else {

			show_404();
		}
	}

	 function export() {

        $_db_columns	=	[];
        $_alphas			=	[];
        $_datas				=	[];

        $_titles[]	=	'#';

        $_start	=	3;
        $_row		=	2;
        $_no		=	1;

        $staffs	=	$this->M_staff->as_array()->get_all();
        if ( $staffs ) {

            foreach ( $staffs as $_lkey => $staff ) {

                $_datas[$staff['id']]['#']	=	$_no;

                $_no++;
            }

            $_filename	=	'list_of_staff_'.date('m_d_y_h-i-s',time()).'.xls';

            $_objSheet	=	$this->excel->getActiveSheet();

            if ( $this->input->post() ) {

                $_export_column	=	$this->input->post('_export_column');
                if ( $_export_column ) {

                    foreach ( $_export_column as $_ekey => $_column ) {

                        $_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'item_abc');
                }
            } else {

                $_filename	=	'list_of_staff_'.date('m_d_y_h-i-s',time()).'.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns	=	$this->_table_fillables;
                if ( !$_db_columns ) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'staff');
                }
            }

            if ( $_db_columns ) {

                foreach ( $_db_columns as $key => $_dbclm ) {

                    $_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ( (strpos( $_name, 'created_') === FALSE) && (strpos( $_name, 'updated_') === FALSE) && (strpos( $_name, 'deleted_') === FALSE) && ($_name !== 'id') ) {

                        if ( (strpos( $_name, '_id') !== FALSE) ) {

                            $_column	=	$_name;

                            $_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                        } elseif ( (strpos( $_name, 'is_') !== FALSE) ) {

                            $_column	=	$_name;

                            $_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ( $staffs as $_lkey => $staff ) {

                                $_datas[$staff['id']][$_title]	=	isset($staff[$_column]) && ($staff[$_column] !== '') ? Dropdown::get_static('bool', $staff[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ( $staffs as $_lkey => $staff ) {

                                if ( $_name === 'status' ) {

                                    $_datas[$staff['id']][$_title]	=	isset($staff[$_name]) && $staff[$_name] ? Dropdown::get_static('inventory_status', $staff[$_name], 'view') : '';

                                } else {

                                    $_datas[$staff['id']][$_title]	=	isset($staff[$_name]) && $staff[$_name] ? $staff[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas	=	$this->__get_excel_columns(count($_titles));

                $_xls_columns	=	array_combine($_alphas, $_titles);
                $_firstAlpha	=	reset($_alphas);
                $_lastAlpha		=	end($_alphas);

                foreach ( $_xls_columns as $_xkey => $_column ) {

                    $_title	=	($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey.$_row, $_title);
                }

                $_objSheet->setTitle('List of Staff');
                $_objSheet->setCellValue('A1', 'LIST OF STAFF');
                $_objSheet->mergeCells('A1:'.$_lastAlpha.'1');

                if ( isset($_datas) && $_datas ) {

                    foreach ( $_datas as $_dkey => $_data ) {

                        foreach ( $_alphas as $_akey => $_alpha ) {

                            $_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha.$_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha.$_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha.$_start.':'.$_lastAlpha.$_start);

                    $_style	=	array(
                        'font'  => array(
                            'bold'	=>	FALSE,
                            'size'	=>	9,
                            'name'	=>	'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha.$_start.':'.$_lastAlpha.$_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ( $_alphas as $_alpha ) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style	=	array(
                    'font'  => array(
                        'bold'	=>	TRUE,
                        'size'	=>	10,
                        'name'	=>	'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:'.$_lastAlpha.$_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="'.$_filename.'"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'item_abc');
            }
        } else {

            $this->notify->error('No Record Found', 'item_abc');
        }
    }

    function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Staff CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_staff->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('staff');
			$_objSheet->setCellValue('A1', 'STAFF');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

    function import () {

		if ( isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name'] ) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if ( TRUE ) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_failed_reasons = [];
				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch ( Exception $e ) {

					$_msg	=	'Error loading CSV "'.pathinfo($_name, PATHINFO_BASENAME).'": '.$e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ( $_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2] ) {

					if ( $_sheetData[1][0] === 'id' ) {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if ( !$_fillables ) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
					}

					foreach ( $_fillables as $_fkey => $_fill ) {

						if ( in_array($_fill, $_sheetData[1]) ) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ( $_sheetData as $_skey => $_sd ) {

						if ( $_skey > 1 ) {

							if ( count(array_filter($_sd)) !== 0 ) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if ( isset($_datas) && $_datas ) {

						foreach ( $_datas as $_dkey => $_data ) {
							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							$_data['birth_date']	=	isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
							$buyerGroupID = ['3'];

							if ( $_id ) {
								$data	=	$this->M_buyer->get($_id);
								if ( $data ) {

									unset($_data['id']);

									$oldPwd = password_format($data['last_name'], $data['birth_date']);
									$newPwd = password_format($_data['last_name'], $_data['birth_date']);

									$oldEmail = $data['email'];
									$newEmail = $_data['email'];

									if( $this->M_auth->email_check($oldEmail) ){

										if ($oldPwd !== $newPwd) {
											// Update User Password
											$this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
										}

										if ($oldEmail !== $newEmail) {
											// Update User Email
											$_user_data = [
												'email' => $newEmail,
												'username' => $newEmail,
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											];
											$this->M_user->update($_user_data, array('id' => $data['user_id']));
										}

										// Update Buyer Info
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_update	=	$this->M_buyer->update($_data, $_id);
										if ( $_update !== FALSE ) {

											$user_data = array(
												'first_name' => $_data['first_name'],
												'last_name' => $_data['last_name'],
												'active' => $_data['is_active'],
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											);
	
											$this->M_user->update($user_data, $data['user_id']);

											$_updated++;
										} else {

											$_failed_reasons[$_data['id']][] = 'update not working.';
											$_failed++;

											break;
										}

									} else {

										$_failed_reasons[$_data['id']][] = 'email already used';
										$_failed++;

										break;
									}
									
								} else {

									// Generate user password
									$buyerPwd = password_format($_data['last_name'], $_data['birth_date']);
									$buyerEmail = $_data['email'];
									
									if( !$this->M_auth->email_check($_data['email']) ){

										$user_data = array(
											'first_name' => $_data['first_name'],
											'last_name' => $_data['last_name'],
											'active' => $_data['is_active'],
											'created_by' => $this->user->id,
											'created_at' => NOW
										);
										
										$userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $user_data, $buyerGroupID);

										if ($userID !== FALSE) {

											$_data['user_id']	= $userID;
											$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['created_at']	=	NOW;
											$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['updated_at']	=	NOW;

											$_insert	=	$this->M_buyer->insert($_data);
											if ( $_insert !== FALSE ) {

												$_inserted++;
											} else {

												$_failed_reasons[$_data['id']][] = 'insert buyer not working';
												$_failed++;

												break;
											}

										} else {

											$_failed_reasons[$_data['id']][] = 'insert ion auth not working';
											$_failed++;

											break;
										}

									} else {

										$_failed_reasons[$_data['id']][] = 'email check already existing';
										$_failed++;

										break;
									}
								}
							} else {

								// Generate user password
								$buyerPwd = password_format($_data['last_name'], $_data['birth_date']);
								$buyerEmail = $_data['email'];
								
								if( !$this->M_auth->email_check($_data['email']) ){

									$user_data = array(
										'first_name' => $_data['first_name'],
										'last_name' => $_data['last_name'],
										'active' => $_data['is_active'],
										'created_by' => $this->user->id,
										'created_at' => NOW
									);
									
									$userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $user_data, $buyerGroupID);

									if ($userID !== FALSE) {

										$_data['user_id']	= $userID;
										$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['created_at']	=	NOW;
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_insert	=	$this->M_buyer->insert($_data);
										if ( $_insert !== FALSE ) {

											$_inserted++;
										} else {

											$_failed_reasons[$_dkey][] = 'insert buyer not working';

											$_failed++;

											break;
										}

									} else {

										$_failed_reasons[$_dkey][] = 'insert ion auth not working';

										$_failed++;

										break;
									}

								} else {

									$_failed_reasons[$_dkey][] = 'email check already existing';

									$_failed++;

									break;

								}
							}
						}

						$_msg	=	'';
						if ( $_inserted > 0 ) {

							$_msg	=	$_inserted.' record/s was successfuly inserted';
						}

						if ( $_updated > 0 ) {

							$_msg	.=	($_inserted ? ' and ' : '').$_updated.' record/s was successfuly updated';
						}

						if ( $_failed > 0 ) {
							$this->notify->error('Upload Failed! Please follow upload guide. ', 'buyer');
						} else {

							$this->notify->success($_msg.'.', 'buyer');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'buyer');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'buyer');
			}
		} else {

			$this->notify->error('Something went wrong!', 'buyer');
		}
	}

	public function getStaff() {

		$data = $this->M_staff->as_array()->get_all();
		$output = array(
			'data' => $data,
		);
		echo json_encode($output);
	}
	
}