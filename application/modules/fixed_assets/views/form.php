<!-- CONTENT HEADER -->
<?php
$info ? extract($info):'';
?>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><?= $operation ?> Fixed Asset</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?= base_url('fixed_assets'); ?>" class="btn btn-label-instagram"><i class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<input type='hidden' value='<?= $id ?>' id='id'>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                <form class="kt-form" method="POST" action="" id="form_fixed_assets" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Name <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon">
                                    <input type="text" class="form-control" placeholder="Enter name..." name="name" value="<?= $name ?>" required>
                                    <span class="kt-input-icon__icon"></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Code <span class="kt-font-danger">*</span></label>
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Enter code..." name="code" value="<?= $code ?>" required>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>ABC</label>
                                <div class="kt-input-icon">
                                    <select class="form-control suggests-show-options" data-module="item_abc" id="item_abc_id" name="abc_id">
                                        <option value="">Select ABC Type</option>
                                        <?php if ($abc) : ?>
                                            <option value="<?= $abc['id']?>" selected><?= $abc['name']; ?></option>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Brand </label>
                                <select class="form-control suggests-show-options" data-module="item_brand" id="item_brand_id" name="brand_id">
                                    <option value="">Select Brand Type</option>
                                    <?php if ($brand) : ?>
                                        <option value="<?= $brand['id']; ?>" selected><?= $brand['name']; ?></option>
                                    <?php endif ?>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Class</label>
                                <select class="form-control suggests-show-options" data-module="item_class" id="item_class_id" name="class_id">
                                    <option value="">Select Class Type</option>
                                    <?php if ($class) : ?>
                                        <option value="<?= $class['id']; ?>" selected><?= $class['name']; ?></option>
                                    <?php endif ?>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Group</label>
                                <select class="form-control suggests-show-options" data-module="item_group" id="item_group_id" name="group_id">
                                    <option value="">Select Group Type</option>
                                    <?php if ($group) : ?>
                                        <option value="<?= $group['id']; ?>" selected><?= $group['name']; ?></option>
                                    <?php endif ?>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Type</label>
                                <select class="form-control suggests-show-options" data-module="item_type" id="item_type_id" name="type_id" >
                                    <option value="">Select Type</option>
                                    <?php if ($type) : ?>
                                        <option value="<?= $type['id']; ?>" selected><?= $type['name']; ?></option>
                                    <?php endif ?>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Description</label>
                                <div class="kt-input-icon">
                                    <input type="text" class="form-control" placeholder="Enter description..." name="description" value="<?= $description ?>">
                                    <span class="kt-input-icon__icon"></span>
                                </div>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class='col-lg-12 kt-align-right'>
                            <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
                                Submit
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>