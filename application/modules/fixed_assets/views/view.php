<!-- CONTENT HEADER -->
<?php
$info ? extract($info) : '';
?>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Fixed Asset</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?= base_url('fixed_assets/form/' . $id); ?>" class="btn btn-label-instagram"><i class="fa fa-edit"></i>
                    Edit</a>&nbsp;
                <a href="javascript:void(0);" class="btn btn-label-instagram remove_asset" data-id="<?= $id ?>"><i class="fa fa-trash-alt"></i>
                    Delete</a>&nbsp;
                <a href="<?= base_url('fixed_assets'); ?>" class="btn btn-label-instagram"><i class="fa fa-reply"></i>
                    Back</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h1 class="kt-portlet__head-title display-3">
                    <?= $name ?> <small><?= $code ?></small>
                </h1>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                <div class="row">
                    <div class="col-sm-4">
                        <div href="#" class="kt-notification-v2__item">
                            <div class="kt-notification-v2__itek-wrapper">
                                <div class="kt-notification-v2__item-title">
                                    <h6 class="kt-portlet__head-title kt-font-primary">ABC Classification</h6>
                                </div>
                                <div class="kt-notification-v2__item-desc">
                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                        <?= $abc ? $abc['name'] : 'N/A' ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div href="#" class="kt-notification-v2__item">
                            <div class="kt-notification-v2__itek-wrapper">
                                <div class="kt-notification-v2__item-title">
                                    <h6 class="kt-portlet__head-title kt-font-primary">Brand</h6>
                                </div>
                                <div class="kt-notification-v2__item-desc">
                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                        <?= $brand ? $brand['name'] : 'N/A' ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div href="#" class="kt-notification-v2__item">
                            <div class="kt-notification-v2__itek-wrapper">
                                <div class="kt-notification-v2__item-title">
                                    <h6 class="kt-portlet__head-title kt-font-primary">Type</h6>
                                </div>
                                <div class="kt-notification-v2__item-desc">
                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                        <?= $type ? $type['name'] : 'N/A' ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div href="#" class="kt-notification-v2__item">
                            <div class="kt-notification-v2__itek-wrapper">
                                <div class="kt-notification-v2__item-title">
                                    <h6 class="kt-portlet__head-title kt-font-primary">Class</h6>
                                </div>
                                <div class="kt-notification-v2__item-desc">
                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                        <?= $class ? $class['name'] : 'N/A' ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div href="#" class="kt-notification-v2__item">
                            <div class="kt-notification-v2__itek-wrapper">
                                <div class="kt-notification-v2__item-title">
                                    <h6 class="kt-portlet__head-title kt-font-primary">Group</h6>
                                </div>
                                <div class="kt-notification-v2__item-desc">
                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                        <?= $group ? $group['name'] : 'N/A' ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>