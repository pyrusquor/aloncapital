<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Fixed_assets_model extends MY_Model
{

    public $table = 'fixed_assets'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'id',
        'name',
        'code',
        'abc_id',
        'group_id',
        'type_id',
        'class_id',
        'brand_id',
        'description',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['abc'] = array('foreign_model' => 'item_abc/Item_abc_model', 'foreign_table' => 'item_abc', 'foreign_key' => 'id', 'local_key' => 'abc_id');
        $this->has_one['brand'] = array('foreign_model' => 'item_brand/item_brand_model', 'foreign_table' => 'item_brand', 'foreign_key' => 'id', 'local_key' => 'brand_id');
        $this->has_one['class'] = array('foreign_model' => 'item_class/item_class_model', 'foreign_table' => 'item_class', 'foreign_key' => 'id', 'local_key' => 'class_id');
        $this->has_one['group'] = array('foreign_model' => 'item_group/item_group_model', 'foreign_table' => 'item_group', 'foreign_key' => 'id', 'local_key' => 'group_id');
        $this->has_one['type'] = array('foreign_model' => 'item_type/item_type_model', 'foreign_table' => 'item_type', 'foreign_key' => 'id', 'local_key' => 'type_id');
    }
}
