<?php

$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$reference = isset($info['reference']) && $info['reference'] ? $info['reference'] : '';
$amount = isset($info['amount']) && $info['amount'] ? $info['amount'] : '';
$payee_type_id = isset($info['received_by_payee_type_id']) && $info['received_by_payee_type_id'] ? $info['received_by_payee_type_id'] : '';
$payee_id = isset($info['received_by_payee_id']) && $info['received_by_payee_id'] ? $info['received_by_payee_id'] : '';
$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';
$disbursed_at = isset($info['disbursed_at']) && $info['disbursed_at'] ? date("Y-m-d", strtotime($info['disbursed_at'])) : '';

if ($payee_type_id == 1 || $payee_type_id == 4) {

    $payee_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'name');
    $payee_data_type = 'payee';
} else {

    $payee_first_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'first_name');
    $payee_last_name = get_value_field($payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $payee_type_id)), 'last_name');
    $payee_name = $payee_last_name . ' ' . $payee_first_name;
}

$payee_name = isset($payee_name) && $payee_name ? $payee_name : '';
$payee_type = isset($payee_type_id) && $payee_type_id ? Dropdown::get_static('accounting_entries_payee_type', $payee_type_id) : '';
$disbursed_by_first_name = isset($info['disbursed_by_id']) && $info['disbursed_by_id'] ? get_value_field($info['disbursed_by_id'], 'staff', 'first_name') : '';
$disbursed_by_last_name = isset($info['disbursed_by_id']) && $info['disbursed_by_id'] ? get_value_field($info['disbursed_by_id'], 'staff', 'last_name') : '';
$disbursed_by = $disbursed_by_first_name . ' ' . $disbursed_by_last_name;
$accounting_entry_id = isset($info['accounting_entry_id']) && $info['accounting_entry_id'] ? $info['accounting_entry_id'] : '';
$petty_cash_replenishment = isset($info['petty_cash_replenishment_id']) && $info['petty_cash_replenishment_id'] ? get_value_field($info['petty_cash_replenishment_id'], 'petty_cash_replenishments', 'reference') : '';

?>

<script type="text/javascript">
    window.petty_cash_replenishment_id = "<?= $id ?>";
    window.accounting_entry_id = "<?= $accounting_entry_id ?>";
</script>

<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Reference
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $reference ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Received By Type
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $payee_type ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Disbursed At
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $disbursed_at ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Amount
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $amount ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Petty Cash Replenishment
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $petty_cash_replenishment ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Received By
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $payee_name ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Disbursed By
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $disbursed_by ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Remarks
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $remarks ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>