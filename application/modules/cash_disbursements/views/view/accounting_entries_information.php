<?php

$accounting_entry = isset($info['accounting_entry']) && $info['accounting_entry'] ? $info['accounting_entry'] : [];
$invoice_number = isset($accounting_entry['invoice_number']) && $accounting_entry['invoice_number'] ? $accounting_entry['invoice_number'] : '';
$payee_type = isset($accounting_entry['payee_type']) && $accounting_entry['payee_type'] ? $accounting_entry['payee_type'] : '';
$payee_id = isset($accounting_entry['payee_type_id']) && $accounting_entry['payee_type_id'] ? $accounting_entry['payee_type_id'] : '';
$cr_total = isset($accounting_entry['cr_total']) && $accounting_entry['cr_total'] ? $accounting_entry['cr_total'] : '';
$dr_total = isset($accounting_entry['dr_total']) && $accounting_entry['dr_total'] ? $accounting_entry['dr_total'] : '';
$company_name = isset($accounting_entry['company_id']) && $accounting_entry['company_id'] ? get_value_field($accounting_entry['company_id'], 'companies', 'name') : '';
$remarks = isset($accounting_entry['remarks']) && $accounting_entry['remarks'] ? $accounting_entry['remarks'] : '';

if ($payee_type == 'suppliers' || $payee_type == 'contractors') {

    $payee_name = get_value_field($payee_id, strtolower($payee_type), 'name');
} else {

    $payee_first_name = get_value_field($payee_id, strtolower($payee_type), 'first_name');
    $payee_last_name = get_value_field($payee_id, strtolower($payee_type), 'last_name');
    $payee_name = $payee_last_name . ' ' . $payee_first_name;
}

$payee_name = isset($payee_name) && $payee_name ? $payee_name : '';

?>

<div class="kt-form__body">
    <div class="kt-section">
        <div class="kt-section__body mb-3">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Invoice Number
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $invoice_number ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Payee Type
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $payee_type ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            CR Total
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $cr_total ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Remarks
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $remarks ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Company
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $company_name ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            Payee
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $payee_name ?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-5 col-form-label kt-font-primary kt-font-bolder">
                            DR Total
                        </label>
                        <div class="col-7">
                            <span class="form-control-plaintext kt-font-dark">
                                <?= $dr_total ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-section__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="accounting_entry_items_table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Type</th>
                        <th>Ledger</th>
                        <th>Amount</th>
                        <th>Description</th>
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>