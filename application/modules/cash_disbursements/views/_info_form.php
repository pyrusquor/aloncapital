<?php

$amount = isset($info['amount']) && $info['amount'] ? $info['amount'] : '';
$received_by_payee_type_id = isset($info['received_by_payee_type_id']) && $info['received_by_payee_type_id'] ? $info['received_by_payee_type_id'] : '5';
$received_by_payee_id = isset($info['received_by_payee_id']) && $info['received_by_payee_id'] ? $info['received_by_payee_id'] : '';
$disbursed_at = isset($info['disbursed_at']) && $info['disbursed_at'] ? date("Y-m-d", strtotime($info['disbursed_at'])) : '';
$disbursed_by_id = isset($info['disbursed_by_id']) && $info['disbursed_by_id'] ? $info['disbursed_by_id'] : '';
$petty_cash_replenishment_id = isset($info['petty_cash_replenishment_id']) && $info['petty_cash_replenishment_id'] ? $info['petty_cash_replenishment_id'] : '';
$received_by_payee_type = isset($received_by_payee_type_id) && $received_by_payee_type_id ? strtolower(Dropdown::get_static('accounting_entries_payee_type', $received_by_payee_type_id)) : '';
$remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';

if ($received_by_payee_type_id == 1 || $received_by_payee_type_id == 4) {

    $payee_name = get_value_field($received_by_payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $received_by_payee_type_id)), 'name');
    $received_by_payee_data_type = 'payee';
} else {

    $payee_first_name = get_value_field($received_by_payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $received_by_payee_type_id)), 'first_name');
    $payee_last_name = get_value_field($received_by_payee_id, strtolower(Dropdown::get_static('accounting_entries_payee_type', $received_by_payee_type_id)), 'last_name');
    $payee_name = $payee_last_name . ' ' . $payee_first_name;
    $received_by_payee_data_type = 'person';
}

$payee_name = isset($payee_name) && $payee_name ? $payee_name : '';
$disbursed_by_first_name = get_value_field($disbursed_by_id, 'staff', 'first_name');
$disbursed_by_last_name = get_value_field($disbursed_by_id, 'staff', 'last_name');
$disbursed_by_name = $disbursed_by_first_name . ' ' . $disbursed_by_last_name;

?>

<script type="text/javascript">
    window.payee_types = <?= json_encode(Dropdown::get_static('accounting_entries_payee_type')) ?>;
</script>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Amount</label>
            <div class="kt-input-icon">
                <input type="number" class="form-control" name="amount" value="<?= set_value('amount', $amount); ?>" placeholder="Amount" autocomplete="off" id="amount">
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Received By Type</label>
            <?= form_dropdown('received_by_payee_type_id', Dropdown::get_static('accounting_entries_payee_type'), set_value('$received_by_payee_type_id', @$received_by_payee_type_id), 'class="form-control" id="received_by_payee_type_id"'); ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Received By</label>
            <div class="row">
                <div class="col-sm-12">
                    <select class="form-control suggests" data-type="<?= $received_by_payee_data_type ?>" data-module="<?= $received_by_payee_type ? $received_by_payee_type : "buyers" ?>" id="received_by_payee_id" name="received_by_payee_id">
                        <option value="">Select Payee</option>
                        <?php if ($payee_name) : ?>
                            <option value="<?= $received_by_payee_id; ?>" selected><?= $payee_name; ?></option>
                        <?php endif ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Disbursed At</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" name="disbursed_at" value="<?= set_value('disbursed_at', @$disbursed_at); ?>" placeholder="Disbursed At" autocomplete="off" readonly>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Disbursed By</label>
            <div class="row">
                <div class="col-sm-12">
                    <select class="form-control suggests" data-type="person" data-module="staff" id="disbursed_by_id" name="disbursed_by_id">
                        <option value="">Select</option>
                        <?php if ($disbursed_by_name) : ?>
                            <option value="<?= $disbursed_by_id; ?>" selected><?= $disbursed_by_name; ?></option>
                        <?php endif ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Remarks</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="remarks" value="<?= set_value('remarks', $remarks); ?>" placeholder="Remarks" autocomplete="off">
            </div>
        </div>
    </div>
</div>