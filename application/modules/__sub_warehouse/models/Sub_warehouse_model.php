<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sub_warehouse_model extends MY_Model
{
    public $table = 'sub_warehouse'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'sub_warehouse_name',
        'sub_warehouse_code',
        'warehouse_id',
        'warehouse_name',
        'address',
        'telephone_number',
        'fax_number',
        'mobile_number',
        'email_address',
        'contact_person',
        'is_active',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'sub_warehouse_name' => array(
            'field' => 'sub_warehouse_name',
            'label' => 'Sub-warehouse Name',
            'rules' => 'trim|required',
        ),
        'sub_warehouse_code' => array(
            'field' => 'sub_warehouse_code',
            'label' => 'Sub-warehouse Code',
            'rules' => 'trim|required',
        ),
        'warehouse_id' => array(
            'field' => 'warehouse_id',
            'label' => 'Warehouse',
            'rules' => 'trim|required',
        ),
        'address' => array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'trim|required',
        ),
        'telephone_number' => array(
            'field' => 'telephone_number',
            'label' => 'Telephone Number',
            'rules' => 'trim|required',
        ),
        'fax_number' => array(
            'field' => 'fax_number',
            'label' => 'Fax Number',
            'rules' => 'trim|required',
        ),
        'mobile_number' => array(
            'field' => 'mobile_number',
            'label' => 'Mobile Number',
            'rules' => 'trim|required',
        ),
        'email_address' => array(
            'field' => 'email_address',
            'label' => 'Email Address',
            'rules' => 'trim|required',
        ),
        'contact_person' => array(
            'field' => 'contact_person',
            'label' => 'Contact Person',
            'rules' => 'trim|required',
        ),
        'is_active' => array(
            'field' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim|required',
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['warehouse'] = array('foreign_model' => 'Warehouse_model', 'foreign_table' => 'warehouse', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'sub_warehouse_name' => $faker->word,
                'sub_warehouse_code' => $faker->word,
                'warehouse_id' => $faker->numberBetween(1, 10),
                'address' => $faker->word,
                'telephone_number' => $faker->numberBetween(10000000, 99999999),
                'fax_number' => $faker->numberBetween(10000000, 99999999),
                'mobile_number' => $faker->numberBetween(10000000, 99999999),
                'email_address' => $faker->word,
                'contact_person' => $faker->word,
                'is_active' => $faker->numberBetween(0, 1),
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}
