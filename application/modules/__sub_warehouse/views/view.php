<?php

$id = isset($sub_warehouse['id']) && $sub_warehouse['id'] ? $sub_warehouse['id'] : '';
$sub_warehouse_name = isset($sub_warehouse['name']) && $sub_warehouse['name'] ? $sub_warehouse['name'] : '';
$sub_warehouse_code = isset($sub_warehouse['sub_warehouse_code']) && $sub_warehouse['sub_warehouse_code'] ? $sub_warehouse['sub_warehouse_code'] : '';
$warehouse_id = isset($sub_warehouse['warehouse']) && $sub_warehouse['warehouse'] ? $sub_warehouse['warehouse'] : '';
$address = isset($sub_warehouse['address']) && $sub_warehouse['address'] ? $sub_warehouse['address'] : '';
$telephone_number = isset($sub_warehouse['telephone_number']) && $sub_warehouse['telephone_number'] ? $sub_warehouse['telephone_number'] : '';
$fax_number = isset($sub_warehouse['fax_number']) && $sub_warehouse['fax_number'] ? $sub_warehouse['fax_number'] : '';
$mobile_number = isset($sub_warehouse['mobile_number']) && $sub_warehouse['mobile_number'] ? $sub_warehouse['mobile_number'] : '';
$email_address = isset($sub_warehouse['email_address']) && $sub_warehouse['email_address'] ? $sub_warehouse['email_address'] : '';
$contact_person = isset($sub_warehouse['contact_person']) && $sub_warehouse['contact_person'] ? $sub_warehouse['contact_person'] : '';
$is_active = isset($sub_warehouse['is_active']) && $sub_warehouse['is_active'] ? $sub_warehouse['is_active'] : '';

// vdebug($sub_warehouse);
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Sub Warehouse</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('sub_warehouse/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('sub_warehouse'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Sub-warehouse Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $sub_warehouse_name; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Sub-warehouse Code
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $sub_warehouse_code; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Warehouse
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $warehouse_id; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Address
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $address; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Telephone Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $telephone_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Fax Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $fax_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Mobile Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $mobile_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Email Address
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $email_address; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Contact Person
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $contact_person; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Is Active
                                    </span>
                                    <span class="kt-widget__data" style="font-weight: 500"><?php echo Dropdown::get_static('warehouse_status', $is_active, 'view'); ?></span>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->

                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
