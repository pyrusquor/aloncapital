<?php
$id = isset($sub_warehouse['id']) && $sub_warehouse['id'] ? $sub_warehouse['id'] : '';
$sub_warehouse_name = isset($sub_warehouse['name']) && $sub_warehouse['name'] ? $sub_warehouse['name'] : '';
$sub_warehouse_code = isset($sub_warehouse['sub_warehouse_code']) && $sub_warehouse['sub_warehouse_code'] ? $sub_warehouse['sub_warehouse_code'] : '';
$warehouse_id = isset($sub_warehouse['warehouse']) && $sub_warehouse['warehouse'] ? $sub_warehouse['warehouse'] : '';
$address = isset($sub_warehouse['address']) && $sub_warehouse['address'] ? $sub_warehouse['address'] : '';
$telephone_number = isset($sub_warehouse['telephone_number']) && $sub_warehouse['telephone_number'] ? $sub_warehouse['telephone_number'] : '';
$fax_number = isset($sub_warehouse['fax_number']) && $sub_warehouse['fax_number'] ? $sub_warehouse['fax_number'] : '';
$mobile_number = isset($sub_warehouse['mobile_number']) && $sub_warehouse['mobile_number'] ? $sub_warehouse['mobile_number'] : '';
$email_address = isset($sub_warehouse['email_address']) && $sub_warehouse['email_address'] ? $sub_warehouse['email_address'] : '';
$contact_person = isset($sub_warehouse['contact_person']) && $sub_warehouse['contact_person'] ? $sub_warehouse['contact_person'] : '';
$is_active = isset($sub_warehouse['is_active']) && $sub_warehouse['is_active'] ? $sub_warehouse['is_active'] : '';
$warehouse_name = isset($sub_warehouse['warehouse']) && $sub_warehouse['warehouse'] ? $sub_warehouse['warehouse'] : 'N/A';

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="sub_warehouse_name" value="<?php echo set_value('sub_warehouse_name', $sub_warehouse_name); ?>" placeholder="Sub-warehouse Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building"></i></span>
            </div>
            <?php echo form_error('sub_warehouse_name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Sub-warehouse Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="sub_warehouse_code" value="<?php echo set_value('sub_warehouse_code', $sub_warehouse_code); ?>" placeholder="Sub-warehouse Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-barcode"></i></span>
            </div>
            <?php echo form_error('sub_warehouse_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Select Warehouse<span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="warehouse"  id="warehouse_id" name="warehouse_id">
                <option value="0">Select Group</option>
                <?php if ($warehouse_name): ?>
                    <option value="<?php echo $warehouse_id; ?>" selected><?php echo $warehouse_name; ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Address <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="address" value="<?php echo set_value('address', $address); ?>" placeholder="Address" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-pin"></i></span>
            </div>
            <?php echo form_error('address'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Telephone Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="telephone_number" value="<?php echo set_value('telephone_number', $telephone_number); ?>" placeholder="Telephone Number" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span>
            </div>
            <?php echo form_error('telephone_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Fax Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="fax_number" value="<?php echo set_value('fax_number', $fax_number); ?>" placeholder="Fax Number" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-fax"></i></span>
            </div>
            <?php echo form_error('fax_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Mobile Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="mobile_number" value="<?php echo set_value('mobile_number', $mobile_number); ?>" placeholder="Mobile Number" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span>
            </div>
            <?php echo form_error('mobile_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Email Address <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="email_address" value="<?php echo set_value('email_address', $email_address); ?>" placeholder="Email Address" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-envelope"></i></span>
            </div>
            <?php echo form_error('email_address'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Contact Person <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="contact_person" value="<?php echo set_value('contact_person', $contact_person); ?>" placeholder="Contact Person" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('contact_person'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Status <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
            <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), set_value('is_active', @$is_active), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->

    <!-- ==================== end: Add form model fields ==================== -->
</div>