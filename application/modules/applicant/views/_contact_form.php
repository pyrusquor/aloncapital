<?php
// Contact Info
$present_address = isset($info['present_address']) && $info['present_address'] ? $info['present_address'] : '';
$mailing_address = isset($info['mailing_address']) && $info['mailing_address'] ? $info['mailing_address'] : '';
$business_address = isset($info['business_address']) && $info['business_address'] ? $info['business_address'] : '';
$other_mobile = isset($info['other_mobile']) && $info['other_mobile'] ? $info['other_mobile'] : '';

$mobile_no = isset($info['mobile_no']) && $info['mobile_no'] ? $info['mobile_no'] : '';
$email = isset($info['email']) && $info['email'] ? $info['email'] : '';
$landline = isset($info['landline']) && $info['landline'] ? $info['landline'] : '';

$social_media = isset($info['social_media']) && $info['social_media'] ? $info['social_media'] : '';

$country_id = $info['country_id'] ?? '';
$region_id = $info['regCode'] ?? '';
$province_id = $info['provCode'] ?? '';
$citymun_id = $info['citymunCode'] ?? '';
$brgy_id = $info['brgyCode'] ?? '';
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Mobile <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Mobile" name="info[mobile_no]" value="<?php echo set_value('info[mobile_no]"', @$mobile_no); ?>">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-mobile-phone"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Email <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Email" id="applicant_email" name="info[email]" value="<?php echo set_value('info[email]"', @$email); ?>" autocomplete="off">
                <input type="hidden" value="<?php echo $email; ?>" id="applicant_email_key" name="key">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-envelope-o"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Landline</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Landline" name="info[landline]" value="<?php echo set_value('info[landline]"', @$landline); ?>">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Other Mobile</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Other Mobile" name="info[other_mobile]" value="<?php echo set_value('info[other_mobile]"', @$other_mobile); ?>">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-mobile-phone"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Present Address: <span class="kt-font-danger">*</span></label>
            <textarea class="form-control" id="present_address" rows="3" spellcheck="false" name="info[present_address]"><?php echo set_value('info[present_address]"', @$present_address); ?></textarea>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Mailing Address:</label>
            <textarea class="form-control" id="mailing_address" rows="3" spellcheck="false" name="info[mailing_address]"><?php echo set_value('info[mailing_address]"', @$mailing_address); ?></textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Business Address:</label>
            <textarea class="form-control" id="business_address" rows="3" spellcheck="false" name="info[business_address]"><?php echo set_value('info[business_address]"', @$business_address); ?></textarea>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Social Media Address (URL):</label>
            <textarea class="form-control" id="social_media" rows="3" spellcheck="false" name="info[social_media]"><?php echo set_value('info[social_media]"', @$social_media); ?></textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Country:</label>
            <select class="form-control" id="country" name="info[country_id]">
                <option value="">Select Country</option>
                <?php foreach ($countries as $value) : ?>
                    <option value="<?php echo $value['id']; ?>" <?= $value['id'] == $country_id ? 'selected' : '' ?>><?php echo $value['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Region:</label>
            <select class="form-control" id="region" name="info[regCode]" disabled>
                <option value="">Select Region</option>
                <?php foreach ($regions as $value) : ?>
                    <option value="<?php echo $value['regCode']; ?>" <?= $value['regCode'] == $region_id ? 'selected' : '' ?>><?php echo $value['regDesc']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Province:</label>
            <select class="form-control" id="province" name="info[provCode]" disabled>
                <option value="">Select Province</option>
                <?php foreach ($provinces as $value) : ?>
                    <option value="<?php echo $value['provCode']; ?>" <?= $value['provCode'] == $province_id ? 'selected' : '' ?>><?php echo $value['provDesc']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>City:</label>
            <select class="form-control" id="city" name="info[citymunCode]" disabled>
                <option value="">Select City</option>
                <?php foreach ($cities as $value) : ?>
                    <option value="<?php echo $value['citymunCode']; ?>" <?= $value['citymunCode'] == $citymun_id ? 'selected' : '' ?>><?php echo $value['citymunDesc']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Barangay:</label>
            <select class="form-control" id="brgy" name="info[brgyCode]" disabled>
                <option value="">Select Barangay</option>
                <?php foreach ($barangays as $value) : ?>
                    <option value="<?php echo $value['brgyCode']; ?>" <?= $value['brgyCode'] == $brgy_id ? 'selected' : '' ?>><?php echo $value['brgyDesc']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Sitio</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Sitio" name="info[sitio]" value="<?php echo set_value('info[sitio]', @$info['sitio']); ?>">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-pin"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>