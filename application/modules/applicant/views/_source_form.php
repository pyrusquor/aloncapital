<?php
    // Source Info
    $reference = isset($info['identifications']) && $info['identifications'] ? $info['identifications'] : '';
?>
<div class="row">
    <div class="col-lg-12">
        <!--begin::Accordion-->
        <div class="accordion  accordion-toggle-arrow" id="accordionExample4">
            <?php if( ! empty($reference) ): ?>
                <?php $count = 1; foreach ($reference as $rkey => $r): ?>
                    <div class="card">
                        <div class="card-header" id="heading-<?php echo $count; ?>">
                            <div class="card-title" data-toggle="collapse" data-target="#collapse-<?php echo $count; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $count; ?>">
                                <i class="flaticon2-file-2"></i> Identification <?php echo $count++; ?>
                            </div>
                        </div>
                        <div id="collapse-<?php echo $count; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample4">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="">Type of ID</label>
                                    <?php echo form_dropdown('type_of_id[]', Dropdown::get_static('ids'), set_value('type_of_id[]', @$r['type_of_id']), 'class="form-control" id="type_of_id"'); ?>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">ID Number</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="ID Number" name="id_number[]" value="<?php echo $r['id_number']; ?>">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon2-information"></i></span>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Date Issued</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control datePicker" placeholder="Date Issued" name="date_issued[]" value="<?php echo db_date($r['date_issued']); ?>" readonly>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon-calendar"></i></span>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Date Expiration</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control datePicker" placeholder="Date Expiration" name="date_expiration[]" value="<?php echo db_date($r['date_expiration']); ?>" readonly>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon-calendar"></i></span>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Place Issued</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="Place Issued" name="place_issued[]" value="<?php echo $r['place_issued']; ?>">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-o"></i></span>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">File Upload</label>
                                    <input type="file" name="file_id[]" class="" aria-invalid="false">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <?php for ($i=1; $i <= 3; $i++): ?>
                    <div class="card">
                        <div class="card-header" id="heading-<?php echo $i; ?>">
                            <div class="card-title <?php echo ($i == '1') ? '' : 'collapsed'; ?>" data-toggle="collapse" data-target="#collapse-<?php echo $i; ?>" aria-expanded="<?php echo ($i == '1') ? 'true' : 'false'; ?>" aria-controls="collapse-<?php echo $i; ?>">
                                <i class="flaticon2-file-2"></i> Identification # <?php echo $i; ?>
                            </div>
                        </div>
                        <div id="collapse-<?php echo $i; ?>" class="collapse <?php echo ($i == '1') ? 'show' : ''; ?>" aria-labelledby="headingTwo1" data-parent="#accordionExample4">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="">Type of ID</label>
                                    <?php echo form_dropdown('type_of_id[]', Dropdown::get_static('ids'), set_value('type_of_id[]', @$type_of_id), 'class="form-control" id="type_of_id"'); ?>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">ID Number</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="ID Number" name="id_number[]" value="">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon2-information"></i></span>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Date Issued</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control datePicker" placeholder="Date Issued" name="date_issued[]" value="" readonly>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon-calendar"></i></span>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Date Expiration</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control datePicker" placeholder="Date Expiration" name="date_expiration[]" value="" readonly>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="flaticon-calendar"></i></span>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">Place Issued</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="Place Issued" name="place_issued[]" value="">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-o"></i></span>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                                <div class="form-group">
                                    <label class="">File Upload</label>
                                    <input type="file" name="file_id[]" class="" aria-invalid="false">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
                
            <?php endif; ?>

        </div>

        <!--end::Accordion-->

    </div>
</div>