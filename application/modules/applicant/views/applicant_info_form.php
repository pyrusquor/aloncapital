<!DOCTYPE html>
<html>

<head>
    <title><?= $applicant['last_name'] . ', ' . $applicant['first_name'] ?></title>
</head>

<body>
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <style>
        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .table-no-margin {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: -6px;
            margin-top: -6px;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 15px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th {
            padding: 5px 8px;
        }

        .table.table-borderless>thead>tr>th {
            border: 2px solid #000000;
        }

        .table>tfoot>tr>td {
            border: 2px solid #000000;
        }

        .table>tbody>tr>td {
            border-left: 2px solid #000000;
            border-right: 2px solid #000000;
            border-top: 2px solid #000000;
            border-bottom: 2px solid #000000;
        }

        .table>tfoot {
            border-top: 2px solid #000000;
        }

        .table>tbody {
            border-bottom: 1px solid #000000;
        }

        .table.table-striped>thead>tr>th {
            background: #00529c;
            color: #fff;
            border: 0;
            padding: 12px 8px;
            text-transform: uppercase;
        }

        .table.table-striped>thead>tr>th a {
            color: #fff;
            font-weight: 400;
        }

        .table.table-striped>thead>tr:nth-child(2)>th {
            background: #0075de;
        }

        .table.table-striped td {
            border: 0;
            vertical-align: middle;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background: #fff;
        }

        .table-striped>tbody>tr:nth-of-type(even) {
            background: #f1f1f1;
        }

        .color-bluegreen {
            color: #169f98;
        }

        .color-white {
            color: #fff;
        }

        .peso_currency {
            font-family: DejaVu Sans;
        }

        .bg-bluegreen {
            background-color: #169f98;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .margin0 {
            margin: 0;
        }

        .padding10 {
            padding: 10px;
        }

        p {
            margin: 0 0 15px;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-warning {
            background-color: #fcf8e3;
            border-color: #faebcc;
            color: #8a6d3b;
        }

        .company-logo-left {
            display: inline-block;
            margin-top: -70px;
            margin-left: auto;
            margin-right: 110px;
            height: 120px;
        }

        .company-logo-right {
            display: inline-block;
            margin-top: -79px;
            margin-left: -100px;
            margin-right: auto;
            height: 150px;
        }

        input[type="text"],
        select.form-control {
            background: transparent;
            border: none;
            border-bottom: 1px solid #000000;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        input[type="text"]:focus,
        select.form-control:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        td.black,
        th.black {
            background: black !important;
            color: white;
            position: sticky !important;
        }

        .square {
            margin-top: 2px;
            height: 13px;
            width: 13px;
            border: solid 2px black;
        }

        .square-fill {
            margin-top: 2px;
            height: 13px;
            width: 13px;
            border: solid 2px black;
            background: black;
        }

        .all-border {
            border-top: 1px solid #000000 !important;
            border-bottom: 1px solid #000000 !important;
            border-left: 1px solid #000000 !important;
            border-right: 1px solid #000000 !important;
        }

        .top-border {
            border-top: 2px solid #000000 !important;
        }

        .bottom-border {
            border-bottom: 2px solid #000000 !important;
        }

        .no-bottom-border {
            border-bottom: none !important;
        }

        .no-top-border {
            border-top: none !important;
        }

        .right-border {
            border-right: 2px solid #000000 !important;
        }

        .no-right-border {
            border-right: none !important;
        }
    </style>
    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body m-5 p-5">
                <div class="row" style="text-align:center">
                    <img class="company-logo-left" src="<?= base_url('assets/img/shjdc.png') ?>" width="220px" height="110px" />
                    <img class="company-logo-right" src="<?= base_url('assets/img/pdp_township.png') ?>" width="200px" height="150px" />
                </div>
                <div class="row" style="margin-top:40px">
                    <div class='col-md-12'>
                        <h4 class='kt-font-boldest' style="text-align:center; color:#646c9a;"><u>BUYER INFORMATION SHEET</u></h4>
                    </div>
                </div>
                <div class="row">
                    <div class='col-md-1'></div>
                    <div class='col-md-10' stlye="text-align:center">
                        <h5><span class='kt-font-boldest'>INSTRUCTIONS: </span>
                            Kindly accomplish this form by filling out or putting a check (✓) on the spaces provided and "N/A" for the fields that are not applicable.
                            Fields with asterisks (<span class='kt-font-danger kt-font-boldest'>*</span>) are mandatory. <span class='kt-font-danger kt-font-boldest'>Please write in block letters (all caps)</span> using either blue or black pen.
                            All details in this form must be consistent with all documents.</details></b></h5>
                    </div>
                    <div class='col-md-1'></div>
                </div>
                <div class="row">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th class="font-weight-bold text-center col-md-6 black" scope="col" colspan=2>
                                    Applicant Personal Data
                                </th>
                                <th class="font-weight-bold text-center black" scope="col" style="width: 1%">

                                </th>
                                <th class="font-weight-bold text-center col-md-6 black" scope="col" colspan=2>
                                    Spouse/Co-Applicant Personal Data
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="font-weight-bold text-center col-md-6" colspan=2>
                                </td>
                                <td class="font-weight-bold text-center black">
                                    &nbsp;
                                </td>
                                <td class="col-md-6" scope="col" colspan=2>
                                    <div class='row'>&nbsp;&nbsp;Relationship to Applicant:&nbsp;&nbsp;<span class='square'></span>&nbsp;Spouse&nbsp;&nbsp;&nbsp;&nbsp;<span class='square'></span>&nbsp;Co-Applicant</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border">
                                    Last Name<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    First Name<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border">
                                    Last Name<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    First Name<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= strtoupper($applicant['last_name']) ?></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= strtoupper($applicant['first_name']) ?></h5>

                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border">
                                </td>
                                <td class="no-top-border">
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border">
                                    Middle Name<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Suffix
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border">
                                    Middle Name<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Suffix
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= strtoupper($applicant['middle_name']) ?></h5>
                                </td>
                                <td class="no-top-border">
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border">
                                </td>
                                <td class="no-top-border">
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Nationality<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Civil Status<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Gender<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'><?= strtoupper($applicant['nationality']) ?></h5>
                                            </td>
                                            <td class="right-border">
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='<?= $applicant['civil_status_id'] == '1' ? 'square-fill' : 'square' ?>'></span>&nbsp;Single
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='<?= $applicant['civil_status_id'] == '2' ? 'square-fill' : 'square' ?>'></span>&nbsp;Married
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='<?= $applicant['civil_status_id'] == '3' ? 'square-fill' : 'square' ?>'></span>&nbsp;Widow/Widower
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='<?= $applicant['civil_status_id'] == '4' ? 'square-fill' : 'square' ?>'></span>&nbsp;Divorce
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='<?= $applicant['civil_status_id'] == '5' ? 'square-fill' : 'square' ?>'></span>&nbsp;Legally Separated
                                                </div>
                                            </td>
                                            <td>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='<?= $applicant['gender'] == '1' ? 'square-fill' : 'square' ?>'></span>&nbsp;Male
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='<?= $applicant['gender'] == '2' ? 'square-fill' : 'square' ?>'></span>&nbsp;Female
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Nationality<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Civil Status<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Gender<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'></h5>
                                            </td>
                                            <td class="right-border">
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='square'></span>&nbsp;Single
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='square'></span>&nbsp;Married
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='square'></span>&nbsp;Widow/Widower
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='square'></span>&nbsp;Divorce
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='square'></span>&nbsp;Legally Separated
                                                </div>
                                            </td>
                                            <td>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='square'></span>&nbsp;Male
                                                </div>
                                                <div class='row'>
                                                    &nbsp;&nbsp;&nbsp;<span class='square'></span>&nbsp;Female
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border">
                                    Date of Birth (mm/dd/yyyy)<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Place of Birth<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border">
                                    Date of Birth (mm/dd/yyyy)<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Place of Birth<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= date('m/d/Y', strtotime($applicant['birth_date'])) ?></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['birth_place'] ?></h5>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border">
                                    Tax ID No. (first 9 digits only)<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Valid ID Type<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border">
                                    Tax ID No. (first 9 digits only)<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Valid ID Type<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['tin'] ?></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['identifications'][0]['type_of_id'] ?></h5>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border">
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td>
                                                ID No. <span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5 class='kt-font-boldest display-5'><?= $applicant['identifications'][0]['id_number'] ?></h5>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                </td>
                                <td class="no-top-border">
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Date Issued<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Place Issued<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'><?= $applicant['identifications'][0]['date_issued'] ?></h5>
                                            </td>
                                            <td>
                                                <h5 class='kt-font-boldest display-5'><?= $applicant['identifications'][0]['place_issued'] ?></h5>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border">
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td>
                                                ID No. <span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h5 class='kt-font-boldest display-5'></h5>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                </td>
                                <td class="no-top-border">
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Date Issued<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Place Issued<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td>
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-6" scope="col" colspan=2>
                                    <div class='row'>&nbsp;&nbsp;<span class='kt-font-boldest'>Residence Address</span>&nbsp;&nbsp;<span class='square'></span>&nbsp;This is my official mailing address&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td class="font-weight-bold text-center black">
                                    &nbsp;
                                </td>
                                <td class="col-md-6" scope="col" colspan=2>
                                    <div class='row'>&nbsp;&nbsp;<span class='kt-font-boldest'>Residence Address</span>&nbsp;&nbsp;<span class='square'></span>&nbsp;This is my official mailing address&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border" colspan=2>
                                    House No. /Street/Brgy.<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border" colspan=2>
                                    House No. /Street/Brgy.<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <h5 class='kt-font-boldest display-5'><?= strtoupper($applicant['barangay']) ?></h5>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan=2>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Municipality/City<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Province<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Country<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Zip Code<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;<?= $applicant['city']['citymunDesc'] ?></h5>
                                            </td>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;<?= $applicant['province']['provDesc'] ?></h5>
                                            </td>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;<?= $applicant['country']['name'] ?></h5>
                                            </td>
                                            <td>
                                                <h5 class='kt-font-boldest display-5'>&nbsp;<?= $applicant['city']['zipCode'] ?></h5>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Municipality/City<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Province<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Country<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Zip Code<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td>
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border">
                                    Mobile No.<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Landline No.
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border">
                                    Mobile No.<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Landline No.
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['mobile_no'] ?></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['landline'] ?></h5>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border">
                                    E-mail Address<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Facebook Messenger Account<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border">
                                    E-mail Address<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Facebook Messenger Account<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['email'] ?></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['social_media'] ?></h5>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-6" scope="col" colspan=2>
                                    <div class='row'>&nbsp;&nbsp;<span class='kt-font-boldest'>Office Address</span>&nbsp;&nbsp;<span class='square'></span>&nbsp;This is my official mailing address&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td class="font-weight-bold text-center black">
                                    &nbsp;
                                </td>
                                <td class="col-md-6" scope="col" colspan=2>
                                    <div class='row'>&nbsp;&nbsp;<span class='kt-font-boldest'>Office Address</span>&nbsp;&nbsp;<span class='square'></span>&nbsp;This is my official mailing address&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border" colspan=2>
                                    Company Name/Employer<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border" colspan=2>
                                    Company Name/Employer<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['employment'] ? $applicant['employment']['employer'] : ' '; ?></h5>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan=2>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border" colspan=2>
                                    Building No./Street/Brgy.<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border" colspan=2>
                                    Building No./Street/Brgy.<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <h5 class='kt-font-boldest display-5'><?= $applicant['employment'] ? $applicant['employment']['address'] : ' '; ?></h5>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan=2>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Municipality/City<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Province<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Country<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Zip Code<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;<?= $applicant['employment'] ? Dropdown::get_static('occupation_location', $applicant['employment']['location_id'], 'view') : ''; ?></h5>
                                            </td>
                                            <td>
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Municipality/City<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Province<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Country<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Zip Code<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td class="right-border">
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                            <td>
                                                <h5 class='kt-font-boldest display-5'>&nbsp;</h5>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border">
                                    Company/Employer Contact No.<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Company/Employer E-mail Address<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border">
                                    Company/Employer E-mail Address<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="no-bottom-border">
                                    Company/Employer Contact No.<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                                <td class="no-top-border">
                                    <h5 class='kt-font-boldest display-5'></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom-border" colspan=2>
                                    Housing Membership<span class='kt-font-danger'>*</span>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-bottom-border" colspan=2>
                                    Housing Membership<span class='kt-font-danger'>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-2'>
                                            <div class='row'>
                                                <span class='<?= $applicant['housing_membership_id'] == '1' ? 'square-fill' : 'square' ?>'></span>&nbsp;SSS
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['housing_membership_id'] == '0' || $applicant['housing_membership_id'] == '' ? 'square-fill' : 'square' ?>'></span>&nbsp;None
                                            </div>
                                        </div>
                                        <div class='col-sm-2'>
                                            <div class='row'>
                                                <span class='<?= $applicant['housing_membership_id'] == '2' ? 'square-fill' : 'square' ?>'></span>&nbsp;GSIS
                                            </div>
                                        </div>
                                        <div class='col-sm-2'>
                                            <div class='row'>
                                                <span class='<?= $applicant['housing_membership_id'] == '3' ? 'square-fill' : 'square' ?>'></span>&nbsp;HDMF
                                            </div>
                                        </div>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='<?= $applicant['housing_membership_id'] == '4' ? 'square-fill' : 'square' ?>'></span>&nbsp;Others (please specify):_____________
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-2'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;SSS
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;None
                                            </div>
                                        </div>
                                        <div class='col-sm-2'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;GSIS
                                            </div>
                                        </div>
                                        <div class='col-sm-2'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;HDMF
                                            </div>
                                        </div>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Others (please specify):_____________
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-center col-md-6 black" scope="col" colspan=2>
                                    Source of Funding <em>(Please check all applicable)</em>
                                </td>
                                <td class="font-weight-bold text-center black" scope="col" style="width: 1%">

                                </td>
                                <td class="font-weight-bold text-center col-md-6 black" scope="col" colspan=2>
                                    Source of Funding <em>(Please check all applicable)</em>
                                    </th>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-3'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Business Income
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Savings
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment'] ? 'square-fill' : 'square' ?>'></span>&nbsp;Salary
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Shares of Stocks
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Benefits/Pension
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Remittances/Allotment
                                            </div>
                                        </div>
                                    </div>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Other Income:________________________
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-3'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Business Income
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Savings
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Salary
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Shares of Stocks
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Benefits/Pension
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Remittances/Allotment
                                            </div>
                                        </div>
                                    </div>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Other Income:________________________
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-center col-md-6 black" scope="col" colspan=2>
                                    Monthly Income<span class='kt-font-black'>*</span>
                                </td>
                                <td class="font-weight-bold text-center black" scope="col" style="width: 1%">

                                </td>
                                <td class="font-weight-bold text-center col-md-6 black" scope="col" colspan=2>
                                    Monthly Income<span class='kt-font-black'>*</span>
                                    </th>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment']['gross_salary'] >= 20000 && $applicant['employment']['gross_salary'] <= 29999  ? 'square-fill' : 'square' ?>'></span>&nbsp;<?= money_php('20000') . ' - ' . money_php('29999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment']['gross_salary'] >= 30000 && $applicant['employment']['gross_salary'] <= 39999  ? 'square-fill' : 'square' ?>'></span>&nbsp;<?= money_php('30000') . ' - ' . money_php('39999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment']['gross_salary'] >= 40000 && $applicant['employment']['gross_salary'] <= 49999  ? 'square-fill' : 'square' ?>'></span>&nbsp;<?= money_php('40000') . ' - ' . money_php('49999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment']['gross_salary'] >= 50000 && $applicant['employment']['gross_salary'] <= 99999  ? 'square-fill' : 'square' ?>'></span>&nbsp;<?= money_php('50000') . ' - ' . money_php('99999') ?>
                                            </div>
                                        </div>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment']['gross_salary'] >= 100000 && $applicant['employment']['gross_salary'] <= 149999  ? 'square-fill' : 'square' ?>'></span>&nbsp;<?= money_php('100000') . ' - ' . money_php('149999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment']['gross_salary'] >= 150000 && $applicant['employment']['gross_salary'] <= 199999  ? 'square-fill' : 'square' ?>'></span>&nbsp;<?= money_php('150000') . ' - ' . money_php('199999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment']['gross_salary'] >= 200000 && $applicant['employment']['gross_salary'] <= 249999  ? 'square-fill' : 'square' ?>'></span>&nbsp;<?= money_php('200000') . ' - ' . money_php('249999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment']['gross_salary'] >= 250000 ? 'square-fill' : 'square' ?>'></span>&nbsp;<?= money_php('250000') . ' or more' ?>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;<?= money_php('20000') . ' - ' . money_php('29999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;<?= money_php('30000') . ' - ' . money_php('39999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;<?= money_php('40000') . ' - ' . money_php('49999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;<?= money_php('50000') . ' - ' . money_php('99999') ?>
                                            </div>
                                        </div>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;<?= money_php('100000') . ' - ' . money_php('149999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;<?= money_php('150000') . ' - ' . money_php('199999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;<?= money_php('200000') . ' - ' . money_php('249999') ?>
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;<?= money_php('250000') . ' or more' ?>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="font-weight-bold text-center col-md-6 black" scope="col" colspan=2>
                                    Source of Income<span class='kt-font-black'>*</span>
                                </td>
                                <td class="font-weight-bold text-center black" scope="col" style="width: 1%">

                                </td>
                                <td class="font-weight-bold text-center col-md-6 black" scope="col" colspan=2>
                                    Source of Income<span class='kt-font-black'>*</span>
                                    </th>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment'] ? ($applicant['employment']['occupation_type_id'] == '2' ? 'square-fill' : 'square') : 'square'; ?>'></span>&nbsp;Private Employee
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment'] ? ($applicant['employment']['occupation_type_id'] == '1' ? 'square-fill' : 'square') : 'square'; ?>'></span>&nbsp;Government Employee
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment'] ? ($applicant['employment']['occupation_type_id'] == '3' ? 'square-fill' : 'square') : 'square'; ?>'></span>&nbsp;Business Owner
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment'] ? ($applicant['employment']['occupation_type_id'] == '4' || $applicant['employment']['occupation_type_id'] == '5' ? 'square-filled' : 'square') : 'square'; ?>'></span>&nbsp;OFW/Immigrant
                                            </div>
                                        </div>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Retired Pensioner
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Allottee
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment'] ? ($applicant['employment']['occupation_type_id'] == '7' ? 'square-fill' : 'square') : 'square'; ?>'></span>&nbsp;Unemployed
                                            </div>
                                            <div class='row'>
                                                <span class='<?= $applicant['employment'] ? ($applicant['employment']['occupation_type_id'] == '6' ? 'square-fill' : 'square') : 'square'; ?>'></span>&nbsp;Others
                                            </div>
                                        </div>
                                    </div>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                &nbsp;
                                            </div>
                                            <div class='row'>
                                                &nbsp;Position/Delegations:<?= $applicant['employment'] ? '   <u>' . $applicant['employment']['designation'] . '</u>' : '___________________________'; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                &nbsp;
                                            </div>
                                            <div class='row'>
                                                &nbsp;No. of years in service:___________________________
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="black">
                                    &nbsp;
                                </td>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Private Employee
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Government Employee
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Business Owner
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;OFW/Immigrant
                                            </div>
                                        </div>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Retired Pensioner
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Allottee
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Unemployed
                                            </div>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Others
                                            </div>
                                        </div>
                                    </div>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                &nbsp;
                                            </div>
                                            <div class='row'>
                                                &nbsp;Position/Delegations:___________________________
                                            </div>
                                        </div>
                                    </div>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-6'>
                                            <div class='row'>
                                                &nbsp;
                                            </div>
                                            <div class='row'>
                                                &nbsp;No. of years in service:___________________________
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="kt-portlet">
            <div class="kt-portlet__body m-5 p-5">
                <div class="row" style="text-align:center">
                    <img class="company-logo-left" src="<?= base_url('assets/img/shjdc.png') ?>" width="220px" height="110px" />
                    <img class="company-logo-right" src="<?= base_url('assets/img/pdp_township.png') ?>" width="200px" height="150px" />
                </div>
                <div class="row">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th class="kt-font-boldest col-md-6 black">
                                    Designated Contact Person
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center col-md-6">
                                    <div class='row'>
                                        &nbsp;&nbsp;<span class='kt-font-boldest'>Relationship to Applicant</span>&nbsp;&nbsp;
                                        <span class='square'></span>&nbsp;Relative&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span class='square'></span>&nbsp;Friend&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span class='square'></span>&nbsp;Relative&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span class='square'></span>&nbsp;Attorney-In-Fact&nbsp;<em>(Fields with double asterisks (<span class='kt-font-danger'>**</span>) are required only for Attorney-In-Fact)</em>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Last Name<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                First Name<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Middle Name<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Suffix<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Nationality<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Tax ID No. (first 9 digits only)<span class='kt-font-danger'>**</span>
                                            </td>
                                            <td class="right-border">
                                                Civil Status<span class='kt-font-danger'>**</span>
                                            </td>
                                            <td class="right-border">
                                                Gender<span class='kt-font-danger'>**</span>
                                            </td>
                                            <td class="right-border">
                                                Date of Birth (mm/dd/yyyy)<span class='kt-font-danger'>**</span>
                                            </td>
                                            <td>
                                                Place of Birth<span class='kt-font-danger'>**</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Valid ID Type<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                ID No.<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Date Issued<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Place Issued<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center col-md-6">
                                    <div class='row'>
                                        &nbsp;&nbsp;<span class='kt-font-boldest'>Resident Address</span>&nbsp;&nbsp;
                                        <span class='square'></span>&nbsp;In case the Applicant cannot be contacted, communications will be sent to the contact person.&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                Last Name<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                First Name<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Middle Name<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Suffix<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td class="right-border">
                                                House No./Street/Brgy.<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Municipality/City<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td class="right-border">
                                                Province<span class='kt-font-danger'>*</span>
                                            </td>
                                            <td>
                                                Country<span class='kt-font-danger'>*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td class="right-border">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="kt-font-boldest col-md-6 black">
                                    Source of Sale
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Booth/Showroom
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Event
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Advertisement
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Repeat Applicant
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Internet
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;E-mail
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Flyer
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Referral
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Employee
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="kt-font-boldest col-md-6 black">
                                    Reason for Buying
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan=2>
                                    <div class='row' style='margin-left:15px;'>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Primary Home
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;Secondary Home
                                            </div>
                                        </div>
                                        <div class='col-sm-1'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;For Rental/Resale
                                            </div>
                                        </div>
                                        <div class='col-sm-2'>
                                            <div class='row'>
                                                <span class='square'></span>&nbsp;For Other Family Members
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-top-border" colspan='2'>
                                    <table class='table-no-margin'>
                                        <tr>
                                            <td>
                                                <h5>Special Instructions (if any)</h5>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                &nbsp;
                &nbsp;
                &nbsp;
                <div class="row">
                    <div class='col-md-1'></div>
                    <div class='col-md-10' stlye="text-align:center">
                        <h5>
                            We certify that the foregoing information/statement is to my/our knowledge, true, correct, complete, and updated. The signature(s) appearing above my/our printed name/names below
                            is/are genuine. Further, I/we promise to notify PUEBLO DE PANAY, INC. of any amendments or changes in m/our personal information indicated herein.
                        </h5>
                        <h5>
                            By signing below, I/we consent to the collection, use,storage, processing, and retention of my/our personal data for the purpose of improving the Company's products and services.
                            I/We understand that my/our consent does not preclude the existence of other criteria for lawful processing of personal data, and does not waive any of my rights under the Data Privacy Act
                            of 2012 and other applicable laws.
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class='col-md-1'></div>
                    <div class='col-md-10' stlye="text-align:center">
                        <table class='table-no-margin' style='margin-top:100px'>
                            <tr>
                                <td>
                                    <h5 style='text-align:center'><strong><?= strtoupper($applicant['first_name'] . ' ' .$applicant['middle_name'] . ' ' .  $applicant['last_name']) ?></strong></h5>
                                </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td>
                                    <h5 style='text-align:center'><strong></strong></h5>
                                </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td>
                                    <h5 style='text-align:center'><strong></strong></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class='top-border'>
                                    <h5 style='text-align:center'>Signature over printed name</h5>
                                    <h5 style='text-align:center'><strong>BUYER</strong></h5>
                                </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td class='top-border'>
                                    <h5 style='text-align:center'>Signature over printed name</h5>
                                    <h5 style='text-align:center'><strong>SPOUSE/CO-OWNER</strong></h5>
                                </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td class='top-border'>
                                    <h5 style='text-align:center'>Signature over printed name</h5>
                                    <h5 style='text-align:center'><strong>ATTORNEY-IN-FACT</strong></h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class='top-border'>
                                    <h5 style='text-align:center'>Date</h5>
                                </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td class='top-border'>
                                    <h5 style='text-align:center'>Date</h5>
                                </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td class='top-border'>
                                    <h5 style='text-align:center'>Date</h5>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class='row' style='text-align:center;margin-left:15px;'>
                    &nbsp;
                </div>
                <div class='row' style='text-align:center;margin-left:15px;'>
                    &nbsp;
                </div>
                <div class='row' style='text-align:center;margin-left:15px;'>
                    &nbsp;
                </div>
                <div class='row' style='text-align:center;margin-left:15px;'>
                    <div class='col-md-1'></div>
                    <div class='col-md-10' stlye="text-align:center">
                        <div class='row'>
                            <strong>ENDORSED BY:</strong>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-md-1'></div>
                    <div class='col-md-3' stlye="text-align:center">
                        <div class='row'>
                            <span class='square'></span>&nbsp;THR
                        </div>
                        <div class='row'>
                            <span class='square'></span>&nbsp;SHJ Prime Realty
                        </div>
                    </div>
                    <div class='col-md-3' stlye="text-align:center">
                        <div class='row'>
                            <span class='square'></span>&nbsp;SHJDC Corp. Sales Division
                        </div>
                        <div class='row'>
                            <span class='square'></span>&nbsp;PDP Realty
                        </div>
                    </div>
                    <div class='col-md-3' stlye="text-align:center">
                        <div class='row'>
                            <span class='square'></span>&nbsp;SHJDC Broker's Network
                        </div>
                        <div class='row'>
                            <span class='square'></span>&nbsp;PDP Global Sales
                        </div>
                    </div>
                </div>
                <div class='row' style='text-align:center;margin-left:15px;'>
                    &nbsp;
                </div>
                <div class='row' style='text-align:center;margin-left:15px;'>
                    &nbsp;
                </div>
                <div class='row' style='text-align:center;margin-left:15px;'>
                    &nbsp;
                </div>
                <div class="row">
                    <div class='col-md-1'></div>
                    <div class='col-md-10' stlye="text-align:center">
                        <table class='table-no-margin'>
                            <tr>
                                <td class='all-border'>
                                    <h5 style='text-align:center'><strong>Designation</strong></h5>
                                </td>
                                <td class='all-border'>
                                    <h5 style='text-align:center'><strong>Name</strong></h5>
                                </td>
                                <td class='all-border'>
                                    <h5 style='text-align:center'><strong>Signature</strong></h5>
                                </td>
                                <td class='all-border'>
                                    <h5 style='text-align:center'><strong>Date</strong></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class='all-border'>
                                    <h5>Property Consultant</h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class='all-border'>
                                    <h5>Sales Manager</h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class='all-border'>
                                    <h5>Marketing Partner</h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class='all-border'>
                                    <h5>Broker</h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class='all-border'>
                                    <h5>Deputy Sales Head</h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style='margin-top:100px'>
                    <div class='col-md-1'></div>
                    <div class='col-md-10' stlye="text-align:center">
                        <table class='table-no-margin'>
                            <tr>
                                <td style='width: 15%'>
                                </td>
                                <td class='all-border'>
                                    <h5 style='text-align:center'><strong>Name</strong></h5>
                                </td>
                                <td class='all-border'>
                                    <h5 style='text-align:center'><strong>Signature</strong></h5>
                                </td>
                                <td class='all-border'>
                                    <h5 style='text-align:center'><strong>Date</strong></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class='all-border'>
                                    <h5>Checked by (Compliance Associate)</h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                            </tr>
                            <tr>
                                <td class='all-border'>
                                    <h5>Approved and accepted by (Compliance Supervisor)</h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                                <td class='all-border'>
                                    <h5></h5>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>