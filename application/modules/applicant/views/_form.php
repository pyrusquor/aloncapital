<!--begin: Basic Info-->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
    <?php $this->load->view('_info_form'); ?>
</div>
<!--end: Basic Info-->

<!--begin: Applicant Contact Form -->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_contact_form'); ?>
</div>
<!--end: Applicant Contact Form -->

<!--begin: Applicant Source of Info -->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_work_exp_form'); ?>
-</div>
<!--end: Applicant Source of Info -->

<!--begin: Applicant Work Experience -->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_source_form'); ?>
-</div>

<!--begin: Form Actions -->
<div class="kt-form__actions">
    <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-prev">
        Previous
    </div>
    <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-submit">
        Submit
    </div>
    <div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-next">
        Next Step
    </div>
</div>
<!--end: Form Actions -->


