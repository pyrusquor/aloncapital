<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$description = isset($info['description']) && $info['description'] ? $info['description'] : '';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name"
                    value="<?php echo set_value('name', $name); ?>" placeholder="Name"
                    autocomplete="off" id="name">
                    <span class="kt-input-icon__icon"></span>
            </div>
                <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Description <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="description"
                   value="<?php echo set_value('description', $description); ?>" placeholder="Description"
                   autocomplete="off" id="description">
                   <span class="kt-input-icon__icon"></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>