<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_document_stage_logs_model extends MY_Model {
    public $table = 'transaction_document_stage_logs'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'transaction_id',
        'previous_id',
        'new_id',
        'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}