<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Create_purchase_order_item_log_table extends CI_Migration
    {

        private $tbl = "purchase_order_item_logs";
        private $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'purchase_order_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'purchase_order_log_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'purchase_order_item_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'supplier_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'warehouse_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'purchase_order_request_item_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'purchase_order_request_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'material_request_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'item_group_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'item_type_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'item_brand_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'item_class_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'item_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'unit_of_measurement_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'unit_cost' => array(
                'type' => 'DOUBLE',
                'constraint' => '20,2',
                'NULL' => FALSE
            ),
            'total_cost' => array(
                'type' => 'DOUBLE',
                'constraint' => '20,2',
                'NULL' => FALSE
            ),
            'quantity' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'receiving_status' => array(
                'type' => 'INT',
                'constraint' => '2',
                'NULL' => TRUE,
                'default' => '1'
            )
        );

        public function up()
        {
            if (!$this->db->table_exists($this->tbl)) {
                $this->dbforge->add_field($this->fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->tbl, TRUE);
            }
        }

        public function down()
        {
            if ($this->db->table_exists($this->tbl)) {
                $this->dbforge->drop_table($this->tbl);
            }
        }
    }
