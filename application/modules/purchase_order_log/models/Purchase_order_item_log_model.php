<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Purchase_order_item_log_model extends MY_Model
    {
        public $table = 'Purchase_order_item_logs'; // you MUST mention the table name
        public $primary_key = 'id';
        public $fillable = [
            'id',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'deleted_at',
            'deleted_by',
            'purchase_order_id',
            'purchase_order_log_id',
            'purchase_order_item_id',
            'supplier_id',
            'warehouse_id',
            'purchase_order_request_item_id',
            'purchase_order_request_id',
            'material_request_id',
            'item_group_id',
            'item_type_id',
            'item_brand_id',
            'item_class_id',
            'item_id',
            'unit_of_measurement_id',
            'unit_cost',
            'total_cost',
            'quantity',
            'receiving_status',
        ];
        public $form_fillables = [
            'id',
            'purchase_order_id',
            'purchase_order_log_id',
            'purchase_order_item_id',
            'supplier_id',
            'warehouse_id',
            'purchase_order_request_item_id',
            'purchase_order_request_id',
            'material_request_id',
            'item_group_id',
            'item_type_id',
            'item_brand_id',
            'item_class_id',
            'item_id',
            'unit_of_measurement_id',
            'unit_cost',
            'total_cost',
            'quantity',
            'receiving_status',
        ];
        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [

        ];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = true;
            $this->return_as = 'array';

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;

            // for relationship tables
            // $this->has_many['table_name'] = array();
        }

        function get_columns()
        {
            $_return = FALSE;

            if ($this->fillable) {
                $_return = $this->fillable;
            }

            return $_return;
        }

        public function insert_dummy()
        {
            require APPPATH . '/third_party/faker/autoload.php';
            $faker = Faker\Factory::create();

            $data = [];

            for ($x = 0; $x < 10; $x++) {
                array_push($data, array(
                    'name' => $faker->word,
                ));
            }
            $this->db->insert_batch($this->table, $data);

        }
    }