<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sales_recognize_model extends MY_Model
{
    public $table = 'sales_recognize'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'reference',
        'is_recognized',
        'entry_id',
        'transaction_id',
        'property_id',
        'project_id',
        'buyer_id',
        'total_collectible_price',
        'total_payments_made',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'deleted_by',
        'deleted_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['transaction'] = array('foreign_model' => 'transaction/transaction_model', 'foreign_table' => 'transactions', 'foreign_key' => 'id', 'local_key' => 'reference');

        $this->has_one['buyer'] = array('foreign_model' => 'buyer/buyer_model', 'foreign_table' => 'buyers', 'foreign_key' => 'id', 'local_key' => 'buyer_id');

        $this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');

        $this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
