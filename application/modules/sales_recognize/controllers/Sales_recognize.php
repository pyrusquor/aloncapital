<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_recognize extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Sales_recognize_model', 'M_Sales_recognize');
        $this->load->model('transaction/Transaction_model', 'M_Transaction');
        $this->load->model('ar_clearing/Ar_clearing_model', 'M_Ar_clearing');
        $this->load->model('transaction_payment/Transaction_official_payment_model', 'M_Transaction_official_payment');
        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');
        $this->load->model('accounting_entries/Accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_setting_items/Accounting_setting_items_model', 'M_Accounting_setting_items');
        $this->load->model('accounting_settings/Accounting_settings_model', 'M_Accounting_settings');
        $this->load->model('project/Project_model', 'M_project');
        $this->load->model('standard_report/Standard_report_model', 'M_report');

        $this->load->helper('form');

        $this->_table_fillables = $this->M_Sales_recognize->fillable;
        $this->_table_columns = $this->M_Sales_recognize->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // $db_columns = $this->M_Sales_recognize->get_columns();
        // if ($db_columns) {
        //     $column = [];
        //     foreach ($db_columns as $key => $dbclm) {
        //         $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

        //         if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
        //             if (strpos($name, '_id') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } elseif (strpos($name, 'is_') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } else {
        //                 $column[$key]['value'] = $name;
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             }
        //         } else {
        //             continue;
        //         }
        //     }

        //     $column_count = count($column);
        //     $cceil = ceil(($column_count / 2));

        //     $this->view_data['columns'] = array_chunk($column, $cceil);
        //     $column_group = $this->M_Sales_recognize->count_rows();
        //     if ($column_group) {
        //         $this->view_data['total'] = $column_group;
        //     }
        // }


        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }
    public function recognize_all($value = '')
    {
        $this->getTransactionTpm();
    }
    public function getTransactionTpm()
    {
        $transactions = $this->M_Transaction->with_buyer()->with_project()->with_property()->as_array()->get_all();

        $transactions_to_be_recognized = array();

        foreach ($transactions as $key => $transaction) {
            $this->transaction_library->initiate($transaction['id']);

            $tpm_p = $this->transaction_library->get_amount_paid(2, 0, 1);

            $tpm = $this->transaction_library->get_amount_paid(2, 0);

            $exists = $this->M_Sales_recognize->get(["transaction_id" => $transaction['id']]);

            $transaction['total_collectible_price'] = $tpm;
            $transaction['total_payments_made'] = $tpm_p;

            if ($tpm_p >= 25 && $exists === false) {
                array_push($transactions_to_be_recognized, $transaction);

                $this->insert_tpms($transaction);
            }
        }

        return $transactions_to_be_recognized;
    }

    public function insert_tpms($transaction)
    {
        if ($transaction) {

            $recognized_sale['reference'] = $transaction['reference'];
            $recognized_sale['transaction_id'] = $transaction['id'];
            $recognized_sale['property_id'] = $transaction['property_id'];
            $recognized_sale['project_id'] = $transaction['project_id'];
            $recognized_sale['buyer_id'] = $transaction['buyer_id'];
            $recognized_sale['total_collectible_price'] = $transaction['total_collectible_price'];
            $recognized_sale['total_payments_made'] = $transaction['total_payments_made'];
            $recognized_sale['is_recognized'] = $transaction['is_recognized'];

            $this->M_Sales_recognize->insert($recognized_sale);
        }
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'property_id' => true,
            'project_id' => true,
            'buyer_id' => true,
            'total_collectible_price' => true,
            'total_payments_made' => true,
            'is_recognized' => true,
            'date_of_reaching_twenty_five_percent' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);


        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Transaction
                ->fields('id,reference,is_recognized,general_status')
                ->with_buyer('fields:first_name,middle_name,last_name')
                ->with_project('fields:name,lts_number')
                ->with_property('fields:name')
                ->where('general_status', '<', 4)
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("reference like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'id' => [
                    'data' => $filters['sales_recognize_id'] ?? null,
                    'operator' => '=',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'buyer_id' => [
                    'data' => $filters['buyer_id'] ?? null,
                    'operator' => '=',
                ],
                'is_recognized' => [
                    'data' => $filters['is_recognized'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                } else {

                    if ($key == 'is_recognized') {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $this->transaction_library->initiate($value['id']);
                        $tcp = $this->transaction_library->get_amount_paid(2, 0);
                        $tpm = $this->transaction_library->get_amount_paid(2, 0, 1);

                        $date_of_reaching_twenty_five_percent = $this->transaction_library->get_date_of_reaching_twenty_five_percent();

                        $items[$key]['reference'] = "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['id'] . "'>" . $value['reference'] . "</a>";

                        $items[$key]['total_collectible_price'] = money_php($tcp);
                        $items[$key]['total_payments_made'] = $tpm . ' %';
                        $items[$key]['property_id'] = isset($value['property']) ? "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['property']['id'] . "'>" . $value['property']['name'] . "</a>" : '';
                        $items[$key]['project_id'] = isset($value['project']) ? "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['project']['id'] . "'>" . $value['project']['name'] . "</a>"  . "<br>LTS : " . $value['project']["lts_number"] : '';
                        $items[$key]['buyer_id'] = isset($value['buyer']) ? "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $value['buyer']['id'] . "'>" . get_fname($value['buyer']) . "</a>" : '';
                        $items[$key]['date_of_reaching_twenty_five_percent'] = $date_of_reaching_twenty_five_percent ?? '';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Transaction->where('general_status', '<', 4)->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function bulkActions()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $ids_arr = $this->input->post('ids_arr');
            $type = $this->input->post('type');

            if ($ids_arr) {
                if ($type === "delete") {
                    foreach ($ids_arr as $value) {
                        $data = [
                            'deleted_by' => $this->session->userdata['user_id'],
                        ];
                        $this->db->update('sales_recognize', $data, array('id' => $value));
                        $deleted = $this->M_Sales_recognize->delete($value);
                    }
                    if ($deleted !== false) {

                        $response['status'] = 1;
                        $response['message'] = 'Sales_recognize Successfully Deleted';
                    }
                } else if ($type === "recognize") {

                    $has_already_reconized = false;
                    $is_less_than_25_percent = false;

                    $this->db->trans_begin();

                    foreach ($ids_arr as $value) {

                        $result = $this->recognize_sale($value, true);

                        if ($result['status'] == 0) {

                            $has_already_reconized = true;
                            break;
                        } else if ($result['status'] == 2) {

                            $is_less_than_25_percent = true;
                            break;
                        }
                    }

                    if ($has_already_reconized) {

                        $response['status'] = 0;
                        $response['message'] = 'Transaction with ID "' . $result['transaction_id'] . '" is already recognized';

                        echo json_encode($response);
                        exit;
                    } else if ($is_less_than_25_percent) {

                        $response['status'] = 0;
                        $response['message'] = 'Transaction with ID "' . $result['transaction_id'] . '" has less than 25% of Total Payments Made';

                        echo json_encode($response);
                        exit;
                    }

                    if ($this->db->trans_status() === FALSE) {

                        $this->db->trans_rollback();
                        $response['status'] = 0;
                        $response['message'] = 'Error!';
                    } else {

                        $this->db->trans_commit();
                        $response['status'] = 1;
                        $response['message'] = 'Transactions Successfully Recognized.';
                    }

                    echo json_encode($response);
                    exit;
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {

            $this->view_data['data'] = $this->M_Transaction->with_buyer()->with_project()->with_property()->as_array()->get($id);

            $this->transaction_library->initiate($id);
            $this->view_data['tcp'] = $this->transaction_library->get_amount_paid(2, 0);
            $this->view_data['tpm'] = $this->transaction_library->get_amount_paid(2, 0, 1);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function derecognize_sale(){
        $data=$this->input->post();
        $transaction_id = $data['transaction_id'];
        $response['status'] = 0;
        $response['message'] = 'An error occured, please check data!';
        $transaction = $this->M_Transaction->fields('id, sales_recognize_entry_id')->get($transaction_id);
        if($transaction['sales_recognize_entry_id']){
            $filters = [
                'id' => $transaction['sales_recognize_entry_id'],
                'remarks' => 'SALES RECOGNIZE'
            ];
            $accounting_entry = $this->M_Accounting_entries->get($filters);
            if($accounting_entry){
                $this->M_Accounting_entries->delete($transaction['sales_recognize_entry_id']);
                $this->M_Accounting_entry_items->delete(['accounting_entry_id' => $transaction['sales_recognize_entry_id']]);
                $this->M_Transaction->update(['is_recognized' => '0', 'sales_recognize_entry_id' => '0'],$transaction['id']);
                $response['status'] = 1;
                $response['message'] = 'Successfully reverted recognition';
            }
        }
        echo json_encode($response);
        exit();
    }

    public function recognize_sale($transaction_id = 0, $is_bulk = false)
    {

        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $data = [];

        if (!$transaction_id) {
            $data = $this->input->post();
            $transaction_id = $data['sr_transaction_id'];
        }

        $transaction = $this->M_Transaction->with_payments()->with_t_property()->get($transaction_id);

        // $ar = $this->M_Ar_clearing->get(['transaction_id' => $transaction_id]);

        $project_id = $transaction['project_id'];

        $project = $this->M_project->get($project_id);

        $is_recognized = $transaction['is_recognized'];

        if ($is_recognized !== 1) {

            $this->transaction_library->initiate($transaction['id']);
            $tpm = $this->transaction_library->get_amount_paid(2, 0, 1);

            // recognize only >= 25%
            if ($tpm < 25) {

                if ($is_bulk) {

                    $response['transaction_id'] = $transaction['id'];
                    $response['status'] = 2;

                    return $response;
                } else {

                    $response['status'] = 0;
                    $response['message'] = 'This transaction is less than 25% of Total Payments Made';

                    echo json_encode($response);
                    exit;
                }
            } else {

                if (!$is_bulk) {
                    $this->db->trans_begin();
                }

                $recognized['is_recognized'] = 1;
                $this->M_Transaction->update($recognized + $additional, $transaction_id);

                $this->transaction_library->initiate($transaction_id);
                $tcp = $this->transaction_library->get_amount_paid(2, 0);

                $entries['or_number'] =  (isset($data['sr_or_number']) && !empty($data['sr_or_number'])) ? $data['sr_or_number']  : "";
                // isset($_land['id']) && $_land['id'] ? $_land['id'] : '';
                $entries['company_id'] = $project['company_id'];
                $entries['invoice_number'] = (isset($data['sr_or_number']) && !empty($data['sr_or_number'])) ? $data['sr_or_number']  : "";
                $entries['journal_type'] = 3;
                $entries['payment_date'] = NOW;
                $entries['payee_type'] = "buyers";
                $entries['payee_type_id'] = $transaction['buyer_id'];
                $entries['remarks'] = "SALES RECOGNIZE";
                $entries['cr_total'] = $tcp;
                $entries['dr_total'] = $tcp;

                $entry_id = $this->M_Accounting_entries->insert($entries);

                $recognized['sales_recognize_entry_id'] = $entry_id;
                $recognized['is_recognized'] = 1;

                $this->M_Transaction->update($recognized + $additional, $transaction_id);

                $all_payment = $this->M_Transaction_official_payment->get_all(["transaction_id" => $transaction['id']]);

                $filter = ['period_id' => 5, 'for_recognize' => 1];

                $accounting_setting = $this->M_Accounting_settings->with_accounting_setting_item()->get($filter);

                $accounting_setting_id = $accounting_setting['id'];

                $accounting_setting_items = $this->M_Accounting_setting_items->get_all(["accounting_settings_id" => $accounting_setting_id]);

                foreach ($accounting_setting_items as $key => $entry) {

                    $entry_item['amount'] = $amount = $this->get_sales_recog_amount($key, $all_payment, $transaction, $data, $is_bulk);

                    if ($amount <= 0) {
                        continue;
                    }

                    $entry_item['accounting_entry_id'] = $entry_id;
                    $entry_item['ledger_id'] = $entry['accounting_entry'];
                    $entry_item['dc'] = $entry['accounting_entry_type'];
                    $entry_item['payee_type'] = "buyers";
                    $entry_item['payee_type_id'] = $transaction['buyer_id'];
                    $entry_item['is_reconciled'] = 0;
                    $entry_item['description'] = "";

                    $item = $this->M_Accounting_entry_items->insert($entry_item);
                }

                // Single
                if (!$is_bulk) {

                    if ($this->db->trans_status() === FALSE) {

                        $this->db->trans_rollback();
                        $response['status'] = 0;
                        $response['message'] = 'Error!';
                    } else {

                        $this->db->trans_commit();
                        $response['status'] = 1;
                        $response['message'] = 'Transaction is now recognized.';
                    }

                    // Multiple
                } else {

                    $response['status'] = 1;
                    return $response;
                }
            }
        } else {

            $response['status'] = 0;
            $response['message'] = 'This transaction is already recognized!';

            if ($is_bulk) {
                $response['transaction_id'] = $transaction['id'];
                return $response;
            }
        }

        echo json_encode($response);
    }

    public function get_sales_recog_amount($key = 0, $payments = array(), $transaction = array(), $data = array(), $is_bulk = false)
    {

        $customers_deposit = 0;
        $accounts_receivable = $transaction['t_property']['collectible_price'];
        $tcp = $transaction['t_property']['total_contract_price'];
        $interest_income = 0; // Collected
        $interest_receivable = 0; // Not yet collected

        $transaction_payments = $transaction['payments'];

        if (is_array($payments)) {

            foreach ($payments as  $payment) {
                $customers_deposit += $payment['principal_amount'];
                $interest_income += $payment['interest_amount'];
            }
        } else {

            $this->notify->error('Oops something went wrong.', 'sales_recognize');
        }

        foreach ($transaction_payments as  $t_payment) {
            $interest_receivable += $t_payment['interest_amount'];
        }

        $accounts_receivable = $accounts_receivable - $customers_deposit;

        $remaining_interest_receivable = $interest_receivable - $interest_income;

        // $unearned_interest_income = $interest_receivable - $interest_income;
        $unearned_interest_income = $interest_receivable;

        $tax_percent = 12;

        $tax = ($tax_percent / 100) * $tcp;

        switch ($key) {
            case 0:
                return $accounts_receivable;
                break;
            case 1:
                return $customers_deposit;
                break;
            case 2:
                return $is_bulk ? 0 : $tcp;
                break;
            case 3:
                return (isset($data['sr_cost_of_sales']) && !empty($data['sr_cost_of_sales'])) ? $data['sr_cost_of_sales']  : 0;
                break;
            case 4:
                return (isset($data['sr_development_cost']) && !empty($data['sr_development_cost'])) ? $data['sr_development_cost']  : 0;
                break;
            case 5:
                return (isset($data['sr_rawland']) && !empty($data['sr_rawland'])) ? $data['sr_rawland']  : 0;
                break;
            case 6:
                return $remaining_interest_receivable;
                break;
            case 7:
                // return $unearned_interest_income;
                return $is_bulk ? $tcp : 0;
                break;
            case 8:
                return $remaining_interest_receivable;
                break;
            case 9:
                return (isset($data['is_vatable']) && !empty($data['is_vatable'])) ? (($tcp / 1.12) * 0.12) : 0;
                break;
                // case 10:
                //     return $unearned_interest_income;
                //     break;
                // case 11:
                //     return (isset($data['is_vatable']) && !empty($data['is_vatable'])) ? (($tcp / 1.12) * 0.12)   : 0;
                //     break;

            default:
                return 0;
                break;
        }
    }
    public function get_sales_recognize_summary()
    {

        $for_recognition_count = 0;
        $recognized_count = 0;
        $for_recognition_amount = 0;
        $recognized_amount = 0;

        if ($this->input->post()) {

            $sales_recognize = $this->M_Sales_recognize->get_all();
            if ($sales_recognize) {

                foreach ($sales_recognize as $row) {

                    switch ($row['is_recognized']) {
                        case '1':
                            $recognized_count += 1;
                            $recognized_amount += $row['total_collectible_price'];
                            break;
                        default:
                            $for_recognition_count += 1;
                            $for_recognition_amount += $row['total_collectible_price'];
                            break;
                    }
                }
            }
        }
        $result['for_recognition_count'] = $for_recognition_count;
        $result['recognized_count'] = $recognized_count;
        $result['for_recognition_amount'] = money_php($for_recognition_amount);
        $result['recognized_amount'] = money_php($recognized_amount);
        echo json_encode($result);
        exit();
    }

     public function dashboard($debug = 0)
    {

        $Projects = $this->M_project->order_by('name', 'ASC')->as_array()->get_all();

        if ($Projects) {

            foreach ($Projects as $key => $project) {

                $params['project_id'] =  $project['id'];

                $data['rows'][$project['id']]  = $this->M_report->sales_recognition($params);
                
            }
        }

        $this->view_data = $data;

        if ($debug == 1) {
            # code...
            vdebug($this->view_data);
        }

        $this->template->build('dashboard', $this->view_data);
    }
}
