<div class=row>
    <div class="col-md-18 col-lg-18 col-xl-6">
        <div class="kt-portlet kt-portlet--solid-brand kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        FOR RECOGNITION
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="for_recognition_count" class="kt-font-boldest">254</h4>&nbsp;item(s)
                </div>
            </div>
            <div class="kt-portlet__body">
            <span class="kt-widget20__number display-4">
                <h4 id="for_recognition_amount"></h4>
            </span>
            </div>
        </div>
    </div>

    <div class="col-md-18 col-lg-18 col-xl-6">
        <div class="kt-portlet kt-portlet--solid-success kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        RECOGNIZED
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="recognized_count" class="kt-font-boldest">254</h4>&nbsp;item(s)
                </div>
            </div>
            <div class="kt-portlet__body">
            <span class="kt-widget20__number display-4">
                <h4 id="recognized_amount"></h4>
            </span>
            </div>
        </div>
    </div>
</div>