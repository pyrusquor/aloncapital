<?php

$transactions = $data;

$id = $transactions['id'];
$is_recognized = $transactions['is_recognized'];
$reference_number = @$transactions["reference"];
$property_id = @$transactions['property']["id"];
$project_id = @$transactions['project']["id"];
$buyer_id = @$transactions['buyer']["id"];

$property = get_person($property_id, 'properties');
$property_name = get_name($property);

$project = get_person($project_id, 'projects');
$project_name = get_name($project);

$buyer = get_person($buyer_id, 'buyers');
$buyer_name = get_fname($buyer);

$entry_id = $transactions['sales_recognize_entry_id'] ?? null;

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Sales Recognize</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('sales_recognize/recognize_sale/' . $id); ?>" target="_BLANK" class="btn btn-label-info btn-elevate btn-sm <?php echo $is_recognized == 1 ? "disabled" : "" ?>">
                    <i class="fa fa-check"></i> Clear<?php echo $is_recognized == 1 ? "ed" : "" ?>
                </a>
                <a href="<?php echo site_url('sales_recognize'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="row">
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Reference #
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $reference_number; ?>
                                        </p>
                                    </div>


                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Property
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $property_name; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Project
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $project_name; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Buyer
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $buyer_name; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Total Collectible Price
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo money_php($tcp ?? 0); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Total Payments Made
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo ($tpm ?? 0) . ' %'; ?>
                                        </p>
                                    </div>
                                    <?php if ($entry_id) : ?>
                                        <div class="col-sm-3 mb-3">
                                            <h6 class="kt-widget13__desc">
                                                Accounting Entries
                                            </h6>
                                            <a href="<?php echo base_url("accounting_entries/view/" . $entry_id) ?>" target="_blank" rel="noopener noreferrer">View Accounting Entries</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <!--end::Form-->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>