<div class="modal fade" id="salesRecognitionModal" tabindex="-1" role="dialog" aria-labelledby="salesRecognitionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Sales Recognize</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="recognizeSaleForm" method="POST" action="<?= base_url(); ?>sales_recognize/recognize_sale" onsubmit="event.preventDefault()">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Receipt Type <span class="kt-font-danger">*</span></label>
                                <select class="form-control" name="receipt_type" id="receipt_type">
                                    <option value="0">Select Type</option>
                                    <option value="1">Collection Receipt</option>
                                    <option value="2">Acknowledgement Receipt</option>
                                    <option value="3">Provisionary Receipt</option>
                                    <option value="4">Official Receipt</option>
                                    <option value="5">Manual Receipt</option>
                                    <option value="6">Sales Invoice</option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <label class="">Receipt Number</label>
                            <input type="text" name="sr_or_number" class="form-control" id="sr_or_number">
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Is Vatable <span class="kt-font-danger">*</span></label>
                                <?php echo form_dropdown('is_vatable', Dropdown::get_static('bool'), set_value('is_vatable', 1), 'class="form-control" id="is_vatable"'); ?>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <div class="col-sm-12 mb-3">
                            <label class="">Cost of Sales</label>
                            <input type="number" name="sr_cost_of_sales" class="form-control" id="sr_cost_of_sales" readonly>
                        </div>
                        <div class="col-sm-12 mb-3">
                            <label class="">Development Cost</label>
                            <input type="number" name="sr_development_cost" class="form-control" id="sr_development_cost">
                        </div>
                        <div class="col-sm-12 mb-3 d-none">
                            <label class="">Transaction ID</label>
                            <input type="text" name="sr_transaction_id" class="form-control" id="sr_transaction_id" readonly>
                        </div>
                        <div class="col-sm-12 mb-3">
                            <label class="form-control-label">Rawland</label>
                            <input type="number" name="sr_rawland" class="form-control" id="sr_rawland">
                        </div>
                        <div class="col-sm-12 mb-3">
                            <label class="form-control-label">Titling</label>
                            <input type="number" name="sr_titling" class="form-control" id="sr_titling">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="recognizeSaleForm" id="recognize_button">Recognize</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>