<?php

$transactions = $data["transaction"];

$id = @$transactions["id"];
$reference_number = @$transactions["reference"];
$property_id = @$transactions["property_id"];
$project_id = @$transactions["project_id"];
$buyer_id = @$transactions["buyer_id"];
$cheque_number = $data["cheque_number"] ? $data["cheque_number"] : "N/A";
$payment_type = $data["payment_type"] ? $data["payment_type"] : "N/A";
$is_active = $data["is_active"];
$amount = @$data["amount"];

$property = get_person($property_id, 'properties');
$property_name = get_name($property);

$project = get_person($project_id, 'projects');
$project_name = get_name($project);

$buyer = get_person($buyer_id, 'buyers');
$buyer_name = get_fname($buyer);

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View AR Clearing</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            <!-- 3 cleared -->
            <!-- 2 bounced -->
                <?php if($is_active != 3): ?>
                <a
                    href="javascript:void(0)"
                    class="btn btn-label-info btn-elevate btn-sm clear_transaction"
                >
                    <i class="fa fa-check"></i> Clear
                </a>
                <?php endif; ?>

                <?php if($is_active != 2 && $is_active != 3): ?>
                <a
                    href="javascript:void(0)"
                    class="btn btn-label-info btn-elevate btn-sm bounce_transaction"
                >
                    <i class="fa fa-circle"></i> Bounce
                </a>
                <?php endif; ?>
                <a
                    href="<?php echo site_url('ar_clearing'); ?>"
                    class="btn btn-label-instagram btn-sm btn-elevate"
                >
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div
    class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="row">
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Reference #
                                        </h6>
                                        <p
                                            class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                            <?php echo $reference_number; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Cheque #
                                        </h6>
                                        <p
                                            class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                            <?php echo $cheque_number; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Amount
                                        </h6>
                                        <p
                                            class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                            <?php echo money_php($amount); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Property
                                        </h6>
                                        <p
                                            class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                            <?php echo $property_name; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Project
                                        </h6>
                                        <p
                                            class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                            <?php echo $project_name; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">Buyer</h6>
                                        <p
                                            class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                            <?php echo $buyer_name; ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Payment Type
                                        </h6>
                                        <p
                                            class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                            <?php echo Dropdown::get_static('payment_types', $payment_type, 'view'); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-3 mb-3">
                                        <h6 class="kt-widget13__desc">
                                            Status
                                        </h6>
                                        <p
                                            class="kt-widget13__text kt-widget13__text--bold"
                                        >
                                            <?php echo Dropdown::get_static('ar_clearing_status', $is_active, 'view'); ?>
                                        </p>
                                    </div>
                                </div>
                                <!--end::Form-->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
