<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ar_clearing extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load models
        $transaction_models = array('transaction/Transaction_model' => 'M_Transaction', 'transaction/Transaction_payment_model' => 'M_Transaction_payment', 'transaction_payment/Transaction_official_payment_model' => 'M_Transaction_official_payment', 'buyer/Buyer_model' => 'M_buyer', 'project/Project_model' => 'M_project', 'transaction_post_dated_check/Transaction_post_dated_check_model' => 'M_pdc', 'transaction/Transaction_billing_logs_model' => 'M_Tbilling_logs', 'transaction_payment/Transaction_penalty_logs_model' => 'M_tpenalty_logs');

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('property/Property_model', 'M_property');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('accounting_entries/accounting_entries_model', 'M_Accounting_entries');
        $this->load->model('accounting_entry_items/accounting_entry_items_model', 'M_Accounting_entry_items');
        $this->load->model('accounting_settings/accounting_settings_model', 'M_Accounting_settings');
        $this->load->model('ar_clearing/Ar_clearing_model', 'M_Ar_clearing');
        $this->load->model('ticketing/Ticketing_model', 'M_ticketing');

        $this->load->model($transaction_models);


        $this->load->helper('form');

        $this->load->library('mortgage_computation');

        $this->_table_fillables = $this->M_Ar_clearing->fillable;
        $this->_table_columns = $this->M_Ar_clearing->__get_columns();
    }

    public function index()
    {
        // $_fills = $this->_table_fillables;
        // $_colms = $this->_table_columns;

        // $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        // $this->view_data['_columns'] = $this->__get_columns($_fills);

        // $db_columns = $this->M_Ar_clearing->get_columns();
        // if ($db_columns) {
        //     $column = [];
        //     foreach ($db_columns as $key => $dbclm) {
        //         $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

        //         if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
        //             if (strpos($name, '_id') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } elseif (strpos($name, 'is_') !== false) {
        //                 $column = $name;
        //                 $column[$key]['value'] = $column;
        //                 $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             } else {
        //                 $column[$key]['value'] = $name;
        //                 $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
        //                 $column[$key]['label'] = ucwords(strtolower($_title));
        //             }
        //         } else {
        //             continue;
        //         }
        //     }

        //     $column_count = count($column);
        //     $cceil = ceil(($column_count / 2));

        //     $this->view_data['columns'] = array_chunk($column, $cceil);
        //     $column_group = $this->M_Ar_clearing->count_rows();
        //     if ($column_group) {
        //         $this->view_data['total'] = $column_group;
        //     }
        // }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'transaction_id' => true,
            'payment_type_id' => true,
            'amount_paid' => true,
            'buyer_id' => true,
            'cheque_number' => true,
            'property_id' => true,
            'project_id' => true,
            'is_active' => true,
            'buyer_ref' => true,
            'property_ref' => true,
            'project_ref' => true,
            'transaction' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Ar_clearing
                ->with_buyer('fields:first_name,middle_name,last_name')
                ->with_project('fields:name')
                ->with_property('fields:name')
                ->with_transaction('fields:reference')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("cheque_number like '%$searchValue%'");
                $query->or_where("amount_paid like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'transaction_id' => [
                    'data' => $filters['transaction_id'] ?? null,
                    'operator' => '=',
                ],
                'payment_type_id' => [
                    'data' => $filters['payment_type_id'] ?? null,
                    'operator' => '=',
                ],
                'amount_paid' => [
                    'data' => $filters['amount_paid'] ?? null,
                    'operator' => 'like',
                ],
                'buyer_id' => [
                    'data' => $filters['buyer_id'] ?? null,
                    'operator' => '=',
                ],
                'cheque_number' => [
                    'data' => $filters['cheque_number'] ?? null,
                    'operator' => 'like',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'is_active' => [
                    'data' => $filters['is_active'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['amount_paid'] = money_php($value['amount_paid']);

                        $items[$key]['transaction_id'] = isset($value['transaction']) ? "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['transaction']['id'] . "'>" . $value['transaction']['reference'] . "</a>" : '';

                        $items[$key]['property_id'] = isset($value['property']) ? "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['property']['id'] . "'>" . $value['property']['name'] . "</a>" : '';

                        $items[$key]['project_id'] = isset($value['project']) ? "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['project']['id'] . "'>" . $value['project']['name'] . "</a>" : '';

                        $items[$key]['buyer_id'] = isset($value['buyer']) ? "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $value['buyer']['id'] . "'>" . get_fname($value['buyer']) . "</a>" : '';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Ar_clearing->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {
        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Ar_clearing->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $permission_status = $this->M_Ar_clearing->update($info + $additional, $id);
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];
                    $permission_status = $this->M_Ar_clearing->insert($info + $additional);
                }

                if ($permission_status) {
                    $response['status'] = 1;
                    $response['message'] = 'Ar_clearing Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $this->M_Ar_clearing->get($id);
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Ar_clearing->get($id);
            if ($list) {

                $deleted = $this->M_Ar_clearing->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Ar_clearing Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkActions()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $ids_arr = $this->input->post('ids_arr');
            $type = $this->input->post('type');

            if ($ids_arr) {
                if ($type === "delete") {
                    foreach ($ids_arr as $value) {
                        $data = [
                            'deleted_by' => $this->session->userdata['user_id'],
                        ];
                        $this->db->update('ar_clearing', $data, array('id' => $value));
                        $deleted = $this->M_Ar_clearing->delete($value);
                    }
                    if ($deleted !== false) {

                        $response['status'] = 1;
                        $response['message'] = 'Ar_clearing Successfully Deleted';
                    }
                } else if ($type === "clear") {
                    foreach ($ids_arr as $value) {
                        $data = [
                            'is_active' => 3,
                        ];
                        $result = $this->db->update('ar_clearing', $data, array('id' => $value));
                    }
                    if ($result !== false) {

                        $response['status'] = 1;
                        $response['message'] = 'AR_clearing Successfully Cleared';
                    }
                } else if ($type === "bounced") {
                    foreach ($ids_arr as $value) {
                        $data = [
                            'is_active' => 2,
                        ];
                        $result = $this->db->update('ar_clearing', $data, array('id' => $value));
                    }
                    if ($result !== false) {

                        $response['status'] = 1;
                        $response['message'] = 'AR_clearing Successfully Bounced';
                    }
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['data'] = $this->M_Ar_clearing->with_transaction()->get($id);

            if ($this->view_data['data']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function clear_transaction($id = false)
    {
        if ($id) {
            $transaction = $this->M_Transaction->get($id);
            $ar = $this->M_Ar_clearing->get(['transaction_id' => $id]);

            $status = $ar['is_active'];

            if ($status == 3 || $id == "undefined") {
                $response['status'] = 0;
                $response['message'] = 'This Transaction was already cleared.';
            } else {
                // $post['info']['period_id'] = $transaction['period_id'];
                // $post['info']['is_waived'] = $transaction['penalty_type'];
                // $post['info']['payment_type_id'] = $ar['payment_type'];
                // $post['info']['amount_paid'] = $ar['amount'];
                // $post['transaction_id'] = $transaction['id'];
                $transaction_id = $transaction['id'];

                $off_payment_data = $this->M_Transaction_official_payment->get(['transaction_id' => $transaction_id]);

                $off_payment_id = $off_payment_data['id'];

                $this->transaction_library->initiate($transaction_id);

                $response = $this->transaction_library->generate_entries($off_payment_id, true);

                $_data_collection = array(
                    "transaction_id" => $transaction_id,
                    "info" => array(
                        "period_count" => $ar['period_count'],
                        "period_id" => $ar['period_id'],
                        "payment_date" => $ar['payment_date'],
                        "or_date" => $ar['or_date'],
                        "is_waived" => $ar['is_waived'],
                        "penalty_amount" => $ar['penalty_amount'],
                        "amount_paid" => $ar['amount_paid'],
                        "payment_type_id" => $ar['payment_type_id'],
                        "post_dated_check_id" => $ar['post_dated_check_id'],
                        "cash_amount" => $ar['cash_amount'],
                        "check_deposit_amount" => $ar['check_deposit_amount'],
                        "receipt_type" => $ar['receipt_type'],
                        "OR_number" => $ar['OR_number'],
                        "adhoc_payment" => $ar['adhoc_payment'],
                        "grand_total" => $ar['grand_total'],
                        "remarks" =>  $ar['remarks'],
                    ),
                    "transaction_payment_id" => $ar['transaction_payment_id'],
                    "payment_application" => $ar['payment_application'],
                );

                $response = $this->insert_payment($_data_collection);

                if ($response) {
                    $response['status'] = 1;
                    $response['message'] = 'Transaction is now cleared.';
                } else {
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                }
            }

            echo json_encode($response);
            exit();
        }
    }

    public function bounce_transaction($id = false)
    {

        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        if ($id) {
            $bounced = ["is_active" => 2];

            $ar = $this->M_Ar_clearing->get($id);
            $status = $ar['is_active'];

            if ($status == 2) {
                $response['status'] = 0;
                $response['message'] = 'Transaction is already bounced.';
            } else if ($status == 3) {
                $response['status'] = 0;
                $response['message'] = 'Cannot bounce transaction. Transaction is already cleared.';
            } else {
                $result = $this->M_Ar_clearing->update($bounced + $additional, $id);

                if ($result) {
                    $response['status'] = 1;
                    $response['message'] = 'Transaction is now bounced.';
                } else {
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                }
            }

            echo json_encode($response);
            exit();
        }
    }

    public function insert_payment($post = array())
    {

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

        $transaction_id = $post['transaction_id'];
        $transaction = $this->M_Transaction->get($transaction_id);

        $is_waived = $post['info']['is_waived'];
        $payment_application = $post['payment_application']; // 1 = normal, 2 = balloon, 3 fully paid

        // unset($post['info']['is_waived']);
        // unset($post['info']'payment_application']);

        $info = $post['info'];

        // $off_p['entry_id'] = 0;
        $off_p['transaction_id'] = $post['transaction_id'];
        // $off_p['ar_number']     = $info['AR_number'];
        $off_p['or_number'] = $info['OR_number'];
        $off_p['receipt_type']     = $info['receipt_type'];
        $off_p['or_date'] = $info['or_date'];
        $off_p['payment_date'] = $info['payment_date'];
        $off_p['amount_paid'] = remove_commas($info['amount_paid']);
        $off_p['adhoc_payment'] = remove_commas($info['adhoc_payment']);
        $off_p['grand_total'] = remove_commas($info['grand_total']);
        $off_p['remarks'] = $info['remarks'];
        $off_p['payment_type_id'] = $info['payment_type_id'];
        $off_p['period_id'] = $info['period_id'];
        $off_p['period_count'] = $info['period_count'];
        $off_p['payment_application'] = $payment_application;
        $period_id = $info['period_id'];

        $amount_paid = $off_p['amount_paid'];

        if (empty($is_waived)) {
            $off_p['penalty_amount'] = remove_commas($info['penalty_amount']);
            $amount_paid = $off_p['amount_paid'] - $off_p['penalty_amount'];
        }

        $relative = $this->get_relative_amount($amount_paid, $off_p['transaction_id'], $off_p['period_id'], $payment_application);

        // vdebug($relative);

        $u_payments = $relative['payments'];

        if ($u_payments) {
            $off_p['principal_amount'] = $relative['principal'];
            $off_p['interest_amount'] = $relative['interest'];
            $off_p['post_dated_check_id'] = $info['post_dated_check_id'];

            $off_payment_id = $this->M_Transaction_official_payment->insert($off_p + $additional);

            $is_recognized = $transaction['is_recognized'];

            $this->transaction_library->generate_entries($off_payment_id, true, false);


            if (!empty($off_p['penalty_amount'])) {
                $this->insert_penalty_logs($off_payment_id, $transaction_id, $post);
            }

            if ($u_payments) {
                foreach ($u_payments as $key => $u_payment) {
                    $u_additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];
                    $_u_payment['is_paid'] = $u_payment['is_paid'];
                    $_u_payment['is_complete'] = $u_payment['is_complete'];
                    $this->M_Transaction_payment->update($_u_payment + $u_additional, array('id' => $u_payment['id']));

                    $off_p_update['transaction_payment_id'] = $u_payment['id'];
                    $this->M_Transaction_official_payment->update($off_p_update, array('id' => $off_payment_id));

                    $period_id = $u_payment['period_id'];
                }
            }

            if ($info['period_id'] == 1) {
                $period_id = 2;
            }

            if (($payment_application == 2) && ($relative['amount_paid'])) {
                $b_amount_paid = $relative['amount_paid'];
                $b_period_id = $info['period_id'];
                $b_transaction_payment_id = $relative['transaction_payment_id'];

                $this->balloon($b_transaction_payment_id, $relative, 1);
            }

            // do this as library
            // $_transaction_data['period_id'] = $period_id;
            // $this->M_Transaction->update($_transaction_data + $u_additional, array('id' => $off_p['transaction_id']));
            $this->transaction_library->initiate($transaction_id);

            $this->transaction_library->update_period_status();

            $_property_data['status'] = 3;
            $this->M_property->update($_property_data + $u_additional, array('id' => $transaction['property_id']));
        } else {
            $response['status'] = 0;
            $response['message'] = 'Oops! Something went wrong. Please check your payment details';

            return $response;
        }

        $this->db->trans_complete(); # Completing transaction

        /*Optional*/
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $this->db->trans_commit();
            $response['status'] = 1;
            $response['message'] = 'Payment Successfully inserted!';
        }
        return $response;
    }

    public function insert_penalty_logs($official_payment_id = 0, $transaction_id = 0, $post = array())
    {
        $info = $post['info'];

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

        $off_p['transaction_official_payment_id'] = $official_payment_id;
        $off_p['transaction_id'] = $transaction_id;
        $off_p['transaction_payment_id'] = $post['transaction_payment_id'];
        $off_p['is_waived'] = $info['is_waived'];
        $off_p['amount'] = $info['penalty_amount'];
        $off_p['days_lapsed'] = 0;

        $off_payment_id = $this->M_tpenalty_logs->insert($off_p + $additional);
        $this->db->trans_complete(); # Completing transaction

        /*Optional*/
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            $response['status'] = 0;
            $response['message'] = 'Error!';
        } else {
            # Everything is Perfect.
            # Committing data to the database.
            $this->db->trans_commit();
            $response['status'] = 1;
            $response['message'] = 'Penalty Log Successfully inserted!';
        }
        return $response;
    }

    public function get_relative_amount($amount_paid = 0, $transaction_id = 0, $period_id = 0, $payment_application = 0, $debug = 0)
    {
        // http://13.251.190.131/rems-sales/transaction_payment/get_relative_amount/100000/30/3/0/1
        // http://localhost/rems/transaction_payment/get_relative_amount/56800/46/2/2/1

        $params['transaction_id'] = $transaction_id;
        $params['period_id'] = $period_id;
        $params['is_complete'] = 0;

        $order['period_id'] = 'ASC';
        $order['id'] = 'ASC';

        $payments = $this->M_Transaction_payment->where($params)->order_by($order)->get_all();
        $return['payments'] = array();

        $no = 0;

        if ($debug == 1) {
            vdebug($payments);
        }

        if ($debug == 2) {
            vdebug($amount_paid);
        }
        $stop = 0;
        $return['principal'] = 0;
        $return['interest'] = 0;

        if ($payments) {
            foreach ($payments as $key => $payment) {

                if (($payment_application == 2) && ($stop)) { // advance payment go to balloon payment function
                    $return['principal'] = $amount_paid + $return['principal'];
                    break;
                }
                $p_id = $payment['id'];
                $stop = 1;

                $is_complete = 0;

                $interest = $payment['interest_amount'];
                $principal = $payment['principal_amount'];
                $total = $interest + $principal;

                if ($payment['is_paid'] == 1) {
                    $existing_principal = 0;
                    $existing_interest = 0;
                    $param['transaction_id'] = $transaction_id;
                    $param['transaction_payment_id'] = $payment['id'];
                    $official_payments = $this->M_Transaction_official_payment->where($param)->get_all();

                    if ($official_payments) {
                        foreach ($official_payments as $key => $official_payment) {
                            $existing_principal = $official_payment['principal_amount'] + $existing_principal;
                            $existing_interest = $official_payment['interest_amount'] + $existing_interest;
                        }
                    }

                    $principal = $payment['principal_amount'] - $existing_principal;
                    $interest = $payment['interest_amount'] - $existing_interest;
                    $total = $interest + $principal;
                    $stop = 0;
                }

                if (($amount_paid == 0) || ($amount_paid <= 0.1)) { // will change this in the future XD dammit!
                    break;
                }

                if ($interest) {
                    if (trim($amount_paid) >= trim($interest)) {
                        $return['interest'] = @$return['interest'] + $interest;
                        $amount_paid = $amount_paid - $interest;
                    } else {
                        $return['interest'] = @$return['interest'] + $amount_paid;
                        $amount_paid = 0;
                    }
                }

                if ($principal) {
                    if (trim($amount_paid) >= trim($principal)) {
                        $return['principal'] = @$return['principal'] + $principal;
                        $amount_paid = $amount_paid - $principal;
                        $is_complete = 1;
                    } else {
                        $return['principal'] = @$return['principal'] + $amount_paid;
                        $amount_paid = 0;
                    }
                }

                $return['payments'][$key]['id'] = $payment['id'];
                $return['payments'][$key]['is_paid'] = 1;
                $return['payments'][$key]['is_complete'] = $is_complete;
                $return['payments'][$key]['period_id'] = $payment['period_id'];
                $return['payments'][$key]['amount_paid'] = $amount_paid;
            }

            // echo $amount_paid;
            // echo "<br>";
            // echo $return['principal'];
            // echo "<br>";

            // if ( ($amount_paid < $return['principal']) && ($payment_application == 2) ) {
            //     $amount_paid = $return['principal'];
            //     $no = 1;
            // }

            $return['transaction_payment_id'] = $p_id;
            $return['amount_paid'] = $amount_paid;
            $return['exact'] = $no;
        }

        if ($debug == 3) {
            vdebug($amount_paid);
        }

        if ($debug == 4) {
            vdebug($principal);
        }

        if ($debug == 5) {
            vdebug($return);
        }
        return $return;
    }

    public function balloon($transaction_payment_id = 0, $relative = [], $save = 0, $debug = 0)
    {

        $u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];

        // http://localhost/rems/transaction_payment/balloon/

        $payment = $this->M_Transaction_payment->get($transaction_payment_id);

        if ($payment) {

            $transaction_id = $payment['transaction_id'];
            $this->transaction_library->initiate($transaction_id);

            $period_id = $payment['period_id'];

            $transaction = $this->M_Transaction->with_tbillings()->with_billings()->get($transaction_id);
            $billings = $transaction['billings'];

            if ($billings) {
                foreach ($billings as $key => $billing) {
                    if ($billing['period_id'] == $period_id) {
                        $terms = $billing['period_term'];
                        $interest_rate = $billing['interest_rate'];
                    }
                }
            }

            $total_beginning_balance = $this->transaction_library->get_remaining_balance(2);
            $remaining_total_beginning_balance = $total_beginning_balance - $relative['principal'];

            $beginning_balance = $payment['beginning_balance'] - $relative['principal'];
            $effectivity_date = add_months_to_date($payment['due_date'], 1);

            if (($beginning_balance == 0) || ($beginning_balance <= 0)) {
                $payment['principal_amount'] = 0;
            }

            $s_u_payment['principal_amount'] = $relative['principal'];
            $s_u_payment['ending_balance'] = $beginning_balance;
            $s_u_payment['total_amount'] = $relative['principal'] + $relative['interest'];

            // $s_u_payment['total_amount'] = $payment['principal_amount'] + $payment['interest_amount'] + $relative['amount'];

            if ($save) {
                $a = $this->M_Transaction_payment->update($s_u_payment + $u_additional, array('id' => $payment['id']));
            }

            if (($beginning_balance == 0) || ($beginning_balance <= 0)) {
                $u_payment['transaction_id'] = $transaction_id;
                $u_payment['is_paid'] = 0;
                $u_payment['period_id'] = $period_id;
                $this->M_Transaction_payment->update(array('deleted_reason' => 'balloon-payment'), $u_payment);
                $this->M_Transaction_payment->delete($u_payment);
                return true;
            }

            $tb_logs['transaction_id'] = $transaction_id;
            $tb_logs['transaction_payment_id'] = $transaction_payment_id;
            $tb_logs['period_amount'] = $beginning_balance;
            $tb_logs['period_term'] = $terms;
            $tb_logs['effectivity_date'] = $effectivity_date;
            $tb_logs['starting_balance'] = $beginning_balance;
            $tb_logs['ending_balance'] = "";
            $tb_logs['interest_rate'] = $interest_rate;
            $tb_logs['period_id'] = $period_id;
            $tb_logs['slug'] = 'balloon-payment';
            $tb_logs['remarks'] = "Balloon Payment stop at " . $payment['particulars'];
            // vdebug($a);

            if ($save) {

                // vdebug($a);

                $tb_id = $this->M_Tbilling_logs->insert($tb_logs + $additional);

                $breakdown = $this->process_mortgage($transaction_id, 'save', 'balloon-payment', $tb_id);
            }
        }

        if ($debug == 1) {
            echo $save;
            echo "<br>";
            vdebug($payment);
        }
        if ($debug == 2) {
            echo $save;
            echo "<br>";
            vdebug($breakdown);
        }
        if ($debug == 3) {
            echo $save;
            echo "<br>";
            vdebug($s_u_payment);
        }
    }

    public function get_ar_clearing_summary()
    {

        $for_clearing_count = 0;
        $cleared_count = 0;
        $bounced_count = 0;
        $for_clearing_amount = 0;
        $cleared_amount = 0;
        $bounced_amount = 0;

        if ($this->input->post()) {

            $ar_clearing = $this->M_Ar_clearing->get_all();
            // vdebug($ar_clearing);
            if ($ar_clearing) {

                foreach ($ar_clearing as $row) {
                    switch ($row['is_active']) {
                        case '1':
                            $for_clearing_count += 1;
                            $for_clearing_amount += $row['amount_paid'];
                            break;
                        case '2':
                            $bounced_count += 1;
                            $bounced_amount += $row['amount_paid'];
                            break;
                        case '3':
                            $cleared_count += 1;
                            $cleared_amount += $row['amount_paid'];
                            break;
                    }
                }
            }
        }

        $result['for_clearing_count'] = $for_clearing_count;
        $result['cleared_count'] = $cleared_count;
        $result['bounced_count'] = $bounced_count;
        $result['for_clearing_amount'] = money_php($for_clearing_amount);
        $result['cleared_amount'] = money_php($cleared_amount);
        $result['bounced_amount'] = money_php($bounced_amount);
        echo json_encode($result);
        exit();
    }
}
