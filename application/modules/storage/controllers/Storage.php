<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Storage extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('storage/Storage_model', 'M_Storage');

        // Format Helper
        $this->load->helper(['format', 'images']);
       
    }

    public function index()
    {

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function save_files () {

        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];

        $post = $this->input->post();

        if(!empty($post)) {

            $data['file_name'] = $post['file_name'];
            $data['file_path'] = $post['file_path'];
            
            $this->M_Storage->insert($data + $additional);
        }


    }
    
}
