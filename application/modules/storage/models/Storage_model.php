<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Storage_model extends MY_Model {

	public $table = 'storage'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'file_name',
		'file_path',
		'is_active',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

	}

	public function create_folder ($project_name = "", $property_name = "") {

		if($project_name !== "" && $property_name !== "") {

			$project_folder = './assets/uploads/storage/' . $project_name;
			$property_folder = $project_folder . "/" . $property_name;

			if(!is_dir($project_folder)) {
				mkdir($project_folder, 0777, true);
			}
			if(!is_dir($property_folder)) {
				
				mkdir($property_folder, 0777, true);
			}

		}


	}
	
}
