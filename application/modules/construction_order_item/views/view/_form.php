<?php
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_name" value="<?php echo set_value('item_name', $item_name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info"></i></span>
            </div>
            <?php echo form_error('item_name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_code" value="<?php echo set_value('item_code', $item_code); ?>" placeholder="Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('item_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Unit of Measurement <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="unit_of_measurement" value="<?php echo set_value('unit_of_measurement', $unit_of_measurement); ?>" placeholder="Unit of Measurement" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('unit_of_measurement'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Quantity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="quantity" value="<?php echo set_value('quantity', $quantity); ?>" placeholder="Quantity" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('quantity'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Amenity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="amenity" value="<?php echo set_value('amenity', $amenity); ?>" placeholder="Amenity" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('amenity'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Sub Amenity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="sub_amenity" value="<?php echo set_value('sub_amenity', $sub_amenity); ?>" placeholder="Sub Amenity" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('sub_amenity'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>