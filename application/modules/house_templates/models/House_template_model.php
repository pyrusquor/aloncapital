<?php
defined('BASEPATH') or exit('No direct script access allowed');
class House_template_model extends MY_Model
{
    public $table = 'house_templates'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'id',
        'name',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'deleted_by'
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert']    =    $this->fields;
        $this->rules['update']    =    $this->fields;

        $this->has_many['house_template_items'] = array('foreign_model' => 'House_template_item_model', 'foreign_table' => 'house_template_items', 'foreign_key' => 'house_template_id', 'local_key' => 'id');
    }
}
