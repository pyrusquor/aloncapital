<?php
defined('BASEPATH') or exit('No direct script access allowed');

class House_templates extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('House_template_model', 'M_house_template');
        $this->load->model('House_template_item_model', 'M_house_template_item');

        $this->_table_fillables = $this->M_house_template->fillable;
        $this->_table_columns = $this->M_house_template->__get_columns();
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'name' => true,
            'items' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);
        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_house_template
                ->with_house_template_items('fields:*count*')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("name like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'name' => [
                    'data' => $filters['name'] ?? null,
                    'operator' => 'like',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {
                        $items[$key]['items'] = $value['house_template_items'][0]['counted_rows'] ?? 0;

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_house_template->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {
        if ($id) {

            if ($this->input->post()) {

                $additional = [
                    'updated_by' => $this->user->id,
                ];

                $post = $this->input->post();
                $info = $post['info'];
                $items = $post['house_template_items'];

                $house_template = $this->M_house_template
                    ->with_house_template_items('fields:id')
                    ->get($id);

                $this->db->trans_begin();

                $this->M_house_template->update($info + $additional, $id);

                $currentItems = $house_template['house_template_items'];

                foreach ($currentItems as $item) {

                    $this->M_house_template_item->delete($item['id']);
                }

                foreach ($items as $item) {

                    $this->createHouseTemplateItem($id, $item);
                }

                if ($this->db->trans_status() === FALSE) {

                    $this->db->trans_rollback();
                    $this->notify->error('Oops something went wrong.');
                } else {

                    $this->db->trans_commit();
                    $this->notify->success('House model template successfully updated.', 'house_templates');
                }
            } else {

                $this->view_data['house_template'] = $this->M_house_template
                    ->with_house_template_items([
                        'fields' => 'id,count',
                        'with' => [
                            [
                                'relation' => 'item',
                                'fields' => 'name'
                            ],
                        ]
                    ])
                    ->get($id);

                $this->template->build('form', $this->view_data);
            }
        } else {

            if ($this->input->post()) {

                $additional = [
                    'created_by' => $this->user->id,
                    'created_at' => NOW
                ];

                $post = $this->input->post();
                $info = $post['info'];
                $items = $post['house_template_items'];

                $this->db->trans_begin();

                $house_template_id = $this->M_house_template->insert($info + $additional);

                foreach ($items as $item) {

                    $this->createHouseTemplateItem($house_template_id, $item);
                }

                if ($this->db->trans_status() === FALSE) {

                    $this->db->trans_rollback();
                    $this->notify->error('Oops something went wrong.');
                } else {

                    $this->db->trans_commit();
                    $this->notify->success('House model template successfully created.', 'house_templates');
                }
            }

            $this->template->build('form', $this->view_data);
        }
    }

    public function createHouseTemplateItem($house_template_id, $data)
    {
        $additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW
        ];

        $this->M_house_template_item->insert(
            ['house_template_id' => $house_template_id] + $data + $additional
        );
    }

    public function view($id = false)
    {
        if ($id) {

            $this->view_data['house_template'] = $this->M_house_template
                ->with_house_template_items([
                    'fields' => 'id,count',
                    'with' => [
                        [
                            'relation' => 'item',
                            'fields' => 'name'
                        ],
                    ]
                ])
                ->get($id);

            if ($this->view_data['house_template']) {

                $this->template->build('view', $this->view_data);
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_house_template->get($id);
            if ($list) {

                $deleted = $this->M_house_template->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Class Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');
            if ($delete_ids) {
                foreach ($delete_ids as $value) {
                    $data = [
                        'deleted_by' => $this->session->userdata['user_id']
                    ];
                    $this->db->update('item_group', $data, array('id' => $value));
                    $deleted = $this->M_house_template->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Item Type Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
}
