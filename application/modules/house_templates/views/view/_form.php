<?php

$name = $house_template['name'] ?? null;

$items = $house_template['house_template_items'] ?? null;

?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="info[name]" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off" required>
                <?php echo form_error('name'); ?>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sticky-note"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div id="form_repeater">
            <label>Items:
                <div id="add_seller_btn" data-repeater-create="" class="btn btn-sm btn-primary">
                    <span>
                        <i class="la la-plus"></i>
                        <span>Add</span>
                    </span>
                </div>
            </label>

            <?php if ($items) : ?>
                <div data-repeater-list="house_template_items" class="col-lg-12">
                    <?php foreach ($items as $item) : ?>
                        <div data-repeater-item class="row kt-margin-b-10 seller_row">
                            <div class="col-md-3">
                                <div class="kt-form__label">
                                    <label class="kt-label m-label--single">Item:</label>
                                </div>
                                <select name="item_id" class="form-control suggests" data-module="item">
                                    <option value="<?= $item['item']['id'] ?>"><?= $item['item']['name'] ?></option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="kt-form__label">
                                    <label class="kt-label m-label--single">Quantity:</label>
                                </div>
                                <input type="number" class="form-control" name="count" value="<?= $item['count'] ?>" placeholder="Quantity">
                            </div>
                            <div class="col-md-1 pt-2">
                                <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon mt-4">
                                    <i class="la la-remove"></i>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else : ?>
                <div data-repeater-list="house_template_items" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10 seller_row">
                        <div class="col-md-3">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Item:</label>
                            </div>
                            <select name="item_id" class="form-control suggests" data-module="item"></select>
                        </div>
                        <div class="col-md-3">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Quantity:</label>
                            </div>
                            <input type="number" class="form-control" name="count" value="<?php echo $sellers_rate; ?>" placeholder="Quantity">
                        </div>
                        <div class="col-md-1 pt-2">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon mt-4">
                                <i class="la la-remove"></i>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>