<div class="row">








    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Path <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                

                
                
                
                
                
                
                
                    <input type="file" class="form-control" id="path" name="path" placeholder="Path" v-model="info.form.path">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>File Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                

                
                
                
                
                
                
                
                    <input type="file" class="form-control" id="filename" name="filename" placeholder="File Name" v-model="info.form.filename">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Object Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                

                
                
                
                
                
                
                    <input type="text" class="form-control" id="object_type" name="object_type" placeholder="Object Type" v-model="info.form.object_type">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Object Type ID <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            
                

                
                    <input type="number" min="0" step="0.01" class="form-control" id="object_type_id" name="object_type_id" placeholder="Object Type ID" v-model="info.form.object_type_id">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>            
                
                
                
                
                
                
                
            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

</div>