<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Document_attachment_model extends MY_Model
    {
        public $table = 'document_attachments'; // you MUST mention the table name
        public $primary_key = 'id';
        public $fillable = [
            "id",
            "created_by",
            "created_at",
            "updated_by",
            "updated_at",
            "deleted_by",
            "deleted_at",
            "path",
            "filename",
            "object_type",
            "object_type_id",

        ];
        public $form_fillables = [
            "id",


            "path",
            "filename",
            "object_type",
            "object_type_id",

        ];
        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [


            "path" => array(
                "field" => "path",
                "label" => "Path",
                "rules" => ""
            ),

            "filename" => array(
                "field" => "filename",
                "label" => "File Name",
                "rules" => ""
            ),

            "object_type" => array(
                "field" => "object_type",
                "label" => "Object Type",
                "rules" => "required"
            ),

            "object_type_id" => array(
                "field" => "object_type_id",
                "label" => "Object Type ID",
                "rules" => "numeric|required"
            ),

        ];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = true;
            $this->return_as = 'array';

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;


        }

        function get_columns()
        {
            $_return = FALSE;

            if ($this->fillable) {
                $_return = $this->fillable;
            }

            return $_return;
        }

        public function insert_dummy()
        {
            require APPPATH . '/third_party/faker/autoload.php';
            $faker = Faker\Factory::create();

            $data = [];

            for ($x = 0; $x < 10; $x++) {
                array_push($data, array(
                    'name' => $faker->word,
                ));
            }
            $this->db->insert_batch($this->table, $data);

        }

    }