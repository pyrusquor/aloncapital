<?php
$id = isset($transaction_loan_document_stage_logs['id']) && $transaction_loan_document_stage_logs['id'] ? $transaction_loan_document_stage_logs['id'] : '';

$transaction_id = isset($transaction_loan_document_stage_logs['transaction_id']) && $transaction_loan_document_stage_logs['transaction_id'] ? $transaction_loan_document_stage_logs['transaction_id'] : '';

$previous_id = isset($transaction_loan_document_stage_logs['previous_id']) && $transaction_loan_document_stage_logs['previous_id'] ? $transaction_loan_document_stage_logs['previous_id'] : '';

$new_id = isset($transaction_loan_document_stage_logs['new_id']) && $transaction_loan_document_stage_logs['new_id'] ? $transaction_loan_document_stage_logs['new_id'] : '';

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Warehouse</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('warehouse/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('warehouse');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Transaction Reference
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $transaction_id; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Previous Stage
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $previous_id; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        New Stage
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $new_id; ?></span>
                                </div>
                                <!-- <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Status
                                    </span>
                                    <span class="kt-widget__transaction_loan_document_stage_logs" style="font-weight: 500"><?php echo Dropdown::get_static('warehouse_status', $is_active, 'view'); ?></span>
                                </div> -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
