<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ticketing_model extends MY_Model {

	public $table = 'transaction_ticketing'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
        'remarks',
        'category',
        'past_due',
        'past_due_status',
        'received_date',
        'sent_date',
        'letter_request',
        'letter_request_name',
        'letter_request_status',
        'date_requested',
        'date_approved',
        'date_disapproved',
        'date_cancelled',
        'assignatory',
        'is_viewed',
        'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [
		'transaction_id' => array(
			'field'=>'transaction_id',
			'label'=>'Transaction ID',
			'rules'=>'trim'
		),
		'remarks' => array(
			'field'=>'remarks',
			'label'=>'Remarks',
			'rules'=>'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		$this->has_one['transaction'] = array('foreign_model'=>'Transaction_model','foreign_table'=>'transactions','foreign_key'=>'id','local_key'=>'transaction_id');

	}

}