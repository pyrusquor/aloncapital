<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ticketing extends MY_Controller
{

	public $fields = [
		'transaction_id' => array(
			'field'=>'transaction_id',
			'label'=>'Transaction ID',
			'rules'=>'trim'
		),
		'category' => array(
			'field'=>'category',
			'label'=>'Category',
			'rules'=>'trim'
		),
		'remarks' => array(
			'field'=>'remarks',
			'label'=>'Remarks',
			'rules'=>'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();
		
		$transaction_models = array('transaction/Transaction_model' => 'M_Transaction','transaction/Transaction_payment_model' => 'M_Transaction_payment',);

		// Load models
		$this->load->model('ticketing/Ticketing_model', 'M_ticketing');
		// $this->load->model('transaction/Transaction_model', 'M_transaction');
		$this->load->model('auth/Ion_auth_model', 'M_auth');
		$this->load->model('user/User_model', 'M_user');
		$this->load->model($transaction_models);

		// Load pagination library 
		$this->load->library('ajax_pagination');
		
		// Per page limit 
		$this->perPage = 10;

		$this->_table_fillables		=	$this->M_ticketing->fillable;
		$this->_table_columns		=	$this->M_ticketing->__get_columns();
	}

	public function index()
	{
        $_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns']		=	$this->__get_columns($_fills);

		$this->view_data['transaction'] = $this->M_Transaction->as_array()->get_all();

        // get all raw data
		$alldata = $this->M_ticketing->order_by('id','DESC')->get_all();

		$this->view_data['totalRec'] = $totalRec = $this->M_ticketing->count_rows();
		
		// print_r($view_data); exit;

		// Pagination configuration 
		$config['target']      = '#ticketingContent';
		$config['base_url']    = base_url('ticketing/paginationData');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'TicketingPagination';

		// Initialize pagination library 
		$this->ajax_pagination->initialize($config);

		// $this->view_data['record'] = $alldata;

		$this->view_data['records'] = $this->M_ticketing->with_transaction('fields: reference')->limit($this->perPage, 0)->get_all();

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function paginationData()
	{
		if ($this->input->is_ajax_request()) {

			// Input from General Search
			$keyword = $this->input->post('keyword');

			$page = $this->input->post('page');

			if (!$page) {
				$offset = 0;
			} else {
				$offset = $page;
			}

			$this->view_data['totalRec'] = $totalRec = $this->M_ticketing->count_rows();
			// Pagination configuration 
			$config['target']      = '#ticketingContent';
			$config['base_url']    = base_url('ticketing/paginationData');
			$config['total_rows']  = $totalRec;
			$config['per_page']    = $this->perPage;
			$config['link_func']   = 'TicketingPagination';

			// Query
			if ( ! empty($keyword) ):
				$this->db->group_start();
					$this->db->like('name', $keyword, 'both');
					$this->db->or_like('date', $keyword, 'both');
				$this->db->group_end();
			endif;

			$totalRec = $this->M_ticketing->count_rows();

			// Pagination configuration 
			$config['total_rows']  = $totalRec;

			// Initialize pagination library 
			$this->ajax_pagination->initialize($config);

			// Query
			if ( ! empty($keyword) ):
				$this->db->group_start();
					$this->db->like('name', $keyword, 'both');
					$this->db->or_like('date', $keyword, 'both');
				$this->db->group_end();
			endif;

			$this->view_data['records'] = $this->M_ticketing->with_transaction('fields: reference')->limit($this->perPage, $offset)->get_all();
			
			// $this->view_data['records'] = $records = $this->M_ticketing->fields(array('id', 'transaction_id', 'name',  'date', 'remarks'))
			// 	->with_transaction('fields: reference')
			// 	->limit($this->perPage, $offset)
			// 	->get_all();

			
			$this->load->view('ticketing/_filter', $this->view_data, false);
		}	
	}
	

	public function view($id = FALSE, $transaction_id = 0, $type = '')
	{

		if ($id) {

			$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
			$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);
		
			$this->view_data['ticketing'] = $this->M_ticketing->with_transaction('fields:reference')->get($id);

			if ($this->view_data['ticketing']) {
				$this->template->build('view', $this->view_data);
			} else {
				show_404();
			}

		} else if ($transaction_id) {
				
			$p['transaction_id'] = $transaction_id;
			
			if ($type) { $p['category'] = $type; }

			$this->view_data['ticketing'] = $this->M_ticketing->with_transaction('fields:reference')->order_by('id','DESC')->where($p)->get_all();

			
			$return['html'] = $this->load->view('ticketing/table', $this->view_data, true);

			echo json_encode($return);

		} else {
		
			show_404();
		}
		
	}

	public function form($transaction_id = 0, $id = 0, $type = '')
	{
		$method = "Create";
		if ( $id ){ $method = "Update"; }

		if ($this->input->post()) {

			$response['status']	= 0;
			$response['message'] = 'Oops! Please refresh the page and try again.';

			$this->form_validation->set_rules($this->fields);

			if ($this->form_validation->run() === TRUE) {

				$post = $this->input->post();

				// vdebug($post);

				if ($id) {
					$additional = [
						'updated_by' => $this->user->id,
						'updated_at' => NOW
					];
				} else {
					$additional = [
						'created_by' => $this->user->id,
						'created_at' => NOW
					];
				}

				$this->db->trans_start(); # Starting Transaction
				$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

				$info = $post;
				
				if ($id) {

					$this->M_ticketing->update($info + $additional, $id);

				} else {

					$this->M_ticketing->insert($info + $additional);

				}

				/*Optional*/
				if ($this->db->trans_status() === FALSE) {
				    # Something went wrong.
				    $this->db->trans_rollback();
				    $response['status']	= 0;
					$response['message'] = 'Error!';
				} 
				else {
				    # Everything is Perfect. 
				    # Committing data to the database.
				    $this->db->trans_commit();
				    $response['status']	= 1;
					$response['message'] = 'Ticketing Successfully '.$method.'d!';

					if(!$this->input->is_ajax_request()){
						redirect('ticketing');
					}
				}

			} else {
				$response['status']	 =	0;
				$response['message'] = validation_errors();
			}

			echo json_encode($response);
			exit();
		}

		// $this->view_data['projects'] = $this->M_project->as_array()->get_all();
		
		if ($transaction_id) {

			$this->view_data['transaction_id'] = $transaction_id;

			$this->view_data['transaction'] =  $this->M_Transaction->get($transaction_id);

			$this->view_data['method'] = $method;

			$this->template->build('form/form', $this->view_data);

		} else {
			redirect('ticketing');
		}

		
	}

	public function delete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_ticketing->get($id);
			if ($list) {

				$deleted = $this->M_ticketing->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Ticket Successfully Deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function bulkDelete()
	{	
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if($this->input->is_ajax_request())
		{
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {

				foreach ($delete_ids as $value) {

					$deleted = $this->M_ticketing->delete($value);

				}
				
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Ticket Successfully Deleted';
				}
			}
		}
		
		echo json_encode($response);
		exit();
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];
		$_extra_datas		=	[];
		$_adatas			=	[];

		$_titles[]	=	'#';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		// $tickets	=	$this->M_transaction->as_array()->get_all();
		$tickets = $this->M_ticketing->as_array()
			->get_all();

		if ($tickets) {

			foreach ($tickets as $skey => $transaction) {

				$_datas[$transaction['id']]['#']	=	$_no;

				$_no++;
			}


			$_filename	=	'list_of_tickets' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				$_additional_column	=	$this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {


						if ((strpos($_name, 'source') !== FALSE) or (strpos($_name, 'reference') !== FALSE) or (strpos($_name, 'academic') !== FALSE)) {

							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($tickets as $skey => $transaction) {

								$_extra_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
							}
						} elseif ((strpos($_name, 'work_experience') !== FALSE)) {
							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? $_name : '';

							foreach ($tickets as $skey => $transaction) {

								$_extra_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
							}
						} else {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($tickets as $skey => $transaction) {

								if ( $_column === 'transaction_id' ) {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? $transaction[$_column] : '';

								}  elseif ( $_column === 'date' ){

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] && (strtotime($transaction[$_column]) > 0) ? date_format(date_create($transaction[$_column]), 'm/d/Y') : '';

								} elseif ( $_column === 'remarks' ){

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? $transaction[$_column] : '';

								}
								 elseif ( $_column === 'name' ){

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? $transaction[$_column] : '';

								}else{
									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
								}

							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title	=	ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('Ticketing Lists');
				$_objSheet->setCellValue('A1', 'LIST OF TICKETS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($tickets as $key => $transaction) {

					$transaction_id	=	isset($transaction['id']) && $transaction['id'] ? $transaction['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title	=	ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$transaction_id][$_xls_columns[$_alpha]]) && $_datas[$transaction_id][$_xls_columns[$_alpha]] ? $_datas[$transaction_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns	=	$_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							if($_a_column === 'contact') {

								$ad_title	=	'Contact Informations';
							} elseif($_a_column === 'reference') {

								$ad_title	=	'References';
							} elseif($_a_column === 'source') {

								$ad_title	=	'Source of Informations';
							} elseif($_a_column === 'academic'){

								$ad_title	=	'Academic History';
							}else {

								$ad_title = $_a_column;
							}
							
							$a_title	=	ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if($key === 'to_report' OR $key === 'to_attend' OR $key === 'commission_based' OR $key === 'is_member'){
												$xa_value	=	isset($_extra_datas[$transaction_id][$_a_column][$key]) && ($_extra_datas[$transaction_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$transaction_id][$_a_column][$key], 'view') : '';
											}else{
												$xa_value	=	$_extra_datas[$transaction_id][$_a_column][$key];
											}

											$xa_titles =	isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$transaction_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$transaction_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					} 
					
					$_start += 1;

				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}
		} else {

			$this->notify->error('No Record Found', 'transaction');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Transaction CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_ticketing->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Ticketing');
			$_objSheet->setCellValue('A1', 'SELLERS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import () {

		if ( isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name'] ) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if ( TRUE ) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_failed_reasons = [];
				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch ( Exception $e ) {

					$_msg	=	'Error loading CSV "'.pathinfo($_name, PATHINFO_BASENAME).'": '.$e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ( $_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2] ) {

					if ( $_sheetData[1][0] === 'id' ) {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if ( !$_fillables ) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
					}

					foreach ( $_fillables as $_fkey => $_fill ) {

						if ( in_array($_fill, $_sheetData[1]) ) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ( $_sheetData as $_skey => $_sd ) {

						if ( $_skey > 1 ) {

							if ( count($_columns) == count($_sd) ) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					function password_format($last_name, $birth_date)
					{
						// Seller Password Format
						// Password : Lastname+Birthdate = Hernandez10131993

						$formatLname = ucfirst(cleaner($last_name, 'username')); //Lastname
						$formatBdate = date("mdY", strtotime($birth_date)); //Birthdate
						$sellerPwd = $formatLname . $formatBdate;
								
						return $sellerPwd;
					}

					if ( isset($_datas) && $_datas ) {

						foreach ( $_datas as $_dkey => $_data ) {
							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							$_data['birth_date']	=	isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
							$transactionGroupID = ['3'];

							if ( $_id ) {
								$data	=	$this->M_ticketing->get($_id);
								if ( $data ) {

									unset($_data['id']);

									$oldPwd = password_format($data['last_name'], $data['birth_date']);
									$newPwd = password_format($_data['last_name'], $_data['birth_date']);

									$oldEmail = $data['email'];
									$newEmail = $_data['email'];

									if( $this->M_auth->email_check($oldEmail) ){

										if ($oldPwd !== $newPwd) {
											// Update User Password
											$this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
										}

										if ($oldEmail !== $newEmail) {
											// Update User Email
											$_user_data = [
												'email' => $newEmail,
												'username' => $newEmail,
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											];
											$this->M_user->update($_user_data, array('id' => $data['user_id']));
										}

										// Update Transaction Info
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_update	=	$this->M_ticketing->update($_data, $_id);
										if ( $_update !== FALSE ) {

											$user_data = array(
												'first_name' => $_data['first_name'],
												'last_name' => $_data['last_name'],
												'active' => $_data['is_active'],
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											);
	
											$this->M_user->update($user_data, $data['user_id']);

											$_updated++;
										} else {

											$_failed_reasons[$_data['id']][] = 'update not working.';
											$_failed++;

											break;
										}

									} else {

										$_failed_reasons[$_data['id']][] = 'email already used';
										$_failed++;

										break;
									}
									
								} else {

									// Generate user password
									$transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
									$transactionEmail = $_data['email'];
									
									if( !$this->M_auth->email_check($_data['email']) ){

										$user_data = array(
											'first_name' => $_data['first_name'],
											'last_name' => $_data['last_name'],
											'active' => $_data['is_active'],
											'created_by' => $this->user->id,
											'created_at' => NOW
										);
										
										$userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

										if ($userID !== FALSE) {

											$_data['user_id']	= $userID;
											$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['created_at']	=	NOW;
											$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['updated_at']	=	NOW;

											$_insert	=	$this->M_Transaction_commission->insert($_data);
											if ( $_insert !== FALSE ) {

												$_inserted++;
											} else {

												$_failed_reasons[$_data['id']][] = 'insert transaction not working';
												$_failed++;

												break;
											}

										} else {

											$_failed_reasons[$_data['id']][] = 'insert ion auth not working';
											$_failed++;

											break;
										}

									} else {

										$_failed_reasons[$_data['id']][] = 'email check already existing';
										$_failed++;

										break;
									}
								}
							} else {

								// Generate user password
								$transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
								$transactionEmail = $_data['email'];
								
								if( !$this->M_auth->email_check($_data['email']) ){

									$user_data = array(
										'first_name' => $_data['first_name'],
										'last_name' => $_data['last_name'],
										'active' => $_data['is_active'],
										'created_by' => $this->user->id,
										'created_at' => NOW
									);
									
									$userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

									if ($userID !== FALSE) {

										$_data['user_id']	= $userID;
										$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['created_at']	=	NOW;
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_insert	=	$this->M_ticketing->insert($_data);
										if ( $_insert !== FALSE ) {

											$_inserted++;
										} else {

											$_failed_reasons[$_dkey][] = 'insert transaction not working';

											$_failed++;

											break;
										}

									} else {

										$_failed_reasons[$_dkey][] = 'insert ion auth not working';

										$_failed++;

										break;
									}

								} else {

									$_failed_reasons[$_dkey][] = 'email check already existing';

									$_failed++;

									break;

								}
							}
						}

						$_msg	=	'';
						if ( $_inserted > 0 ) {

							$_msg	=	$_inserted.' record/s was successfuly inserted';
						}

						if ( $_updated > 0 ) {

							$_msg	.=	($_inserted ? ' and ' : '').$_updated.' record/s was successfuly updated';
						}

						if ( $_failed > 0 ) {
							$this->notify->error('Upload Failed! Please follow upload guide. ', 'transaction');
						} else {

							$this->notify->success($_msg.'.', 'transaction');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'transaction');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'transaction');
			}
		} else {

			$this->notify->error('Something went wrong!', 'transaction');
		}
	}

	public function update_ticket_status($transaction_id) {
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if($transaction_id) {

			// Get all tickets with this transaction_id
			$tickets = $this->M_ticketing->as_array()->get_all(["transaction_id" => $transaction_id]);
	
			// Filter tickets that are not viewed yet
			$filtered_tickets = array_filter($tickets, function($t) {
				return $t['is_viewed'] !== 1;
			});
	
			// Loop though filtered tickets 
			foreach ($filtered_tickets as $key => $ticket) {
	
				// Update ticket status
				$result = $this->M_ticketing->update(["is_viewed" => 1], $ticket['id']);
	
			};
	
			$response['status']	= 1;
			$response['message'] = 'Ticket status successfully updated.';
	
			echo json_encode($response);
			
		}
	}
    
}
