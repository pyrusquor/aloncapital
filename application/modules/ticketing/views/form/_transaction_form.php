<?php
    // print_r($records); exit;
    $id             = isset($ticketing['id']) && $ticketing['id'] ? $ticketing['id'] : '';
    $remarks        = isset($ticketing['remarks']) && $ticketing['remarks'] ? $ticketing['remarks'] : '';
    $category       = isset($ticketing['category']) && $ticketing['category'] ? $ticketing['category'] : '';

    $past_due       = isset($ticketing['past_due']) && $ticketing['past_due'] ? $ticketing['past_due'] : '';
    $past_due_status       = isset($ticketing['past_due_status']) && $ticketing['past_due_status'] ? $ticketing['past_due_status'] : '';
    $received_date       = isset($ticketing['received_date']) && $ticketing['received_date'] ? $ticketing['received_date'] : '';
    $sent_date       = isset($ticketing['sent_date']) && $ticketing['sent_date'] ? $ticketing['sent_date'] : '';
    $letter_request       = isset($ticketing['letter_request']) && $ticketing['letter_request'] ? $ticketing['letter_request'] : '';
    $letter_request_name       = isset($ticketing['letter_request_name']) && $ticketing['letter_request_name'] ? $ticketing['letter_request_name'] : '';
    $letter_request_status       = isset($ticketing['letter_request_status']) && $ticketing['letter_request_status'] ? $ticketing['letter_request_status'] : '';
    $date_requested       = isset($ticketing['date_requested']) && $ticketing['date_requested'] ? $ticketing['date_requested'] : '';
    $date_approved       = isset($ticketing['date_approved']) && $ticketing['date_approved'] ? $ticketing['date_approved'] : '';
    $date_disapproved       = isset($ticketing['date_disapproved']) && $ticketing['date_disapproved'] ? $ticketing['date_disapproved'] : '';
    $date_cancelled       = isset($ticketing['date_cancelled']) && $ticketing['date_cancelled'] ? $ticketing['date_cancelled'] : '';
    $assignatory       = isset($ticketing['assignatory']) && $ticketing['assignatory'] ? $ticketing['assignatory'] : '';

    $transaction_id = isset($transaction['id']) && $transaction['id'] ? $transaction['id'] : '';
    $reference      = isset($transaction['reference']) && $transaction['reference'] ? $transaction['reference'] : '';
?>

<div class="row">

    <div class="col-md-6">

        <div class="row">
            <div class="col-md-12">
                <input type="hidden" id="transaction_id" name="transaction_id" value="<?php echo $transaction_id;?>">
                <div class="form-group">
                    <label>Reference # <span class="kt-font-danger">*</span></label>
                    <select class="form-control" id="reference" name="">
                        <?php if ($reference): ?>
                            <option selected><?php echo $reference; ?></option>
                        <?php endif ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Category<span class="kt-font-danger">*</span></label>
                    <?php echo form_dropdown('category', Dropdown::get_static('ticket_categories'), set_value('category', @$category), 'class="form-control" id="category"'); ?>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Remarks </label>
                    <textarea class="form-control" name="remarks" placeholder="Remarks"
                        autocomplete="off"><?php echo set_value('remarks', $remarks); ?></textarea>

                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

    </div>


    <div class="col-md-6">

        <div id="past_due_form" style="display: none">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Past Due Notice<span class="kt-font-danger">*</span></label>
                        <?php echo form_dropdown('past_due', Dropdown::get_static('past_dues'), set_value('past_due', @$past_dues), 'class="form-control" id="past_due"'); ?>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Past Due Status<span class="kt-font-danger">*</span></label>
                        <?php echo form_dropdown('past_due_status', Dropdown::get_static('past_due_status'), set_value('past_due_status', @$past_due_status), 'class="form-control" id="past_due_status"'); ?>
                    </div>
                </div>
            </div>   

            <div class="row pds" id="pd_1"   style="display: none">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Date Sent to Client <code>(yyyy-mm-dd)</code></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <div class="kt-input-icon  kt-input-icon--left">
                                <input type="text" class="form-control kt_datepicker datePicker" name="sent_date" value="<?php echo set_value('date', $sent_date); ?>"
                                    placeholder="Date" autocomplete="off" readonly>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pds" id="pd_2" style="display: none">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Received Date by the Company <code>(yyyy-mm-dd)</code></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <div class="kt-input-icon  kt-input-icon--left">
                                <input type="text" class="form-control kt_datepicker datePicker" name="received_date" value="<?php echo set_value('received_date', $received_date); ?>"
                                    placeholder="Date" autocomplete="off" readonly>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>

        <div id="letter_request_form" style="display: none">
        
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Letter of Request<span class="kt-font-danger">*</span></label>
                        <?php echo form_dropdown('letter_request', Dropdown::get_static('letter_requests'), set_value('letter_request', @$letter_requests), 'class="form-control" id="letter_request"'); ?>
                    </div>
                </div>
            </div>
            

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>AR Letter Number <span class="kt-font-danger">*</span></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control" name="letter_request_name" value="<?php echo set_value('letter_request_name', $letter_request_name); ?>"
                                placeholder="AR Letter Number " autocomplete="off">
                            <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square"></i></span>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Letter of Request Status<span class="kt-font-danger">*</span></label>
                        <?php echo form_dropdown('letter_request_status', Dropdown::get_static('letter_request_status'), set_value('letter_request_status', @$letter_request_status), 'class="form-control" id="letter_request_status"'); ?>
                    </div>
                </div>
            </div>        

            <div class="row ars" id="ar_1" style="display: none">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Date Requested by the Client <code>(yyyy-mm-dd)</code></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <div class="kt-input-icon  kt-input-icon--left">
                                <input type="text" class="form-control kt_datepicker datePicker" name="date_requested" value="<?php echo set_value('date', $date_requested); ?>"
                                    placeholder="Date" autocomplete="off" readonly>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row ars" id="ar_2" style="display: none">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Date Approved by the Company <code>(yyyy-mm-dd)</code></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <div class="kt-input-icon  kt-input-icon--left">
                                <input type="text" class="form-control kt_datepicker datePicker" name="date_approved" value="<?php echo set_value('date', $date_approved); ?>"
                                    placeholder="Date" autocomplete="off" readonly>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row ars" id="ar_3" style="display: none">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Date Disapproved by the Company <code>(yyyy-mm-dd)</code></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <div class="kt-input-icon  kt-input-icon--left">
                                <input type="text" class="form-control kt_datepicker datePicker" name="date_disapproved" value="<?php echo set_value('date', $date_disapproved); ?>"
                                    placeholder="Date" autocomplete="off" readonly>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row ars" id="ar_4" style="display: none">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Date Cancelled by the Company <code>(yyyy-mm-dd)</code></label>
                        <div class="kt-input-icon kt-input-icon--left">
                            <div class="kt-input-icon  kt-input-icon--left">
                                <input type="text" class="form-control kt_datepicker datePicker" name="date_cancelled" value="<?php echo set_value('date', $date_cancelled); ?>"
                                    placeholder="Date" autocomplete="off" readonly>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar-check-o"></i></span></span>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Assignatory<span class="kt-font-danger">*</span></label>
                        <select class="suggests form-control" name="assignatory" id="assignatory" data-module="staff" data-type="person">
                            <option value="0">SELECT STAFF</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>





