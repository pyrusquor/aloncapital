<!--begin: Basic Property Info-->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
    <?php $this->load->view('_transaction_form'); ?>
</div>

<div class="kt-form__actions">
    <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        value="Submit">Submit</button>
</div>

<!--begin: Form Actions -->
<!-- <div class="kt-form__actions">
    <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-submit">
        Submit
    </div>
</div> -->