<?php
	// echo "<pre>";
	// print_r($ticketing);
	// echo "</pre>";
?>


<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">AR Note Logs</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body">
	
	
	<div class="row">
		<div class="col-md-12">


			<div class="kt-notes">
				<div class="kt-notes__items">
					

					<?php if ($ticketing): ?>
						<?php foreach ($ticketing as $key => $ticket): ?>
							<div class="kt-notes__item">
						<div class="kt-notes__media">
							<span class="kt-notes__icon">
								<i class="flaticon-notes kt-font-brand"></i>
							</span>
						</div>
						<div class="kt-notes__content">
							<div class="kt-notes__section">
								<div class="kt-notes__info">
									<a href="#" class="kt-notes__title">
										Charles Hernandez
									</a>
									<span class="kt-notes__desc">
										<?=view_hrdate($ticket['created_at']);?>
									</span>
									<span class="kt-badge kt-badge--brand kt-badge--inline">important</span>
								</div>
							</div>
							<span class="kt-notes__body">
								<?php echo $ticket['remarks']; ?>
							</span>
						</div>
					</div>
						<?php endforeach ?>

						<?php else: ?>
							<p class="font-italic text-center">No Logs found</p>
					<?php endif ?>
				


				</div>
			</div>



		</div>
	</div>	

	
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>



