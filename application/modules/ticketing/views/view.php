<?php
	// print_r($records); exit;
    $id                 =    isset($ticketing['id']) && $ticketing['id'] ? $ticketing['id'] : '';
	// $transaction_id     =    isset($ticketing['transaction_id']) && $ticketing['transaction_id'] ? $ticketing['transaction_id'] : '';
	$transaction_id     =    isset($ticketing['transaction']['reference']) && $ticketing['transaction']['reference'] ? $ticketing['transaction']['reference'] : '';
    $date               =    isset($ticketing['created_at']) && $ticketing['created_at'] ? $ticketing['created_at'] : '';
	$category			=	isset($ticketing['category']) && $ticketing['category'] ? $ticketing['category'] : '';
    $remarks            =    isset($ticketing['remarks']) && $ticketing['remarks'] ? $ticketing['remarks'] : '';
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Transaction Information</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo site_url('ticketing/update/'.$id); ?>" class="btn btn-label-success btn-elevate btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<a href="<?php echo site_url('ticketing');  ?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-6">

			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__body">

					<!--begin::Form-->
					<div class="kt-widget4">
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Reference #
							</span>
							<span class="kt-widget4__text kt-widget4__text--bold">
								<a href="#" class="class="btn btn-label-instagram btn-sm btn-elevate""><span class="kt-widget__data" style="font-weight: 500; font-size: 18px;"><?php echo $transaction_id; ?></span></a>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc kt-align-right">
								Date Created
							</span>
							<span class="kt-widget4__text">
								<span class="kt-widget__data" style="font-weight: 500"><?php echo $date; ?></span>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Remarks
							</span>
							<span class="kt-widget4__text kt-widget4__text--bold">
								<span class="kt-widget__data" style="font-weight: 500"><?php echo $remarks; ?></span>
							</span>
						</div>
						<div class="kt-widget4__item">
							<span class="kt-widget4__desc">
								Category
							</span>
							<span class="kt-widget4__text kt-widget4__text--bold">
								<span class="kt-widget__data" style="font-weight: 500">
								<?php 
									$category_array = Dropdown::get_static('ticket_categories');
									echo $category_array[$category];
								?>
								</span>
							</span>
						</div>
					</div>
					<!--end::Form-->
				</div>
			</div>
			<!--end::Portlet-->

		</div>
	</div>
</div>
<!-- begin:: Footer -->