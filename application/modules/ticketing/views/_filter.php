    <!--begin:: Portlet-->
<?php if (!empty($records)) : ?>
    <div class="row" id="transactionList">
        <?php foreach ($records as $r):

            $id                 =    isset($r['id']) && $r['id'] ? $r['id'] : '';
            $transaction_id     =    isset($r['transaction']['reference']) && $r['transaction']['reference'] ? $r['transaction']['reference'] : '';

            $category           =    isset($r['category']) && $r['category'] ? Dropdown::get_static('ticket_categories',$r['category']) : '';

            $cat           =    isset($r['category']) && $r['category'] ? $r['category'] : '';

            $remarks            =    isset($r['remarks']) && $r['remarks'] ? $r['remarks'] : '';
            $reference          =    isset($r['transaction_id']) && $r['transaction_id'] ? $r['transaction_id'] : '';

            $past_due           =    isset($r['past_due']) && $r['past_due'] ? Dropdown::get_static('past_dues',$r['past_due']) : '';
            $past_due_status    =    isset($r['past_due_status']) && $r['past_due_status'] ? Dropdown::get_static('past_due_status',$r['past_due_status']) : '';


            $letter_request_name     =    isset($r['letter_request_name']) && $r['letter_request_name'] ? $r['letter_request_name'] : '';
            $letter_of_request    =    isset($r['letter_request']) && $r['letter_request'] ? Dropdown::get_static('letter_requests',$r['letter_request']) : '';
            $letter_request_status    =    isset($r['letter_request_status']) && $r['letter_request_status'] ? Dropdown::get_static('letter_request_status',$r['letter_request_status']) : '';
            $letter_request_status_id    =    isset($r['letter_request_status']) && $r['letter_request_status'] ?  $r['letter_request_status'] : '';


            $received_date    =    isset($r['received_date']) && $r['received_date'] ? view_date($r['received_date']) : '';
            $sent_date    =    isset($r['sent_date']) && $r['sent_date'] ? view_date($r['sent_date']) : '';

            $date_requested    =    isset($r['date_requested']) && $r['date_requested'] ? view_date($r['date_requested']) : '';
            $date_approved    =    isset($r['date_approved']) && $r['date_approved'] ? view_date($r['date_approved']) : '';
            $date_disapproved    =    isset($r['date_disapproved']) && $r['date_disapproved'] ? view_date($r['date_disapproved']) : '';
            $date_cancelled    =    isset($r['date_cancelled']) && $r['date_cancelled'] ? view_date($r['date_cancelled']) : '';
            $date_added    =    isset($r['created_at']) && $r['created_at'] ? view_date($r['created_at']) : '';


            // assignatory

        ?>
            <div class="col-md-12">
            <div class="kt-portlet ">
                <div class="kt-portlet__body custom-transaction_payments">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden">
                                <img src="./assets/media/project-logos/5.png" alt="image">
                            </div>
                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-primary kt-font-boldest kt-font-light kt-hidden-">
                                <?php echo get_initials($category); ?>
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="<?php echo site_url('ticketing/view/'. $id); ?>" class="kt-widget__username">
                                        <?php echo ucfirst($category); ?>  
                                        <!-- <i class="flaticon2-correct"></i>                               -->
                                    </a>


                                    <div class="kt-widget__action custom_portlet_header">
                                        <div class="kt-portlet__head kt-portlet__head--noborder" style="min-height: 0px;">
                                            <div class="kt-portlet__head-label">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" name="id[]" value="<?php echo $id; ?>" class="m-checkable delete_check" data-id="<?php echo $id; ?>">
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                    <i class="flaticon-more-1 kt-font-brand"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <a href="<?php echo site_url('ticketing/view/'. $id); ?>" class="kt-nav__link">
                                                                <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                                <span class="kt-nav__link-text">View</span>
                                                            </a>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <a href="javascript: void(0)" class="kt-nav__link remove_ticketing" data-id="<?php echo $r['id']; ?>">
                                                                <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                                <span class="kt-nav__link-text">Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            
                                            <div class="kt-portlet__head-label custom_edit_icon">
                                                <a href="<?php echo site_url('ticketing/form/'. $id); ?>"><i class="la la-edit" style="font-size: 20px;"></i></a>
                                                
                                            </div>              
                                        </div>


                                    </div>
                                </div>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">

                                        <div class="kt-widget__text kt-align-left" style="float: left;">
                                            Reference #: <span class="kt-widget__data" style="font-weight: 500"><?php echo $transaction_id; ?></span>
                                        </div>
                                        <br>
                                        <div class="kt-widget__text kt-align-left" style="float: left;">
                                            Remarks: <span class="kt-widget__data" style="font-weight: 500"><?php echo ucfirst($remarks); ?></span>
                                        </div>
                                        <br>
                                        <div class="kt-widget__text kt-align-left" style="float: left;">
                                            Date Created: <span class="kt-widget__data" style="font-weight: 500"><?php echo $date_added; ?></span>
                                        </div>
                                    </div>

                                    <div class="kt-widget__desc">

                                        <?php if ($cat == "past_due_notices"){ ?>
                                            <div class="kt-widget__text kt-align-left" style="float: left;">
                                                Past Due: <span class="kt-widget__data" style="font-weight: 500"><?php echo ucfirst($past_due); ?></span>
                                            </div>
                                            <br>
                                            <div class="kt-widget__text kt-align-left" style="float: left;">
                                                Past Due Status: <span class="kt-widget__data" style="font-weight: 500"><?php echo ucfirst($past_due_status); ?></span>
                                            </div>
                                            <br>
                                            <div class="kt-widget__text kt-align-left" style="float: left;">
                                                Sent Date to Client: <span class="kt-widget__data" style="font-weight: 500"><?php echo $sent_date; ?></span>
                                            </div>
                                            <br>
                                            <div class="kt-widget__text kt-align-left" style="float: left;">
                                                Received Date to Client: <span class="kt-widget__data" style="font-weight: 500"><?php echo $received_date; ?></span>
                                            </div>
                                        <?php } else if($cat == "letter_of_request") { ?>
                                            <div class="kt-widget__text kt-align-left" style="float: left;">
                                                Letter of Request: <span class="kt-widget__data" style="font-weight: 500"><?php echo ucfirst($letter_of_request); ?></span>
                                            </div>
                                            <br>
                                            <div class="kt-widget__text kt-align-left" style="float: left;">
                                                Letter Request Name: <span class="kt-widget__data" style="font-weight: 500"><?php echo ucfirst($letter_request_name); ?></span>
                                            </div>
                                            <br>
                                            <div class="kt-widget__text kt-align-left" style="float: left;">
                                                Letter Request Status: <span class="kt-widget__data" style="font-weight: 500"><?php echo ucfirst($letter_request_status); ?></span>
                                            </div>
                                            <br>
                                                <div class="kt-widget__text kt-align-left" style="float: left;">
                                                    Date Requested: <span class="kt-widget__data" style="font-weight: 500"><?php echo $date_requested; ?></span>
                                                </div>
                                            <?php if($letter_request_status_id == 2){ ?>
                                                <br>
                                                <div class="kt-widget__text kt-align-left" style="float: left;">
                                                    Approved Date: <span class="kt-widget__data" style="font-weight: 500"><?php echo $date_approved; ?></span>
                                                </div>
                                            <?php } else if($letter_request_status_id == 3){ ?>
                                                <br>
                                                <div class="kt-widget__text kt-align-left" style="float: left;">
                                                    Disapproved Date: <span class="kt-widget__data" style="font-weight: 500"><?php echo $date_disapproved; ?></span>
                                                </div>
                                            <?php } else if($letter_request_status_id == 4){ ?>
                                                <br>
                                                <div class="kt-widget__text kt-align-left" style="float: left;">
                                                    Cancelled Date: <span class="kt-widget__data" style="font-weight: 500"><?php echo $date_cancelled; ?></span>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>

                                    <div class="kt-widget__desc">
                                        &nbsp;
                                    </div>

                                     <div class="kt-widget__desc">
                                        &nbsp;
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>          
        <?php endforeach; ?>
        </div>
        <div class="row">
            <div class="col-xl-12">

                <!--begin:: Components/Pagination/Default-->
                <div class="kt-portlet">
                    <div class="kt-portlet__body">

                        <!--begin: Pagination-->
                        <div class="kt-pagination kt-pagination--brand">
                            <?php echo $this->ajax_pagination->create_links(); ?>

                            <div class="kt-pagination__toolbar">
                                <span class="pagination__desc">
                                    <?php echo $this->ajax_pagination->show_count(); ?>
                                </span>
                            </div>
                        </div>

                        <!--end: Pagination-->
                    </div>
                </div>

                <!--end:: Components/Pagination/Default-->
            </div>
        </div>
        <?php else : ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="kt-portlet kt-callout">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title">No Records Found</h3>
                                    <p class="kt-callout__desc">
                                        Sorry no record were found.
                                    </p>
                                </div>
                                <div class="kt-callout__action">
                                    <a href="<?php echo base_url('transaction'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	<?php endif;?>