<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contractors_model extends MY_Model
{
    public $table = 'contractors'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'name',
        'code',
        'type',
        'payment_terms',
        'delivery_terms',
        'address',
        'alternate_address',
        'mobile_number',
        'alternate_mobile_number',
        'landline_number',
        'fax_number',
        'email_address',
        'tin_number',
        'sales_contact_person',
        'sales_email_address',
        'sales_mobile_number',
        'finance_contact_person',
        'finance_email_address',
        'finance_mobile_number',
        'image',
        'payment_type',
        'vat_type',
        'tax_type',
        'bank_id',
        'bank_account_number',
        'cor_image',
        'auth_payee',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['bank'] = array('foreign_model' => 'accounting_ledgers/accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'bank_id');

    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    // public function insert_dummy()
    // {
    //     require APPPATH . '/third_party/faker/autoload.php';
    //     $faker = Faker\Factory::create();

    //     $data = [];

    //     for ($x = 0; $x < 10; $x++) {
    //         array_push($data, array(
    //             'company' => $faker->word,
    //             'sbu' => $faker->word,
    //             'warehouse' => $faker->word,
    //             'item_code' => $faker->word,
    //             'item_name' => $faker->word,
    //             'current_physical_count' => $faker->word,
    //             'is_active' => $faker->numberBetween(0, 1),
    //         ));
    //     }
    //     $this->db->insert_batch($this->table, $data);

    // }
}
