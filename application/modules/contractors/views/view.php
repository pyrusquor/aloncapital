<?php

$id = isset($data['id']) && $data['id'] ? $data['id'] : 'N/A';
$name = isset($data['name']) && $data['name'] ? $data['name'] : 'N/A';
$code = isset($data['code']) && $data['code'] ? $data['code'] : 'N/A';
$type = isset($data['type']) && $data['type'] ? Dropdown::get_static('supplier_types', $data['type'], 'view') : '';
$payment_terms = isset($data['payment_terms']) && $data['payment_terms'] ? Dropdown::get_static('payment_terms', $data['payment_terms'], 'view') : '';
$delivery_terms = isset($data['delivery_terms']) && $data['delivery_terms'] ? Dropdown::get_static('delivery_terms', $data['delivery_terms'], 'view') : '';
$address = isset($data['address']) && $data['address'] ? $data['address'] : 'N/A';
$alternate_address = isset($data['alternate_address']) && $data['alternate_address'] ? $data['alternate_address'] : 'N/A';
$mobile_number = isset($data['mobile_number']) && $data['mobile_number'] ? $data['mobile_number'] : 'N/A';
$alternate_mobile_number = isset($data['alternate_mobile_number']) && $data['alternate_mobile_number'] ? $data['alternate_mobile_number'] : 'N/A';
$landline_number = isset($data['landline_number']) && $data['landline_number'] ? $data['landline_number'] : 'N/A';
$fax_number = isset($data['fax_number']) && $data['fax_number'] ? $data['fax_number'] : 'N/A';
$email_address = isset($data['email_address']) && $data['email_address'] ? $data['email_address'] : 'N/A';
$tin_number = isset($data['tin_number']) && $data['tin_number'] ? $data['tin_number'] : 'N/A';
$sales_contact_person = isset($data['sales_contact_person']) && $data['sales_contact_person'] ? $data['sales_contact_person'] : 'N/A';
$sales_email_address = isset($data['sales_email_address']) && $data['sales_email_address'] ? $data['sales_email_address'] : 'N/A';
$sales_mobile_number = isset($data['sales_mobile_number']) && $data['sales_mobile_number'] ? $data['sales_mobile_number'] : 'N/A';
$finance_contact_person = isset($data['finance_contact_person']) && $data['finance_contact_person'] ? $data['finance_contact_person'] : 'N/A';
$finance_email_address = isset($data['finance_email_address']) && $data['finance_email_address'] ? $data['finance_email_address'] : 'N/A';
$finance_mobile_number = isset($data['finance_mobile_number']) && $data['finance_mobile_number'] ? $data['finance_mobile_number'] : 'N/A';
$image = isset($data['image']) && $data['image'] ? $data['image'] : 'N/A';
$payment_type = isset($data['payment_type']) && $data['payment_type'] ? Dropdown::get_static('ap_payment_types', $data['payment_type'], 'view') : '';
$vat_type = isset($data['vat_type']) && $data['vat_type'] ? Dropdown::get_static('vat_types', $data['vat_type'], 'view') : '';
$tax_type = isset($data['tax_type']) && $data['tax_type'] ? Dropdown::get_static('tax_types', $data['tax_type'], 'view') : '';
$bank_id = isset($data['bank_id']) && $data['bank_id'] ? $data['bank_id'] : '';
$bank_account_number = isset($data['bank_account_number']) && $data['bank_account_number'] ? $data['bank_account_number'] : '';
$cor_image = isset($data['cor_image']) && $data['cor_image'] ? $data['cor_image'] : '';
$auth_payee = isset($data['auth_payee']) && $data['auth_payee'] ? $data['auth_payee'] : '';
$bank_name = isset($data['bank']['name']) && $data['bank']['name'] ? $data['bank']['name'] : '';
?>

<script type="text/javascript">
    window.payee_type_id = "<?php echo $id ?>";
</script>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Contractor</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('suppliers/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('suppliers'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab" aria-selected="true">
                            <i class="flaticon2-information"></i> General Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payment_request" role="tab" aria-selected="false">
                            <i class="fa fa-hand-holding-usd"></i> Payment Request
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payment_voucher" role="tab" aria-selected="false">
                            <i class="flaticon2-list"></i> Payment Voucher
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div class="tab-content  kt-margin-t-20">
                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="basic_information" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row ">
                                    <div class="col-lg-6">
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Name
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $name; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Code
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $code; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Type
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $type; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Payment Terms
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $payment_terms; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Delivery Terms
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $delivery_terms; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Address
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $address; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Alternate Address
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $alternate_address; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Mobile Number
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $mobile_number; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Alternate Mobile Number
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $alternate_mobile_number; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Landline
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $landline_number; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Fax
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $fax_number; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Email
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $email_address; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                TIN
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $tin_number; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Sales Contact Person
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $sales_contact_person; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Sales Email
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $sales_email_address; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Sales Mobile Number
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $sales_mobile_number; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Finance Contact Person
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $finance_contact_person; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Finance Email
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $finance_email_address; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Finance Mobile Number
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $finance_mobile_number; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Payee Type
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $payment_type; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                VAT Type
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $vat_type; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Tax Type
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $tax_type; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Bank
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $bank_name; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Bank Account Number
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $bank_account_number; ?></span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                COR Image
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder">
                                                    <?php if (get_image('supplier', 'images', $cor_image)) : ?>
                                                        <img class="kt-widget__img center" src="<?php echo base_url(get_image('supplier', 'images', $cor_image)); ?>" width="90px" height="90px" />
                                                    <?php else : ?>
                                                        N/A
                                                    <?php endif; ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="kt-form-group form-group-xs row">
                                            <label class="col col-form-label text-right">
                                                Authorized Payee
                                            </label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $auth_payee; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="payment_request" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentrequest_table">
                                    <thead>
                                        <tr>
                                            <th width="1%">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Payee</th>
                                            <th>Payable Type</th>
                                            <th>Property</th>
                                            <th>Due Date</th>
                                            <th>Gross Amount</th>
                                            <th>Tax Rate (VAT - WHT)</th>
                                            <th>Paid Amount</th>
                                            <th>Remaining Amount</th>
                                            <th>Payment Status</th>
                                            <th>Status</th>
                                            <th>Particulars</th>
                                        </tr>
                                    </thead>
                                </table>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="payment_voucher" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="paymentvoucher_table">
                                    <thead>
                                        <tr>
                                            <th width="1%">
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" value="all" class="m-checkable" id="select-all">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>ID</th>
                                            <th>Reference</th>
                                            <th>Payment Type</th>
                                            <th>Payee</th>
                                            <th>Payee Type</th>
                                            <th>Paid Date</th>
                                            <th>Paid Amount</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                </table>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->
            </div>
        </div>
    </div>
</div>