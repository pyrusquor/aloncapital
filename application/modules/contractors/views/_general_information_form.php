<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
$code = isset($data['code']) && $data['code'] ? $data['code'] : '';
$type = isset($data['type']) && $data['type'] ? $data['type'] : '';
$payment_terms = isset($data['payment_terms']) && $data['payment_terms'] ? $data['payment_terms'] : '';
$delivery_terms = isset($data['delivery_terms']) && $data['delivery_terms'] ? $data['delivery_terms'] : '';
$address = isset($data['address']) && $data['address'] ? $data['address'] : '';
$alternate_address = isset($data['alternate_address']) && $data['alternate_address'] ? $data['alternate_address'] : '';
$mobile_number = isset($data['mobile_number']) && $data['mobile_number'] ? $data['mobile_number'] : '';
$alternate_mobile_number = isset($data['alternate_mobile_number']) && $data['alternate_mobile_number'] ? $data['alternate_mobile_number'] : '';
$landline_number = isset($data['landline_number']) && $data['landline_number'] ? $data['landline_number'] : '';
$fax_number = isset($data['fax_number']) && $data['fax_number'] ? $data['fax_number'] : '';
$email_address = isset($data['email_address']) && $data['email_address'] ? $data['email_address'] : '';
$tin_number = isset($data['tin_number']) && $data['tin_number'] ? $data['tin_number'] : '';
$sales_contact_person = isset($data['sales_contact_person']) && $data['sales_contact_person'] ? $data['sales_contact_person'] : '';
$sales_email_address = isset($data['sales_email_address']) && $data['sales_email_address'] ? $data['sales_email_address'] : '';
$sales_mobile_number = isset($data['sales_mobile_number']) && $data['sales_mobile_number'] ? $data['sales_mobile_number'] : '';
$finance_contact_person = isset($data['finance_contact_person']) && $data['finance_contact_person'] ? $data['finance_contact_person'] : '';
$finance_email_address = isset($data['finance_email_address']) && $data['finance_email_address'] ? $data['finance_email_address'] : '';
$finance_mobile_number = isset($data['finance_mobile_number']) && $data['finance_mobile_number'] ? $data['finance_mobile_number'] : '';
$image = isset($data['image']) && $data['image'] ? $data['image'] : '';
$payment_type = isset($data['payment_type']) && $data['payment_type'] ? $data['payment_type'] : '';
$vat_type = isset($data['vat_type']) && $data['vat_type'] ? $data['vat_type'] : '';
$tax_type = isset($data['tax_type']) && $data['tax_type'] ? $data['tax_type'] : '';
$bank_id = isset($data['bank_id']) && $data['bank_id'] ? $data['bank_id'] : '';
$bank_account_number = isset($data['bank_account_number']) && $data['bank_account_number'] ? $data['bank_account_number'] : '';
$cor_image = isset($data['cor_image']) && $data['cor_image'] ? $data['cor_image'] : '';
$auth_payee = isset($data['auth_payee']) && $data['auth_payee'] ? $data['auth_payee'] : '';

?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Contractor Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="kt-input-icon">
                    <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>"
                        placeholder="Contractor Name" autocomplete="off">
                        <span class="kt-input-icon__icon"></span>
                </div>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Contractor Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <div class="kt-input-icon">
                    <input type="text" class="form-control" name="code" value="<?php echo set_value('code', $code); ?>"
                        placeholder="Contractor Code" autocomplete="off">
                        <span class="kt-input-icon__icon"><</span>
                </div>
            </div>
            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Contractor Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('type', Dropdown::get_static('contractor_types'), set_value('type', @$type), 'class="form-control" id="type"'); ?>

            </div>
            <?php echo form_error('type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Delivery Terms <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('delivery_terms', Dropdown::get_static('delivery_terms'), set_value('delivery_terms', @$delivery_terms), 'class="form-control" name="delivery_terms"'); ?>

            </div>
            <?php echo form_error('delivery_terms'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Payment Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('payment_type', Dropdown::get_static('ap_payment_types'), set_value('payment_type', @$payment_type), 'class="form-control" name="payment_type"'); ?>

            </div>
            <?php echo form_error('payment_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Payment Terms</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="payment_terms"
                       value="<?php echo set_value('payment_terms', $payment_terms); ?>" placeholder="Payment Terms"
                       autocomplete="off">
                       <span class="kt-input-icon__icon"></span>

            </div>
            <?php echo form_error('payment_terms'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Email Address <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="email" class="form-control" name="email_address"
                       value="<?php echo set_value('email_address', $email_address); ?>" placeholder="Email Address"
                       autocomplete="off">
                       <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('email_address'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Landline Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="landline_number"
                       value="<?php echo set_value('landline_number', $landline_number); ?>"
                       placeholder="Landline Number" autocomplete="off">
                       <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('landline_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>FAX Number </label>
            <div class="kt-input-icon">
                <input type="number" class="form-control" name="fax_number"
                       value="<?php echo set_value('fax_number', $fax_number); ?>" placeholder="FAX Number"
                       autocomplete="off">
                       <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('fax_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Address <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="address"
                       value="<?php echo set_value('address', $address); ?>" placeholder="Address" autocomplete="off">
                       <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('address'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Alternate Address</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="alternate_address"
                       value="<?php echo set_value('alternate_address', $alternate_address); ?>"
                       placeholder="Alternate Address" autocomplete="off">
                       <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('alternate_address'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>TIN Number <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="tin_number"
                       value="<?php echo set_value('tin_number', $tin_number); ?>" placeholder="TIN Number"
                       autocomplete="off">
                       <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('tin_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Image</label>
            <div class="kt-input-icon">
                <?php if (get_image('contractor', 'images', $image)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('contractor', 'images', $image)); ?>"
                         width="90px" height="90px"/>
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="image" class="" aria-invalid="false">
                </span>
            </div>
            <?php echo form_error('image'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Vat Type</label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('vat_type', Dropdown::get_static('vat_types'), set_value('vat_type', @$vat_type), 'class="form-control" name="vat_type"'); ?>

            </div>
            <?php echo form_error('vat_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Tax Type</label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('tax_type', Dropdown::get_static('tax_types'), set_value('tax_type', @$vat_type), 'class="form-control" name="tax_type"'); ?>

            </div>
            <?php echo form_error('tax_type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Bank</label>
            <select class="form-control suggests" data-module="banks" data-type="banks"  id="bank_id" name="bank_id">
                <option value="">Select Bank</option>
                <?php if ($bank_name): ?>
                    <option value="<?php echo $bank_id; ?>" selected><?php echo $bank_name; ?></option>
                <?php endif ?>
            </select>
            <?php echo form_error('bank_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Bank Account Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="bank_account_number"
                       value="<?php echo set_value('bank_account_number', $bank_account_number); ?>"
                       placeholder="Bank Account Number"
                       autocomplete="off">

            </div>
            <?php echo form_error('bank_account_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Authorized Payee</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="auth_payee"
                       value="<?php echo set_value('auth_payee', $auth_payee); ?>" placeholder="Authorized Payee"
                       autocomplete="off">
                       <span class="kt-input-icon__icon"></span>

            </div>
            <?php echo form_error('auth_payee'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>COR File</label>
            <div class="kt-input-icon">
                <?php if (get_image('contractor', 'images', $cor_image)): ?>
                    <img class="kt-widget__img"
                         src="<?php echo base_url(get_image('contractor', 'images', $cor_image)); ?>"
                         width="90px" height="90px"/>
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="cor_image" class="" aria-invalid="false">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>