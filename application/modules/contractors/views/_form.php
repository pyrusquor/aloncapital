<!--begin: Basic Transaction Info-->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
    <?php $this->load->view('_general_information_form');?>
</div>
<!--end: Basic Transaction Info-->

<!--begin: Item Form -->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_sales_form');?>
</div>
<!--end: Item Form -->

<!--begin: Payment Form -->
<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
    <?php $this->load->view('_finance_form');?>
</div>
<!--end: Payment Form -->

<!--begin: Form Actions -->
<div class="kt-form__actions">
    <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-prev">
        Previous
    </div>
    <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-submit">
        Submit
    </div>
    <div id="next_btn" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
        data-ktwizard-type="action-next">
        Next Step
    </div>
</div>

<!--end: Form Actions -->