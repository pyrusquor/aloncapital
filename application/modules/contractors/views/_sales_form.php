<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
$code = isset($data['code']) && $data['code'] ? $data['code'] : '';
$type = isset($data['type']) && $data['type'] ? $data['type'] : '';
$payment_terms = isset($data['payment_terms']) && $data['payment_terms'] ? $data['payment_terms'] : '';
$delivery_terms = isset($data['delivery_terms']) && $data['delivery_terms'] ? $data['delivery_terms'] : '';
$address = isset($data['address']) && $data['address'] ? $data['address'] : '';
$alternate_address = isset($data['alternate_address']) && $data['alternate_address'] ? $data['alternate_address'] : '';
$mobile_number = isset($data['mobile_number']) && $data['mobile_number'] ? $data['mobile_number'] : '';
$alternate_mobile_number = isset($data['alternate_mobile_number']) && $data['alternate_mobile_number'] ? $data['alternate_mobile_number'] : '';
$landline_number = isset($data['landline_number']) && $data['landline_number'] ? $data['landline_number'] : '';
$fax_number = isset($data['fax_number']) && $data['fax_number'] ? $data['fax_number'] : '';
$email_address = isset($data['email_address']) && $data['email_address'] ? $data['email_address'] : '';
$tin_number = isset($data['tin_number']) && $data['tin_number'] ? $data['tin_number'] : '';
$sales_contact_person = isset($data['sales_contact_person']) && $data['sales_contact_person'] ? $data['sales_contact_person'] : '';
$sales_email_address = isset($data['sales_email_address']) && $data['sales_email_address'] ? $data['sales_email_address'] : '';
$sales_mobile_number = isset($data['sales_mobile_number']) && $data['sales_mobile_number'] ? $data['sales_mobile_number'] : '';
$finance_contact_person = isset($data['finance_contact_person']) && $data['finance_contact_person'] ? $data['finance_contact_person'] : '';
$finance_email_address = isset($data['finance_email_address']) && $data['finance_email_address'] ? $data['finance_email_address'] : '';
$finance_mobile_number = isset($data['finance_mobile_number']) && $data['finance_mobile_number'] ? $data['finance_mobile_number'] : '';
$image = isset($data['image']) && $data['image'] ? $data['image'] : '';

?>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Sales Contact Person</label>
                <div class="kt-input-icon">
                    <input type="text" class="form-control" name="sales_contact_person"
                        value="<?php echo set_value('sales_contact_person', $sales_contact_person); ?>"
                        placeholder="Sales Contact Person" autocomplete="off">

                </div>
                <?php echo form_error('sales_contact_person'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Sales Email Address</label>
                <div class="kt-input-icon">
                    <input type="email" class="form-control" name="sales_email_address"
                        value="<?php echo set_value('sales_email_address', $sales_email_address); ?>"
                        placeholder="Sales Email Address" autocomplete="off">

                </div>
                <?php echo form_error('sales_email_address'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Sales Mobile Number</label>
                <div class="kt-input-icon">
                    <input type="number" class="form-control" name="sales_mobile_number"
                        value="<?php echo set_value('sales_mobile_number', $sales_mobile_number); ?>"
                        placeholder="Sales Mobile Number" autocomplete="off">

                </div>
                <?php echo form_error('sales_mobile_number'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>