<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Leasing_property_model extends My_Model
{
    public $table = 'leasing_properties';
    public $primary_key = 'id';
    public $fillable = [
        'leasing_project_id',
        'unit_type_id',
        'unit_number',
        'property_code',
        'general_status_id',
        'company_id',
        'real_property_tax',
        'area',
        'cusa_price',
        'price_per_sqm',
        'base_rental_rate',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id'];
    public $rules = [];
    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['leasing_project'] = array('foreign_model' => 'leasing_project/Leasing_project_model', 'foreign_table' => 'leasing_projects', 'foreign_key' => 'id', 'local_key' => 'leasing_project_id');

        $this->has_one['company'] = array('foreign_model' => 'company/Company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
    }
}
