<?php

$id = $info['id'] ?? '';
$leasing_project_name = $info['leasing_project']['project_name'] ?? '';
$unit_type_id = $info['unit_type_id'] ?? '';
$unit_number = $info['unit_number'] ?? '';
$property_code = $info['property_code'] ?? '';
$general_status_id = $info['general_status_id'] ?? '';
$company_name = $info['company']['name'] ?? '';
$real_property_tax = $info['real_property_tax'] ?? '';
$area = $info['area'] ?? '';
$cusa_price = $info['cusa_price'] ?? '';
$price_per_sqm = $info['price_per_sqm'] ?? '';
$base_rental_rate = $info['base_rental_rate'] ?? '';

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Leasing Property</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('leasing_property/form/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('leasing_property'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Leasing Property
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Project Name:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $leasing_project_name ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Unit Type:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= Dropdown::get_static('leasing_property_unit_type', $unit_type_id) ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Unit #:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $unit_number ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Property Code:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $property_code ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        General Status:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= Dropdown::get_static('leasing_property_general_status', $general_status_id) ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Company:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $company_name ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Real Property Tax:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $real_property_tax ?></p>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Area:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $area ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Cusa Price:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $cusa_price ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Price / SQM:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $price_per_sqm ?></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="font-weight-bold">
                                        Base Rental Rate:
                                    </p>
                                    <p class="kt-widget13__text kt-widget13__text--bold"><?= $base_rental_rate ?></p>
                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->