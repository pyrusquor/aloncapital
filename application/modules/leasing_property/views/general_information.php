<?php

$leasing_project_id = $info['leasing_project']['id'] ?? '';
$leasing_project_name = $info['leasing_project']['project_name'] ?? '';
$unit_type_id = $info['unit_type_id'] ?? '';
$unit_number = $info['unit_number'] ?? '';
$property_code = $info['property_code'] ?? '';
$general_status_id = $info['general_status_id'] ?? '';
$company_id = $info['company']['id'] ?? '';
$company_name = $info['company']['name'] ?? '';
$real_property_tax = $info['real_property_tax'] ?? '';

?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Project Name <span class="kt-font-danger">*</span></label>
            <select type="text" class="form-control suggests" name="info[leasing_project_id]" data-module="leasing_projects" data-select="project_name" required>
                <option value="<?= $leasing_project_id ?>"><?= $leasing_project_name ?></option>
            </select>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Unit Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[unit_type_id]', Dropdown::get_static("leasing_property_unit_type"), $unit_type_id, 'class="form-control" required') ?>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Unit # <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[unit_number]" value="<?= $unit_number ?>" placeholder="Unit #" required>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Property Code <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="info[property_code]" value="<?= $property_code ?>" placeholder="Property Code" required>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>General Status <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[general_status_id]', Dropdown::get_static("leasing_property_general_status"), $general_status_id, 'class="form-control" required') ?>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <select type="text" class="form-control suggests" name="info[company_id]" data-module="companies" required>
                <option value="<?= $company_id ?>"><?= $company_name ?></option>
            </select>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Real Property Tax</label>
            <input type="number" class="form-control" name="info[real_property_tax]" value="<?= $real_property_tax ?>" placeholder="Real Property Tax">
        </div>
    </div>
</div>