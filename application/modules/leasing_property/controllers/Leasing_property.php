<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Leasing_property extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $models = [
            'Leasing_property_model' => 'M_leasing_property'
        ];

        // Load models
        $this->load->model($models);
    }

    public function index()
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index');
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'leasing_project' => true,
            'company' => true,
            'unit_type' => true,
            'unit_number' => true,
            'created_by' => true,
            'updated_by' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_leasing_property
                ->with_leasing_project('fields:project_name')
                ->with_company('fields:name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("unit_number like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'leasing_project_id' => [
                    'data' => $filters['leasing_project_id'] ?? null,
                    'operator' => '=',
                ],
                'company_id' => [
                    'data' => $filters['company_id'] ?? null,
                    'operator' => '=',
                ],
                'unit_type_id' => [
                    'data' => $filters['unit_type_id'] ?? null,
                    'operator' => '=',
                ],
                'unit_number' => [
                    'data' => $filters['unit_number'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    $query->where($key, $value['operator'], $value['data']);
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['leasing_project'] = $value['leasing_project']['project_name'] ?? '';

                        $items[$key]['company'] = $value['company']['name'] ?? '';

                        $items[$key]['unit_type'] = Dropdown::get_static('leasing_property_unit_type', $value['unit_type_id']);

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_leasing_property->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {
        $method = !!$id ? "Update" : "Create";

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->db->trans_begin();

            $info = $this->input->post('info');

            if ($id) {

                $updated_by = ['updated_by' => $this->user->id ?? NULL];

                $this->M_leasing_property->update($info + $updated_by, $id);
            } else {

                $id = $this->M_leasing_property->insert($info);
            }

            if ($this->db->trans_status() === FALSE) {

                $this->db->trans_rollback();
            } else {

                $this->db->trans_commit();
                $response['status'] = 1;
                $response['message'] = 'Leasing Property Successfully ' . $method . 'd!';
            }

            echo json_encode($response);
            exit();
        } else {

            if ($id) {

                $this->view_data['info'] = $this->M_leasing_property->with_leasing_project()->with_company()->get($id);
            }

            $this->template->build('form', $this->view_data);
        }
    }

    public function view($id = null)
    {
        if ($id) {

            $this->view_data['info'] = $this->M_leasing_property
                ->with_leasing_project('fields:project_name')
                ->with_company('fields:name')
                ->get($id);

            if ($this->view_data['info']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_leasing_property->get($id);
            if ($list) {

                $deleted = $this->M_leasing_property->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Leasing Project Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_leasing_property->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Leasing Project/s Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }
}
