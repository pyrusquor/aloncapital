<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchase_order_request_model extends MY_Model
{

    public $table = 'purchase_order_requests'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'id',
        /* ==================== begin: Add model fields ==================== */
        'reference',
        'warehouse_id',
        'company_id',
        'approving_staff_id',
        'requesting_staff_id',
        'accounting_ledger_id',
        'particulars',
        'request_date',
        'request_reason',
        'request_status',
        'total_cost',
        /* ==================== end: Add model fields ==================== */
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $form_fillables = [
        'id',
        'reference',
        'company_id',
        'warehouse_id',
        'approving_staff_id',
        'requesting_staff_id',
        'accounting_ledger_id',
        'particulars',
        'total_cost',
        'request_date',
        'request_reason',
        'request_status',
    ];

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['company'] = array('foreign_model' => 'company/company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');
        $this->has_one['accounting_ledger'] = array('foreign_model' => 'accounting_ledgers/accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'accounting_ledger_id');
        $this->has_one['approving_staff'] = array('foreign_model' => 'staff/staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'approving_staff_id');
        $this->has_one['requesting_staff'] = array('foreign_model' => 'staff/staff_model', 'foreign_table' => 'staff', 'foreign_key' => 'id', 'local_key' => 'requesting_staff_id');
        $this->has_one['warehouse'] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouses', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');

        $this->has_many['rpo_items'] = array('foreign_model'=>'purchase_order_request_item/purchase_order_request_item_model','foreign_table'=>'purchase_order_request_items','foreign_key'=>'purchase_order_request_id','local_key'=>'id');

    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}
