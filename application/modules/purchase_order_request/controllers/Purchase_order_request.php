<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchase_order_request extends MY_Controller
{
    private $fields = [
        array(
            'field' => 'info[name]',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        /* ==================== begin: Add model fields ==================== */

        /* ==================== end: Add model fields ==================== */

    ];

    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Purchase_order_request_model', 'M_Purchase_order_request');
        $this->load->model('purchase_order_request_item/purchase_order_request_item_model', 'M_Purchase_order_request_item');
        $this->load->model('purchase_order_item/purchase_order_item_model', 'M_Purchase_order_item');
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('staff/Staff_model', 'M_staff');



        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Purchase_order_request->fillable;
        $this->_table_columns = $this->M_Purchase_order_request->__get_columns();
        $this->_table = 'purchase_order_requests';
        $this->module_name = $this->uri->segment(1, '');
    }

    private function __get_obj($id, $with_relations = TRUE)
    {
        $this->load->helper('purchase_order_request');
        if ($with_relations) {
            $result = $this->M_Purchase_order_request
                ->with_company()
                ->with_approving_staff()
                ->with_requesting_staff()
                ->with_accounting_ledger()
                ->get($id);
        } else {
            $result = $this->M_Purchase_order_request->get($id);
        }


        return $result;
    }

    private function __get_items($id, $with_relations = TRUE)
    {
        $__items = $this->M_Purchase_order_request_item
            ->where('purchase_order_request_id', $id)
            ->with_item_group()
            ->with_item_type()
            ->with_item_brand()
            ->with_item_class()
            ->with_item()
            ->with_unit_of_measurement()
            ->with_material_request()
            ->get_all();
        return $__items;
    }

    public function get_all()
    {
        $data['row'] = $this->M_Purchase_order_request->get_all();

        echo json_encode($data);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Purchase_order_request->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Purchase_order_request->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue([
            'vendors/custom/datatables/datatables.bundle.js',
            'js/vue2.js',
            'js/axios.min.js'
        ]);

        $this->template->build('index', $this->view_data);
    }

    function filterKeyword($data, $search, $field = '')
    {
        $filter = '';

        if (isset($search['value'])) {
            $filter = $search['value'];
        }

        if (!empty($filter)) {
            if (!empty($field)) {
                if (strpos(strtolower($field), 'date') !== false) {
                    // filter by date range
                    $data = $this->filterByDateRange($data, $filter, $field);
                } else {
                    // filter by column
                    $data = array_filter($data, function ($a) use ($field, $filter) {

                        if ($a[$field] == $filter) {
                            return 1;
                        } else {
                            return 0;
                        }

                        // return (boolean) preg_match( "/$filter/i", $a[ $field ] );

                    });
                }
            } else {
                // general filter
                $data = array_filter($data, function ($a) use ($filter) {
                    // print_r($a); die();
                    // return (boolean) preg_grep( "/$filter/i", (array) $a );
                    foreach ($a as $key => $row) {
                        if (is_array($a[$key])) {
                            if (key_exists('name', $a[$key])) {
                                if ($a[$key]['name'] == $filter) {
                                    return 1;
                                }
                            }
                        } else {
                            if ($key == 'request_status') {
                                $this->load->helper('purchase_order_request');
                                if (rpo_status_reverse_lookup($filter) === 1) {
                                    return 1;
                                }
                            } else {
                                if ($a[$key] == $filter) {
                                    return 1;
                                }
                            }
                        }
                    }
                    return 0;
                });
            }
        }

        return $data;
    }

    public function showPurchaseOrderRequests()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            /* ==================== begin: Add model fields ==================== */
            'reference' => true,
            'company_id' => false,
            'company' => true,
            'accounting_ledger_id' => false,
            'accounting_ledger' => true,
            'request_date' => true,
            'request_status' => true,
            'total_cost' => true,
            'approving_staff_id' => true,
            'requesting_staff_id' => true,
            'created_by' => true,
            'updated_by' => true,
            'request_status_label' => true,
            'items' => true,

            /* ==================== end: Add model fields ==================== */
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        //            if (isset($_REQUEST['filter'])) {
        //                $_filters = $_GET;
        //
        //            } else {
        //                $purchase_order_requests = $this->M_Purchase_order_request->with_company()->with_accounting_ledger()->order_by('id', 'DESC')->as_array()->get_all();
        //            }
        $purchase_order_requests = $this->__search($_GET);
        $data = [];

        if ($purchase_order_requests) {
            $this->load->helper('purchase_order_request');

            foreach ($purchase_order_requests as $key => $value) {

                $purchase_order_requests[$key]['request_date'] = view_date($value['request_date']);
                $purchase_order_requests[$key]['total_cost'] = money_php($value['total_cost']);
                $purchase_order_requests[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';
                $purchase_order_requests[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';

                $purchase_order_requests[$key]['request_status_label'] = rpo_status_lookup($value['request_status']);

                $purchase_order_requests[$key]['items'] = $this->M_Purchase_order_request_item->where(
                    array(
                        'purchase_order_request_id' => $value['id']
                    )
                )->with_item()->with_material_request()->get_all();
            }

            foreach ($purchase_order_requests as $d) {
                $__data = $this->filterArray($d, $columnsDefault);

                array_push($data, $__data);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {
                $column = $_REQUEST['order'][0]['column'];
                $dir = $_REQUEST['order'][0]['dir'];
                usort($data, function ($a, $b) use ($column, $dir) {
                    $a = array_slice($a, $column, 1);
                    $b = array_slice($b, $column, 1);
                    $a = array_pop($a);
                    $b = array_pop($b);

                    if ($dir === 'asc') {
                        return $a > $b ? true : false;
                    }

                    return $a < $b ? true : false;
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );
        }
        header('Content-Type: application/json');
        echo json_encode($output);
        exit();
    }

    public function __index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // Get record count
        // $conditions['returnType'] = 'count';
        $this->view_data['totalRec'] = $totalRec = $this->M_Purchase_order_request->count_rows();

        // Pagination configuration
        $config['target'] = '#purchase_order_request_content';
        $config['base_url'] = base_url('purchase_order_request/paginationData');
        $config['total_rows'] = $totalRec;
        $config['per_page'] = $this->perPage;
        $config['link_func'] = 'PurchaseOrderRequestPagination';

        // Initialize pagination library
        $this->ajax_pagination->initialize($config);

        // Get records
        $this->view_data['records'] = $this->purchase_order_request
            ->limit($this->perPage, 0)
            ->get_all();
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function get_items()
    {
        $id = $this->input->get('purchase_order_request_id');
        $id = $id ? $id : null;

        if (!$id) {
            die('ID required');
        }

        $with_canvass = $this->input->get('with_canvass');
        $with_canvass = $with_canvass ? $with_canvass : 'no';

        $supplier_id = $this->input->get('supplier_id');
        $supplier_id = $supplier_id ? $supplier_id : null;

        $with_current_quantity = $this->input->get('with_current_quantity');

        //            $this->load->model('purchase_order_request_item/purchase_order_request_item_model', 'M_Purchase_order_request_item');
        //            $__items = $this->M_Purchase_order_request_item->where('purchase_order_request_id', $id)->with_item_group()->with_item_type()->with_item_brand()->with_item_class()->with_item()->with_unit_of_measurement()->get_all();
        $__items = $this->__get_items($id);

        if (isset($with_current_quantity) && $with_current_quantity === 'true' && !!$__items) {

            foreach ($__items as $key => $value) {

                $po_items = $this->M_Purchase_order_item->where("purchase_order_request_item_id", $value['id'])->get_all();

                if (!!$po_items) {

                    $po_quantity_total = 0;

                    foreach ($po_items as $_key => $_value) {

                        $po_quantity_total += $_value['quantity'];
                    }

                    $quantity = ($value['quantity'] - $po_quantity_total);

                    $__items[$key]["quantity"] = $quantity < 0 ? 0 : $quantity;
                }
            }
        }

        //            $__items = get_objects_from_table_by_field('purchase_order_request_items', 'purchase_order_request_id', $id)->result();
        $items = array();

        if ($with_canvass === 'yes') {
            $this->load->model('canvassing_item/canvassing_item_model', 'M_Canvassing_item');
            foreach ($__items as $__item) {
                $this->M_Canvassing_item->where('purchase_order_request_item_id', $__item['id']);
                if ($supplier_id) {
                    $this->M_Canvassing_item->where('supplier_id', $supplier_id);
                }
                $canvass = $this->M_Canvassing_item->with_canvassing()->with_supplier()->with_warehouse()->with_material_request()->with_purchase_order_request()->with_item_group()->with_item_type()->with_item_brand()->with_item_class()->with_item()->with_unit_of_measurement()->order_by('updated_at, created_at, canvassing_id', 'DESC')->get_all();

                $item = $__item;
                $item['canvass'] = $canvass;
                array_push($items, $item);
            }
        } else {

            $items = $__items;
        }

        header('Content-Type: application/json');
        echo json_encode($items);
    }

    private function __search($params)
    {
        $id = isset($params['id']) ? $params['id'] : null;
        $with_relations = isset($params['with_relations']) ? $params['with_relations'] : 'yes';

        if (!$id) {

            $company_id = isset($params['company_id']) && !empty($params['company_id']) ? $params['company_id'] : null;
            $request_date = isset($params['request_date']) && !empty($params['request_date']) ? $params['request_date'] : null;
            $accounting_ledger_id = isset($params['accounting_ledger_id']) && !empty($params['accounting_ledger_id']) ? $params['accounting_ledger_id'] : null;
            $request_status = isset($params['request_status']) && !empty($params['request_status']) ? $params['request_status'] : null;
            $reference = isset($params['reference']) && !empty($params['reference']) ? $params['reference'] : null;

            if ($company_id) {
                $this->M_Purchase_order_request->where('company_id', $company_id);
            }
            if ($request_date) {
                $this->M_Purchase_order_request->where('request_date', $request_date);
            }
            if ($accounting_ledger_id) {
                $this->M_Purchase_order_request->where('accounting_ledger_id', $accounting_ledger_id);
            }
            if ($request_status) {
                $this->M_Purchase_order_request->where('request_status', $request_status);
            }
            if ($reference) {
                $this->M_Purchase_order_request->like('reference', $reference, 'both');
            }
        } else {
            $this->M_Purchase_order_request->where('id', $id);
        }

        $this->M_Purchase_order_request->order_by('id', 'DESC');

        $item_id = isset($params['item_id']) && !empty($params['item_id']) ? $params['item_id'] : null;

        if ($item_id) {
            $ids = [];
            $items = $this->M_Purchase_order_request_item->where('item_id', $item_id)->get_all();
            foreach ($items as $item) {
                $ids[] = $item['purchase_order_request_id'];
            }

            if (sizeof($ids) > 0) {
                $this->M_Purchase_order_request->where('id', $ids);
            }
        }

        if ($with_relations === 'yes') {
            $result = $this->M_Purchase_order_request
                ->with_company()
                ->with_approving_staff()
                ->with_requesting_staff()
                ->with_accounting_ledger()
                ->get_all();
        } else {
            $result = $this->M_Purchase_order_request->get_all();
        }


        return $result;
    }

    public function search()
    {

        $result = $this->__search($_GET);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            // Input from Advanced Filter
            $name = $this->input->post('name');
            /* ==================== begin: Add model fields ==================== */

            /* ==================== end: Add model fields ==================== */

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->purchase_order_request->count_rows();
            $where = array();

            // Pagination configuration
            $config['target'] = '#purchase_order_request_content';
            $config['base_url'] = base_url('purchase_order_request/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'PurchaseOrderRequestPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('last_name', $keyword, 'both');
                $this->db->or_like('first_name', $keyword, 'both');
                $this->db->or_like('middle_name', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($name)) :
                $this->db->group_start();
                $this->db->like('last_name', $name, 'both');
                $this->db->or_like('first_name', $name, 'both');
                $this->db->or_like('middle_name', $name, 'both');
                $this->db->group_end();
            endif;

            $totalRec = $this->purchase_order_request->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('last_name', $keyword, 'both');
                $this->db->or_like('first_name', $keyword, 'both');
                $this->db->or_like('middle_name', $keyword, 'both');
                $this->db->group_end();
            endif;


            if (!empty($civil_status_id) && !empty($civil_status_id)) :
                $this->db->where('civil_status_id', $civil_status_id);
                $where['civil_status_id'] = $civil_status_id;
            endif;

            $this->view_data['records'] = $records = $this->purchase_order_request
                ->limit($this->perPage, $offset)
                ->get_all();


            $this->load->view('purchase_order_request/_filter', $this->view_data, false);
        }
    }

    public function view($id = FALSE)
    {
        $this->js_loader->queue([
            'js/vue2.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);

        if ($id) {

            $this->view_data['data'] = $this->__get_obj($id);
            $this->view_data['id'] = $id;
            if ($this->view_data['data']) {
                $this->view_data['items'] = $this->__get_items($id);
                $this->template->build('view', $this->view_data);
            } else {
                show_404();
            }
        } else {

            show_404();
        }
    }

    private function process_create_master($data)
    {
        unset($data['id']);

        $data['reference'] = uniqidRPO();
        // $request = $this->M_Purchase_order_request->insert($data);
        $request = $this->db->insert('purchase_order_requests', $data);
        if ($request) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    private function process_update_master($id, $data, $additional)
    {
        return $this->M_Purchase_order_request->update($data + $additional, $id);
    }

    function process_update_slave($data, $additional, $request_id)
    {
        $id = isset($data['id']) ? $data['id'] : null;
        $this->load->model('purchase_order_request_item/Purchase_order_request_item_model', 'M_Purchase_order_request_item');
        if ($id) {
            // return $this->db->update('purchase_order_request_items');
            return $this->M_Purchase_order_request_item->update($data + $additional, $id);
        } else {
            $data['purchase_order_request_id'] = $request_id;
            $additional = [
                'created_by' => $additional['updated_by'],
                'created_at' => $additional['updated_at']
            ];

            return $this->process_create_slave($data + $additional, $request_id);
        }
    }

    private function process_create_slave($data, $reference)
    {
        $this->load->model('purchase_order_request_item/Purchase_order_request_item_model', 'M_Purchase_order_request_item');
        $item = array(
            "item_group_id" => $data['item_group_id'],
            "item_type_id" => $data['item_type_id'],
            "item_brand_id" => $data['item_brand_id'],
            "item_class_id" => $data['item_class_id'],
            "item_id" => $data['item_id'],
            "unit_of_measurement_id" => $data['unit_of_measurement_id'],
            "material_request_id" => $data['material_request_id'],
            "purchase_order_request_id" => $reference,
            "item_brand_name" => $data['item_brand_name'],
            "item_class_name" => $data['item_class_name'],
            "item_name" => $data['item_name'],
            "unit_of_measurement_name" => $data['unit_of_measurement_name'],
            "item_group_name" => $data['item_group_name'],
            "item_type_name" => $data['item_type_name'],
            "quantity" => $data['quantity'],
            "unit_cost" => $data['unit_cost'],
            "total_cost" => $data['total_cost'],
        );
        return $this->db->insert('purchase_order_request_items', $item);
    }

    public function process_update($id)
    {
        header('Content-Type: application/json');
        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];
        if ($this->input->post()) {
            $request = $this->input->post('request');
            $items = $this->input->post('items');

            $this->db->trans_start();
            $this->db->trans_strict(false);
            $rpo = $this->process_update_master($id, $request, $additional);
            if ($rpo) {
                foreach ($items as $item) {
                    $this->process_update_slave($item, $additional, $id);
                }
            }
            $deleted_items = $this->input->post('deleted_items');

            $this->db->trans_complete(); # Completing request
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
                $response['message'] = 'Error!';
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $this->process_purge_deleted($deleted_items, $additional);

                $response['status'] = 1;
                $response['message'] = 'Request Successfully saved!';
            }

            echo json_encode($response);
            exit();
        }
    }

    private function process_purge_deleted($items, $additional)
    {
        $this->load->model('purchase_order_request_item/Purchase_order_request_item_model', 'M_Purchase_order_request_item');
        if ($items) {
            foreach ($items as $item) {
                if ($item) {
                    $this->M_Purchase_order_request_item->delete($item);
                }
            }
        }
    }

    public function process_create()
    {
        header('Content-Type: application/json');
        if ($this->input->post()) {
            $request = $this->input->post('request');
            $items = $this->input->post('items');

            $this->db->trans_start();
            $this->db->trans_strict(false);
            $rpo = $this->process_create_master($request);
            if ($rpo) {
                foreach ($items as $item) {
                    $this->process_create_slave($item, $rpo);
                }
            }

            $this->db->trans_complete(); # Completing request
            if ($this->db->trans_status() === false) {
                # Something went wrong.
                $this->db->trans_rollback();
                $response['status'] = 0;
                $response['message'] = 'Error!';
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();

                $response['status'] = 1;
                $response['message'] = 'Request Successfully saved!';
            }

            echo json_encode($response);
            exit();
        }
    }

    public function form($id = FALSE)
    {
        $this->load->helper('approver');

        $approver_setting = get_approver($this->module_name);
        $this->db->where('user_id = ' . $this->user->id);
        $staff = $this->M_staff->get();
        if ($id) {
            $method = "Update";
            $purchase_order_request = $this->M_Purchase_order_request->get($id);
            $form_data = fill_form_data($this->M_Purchase_order_request->form_fillables, $purchase_order_request);
            $reference = $form_data['reference'];
        } else {
            $method = "Create";
            $purchase_order_request = null;
            $reference = null;
            $form_data = fill_form_data($this->M_Purchase_order_request->form_fillables);
            $this->view_data['approver'] = $approver_setting;
            $this->view_data['staff'] = $staff;
        }

        $this->view_data['fillables'] = $this->M_Purchase_order_request->form_fillables;
        $this->view_data['current_user'] = $this->user->id;
        $this->view_data['method'] = $method;
        $this->view_data['purchase_order_request'] = $purchase_order_request;
        $this->view_data['id'] = $id ? $id : null;
        $this->view_data['form_data'] = $form_data;
        $this->view_data['reference'] = $reference;

        $this->js_loader->queue([
            'js/vue2.js',
            // 'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js',
            'js/axios.min.js',
            'js/utils.js'
        ]);

        $this->template->build('form', $this->view_data);
    }

    public function create()
    {
        if ($this->input->post()) {
            $_input = $this->input->post();
            $_input['created_by'] = $this->session->userdata['user_id'];

            $result = $this->purchase_order_request->from_form()->insert($_input);

            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Purchase Order Request successfully created.', 'purchase_order_request');
            }
        }

        $this->template->build('create');
    }

    public function update($id = false)
    {
        if ($id) {

            $this->view_data['purchase_order_request'] = $data = $this->purchase_order_request->get($id);

            if ($data) {

                if ($this->input->post()) {

                    $_input = $this->input->post();
                    $_input['updated_by'] = $this->session->userdata['user_id'];

                    $result = $this->purchase_order_request->from_form()->update($_input, $data['id']);

                    if ($result === false) {

                        // Validation
                        $this->notify->error('Oops something went wrong.');
                    } else {

                        // Success
                        $this->notify->success('Successfully Updated.', 'purchase_order_request');
                    }
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Purchase_order_request->get($id);
            if ($list) {

                $deleted = $this->M_Purchase_order_request->delete($list['id']);
                if ($deleted !== FALSE) {

                    $response['status'] = 1;
                    $response['message'] = 'Purchase Order Request successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Purchase_order_request->delete($value);
                }
                if ($deleted !== FALSE) {

                    $response['status'] = 1;
                    $response['message'] = 'Purchase Order Request successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $purchase_order_requests = $this->M_Purchase_order_request->as_array()->get_all();
        if ($purchase_order_requests) {

            foreach ($purchase_order_requests as $_lkey => $purchase_order_request) {

                $_datas[$purchase_order_request['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_purchase_order_requests_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'purchase_order_request');
                }
            } else {

                $_filename = 'list_of_purchase_order_requests_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns	=	$this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'purchase_order_request');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';
                        } elseif ((strpos($_name, 'is_') !== FALSE)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($purchase_order_requests as $_lkey => $purchase_order_request) {

                                $_datas[$purchase_order_request['id']][$_title] = isset($purchase_order_request[$_column]) && ($purchase_order_request[$_column] !== '') ? Dropdown::get_static('bool', $purchase_order_request[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($purchase_order_requests as $_lkey => $purchase_order_request) {

                                if ($_name === 'status') {

                                    $_datas[$purchase_order_request['id']][$_title] = isset($purchase_order_request[$_name]) && $purchase_order_request[$_name] ? Dropdown::get_static('inventory_status', $purchase_order_request[$_name], 'view') : '';
                                } else {

                                    $_datas[$purchase_order_request['id']][$_title] = isset($purchase_order_request[$_name]) && $purchase_order_request[$_name] ? $purchase_order_request[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Purchase Order Request');
                $_objSheet->setCellValue('A1', 'LIST OF PURCHASE ORDER REQUEST');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
                }

                $_style = array(
                    'font' => array(
                        'bold' => TRUE,
                        'size' => 10,
                        'name' => 'Verdana'
                    )
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'purchase_order_request');
            }
        } else {

            $this->notify->error('No Record Found', 'purchase_order_request');
        }
    }

    function export_csv()
    {
        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? TRUE : FALSE;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Purchase Order Request CSV Template.csv';

            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'purchase_order_request');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $_group = $this->M_Purchase_order_request->as_array()->get_all();
                if ($_group) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($_group as $_dkey => $li) {

                            $_datas[$li['id']][$_title] = isset($li[$_title]) && ($li[$_title] !== '') ? $li[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Purchase Order Request');
            $_objSheet->setCellValue('A1', 'PURCHASE ORDER REQUEST');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function import()
    {

        $file = $_FILES['csv_file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $err = 0;


        foreach ($sheetData as $key => $upload_data) {

            if ($key > 0) {

                if ($this->input->post('status') == '1') {
                    $fields = array(
                        'name' => $upload_data[1],
                        /* ==================== begin: Add model fields ==================== */

                        /* ==================== end: Add model fields ==================== */
                    );

                    $purchase_order_request_id = $upload_data[0];
                    $purchase_order_request = $this->M_Purchase_order_request->get($purchase_order_request_id);

                    if ($purchase_order_request) {
                        $result = $this->M_Purchase_order_request->update($fields, $purchase_order_request_id);
                    }
                } else {

                    if (!is_numeric($upload_data[0])) {
                        $fields = array(
                            'name' => $upload_data[0],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Purchase_order_request->insert($fields);
                    } else {
                        $fields = array(
                            'name' => $upload_data[1],
                            /* ==================== begin: Add model fields ==================== */

                            /* ==================== end: Add model fields ==================== */
                        );

                        $result = $this->M_Purchase_order_request->insert($fields);
                    }
                }
                if ($result === FALSE) {

                    // Validation
                    $this->notify->error('Oops something went wrong.');
                    $err = 1;
                    break;
                }
            }
        }

        if ($err == 0) {
            $this->notify->success('CSV successfully imported.', 'purchase_order_request');
        } else {
            $this->notify->error('Oops something went wrong.');
        }

        header('Location: ' . base_url() . 'purchase_order_request');
        die();
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            $this->view_data['info'] = $info = $this->M_Purchase_order_request
                ->with_company()
                ->with_warehouse()
                ->with_accounting_ledger()
                ->with_approving_staff()
                ->with_requesting_staff()
                ->with_material_request()
                ->get($id);

            $this->load->model('purchase_order_request_item/Purchase_order_request_item_model', 'M_Purchase_order_request_item');

            $purchase_order_request_items = $this->M_Purchase_order_request_item
                ->where('purchase_order_request_id', $id)
                ->with_item_group()
                ->with_item_type()
                ->with_item_brand()
                ->with_item_class()
                ->with_item()
                ->with_unit_of_measurement()
                ->with_material_request()
                ->get_all();

            $this->view_data['purchase_order_request_items'] = $purchase_order_request_items;

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }
}
