<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Purchase Order Request</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="
                    <?php echo site_url('purchase_order_request/form/' . $data['id']); ?>"
                   class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('purchase_order_request'); ?>"
                   class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-3">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-widget13">

                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Reference
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['reference']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Date
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['request_date']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Company
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['company']['name']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Warehouse
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['warehouse']['name']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Accounting Ledger
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['accounting_ledger']['name']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Reason
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['request_reason']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Particulars
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo $data['particulars']; ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Status
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo rpo_status_lookup($data['request_status']); ?>
                            </span>
                        </div>
                        <div class="kt-widget13__item">
                            <span class="kt-widget13__desc">
                                Total Cost
                            </span>
                            <span class="kt-widget13__text kt-widget13__text--bold">
                                <?php echo money_php($data['total_cost']); ?>
                            </span>
                        </div>

                        <!-- ==================== end: Add model details ==================== -->
                    </div>
                    <!--end::Portlet-->
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Created
                                    </span>
                        <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['created_at']; ?> by <?php echo get_person_name($data['created_by'], 'staff'); ?></span>
                    </div>
                    <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Updated
                                    </span>
                        <span class="kt-widget13__text kt-widget13__text--bold"><?php echo $data['updated_at']; ?> by <?php echo get_person_name($data['updated_by'], 'staff'); ?></span>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
            <?php $this->load->view('modals/document_attachment') ?>
        </div>

        <div class="col-md-9">
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Items
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="table-responsive">
                                <table class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Group</th>
                                        <th>Type</th>
                                        <th>Class</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>Unit Cost</th>
                                        <th>Total Cost</th>
                                        <th>MR</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (sizeof($items) > 0): ?>
                                        <?php foreach ($items as $item): ?>
                                            <tr>
                                                <td><?= $item['item']['name'] ?></td>
                                                <td><?= $item['item_group']['name'] ?></td>
                                                <td><?= $item['item_type']['name'] ?></td>
                                                <td><?= $item['item_class']['name'] ?></td>
                                                <td><?= $item['unit_of_measurement']['name'] ?></td>
                                                <td><?= $item['quantity'] ?></td>
                                                <td><?= money_php($item['unit_cost']) ?></td>
                                                <td><?= money_php($item['total_cost']) ?></td>
                                                <td>
                                                    <?php if($item['material_request_id']):?>
                                                    <a href="<?= site_url('material_request/view/' . $item['material_request_id']); ?>">
                                                        <?= $item['material_request']['reference'] ?>
                                                    </a>
                                                    <?php endif?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="9">
                                                No Items
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Footer -->
