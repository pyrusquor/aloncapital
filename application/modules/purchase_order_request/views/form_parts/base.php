<script type="text/javascript">
    window.info_form_data = [];
    <?php foreach ($form_data as $key => $value) : ?>
        window.info_form_data['<?php echo $key; ?>'] = "<?php echo $value; ?>";
    <?php endforeach; ?>
    window.purchase_request_id = "<?php $id; ?>";
</script>
<!--begin: Basic Transaction Info-->
<div :class="wizardStepStateClass(1)" :data-ktwizard-state="wizardLabelState(1)">
    <?php $this->load->view('item'); ?>
</div>
<!--end: Basic Transaction Info-->

<!--begin: Item Form -->
<div :class="wizardStepStateClass(2)" :data-ktwizard-state="wizardLabelState(2)">
    <?php $this->load->view('info'); ?>
</div>
<!--end: Item Form -->

<!--begin: Form Actions -->
<div class="kt-form__actions">
    <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
        Previous
    </div>
    <button class="btn btn-success btn-md btn-tall btn-wide" type="submit" @click.prevent="confirmSubmitRequest()" v-if="info.form_is_valid && wizard.current == 2 && request_items.form_is_valid">
        Submit
    </button>
    <div id="next_btn" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" v-if="info.form_is_valid && wizard.current == 1" @click.prevent="wizardSwitchPane(2)">
        Next Step
    </div>
</div>
<!--end: Form Actions -->