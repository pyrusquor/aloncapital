<div class="row">
    <div class="container" v-if="material_requests.showFilter">
        <h4>Filter Material Requests</h4>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Request Status</label>
                    <?php echo form_dropdown('mr_status', Dropdown::get_static("mr_status"), null, 'class="form-control form-control-sm _filter" v-model="material_requests.filter.request_status"')?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <div class="label">Reference</div>
                    <input type="text" v-model="material_requests.filter.reference" class="form-control">
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Project</div>
                    <select v-model="material_requests.filter.project_id" class="form-control" @change="fetchObjects('sub_projects', 'project_id'); fetchObjects('properties', 'project_id')">
                        <option></option>
                        <option v-for="obj in sources.projects" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Sub Project</div>
                    <select v-model="material_requests.filter.sub_project_id" class="form-control">
                        <option></option>
                        <option v-for="obj in sources.sub_projects" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Property</div>
                    <select v-model="material_requests.filter.property_id" class="form-control" v-if="sources.properties.length > 0">
                        <option></option>
                        <option v-for="obj in sources.properties" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                    <div v-else>
                        Select Project...
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Amenity</div>
                    <select v-model="material_requests.filter.amenity_id" class="form-control">
                        <option></option>
                        <option v-for="obj in sources.amenities" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Sub Amenity</div>
                    <select v-model="material_requests.filter.sub_amenity_id" class="form-control">
                        <option></option>
                        <option v-for="obj in sources.sub_amenities" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Department</div>
                    <select v-model="material_requests.filter.department_id" class="form-control">
                        <option></option>
                        <option v-for="obj in sources.departments" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <div class="label">Vehicle/Equipment</div>
                    <select v-model="material_requests.filter.equipment_id" class="form-control">
                        <option></option>
                        <option v-for="obj in sources.equipments" :value="obj.id">
                            {{ obj.name }}
                        </option>
                    </select>
                </div>
            </div>

        </div>
        <div class="form-group">
            <a
                    class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                    href="#!"
                    @click.prevent="fetchMaterialRequests()"
            >
                Filter
            </a>
        </div>

        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Request Date</th>
                    <th>Reference</th>
                    <th>Status</th>
                    <th>Approving Staff</th>
                    <th>Requesting Staff</th>
                    <th>Items</th>
                </tr>
                </thead>
                <tbody>
                    <tr v-for="(mr, key) in material_requests.results">
                        <td>{{ mr.request_date }}</td>
                        <td>{{ mr.reference }}</td>
                        <td>{{ mr.request_status }}</td>
                        <td>{{ mr.approving_staff_display_name }}</td>
                        <td>{{ mr.requesting_staff_display_name }}</td>
                        <td>
                            <a class="btn btn-sm btn-success" href="#!" @click.prevent="fetchMaterialRequestItems(mr.id, key)">Load</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container" v-else>
        <h4>Material Request Items</h4>
        <a href="#!" class="btn btn-primary" @click.prevent="resetMaterialRequestItems()">&laquo; Back</a>
        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                    <th>Item</th>
                    <th>Group</th>
                    <th>Type</th>
                    <th>Brand</th>
                    <th>Class</th>
                    <th>Qty</th>
                    <th>Unit Cost</th>
                    <th>Total Cost</th>
                    <th>Add</th>
                </thead>
                <tbody v-if="material_requests.items">
                    <tr v-for="(mr_item, key) in material_requests.items">
                        <td>{{ mr_item.item.name }}</td>
                        <td>{{ mr_item.item_group.name }}</td>
                        <td>{{ mr_item.item_type.name }}</td>
                        <td>{{ mr_item.item_brand.name }}</td>
                        <td>{{ mr_item.item_class.name }}</td>
                        <td>{{ mr_item.quantity }}</td>
                        <td>{{ mr_item.unit_cost }}</td>
                        <td>{{ mr_item.total_cost }}</td>
                        <td>
                            <a href="#!" class="btn btn-sm btn-success" @click.prevent="addItemToPurchaseRequest(key)">
                                Add
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <hr>
    </div>
    <div class="container">
        <h4>Purchase Request Order Items</h4>
        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>MR</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Unit Cost</th>
                        <th>Total Cost</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody v-if="request_items.cart">
                    <tr v-for="(ri, key) in request_items.cart">
                        <td>
                            {{ ri.material_request_id }}
                        </td>
                        <td>
                            {{ ri.item_name }}
                        </td>
                        <td>
                            <input class="form-control" min="1" type="number" v-model="request_items.cart[key].quantity" @change="updateCartItemTotalCost(key)">
                        </td>
                        <td>
                            {{ ri.unit_cost }}
                        </td>
                        <td>
                            {{ ri.total_cost }}
                        </td>
                        <td>
                            <a href="#!" class="btn btn-danger btn-sm text-white" @click.prevent="removeItemFromPurchaseRequest(key)">
                                Remove
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <h5>Total Cost: {{ request_items.total_cost }}</h5>
            </div>
            <div class="form-group">

            </div>
        </div>
    </div>
</div>