<?php
    if ($id) {
        $approving_staff_info = get_person($form_data['approving_staff_id'], 'staff');
        $approving_staff_name = get_fname($approving_staff_info);

        $requesting_staff_id_info = get_person($form_data['requesting_staff_id'], 'staff');
        $requesting_staff_name = get_fname($requesting_staff_id_info);

        $company_data = get_object_from_table($form_data['company_id'], 'companies');
        $company_name = $company_data->name;

        $warehouse_data = get_object_from_table($form_data['warehouse_id'], 'warehouses');
        $warehouse_name = $warehouse_data->name;

    } else {
        $approving_staff_info = null;
        $approving_staff_name = '';
        $requesting_staff_id_info = null;
        $requesting_staff_name = '';
        $company_data = null;
        $company_name = '';
        $warehouse_data = null;
        $warehouse_name = '';
        if(isset($staff)){
            $form_data['requesting_staff_id'] = $staff['id'];
            $requesting_staff_name = $staff['last_name'] . ' ' . $staff['first_name'];
        }
        if(isset($approver)){
            $form_data['approving_staff_id'] = $approver['staff']['id'];
            $approving_staff_name = $approver['staff']['last_name'] . ' ' . $approver['staff']['first_name'];
        }

    }
?>
<div class="row">
    <div class="container">
        <div class="row">
            <?php /*
            <div class="col-sm-12">
                <div class="form-group">
                    <label>
                        Expense Account<span class="kt-font-danger">*</span>
                    </label>
                    <select class="form-control" name="accounting_ledger_id" id="accounting_ledger_id"
                            v-model="info.form.accounting_ledger_id" @change="checkInfoForm()">
                        <option v-for="ledger in sources.accounting_ledgers" :value="ledger.id">
                            {{ ledger.name }}
                        </option>
                    </select>
                </div>
            </div>
            */ ?>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Date of Request <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control kt_datepicker" placeholder="Date of Request"
                               name="request[request_date]" id="request_date"
                               v-model="info.form.request_date" readonly>
                        <span class="kt-input-icon__icon"></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label class="">Company <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="companies" id="company_id"
                            name="request[company_id]">
                        <?php if ($company_name): ?>
                            <option value="<?php echo $form_data['company_id']; ?>"
                                    selected><?php echo $company_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label class="">Warehouse <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="warehouses" id="warehouse_id"
                            name="request[warehouse_id]">
                        <?php if ($warehouse_name): ?>
                            <option value="<?php echo $form_data['warehouse_id']; ?>"
                                    selected><?php echo $warehouse_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Particulars</label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" placeholder="Particulars" name="particulars"
                               value="<?php echo $form_data['particulars'] ?>" v-model="info.form.particulars">
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Reason for RPO</label>
                    <div class="kt-input-icon">
                        <input type="text" class="form-control" placeholder="Reason for RPO" name="request_reason"
                               value="<?php echo $form_data['request_reason'] ?>" v-model="info.form.request_reason">
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label class="">Approving Staff <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person" id="approving_staff_id"
                            name="request[approving_staff_id]">
                        <?php if ($approving_staff_name): ?>
                            <option value="<?php echo $form_data['approving_staff_id']; ?>"
                                    selected>
                                <?php echo $approving_staff_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label class="">Requesting Staff <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="staff" data-type="person"
                            id="requesting_staff_id" name="request[requesting_staff_id]">
                        <?php if ($requesting_staff_name): ?>
                            <option value="<?php echo $form_data['requesting_staff_id']; ?>"
                                    selected><?php echo $requesting_staff_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>
    </div>
</div>