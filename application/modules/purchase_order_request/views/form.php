<script type="text/javascript">
    window.object_request_id = "<?php echo $id; ?>";
    window.object_request_fillables = {};
    <?php foreach ($fillables as $fillable) : ?>
        window.object_request_fillables["<?php echo $fillable; ?>"] = null;
    <?php endforeach; ?>
</script>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title"><?= $method ?> Purchase Order Request</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo base_url('purchase_order_request'); ?>" class="btn btn-label-instagram"><i class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>
<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="purchase_order_request_app">
    <div class="spinner-border text-success" role="status" v-if="! is_ready">
        <span class="sr-only">Loading...</span>
    </div>
    <div class="kt-portlet" v-else>
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                <div class="kt-grid__item">

                    <!--begin: Form Wizard Nav -->
                    <div class="kt-wizard-v3__nav">
                        <div class="kt-wizard-v3__nav-items">
                            <a class="kt-wizard-v3__nav-item" :data-ktwizard-state="wizardLabelState(1)">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label" @click.prevent="wizardSwitchPane(1)">
                                        <span>1</span> List of Items
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>
                            <a class="kt-wizard-v3__nav-item" :data-ktwizard-state="wizardLabelState(2)">
                                <div class="kt-wizard-v3__nav-body">
                                    <div class="kt-wizard-v3__nav-label" @click.prevent="wizardSwitchPane(2)">
                                        <span>2</span> General Information
                                    </div>
                                    <div class="kt-wizard-v3__nav-bar"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!--end: Form Wizard Nav -->
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                    <!--begin: Form Wizard Form-->
                    <form class="kt-form" method="POST" action="<?php form_open('purchase_order_request/form/' . @$id); ?>" id="form_purchase_order_request" enctype="multipart/form-data">
                        <?php $this->load->view('form_parts/base'); ?>
                    </form>

                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
</div>