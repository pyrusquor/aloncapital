<?php
    $activity_type_id = isset($info['activity_type_id']) && $info['activity_type_id'] ? $info['activity_type_id'] : '';

    $activity_number = isset($info['activity_number']) && $info['activity_number'] ? $info['activity_number'] : '';

    $driver_id = isset($info['driver_id']) && $info['driver_id'] ? $info['driver_id'] : '';

    $date = isset($info['date']) && $info['date'] ? $info['date'] : '';

    $vehicle_id = isset($info['vehicle_id']) && $info['vehicle_id'] ? $info['vehicle_id'] : '';

    $vehicle = isset($info['vehicle']) && $info['vehicle'] ? $info['vehicle']['name'] : '';

    $fleet = isset($info['fleet']) && $info['fleet'] ? $info['fleet'] : '';
?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Activity Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('activity_type_id', Dropdown::get_static('vehicle_activity_type'), set_value('activity_type_id', @$activity_type_id), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('activity_type_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Activity Number <span class="kt-font-danger">*</span></label>
            <input type="number" class="form-control" name="activity_number" value="<?php echo set_value('activity_number', @$activity_number); ?>" placeholder="Activity Number">

            <?php echo form_error('activity_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Driver <span class="kt-font-danger">*</span></label>
            <input type="number" class="form-control" name="driver_id" value="<?php echo set_value('driver_id', @$driver_id); ?>" placeholder="Driver">

            <?php echo form_error('driver_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Date <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker datePicker" name="date"
                    value="<?php echo set_value('date', @$date); ?>" placeholder="Date"
                    autocomplete="off" readonly>
            </div>

            <?php echo form_error('date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Vehicle <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="vehicles" id="vehicle_id" name="vehicle_id" required>
                <option value="">Select Vehicle</option>
                <?php if ($vehicle): ?>
                    <option value="<?php echo $vehicle_id; ?>" selected><?php echo $vehicle; ?></option>
                <?php endif ?>
            </select>

            <?php echo form_error('vehicle_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Fleet <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="fleet" value="<?php echo set_value('fleet', @$fleet); ?>" placeholder="Fleet">

            <?php echo form_error('fleet'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>