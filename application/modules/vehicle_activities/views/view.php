<?php
    $id = isset($vehicle_activities['id']) && $vehicle_activities['id'] ? $vehicle_activities['id'] : '';

    $activity_type_id = isset($vehicle_activities['activity_type_id']) && $vehicle_activities['activity_type_id'] ? $vehicle_activities['activity_type_id'] : '';

    $activity_number = isset($vehicle_activities['activity_number']) && $vehicle_activities['activity_number'] ? $vehicle_activities['activity_number'] : '';

    $driver_id = isset($vehicle_activities['driver_id']) && $vehicle_activities['driver_id'] ? $vehicle_activities['driver_id'] : '';

    $date = isset($vehicle_activities['date']) && $vehicle_activities['date'] ? $vehicle_activities['date'] : '';

    $vehicle_id = isset($vehicle_activities['vehicle_id']) && $vehicle_activities['vehicle_id'] ? $vehicle_activities['vehicle_id'] : '';

    $fleet = isset($vehicle_activities['fleet']) && $vehicle_activities['fleet'] ? $vehicle_activities['fleet'] : '';
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Vehicle Activity</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('vehicle_activities/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('vehicle_activities');?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">

                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Activity Type
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('vehicle_activity_type', $activity_type_id, 'view'); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Activity Number
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo @$activity_number; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Driver
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo @$driver_id; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Date
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo view_date(@$date); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Vehicle
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo @$vehicle_id; ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Fleet
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold"><?php echo @$fleet; ?></span>
                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->

        </div>

        <div class="col-md-6">

        </div>
    </div>
</div>
<!-- begin:: Footer -->
