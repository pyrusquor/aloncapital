<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vehicle_activities extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $vehicle_activities_model = array(
            'vehicle_activities/Vehicle_activities_model' => 'M_vehicle_activities',
            'vehicle/Vehicle_model' => 'M_vehicle',
        );

        // Load models
        $this->load->model($vehicle_activities_model);
        

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_vehicle_activities->fillable;
        $this->_table_columns = $this->M_vehicle_activities->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'is_active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];

        $this->load->helper('format');
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_vehicle_activities->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_vehicle_activities->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }

        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showVehicleActivities()
    {
        $output = ['data' => ''];

        $columnsDefault = [
            'id' => true,
            'activity_type_id'=> true,
            'activity_number'=> true,
            'driver_id'=> true,
            'date'=> true,
            'vehicle_id'=> true,
            'fleet'=> true,
            'date_ref'=> true,
            // Columns
        ];

        if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
            $columnsDefault = [];
            foreach ($_REQUEST['columnsDef'] as $field) {
                $columnsDefault[$field] = true;
            }
        }

        // get all raw data
        $vehicle_activities = $this->M_vehicle_activities->with_project()->as_array()->get_all();

        $vehicles = $this->M_vehicle->get_all();

        $data = [];

        if ($vehicle_activities) {

            foreach($vehicle_activities as $key => $value) {
                foreach($vehicles as $vehicle) {
                    if ($vehicle['id'] === @$value['vehicle_id']) {
                        $vehicle_activities[$key]['vehicle_id'] = $vehicle['name'];
                    }
                }
                $vehicle_activities[$key]['date'] = view_date(@$value['date']);
                $vehicle_activities[$key]['date_ref'] = $value['date'];
                
            }

            foreach ($vehicle_activities as $d) {
                $data[] = $this->filterArray($d, $columnsDefault);
            }

            // count data
            $totalRecords = $totalDisplay = count($data);

            // filter by general search keyword
            if (isset($_REQUEST['search'])) {
                $data = $this->filterKeyword($data, $_REQUEST['search']);
                $totalDisplay = count($data);
            }

            if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                foreach ($_REQUEST['columns'] as $column) {
                    if (isset($column['search'])) {
                        $data = $this->filterKeyword($data, $column['search'], $column['data']);
                        $totalDisplay = count($data);
                    }
                }
            }

            // sort
            if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                $_column = $_REQUEST['order'][0]['column'] - 1;
                $_dir = $_REQUEST['order'][0]['dir'];

                usort($data, function ($x, $y) use ($_column, $_dir) {

                    $x = array_slice($x, $_column, 1);

                    $x = array_pop($x);

                    $y = array_slice($y, $_column, 1);
                    $y = array_pop($y);

                    if ($_dir === 'asc') {

                        return $x > $y ? true : false;
                    } else {

                        return $x < $y ? true : false;
                    }
                });
            }

            // pagination length
            if (isset($_REQUEST['length'])) {
                $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
            }

            // return array values only without the keys
            if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                $tmp = $data;
                $data = [];
                foreach ($tmp as $d) {
                    $data[] = array_values($d);
                }
            }
            $secho = 0;
            if (isset($_REQUEST['sEcho'])) {
                $secho = intval($_REQUEST['sEcho']);
            }

            $output = array(
                'sEcho' => $secho,
                'sColumns' => '',
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalDisplay,
                'data' => $data,
            );

        }

        echo json_encode($output);
        exit();
    }

    public function form($id = false)
    {

        $method = "Create";
        if ($id) {$method = "Update";}

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $info = $this->input->post();

            if($id) {

                $result = $this->M_vehicle_activities->update($info, $id);

            } else {

                $result = $this->M_vehicle_activities->insert($info);
            }


            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Vehicle Activities successfully' . " " . $method, 'vehicle_activities');
            }
        }

        if ($id) {
            $this->view_data['info'] = $info = $this->M_vehicle_activities->with_vehicle()->get($id);
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['vehicle_activities'] = $this->M_vehicle_activities->get($id);

            if ($this->view_data['vehicle_activities']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_vehicle_activities->get($id);
            if ($list) {

                $deleted = $this->M_vehicle_activities->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Vehicle Activities Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_vehicle_activities->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Vehicle Activities/s Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

}