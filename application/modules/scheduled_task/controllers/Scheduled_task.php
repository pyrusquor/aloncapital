<?php
/**
 * CodeIgniter Migrate
 *
 * @author  Natan Felles <natanfelles@gmail.com>
 * @link    http://github.com/natanfelles/codeigniter-migrate
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Scheduled_task extends MX_Controller{
    public function __construct()
    {
        $this->load->model('property/Property_model', 'M_property');
        $this->load->model('sellers_reservations/sellers_reservations_model', 'M_sellers_reservations');
        $this->load->model('transaction/transaction_model', 'M_transaction');
        $this->load->model('transaction_payment/transaction_official_payment_model', 'M_transaction_official_payments');
        $this->load->model('aris/aris_model', 'M_aris_project_information');
        $this->load->model('aris/aris_payment_model', 'M_aris_payment');
        $this->load->model('aris/aris_document_model', 'M_aris_document');
        $this->load->model('aris/aris_lot_model', 'M_aris_lot');
    }

    public function check_reservations(){
        $this->input->is_cli_request() ?  : show_404();
        $this->db->where("expiration_date < '" . NOW . "'");
        $this->db->where("reservation_status = '1'");
        $reservations = $this->M_sellers_reservations->get_all();

        if($reservations){
            foreach($reservations as $key => $reservation){
                $property_id = $reservation['property_id'];
                $reservation_id = $reservation['id'];
                $property_data = [
                    'status' => 1,
                    'updated_at' => NOW
                ];
                $reservation_data = [
                    'reservation_status' => 4,
                    'updated_at' => NOW
                ];
                $this->M_property->update($property_data, $property_id);
                $this->M_sellers_reservations->update($reservation_data,$reservation_id);
            }
        }
    }
    public function rebate_amount_remove($project = 0){
        $this->input->is_cli_request() ?  : show_404();
        $ids = [];
        // $this->db->where('aris_id is not null');
        $transactions = $this->M_transaction->where(['project_id' => $project])->with_official_payments()->get_all();
        $official_payments = array_column($transactions, 'official_payments');
        foreach($official_payments as $key => $official_payment){
            foreach ($official_payment as $p_key => $payment){
                $amount_paid = $payment['amount_paid'] - $payment['rebate_amount'];
                $grand_total = $amount_paid + $payment['rebate_amount'];
                $amount_paid = number_format($amount_paid,2,'.','');
                $grand_total = number_format($grand_total,2,'.','');
                if($grand_total < $payment['grand_total']){
                    $ids[] = $payment['transaction_id'];
                }
                $this->M_transaction_official_payments->update([
                    'amount_paid' => $amount_paid,
                    'grand_total' => $grand_total,
                    'updated_at' => NOW
                ], $payment['id']);
                echo $payment['transaction_id'] . " " . $payment['grand_total'] . " " . $grand_total . " " . $payment['amount_paid'] . " " . $amount_paid . " \n" ;
            }
        }
        echo json_encode($ids);
        exit();

    }
    public function get_aris_ids(){
        $this->input->post('api_key') == 'SXm7qyE8kVM' ?  : show_403();
        $data = $this->M_aris_project_information->fields('id,projectname,filename')->get_all();
        foreach($data as $key => $datum){
            $this->db->where('project_id = ' . $datum['id']);
            $this->db->order_by('PaymentID', 'desc');
            $payment_data = $this->M_aris_payment->fields('PaymentID')->get()['PaymentID'];
            $data[$key]['payment_id'] = $payment_data;
        }
        echo json_encode($data);
        exit();
    }
    public function receive_aris_data(){
        $return['count'] = 0;
        $aris_data = json_decode($this->input->post('data'),true);
        $project_id = ['project_id' => $this->input->post('project_id')];
        foreach($aris_data as $key => $data){
            $aris_payment_status = $this->M_aris_payment->insert($data + $project_id + ['created_at' => NOW, 'created_by' => '']);
            if($aris_payment_status){
                $return['count'] += 1;
            }
        }
        echo json_encode($return);
        exit();
    }
    public function get_aris_ids_document(){
        $this->input->post('api_key') == 'SXm7qyE8kVM' ?  : show_403();
        $data = $this->M_aris_project_information->fields('id,projectname,filename')->get_all();
        foreach($data as $key => $datum){
            $this->db->where('project_id = ' . $datum['id']);
            $this->db->order_by('DocumentID', 'desc');
            $payment_data = $this->M_aris_document->fields('DocumentID')->get()['DocumentID'];
            $data[$key]['document_id'] = $payment_data;
        }
        echo json_encode($data);
        exit();
    }
    public function receive_aris_data_document(){
        $return['count'] = 0;
        $aris_data = json_decode($this->input->post('data'),true);
        $project_id = ['project_id' => $this->input->post('project_id')];
        foreach($aris_data as $key => $data){
            $aris_payment_status = $this->M_aris_document->insert($data + $project_id + ['created_at' => NOW, 'created_by' => '']);
            if($aris_payment_status){
                $return['count'] += 1;
            }
        }
        echo json_encode($return);
        exit();
    }
    public function get_aris_ids_lot(){
        $this->input->post('api_key') == 'SXm7qyE8kVM' ?  : show_403();
        $data = $this->M_aris_project_information->fields('id,projectname,filename')->get_all();
        foreach($data as $key => $datum){
            $this->db->where('project_id = ' . $datum['id']);
            $this->db->order_by('LotID', 'desc');
            $payment_data = $this->M_aris_lot->fields('LotID')->get()['LotID'];
            $data[$key]['lot_id'] = $payment_data;
        }
        echo json_encode($data);
        exit();
    }
    public function receive_aris_data_lot(){
        $return['count'] = 0;
        $aris_data = json_decode($this->input->post('data'),true);
        $project_id = ['project_id' => $this->input->post('project_id')];
        foreach($aris_data as $key => $data){
            $aris_payment_status = $this->M_aris_lot->insert($data + $project_id + ['created_at' => NOW, 'created_by' => '']);
            if($aris_payment_status){
                $return['count'] += 1;
            }
        }
        echo json_encode($return);
        exit();
    }
}