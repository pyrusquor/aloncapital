<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Sub_warehouse_inventory_model extends MY_Model
    {
        public $table = 'sub_warehouse_inventories'; // you MUST mention the table name
        public $primary_key = 'id';
        public $fillable = [
            "id",
            "created_by",
            "created_at",
            "updated_by",
            "updated_at",
            "deleted_by",
            "deleted_at",
            "warehouse_id",
            "sub_warehouse_id",
            "item_id",
            "unit_of_measurement_id",
            "actual_quantity",
            "available_quantity",
            "unit_price",

        ];
        public $form_fillables = [
            "id",
            "warehouse_id",
            "sub_warehouse_id",
            "item_id",
            "unit_of_measurement_id",
            "actual_quantity",
            "available_quantity",
            "unit_price",

        ];
        public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
        public $rules = [];

        public $fields = [


            "warehouse_id" => array(
                "field" => "warehouse_id",
                "label" => "Warehouse",
                "rules" => "numeric"
            ),

            "sub_warehouse_id" => array(
                "field" => "sub_warehouse_id",
                "label" => "Sub Warehouse",
                "rules" => "numeric"
            ),

            "item_id" => array(
                "field" => "item_id",
                "label" => "Item",
                "rules" => "numeric|required"
            ),

            "unit_of_measurement_id" => array(
                "field" => "unit_of_measurement_id",
                "label" => "Unit of Measurement",
                "rules" => "numeric|required"
            ),

            "actual_quantity" => array(
                "field" => "actual_quantity",
                "label" => "Actual Quantity",
                "rules" => "required|numeric"
            ),

            "available_quantity" => array(
                "field" => "available_quantity",
                "label" => "Available Quantity",
                "rules" => "required|numeric"
            ),

            "unit_price" => array(
                "field" => "unit_price",
                "label" => "Unit Price",
                "rules" => "numeric"
            ),

        ];

        public function __construct()
        {
            parent::__construct();

            $this->soft_deletes = true;
            $this->return_as = 'array';

            $this->rules['insert'] = $this->fields;
            $this->rules['update'] = $this->fields;


            $this->has_one["warehouse"] = array('foreign_model' => 'warehouse/warehouse_model', 'foreign_table' => 'warehouses', 'foreign_key' => 'id', 'local_key' => 'warehouse_id');


            $this->has_one["sub_warehouse"] = array('foreign_model' => 'sub_warehouse/sub_warehouse_model', 'foreign_table' => 'sub_warehouses', 'foreign_key' => 'id', 'local_key' => 'sub_warehouse_id');


            $this->has_one["item"] = array('foreign_model' => 'item/item_model', 'foreign_table' => 'item', 'foreign_key' => 'id', 'local_key' => 'item_id');


            $this->has_one["unit_of_measurement"] = array('foreign_model' => 'inventory_settings_unit_of_measurement/inventory_settings_unit_of_measurement_model', 'foreign_table' => 'inventory_settings_unit_of_measurements', 'foreign_key' => 'id', 'local_key' => 'unit_of_measurement_id');


        }

        function get_columns()
        {
            $_return = FALSE;

            if ($this->fillable) {
                $_return = $this->fillable;
            }

            return $_return;
        }

        public function insert_dummy()
        {
            require APPPATH . '/third_party/faker/autoload.php';
            $faker = Faker\Factory::create();

            $data = [];

            for ($x = 0; $x < 10; $x++) {
                array_push($data, array(
                    'name' => $faker->word,
                ));
            }
            $this->db->insert_batch($this->table, $data);

        }

    }