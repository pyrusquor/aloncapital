<div class="row">


    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <select class="form-control suggests" data-module="warehouses" id="warehouse_id"
                        name="id">
                    <?php if ($warehouse_id): ?>
                        <option value="<?php echo $warehouse_id['id']; ?>"
                                selected><?php echo $warehouse_id['name']; ?></option>
                    <?php endif ?>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Sub Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <select class="form-control" id="sub_warehouse_id" name="sub_warehouse_id"
                        v-model="info.form.sub_warehouse_id">
                    <option v-for="sub_warehouse in sources.sub_warehouses" :value="sub_warehouse.id">
                        {{ sub_warehouse.name }}
                    </option>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Item <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <select class="form-control suggests" data-module="item" id="item_id"
                        name="id">
                    <?php if ($item_id): ?>
                        <option value="<?php echo $item_id['id']; ?>"
                                selected><?php echo $item_id['name']; ?></option>
                    <?php endif ?>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Unit of Measurement <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <select class="form-control suggests" data-module="inventory_settings_unit_of_measurements"
                        id="unit_of_measurement_id"
                        name="id">
                    <?php if ($unit_of_measurement_id): ?>
                        <option value="<?php echo $unit_of_measurement_id['id']; ?>"
                                selected><?php echo $unit_of_measurement_id['name']; ?></option>
                    <?php endif ?>
                </select>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Actual Quantity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <input type="number" min="0" step="0.01" class="form-control" id="actual_quantity"
                       name="actual_quantity" placeholder="Actual Quantity" v-model="info.form.actual_quantity">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Available Quantity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <input type="number" min="0" step="0.01" class="form-control" id="available_quantity"
                       name="available_quantity" placeholder="Available Quantity"
                       v-model="info.form.available_quantity">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    

    <div class="col-sm-12 col-md-6">
        <div class="form-group my-3">
            <label>Unit Price <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">


                <input type="number" min="0" step="0.01" class="form-control" id="unit_price" name="unit_price"
                       placeholder="Unit Price" v-model="info.form.unit_price">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>

            
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>


</div>