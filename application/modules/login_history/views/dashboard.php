<?php if ($login_history):
    foreach ($login_history as $key => $history):
        $fullname = $history['user']['first_name'] . ' ' . $history['user']['last_name']; ?>
        <a href="javascript: void(0)" class="kt-notification-v2__item">
            <div class="kt-notification-v2__item-icon">
                <?php if (isset($history['staff'])): ?>
                    <?php if (get_image('staff', 'images', $history['staff']['image'])) : ?>
                        <img class="kt-widget__img"
                             src="<?php echo base_url(get_image('staff', 'images', $history['staff']['image'])); ?>"
                             width="50px" height="50px"/>
                    <?php else : ?>
                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                            <?php echo get_initials($history['staff']['first_name']) . '' . get_initials($history['staff']['last_name']); ?>
                        </div>
                    <?php endif; ?>
                <?php else: ?>
                    <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                        <?php echo get_initials($history['user']['first_name']) . '' . get_initials($history['user']['last_name']); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="kt-notification-v2__itek-wrapper">
                <div class="kt-notification-v2__item-title">
                    <?= $fullname; ?>
                </div>
                <div class="kt-notification-v2__item-desc">
                    <?= view_hrdate($history['created_at']); ?> - <?= $history['ip_address']; ?>
                </div>
            </div>
        </a>
    <?php endforeach ?>
<?php endif ?>
