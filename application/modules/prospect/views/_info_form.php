<?php
// Prospect Information
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$type_id = isset($info['type_id']) && $info['type_id'] ? $info['type_id'] : '';
$image = isset($info['image']) && $info['image'] ? $info['image'] : '';
$last_name = isset($info['last_name']) && $info['last_name'] ? $info['last_name'] : '';
$first_name = isset($info['first_name']) && $info['first_name'] ? $info['first_name'] : '';
$middle_name = isset($info['middle_name']) && $info['middle_name'] ? $info['middle_name'] : '';
$designation = isset($info['designation']) && $info['designation'] ? $info['designation'] : '';
$birth_place = isset($info['birth_place']) && $info['birth_place'] ? $info['birth_place'] : '';
$birth_date = isset($info['birth_date']) && $info['birth_date'] ? $info['birth_date'] : '';
$gender = isset($info['gender']) ? $info['gender'] : '';
$citizenship = isset($info['citizenship']) && $info['citizenship'] ? $info['citizenship'] : '';
$buyer_position = isset($info['position']['name']) && $info['position']['name'] ? $info['position']['name'] : '';
$civil_status_id = isset($info['civil_status_id']) && $info['civil_status_id'] ? $info['civil_status_id'] : '';
$nationality = isset($info['nationality']) && $info['nationality'] ? $info['nationality'] : '';
$housing_membership_id = isset($info['housing_membership_id']) && $info['housing_membership_id'] ? $info['housing_membership_id'] : '';
?>


<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Buyer Type <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[type_id]', Dropdown::get_static('buyer_type'), set_value('info[type_id]', @$type_id), 'class="form-control" id="type_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Lastname <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Last Name" name="info[last_name]" value="<?php echo set_value('info[last_name]', @$last_name); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Firstname <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="First Name" name="info[first_name]"  value="<?php echo set_value('info[first_name]"', @$first_name); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Middlename </label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Middlename" name="info[middle_name]" value="<?php echo set_value('info[middle_name]"', @$middle_name); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Birth Place <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Place of Birth" name="info[birth_place]" value="<?php echo set_value('info[birth_place]"', @$birth_place); ?>">
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Birth Date <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control datePicker" placeholder="Birth Date" name="info[birth_date]" value="<?php echo set_value('info[birth_date]"', @$birth_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Gender <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[gender]', Dropdown::get_static('sex'), set_value('info[gender]', @$gender), 'class="form-control" id="gender"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Civil Status <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[civil_status_id]', Dropdown::get_static('civil_status'), set_value('info[civil_status]', @$civil_status_id), 'class="form-control" id="civil_status_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Nationality <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[nationality]', Dropdown::get_static('nationality'), set_value('info[nationality]', @$nationality), 'class="form-control" id="nationality"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
            <label>Housing Membership <span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('info[housing_membership_id]', Dropdown::get_static('housing_membership'), set_value('info[housing_membership_id]', @$housing_membership_id), 'class="form-control" id="housing_membership_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>Profile Photo</label>
            <div>
                <?php if (get_image('prospect', 'images', $image)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('prospect', 'images', $image)); ?>" width="90px" height="90px"/>
                <?php endif;?>
                <span class="btn btn-sm">
                    <input type="file" name="prospect_image" class="" aria-invalid="false">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
