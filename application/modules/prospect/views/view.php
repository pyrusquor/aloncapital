<?php
// Buyer Information
$id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
$buyer_type_id = isset($info['type_id']) && $info['type_id'] ? $info['type_id'] : 'N/A';
$last_name = isset($info['last_name']) && $info['last_name'] ? $info['last_name'] : 'N/A';
$image = isset($info['image']) && $info['image'] ? $info['image'] : 'N/A';
$first_name = isset($info['first_name']) && $info['first_name'] ? $info['first_name'] : 'N/A';
$middle_name = isset($info['middle_name']) && $info['middle_name'] ? $info['middle_name'] : 'N/A';
$birth_place = isset($info['birth_place']) && $info['birth_place'] ? $info['birth_place'] : 'N/A';
$birth_date = isset($info['birth_date']) && $info['birth_date'] && (strtotime($info['birth_date']) > 0) ? date_format(date_create($info['birth_date']), 'F j, Y') : 'N/A';
$nationality = isset($info['nationality']) && $info['nationality'] ? $info['nationality'] : 'N/A';
$civil_status_id = isset($info['civil_status_id']) && $info['civil_status_id'] ? $info['civil_status_id'] : 'N/A';
$gender = isset($info['gender']) && $info['gender'] ? $info['gender'] : 'N/A';
$housing_membership_id = isset($info['housing_membership_id']) && $info['housing_membership_id'] ? $info['housing_membership_id'] : 'N/A';

// Contact Info
$present_address = isset($info['present_address']) && $info['present_address'] ? $info['present_address'] : 'N/A';
$mailing_address = isset($info['mailing_address']) && $info['mailing_address'] ? $info['mailing_address'] : 'N/A';
$business_address = isset($info['business_address']) && $info['business_address'] ? $info['business_address'] : 'N/A';
$mobile_no = isset($info['mobile_no']) && $info['mobile_no'] ? $info['mobile_no'] : 'N/A';
$other_mobile = isset($info['other_mobile']) && $info['other_mobile'] ? $info['other_mobile'] : 'N/A';
$email = isset($info['email']) && $info['email'] ? $info['email'] : 'N/A';
$landline = isset($info['landline']) && $info['landline'] ? $info['landline'] : 'N/A';
$social_media = isset($info['social_media']) && $info['social_media'] ? $info['social_media'] : 'N/A';

?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Prospect</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('prospect/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('prospect'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!--begin:: Portlet-->
    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <!-- <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                        <?php echo get_initials($first_name . ' ' . $last_name); ?>
                    </div> -->
                    <?php if (get_image('buyer', 'images', $image)): ?>
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<?php echo base_url(get_image('buyer', 'images', $image)); ?>" width="110px" height="110px" />
                        </div>
                    <?php else: ?>
                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                            <?php echo get_initials($first_name . ' ' . $last_name); ?>
                        </div>
                    <?php endif;?>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <span class="kt-widget__username"><?php echo ucwords($first_name . ' ' . $last_name); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab" aria-selected="true">
                            <i class="flaticon2-user-outline-symbol"></i> Basic Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#contact_information" role="tab" aria-selected="false">
                            <i class="flaticon-support"></i> Contact Info
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content kt-margin-t-20">
                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="basic_information" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row ">
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Buyer Type:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('buyer_type', $buyer_type_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Last Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($last_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Place of Birth:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($birth_place); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Civil Status:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('civil_status', $civil_status_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">&nbsp;</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">&nbsp;</span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">First Name:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($first_name); ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Birthdate:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $birth_date; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Nationality:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('nationality', $nationality, 'view'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">&nbsp;</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">&nbsp;</span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Middle Name:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($middle_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Gender:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ($gender == '1') ? 'Male' : 'Female'; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Housing Membership:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('housing_membership', $housing_membership_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="contact_information" role="tabpanel-2">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Mobile:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $mobile_no; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Email:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $email; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Landline:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $landline; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Other Mobile:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $other_mobile; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Present Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($present_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Mailing Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($mailing_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Business Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($business_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Social Media Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($social_media); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->
            </div>
        </div>
    </div>
</div>