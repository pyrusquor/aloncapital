<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Prospect extends MY_Controller
{
    private $fields = [
        array(
            'field' => 'info[type_id]',
            'label' => 'Buyer Type',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[last_name]',
            'label' => 'Last Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[first_name]',
            'label' => 'First Name',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[birth_place]',
            'label' => 'Birth Place',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[birth_date]',
            'label' => 'Birth Date',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[gender]',
            'label' => 'Gender',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'info[email]',
            'label' => 'Email',
            'rules' => 'trim|required|is_unique[users.email]',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Prospect_model', 'M_prospect');
        $this->load->model('buyer/Buyer_identification_model', 'M_buyer_identification');
        $this->load->model('buyer/Buyer_employment_model', 'M_buyer_employment');
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_prospect->fillable;
        $this->_table_columns = $this->M_prospect->__get_columns();
        $this->_table = 'prospect';
    }

    public function get_all()
    {
        $data['row'] = $this->M_buyer->fields(array('id', 'first_name', 'last_name', 'mobile_no', 'email'))->get_all();

        echo json_encode($data);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // Get record count
        // $conditions['returnType'] = 'count';
        $this->view_data['totalRec'] = $totalRec = $this->M_prospect->count_rows();

        // Pagination configuration
        $config['target'] = '#prospectContent';
        $config['base_url'] = base_url('prospect/paginationData');
        $config['total_rows'] = $totalRec;
        $config['per_page'] = $this->perPage;
        $config['link_func'] = 'ProspectPagination';

        // Initialize pagination library
        $this->ajax_pagination->initialize($config);

        // Get records
        $this->view_data['records'] = $this->M_prospect->get_all();
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            // Input from Advanced Filter
            $name = $this->input->post('name');
            $buyer_type_id = $this->input->post('buyer_type_id');
            $occupation_type_id = $this->input->post('occupation_type_id');
            $occupation_location_id = $this->input->post('occupation_location_id');
            $civil_status_id = $this->input->post('civil_status_id');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_prospect->with_employment()->count_rows();
            $where = array();

            // Pagination configuration
            $config['target'] = '#prospectContent';
            $config['base_url'] = base_url('prospect/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'ProspectPagination';

            // Query
            if (!empty($keyword)):
                $this->db->group_start();
                $this->db->like('last_name', $keyword, 'both');
                $this->db->or_like('first_name', $keyword, 'both');
                $this->db->or_like('middle_name', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($name)):
                $this->db->group_start();
                $this->db->like('last_name', $name, 'both');
                $this->db->or_like('first_name', $name, 'both');
                $this->db->or_like('middle_name', $name, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_type_id) && !empty($buyer_type_id)):
                $this->db->where('type_id', $buyer_type_id);
            endif;

            if (!empty($occupation_type_id) && !empty($occupation_type_id)):
                $this->db->where('occupation_type_id', $occupation_type_id);
            endif;

            if (!empty($occupation_location_id) && !empty($occupation_location_id)):
                $this->db->where('location_id', $occupation_location_id);
            endif;

            if (!empty($civil_status_id) && !empty($civil_status_id)):
                $this->db->where('civil_status_id', $civil_status_id);
            endif;

            $totalRec = $this->M_prospect->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)):
                $this->db->group_start();
                $this->db->like('last_name', $keyword, 'both');
                $this->db->or_like('first_name', $keyword, 'both');
                $this->db->or_like('middle_name', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($name)):
                $this->db->group_start();
                $this->db->like('last_name', $name, 'both');
                $this->db->or_like('first_name', $name, 'both');
                $this->db->or_like('middle_name', $name, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_type_id) && !empty($buyer_type_id)):
                $this->db->where('type_id', $buyer_type_id);
                $where['type_id'] = $buyer_type_id;
            endif;

            /*if ( ! empty($occupation_type_id) && ! empty($occupation_type_id) ):
            $this->db->where('occupation_type_id',$occupation_type_id); $where['occupation_type_id'] = $occupation_type_id;
            endif;

            if ( ! empty($occupation_location_id) && ! empty($occupation_location_id) ):
            $this->db->where('location_id',$occupation_location_id); $where['location_id'] = $location_id;
            endif;*/

            if (!empty($civil_status_id) && !empty($civil_status_id)):
                $this->db->where('civil_status_id', $civil_status_id);
                $where['civil_status_id'] = $civil_status_id;
            endif;

            $this->view_data['records'] = $records = $this->M_prospect->fields(array('id', 'first_name', 'last_name', 'mobile_no', 'email', 'image', 'present_address'))
                ->with_employment('fields: designation')
                ->limit($this->perPage, $offset)
                ->get_all();

            $this->load->view('prospect/_filter', $this->view_data, false);
        }
    }

    public function view($id = false)
    {
        $this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

        $this->js_loader->queue([
            '//www.amcharts.com/lib/3/amcharts.js',
            '//www.amcharts.com/lib/3/serial.js',
            '//www.amcharts.com/lib/3/radar.js',
            '//www.amcharts.com/lib/3/pie.js',
            '//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
            '//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
            '//www.amcharts.com/lib/3/plugins/export/export.min.js',
            '//www.amcharts.com/lib/3/themes/light.js',
        ]);

        if ($id) {

            $this->view_data['info'] = $this->M_prospect
                ->with_employment()
                ->get($id);

            $this->view_data['audit'] = $this->__get_audit($id);

            if ($this->view_data['info']) {
                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function create()
    {
        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);
            if ($this->form_validation->run() === true) {
                $post = $this->input->post();
                $buyer_info = $post['info'];
                // $buyer_employment = $post['work_exp'];

                if (!$this->M_auth->email_check($buyer_info['email'])) {

                    $buyerPwd = password_format($buyer_info['last_name'], $buyer_info['birth_date']);
                    $buyerEmail = $buyer_info['email'];

                    // Buyer Group ID
                    $buyerGroupID = ['4'];

                    $buyerAccAuth = array(
                        'first_name' => $buyer_info['first_name'],
                        'last_name' => $buyer_info['last_name'],
                        'active' => 1,
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    );

                    $userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $buyerAccAuth, $buyerGroupID);

                    if ($userID !== false) {
                        // Image Upload
                        $upload_path = './assets/uploads/prospect/images';
                        $config = array();
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = 5000;
                        $config['encrypt_name'] = true;
                        $this->load->library('upload', $config, 'prospect_image');
                        $this->prospect_image->initialize($config);

                        if (!empty($_FILES['prospect_image']['name'])) {

                            if (!$this->prospect_image->do_upload('prospect_image')) {

                                $this->notify->error($this->prospect_image->display_errors(), 'prospect/create');
                            } else {

                                $img_data = $this->prospect_image->data();

                                $buyer_info['image'] = $img_data['file_name'];
                            }
                        }

                        $additional = [
                            'is_active' => 1,
                            'user_id' => $userID,
                            'created_by' => $this->user->id,
                            'created_at' => NOW,
                        ];
                        $buyerID = $this->M_prospect->insert($buyer_info + $additional);

                        if ($buyerID !== false) {

                            // Insert Buyer Employment Info
                            // $buyer_employment['buyer_id'] = $buyerID;
                            // $this->M_buyer_employment->insert($buyer_employment);

                            // $refFields = array('type_of_id', 'id_number', 'date_issued', 'date_expiration', 'place_issued');

                            // $refData = [];

                            // foreach ($refFields as $field) {
                            //     foreach ($this->input->post($field) as $key => $value) {
                            //         if (!empty($value)) {
                            //             $refData[$key]['buyer_id'] = $buyerID;
                            //             $refData[$key][$field] = $value;
                            //         }
                            //     }
                            // }

                            // if ($refData) {
                            //     $this->M_buyer_identification->insert($refData);
                            // }

                            // Start emailing email and password to buyer here

                            $response['status'] = 1;
                            $response['message'] = 'Prospect Successfully Created!';
                        }
                    }
                } else {
                    $response['status'] = 0;
                    $response['message'] = 'Oops! Email Address already exists.';
                }

            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->template->build('create', $this->view_data);
    }

    public function update($id = false)
    {
        if ($id) {

            $this->view_data['info'] = $data = $this->M_prospect
                ->with_employment()
                ->with_identifications()
                ->get($id);

            // vdebug($data);

            if ($data) {

                $response['status'] = 0;
                $response['msg'] = 'Oops! Please refresh the page and try again.';

                if ($this->input->post()) {

                    $post = $this->input->post();

                    // vdebug($post);

                    $buyer_info = $post['info'];
                    // $buyer_employment = $post['work_exp'];

                    if ($this->M_auth->email_check($data['email'])) {

                        $oldPwd = password_format($data['last_name'], $data['birth_date']);
                        $newPwd = password_format($buyer_info['last_name'], $buyer_info['birth_date']);

                        if ($oldPwd !== $newPwd) {
                            // Update User Password
                            $this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
                        }

                        $oldEmail = $data['email'];
                        $newEmail = $buyer_info['email'];

                        if ($oldEmail !== $newEmail) {
                            // Update User Email
                            $_user_data = [
                                'email' => $newEmail,
                                'username' => $newEmail,
                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : null,
                                'updated_at' => NOW,
                            ];
                            $this->M_user->update($_user_data, array('id' => $data['user_id']));
                        }

                        // Update Buyer Info
                        $additional = [
                            'updated_by' => $this->user->id,
                            'updated_at' => NOW,
                        ];

                        $upload_path = './assets/uploads/prospect/images';
                        $config = array();
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = 5000;
                        $config['encrypt_name'] = true;
                        $this->load->library('upload', $config, 'prospect_image');
                        $this->prospect_image->initialize($config);

                        if (!empty($_FILES['prospect_image']['name'])) {
                            if (!$this->prospect_image->do_upload('prospect_image')) {

                                $this->notify->error($this->prospect_image->display_errors(), 'buyer/update');
                            } else {

                                if (file_exists($upload_path . '/' . $data['image'])) {

                                    unlink($upload_path . '/' . $data['image']);
                                }

                                $img_data = $this->prospect_image->data();

                                $buyer_info['image'] = $img_data['file_name'];
                            }
                        } else {

                            $buyer_info['image'] = $data['image'];
                        }

                        $update = $this->M_prospect->update($buyer_info + $additional, $data['id']);

                        if ($update !== false) {
                            $user_data = array(
                                'first_name' => $buyer_info['first_name'],
                                'last_name' => $buyer_info['last_name'],
                                'active' => $data['is_active'],
                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : null,
                                'updated_at' => NOW,
                            );

                            $this->M_user->update($user_data, $data['user_id']);

                            // Update Buyer Source of Info
                            // if (!empty($buyer_employment)) {
                            //     $this->M_buyer_employment->update($buyer_employment, array('buyer_id' => $data['id']));
                            // } else {
                            //     $buyer_employment['buyer_id'] = $data['id'];
                            //     $this->M_buyer_employment->insert($buyer_employment);
                            // }

                            // // Delete all Reference of buyer and Add again
                            // $this->M_buyer_identification->delete(array('buyer_id' => $data['id']));

                            // Insert Reference
                            // $refFields = array('type_of_id', 'id_number', 'date_issued', 'date_expiration', 'place_issued');
                            // $refData = [];

                            // foreach ($refFields as $field) {
                            //     foreach ($this->input->post($field) as $key => $value) {
                            //         if (!empty($value)) {
                            //             $refData[$key]['buyer_id'] = $data['id'];
                            //             $refData[$key][$field] = $value;
                            //         }
                            //     }
                            // }
                            // if ($refData) {
                            //     $this->M_buyer_identification->insert($refData);
                            // }

                            // Start emailing email and password to buyer here

                            $response['status'] = 1;
                            $response['message'] = 'Buyer Account Successfully Update!';

                        } else {

                            $response['status'] = 0;
                            $response['message'] = 'Oops! Something went wrong. Please refresh the page and try again.';
                        }

                    } else {

                        $response['status'] = 0;
                        $response['message'] = 'Oops! Email Address already exists.';
                    }

                    echo json_encode($response);
                    exit();
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_prospect->get($id);
            if ($list) {

                $deleted = $this->M_prospect->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Buyer successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_prospect->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Buyer successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function checkMail()
    {
        if ($this->input->is_ajax_request()) {

            $response = false;
            $key = $this->input->post('info[key]');
            $email = $this->input->post('info[email]');

            if (!empty($key)) {
                $existing_email = $this->M_user->where('email =', $key)->get();

                if ($email == $existing_email['email']) {
                    $response = true;
                } else {
                    $check = $this->M_user->where('email', $email)->get();
                    // Email doesnt exists / or not yet taken
                    if ($check == false) {
                        $response = true;
                    }
                }
            } else {

                $check = $this->M_user->where('email', $email)->get();
                // Email doesnt exists / or not yet taken
                if ($check == false) {
                    $response = true;
                }
            }
            echo json_encode($response);
        } else {

            show_404();
        }

    }

    public function fetchBuyerPositions()
    {
        if ($this->input->is_ajax_request()) {
            $items = [];
            $sales_group_id = $this->input->post('id');

            $datas = $this->M_buyer_position->fields('id, name')->where('sales_group_id', $sales_group_id)->as_object()->get_all();

            if ($datas) {
                foreach ($datas as $key => $data) {

                    $items[$data->id]['id'] = $data->id;
                    $items[$data->id]['name'] = $data->name;
                }

                echo json_encode($items);
                exit();
            }
        } else {

            show_404();
        }
    }

    public function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];
        $_extra_datas = [];
        $_adatas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $buyers = $this->M_prospect
            ->with_employment()
            ->with_identifications()
            ->get_all();

        // vdebug($buyers);

        if ($buyers) {

            foreach ($buyers as $skey => $buyer) {

                $_datas[$buyer['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_prospects' . date('m_d_y_h-i-s', time()) . '.xls';

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');
                $_additional_column = $this->input->post('_additional_column');

                if ($_export_column) {
                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
                }
            }

            if ($_db_columns) {

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id') && ($_name !== 'user_id')) {

                        if ((strpos($_name, 'employment') !== false)) {

                            $_extra_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($buyers as $skey => $buyer) {

                                $_extra_datas[$buyer['id']][$_title][0] = isset($buyer[$_name]) && $buyer[$_name] ? $buyer[$_name] : '';
                            }
                        } elseif ((strpos($_name, 'identifications') !== false)) {
                            $_extra_titles[] = $_title = isset($_name) && $_name ? $_name : '';

                            foreach ($buyers as $skey => $buyer) {

                                $_extra_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? $buyer[$_name] : '';
                            }
                        } else {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($buyers as $skey => $buyer) {

                                if ($_column === 'birth_date') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] && (strtotime($buyer[$_column]) > 0) ? date_format(date_create($buyer[$_column]), 'm/d/Y') : '';

                                } elseif ($_column === 'gender') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('sex', $buyer[$_column], 'view') : '';

                                } elseif ($_column === 'nationality') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('nationality', $buyer[$_column], 'view') : '';

                                } elseif ($_column === 'civil_status_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('civil_status', $buyer[$_column], 'view') : '';

                                } elseif ($_column === 'housing_membership_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('housing_membership', $buyer[$_column], 'view') : '';

                                } elseif ($_column === 'is_active') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('bool', $buyer[$_column], 'view') : '';

                                } elseif ($_column === 'type_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('buyer_type', $buyer[$_column], 'view') : '';

                                } else {
                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? $buyer[$_name] : '';
                                }

                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                $_xls_columns = array_combine($_alphas, $_titles);
                $_lastAlpha = end($_alphas);

                if (empty($_extra_datas)) {
                    foreach ($_xls_columns as $_xkey => $_column) {

                        $_title = ucwords(strtolower($_column));

                        $_objSheet->setCellValue($_xkey . $_row, $_title);
                        $_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                }

                $_objSheet->setTitle('List of Prospects');
                $_objSheet->setCellValue('A1', 'LIST OF PROSPECTS');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                $col = 1;

                foreach ($buyers as $key => $buyer) {

                    $buyer_id = isset($buyer['id']) && $buyer['id'] ? $buyer['id'] : '';

                    if (!empty($_extra_datas)) {

                        foreach ($_xls_columns as $_xkey => $_column) {

                            $_title = ucwords(strtolower($_column));

                            $_objSheet->setCellValue($_xkey . $_start, $_title);

                            $_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        }

                        $_start++;
                    }
                    // PRIMARY INFORMATION COLUMN
                    foreach ($_alphas as $_akey => $_alpha) {
                        $_value = isset($_datas[$buyer_id][$_xls_columns[$_alpha]]) && $_datas[$buyer_id][$_xls_columns[$_alpha]] ? $_datas[$buyer_id][$_xls_columns[$_alpha]] : '';
                        $_objSheet->setCellValue($_alpha . $_start, $_value);
                    }

                    // ADDITIONAL INFORMATION COLUMN
                    if (!empty($_extra_datas)) {
                        $_start += 2;

                        $_addtional_columns = $_extra_titles;
                        // echo "<pre>";
                        // print_r($_extra_datas);
                        // die();
                        foreach ($_addtional_columns as $adkey => $_a_column) {

                            // MAIN TITLE OF ADDITIONAL DATA

                            if ($_a_column === 'employment') {
                                $ad_title = 'Employment Information';
                            } else if ($_a_column === 'identifications') {
                                $ad_title = 'Proof of Identification';
                            } else {
                                $ad_title = $_a_column;
                            }

                            $a_title = ucwords(str_replace('_', ' ', strtolower($ad_title)));

                            $_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

                            // Style
                            $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
                            $_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                            // LOOP DATAS
                            if (strpos($_a_column, 'employment') !== false) {

                                if (!empty($_extra_datas[$buyer_id][$_a_column])) {
                                    $refno = 1;

                                    foreach ($_extra_datas[$buyer_id][$_a_column] as $rkey => $ref) {

                                        $ref_col = $ref;
                                        $_start++;

                                        $_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);
                                        $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
                                        $_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

                                        foreach ($ref_col as $reftitles => $k) {

                                            if ((strpos($reftitles, 'buyer_id')) === false) {

                                                $ref_title = str_replace('_id', ' ', $reftitles);

                                                if ($reftitles === 'occupation_type_id') {
                                                    $k = isset($k) && $k ? Dropdown::get_static('occupation_type', $k, 'view') : '';
                                                }
                                                if ($reftitles === 'occupation_id') {
                                                    $k = isset($k) && $k ? Dropdown::get_static('occupation', $k, 'view') : '';
                                                }
                                                if ($reftitles === 'location_id') {
                                                    $k = isset($k) && $k ? Dropdown::get_static('occupation_location', $k, 'view') : '';
                                                }
                                                if ($reftitles === 'industry_id') {
                                                    $k = isset($k) && $k ? Dropdown::get_static('industry', $k, 'view') : '';
                                                }

                                                $_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($ref_title));
                                                $_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($k));
                                            }

                                            $_start++;
                                        }
                                    }
                                } else {
                                    $_start++;

                                    $_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

                                    // Style
                                    $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
                                    $_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                                    $_start += 1;
                                }
                            } elseif (strpos($_a_column, 'identifications') !== false) {
                                if (!empty($_extra_datas[$buyer_id][$_a_column])) {

                                    foreach ($_extra_datas[$buyer_id][$_a_column] as $ackey => $acad) {

                                        $acad_col = array_flip($acad);

                                        $_start++;

                                        $_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

                                        $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
                                        $_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

                                        foreach ($acad_col as $acolkey => $acadtitles) {

                                            if ((strpos($acadtitles, '_id')) === false) {
                                                $acad_title = str_replace('_', ' ', $acadtitles);
                                                $_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($acad_title));

                                                $_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
                                            }

                                            $_start++;
                                        }
                                    }
                                } else {
                                    $_start++;

                                    $_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

                                    // Style
                                    $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
                                    $_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                                    $_start += 1;
                                }
                            }

                            $_start++;
                        }
                    }

                    $_start += 1;

                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'prospect');
            }
        } else {

            $this->notify->error('No Record Found', 'prospect');
        }
    }

    public function export_csv()
    {

        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? true : false;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Buyer CSV Template.csv';

            // $_fillables    =    $this->M_document->fillable;
            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === false) && (strpos($_fill, 'updated_') === false) && (strpos($_fill, 'deleted_') === false && ($_fill !== 'user_id'))) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $records = $this->M_prospect->as_array()->get_all(); #up($_documents);
                if ($records) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($records as $_dkey => $record) {

                            $_datas[$record['id']][$_title] = isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Buyers');
            $_objSheet->setCellValue('A1', 'BUYERS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
            }

            $_style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Verdana',
                ),
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    public function import()
    {

        if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

            // if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
            if (true) {

                $_tmp_name = $_FILES['csv_file']['tmp_name'];
                $_name = $_FILES['csv_file']['name'];

                set_time_limit(0);

                $_columns = [];
                $_datas = [];

                $_failed_reasons = [];
                $_inserted = 0;
                $_updated = 0;
                $_failed = 0;

                /**
                 * Read Uploaded CSV File
                 */
                try {

                    $_file_type = PHPExcel_IOFactory::identify($_tmp_name);
                    $_objReader = PHPExcel_IOFactory::createReader($_file_type);
                    $_objPHPExcel = $_objReader->load($_tmp_name);
                } catch (Exception $e) {

                    $_msg = 'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

                    $this->notify->error($_msg, 'document');
                }

                $_objWorksheet = $_objPHPExcel->getActiveSheet();
                $_highestColumn = $_objWorksheet->getHighestColumn();
                $_highestRow = $_objWorksheet->getHighestRow();
                $_sheetData = $_objWorksheet->toArray();
                if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

                    if ($_sheetData[1][0] === 'id') {

                        $_columns[] = 'id';
                    }

                    // $_fillables    =    $this->M_document->fillable;
                    $_fillables = $this->_table_fillables;
                    if (!$_fillables) {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
                    }

                    foreach ($_fillables as $_fkey => $_fill) {

                        if (in_array($_fill, $_sheetData[1])) {

                            $_columns[] = $_fill;
                        } else {

                            continue;
                        }
                    }

                    foreach ($_sheetData as $_skey => $_sd) {

                        if ($_skey > 1) {

                            if (count(array_filter($_sd)) !== 0) {

                                $_datas[] = array_combine($_columns, $_sd);
                            }
                        } else {

                            continue;
                        }
                    }

                    if (isset($_datas) && $_datas) {

                        foreach ($_datas as $_dkey => $_data) {
                            $_id = isset($_data['id']) && $_data['id'] ? $_data['id'] : false;
                            $_data['birth_date'] = isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
                            $buyerGroupID = ['3'];

                            if ($_id) {
                                $data = $this->M_prospect->get($_id);
                                if ($data) {

                                    unset($_data['id']);

                                    $oldPwd = password_format($data['last_name'], $data['birth_date']);
                                    $newPwd = password_format($_data['last_name'], $_data['birth_date']);

                                    $oldEmail = $data['email'];
                                    $newEmail = $_data['email'];

                                    if ($this->M_auth->email_check($oldEmail)) {

                                        if ($oldPwd !== $newPwd) {
                                            // Update User Password
                                            $this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
                                        }

                                        if ($oldEmail !== $newEmail) {
                                            // Update User Email
                                            $_user_data = [
                                                'email' => $newEmail,
                                                'username' => $newEmail,
                                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : null,
                                                'updated_at' => NOW,
                                            ];
                                            $this->M_user->update($_user_data, array('id' => $data['user_id']));
                                        }

                                        // Update Buyer Info
                                        $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                        $_data['updated_at'] = NOW;

                                        $_update = $this->M_buyer->update($_data, $_id);
                                        if ($_update !== false) {

                                            $user_data = array(
                                                'first_name' => $_data['first_name'],
                                                'last_name' => $_data['last_name'],
                                                'active' => $_data['is_active'],
                                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : null,
                                                'updated_at' => NOW,
                                            );

                                            $this->M_user->update($user_data, $data['user_id']);

                                            $_updated++;
                                        } else {

                                            $_failed_reasons[$_data['id']][] = 'update not working.';
                                            $_failed++;

                                            break;
                                        }

                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'email already used';
                                        $_failed++;

                                        break;
                                    }

                                } else {

                                    // Generate user password
                                    $buyerPwd = password_format($_data['last_name'], $_data['birth_date']);
                                    $buyerEmail = $_data['email'];

                                    if (!$this->M_auth->email_check($_data['email'])) {

                                        $user_data = array(
                                            'first_name' => $_data['first_name'],
                                            'last_name' => $_data['last_name'],
                                            'active' => $_data['is_active'],
                                            'created_by' => $this->user->id,
                                            'created_at' => NOW,
                                        );

                                        $userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $user_data, $buyerGroupID);

                                        if ($userID !== false) {

                                            $_data['user_id'] = $userID;
                                            $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                            $_data['created_at'] = NOW;
                                            $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                            $_data['updated_at'] = NOW;

                                            $_insert = $this->M_buyer->insert($_data);
                                            if ($_insert !== false) {

                                                $_inserted++;
                                            } else {

                                                $_failed_reasons[$_data['id']][] = 'insert buyer not working';
                                                $_failed++;

                                                break;
                                            }

                                        } else {

                                            $_failed_reasons[$_data['id']][] = 'insert ion auth not working';
                                            $_failed++;

                                            break;
                                        }

                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'email check already existing';
                                        $_failed++;

                                        break;
                                    }
                                }
                            } else {

                                // Generate user password
                                $buyerPwd = password_format($_data['last_name'], $_data['birth_date']);
                                $buyerEmail = $_data['email'];

                                if (!$this->M_auth->email_check($_data['email'])) {

                                    $user_data = array(
                                        'first_name' => $_data['first_name'],
                                        'last_name' => $_data['last_name'],
                                        'active' => $_data['is_active'],
                                        'created_by' => $this->user->id,
                                        'created_at' => NOW,
                                    );

                                    $userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $user_data, $buyerGroupID);

                                    if ($userID !== false) {

                                        $_data['user_id'] = $userID;
                                        $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                        $_data['created_at'] = NOW;
                                        $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : null;
                                        $_data['updated_at'] = NOW;

                                        $_insert = $this->M_buyer->insert($_data);
                                        if ($_insert !== false) {

                                            $_inserted++;
                                        } else {

                                            $_failed_reasons[$_dkey][] = 'insert buyer not working';

                                            $_failed++;

                                            break;
                                        }

                                    } else {

                                        $_failed_reasons[$_dkey][] = 'insert ion auth not working';

                                        $_failed++;

                                        break;
                                    }

                                } else {

                                    $_failed_reasons[$_dkey][] = 'email check already existing';

                                    $_failed++;

                                    break;

                                }
                            }
                        }

                        $_msg = '';
                        if ($_inserted > 0) {

                            $_msg = $_inserted . ' record/s was successfuly inserted';
                        }

                        if ($_updated > 0) {

                            $_msg .= ($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
                        }

                        if ($_failed > 0) {
                            $this->notify->error('Upload Failed! Please follow upload guide. ', 'prospect');
                        } else {

                            $this->notify->success($_msg . '.', 'prospect');
                        }
                    }
                } else {

                    $this->notify->warning('CSV was empty.', 'prospect');
                }
            } else {

                $this->notify->warning('Not a CSV file!', 'prospect');
            }
        } else {

            $this->notify->error('Something went wrong!', 'prospect');
        }
    }

}
