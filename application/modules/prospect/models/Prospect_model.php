<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Prospect_model extends MY_Model
{

    public $table = 'prospect'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'user_id',
        'type_id',
        'image',
        'last_name',
        'first_name',
        'middle_name',
        'birth_place',
        'birth_date',
        'gender',
        'nationality',
        'civil_status_id',
        'housing_membership_id',
        'present_address',
        'mailing_address',
        'business_address',
        'other_mobile',
        'mobile_no',
        'email',
        'landline',
        'social_media',
        'is_active',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['employment'] = array('foreign_model' => 'buyer/buyer_employment_model', 'foreign_table' => 'buyer_employment_table', 'foreign_key' => 'buyer_id', 'local_key' => 'id');

        // $this->has_many['identifications'] = array('foreign_model' => 'buyer/buyer_identification_model', 'foreign_table' => 'buyer_identifications', 'foreign_key' => 'buyer_id', 'local_key' => 'id');
    }

}
