<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaction_past_due_notice_model extends MY_Model
{

    public $table = 'transaction_past_due_notices'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'transaction_id',
        'past_due_date',
        'notice',
        'is_void',
        'is_email_sent',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update

    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;
        $this->return_as = 'array';

        // Pagination
        $this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
        $this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['transaction'] = array('foreign_model' => 'transaction/Transaction_model', 'foreign_table' => 'transactions', 'foreign_key' => 'id', 'local_key' => 'transaction_id');

        $this->has_one['creator'] = array('foreign_model' => 'user/User_model', 'foreign_table' => 'users', 'foreign_key' => 'id', 'local_key' => 'created_by');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function get_search_pdn($date_range = [])
    {
        $result = [];
        if (count($date_range) > 1) {
            $this->db->where("transaction_past_due_notices.past_due_date between '$date_range[0]' and '$date_range[1]'");
        }
        $this->db->select('transaction_past_due_notices.*');
        $this->db->select('transactions.project_id as transaction_project_id');
        $this->db->select('transactions.reference as transaction_reference');
        $this->db->select('transactions.property_id as transaction_property_id');
        $this->db->select('transactions.buyer_id as transaction_buyer_id');
        $this->db->join('transactions', 'transaction_past_due_notices.transaction_id=transactions.id', 'left');
        $this->db->where('transaction_past_due_notices.deleted_at is null');
        $this->db->where('transactions.deleted_at is null');
        $query = $this->db->get('transaction_past_due_notices')->result_array();

        if ($query) {
            $result = $query;
        }

        return $result;
    }
}
