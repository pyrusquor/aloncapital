<!DOCTYPE html>
<html>

<head>
    <?php
    $header = isset($company['header']) && $company['header'] ? $company['header'] : "";
    $footer = isset($company['footer']) && $company['footer'] ? $company['footer'] : "";
    ?>
    <title>FIRST NOTICE</title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        @media print {
            .page-break {
                page-break-before: always;
            }
        }
    </style>
</head>

<body style="max-width: 1050px;">

    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">

        <!--begin:: Portlet-->
        <div class="kt-portlet">

            <div class="kt-portlet__body m-5 p-5">
                <?php

                $buyer = isset($transaction['buyer']) && $transaction['buyer'] ? $transaction['buyer']['last_name'] . ', ' . $transaction['buyer']['first_name'] . ' ' . $transaction['buyer']['middle_name'] : "";

                $buyer_address = isset($transaction['buyer']['present_address']) && $transaction['buyer']['present_address'] ? $transaction['buyer']['present_address'] : "";

                $mobile_no = isset($transaction['buyer']['mobile_no']) && $transaction['buyer']['mobile_no'] ? $transaction['buyer']['mobile_no'] : "";

                $project_name = isset($transaction['project']['name']) && $transaction['project']['name'] ? $transaction['project']['name'] : "";

                $project_location = isset($transaction['project']['location']) && $transaction['project']['location'] ? $transaction['project']['location'] : "";

                $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : "";

                $property_block = isset($transaction['property']['block']) && $transaction['property']['block'] ? $transaction['property']['block'] : "";

                $property_lot = isset($transaction['property']['lot']) && $transaction['property']['lot'] ? $transaction['property']['lot'] : "";

                $property_lot_area = isset($transaction['property']['lot_area']) && $transaction['property']['lot_area'] ? $transaction['property']['lot_area'] : "";

                $month_overdue = floor($this->transaction_library->payment_status(1)['days'] / 30);

                ?>

                <div class="text-center" style="line-height: 0.8;">
                    <h4 class="font-weight-bold"><?= $company['name'] ?></h4>
                    <p><?= $company['location'] ?></p>
                    <p>Phone No.: <?= $company['contact_number'] ?>, Email: <?= $company['email'] ?></p>
                    <p>Website: <a href="https://<?= $company['website'] ?>"><?= $company['website'] ?></a></p>
                </div>

                <br><br>

                <div class="text-center">
                    <p style="font-size: 16px;"><u>CREDIT & COLLECTION DEPARTMENT</u></p>
                </div>

                <br><br>

                <div class="text-right">
                    <?= view_date() ?>
                </div>

                <br><br><br>

                <div>
                    <p><strong><?= @$buyer ?></strong></p>
                    <p><?= @$buyer_address ?></p>
                    <p><?= @$mobile_no ?></p>
                </div>

                <br><br><br>

                <div class="text-center">
                    <h5 class="font-weight-bold">
                        NOTICE OF PAST DUE
                    </h5>
                </div>

                <br><br>

                <div>
                    <p>Dear Sir/Madam:</p>
                </div>

                <div>
                    <div>
                        <p>
                            Please be informed that as per our company record as of <b><u><?= view_date() ?></u></b>, you have not paid <strong><u><?= number_to_words($month_overdue) ?></u> (<?= $month_overdue ?>)</strong> monthly amortization in the total amount of <strong><u><?= number_to_words($this->transaction_library->total_amount_due()) ?></u> PESOS ONLY (<u><?= generate($transaction['id'], 'total_amount_due') ?></u>)</strong>, exclusive of penalty charges, for Block <strong><u><?= @$property_block ?></u></strong> Lot <strong><u><?= @$property_lot ?></u></strong> of &nbsp;<strong><u><?= @$project_name ?></u></strong> at <?= @$project_location ?>, subject of our Contract to Sell.
                        </p>

                        <br>

                        <p>
                            We know that you are a very busy person and that you might have just overlooked paying this account. <u>We are therefore reminding you that as of today, your account is considered past due.</u>
                        </p>

                        <br>

                        <p>
                            We will appreciate your visit by presenting this letter to _______________, _______________, to update or settle this unpaid account within <b>Fifteen (15)</b> days from receipt hereof; otherwise, we would be constrained to institute the cancellation of your <b>Contract to Sell</b> in accordance with the provisions of <b>Republic Act no. 6552</b> (Realty Installment Buyer Protection Act or “Maceda Law”).
                        </p>

                        <br>

                        <p>
                            However, if you have already paid or settled this account prior to receipt hereof, kindly ignore this notice.
                        </p>
                    </div>

                    <br><br><br>

                    <div>
                        <p>Very truly yours,</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>