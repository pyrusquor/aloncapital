<!DOCTYPE html>
<html>

<head>
<?php
$header = isset($company['header']) && $company['header'] ? $company['header'] : "";
$footer = isset($company['footer']) && $company['footer'] ? $company['footer'] : "";
?>
    <title>FIRST NOTICE</title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 15px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th {
            padding: 5px 8px;
        }

        .table>thead>tr>th {
            border-bottom: 2px solid #00529c;
        }

        .table>tfoot>tr>td {
            border-top: 2px solid #00529c;
        }

        .table>tfoot {
            border-top: 2px solid #ddd;
        }

        .table>tbody {
            border-bottom: 1px solid #fff;
        }

        .table.table-striped>thead>tr>th {
            background: #00529c;
            color: #fff;
            border: 0;
            padding: 12px 8px;
            text-transform: uppercase;
        }

        .table.table-striped>thead>tr>th a {
            color: #fff;
            font-weight: 400;
        }

        .table.table-striped>thead>tr:nth-child(2)>th {
            background: #0075de;
        }

        .table.table-striped td {
            border: 0;
            vertical-align: middle;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background: #fff;
        }

        .table-striped>tbody>tr:nth-of-type(even) {
            background: #f1f1f1;
        }

        .color-bluegreen {
            color: #169f98;
        }

        .color-white {
            color: #fff;
        }

        .peso_currency {
            font-family: DejaVu Sans;
        }

        .bg-bluegreen {
            background-color: #169f98;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .margin0 {
            margin: 0;
        }

        .padding10 {
            padding: 10px;
        }

        p {
            margin: 0px;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-warning {
            background-color: #fcf8e3;
            border-color: #faebcc;
            color: #8a6d3b;
        }

        .d-grid {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            gap: 1rem 3rem;
        }

        input[type='text'],
        select.form-control {
            background: transparent;
            border: none;
            border-bottom: 1px solid #000000;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        input[type='text']:focus,
        select.form-control:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>

<body style="max-width: 1050px;">

    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">

        <!--begin:: Portlet-->
        <div class="kt-portlet">
        <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'header', $header)); ?>" width="950px" height="150px" style='position:relative;margin-bottom:-70px'/>

            <div class="kt-portlet__body m-5 p-5">
                <?php

                $buyer = isset($transaction['buyer']) && $transaction['buyer'] ? $transaction['buyer']['last_name'] . ', ' . $transaction['buyer']['first_name'] . ' ' . $transaction['buyer']['middle_name'] : "";

                $buyer_address = isset($transaction['buyer']['present_address']) && $transaction['buyer']['present_address'] ? $transaction['buyer']['present_address'] : "";

                $mobile_no = isset($transaction['buyer']['mobile_no']) && $transaction['buyer']['mobile_no'] ? $transaction['buyer']['mobile_no'] : "";

                $project_name = isset($transaction['project']['name']) && $transaction['project']['name'] ? $transaction['project']['name'] : "";

                $project_location = isset($transaction['project']['location']) && $transaction['project']['location'] ? $transaction['project']['location'] : "";

                $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : "";

                $property_block = isset($transaction['property']['block']) && $transaction['property']['block'] ? $transaction['property']['block'] : "";

                $property_lot = isset($transaction['property']['lot']) && $transaction['property']['lot'] ? $transaction['property']['lot'] : "";

                $month_overdue = floor($this->transaction_library->payment_status(1)['days'] / 30);

                ?>

                <p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: right;" align="right"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; font-family: 'Times New Roman','serif';">1<sup>st</sup> Notice</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center;" align="center">&nbsp;</p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center;" align="center"><u><span lang="EN-US" style="font-family: 'Times New Roman','serif';">CREDIT &amp; COLLECTION DEPARTMENT</span></u></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt;">&nbsp;</p>
                <p style="text-align: right;">
                    <u>
                        <strong>
                            <span style="font-family: 'times new roman', times, serif;">
                                <?= view_date() ?>
                            </span>
                        </strong>
                    </u>
                </p>
                <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: right;">&nbsp;</p>
                <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: right;">&nbsp;</p>
                <p>
                    <span style="font-family: 'times new roman', times, serif;">
                        <strong>
                            <?= @$buyer ?>
                        </strong>
                    </span>
                </p>
                <p>
                    <strong>
                        <span style="font-family: 'times new roman', times, serif;">
                            <?= @$buyer_address ?>
                        </span>
                    </strong>
                </p>
                <p>
                    <strong>
                        <span style="font-family: 'times new roman', times, serif;">
                            <?= @$mobile_no ?>
                        </span>
                    </strong>
                </p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><span style="font-family: 'times new roman', times, serif;">Subject: <u>REMINDER AND NOTICE OF PAST DUE</u></span><u></u><u></u></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><span style="font-family: 'times new roman', times, serif;">Dear Sir/Madam:</span></p>
                <p>&nbsp;</p>
                <p>
                    <span style="font-family: 'times new roman', times, serif;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please be informed that as of <strong><u><?= view_date() ?></u></strong> you have not paid <strong><u><?= number_to_words($month_overdue) ?></u> (<?= $month_overdue ?>)</strong> monthly amortization in the total amount of <strong><u><?= number_to_words($this->transaction_library->total_amount_due()) ?></u> PESOS ONLY (<u><?= generate($transaction['id'], 'total_amount_due') ?></u>)</strong>, exclusive of penalty charges, for Block &nbsp;<strong><u><?= @$property_block ?></u></strong> Lot <strong><u><?= @$property_lot ?></u></strong> of &nbsp;<strong><u><?= @$project_name ?></u></strong> at <?= @$project_location ?> subject of our Contract to Sell, and other relevant &amp; material contracts or agreement between the parties.
                    </span>
                </p>
                <p>&nbsp;</p>
                <p><span style="font-family: 'times new roman', times, serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><u>We hereby advise you that as of today, your account is considered past due.</u></strong></span></p>
                <p>&nbsp;</p>
                <p>
                    <span style="font-family: 'times new roman', times, serif;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please present this letter to Ms. Judith O. Alforte, Accounts Receivable Supervisor, to update or settle this unpaid account within seven (7) days from receipt hereof. Otherwise, we will be constrained to institute the cancellation of our Contract to Sell, and other relevant &amp; material contracts or agreement entered into in accordance with existing laws.
                    </span>
                </p>
                <p>&nbsp;</p>
                <p><span style="font-family: 'times new roman', times, serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>However, if you have already paid or settled this account prior to receipt hereof, kindly ignore this notice</strong>.</span></p>
                <p>&nbsp;</p>
                <p><span style="font-family: 'times new roman', times, serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Thank you.</span></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><span style="font-family: 'times new roman', times, serif;">Very truly yours,</span></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><span style="font-family: 'times new roman', times, serif;"><strong>GENALYN C. BAGUIO</strong></span></p>
                <p><span style="font-family: 'times new roman', times, serif;"><u>Credit &amp; Collection Manager</u></span></p>

            </div>
            <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'footer', $footer)); ?>" width="950px" height="150px" style='position:relative;margin-top:-70px'/>
        </div>

    </div>
</body>

</html>