<!DOCTYPE html>
<html>

<head>
    <?php
    $header = isset($company['header']) && $company['header'] ? $company['header'] : "";
    $footer = isset($company['footer']) && $company['footer'] ? $company['footer'] : "";
    ?>
    <title>FIRST NOTICE</title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        @media print {
            .page-break {
                page-break-before: always;
            }
        }
    </style>
</head>

<body style="max-width: 1050px;">

    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">

        <!--begin:: Portlet-->
        <div class="kt-portlet">

            <div class="kt-portlet__body m-5 p-5">
                <?php

                $buyer = isset($transaction['buyer']) && $transaction['buyer'] ? $transaction['buyer']['last_name'] . ', ' . $transaction['buyer']['first_name'] . ' ' . $transaction['buyer']['middle_name'] : "";

                $buyer_address = isset($transaction['buyer']['present_address']) && $transaction['buyer']['present_address'] ? $transaction['buyer']['present_address'] : "";

                $mobile_no = isset($transaction['buyer']['mobile_no']) && $transaction['buyer']['mobile_no'] ? $transaction['buyer']['mobile_no'] : "";

                $project_name = isset($transaction['project']['name']) && $transaction['project']['name'] ? $transaction['project']['name'] : "";

                $project_location = isset($transaction['project']['location']) && $transaction['project']['location'] ? $transaction['project']['location'] : "";

                $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : "";

                $property_block = isset($transaction['property']['block']) && $transaction['property']['block'] ? $transaction['property']['block'] : "";

                $property_lot = isset($transaction['property']['lot']) && $transaction['property']['lot'] ? $transaction['property']['lot'] : "";

                $property_lot_area = isset($transaction['property']['lot_area']) && $transaction['property']['lot_area'] ? $transaction['property']['lot_area'] : "";

                $month_overdue = floor($this->transaction_library->payment_status(1)['days'] / 30);

                ?>

                <div class="text-center" style="line-height: 0.8;">
                    <h4 class="font-weight-bold"><?= $company['name'] ?></h4>
                    <p><?= $company['location'] ?></p>
                    <p>Phone No.: <?= $company['contact_number'] ?>, Email: <?= $company['email'] ?></p>
                    <p>Website: <a href="https://<?= $company['website'] ?>"><?= $company['website'] ?></a></p>
                </div>

                <br><br>

                <div class="text-center">
                    <p style="font-size: 16px;"><u>CREDIT & COLLECTION DEPARTMENT</u></p>
                </div>

                <br><br>

                <div class="text-right">
                    <?= view_date() ?>
                </div>

                <br><br><br>

                <div>
                    <p><strong><?= @$buyer ?></strong></p>
                    <p><?= @$buyer_address ?></p>
                    <p><?= @$mobile_no ?></p>
                </div>

                <br><br><br>

                <div class="text-center">
                    <h5 class="font-weight-bold">
                        DEMAND LETTER
                    </h5>
                </div>

                <br><br>

                <div>
                    <p>Dear Sir/Madam:</p>
                </div>

                <div>
                    <div>
                        <p>
                            We wish to remind you that your account has neither been settled nor updated despite receipt of your <b>Notice of Past Due</b>.
                        </p>

                        <br>

                        <p>
                            Please be informed that as per our company record as of <u><?= view_date() ?></u>, your <strong><u><?= number_to_words($month_overdue) ?></u> (<?= $month_overdue ?>)</strong> periodic amortization for Block <u><b><?= $property_block ?></b></u> Lot <u><b><?= $property_lot ?></b></u> of (project) <u><b><?= $property_name ?></b></u> at <?= $project_location ?>, in the total amount of <strong><u><?= number_to_words($this->transaction_library->total_amount_due()) ?></u> PESOS ONLY (<u><?= generate($transaction['id'], 'total_amount_due') ?></u>)</strong>, exclusive of penalty charges, has already become delinquent.
                        </p>

                        <br>

                        <p>
                            We are, therefore, sending this <b>DEMAND LETTER</b> for you to settle and/or update your account within a grace period of ____ ( ____ ) days from receipt hereof as provided for in <b>Republic Act No. 6552</b> (Maceda Law); otherwise, we will proceed with the <b>NOTARIAL CANCELLATION AND/OR RESCISSION</b> of your <b>CONTRACT TO SELL</b> pursuant to the provisions of <b>Republic Act No. 6552</b> (Maceda Law).
                        </p>

                        <br>

                        <p>
                            Please present this letter to ___________, _____________, to secure a clearance for proper documentation prior to payment.
                        </p>

                        <br>

                        <p>
                            <b><i>* If you have already settled or updated your account prior to receipt hereof, please ignore this letter.</i></b>
                        </p>
                    </div>

                    <br><br><br>

                    <div>
                        <p>Very truly yours,</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>