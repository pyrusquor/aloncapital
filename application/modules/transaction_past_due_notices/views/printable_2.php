<!DOCTYPE html>
<html>
<?php
$header = isset($company['header']) && $company['header'] ? $company['header'] : "";
$footer = isset($company['footer']) && $company['footer'] ? $company['footer'] : "";
?>
<head>
    <title>SECOND NOTICE</title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 15px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th {
            padding: 5px 8px;
        }

        .table>thead>tr>th {
            border-bottom: 2px solid #00529c;
        }

        .table>tfoot>tr>td {
            border-top: 2px solid #00529c;
        }

        .table>tfoot {
            border-top: 2px solid #ddd;
        }

        .table>tbody {
            border-bottom: 1px solid #fff;
        }

        .table.table-striped>thead>tr>th {
            background: #00529c;
            color: #fff;
            border: 0;
            padding: 12px 8px;
            text-transform: uppercase;
        }

        .table.table-striped>thead>tr>th a {
            color: #fff;
            font-weight: 400;
        }

        .table.table-striped>thead>tr:nth-child(2)>th {
            background: #0075de;
        }

        .table.table-striped td {
            border: 0;
            vertical-align: middle;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background: #fff;
        }

        .table-striped>tbody>tr:nth-of-type(even) {
            background: #f1f1f1;
        }

        .color-bluegreen {
            color: #169f98;
        }

        .color-white {
            color: #fff;
        }

        .peso_currency {
            font-family: DejaVu Sans;
        }

        .bg-bluegreen {
            background-color: #169f98;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .margin0 {
            margin: 0;
        }

        .padding10 {
            padding: 10px;
        }

        p {
            margin: 0px;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-warning {
            background-color: #fcf8e3;
            border-color: #faebcc;
            color: #8a6d3b;
        }

        .d-grid {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            gap: 1rem 3rem;
        }

        input[type='text'],
        select.form-control {
            background: transparent;
            border: none;
            border-bottom: 1px solid #000000;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        input[type='text']:focus,
        select.form-control:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>

<body style="max-width: 1050px;">
    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <!--begin:: Portlet-->
        <div class="kt-portlet">
        <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'header', $header)); ?>" width="950px" height="150px" style='position:relative;margin-bottom:-70px'/>

            <div class="kt-portlet__body m-5 p-5">
                <?php

                $buyer = isset($transaction['buyer']) && $transaction['buyer'] ? $transaction['buyer']['last_name'] . ', ' . $transaction['buyer']['first_name'] . ' ' . $transaction['buyer']['middle_name'] : "";

                $buyer_address = isset($transaction['buyer']['present_address']) && $transaction['buyer']['present_address'] ? $transaction['buyer']['present_address'] : "";

                $mobile_no = isset($transaction['buyer']['mobile_no']) && $transaction['buyer']['mobile_no'] ? $transaction['buyer']['mobile_no'] : "";

                $project_name = isset($transaction['project']['name']) && $transaction['project']['name'] ? $transaction['project']['name'] : "";

                $project_location = isset($transaction['project']['location']) && $transaction['project']['location'] ? $transaction['project']['location'] : "";

                $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : "";

                $property_block = isset($transaction['property']['block']) && $transaction['property']['block'] ? $transaction['property']['block'] : "";

                $property_lot = isset($transaction['property']['lot']) && $transaction['property']['lot'] ? $transaction['property']['lot'] : "";

                $month_overdue = floor($this->transaction_library->payment_status(1)['days'] / 30);

                ?>

                <p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: right;" align="right"><span lang="EN-US" style="font-size: 12.0pt; line-height: 115%; font-family: 'Times New Roman','serif';">2<sup>nd</sup> Notice</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center;" align="center">&nbsp;</p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center;" align="center"><u><span lang="EN-US" style="font-family: 'Times New Roman','serif';">CREDIT &amp; COLLECTION DEPARTMENT</span></u></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt;">&nbsp;</p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: right;" align="right">
                    <strong>
                        <span lang="EN-US" style="font-family: 'Times New Roman','serif'; text-decoration: underline;">
                            <?= view_date() ?>
                        </span>
                    </strong>
                </p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;">
                    <strong>
                        <span style="font-family: 'times new roman', times, serif;">
                            <?= @$buyer ?>
                        </span>
                    </strong>
                </p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;">
                    <strong>
                        <span style="font-family: 'times new roman', times, serif;">
                            <?= @$buyer_address ?>
                        </span>
                    </strong>
                </p>
                <p>
                    <strong>
                        <span style="font-family: 'times new roman', times, serif;">
                            <?= @$mobile_no ?>
                        </span>
                    </strong>
                </p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">Dear Sir/ Madam:</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;">
                    <span lang="EN-US" style="font-family: 'Times New Roman','serif';">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; We wish to remind you that your account has not been updated despite receipt of your <strong>First Notice (Reminder and Notice of Past Due).</strong>
                    </span>
                </p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please be informed that as of <strong><span style="text-decoration: underline;"><?= view_date() ?></span>&nbsp;</strong>your <strong><u><?= number_to_words($month_overdue) ?></u></strong> <strong>(<?= $month_overdue ?>)</strong> periodic amortizations for Block <span style="font-family: 'Times New Roman','serif';"><strong><?= @$property_block ?></strong></span> Lot <span style="font-family: 'Times New Roman','serif';"><strong><?= @$property_lot ?></strong></span> of <span style="text-decoration: underline;"><?= @$project_name ?></span><strong>&nbsp;</strong>at <?= @$project_location ?>, in the total amount of <strong><span style="text-decoration: underline;"><?= number_to_words($this->transaction_library->total_amount_due()) ?></span><u>&nbsp;PESOS ONLY</u>&nbsp;(<span style="text-decoration: underline;"><?= generate($transaction['id'], 'total_amount_due') ?></span>)</strong>, exclusive of penalty charges, has been delinquent.<u></u></span>
                </p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; We are, therefore, sending this FINAL NOTICE for you to update your account within seven (7) days from receipt hereof. Otherwise, we will proceed with the </span><strong><span lang="EN-US" style="font-family: 'Times New Roman','serif'; color: windowtext;">NOTICE OF CANCELLATION BASED ON CONTRACT TO SELL, AND OTHER RELEVANT &amp; MATERIAL CONTRACTS OR AGREEMENTS ENTERED INTO AND/OR RESCISSION OF THE CONTRACT BY A NOTARIAL ACT</span></strong><span lang="EN-US" style="font-family: 'Times New Roman','serif'; color: windowtext;"> pursuant to the </span><span lang="EN-US" style="font-family: 'Times New Roman','serif';">provisions stipulated under Republic Act 655 (Maceda Law), if applicable. </span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please present this letter to Ms. Judith O. Alforte, Accounts Receivable Supervisor, to secure a clearance for proper documentation prior to payment.</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>If you have already settled or updated your account prior to receipt hereof, please ignore this letter.</strong></span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Thank you.</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-align: justify;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">Very truly yours,</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-indent: .5in;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-indent: .5in;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNormal" style="margin-bottom: .0001pt; text-indent: .5in;"><span lang="EN-US" style="font-family: 'Times New Roman','serif';">&nbsp;</span></p>
                <p class="MsoNoSpacing" style="text-align: justify;"><strong><span style="font-family: 'Times New Roman','serif';">&nbsp;</span></strong></p>
                <p class="MsoNormal" style="margin-bottom: 2.3pt; line-height: normal;"><strong><span lang="EN-US" style="font-family: 'Times New Roman','serif';">GENALYN C. BAGUIO</span></strong></p>
                <p><span lang="EN-US" style="font-size: 11.0pt; line-height: 115%; font-family: 'Times New Roman','serif'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; color: #00000a; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;">Credit &amp; Collection Manager</span></p>

            </div>
            <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'footer', $footer)); ?>" width="950px" height="150px" style='position:relative;margin-top:-70px'/>
        </div>
    </div>
</body>

</html>