<!DOCTYPE html>
<html>

<head>

<?php
$header = isset($company['header']) && $company['header'] ? $company['header'] : "";
$footer = isset($company['footer']) && $company['footer'] ? $company['footer'] : "";
?>
    <title>NOTICE FOR CANCELLATION OF RESERVATION</title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 15px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th {
            padding: 5px 8px;
        }

        .table>thead>tr>th {
            border-bottom: 2px solid #00529c;
        }

        .table>tfoot>tr>td {
            border-top: 2px solid #00529c;
        }

        .table>tfoot {
            border-top: 2px solid #ddd;
        }

        .table>tbody {
            border-bottom: 1px solid #fff;
        }

        .table.table-striped>thead>tr>th {
            background: #00529c;
            color: #fff;
            border: 0;
            padding: 12px 8px;
            text-transform: uppercase;
        }

        .table.table-striped>thead>tr>th a {
            color: #fff;
            font-weight: 400;
        }

        .table.table-striped>thead>tr:nth-child(2)>th {
            background: #0075de;
        }

        .table.table-striped td {
            border: 0;
            vertical-align: middle;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background: #fff;
        }

        .table-striped>tbody>tr:nth-of-type(even) {
            background: #f1f1f1;
        }

        .color-bluegreen {
            color: #169f98;
        }

        .color-white {
            color: #fff;
        }

        .peso_currency {
            font-family: DejaVu Sans;
        }

        .bg-bluegreen {
            background-color: #169f98;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .margin0 {
            margin: 0;
        }

        .padding10 {
            padding: 10px;
        }

        p {
            margin: 0px;
        }

        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .alert-warning {
            background-color: #fcf8e3;
            border-color: #faebcc;
            color: #8a6d3b;
        }

        .d-grid {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            gap: 1rem 3rem;
        }

        input[type='text'],
        select.form-control {
            background: transparent;
            border: none;
            border-bottom: 1px solid #000000;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        input[type='text']:focus,
        select.form-control:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>

<body style="max-width: 1050px;">
    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <!--begin:: Portlet-->
        <div class="kt-portlet">
        <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'header', $header)); ?>" width="950px" height="150px" style='position:relative;margin-bottom:-70px'/>
            <div class="kt-portlet__body m-5 p-5">
                <?php

                $buyer = isset($transaction['buyer']) && $transaction['buyer'] ? $transaction['buyer']['last_name'] . ', ' . $transaction['buyer']['first_name'] . ' ' . $transaction['buyer']['middle_name'] : "";

                $buyer_address = isset($transaction['buyer']['present_address']) && $transaction['buyer']['present_address'] ? $transaction['buyer']['present_address'] : "";

                $mobile_no = isset($transaction['buyer']['mobile_no']) && $transaction['buyer']['mobile_no'] ? $transaction['buyer']['mobile_no'] : "";

                $project_name = isset($transaction['project']['name']) && $transaction['project']['name'] ? $transaction['project']['name'] : "";

                $project_location = isset($transaction['project']['location']) && $transaction['project']['location'] ? $transaction['project']['location'] : "";

                $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : "";

                $property_block = isset($transaction['property']['block']) && $transaction['property']['block'] ? $transaction['property']['block'] : "";

                $property_lot = isset($transaction['property']['lot']) && $transaction['property']['lot'] ? $transaction['property']['lot'] : "";

                $month_overdue = floor($this->transaction_library->payment_status(1)['days'] / 30);

                ?>

                <p><strong>NOTICE FOR CANCELLATION OF RESERVATION</strong></p>
                <p><strong>&nbsp;</strong></p>
                <p><em>&nbsp;To:&nbsp;&nbsp; REBG &ndash; Admin</em></p>
                <p><em>Date: <span style="text-decoration: underline;"><?= view_date() ?></span></em></p>
                <p><em>&nbsp;</em></p>
                <p><em>&nbsp;</em></p>
                <p>We wish to inform your department that this reservation has been cancelled</p>
                <p>Effective <em><span style="text-decoration: underline;"><?= view_date() ?> </span></em>as stated below:</p>
                <p>(Date)</p>
                <p>Name of Client : <span style="text-decoration: underline;"><?= @$buyer ?></span></p>
                <p>RA No. :&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="text-decoration: underline;"><?= @$transaction['reference'] ?></span></p>
                <p>Project:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="text-decoration: underline;"><?= @$project_name ?></span></p>
                <p>Block/Lot:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="text-decoration: underline;"><?= @$property_block ?> / <?= @$property_lot ?></span></p>
                <p>Reason:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _________________________________________________________</p>
                <p>&nbsp;</p>
                <p>PREPARED BY:&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; APPROVED BY:</p>
                <p>&nbsp;</p>
                <p>______________________________&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; _____________________________</p>
                <p>Department Head&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Credit and Collection</p>
                <p>SHJDC Corporate Sales Division /THR</p>

            </div>
            <img class="kt-widget__img" src="<?php echo base_url(get_image('company', 'footer', $footer)); ?>" width="950px" height="150px" style='position:relative;margin-top:-70px'/>
        </div>
    </div>
</body>

</html>