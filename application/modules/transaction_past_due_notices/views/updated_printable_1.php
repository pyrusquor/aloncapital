<!DOCTYPE html>
<html>

<head>
    <?php
    $header = isset($company['header']) && $company['header'] ? $company['header'] : "";
    $footer = isset($company['footer']) && $company['footer'] ? $company['footer'] : "";
    ?>
    <title>FIRST NOTICE</title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        @media print {
            .page-break {
                page-break-before: always !important;
            }
        }
    </style>
</head>

<body style="max-width: 1050px;">

    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">

        <!--begin:: Portlet-->
        <div class="kt-portlet">

            <div class="kt-portlet__body m-5 p-5">
                <?php

                $buyer = isset($transaction['buyer']) && $transaction['buyer'] ? $transaction['buyer']['last_name'] . ', ' . $transaction['buyer']['first_name'] . ' ' . $transaction['buyer']['middle_name'] : "";

                $buyer_address = isset($transaction['buyer']['present_address']) && $transaction['buyer']['present_address'] ? $transaction['buyer']['present_address'] : "";

                $mobile_no = isset($transaction['buyer']['mobile_no']) && $transaction['buyer']['mobile_no'] ? $transaction['buyer']['mobile_no'] : "";

                $project_name = isset($transaction['project']['name']) && $transaction['project']['name'] ? $transaction['project']['name'] : "";

                $project_location = isset($transaction['project']['location']) && $transaction['project']['location'] ? $transaction['project']['location'] : "";

                $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : "";

                $property_block = isset($transaction['property']['block']) && $transaction['property']['block'] ? $transaction['property']['block'] : "";

                $property_lot = isset($transaction['property']['lot']) && $transaction['property']['lot'] ? $transaction['property']['lot'] : "";

                $property_lot_area = isset($transaction['property']['lot_area']) && $transaction['property']['lot_area'] ? $transaction['property']['lot_area'] : "";

                $month_overdue = floor($this->transaction_library->payment_status(1)['days'] / 30);

                ?>

                <div class="text-center" style="line-height: 0.8;">
                    <h4 class="font-weight-bold"><?= $company['name'] ?></h4>
                    <p><?= $company['location'] ?></p>
                    <p>Phone No.: <?= $company['contact_number'] ?>, Email: <?= $company['email'] ?></p>
                    <p>Website: <a href="https://<?= $company['website'] ?>"><?= $company['website'] ?></a></p>
                </div>

                <br><br><br>

                <div>
                    <p><?= view_date() ?></p>
                    <p><strong><?= @$buyer ?></strong></p>
                    <p><?= @$buyer_address ?></p>
                    <p><?= @$mobile_no ?></p>
                </div>

                <br><br><br>

                <div class="text-center">
                    <u>
                        <h5 class="font-weight-bold">
                            NOTICE OF CANCELLATION
                            <br>OF
                            <br>RESERVATION AGREEMENT
                        </h5>
                    </u>
                </div>

                <br><br>

                <div>
                    <div>
                        <div>
                            <p>
                                This notice is being served by me in my capacity as Manager, External Affairs of Legal Services Department of <b><?= $company['name'] ?></b> with office address at <?= $company['location'] ?>.
                            </p>
                            <br>
                            <p>
                                Our records show that on <u><?= view_date($transaction['reservation_date']) ?></u> you have executed a <b>Reservation Agreement</b> with our company, a photocopy of which is attached hereto as <b>Annex "A"</b> concerning a <span class="text-danger">(commercial/ residential)</span> property more particularly described as follows;
                            </p>
                        </div>
                    </div>
                    <br>
                    <div>
                        <table class="w-100 table-bordered">
                            <thead class="table" style="background-color: rgb(59,56,56); color: gray;">
                                <tr>
                                    <th>Block No.</th>
                                    <th>Lot No.</th>
                                    <th>Title No.</th>
                                    <th>Area</th>
                                    <th>Project No.</th>
                                    <th>Project Location</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="p-2">
                                        <?= $property_block ?>
                                    </td>
                                    <td class="p-2">
                                        <?= $property_lot ?>
                                    </td>
                                    <td class="p-2">
                                        <?= $property_name ?>
                                    </td>
                                    <td class="p-2">
                                        <?= $property_lot_area ?>
                                    </td>
                                    <td class="p-2">
                                        <?= $project_name ?>
                                    </td>
                                    <td class="p-2">
                                        <?= $project_location ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div>
                        <div>
                            <p>
                                Likewise, further review of your <b>Reservation Agreement</b> reveals that you failed to consummate your reservation within the <b>Fifteen (15)</b> days prescribed period.
                            </p>
                            <br>
                            <p>
                                Consequently, we regret to inform you that we will be canceling your <b>Reservation Agreement</b> and forfeiting the corresponding <b>Reservation Fee</b> under the terms and conditions stipulated thereon, thereby <b>extinguishing</b>, <b>terminating</b>, and <b>rendering</b> all your rights arising from said agreement without further force and effect.
                            </p>
                        </div>
                    </div>
                    <br>
                    <div>
                        <p>Roxas City, Philippines ____________________________.</p>
                    </div>

                    <br>

                    <div class="text-right" style="line-height: 0.8;">
                        <p><b>REYNA JOY A. BALTERO</b></p>
                        <p>Manager, External Affairs</p>
                        <p>Legal Services Department</p>
                    </div>

                    <br>

                    <div class="text-center page-break" style="line-height: 0.8;">
                        <br><br><br><br><br><br><br>
                        <h4 class="font-weight-bold"><?= $company['name'] ?></h4>
                        <p><?= $company['location'] ?></p>
                        <p>Phone No.: <?= $company['contact_number'] ?>, Email: <?= $company['email'] ?></p>
                        <p>Website: <a href="https://<?= $company['website'] ?>"><?= $company['website'] ?></a></p>
                    </div>

                    <br><br><br>

                    <div class="text-center">
                        <h3 class="font-weight-bold">ANNEX “A”</h3>
                    </div>

                    <br><br><br><br><br>

                    <div class="text-center">
                        <p>(Attach a photocopy of the corresponding Reservation Agreement )</p>
                    </div>
                </div>

                <!-- Page 2 -->
                <!-- <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br> -->
            </div>
        </div>
    </div>
</body>

</html>