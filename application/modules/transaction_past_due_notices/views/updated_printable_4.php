<!DOCTYPE html>
<html>

<head>
    <?php
    $header = isset($company['header']) && $company['header'] ? $company['header'] : "";
    $footer = isset($company['footer']) && $company['footer'] ? $company['footer'] : "";
    ?>
    <title>FIRST NOTICE</title>
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/favicon.ico" />
    <!--end::Layout Skins -->
    <style>
        body {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        @media print {
            .page-break {
                page-break-before: always !important;
            }
        }
    </style>
</head>

<body style="max-width: 1050px;">

    <!-- CONTENT -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">

        <!--begin:: Portlet-->
        <div class="kt-portlet">

            <div class="kt-portlet__body m-5 p-5">
                <?php

                $buyer = isset($transaction['buyer']) && $transaction['buyer'] ? $transaction['buyer']['last_name'] . ', ' . $transaction['buyer']['first_name'] . ' ' . $transaction['buyer']['middle_name'] : "";

                $buyer_address = isset($transaction['buyer']['present_address']) && $transaction['buyer']['present_address'] ? $transaction['buyer']['present_address'] : "";

                $mobile_no = isset($transaction['buyer']['mobile_no']) && $transaction['buyer']['mobile_no'] ? $transaction['buyer']['mobile_no'] : "";

                $project_name = isset($transaction['project']['name']) && $transaction['project']['name'] ? $transaction['project']['name'] : "";

                $project_location = isset($transaction['project']['location']) && $transaction['project']['location'] ? $transaction['project']['location'] : "";

                $property_name = isset($transaction['property']['name']) && $transaction['property']['name'] ? $transaction['property']['name'] : "";

                $property_block = isset($transaction['property']['block']) && $transaction['property']['block'] ? $transaction['property']['block'] : "";

                $property_lot = isset($transaction['property']['lot']) && $transaction['property']['lot'] ? $transaction['property']['lot'] : "";

                $property_lot_area = isset($transaction['property']['lot_area']) && $transaction['property']['lot_area'] ? $transaction['property']['lot_area'] : "";

                $month_overdue = floor($this->transaction_library->payment_status(1)['days'] / 30);

                ?>

                <div class="text-center" style="line-height: 0.8;">
                    <h4 class="font-weight-bold"><?= $company['name'] ?></h4>
                    <p><?= $company['location'] ?></p>
                    <p>Phone No.: <?= $company['contact_number'] ?>, Email: <?= $company['email'] ?></p>
                    <p>Website: <a href="https://<?= $company['website'] ?>"><?= $company['website'] ?></a></p>
                </div>

                <br><br><br>

                <div>
                    <p><?= view_date() ?></p>
                    <p><strong><?= @$buyer ?></strong></p>
                    <p><?= @$buyer_address ?></p>
                    <p><?= @$mobile_no ?></p>
                </div>

                <br>

                <div>
                    <p>Republic of the Philippines)</p>
                    <p>City of Roxas ) SS.</p>
                    <p>Province of Capiz )</p>
                    <p>x-----------------------------------)</p>
                </div>

                <br><br>

                <div class="text-center">
                    <u>
                        <h5 class="font-weight-bold">
                            NOTARIAL CANCELLATION
                            <br>OF
                            <br>CONTRACT TO SELL
                        </h5>
                    </u>
                </div>

                <br><br>

                <div>
                    <b>Dear Mr/Mrs. <?= $transaction['buyer']['last_name'] ?></b>
                </div>

                <br>

                <div>
                    <div>
                        <p>
                            This notice is being served by me in my capacity as Manager, External Affairs of Legal Services Department of <b>PUEBLO DE PANAY, INC.</b> with office address at Punta Dulog Bldg. St. Joseph Avenue, Pueblo de Panay Township, Lawaan, Roxas City, Capiz.
                        </p>

                        <br>

                        <p>
                            Our records show that on _____________, ____ you have executed a Contract to Sell with our company, involving a (commercial/ residential) property more particularly described as follows;
                        </p>

                        <br>

                        <div>
                            <table class="w-100 table-bordered">
                                <thead class="table" style="background-color: rgb(59,56,56); color: gray;">
                                    <tr>
                                        <th>Block No.</th>
                                        <th>Lot No.</th>
                                        <th>Title No.</th>
                                        <th>Area</th>
                                        <th>Project No.</th>
                                        <th>Project Location</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-2">
                                            <?= $property_block ?>
                                        </td>
                                        <td class="p-2">
                                            <?= $property_lot ?>
                                        </td>
                                        <td class="p-2">
                                            <?= $property_name ?>
                                        </td>
                                        <td class="p-2">
                                            <?= $property_lot_area ?>
                                        </td>
                                        <td class="p-2">
                                            <?= $project_name ?>
                                        </td>
                                        <td class="p-2">
                                            <?= $project_location ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <br>

                        <p>
                            Further review of said records also reveal that your account has been past due since ___________________________. Despite service of corresponding DEMAND LETTER, and affording you the required grace period of ________ (__) to settle your account as provided for in Sec. __ of R.A. No. 6552 "Realty Installment Buyer Protection Act (Maceda Law)" the same remains unpaid.
                        </p>

                        <br>

                        <p>
                            Consequently, we are canceling and rescinding by way of notarial act your above-described Contract to Sell, as per Sec. 4 of R.A. No. 6552; which shall take effect THIRTY (30) days from receipt of this NOTARIAL CANCELLATION of CONTRACT to SELL.
                        </p>

                        <br>

                        <p>
                            Lastly, we are attaching photocopies of your above-described “Contract to Sell’ and the corresponding demand letters for your reference.
                        </p>

                        <br>

                        <p>
                            Roxas City, Philippines ____________________________.
                        </p>

                        <br>

                        <div class="text-right" style="line-height: 0.8;">
                            <p><b>REYNA JOY A. BALTERO</b></p>
                            <p>Manager, External Affairs</p>
                            <p>Legal Services Department</p>
                        </div>

                        <br>

                        <div class="text-center page-break" style="line-height: 0.8;">
                            <br><br><br><br><br><br><br>
                            <h4 class="font-weight-bold"><?= $company['name'] ?></h4>
                            <p><?= $company['location'] ?></p>
                            <p>Phone No.: <?= $company['contact_number'] ?>, Email: <?= $company['email'] ?></p>
                            <p>Website: <a href="https://<?= $company['website'] ?>"><?= $company['website'] ?></a></p>
                        </div>

                        <br>

                        <div class="text-center">
                            <u>
                                <h5 class="font-weight-bold">
                                    ACKNOWLEDGEMENT
                                </h5>
                            </u>
                        </div>

                        <div>
                            <p>Before ME, this <u><?= view_date() ?></u> in the City of Roxas, Capiz, Philippines, personally appeared:</p>

                            <br>

                            <div>
                                <table class="w-100 table-bordered">
                                    <thead class="table" style="background-color: rgb(59,56,56); color: gray;">
                                        <tr>
                                            <th>Name</th>
                                            <th>Competent Evidence of Identity</th>
                                            <th>Date/Place Issued</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="p-2">
                                                &nbsp;
                                            </td>
                                            <td class="p-2">
                                                &nbsp;
                                            </td>
                                            <td class="p-2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="p-2">
                                                &nbsp;
                                            </td>
                                            <td class="p-2">
                                                &nbsp;
                                            </td>
                                            <td class="p-2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <br>

                            <p>
                                Manager, External Affairs of Legal Services Department and authorized representative of <b>PUEBLO DE PANAY, INC. (PDPI)</b>, known to me to be the same person who executed the foregoing instrument for and in behalf of the said corporation, and she acknowledged to me that same is a corporate act.
                            </p>

                            <br>

                            <p>
                                This instrument consisting of ____ (__) pages including this page on which this acknowledgment is written duly signed by the party refers to a <b>Notarial Cancellation</b> of <b>Contract to Sell</b>.
                            </p>

                            <br>

                            <p>
                                <b>IN WITNESS WHEREOF</b>, I have hereunto set my hand and affixed my notarial seal, the day, year and place above written.
                            </p>

                            <br>

                            <div class="text-right">
                                <p>NOTARY PUBLIC</p>
                            </div>

                            <br>

                            <div style="line-height: 0.8;">
                                <p>Doc. No. _____;</p>
                                <p>Page No. _____;</p>
                                <p>Book No. _____;</p>
                                <p>Series of _____.</p>
                            </div>
                        </div>

                        <br>

                        <div class="text-center page-break" style="line-height: 0.8;">
                            <br><br><br><br><br><br><br>
                            <h4 class="font-weight-bold"><?= $company['name'] ?></h4>
                            <p><?= $company['location'] ?></p>
                            <p>Phone No.: <?= $company['contact_number'] ?>, Email: <?= $company['email'] ?></p>
                            <p>Website: <a href="https://<?= $company['website'] ?>"><?= $company['website'] ?></a></p>
                        </div>

                        <br><br><br>

                        <div class="text-center">
                            <h3 class="font-weight-bold">ANNEX “A”</h3>
                        </div>

                        <br><br><br><br><br>

                        <div class="text-center">
                            <p>(Attach a photocopy of the corresponding Reservation Agreement )</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>