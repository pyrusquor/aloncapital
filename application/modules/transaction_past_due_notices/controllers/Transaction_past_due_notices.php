<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_past_due_notices extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('transaction_past_due_notices/Transaction_past_due_notice_model', 'M_Transaction_past_due_notice');
        $this->_table_fillables = $this->M_Transaction_past_due_notice->fillable;
        $this->_table_columns = $this->M_Transaction_past_due_notice->__get_columns();

        // Format Helper
        $this->load->helper(['format', 'images']);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Transaction_past_due_notice->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Transaction_past_due_notice->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'transaction' => true,
            'project' => true,
            'property' => true,
            'buyer' => true,
            'past_due_date' => true,
            'notice' => true,
            'is_void' => true,
            'is_email_sent' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Transaction_past_due_notice
                ->with_transaction([
                    'fields' => 'reference',
                    'with' => [
                        [
                            'relation' => 'project',
                            'fields' => 'name'
                        ],
                        [
                            'relation' => 'property',
                            'fields' => 'name'
                        ],
                        [
                            'relation' => 'buyer',
                            'fields' => 'first_name,middle_name,last_name'
                        ],
                    ]
                ])
                ->with_creator('fields:first_name,last_name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("past_due_date like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'transaction_id' => [
                    'data' => $filters['transaction_id'] ?? null,
                    'operator' => '=',
                ],
                'date_range' => [
                    'data' => $filters['past_due_date'] ?? null,
                    'column' => 'past_due_date',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range') {

                        $query->where($value['column'], '>=', explode(' - ', $value['data'])[0]);
                        $query->where($value['column'], '<=', explode(' - ', $value['data'])[1]);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['property'] = isset($value['transaction']['property']) ? "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['transaction']['property']['id'] . "'>" . $value['transaction']['property']['name'] . "</a>" : 'N/A';
                        $items[$key]['project'] = isset($value['transaction']['project']) ? "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['transaction']['project']['id'] . "'>" . $value['transaction']['project']['name'] . "</a>" : 'N/A';
                        $items[$key]['buyer'] = isset($value['transaction']['buyer']) ? "<a target='_BLANK' href='" . base_url() . "buyer/view/" . $value['transaction']['buyer']['id'] . "'>" . get_fname($value['transaction']['buyer']) . "</a>" : 'N/A';
                        $items[$key]['transaction'] = isset($value['transaction']) ? "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['transaction']['id'] . "'>" . $value['transaction']['reference'] . "</a>" : 'N/A';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Transaction_past_due_notice->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['transaction_past_due_notices'] = $this->M_Transaction_past_due_notice->get($id);

            if ($this->view_data['transaction_past_due_notices']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }

    public function generate($id = false, $debug = 0)
    {
        if ($id) {

            $this->view_data['info'] = $this->M_Transaction_past_due_notice->get($id);

            $this->load->model('transaction/Transaction_model', 'M_Transaction');
            $this->load->model('company/Company_model', 'M_Company');

            $this->view_data['transaction'] = $this->M_Transaction
                ->with_buyer()
                ->with_project()
                ->with_company()
                ->with_property()
                ->get($this->view_data['info']['transaction_id']);

            // Initiate Transaction_library
            $CI = &get_instance();
            $CI->transaction_library->initiate($this->view_data['info']['transaction_id']);

            $this->view_data['company'] = $this->M_Company->get($this->view_data['transaction']['project']['company_id']);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('updated_printable_' . $this->view_data['info']['notice'], $this->view_data);

            $generateHTML = $this->load->view('updated_printable_' . $this->view_data['info']['notice'], $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }

    public function approve_request($id = false)
    {
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        if ($this->input->post()) {

            // Image Upload
            $upload_path = './assets/uploads/letter_request/approved_images';
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 5000;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config, 'approved_file');
            $this->approved_file->initialize($config);

            $info = $this->input->post();
            $data['approved_image'] =  "";

            if (!empty($_FILES['approved_file']['name'])) {

                if (!$this->approved_file->do_upload('approved_file')) {
                    $this->notify->error($this->approved_file->display_errors(), 'transaction_past_due_notices' . $id);
                } else {
                    $img_data = $this->approved_file->data();
                    $data['approved_image'] =  $img_data['file_name'];
                }
            }

            $data['status'] = 1;
            $data['approve_date'] = $info['approved_date'];

            // Get the request
            $request = $this->M_Transaction_past_due_notice->get($id);

            // Check if already approve
            $is_approved = $request['status'];


            // If not change status to approve
            if ($is_approved != 1) {
                $result = $this->M_Transaction_past_due_notice->update($data + $additional, $id);

                // If successfully changed notify user
                if ($result) {
                    $response['status'] = 1;
                    $response['message'] = 'Request is now approved.';
                } else {
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                }
                if ($response['status'] == 1) {
                    $this->notify->success($response['message'], 'transaction_past_due_notices');
                } else {
                    $this->notify->error($response['message'], 'transaction_past_due_notices');
                }
            } else {
                // If already approve notify user
                $response['status'] = 0;
                $response['message'] = 'This Request is already approved!';
                $this->notify->error($response['message'], 'transaction_past_due_notices');
            }

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Transaction_past_due_notice->get($id);
            if ($list) {

                $deleted = $this->M_Transaction_past_due_notice->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Letter Request Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Transaction_past_due_notice->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Vehicle/s Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function run_due($debug = false)
    {
        $this->db->select('transactions.id as transactions_id, transaction_payments.due_date as transaction_payment_due_date')
            ->from('transactions')
            ->where('transactions.deleted_at IS NULL')
            ->where('transactions.general_status !=', 1)
            ->order_by('transactions.id', 'ASC')
            ->join('transaction_payments', 'transactions.id = transaction_payments.transaction_id', 'inner')
            ->where('transaction_payments.deleted_at IS NULL')
            ->where('transaction_payments.is_paid', 0)
            ->where('transaction_payments.period_id !=', 1)
            ->where('transaction_payments.due_date <', date('Y-m-d 00:00:00', strtotime("- 3 days")))
            ->order_by('transaction_payments.due_date', 'ASC');

        $collections_due_dates = $this->db->get()->result_array();

        $transactions_due_dates = [];

        $transactions_ids = [];

        foreach ($collections_due_dates as $collection_due_date) {

            if (!in_array($collection_due_date['transactions_id'], $transactions_ids)) {

                array_push($transactions_ids, $collection_due_date['transactions_id']);

                $filtered_array = array_filter($collections_due_dates, function ($item) use ($collection_due_date) {

                    return $item['transactions_id'] == $collection_due_date['transactions_id'];
                });

                // Reset keys and get last item
                array_push($transactions_due_dates, array_values($filtered_array)[count($filtered_array) - 1]);
            }
        }

        if ($debug == 1) {
            vdebug($transactions_due_dates);
        }

        $this->db->select('id, transaction_id, past_due_date, notice, is_void')
            ->from('transaction_past_due_notices')
            ->where('deleted_at IS NULL')
            ->where('is_void', 0);

        $transaction_past_due_notices = $this->db->get()->result_array();

        if ($debug == 2) {
            vdebug($transaction_past_due_notices);
        }

        foreach ($transactions_due_dates as $transaction_due_date) {

            // Get current transactions with current transaction id
            $filtered_past_due_notices = array_filter($transaction_past_due_notices, function ($transaction_past_due_notice) use ($transaction_due_date) {

                return $transaction_past_due_notice['transaction_id'] == $transaction_due_date['transactions_id'];
            });

            $transaction_last_notice = null;

            foreach ($filtered_past_due_notices as $key => $past_due_notice) {

                if (!$key) {

                    $transaction_last_notice = $past_due_notice;
                } else {

                    if ($past_due_notice['notice'] > $transaction_last_notice['notice']) {
                        $transaction_last_notice = $past_due_notice;
                    }
                }
            }

            if (isset($transaction_last_notice)) {

                if ($transaction_last_notice['past_due_date'] != $transaction_due_date['transaction_payment_due_date']) {

                    $past_due_notice_type = $transaction_last_notice['notice'] + 1;

                    if ($past_due_notice_type <= 4) {
                        $this->create_past_due_notice($transaction_due_date['transactions_id'], $transaction_due_date['transaction_payment_due_date'], $past_due_notice_type);
                    }
                }
            } else {

                $this->create_past_due_notice($transaction_due_date['transactions_id'], $transaction_due_date['transaction_payment_due_date'], 1);
            }
        }

        redirect(site_url('transaction_past_due_notices'));
    }

    public function create_past_due_notice($transaction_id, $past_due_date, $notice_id)
    {
        if ($transaction_id && $past_due_date && $notice_id) {

            $data = array(
                'transaction_id' => $transaction_id,
                'past_due_date' => $past_due_date,
                'notice' => $notice_id,
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d'),
            );

            $this->M_Transaction_past_due_notice->insert($data);
        }
    }

    public function send_email($id, $debug = 0)
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($id && $this->input->is_ajax_request()) {
            $this->view_data['info'] = $this->M_Transaction_past_due_notice->get($id);

            $this->load->model('transaction/Transaction_model', 'M_Transaction');

            $this->view_data['transaction'] = $this->M_Transaction
                ->with_buyer()
                ->with_project()
                ->with_property()
                ->get($this->view_data['info']['transaction_id']);

            // Initiate Transaction_library
            $CI = &get_instance();
            $CI->transaction_library->initiate($this->view_data['info']['transaction_id']);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('updated_printable_' . $this->view_data['info']['notice'], $this->view_data);

            $generateHTML = $this->load->view('updated_printable_' . $this->view_data['info']['notice'], $this->view_data, true);

            $buyer_name = $this->view_data['transaction']['buyer']['first_name'] . ' ' . $this->view_data['transaction']['buyer']['last_name'];

            $email = [
                'subject' => Dropdown::get_static('past_due_notice_type', $this->view_data['info']['notice'], 'view'),
                // 'email' => 'charleskennethhernandez@gmail.com',
                'email' => $this->view_data['transaction']['buyer']['email'],
                'name' => $buyer_name
            ];

            Communication_library::send_email($email, $generateHTML);

            $result = $this->M_Transaction_past_due_notice->update(['is_email_sent' => 1], $id);

            $response['status'] = $result;
            $response['message'] = 'Email successfully sent!';
        }

        echo json_encode($response);
    }
}
