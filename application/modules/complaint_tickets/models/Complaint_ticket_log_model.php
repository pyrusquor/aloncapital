<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Complaint_ticket_log_model extends MY_Model
{
    public $table = 'complaint_ticket_logs'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'complaint_id',
        'department_id',
        'remarks',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        /* ==================== begin: Add model fields ==================== */
        'complaint_id' => array(
            'field' => 'complaint_id',
            'label' => 'Complaint',
            'rules' => 'trim|required'
        ),
        'remarks' => array(
            'field' => 'remarks',
            'label' => 'Remarks',
            'rules' => 'trim|required'
        ),
        'status' => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['project'] = array('foreign_model' => 'complaint_ticket_model', 'foreign_table' => 'complaint_tickets', 'foreign_key' => 'id', 'local_key' => 'complaint_ticket_id');

        $this->has_one['document'] = array('foreign_model' => 'Ticket_log_uploaded_document_model', 'foreign_table' => 'ticket_log_uploaded_documents', 'foreign_key' => 'ticket_log_id', 'local_key' => 'id');

        $this->has_one['department'] = array('foreign_model' => 'department/department_model', 'foreign_table' => 'departments', 'foreign_key' => 'id', 'local_key' => 'department_id');
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
