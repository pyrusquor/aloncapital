<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 
 */
class Ticket_log_uploaded_document_model extends MY_Model
{

    public $primary_key = 'id'; // you MUST mention the primary key
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $fillable = [
        'type',
        'ticket_log_id',
        'file_name',
        'file_type',
        'file_src',
        'file_path',
        'full_path',
        'raw_name',
        'orig_name',
        'client_name',
        'file_ext',
        'file_size',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at'
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $table = 'ticket_log_uploaded_documents'; // you MUST mention the table name
    public $rules = [];
    public $_fields = [];

    function __construct()
    {
        parent::__construct();

        $this->soft_deletes = TRUE;

        $this->return_as = 'array';

        $this->rules['insert'] = $this->_fields;
        $this->rules['update'] = $this->_fields;
    }
}
