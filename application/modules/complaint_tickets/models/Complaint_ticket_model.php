<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Complaint_ticket_model extends MY_Model
{
    public $table = 'complaint_tickets'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'reference',
        'project_id',
        'property_id',
        'house_model_id',
        'buyer_id',
        'department_id',
        'turn_over_date',
        'warranty_date',
        'warranty',
        'complaint_category_id',
        'complaint_sub_category_id',
        'description',
        'image',
        'status',
        'transaction_id',
        'completion_turn_around_time',
        'closing_turn_around_time',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        /* ==================== begin: Add model fields ==================== */

        'reference' => array(
            'field' => 'reference',
            'label' => 'Reference',
            'rules' => 'trim'
        ),
        'project_id' => array(
            'field' => 'project_id',
            'label' => 'Project',
            'rules' => 'trim|required'
        ),
        'property_id' => array(
            'field' => 'property_id',
            'label' => 'Property',
            'rules' => 'trim|required'
        ),
        'house_model_id' => array(
            'field' => 'house_model_id',
            'label' => 'House Model',
            'rules' => 'trim|required'
        ),
        'buyer_id' => array(
            'field' => 'buyer_id',
            'label' => 'Buyer',
            'rules' => 'trim|required'
        ),
        'department_id' => array(
            'field' => 'department_id',
            'label' => 'Department',
            'rules' => 'trim'
        ),
        'turn_over_date' => array(
            'field' => 'turn_over_date',
            'label' => 'Turn Over Date',
            'rules' => 'trim'
        ),
        'warranty' => array(
            'field' => 'warranty',
            'label' => 'Warranty',
            'rules' => 'trim'
        ),
        'complaint_category_id' => array(
            'field' => 'complaint_category_id',
            'label' => 'Complaint Category',
            'rules' => 'trim|required'
        ),
        'complaint_sub_category_id' => array(
            'field' => 'complaint_sub_category_id',
            'label' => 'Complaint Sub Category',
            'rules' => 'trim|required'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim'
        ),
        'image' => array(
            'field' => 'image',
            'label' => 'Supporting Image',
            'rules' => 'trim'
        ),
        'status' => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
        $this->has_one['property'] = array('foreign_model' => 'property/property_model', 'foreign_table' => 'properties', 'foreign_key' => 'id', 'local_key' => 'property_id');
        $this->has_one['house_model'] = array('foreign_model' => 'house/house_model', 'foreign_table' => 'house_models', 'foreign_key' => 'id', 'local_key' => 'house_model_id');
        $this->has_one['buyer'] = array('foreign_model' => 'buyer/buyer_model', 'foreign_table' => 'buyers', 'foreign_key' => 'id', 'local_key' => 'buyer_id');
        $this->has_one['department'] = array('foreign_model' => 'department/department_model', 'foreign_table' => 'departments', 'foreign_key' => 'id', 'local_key' => 'department_id');
        $this->has_one['complaint_category'] = array('foreign_model' => 'complaint_categories/complaint_category_model', 'foreign_table' => 'complaint_categories', 'foreign_key' => 'id', 'local_key' => 'complaint_category_id');
        $this->has_one['complaint_sub_category'] = array('foreign_model' => 'complaint_sub_categories/complaint_sub_category_model', 'foreign_table' => 'complaint_sub_categories', 'foreign_key' => 'id', 'local_key' => 'complaint_sub_category_id');
        $this->has_many['complaint_logs'] = array('foreign_model' => 'complaint_ticket_log_model', 'foreign_table' => 'complaint_logs', 'foreign_key' => 'complaint_id', 'local_key' => 'id');
    }

    function get_columns()
    {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);
    }
}
