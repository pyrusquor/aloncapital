<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
$property_id = isset($info['property_id']) && $info['property_id'] ? $info['property_id'] : '';
$house_model_id = isset($info['house_model_id']) && $info['house_model_id'] ? $info['house_model_id'] : '';
$buyer_id = isset($info['buyer_id']) && $info['buyer_id'] ? $info['buyer_id'] : '';
$department_id = isset($info['department_id']) && $info['department_id'] ? $info['department_id'] : '';
$turn_over_date = isset($info['turn_over_date']) && $info['turn_over_date'] ? date('Y-m-d', strtotime($info['turn_over_date'])) : '';
$warranty_date = isset($info['warranty_date']) && $info['warranty_date'] ? date('Y-m-d', strtotime($info['warranty_date'])) : '';
$warranty = isset($info['warranty']) && $info['warranty'] ? $info['warranty'] : '';
$complaint_category_id = isset($info['complaint_category_id']) && $info['complaint_category_id'] ? $info['complaint_category_id'] : '';
$complaint_sub_category_id = isset($info['complaint_sub_category_id']) && $info['complaint_sub_category_id'] ? $info['complaint_sub_category_id'] : '';
$description = isset($info['description']) && $info['description'] ? $info['description'] : '';
$image = isset($info['image']) && $info['image'] ? $info['image'] : '';
$status = isset($info['status']) && $info['status'] ? $info['status'] : '';

$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : '';
$property_name = isset($info['property']['name']) && $info['property']['name'] ? $info['property']['name'] : '';
$house_model_name = isset($info['house_model']['name']) && $info['house_model']['name'] ? $info['house_model']['name'] : '';
$department_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : '';
$complaint_category_name = isset($info['complaint_category']['name']) && $info['complaint_category']['name'] ? $info['complaint_category']['name'] : '';
$complaint_sub_category_name = isset($info['complaint_sub_category']['name']) && $info['complaint_sub_category']['name'] ? $info['complaint_sub_category']['name'] : 'N/A';
$buyer_name = isset($info['buyer']) && $info['buyer'] ? $info['buyer']['last_name'] . ' ' . $info['buyer']['first_name'] : '';
$complaint_categories = isset($complaint_categories) && $complaint_categories ? $complaint_categories : [];
$complaint_sub_categories = isset($complaint_sub_categories) && $complaint_sub_categories ? $complaint_sub_categories : [];
// ==================== end: Add model fields ====================
?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Buyer <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="buyers" data-type="person" id="buyer_id" name="buyer_id">
                <option value="">Select Buyer</option>
                <?php if ($buyer_name) : ?>
                    <option value="<?php echo $buyer_id; ?>" selected><?php echo $buyer_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Project <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests-show-options" data-module="projects" data-type='buyer_filter' data-select='project_id' data-param='buyer_id' id="project_id" name="project_id">
                <option value="">Select Project</option>
                <?php if ($project_name) : ?>
                    <option value="<?php echo $project_id; ?>" selected><?php echo $project_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Property <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests-show-options" data-type='buyer_filter' data-select='property_id' data-module="properties" data-param='buyer_id' id="property_id" name="property_id" data-param="buyer_id" data-param-2="project_id">
                <option value="">Select Project</option>
                <?php if ($property_name) : ?>
                    <option value="<?php echo $property_id; ?>" selected><?php echo $property_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">House Model <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="house_models" id="house_model_id" name="house_model_id" data-param="project_id">
                <option value="">Select House Model</option>
                <?php if ($house_model_name) : ?>
                    <option value="<?php echo $house_model_id; ?>" selected><?php echo $house_model_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Turn Over Date <span class="kt-font-danger"></span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" placeholder="Turn Over Date" name="turn_over_date" id="turn_over_date" value="<?php echo set_value('turn_over_date"', @$turn_over_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Warranty Date</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" placeholder="Warranty Date" name="warranty_date" id="warranty_date" value="<?php echo set_value('warranty_date"', @$warranty_date); ?>" readonly>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Warranty</label>
            <div class="kt-input-icon">
                <select class="form-control" name="warranty" placeholder="Type" autocomplete="off" id="warranty">
                    <!-- <option value="">Select option</option> -->
                    <option value="0" <?php if ($warranty == '0') : ?>selected<?php endif ?>>No</option>
                    <option value="1" <?php if ($warranty == '1') : ?>selected<?php endif ?>>Yes</option>
                </select>
            </div>
            <?php echo form_error('warranty'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Complaint Category <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control" name="complaint_category_id" placeholder="Type" autocomplete="off" id="complaint_category_id">
                    <option value="">Select option</option>
                    <?php foreach ($complaint_categories as $complaint_category) : ?>
                        <option value="<?= $complaint_category['id'] ?>" <?php if ($complaint_category_name == $complaint_category['name']) : ?>selected<?php endif; ?>><?= $complaint_category['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <?php echo form_error('complaint_category_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Sub Category <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control" name="complaint_sub_category_id" placeholder="Type" autocomplete="off" id="complaint_sub_category_id">
                    <option value="">Select option</option>
                    <?php foreach ($complaint_sub_categories as $complaint_sub_category) : ?>
                        <option value="<?= $complaint_sub_category['id'] ?>" <?php if ($complaint_sub_category_name == $complaint_sub_category['name']) : ?>selected<?php endif; ?>><?= $complaint_sub_category['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <?php echo form_error('complaint_sub_category_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Description <span class="kt-font-danger"></span></label>
            <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description" autocomplete="off" id="description">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Supporting Image</label>
            <div>
                <?php if (get_image('tickets', 'supporting_file', $image)) : ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('tickets', 'supporting_file', $image)); ?>" width="90px" height="90px" />
                <?php endif; ?>
                <span class="btn btn-sm">
                    <input type="file" name="file" class="" aria-invalid="false" accept="image/png, image/gif, image/jpeg">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>