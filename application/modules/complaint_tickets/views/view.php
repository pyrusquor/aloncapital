<?php
$id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
// ==================== begin: Add model fields ====================
$reference = isset($info['reference']) && $info['reference'] ? $info['reference'] : 'N/A';
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : 'N/A';
$property_id = isset($info['property_id']) && $info['property_id'] ? $info['property_id'] : 'N/A';
$house_model_id = isset($info['house_model_id']) && $info['house_model_id'] ? $info['house_model_id'] : 'N/A';
$buyer_id = isset($info['buyer_id']) && $info['buyer_id'] ? $info['buyer_id'] : 'N/A';
$department_id = isset($info['department_id']) && $info['department_id'] ? $info['department_id'] : 'N/A';
$turn_over_date = isset($info['turn_over_date']) && $info['turn_over_date'] ? $info['turn_over_date'] : 'N/A';
$warranty_date = isset($info['warranty_date']) && $info['warranty_date'] ? $info['warranty_date'] : 'N/A';
$warranty = isset($info['warranty']) && $info['warranty'] ? "Yes" : "No";
$complaint_category_id = isset($info['complaint_category_id']) && $info['complaint_category_id'] ? $info['complaint_category_id'] : 'N/A';
$complaint_sub_category_id = isset($info['complaint_sub_category_id']) && $info['complaint_sub_category_id'] ? $info['complaint_sub_category_id'] : 'N/A';
$description = isset($info['description']) && $info['description'] ? $info['description'] : 'N/A';
$image = isset($info['image']) && $info['image'] ? $info['image'] : 'N/A';
$status = isset($info['status']) && $info['status'] ? $info['status'] : 'N/A';
$status_value = Dropdown::get_static('customer_care_status', $info['status'], 'view');
$created_at = isset($info['created_at']) && $info['created_at'] ? $info['created_at'] : 'N/A';
$updated_at = isset($info['updated_at']) && $info['updated_at'] ? $info['updated_at'] : 'N/A';
$complaint_logs = isset($complaint_logs) && $complaint_logs ? $complaint_logs : [];

$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : 'N/A';
$property_name = isset($info['property']['name']) && $info['property']['name'] ? $info['property']['name'] : 'N/A';
$house_model_name = isset($info['house_model']['name']) && $info['house_model']['name'] ? $info['house_model']['name'] : 'N/A';
$buyer = get_person($buyer_id, 'buyers');
$buyer_name = get_fname($buyer);
$department_name = isset($info['department']['name']) && $info['department']['name'] ? $info['department']['name'] : 'N/A';
$complaint_category = isset($info['complaint_category']['name']) && $info['complaint_category']['name'] ? $info['complaint_category']['name'] : 'N/A';
$complaint_sub_category = isset($info['complaint_sub_category']['name']) && $info['complaint_sub_category']['name'] ? $info['complaint_sub_category']['name'] : 'N/A';

$overdue = cc_date_diff($info);
$status_code = cc_status($status, $overdue['status']);
// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Ticket View</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo base_url('complaint_tickets/printable/' . $id); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa flaticon-technology"></i> Print Ticket
                </a>
                <a href="<?php echo site_url('complaint_tickets'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <h3 class="kt-widget13__desc">
                                        Reference: <strong><?= $reference ?></strong>
                                    </h3>
                                </div>
                                <!-- ==================== begin: Add fields details  ==================== -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Project:</h6>
                                            <h6 class="kt-widget13__text"><?= $project_name ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">House Model:</h6>
                                            <h6 class="kt-widget13__text"><?= $house_model_name ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Buyer:</h6>
                                            <h6 class="kt-widget13__text"><?= $buyer_name ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Turn Over Date:</h6>
                                            <h6 class="kt-widget13__text"><?= view_date($turn_over_date) ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Warranty Date:</h6>
                                            <h6 class="kt-widget13__text"><?= view_date($warranty_date) ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Warranty:</h6>
                                            <h6 class="kt-widget13__text"><?= $warranty ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Date Encoded:</h6>
                                            <h6 class="kt-widget13__text"><?= view_date($created_at) ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Date Updated:</h6>
                                            <h6 class="kt-widget13__text"><?= view_date($updated_at) ?></h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Category:</h6>
                                            <h6 class="kt-widget13__text"><?= $complaint_category ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Sub-category:</h6>
                                            <h6 class="kt-widget13__text"><?= $complaint_sub_category ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Description:</h6>
                                            <h6 class="kt-widget13__text"><?= $description ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <h6 class="kt-widget13__desc">Department:</h6>
                                            <h6 class="kt-widget13__text"><?= $department_name ?></h6>
                                        </div>
                                        <div class="kt-widget13__item">
                                            <div class="card text-white <?= $status_code ?> mb-3 pr-5">
                                                <div class="card-body">
                                                    <h6 class="card-title"><?= $status_value ?></h6>
                                                    <p class="card-text"><?= $overdue['date_diff'] ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget13__item d-flex flex-wrap align-content-start">
                                            <h6 class="kt-widget13__desc mb-2">Supporting Image:</h6>
                                            <?php if (get_image('tickets', 'supporting_file', $image)) : ?>
                                                <div>
                                                    <img class="img-fluid" src="<?= base_url(get_image('tickets', 'supporting_file', $image)); ?>" alt="Image">
                                                </div>
                                            <?php else : ?>
                                                <h6 class="kt-widget13__text">N/A</h6>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- ==================== end: Add model details ==================== -->
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Remarks Log
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Table-->
                            <table class="table table-striped- table-bordered table-hover table-checkable">
                                <thead>
                                    <tr>
                                        <th>Remarks</th>
                                        <th>General Status</th>
                                        <th>Department</th>
                                        <th>File</th>
                                        <th>Date</th>
                                        <th>Created By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($complaint_logs) : $complaint_logs = array_reverse($complaint_logs); ?>
                                        <?php foreach ($complaint_logs as $key => $value) : ?>
                                            <?php
                                            $value['status'] = Dropdown::get_static('customer_care_status',  $value['status'], 'view');
                                            $user = get_person($value['created_by'], 'users');
                                            $value['created_by'] = get_fname($user);
                                            $department_name = isset($value['department']['name']) && $value['department']['name'] ? $value['department']['name'] : '';
                                            ?>
                                            <tr>
                                                <td><?= $value['remarks'] ?></td>
                                                <td><?= $value['status'] ?></td>
                                                <td><?= $department_name ?></td>
                                                <td>
                                                    <?php if ($value['document']['file_src']) : ?>
                                                        <button class="btn btn-linkedin btn-sm btn-elevate btn-icon view_file" data-toggle="modal" data-target="#view_file_modal" data-id="<?= $value['document']['id'] ?>">
                                                            <i class="la la-eye"></i>
                                                        </button></span>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?= view_hrdate($value['created_at']) ?></td>
                                                <td><?= $value['created_by'] ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <tr>
                                            <td colspan="5" class="text-center">No record found</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!-- begin:: Footer -->

<!-- Start View Modal-->
<div class="modal fade" id="view_file_modal" tabindex="-1" role="dialog" aria-labelledby="_view_file_modal_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_view_file_modal_label">View</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div id="document_display_container"></div>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <a target="_blank" class="btn btn-primary btn-sm btn-font-sm pull-right" id="download">
                        <i class="fa fa-download"></i> Download
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End View Modal-->