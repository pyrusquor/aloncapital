<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Complaint_tickets extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $payment_voucher_models = array(
            'Complaint_ticket_model' => 'M_Complaint_ticket',
            'Complaint_ticket_log_model' => 'M_Complaint_ticket_log',
            'complaint_tickets/Ticket_log_uploaded_document_model' => 'M_ticket_log_uploaded_document'
        );

        // Load models
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('Transaction/Transaction_model', 'M_transaction');
        $this->load->model($payment_voucher_models);

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper
        $this->load->helper('form');

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_Complaint_ticket->fillable;
        $this->_table_columns = $this->M_Complaint_ticket->__get_columns();
    }

    public function template($page = "")
    {

        $this->template->build($page);
    }

    public function index($debug = 0)
    {
        // $_fills = $this->_table_fillables;
        // $_colms = $this->_table_columns;

        // $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        // $this->view_data['_columns'] = $this->__get_columns($_fills);

        // // Get record count
        // // $conditions['returnType'] = 'count';
        // $this->view_data['totalRec'] = $totalRec = $this->M_Complaint_ticket->count_rows();

        // // Pagination configuration
        // $config['target'] = '#complaint_ticketContent';
        // $config['base_url'] = base_url('complaint_tickets/paginationData');
        // $config['total_rows'] = $totalRec;
        // $config['per_page'] = $this->perPage;
        // $config['link_func'] = 'ComplaintTicketPagination';

        // // Initialize pagination library
        // $this->ajax_pagination->initialize($config);

        // // Get records
        // $this->view_data['records'] = $this->M_Complaint_ticket
        //     ->with_project()
        //     ->with_property()
        //     ->with_house_model()
        //     ->with_buyer()
        //     ->with_department()
        //     ->with_complaint_category()
        //     ->with_complaint_sub_category()
        //     ->order_by('id', 'DESC')
        //     ->limit($this->perPage, 0)
        //     ->get_all();

        // if($debug){
        //     vdebug($this->view_data['records']);
        // }

        // $this->template->build('index', $this->view_data);

        $_fills    =    $this->_table_fillables;
        $_colms    =    $this->_table_columns;
        $_table =    $this->M_Complaint_ticket->table;

        $this->view_data['_fillables']    =    $this->__get_fillables($_colms, $_fills, $_table); #pd($this->view_data['_fillables']);
        $this->view_data['_columns']        =    $this->__get_columns($_fills); #ud($this->view_data['_columns']);

        // Get record count 
        $this->view_data['totalRec'] = $totalRec = $this->M_Complaint_ticket->count_rows();
        // $this->view_data['totalRec'] = $totalRec = 0;

        // Pagination configuration 
        $config['target']      = '#inquiryContent';
        $config['base_url']    = base_url('complaint_tickets/paginationData');
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['link_func']   = 'InquiryPagination';

        // Initialize pagination library 
        $this->ajax_pagination->initialize($config);

        // Get records 
        $this->view_data['records'] = $this->M_Complaint_ticket->with_project()->with_property()->with_house_model()->with_buyer()->with_department()->as_array()->limit($this->perPage, 0)->get_all();
        // $this->view_data['records'] = [];

        // $this->view_data['records'] = [];

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'reference' => true,
            'ref' => true,
            'project_id' => true,
            'property_id' => true,
            'house_model_id' => true,
            'buyer_id' => true,
            'turn_over_date' => true,
            'warranty_date' => true,
            'inquiry_category_id' => true,
            'department_id' => true,
            'warranty' => true,
            'created_at' => true,
            'updated_at' => true,
            'status' => true,
            'status_id' => true,
            'completion_turn_around_time' => true,
            'closing_turn_around_time' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Complaint_ticket
                ->with_project('fields:name')
                ->with_property('fields:name')
                ->with_house_model('fields:name')
                ->with_buyer('fields:first_name,middle_name,last_name')
                ->with_department('fields:name')
                ->with_inquiry_category('fields:name')
                ->with_inquiry_sub_category('fields:name')
                // ->with_inquiry_inquiry_logs()
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {
                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("reference like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
                'property_id' => [
                    'data' => $filters['property_id'] ?? null,
                    'operator' => '=',
                ],
                'house_model_id' => [
                    'data' => $filters['house_model_id'] ?? null,
                    'operator' => '=',
                ],
                'department_id' => [
                    'data' => $filters['department_id'] ?? null,
                    'operator' => '=',
                ],
                'buyer_id' => [
                    'data' => $filters['buyer_id'] ?? null,
                    'operator' => '=',
                ],
                'status' => [
                    'data' => $filters['status'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['reference'] = isset($value['reference']) ? "<a href='/complaint_tickets/view/" . $value['id'] . "'>" . $value['reference'] . "</a>" : 'N/A';
                        $items[$key]['ref'] =  $value['reference'];
                        $items[$key]['project_id'] = isset($value['project']) ? "<a href='/project/view/" . $value['project']['id'] . "'>" . $value['project']['name'] . "</a>" : 'N/A';
                        $items[$key]['property_id'] = isset($value['property']) ? "<a href='/property/view/" . $value['property']['id'] . "'>" . $value['property']['name'] . "</a>" : 'N/A';
                        $items[$key]['buyer_id'] = isset($value['buyer']) ? "<a href='/buyer/view/" . $value['buyer']['id'] . "'>" . get_fname($value['buyer']) . "</a>" : 'N/A';
                        $items[$key]['house_model_id'] = isset($value['house_model']) ? "<a href='/house/view/" . $value['house_model']['id'] . "'>" . $value['house_model']['name'] . "</a>" : 'N/A';
                        $items[$key]['department_id'] = isset($value['department']) ? "<a href='/department/view/" . $value['department']['id'] . "'>" . $value['department']['name'] . "</a>" : 'N/A';
                        $items[$key]['inquiry_category_id'] = isset($value['inquiry_category']) ? "<a href='/inquiry_categories/view/" . $value['inquiry_category']['id'] . "'>" . $value['inquiry_category']['name'] . "</a>" : 'N/A';

                        $items[$key]['turn_over_date'] = view_date($value['turn_over_date']);
                        $items[$key]['warranty_date'] = $value['warranty_date'] ? view_date($value['warranty_date']) : "";
                        $items[$key]['created_at'] = view_date($value['created_at']);
                        $items[$key]['updated_at'] = view_date($value['updated_at']);

                        $status = $value['status'];
                        $items[$key]['status_id'] = $status;
                        $status_value = Dropdown::get_static('customer_care_status', $value['status'], 'view');

                        $overdue = cc_date_diff($value);
                        $status_code = cc_status($status, $overdue['status']);

                        $created_at = new DateTime($value['created_at']);

                        $items[$key]['completion_turn_around_time'] = $value['completion_turn_around_time'] ? $created_at->diff(new DateTime($value['completion_turn_around_time']))->format('%r%a day(s)') : "";

                        $items[$key]['closing_turn_around_time'] = $value['closing_turn_around_time'] ? $created_at->diff(new DateTime($value['closing_turn_around_time']))->format('%r%a day(s)') : "";

                        $a  = '<div class="card text-white ' . $status_code . ' mb-3 pr-5" style="max-width: 18rem;"><div class="card-body"><h5 class="card-title">' . $status_value . '</h5><p class="card-text">' . $overdue['date_diff'] . '</p></div></div>';

                        $items[$key]['status'] = $a;

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . (get_person_name($value['created_by'], "users") ?? 'N/A') . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Complaint_ticket->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function generate_transactions()
    {
        // code...
        $rows = $this->M_Complaint_ticket->get_all();

        if ($rows) {
            foreach ($rows as $key => $row) {

                $id = $row['id'];

                $w['project_id'] = $row['project_id'];
                $w['property_id'] = $row['property_id'];
                $w['buyer_id'] = $row['buyer_id'];

                $oof['transaction_id'] = get_value_field_multiple($w, 'transactions', 'id');

                $this->M_Complaint_ticket->update($oof, $id);
            }
        }
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            $project_id = $this->input->post('project_id');
            $property_id = $this->input->post('property_id');
            $house_model_id = $this->input->post('house_model_id');
            $buyer_id = $this->input->post('buyer_id');
            $department_id = $this->input->post('department_id');
            $turn_over_date = $this->input->post('turn_over_date');
            $complaint_category_id = $this->input->post('complaint_category_id');
            $complaint_sub_category_id = $this->input->post('complaint_sub_category_id');
            $page = $this->input->post('page');

            if (!empty($turn_over_date)) {
                $date_range = explode('-', $turn_over_date);

                $from_str   = strtotime($date_range[0]);
                $from       = date('Y-m-d H:i:s', $from_str);

                $to_str     = strtotime($date_range[1] . '23:59:59');
                $to         = date('Y-m-d H:i:s', $to_str);
            }

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_Complaint_ticket->count_rows();
            //            $where = array();

            // Pagination configuration
            $config['target'] = '#complaint_ticketContent';
            $config['base_url'] = base_url('complaint_tickets/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'ComplaintTicketPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($house_model_id)) :
                $this->db->group_start();
                $this->db->where('house_model_id', $house_model_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_id)) :
                $this->db->group_start();
                $this->db->where('buyer_id', $buyer_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($department_id)) :
                $this->db->group_start();
                $this->db->where('department_id', $department_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($turn_over_date)) :
                $this->db->group_start();
                $this->db->where('turn_over_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            if (!empty($complaint_category_id)) :
                $this->db->group_start();
                $this->db->where('complaint_category_id', $complaint_category_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($complaint_sub_category_id)) :
                $this->db->group_start();
                $this->db->where('complaint_sub_category_id', $complaint_sub_category_id, 'both');
                $this->db->group_end();
            endif;

            $totalRec = $this->M_Complaint_ticket->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('reference', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($project_id)) :
                $this->db->group_start();
                $this->db->where('project_id', $project_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($property_id)) :
                $this->db->group_start();
                $this->db->where('property_id', $property_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($house_model_id)) :
                $this->db->group_start();
                $this->db->where('house_model_id', $house_model_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_id)) :
                $this->db->group_start();
                $this->db->where('buyer_id', $buyer_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($department_id)) :
                $this->db->group_start();
                $this->db->where('department_id', $department_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($turn_over_date)) :
                $this->db->group_start();
                $this->db->where('turn_over_date BETWEEN "' . $from . '" AND "' .  $to . '"');
                $this->db->group_end();
            endif;

            if (!empty($complaint_category_id)) :
                $this->db->group_start();
                $this->db->where('complaint_category_id', $complaint_category_id, 'both');
                $this->db->group_end();
            endif;

            if (!empty($complaint_sub_category_id)) :
                $this->db->group_start();
                $this->db->where('complaint_sub_category_id', $complaint_sub_category_id, 'both');
                $this->db->group_end();
            endif;

            $this->view_data['records'] = $records = $this->M_Complaint_ticket
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_complaint_category()
                ->with_complaint_sub_category()
                ->order_by('id', 'DESC')
                ->limit($this->perPage, $offset)
                ->get_all();

            $this->load->view('complaint_tickets/_filter', $this->view_data, false);
        }
    }

    public function view($id = false, $type = '')
    {

        if ($id) {

            $this->view_data['info'] = $info = $this->M_Complaint_ticket
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_complaint_category()
                ->with_complaint_sub_category()
                ->get($id);

            $this->view_data['complaint_logs'] = $this->M_Complaint_ticket_log->with_department()->where('complaint_id', $id)->with_document()->get_all();

            if ($this->view_data['info']) {

                if ($type == "debug") {

                    vdebug($this->view_data);
                }

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function form($id = false, $debug = 0)
    {
        $related_ticket_id = $this->input->get('ticket_id') != null ? $this->input->get('ticket_id') : null;

        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['message'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Complaint_ticket->fields);

            if ($this->form_validation->run() === true) {

                $oof = $this->input->post();

                $w['project_id'] = $oof['project_id'];
                $w['property_id'] = $oof['property_id'];
                $w['buyer_id'] = $oof['buyer_id'];

                $oof['transaction_id'] = get_value_field_multiple($w, 'transactions', 'id');

                //get the status of the current complaint ticket
                $status = $this->M_Complaint_ticket->get($id)['status'];

                //if the complaint ticket is closed, cancelled or completed, the method will create a new ticket with the data from the old ticket
                if ($id && $status < 5) {

                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] !== '')) {

                        $image_name = $this->upload_ticket_document();

                        if (!$image_name) {

                            echo json_encode($response);
                            exit();
                        }

                        $additional['image'] = $image_name;

                        $compaint_ticket = $this->M_Complaint_ticket->fields('image')->get($id);

                        if ($compaint_ticket['image']) {
                            @unlink('assets/uploads/tickets/supporting_file/' . $compaint_ticket['image']);
                        }
                    } else {
                        unset($oof['file']);
                    }

                    $complaint_ticket_id = $this->M_Complaint_ticket->update($oof + $additional, $id);
                } else {

                    $image_name = null;

                    if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] !== '')) {

                        $image_name = $this->upload_ticket_document();

                        if (!$image_name) {

                            echo json_encode($response);
                            exit();
                        }
                    } else {
                        unset($oof['file']);
                    }

                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                        'reference' => uniqidCT(),
                        'status' => 1,
                        'image' => $image_name,
                    ];

                    $this->db->trans_start(); # Starting Transaction
                    $this->db->trans_strict(false); # See Note 01. If you wish can remove as well

                    $complaint_ticket_id = $this->M_Complaint_ticket->insert($oof + $additional);

                    $this->db->trans_complete(); # Completing complaint_tickets
                }

                /*Optional*/
                if ($this->db->trans_status() === false) {
                    # Something went wrong.
                    $this->db->trans_rollback();
                    $response['status'] = 0;
                    $response['message'] = 'Error!';
                } else {
                    # Everything is Perfect.
                    # Committing data to the database.
                    $this->db->trans_commit();
                    $response['status'] = 1;
                    $response['message'] = 'Complaint ticket Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->view_data['method'] = $method;

        $this->load->model('complaint_categories/Complaint_category_model', 'M_Complaint_category');

        $this->view_data['complaint_categories'] = $this->M_Complaint_category->as_array()->get_all();

        $complaint_id = $related_ticket_id ? $related_ticket_id : $id;

        if ($complaint_id) {

            $this->view_data['info'] = $this->M_Complaint_ticket
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_complaint_category()
                ->with_complaint_sub_category()
                ->get($complaint_id);

            if ($related_ticket_id) {

                $this->view_data['info']['image'] = '';
                $this->view_data['info']['description'] = '';
                $this->view_data['info']['complaint_category'] = null;
                $this->view_data['info']['complaint_sub_category'] = null;
            }

            $this->load->model('complaint_sub_categories/Complaint_sub_category_model', 'M_Complaint_sub_category');

            $this->view_data['complaint_sub_categories'] = $this->M_Complaint_sub_category->where('complaint_category_id =', $this->view_data['info']['complaint_category']['id'])->as_array()->get_all();
        }

        if ($this->ion_auth->is_buyer()) {
            # code...
            $buyer_id = get_value_field($this->session->userdata['user_id'], 'buyers', 'id', 'user_id');
            $transactions = $this->M_transaction
                ->order_by('id', 'DESC')
                ->with_project()
                ->with_property()
                ->where('buyer_id', $buyer_id)
                ->get_all();

            vdebug($transactions);
        }

        if ($debug) {
            vdebug($this->view_data);
        }

        // vdebug($this->view_data);
        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Complaint_ticket->get($id);
            if ($list) {

                $deleted = $this->M_Complaint_ticket->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Complaint ticket successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Complaint_ticket->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Complaint ticket successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function printable($id = false, $debug = 0)
    {
        if ($id) {

            // $this->view_data['info'] = $info = $this->M_Complaint_ticket->with_requests()->with_payable_items()->with_entry_items()->with_item()->as_array()->get($id);

            $this->view_data['info'] = $info = $this->M_Complaint_ticket
                ->with_project()
                ->with_property()
                ->with_house_model()
                ->with_buyer()
                ->with_department()
                ->with_complaint_category()
                ->with_complaint_sub_category()
                ->get($id);

            if ($debug) {
                vdebug($this->view_data);
            }

            $this->template->build('printable', $this->view_data);

            $generateHTML = $this->load->view('printable', $this->view_data, true);

            echo $generateHTML;
            die();

            pdf_create($generateHTML, 'generated_form');
        } else {

            show_404();
        }
    }

    public function forward_status()
    {
        $response['status'] = 0;
        $response['message'] = 'Error!';

        $complaint_ticket_id = $this->input->post('complaint_ticket_id');
        $department_id = $this->input->post('department_id');
        $remarks = "Fowarded to department-in-charge";
        $status_id = 2;

        if ($complaint_ticket_id) {

            $info = [
                'complaint_id' => $complaint_ticket_id,
                'department_id' => $department_id,
                'remarks' => $remarks,
                'status' => $status_id
            ];

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            $update = [
                'department_id' => $department_id,
                'status' => $status_id,
                'updated_by' => $this->user->id,
                'updated_at' => NOW,
            ];

            // Insert complaint ticket log
            $complaint_ticket_log_id = $this->M_Complaint_ticket_log->insert($info + $additional);
            $complaint_ticket = $this->M_Complaint_ticket->update($update, $complaint_ticket_id);

            if ($complaint_ticket_log_id) {

                if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] !== '')) {

                    if ($this->upload_ticket_log_document($complaint_ticket_log_id)) {

                        $response['status'] = 1;
                        $response['message'] = $remarks;
                    }
                } else {

                    $response['status'] = 1;
                    $response['message'] = $remarks;
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function update_status()
    {

        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $complaint_ticket_id = $this->input->post('complaint_ticket_id');
        $status = $this->input->post('status_id');
        $remarks = $this->input->post('remarks');

        if ($complaint_ticket_id) {
            $info = [
                'complaint_id' => $complaint_ticket_id,
                'remarks' => $remarks,
                'status' => $status,
            ];

            $additional = [
                'created_by' => $this->user->id,
                'created_at' => NOW,
            ];

            if ($status == 5) {

                $update = [
                    'status' => $status,
                    'completion_turn_around_time' => NOW,
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];
            } else if ($status == 6) {

                $update = [
                    'status' => $status,
                    'closing_turn_around_time' => NOW,
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];
            } else {

                $update = [
                    'status' => $status,
                    'updated_by' => $this->user->id,
                    'updated_at' => NOW,
                ];
            }

            // Insert complaint ticket log
            $complaint_ticket_log_id = $this->M_Complaint_ticket_log->insert($info + $additional);
            $complaint_ticket = $this->M_Complaint_ticket->update($update, $complaint_ticket_id);

            if ($complaint_ticket_log_id) {

                if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] !== '')) {

                    if ($this->upload_ticket_log_document($complaint_ticket_log_id)) {

                        $response['status'] = 1;
                        $response['message'] = $remarks;
                    }
                } else {

                    $response['status'] = 1;
                    $response['message'] = $remarks;
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    private function upload_ticket_log_document($ticket_log_id)
    {
        $upload_path = 'assets/img/tickets';

        if (!file_exists($upload_path)) {
            mkdir($upload_path, 0777);
        }

        unset($config);
        $config = [];
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = '*';
        $_config['overwrite'] = TRUE;
        $config['max_size'] = '1000000';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {

            return false;
        }

        $upload_data = $this->upload->data();

        $_insert['type'] = 'complaint';
        $_insert['ticket_log_id'] = $ticket_log_id;
        $_insert['file_name'] = isset($upload_data['file_name']) && $upload_data['file_name'] ? $upload_data['file_name'] : NULL;
        $_insert['file_type'] = isset($upload_data['file_type']) && $upload_data['file_type'] ? $upload_data['file_type'] : NULL;
        $_insert['file_src'] = isset($upload_data['full_path']) && $upload_data['full_path'] ? strstr($upload_data['full_path'], 'assets') : NULL;
        $_insert['file_path'] = isset($upload_data['file_path']) && $upload_data['file_path'] ? $upload_data['file_path'] : NULL;
        $_insert['full_path'] = isset($upload_data['full_path']) && $upload_data['full_path'] ? $upload_data['full_path'] : NULL;
        $_insert['raw_name'] = isset($upload_data['raw_name']) && $upload_data['raw_name'] ? $upload_data['raw_name'] : NULL;
        $_insert['orig_name'] = isset($upload_data['orig_name']) && $upload_data['orig_name'] ? $upload_data['orig_name'] : NULL;
        $_insert['client_name'] = isset($upload_data['client_name']) && $upload_data['client_name'] ? $upload_data['client_name'] : NULL;
        $_insert['file_ext'] = isset($upload_data['file_ext']) && $upload_data['file_ext'] ? $upload_data['file_ext'] : NULL;
        $_insert['file_size'] = isset($upload_data['file_size']) && $upload_data['file_size'] ? $upload_data['file_size'] : NULL;
        $_insert['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
        $_insert['created_at'] = NOW;
        $_insert['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
        $_insert['updated_at'] = NOW;

        $ticket_log_uploaded_document_id = $this->M_ticket_log_uploaded_document->insert($_insert);

        if (!$ticket_log_uploaded_document_id) {

            return false;
        } else {

            return true;
        }
    }

    public function preview_file()
    {
        $data['status'] = 0;

        $document_id = $this->input->post('document_id');

        $document = $this->M_ticket_log_uploaded_document->get($document_id);

        if ($document) {
            $this->view_data['_src']    =    base_url($document['file_src']);
            $this->view_data['name'] = $document['client_name'];
            $this->view_data['ext'] = $document['file_ext'];

            $data['name'] = $document['client_name'];
            $data['src'] = base_url($document['file_src']);
            $data['ext'] = $document['file_ext'];
            $data['content'] = $this->load->view('modal/view_file', $this->view_data, TRUE);

            $data['status'] = 1;
        }

        echo json_encode($data);

        exit();
    }

    private function upload_ticket_document()
    {
        $path = 'assets/uploads/tickets/supporting_file';

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $config = [];
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $_config['overwrite'] = TRUE;
        $config['max_size'] = '1000000';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {

            return null;
        }

        $upload_data = $this->upload->data();

        return $upload_data['file_name'];
    }
}
