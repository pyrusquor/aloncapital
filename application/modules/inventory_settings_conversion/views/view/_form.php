<?php
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$input_unit_of_measure = isset($info['input_unit_of_measure']) && $info['input_unit_of_measure'] ? $info['input_unit_of_measure'] : '';
$input_quantity = isset($info['input_quantity']) && $info['input_quantity'] ? $info['input_quantity'] : '';
$output_unit_of_measure = isset($info['output_unit_of_measure']) && $info['output_unit_of_measure'] ? $info['output_unit_of_measure'] : '';
$output_quantity = isset($info['output_quantity']) && $info['output_quantity'] ? $info['output_quantity'] : '';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================

?>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label>Input Unit of Measure <span class="kt-font-danger">*</span></label>
<!--            <div class="kt-input-icon">-->
<!--                <input type="text" class="form-control suggests" data-module="inventory_settings_unit_of_measurements" name="input_unit_of_measure" value="--><?php //echo set_value('input_unit_of_measure', $input_unit_of_measure); ?><!--" placeholder="Input Unit of Measure" autocomplete="off">-->
<!--                <span class="kt-input-icon__icon"></span>-->
<!--            </div>-->
            <select class="form-control" name="input_unit_of_measure" id="input_unit_of_measure">
                <?php foreach($units_of_measurement as $unit):?>
                    <option value="<?=$unit['id'];?>" <?php if($unit['id'] == $input_unit_of_measure) echo "selected";?>>
                        <?php echo $unit['name'];?>
                    </option>
                <?php endforeach ?>
            </select>
            <?php echo form_error('input_unit_of_measure'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Input Quantity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="input_quantity" value="<?php echo set_value('input_quantity', $input_quantity); ?>" placeholder="Input Quantity" autocomplete="off">
                <span class="kt-input-icon__icon"></span>
            </div>

            <?php echo form_error('input_quantity'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Output Unit of Measure <span class="kt-font-danger">*</span></label>
<!--            <div class="kt-input-icon">-->
<!--                <input type="text" class="form-control suggests" data-module="inventory_settings_unit_of_measurements"  name="output_unit_of_measure" value="--><?php //echo set_value('output_unit_of_measure', $output_unit_of_measure); ?><!--" placeholder="Output Unit of Measure" autocomplete="off">-->
<!--                <span class="kt-input-icon__icon"></span>-->
<!--            </div>-->
            <select class="form-control" name="output_unit_of_measure" id="output_unit_of_measure">
                <?php foreach($units_of_measurement as $unit):?>
                    <option value="<?=$unit['id'];?>" <?php if($unit['id'] == $output_unit_of_measure) echo "selected";?>>
                        <?php echo $unit['name'];?>
                    </option>
                <?php endforeach ?>
            </select>
            <?php echo form_error('output_unit_of_measure'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Output Quantity <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="output_quantity" value="<?php echo set_value('output_quantity', $output_quantity); ?>" placeholder="Output Quantity" autocomplete="off">
                <span class="kt-input-icon__icon"></span>
            </div>
            <?php echo form_error('output_quantity'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>