<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sub_project_model extends MY_Model
{
    public $table = 'sub_projects'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = [
        'project_id',
        'name',
        'code',
        'description',
        'is_active',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ]; // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        $this->has_one['project'] = array('foreign_model' => 'project/Project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

}
