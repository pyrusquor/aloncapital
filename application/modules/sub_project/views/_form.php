<?php
    $project_id            =    isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
    $name            =    isset($info['name']) && $info['name'] ? $info['name'] : '';
    $code            =    isset($info['code']) && $info['code'] ? $info['code'] : '';
    $description            =    isset($info['description']) && $info['description'] ? $info['description'] : '';
    $is_active            =    isset($info['is_active']) && $info['is_active'] ? $info['is_active'] : '';
    $project = isset($info['project']) && $info['project'] ? $info['project']['name'] : '';
?>


<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>Project <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <select class="form-control suggests" data-module="projects" id="project_id" name="project_id" required>
                    <option value="">Select Project</option>
                    <?php if ($project): ?>
                        <option value="<?php echo $project_id; ?>" selected><?php echo $project; ?></option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('or_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name">
            
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Code <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="code" value="<?php echo set_value('code', $code); ?>" placeholder="Code">

            <?php echo form_error('code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="">Description <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="description" value="<?php echo set_value('description', $description); ?>" placeholder="Description">

            <?php echo form_error('description'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label>Is Active <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
            <?php echo form_dropdown('is_active', Dropdown::get_static('inventory_status'), set_value('is_active', @$is_active ? @$is_active : 1), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>