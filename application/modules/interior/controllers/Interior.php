<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interior extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('house/House_model', 'M_house');
		$this->load->model('interior/Interior_model', 'M_interior');
		$this->load->model('interior/Price_model', 'M_price');

		// Load pagination library 
		$this->load->library('ajax_pagination');

		// Load Helper
		$this->load->helper('images');

		// Per page limit 
		$this->perPage = 12;

		$this->_table_fillables		=	$this->M_interior->fillable;
		$this->_table_columns		=	$this->M_interior->__get_columns();
	}
	 
	public function index_old()
	{
		$this->template->title('REMS', 'House Interiors');

		// overwrite default theme and layout if needed
		$this->template->set_theme('default');
		$this->template->set_layout('default');
		
		// get all raw data
		$alldata = $this->M_interior->with_house('fields:name')->as_array()->get_all();

		// print_r($alldata); exit;

		$this->view_data['record'] = $alldata; 

		$this->view_data['house'] = $this->M_house->as_array()->get_all();
		
		if ($this->view_data['house']) 
		{
			$this->template->build('index', $this->view_data);
		} 
		else 
		{
			show_404();
		}

	}

	function index_old_2 () {

		$this->template->title('REMS', 'House Interiors');

		// overwrite default theme and layout if needed
		$this->template->set_theme('default');
		$this->template->set_layout('default');
		
		// get all raw data
		$alldata = $this->M_interior->with_house('fields:name')->as_array()->get_all();

		// print_r($alldata); exit;

		$this->view_data['record'] = $alldata; 

		$this->view_data['house'] = $this->M_house->as_array()->get_all();
		
		$this->template->build('index', $this->view_data);
	}

	function index () {

		$this->template->title('REMS', 'House Interiors');

		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;
		$_table =	$this->M_interior->table;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills, $_table); #pd($this->view_data['_fillables']);
		$this->view_data['_columns']		=	$this->__get_columns($_fills); #ud($this->view_data['_columns']);

		// Get record count 
		$this->view_data['totalRec'] = $totalRec = $this->M_interior->count_rows();
		$this->view_data['houses'] = $this->M_house->with_project()->as_array()->get_all();

		// Pagination configuration 
		$config['target']      = '#recordsContent';
		$config['base_url']    = base_url('interior/paginationData');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'RecordPagination';

		// Initialize pagination library 
		$this->ajax_pagination->initialize($config);

		$this->view_data['records'] = $this->M_interior->with_house('fields:name')->as_array()->limit($this->perPage, 0)->get_all(); #ud($this->view_data['records']);

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function paginationData()
	{
		if ($this->input->is_ajax_request()) {

			$keyword = $this->input->post('keyword');

			$name = $this->input->post('name');
			$hm_id = $this->input->post('hm_id');
			$price = $this->input->post('price');

			$page = $this->input->post('page');

			if (!$page) {
				$offset = 0;
			} else {
				$offset = $page;
			}

			$this->view_data['totalRec'] = $totalRec = $this->M_interior->count_rows();
			// Pagination configuration 
			$config['target']      = '#recordsContent';
			$config['base_url']    = base_url('interior/paginationData');
			$config['total_rows']  = $totalRec;
			$config['per_page']    = $this->perPage;
			$config['link_func']   = 'RecordPagination';

			// Query
			if (!empty($keyword)) :
				$this->db->group_start();
				$this->db->like('name', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (!empty($name)) :

				$this->db->group_start();
				$this->db->like('name', $name, 'both');
				$this->db->group_end();
			endif;

			if (!empty($price)) :

				$this->db->where('price', $price);
			endif;

			if (!empty($hm_id)) :

				$this->db->where('house_model_id', $hm_id);
			endif;

			$totalRec = $this->M_interior->count_rows();

			// Pagination configuration 
			$config['total_rows']  = $totalRec;

			// Initialize pagination library 
			$this->ajax_pagination->initialize($config);

			// Query
			if (!empty($keyword)) :
				$this->db->group_start();
				$this->db->like('name', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (!empty($name)) :

				$this->db->group_start();
				$this->db->like('name', $name, 'both');
				$this->db->group_end();
			endif;

			if (!empty($price)) :

				$this->db->where('price', $price);
			endif;

			if (!empty($hm_id)) :

				$this->db->where('house_model_id', $hm_id);
			endif;

			$this->view_data['records'] = $records = $this->M_interior->with_house('fields: name')
				->limit($this->perPage, $offset)
				->get_all();


			$this->load->view('interior/_filter', $this->view_data, false);
		}
	}
	
	public function view_old($id = FALSE)
	{
		if ($id) {

			$this->view_data['interior'] = $this->M_interior->with_house('fields:name')->as_array()->get($id);
			
			if ($this->view_data['interior']) {

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function view ( $_id = FALSE, $_type = 'build' ) {

		if ( $_id ) {

			$this->view_data['interior'] = $interior = $this->M_interior->with_house('fields:name,floor_area,lot_area')->as_array()->get($_id);

			if ( $this->view_data['interior'] ) {

				$this->view_data['price_logs']	=	$this->M_price->where('house_model_interior_id',$_id)->get_all(); #lqq(); 
				
				if ($_type == "json") {
					echo json_encode( $this->view_data );
				} else {
					$this->template->build('view', $this->view_data);
				}
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}
	
	public function create_old()
	{	
		if ($this->input->post()) 
		{
			// echo $this->session->userdata('user_id'); exit;
			$is_multiply_floor_area = ($this->input->post('is_multiply_floor_area') !== null )  ? 1 : 0;
			$is_multiply_lot_area = ($this->input->post('is_multiply_lot_area') !== null )  ? 1 : 0;
			$additional_fields = array(
				'is_active' => 1,
				'is_multiply_floor_area' => $is_multiply_floor_area,
				'is_multiply_lot_area' => $is_multiply_lot_area ,
				'created_by' => $this->session->userdata('user_id'),
				'created_at' => date('Y-m-d')
			);
			
			$result = $this->M_interior->from_form(NULL, $additional_fields)->insert();
			
			// $this->db->last_query(); exit;

			if ($result === FALSE) {

				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Property Model Interior successfully created.', 'interior');
			}
		}
	
		$this->view_data['house'] = $this->M_house->as_array()->get_all();
		
		if ($this->view_data['house']) 
		{
			$this->template->build('create', $this->view_data);
		} 
		else 
		{
			show_404();
		}
	}

	function create_old_2 () {

		if ($this->input->post()) 
		{
			// echo $this->session->userdata('user_id'); exit;
			$is_multiply_floor_area = ($this->input->post('is_multiply_floor_area') !== null )  ? 1 : 0;
			$is_multiply_lot_area = ($this->input->post('is_multiply_lot_area') !== null )  ? 1 : 0;
			$additional_fields = array(
				'is_active' => 1,
				'is_multiply_floor_area' => $is_multiply_floor_area,
				'is_multiply_lot_area' => $is_multiply_lot_area ,
				'created_by' => $this->session->userdata('user_id'),
				'created_at' => date('Y-m-d')
			);
			
			$result = $this->M_interior->from_form(NULL, $additional_fields)->insert(); lqq(); ud($result);
			
			// $this->db->last_query(); exit;

			if ($result === FALSE) {

				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Property Model Interior successfully created.', 'interior');
			}
		}
	
		$this->view_data['house'] = $this->M_house->as_array()->get_all();
		
		$this->template->build('create', $this->view_data);
	}

	function create () {

		$_datas	=	[];

		$this->view_data['houses'] = $this->M_house->with_project()->as_array()->get_all();

		if ( $this->input->post() ) {

			$_input	=	$this->input->post();

			$_insert	=	$this->M_interior->insert($_input); #lqq(); ud($_insert);
			if ( $_insert !== FALSE ) {

				$this->notify->success('Record successfully created.', 'interior');
			} else {

				$this->notify->error('Oh snap! Please refresh the page and try again.');
			}
		}

		$this->template->build('create', $this->view_data);
	}
	
	public function update_old($id = FALSE)
	{
		if ($id) {
			$this->view_data['interior'] = $data = $this->M_interior->as_array()->get($id);
			$this->view_data['house'] = $this->M_house->as_array()->get_all();
			
			if ($data) 
			{
				if ($this->input->post()) 
				{
					// echo $this->session->userdata('user_id'); exit;
					$is_multiply_floor_area = ($this->input->post('is_multiply_floor_area') !== null )  ? 1 : 0;
					$is_multiply_lot_area = ($this->input->post('is_multiply_lot_area') !== null )  ? 1 : 0;
					$additional_fields = array(
						'is_multiply_floor_area' => $is_multiply_floor_area,
						'is_multiply_lot_area' => $is_multiply_lot_area ,
						'updated_by' => $this->session->userdata('user_id'),
						'updated_at' => date('Y-m-d')
					);
					
					$this->M_interior->rules['update'] = $this->M_interior->fields;
					
					$result = $this->M_interior->from_form(NULL, $additional_fields)->update(NULL, $data['id']);
					
					// $this->db->last_query(); exit;

					if ($result === FALSE) {

						// Validation
						$this->notify->error('Oops something went wrong.');
					} else {

						// Success
						$this->notify->success('Property Model Interior successfully updated.', 'interior');
					}
				}
				
				$this->template->build('update', $this->view_data);
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}

	function update ( $_id = FALSE ) {

		if ( $_id ) {

			$record	=	$this->M_interior->get($_id);
			$this->view_data['houses'] = $this->M_house->with_project()->as_array()->get_all();

			if ( $record ) {

				$this->view_data['interior']	=	$record;

				if ( $this->input->post() ) {

					$_input	=	$this->input->post();

					$_updated	=	$this->M_interior->update($_input, $record['id']);
					if ( $_updated !== FALSE ) {

						$this->notify->success('Record successfully updated.', 'interior');
					} else {

						$this->notify->error('Oh snap! Please refresh the page and try again.');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	function update_price ( $_id = FALSE ) {

		if ( $_id ) {

			$record	=	$this->M_interior->get($_id);
			$this->view_data['houses'] = $this->M_house->with_project()->as_array()->get_all();
			
			if ( $record ) {

				$this->view_data['update_price']	=	TRUE;
				$this->view_data['interior']			=	$record;

				if ( $this->input->post() ) {

					$_input	=	$this->input->post();

					if (!isset($_input['is_multiply_floor_area'])) {
						$_input['is_multiply_floor_area'] = 0;
					}

					if (!isset($_input['is_multiply_lot_area'])) {
						$_input['is_multiply_lot_area'] = 0;
					}

					$_updated	=	$this->M_interior->update($_input, $record['id']); #lqq(); ud($_updated);

					
					if ( $_updated !== FALSE ) {

						unset($_data);
						$_data['house_model_interior_id']	=	$record['id'];
						$_data['price']						=	$_input['price'];
						$_data['improved_price']			=	$_input['improved_price'];
						$_data['fully_furnished_price']		=	$_input['fully_furnished_price'];
						$_data['date_effectivity']			=	date_format(date_create($_input['date_effectivity']), 'Y-m-d H:i:s');

						$_insert	=	$this->M_price->insert($_data); #lqq(); ud($_insert);
						if ( $_insert !== FALSE ) {

							$this->notify->success('Record successfully updated.', 'interior');
						} else {

							$this->notify->error('Oh snap! Please refresh the page and try again.');
						}
					} else {

						$this->notify->error('Oh snap! Please refresh the page and try again.');
					}
				}

				$this->template->build('update_price', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}	
	
	public function delete($id = FALSE)
	{
		$id = $this->input->post('id');
	
		$result = (bool)$this->M_interior->delete($id);
		if ($result) {
			$fields = array(
				'deleted_by' => $this->session->userdata('user_id')
			);
			
			$this->M_interior->rules['update'] = $this->M_interior->delete_fields;
			
			$project_result = $this->M_interior->update($fields, $id);

			$response['status']  = TRUE;
			$response['message'] = 'Property Model Interior Deleted Successfully ...';
		} else {

			$response['status']  = FALSE;
			$response['message'] = 'Unable to delete Project ...';
		}

		echo json_encode($response);
	}

	public function bulkDelete()
	{	
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if($this->input->is_ajax_request())
		{
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_interior->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Record/s successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function filter()
	{
		$this->template->title('REMS', 'House Interiors');

		// overwrite default theme and layout if needed
		$this->template->set_theme('default');
		$this->template->set_layout('default');

		$house_model_id = $this->input->post('filter_house_model_id');
		$name = $this->input->post('filter_name');
		$price = $this->input->post('filter_price');
		
		// get all raw data
		$alldata = $this->M_interior->where('house_model_id', 'like', $house_model_id)->where('name', 'like', $name)->where('price', 'like', $price)->with_house('fields:name')->as_array()->get_all();

		// echo $this->db->last_query(); exit;

		// print_r($alldata); exit;

		$this->view_data['record'] = $alldata; 

		$this->view_data['house'] = $this->M_house->as_array()->get_all();

		$this->view_data['filter'] = $this->input->post(); 
		
		if ($this->view_data['house']) 
		{
			$this->template->build('index', $this->view_data);
		} 
		else 
		{
			show_404();
		}
	}

	public function export_old()
	{
		// get all raw data
		$alldata = $this->M_interior->with_house('fields:name')->as_array()->get_all();

		$date_file_name = date('F d Y', strtotime(date("Y-m-d H:i:s")));
        $date_title_name = date('F d, Y', strtotime(date("Y-m-d H:i:s")));

        $file = FCPATH.'assets/excel_templates/interior.xlsx';
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->setPreCalculateFormulas(true);

        $baseRow = 3;
		$count = count($alldata);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', "List of Property Model Interior as of ".strtoupper($date_title_name)); 

		if($alldata)
        {
			foreach($alldata as $key => $house)
            {
				$house = json_decode(json_encode($house), true);
				$row_no = $key + 1;

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$baseRow, ucwords($house['house']['name'])) 
							->setCellValue('B'.$baseRow, ucwords($house['name']))
							->setCellValue('C'.$baseRow, $house['price'])
							;

				$baseRow++;
				if($count != $row_no)
				{
					$objPHPExcel->getActiveSheet()->insertNewRowBefore($baseRow,1);
				}
			}
		}
		else
        {
            $objPHPExcel->getActiveSheet()->removeRow($baseRow);
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$baseRow.':S'.$baseRow);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$baseRow, 'NO RESULT FOUND.');
            $styleArray = array(
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => 'FF0000'),
                    'size'  => 15,
                    'name'  => 'Verdana'
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A'.$baseRow)->applyFromArray($styleArray);
        }

		$file_name = "List of Property Model Interiors - " . $date_file_name . ".xls";

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="'.$file_name.'"');
		// Write file to the browser
        $objWriter->save('php://output');

	}

	function export () {

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];

		$_titles[]	=	'#';
		// $_titles[]	=	'ID';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		$_records	=	$this->M_interior->as_array()->get_all();
		if ( $_records ) {

			foreach ( $_records as $_rkey => $_record ) {

				$_datas[$_record['id']]['#']	=	$_no;
				// $_datas[$_record['id']]['ID']	=	isset($_record['id']) && $_record['id'] ? $_record['id'] : $_no;

				$_no++;
			}

			$_filename	=	'list_of_house_interiors_'.date('m_d_y_h-i-s',time()).'.xls';

			$_objSheet	=	$this->excel->getActiveSheet();

			if ( $this->input->post() ) {

				$_export_column	=	$this->input->post('_export_column');
				if ( $_export_column ) {

					foreach ( $_export_column as $_ekey => $_column ) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'interior');
				}
			} else {

				$_filename	=	'list_of_house_interiors_'.date('m_d_y_h-i-s',time()).'.csv';

				// $_db_columns	=	$this->M_interior->fillable;
				$_db_columns	=	$this->_table_fillables;
				if ( !$_db_columns ) {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'interior');
				}
			}

			if ( $_db_columns ) {

				foreach ( $_db_columns as $key => $_dbclm ) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ( (strpos( $_name, 'created_') === FALSE) && (strpos( $_name, 'updated_') === FALSE) && (strpos( $_name, 'deleted_') === FALSE) && ($_name !== 'id') ) {

						if ( (strpos( $_name, '_id') !== FALSE) ) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ( $_records as $_rkey => $_record ) {

								if ( $_column === 'classification_id' ) {

									$_datas[$_record['id']][$_title]	=	isset($_record[$_column]) && $_record[$_column] ? Dropdown::get_static('classification', $_record[$_column], 'view') : '';
								} elseif ( $_column === 'owner_id' ) {

									$_datas[$_record['id']][$_title]	=	isset($_record[$_column]) && $_record[$_column] ? Dropdown::get_static('document_owner', $_record[$_column], 'view') : '';
								} elseif ( $_column === 'security_classification_id' ) {

									$_datas[$_record['id']][$_title]	=	isset($_record[$_column]) && $_record[$_column] ? Dropdown::get_static('security', $_record[$_column], 'view') : '';
								} elseif ( $_column === 'issuing_party_id' ) {

									$_datas[$_record['id']][$_title]	=	isset($_record[$_column]) && $_record[$_column] ? Dropdown::get_static('parties', $_record[$_column], 'view') : '';
								} elseif ( $_column === 'access_right_id' ) {

									$_datas[$_record['id']][$_title]	=	isset($_record[$_column]) && $_record[$_column] ? Dropdown::get_static('access_right', $_record[$_column], 'view') : '';
								} else {

									$_datas[$_record['id']][$_title]	=	isset($_record[$_column]) && $_record[$_column] ? $_record[$_column] : '';
								}
							}
						} elseif ( (strpos( $_name, 'is_') !== FALSE) ) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ( $_records as $_rkey => $_record ) {

								$_datas[$_record['id']][$_title]	=	isset($_record[$_column]) && ($_record[$_column] !== '') ? Dropdown::get_static('bool', $_record[$_column], 'view') : '';
							}
						} else {

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ( $_records as $_rkey => $_record ) {

								$_datas[$_record['id']][$_title]	=	isset($_record[$_name]) && $_record[$_name] ? $_record[$_name] : '';
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_firstAlpha	=	reset($_alphas);
				$_lastAlpha		=	end($_alphas);

	      foreach ( $_xls_columns as $_xkey => $_column ) {

	      	$_title	=	($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

	      	$_objSheet->setCellValue($_xkey.$_row, $_title);
	      }

				$_objSheet->setTitle('List of House Interiors');
				$_objSheet->setCellValue('A1', 'LIST OF HOUSE INTERIORS');
				$_objSheet->mergeCells('A1:'.$_lastAlpha.'1');

	      if ( isset($_datas) && $_datas ) {

      		foreach ( $_datas as $_rkey => $_data ) {

	      		foreach ( $_alphas as $_akey => $_alpha ) {

		        	$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

		        	$_objSheet->setCellValue($_alpha.$_start, $_value);
		        }

		        $_start++;
	      	}
      	} else {

      		$_objSheet->setCellValue($_firstAlpha.$_start, 'No Record Found');
      		$_objSheet->mergeCells($_firstAlpha.$_start.':'.$_lastAlpha.$_start);

      		$_style	=	array(
		      						'font'  => array(
			                	'bold'	=>	FALSE,
			                	'size'	=>	9,
			                	'name'	=>	'Verdana'
			                )
		      					);
		      $_objSheet->getStyle($_firstAlpha.$_start.':'.$_lastAlpha.$_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      	}

				foreach ( $_alphas as $_alpha ) {

	      	$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
	      }

	      $_style	=	array(
	      						'font'  => array(
		                	'bold'	=>	TRUE,
		                	'size'	=>	10,
		                	'name'	=>	'Verdana'
		                )
	      					);
	      $_objSheet->getStyle('A1:'.$_lastAlpha.$_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="'.$_filename.'"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'interior');
			}
		} else {

			$this->notify->error('No Record Found', 'interior');
		}
	}

	function export_csv () {

		if ( $this->input->post() && ($this->input->post('update_existing_data') !== '') ) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'House Interior CSV Template.csv';

			// $_fillables	=	$this->M_interior->fillable;
			$_fillables	=	$this->_table_fillables;
			if ( !$_fillables ) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'interior');
			}

			foreach ( $_fillables as $_fkey => $_fill ) {

				if ( (strpos( $_fill, 'created_') === FALSE) && (strpos( $_fill, 'updated_') === FALSE) && (strpos( $_fill, 'deleted_') === FALSE) ) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ( $_is_update ) {

				$_records	=	$this->M_interior->as_array()->get_all(); #up($_records);
				if ( $_records ) {

					foreach ( $_titles as $_tkey => $_title ) {

						foreach ( $_records as $_rkey => $_record ) {

							$_datas[$_record['id']][$_title]	=	isset($_record[$_title]) && ($_record[$_title] !== '') ? $_record[$_title] : '';
						}
					}
				}
			} else {

				if ( isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id' ) {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('House Interiors');
      $_objSheet->setCellValue('A1', 'HOUSE INTERIORS');
      $_objSheet->mergeCells('A1:'.$_lastAlpha.'1');

			foreach ( $_xls_columns as $_xkey => $_column ) {

      	$_objSheet->setCellValue($_xkey.$_row, $_column);
      }

      if ( $_is_update ) {

      	if ( isset($_datas) && $_datas ) {

      		foreach ( $_datas as $_dkey => $_data ) {

	      		foreach ( $_alphas as $_akey => $_alpha ) {

		        	$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

		        	$_objSheet->setCellValue($_alpha.$_start, $_value);
		        }

		        $_start++;
	      	}
      	} else {

      		$_objSheet->setCellValue($_firstAlpha.$_start, 'No Record Found');
      		$_objSheet->mergeCells($_firstAlpha.$_start.':'.$_lastAlpha.$_start);

      		$_style	=	array(
		      						'font'  => array(
			                	'bold'	=>	FALSE,
			                	'size'	=>	9,
			                	'name'	=>	'Verdana'
			                )
		      					);
		      $_objSheet->getStyle($_firstAlpha.$_start.':'.$_lastAlpha.$_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      	}
      }

			foreach ( $_alphas as $_alpha ) {

      	$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
      }

      $_style	=	array(
      						'font'  => array(
	                	'bold'	=>	TRUE,
	                	'size'	=>	10,
	                	'name'	=>	'Verdana'
	                )
      					);
      $_objSheet->getStyle('A1:'.$_lastAlpha.$_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="'.$_filename.'"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import () {

		if ( isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name'] ) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if ( TRUE ) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch ( Exception $e ) {

					$_msg	=	'Error loading CSV "'.pathinfo($_name, PATHINFO_BASENAME).'": '.$e->getMessage();

					$this->notify->error($_msg, 'interior');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ( $_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2] ) {

					if ( $_sheetData[1][0] === 'id' ) {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_interior->fillable;
					$_fillables	=	$this->_table_fillables;
					if ( !$_fillables ) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'interior');
					}

					foreach ( $_fillables as $_fkey => $_fill ) {

						if ( in_array($_fill, $_sheetData[1]) ) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ( $_sheetData as $_skey => $_sd ) {

						if ( $_skey > 1 ) {

							if ( count(array_filter($_sd)) !== 0 ) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if ( isset($_datas) && $_datas ) {

						foreach ( $_datas as $_dkey => $_data ) {

							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							if ( $_id ) {

								$_record	=	$this->M_interior->get($_id);
								if ( $_record ) {

									unset($_data['id']);

									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_update	=	$this->M_interior->update($_data, $_id);
									if ( $_update !== FALSE ) {

										$_updated++;
									} else {

										$_failed++;

										break;
									}
								} else {

									$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['created_at']	=	NOW;
									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_insert	=	$this->M_interior->insert($_data);
									if ( $_insert !== FALSE ) {

										$_inserted++;
									} else {

										$_failed++;

										break;
									}
								}
							} else {

								$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['created_at']	=	NOW;
								$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['updated_at']	=	NOW;

								$_insert	=	$this->M_interior->insert($_data);
								if ( $_insert !== FALSE ) {

									$_inserted++;
								} else {

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ( $_inserted > 0 ) {

							$_msg	=	$_inserted.' record/s was successfuly inserted';
						}

						if ( $_updated > 0 ) {

							$_msg	.=	($_inserted ? ' and ' : '').$_updated.' record/s was successfuly updated';
						}

						if ( $_failed > 0 ) {

							$this->notify->error('Upload Failde! Please follow upload guide.', 'interior');
						} else {

							$this->notify->success($_msg.'.', 'interior');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'interior');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'interior');
			}
		} else {

			$this->notify->error('Something went wrong!', 'interior');
		}
	}
}