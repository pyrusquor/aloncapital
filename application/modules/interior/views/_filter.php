<?php
	$this->load->model('house/House_model', 'M_house');
	$this->load->model('project/Project_model', 'M_project');
?>
<?php if ( isset($records) && !empty($records) ): ?>

	<div class="row" id="recordList">
		<?php if ( isset($records) && is_array($records) && !empty($records) ): ?>

			<?php foreach ( $records as $_rkey => $record ): ?>

				<?php
					$_price	=	isset($record['price']) && $record['price'] ? money_php($record['price']) : '';
					$improved_price	=	isset($record['improved_price']) && $record['improved_price'] ? money_php($record['improved_price']) : '';
					$fully_furnished_price	=	isset($record['fully_furnished_price']) && $record['fully_furnished_price'] ? money_php($record['fully_furnished_price']) : '';
					$_name	=	isset($record['name']) && $record['name'] ? $record['name'] : '';
					$_hid	=	isset($record['house_model_id']) && $record['house_model_id'] ? $record['house_model_id'] : '';

					$project	=	 $this->M_house->with_project('fields: name')->get($_hid);
					$_project	=	 $project['project']['name'];

					$_house	=	'';

					if ( isset($record['house']) && $record['house'] && !empty($record['house']) ) {

						$_hname	=	isset($record['house']['name']) && $record['house']['name'] ? $record['house']['name'] : '';

						// $_house	=	isset($_hname) && $_hname ? get_initials($_hname) : '';
						$_house	=	isset($_hname) && $_hname ? $_hname : '';
					}

					$_id	=	isset($record['id']) && $record['id'] ? $record['id'] : '';

					if ( $_id ) {

						$_update_price	=	site_url('interior/update_price/'.$_id);
						$_update				=	site_url('interior/update/'.$_id);
						$_view					=	site_url('interior/view/'.$_id);



						// if ($_id == 126) {
							// vdebug($record);
							// vdebug($project);
						// }

						$_sub		=	 isset($_name) && $_name ? get_initials($_name) : '';
					}
				?>

				<div class="col-md-4 col-xl-3">
					<div class="kt-portlet kt-portlet--height-fluid">
						<div class="kt-portlet__head kt-portlet__head--noborder">
							<div class="kt-portlet__head-label">
								<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
									<input type="checkbox" name="id[]" value="<?php echo @$_id;?>" class="m-checkable delete_check" data-id="<?php echo @$_id;?>">
									<span></span>
								</label>
							</div>
							<div class="kt-portlet__head-toolbar">
								<a href="#" class="btn btn-icon" data-toggle="dropdown">
									<i class="flaticon-more-1 kt-font-brand"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<ul class="kt-nav">
                                        <?php if($this->ion_auth->is_admin()): ?>
                                            <li class="kt-nav__item">
                                                <a href="<?php echo @$_update_price;?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                    <span class="kt-nav__link-text">Update Price</span>
                                                </a>
                                            </li>
                                            <!-- <li class="kt-nav__item">
                                                <a href="<?php echo @$_view;?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                    <span class="kt-nav__link-text">Price Logs</span>
                                                </a>
                                            </li> -->
                                            <li class="kt-nav__item">
                                                <a href="<?php echo @$_view;?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                    <span class="kt-nav__link-text">View</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="<?php echo @$_update;?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                    <span class="kt-nav__link-text">Update</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="javascript: void(0)" class="kt-nav__link remove_record" data-id="<?php echo @$_id;?>">
                                                    <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                    <span class="kt-nav__link-text">Delete</span>
                                                </a>
                                            </li>
                                        <?php else: ?>
                                            <li class="kt-nav__item">
                                                <a href="<?php echo @$_view;?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                    <span class="kt-nav__link-text">View</span>
                                                </a>
                                            </li>
                                        <?php endif; ?>
									</ul>
								</div>
							</div>
						</div>
						 <div class="kt-portlet__body kt-portlet__body--fit-y">
						 	<div class="kt-widget kt-widget--user-profile-4">
						 		<div class="kt-widget__head">
						 			<div class="kt-widget__media">
						 				<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
						 					<?php echo @$_sub;?>
						 				</div>
						 			</div>
						 			<div class="kt-widget__content">
						 				<div class="kt-widget__section">
						 					<a href="javascript: void(0);" class="kt-widget__username">
						 						<?php echo @$_id;?>. <?php echo @$_name;?>
						 					</a>
						 					<div class="kt-widget__button">
						 						<span class="btn btn-label-warning btn-sm">
						 							<?php echo @$_house;?>
						 						</span>
						 					</div>
						 				</div>
						 			</div>
						 		</div>
						 	</div>
						 	<div class="kt-widget kt-widget--user-profile-2">
						 		<div class="kt-widget__body">
						 			<div class="kt-widget__item">
						 				<div class="kt-widget__contact">
						 					<span class="kt-widget__data">Project</span>
						 					<span class="kt-widget__label">
						 						<?php echo @$_project;?>
						 					</span>
						 				</div>
						 				<div class="kt-widget__contact">
						 					<span class="kt-widget__data">Latest Basic Price</span>
						 					<span class="kt-widget__label">
						 						<?php echo @$_price;?>
						 					</span>
						 				</div>
						 				<div class="kt-widget__contact">
						 					<span class="kt-widget__data">Latest Improved Price</span>
						 					<span class="kt-widget__label">
						 						<?php echo @$improved_price;?>
						 					</span>
						 				</div>
						 				<div class="kt-widget__contact">
						 					<span class="kt-widget__data">Latest Furnished Price</span>
						 					<span class="kt-widget__label">
						 						<?php echo @$fully_furnished_price;?>
						 					</span>
						 				</div>
						 			</div>
						 		</div>
						 	</div>
						 </div>
						<div class="kt-portlet__foot kt-portlet__foot--sm text-center">
							<a href="<?php echo @$_view;?>" class="btn btn-brand btn-sm btn-upper">
						 		View
						 	</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<div class="row">
		<div class="col-xl-12">

			<!--begin:: Components/Pagination/Default-->
			<div class="kt-portlet">
				<div class="kt-portlet__body">

					<!--begin: Pagination-->
					<div class="kt-pagination kt-pagination--brand">
						<?php echo $this->ajax_pagination->create_links(); ?>

						<div class="kt-pagination__toolbar">
							<span class="pagination__desc">
								<?php echo $this->ajax_pagination->show_count(); ?>
							</span>
						</div>
					</div>

					<!--end: Pagination-->
				</div>
			</div>

			<!--end:: Components/Pagination/Default-->
		</div>
	</div>
<?php else: ?>

	<div class="row">
		<div class="col-lg-12">
			<div class="kt-portlet kt-callout">
				<div class="kt-portlet__body">
					<div class="kt-callout__body">
						<div class="kt-callout__content">
							<h3 class="kt-callout__title">No Records Found</h3>
							<p class="kt-callout__desc">
								Sorry no record were found. Please adjust your search criteria and try again.
							</p>
						</div>
						<div class="kt-callout__action">
							<button class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand" data-toggle="modal" data-target="#filterModal">Try Again</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>