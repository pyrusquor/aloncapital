<?php
	// print_r($house); exit;
	
	
	$id =	isset($interior['id']) && $interior['id'] ? $interior['id'] : '';
    $house_model_id =	isset($interior['house_model_id']) && $interior['house_model_id'] ? $interior['house_model_id'] : '';
    $house_model_name =	isset($interior['house']['name']) && $interior['house']['name'] ? $interior['house']['name'] : '';
    $name =	isset($interior['name']) && $interior['name'] ? $interior['name'] : '';
    $price =	isset($interior['price']) && $interior['price'] ? $interior['price'] : '0';
    $is_multiply_floor_area =	isset($interior['is_multiply_floor_area']) && $interior['is_multiply_floor_area'] ? $interior['is_multiply_floor_area'] : '0';
    $is_multiply_lot_area =	isset($interior['is_multiply_lot_area']) && $interior['is_multiply_lot_area'] ? $interior['is_multiply_lot_area'] : '0';
    $active =	isset($interior['is_active']) && $interior['is_active'] ? $interior['is_active'] : '';
?>

<div class="row">
	<div class="col-md-6 col-xl-6" >
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Property Model<span class="kt-font-danger">&nbsp;&nbsp;*</span></label>
			<label class="col-8 col-form-label">
				<select class="form-control kt-select2" id="house_model_id" name="house_model_id">
					<?php foreach ($house as $h): ?>
						<option value="<?php echo $h['id'] ?>" <?php echo ($house_model_id == $h['id']) ? 'selected' : '' ?>><?php echo ucwords($h['code']).' - '.ucwords($h['name']); ?></option>
					<?php endforeach;?>
				</select>
			</label>
		</div>
		<!--<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Project</label>
			<div class="col-8">
				<input type="text" class="form-control" id="project" name="project" placeholder="" disabled>
			</div>
		</div>-->
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Name</label>
			<div class="col-8">
				<input type="text" class="form-control" id="name" name="name" value="<?php echo set_value('name', $name); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<label class="col-4 col-form-label kt-font-bolder">Price</label>
			<div class="col-8">
				<input type="text" class="form-control" id="price" name="price" value="<?php echo set_value('name', $price); ?>"  placeholder="">
			</div>
		</div>
		<div class="form-group row" style="margin-bottom: -1rem;">
			<div class="col-1 col-form-label">
				<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
					<input type="checkbox" value="" class="m-checkable" id="is_multiply_floor_area" name="is_multiply_floor_area" <?php echo ($is_multiply_floor_area > 0) ? 'checked="checked"' : '' ?> >
					<span></span>
				</label>
			</div>
			<label class="col-11 col-form-label kt-font-bolder">Multiply by Floor Area</label>
		</div>
		<div class="form-group row" style="margin-bottom: 1rem;">
			<div class="col-1 col-form-label">
				<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
					<input type="checkbox" value="" class="m-checkable" id="is_multiply_lot_area" name="is_multiply_lot_area" <?php echo ($is_multiply_lot_area > 0) ? 'checked="checked"' : '' ?>>
					<span></span>
				</label>
			</div>
			<label class="col-11 col-form-label kt-font-bolder">Multiply by Lot Area</label>
		</div>
	</div>
</div>