<?php 
    if(isset($filter))
    {
        if ($filter['filter_house_model_id'] == '' && $filter['filter_name'] == '' && $filter['filter_price'] == '')
        {
            unset($filter);
        }
    }
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">House Interiors</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc"><?php echo (!empty($record)) ? count($record) : 0;  ?> TOTAL</span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search interior..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<a href="<?php echo base_url('interior/create'); ?>" class="btn btn-label-primary"><i class="la la-plus"></i> Create Interior</a>&nbsp;
				<button class="<?php echo (!empty($filter)) ? 'btn btn-label-danger' :'btn btn-label-primary' ?>" data-toggle="modal" data-target="#filterModal"><i class="la la-filter"></i> Filter</button>&nbsp;
                <a href="#" class="btn btn-label-primary"><i class="la la-upload"></i> Import</a>&nbsp;
                <a href="<?php echo base_url('interior/export'); ?>" class="btn btn-label-primary"><i class="la la-download"></i> Export</a>&nbsp;
			</div>
		</div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
		<?php if (!empty($record)) : ?>
		<?php foreach ($record as $r): ?>
			<div class="col-md-4 col-xl-4">
				<div class="kt-portlet">
					<div class="kt-portlet__head kt-portlet__head--noborder">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<a href="#" class="btn btn-icon" data-toggle="dropdown">
								<i class="flaticon-more-1 kt-font-brand"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<ul class="kt-nav">
									<li class="kt-nav__item">
										<a href="<?php echo base_url('interior/view/'.$r['id']); ?>" class="kt-nav__link">
											<i class="kt-nav__link-icon flaticon2-line-chart"></i>
											<span class="kt-nav__link-text">View</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="<?php echo base_url('interior/update/'.$r['id']); ?>" class="kt-nav__link">
											<i class="kt-nav__link-icon flaticon2-settings"></i>
											<span class="kt-nav__link-text">Update</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="javascript: void(0)" class="kt-nav__link remove_interior" data-id="<?php echo $r['id']; ?>">
											<i class="kt-nav__link-icon flaticon2-send"></i>
											<span class="kt-nav__link-text">Delete</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon flaticon2-send"></i>
											<span class="kt-nav__link-text">Price Logs</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body" style="padding: 0px 20px 15px;">
						<!--begin::Widget -->
						<div class="kt-widget kt-widget--user-profile-4" style="margin-top: -40px;">
							<div class="kt-widget__head">
								<div class="kt-widget__media">
									<img class="kt-widget__img kt-hidden" src="<?php echo base_url('assets/media/users/300_21.jpg'); ?>" alt="image">
									<div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-" style="width: 150px; height: 150px;">
										<h1>HI</h1>
									</div>
								</div>
								<div class="kt-widget__content">
									<div class="kt-widget__section" style="text-align: center;">
										<a href="<?php echo base_url('interior/view/'.$r['id']); ?>" class="kt-widget__username"><h2><?php echo ucwords($r['name']); ?></h2></a>
										<span class="kt-widget__desc"><h5><?php echo ucwords($r['house']['name']); ?></h5></span>
										<br />
										<br />
										<div class="kt-widget__text kt-align-left" style="float: left; margin-left: 30px;">Price</div>
										<div class="kt-widget__text kt-align-right" style="font-weight: 600; margin-right: 30px;"><?php  echo $r['price']; ?></div>
										<br />
										<div class="kt-widget__button">
											<a href="<?php echo base_url('interior/view/'.$r['id']); ?>" class="btn btn-brand btn-sm">View</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--end::Widget -->
					</div>
				</div>
			</div>
		<?php endforeach;?>
		<?php endif;?>
	</div>
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('interior/filter'); ?>" method="POST" id="filter_form">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="filter_project" class="form-control-label">Property Model:</label>
                            <select class="form-control kt-select2" id="filter_house_model_id" name="filter_house_model_id">
								<option value=""></option>
								<?php foreach ($house as $h): ?>
									<option value="<?php echo $h['id'] ?>" <?php echo ((isset($filter['filter_house_model_id'])) ? $filter['filter_house_model_id'] : '' == $h['id']) ? 'selected' : '' ?>><?php echo ucwords($h['code']).' - '.ucwords($h['name']); ?></option>
								<?php endforeach;?>
							</select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="filter_name" class="form-control-label">Interior Name:</label>
                            <input type="text" class="form-control" id="filter_name" name="filter_name" value="<?php echo set_value('filter_name', (isset($filter['filter_name'])) ? $filter['filter_name'] : ''); ?>">
                        </div>
						<div class="form-group col-md-6">
                            <label for="filter_price" class="form-control-label">Price:</label>
                            <input type="text" class="form-control" id="filter_price" name="filter_price" value="<?php echo set_value('filter_price', (isset($filter['filter_price'])) ? $filter['filter_price'] : ''); ?>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="apply_filter">Apply</button>
                <a href="<?php echo base_url('interior'); ?>" class="btn btn-danger" id="remove_filter" style="<?php echo (!empty($filter)) ? '' : 'display: none'; ?>">Remove</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>