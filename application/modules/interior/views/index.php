<?php
if (isset($filter)) {
    if ($filter['filter_house_model_id'] == '' && $filter['filter_name'] == '' && $filter['filter_price'] == '') {
        unset($filter);
    }
}
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">House Interiors</h3>
            <?php if (isset($totalRec) && $totalRec): ?>

                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc" id="totalRec"><?php echo $totalRec; ?> TOTAL</span>
            <?php endif; ?>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search project..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
					<span><i class="flaticon2-search-1"></i></span>
				</span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <?php if ($this->ion_auth->is_admin()): ?>
                    <!-- <a href="<?php echo site_url('interior/create'); ?>"
                       class="btn btn-label-primary btn-elevate btn-sm">
                        <i class="fa fa-plus"></i> Create
                    </a>
                    <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal"
                            data-target="#filterModal">
                        <i class="fa fa-filter"></i> Filter
                    </button> -->
                    <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm"
                            data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true"
                            aria-controls="_batch_upload">
                        <i class="fa fa-upload"></i> Import
                    </button>
                    <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal"
                            data-target="#_export_option">
                        <i class="fa fa-download"></i> Export
                    </button>
                    <button type="button" class="btn btn-label-primary btn-elevate btn-sm" id="bulkDelete" disabled>
                        <i class="fa fa-trash"></i> Delete Selected
                    </button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
                <a href="<?php echo site_url('interior/create'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Create Project
                </a>             
        </div>

        <div class="module__filter">
                <button class="btn btn-label-primary btn-elevate btn-sm btn-filter"
                    data-toggle="modal" aria-expanded="true" data-target="#filterModal">
                    <i class="fa fa-filter"></i> Filter
                </button>
        </div>
    </div>
</div>

<!-- Batch Upload -->
<div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <form class="kt-form" id="_export_csv" action="<?php echo site_url('interior/export_csv'); ?>" method="POST"
                  enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">File Type</label>
                            <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control form-control-sm" name="update_existing_data">
                                    <option value=""> -- Update Existing Data --</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-cloud-upload"></i></span></span>
                            </div>
                            <?php echo form_error('update_existing_data'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <form class="kt-form" id="_upload_form" action="<?php echo site_url('interior/import'); ?>" method="POST"
                  enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Upload CSV file:</label>
                            <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per
                                file.</label>
                            <input type="file" name="csv_file" class="" size="1000" accept="*.csv">
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group form-group-last row custom_import_style">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm"
                                    form="_upload_form">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                        <div class="col-lg-6 kt-align-right">
                            <button type="submit"
                                    class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm"
                                    form="_export_csv">
                                <i class="fa fa-file-csv"></i>
                            </button>
                            <button type="button"
                                    class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm"
                                    data-toggle="modal" data-target="#upload_guide">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Batch Upload Guide -->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upload_guide_label">House Interior Upload Guide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-head-bg-brand table-striped table-hover table-bordered table-condensed table-checkable"
                       id="_upload_guide_modal">
                    <thead>
                    <tr class="text-center">
                        <th width="1%" scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Type</th>
                        <th scope="col">Format</th>
                        <th scope="col">Option</th>
                        <th scope="col">Required</th>
                    </tr>
                    </thead>
                    <?php if (isset($_fillables) && $_fillables): ?>

                        <tbody>
                        <?php foreach ($_fillables as $_fkey => $_fill): ?>

                            <?php
                            $_no = isset($_fill['no']) && $_fill['no'] ? $_fill['no'] : '';
                            $_name = isset($_fill['name']) && $_fill['name'] ? $_fill['name'] : '';
                            $_type = isset($_fill['type']) && $_fill['type'] ? $_fill['type'] : '';
                            $_required = isset($_fill['required']) && $_fill['required'] ? $_fill['required'] : '';
                            $_dropdown = isset($_fill['dropdown']) && $_fill['dropdown'] && !empty($_fill['dropdown']) ? $_fill['dropdown'] : '';
                            ?>

                            <tr>
                                <td><?php echo @$_no; ?></td>
                                <td class="_ug_name"><?php echo @$_name; ?></td>
                                <td><?php echo @$_type; ?></td>
                                <td></td>
                                <td class="_ug_option">
                                    <?php if ($_dropdown): ?>

                                        <ul class="_ul">
                                            <?php foreach ($_dropdown as $_dkey => $_drop): ?>

                                                <li><?php echo @$_drop; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if ($_required === 'Yes'): ?>

                                        <span class="kt-font-danger">Yes</span>
                                    <?php elseif ($_required === 'No'): ?>

                                        No
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm btn-icon-sm"
                        data-dismiss="modal">
                    <i class="la la-close"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->model('house/House_model', 'M_house');
$this->load->model('project/Project_model', 'M_project');
?>
<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="recordsContent">
    <div class="row" id="recordList">
        <?php if (isset($records) && is_array($records) && !empty($records)): ?>

            <?php foreach ($records as $_rkey => $record): ?>

                <?php
                $_price = isset($record['price']) && $record['price'] ? money_php($record['price']) : '';
                $improved_price = isset($record['improved_price']) && $record['improved_price'] ? money_php($record['improved_price']) : '';
                $fully_furnished_price = isset($record['fully_furnished_price']) && $record['fully_furnished_price'] ? money_php($record['fully_furnished_price']) : '';
                $_name = isset($record['name']) && $record['name'] ? $record['name'] : '';
                $_house = '';
                if (isset($record['house']) && $record['house'] && !empty($record['house'])) {

                    $_hname = isset($record['house']['name']) && $record['house']['name'] ? $record['house']['name'] : '';
                    $_hid = isset($record['house_id']) && $record['house_id'] ? $record['house_id'] : '';

                    // $_house	=	isset($_hname) && $_hname ? get_initials($_hname) : '';
                    $_house = isset($_hname) && $_hname ? $_hname : '';
                }

                $_id = isset($record['id']) && $record['id'] ? $record['id'] : '';
                if ($_id) {

                    $_update_price = site_url('interior/update_price/' . $_id);
                    $_update = site_url('interior/update/' . $_id);
                    $_view = site_url('interior/view/' . $_id);

                    $project = $this->M_house->with_project('fields: name')->get($_hid);
                    $_project = $project['project']['name'];
                    $_sub = isset($_name) && $_name ? get_initials($_name) : '';
                }
                ?>

                <div class="col-md-4 col-xl-3">
                    <div class="kt-portlet kt-portlet--height-fluid">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input type="checkbox" name="id[]" value="<?php echo @$_id; ?>"
                                           class="m-checkable delete_check" data-id="<?php echo @$_id; ?>">
                                    <span></span>
                                </label>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                    <i class="flaticon-more-1 kt-font-brand"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <?php if ($this->ion_auth->is_admin()): ?>
                                            <li class="kt-nav__item">
                                                <a href="<?php echo @$_update_price; ?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                    <span class="kt-nav__link-text">Update Price</span>
                                                </a>
                                            </li>
                                            <!-- <li class="kt-nav__item">
											<a href="<?php echo @$_view; ?>" class="kt-nav__link">
												<i class="kt-nav__link-icon flaticon2-checking"></i>
												<span class="kt-nav__link-text">Price Logs</span>
											</a>
										    </li> -->
                                            <li class="kt-nav__item">
                                                <a href="<?php echo @$_view; ?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                    <span class="kt-nav__link-text">View</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="<?php echo @$_update; ?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                    <span class="kt-nav__link-text">Update</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="javascript: void(0)" class="kt-nav__link remove_record"
                                                   data-id="<?php echo @$_id; ?>">
                                                    <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                    <span class="kt-nav__link-text">Delete</span>
                                                </a>
                                            </li>
                                        <?php else: ?>
                                            <li class="kt-nav__item">
                                                <a href="<?php echo @$_view; ?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                    <span class="kt-nav__link-text">View</span>
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit-y">
                            <div class="kt-widget kt-widget--user-profile-4">
                                <div class="kt-widget__head">
                                    <div class="kt-widget__media">
                                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                            <?php echo @$_sub; ?>
                                        </div>
                                    </div>
                                    <div class="kt-widget__content">
                                        <div class="kt-widget__section">
                                            <a href="javascript: void(0);" class="kt-widget__username">
                                                <?php echo @$_id; ?>. <?php echo @$_name; ?>
                                            </a>
                                            <div class="kt-widget__button">
						 						<span class="btn btn-label-warning btn-sm">
						 							<?php echo @$_house; ?>
						 						</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget kt-widget--user-profile-2">
                                <div class="kt-widget__body">
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__data">Project</span>
                                            <span class="kt-widget__label">
						 						<?php echo @$_project; ?>
						 					</span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__data">Latest Basic Price</span>
                                            <span class="kt-widget__label">
						 						<?php echo @$_price; ?>
						 					</span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__data">Latest Improved Price</span>
                                            <span class="kt-widget__label">
						 						<?php echo @$improved_price; ?>
						 					</span>
                                        </div>
                                        <div class="kt-widget__contact">
                                            <span class="kt-widget__data">Latest Furnished Price</span>
                                            <span class="kt-widget__label">
						 						<?php echo @$fully_furnished_price; ?>
						 					</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot kt-portlet__foot--sm text-center">
                            <a href="<?php echo @$_view; ?>" class="btn btn-brand btn-sm btn-upper">
                                View
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
							<span class="pagination__desc">
								<?php echo $this->ajax_pagination->show_count(); ?>
							</span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="advanceSearch">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="filter_house_model_id" class="form-control-label">Property Model:</label>
                            <select class="form-control" id="filter_house_model_id" name="filter_house_model_id">
                                <?php foreach ($houses as $h): ?>
                                    <option value="<?php echo $h['id'] ?>" <?php echo (@$filter_house_model_id == $h['id']) ? 'selected' : '' ?>>
                                        <?php echo ucwords($h['project']['name']); ?>
                                        - <?php echo ucwords($h['name']); ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="filter_name" class="form-control-label">Interior Name:</label>
                            <input type="text" class="form-control" id="filter_name" name="filter_name"
                                   value="<?php echo set_value('filter_name', (isset($filter['filter_name'])) ? $filter['filter_name'] : ''); ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="filter_price" class="form-control-label">Price:</label>
                            <input type="text" class="form-control" id="filter_price" name="filter_price"
                                   value="<?php echo set_value('filter_price', (isset($filter['filter_price'])) ? $filter['filter_price'] : ''); ?>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form" target="_blank"
                      action="<?php echo site_url('interior/export'); ?>" method="POST">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group form-group-last kt-hide">
                                <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                    <div class="alert-text">
                                        Oh snap! You need select at least one.
                                    </div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($_columns) && $_columns): ?>

                            <div class="col-lg-11 offset-lg-1">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" id="_export_select_all"> Field
                                        <span></span>
                                    </label>
                                    <label class="kt-checkbox kt-checkbox--bold"></label>
                                </div>
                            </div>

                            <?php foreach ($_columns as $key => $_column): ?>

                                <?php if ($_column): ?>

                                    <?php
                                    $_offset = '';
                                    if ($_column === reset($_columns)) {

                                        $_offset = 'offset-lg-1';
                                    }
                                    ?>

                                    <div class="col-lg-5 <?php echo isset($_offset) && $_offset ? $_offset : ''; ?>">
                                        <div class="kt-checkbox-list">
                                            <?php foreach ($_column as $_ckey => $_clm): ?>

                                                <?php
                                                $_label = isset($_clm['label']) && $_clm['label'] ? $_clm['label'] : '';
                                                $_value = isset($_clm['value']) && $_clm['value'] ? $_clm['value'] : '';
                                                ?>

                                                <label class="kt-checkbox kt-checkbox--bold">
                                                    <input type="checkbox" name="_export_column[]"
                                                           class="_export_column"
                                                           value="<?php echo @$_value; ?>"> <?php echo @$_label; ?>
                                                    <span></span>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>

                            <div class="col-lg-10 offset-lg-1">
                                <div class="form-group form-group-last">
                                    <div class="alert alert-solid-danger alert-bold fade show" role="alert"
                                         id="form_msg">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            Something went wrong. Please contact your system administrator.
                                        </div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left"
                            data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit"
                            class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right"
                            form="_export_form">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->