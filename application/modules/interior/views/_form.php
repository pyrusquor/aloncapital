<?php
	// print_r($house); exit;
	
	
	$id =	isset($interior['id']) && $interior['id'] ? $interior['id'] : '';
    $house_model_id =	isset($interior['house_model_id']) && $interior['house_model_id'] ? $interior['house_model_id'] : '';
    $house_model_name =	isset($interior['house']['name']) && $interior['house']['name'] ? $interior['house']['name'] : '';
    $name =	isset($interior['name']) && $interior['name'] ? $interior['name'] : '';
    $price =	isset($interior['price']) && $interior['price'] && ($interior['price'] > 0) ? $interior['price'] : NULL;
    $improved_price =	isset($interior['improved_price']) && $interior['improved_price'] && ($interior['improved_price'] > 0) ? $interior['improved_price'] : NULL;
    $fully_furnished_price =	isset($interior['fully_furnished_price']) && $interior['fully_furnished_price'] && ($interior['fully_furnished_price'] > 0) ? $interior['fully_furnished_price'] : NULL;
    $is_multiply_floor_area =	isset($interior['is_multiply_floor_area']) && $interior['is_multiply_floor_area'] ? TRUE : FALSE;
    $is_multiply_lot_area =	isset($interior['is_multiply_lot_area']) && $interior['is_multiply_lot_area'] ? TRUE : FALSE;
    $active =	isset($interior['is_active']) && $interior['is_active'] ? $interior['is_active'] : '';
	$date_effectivity	=	isset($interior['date_effectivity']) && $interior['date_effectivity'] && ($interior['date_effectivity'] > 0) ? date_format(date_create($interior['date_effectivity']), 'F j, Y') : '';

    $_disabled	=	isset($update_price) && $update_price ? 'disabled' : '';
?>

<div class="kt-section kt-section--first">
	<div class="row">
		<div class="col-lg-6">
			<div class="row">
				<label class="col-lg-4 col-form-label form-control-label">Property Model <code>*</code></label>
				<div class="form-group col-lg-8">
					<div class="kt-input-icon kt-input-icon--left">
						<?php //echo form_dropdown('house_model_id', Dropdown::get(['id', 'name', 'code'], 'House_model', 'id', 'name', 'Select Property Model'), set_value('house_model_id', @$house_model_id), 'class="form-control" id="house_model_id" '.@$_disabled); ?>

	            			<select class="form-control"  id="house_model_id" name="house_model_id" <?php echo @$_disabled; ?>>
								
							<?php foreach ($houses as $h): ?>
	                                <option value="<?php echo $h['id'] ?>" <?php echo ($house_model_id == $h['id']) ? 'selected' : '' ?>>
	                                    <?php echo ucwords($h['project']['name']); ?> - <?php echo ucwords($h['name']); ?>
	                                </option>
	                        <?php endforeach;?>
							
							</select>

						<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-certificate"></i></span></span>
					</div>
					<?php echo form_error('house_model_id'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="row">
				<label class="col-lg-4 col-form-label form-control-label">Name</label>
				<div class="form-group col-lg-8">
					<div class="kt-input-icon kt-input-icon--left">
						<input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo set_value('name', @$name);?>" autocomplete="off" <?php echo @$_disabled;?>>
						<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-legal"></i></span></span>
					</div>
					<?php echo form_error('name'); ?>
					<span class="form-text text-muted"></span>
				</div>
			</div>
		</div>
	</div>
	<?php if( isset($update_price) && $update_price ): ?>

		<div class="row">
			<div class="col-lg-6">
				<div class="row">
					<label class="col-lg-4 col-form-label form-control-label">Basic Price</label>
					<div class="form-group col-lg-8">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" name="price" class="form-control" placeholder="Price" value="<?php echo set_value('price', @$price);?>" autocomplete="off">
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-money"></i></span></span>
						</div>
						<?php echo form_error('price'); ?>
						<span class="form-text text-muted"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="row">
					<label class="col-lg-4 col-form-label form-control-label">As Improved Amount Price</label>
					<div class="form-group col-lg-8">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" name="improved_price" class="form-control" placeholder="Improved Price" value="<?php echo set_value('improved_price', @$improved_price);?>" autocomplete="off">
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-money"></i></span></span>
						</div>
						<?php echo form_error('improved_price'); ?>
						<span class="form-text text-muted"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="row">
					<label class="col-lg-4 col-form-label form-control-label">Fully Furnished Price</label>
					<div class="form-group col-lg-8">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" name="fully_furnished_price" class="form-control" placeholder="Fully Furnished Price" value="<?php echo set_value('fully_furnished_price', @$fully_furnished_price);?>" autocomplete="off">
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-money"></i></span></span>
						</div>
						<?php echo form_error('fully_furnished_price'); ?>
						<span class="form-text text-muted"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="row">
					<label class="col-lg-4 col-form-label form-control-label">Date Effectivity</label>
					<div class="form-group col-lg-8">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" name="date_effectivity" class="form-control kt_datepicker" placeholder="Date Effectivity" value="<?php echo set_value('date_effectivity', @$date_effectivity);?>" autocomplete="off" readonly>
							<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
						</div>
						<?php echo form_error('date_effectivity'); ?>
						<span class="form-text text-muted"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="kt-checkbox-list">
					<label class="kt-checkbox kt-checkbox--bold">
						<input type="checkbox" name="is_multiply_floor_area" class="hide is_multiply_floor_area" value="1" <?php echo set_checkbox('is_multiply_floor_area', '1', @$is_multiply_floor_area);?>> Multiply Basic Price by Floor Area
						<span></span>
					</label>
					<label class="kt-checkbox kt-checkbox--bold"></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="kt-checkbox-list">
					<label class="kt-checkbox kt-checkbox--bold">
						<input type="checkbox" name="is_multiply_lot_area" class="hide is_multiply_lot_area" value="1" <?php echo set_checkbox('is_multiply_lot_area', '1', @$is_multiply_lot_area);?>> Multiply Basic Price by Lot Area
						<span></span>
					</label>
					<label class="kt-checkbox kt-checkbox--bold"></label>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>