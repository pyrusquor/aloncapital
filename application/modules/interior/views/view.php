<?php
    // print_r($interior); exit;
	
	$id =	isset($interior['id']) && $interior['id'] ? $interior['id'] : '';
    $house_model_id =	isset($interior['house_model_id']) && $interior['house_model_id'] ? $interior['house_model_id'] : '';
    $house_model_name =	isset($interior['house']['name']) && $interior['house']['name'] ? $interior['house']['name'] : '';
    $name =	isset($interior['name']) && $interior['name'] ? $interior['name'] : '';
    $price =	isset($interior['price']) && $interior['price'] ? $interior['price'] : '0';
    $improved_price =	isset($interior['improved_price']) && $interior['improved_price'] ? $interior['improved_price'] : '0';
    $fully_furnished_price =	isset($interior['fully_furnished_price']) && $interior['fully_furnished_price'] ? $interior['fully_furnished_price'] : '0';
    $is_multiply_floor_area =	isset($interior['is_multiply_floor_area']) && $interior['is_multiply_floor_area'] ? $interior['is_multiply_floor_area'] : '0';
    $is_multiply_lot_area =	isset($interior['is_multiply_lot_area']) && $interior['is_multiply_lot_area'] ? $interior['is_multiply_lot_area'] : '0';
    $active =	isset($interior['is_active']) && $interior['is_active'] ? $interior['is_active'] : '';
	
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">House Interior</h3>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
                <?php if($this->ion_auth->is_admin()): ?>
                    <a href="<?php echo site_url('interior/update/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
                        <i class="fa fa-edit"></i> Edit
                    </a>
                <?php endif; ?>
				<a href="<?php echo site_url('interior');?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
			</div>
		</div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-md-4 col-xl-4" >
							<div class="form-group row" style="margin-bottom: 1rem;">
								<label class="col-4 col-form-label kt-font-bolder">Property Model</label>
								<label class="col-8 col-form-label">
									<?php echo ucwords($house_model_name); ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 1rem;">
								<label class="col-4 col-form-label kt-font-bolder">Name</label>
								<label class="col-8 col-form-label">
									<?php echo ucwords($name); ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 1rem;">
								<label class="col-4 col-form-label kt-font-bolder">Basic Price</label>
								<label class="col-8 col-form-label">
									<?php echo $price; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 1rem;">
								<label class="col-4 col-form-label kt-font-bolder">As Improved Amount Price</label>
								<label class="col-8 col-form-label">
									<?php echo $improved_price; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 1rem;">
								<label class="col-4 col-form-label kt-font-bolder">Fully Furnished Price</label>
								<label class="col-8 col-form-label">
									<?php echo $fully_furnished_price; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: -1rem;">
								<div class="col-1 col-form-label">
									<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
										<input type="checkbox" value="1" class="m-checkable" <?php echo ($is_multiply_floor_area > 0) ? 'checked="checked"' : '' ?> >
										<span></span>
									</label>
								</div>
								<label class="col-11 col-form-label kt-font-bolder">Multiply Basic Price by Floor Area</label>
							</div>
							<div class="form-group row" style="margin-bottom: 1rem;">
								<div class="col-1 col-form-label">
									<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
										<input type="checkbox" value="" class="m-checkable" <?php echo ($is_multiply_lot_area > 0) ? 'checked="checked"' : '' ?>>
										<span></span>
									</label>
								</div>
								<label class="col-11 col-form-label kt-font-bolder">Multiply Basic Price by Lot Area</label>
							</div>
						</div>

						<?php if ( isset($price_logs) && $price_logs ): ?>
							<div class="col-md-8 col-xl-8">
								<div class="kt-section__body">
									<h5 class="kt-font-primary text-center">Price Logs</h5>
									<div class="kt-portlet kt-portlet--bordered">
										<div class="kt-section__body">
											<div class="row">
												<div class="col-sm-12">
													<div class="d-flex flex-column bd-highlight mb-3">
														<div class="p-4 bd-highlight">
														  	<div class="row">
														  		<div class="col-sm-2 text-center">
														  			<h6 class="kt-font-dark">Basic Price</h6>
																</div>
															  <div class="col-sm-3 text-center">
													  			<h6 class="kt-font-dark">As Improved</h6>
															  </div>
															  <div class="col-sm-3 text-center">
													  			<h6 class="kt-font-dark">Fully Furnished</h6>
															  </div>
																<div class="col-sm-2 text-center">
														  			<h6 class="kt-font-dark">Date Effectivity</h6>
														  		</div>
														  		<div class="col-sm-2 text-center">
														  			<h6 class="kt-portlet__head-title kt-font-dark">Date Updated</h6>
														  		</div>
														  	</div>	
													  	</div>
														<?php foreach ( $price_logs as $plkey => $pl ): ?>

															<?php
																$_value	=	isset($pl['price']) && $pl['price'] && ($pl['price'] > 0) ? money_php($pl['price'], 'PHP') : '';

																$improved_price	=	isset($pl['improved_price']) && $pl['improved_price'] && ($pl['improved_price'] > 0) ? money_php($pl['improved_price'], 'PHP') : '';

																$fully_furnished_price	=	isset($pl['fully_furnished_price']) && $pl['fully_furnished_price'] && ($pl['fully_furnished_price'] > 0) ? money_php($pl['fully_furnished_price'], 'PHP') : '';


																$_date_effectiviy	=	isset($pl['date_effectivity']) && $pl['date_effectivity'] && ($pl['date_effectivity'] > 0) ? date_format(date_create($pl['date_effectivity']), 'F j, Y') : '';

																$_date	=	isset($pl['created_at']) && $pl['created_at'] && ($pl['created_at'] > 0) ? date_format(date_create($pl['created_at']), 'F j, Y') : '';
															?>

															<div class="p-4 bd-highlight">
														  	<div class="row">
														  		<div class="col-sm-2 text-center">
														  			<h6 class="kt-font-dark">
														  				<?php echo @$_value;?>
														  			</h6>
																</div>

																<div class="col-sm-3 text-center">
														  			<h6 class="kt-font-dark">
														  				<?php echo @$improved_price;?>
														  			</h6>
																</div>

																<div class="col-sm-3 text-center">
														  			<h6 class="kt-font-dark">
														  				<?php echo @$fully_furnished_price;?>
														  			</h6>
																</div>

																<div class="col-sm-2 text-center">
														  			<h6 class="kt-font-dark">
														  				<?php echo @$_date_effectiviy;?>
														  			</h6>
														  		</div>
														  		
														  		<div class="col-sm-2 text-center">
														  			<h6 class="kt-portlet__head-title kt-font-dark">
																			<?php echo @$_date;?>
																		</h6>
														  		</div>
														  	</div>
														  </div>
														<?php endforeach; ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>