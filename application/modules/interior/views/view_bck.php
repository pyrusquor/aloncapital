<?php
    // print_r($interior); exit;
	
	$id =	isset($interior['id']) && $interior['id'] ? $interior['id'] : '';
    $house_model_id =	isset($interior['house_model_id']) && $interior['house_model_id'] ? $interior['house_model_id'] : '';
    $house_model_name =	isset($interior['house']['name']) && $interior['house']['name'] ? $interior['house']['name'] : '';
    $name =	isset($interior['name']) && $interior['name'] ? $interior['name'] : '';
    $price =	isset($interior['price']) && $interior['price'] ? $interior['price'] : '0';
    $is_multiply_floor_area =	isset($interior['is_multiply_floor_area']) && $interior['is_multiply_floor_area'] ? $interior['is_multiply_floor_area'] : '0';
    $is_multiply_lot_area =	isset($interior['is_multiply_lot_area']) && $interior['is_multiply_lot_area'] ? $interior['is_multiply_lot_area'] : '0';
    $active =	isset($interior['is_active']) && $interior['is_active'] ? $interior['is_active'] : '';
	
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">House Interior Details</h3>
		</div>
		<div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo base_url('interior/update/'.$id); ?>" class="btn btn-label-primary">Edit</a>
				<a href="<?php echo base_url('interior'); ?>" class="btn btn-label-primary">Back</a>&nbsp;
            </div>
        </div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-md-6 col-xl-6" >
							<div class="form-group row" style="margin-bottom: 1rem;">
								<label class="col-4 col-form-label kt-font-bolder">Property Model</label>
								<label class="col-8 col-form-label">
									<?php echo ucwords($house_model_name); ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 1rem;">
								<label class="col-4 col-form-label kt-font-bolder">Name</label>
								<label class="col-8 col-form-label">
									<?php echo ucwords($name); ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: 1rem;">
								<label class="col-4 col-form-label kt-font-bolder">Price</label>
								<label class="col-8 col-form-label">
									<?php echo $price; ?>
								</label>
							</div>
							<div class="form-group row" style="margin-bottom: -1rem;">
								<div class="col-1 col-form-label">
									<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
										<input type="checkbox" value="1" class="m-checkable" <?php echo ($is_multiply_floor_area > 0) ? 'checked="checked"' : '' ?> >
										<span></span>
									</label>
								</div>
								<label class="col-11 col-form-label kt-font-bolder">Multiply by Floor Area</label>
							</div>
							<div class="form-group row" style="margin-bottom: 1rem;">
								<div class="col-1 col-form-label">
									<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
										<input type="checkbox" value="" class="m-checkable" <?php echo ($is_multiply_lot_area > 0) ? 'checked="checked"' : '' ?>>
										<span></span>
									</label>
								</div>
								<label class="col-11 col-form-label kt-font-bolder">Multiply by Lot Area</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>