<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Create House Interior</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                 <button type="submit" form="interior_form" class="btn btn-label-success"><i class="la la-plus"></i>Submit</button>&nbsp;
                <a href="<?php echo base_url('interior'); ?>" class="btn btn-label-instagram"><i class="la la-times"></i> Cancel</a>&nbsp;
            </div>
        </div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__body">
					<form method="POST" class="kt-form" id="interior_form">
                        <?php $this->load->view('_form'); ?>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>