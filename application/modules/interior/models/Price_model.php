<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Price_model extends MY_Model {
	public $table = 'house_model_interior_price_logs'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
    // public $fillable = ['house_model_id', 'price', 'date_effectivity', 'status', 'deleted_by']; // If you want, you can set an array with the fields that can be filled by insert/update
    public $fillable = ['house_model_interior_id', 'price','fully_furnished_price','improved_price', 'date_effectivity', 'is_multiply_floor_area', 'is_multiply_lot_area', 'is_active',
													'created_by',
													'created_at',
													'updated_by',
													'updated_at',
													'deleted_by',
													'deleted_at']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
	
	// public $fields = [
	// 	'house_model_id' => array(
	// 		'field'=>'house_model_id',
	// 		'label'=>'Property Model ID',
	// 		'rules'=>'trim|required'
	// 	),
	// 	'price' => array(
	// 		'field'=>'price',
	// 		'label'=>':Price',
	// 		'rules'=>'trim|required'
	// 	),
	// 	'date_effectivity' => array(
	// 		'field'=>'date_effectivity',
	// 		'label'=>'Date of Effectivity',
	// 		'rules'=>'trim'
	// 	),
	// 	'status' => array(
	// 		'field'=>'status',
	// 		'label'=>'Status',
	// 		'rules'=>'trim'
	// 	)
	// ];
	
	
	// public $delete_fields = [
	// 	'deleted_by' => array(
	// 		'field'=>'deleted_by',
	// 		'label'=>'Deleted By',
	// 		'rules'=>'trim'
	// 	)
	// ];
	
	public $fields = [];
	
	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
	}

	function find_all ( $_where = FALSE, $_columns = FALSE, $_row = FALSE ) {

		$_return	=	FALSE;

		if ( $_where ) {

			if ( is_array($_where) ) {

				foreach ( $_where as $key => $_wang ) {

					$this->db->where( $key, $_wang );
				}
			} else {

				$this->db->where( 'id', $_where );
			}
		} else {

			$this->db->where('deleted_at IS NULL');
		}

		if ( $_columns ) {

			if ( is_array($_columns) ) {

				foreach ( $_columns as $key => $_col ) {

					$this->db->select($_col);
				}
			} else {

				$this->db->select($_columns);
			}
		} else {

			$this->db->select('*');
		}

		// if ( $_limit ) {

		// 	if ( is_numeric($_limit) ) {

		// 		$this->db->limit($_limit);
		// 	}
		// }

		$_query	=	$this->db->get($this->table);
		if ( $_row ) {

			$_return	=	$_query->num_rows() > 0 ? $_query->row() :  FALSE;
		} else {

			$_return	=	$_query->num_rows() > 0 ? $_query->result() :  FALSE;
		}

		return $_return;
	}	
}