<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Interior_model extends MY_Model {
	public $table = 'house_model_interiors'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = ['house_model_id', 'name', 'price', 'fully_furnished_price', 'improved_price', 'date_effectivity', 'is_multiply_floor_area', 'is_multiply_lot_area', 'is_active',
													'created_by',
													'created_at',
													'updated_by',
													'updated_at',
													'deleted_by',
													'deleted_at']; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];
	
	public $fields = [
		'house_model_id' => array(
			'field'=>'house_model_id',
			'label'=>'Property Model ID',
			'rules'=>'trim|required'
		),
		'name' => array(
			'field'=>'name',
			'label'=>'Name',
			'rules'=>'trim|required'
		),
		'price' => array(
			'field'=>'price',
			'label'=>':Price',
			'rules'=>'trim|required'
		),
		'no_bedroom' => array(
			'field'=>'no_bedroom',
			'label'=>'No of Bedroom',
			'rules'=>'trim'
		),
		'is_multiply_floor_area' => array(
			'field'=>'is_multiply_floor_area',
			'label'=>'Multiply by Floor Area',
			'rules'=>'trim'
		),
		'is_multiply_lot_area' => array(
			'field'=>'is_multiply_lot_area',
			'label'=>'Multiply by Lot Area',
			'rules'=>'trim'
		),
		'is_active' => array(
			'field'=>'is_active',
			'label'=>'Is Active',
			'rules'=>'trim'
        ),
	];
	
	public $delete_fields = [
		'deleted_by' => array(
			'field'=>'deleted_by',
			'label'=>'Deleted By',
			'rules'=>'trim'
		)
	];
	
	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;
		
		$this->has_one['house'] = array('foreign_model'=>'House_model','foreign_table'=>'house_models','foreign_key'=>'id','local_key'=>'house_model_id');
	}
    
}