<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_commission extends MY_Controller
{

	private $fields = [
		array(
			'field' => 'property[property_id]',
			'label' => 'Property Name',
			'rules' => 'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();

		$transaction_models = array(
			'transaction/Transaction_model' => 'M_transaction',
			'Transaction_commission_model' => 'M_Transaction_commission',
			'Transaction/transaction_seller_model' => 'M_Transaction_seller'
		);

		// Load models
		$this->load->model('auth/Ion_auth_model', 'M_auth');
		$this->load->model('user/User_model', 'M_user');
		$this->load->model('seller/Seller_model', 'M_seller');
		$this->load->model($transaction_models);


		// Load pagination library
		$this->load->library('ajax_pagination');

		// Format Helper
		$this->load->helper(['format', 'images']); // Load Helper

		$this->load->library('mortgage_computation');

		// Per page limit
		$this->perPage = 12;

		$this->_table_fillables		=	$this->M_Transaction_commission->fillable;
		$this->_table_columns		=	$this->M_Transaction_commission->__get_columns();
	}

	public function template($page = "")
	{

		$this->template->build($page);
	}

	public function index()
	{
		$_fills = $this->_table_fillables;
		$_colms = $this->_table_columns;

		$this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns'] = $this->__get_columns($_fills);

		// $db_columns = $this->M_Transaction_commission->get_columns();
		// if ($db_columns) {
		// 	$column = [];
		// 	foreach ($db_columns as $key => $dbclm) {
		// 		$name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

		// 		if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
		// 			if (strpos($name, '_id') !== false) {
		// 				$column = $name;
		// 				$column[$key]['value'] = $column;
		// 				$name = isset($name) && $name ? str_replace('_id', '', $name) : '';
		// 				$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
		// 				$column[$key]['label'] = ucwords(strtolower($_title));
		// 			} elseif (strpos($name, 'is_') !== false) {
		// 				$column = $name;
		// 				$column[$key]['value'] = $column;
		// 				$name = isset($name) && $name ? str_replace('is_', '', $name) : '';
		// 				$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
		// 				$column[$key]['label'] = ucwords(strtolower($_title));
		// 			} else {
		// 				$column[$key]['value'] = $name;
		// 				$_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
		// 				$column[$key]['label'] = ucwords(strtolower($_title));
		// 			}
		// 		} else {
		// 			continue;
		// 		}
		// 	}

		// 	$column_count = count($column);
		// 	$cceil = ceil(($column_count / 2));

		// 	$this->view_data['columns'] = array_chunk($column, $cceil);
		// 	$column_group = $this->M_Transaction_commission->count_rows();
		// 	if ($column_group) {
		// 		$this->view_data['total'] = $column_group;
		// 	}
		// }

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}


	public function index_()
	{
		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns']	=	$this->__get_columns($_fills);

		// Get record count
		// $conditions['returnType'] = 'count';
		$this->view_data['totalRec'] = $totalRec = $this->M_Transaction_seller->count_rows();

		// Pagination configuration
		$config['target']      = '#transactionContent';
		$config['base_url']    = base_url('transaction_commission/paginationData');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'TransactionPagination';

		// Initialize pagination library
		$this->ajax_pagination->initialize($config);

		// Get records
		$this->view_data['records'] = $this->M_Transaction_seller->order_by('id', 'DESC')
			->with_transaction()->with_seller()
			->limit($this->perPage, 0)
			->get_all();


		foreach ($this->view_data['records'] as $key => $row) {
			$data = array(
				'waiting_for_approval' => 0,
				'approved_commission' => 0,
				'paid_commission' => 0
			);
			$this->view_data['records'][$key]['commission_status'] = $data;
			// 1 - Pending
			// 2 - For RFP
			// 3 - Processing
			// 4 - Released
			if (isset($row['commissions'])) {
				foreach ($row['commissions'] as $value) {
					if ($value['status'] == 2) {
						$this->view_data['records'][$key]['commission_status']['waiting_for_approval'] += $value['amount_rate_to_collect'];
					} elseif ($value['status'] == 3) {
						$this->view_data['records'][$key]['commission_status']['approved_commission'] += $value['amount_rate_to_collect'];
					} elseif ($value['status'] == 4) {
						$this->view_data['records'][$key]['commission_status']['paid_commission'] += $value['amount_rate_to_collect'];
					}
				}
			}
		}

		// vdebug($this->view_data['records']);
		// $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		// $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}
	public function sellers()
	{
		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills);
		$this->view_data['_columns']	=	$this->__get_columns($_fills);

		// vdebug($this->view_data['records']);
		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('sellers', $this->view_data);
	}

	public function showItemssellers()
	{
		$columnsDefault = [
			'seller_id' => true,
			'seller_name' => true,
			'commission_count' => true,
			'amount_sum' => true,
		];

		$draw = $_REQUEST['draw'];
		$start = $_REQUEST['start'];
		$rowperpage = $_REQUEST['length'];
		$columnIndex = $_REQUEST['order'][0]['column'];
		$columnName = $_REQUEST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_REQUEST['order'][0]['dir'];
		$searchValue = $_REQUEST['search']['value'] ?? null;

		$filters = [];
		parse_str($_POST['filter'], $filters);

		$items = [];
		$totalRecordwithFilter = 0;
		$filteredItems = [];

		for ($query_loop = 0; $query_loop < 2; $query_loop++) {
			$this->db->select('seller_id,count(transaction_commissions.id) as commission_count, concat(sellers.first_name, " ", sellers.last_name) as seller_name');
			$this->db->join('sellers', 'transaction_commissions.seller_id = sellers.id');
			$this->db->where('status = 2');
			$query = $this->M_Transaction_commission
				->fields('seller_id')
				->order_by($columnName, $columnSortOrder)
				->group_by('seller_id')
				->as_array();
			// General Search
			if ($searchValue) {

				$query->like("concat_ws(' ',first_name,last_name)", $searchValue, 'both');
			}

			$advanceSearchValues = [
				// 'transaction_id' => [
				// 	'data' => $filters['transaction_id'] ?? null,
				// 	'operator' => '=',
				// ],
				// 'seller_id' => [
				// 	'data' => $filters['seller_id'] ?? null,
				// 	'operator' => '=',
				// ],
				// 'sellers_rate' => [
				// 	'data' => $filters['sellers_rate'] ?? null,
				// 	'operator' => 'like',
				// ],
				// 'sellers_rate_amount' => [
				// 	'data' => $filters['sellers_rate_amount'] ?? null,
				// 	'operator' => 'like',
				// ],
			];

			// Advance Search
			foreach ($advanceSearchValues as $key => $value) {

				if ($value['data']) {

					if ($key == 'date_range_start' || $key == 'date_range_end') {

						$query->where($value['column'], $value['operator'], $value['data']);
					} else {

						$query->where($key, $value['operator'], $value['data']);
					}
				}
			}

			if ($query_loop) {

				$totalRecordwithFilter = $query->count_rows();
			} else {
				$query->limit($rowperpage, $start);
				$items = $query->get_all();
				if (!!$items) {
					// Transform data
					foreach ($items as $key => $value) {
						$commission_sum = 0;
						$items[$key]['seller_id'] = $value['seller_id'];
						$items[$key]['seller_name'] = "<a target='_BLANK' href='" . base_url() . "seller/view/" . $value['seller_id'] . "'>" . $value['seller_name'] . "</a>";

						$items[$key]['commission_count'] = $value['commission_count'];
						$commission_data = $this->M_Transaction_commission->show_rfp($value['seller_id']);

						if ($commission_data) {
							$first_release_amount = @$commission_data[0]['commission_amount_rate'];
							$transaction_id = $commission_data[0]['transaction_id'];
							$key_w = 0;
							foreach ($commission_data as $key_cd => $data) {
								if ($data['transaction_id'] != $transaction_id) {
									$key_w = 0;
									$first_release_amount = $data['commission_amount_rate'];
									$transaction_id = $data['transaction_id'];
								}
								$commission_sum += get_net_amount($data['commission_amount_rate'], $data['wht_rate'], $first_release_amount, $data['period_type_id'], $key_w);
								$key_w++;
							}
						}
						$items[$key]['amount_sum'] = money_php($commission_sum);
					}

					foreach ($items as $item) {
						$filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
					}
				}
			}
		}
		// vdebug($filteredItems);
		$totalRecords = $this->M_Transaction_seller->count_rows();

		$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"data" => $filteredItems,
		);

		echo json_encode($response);
		exit();
	}

	public function showItems()
	{
		$columnsDefault = [
			'id' => true,
			'transaction_id' => true,
			'property' => true,
			'project' => true,
			'seller_id' => true,
			'total_count' => true,
			'sellers_rate' => true,
			'sellers_rate_amount' => true,
			'approved_commission' => true,
			'waiting_for_approval' => true,
			'paid_commission' => true,
			'transaction_id' => true,
			'seller_view_id' => true,
			'transaction_view_id' => true,
			'pending' => true,
			'created_by' => true,
			'updated_by' => true
		];

		$draw = $_REQUEST['draw'];
		$start = $_REQUEST['start'];
		$rowperpage = $_REQUEST['length'];
		$columnIndex = $_REQUEST['order'][0]['column'];
		$columnName = $_REQUEST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_REQUEST['order'][0]['dir'];
		$searchValue = $_REQUEST['search']['value'] ?? null;

		$filters = [];
		parse_str($_POST['filter'], $filters);

		$items = [];
		$totalRecordwithFilter = 0;
		$filteredItems = [];

		for ($query_loop = 0; $query_loop < 2; $query_loop++) {

			$query = $this->M_Transaction_seller
				->with_transaction([
					'fields' => 'reference',
					'with' => [
						[
							'relation' => 'project',
							'fields' => 'name'
						],
						[
							'relation' => 'property',
							'fields' => 'name'
						],
					],
				])
				->with_seller('fields:first_name,middle_name,last_name')
				->with_commissions('fields:*count*')
				->order_by($columnName, $columnSortOrder)
				->as_array();

			// General Search
			if ($searchValue) {

				$query->or_where("(id like '%$searchValue%'");
				$query->or_where("sellers_rate like '%$searchValue%'");
				$query->or_where("sellers_rate_amount like '%$searchValue%')");
			}

			$advanceSearchValues = [
				'transaction_id' => [
					'data' => $filters['transaction_id'] ?? null,
					'operator' => '=',
				],
				'seller_id' => [
					'data' => $filters['seller_id'] ?? null,
					'operator' => '=',
				],
				'sellers_rate' => [
					'data' => $filters['sellers_rate'] ?? null,
					'operator' => 'like',
				],
				'sellers_rate_amount' => [
					'data' => $filters['sellers_rate_amount'] ?? null,
					'operator' => 'like',
				],
			];

			// Advance Search
			foreach ($advanceSearchValues as $key => $value) {

				if ($value['data']) {

					if ($key == 'date_range_start' || $key == 'date_range_end') {

						$query->where($value['column'], $value['operator'], $value['data']);
					} else {

						$query->where($key, $value['operator'], $value['data']);
					}
				}
			}

			if ($query_loop) {

				$totalRecordwithFilter = $query->count_rows();
			} else {

				$query->limit($rowperpage, $start);
				$items = $query->get_all();

				if (!!$items) {

					// Transform data
					foreach ($items as $key => $value) {
						$waiting_for_approval = 0;
						$approved_commission = 0;
						$paid_commission = 0;

						$transaction_id = $value['transaction']['id'] ?? null;
						$seller_id = $value['seller']['id'] ?? null;

						$pending = $this->get_commission_summary_table($transaction_id, $seller_id, 1);
						$waiting_for_approval = $this->get_commission_summary_table($transaction_id, $seller_id, 2);
						$approved_commission = $this->get_commission_summary_table($transaction_id, $seller_id, 3);
						$paid_commission = $this->get_commission_summary_table($transaction_id, $seller_id, 4);

						$total_commission  = $value['sellers_rate_amount'];
						$percentage  = $value['sellers_rate'];

						$total_count = $value['commissions'][0]['counted_rows'] ?? 0;

						$items[$key]['transaction_id'] = isset($value['transaction']) ? "<a target='_BLANK' href='" . base_url() . "transaction/view/" . $value['transaction']['id'] . "'>" . $value['transaction']['reference'] . "</a>" : '';
						$items[$key]['property'] = isset($value['transaction']['property']) ? "<a target='_BLANK' href='" . base_url() . "property/view/" . $value['transaction']['property']['id'] . "'>" . $value['transaction']['property']['name'] . "</a>" : '';
						$items[$key]['project'] = isset($value['transaction']['project']) ? "<a target='_BLANK' href='" . base_url() . "project/view/" . $value['transaction']['project']['id'] . "'>" . $value['transaction']['project']['name'] . "</a>" : '';
						$items[$key]['seller_id'] = isset($value['seller']) ? "<a target='_BLANK' href='" . base_url() . "seller/view/" . $value['seller']['id'] . "'>" . get_fname($value['seller']) . "</a>" : '';

						$items[$key]['total_count'] = $this->get_commission_summary_table($transaction_id, $seller_id, 0, 1);
						$items[$key]['sellers_rate'] = $percentage;
						$items[$key]['sellers_rate_amount'] = money_php($total_commission);
						$items[$key]['approved_commission'] = money_php($approved_commission);
						$items[$key]['waiting_for_approval'] = money_php($waiting_for_approval);
						$items[$key]['paid_commission'] = money_php($paid_commission);
						$items[$key]['pending'] = money_php($pending);

						$items[$key]['seller_view_id'] = $value['seller']['id'];
						$items[$key]['transaction_view_id'] = $value['transaction']['id'] ?? '';

						$items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

						$items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
					}

					foreach ($items as $item) {
						$filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
					}
				}
			}
		}

		$totalRecords = $this->M_Transaction_seller->count_rows();

		$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"data" => $filteredItems,
		);

		echo json_encode($response);
		exit();
	}

	public function view($transaction_id = FALSE, $seller_id = FALSE, $type = '')
	{

		if ($transaction_id) {

			$this->view_data['info'] =  $this->M_transaction
				->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
				->with_t_property()->with_billings()->with_buyers()->with_commission_setup()
				->with_sellers()->with_fees()->with_promos()->with_payments()->with_payments()
				->get($transaction_id);

			$this->view_data['seller'] = $this->M_Transaction_seller->order_by('id', 'DESC')->with_seller()
				->get(array('transaction_id' => $transaction_id, 'seller_id' => $seller_id));

			$this->view_data['commissions'] = $_commissions = $this->M_Transaction_commission->where(array('transaction_id' => $transaction_id, 'seller_id' => $seller_id))->get_all();

			$total_amt_to_release = 0;
			$this->view_data['transaction_id'] = $transaction_id;

			if ($_commissions) {
				foreach ($_commissions as $key => $value) {
					$total_amt_to_release += $value['commission_amount_rate'];
				}
			}

			$this->view_data['total_amt_to_release'] = $total_amt_to_release;

			if ($this->view_data['info']) {

				if ($type == "debug") {

					vdebug($this->view_data);
				}

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}
	public function view_seller($seller_id = FALSE, $type = '')
	{

		if ($seller_id) {

			$this->view_data['gen_info'] = $this->M_seller->fields('id,first_name, last_name, middle_name, sales_group_id, seller_position_id, seller_team_id')->get(array('id' => $seller_id));

			$this->view_data['commissions'] = $_commissions = $this->M_Transaction_commission
				->with_transaction(['fields' => 'project_id,property_id', 'with' => ['relation' => 'project', 'fields' => 'company_id']])
				->where(array('seller_id' => $seller_id, 'status' => '2'))
				->get_all();
				
			$total_amt_to_release = 0;
			if ($_commissions) {
				$key_w = 0;
				$transaction_id = $_commissions[0]['transaction_id'];
				$first_release_amount = @$_commissions[0]['commission_amount_rate'];
				foreach ($_commissions as $key => $value) {
					if ($value['transaction_id'] != $transaction_id) {
						$key_w = 0;
						$first_release_amount = $value['commission_amount_rate'];
						$transaction_id = $value['transaction_id'];
					}
					$total_amt_to_release += get_net_amount($value['commission_amount_rate'], $value['wht_rate'], $first_release_amount, $value['period_type_id'], $key_w);
					$key_w++;
				}
			}

			$this->view_data['total_amt_to_release'] = money_php($total_amt_to_release);

			$this->template->build('view_seller', $this->view_data);
		} else {

			show_error('No Seller ID was provided', '403', 'Incomplete Data');
		}
	}

	public function form($id = FALSE)
	{
		$method = "Create";
		if ($id) {
			$method = "Update";
		}

		if ($this->input->post()) {

			$response['status']	= 0;
			$response['msg'] = 'Oops! Please refresh the page and try again.';

			$this->form_validation->set_rules($this->fields);

			if ($this->form_validation->run() === TRUE) {

				$post = $this->input->post();

				$info 	= $post['info'];
				$billings 	= $post['billing'];
				$property 	= $post['property'];
				$sellers 	= $post['seller'];
				$clients 	= $post['client'];
				$fees 		= $post['fees'];
				$promos 	= $post['promos'];
				$terms 		= $post['period_term'];
				$interest_rate 		= $post['interest_rate'];
				$percentage_rate	= $post['percentage_rate'];
				$period_amount 		= $post['period_amount'];
				$effectivity_date 	= $post['effectivity_date'];
				$period_id 			= $post['period_id'];

				if ($id) {
					$additional = [
						'updated_by' => $this->user->id,
						'updated_at' => NOW
					];
				} else {
					$additional = [
						'is_active' => 1,
						'created_by' => $this->user->id,
						'created_at' => NOW
					];
				}

				$this->db->trans_start(); # Starting Transaction
				$this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well

				$transaction['reference'] = uniqidReal();
				$transaction['period_id'] = 1;
				$transaction['buyer_id'] = $clients[0]['client_id'];
				$transaction['seller_id'] = $sellers[0]['seller_id'];
				$transaction['project_id'] = $property['project_id'];
				$transaction['property_id'] = $property['property_id'];
				$transaction['financing_scheme_id'] = $billings['financing_scheme_id'];
				$transaction['expiration_date'] = $billings['expiration_date'];
				$transaction['reservation_date'] = $billings['reservation_date'];
				$transaction['remarks'] = $info['remarks'];
				$transaction_id = $this->M_Transaction_commission->insert($transaction + $additional);

				// M_transaction_property
				$property['total_miscellaneous_amount'] = 0;
				$property['total_discount_price'] = 0;
				$property['transaction_id'] = $transaction_id;
				$this->M_transaction_property->insert($property + $additional);

				if ($terms) {
					foreach ($terms as $key => $term) {
						// M_transaction_billing
						$billing['period_term'] = $term;
						$billing['period_amount'] = $period_amount[$key];
						$billing['interest_rate'] = $interest_rate[$key];
						$billing['effectivity_date'] = $effectivity_date[$key];
						$billing['period_id'] = $period_id[$key];

						$billing['transaction_id'] = $transaction_id;
						$this->M_transaction_billing->insert($billing + $additional);
					}
				}

				if ($fees) {
					foreach ($fees as $key => $fee) {
						$fee['transaction_id'] = $transaction_id;

						if ($fee['fees_id']) {
							// M_transaction_fee
							$this->M_transaction_fee->insert($fee + $additional);
						}
					}
				}

				if ($promos) {
					foreach ($promos as $key => $promo) {
						$promo['transaction_id'] = $transaction_id;

						if ($promo['discount_id']) {
							// M_transaction_discount
							$this->M_transaction_discount->insert($promo + $additional);
						}
					}
				}

				if ($sellers) {
					foreach ($sellers as $key => $seller) {
						// M_transaction_seller
						$seller['transaction_id'] = $transaction_id;
						$this->M_transaction_seller->insert($seller + $additional);
					}
				}

				if ($clients) {
					foreach ($clients as $key => $client) {
						// M_transaction_client
						$client['transaction_id'] = $transaction_id;
						$this->M_transaction_client->insert($client + $additional);
					}
				}

				// M_transaction_payment
				$this->process_mortgage($transaction_id, 'save');

				// M_transaction_commission
				$this->process_commission($transaction_id, 'save');

				$this->db->trans_complete(); # Completing transaction

				/*Optional*/
				if ($this->db->trans_status() === FALSE) {
					# Something went wrong.
					$this->db->trans_rollback();
					$response['status']	= 0;
					$response['message'] = 'Error!';
				} else {
					# Everything is Perfect.
					# Committing data to the database.
					$this->db->trans_commit();
					$response['status']	= 1;
					$response['message'] = 'Transaction Successfully ' . $method . 'd!';
				}
			} else {
				$response['status']	 =	0;
				$response['message'] = validation_errors();
			}

			echo json_encode($response);
			exit();
		}

		$this->view_data['projects'] = $this->M_project->as_array()->get_all();
		$this->view_data['fees'] = $this->M_fees->as_array()->get_all();
		$this->view_data['promos'] = $this->M_promo->as_array()->get_all();
		$this->view_data['schemes'] = $this->M_scheme->as_array()->get_all();
		$this->view_data['method'] = $method;

		if ($this->view_data['projects']) {
			if ($id) {
				$this->view_data['info'] =  $this->M_transaction
					->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
					->with_t_property()->with_billings()->with_buyers()
					->with_sellers()->with_fees()->with_promos()
					->get($id);
			}

			$this->template->build('form', $this->view_data);
		} else {
			show_404();
		}
	}

	public function delete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_Transaction_commission->get($id);
			if ($list) {

				$deleted = $this->M_Transaction_commission->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Transaction successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function bulkDelete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_Transaction_commission->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Transaction successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}


	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];
		$_extra_datas		=	[];
		$_adatas			=	[];

		$_titles[]	=	'#';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		// $transactions	=	$this->M_Transaction_commission->as_array()->get_all();
		$transactions = $this->M_transaction
			->with_source('fields:recruiter, supervisor, net_worth, to_report, to_attend, commission_based, is_member, group')
			->with_reference('fields: ref_name, ref_address, ref_contact_no')
			->with_work_experience('fields: employer, designation, emp_address, salary, emp_contact_no, supervisor, start_date, end_date, reason, to_contact')
			->with_academic('fields: level, course, school, year, rating')
			->with_exam()
			->with_training()
			->with_position('fields:name')
			->get_all();

		if ($transactions) {

			foreach ($transactions as $skey => $transaction) {

				$_datas[$transaction['id']]['#']	=	$_no;

				$_no++;
			}


			$_filename	=	'list_of_transactions' . date('m_d_y_h-i-s', time()) . '.xls';

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				$_additional_column	=	$this->input->post('_additional_column');

				if ($_export_column) {
					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'commission');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

						if ((strpos($_name, 'source') !== FALSE) or (strpos($_name, 'reference') !== FALSE) or (strpos($_name, 'academic') !== FALSE)) {

							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($transactions as $skey => $transaction) {

								$_extra_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
							}
						} elseif ((strpos($_name, 'work_experience') !== FALSE)) {
							$_extra_titles[]	=	$_title =	isset($_name) && $_name ? $_name : '';

							foreach ($transactions as $skey => $transaction) {

								$_extra_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
							}
						} else {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($transactions as $skey => $transaction) {

								if ($_column === 'sales_group_id') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_static('group_type', $transaction[$_column], 'view') : '';
								} elseif ($_column === 'transaction_position_id') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_dynamic('transaction_positions', $transaction[$_column], 'name', 'id', 'view') : '';
								} elseif ($_column === 'birth_date') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] && (strtotime($transaction[$_column]) > 0) ? date_format(date_create($transaction[$_column]), 'm/d/Y') : '';
								} elseif ($_column === 'gender') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_static('sex', $transaction[$_column], 'view') : '';
								} elseif ($_column === 'is_active') {

									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_column]) && $transaction[$_column] ? Dropdown::get_static('bool', $transaction[$_column], 'view') : '';
								} else {
									$_datas[$transaction['id']][$_title]	=	isset($transaction[$_name]) && $transaction[$_name] ? $transaction[$_name] : '';
								}
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_lastAlpha		=	end($_alphas);

				if (empty($_extra_datas)) {
					foreach ($_xls_columns as $_xkey => $_column) {

						$_title	=	ucwords(strtolower($_column));

						$_objSheet->setCellValue($_xkey . $_row, $_title);
						$_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$_objSheet->setTitle('List of Transactions');
				$_objSheet->setCellValue('A1', 'LIST OF SELLERS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				$col = 1;

				foreach ($transactions as $key => $transaction) {

					$transaction_id	=	isset($transaction['id']) && $transaction['id'] ? $transaction['id'] : '';

					if (!empty($_extra_datas)) {

						foreach ($_xls_columns as $_xkey => $_column) {

							$_title	=	ucwords(strtolower($_column));

							$_objSheet->setCellValue($_xkey . $_start, $_title);

							$_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						$_start++;
					}
					// PRIMARY INFORMATION COLUMN
					foreach ($_alphas as $_akey => $_alpha) {
						$_value	=	isset($_datas[$transaction_id][$_xls_columns[$_alpha]]) && $_datas[$transaction_id][$_xls_columns[$_alpha]] ? $_datas[$transaction_id][$_xls_columns[$_alpha]] : '';
						$_objSheet->setCellValue($_alpha . $_start, $_value);
					}

					// ADDITIONAL INFORMATION COLUMN
					if (!empty($_extra_datas)) {
						$_start += 2;

						$_addtional_columns	=	$_extra_titles;

						foreach ($_addtional_columns as $adkey => $_a_column) {

							// MAIN TITLE OF ADDITIONAL DATA

							if ($_a_column === 'contact') {

								$ad_title	=	'Contact Informations';
							} elseif ($_a_column === 'reference') {

								$ad_title	=	'References';
							} elseif ($_a_column === 'source') {

								$ad_title	=	'Source of Informations';
							} elseif ($_a_column === 'academic') {

								$ad_title	=	'Academic History';
							} else {

								$ad_title = $_a_column;
							}

							$a_title	=	ucwords(str_replace('_', ' ', strtolower($ad_title)));

							$_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

							// Style
							$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
							$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

							// LOOP DATAS
							if ((strpos($_a_column, 'contact') !== FALSE) or (strpos($_a_column, 'source') !== FALSE or (strpos($_a_column, 'work_experience') !== FALSE))) {

								$col = 1;
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $key => $value) {

										if ((strpos($key, '_id') === FALSE) && (strpos($key, 'id') === FALSE)) {

											if ($key === 'to_report' or $key === 'to_attend' or $key === 'commission_based' or $key === 'is_member') {
												$xa_value	=	isset($_extra_datas[$transaction_id][$_a_column][$key]) && ($_extra_datas[$transaction_id][$_a_column][$key] !== '') ? Dropdown::get_static('bool', $_extra_datas[$transaction_id][$_a_column][$key], 'view') : '';
											} else {
												$xa_value	=	$_extra_datas[$transaction_id][$_a_column][$key];
											}

											$xa_titles =	isset($key) && $key ? str_replace('_', ' ', $key) : '';


											$_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($xa_titles));
											$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, $xa_value);
										}

										$_start++;
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'reference') !== FALSE) {

								if (!empty($_extra_datas[$transaction_id][$_a_column])) {
									$refno = 1;

									foreach ($_extra_datas[$transaction_id][$_a_column] as $rkey => $ref) {

										$ref_col = array_flip($ref);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($ref_col as $key => $reftitles) {

											if ((strpos($reftitles, '_id')) === FALSE) {

												switch ($reftitles) {
													case 'ref_name':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Name');
														break;

													case 'ref_address':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Address');
														break;

													case 'ref_contact_no':
														$_objSheet->setCellValueByColumnAndRow($col, $_start, 'Contact No');
														break;

													default:
														$ref_title = str_replace('_', ' ', $reftitles);
														$_objSheet->setCellValueByColumnAndRow($col, $_start, $ref_title);
														break;
												}

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($ref[$reftitles]));
											}


											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							} elseif (strpos($_a_column, 'academic') !== FALSE) {
								if (!empty($_extra_datas[$transaction_id][$_a_column])) {

									foreach ($_extra_datas[$transaction_id][$_a_column] as $ackey => $acad) {

										$acad_col = array_flip($acad);

										$_start++;

										$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

										$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
										$_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

										foreach ($acad_col as $acolkey => $acadtitles) {

											if ((strpos($acadtitles, '_id')) === FALSE) {
												$acad_title = str_replace('_', ' ', $acadtitles);
												$_objSheet->setCellValueByColumnAndRow($col, $_start, $acad_title);

												$_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
											}

											$_start++;
										}
									}
								} else {
									$_start++;

									$_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

									// Style
									$_objSheet->mergeCells('B' . $_start . ':C' . $_start);
									$_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

									$_start += 1;
								}
							}

							$_start++;
						}
					}

					$_start += 1;
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}


				$_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}
		} else {

			$this->notify->error('No Record Found', 'transaction');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Transaction CSV Template.csv';

			// $_fillables	=	$this->M_commission->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_Transaction_commission->as_array()->get_all(); #up($_commissions);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Transactions');
			$_objSheet->setCellValue('A1', 'SELLERS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_failed_reasons = [];
				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'commission');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_commission->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'transaction');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {
							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							$_data['birth_date']	=	isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
							$transactionGroupID = ['3'];

							if ($_id) {
								$data	=	$this->M_Transaction_commission->get($_id);
								if ($data) {

									unset($_data['id']);

									$oldPwd = password_format($data['last_name'], $data['birth_date']);
									$newPwd = password_format($_data['last_name'], $_data['birth_date']);

									$oldEmail = $data['email'];
									$newEmail = $_data['email'];

									if ($this->M_auth->email_check($oldEmail)) {

										if ($oldPwd !== $newPwd) {
											// Update User Password
											$this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
										}

										if ($oldEmail !== $newEmail) {
											// Update User Email
											$_user_data = [
												'email' => $newEmail,
												'username' => $newEmail,
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											];
											$this->M_user->update($_user_data, array('id' => $data['user_id']));
										}

										// Update Transaction Info
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_update	=	$this->M_Transaction_commission->update($_data, $_id);
										if ($_update !== FALSE) {

											$user_data = array(
												'first_name' => $_data['first_name'],
												'last_name' => $_data['last_name'],
												'active' => $_data['is_active'],
												'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
												'updated_at' => NOW
											);

											$this->M_user->update($user_data, $data['user_id']);

											$_updated++;
										} else {

											$_failed_reasons[$_data['id']][] = 'update not working.';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email already used';
										$_failed++;

										break;
									}
								} else {

									// Generate user password
									$transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
									$transactionEmail = $_data['email'];

									if (!$this->M_auth->email_check($_data['email'])) {

										$user_data = array(
											'first_name' => $_data['first_name'],
											'last_name' => $_data['last_name'],
											'active' => $_data['is_active'],
											'created_by' => $this->user->id,
											'created_at' => NOW
										);

										$userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

										if ($userID !== FALSE) {

											$_data['user_id']	= $userID;
											$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['created_at']	=	NOW;
											$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
											$_data['updated_at']	=	NOW;

											$_insert	=	$this->M_Transaction_commission->insert($_data);
											if ($_insert !== FALSE) {

												$_inserted++;
											} else {

												$_failed_reasons[$_data['id']][] = 'insert transaction not working';
												$_failed++;

												break;
											}
										} else {

											$_failed_reasons[$_data['id']][] = 'insert ion auth not working';
											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_data['id']][] = 'email check already existing';
										$_failed++;

										break;
									}
								}
							} else {

								// Generate user password
								$transactionPwd = password_format($_data['last_name'], $_data['birth_date']);
								$transactionEmail = $_data['email'];

								if (!$this->M_auth->email_check($_data['email'])) {

									$user_data = array(
										'first_name' => $_data['first_name'],
										'last_name' => $_data['last_name'],
										'active' => $_data['is_active'],
										'created_by' => $this->user->id,
										'created_at' => NOW
									);

									$userID = $this->ion_auth->register($transactionEmail, $transactionPwd, $transactionEmail, $user_data, $transactionGroupID);

									if ($userID !== FALSE) {

										$_data['user_id']	= $userID;
										$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['created_at']	=	NOW;
										$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
										$_data['updated_at']	=	NOW;

										$_insert	=	$this->M_Transaction_commission->insert($_data);
										if ($_insert !== FALSE) {

											$_inserted++;
										} else {

											$_failed_reasons[$_dkey][] = 'insert transaction not working';

											$_failed++;

											break;
										}
									} else {

										$_failed_reasons[$_dkey][] = 'insert ion auth not working';

										$_failed++;

										break;
									}
								} else {

									$_failed_reasons[$_dkey][] = 'email check already existing';

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {
							$this->notify->error('Upload Failed! Please follow upload guide. ', 'transaction');
						} else {

							$this->notify->success($_msg . '.', 'transaction');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'transaction');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'transaction');
			}
		} else {

			$this->notify->error('Something went wrong!', 'transaction');
		}
	}

	public function update_status($t_commission_id = 0, $status = 0)
	{

		if ($t_commission_id) {
			$this->commission_library->update_status($t_commission_id, $status);
		}
	}

	public function get_commission_summary()
	{

		$for_approval_count = 0;
		$approved_commissions_count = 0;
		$paid_commissions_count = 0;
		$total_commission_count = 0;
		$for_approval_amount = 0;
		$approved_commissions_amount = 0;
		$paid_commissions_amount = 0;
		$total_commission_amount = 0;

		if ($this->input->post()) {
			$transaction_commission = $this->M_Transaction_commission->get_all();
			if ($transaction_commission) {

				foreach ($transaction_commission as $row) {

					switch ($row['status']) {
						case '1':
							$total_commission_count += 1;
							$total_commission_amount += $row['commission_amount_rate'];
							break;
						case '2':
							$for_approval_count += 1;
							$for_approval_amount += $row['commission_amount_rate'];
							break;
						case '3':
							$approved_commissions_count += 1;
							$approved_commissions_amount += $row['commission_amount_rate'];
							break;
						case '4':
							$paid_commissions_count += 1;
							$paid_commissions_amount += $row['commission_amount_rate'];
							break;
					}
				}
			}
		}
		$result['for_approval_count'] = $for_approval_count;
		$result['approved_commissions_count'] = $approved_commissions_count;
		$result['paid_commissions_count'] = $paid_commissions_count;
		$result['total_commission_count'] = $total_commission_count;
		$result['for_approval_amount'] = money_php($for_approval_amount);
		$result['approved_commissions_amount'] = money_php($approved_commissions_amount);
		$result['paid_commissions_amount'] = money_php($paid_commissions_amount);
		$result['total_commission_amount'] = money_php($total_commission_amount);
		echo json_encode($result);
		exit();
	}

	public function get_commission_summary_table($transaction_id = 0, $seller_id = 0, $status = 0, $type = 0)
	{

		$count = 0;
		$sum = 0;

		if ($transaction_id) {
			$this->db->where('transaction_id', $transaction_id);
		}

		if ($seller_id) {
			$this->db->where('seller_id', $seller_id);
		}

		if ($status) {
			$this->db->where('status', $status);
		}

		$w['transaction_id'] = $transaction_id;
		$w['seller_id'] = $seller_id;
		if ($status) {
			$w['status'] = $status;
		}
		$transaction_commission = $this->M_Transaction_commission->where($w)->get_all();


		if ($transaction_commission) {

			foreach ($transaction_commission as $row) {

				$count += 1;
				$sum += $row['commission_amount_rate'];
			}
		}

		if ($type) {
			return $count;
		}
		return $sum;
	}
}
