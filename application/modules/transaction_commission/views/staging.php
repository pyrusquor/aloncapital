    <!--begin:: Portlet-->
    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media kt-hidden">
                        <img src="./assets/media/project-logos/3.png" alt="image">
                    </div>
                    <div
                        class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                        JM
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <a href="#" class="kt-widget__username kt-hidden">
                                Jason Muller
                                <i class="flaticon2-correct"></i>
                            </a>
                            <a href="#" class="kt-widget__title">Nexa - Next generation SAAS</a>
                            <div class="kt-widget__action">
                                <button type="button" class="btn btn-sm btn-upper"
                                    style="background: #edeff6">edit</button>&nbsp;
                                <button type="button" class="btn btn-success btn-sm btn-upper">add user</button>&nbsp;
                                <button type="button" class="btn btn-brand btn-sm btn-upper">new task</button>
                            </div>
                        </div>
                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">
                                I distinguish three main text objektive.First, your could
                                <br> be merely to inform people a second could be persuade people.
                                <br> You want people to bay your products
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Required Copies</span>
                            <span class="kt-widget__value"><span>$</span>249,500</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Submitted Copies</span>
                            <span class="kt-widget__value"><span>$</span>164,700</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Original File</span>
                            <span class="kt-widget__value"><span>$</span>84,060</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Submitted File</span>
                            <span class="kt-widget__value"><span>$</span>84,060</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">File Status</span>
                            <span class="kt-widget__value">Pending</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Date Uploaded</span>
                            <span class="kt-widget__value">Upload file first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media kt-hidden">
                        <img src="./assets/media/project-logos/3.png" alt="image">
                    </div>
                    <div
                        class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                        JM
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <a href="#" class="kt-widget__username kt-hidden">
                                Jason Muller
                                <i class="flaticon2-correct"></i>
                            </a>
                            <a href="#" class="kt-widget__title">Nexa - Next generation SAAS</a>
                            <div class="kt-widget__action">
                                <button type="button" class="btn btn-sm btn-upper"
                                    style="background: #edeff6">edit</button>&nbsp;
                                <button type="button" class="btn btn-success btn-sm btn-upper">add user</button>&nbsp;
                                <button type="button" class="btn btn-brand btn-sm btn-upper">new task</button>
                            </div>
                        </div>
                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">
                                I distinguish three main text objektive.First, your could
                                <br> be merely to inform people a second could be persuade people.
                                <br> You want people to bay your products
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Required Copies</span>
                            <span class="kt-widget__value"><span>$</span>249,500</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Submitted Copies</span>
                            <span class="kt-widget__value"><span>$</span>164,700</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Original File</span>
                            <span class="kt-widget__value"><span>$</span>84,060</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Submitted File</span>
                            <span class="kt-widget__value"><span>$</span>84,060</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">File Status</span>
                            <span class="kt-widget__value">Pending</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Date Uploaded</span>
                            <span class="kt-widget__value">Upload file first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media kt-hidden">
                        <img src="./assets/media/project-logos/3.png" alt="image">
                    </div>
                    <div
                        class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                        JM
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <a href="#" class="kt-widget__username kt-hidden">
                                Jason Muller
                                <i class="flaticon2-correct"></i>
                            </a>
                            <a href="#" class="kt-widget__title">Nexa - Next generation SAAS</a>
                            <div class="kt-widget__action">
                                <button type="button" class="btn btn-sm btn-upper"
                                    style="background: #edeff6">edit</button>&nbsp;
                                <button type="button" class="btn btn-success btn-sm btn-upper">add user</button>&nbsp;
                                <button type="button" class="btn btn-brand btn-sm btn-upper">new task</button>
                            </div>
                        </div>
                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">
                                I distinguish three main text objektive.First, your could
                                <br> be merely to inform people a second could be persuade people.
                                <br> You want people to bay your products
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Required Copies</span>
                            <span class="kt-widget__value"><span>$</span>249,500</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Submitted Copies</span>
                            <span class="kt-widget__value"><span>$</span>164,700</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Original File</span>
                            <span class="kt-widget__value"><span>$</span>84,060</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Submitted File</span>
                            <span class="kt-widget__value"><span>$</span>84,060</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">File Status</span>
                            <span class="kt-widget__value">Pending</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Date Uploaded</span>
                            <span class="kt-widget__value">Upload file first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media kt-hidden">
                        <img src="./assets/media/project-logos/3.png" alt="image">
                    </div>
                    <div
                        class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                        JM
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <a href="#" class="kt-widget__username kt-hidden">
                                Jason Muller
                                <i class="flaticon2-correct"></i>
                            </a>
                            <a href="#" class="kt-widget__title">Nexa - Next generation SAAS</a>
                            <div class="kt-widget__action">
                                <button type="button" class="btn btn-sm btn-upper"
                                    style="background: #edeff6">edit</button>&nbsp;
                                <button type="button" class="btn btn-success btn-sm btn-upper">add user</button>&nbsp;
                                <button type="button" class="btn btn-brand btn-sm btn-upper">new task</button>
                            </div>
                        </div>
                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">
                                I distinguish three main text objektive.First, your could
                                <br> be merely to inform people a second could be persuade people.
                                <br> You want people to bay your products
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Required Copies</span>
                            <span class="kt-widget__value"><span>$</span>249,500</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Submitted Copies</span>
                            <span class="kt-widget__value"><span>$</span>164,700</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Original File</span>
                            <span class="kt-widget__value"><span>$</span>84,060</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Submitted File</span>
                            <span class="kt-widget__value"><span>$</span>84,060</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">File Status</span>
                            <span class="kt-widget__value">Pending</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Date Uploaded</span>
                            <span class="kt-widget__value">Upload file first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>