<?php
    $buyers = isset($info['buyers']) && $info['buyers'] ? $info['buyers'] : '';
?>

<div class="row">
    <div class="col-md-12">

        <a href="<?php echo base_url(); ?>buyer/create" target="_blank" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Buyer Record Here</a>

    </div>
</div>
<br>    
<br>    
<div class="row">

    <div class="col-md-12">

         <div id="buyer_form_repeater">
            <div class="form-grouprow">
                <label>Buyer Information : 
                    <div data-repeater-create="" class="btn btn-sm btn-primary">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Add</span>
                        </span>
                    </div>
                </label>

                <div data-repeater-list="client" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10">
                        
                        <div class="col-sm-2">
                            <div class="input-group">
                                <select class="form-control suggests" data-module="buyers" data-type="person" id="client_id" name="client_id">
                                    <option value="0">Select Buyer Name</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <?php echo form_dropdown('client_type', Dropdown::get_static('client_transaction_type'), set_value('client_type', @$client_type), 'class="form-control" id="client_type"'); ?>
                        </div>

                        <div class="col-sm-2">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                <i class="la la-remove"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>