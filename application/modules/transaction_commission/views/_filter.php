<?php if (!empty($records)) : ?>
    <div class="row" id="transactionList">
        <?php foreach ($records as $r) : if(!isset($r['seller'])){ continue ;} ?>
            <?php
                $id = $r['id'];
                $seller = @$r['seller'];
                $transaction = $r['transaction'];
                $transaction_id = $transaction['id'];
                $reference = $transaction['reference'];

                $project_name = get_value_field($transaction['project_id'],'projects','name');
                $property_name = get_value_field($transaction['property_id'],'properties','name');

                $seller_id = $seller['id'];
                $seller_name = get_fname($seller);

                $project_id = $transaction['project_id'];
                $property_id = $transaction['property_id'];


                $total_commission  = $r['sellers_rate_amount'];
                $percentage  = $r['sellers_rate'];

                $this->load->model('Transaction_commission/transaction_commission_model', 'M_commission');

                $commissions = $this->M_commission->where( array('transaction_id' => $transaction_id, 'seller_id' => $seller_id) )->get_all();

                $r['commissions'] = $commissions;

                $waiting_for_approval = isset($r['commission_status']['waiting_for_approval']) && $r['commission_status']['waiting_for_approval'] ? $r['commission_status']['waiting_for_approval'] : '0';;
                $approved_commission = isset($r['commission_status']['approved_commission']) && $r['commission_status']['approved_commission'] ? $r['commission_status']['approved_commission'] : '0';
                $paid_commission = isset($r['commission_status']['paid_commission']) && $r['commission_status']['paid_commission'] ? $r['commission_status']['paid_commission'] : '0';;
                $total_count = isset($r['commissions']) && $r['commissions'] ? count($r['commissions']) : '0';
            ?>
        <!--begin:: Portlet-->
        <div class="kt-portlet col-sm-12">
            <div class="kt-portlet__body custom-transaction_payments">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__media kt-hidden">
                            <img src="./assets/media/project-logos/3.png" alt="image">
                        </div>
                        <div
                            class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                            <?php echo get_initials($r['seller']['first_name'] . ' ' . $r['seller']['last_name']); ?>
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="<?php echo base_url(); ?>transaction/view/<?php echo $transaction_id; ?>" class="kt-widget__username">
                                    Reference : <?php echo $reference; ?>
                                    <i class="flaticon2-correct"></i>
                                </a>


                                <div class="kt-widget__action custom_portlet_header">

                                    <div class="kt-portlet__head kt-portlet__head--noborder" style="min-height: 0px;">
                                        <div class="kt-portlet__head-label">
                                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>" class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="kt-portlet__head-toolbar">
                                            <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                                <i class="flaticon-more-1 kt-font-brand"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <a href="<?php echo base_url('transaction_commission/view/' . $transaction_id. '/'. $seller_id); ?>" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-checking"></i>
                                                            <span class="kt-nav__link-text">View</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    <a href="<?php echo base_url(); ?>project/view/<?php echo $project_id; ?>" target="_BLANK" class="kt-widget__username">
                                        Project : <?php echo $project_name; ?>
                                    </a>
                                    <br>
                                    <a href="<?php echo base_url(); ?>property/view/<?php echo $property_id; ?>" target="_BLANK" class="kt-widget__username">
                                        Property : <?php echo $property_name; ?>
                                    </a>
                                    <br>
                                    <a href="<?php echo base_url(); ?>seller/view/<?php echo $seller_id; ?>" target="_BLANK" class="kt-widget__username">
                                        Seller Name : <?php echo $seller_name; ?>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__bottom">
                       
                        
                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Total Count</span>
                                <span class="kt-widget__value"><?php echo $total_count; ?></span>
                            </div>
                        </div>


                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Total Commission</span>
                                <span class="kt-widget__value">(<?php echo $percentage.'%'; ?>) : <?php echo money_php($total_commission); ?> </span>
                            </div>
                        </div>
                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Approved Commission</span>
                                <span class="kt-widget__value"><?php echo money_php($approved_commission); ?></span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Waiting for Approval</span>
                                <span class="kt-widget__value"><?php echo money_php($waiting_for_approval); ?></span>
                            </div>
                        </div>

                        <div class="kt-widget__item">
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Paid Commission</span>
                                <span class="kt-widget__value"><?php echo money_php($paid_commission); ?></span>
                            </div>
                        </div>

                        

                    </div>
                </div>
            </div>
        </div>

        <?php endforeach; ?>
    </div>

    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="<?php echo base_url('transaction/form'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Record Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>