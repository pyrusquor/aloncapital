<?php
    $financing_scheme = isset($_financing_scheme) && $_financing_scheme ? $_financing_scheme : '';
    $financing_scheme_values = isset($_financing_scheme['values']) && $_financing_scheme['values'] ? $_financing_scheme['values'] : '';

    // vdebug($financing_scheme_values);
?>

<div class="row">
    
    <div class="col-sm-12">
        
        <div data-repeater-list="" class="col-lg-12">
            <div data-repeater-item="" class="form-group row align-items-center">

                <div class="col-md-1">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label>&nbsp;</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Effectivity Date</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Terms</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Interest</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Percentage</label>
                        </div>
                       
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="kt-form__group--inline">
                        <div class="kt-form__label">
                            <label class="kt-label m-label--single">Amount</label>
                        </div>
                       
                    </div>
                </div>

            </div>
        </div>

        <?php if ($financing_scheme_values): $reservation_rate = 0; ?>
            <?php foreach ($financing_scheme_values as $key => $value) {

                $period_id = $value['period_id'];

                if ($period_id == 1) {
                    $reservation_rate = $value['period_rate_amount'];
                    $effectivity_date = $reservation_date;
                    $period_term = $value['period_term'];
                } else {
                    $effectivity_date = add_months_to_date($effectivity_date,$period_term);
                    $period_term = $value['period_term'];
                }

                if ($value['period_rate_amount'] != "0.00") {
                    $period_amount = $value['period_rate_amount'];

                } else {
                    $period_amount = get_percentage_amount($collectible_price, $value['period_rate_percentage']);

                    if ($period_id == 2) { // deduct the reservation rate to the period amount of dp

                        $period_amount = $period_amount - $reservation_rate;

                    }

                }

            ?>

                <div data-repeater-list="" class="col-lg-12">
                    <div data-repeater-item="" class="form-group row align-items-center">

                        <div class="col-md-1">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">
                                    <?php echo Dropdown::get_static('periods', $period_id, 'view'); ?>
                                    <input type="hidden" id="period_id[]"  name="period_id[]" value="<?php echo $period_id; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                     <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control datePicker" id="effectivity_date[]" placeholder="" name="effectivity_date[]" value="<?php echo $effectivity_date; ?>" readonly>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Months</span></div>
                                            <input type="text" class="form-control" id="period_term" placeholder="" name="period_term[]" value="<?php echo $value['period_term']; ?>" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="interest_rate" placeholder="Interest Rate" name="interest_rate[]" value="<?php echo $value['period_interest_percentage']; ?>" readonly>
                                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                                        </div>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="kt-form__group--inline">
                                <div class="kt-form__control">

                                    <div class="kt-input-icon kt-input-icon--left">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="percentage_rate" placeholder="Percentage Rate" name="percentage_rate[]" value="<?php echo $value['period_rate_percentage']; ?>" readonly>
                                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                                        </div>
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="kt-form__group--inline">
                               
                                <div class="kt-input-icon kt-input-icon--left">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                                        <input type="text" class="form-control" id="period_amount" placeholder="Period Amount" name="period_amount[]" value="<?php echo $period_amount; ?>" readonly>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>

            <?php } ?>
        <?php endif ?>
       
     
    </div>

</div>