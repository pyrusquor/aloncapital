<?php
extract($gen_info);
$full_name = ucwords(strtolower("$first_name $middle_name $last_name"));
$name = ucwords(strtolower("$last_name $first_name"));
$sales_team = get_person($seller_team_id, 'seller_teams');
$seller_group = get_person($sales_group_id, 'seller_group');
$seller_position = get_person($seller_position_id, 'seller_positions');
?>
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Transaction Commission Details</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?= base_url('transaction_commission/sellers'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_seller_information">
                <div class="card">
                    <div class="card-header" id="head_seller_information">
                        <div class="card-title" data-toggle="collapse" data-target="#seller_information" aria-expanded="true" aria-controls="seller_information">
                            Seller Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                    <div id="seller_information" class="collapse show" aria-labelledby="head_seller_information" data-parent="#accord_seller_information">
                        <div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Full Name</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?= $full_name; ?>
                                                </h6>
                                            </div>
                                            <input type='hidden' id='seller_info' data-id='<?= $id ?>' data-name='<?= $name ?>'>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Seller Position</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?= $seller_position['name'] ?: 'None'; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Seller Team</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?= $sales_team['name'] ?: 'None'; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Sales Group</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?= $seller_group['name'] ?: 'None'; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="kt-portlet">
            <div class="accordion accordion-solid accordion-toggle-svg" id="accord_general_information">
                <div class="card">
                    <div class="card-header" id="head_general_information">
                        <div class="card-title" data-toggle="collapse" data-target="#general_information" aria-expanded="true" aria-controls="general_information">
                            Commission Table <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
                    <div id="general_information" class="collapse show" aria-labelledby="head_general_information" data-parent="#accord_general_information">
                        <div class="card-body">
                            <div class="form-group form-group-xs row">
                                <div class="col-sm-10">
                                    <span href="#" class="kt-notification-v2__item">
                                        <div class="kt-notification-v2__itek-wrapper">
                                            <div class="kt-notification-v2__item-title">
                                                <h7 class="kt-portlet__head-title kt-font-primary">Gross Commission Total</h7>
                                            </div>
                                            <div class="kt-notification-v2__item-desc">
                                                <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <?= $total_amt_to_release; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-sm-2">
                                    <span href="#" class="kt-notification-v2__item">
                                        <span href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <a href="javascript:void(0);" class="btn btn-label-instagram btn-sm btn-elevate" id='create_payment_requests' >
                                                    <i class="fa fa-money-check-alt"></i> Create Payment Request
                                                </a>
                                            </div>
                                        </span>
                                    </span>
                                </div>
                                <div class="col-sm-12">
                                    <?php if ($commissions) { ?>
                                        <table class="table table-striped- table-bordered table-hover" id='commission_history'>
                                            <tr>
                                                <th> <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success" style='padding-bottom:5px;'>
                                                        <input type="checkbox" class='check_all'>
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th>Transaction Reference</th>
                                                <th>Commission Amount</th>
                                                <th>Net Amount</th>
                                                <th>Property</th>
                                                <th>Project</th>
                                                <th>Remarks</th>
                                                <th>Target Date</th>
                                            </tr>
                                            <?php $total_net = 0;
                                            $total_amt_to_collect = 0;
                                            $first_release_amount = @$commissions[0]['commission_amount_rate'];
                                            $transaction_id = $commissions[0]['transaction_id'];
                                            $key_w = 0;?>

                                            <?php foreach ($commissions as $key => $commission) {
                                                $project_name = Dropdown::get_dynamic('projects',$commission['transaction']['project_id'],'name','id','view');
                                                $property_name = Dropdown::get_dynamic('properties',$commission['transaction']['property_id'],'name','id','view');
                                                if($commission['transaction_id'] != $transaction_id){
                                                    $key_w = 0;
                                                    $first_release_amount = $commission['commission_amount_rate'];
                                                    $transaction_id = $commission['transaction_id'];
                                                }
                                                $net = get_net_amount($commission['commission_amount_rate'], $commission['wht_rate'], $first_release_amount, $commission['period_type_id'], $key_w);
                                                $key_w++;
                                            ?>
                                                <tr>
                                                    <td>
                                                        <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success" style='padding-bottom:5px;'>
                                                            <input type="checkbox" class='commission_box' data-commission-id="<?= $commission['id'] ?>" data-net=<?= $net ?> data-gross="<?= $commission['commission_amount_rate'] ?>" data-company='<?= @$commission['transaction']['project']['company_id'] ?>' data-project='<?= $commission['transaction']['project_id']?>' data-property='<?= $commission['transaction']['property_id']?>'>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td><a href='<?= base_url('transaction/view/'.$commission['transaction_id']) ?>'><?= Dropdown::get_dynamic('transactions',$commission['transaction_id'],'reference','id','view'); ?></a></td>
                                                    <td><?= money_php($commission['commission_amount_rate']) ?></td>
                                                    <td><?= money_php($net); ?></td>
                                                    <td><a href='<?= base_url('property/view/'. $commission['transaction']['property_id']) ?>'><?= $property_name ?></a></td>
                                                    <td><a href='<?= base_url('project/view/'. $commission['transaction']['project_id']) ?>'><?= $project_name ?></a></td>
                                                    <td><?=$commission['remarks']; ?></td>
                                                    <td><?= view_date($commission['target_date']); ?></td>
                                                </tr>
                                            <?php } ?>
                                        </table>

                                    <?php } else { ?>

                                        <div class="alert alert-outline-danger fade show" role="alert">
                                            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                                            <div class="alert-text"> No commission schedule for this seller. </div>
                                            <div class="alert-close">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>


                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>