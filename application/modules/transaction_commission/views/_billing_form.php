<?php
    // Billing Information
    $financing_scheme_id = isset($info['financing_scheme_id']) && $info['financing_scheme_id'] ? $info['financing_scheme_id'] : '';
    $reservation_date = isset($info['reservation_date']) && $info['reservation_date'] ? $info['reservation_date'] : '';
    $expiration_date = isset($info['expiration_date']) && $info['expiration_date'] ? $info['expiration_date'] : '';
?>




<div class="row">

    <div class="col-md-6">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Reservation Date <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="reservation_date" placeholder="Reservation Date" name="billing[reservation_date]" value="<?php echo set_value('billing[reservation_date]"', $reservation_date); ?>">
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-6">

        <div class="row">
            <div class="col-sm-12">
                <label class="">Expiration Date <span class="kt-font-danger">*</span></label>
                <div class="form-group">
                    <div class="kt-input-icon kt-input-icon--left">
                        <input type="text" class="form-control datePicker" id="expiration_date" placeholder="Expiration Date" name="billing[expiration_date]" value="<?php echo set_value('billing[expiration_date]"', $expiration_date); ?>" readonly>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-md-6">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Financing Scheme <span class="kt-font-danger">*</span></label>
                    <select class="form-control kt-select2" id="financing_scheme_id" name="billing[financing_scheme_id]" <?php //echo ($this->router->fetch_method() === 'form' && !empty($this->uri->segment(3)) ) ? 'disabled' : ''; ?>>
                        <option value="">Select Option</option>
                        <?php foreach ($schemes as $scheme): ?>
                                <option value="<?php echo $scheme['id'] ?>" <?php echo ($financing_scheme_id == $scheme['id']) ? 'selected' : '' ?>><?php echo ucwords($scheme['name']); ?></option>
                        <?php endforeach;?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

    </div>
</div>


 <div class="row">

    <div class="col-sm-12 financing_scheme_steps">
        
    </div>

</div>