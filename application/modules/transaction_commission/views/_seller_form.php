<?php
    $sellers = isset($info['sellers']) && $info['sellers'] ? $info['sellers'] : '';
?>

<div class="row">
    <div class="col-md-12">

        <a href="<?php echo base_url(); ?>seller/create" target="_blank" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand">Add Seller Record Here</a>

    </div>
</div>

<br>    
<br>  

<div class="row">
    <div class="col-md-8">

        <label>Sales Force Information : </label>

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">Commissionable Amount <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <?php echo form_dropdown('property[commissionable_type_id]', Dropdown::get_static('commissionable_types'), set_value('commissionable_type_id', @$commissionable_type_id), 'class="form-control sellerComp" id="commissionable_type_id"'); ?>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">&nbsp;</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="commissionable_amount" placeholder="" name="property[commissionable_amount]" value="<?php echo set_value('property[commissionable_amount]"', @$commissionable_amount); ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="">Remarks</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <textarea class="form-control" name="property[commissionable_amount_remarks]" id="commissionable_amount_remarks"></textarea>
                    </div>
                </div>
            </div>

        </div>
       
        
    </div>
</div>

<div class="row">

    <div class="col-md-12">

        <div id="seller_form_repeater">
            <div class="form-grouprow">
                <label>Sales Force Information : 
                    <div data-repeater-create="" class="btn btn-sm btn-primary">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Add</span>
                        </span>
                    </div>
                </label>

                <div data-repeater-list="seller" class="col-lg-12">
                    <div data-repeater-item class="row kt-margin-b-10 seller_row">
                        <div class="col-sm-2">

                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Name:</label>
                            </div>

                            <div class="input-group">
                                <select class="form-control suggests sellerComp" data-module="sellers" data-parent="seller_row" data-needed="seller_position_id" data-target="seller_position_id" data-type="person" id="seller_id" name="seller_id">
                                    <option value="0">Select Seller Name</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-2">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller Position:</label>
                            </div>
                            <?php echo form_dropdown('seller_position_id', Dropdown::get_dynamic('seller_positions'), set_value('seller_position_id', @$seller_position_id), 'class="form-control sellerComp seller_position_id" id="seller_position_id"'); ?>
                        </div>

                        <div class="col-sm-2">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Percentage Rate:</label>
                            </div>
                            <input type="text" class="form-control sellerComp sellers_rate" id="sellers_rate" name="sellers_rate" value="0">
                        </div>

                        <div class="col-sm-2">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Seller's Rate Amount:</label>
                            </div>
                            <input type="text" class="form-control sellers_rate_amount" id="sellers_rate_amount" name="sellers_rate_amount" readonly="" value="0.00">
                        </div>

                        <div class="col-sm-2">
                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                <i class="la la-remove"></i>
                            </a>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

</div>