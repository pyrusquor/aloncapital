<?php
	$property = $info['property'];
	$project = $info['project'];
	$t_property = $info['t_property'];
    $property_name = $property['name'];
    $project_name = $project['name'];
    $project_name = $project['name'];
    $floor_area = $t_property['floor_area'];
    $lot_area = $t_property['lot_area'];
    $total_lot_price = $t_property['total_lot_price'];
    $lot_price_per_sqm = $t_property['lot_price_per_sqm'];
    $total_house_price = $t_property['total_house_price'];
    $total_selling_price = $t_property['total_selling_price'];
    $total_contract_price = $t_property['total_contract_price'];
    $total_discount_price = $t_property['total_discount_price'];
    $total_miscellaneous_amount = $t_property['total_miscellaneous_amount'];
    $collectible_price = $t_property['collectible_price'];

    $house_model = get_value_field($t_property['house_model_id'],'house_models','name');
    $house_model_interior = get_value_field($t_property['house_model_interior_id'],'house_model_interiors','name');
?>
<script type='text/javascript'>
var gen_project_id = '<?=$project['id']?>';
var gen_project_name = '<?=$project['name']?>';
var gen_property_id = '<?=$property['id']?>';
var gen_property_name = '<?=$property['name']?>';
</script>

<!-- Property Information -->
<div class="accordion accordion-solid accordion-toggle-svg" id="accord_property_information">
	<div class="card">
		<div class="card-header" id="head_property_information">
			<div class="card-title" data-toggle="collapse" data-target="#property_information" aria-expanded="true" aria-controls="property_information">
				Property Information <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
						<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
						<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
					</g>
				</svg>
			</div>
		</div>
		<div class="kt-separator kt-separator--border-solid kt-separator--space-none"></div>
		<div id="property_information" class="collapse show" aria-labelledby="head_property_information" data-parent="#accord_property_information">
			<div class="card-body">
				<div class="form-group form-group-xs row">
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Project Name</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<a href="<?php echo base_url(); ?>project/view/<?php echo $project['id']; ?>" target="_BLANK"><?php echo $project_name; ?></a>
									</h6>
								</div>
							</div>
						</span>
					</div>
				</div>
				<div class="form-group form-group-xs row">
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Property Name</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<a href="<?php echo base_url(); ?>property/view/<?php echo $property['id']; ?>" target="_BLANK"><?php echo $property_name; ?></a>
									</h6>
								</div>
							</div>
						</span>
					</div>
				</div>
				<div class="form-group form-group-xs row">
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Lot Area</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo $lot_area; ?> sqm (<?php echo money_php($lot_price_per_sqm); ?> per sqm)
									</h6>
								</div>
							</div>
						</span>
					</div>
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Total Lot Price</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo money_php($total_lot_price); ?>
									</h6>
								</div>
							</div>
						</span>
					</div>
				</div>
				<div class="form-group form-group-xs row">
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Floor Area</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo $floor_area; ?> sqm
									</h6>
								</div>
							</div>
						</span>
					</div>
					<div class="col-sm-6">
						&nbsp;
					</div>
				</div>
				
				<div class="form-group form-group-xs row">
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Property Model & Interior</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo $house_model.' : '.$house_model_interior; ?> 
									</h6>
								</div>
							</div>
						</span>
					</div>
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">House Price</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo money_php($total_house_price);?>
									</h6>
								</div>
							</div>
						</span>
					</div>
				</div>

				<div class="form-group form-group-xs row">
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Total Selling Price</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo money_php($total_selling_price);?>
									</h6>
								</div>
							</div>
						</span>
					</div>
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Description / Formula</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										Total Selling Price = Total Lot Price + Total House Price
										<br>
										<?php echo money_php($total_selling_price);?> = <?php echo money_php($total_lot_price)." + ". money_php($total_house_price);?>
									</h6>
								</div>
							</div>
						</span>
					</div>
				</div>

				<div class="form-group form-group-xs row">
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Total Miscellaneous Price</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo money_php($total_miscellaneous_amount);?>
									</h6>
								</div>
							</div>
						</span>
					</div>
					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Total Contract Price</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo money_php($total_contract_price);?>
									</h6>
								</div>
							</div>
						</span>
					</div>
				</div>

				<div class="form-group form-group-xs row">

					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Total Promos and Discount Price</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo money_php($total_discount_price);?>
									</h6>
								</div>
							</div>
						</span>
					</div>

					<div class="col-sm-6">
						<span href="#" class="kt-notification-v2__item">
							<div class="kt-notification-v2__itek-wrapper">
								<div class="kt-notification-v2__item-title">
									<h7 class="kt-portlet__head-title kt-font-primary">Total Collectible Price</h7>
								</div>
								<div class="kt-notification-v2__item-desc">
									<h6 class="kt-portlet__head-title kt-font-dark">
										<?php echo money_php($collectible_price);?>
									</h6>
								</div>
							</div>
						</span>
					</div>
				</div>

				
			</div>
		</div>
	</div>
</div>