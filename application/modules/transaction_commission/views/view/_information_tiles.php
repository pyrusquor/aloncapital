<div class=row>
    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-light kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        TOTAL COMMISSIONS
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="total_commission_count" class="kt-font-boldest">254</h4>&nbsp;item(s)
                </div>
            </div>
            <div class="kt-portlet__body">
                <span class="kt-widget20__number display-4">
                    <h4 id="total_commission_amount"></h4>
                </span>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-brand kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        FOR RFP
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="for_approval_count" class="kt-font-boldest">254</h4>&nbsp;item(s)
                </div>
            </div>
            <div class="kt-portlet__body">
            <span class="kt-widget20__number display-4">
                <h4 id="for_approval_amount"></h4>
            </span>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-success kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check"></i>
                    </span>
                    <div class="kt-portlet__head-title">
                        UNDER PROCESSING
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="approved_commissions_count" class="kt-font-boldest">254</h4>&nbsp;item(s)
                </div>
            </div>
            <div class="kt-portlet__body">
            <span class="kt-widget20__number display-4">
                <h4 id="approved_commissions_amount"></h4>
            </span>
            </div>
        </div>
    </div>

    <div class="col-md-9 col-lg-9 col-xl-3">
        <div class="kt-portlet kt-portlet--solid-danger kt-portlet--fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="fa fa-money-check kt-font-light"></i>
                    </span>
                    <div class="kt-portlet__head-title kt-font-light">
                        PAID COMMISSIONS
                    </div>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <h4 id="paid_commissions_count" class="kt-font-boldest">254</h4>&nbsp;item(s)
                </div>
            </div>
            <div class="kt-portlet__body">
                <span class="kt-widget20__number display-4 kt-font-light">
                <h4 id="paid_commissions_amount"></h4>
                </span>
            </div>
        </div>
    </div>
</div>