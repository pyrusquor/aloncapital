<?php
    $id = isset($property['id']) && $property['id'] ? $property['id'] : '';

    $remarks = isset($info['remarks']) && $info['remarks'] ? $info['remarks'] : '';

    $property = isset($info['property']) && $info['property'] ? $info['property'] : '';
    $property_name = isset($property['name']) && $property['name'] ? $property['name'] : '';
    $property_id = isset($property['id']) && $property['id'] ? $property['id'] : '';

    $project = isset($info['project']) && $info['project'] ? $info['project'] : '';
    $project_name = isset($project['name']) && $project['name'] ? $project['name'] : '';
    $project_id = isset($project['id']) && $project['id'] ? $project['id'] : '';

    $t_property = isset($info['t_property']) && $info['t_property'] ? $info['t_property'] : '';
    $floor_area = isset($t_property['floor_area']) && $t_property['floor_area'] ? $t_property['floor_area'] : '';
    $lot_area = isset($t_property['lot_area']) && $t_property['lot_area'] ? $t_property['lot_area'] : '';
    $total_lot_price = isset($t_property['total_lot_price']) && $t_property['total_lot_price'] ? $t_property['total_lot_price'] : '';
    $lot_price_per_sqm = isset($t_property['lot_price_per_sqm']) && $t_property['lot_price_per_sqm'] ? $t_property['lot_price_per_sqm'] : '';
    $total_house_price = isset($t_property['total_house_price']) && $t_property['total_house_price'] ? $t_property['total_house_price'] : '';
    $total_selling_price = isset($t_property['total_selling_price']) && $t_property['total_selling_price'] ? $t_property['total_selling_price'] : '';
    $total_contract_price = isset($t_property['total_contract_price']) && $t_property['total_contract_price'] ? $t_property['total_contract_price'] : '';
    $collectible_price = isset($t_property['collectible_price']) && $t_property['collectible_price'] ? $t_property['collectible_price'] : '';

    $total_discount_price = isset($t_property['total_discount_price']) && $t_property['total_discount_price'] ? $t_property['total_discount_price'] : '';
    $total_miscellaneous_amount = isset($t_property['total_miscellaneous_amount']) && $t_property['total_miscellaneous_amount'] ? $t_property['total_miscellaneous_amount'] : '';
    
    $house_model_id = isset($t_property['house_model_id']) && $t_property['house_model_id'] ? $t_property['house_model_id'] : '';
    $house_model = get_value_field($house_model_id,'house_models','name');

    $house_model_interior_id = isset($t_property['house_model_interior_id']) && $t_property['house_model_interior_id'] ? $t_property['house_model_interior_id'] : '';
    $house_model_interior = get_value_field($house_model_interior_id,'house_model_interiors','name');
?>

<div class="row">
    <div class="col-md-6">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Project Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control kt-select2" id="project_id" name="property[project_id]" <?php //echo ($this->router->fetch_method() === 'form' && !empty($this->uri->segment(3)) ) ? 'disabled' : ''; ?>>
                        <?php foreach ($projects as $p): ?>
                                <option value="<?php echo $p['id'] ?>" <?php echo ($project_id == $p['id']) ? 'selected' : '' ?>>
                                    <?php echo ucwords($p['name']); ?>
                                </option>
                        <?php endforeach;?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">Property Model Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests popField" data-module="house_models" data-param="project_id" id="house_model_id" name="property[house_model_id]">
                        <option value="">Select Property Model</option>
                        <?php if ($house_model): ?>
                            <option value="<?php echo $house_model_id; ?>" selected><?php echo $house_model; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="">House Interior <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests popField" data-module="house_model_interiors" data-param="house_model_id" id="interior_id" name="property[house_model_interior_id]">
                        <option value="">Select House Interior</option>
                        <?php if ($house_model_interior): ?>
                            <option value="<?php echo $house_model_interior_id; ?>" selected><?php echo $house_model_interior; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Lot Area <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control popField" id="lot_area" placeholder="Lot Area" name="property[lot_area]" value="<?php echo set_value('property[lot_area]"', $lot_area); ?>" readonly>
                            <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                        </div>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Floor Area <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control popField" id="floor_area" placeholder="Floor Area" name="property[floor_area]" value="<?php echo set_value('property[floor_area]"', $floor_area); ?>" readonly>
                            <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                        </div>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Lot Price / SQM<span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control popField" id="lot_price_per_sqm" placeholder="Lot Price / SQM" name="property[lot_price_per_sqm]" value="<?php echo set_value('property[lot_price_per_sqm]"', $lot_price_per_sqm); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">price in this field is fetch from project base lot of the chosen property</span>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Total Lot Price <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control popField" id="total_lot_price" placeholder="Total Lot Price" name="property[total_lot_price]" value="<?php echo set_value('property[total_lot_price]"', $total_lot_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Formula = Lot Price per SQM * Lot Area</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Property Model Price </label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control popField" placeholder="Property Model Price"  id="total_house_price" name="property[total_house_price]" value="<?php echo set_value('property[total_house_price]"', $total_house_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - price in this field is fetch from house interior latest price</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Remarks </label>
                    <textarea class="form-control" id="remarks" name="info[remarks]">
                        <?php echo set_value('info[remarks]"', $remarks); ?>
                    </textarea>
                </div>
            </div>
        </div>
        
    </div>

    <div class="col-md-6">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="">Property Name <span class="kt-font-danger">*</span></label>
                    <select class="form-control suggests" data-module="properties" data-param="project_id" id="property_id" name="property[property_id]">
                        <option value="">Select Property</option>
                        <?php if ($property_name): ?>
                            <option value="<?php echo $property_id; ?>" selected><?php echo $property_name; ?></option>
                        <?php endif ?>
                    </select>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Total Selling Price <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control popField" id="total_selling_price" placeholder="Total Selling Price" name="property[total_selling_price]" value="<?php echo set_value('property[total_selling_price]"', $total_selling_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Formula = Total Lot Price + Property Model Price</span>
                </div>
            </div>
        </div>

         <div class="row hide">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>VAT </label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <input type="text" class="form-control" id="vat" placeholder="VAT" name="" value="<?php echo set_value('property[vat]"', @$vat); ?>" readonly>
                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                        </div>
                        <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-info-circle"></i></span></span>
                    </div>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            
            <div class="col-sm-6">
                <div class="form-group">
                    <label>VAT Amount</label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="vat_amount" placeholder="VAT Amount" name="" value="<?php echo set_value('property[vat_amount]"', @$total_lot_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Formula =  Total Selling Price * VAT</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div id="fees_form_repeater">
                    <div class="form-grouprow">
                        <label>Additional Fees : 
                            <div data-repeater-create="" class="btn btn btn-primary">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add</span>
                                </span>
                            </div>
                        </label>

                        <div data-repeater-list="fees" class="col-lg-12">
                            <div data-repeater-item class="row kt-margin-b-10">

                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <select class="form-control feesID" id="fees_id" name="fees_id">
                                            <option value="0">Select Fees</option>
                                            <?php if ($fees): ?>
                                                <?php foreach ($fees as $key => $fee) { ?>
                                                    <option data-value="<?php echo $fee['value']; ?>" data-value-type="<?php echo $fee['value_type']; ?>" data-fees-type="<?php echo $fee['fees_type']; ?>" data-add-to="<?php echo $fee['add_to']; ?>" value="<?php echo $fee['id']; ?>"><?php echo $fee['name']; ?></option>
                                                <?php } ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                                        <input type="text" class="form-control feesPrice" id="fees_amount_rate" placeholder="Fee Amount" name="fees_amount_rate" value="<?php echo set_value('fees[price]"', @$fees_amount_rate); ?>" readonly>
                                    </div>
                                    <span class="form-text text-muted">auto-generated - Formula = <span class="formula"></span> </span>
                                </div>

                                <div class="col-sm-2">
                                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                        <i class="la la-remove"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Total Contract Price <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="total_contract_price" placeholder="Total Contract Price" name="property[total_contract_price]" value="<?php echo set_value('property[total_contract_price]"', $total_contract_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Total Selling Price + Total Additional Fees</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div id="promos_form_repeater">
                    <div class="form-grouprow">
                        <label>Promos & Discounts : 
                            <div data-repeater-create="" class="btn btn btn-primary">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add</span>
                                </span>
                            </div>
                        </label>

                        <div data-repeater-list="promos" class="col-lg-12">
                            <div data-repeater-item class="row kt-margin-b-10">

                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <select class="form-control promoID" id="discount_id" name="discount_id">
                                            <option value="0">Select Option</option>
                                            <?php if ($promos): ?>
                                                <?php foreach ($promos as $key => $promo) { ?>
                                                    <option data-value="<?php echo $promo['value']; ?>" data-value-type="<?php echo $promo['value_type']; ?>" data-promo-type="<?php echo $promo['promo_type']; ?>" data-deduct-to="<?php echo $promo['deduct_to']; ?>" value="<?php echo $promo['id']; ?>"><?php echo $promo['name']; ?></option>
                                                <?php } ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                                        <input type="text" class="form-control promoPrice" id="discount_amount_rate" placeholder="Promo Amount" name="discount_amount_rate" value="<?php echo set_value('promos[price]"', @$discount_amount_rate); ?>" readonly>
                                    </div>
                                    <span class="form-text text-muted">auto-generated - Formula = <span class="formula"></span> </span>
                                </div>

                                <div class="col-sm-2">
                                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                        <i class="la la-remove"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Collectible Price <span class="kt-font-danger">*</span></label>
                    <div class="kt-input-icon kt-input-icon--left">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text">PHP</span></div>
                            <input type="text" class="form-control" id="collectible_price" placeholder="Collectible Price" name="property[collectible_price]" value="<?php echo set_value('property[collectible_price]"', $collectible_price); ?>" readonly>
                        </div>
                    </div>
                    <span class="form-text text-muted">auto-generated - Total Contract Price - Promos & Discounts</span>
                </div>
            </div>
        </div>

    </div>
</div>
