<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction_commission_model extends MY_Model {

	public $table = 'transaction_commissions'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'transaction_id',
		'seller_id',
		'period_type_id',
		'percentage_rate_to_collect',
		'amount_rate_to_collect',
		'commission_percentage_rate',
		'commission_amount_rate',
		'wht_rate',
		'target_date',
		'status',
		'remarks',
        'payment_request_id',
        'payment_voucher_id',
		'is_active',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

        $this->soft_deletes = TRUE;
		$this->return_as = 'array';
		
		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">','</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>','<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;


		$this->has_one['transaction'] = array('foreign_model'=>'transaction/transaction_model','foreign_table'=>'transactions','foreign_key'=>'id','local_key'=>'transaction_id');
		$this->has_one['seller'] = array('foreign_model'=>'seller/seller_model','foreign_table'=>'sellers','foreign_key'=>'id','local_key'=>'seller_id');

		$this->has_one['payment_voucher'] = array('foreign_model'=>'payment_voucher/Payment_voucher_model','foreign_table'=>'payment_vouchers','foreign_key'=>'id','local_key'=>'payment_voucher_id');
		$this->has_one['payment_request'] = array('foreign_model'=>'payment_request/Payment_request_model','foreign_table'=>'payment_requests','foreign_key'=>'id','local_key'=>'payment_request_id');

	}

	function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }


    public function show_rfp($seller_id = 0)
    {
        $this->db->select('transaction_commissions.id, transaction_id ,sellers.last_name,sellers.id as seller_id, sellers.first_name, projects.name, period_type_id');
        $this->db->select('percentage_rate_to_collect, amount_rate_to_collect, commission_percentage_rate, transaction_commissions.remarks');
        $this->db->select('commission_amount_rate, transaction_commissions.wht_rate, transaction_commissions.status');
        $this->db->from('transaction_commissions');
        $this->db->where('transaction_commissions.status', 2);
        $this->db->where('transaction_commissions.deleted_at is null');
		if($seller_id){
			$this->db->where('transaction_commissions.seller_id',$seller_id);
		}
        $this->db->join('transactions', 'transaction_commissions.transaction_id = transactions.id');
        $this->db->join('sellers', 'transaction_commissions.seller_id = sellers.id');
        $this->db->join('projects', 'transactions.project_id = projects.id');
        $query = $this->db->get()->result_array();
        return $query;
    }

	public function get_target_date($period = 0, $transaction_id = 0, $amount_to_collect = 0)
	{
		
		$result = '00-00-00 00:00:00';
		if ($period == 1) {

			$this->db->select('DATE_FORMAT(due_date,"%Y-%m-%d") as due_date');
			$this->db->where('transaction_id', $transaction_id);
			$this->db->where('deleted_at IS NULL');
			$this->db->order_by('due_date', 'ASC');
			$query = $this->db->get('transaction_payments')->result_array();
			if ($query) {
				$result = date($query[0]['due_date']);
			}
		} elseif ($period == 2 || $period == 3) {
			$this->db->select('principal_amount,DATE_FORMAT(due_date,"%Y-%m-%d") as due_date');
			$this->db->where('transaction_id', $transaction_id);
			$this->db->where('deleted_at IS NULL');
			$this->db->order_by('due_date', 'ASC');
			$query = $this->db->get('transaction_payments')->result_array();
			$temp_val = 0;
			if ($query) {
				foreach ($query as $key => $value) {

					$temp_val += $value['principal_amount'];
					if ($temp_val >= $amount_to_collect) {
						$result = $value['due_date'];
						break;
					}
				}
			}
		}
		return $result;
	}

	public function get_monthly_commissions($date = "", $commission_status=0,$project_id=0)
	{
		$date = "$date-01";
		$this->db->select('sum(transaction_commissions.commission_amount_rate) as sum');
		$this->db->join('transactions', 'transaction_commissions.transaction_id = transactions.id');
		if ($commission_status != 0) {
			$this->db->where("transaction_commissions.status=$commission_status");
		}
		if ($project_id){
			$this->db->where("transactions.project_id=$project_id");
		}
		$this->db->where("transaction_commissions.deleted_at is NULL");
		$this->db->where("transactions.deleted_at is NULL");
		$this->db->where("year(transaction_commissions.target_date)=year('$date')");
		$this->db->where("month(transaction_commissions.target_date)=month('$date')");
		$query = $this->db->get('transaction_commissions')->result_array();

		if ($query) {
			if ($query[0]['sum'] == 0.00) {
				$result = 0.00;
			} else {
				$result = $query[0]['sum'];
			}
		} else {
			$result = 0.00;
		}
		return $result;
	}
}
