<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends My_Controller
{

	public function __construct()
	{
		$this->load->model('house/House_model', 'M_house');
        $this->load->model('locations/Address_regions_model', 'M_region');
        $this->load->model('locations/Address_provinces_model', 'M_province');
        $this->load->model('locations/Address_city_municipalities_model', 'M_city');
        $this->load->model('locations/Address_barangays_model', 'M_barangay');
        $this->load->model('taxes/Taxes_model', 'M_taxes');
        $this->load->model('transaction/Transaction_model', 'M_transaction');
	}

	public function index(){

		$get = $this->input->get();
		$term = @$get['term'];
		$param = @$get['param'];
		$param2 = @$get['param2'];
		$param_val = @$get['param_val'];
		$param_val2 = @$get['param_val2'];
		$param_type = @$get['param_type'];
		$s = @$get['select'];
		$mod = $get['mod'];
		$special = 0;

		$page = !empty($get['page']) ? $get['page'] : 0;
		$perpage = 10;
		$offset = $page === 0 ? $page : ($page * $perpage) - $perpage;

		if ( (($param == "transaction_category_stage_id") && ($mod == "transaction_document_stages")) || (($param == "loan_category_stage_id") && ($mod == "transaction_loan_document_stages")) ) {
			$param = 'category_id';
		}

		if (isset($param_type) && ($param_type == "person") && ($mod == "buyers")) {
            $where = "
				`company_name` LIKE '%" . $term . "%' OR `first_name` LIKE '%" . $term . "%' OR `last_name` LIKE '%" . $term . "%'";

            $select = array("company_name", "first_name", "last_name", "id");
            $order_by[] = array('first_name', 'ASC');

        } else if (isset($param_type) && ($param_type == "person")) {
            $where = "
				`first_name` LIKE '%" . $term . "%' OR `last_name` LIKE '%" . $term . "%'";

            $select = array("first_name", "last_name", "id");
            $order_by[] = array('first_name', 'ASC');

        } else if (isset($param_type) && ($param_type == "bank")) {
            $where = "`name` LIKE '%" . $term . "%' AND type = 1";

            $select = array('name','id' );
            $order_by[] = array('name', 'ACS');

        } else if (isset($param_type) && ($param_type == "payee")) {
            $where = "`name` LIKE '%" . $term . "%' OR auth_payee LIKE '%" . $term . "%'";

            $select = array('name', 'auth_payee', 'id' );
            $order_by[] = array('name', 'ACS');
		} else if (isset($param_type) && ($param_type == "land_inventory")) {

			$where = "`title` LIKE '%" . $term . "%'";
			$select = array('title', 'id');
			$order_by[] = array('title', 'ASC');
		} else if (isset($param_type) && ($mod == "address_regions")) {

			$where = "`regDesc` LIKE '%" . $term . "%'";
			$select = array('regDesc', 'regCode');
			$order_by[] = array('regDesc', 'ASC');
		} else if (isset($param_type) && ($mod == "address_provinces")) {
			
			$where = "`provDesc` LIKE '%" . $term . "%'";
			$select = array('provDesc', 'provCode');
			$order_by[] = array('provDesc', 'ASC');
		} else if (isset($param_type) && ($mod == "address_city_municipalities")) {

			$where = "`citymunDesc` LIKE '%" . $term . "%'";
			$select = array('citymunDesc', 'citymunCode');
			$order_by[] = array('citymunDesc', 'ASC');
		} else if (isset($param_type) && ($mod == "address_barangays")) {

			$where = "`brgyDesc` LIKE '%" . $term . "%'";
			$select = array('brgyDesc', 'brgyCode');
			$order_by[] = array('brgyDesc', 'ASC');
		} else if (isset($param_type) && ($param_type == "buyer_filter")) {
			if($param_val){
				$special = 1;
			} else{
				$where = "
				`name` LIKE '%" . $term . "%'";
				$select = array('name','id' );
				$order_by[] = array('name', 'ACS');
			}

		} else if ( isset($s) && !empty($s)) {
			$where = "
				`".$s."` LIKE '%" . $term . "%'"
			;
			
			$select = array($s,"id");
			$order_by[] = array($s,'ASC');
		} else {
			$where = "
				`name` LIKE '%" . $term . "%'
			";
			$select = array("name","id");
			$order_by[] = array('name','ASC');
		}

		if($special){
			
			// $s for the id
			// $mod for the table
			// $param_val for the buyer_id

			$this->db->select("transactions.$s as $s");
			$this->db->from('buyers');
			$this->db->join('transactions','transactions.buyer_id = buyers.id and transactions.deleted_at is null');
			$this->db->join($mod,"transactions.$s = $mod.id and $mod.deleted_at is null");
			if($param2){
				$this->db->where("$mod.$param2 = '$param_val2'");
			}
			$this->db->where("$mod.name like '%$term%'");
			$this->db->where("transactions.buyer_id = $param_val");
			$this->db->group_by("transactions.$s");
			$total_count = $this->db->count_all_results('',FALSE);
			$this->db->limit($perpage,$offset);
			$result = $this->db->get()->result();

		}else{
			if($param_type!='address' || $param_type!='buyer_filter'){
				$where .= " AND `deleted_at` IS NULL";
			}

			if ($param && $param_val) {
				$where .= " AND
				`".$param."` = '" . $param_val . "'
				";
			}

			if ($param2 && $param_val2) {
				$where .= " AND
				`".$param2."` = '" . $param_val2 . "'
				";
			}

			$params = array(
				'select'=> $select,
				'where' => $where,
				'order' => $order_by,
				'limit' => array($perpage, $offset),
				'table' => $mod
			);
			$total_count = $this->M_house->get_dynamic($params, TRUE);
			$result = $this->M_house->get_dynamic($params/*, FALSE, TRUE*/);
		}


		$resultArray = array();
		foreach ($result as $data) {
			if (isset($data->name)) {
				$name = $data->name;
			} else if (isset($s) && !empty($s) && !$special) {
				$name = $data->$s;
			} else if (isset($data->title)){
            	$name = $data->title;
			} else if (isset($data->regDesc)){
            	$name = $data->regDesc;
				$data->id = $data->regCode;
			} else if (isset($data->provDesc)){
            	$name = $data->provDesc;
				$data->id = $data->provCode;
			} else if (isset($data->citymunDesc)){
            	$name = $data->citymunDesc;
				$data->id = $data->citymunCode;
			} else if (isset($data->brgyDesc)){
            	$name = $data->brgyDesc;
				$data->id = $data->brgyCode;
			} else if ($special){
				$data->id = $data->$s;
				$name = Dropdown::get_dynamic($mod,$data->$s,'name','id','view');
			} else {
				$name = @$data->company_name." ".$data->last_name." ".$data->first_name;
			}

			array_push($resultArray, array('id' => $data->id, 'text' => $name));
		}
		
		$response = new stdClass;
		$response->items = $resultArray;
		$response->offset = $offset;
		$response->incomplete_results = ($total_count <= $offset + $perpage) ? FALSE : TRUE;
		$response->total_count = $total_count;
		exit(json_encode($response));
	}

	public function append(){

		$get = $this->input->get();
		$term = $get['term'];
		$param = $get['param'];
		$param_val = $get['param_val'];
		$mod = $get['mod'];

		$page = !empty($get['page']) ? $get['page'] : 0;
		$perpage = 10;
		$offset = $page === 0 ? $page : ($page * $perpage) - $perpage;

		$where = "
			`name` LIKE '%" . $term . "%'
		";

		if ($param) {
			$where .= " AND
			`".$param."` = '" . $param_val . "'
			";
		}

		$params = array(
			'select' => array(
				'name',
				'id'
			),
			'where' => $where,
			'order' => array(
				array('name', 'ASC')
			),
			'limit' => array($perpage, $offset),
			'table' => $mod
		);
		$total_count = $this->M_house->get_dynamic($params, TRUE);
		$result = $this->M_house->get_dynamic($params/*, FALSE, TRUE*/);

		$resultArray = array();
		foreach ($result as $data) {
			array_push($resultArray, array('id' => $data->id, 'text' => $data->name));
		}
		
		$response = new stdClass;
		$response->items = $resultArray;
		$response->offset = $offset;
		$response->incomplete_results = ($total_count <= $offset + $perpage) ? FALSE : TRUE;
		$response->total_count = $total_count;
		exit(json_encode($response));
	}
	

	public function calculator($amt = 0, $int = 0, $term = 0)
    {

        $data['monthly'] = monthly_payment($amt,$int,$term);
        $data['interest'] = ($data['monthly'] * $term) - $amt;
        $data['total'] = $data['monthly'] * $term;

        $return['html'] = $this->load->view('result_calculator',$data,true);
        echo json_encode($return);

    }


    public function pdcalculator($buyer_id = 0, $property_id = 0)
    {

        $where['buyer_id'] = $buyer_id;
        $where['property_id'] = $property_id;
        $this->db->where($where);
        $transaction = $this->M_transaction->as_array()->get();

	    $data['success'] = 0;

        if ($transaction) { 

	        $data['id'] = $transaction_id = $transaction['id']; 
	        $data['reference'] = $transaction['reference']; 
	        $data['past_due_amount'] = @$past_due_amount;
	        $data['transaction_id'] = @$transaction_id;

	        $this->transaction_library->initiate($transaction_id);
	        $payment_status = $this->transaction_library->payment_status(1);
	        $payment_status = $payment_status['days'];
	        $due_date = $this->transaction_library->due_date();
	        $amount_due = $this->transaction_library->total_amount_due();
	        $no_of_mos = $this->transaction_library->total_amount_due(4);

	        $data['total_amount_due'] = ($amount_due);
	        $data['days_delayed'] = $payment_status.' day(s) overdue'; 
	        $data['no_of_mos'] = $no_of_mos;
	        $data['last_date'] = view_date($due_date);
	        $data['period'] = Dropdown::get_static('period_names', $transaction['period_id'], 'view');

	        $data['success'] = 1;
	    }

        $return['html'] = $this->load->view('pd_calculator',$data,true);

        echo json_encode($return);

    }

    public function regions()
    {
        $return = $this->M_region->as_array()->get_all();

        echo json_encode($return);
    }

    public function provinces()
    {
        $return = $this->M_province->where('regCode =', $this->input->post('regionID'))->as_array()->get_all();

        echo json_encode($return);
    }

    public function cities()
    {
        $return = $this->M_city->where('provCode =', $this->input->post('provinceID'))->as_array()->get_all();

        echo json_encode($return);
    }

    public function barangays()
    {
        $return = $this->M_barangay->where('citymunCode =', $this->input->post('cityID'))->as_array()->get_all();

        echo json_encode($return);
    }

    public function tax()
    {
        $return = $this->M_taxes->where('id =', $this->input->post('tax_id'))->as_array()->get_all();

        echo json_encode($return);
    }
}
