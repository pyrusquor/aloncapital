<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Buyer_model extends MY_Model
{

	public $table = 'buyers'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'user_id',
		'type_id',
		'image',
		'company_name',
		'last_name',
		'first_name',
		'middle_name',
		// 'spouse_name',
		'birth_place',
		'birth_date',
		'gender',
		'nationality',
		'civil_status_id',
		'housing_membership_id',
		'present_address',
		'mailing_address',
		'business_address',
		'other_mobile',
		'mobile_no',
		'email',
		'landline',
		'social_media',
		'country_id',
		'regCode',
		'provCode',
		'citymunCode',
		'brgyCode',
		'sitio',
		'is_active',
		'CID',
		'PID',
		'is_exported',
		'aris_id',
		'source_referral',
		'tin',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update

	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [];

	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = TRUE;
		$this->return_as = 'array';

		// Pagination
		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		$this->has_one['employment'] = array('foreign_model' => 'buyer/buyer_employment_model', 'foreign_table' => 'buyer_employment_table', 'foreign_key' => 'buyer_id', 'local_key' => 'id');
		$this->has_one['country'] = array('foreign_model' => 'locations/address_countries_model', 'foreign_table' => 'address_countries', 'foreign_key' => 'id', 'local_key' => 'country_id');
		$this->has_one['region'] = array('foreign_model' => 'locations/address_regions_model', 'foreign_table' => 'address_regions', 'foreign_key' => 'regCode', 'local_key' => 'regCode');
		$this->has_one['province'] = array('foreign_model' => 'locations/address_provinces_model', 'foreign_table' => 'address_provinces', 'foreign_key' => 'provCode', 'local_key' => 'provCode');
		$this->has_one['city'] = array('foreign_model' => 'locations/address_city_municipalities_model', 'foreign_table' => 'address_city_municipalities', 'foreign_key' => 'citymunCode', 'local_key' => 'citymunCode');
		$this->has_one['barangay'] = array('foreign_model' => 'locations/Address_barangays_model', 'foreign_table' => 'address_barangays', 'foreign_key' => 'brgyCode', 'local_key' => 'brgyCode');

		$this->has_many['identifications'] = array('foreign_model' => 'buyer/buyer_identification_model', 'foreign_table' => 'buyer_identifications', 'foreign_key' => 'buyer_id', 'local_key' => 'id');
	}

	function role_exists($table, $field, $value)
	{
		$this->db->where($field, $value);
		$query = $this->db->get($table);
		$exist = !empty($query->result_array());
		if ($exist) {
			return 1;
		} else {
			return 0;
		}
	}
}
