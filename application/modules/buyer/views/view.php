<?php
// Buyer Information
$id = isset($info['id']) && $info['id'] ? $info['id'] : 'N/A';
$buyer_type_id = isset($info['type_id']) && $info['type_id'] ? $info['type_id'] : 'N/A';
$company_name = isset($info['company_name']) && $info['company_name'] ? $info['company_name'] : 'N/A';
$last_name = isset($info['last_name']) && $info['last_name'] ? $info['last_name'] : 'N/A';
$image = isset($info['image']) && $info['image'] ? $info['image'] : 'N/A';
$first_name = isset($info['first_name']) && $info['first_name'] ? $info['first_name'] : 'N/A';
$middle_name = isset($info['middle_name']) && $info['middle_name'] ? $info['middle_name'] : 'N/A';
$birth_place = isset($info['birth_place']) && $info['birth_place'] ? $info['birth_place'] : 'N/A';
$birth_date = isset($info['birth_date']) && $info['birth_date'] && (strtotime($info['birth_date']) > 0) ? date_format(date_create($info['birth_date']), 'F j, Y') : 'N/A';
$nationality = isset($info['nationality']) && $info['nationality'] ? $info['nationality'] : 'N/A';
$civil_status_id = isset($info['civil_status_id']) && $info['civil_status_id'] ? $info['civil_status_id'] : 'N/A';
$gender = isset($info['gender']) && $info['gender'] ? $info['gender'] : 'N/A';
$housing_membership_id = isset($info['housing_membership_id']) && $info['housing_membership_id'] ? $info['housing_membership_id'] : 'N/A';
$source_referral = isset($info['source_referral']) && $info['source_referral'] ? $info['source_referral'] : 'N/A';
$tin = isset($info['tin']) && $info['tin'] ? $info['tin'] : 'N/A';

// Contact Info
$present_address = isset($info['present_address']) && $info['present_address'] ? $info['present_address'] : 'N/A';
$mailing_address = isset($info['mailing_address']) && $info['mailing_address'] ? $info['mailing_address'] : 'N/A';
$business_address = isset($info['business_address']) && $info['business_address'] ? $info['business_address'] : 'N/A';
$mobile_no = isset($info['mobile_no']) && $info['mobile_no'] ? $info['mobile_no'] : 'N/A';
$other_mobile = isset($info['other_mobile']) && $info['other_mobile'] ? $info['other_mobile'] : 'N/A';
$email = isset($info['email']) && $info['email'] ? $info['email'] : 'N/A';
$landline = isset($info['landline']) && $info['landline'] ? $info['landline'] : 'N/A';
$region = isset($info['region']->regDesc) && $info['region']->regDesc ? $info['region']->regDesc : '';
$province = isset($info['province']->provDesc) && $info['province']->provDesc ? $info['province']->provDesc : '';
$city = isset($info['city']->citymunDesc) && $info['city']->citymunDesc ? $info['city']->citymunDesc : '';
$barangay = isset($info['barangay']->brgyDesc) && $info['barangay']->brgyDesc ? $info['barangay']->brgyDesc : '';
$sitio = isset($info['sitio']) && $info['sitio'] ? $info['sitio'] : '';
$social_media = isset($info['social_media']) && $info['social_media'] ? $info['social_media'] : 'N/A';

// Employment
$occupation_type_id = isset($info['employment']['occupation_type_id']) && $info['employment']['occupation_type_id'] ? $info['employment']['occupation_type_id'] : 'N/A';
$industry_id = isset($info['employment']['industry_id']) && $info['employment']['industry_id'] ? $info['employment']['industry_id'] : 'N/A';
$occupation_id = isset($info['employment']['occupation_id']) && $info['employment']['occupation_id'] ? $info['employment']['occupation_id'] : 'N/A';
$designation = isset($info['employment']['designation']) && $info['employment']['designation'] ? $info['employment']['designation'] : 'N/A';
$employer = isset($info['employment']['employer']) && $info['employment']['employer'] ? $info['employment']['employer'] : 'N/A';
$gross_salary = isset($info['employment']['gross_salary']) && $info['employment']['gross_salary'] ? $info['employment']['gross_salary'] : 'N/A';
$location_id = isset($info['employment']['location_id']) && $info['employment']['location_id'] ? $info['employment']['location_id'] : 'N/A';
$address = isset($info['employment']['address']) && $info['employment']['address'] ? $info['employment']['address'] : 'N/A';

$customer_detail = isset($customer_detail) && $customer_detail ? $customer_detail : 0;
$aris_phone_no = isset($customer_detail['PHONE_NO']) && $customer_detail['PHONE_NO'] ? $customer_detail['PHONE_NO'] : 'N/A';
$aris_mobile_no = isset($customer_detail['CEL_NO']) && $customer_detail['CEL_NO'] ? $customer_detail['CEL_NO'] : 'N/A';
$aris_email = isset($customer_detail['EMAIL_ADDR']) && $customer_detail['EMAIL_ADDR'] ? $customer_detail['EMAIL_ADDR'] : 'N/A';
$aris_address = isset($customer_detail['ADDRESS']) && $customer_detail['ADDRESS'] ? $customer_detail['ADDRESS'] : 'N/A';
$aris_city = isset($customer_detail['CITY']) && $customer_detail['CITY'] ? $customer_detail['CITY'] : 'N/A';
$aris_province = isset($customer_detail['PROVINCE']) && $customer_detail['PROVINCE'] ? $customer_detail['PROVINCE'] : 'N/A';

$project = isset($aris_project) && $aris_project ? $aris_project : 0;
$project_name = isset($project['ProjectName']) && $project['ProjectName'] ? $project['ProjectName'] : 'N/A';
$project_phone = isset($project['PhoneNumber']) && $project['PhoneNumber'] ? $project['PhoneNumber'] : 'N/A';
$project_fax = isset($project['FaxNumber']) && $project['FaxNumber'] ? $project['PhoneNumber'] : 'N/A';
$project_address = isset($project['Address']) && $project['Address'] ? $project['Address'] : 'N/A';
$project_city = isset($project['City']) && $project['City'] ? $project['City'] : 'N/A';
$project_province = isset($project['Province']) && $project['Province'] ? $project['Province'] : 'N/A';

?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Buyer</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <?php if ($this->ion_auth->is_admin()) : ?>
                    <a href="<?php echo site_url('buyer/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                        <i class="fa fa-edit"></i> Edit
                    </a>
                <?php endif; ?>
                <a href="<?php echo site_url('buyer'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!--begin:: Portlet-->
    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <!-- <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                        <?php echo get_initials($first_name . ' ' . $last_name); ?>
                    </div> -->
                    <?php if (get_image('buyer', 'images', $image)) : ?>
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<?php echo base_url(get_image('buyer', 'images', $image)); ?>" width="110px" height="110px" />
                        </div>
                    <?php else : ?>
                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                            <?php echo get_initials($first_name . ' ' . $last_name); ?>
                        </div>
                    <?php endif; ?>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <span class="kt-widget__username"><?php echo ucwords($company_name); ?></span>
                        </div>

                        <div class="kt-widget__head">
                            <span class="kt-widget__username"><?php echo ucwords($first_name . ' ' . $last_name); ?></span>
                        </div>

                        <div class="kt-widget__desc">
                            <?php echo ucwords($designation); ?>
                        </div>
                    </div>
                    <div class="kt-widget__stats kt-margin-t-20 hide">
                        <div class="kt-widget__icon">
                            <i class="flaticon-piggy-bank"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title kt-font-bold">Total Paid</span><br>
                            <span class="kt-widget__value">P250,000.00</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#basic_information" role="tab" aria-selected="true">
                            <i class="flaticon2-user-outline-symbol"></i> Basic Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#contact_information" role="tab" aria-selected="false">
                            <i class="flaticon-support"></i> Contact Info
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#work_experience" role="tab" aria-selected="false">
                            <i class="flaticon-trophy"></i> Employment Information
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#source_identification" role="tab" aria-selected="false">
                            <i class="flaticon-medal"></i> Source of Identification
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#communication" role="tab" aria-selected="false">
                            <i class="flaticon-mail-1"></i> Email & SMS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#saleslog" role="tab" aria-selected="false">
                            <i class="flaticon-mail-1"></i> Sales Logs
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#aris_detail" role="tab" aria-selected="false">
                            <i class="flaticon2-user-outline-symbol"></i> ARIS Detail
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#audittrail" role="tab" aria-selected="false">
                            <i class="flaticon2-note"></i> Audit
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content kt-margin-t-20">
                <!--Begin:: Tab Content-->
                <div class="tab-pane active" id="basic_information" role="tabpanel-1">
                    <div class="kt-form__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row ">
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Buyer Type:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('buyer_type', $buyer_type_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Last Name:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($last_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Place of Birth:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($birth_place); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">Civil Status:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('civil_status', $civil_status_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col col-form-label text-right">TIN:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $tin; ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">&nbsp;</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">&nbsp;</span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">First Name:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($first_name); ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Birthdate:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $birth_date; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Nationality:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('nationality', $nationality, 'view'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Source Referral:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('source_referral_type', $source_referral, 'view'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">&nbsp;</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder">&nbsp;</span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Middle Name:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($middle_name); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Gender:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?= Dropdown::get_static('sex', $gender, 'view') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Housing Membership:</label>
                                            <div class="col">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('housing_membership', $housing_membership_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="contact_information" role="tabpanel-2">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Mobile:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $mobile_no; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Email:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $email; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Landline:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $landline; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Other Mobile:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $other_mobile; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Present Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($present_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Region:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($region); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Province:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($province); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">City:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($city); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Barangay:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($barangay); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Sitio:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($sitio); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Mailing Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($mailing_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Business Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($business_address); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-4 col-form-label text-right">Social Media Address:</label>
                                            <div class="col-8">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($social_media); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="work_experience" role="tabpanel-3">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row justify-content-center">
                                    <div class="col-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Occupation Type:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('occupation_type', $occupation_type_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Job / Occupation:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('occupation_type', $occupation_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Business / Employer Name:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($employer); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Location:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('occupation_location', $location_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Industry:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo Dropdown::get_static('industry', $industry_id, 'view'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Designation / Title:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($designation); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Gross Salary:</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo $gross_salary; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-xs row">
                                            <label class="col-6 col-form-label text-right">Employment Full Address</label>
                                            <div class="col-6">
                                                <span class="form-control-plaintext kt-font-bolder"><?php echo ucwords($address) ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="source_identification" role="tabpanel-4">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row">
                                    <div class="col-12">
                                        <?php if (!empty($info['identifications'])) :
                                            $count = 1;
                                        ?>
                                            <?php foreach ($info['identifications'] as $ref => $r) : ?>
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2" class="kt-shape-bg-color-1">IDENTIFICATION # <?php echo $count++; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row" style="width: 30%;">Type of ID</th>
                                                            <td><?php echo Dropdown::get_static('ids', $r['type_of_id'], 'view'); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">ID Number</th>
                                                            <td><?php echo ucwords($r['id_number']); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Date Issued</th>
                                                            <td><?php echo date_format(date_create($r['date_issued']), 'F j, Y'); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Date Expiration</th>
                                                            <td><?php echo date_format(date_create($r['date_expiration']), 'F j, Y'); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Place Issued</th>
                                                            <td><?php echo ucwords($r['place_issued']); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">File Uploaded</th>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <?php endforeach; ?>
                                        <?php else : ?>

                                            <div class="kt-portlet kt-callout">
                                                <div class="kt-portlet__body">
                                                    <div class="kt-callout__body">
                                                        <div class="kt-callout__content">
                                                            <h3 class="kt-callout__title">No Records Found</h3>
                                                            <p class="kt-callout__desc">
                                                                Sorry no record were found.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="communication" role="tabpanel-5">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="row row-no-padding row-col-separator-lg">
                                    <div class="col-12">
                                        <span class="kt-section__title">
                                            Email
                                        </span>
                                        <div class="kt-portlet__body">
                                            <div class="kt-widget3">
                                                <?php if ($emails) : ?>
                                                    <?php foreach ($emails as $key => $email) : //vdebug($email) 
                                                    ?>
                                                        <?php
                                                        $buyer_name = @$email['buyer']["first_name"] . " " . @$email['buyer']["middle_name"] . " " . @$email['buyer']["last_name"];
                                                        $date_sent = $email['date_sent'];
                                                        $content = isset($email['content']) && $email['content'] ? $email['content'] : 'N/A';;

                                                        ?>
                                                        <div class="kt-widget3__item">
                                                            <div class="kt-widget3__header">
                                                                <div class="kt-widget3__user-img">
                                                                    <img class="kt-widget3__img" src="http://localhost/rems-dev//assets/media/users/user1.jpg" alt="">
                                                                </div>
                                                                <div class="kt-widget3__info">
                                                                    <a href="#" class="kt-widget3__username">
                                                                        <?= $buyer_name; ?>
                                                                    </a>
                                                                </div>
                                                                <span class="kt-widget3__status kt-font-success">
                                                                    <?= view_date($date_sent); ?>
                                                                </span>
                                                            </div>
                                                            <div class="kt-widget3__body">
                                                                <p class="kt-widget3__text">
                                                                    <?= $content ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php else : ?>
                                                    <h4 class="font-weight-italic text-center">
                                                        No emails found
                                                    </h4>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-no-padding row-col-separator-lg">
                                    <div class="col-12">
                                        <span class="kt-section__title">
                                            SMS
                                        </span>
                                        <div class="kt-portlet__body">
                                            <div class="kt-widget3">
                                                <?php if ($sms) : ?>
                                                    <?php foreach ($sms as $key => $sm) : ?>
                                                        <?php
                                                        $buyer_name = @$email['buyer']["first_name"] . " " . @$email['buyer']["middle_name"] . " " . @$email['buyer']["last_name"];
                                                        $date_sent = $email['date_sent'];
                                                        $content = $email['content'];
                                                        ?>
                                                        <div class="kt-widget3__item">
                                                            <div class="kt-widget3__header">
                                                                <span class="kt-userpic kt-userpic--circle kt-userpic--success">
                                                                    <span><?php echo get_initials(@$email['buyer']['first_name'] . ' ' . @$email['buyer']['last_name']); ?></span>
                                                                </span>
                                                                <div class="kt-widget3__info">
                                                                    <a href="#" class="kt-widget3__username">
                                                                        <?= $buyer_name; ?>
                                                                    </a>
                                                                </div>
                                                                <span class="kt-widget3__status kt-font-success">
                                                                    <?= view_date($date_sent); ?>
                                                                </span>
                                                            </div>
                                                            <div class="kt-widget3__body">
                                                                <p class="kt-widget3__text">
                                                                    <?= $content ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php else : ?>
                                                    <h4 class="font-weight-italic text-center">
                                                        No sms found
                                                    </h4>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="saleslog" role="tabpanel-6">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="kt-portlet__body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="kt_widget5_tab1_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <?php if ($sales_logs) : ?>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <td style="width:20%">Property</td>
                                                                    <td style="width:25%">Reference Number</td>
                                                                    <td style="width:20%">Reservation Date</td>
                                                                    <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                    <td class="kt-align-center" style="width:22%">Buyer</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($sales_logs as $key => $sales_log) : ?>
                                                                    <?php
                                                                    $reference = $sales_log['reference'];
                                                                    $transaction_id = $sales_log['id'];
                                                                    $property_name = $sales_log['property']['name'];
                                                                    $property_id = $sales_log['property']['id'];
                                                                    $reservation_date = $sales_log['reservation_date'];
                                                                    $this->transaction_library->initiate($sales_log['id']);
                                                                    $tpm_p = $this->transaction_library->get_amount_paid(1, 0, 1);
                                                                    $payment_percentage = $tpm_p . "%";
                                                                    $buyer_name = @$sales_log['buyer']["first_name"] . " " . @$sales_log['buyer']["middle_name"] . " " . @$sales_log['buyer']["last_name"];
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="<?php echo base_url("property/view/" . $property_id) ?>"><?= $property_name ?></a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="<?php echo base_url("transaction/view/" . $transaction_id) ?>"><?= $reference ?></a>
                                                                        </td>
                                                                        <td><?= view_date($reservation_date) ?></td>
                                                                        <td class="kt-align-center"><?= $payment_percentage ?></td>
                                                                        <td class="kt-align-center"><?= $buyer_name ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    <?php else : ?>
                                                        <h4 class="font-weight-italic text-center">
                                                            No records found
                                                        </h4>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_widget5_tab2_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td style="width:20%">Property</td>
                                                                <td style="width:25%">Reference Number</td>
                                                                <td style="width:20%">Reservation Date</td>
                                                                <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                <td class="kt-align-center" style="width:22%">Buyer</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_widget5_tab3_content">
                                            <div class="kt-widget5">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td style="width:20%">Property</td>
                                                                <td style="width:25%">Reference Number</td>
                                                                <td style="width:20%">Reservation Date</td>
                                                                <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                                <td class="kt-align-center" style="width:22%">Buyer</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                            <tr>
                                                                <td>0001001001</td>
                                                                <td>CTV111R00001</td>
                                                                <td>August 10, 2019</td>
                                                                <td class="kt-align-center">10%</td>
                                                                <td class="kt-align-center">Pedro Cruz</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->
                <div class="tab-pane" id="aris_detail" role="tabpanel-7">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="kt-portlet__body">
                                    <div class="row row-no-padding row-col-separator-lg">
                                        <div class="col-12">
                                            <span class="kt-section__title">
                                                ARIS Customer Information
                                            </span>
                                            <div class="kt-portlet__body">
                                                <div class="row ">
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Phone Number:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $aris_phone_no ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Mobile Number:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $aris_mobile_no ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Email:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $aris_email ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row ">
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Address:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $aris_address ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">City:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $aris_city ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Province:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $aris_province ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="kt-section__title">
                                                Project Information
                                            </span>
                                            <div class="kt-portlet__body">
                                                <div class="row ">
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Project:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $project_name ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Phone Number:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $project_phone ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Fax number:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $project_fax ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row ">
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Address:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $project_address ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">City:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $project_city ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group form-group-xs row">
                                                            <label class="col col-form-label text-right">Province:</label>
                                                            <div class="col">
                                                                <span class="form-control-plaintext kt-font-bolder"><?= $project_province ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->

                <!--Begin:: Tab Content-->
                <div class="tab-pane" id="audittrail" role="tabpanel-8">
                    <div class="kt-form__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">Audit Trail</h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="table-responsive">
                                        <?php if ($audit) : ?>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <td style="width:10%">ID</td>
                                                        <td style="width:15%">Affected Table</td>
                                                        <td style="width:15%">Action</td>
                                                        <td class="kt-align-center" style="width:22%">Date</td>
                                                        <td class="kt-align-center" style="width:25%">Created By</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (isset($audit) && ($audit)) {
                                                        foreach ($audit as $key => $value) { ?>
                                                            <tr class="clickable" data-toggle="collapse" style="cursor: pointer;" id="row-<?php echo $value['id'] ?>" data-target="#hidden-row-<?php echo $value['id'] ?>">
                                                                <td><?php echo $value['id'] ?></td>
                                                                <td><?php echo $value['affected_table'] ?></td>
                                                                <td><?php echo $value['action'] ?></td>
                                                                <td><?php echo view_date($value['created_at']) ?></td>
                                                                <td><?php echo $value['user']['first_name'] . ' ' . $value['user']['last_name'] ?></td>
                                                            </tr>
                                                            <tr class="collapse" id="hidden-row-<?php echo $value['id'] ?>">
                                                                <td colspan="3">
                                                                    Previous Record: <code><?php echo $value['previous_record'] ?></code>
                                                                </td>
                                                                <td colspan="2">
                                                                    New Record: <code><?php echo $value['new_record'] ?></code>
                                                                </td>
                                                            </tr>
                                                    <?php }
                                                    } ?>
                                                </tbody>
                                            </table>
                                        <?php else : ?>
                                            <h4 class="font-weight-italic text-center">
                                                No records found
                                            </h4>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End:: Tab Content-->
            </div>
        </div>
    </div>

    <div class="row hide">
        <div class="col-xl-12">
            <!--begin:: Widgets/Order Statistics-->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Collections Overview
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body  kt-portlet__body--fit">
                    <div class="row row-no-padding row-col-separator-lg">
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Collected
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Open Receivables
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Total Over Due Collections
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <!--begin::Total Profit-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Collections
                                            <br>
                                            <i>(due next 30 days)</i>
                                        </h4>
                                        <span class="kt-widget24__stats kt-font-brand">
                                            PHP 4,003,566.32
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Total Profit-->
                        </div>
                    </div>
                    <!-- To be Continued -->

                    <div class="row row-no-padding row-col-separator-lg">
                        <div class="col-lg-9">
                            <div id="kt_amcharts_1" style="height: 450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Order Statistics-->
        </div>
    </div>
</div>