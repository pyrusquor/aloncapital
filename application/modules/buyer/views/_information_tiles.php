<div class="row">
    <div class="col-sm col-12">
        <div class="kt-portlet kt-portlet--fluid kt-portlet--solid-success">
            <div class="kt-portlet__head text-white">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-titles d-flex" style="align-items: center;">
                        <span class="kt-portlet__head-icon">
                            <i class="text-white fas fa-check" style="font-size: 20px;"></i>
                        </span>
                        <h4 class="mb-0">
                            COMPLETE
                        </h4>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body text-white">
                <span class="d-block font-weight-bold mb-7 text-dark-50 d-flex" style="align-items: baseline;">
                    <h1 class="mb-0"><?= $complete ?></h1>&nbsp;<h5 class="mb-0">Buyer<?= @$complete > 1 ? 's' : '' ?></h5>
                </span>
            </div>
        </div>
    </div>

    <div class="col-sm col-12">
        <div class="kt-portlet kt-portlet--fluid kt-portlet--solid-danger">
            <div class="kt-portlet__head text-white">
                <div class="kt-portlet__head-label">
                    <div class="kt-portlet__head-titles d-flex" style="align-items: center;">
                        <span class="kt-portlet__head-icon">
                            <i class="text-white fas fa-times" style="font-size: 20px;"></i>
                        </span>
                        <h4 class="mb-0">
                            INCOMPLETE
                        </h4>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body text-white">
                <span class="d-block font-weight-bold mb-7 text-dark-50 d-flex" style="align-items: baseline;">
                    <h1 class="mb-0"><?= $incomplete ?></h1>&nbsp;<h5 class="mb-0">Buyer<?= @$incomplete > 1 ? 's' : '' ?></h5>
                </span>
            </div>
        </div>
    </div>
</div>