<?php
// Work Experience
$occupation_type_id = isset($info['employment']['occupation_type_id']) && $info['employment']['occupation_type_id'] ? $info['employment']['occupation_type_id'] : '';
$industry_id = isset($info['employment']['industry_id']) && $info['employment']['industry_id'] ? $info['employment']['industry_id'] : '';
$occupation_id = isset($info['employment']['occupation_id']) && $info['employment']['occupation_id'] ? $info['employment']['occupation_id'] : '';
$designation = isset($info['employment']['designation']) && $info['employment']['designation'] ? $info['employment']['designation'] : '';
$employer = isset($info['employment']['employer']) && $info['employment']['employer'] ? $info['employment']['employer'] : '';
$gross_salary = isset($info['employment']['gross_salary']) && $info['employment']['gross_salary'] ? $info['employment']['gross_salary'] : '';
$occupation_location = isset($info['employment']['occupation_location']) && $info['employment']['occupation_location'] ? $info['employment']['occupation_location'] : '';
$location_id = isset($info['employment']['location_id']) && $info['employment']['location_id'] ? $info['employment']['location_id'] : '';
$address = isset($info['employment']['address']) && $info['employment']['address'] ? $info['employment']['address'] : '';
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Occupation Type<span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('work_exp[occupation_type_id]', Dropdown::get_static('occupation_type'), set_value('work_exp[occupation_type_id]', @$occupation_type_id), 'class="form-control" id="occupation_type_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Industry</label>
            <?php echo form_dropdown('work_exp[industry_id]', Dropdown::get_static('industry'), set_value('work_exp[industry_id]', @$industry_id), 'class="form-control" id="industry_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Job / Occupation<span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('work_exp[occupation_id]', Dropdown::get_static('occupation'), set_value('work_exp[occupation_id]', @$occupation_id), 'class="form-control" id="occupation_id" required'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Designation / Title</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Designation" name="work_exp[designation]" value="<?php echo set_value('work_exp[designation]"', @$designation); ?>">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square-o"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Business / Employer Name</label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Employer" name="work_exp[employer]" value="<?php echo set_value('work_exp[employer]"', @$employer); ?>">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square-o"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Gross Salary<span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Gross Salary" name="work_exp[gross_salary]" value="<?php echo set_value('work_exp[gross_salary]"', @$gross_salary); ?>" required>
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-pencil-square-o"></i></span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Location<span class="kt-font-danger">*</span></label>
            <?php echo form_dropdown('work_exp[location_id]', Dropdown::get_static('occupation_location'), set_value('work_exp[location_id]', @$location_id), 'class="form-control" id="location_id" required'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Full Address:</label>
            <textarea class="form-control" id="address" rows="3" spellcheck="false" name="work_exp[address]"><?php echo set_value('info[address]"', @$address); ?></textarea>
        </div>
    </div>

</div>