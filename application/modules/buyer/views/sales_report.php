<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Buyer Sales Report</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button>
                <a href="<?php echo base_url('buyer'); ?>" class="btn btn-label-instagram">
                    Back</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- Form needs 3 filters
Company - suggests
Project - suggests
House Model - suggests-->

<div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
    <div class="row">
        <div class="col-lg-12">
            <form class="kt-form">
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label class="form-control-label">Company:</label>
                            <select class="form-control suggests" data-module="companies" id="company_id">
                                <option value="">ALL Companies</option>
                            </select>
                    </div>
                    <div class="col-sm-3">
                        <label class="form-control-label">Project:</label>
                            <select class="form-control suggests" data-module="projects" id="project_id">
                                <option value="">ALL Projects</option>
                            </select>
                    </div>
                    <div class="col-sm-3">
                        <label class="form-control-label">House Model:</label>
                            <select class="form-control suggests" data-module="house_models" id="house_model_id">
                                <option value="">ALL House Models</option>
                            </select>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--mobile">
            <!--begin: Datatable -->
            <table class="table table-striped table-bordered table-hover" id="buyer_sales_report_table">
                <thead>
                    <tr>
                        <th class='text-center'>Buyer</th>
                        <th class='text-center'>Reference</th>
                        <th class='text-center'>Property</th>
                        <th class='text-center'>House Model</th>
                        <th class='text-center'>Reservation Date</th>
                        <th class='text-center'>Total Collectible Price</th>
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>