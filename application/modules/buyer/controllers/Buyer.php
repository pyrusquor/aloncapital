<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Buyer extends MY_Controller
{
    private $fields = [
        array(
            'field' => 'info[type_id]',
            'label' => 'Buyer Type',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'info[last_name]',
            'label' => 'Last Name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'info[first_name]',
            'label' => 'First Name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'info[birth_place]',
            'label' => 'Birth Place',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'info[birth_date]',
            'label' => 'Birth Date',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'info[gender]',
            'label' => 'Gender',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'info[email]',
            'label' => 'Email',
            'rules' => 'trim|required|is_unique[users.email]'
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        // Load models
        $this->load->model('Buyer_model', 'M_buyer');
        $this->load->model('buyer/Buyer_identification_model', 'M_buyer_identification');
        $this->load->model('buyer/Buyer_employment_model', 'M_buyer_employment');
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        $this->load->model('locations/Address_countries_model', 'M_country');
        $this->load->model('locations/Address_regions_model', 'M_region');
        $this->load->model('locations/Address_provinces_model', 'M_province');
        $this->load->model('locations/Address_city_municipalities_model', 'M_city');
        $this->load->model('locations/Address_barangays_model', 'M_barangay');
        $this->load->model('transaction/Transaction_model', 'M_transaction');
        $this->load->model('communication/Communication_model', 'M_coms');
        $this->load->model('aris/Aris_model', 'M_aris');
        $this->load->model('aris/Aris_customer_model', 'M_aris_customer');

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_buyer->fillable;
        $this->_table_columns = $this->M_buyer->__get_columns();
        $this->_table = 'buyers';

        $this->view_data['countries'] = $this->M_country->as_array()->get_all();
        $this->view_data['regions'] = $this->M_region->as_array()->get_all();
        $this->view_data['provinces'] = $this->M_province->as_array()->get_all();
        $this->view_data['cities'] = $this->M_city->as_array()->get_all();
        $this->view_data['barangays'] = $this->M_barangay->as_array()->get_all();
        $this->load->helper('dompdf_helper');
    }

    public function get_all()
    {
        $data['row'] = $this->M_buyer->fields(array('id', 'first_name', 'last_name', 'mobile_no', 'email'))->get_all();

        echo json_encode($data);
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        // Get record count
        // $conditions['returnType'] = 'count';
        // $this->view_data['totalRec'] = $totalRec = $this->M_buyer->count_rows();

        // Pagination configuration
        // $config['target'] = '#buyerContent';
        // $config['base_url'] = base_url('buyer/paginationData');
        // $config['total_rows'] = $totalRec;
        // $config['per_page'] = $this->perPage;
        // $config['link_func'] = 'BuyerPagination';

        // Initialize pagination library
        // $this->ajax_pagination->initialize($config);

        // Get records
        $this->view_data['records'] = $this->M_buyer->fields(array('id', 'first_name', 'last_name', 'mobile_no', 'email', 'image', 'present_address', 'aris_id'))
            ->with_employment('fields: designation,employer')
            ->limit($this->perPage, 0)
            ->order_by('id', 'DESC')
            ->get_all();

        // Get complete/incomplete count
        $this->db->select('COUNT(buyers.id) as complete')
            ->from('buyers')
            ->where('buyers.deleted_at', NULL)
            ->join('buyer_employment_table', 'buyer_employment_table.buyer_id = buyers.id', 'left')
            ->join('buyer_identifications', 'buyer_identifications.buyer_id = buyers.id', 'left');

        $buyer_fields = $this->M_buyer->fillable;

        foreach ($buyer_fields as $key => $value) {

            if ($value == 'created_by' || $value == 'created_at' || $value == 'updated_by' || $value == 'updated_at') {
                continue;
            }

            $this->db->where($value . ' !=', NULL);
        }

        $employment_fields = $this->M_buyer_employment->fillable;

        foreach ($employment_fields as $key => $value) {

            if ($value == 'buyer_id' || $value == 'created_by' || $value == 'created_at' || $value == 'updated_by' || $value == 'updated_at') {
                continue;
            }

            $this->db->where($value . ' !=', NULL);
        }

        $identification_fields = $this->M_buyer_identification->fillable;

        foreach ($identification_fields as $key => $value) {

            if ($value == 'buyer_id' || $value == 'created_by' || $value == 'created_at' || $value == 'updated_by' || $value == 'updated_at') {
                continue;
            }

            $this->db->where($value . ' !=', NULL);
        }

        $query = $this->db->get()->row_array();

        $this->view_data['complete'] = $query['complete'];

        $this->db->select('COUNT(id) as count')
            ->from('buyers')
            ->where('deleted_at', NULL);

        $query = $this->db->get()->row_array();

        $this->view_data['incomplete'] = $query['count'] - $this->view_data['complete'];

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'first_name' => true,
            'employer' => true,
            'mobile_no' => true,
            'email' => true,
            'present_address' => true,
            'last_login_time' => true,
            'created_by' => true,
            'updated_by' => true
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);
        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];
        for ($query_loop = 0; $query_loop < 2; $query_loop++) {
            $query = $this->M_buyer
                ->with_employment('fields:employer')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%'");
                $query->or_where("first_name like '%$searchValue%'");
                $query->or_where("last_name like '%$searchValue%'");
                $query->or_where("mobile_no like '%$searchValue%'");
                $query->or_where("email like '%$searchValue%'");
                $query->or_where("present_address like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'id' => [
                    'data' => $filters['buyer_id'] ?? null,
                    'operator' => '=',
                ],
                'mobile_no' => [
                    'data' => $filters['mobile_no'] ?? null,
                    'operator' => 'like',
                ],
                'email' => [
                    'data' => $filters['email'] ?? null,
                    'operator' => 'like',
                ],
                'present_address' => [
                    'data' => $filters['present_address'] ?? null,
                    'operator' => 'like',
                ],
                'type_id' => [
                    'data' => $filters['type_id'] ?? null,
                    'operator' => '=',
                ],
                'gender' => [
                    'data' => $filters['gender_id'] ?? null,
                    'operator' => '=',
                ],
                'civil_status_id' => [
                    'data' => $filters['civil_status'] ?? null,
                    'operator' => '=',
                ],
                'nationality' => [
                    'data' => $filters['nationality'] ?? null,
                    'operator' => '=',
                ],
                'country_id' => [
                    'data' => $filters['country_id'] ?? null,
                    'operator' => '=',
                ],
                'regCode' => [
                    'data' => $filters['region_id'] ?? null,
                    'operator' => '=',
                ],
                'provCode' => [
                    'data' => $filters['province_id'] ?? null,
                    'operator' => '=',
                ],
                'citymunCode' => [
                    'data' => $filters['city_id'] ?? null,
                    'operator' => '=',
                ],
                'brgyCode' => [
                    'data' => $filters['brgy_id'] ?? null,
                    'operator' => '=',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } elseif ($key == 'project_id') {
                        $this->db->where('id in (SELECT buyer_id FROM transactions WHERE project_id=' . $value['data'] . ')');
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {
                    // Transform data
                    foreach ($items as $key => $value) {
                        $latest_login = $this->M_user->get_last_login_time($value['user_id']);
                        $items[$key]['employer'] = isset($entry['employment']) ? $entry['employment']['employer'] : 'N/A';
                        $items[$key]['first_name'] = get_fname($value) ?? '';
                        $items[$key]['last_login_time'] = $latest_login ? $latest_login : 'No Record Found';

                        $items[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

                        $items[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_buyer->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
        );

        echo json_encode($response);
        exit();
    }

    public function paginationData()
    {
        if ($this->input->is_ajax_request()) {

            // Input from General Search
            $keyword = $this->input->post('keyword');

            // Input from Advanced Filter
            $name = $this->input->post('name');
            $buyer_type_id = $this->input->post('buyer_type_id');
            $occupation_type_id = $this->input->post('occupation_type_id');
            $occupation_location_id = $this->input->post('occupation_location_id');
            $civil_status_id = $this->input->post('civil_status_id');

            $page = $this->input->post('page');

            if (!$page) {
                $offset = 0;
            } else {
                $offset = $page;
            }

            $totalRec = $this->M_buyer->with_employment()->count_rows();
            $where = array();

            // Pagination configuration
            $config['target'] = '#buyerContent';
            $config['base_url'] = base_url('buyer/paginationData');
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'BuyerPagination';

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('last_name', $keyword, 'both');
                $this->db->or_like('first_name', $keyword, 'both');
                $this->db->or_like('middle_name', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($name)) :
                $this->db->group_start();
                $this->db->like('last_name', $name, 'both');
                $this->db->or_like('first_name', $name, 'both');
                $this->db->or_like('middle_name', $name, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_type_id) && !empty($buyer_type_id)) :
                $this->db->where('type_id', $buyer_type_id);
            endif;

            // if ( ! empty($occupation_type_id) && ! empty($occupation_type_id) ):
            // 	$this->db->where('occupation_type_id',$occupation_type_id);
            // endif;

            // if ( ! empty($occupation_location_id) && ! empty($occupation_location_id) ):
            // 	$this->db->where('location_id',$occupation_location_id);
            // endif;

            if (!empty($civil_status_id) && !empty($civil_status_id)) :
                $this->db->where('civil_status_id', $civil_status_id);
            endif;

            $totalRec = $this->M_buyer->count_rows();

            // Pagination configuration
            $config['total_rows'] = $totalRec;

            // Initialize pagination library
            $this->ajax_pagination->initialize($config);

            // Query
            if (!empty($keyword)) :
                $this->db->group_start();
                $this->db->like('last_name', $keyword, 'both');
                $this->db->or_like('first_name', $keyword, 'both');
                $this->db->or_like('middle_name', $keyword, 'both');
                $this->db->group_end();
            endif;

            if (!empty($name)) :
                $this->db->group_start();
                $this->db->like('last_name', $name, 'both');
                $this->db->or_like('first_name', $name, 'both');
                $this->db->or_like('middle_name', $name, 'both');
                $this->db->group_end();
            endif;

            if (!empty($buyer_type_id) && !empty($buyer_type_id)) :
                $this->db->where('type_id', $buyer_type_id);
                $where['type_id'] = $buyer_type_id;
            endif;

            /*if ( ! empty($occupation_type_id) && ! empty($occupation_type_id) ):
                $this->db->where('occupation_type_id',$occupation_type_id); $where['occupation_type_id'] = $occupation_type_id;
            endif;

            if ( ! empty($occupation_location_id) && ! empty($occupation_location_id) ):
                $this->db->where('location_id',$occupation_location_id); $where['location_id'] = $location_id;
            endif;*/

            if (!empty($civil_status_id) && !empty($civil_status_id)) :
                $this->db->where('civil_status_id', $civil_status_id);
                $where['civil_status_id'] = $civil_status_id;
            endif;

            $this->view_data['records'] = $records = $this->M_buyer->fields(array('id', 'first_name', 'last_name', 'mobile_no', 'email', 'image', 'present_address', 'aris_id'))
                ->with_employment('fields: designation')
                ->limit($this->perPage, $offset)
                ->get_all();


            $this->load->view('buyer/_filter', $this->view_data, false);
        }
    }

    public function view($id = FALSE)
    {
        $this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

        $this->js_loader->queue([
            '//www.amcharts.com/lib/3/amcharts.js',
            '//www.amcharts.com/lib/3/serial.js',
            '//www.amcharts.com/lib/3/radar.js',
            '//www.amcharts.com/lib/3/pie.js',
            '//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
            '//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
            '//www.amcharts.com/lib/3/plugins/export/export.min.js',
            '//www.amcharts.com/lib/3/themes/light.js'
        ]);

        if ($id) {

            $this->view_data['info'] = $info = $this->M_buyer
                ->with_region()
                ->with_province()
                ->with_city()
                ->with_barangay()
                ->with_employment()
                ->with_identifications()
                ->get($id);

            $this->view_data['audit'] = $this->__get_audit($id);

            $this->view_data['sales_logs'] = $this->M_transaction->with_buyer()->with_property()->where("buyer_id", $id)->get_all();

            $this->view_data['customer_detail'] = $customer = $this->M_aris_customer->where('id', $info['aris_id'])->get();

            $this->view_data['aris_project'] = $this->M_aris->where('id', $customer['project_id'])->get();

            $coms_params['recipient_id'] = $id;
            $coms_params["recipient_table"] = "buyer";

            $communications = $this->M_coms->with_buyer()->where($coms_params)->get_all();

            $emails = array();
            $sms = array();

            if ($communications) {
                foreach ($communications as $key => $comm) {
                    if ($comm["medium_type"] == "sms") {
                        array_push($sms, $comm);
                    } else if ($comm["medium_type"] == "email") {
                        array_push($emails, $comm);
                    }
                }
            }

            $this->view_data['emails'] = $emails;
            $this->view_data['sms'] = $sms;

            if ($this->view_data['info']) {
                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function aris($id = FALSE)
    {
        $this->css_loader->queue('//www.amcharts.com/lib/3/plugins/export/export.css');

        $this->js_loader->queue([
            '//www.amcharts.com/lib/3/amcharts.js',
            '//www.amcharts.com/lib/3/serial.js',
            '//www.amcharts.com/lib/3/radar.js',
            '//www.amcharts.com/lib/3/pie.js',
            '//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.jss',
            '//www.amcharts.com/lib/3/plugins/animate/animate.min.js',
            '//www.amcharts.com/lib/3/plugins/export/export.min.js',
            '//www.amcharts.com/lib/3/themes/light.js'
        ]);

        if ($id) {

            $this->view_data['info'] = $info = $this->M_buyer
                ->with_region()
                ->with_province()
                ->with_city()
                ->with_barangay()
                ->with_employment()
                ->with_identifications()
                ->where('aris_id', $id)
                ->get();

            $this->view_data['audit'] = $this->__get_audit($id);

            $this->view_data['sales_logs'] = $this->M_transaction->with_buyer()->with_property()->where("buyer_id", $info['id'])->get_all();

            $this->view_data['customer_detail'] = $customer = $this->M_aris_customer->where('id', $id)->get();

            $this->view_data['aris_project'] = $this->M_aris->where('id', $customer['project_id'])->get();

            $coms_params['recipient_id'] = $info['id'];
            $coms_params["recipient_table"] = "buyer";

            $communications = $this->M_coms->with_buyer()->where($coms_params)->get_all();

            $emails = array();
            $sms = array();

            if ($communications) {
                foreach ($communications as $key => $comm) {
                    if ($comm["medium_type"] == "sms") {
                        array_push($sms, $comm);
                    } else if ($comm["medium_type"] == "email") {
                        array_push($emails, $comm);
                    }
                }
            }

            $this->view_data['emails'] = $emails;
            $this->view_data['sms'] = $sms;

            if ($this->view_data['info']) {
                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function create()
    {
        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->fields);
            if ($this->form_validation->run() === TRUE) {
                $post = $this->input->post();
                $buyer_info = $post['info'];
                $buyer_employment = $post['work_exp'];

                if (!$this->M_auth->email_check($buyer_info['email'])) {

                    $buyerPwd = password_format($buyer_info['last_name'], $buyer_info['birth_date']);
                    $buyerEmail = $buyer_info['email'];

                    // Buyer Group ID
                    $buyerGroupID = ['4'];

                    $buyerAccAuth = array(
                        'first_name' => $buyer_info['first_name'],
                        'last_name' => $buyer_info['last_name'],
                        'active' => 1,
                        'created_by' => $this->user->id,
                        'created_at' => NOW
                    );

                    $userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $buyerAccAuth, $buyerGroupID);

                    if ($userID !== FALSE) {
                        // Image Upload
                        $upload_path = './assets/uploads/buyer/images';
                        $config = array();
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = 5000;
                        $config['encrypt_name'] = TRUE;
                        $this->load->library('upload', $config, 'buyer_image');
                        $this->buyer_image->initialize($config);

                        if (!empty($_FILES['buyer_image']['name'])) {

                            if (!$this->buyer_image->do_upload('buyer_image')) {

                                $this->notify->error($this->buyer_image->display_errors(), 'buyer/create');
                            } else {

                                $img_data = $this->buyer_image->data();

                                $buyer_info['image'] = $img_data['file_name'];
                            }
                        }

                        $additional = [
                            'is_active' => 1,
                            'user_id' => $userID,
                            'created_by' => $this->user->id,
                            'created_at' => NOW
                        ];
                        $buyerID = $this->M_buyer->insert($buyer_info + $additional);

                        if ($buyerID !== FALSE) {

                            // Insert Buyer Employment Info
                            $buyer_employment['buyer_id'] = $buyerID;
                            $this->M_buyer_employment->insert($buyer_employment);

                            $refFields = array('type_of_id', 'id_number', 'date_issued', 'date_expiration', 'place_issued');

                            $refData = [];

                            foreach ($refFields as $field) {
                                foreach ($this->input->post($field) as $key => $value) {
                                    if (!empty($value)) {
                                        $refData[$key]['buyer_id'] = $buyerID;
                                        $refData[$key][$field] = $value;
                                    }
                                }
                            }

                            if ($refData) {
                                $this->M_buyer_identification->insert($refData);
                            }

                            // Start emailing email and password to buyer here

                            $response['status'] = 1;
                            $response['message'] = 'Buyer Account Successfully Created!';
                        }
                    }
                } else {
                    $response['status'] = 0;
                    $response['message'] = 'Oops! Email Address already exists.';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        $this->template->build('create', $this->view_data);
    }

    public function update($id = FALSE)
    {
        if ($id) {

            $this->view_data['info'] = $data = $this->M_buyer
                ->with_employment()
                ->with_identifications()
                ->get($id);

            // vdebug($data);

            if ($data) {

                $response['status'] = 0;
                $response['msg'] = 'Oops! Please refresh the page and try again.';

                if ($this->input->post()) {

                    $post = $this->input->post();


                    $buyer_info = $post['info'];
                    $buyer_employment = $post['work_exp'];

                    // $email_exist = $this->rolekey_exists("buyers", "email", $buyer_info['email']);
                    $email_exist = false;


                    if (!$email_exist) {

                        $oldPwd = password_format($data['last_name'], $data['birth_date']);
                        $newPwd = password_format($buyer_info['last_name'], $buyer_info['birth_date']);

                        if ($oldPwd !== $newPwd) {
                            // Update User Password
                            $this->M_auth->change_password($buyer_info['email'], $oldPwd, $newPwd);
                        }

                        $oldEmail = $data['email'];
                        $newEmail = $buyer_info['email'];

                        if ($oldEmail !== $newEmail) {
                            // Update User Email
                            $_user_data = [
                                'email' => $newEmail,
                                'username' => $newEmail,
                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
                                'updated_at' => NOW
                            ];
                            $this->M_user->update($_user_data, array('id' => $data['user_id']));
                        }

                        // Update Buyer Info
                        $additional = [
                            'updated_by' => $this->user->id,
                            'updated_at' => NOW
                        ];

                        $upload_path = './assets/uploads/buyer/images';
                        $config = array();
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = 5000;
                        $config['encrypt_name'] = TRUE;
                        $this->load->library('upload', $config, 'buyer_image');
                        $this->buyer_image->initialize($config);

                        if (!empty($_FILES['buyer_image']['name'])) {
                            if (!$this->buyer_image->do_upload('buyer_image')) {

                                $this->notify->error($this->buyer_image->display_errors(), 'buyer/update');
                            } else {

                                if (file_exists($upload_path . '/' . $data['image'])) {

                                    unlink($upload_path . '/' . $data['image']);
                                }

                                $img_data = $this->buyer_image->data();

                                $buyer_info['image'] = $img_data['file_name'];
                            }
                        } else {

                            $buyer_info['image'] = $data['image'];
                        }

                        $update = $this->M_buyer->update($buyer_info + $additional, $data['id']);

                        if ($update !== FALSE) {
                            $user_data = array(
                                'first_name' => $buyer_info['first_name'],
                                'last_name' => $buyer_info['last_name'],
                                'active' => $data['is_active'],
                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
                                'updated_at' => NOW
                            );

                            $this->M_user->update($user_data, $data['user_id']);

                            $e_buyer_employment = $this->M_buyer_employment->get(array('buyer_id' => $data['id']));

                            // Update Buyer Source of Info
                            if (!empty($buyer_employment) && !empty($e_buyer_employment)) {
                                $this->M_buyer_employment->update($buyer_employment, array('buyer_id' => $data['id']));
                            } else {
                                $buyer_employment['buyer_id'] = $data['id'];
                                $this->M_buyer_employment->insert($buyer_employment);
                            }

                            // Delete all Reference of buyer and Add again
                            $this->M_buyer_identification->delete(array('buyer_id' => $data['id']));

                            // Insert Reference
                            $refFields = array('type_of_id', 'id_number', 'date_issued', 'date_expiration', 'place_issued');
                            $refData = [];

                            foreach ($refFields as $field) {
                                foreach ($this->input->post($field) as $key => $value) {
                                    if (!empty($value)) {
                                        $refData[$key]['buyer_id'] = $data['id'];
                                        $refData[$key][$field] = $value;
                                    }
                                }
                            }
                            if ($refData) {
                                $this->M_buyer_identification->insert($refData);
                            }

                            // Start emailing email and password to buyer here


                            $response['status'] = 1;
                            $response['message'] = 'Buyer Account Successfully Update!';
                        } else {

                            $response['status'] = 0;
                            $response['message'] = 'Oops! Something went wrong. Please refresh the page and try again.';
                        }
                    } else {

                        $response['status'] = 0;
                        $response['message'] = 'Oops! Email Address already exists.';
                    }

                    echo json_encode($response);
                    exit();
                }

                $this->template->build('update', $this->view_data);
            } else {

                show_404();
            }
        } else {

            show_404();
        }
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_buyer->get($id);
            if ($list) {

                $deleted = $this->M_buyer->delete($list['id']);
                if ($deleted !== FALSE) {

                    $response['status'] = 1;
                    $response['message'] = 'Buyer successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_buyer->delete($value);
                }
                if ($deleted !== FALSE) {

                    $response['status'] = 1;
                    $response['message'] = 'Buyer successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    function checkMail()
    {
        if ($this->input->is_ajax_request()) {

            $response = FALSE;
            $key = $this->input->post('info[key]');
            $email = $this->input->post('info[email]');

            if (!empty($key)) {
                $existing_email = $this->M_user->where('email =', $key)->get();

                if ($email == $existing_email['email']) {
                    $response = TRUE;
                } else {
                    $check = $this->M_user->where('email', $email)->get();
                    // Email doesnt exists / or not yet taken
                    if ($check == FALSE) {
                        $response = TRUE;
                    }
                }
            } else {

                $check = $this->M_user->where('email', $email)->get();
                // Email doesnt exists / or not yet taken
                if ($check == FALSE) {
                    $response = TRUE;
                }
            }
            echo json_encode($response);
        } else {

            show_404();
        }
    }

    function fetchBuyerPositions()
    {
        if ($this->input->is_ajax_request()) {
            $items = [];
            $sales_group_id = $this->input->post('id');

            $datas = $this->M_buyer_position->fields('id, name')->where('sales_group_id', $sales_group_id)->as_object()->get_all();

            if ($datas) {
                foreach ($datas as $key => $data) {

                    $items[$data->id]['id'] = $data->id;
                    $items[$data->id]['name'] = $data->name;
                }

                echo json_encode($items);
                exit();
            }
        } else {

            show_404();
        }
    }

    public function export()
    {

        $_db_columns = [];
        $_alphas = [];
        $_datas = [];

        $_titles[] = '#';

        $_start = 3;
        $_row = 2;
        $_no = 1;

        $buyers = $this->M_buyer
            ->with_employment()
            ->with_identifications()
            ->get_all();

        if ($buyers) {
            foreach ($buyers as $_lkey => $buyer) {

                $_datas[$buyer['id']]['#'] = $_no;

                $_no++;
            }

            $_filename = 'list_of_buyers_' . date('m_d_y_h-i-s', time()) . '.xls';

            $_objSheet = $this->excel->getActiveSheet();

            if ($this->input->post()) {

                $_export_column = $this->input->post('_export_column');

                if ($_export_column) {

                    foreach ($_export_column as $_ekey => $_column) {

                        $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
                    }
                } else {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
                }
            } else {

                $_filename = 'list_of_buyers_' . date('m_d_y_h-i-s', time()) . '.csv';

                // $_db_columns    =    $this->M_land_inventory->fillable;
                $_db_columns = $this->_table_fillables;
                if (!$_db_columns) {

                    $this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
                }
            }

            if ($_db_columns) {

                $additional = [
                    'occupation_type_id',
                    'industry_id',
                    'occupation_id',
                    'designation',
                    'employer',
                    'gross_salary',
                    'location_id',
                    'address'
                ];

                $_db_columns = array_merge($_db_columns, $additional);

                // vdebug($_db_columns);

                foreach ($_db_columns as $key => $_dbclm) {

                    $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

                    if ((strpos($_name, 'created_') === false) && (strpos($_name, 'updated_') === false) && (strpos($_name, 'deleted_') === false) && ($_name !== 'id')) {

                        if ((strpos($_name, '_id') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($buyers as $_lkey => $buyer) {

                                // vdebug($buyer);

                                // columns with _id on name
                                if ($_column === 'type_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('buyer_type', $buyer[$_column], 'view') : '';
                                } else if ($_column === 'civil_status_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('civil_status', $buyer[$_column], 'view') : '';
                                } else if ($_column === 'housing_membership_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('housing_membership', $buyer[$_column], 'view') : '';
                                } else if ($_column === 'country_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? get_value_field($buyer[$_column], 'address_countries', 'name') : '';
                                } else if ($_column === 'occupation_type_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer['employment'][$_column]) && $buyer['employment'][$_column] ? Dropdown::get_static('occupation_type', $buyer['employment'][$_column], 'view') : '';
                                } else if ($_column === 'industry_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer['employment'][$_column]) && $buyer['employment'][$_column] ? Dropdown::get_static('industry', $buyer['employment'][$_column], 'view') : '';
                                } else if ($_column === 'occupation_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer['employment'][$_column]) && $buyer['employment'][$_column] ? Dropdown::get_static('occupation_type', $buyer['employment'][$_column], 'view') : '';
                                } else if ($_column === 'location_id') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer['employment'][$_column]) && $buyer['employment'][$_column] ? Dropdown::get_static('occupation_location', $buyer['employment'][$_column], 'view') : '';
                                } else {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? $buyer[$_name] : '';
                                }
                            }
                        } elseif ((strpos($_name, 'is_') !== false)) {

                            $_column = $_name;

                            $_name = isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($buyers as $_lkey => $buyer) {

                                $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && ($buyer[$_column] !== '') ? Dropdown::get_static('bool', $buyer[$_column], 'view') : '';
                            }
                        } else {

                            $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

                            foreach ($buyers as $_lkey => $buyer) {

                                // columns without _id on name but has id value
                                if ($_name === 'status') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? Dropdown::get_static('inventory_status', $buyer[$_name], 'view') : '';
                                } else if ($_name === 'gender') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? Dropdown::get_static('sex', $buyer[$_name], 'view') : '';
                                } else if ($_name === 'nationality') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? Dropdown::get_static('nationality', $buyer[$_name], 'view') : '';
                                } else if ($_name === 'housing_membership') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? Dropdown::get_static('housing_membership', $buyer[$_name], 'view') : '';
                                } else if ($_name === 'regCode') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? get_value_field($buyer[$_name], 'address_regions', 'regDesc', 'regCode') : '';
                                } else if ($_name === 'provCode') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? get_value_field($buyer[$_name], 'address_provinces', 'provDesc', 'provCode') : '';
                                } else if ($_name === 'citymunCode') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? get_value_field($buyer[$_name], 'address_city_municipalities', 'citymunDesc', 'citymunCode') : '';
                                } else if ($_name === 'brgyCode') {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? get_value_field($buyer[$_name], 'address_barangays', 'brgyDesc', 'brgyCode') : '';
                                } else if ($_name === 'designation') {

                                    $_datas[$buyer['id']][$_title] = $buyer['employment'][$_name] ?? '';
                                } else if ($_name === 'employer') {

                                    $_datas[$buyer['id']][$_title] = $buyer['employment'][$_name] ?? '';
                                } else if ($_name === 'gross_salary') {

                                    $_datas[$buyer['id']][$_title] = $buyer['employment'][$_name] ?? '';
                                } else if ($_name === 'address') {

                                    $_datas[$buyer['id']][$_title] = $buyer['employment'][$_name] ?? '';
                                } else {

                                    $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? $buyer[$_name] : '';
                                }
                            }
                        }
                    } else {

                        continue;
                    }
                }

                $_alphas = $this->__get_excel_columns(count($_titles));

                // vdebug($_alphas);

                $_xls_columns = array_combine($_alphas, $_titles);
                $_firstAlpha = reset($_alphas);
                $_lastAlpha = end($_alphas);

                foreach ($_xls_columns as $_xkey => $_column) {

                    $_title = ($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

                    $_objSheet->setCellValue($_xkey . $_row, $_title);
                }

                $_objSheet->setTitle('List of Buyers Class');
                $_objSheet->setCellValue('A1', 'LIST OF BUYERS');
                $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => false,
                            'size' => 9,
                            'name' => 'Verdana',
                        ),
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                foreach ($_alphas as $_alpha) {

                    $_objSheet->getColumnDimension($_alpha)->setAutoSize(true);
                }

                $_style = array(
                    'font' => array(
                        'bold' => true,
                        'size' => 10,
                        'name' => 'Verdana',
                    ),
                );
                $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $_filename . '"');
                header('Cache-Control: max-age=0');
                $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                @ob_end_clean();
                $_objWriter->save('php://output');
                @$_objSheet->disconnectWorksheets();
                unset($_objSheet);
            } else {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
            }
        } else {

            $this->notify->error('No Record Found', 'buyer');
        }
    }

    // function export()
    // {

    //     $_db_columns = [];
    //     $_alphas = [];
    //     $_datas = [];
    //     $_extra_datas = [];
    //     $_adatas = [];

    //     $_titles[] = '#';

    //     $_start = 3;
    //     $_row = 2;
    //     $_no = 1;

    //     $buyers = $this->M_buyer
    //         ->with_employment()
    //         ->with_identifications()
    //         ->get_all();

    //     // vdebug($buyers);

    //     if ($buyers) {

    //         foreach ($buyers as $skey => $buyer) {

    //             $_datas[$buyer['id']]['#'] = $_no;

    //             $_no++;
    //         }


    //         $_filename = 'list_of_buyers' . date('m_d_y_h-i-s', time()) . '.xls';

    //         $_style = array(
    //             'font' => array(
    //                 'bold' => TRUE,
    //                 'size' => 10,
    //                 'name' => 'Verdana'
    //             )
    //         );

    //         $_objSheet = $this->excel->getActiveSheet();

    //         if ($this->input->post()) {

    //             $_export_column = $this->input->post('_export_column');
    //             $_additional_column = $this->input->post('_additional_column');

    //             if ($_export_column) {
    //                 foreach ($_export_column as $_ekey => $_column) {

    //                     $_db_columns[$_ekey] = isset($_column) && $_column ? $_column : '';
    //                 }
    //             } else {

    //                 $this->notify->error('Something went wrong. Please refresh the page and try again.', 'document');
    //             }
    //         }

    //         if ($_db_columns) {

    //             foreach ($_db_columns as $key => $_dbclm) {

    //                 $_name = isset($_dbclm) && $_dbclm ? $_dbclm : '';

    //                 if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id') && ($_name !== 'user_id')) {

    //                     if ((strpos($_name, 'employment') !== FALSE)) {

    //                         $_extra_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

    //                         foreach ($buyers as $skey => $buyer) {

    //                             $_extra_datas[$buyer['id']][$_title][0] = isset($buyer[$_name]) && $buyer[$_name] ? $buyer[$_name] : '';
    //                         }
    //                     } elseif ((strpos($_name, 'identifications') !== FALSE)) {
    //                         $_extra_titles[] = $_title = isset($_name) && $_name ? $_name : '';

    //                         foreach ($buyers as $skey => $buyer) {

    //                             $_extra_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? $buyer[$_name] : '';
    //                         }
    //                     } else {

    //                         $_column = $_name;

    //                         $_name = isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

    //                         $_titles[] = $_title = isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

    //                         foreach ($buyers as $skey => $buyer) {

    //                             if ($_column === 'birth_date') {

    //                                 $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] && (strtotime($buyer[$_column]) > 0) ? date_format(date_create($buyer[$_column]), 'm/d/Y') : '';

    //                             } elseif ($_column === 'gender') {

    //                                 $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('sex', $buyer[$_column], 'view') : '';

    //                             } elseif ($_column === 'nationality') {

    //                                 $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('nationality', $buyer[$_column], 'view') : '';

    //                             } elseif ($_column === 'civil_status_id') {

    //                                 $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('civil_status', $buyer[$_column], 'view') : '';

    //                             } elseif ($_column === 'housing_membership_id') {

    //                                 $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('housing_membership', $buyer[$_column], 'view') : '';

    //                             } elseif ($_column === 'is_active') {

    //                                 $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('bool', $buyer[$_column], 'view') : '';

    //                             } elseif ($_column === 'type_id') {

    //                                 $_datas[$buyer['id']][$_title] = isset($buyer[$_column]) && $buyer[$_column] ? Dropdown::get_static('buyer_type', $buyer[$_column], 'view') : '';

    //                             } else {
    //                                 $_datas[$buyer['id']][$_title] = isset($buyer[$_name]) && $buyer[$_name] ? $buyer[$_name] : '';
    //                             }

    //                         }
    //                     }
    //                 } else {

    //                     continue;
    //                 }
    //             }

    //             $_alphas = $this->__get_excel_columns(count($_titles));

    //             $_xls_columns = array_combine($_alphas, $_titles);
    //             $_lastAlpha = end($_alphas);

    //             if (empty($_extra_datas)) {
    //                 foreach ($_xls_columns as $_xkey => $_column) {

    //                     $_title = ucwords(strtolower($_column));

    //                     $_objSheet->setCellValue($_xkey . $_row, $_title);
    //                     $_objSheet->getStyle($_xkey . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    //                 }
    //             }

    //             $_objSheet->setTitle('List of Buyers');
    //             $_objSheet->setCellValue('A1', 'LIST OF BUYERS');
    //             $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

    //             $col = 1;

    //             foreach ($buyers as $key => $buyer) {

    //                 $buyer_id = isset($buyer['id']) && $buyer['id'] ? $buyer['id'] : '';

    //                 if (!empty($_extra_datas)) {

    //                     foreach ($_xls_columns as $_xkey => $_column) {

    //                         $_title = ucwords(strtolower($_column));

    //                         $_objSheet->setCellValue($_xkey . $_start, $_title);

    //                         $_objSheet->getStyle($_xkey . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    //                     }

    //                     $_start++;
    //                 }
    //                 // PRIMARY INFORMATION COLUMN
    //                 foreach ($_alphas as $_akey => $_alpha) {
    //                     $_value = isset($_datas[$buyer_id][$_xls_columns[$_alpha]]) && $_datas[$buyer_id][$_xls_columns[$_alpha]] ? $_datas[$buyer_id][$_xls_columns[$_alpha]] : '';
    //                     $_objSheet->setCellValue($_alpha . $_start, $_value);
    //                 }

    //                 // ADDITIONAL INFORMATION COLUMN
    //                 if (!empty($_extra_datas)) {
    //                     $_start += 2;

    //                     $_addtional_columns = $_extra_titles;
    //                     // echo "<pre>";
    //                     // print_r($_extra_datas);
    //                     // die();
    //                     foreach ($_addtional_columns as $adkey => $_a_column) {

    //                         // MAIN TITLE OF ADDITIONAL DATA

    //                         if ($_a_column === 'employment') {
    //                             $ad_title = 'Employment Information';
    //                         } else if ($_a_column === 'identifications') {
    //                             $ad_title = 'Proof of Identification';
    //                         } else {
    //                             $ad_title = $_a_column;
    //                         }

    //                         $a_title = ucwords(str_replace('_', ' ', strtolower($ad_title)));

    //                         $_objSheet->setCellValueByColumnAndRow($col, $_start, $a_title);

    //                         // Style
    //                         $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
    //                         $_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    //                         // LOOP DATAS
    //                         if (strpos($_a_column, 'employment') !== FALSE) {

    //                             if (!empty($_extra_datas[$buyer_id][$_a_column])) {
    //                                 $refno = 1;

    //                                 foreach ($_extra_datas[$buyer_id][$_a_column] as $rkey => $ref) {

    //                                     $ref_col = $ref;
    //                                     $_start++;

    //                                     $_objSheet->setCellValueByColumnAndRow($col, $_start, 'Reference ' . $refno++);
    //                                     $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
    //                                     $_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

    //                                     foreach ($ref_col as $reftitles => $k) {

    //                                         if ((strpos($reftitles, 'buyer_id')) === FALSE) {

    //                                             $ref_title = str_replace('_id', ' ', $reftitles);

    //                                             if ($reftitles === 'occupation_type_id') {
    //                                                 $k = isset($k) && $k ? Dropdown::get_static('occupation_type', $k, 'view') : '';
    //                                             }
    //                                             if ($reftitles === 'occupation_id') {
    //                                                 $k = isset($k) && $k ? Dropdown::get_static('occupation', $k, 'view') : '';
    //                                             }
    //                                             if ($reftitles === 'location_id') {
    //                                                 $k = isset($k) && $k ? Dropdown::get_static('occupation_location', $k, 'view') : '';
    //                                             }
    //                                             if ($reftitles === 'industry_id') {
    //                                                 $k = isset($k) && $k ? Dropdown::get_static('industry', $k, 'view') : '';
    //                                             }

    //                                             $_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($ref_title));
    //                                             $_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($k));
    //                                         }


    //                                         $_start++;
    //                                     }
    //                                 }
    //                             } else {
    //                                 $_start++;

    //                                 $_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

    //                                 // Style
    //                                 $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
    //                                 $_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    //                                 $_start += 1;
    //                             }
    //                         } elseif (strpos($_a_column, 'identifications') !== FALSE) {
    //                             if (!empty($_extra_datas[$buyer_id][$_a_column])) {

    //                                 foreach ($_extra_datas[$buyer_id][$_a_column] as $ackey => $acad) {

    //                                     $acad_col = array_flip($acad);

    //                                     $_start++;

    //                                     $_objSheet->setCellValueByColumnAndRow($col, $_start, $acad["level"]);

    //                                     $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
    //                                     $_objSheet->getStyle('B' . $_start . ':C' . $_start)->applyFromArray($_style);

    //                                     foreach ($acad_col as $acolkey => $acadtitles) {

    //                                         if ((strpos($acadtitles, '_id')) === FALSE) {
    //                                             $acad_title = str_replace('_', ' ', $acadtitles);
    //                                             $_objSheet->setCellValueByColumnAndRow($col, $_start, ucwords($acad_title));

    //                                             $_objSheet->setCellValueByColumnAndRow($col + 1, $_start, ucwords($acad[$acadtitles]));
    //                                         }

    //                                         $_start++;
    //                                     }
    //                                 }
    //                             } else {
    //                                 $_start++;

    //                                 $_objSheet->setCellValueByColumnAndRow($col, $_start, 'No Records Found');

    //                                 // Style
    //                                 $_objSheet->mergeCells('B' . $_start . ':C' . $_start);
    //                                 $_objSheet->getStyle('B' . $_start . ':C' . $_start)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    //                                 $_start += 1;
    //                             }
    //                         }

    //                         $_start++;
    //                     }
    //                 }

    //                 $_start += 1;

    //             }

    //             foreach ($_alphas as $_alpha) {

    //                 $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
    //             }


    //             $_objSheet->getStyle('A1')->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    //             header('Content-Type: application/vnd.ms-excel');
    //             header('Content-Disposition: attachment; filename="' . $_filename . '"');
    //             header('Cache-Control: max-age=0');
    //             $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    //             @ob_end_clean();
    //             $_objWriter->save('php://output');
    //             @$_objSheet->disconnectWorksheets();
    //             unset($_objSheet);
    //         } else {

    //             $this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
    //         }
    //     } else {

    //         $this->notify->error('No Record Found', 'buyer');
    //     }
    // }

    function export_csv()
    {

        if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

            $_ued = $this->input->post('update_existing_data');

            $_is_update = $_ued === '1' ? TRUE : FALSE;

            $_alphas = [];
            $_datas = [];

            $_titles[] = 'id';

            $_start = 3;
            $_row = 2;

            $_filename = 'Buyer CSV Template.csv';

            // $_fillables	=	$this->M_document->fillable;
            $_fillables = $this->_table_fillables;
            if (!$_fillables) {

                $this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
            }

            foreach ($_fillables as $_fkey => $_fill) {

                if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE && ($_fill !== 'user_id'))) {

                    $_titles[] = $_fill;
                } else {

                    continue;
                }
            }

            if ($_is_update) {

                $records = $this->M_buyer->as_array()->get_all(); #up($_documents);
                if ($records) {

                    foreach ($_titles as $_tkey => $_title) {

                        foreach ($records as $_dkey => $record) {

                            $_datas[$record['id']][$_title] = isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
                        }
                    }
                }
            } else {

                if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

                    unset($_titles[0]);
                }
            }

            $_alphas = $this->__get_excel_columns(count($_titles));
            $_xls_columns = array_combine($_alphas, $_titles);
            $_firstAlpha = reset($_alphas);
            $_lastAlpha = end($_alphas);

            $_objSheet = $this->excel->getActiveSheet();
            $_objSheet->setTitle('Buyers');
            $_objSheet->setCellValue('A1', 'BUYERS');
            $_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

            foreach ($_xls_columns as $_xkey => $_column) {

                $_objSheet->setCellValue($_xkey . $_row, $_column);
            }

            if ($_is_update) {

                if (isset($_datas) && $_datas) {

                    foreach ($_datas as $_dkey => $_data) {

                        foreach ($_alphas as $_akey => $_alpha) {

                            $_value = isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

                            $_objSheet->setCellValue($_alpha . $_start, $_value);
                        }

                        $_start++;
                    }
                } else {

                    $_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
                    $_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

                    $_style = array(
                        'font' => array(
                            'bold' => FALSE,
                            'size' => 9,
                            'name' => 'Verdana'
                        )
                    );
                    $_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
            }

            foreach ($_alphas as $_alpha) {

                $_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
            }

            $_style = array(
                'font' => array(
                    'bold' => TRUE,
                    'size' => 10,
                    'name' => 'Verdana'
                )
            );
            $_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $_filename . '"');
            header('Cache-Control: max-age=0');
            $_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            @ob_end_clean();
            $_objWriter->save('php://output');
            @$_objSheet->disconnectWorksheets();
            unset($_objSheet);
        } else {

            show_404();
        }
    }

    function import()
    {

        if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

            // if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
            if (TRUE) {

                $_tmp_name = $_FILES['csv_file']['tmp_name'];
                $_name = $_FILES['csv_file']['name'];

                set_time_limit(0);

                $_columns = [];
                $_datas = [];

                $_failed_reasons = [];
                $_inserted = 0;
                $_updated = 0;
                $_failed = 0;

                /**
                 * Read Uploaded CSV File
                 */
                try {

                    $_file_type = PHPExcel_IOFactory::identify($_tmp_name);
                    $_objReader = PHPExcel_IOFactory::createReader($_file_type);
                    $_objPHPExcel = $_objReader->load($_tmp_name);
                } catch (Exception $e) {

                    $_msg = 'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

                    $this->notify->error($_msg, 'document');
                }

                $_objWorksheet = $_objPHPExcel->getActiveSheet();
                $_highestColumn = $_objWorksheet->getHighestColumn();
                $_highestRow = $_objWorksheet->getHighestRow();
                $_sheetData = $_objWorksheet->toArray();
                if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

                    if ($_sheetData[1][0] === 'id') {

                        $_columns[] = 'id';
                    }

                    // $_fillables	=	$this->M_document->fillable;
                    $_fillables = $this->_table_fillables;
                    if (!$_fillables) {

                        $this->notify->error('Something went wrong. Please refresh the page and try again.', 'buyer');
                    }

                    foreach ($_fillables as $_fkey => $_fill) {

                        if (in_array($_fill, $_sheetData[1])) {

                            $_columns[] = $_fill;
                        } else {

                            continue;
                        }
                    }

                    foreach ($_sheetData as $_skey => $_sd) {

                        if ($_skey > 1) {

                            if (count(array_filter($_sd)) !== 0) {

                                $_datas[] = array_combine($_columns, $_sd);
                            }
                        } else {

                            continue;
                        }
                    }

                    if (isset($_datas) && $_datas) {

                        foreach ($_datas as $_dkey => $_data) {
                            $_id = isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
                            $_data['birth_date'] = isset($_data['birth_date']) && $_data['birth_date'] ? date('Y-m-d', strtotime(str_replace('-', '/', $_data['birth_date']))) : '';
                            $buyerGroupID = ['4'];

                            if ($_id) {
                                $data = $this->M_buyer->get($_id);
                                if ($data) {

                                    unset($_data['id']);

                                    $oldPwd = password_format($data['last_name'], $data['birth_date']);
                                    $newPwd = password_format($_data['last_name'], $_data['birth_date']);

                                    $oldEmail = $data['email'];
                                    $newEmail = $_data['email'];

                                    if ($this->M_auth->email_check($oldEmail)) {

                                        if ($oldPwd !== $newPwd) {
                                            // Update User Password
                                            $this->M_auth->change_password($data['email'], $oldPwd, $newPwd);
                                        }

                                        if ($oldEmail !== $newEmail) {
                                            // Update User Email
                                            $_user_data = [
                                                'email' => $newEmail,
                                                'username' => $newEmail,
                                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
                                                'updated_at' => NOW
                                            ];
                                            $this->M_user->update($_user_data, array('id' => $data['user_id']));
                                        }

                                        // Update Buyer Info
                                        $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
                                        $_data['updated_at'] = NOW;

                                        $_update = $this->M_buyer->update($_data, $_id);
                                        if ($_update !== FALSE) {

                                            $user_data = array(
                                                'first_name' => $_data['first_name'],
                                                'last_name' => $_data['last_name'],
                                                'active' => $_data['is_active'],
                                                'updated_by' => isset($this->user->id) && $this->user->id ? $this->user->id : NULL,
                                                'updated_at' => NOW
                                            );

                                            $this->M_user->update($user_data, $data['user_id']);

                                            $_updated++;
                                        } else {

                                            $_failed_reasons[$_data['id']][] = 'update not working.';
                                            $_failed++;

                                            break;
                                        }
                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'email already used';
                                        $_failed++;

                                        break;
                                    }
                                } else {

                                    // Generate user password
                                    $buyerPwd = password_format($_data['last_name'], $_data['birth_date']);
                                    $buyerEmail = $_data['email'];

                                    if (!$this->M_auth->email_check($_data['email'])) {

                                        $user_data = array(
                                            'first_name' => $_data['first_name'],
                                            'last_name' => $_data['last_name'],
                                            'active' => $_data['is_active'],
                                            'created_by' => $this->user->id,
                                            'created_at' => NOW
                                        );

                                        $userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $user_data, $buyerGroupID);

                                        if ($userID !== FALSE) {

                                            $_data['user_id'] = $userID;
                                            $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
                                            $_data['created_at'] = NOW;
                                            $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
                                            $_data['updated_at'] = NOW;

                                            $_insert = $this->M_buyer->insert($_data);
                                            if ($_insert !== FALSE) {

                                                $_inserted++;
                                            } else {

                                                $_failed_reasons[$_data['id']][] = 'insert buyer not working';
                                                $_failed++;

                                                break;
                                            }
                                        } else {

                                            $_failed_reasons[$_data['id']][] = 'insert ion auth not working';
                                            $_failed++;

                                            break;
                                        }
                                    } else {

                                        $_failed_reasons[$_data['id']][] = 'email check already existing';
                                        $_failed++;

                                        break;
                                    }
                                }
                            } else {

                                // Generate user password
                                $buyerPwd = password_format($_data['last_name'], $_data['birth_date']);
                                $buyerEmail = $_data['email'];

                                if (!$this->M_auth->email_check($_data['email'])) {

                                    $user_data = array(
                                        'first_name' => $_data['first_name'],
                                        'last_name' => $_data['last_name'],
                                        'active' => $_data['is_active'],
                                        'created_by' => $this->user->id,
                                        'created_at' => NOW
                                    );

                                    $userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $user_data, $buyerGroupID);

                                    if ($userID !== FALSE) {

                                        $_data['user_id'] = $userID;
                                        $_data['created_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
                                        $_data['created_at'] = NOW;
                                        $_data['updated_by'] = isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
                                        $_data['updated_at'] = NOW;

                                        $_insert = $this->M_buyer->insert($_data);
                                        if ($_insert !== FALSE) {

                                            $_inserted++;
                                        } else {

                                            $_failed_reasons[$_dkey][] = 'insert buyer not working';

                                            $_failed++;

                                            break;
                                        }
                                    } else {

                                        $_failed_reasons[$_dkey][] = 'insert ion auth not working';

                                        $_failed++;

                                        break;
                                    }
                                } else {

                                    $_failed_reasons[$_dkey][] = 'email check already existing';

                                    $_failed++;

                                    break;
                                }
                            }
                        }

                        $_msg = '';
                        if ($_inserted > 0) {

                            $_msg = $_inserted . ' record/s was successfuly inserted';
                        }

                        if ($_updated > 0) {

                            $_msg .= ($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
                        }

                        if ($_failed > 0) {
                            $this->notify->error('Upload Failed! Please follow upload guide. ', 'buyer');
                        } else {

                            $this->notify->success($_msg . '.', 'buyer');
                        }
                    }
                } else {

                    $this->notify->warning('CSV was empty.', 'buyer');
                }
            } else {

                $this->notify->warning('Not a CSV file!', 'buyer');
            }
        } else {

            $this->notify->error('Something went wrong!', 'buyer');
        }
    }

    function rolekey_exists($table, $field, $value)
    {
        return $this->M_buyer->role_exists($table, $field, $value);
    }

    public function total_count_buyers()
    {
        $total_count = 0;
        if ($this->input->post()) {
            $filter = $this->input->post('filter');

            $new_buyers = $this->db->query(
                "SELECT y, m, count(id) as count
				FROM (
				  SELECT y, m
				  FROM
					(SELECT YEAR(CURDATE()) y UNION ALL SELECT YEAR(CURDATE())-1) years,
					(SELECT 1 m UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4
					  UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8
					  UNION ALL SELECT 9 UNION ALL SELECT 10 UNION ALL SELECT 11 UNION ALL SELECT 12) months) ym
				LEFT JOIN buyers
                ON ym.y = YEAR(created_at)
                    AND ym.m = MONTH(created_at)
				WHERE
				  ((y=YEAR(CURDATE()) AND m<=MONTH(CURDATE()))
				  OR
				  (y<YEAR(CURDATE()) AND m>MONTH(CURDATE() - interval $filter month)))
				AND (deleted_at IS NULL and deleted_by IS NULL)
				group by 
				y,m"
            )->result_array();

            $new_buyer_count = $this->db->query(
                "SELECT coalesce(count(id),0) as count
                FROM buyers
                WHERE
                (year(created_at)=year(CURDATE()) and month(created_at)=MONTH(CURDATE())) AND
                (deleted_at is null and deleted_by IS NULL)
                "
            )->result_array();

            $result['month'] = get_date_range("month", $filter) . ' - ' . get_date_range("month", 0);
            $result['indiv_amounts'] = [];
            $result['dates'] = [];
            if ($new_buyers) {

                foreach ($new_buyers as $row) {
                    array_push($result['dates'], date("F", mktime(0, 0, 0, $row['m'], 10)) . ' ' . $row['y']);
                    array_push($result['indiv_amounts'], $row['count']);
                }
            }
        }
        $result['count'] = $new_buyer_count[0]['count'];
        echo json_encode($result);
        exit();
    }

    public function sales_report()
    {
        // Form to get the parameters for the sales report
        if ($this->input->is_ajax_request()) {
            $output = ['data' => ''];

            $columnsDefault = [
                'buyer' => true,
                'reference' => true,
                'property' => true,
                'house_model' => true,
                'reservation_date' => true,
                'total_collectible_price' => true,
                /* ==================== end: Add model fields ==================== */
            ];

            if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
                $columnsDefault = [];
                foreach ($_REQUEST['columnsDef'] as $field) {
                    $columnsDefault[$field] = true;
                }
            }

            // get the filters from post

            $company = $this->input->post('company');
            $project = $this->input->post('project');
            $house_model = $this->input->post('house_model');

            if ($project) {
                $this->db->where('project_id', $project);
            }

            // get all raw data
            $transactions = $this->M_transaction->with_buyer()->with_property()->with_t_property()->get_all();

            $data = [];

            if ($transactions) {

                foreach ($transactions as $key => $entry) {
                    $first_name = isset($entry['buyer']['first_name']) && $entry['buyer']['first_name'] ? $entry['buyer']['first_name'] : '';
                    $last_name = isset($entry['buyer']['last_name']) && $entry['buyer']['last_name'] ? $entry['buyer']['last_name'] : '';
                    $model_id = isset($entry['property']['model_id']) && $entry['property']['model_id'] ? $entry['property']['model_id'] : '';
                    $project_id = isset($entry['property']['project_id']) && $entry['property']['project_id'] ? $entry['property']['project_id'] : '';
                    $model_name = get_value_field($model_id, 'house_models', 'name');
                    $transactions[$key]['buyer'] = $first_name . ' ' .  $last_name;
                    $transactions[$key]['reservation_date'] = date('Y-m-d', strtotime($transactions[$key]['reservation_date']));
                    $transactions[$key]['property'] = isset($entry['property']['name']) && $entry['property']['name'] ? $entry['property']['name'] : '';
                    $transactions[$key]['house_model'] = $model_name;
                    $transactions[$key]['total_collectible_price'] = money_php(isset($entry['t_property']['collectible_price']) && $entry['t_property']['collectible_price'] ? $entry['t_property']['collectible_price'] : '0');

                    $count = 0;

                    // Check if house_model is set, if it is not set, increment the counter
                    if ($house_model) {
                        $item_house_model = get_value_field($house_model, 'house_models', 'name');
                        if ($item_house_model == $model_name) {
                            $count++;
                        }
                    } else {
                        $count++;
                    }

                    // Check if company is set, if it is not set, increment the counter.
                    if ($company) {
                        $item_company_id = get_value_field($project_id, 'projects', 'company_id');
                        if ($item_company_id == $company) {
                            $count++;
                        }
                    } else {
                        $count++;
                    }

                    // The counter is checked. The counter is responsible for determining if the house_model or company is set and if it is, do they match the current data.

                    if ($count == 2) {
                        $data[] = $this->filterArray($transactions[$key], $columnsDefault);
                    }
                }

                // count data
                $totalRecords = $totalDisplay = count($data);

                // filter by general search keyword
                if (isset($_REQUEST['search'])) {
                    $data = $this->filterKeyword($data, $_REQUEST['search']);
                    $totalDisplay = count($data);
                }

                if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
                    foreach ($_REQUEST['columns'] as $column) {
                        if (isset($column['search'])) {
                            $data = $this->filterKeyword($data, $column['search'], $column['data']);
                            $totalDisplay = count($data);
                        }
                    }
                }

                // sort
                if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

                    $_column = $_REQUEST['order'][0]['column'] - 1;
                    $_dir = $_REQUEST['order'][0]['dir'];

                    usort($data, function ($x, $y) use ($_column, $_dir) {

                        // echo "<pre>";
                        // print_r($x) ;echo "<br>";
                        // echo $_column;echo "<br>";

                        $x = array_slice($x, $_column, 1);

                        // vdebug($x);

                        $x = array_pop($x);

                        $y = array_slice($y, $_column, 1);
                        $y = array_pop($y);

                        if ($_dir === 'asc') {

                            return $x > $y ? true : false;
                        } else {

                            return $x < $y ? true : false;
                        }
                    });
                }

                // pagination length
                if (isset($_REQUEST['length'])) {
                    $data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
                }

                // return array values only without the keys
                if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
                    $tmp = $data;
                    $data = [];
                    foreach ($tmp as $d) {
                        $data[] = array_values($d);
                    }
                }
                $secho = 0;
                if (isset($_REQUEST['sEcho'])) {
                    $secho = intval($_REQUEST['sEcho']);
                }

                $output = array(
                    'sEcho' => $secho,
                    'sColumns' => '',
                    'iTotalRecords' => $totalRecords,
                    'iTotalDisplayRecords' => $totalDisplay,
                    'data' => $data,
                );
            }

            echo json_encode($output);
            exit();
        }
        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('sales_report', $this->view_data);
    }

    public function names_to_uppercase_words()
    {
        $this->db->query("UPDATE buyers SET first_name=CONCAT(UPPER(LEFT(first_name, 1)), LOWER(SUBSTRING(first_name, 2))), middle_name=CONCAT(UPPER(LEFT(middle_name, 1)), LOWER(SUBSTRING(middle_name, 2))), last_name=CONCAT(UPPER(LEFT(last_name, 1)), LOWER(SUBSTRING(last_name, 2)))");

        $next_word_queries = [
            "UPDATE buyers SET first_name = REPLACE(first_name,' a',' A'), middle_name = REPLACE(middle_name,' a',' A'), last_name = REPLACE(last_name,' a',' A')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' b',' B'), middle_name = REPLACE(middle_name,' b',' B'), last_name = REPLACE(last_name,' b',' B')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' c',' C'), middle_name = REPLACE(middle_name,' c',' C'), last_name = REPLACE(last_name,' c',' C')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' d',' D'), middle_name = REPLACE(middle_name,' d',' D'), last_name = REPLACE(last_name,' d',' D')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' e',' E'), middle_name = REPLACE(middle_name,' e',' E'), last_name = REPLACE(last_name,' e',' E')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' f',' F'), middle_name = REPLACE(middle_name,' f',' F'), last_name = REPLACE(last_name,' f',' F')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' g',' G'), middle_name = REPLACE(middle_name,' g',' G'), last_name = REPLACE(last_name,' g',' G')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' h',' H'), middle_name = REPLACE(middle_name,' h',' H'), last_name = REPLACE(last_name,' h',' H')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' i',' I'), middle_name = REPLACE(middle_name,' i',' I'), last_name = REPLACE(last_name,' i',' I')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' j',' J'), middle_name = REPLACE(middle_name,' j',' J'), last_name = REPLACE(last_name,' j',' J')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' k',' K'), middle_name = REPLACE(middle_name,' k',' K'), last_name = REPLACE(last_name,' k',' K')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' l',' L'), middle_name = REPLACE(middle_name,' l',' L'), last_name = REPLACE(last_name,' l',' L')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' m',' M'), middle_name = REPLACE(middle_name,' m',' M'), last_name = REPLACE(last_name,' m',' M')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' n',' N'), middle_name = REPLACE(middle_name,' n',' N'), last_name = REPLACE(last_name,' n',' N')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' o',' O'), middle_name = REPLACE(middle_name,' o',' O'), last_name = REPLACE(last_name,' o',' O')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' p',' P'), middle_name = REPLACE(middle_name,' p',' P'), last_name = REPLACE(last_name,' p',' P')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' q',' Q'), middle_name = REPLACE(middle_name,' q',' Q'), last_name = REPLACE(last_name,' q',' Q')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' r',' R'), middle_name = REPLACE(middle_name,' r',' R'), last_name = REPLACE(last_name,' r',' R')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' s',' S'), middle_name = REPLACE(middle_name,' s',' S'), last_name = REPLACE(last_name,' s',' S')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' t',' T'), middle_name = REPLACE(middle_name,' t',' T'), last_name = REPLACE(last_name,' t',' T')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' u',' U'), middle_name = REPLACE(middle_name,' u',' U'), last_name = REPLACE(last_name,' u',' U')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' v',' V'), middle_name = REPLACE(middle_name,' v',' V'), last_name = REPLACE(last_name,' v',' V')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' w',' W'), middle_name = REPLACE(middle_name,' w',' W'), last_name = REPLACE(last_name,' w',' W')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' x',' X'), middle_name = REPLACE(middle_name,' x',' X'), last_name = REPLACE(last_name,' x',' X')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' y',' Y'), middle_name = REPLACE(middle_name,' y',' Y'), last_name = REPLACE(last_name,' y',' Y')",
            "UPDATE buyers SET first_name = REPLACE(first_name,' z',' Z'), middle_name = REPLACE(middle_name,' z',' Z'), last_name = REPLACE(last_name,' z',' Z')",
        ];

        foreach ($next_word_queries as $query) {
            $this->db->query($query);
        }
    }
    public function update_password($id, $mode = 0)
    {
        if ($id) {
            $results['status'] = 0;
            $results['message'] = 'Password not updated.';
            $additional['updated_by'] = $this->user->id;
            $additional['updated_at'] = NOW;
            $buyer = $this->M_buyer->get($id);
            $user = $this->M_user->get($buyer['user_id']);
            $b_email = str_replace(' ', '', $buyer['email']);
            $u_email = str_replace(' ', '', $user['email']);
            if ($b_email != $u_email) {
                $email = $u_email;
            } else {
                $email = $b_email;
            }
            $email = str_replace('..', '.', $email);
            $last_name = strtolower($buyer['last_name']);
            $first_name = strtolower($buyer['first_name']);
            $birthday = $buyer['birth_date'];
            if ($birthday == '0000-00-00') {
                $password = password_format($last_name, $first_name, 1);
            } else {
                $password = password_format($last_name, $birthday);
            }
            if ($mode) {
                $info['username'] = $email;
                $info['password'] = $password;
                return $info;
            }
            $info['password'] = $this->M_auth->hash_password($password);
            $info['email'] = $email;
            $info['username'] = $email;
            $buyer_update = $this->M_buyer->update(['email' => $email] + $additional, $buyer['id']);
            $user_update = $this->M_user->update($info + $additional, $buyer['user_id']);
            if ($buyer_update && $user_update) {
                $results['status'] = 1;
                $results['message'] = 'Password Successfully changed!';
            }
            echo json_encode($results);
            exit();
        }
    }

    public function update_all_passwords()
    {
        $results = [];
        if ($this->user->id == 6267) {
            $this->db->where('id > 2');
            $buyers = $this->M_buyer->fields('id')->get_all();
            for ($i = 0; $i < count($buyers); $i++) {
                $result = $this->check_duplicates($buyers[$i]['id']);
                // $result = $this->update_password($buyers[$i]['id']);
                $results[] = $result;
            }
            vdebug(array_filter($results));
        }
        show_403('You have no permission to access this page.');
    }

    public function send_email($buyer_id)
    {
        $response['status'] = 0;
        $response['message'] = 'Failed to send email!';
        $info = $this->update_password($buyer_id, 1);
        $this->load->library('email');
        $this->email->from('johngaebriel.vargas@pueblodepanay.com', 'REMS Notification');
        $this->email->to('sekajii@gmail.com');

        $this->email->subject('REMS Access Credentials');
        $this->email->message("Username: " . $info['username'] . "\r\n Password: " . $info['password']);
        if ($this->email->send()) {
            $response['status'] = 1;
            $response['status'] = 'Email Sent!';
        }
        exit();
    }

    public function check_duplicates($buyer_id)
    {
        $result = [];
        $source = $this->M_buyer->where(['id' => $buyer_id])->fields('last_name,first_name')->get();
        $name = str_replace(' ', '', $source['last_name']) . str_replace(' ', '', $source['first_name']);
        $this->db->where("concat(replace(last_name,' ',''), replace(first_name,' ','')) = \"$name\"");
        $buyers = array_column($this->M_buyer->get_all(), 'id');
        foreach ($buyers as $buyer) {
            $trans = $this->M_transaction->where(['buyer_id' => $buyer])->get();
            if ($trans) {
                $result[] = $buyer;
            }
        }
        return count($result) > 1 ? $result : '';
    }

    public function print_info_form($buyer_id,$debug=0){
        $buyer_data = $this->M_buyer
                        ->with_identifications()
                        ->with_barangay()
                        ->with_city(['fields'=>'citymunDesc,zipCode'])
                        ->with_province(['with'=>['relation'=>'region']])
                        ->with_region()
                        ->with_country()
                        ->with_employment()
                        ->get($buyer_id);
        $buyer_data['nationality'] = Dropdown::get_static('nationality', $buyer_data['nationality'], 'view');
        if(strlen($buyer_data['tin']) >= 9){        
            $numbers = preg_replace("/[^0-9]/", "", $buyer_data['tin']);
            $buyer_data['tin'] = substr($numbers,0,3) . '-' . substr($numbers,3,3) . '-' . substr($numbers,6,3);
        }else{
            $buyer_data['tin'] = '';
        }
        if($buyer_data['identifications']){
            $buyer_data['identifications'][0]['type_of_id'] = Dropdown::get_static('ids', $buyer_data['identifications'][0]['type_of_id'], 'view');
            $buyer_data['identifications'][0]['id_number'] = $buyer_data['identifications'][0]['id_number'] ? : 'None';
            $buyer_data['identifications'][0]['date_issued'] = $buyer_data['identifications'][0]['date_issued'] ? date('m/d/Y', strtotime($buyer_data['identifications'][0]['date_issued'])) : 'None';
            $buyer_data['identifications'][0]['date_expiration'] = $buyer_data['identifications'][0]['date_expiration'] ? date('m/d/Y', strtotime($buyer_data['identifications'][0]['date_issued'])) : 'None';
            $buyer_data['identifications'][0]['place_issued'] = $buyer_data['identifications'][0]['place_issued'] ? : 'None';
        }else{
            $buyer_data['identifications'][0]['type_of_id'] = 'None';
            $buyer_data['identifications'][0]['id_number'] = 'None';
            $buyer_data['identifications'][0]['date_issued'] = 'None';
            $buyer_data['identifications'][0]['date_expiration'] = 'None';
            $buyer_data['identifications'][0]['place_issued'] = 'None';
        }
        $buyer_data['barangay'] = json_decode(json_encode($buyer_data['barangay']),true);
        $buyer_data['barangay'] = $buyer_data['barangay'] ? $buyer_data['barangay']['brgyDesc'] : ' ';
        $buyer_data['city'] = json_decode(json_encode($buyer_data['city']),true);

        $buyer_data['province'] = json_decode(json_encode($buyer_data['province']),true);
        $buyer_data['country'] = json_decode(json_encode($buyer_data['country']),true);

        if($buyer_data['employment']){
            $buyer_data['employment']['gross_salary'] = floatval($buyer_data['employment']['gross_salary']);
        }

        if($debug){
            vdebug($buyer_data);
        }
        $this->view_data['buyer'] = $buyer_data;
        $generateHTML = $this->load->view('buyer_info_form', $this->view_data,true);
        echo $generateHTML;
        die();
        // pdf_create($generateHTML);
    }
}
