<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends MY_Controller {

	public $filter_var = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('project/Project_model', 'M_project');
		$this->load->model('house/House_model', 'M_house');
		$this->load->model('company/Company_model', 'M_company');
	}

	public function index($offset = 0)
	{

		// overwrite default theme and layout if needed
		$this->template->set_theme('default');
		$this->template->set_layout('default');
		
		$this->css_loader->queue("vendors/custom/datatables/datatables.bundle.css");
		$this->js_loader->queue("vendors/custom/datatables/datatables.bundle.js");

		$this->session->unset_userdata('filter_var');

		$total_projects = $this->M_project->count_rows();
		$limit = 9;

		// get all raw data
		$alldata = $this->M_project->with_company('fields: name')->as_array()->limit($limit, $offset)->paginate($limit,$total_projects);

		// print_r($alldata); exit;

		$this->view_data['record'] = $alldata; 
		$this->view_data['pagination'] = $this->M_project->all_pages;

		$this->template->build('index', $this->view_data);

	}

	public function view($id = FALSE)
	{
		$this->js_loader->queue("//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM");
		$this->js_loader->queue("vendors/custom/gmaps/gmaps.js");
		
		if ($id) {

			$this->view_data['project'] = $this->M_project->with_company('fields: id, name')->get($id);
			$this->view_data['house'] = $this->M_house->where('project_id', $id)->as_array()->get_all();
			
			if ($this->view_data['project'] || $this->view_data['house']) {

				$this->template->build('view', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function create()
	{
		$this->css_loader->queue("vendors/general/select2/dist/css/select2.css");
		$this->js_loader->queue("vendors/general/select2/dist/js/select2.full.js");

		if ($this->input->post()) 
		{
			// echo $this->session->userdata('user_id'); exit;
			$additional_fields = array(
				'created_by' => $this->session->userdata('user_id'),
				'created_at' => date('Y-m-d')
			);
			$result = $this->M_project->from_form(NULL, $additional_fields)->insert();

			if ($result === FALSE) {

				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Project successfully created.', 'project');
			}
		}

		$this->template->build('create');
	}
	
	public function update($id = FALSE)
	{
		$this->css_loader->queue("vendors/general/select2/dist/css/select2.css");
		$this->js_loader->queue("vendors/general/select2/dist/js/select2.full.js");
		
		if ($id) {
			$this->view_data['project'] = $data = $this->M_project->as_array()->get($id);
			
			if ($data) 
			{
				if ($this->input->post()) {
					$additional_fields = array(
						'updated_by' => $this->session->userdata('user_id'),
						'updated_at' => date('Y-m-d')
					);
					
					$this->M_project->rules['update'] = $this->M_project->fields;
					
					$project_result = $this->M_project->from_form(NULL, $additional_fields)->update(NULL, $data['id']);

					if ($project_result === FALSE) {

						// Validation
						$this->notify->error('Oops something went wrong.');
					} else {

						// Success
						$this->notify->success('Project successfully updated.', 'project');
					}
				}
				
				$this->template->build('update', $this->view_data);
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}

	public function delete($id = FALSE)
	{
		$id = $this->input->post('id');
	
		$result = (bool)$this->M_project->delete($id);
		if ($result) {
			$fields = array(
				'deleted_by' => $this->session->userdata('user_id')
			);
			
			$this->M_project->rules['update'] = $this->M_project->delete_fields;
			
			$project_result = $this->M_project->update($fields, $id);

			$response['status']  = TRUE;
			$response['message'] = 'Project Deleted Successfully ...';
		} else {

			$response['status']  = FALSE;
			$response['message'] = 'Unable to delete Project ...';
		}

		echo json_encode($response);
	}
	
	public function checklist()
	{
		$this->css_loader->queue("vendors/custom/datatables/datatables.bundle.css");
		$this->js_loader->queue("vendors/custom/datatables/datatables.bundle.js");
		
		$this->template->build('checklist');
	}

	public function document()
	{
		$this->css_loader->queue("vendors/custom/datatables/datatables.bundle.css");
		$this->js_loader->queue("vendors/custom/datatables/datatables.bundle.js");
		
		$this->template->build('document');
	}

	public function filter($offset = 0)
	{	
		$this->template->title('REMS', 'Projects');

		// overwrite default theme and layout if needed
		$this->template->set_theme('default');
		$this->template->set_layout('default');
		
		$this->css_loader->queue("vendors/custom/datatables/datatables.bundle.css");
		$this->js_loader->queue("vendors/custom/datatables/datatables.bundle.js");

		if(count($this->input->post()) > 0)
		{
			$this->session->set_userdata('filter_var', $this->input->post());
		}

		if(count($this->input->post()) == 0)
		{
			$name = $this->session->userdata('filter_var')['filter_name'];
			$type = $this->session->userdata('filter_var')['filter_type'];
			$company = $this->session->userdata('filter_var')['filter_company'];
			$status = $this->session->userdata('filter_var')['filter_status'];
		}
		else{
			$name = $this->input->post('filter_name');
			$type = $this->input->post('filter_type');
			$company = $this->input->post('filter_company');
			$status = $this->input->post('filter_status');
		}

		$total_projects = $this->M_project->where('name','like',$name)->where('type','like',$type)->where('company_id','like',$company)->where('status','like',$status)->count_rows();
		$limit = 9;

		// get all raw data
		$alldata = $this->M_project->where('name','like',$name)->where('type','like',$type)->where('company_id','like',$company)->where('status','like',$status)->as_array()->limit($limit, $offset)->paginate($limit,$total_projects);

		$this->view_data['record'] = $alldata; 
		$this->view_data['filter'] = (count($this->input->post()) == 0) ? $this->session->userdata('filter_var') : $this->input->post(); 
		$this->view_data['pagination'] = $this->M_project->all_pages;

		$this->template->build('index', $this->view_data);
	}

	public function export()
	{
		// print_r($this->input->get()); exit;
		$name = $this->input->get('filter_name');
		$type = $this->input->get('filter_type');
		$company = $this->input->get('filter_company');
		$status = $this->input->get('filter_status');

		// get all raw data
		$alldata = $this->M_project->where('name','like',$name)->where('type','like',$type)->where('company_id','like',$company)->where('status','like',$status)->as_array()->get_all();


		$date_file_name = date('F d Y', strtotime(date("Y-m-d H:i:s")));
        $date_title_name = date('F d, Y', strtotime(date("Y-m-d H:i:s")));

        $file = FCPATH.'assets/excel_templates/project.xlsx';
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->setPreCalculateFormulas(true);

        $baseRow = 3;
		$count = count($alldata);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', "List of Projects as of ".strtoupper($date_title_name)); 
        
		if($alldata)
        {
			foreach($alldata as $key => $project)
            {
				$project = json_decode(json_encode($project), true);
				$row_no = $key + 1;
				
				$type = ($project['type'] == 1 ? 'Subdivision' : 'Condominium');
				$status = ($project['status'] == 'O' ? 'Ongoing' : 'Completed');

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$baseRow, $project['company_id']) 
							->setCellValue('B'.$baseRow, ucwords($project['name']))
							->setCellValue('C'.$baseRow, $type)
							->setCellValue('D'.$baseRow, $project['min_price'])
							->setCellValue('E'.$baseRow, $project['max_price'])
							->setCellValue('F'.$baseRow, $project['location'])
							->setCellValue('G'.$baseRow, date('F j, Y', strtotime($project['project_start_date'])))
							->setCellValue('H'.$baseRow, date('F j, Y', strtotime($project['dpc_palc_date'])))
							->setCellValue('I'.$baseRow, date('F j, Y', strtotime($project['cr_lts_date'])))
							->setCellValue('J'.$baseRow, date('F j, Y', strtotime($project['titling_date'])))
							->setCellValue('K'.$baseRow, $project['project_area'])
							->setCellValue('L'.$baseRow, $project['saleable_area'])
							->setCellValue('M'.$baseRow, $project['no_saleable_unit'])
							->setCellValue('N'.$baseRow, date('F j, Y', strtotime($project['coc_date'])))
							->setCellValue('O'.$baseRow, date('F j, Y', strtotime($project['etd_date'])))
							->setCellValue('P'.$baseRow, $project['lts_number'])
							->setCellValue('Q'.$baseRow, $project['PD_units'])
							->setCellValue('R'.$baseRow, $project['BP_units'])
							->setCellValue('S'.$baseRow, $status)
							;

				$baseRow++;
				if($count != $row_no)
				{
					$objPHPExcel->getActiveSheet()->insertNewRowBefore($baseRow,1);
				}
			}
		}
		else
        {
            $objPHPExcel->getActiveSheet()->removeRow($baseRow);
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$baseRow.':S'.$baseRow);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$baseRow, 'NO RESULT FOUND.');
            $styleArray = array(
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => 'FF0000'),
                    'size'  => 15,
                    'name'  => 'Verdana'
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A'.$baseRow)->applyFromArray($styleArray);
        }


		
		$file_name = "List of Projects - " . $date_file_name . ".xls";

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="'.$file_name.'"');
        // Write file to the browser
        $objWriter->save('php://output');
	}
	
	public function import()
	{
		// print_r($_FILES); 
		// print_r($this->input->post()); exit;
		
		$file = $_FILES['file']['tmp_name'];
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);
		
		$sheetData = $objPHPExcel->getActiveSheet()->toArray();
		// print_r($sheetData); exit;

		$err = 0;
		
		if($sheetData[0][0] == 'ID' && $sheetData[0][27] == 'DELETE AT')
		{
			foreach($sheetData as $key => $upload_data)
            {
				if($key > 0)
				{
					$fields = array(
						'company_id' => $upload_data[1],
						'name' => $upload_data[2],
						'type' => $upload_data[3],
						'embedded_map' => $upload_data[4],
						'max_price' => $upload_data[5],
						'min_price' => $upload_data[6],
						'location' => $upload_data[7],
						'project_start_date' => $upload_data[8],
						'dpc_palc_date' => $upload_data[9],
						'cr_lts_date' => $upload_data[10],
						'titling_date' => $upload_data[11],
						'project_area' => $upload_data[12],
						'saleable_area' => $upload_data[13],
						'no_saleable_unit' => $upload_data[14],
						'coc_date' => $upload_data[15],
						'etd_date' => $upload_data[16],
						'lts_number' => $upload_data[17],
						'PD_units' => $upload_data[18],
						'BP_units' => $upload_data[19],
						'status' => $upload_data[20]
					);
					
					$result = $this->M_project->insert($fields);

					if ($result === FALSE) {
						
						// Validation
						$this->notify->error('Oops something went wrong.');
						$err = 1;
						break;
					}
				}
			}
		}
		else
		{
			$this->notify->error('Oops something went wrong.');
		}
		
		if($err == 0)
		{
			$this->notify->success('CSV successfully imported.', 'project');
		}
		else{
			$this->notify->error('Oops something went wrong.');
		}
		
		header('Location: '. base_url().'project');
		die();
	}
	
	public function export_csv()
	{
		// print_r($this->input->post()); exit;
		
		$date_file_name = date('F d Y', strtotime(date("Y-m-d H:i:s")));
        $date_title_name = date('F d, Y', strtotime(date("Y-m-d H:i:s")));

        $file = FCPATH.'assets/excel_templates/csv_project.csv';
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->setPreCalculateFormulas(true);
		
		$baseRow = 1;
		$baseColumn = 'A';
		
		$query = $this->db->query('SHOW COLUMNS FROM projects');
		$queryResultArray = $query->result_array();
		$array = array();
		$project_fillable_fields = $this->M_project->fillable;
		unset($project_fillable_fields['20']);
		
		foreach ($queryResultArray as $field)
		{
			if (in_array($field['Field'], $project_fillable_fields)) {
				$objPHPExcel->getActiveSheet()->setCellValue($baseColumn.$baseRow, $field['Field']);
				$baseColumn++;				
			}
		}
		
		// reset bases
		$baseRow++;
		$baseColumn = 'A';
		
		$alldata = $this->M_project->as_array()->get_all();
		
		foreach ($alldata as $p)
		{
			foreach ($queryResultArray as $field)
			{
				if (in_array($field['Field'], $project_fillable_fields)) {
					$objPHPExcel->getActiveSheet()->setCellValue($baseColumn.$baseRow, $p[$field['Field']]);
					$baseColumn++;				
				}
			}
			// reset bases
			$baseRow++;
			$baseColumn = 'A';
			if($this->input->post('status') == 0)
			{
				break;
			}
		}
		
		$file_name = "Project CSV Template.csv";

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="'.$file_name.'"');
        // Write file to the browser
        $objWriter->save('php://output');
	}
	
	public function get_table_schema()
	{
		$query = $this->db->query('SHOW COLUMNS FROM projects');
		$queryResultArray = $query->result_array();
		$array = array();
		$project_fillable_fields = $this->M_project->fillable;
		unset($project_fillable_fields['20']);
		$ctr = 1;
		foreach ($queryResultArray as $field)
		{
			if (in_array($field['Field'], $project_fillable_fields)) {
				$det = array();
				$det['no'] = $ctr;
				$det['name'] = $field['Field'];
				$det['type'] = $field['Type'];
				$det['format'] = '';
				$det['option'] = '';
				$det['required'] = ($field['Null'] == 'NO') ? 'Yes' : 'No';
				
				if($det['name'] == 'embedded_map')
				{
					$det['format'] = 'Shared link from Google Maps.';
				}
				
				if($det['name'] == 'project_start_date' || $det['name'] == 'dpc_palc_date' || $det['name'] == 'cr_lts_date' || $det['name'] == 'titling_date' || $det['name'] == 'coc_date' || $det['name'] == 'etd_date')
				{
					$det['format'] = 'YYYY-MM-DD';
				}
				
				if($det['name'] == 'company_id')
				{
					$companies = $this->M_company->as_array()->get_all();
					$det['option'] = '';
					$det['option'] .= '<select>';
					foreach($companies as $c)
					{
						$det['option'] .= '<option>'.$c['id'].' - '.$c['name'].'</option>';
					}
					$det['option'] .= '</select>';
				}
				
				if($det['name'] == 'type')
				{
					$det['option'] = '1 - Subdivision <br /> 2 - Condominium';
				}
				
				if($det['name'] == 'status')
				{
					$det['option'] = 'O - Ongoing <br /> C - Completed';
				}
				
				$array[] = $det;
				$ctr++;
			}
		}
		
		$_response	=	[];

		$_total['_displays']	=	0;
		$_total['_records']		=	0;

		$_columns	=	[
			'no'	=>	TRUE,
			'name'	=>	TRUE,
			'type'	=>	TRUE,
			'format'	=>	TRUE,
			'option'	=>	TRUE,
			'required'	=>	TRUE
		];

		if ( isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef']) ) {

			$_columns	=	[];

			foreach ( $_REQUEST['columnsDef'] as $_dkey => $_def ) {

				$_columns[$_def]	=	TRUE;
			}
		}
		
		$_checklists = $array;
		
		if ( $_checklists ) {

			$_datas	=	[];

			foreach ( $_checklists as $_ckkey => $_ck ) {

				$_datas[]	=	$this->filterArray($_ck, $_columns);
			}

			$_total['_displays']	=	$_total['_records']	=	count($_datas);

			if ( isset($_REQUEST['search']) ) {

				$_datas	=	$this->filterKeyword($_datas, $_REQUEST['search']);

				$_total['_displays']	=	$_datas ? count($_datas) : 0;
			}

			if ( isset($_REQUEST['columns']) && is_array($_REQUEST['columns']) ) {

				foreach ( $_REQUEST['columns'] as $_ckey => $_clm ) {

					if ( $_clm['search'] ) {

						$_datas	=	$this->filterKeyword($_datas, $_clm['search'], $_clm['data']);

						$_total['_displays']	=	$_datas ? count($_datas) : 0;
					}
				}
			}

			if ( isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir'] ) {

				$_column	=	$_REQUEST['order'][0]['column'];
				$_dir			=	$_REQUEST['order'][0]['dir'];

				usort( $_datas, function ( $x, $y ) use ( $_column, $_dir ) {

					$x	=	array_slice($x, $_column, 1);
					$x	=	array_pop($x);

					$y	=	array_slice($y, $_column, 1);
					$y	=	array_pop($y);

					if ( $_dir === 'asc' ) {

						return $x > $y ? TRUE : FALSE;
					} else {

						return $x < $y ? TRUE : FALSE;
					}
				});
			}

			if ( isset($_REQUEST['length']) ) {

				$_datas	=	array_splice($_datas, $_REQUEST['start'], $_REQUEST['length']);
			}

			if ( isset($_REQUEST['array_values']) && $_REQUEST['array_values'] ) {

				$_temp	=	$_datas;
				$_datas	=	[];

				foreach ( $_temp as $key => $_tmp ) {

					$_datas[]	=	array_values($_tmp);
				}
			}

			$_secho	=	0;
			if ( isset($_REQUEST['sEcho']) ) {

				$_secho	=	intval($_REQUEST['sEcho']);
			}

			$_response	=	[
											'iTotalDisplayRecords'	=>	$_total['_displays'],
											'iTotalRecords'					=>	$_total['_records'],
											'sColumns'							=>	'',
											'sEcho'									=>	$_secho,
											'data'									=>	$_datas
										];
		}
		
		echo json_encode($_response);
	}

	function document_checklist ( $_proj_id = FALSE ) {

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('project/Project_document_model', 'M_p_document');

		if ( $_proj_id ) {

			$this->view_data['_proj_id']	=	$_proj_id;

			$_documents	=	$this->M_p_document->_get_document_checklist($_proj_id);
			if ( $_documents ) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('document_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function documents ( $_proj_id = FALSE ) {

		$this->view_data['_largescreen']	=	TRUE;

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('checklist/Checklist_model', 'M_checklist');

		if ( $_proj_id ) {

			$this->view_data['_proj_id']	=	$_proj_id;

			$_checklists	=	$this->M_checklist->find_all(FALSE, ['id', 'name']);
			if ( $_checklists ) {

				$this->view_data['_checklists']	=	$_checklists; #ud($_checklists);
			}

			$this->template->build('documents', $this->view_data);
		} else {

			show_404();
		}
	}

	function get_filtered_document_checklist () {

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'';
		$_view_data	=	[];

		$_checklist_id	=	$this->input->post('value');
		$_proj_id				=	$this->input->post('proj_id');
		if ( $_checklist_id && $_proj_id ) {

			$_view_data['_proj_id']	=	$_proj_id;

			$this->load->model('checklist/Document_checklist_model', 'M_document_checklist');

			$_documents	=	$this->M_document_checklist->_get_documents($_checklist_id);
			if ( $_documents ) {

				$_view_data['_documents']	=	$_documents;
			}

			$_respo['_status']	=	1;

			$_respo['_html']	=	$this->load->view('_checklist/documents', $_view_data, TRUE);
		} else {

			$_respo['_msg']	=	'Oops! Please refresh the page and try again.';
		}

		echo json_encode($_respo);

		exit();
	}

	function insert_document_checklist ( $_proj_id = FALSE ) {

		if ( $_proj_id ) {

			$_dcs	=	[];

			$_inserted	=	0;
			$_updated		=	0;
			$_removed		=	0;
			$_failed		=	0;

			$_inputs	=	$this->input->post();
			if ( $_inputs ) {

				foreach ( $_inputs as $key => $_input ) {

					if ( $key !== '_document_checklist_length' ) {

						$_dcs[$key]['project_id']	=	$_proj_id;
						$_dcs[$key]['document_id']	=	$key;

						foreach ( $_input as $_ikey => $_int ) {

							if ( $_ikey === 'start_date' ) {

								$_dcs[$key][$_ikey]	=	date_format(date_create($_int), 'Y-m-d H:i:s');
							} else {

								$_dcs[$key][$_ikey]	=	$_int;
							}
						}
					} else {

						continue;
					}
				}

				$this->load->model('project/Project_document_model', 'M_p_document');

				if ( $_dcs && !empty($_dcs) ) {

					foreach ( $_dcs as $_dkey => $_dc ) {

						unset($_where);
						$_where['document_id']	=	$_dc['document_id'];
						$_where['project_id']		=	$_dc['project_id'];

						$_pd	=	$this->M_p_document->find_all($_where, FALSE, TRUE); #lqq(); up($_pd);
						if ( $_pd ) {

							unset($_datas);
							foreach ( $_dc as $_dkey => $_d ) {

								if ( ($_dkey !== 'project_id') && ($_dkey !== 'document_id') ) {

									$_datas[$_dkey]	=	$_d;
									
									$_datas[$_dkey]['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_datas[$_dkey]['updated_at']	=	NOW;
								}
							}

							$_update	=	$this->M_p_document->update($_datas, $_pd->id);
							if ( $_update ) {

								$_updated++;
							} else {

								$_failed++;

								break;
							}
						} else {

							unset($_datas);
							$_datas	=	$_dc;

							$_datas['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['created_at']	=	NOW;
							$_datas['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['updated_at']	=	NOW;

							$_insert	=	$this->M_p_document->insert($_datas);
							if ( $_insert ) {

								$_inserted++;
							} else {

								$_failed++;

								break;
							}
						}
					}

					$_msg	=	'';
					if ( $_inserted > 0 ) {

						$_msg	=	$_inserted.' document/s was successfuly inserted';
					}

					if ( $_updated > 0 ) {

						$_msg	.=	($_inserted ? ' and ' : '').$_updated.' document/s was successfuly updated';
					}

					if ( $_failed > 0 ) {

						$this->notify->error('Something went wrong! Please refresh the page and try again.', 'project/document_checklist/'.$_proj_id);
					} else {

						$this->notify->success($_msg.'.', 'project/document_checklist/'.$_proj_id);
					}
				}
			}
		} else {

			show_404();
		}
	}

	function process_checklist ( $_proj_id = FALSE ) {

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('project/Project_document_model', 'M_p_document');

		if ( $_proj_id ) {

			$this->view_data['_proj_id']	=	$_proj_id;

			// ud(Dropdown::get_static('document_owner'));

			$_documents	=	$this->M_p_document->_get_document_checklist($_proj_id); #lqq(); ud($_documents);
			if ( $_documents ) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('process_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function process_checklist_upload ( $_proj_id = FALSE, $_doc_id = FALSE ) {

		if ( $_proj_id && $_doc_id && isset($_FILES['_file']['name']) && ($_FILES['_file']['name'] !== '') ) {

			$this->load->model('project/Project_document_model', 'M_p_document');

			unset($_where);
			$_where['document_id']	=	$_doc_id;
			$_where['project_id']		=	$_proj_id;

			$_p_document	=	$this->M_p_document->find_all($_where, ['id'], TRUE);
			if ( $_p_document && isset($_p_document->id) && $_p_document->id ) {

				$_ext	=	['.jpg', '.jpeg', '.png'];

				set_time_limit(0);

				unset($_config);
				$_config['upload_path']	=	$_location	=	'assets/img/_project/_documents/_process';
				$_config['allowed_types'] = 'jpeg|jpg|png';
				$_config['overwrite'] = TRUE;
				$_config['max_size'] = '1000000';
				$_config['file_name'] = $_filename = $_doc_id.'_thumb';

				if ( isset($_ext) && $_ext ) {

					$_image	=	$_location.'/'.$_filename;

					foreach ( $_ext as $key => $x ) {

						if ( file_exists($_image.$x) ) {

							unlink($_image.$x);
						}
					}
				}

				$this->load->library('upload', $_config);

				if ( !$this->upload->do_upload('_file') ) {

					// ud($this->upload->displaY_errors());

					$this->notify->error($this->upload->displaY_errors(), 'project/process_checklist/'.$_proj_id);
				} else {

					$_uploaded_document_id	=	FALSE;

					$this->load->model('document/Project_document_upload_model', 'M_uploaded_project_document');

					// ud($this->upload->data());

					$_udatas	=	$this->upload->data();
					if ( $_udatas ) {

						unset($_where);
						$_where['project_id']	=	$_proj_id;
						$_where['document_id']				=	$_doc_id;

						$_uploaded_document	=	$this->M_uploaded_project_document->find_all($_where, ['id'], TRUE); #lqq(); ud($_uploaded_document);
						if ( $_uploaded_document && isset($_uploaded_document->id) && $_uploaded_document->id ) {

							$_uploaded_document_id	=	$_uploaded_document->id;

							unset($_update);
							$_update['project_id']	=	$_proj_id;
							$_update['document_id']	=	$_doc_id;
							$_update['file_name']		=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_update['file_type']		=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_update['file_src']		=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_update['file_path']		=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_update['full_path']		=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_update['raw_name']		=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_update['orig_name']		=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_update['client_name']	=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_update['file_ext']		=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_update['file_size']		=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_update['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']	=	NOW;

							$_updated	=	$this->M_uploaded_project_document->update($_update, $_uploaded_document_id);
							if ( !$_updated ) {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'project/process_checklist/'.$_proj_id);
							}
						} else {

							unset($_insert);
							$_insert['project_id']	=	$_proj_id;
							$_insert['document_id']	=	$_doc_id;
							$_insert['file_name']		=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_insert['file_type']		=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_insert['file_src']		=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_insert['file_path']		=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_insert['full_path']		=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_insert['raw_name']		=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_insert['orig_name']		=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_insert['client_name']	=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_insert['file_ext']		=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_insert['file_size']		=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_insert['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['created_at']	=	NOW;
							$_insert['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['updated_at']	=	NOW;

							$_inserted	=	$this->M_uploaded_project_document->insert($_insert);
							if ( $_inserted ) {

								$_uploaded_document_id	=	$this->db->insert_id();
							} else {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'project/process_checklist/'.$_proj_id);
							}
						}

						if ( $_uploaded_document_id ) {

							unset($_update);
							$_update['uploaded_document_id']	=	$_uploaded_document_id;
							$_update['updated_by']						=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']						=	NOW;

							$_updated	=	$this->M_p_document->update($_update, $_p_document->id);
							if ( $_updated ) {

								$this->notify->success('Success! Image uploaded.', 'project/process_checklist/'.$_proj_id);
							} else {

								$this->notify->error('Upload Failde. Please refresh the page and try again.', 'project/process_checklist/'.$_proj_id);
							}
						} else {

							$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project/process_checklist/'.$_proj_id);
						}
					}
				}
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}
}
