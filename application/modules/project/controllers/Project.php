<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Project extends MY_Controller
{

	public $filter_var = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('project/Project_model', 'M_project');
		$this->load->model('property/Property_model', 'M_property');
		$this->load->model('house/House_model', 'M_house');
		$this->load->model('company/Company_model', 'M_company');

		// Load pagination library 
		$this->load->library('ajax_pagination');

		// Load Helper
		$this->load->helper('images');

		// Per page limit 
		$this->perPage = 12;

		$this->_table_fillables		=	$this->M_project->fillable;
		$this->_table_columns		=	$this->M_project->__get_columns();
	}

	public function get_all()
	{
		$data['row'] = $this->M_project
			->fields(array('id', 'name'))
			->with_buyers()
			->get_all();

		echo json_encode($data);
	}

	public function index()
	{

		$_fills	=	$this->_table_fillables;
		$_colms	=	$this->_table_columns;
		$_table =	$this->M_project->table;

		$this->view_data['_fillables']	=	$this->__get_fillables($_colms, $_fills, $_table); #pd($this->view_data['_fillables']);
		$this->view_data['_columns']		=	$this->__get_columns($_fills); #ud($this->view_data['_columns']);

		// Get record count 
		$this->view_data['totalRec'] = $totalRec = $this->M_project->count_rows();
		// $this->view_data['totalRec'] = $totalRec = 0;

		// Pagination configuration 
		$config['target']      = '#projectContent';
		$config['base_url']    = base_url('project/paginationData');
		$config['total_rows']  = $totalRec;
		$config['per_page']    = $this->perPage;
		$config['link_func']   = 'ProjectPagination';

		// Initialize pagination library 
		$this->ajax_pagination->initialize($config);

		// Get records 
		$this->view_data['records'] = $this->M_project->with_company('fields: name')->as_array()->limit($this->perPage, 0)->get_all();
		// $this->view_data['records'] = [];

		// $this->view_data['records'] = [];

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index', $this->view_data);
	}

	public function showProjects()
	{
		$output = ['data' => ''];

		$columnsDefault = [
			'id' => true,
			'name' => true,
			'company' => true,
			'company_id' => true,
			/* ==================== begin: Add model fields ==================== */
			'location' => true,
			'project_area' => true,
			'no_saleable_unit' => true,
			'status' => true,
			'created_by' => true,
			'updated_by' => true
			/* ==================== end: Add model fields ==================== */
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}

		// get all raw data
		// $project = $this->M_project->as_array()->get_all();
		$project = $this->M_project->with_company()->as_array()->get_all();
		// vdebug($buyer);
		$data = [];

		if ($project) {

			// foreach ($project as $d) {
			//     $data[] = $this->filterArray($d, $columnsDefault);
			// }

			foreach ($project as $key => $value) {
				# code...
				//$r = get_payee($value);

				$project[$key]['company'] = isset($value['company']) && $value['company'] ? $value['company']['name'] : 'N/A';

				$project[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . view_date($value['created_at']) . '</div></div>';

				$project[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . view_date($value['updated_at']) . '</div></div>';
			}

			foreach ($project as $d) {
				$data[] = $this->filterArray($d, $columnsDefault);
			}

			// count data
			$totalRecords = $totalDisplay = count($data);

			// filter by general search keyword
			if (isset($_REQUEST['search'])) {
				$data = $this->filterKeyword($data, $_REQUEST['search']);
				$totalDisplay = count($data);
			}

			if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
				foreach ($_REQUEST['columns'] as $column) {
					if (isset($column['search'])) {
						$data = $this->filterKeyword($data, $column['search'], $column['data']);
						$totalDisplay = count($data);
					}
				}
			}

			// sort
			if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

				$_column = $_REQUEST['order'][0]['column'] - 1;
				$_dir = $_REQUEST['order'][0]['dir'];

				usort($data, function ($x, $y) use ($_column, $_dir) {

					// echo "<pre>";
					// print_r($x) ;echo "<br>";
					// echo $_column;echo "<br>";

					$x = array_slice($x, $_column, 1);

					// vdebug($x);

					$x = array_pop($x);

					$y = array_slice($y, $_column, 1);
					$y = array_pop($y);

					if ($_dir === 'asc') {

						return $x > $y ? true : false;
					} else {

						return $x < $y ? true : false;
					}
				});
			}

			// pagination length
			if (isset($_REQUEST['length'])) {
				$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
			}

			// return array values only without the keys
			if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
				$tmp = $data;
				$data = [];
				foreach ($tmp as $d) {
					$data[] = array_values($d);
				}
			}
			$secho = 0;
			if (isset($_REQUEST['sEcho'])) {
				$secho = intval($_REQUEST['sEcho']);
			}

			$output = array(
				'sEcho' => $secho,
				'sColumns' => '',
				'iTotalRecords' => $totalRecords,
				'iTotalDisplayRecords' => $totalDisplay,
				'data' => $data,
			);
		}

		echo json_encode($output);
		exit();
	}

	public function paginationData()
	{
		if ($this->input->is_ajax_request()) {

			$keyword = $this->input->post('keyword');

			$name = $this->input->post('name');
			$type = $this->input->post('type');
			$company = $this->input->post('company');
			$status = $this->input->post('status');

			$page = $this->input->post('page');

			if (!$page) {
				$offset = 0;
			} else {
				$offset = $page;
			}

			$this->view_data['totalRec'] = $totalRec = $this->M_project->count_rows();
			// Pagination configuration 
			$config['target']      = '#projectContent';
			$config['base_url']    = base_url('project/paginationData');
			$config['total_rows']  = $totalRec;
			$config['per_page']    = $this->perPage;
			$config['link_func']   = 'ProjectPagination';

			// Query
			if (!empty($keyword)) :
				$this->db->group_start();
				$this->db->like('name', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (!empty($name)) :

				$this->db->group_start();
				$this->db->like('name', $name, 'both');
				$this->db->group_end();
			endif;

			if (!empty($company)) :

				$this->db->where('company_id', $company);
			endif;

			if (!empty($type)) :

				$this->db->where('type', $type);
			endif;

			if (!empty($status)) :

				$this->db->where('status', $status);
			endif;

			$totalRec = $this->M_project->count_rows();

			// Pagination configuration 
			$config['total_rows']  = $totalRec;

			// Initialize pagination library 
			$this->ajax_pagination->initialize($config);

			// Query
			if (!empty($keyword)) :
				$this->db->group_start();
				$this->db->like('name', $keyword, 'both');
				$this->db->group_end();
			endif;

			if (!empty($name)) :

				$this->db->group_start();
				$this->db->like('name', $name, 'both');
				$this->db->group_end();
			endif;

			if (!empty($company)) :

				$this->db->where('company_id', $company);
			endif;

			if (!empty($type)) :

				$this->db->where('type', $type);
			endif;

			if (!empty($status)) :

				$this->db->where('status', $status);
			endif;

			$this->view_data['records'] = $records = $this->M_project->with_company('fields: name')
				->limit($this->perPage, $offset)
				->get_all();


			$this->load->view('project/_filter', $this->view_data, false);
		}
	}

	public function view($id = FALSE, $_type = 'build')
	{
		$this->js_loader->queue("//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM");
		$this->js_loader->queue("vendors/custom/gmaps/gmaps.js");

		if ($id) {

			$this->view_data['project'] = $this->M_project->with_company('fields: id, name')->get($id);
			$this->view_data['house'] = $this->M_house->where('project_id', $id)->as_array()->get_all();

			$this->view_data['bp'] = $this->M_property->where(array('project_id' => $id, 'hlurb_classification_id' => 2))->count_rows();
			$this->view_data['pd'] = $this->M_property->where(array('project_id' => $id, 'hlurb_classification_id' => 1))->count_rows();
			$this->view_data['sh'] = $this->M_property->where(array('project_id' => $id, 'hlurb_classification_id' => 3))->count_rows();
			$this->view_data['saleable_units'] = $this->M_property->where('project_id', $id)->count_rows();


			if ($this->view_data['project'] || $this->view_data['house']) {

				if ($_type == "json") {
					echo json_encode($this->view_data);
				} else {
					$this->template->build('view', $this->view_data);
				}
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function blocks($id = FALSE, $_type = 0)
	{
		if ($id) {
			$this->db->order_by('total_selling_price', 'DESC');
			$max_price = $this->M_property->fields('total_selling_price')->where('project_id', $id)->get()['total_selling_price'];
			$this->view_data['max_price'] = $max_price;
			$this->view_data['project_id'] = $id;
			$this->template->build('blocks', $this->view_data);
		} else {
			redirect('/project');
		}
	}

	public function load_blocks()
	{
		$this->db->query('SET SESSION sql_mode =
                  REPLACE(REPLACE(REPLACE(
                  @@sql_mode,
                  "ONLY_FULL_GROUP_BY,", ""),
                  ",ONLY_FULL_GROUP_BY", ""),
                  "ONLY_FULL_GROUP_BY", "")');
		$data = [];
		$bg_colors = array(
			'1' => 'table-success',
			'2' => 'table-warning',
			'3' => 'table-danger',
			'4' => 'table-info',
			'5' => 'table-gray'
		);
		if ($this->input->post()) {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			$model = $this->input->post('model');
			$progress_f = $this->input->post('progress_f');
			$progress_c = $this->input->post('progress_c');
			$price_f = $this->input->post('price_f');
			$price_c = $this->input->post('price_c');
			$is_default = $this->input->post('is_default');

			$this->view_data['properties'] = $properties = $this->M_property->where('project_id', $id)->get_all();

			$this->db->group_by('block');
			$this->view_data['blocks'] = $this->M_property->fields('block')->where('project_id', $id)->count_rows();
			$this->view_data['colors'] = $bg_colors;
			$this->view_data['is_default'] = $is_default;
			if ($is_default != 'default') {
				foreach ($properties as $key => $property) {
					$count = 0;
					$properties[$key]['color'] = 'table-gray';
					if ($property['progress'] >= $progress_f && $property['progress'] <= $progress_c) {
						$count++;
					}
					if ($property['total_selling_price'] >= $price_f && $property['total_selling_price'] <= $price_c) {
						$count++;
					}
					if ($count == 2) {
						if ($status || $model) {
							$count_n = 0;
							if ($status) {
								if ($status == $property['status']) {
									$count_n++;
								}
							} else {
								$count_n++;
							}
							if ($model) {
								if ($model == $property['model_id']) {
									$count_n++;
								}
							} else {
								$count_n++;
							}
							if ($count_n == 2) {
								$properties[$key]['color'] = 'table-success';
							}
						} else {
							$properties[$key]['color'] = 'table-success';
						}
					}
				}
				$this->view_data['properties'] = $properties;
			}


			$data['html'] = $this->load->view('_blocks_table', $this->view_data, true);
		} else {
			redirect('/project');
		}
		echo json_encode($data);
		exit();
	}

	public function create()
	{
		if (!$this->ion_auth->is_admin()) {
			redirect('403 page');
		}

		$this->css_loader->queue("vendors/general/select2/dist/css/select2.css");
		$this->js_loader->queue("vendors/general/select2/dist/js/select2.full.js");

		if ($this->input->post()) {
			$post = $this->input->post();

			$upload_path = './assets/uploads/project/photo';
			$config = array();
			$config['upload_path'] = $upload_path;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 5000;
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config, 'project_photo');
			$this->project_photo->initialize($config);

			if ($_FILES['project_image']['name']) {
				if (!$this->project_photo->do_upload('project_image')) {
					$this->notify->error($this->project_photo->display_errors(), 'project/create');
				} else {
					$p_data = $this->project_photo->data();

					$post['image'] = $p_data['file_name'];
				}
			}

			// echo $this->session->userdata('user_id'); exit;
			$additional = array(
				'created_by' => $this->user->id,
				'created_at' => NOW
			);

			$result = $this->M_project->insert($post + $additional);

			if ($result === FALSE) {

				// Validation
				$this->notify->error('Oops something went wrong.');
			} else {

				// Success
				$this->notify->success('Project successfully created.', 'project');
			}
		}

		$this->template->build('create');
	}

	public function update($id = FALSE)
	{
		$this->css_loader->queue("vendors/general/select2/dist/css/select2.css");
		$this->js_loader->queue("vendors/general/select2/dist/js/select2.full.js");

		if ($id) {
			$this->view_data['project'] = $data = $this->M_project->as_array()->get($id);

			if ($data) {
				if ($this->input->post()) {
					$post = $this->input->post();

					$upload_path = './assets/uploads/project/photo';
					$config = array();
					$config['upload_path'] = $upload_path;
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size'] = 5000;
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload', $config, 'project_photo');
					$this->project_photo->initialize($config);

					if ($_FILES['project_image']['name']) {
						if (!$this->project_photo->do_upload('project_image')) {

							$this->notify->error($this->project_photo->display_errors(), 'project/create');
						} else {

							unlink($upload_path . '/' . $data['image']);

							$p_data = $this->project_photo->data();

							$post['image'] = $p_data['file_name'];
						}
					} else {

						$post['image'] = $data['image'];
					}

					$additional = array(
						'updated_by' => $this->user->id,
						'updated_at' => NOW
					);

					$result = $this->M_project->update($post + $additional, $data['id']);

					if ($result === FALSE) {

						// Validation
						$this->notify->error('Oops something went wrong.');
					} else {

						// Success
						$this->notify->success('Project successfully updated.', 'project');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}

	public function delete($id = FALSE)
	{
		$id = $this->input->post('id');

		$result = (bool) $this->M_project->delete($id);
		if ($result) {
			$fields = array(
				'deleted_by' => $this->session->userdata('user_id')
			);

			$this->M_project->rules['update'] = $this->M_project->delete_fields;

			$project_result = $this->M_project->update($fields, $id);

			$response['status']  = TRUE;
			$response['message'] = 'Project Deleted Successfully ...';
		} else {

			$response['status']  = FALSE;
			$response['message'] = 'Unable to delete Project ...';
		}

		echo json_encode($response);
	}

	public function checklist()
	{
		$this->css_loader->queue("vendors/custom/datatables/datatables.bundle.css");
		$this->js_loader->queue("vendors/custom/datatables/datatables.bundle.js");

		$this->template->build('checklist');
	}

	public function document()
	{
		$this->css_loader->queue("vendors/custom/datatables/datatables.bundle.css");
		$this->js_loader->queue("vendors/custom/datatables/datatables.bundle.js");

		$this->template->build('document');
	}

	function export()
	{

		$_db_columns	=	[];
		$_alphas			=	[];
		$_datas				=	[];

		$_titles[]	=	'#';
		// $_titles[]	=	'ID';

		$_start	=	3;
		$_row		=	2;
		$_no		=	1;

		$records	=	$this->M_project->as_array()->get_all();
		if ($records) {

			foreach ($records as $_dkey => $_proj) {

				$_datas[$_proj['id']]['#']	=	$_no;
				// $_datas[$_proj['id']]['ID']	=	isset($_proj['id']) && $_proj['id'] ? $_proj['id'] : $_no;

				$_no++;
			}

			$_filename	=	'list_of_projects_' . date('m_d_y_h-i-s', time()) . '.xls';

			$_objSheet	=	$this->excel->getActiveSheet();

			if ($this->input->post()) {

				$_export_column	=	$this->input->post('_export_column');
				if ($_export_column) {

					foreach ($_export_column as $_ekey => $_column) {

						$_db_columns[$_ekey]	=	isset($_column) && $_column ? $_column : '';
					}
				} else {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project');
				}
			} else {

				$_filename	=	'list_of_projects_' . date('m_d_y_h-i-s', time()) . '.csv';

				// $_db_columns	=	$this->M_document->fillable;
				$_db_columns	=	$this->_table_fillables;
				if (!$_db_columns) {

					$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project');
				}
			}

			if ($_db_columns) {

				foreach ($_db_columns as $key => $_dbclm) {

					$_name	=	isset($_dbclm) && $_dbclm ? $_dbclm : '';

					if ((strpos($_name, 'created_') === FALSE) && (strpos($_name, 'updated_') === FALSE) && (strpos($_name, 'deleted_') === FALSE) && ($_name !== 'id')) {

						if ((strpos($_name, '_id') !== FALSE)) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('_id', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($records as $_dkey => $_proj) {

								if ($_column === 'company_id') {

									$_datas[$_proj['id']][$_title]	=	isset($_proj[$_column]) && $_proj[$_column] ? Dropdown::get_dynamic('companies', $_proj[$_column], 'name', 'id', 'view') : '';
								} else {

									$_datas[$_proj['id']][$_title]	=	isset($_proj[$_column]) && $_proj[$_column] ? $_proj[$_column] : '';
								}
							}
						} elseif ((strpos($_name, 'is_') !== FALSE)) {

							$_column	=	$_name;

							$_name	=	isset($_name) && $_name ? str_replace('is_', '', $_name) : '';

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($records as $_dkey => $_proj) {

								$_datas[$_proj['id']][$_title]	=	isset($_proj[$_column]) && ($_proj[$_column] !== '') ? Dropdown::get_static('bool', $_proj[$_column], 'view') : '';
							}
						} else {

							$_column	=	$_name;

							$_titles[]	=	$_title =	isset($_name) && $_name ? str_replace('_', ' ', $_name) : '';

							foreach ($records as $_dkey => $_proj) {

								if ($_column === 'type') {

									$_datas[$_proj['id']][$_title]	=	isset($_proj[$_column]) && $_proj[$_column] ? Dropdown::get_static('project_type', $_proj[$_column], 'view') : '';
								} else if ($_column === 'status') {

									$_datas[$_proj['id']][$_title]	=	isset($_proj[$_column]) && $_proj[$_column] ? Dropdown::get_static('project_status', $_proj[$_column], 'view') : '';
								} else {

									$_datas[$_proj['id']][$_title]	=	isset($_proj[$_name]) && $_proj[$_name] ? $_proj[$_name] : '';
								}
							}
						}
					} else {

						continue;
					}
				}

				$_alphas	=	$this->__get_excel_columns(count($_titles));

				$_xls_columns	=	array_combine($_alphas, $_titles);
				$_firstAlpha	=	reset($_alphas);
				$_lastAlpha		=	end($_alphas);

				foreach ($_xls_columns as $_xkey => $_column) {

					$_title	=	($_column !== 'ID') ? ucwords(strtolower($_column)) : $_column;

					$_objSheet->setCellValue($_xkey . $_row, $_title);
				}

				$_objSheet->setTitle('List of Projects');
				$_objSheet->setCellValue('A1', 'LIST OF PROJECTS');
				$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							if ($_xls_columns[$_alpha] === 'image') {

								// if( ! empty($_value) ){
								// 	$objDrawing = new PHPExcel_Worksheet_Drawing();    //create object for Worksheet drawing

								// 	$objDrawing->setName('Project Photo');        //set name to image

								// 	$objDrawing->setDescription('Customer Signature'); //set description to image

								// 	$objDrawing->setPath('./assets/uploads/project/photo/'.$_value);

								// 	$objDrawing->setCoordinates($_alpha . $_start);        //set image to cell

								// 	$objDrawing->setOffsetX(5); 
								// 	$objDrawing->setOffsetY(5); 

								// 	$objDrawing->setWidth(32);                 //set width, height
								// 	$objDrawing->setHeight(32);  

								// 	$objDrawing->setWorksheet($this->excel->getActiveSheet());  //save
								// }else{

								// 	$_objSheet->setCellValue($_alpha . $_start, 'No Image Found');
								// }


							} else {

								$_objSheet->setCellValue($_alpha . $_start, $_value);
							}
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}

				foreach ($_alphas as $_alpha) {

					$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
				}

				$_style	=	array(
					'font'  => array(
						'bold'	=>	TRUE,
						'size'	=>	10,
						'name'	=>	'Verdana'
					)
				);
				$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="' . $_filename . '"');
				header('Cache-Control: max-age=0');
				$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				@ob_end_clean();
				$_objWriter->save('php://output');
				@$_objSheet->disconnectWorksheets();
				unset($_objSheet);
			} else {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project');
			}
		} else {

			$this->notify->error('No Record Found', 'project');
		}
	}

	function export_csv()
	{

		if ($this->input->post() && ($this->input->post('update_existing_data') !== '')) {

			$_ued	=	$this->input->post('update_existing_data');

			$_is_update	=	$_ued === '1' ? TRUE : FALSE;

			$_alphas		=	[];
			$_datas			=	[];

			$_titles[]	=	'id';

			$_start	=	3;
			$_row		=	2;

			$_filename	=	'Project CSV Template.csv';

			// $_fillables	=	$this->M_document->fillable;
			$_fillables	=	$this->_table_fillables;
			if (!$_fillables) {

				$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project');
			}

			foreach ($_fillables as $_fkey => $_fill) {

				if ((strpos($_fill, 'created_') === FALSE) && (strpos($_fill, 'updated_') === FALSE) && (strpos($_fill, 'deleted_') === FALSE)) {

					$_titles[]	=	$_fill;
				} else {

					continue;
				}
			}

			if ($_is_update) {

				$records	=	$this->M_project->as_array()->get_all(); #up($_documents);
				if ($records) {

					foreach ($_titles as $_tkey => $_title) {

						foreach ($records as $_dkey => $record) {

							$_datas[$record['id']][$_title]	=	isset($record[$_title]) && ($record[$_title] !== '') ? $record[$_title] : '';
						}
					}
				}
			} else {

				if (isset($_titles[0]) && $_titles[0] && strtolower($_titles[0]) === 'id') {

					unset($_titles[0]);
				}
			}

			$_alphas			=	$this->__get_excel_columns(count($_titles));
			$_xls_columns	=	array_combine($_alphas, $_titles);
			$_firstAlpha	=	reset($_alphas);
			$_lastAlpha		=	end($_alphas);

			$_objSheet	=	$this->excel->getActiveSheet();
			$_objSheet->setTitle('Projects');
			$_objSheet->setCellValue('A1', 'PROJECTS');
			$_objSheet->mergeCells('A1:' . $_lastAlpha . '1');

			foreach ($_xls_columns as $_xkey => $_column) {

				$_objSheet->setCellValue($_xkey . $_row, $_column);
			}

			if ($_is_update) {

				if (isset($_datas) && $_datas) {

					foreach ($_datas as $_dkey => $_data) {

						foreach ($_alphas as $_akey => $_alpha) {

							$_value	=	isset($_data[$_xls_columns[$_alpha]]) ? $_data[$_xls_columns[$_alpha]] : '';

							$_objSheet->setCellValue($_alpha . $_start, $_value);
						}

						$_start++;
					}
				} else {

					$_objSheet->setCellValue($_firstAlpha . $_start, 'No Record Found');
					$_objSheet->mergeCells($_firstAlpha . $_start . ':' . $_lastAlpha . $_start);

					$_style	=	array(
						'font'  => array(
							'bold'	=>	FALSE,
							'size'	=>	9,
							'name'	=>	'Verdana'
						)
					);
					$_objSheet->getStyle($_firstAlpha . $_start . ':' . $_lastAlpha . $_start)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}

			foreach ($_alphas as $_alpha) {

				$_objSheet->getColumnDimension($_alpha)->setAutoSize(TRUE);
			}

			$_style	=	array(
				'font'  => array(
					'bold'	=>	TRUE,
					'size'	=>	10,
					'name'	=>	'Verdana'
				)
			);
			$_objSheet->getStyle('A1:' . $_lastAlpha . $_row)->applyFromArray($_style)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="' . $_filename . '"');
			header('Cache-Control: max-age=0');
			$_objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			@ob_end_clean();
			$_objWriter->save('php://output');
			@$_objSheet->disconnectWorksheets();
			unset($_objSheet);
		} else {

			show_404();
		}
	}

	function import()
	{

		if (isset($_FILES['csv_file']['name']) && $_FILES['csv_file']['name'] && isset($_FILES['csv_file']['tmp_name']) && $_FILES['csv_file']['tmp_name']) {

			// if ( isset($_FILES['csv_file']['type']) && $_FILES['csv_file']['type'] && ($_FILES['csv_file']['type'] === 'text/csv') ) {
			if (TRUE) {

				$_tmp_name	=	$_FILES['csv_file']['tmp_name'];
				$_name			=	$_FILES['csv_file']['name'];

				set_time_limit(0);

				$_columns	=	[];
				$_datas		=	[];

				$_inserted	=	0;
				$_updated		=	0;
				$_failed		=	0;

				/**
				 * Read Uploaded CSV File
				 */
				try {

					$_file_type		=	PHPExcel_IOFactory::identify($_tmp_name);
					$_objReader		=	PHPExcel_IOFactory::createReader($_file_type);
					$_objPHPExcel	=	$_objReader->load($_tmp_name);
				} catch (Exception $e) {

					$_msg	=	'Error loading CSV "' . pathinfo($_name, PATHINFO_BASENAME) . '": ' . $e->getMessage();

					$this->notify->error($_msg, 'document');
				}

				$_objWorksheet	=	$_objPHPExcel->getActiveSheet();
				$_highestColumn	=	$_objWorksheet->getHighestColumn();
				$_highestRow		=	$_objWorksheet->getHighestRow();
				$_sheetData			=	$_objWorksheet->toArray();
				if ($_sheetData && isset($_sheetData[1]) && $_sheetData[1] && isset($_sheetData[2]) && $_sheetData[2]) {

					if ($_sheetData[1][0] === 'id') {

						$_columns[]	=	'id';
					}

					// $_fillables	=	$this->M_document->fillable;
					$_fillables	=	$this->_table_fillables;
					if (!$_fillables) {

						$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project');
					}

					foreach ($_fillables as $_fkey => $_fill) {

						if (in_array($_fill, $_sheetData[1])) {

							$_columns[]	=	$_fill;
						} else {

							continue;
						}
					}

					foreach ($_sheetData as $_skey => $_sd) {

						if ($_skey > 1) {

							if (count(array_filter($_sd)) !== 0) {

								$_datas[]	=	array_combine($_columns, $_sd);
							}
						} else {

							continue;
						}
					}

					if (isset($_datas) && $_datas) {

						foreach ($_datas as $_dkey => $_data) {

							$_id	=	isset($_data['id']) && $_data['id'] ? $_data['id'] : FALSE;
							if ($_id) {

								$data	=	$this->M_project->get($_id);
								if ($data) {

									unset($_data['id']);

									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_update	=	$this->M_project->update($_data, $_id);
									if ($_update !== FALSE) {

										$_updated++;
									} else {

										$_failed++;

										break;
									}
								} else {

									$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['created_at']	=	NOW;
									$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_data['updated_at']	=	NOW;

									$_insert	=	$this->M_project->insert($_data);
									if ($_insert !== FALSE) {

										$_inserted++;
									} else {

										$_failed++;

										break;
									}
								}
							} else {

								$_data['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['created_at']	=	NOW;
								$_data['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
								$_data['updated_at']	=	NOW;

								$_insert	=	$this->M_project->insert($_data);
								if ($_insert !== FALSE) {

									$_inserted++;
								} else {

									$_failed++;

									break;
								}
							}
						}

						$_msg	=	'';
						if ($_inserted > 0) {

							$_msg	=	$_inserted . ' record/s was successfuly inserted';
						}

						if ($_updated > 0) {

							$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' record/s was successfuly updated';
						}

						if ($_failed > 0) {

							$this->notify->error('Upload Failed! Please follow upload guide.', 'project');
						} else {

							$this->notify->success($_msg . '.', 'project');
						}
					}
				} else {

					$this->notify->warning('CSV was empty.', 'project');
				}
			} else {

				$this->notify->warning('Not a CSV file!', 'project');
			}
		} else {

			$this->notify->error('Something went wrong!', 'project');
		}
	}

	function document_checklist($_proj_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('project/Project_document_model', 'M_p_document');

		if ($_proj_id) {

			$this->view_data['_proj_id']	=	$_proj_id;

			$_documents	=	$this->M_p_document->_get_document_checklist($_proj_id);
			if ($_documents) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('document_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function documents($_proj_id = FALSE)
	{

		$this->view_data['_largescreen']	=	TRUE;

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('checklist/Checklist_model', 'M_checklist');

		if ($_proj_id) {

			$this->view_data['_proj_id']	=	$_proj_id;

			$_checklists	=	$this->M_checklist->find_all(FALSE, ['id', 'name']);
			if ($_checklists) {

				$this->view_data['_checklists']	=	$_checklists; #ud($_checklists);
			}

			$this->template->build('documents', $this->view_data);
		} else {

			show_404();
		}
	}

	function get_filtered_document_checklist()
	{

		$_respo['_status']	=	0;
		$_respo['_msg']			=	'';
		$_view_data	=	[];

		$_checklist_id	=	$this->input->post('value');
		$_proj_id				=	$this->input->post('proj_id');
		if ($_checklist_id && $_proj_id) {

			$_view_data['_proj_id']	=	$_proj_id;

			$this->load->model('checklist/Document_checklist_model', 'M_document_checklist');

			$_documents	=	$this->M_document_checklist->_get_documents($_checklist_id);
			if ($_documents) {

				$_view_data['_documents']	=	$_documents;
			}

			$_respo['_status']	=	1;

			$_respo['_html']	=	$this->load->view('_checklist/documents', $_view_data, TRUE);
		} else {

			$_respo['_msg']	=	'Oops! Please refresh the page and try again.';
		}

		echo json_encode($_respo);

		exit();
	}

	function insert_document_checklist($_proj_id = FALSE)
	{

		if ($_proj_id) {

			$_dcs	=	[];

			$_inserted	=	0;
			$_updated		=	0;
			$_removed		=	0;
			$_failed		=	0;

			$_inputs	=	$this->input->post();
			if ($_inputs) {

				foreach ($_inputs as $key => $_input) {

					if ($key !== '_document_checklist_length') {

						$_dcs[$key]['project_id']	=	$_proj_id;
						$_dcs[$key]['document_id']	=	$key;

						foreach ($_input as $_ikey => $_int) {

							if ($_ikey === 'start_date') {

								$_dcs[$key][$_ikey]	=	date_format(date_create($_int), 'Y-m-d H:i:s');
							} else {

								$_dcs[$key][$_ikey]	=	$_int;
							}
						}
					} else {

						continue;
					}
				}

				$this->load->model('project/Project_document_model', 'M_p_document');

				if ($_dcs && !empty($_dcs)) {

					foreach ($_dcs as $_dkey => $_dc) {

						unset($_where);
						$_where['document_id']	=	$_dc['document_id'];
						$_where['project_id']		=	$_dc['project_id'];

						$_pd	=	$this->M_p_document->find_all($_where, FALSE, TRUE); #lqq(); up($_pd);
						if ($_pd) {

							unset($_datas);
							foreach ($_dc as $_dkey => $_d) {

								if (($_dkey !== 'project_id') && ($_dkey !== 'document_id')) {

									$_datas[$_dkey]	=	$_d;

									$_datas[$_dkey]['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
									$_datas[$_dkey]['updated_at']	=	NOW;
								}
							}

							$_update	=	$this->M_p_document->update($_datas, $_pd->id);
							if ($_update) {

								$_updated++;
							} else {

								$_failed++;

								break;
							}
						} else {

							unset($_datas);
							$_datas	=	$_dc;

							$_datas['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['created_at']	=	NOW;
							$_datas['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_datas['updated_at']	=	NOW;

							$_insert	=	$this->M_p_document->insert($_datas);
							if ($_insert) {

								$_inserted++;
							} else {

								$_failed++;

								break;
							}
						}
					}

					$_msg	=	'';
					if ($_inserted > 0) {

						$_msg	=	$_inserted . ' document/s was successfuly inserted';
					}

					if ($_updated > 0) {

						$_msg	.=	($_inserted ? ' and ' : '') . $_updated . ' document/s was successfuly updated';
					}

					if ($_failed > 0) {

						$this->notify->error('Something went wrong! Please refresh the page and try again.', 'project/document_checklist/' . $_proj_id);
					} else {

						$this->notify->success($_msg . '.', 'project/document_checklist/' . $_proj_id);
					}
				}
			}
		} else {

			show_404();
		}
	}

	function process_checklist($_proj_id = FALSE)
	{

		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->load->model('project/Project_document_model', 'M_p_document');

		if ($_proj_id) {

			$this->view_data['_proj_id']	=	$_proj_id;

			// ud(Dropdown::get_static('document_owner'));

			$_documents	=	$this->M_p_document->_get_document_checklist($_proj_id); #lqq(); ud($_documents);
			if ($_documents) {

				$this->view_data['_documents']	=	$_documents; #ud($_documents);
				$this->view_data['_total']			=	count($_documents);
			}

			$this->template->build('process_checklist', $this->view_data);
		} else {

			show_404();
		}
	}

	function process_checklist_upload($_proj_id = FALSE, $_doc_id = FALSE)
	{

		if ($_proj_id && $_doc_id && isset($_FILES['_file']['name']) && ($_FILES['_file']['name'] !== '')) {

			$this->load->model('project/Project_document_model', 'M_p_document');

			unset($_where);
			$_where['document_id']	=	$_doc_id;
			$_where['project_id']		=	$_proj_id;

			$_p_document	=	$this->M_p_document->find_all($_where, ['id'], TRUE);
			if ($_p_document && isset($_p_document->id) && $_p_document->id) {

				$_ext	=	['.jpg', '.jpeg', '.png'];

				set_time_limit(0);

				unset($_config);
				$_config['upload_path']	=	$_location	=	'assets/img/_project/_documents/_process';
				$_config['allowed_types'] = 'jpeg|jpg|png';
				$_config['overwrite'] = TRUE;
				$_config['max_size'] = '1000000';
				$_config['file_name'] = $_filename = $_doc_id . '_thumb';

				if (isset($_ext) && $_ext) {

					$_image	=	$_location . '/' . $_filename;

					foreach ($_ext as $key => $x) {

						if (file_exists($_image . $x)) {

							unlink($_image . $x);
						}
					}
				}

				$this->load->library('upload', $_config);

				if (!$this->upload->do_upload('_file')) {

					// ud($this->upload->displaY_errors());

					$this->notify->error($this->upload->displaY_errors(), 'project/process_checklist/' . $_proj_id);
				} else {

					$_uploaded_document_id	=	FALSE;

					$this->load->model('document/Project_document_upload_model', 'M_uploaded_project_document');

					// ud($this->upload->data());

					$_udatas	=	$this->upload->data();
					if ($_udatas) {

						unset($_where);
						$_where['project_id']	=	$_proj_id;
						$_where['document_id']				=	$_doc_id;

						$_uploaded_document	=	$this->M_uploaded_project_document->find_all($_where, ['id'], TRUE); #lqq(); ud($_uploaded_document);
						if ($_uploaded_document && isset($_uploaded_document->id) && $_uploaded_document->id) {

							$_uploaded_document_id	=	$_uploaded_document->id;

							unset($_update);
							$_update['project_id']	=	$_proj_id;
							$_update['document_id']	=	$_doc_id;
							$_update['file_name']		=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_update['file_type']		=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_update['file_src']		=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_update['file_path']		=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_update['full_path']		=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_update['raw_name']		=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_update['orig_name']		=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_update['client_name']	=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_update['file_ext']		=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_update['file_size']		=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_update['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']	=	NOW;

							$_updated	=	$this->M_uploaded_project_document->update($_update, $_uploaded_document_id);
							if (!$_updated) {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'project/process_checklist/' . $_proj_id);
							}
						} else {

							unset($_insert);
							$_insert['project_id']	=	$_proj_id;
							$_insert['document_id']	=	$_doc_id;
							$_insert['file_name']		=	isset($_udatas['file_name']) && $_udatas['file_name'] ? $_udatas['file_name'] : NULL;
							$_insert['file_type']		=	isset($_udatas['file_type']) && $_udatas['file_type'] ? $_udatas['file_type'] : NULL;
							$_insert['file_src']		=	isset($_udatas['full_path']) && $_udatas['full_path'] ? strstr($_udatas['full_path'], 'assets') : NULL;
							$_insert['file_path']		=	isset($_udatas['file_path']) && $_udatas['file_path'] ? $_udatas['file_path'] : NULL;
							$_insert['full_path']		=	isset($_udatas['full_path']) && $_udatas['full_path'] ? $_udatas['full_path'] : NULL;
							$_insert['raw_name']		=	isset($_udatas['raw_name']) && $_udatas['raw_name'] ? $_udatas['raw_name'] : NULL;
							$_insert['orig_name']		=	isset($_udatas['orig_name']) && $_udatas['orig_name'] ? $_udatas['orig_name'] : NULL;
							$_insert['client_name']	=	isset($_udatas['client_name']) && $_udatas['client_name'] ? $_udatas['client_name'] : NULL;
							$_insert['file_ext']		=	isset($_udatas['file_ext']) && $_udatas['file_ext'] ? $_udatas['file_ext'] : NULL;
							$_insert['file_size']		=	isset($_udatas['file_size']) && $_udatas['file_size'] ? $_udatas['file_size'] : NULL;
							$_insert['created_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['created_at']	=	NOW;
							$_insert['updated_by']	=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_insert['updated_at']	=	NOW;

							$_inserted	=	$this->M_uploaded_project_document->insert($_insert);
							if ($_inserted) {

								$_uploaded_document_id	=	$this->db->insert_id();
							} else {

								$this->notify->error('Upload Failed. Please refresh the page and try again.', 'project/process_checklist/' . $_proj_id);
							}
						}

						if ($_uploaded_document_id) {

							unset($_update);
							$_update['uploaded_document_id']	=	$_uploaded_document_id;
							$_update['updated_by']						=	isset($this->user->id) && $this->user->id ? $this->user->id : NULL;
							$_update['updated_at']						=	NOW;

							$_updated	=	$this->M_p_document->update($_update, $_p_document->id);
							if ($_updated) {

								$this->notify->success('Success! Image uploaded.', 'project/process_checklist/' . $_proj_id);
							} else {

								$this->notify->error('Upload Failde. Please refresh the page and try again.', 'project/process_checklist/' . $_proj_id);
							}
						} else {

							$this->notify->error('Something went wrong. Please refresh the page and try again.', 'project/process_checklist/' . $_proj_id);
						}
					}
				}
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function bulkDelete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		if ($this->input->is_ajax_request()) {
			$delete_ids = $this->input->post('deleteids_arr');

			if ($delete_ids) {
				foreach ($delete_ids as $value) {

					$deleted = $this->M_project->delete($value);
				}
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Property Model successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}
}
