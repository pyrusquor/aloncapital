<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Project_model extends MY_Model
{
	public $table = 'projects'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = [
		'name',
		'type',
		'company_id',
		'project_code',
		'image',
		'embedded_map',
		'location',
		'min_price',
		'max_price',
		'project_start_date',
		'dpc_palc_date',
		'cr_lts_date',
		'lts_name',
		'titling_date',
		'status',
		'project_area',
		'saleable_area',
		'no_saleable_unit',
		'coc_date',
		'etd_date',
		'lts_number',
		'pd_units',
		'base_lot',
		'bp_units',
		'zonal_value',
		'fair_market_value',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
		'deleted_by',
		'deleted_at'
	]; // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
	public $rules = [];

	public $fields = [
		'name' => array(
			'field' => 'name',
			'label' => 'Project Name',
			'rules' => 'trim|required'
		),
		'type' => array(
			'field' => 'type',
			'label' => 'Project Type',
			'rules' => 'trim|required'
		),
		'company_id' => array(
			'field' => 'company_id',
			'label' => 'Company',
			'rules' => 'trim'
		),
		'embedded_map' => array(
			'field' => 'embedded_map',
			'label' => 'Embedded Map',
			'rules' => 'trim'
		),
		'location' => array(
			'field' => 'location',
			'label' => 'Location',
			'rules' => 'trim|required'
		),
		'min_price' => array(
			'field' => 'min_price',
			'label' => 'minimum Price',
			'rules' => 'trim'
		),
		'max_price' => array(
			'field' => 'max_price',
			'label' => 'Maximum Price',
			'rules' => 'trim'
		),
		'project_start_date' => array(
			'field' => 'project_start_date',
			'label' => 'Project Start Date',
			'rules' => 'trim'
		),
		'dpc_palc_date' => array(
			'field' => 'dpc_palc_date',
			'label' => 'DPC PALC Date',
			'rules' => 'trim'
		),
		'cr_lts_date' => array(
			'field' => 'cr_lts_date',
			'label' => 'CR LTS Date',
			'rules' => 'trim'
		),
		'titling_date' => array(
			'field' => 'titling_date',
			'label' => 'Titling Date',
			'rules' => 'trim'
		),
		'status' => array(
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'trim|required'
		),
		'project_area' => array(
			'field' => 'project_area',
			'label' => 'Project Area',
			'rules' => 'trim'
		),
		'saleable_area' => array(
			'field' => 'saleable_area',
			'label' => 'Saleable Area',
			'rules' => 'trim'
		),
		'no_saleable_unit' => array(
			'field' => 'no_saleable_unit',
			'label' => 'No of Saleable Units',
			'rules' => 'trim'
		),
		'coc_date' => array(
			'field' => 'coc_date',
			'label' => 'COC Date',
			'rules' => 'trim'
		),
		'etd_date' => array(
			'field' => 'etd_date',
			'label' => 'ETD Date',
			'rules' => 'trim'
		),
		'lts_number' => array(
			'field' => 'lts_number',
			'label' => 'LTS Number',
			'rules' => 'trim'
		),
		'PD_units' => array(
			'field' => 'PD_units',
			'label' => 'PD957 Units',
			'rules' => 'trim'
		),
		'BP_units' => array(
			'field' => 'BP_units',
			'label' => 'BP220 Units',
			'rules' => 'trim'
		)
	];

	public $delete_fields = [
		'deleted_by' => array(
			'field' => 'deleted_by',
			'label' => 'Deleted By',
			'rules' => 'trim'
		)
	];



	public function __construct()
	{
		parent::__construct();

		$this->soft_deletes = true;
		$this->return_as = 'array';

		$this->rules['insert']	=	$this->fields;
		$this->rules['update']	=	$this->fields;

		$this->pagination_delimiters = array('<li class="kt-pagination__link--next">', '</li>');
		$this->pagination_arrows = array('<i class="fa fa-angle-left kt-font-brand"></i>', '<i class="fa fa-angle-right kt-font-brand"></i>');

		$this->has_one['company'] = array('foreign_model' => 'company/Company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');

		$this->has_many_pivot['buyers'] = array(
			'foreign_model' => 'buyer/Buyer_model',
			'pivot_table' => 'transactions',
			'local_key' => 'id',
			'pivot_local_key' => 'project_id',
			'pivot_foreign_key' => 'buyer_id',
			'foreign_key' => 'id',
			'get_relate' => FALSE
		);
	}

	public function get_columns()
	{
		$_return = false;

		if ($this->fillable) {
			$_return = $this->fillable;
		}

		return $_return;
	}
}
