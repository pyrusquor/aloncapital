

<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Project</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="total"></span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search Project..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <!-- <a href="<?php echo site_url('company/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
					<i class="fa fa-plus"></i> Create
				</a>
				<button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
					<i class="fa fa-filter"></i> Filter
				</button> -->
                <?php if ($this->ion_auth->is_admin()) { ?>

                <a class="btn btn-label-primary btn-elevate btn-sm" href="sub_project">
                    Sub Projects
                </a>
                <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
                    <i class="fa fa-upload"></i> Import
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#_export_option">
                    <i class="fa fa-download"></i> Export
                </button>

                <?php } ?>

            </div>
        </div>
    </div>
</div>


<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <?php if ($this->ion_auth->is_admin()) { ?>
        <div class="module__create">
            <a href="<?php echo site_url('project/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Create Project
            </a>
        </div>
        <?php } ?>
        <div class="module__filter">
            <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                <i class="fa fa-filter"></i> Filter
            </button>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">

        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">Project Name</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="name" class="form-control form-control-sm _filter" placeholder="Name" id="_column_2" data-column="2">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building-o"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Location</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="company_address" class="form-control form-control-sm _filter" placeholder="Address" id="_column_3" data-column="3">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Company:</label>
                                <div class="kt-input-icon">
                                    <select type="text" name="login_history_id" class="form-control form-control-sm _filter suggests" placeholder="Created By" data-module="companies" id="_column_4" data-column="4">
                                        <option value="">Select option</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label class="form-control-label">No. of Saleable Units:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="name" class="form-control form-control-sm _filter" placeholder="Name" id="_column_6" data-column="6">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building-o"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Status</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php echo form_dropdown('project_status', Dropdown::get_static('project_status'), '', 'class="form-control form-control-sm _filter" id="_column_7" data-column="7"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-map-marker"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label">Total Project Area:</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="number" name="company_contact_info" class="form-control form-control-sm _filter" placeholder="Contact" id="_column_5" data-column="5">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-phone"></i></span></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
            </div>
        </div>

        <div id="_batch_upload" class="collapse kt-margin-b-35 kt-margin-t-10">
            <form class="kt-form" id="export_csv" action="<?php echo site_url('company/export_csv'); ?>" method="POST">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Document Type</label>
                            <div class="kt-input-icon  kt-input-icon--left">
                                <select class="form-control form-control-sm" name="status" id="import_status">
                                    <option value=""> -- Update Existing Data -- </option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
                            </div>
                            <?php echo form_error('status'); ?>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <form class="kt-form" id="upload_form" action="<?php echo site_url('company/import'); ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Upload CSV file:</label>
                            <label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
                            <input type="file" name="file" class="" size="1000" accept="*.csv">
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group form-group-last row custom_import_style">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm disabled" form="upload_form">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                        <div class="col-lg-6 kt-align-right">
                            <button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm disabled" form="export_csv">
                                <i class="fa fa-file-csv"></i>
                            </button>
                            <button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm disabled" disabled id="btn_upload_guide" data-toggle="modal" data-target="#upload_guide">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="project_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Project Name</th>
                    <th>Company</th>
                    <th>Company ID</th>
                    <th>Location</th>
                    <th>Total Project Area</th>
                    <th>No. of Saleable Units</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Last Update By</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>

<!-- EXPORT MODAL -->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="_export_form" target="_blank" action="<?php echo site_url('company/export'); ?>" method="POST">
                    <div class="row">
                        <?php if (isset($columns) && $columns) : ?>

                            <div class="col-lg-11 offset-lg-1">
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--bold">
                                        <input type="checkbox" id="_export_select_all"> Field
                                        <span></span>
                                    </label>
                                    <label class="kt-checkbox kt-checkbox--bold"></label>
                                </div>
                            </div>

                            <?php foreach ($columns as $key => $column) :  ?>

                                <?php if ($column) : ?>

                                    <?php
                                    $offset    =    '';
                                    if ($column === reset($columns)) {

                                        $offset    =    'offset-lg-1';
                                    }
                                    ?>

                                    <div class="col-lg-5 <?php echo isset($offset) && $offset ? $offset : ''; ?>">
                                        <div class="kt-checkbox-list">
                                            <?php foreach ($column as $ckey => $clm) : ?>

                                                <?php
                                                $label    =    isset($clm['label']) && $clm['label'] ? $clm['label'] : '';
                                                $value    =    isset($clm['value']) && $clm['value'] ? $clm['value'] : '';
                                                ?>

                                                <label class="kt-checkbox kt-checkbox--bold">
                                                    <input type="checkbox" name="_export_column[]" class="_export_column" value="<?php echo @$value; ?>"> <?php echo @$label; ?>
                                                    <span></span>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else : ?>

                            <div class="col-lg-10 offset-lg-1">
                                <div class="form-group form-group-last">
                                    <div class="alert alert-solid-danger alert-bold fade show" role="alert" id="form_msg">
                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                        <div class="alert-text">
                                            Something went wrong. Please contact your system administrator.
                                        </div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left" data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm btn-font-sm pull-right" form="_export_form">
                        <i class="fa fa-file-export"></i> Export
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- UPLOAD GUIDE MODAL -->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="upload_guide_label">Company - Upload Guide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="upload_guide_table">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Format</th>
                            <th>Options</th>
                            <th>Required</th>
                        </tr>
                    </thead>
                </table>
                <!--end: Datatable -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>