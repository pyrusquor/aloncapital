<?php
// vdebug($properties);
?>
<!-- CONTENT --> 
<div class="kt-container--responsive  kt-grid__item kt-grid__item--responsive">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet kt-portlet--height-responsive">
                <div class="kt-portlet__body">
                    <div class="kt-section">
                        <div class="kt-section__content" style="overflow: scroll";>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Block</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i=0; $i < $blocks; $i++) {  ?>
                                        <tr>
                                            <th><?=$i;?></th>

                                            <?php foreach ($properties as $key => $property) { if($property['block'] == $i){  ?>
                                                <?php if($is_default=='default'): ?>
                                                <th class="<?=$colors[$property['status']];?>">
                                                <?php else: ?>
                                                <th class="<?=$property['color'];?>">
                                                <?php endif?>
                                                    <a class='specific_property' href="javascript:void(0);" data-property-id='<?=$property['id'];?>'>
                                                        <?=$property['lot'];?>
                                                    </a>
                                                </th>

                                            <?php } } ?>

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>