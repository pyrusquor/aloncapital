<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Project Documents</h3>
			<?php if ( isset($_total) && $_total ): ?>

				<span class="kt-subheader__separator kt-subheader__separator--v"></span>
				<span class="kt-subheader__desc" id="_total"><?php echo $_total;?> TOTAL</span>
			<?php endif; ?>
		</div>
		<div class="kt-subheader__toolbar">
			<div class="kt-subheader__wrapper">
				<?php if ( isset($_proj_id) && $_proj_id ): ?>

					<button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="_document_checklist_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button>
					<a href="<?php echo site_url('project/document_checklist/'.$_proj_id);?>" class="btn btn-label-instagram btn-elevate btn-sm">
						<i class="fa fa-reply"></i> Cancel
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__body">

		<div class="kt-margin-b-35 kt-margin-t-10">
			<div class="row">
				<div class="col-lg-12">
					<form class="kt-form">
						<div class="form-group row">
							<div class="col-lg-4">
								<label class="form-control-label">Checklist Template <code>*</code></label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select class="form-control kt-select2" name="checklist" id="checklist" data-proj_id="<?php echo @$_proj_id;?>">
										<?php if ( isset($_checklists) && $_checklists ): ?>

											<option value=""></option>

											<?php foreach ( $_checklists as $key => $_checklist ): ?>

												<?php
													$_name	=	isset($_checklist->name) && $_checklist->name ? $_checklist->name : '';
													$_id		=	isset($_checklist->id) && $_checklist->id ? $_checklist->id : '';
												?>

												<option value="<?php echo @$_id;?>">
													<?php echo @$_name;?>
												</option>
											<?php endforeach; ?>
										<?php else: ?>

											<option value="">-- No record found --</option>
										<?php endif; ?>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-book"></i></span></span>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<form class="kt-form" action="<?php echo site_url('project/insert_document_checklist/'.@$_proj_id);?>" method="POST" id="_document_checklist_form">
			<div id="_filtered_document_checklist"></div>
		</form>
	</div>
</div>

<script type="text/javascript">
	var _proj_id	=	'<?php echo isset($_proj_id) && $_proj_id ? $_proj_id : '';?>';
</script>