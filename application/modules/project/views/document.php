<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Project</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="" class="kt-subheader__breadcrumbs-link">Document Checklist</a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="#" class="btn btn-label-primary"><i class="la la-plus"></i> Create Project Document</a>&nbsp;
                <a href="#" class="btn btn-label-primary"><i class="la la-filter"></i> Filter Checklist</a>&nbsp;
                <a href="#" class="btn btn-label-primary"><i class="la la-upload"></i> Batch Upload</a>&nbsp;
                <a href="#" class="btn btn-label-primary"><i class="la la-download"></i> Export</a>&nbsp;
            </div>
        </div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="kt-portlet kt-portlet--height-fluid">
                <!-- <div class="kt-portlet__head kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"></h3>
                    </div>
                    <div class="kt-portlet__head-toolbar"> </div>
                </div> -->
                <div class="kt-portlet__body">
                
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="checklist">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Owner</th>
                                <th>Timeline</th>
                                <th>Due Date</th>
                                <th>Last Update</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td>1</td>
                                <td>1 x 1 Picture</td>
                                <td>1 x 1 Picture</td>
                                <td>Legal</td>
                                <td>30 Days</td>
                                <td class="dt-center"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge kt-font-bold">(0) months 14 Day(s) overdue)</span><br/>September 09, 2019</td>
                                <td>August 27, 2019 10:00 AM</td>
                                <td nowrap></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>2</td>
                                <td>2000-OT Form</td>
                                <td>2000-OT</td>
                                <td>Legal</td>
                                <td>30 Days</td>
                                <td class="dt-center"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge kt-font-bold">(0) months 14 Day(s) overdue)</span><br/>September 09, 2019</td>
                                <td>August 27, 2019 10:00 AM</td>
                                <td nowrap></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>3</td>
                                <td>Accountability Statement of the Project Proponent</td>
                                <td>Accountability Statement of the Project Proponent</td>
                                <td>Legal</td>
                                <td>30 Days</td>
                                <td class="dt-center"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge kt-font-bold">(0) months 14 Day(s) overdue)</span><br/>September 09, 2019</td>
                                <td>August 27, 2019 10:00 AM</td>
                                <td nowrap></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>4</td>
                                <td>Acknowledgement of Equity Full Payment (Company Form)</td>
                                <td>Acknowledgement of Equity Full Payment (Company Form)</td>
                                <td>C & C</td>
                                <td>30 Days</td>
                                <td class="dt-center"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge kt-font-bold">(0) months 14 Day(s) overdue)</span><br/>September 09, 2019</td>
                                <td>August 27, 2019 10:00 AM</td>
                                <td nowrap></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>5</td>
                                <td>Affidavit of Tree Planting</td>
                                <td>Affidavit of Tree Planting</td>
                                <td>Legal</td>
                                <td>30 Days</td>
                                <td class="dt-center"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge kt-font-bold">(0) months 14 Day(s) overdue)</span><br/>September 09, 2019</td>
                                <td>August 27, 2019 10:00 AM</td>
                                <td nowrap></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>