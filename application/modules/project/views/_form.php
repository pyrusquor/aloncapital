<?php
// print_r($project); exit;

$id = isset($project['id']) && $project['id'] ? $project['id'] : '';
$company_id = isset($project['company_id']) && $project['company_id'] ? $project['company_id'] : '';
$name = isset($project['name']) && $project['name'] ? $project['name'] : '';
$project_code = isset($project['project_code']) && $project['project_code'] ? $project['project_code'] : '';
$base_lot = isset($project['base_lot']) && $project['base_lot'] ? $project['base_lot'] : '';
$lts_name = isset($project['lts_name']) && $project['lts_name'] ? $project['lts_name'] : '';
$image = isset($project['image']) && $project['image'] ? $project['image'] : '';
$type = isset($project['type']) && $project['type'] ? $project['type'] : '';
$max_price = isset($project['max_price']) && $project['max_price'] ? $project['max_price'] : '';
$min_price = isset($project['min_price']) && $project['min_price'] ? $project['min_price'] : '';
$location = isset($project['location']) && $project['location'] ? $project['location'] : '';
$project_start_date = isset($project['project_start_date']) && $project['project_start_date'] ? $project['project_start_date'] : '';
$dpc_palc_date = isset($project['dpc_palc_date']) && $project['dpc_palc_date'] ? $project['dpc_palc_date'] : '';
$cr_lts_date = isset($project['ctr_lts_date']) && $project['ctr_lts_date'] ? $project['ctr_lts_date'] : '';
$titling_date = isset($project['titling_date']) && $project['titling_date'] ? $project['titling_date'] : '';
$project_area = isset($project['project_area']) && $project['project_area'] ? $project['project_area'] : '';
$saleable_area = isset($project['saleable_area']) && $project['saleable_area'] ? $project['saleable_area'] : '';
$no_saleable_unit = isset($project['no_saleable_unit']) && $project['no_saleable_unit'] ? $project['no_saleable_unit'] : '';
$coc_date = isset($project['coc_date']) && $project['coc_date'] ? $project['coc_date'] : '';
$etd_date = isset($project['etd_date']) && $project['etd_date'] ? $project['etd_date'] : '';
$lts_number = isset($project['lts_number']) && $project['lts_number'] ? $project['lts_number'] : '';
$pd_units = isset($project['pd_units']) && $project['pd_units'] ? $project['pd_units'] : '';
$bp_units = isset($project['bp_units']) && $project['bp_units'] ? $project['bp_units'] : '';
$embedded_map = isset($project['embedded_map']) && $project['embedded_map'] ? $project['embedded_map'] : '';
$status = isset($project['status']) && $project['status'] ? $project['status'] : '';

$zonal_value = isset($project['zonal_value']) && $project['zonal_value'] ? $project['zonal_value'] : '';
$fair_market_value = isset($project['fair_market_value']) && $project['fair_market_value'] ? $project['fair_market_value'] : '';

$is_active = isset($project['is_active']) && $project['is_active'] ? $project['is_active'] : '';

?>

<div class="row">
    <div class="col-md-3 col-xl-3">
        <div class="form-group">
            <label>Project Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Project Name">
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-3 col-xl-3">
        <div class="form-group">
            <label>Project Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="project_code" value="<?php echo set_value('project_code', $project_code); ?>" placeholder="Project Code">
            </div>
            <?php echo form_error('project_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-3 col-xl-3">
        <div class="form-group">
            <label>LTS Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="lts_name" value="<?php echo set_value('lts_name', $lts_name); ?>" placeholder="LTS Name">
            </div>
            <?php echo form_error('lts_name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-3 col-xl-3">
        <div class="form-group">
            <label>Project Type <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('type', Dropdown::get_static('project_type'), set_value('type', @$type), 'class="form-control" id="type"'); ?>
            </div>
            <?php echo form_error('type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-xl-3">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('company_id', Dropdown::get(['id', 'name'], 'Company_model', 'id', 'name', 'Select Company'), set_value('company_id', @$company_id), 'class="form-control" id="company_id"'); ?>
            </div>
            <?php echo form_error('company_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-3 col-xl-3">
        <div class="form-group">
            <label>Location (Full Address) <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" name="location" value="<?php echo set_value('location', $location); ?>" placeholder="Location">
            </div>
            <?php echo form_error('location'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

    <div class="col-md-3 col-xl-3">
        <div class="form-group">
            <label class="form-control-label">Base Lot</label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <input type="text" class="form-control" id="base_lot" name="base_lot" value="<?php echo set_value('base_lot', $base_lot); ?>" placeholder="Base Lot" autocomplete="off">
                    <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                </div>
            </div>
            <?php echo form_error('base_lot'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <div class="form-group">
                <label class="form-control-label">Price Minimum</label>
                <div class="kt-input-icon">
                    <input type="text" class="form-control" id="min_price" name="min_price" value="<?php echo set_value('min_price', $min_price); ?>" placeholder="Price Minimum" autocomplete="off">
                </div>
                <?php echo form_error('min_price'); ?>
                <span class="form-text text-muted"></span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">Price Maximum</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="max_price" name="max_price" value="<?php echo set_value('max_price', $max_price); ?>" placeholder="Price Maximum" autocomplete="off">
            </div>
            <?php echo form_error('max_price'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>Project Start Date <code>(yyyy-mm-dd)</code></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" id="project_start_date" name="project_start_date" value="<?php echo set_value('project_start_date_date', $project_start_date); ?>" placeholder="Project Start Date" autocomplete="off" readonly>
            </div>
            <?php echo form_error('project_start_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>DP-PALC Date <code>(yyyy-mm-dd)</code></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" id="dpc_palc_date" name="dpc_palc_date" value="<?php echo set_value('dpc_palc_date', $dpc_palc_date); ?>" placeholder="DP-PALC Date" readonly>
            </div>
            <?php echo form_error('dpc_palc_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>CR-LTS Date <code>(yyyy-mm-dd)</code></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" id="cr_lts_date" name="cr_lts_date" value="<?php echo set_value('cr_lts_date', $cr_lts_date); ?>" placeholder="CR-LTS Date" readonly>
            </div>
            <?php echo form_error('cr_lts_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>Individual Titling Date <code>(yyyy-mm-dd)</code></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" id="titling_date" name="titling_date" value="<?php echo set_value('titling_date', $titling_date); ?>" placeholder="Individual Titling Date" readonly>
            </div>
            <?php echo form_error('titling_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>Status <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
                <?php echo form_dropdown('status', Dropdown::get_static('project_status'), set_value('status', @$status), 'class="form-control" id="status"'); ?>
            </div>
            <?php echo form_error('type'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">Total Project Area</label>
            <div class="kt-input-icon ">
                <div class="input-group">
                    <input type="text" class="form-control" id="project_area" name="project_area" value="<?php echo set_value('project_area', $project_area); ?>" placeholder="Total Project Area" autocomplete="off">
                    <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                </div>
            </div>
            <?php echo form_error('project_area'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">Saleable Area</label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <input type="text" class="form-control" id="saleable_area" name="saleable_area" value="<?php echo set_value('saleable_area', $saleable_area); ?>" placeholder="Saleable Area" autocomplete="off">
                    <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                </div>
            </div>
            <?php echo form_error('saleable_area'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">Number of Saleable Units</label>
            <div class="kt-input-icon">
                <div class="input-group">
                    <input type="text" class="form-control" id="no_saleable_unit" name="no_saleable_unit" value="<?php echo set_value('no_saleable_unit', $no_saleable_unit); ?>" placeholder="Number of Saleable Units" autocomplete="off">
                    <div class="input-group-append"><span class="input-group-text">SQM</span></div>
                </div>
            </div>
            <?php echo form_error('no_saleable_unit'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>Certificate of Completion Date <code>(yyyy-mm-dd)</code></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" id="coc_date" name="coc_date" value="<?php echo set_value('coc_date', $coc_date); ?>" placeholder="COC Date" readonly>
            </div>
            <?php echo form_error('coc_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>ETD Date <code>(yyyy-mm-dd)</code></label>
            <div class="kt-input-icon">
                <input type="text" class="form-control kt_datepicker" id="etd_date" name="etd_date" value="<?php echo set_value('etd_date', $etd_date); ?>" placeholder="ETD Date" readonly>
            </div>
            <?php echo form_error('etd_date'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">LTS Number</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="lts_number" name="lts_number" value="<?php echo set_value('lts_number', $lts_number); ?>" placeholder="LTS Number" autocomplete="off">
            </div>
            <?php echo form_error('lts_number'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">PD957 Units</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="pd_units" name="pd_units" value="<?php echo set_value('pd_units', $pd_units); ?>" placeholder="PD957 Units" autocomplete="off">
            </div>
            <?php echo form_error('pd_units'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">BP220 Units</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="bp_units" name="bp_units" value="<?php echo set_value('bp_units', $bp_units); ?>" placeholder="BP220 Units" autocomplete="off">
            </div>
            <?php echo form_error('bp_units'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">Embedded Map</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="embedded_map" name="embedded_map" value="<?php echo set_value('embedded_map', $embedded_map); ?>" placeholder="Embedded Map" autocomplete="off">
            </div>
            <?php echo form_error('embedded_map'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">Zonal Value</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="zonal_value" name="zonal_value" value="<?php echo set_value('zonal_value', $zonal_value); ?>" placeholder="Zonal Value" autocomplete="off">
            </div>
            <?php echo form_error('zonal_value'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label class="form-control-label">Fair Market Value</label>
            <div class="kt-input-icon">
                <input type="text" class="form-control" id="fair_market_value" name="fair_market_value" value="<?php echo set_value('fair_market_value', $fair_market_value); ?>" placeholder="fair_market_value" autocomplete="off">
            </div>
            <?php echo form_error('fair_market_value'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-xl-6">
        <div class="form-group">
            <label>Project Photo</label>
            <div>
                <?php if (get_image('project', 'photo', $image)): ?>
                    <img class="kt-widget__img" src="<?php echo base_url(get_image('project', 'photo', $image)); ?>" width="90px" height="90px"/>
                <?php endif;?>
                <span class="btn btn-sm">
                    <input type="file" name="project_image" class="" aria-invalid="false">
                </span>
            </div>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
