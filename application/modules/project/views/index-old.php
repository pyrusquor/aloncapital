<?php 
    // print_r($filter); exit;
    if(isset($filter)) 
    {
        if ($filter['filter_name'] == '' && $filter['filter_company'] == '' && $filter['filter_type'] == '' && $filter['filter_status'] == '')
        {
            unset($filter);
        }
    }
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Projects</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc"><?php echo (!empty($records)) ? count($records) : 0; ?> TOTAL</span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search project..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('project/create');?>" class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Create
                </a>
                <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="modal" data-target="#filterModal">
                    <i class="fa fa-filter"></i> Filter
                </button>
                <button class="btn btn-label-primary btn-elevate btn-sm" data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true" aria-controls="_batch_upload">
                    <i class="fa fa-upload"></i> Import
                </button>
                <button type="button" class="btn btn-label-primary btn-elevate btn-sm" id="export">
                    <i class="fa fa-download"></i> Export
                </button>
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div id="_batch_upload" class="row collapse">
		<div class="col-md-12 col-xl-12">
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__body">
					<form class="kt-form" method="POST" action="<?php echo base_url('project/import'); ?>" enctype="multipart/form-data">
						<div class="form-group row">
							<div class="col-lg-4">
								<label class="form-control-label">Document Type</label>
								<div class="kt-input-icon  kt-input-icon--left">
									<select class="form-control form-control-sm" name="status" id="import_status">
										<option value=""> -- Update Existing Data -- </option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
									<span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-cloud-upload"></i></span></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4">
								<label class="form-control-label">Upload CSV file:</label>
								<label class="form-control-label text-muted">Note: Maximum of 1,000 items only per file.</label>
								<input type="file" name="file" class="" size='100' accept=".csv">
							</div>
						</div>
						<div class="form-group form-group-last row">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-lg-6">
										<button type="submit" class="btn btn-brand btn-success btn-elevate btn-sm">
											<i class="fa fa-upload"></i> Upload
										</button>
									</div>
					</form>
									<div class="col-lg-6 kt-align-right">
					<form class="kt-form" method="POST" action="<?php echo base_url('project/export_csv'); ?>">
										<input type="hidden" name="status" id="export_csv_status"/>
										<button type="submit" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm">
											<i class="fa fa-file-csv"></i>
										</button>
										<button type="button" class="btn btn-brand btn-success btn-elevate btn-icon btn-icon-lg btn-sm" id="btn_upload_guide" data-toggle="modal" data-target="#upload_guide">
											<i class="fa fa-info-circle"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
    <div class="row" id="projectList">
		<?php if (!empty($record)) : ?>
        <?php foreach ($record as $r): ?>
            <div class="col-md-4 col-xl-3">
                <!--Begin::Portlet-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                <i class="flaticon-more-1 kt-font-brand"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="<?php echo site_url('project/document_checklist/'.$r['id']);?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-folder"></i>
                                            <span class="kt-nav__link-text">Document Checklist</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="<?php echo site_url('project/process_checklist/'.$r['id']);?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-soft-icons"></i>
                                            <span class="kt-nav__link-text">Process Checklist</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="<?php echo base_url('project/view/'.$r['id']); ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-checking"></i>
                                            <span class="kt-nav__link-text">View</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="<?php echo base_url('project/update/'.$r['id']); ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                            <span class="kt-nav__link-text">Update</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="javascript: void(0)" class="kt-nav__link remove_project" data-id="<?php echo $r['id']; ?>">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Delete</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-position"></i>
                                            <span class="kt-nav__link-text">View Property Models</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-add"></i>
                                            <span class="kt-nav__link-text">Add Property Models</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-2">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden" src="<?php echo base_url('assets/media/project-logos/1.png'); ?>" alt="image" style="width: 50px; height: 50px;">
                                    <img class="kt-widget__img kt-hidden" src="<?php echo base_url('assets/media/users/300_21.jpg'); ?>" alt="image">
                                    <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                        <?php echo get_initials($r['name']); ?>
                                    </div>
                                </div>
                                <div class="kt-widget__info">
                                    <a href="<?php echo base_url('project/view/'.$r['id']); ?>" class="kt-widget__titel kt-hidden-">
                                        <?php echo ucwords($r['name']); ?>
                                    </a>
                                    <a href="#" class="kt-widget__username kt-hidden">
                                        Luca Doncic
                                    </a>
                                    <span class="kt-widget__desc">
                                        <?php echo ( ! empty($r['company']['name']) ) ? $r['company']['name'] : 'None'; ?>
                                    </span>
                                </div>
                            </div>
                            <div class="kt-widget__body">
                                <!-- <div class="kt-widget__section">
                                    I distinguish three <a href="#" class="kt-font-brand kt-link kt-font-transform-u kt-font-bold">#xrs-54pq</a> objectsves First
                                    esetablished and nice coocked rice
                                </div> -->
                                <div class="kt-widget__item">
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400; margin-right: 10px;">Location:</span>
                                        <a href="#" class="kt-widget__data" style="font-weight: 600"><?php echo ucwords($r['location']); ?></a>
                                    </div>
                                    <div class="kt-widget__contact" >
                                        <span class="kt-widget__label" style="font-weight: 400">Total Project Area:</span>
                                        <a href="#" class="kt-widget__data" style="font-weight: 600"><?php echo $r['project_area']; ?> sqm</a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400">Saleable Area:</span>
                                        <span class="kt-widget__data" style="font-weight: 600"><?php echo $r['saleable_area']; ?> sqm</span>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400">No. of Saleable Units:</span>
                                        <span class="kt-widget__data" style="font-weight: 600"><?php echo $r['no_saleable_unit']; ?> units</span>
                                    </div>
									 <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400">Status:</span>
                                        <span class="kt-widget__data" style="font-weight: 600"><?php echo ($r['status'] == 'O' ? 'Ongoing' : 'Completed'); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                    <div class="kt-portlet__foot kt-portlet__foot--sm">
                        <div class="kt-widget__text kt-align-left" style="float: left; font-weigt: 600;">65%</div>
                        <div class="kt-widget__text kt-align-right">Progress</div>
                        <div class="progress" style="height: 5px;width: 100%;">
                            <div class="progress-bar kt-bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--End::Portlet-->
            </div>
        <?php endforeach;?>
		<?php endif;?>
    </div>
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="kt-pagination  kt-pagination--brand">
                <ul class="kt-pagination__links">
                    <?php echo $pagination; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- FILTER MODAL -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('project/filter'); ?>" method="POST" id="filter_form">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="recipient-name" class="form-control-label">Project Name:</label>
                            <input type="text" class="form-control" id="filter_name" name="filter_name" value="<?php echo set_value('filter_name', (isset($filter['filter_name'])) ? $filter['filter_name'] : ''); ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="message-text" class="form-control-label">Project Type:</label>
                            <select class="form-control kt-select2" id="filter_type" name="filter_type">
                                <option value=""></option>
                                <?php $filter_type = (isset($filter['filter_type'])) ? $filter['filter_type'] : ''; ?>
                                <option value="1" <?php echo (($filter_type == 1) ? 'selected' : ''); ?>>Subdivision</option>
                                <option value="2" <?php echo ($filter_type == 2 ? 'selected' : ''); ?>>Condominium</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="message-text" class="form-control-label">Company:</label>
                            <input type="text" class="form-control" id="filter_company" name="filter_company" value="<?php echo set_value('filter_company', (isset($filter['filter_company'])) ? $filter['filter_company'] : ''); ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="message-text" class="form-control-label">Status:</label>
                            <select class="form-control kt-select2" id="filter_status" name="filter_status">
                                <option value=""></option>
                                <?php $filter_status = (isset($filter['filter_status'])) ? $filter['filter_status'] : ''; ?>
                                <option value="O" <?php echo ($filter_status == 'O' ? 'selected' : ''); ?>>Ongoing</option>
                                <option value="C" <?php echo ($filter_status == 'C' ? 'selected' : ''); ?>>Completed</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="apply_filter">Apply</button>
                <a href="<?php echo base_url('project'); ?>" class="btn btn-danger" id="remove_filter" style="<?php echo (!empty($filter)) ? '' : 'display: none'; ?>">Remove</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- UPLOAD GUIDE MODAL -->
<div class="modal fade" id="upload_guide" tabindex="-1" role="dialog" aria-labelledby="upload_guide_label" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="upload_guide_label">Project - Upload Guide</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<!--begin: Datatable -->
				<table class="table table-striped- table-bordered table-hover table-condensed table-checkable" id="upload_guide_table">
					<thead>
						<tr class="text-center">
							<th>#</th>
							<th>Name</th>
							<th>Type</th>
							<th>Format</th>
							<th>Options</th>
							<th>Required</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-elevate btn-outline-hover-brand btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>