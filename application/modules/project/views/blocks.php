<!-- CONTENT HEADER -->
<?php
$project_id = isset($project_id) && $project_id ? $project_id : '';
$max_price = isset($max_price) && $max_price ? $max_price : '0';
?>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Project Block View - <?=get_value_field($project_id,'projects','name');?></h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn" data-toggle="modal" data-target="#filterModal" aria-expanded="true">
                    <i class="fa fa-filter"></i> Filter
                </button>
                <a href="<?php echo site_url('project'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>
<input type='hidden' id='project_id' value='<?= $project_id; ?>'>
<input type='hidden' id='max_price' value='<?= $max_price; ?>'>
<style>
    /* Style to only show slider tooltips while dragging */
    .noUi-tooltip {
        display: none;
    }

    .noUi-active .noUi-tooltip {
        display: block;
    }

    .table-gray,
    .table-gray>th,
    .table-gray>td {
        background-color: #E8E8E8;
    }

    .table-gray th,
    .table-gray td,
    .table-gray thead th,
    .table-gray tbody+tbody {
        border-color: #E8E8E8;
    }

    .table-hover .table-gray:hover {
        background-color: #F5F5F5;
    }

    .table-hover .table-gray:hover>td,
    .table-hover .table-gray:hover>th {
        background-color: #F5F5F5;
    }

    th a {
        width: 100%;
        line-height: 40px;
        display: inline-block;
        vertical-align: middle;
    }
</style>
<div id='blocks_table'></div>
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <!-- <form method="POST" id="advanceSearch"> -->

                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="form-control-label">Property Status:</label>
                        <?php echo form_dropdown('status', Dropdown::get_static('property_status'), '', 'class="form-control" id="status"'); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-control-label">Model:</label>
                        <select class="form-control suggests_modal" data-module="house_models" id="model_id" data-param='project_id'>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="form-control-label">Progress:</label>
                        <div id='percentage'></div>
                    </div>
                </div>
                &nbsp;
                &nbsp;
                <hr />
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="form-control-label">Price Range:</label>
                        <div id='price_range'></div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="form-control-label">Start Price:</label>
                        <input type="text" id="price_floor" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-control-label">End Price:</label>
                        <input type="text" id="price_ceiling" class="form-control">
                    </div>
                </div>
            </div>
            <input type='hidden' value='0' id='progress_floor'>
            <input type='hidden' value='100' id='progress_ceiling'>
            <input type='hidden' value='default' id='is_default'>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="apply_filter" form="advanceSearch">Apply</button>
                <button type="button" class="btn btn-danger" id="reset_filter" form="advanceSearch">Reset Filters</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id='property_modal_info'></div>