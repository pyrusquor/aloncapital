<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Update Project</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="submit" class="btn btn-label-success btn-elevate btn-sm" form="project_form">
					<i class="fa fa-sync"></i> Update
				</button>
				<a href="<?php echo site_url('project');?>" class="btn btn-label-instagram btn-elevate btn-sm">
					<i class="fa fa-reply"></i> Back
				</a>
            </div>
        </div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body">
                    <form method="POST" class="kt-form" id="project_form" enctype="multipart/form-data">
                        <?php $this->load->view('_form'); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>