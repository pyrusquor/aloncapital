<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>

<?php
// print_r($house); exit;
$id = isset($project['id']) && $project['id'] ? $project['id'] : 'N/A';
$company = isset($project['company']['name']) && $project['company']['name'] ? $project['company']['name'] : 'N/A';
$company_id = isset($project['company']['id']) && $project['company']['id'] ? $project['company']['id'] : 'N/A';
$name = isset($project['name']) && $project['name'] ? $project['name'] : 'N/A';
$lts_name = isset($project['lts_name']) && $project['lts_name'] ? $project['lts_name'] : 'N/A';
$project_code = isset($project['project_code']) && $project['project_code'] ? $project['project_code'] : 'N/A';
$base_lot = isset($project['base_lot']) && $project['base_lot'] ? money_php($project['base_lot']) : 'N/A';
$image = isset($project['image']) && $project['image'] ? $project['image'] : 'N/A';
$type = isset($project['type']) && $project['type'] ? $project['type'] : 'N/A';
$embedded_map = isset($project['embedded_map']) && $project['embedded_map'] ? $project['embedded_map'] : 'N/A';
$max_price = isset($project['max_price']) && $project['max_price'] ? $project['max_price'] : '0.00';
$min_price = isset($project['min_price']) && $project['min_price'] ? $project['min_price'] : '0.00';
$location = isset($project['location']) && $project['location'] ? $project['location'] : 'N/A';
$project_start_date = isset($project['project_start_date']) && $project['project_start_date'] ? $project['project_start_date'] : 'N/A';
$dpc_palc_date = isset($project['dpc_palc_date']) && $project['dpc_palc_date'] ? $project['dpc_palc_date'] : 'N/A';
$cr_lts_date = isset($project['ctr_lts_date']) && $project['ctr_lts_date'] ? $project['ctr_lts_date'] : 'N/A';
$titling_date = isset($project['titling_date']) && $project['titling_date'] ? $project['titling_date'] : 'N/A';
$project_area = isset($project['project_area']) && $project['project_area'] ? $project['project_area'] : '0.00';
$saleable_area = isset($project['saleable_area']) && $project['saleable_area'] ? $project['saleable_area'] : '0.00';
$no_saleable_unit = isset($saleable_units) && $saleable_units ? $saleable_units : '0';
$coc_date = isset($project['coc_date']) && $project['coc_date'] ? $project['coc_date'] : 'N/A';
$etd_date = isset($project['etd_date']) && $project['etd_date'] ? $project['etd_date'] : 'N/A';
$lts_number = isset($project['lts_number']) && $project['lts_number'] ? $project['lts_number'] : 'N/A';

$PD_units = isset($bp) && $bp ? $bp : '0';
$BP_units = isset($pd) && $pd ? $pd : '0';
$SH_units = isset($sh) && $sh ? $sh : '0';

$status = isset($project['status']) && $project['status'] ? $project['status'] : 'N/A';
$active = isset($project['is_active']) && $project['is_active'] ? $project['is_active'] : 'N/A';

$zonal_value = isset($project['zonal_value']) && $project['zonal_value'] ? $project['zonal_value'] : 'N/A';
$fair_market_value = isset($project['fair_market_value']) && $project['fair_market_value'] ? $project['fair_market_value'] : 'N/A';
?>

<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Project Details</h3>
		</div>
		<div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <?php if ($this->ion_auth->is_admin()): ?>
                    <a href="<?php echo site_url('project/update/' . $id); ?>" class="btn btn-label-success btn-elevate btn-sm">
                        <i class="fa fa-edit"></i> Edit
                    </a>
                <?php endif;?>
				<a href="<?php echo site_url('project'); ?>" class="btn btn-label-instagram btn-sm btn-elevate">
					<i class="fa fa-reply"></i> Back
				</a>
            </div>
        </div>
	</div>
</div>

<!-- CONTENT -->
<div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-5 col-xl-5">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                    </div>
                </div>
                <div class="kt-portlet__body">

                    <!--begin::Widget -->
                    <div class="kt-widget kt-widget--user-profile-2">
                        <div class="kt-widget__head">
                            <div class="kt-widget__media">
                                <img class="kt-widget__img kt-hidden" src="<?php echo base_url('assets/media/project-logos/1.png'); ?>" alt="image" style="width: 70px; height: 70px;">
                                <?php if (get_image('project', 'photo', $image)): ?>

                                <img class="kt-widget__img" src="<?php echo base_url(get_image('project', 'photo', $image)); ?>" width="90px" height="90px"/>
                                <?php else: ?>

                                <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                    <?php echo get_initials($name); ?>
                                </div>
                                <?php endif;?>
                            </div>
                            <div class="kt-widget__info">
                                <span class="kt-widget__titel kt-hidden-">
                                    <?php echo ucwords($name); ?>
                                </span>
                                <span class="kt-widget__username kt-hidden">
                                    Luca Doncic
                                </span>
                                <a href="<?php echo base_url('company/view/' . $company_id); ?>" class="kt-widget__desc">
                                    <?php echo ucwords($company); ?>
                                </a>
                            </div>
                        </div>
                        <div class="kt-widget__body">
                            <div class="kt-widget__item">
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Project Name</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo ucwords($name); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">LTS Name</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo ucwords($lts_name); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Project Code</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo ucwords($project_code); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Base Lot</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo $base_lot; ?></span>
                                </div>
                                <div class="kt-widget__contact" >
                                    <span class="kt-widget__label" style="font-weight: 400">Project Type</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo Dropdown::get_static('project_type', @$type, 'view'); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Company</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo ucwords($company); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Price Range</span>
                                    <span class="kt-widget__data" style="font-weight: 600">P <?php echo $min_price; ?> - P <?php echo $max_price; ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Address</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo ucwords($location); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Date Start of Project</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo date('F d, Y', strtotime($project_start_date)); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Date DP-PALC</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo date('F d, Y', strtotime($dpc_palc_date)); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Date CR-LTS</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo date('F d, Y', strtotime($cr_lts_date)); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Date Individual Titling</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo date('F d, Y', strtotime($titling_date)); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Total Project Area</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo $project_area; ?> sqm</span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Saleable Area</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo $saleable_area; ?> sqm</span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">COC Date</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo date('F d, Y', strtotime($coc_date)); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">ETD Date</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo date('F d, Y', strtotime($etd_date)); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">LTS Number</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo $lts_number; ?></span>
                                </div>
								<div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Status</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo Dropdown::get_static('project_status', @$status, 'view'); ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Zonal Value</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo $zonal_value; ?></span>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label" style="font-weight: 400">Fair Market Value</span>
                                    <span class="kt-widget__data" style="font-weight: 600"><?php echo $fair_market_value; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Widget -->
                </div>
            </div>
        </div>
        <div class="col-md-7 col-xl-7">
            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--noborder" style="border-top-left-radius: 0px; border-top-right-radius: 0px;">
                    <div class="kt-portlet__head-label" style="width: 100%;">
                        <div class="col-md-3" style="text-align: center; border-right: solid 1px lightgray;">
                            <h3 class="kt-portlet__head-title">Total Units</h3>
                            <h3 class="kt-portlet__head-title kt-font-warning"><b><?php echo $no_saleable_unit; ?></b></h3>
                        </div>
                        <div class="col-md-3" style="text-align: center; border-left: solid 1px lightgray; border-right: solid 1px lightgray;">
                            <h3 class="kt-portlet__head-title">PD957 Units</h3>
                            <h3 class="kt-portlet__head-title kt-font-warning"><b><?php echo $PD_units; ?></b></h3>
                        </div>
                        <div class="col-md-3" style="text-align: center; border-left: solid 1px lightgray;">
                            <h3 class="kt-portlet__head-title">BP220 Units</h3>
                            <h3 class="kt-portlet__head-title kt-font-warning"><b><?php echo $BP_units; ?></b></h3>
                        </div>
                         <div class="col-md-3" style="text-align: center; border-left: solid 1px lightgray;">
                            <h3 class="kt-portlet__head-title">Socialized Units</h3>
                            <h3 class="kt-portlet__head-title kt-font-warning"><b><?php echo $SH_units; ?></b></h3>
                        </div>
                    </div>
                    <div class="kt-portlet__head-toolbar"> </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--noborder" style="border-top-left-radius: 0px; border-top-right-radius: 0px;">
                    <div class="kt-portlet__head-label" style="width: 100%;">
                        <h3 class="kt-portlet__head-title">Map</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown">
                            <i class="flaticon-more-1 kt-font-brand"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="kt-nav">
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                        <span class="kt-nav__link-text">Export</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body" style="height: 330px; padding-top: 0px;">
                    <div id="kt_gmap_1" style="height:300px;">
						<iframe src="<?php echo $embedded_map; ?>" width="100%" height="298" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					</div>
                </div>
                <div class="kt-portlet__foot kt-portlet__foot--sm">
                    <div class="row">
                        <?php
$stats = Dropdown::get_static('property_status');
if ($stats) {
    foreach ($stats as $key => $stat) {if (empty($key)) {continue;}?>
                                    <div class="col-md-6">
                                        <div class="kt-widget__text kt-align-left" style="float: left; font-weigth: 600;"><h3>40%</h3></div>
                                        <div class="kt-widget__text kt-align-right"><?php echo $stat; ?></div>
                                        <div class="progress" style="height: 5px;width: 100%;">
                                            <div class="progress-bar kt-bg-success" role="progressbar" style="width: 40%;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                <?php }
}
?>

                       <!--  <div class="col-md-6">
                            <div class="kt-widget__text kt-align-left" style="float: left; font-weigth: 600;"><h3>40%</h3></div>
                            <div class="kt-widget__text kt-align-right">Available</div>
                            <div class="progress" style="height: 5px;width: 100%;">
                                <div class="progress-bar kt-bg-success" role="progressbar" style="width: 40%;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div> -->

                        <!-- <div class="col-md-6">
                            <div class="kt-widget__text kt-align-left" style="float: left; font-weigth: 600;"><h3>45%</h3></div>
                            <div class="kt-widget__text kt-align-right">Reserved</div>
                            <div class="progress" style="height: 5px;width: 100%;">
                                <div class="progress-bar kt-bg-info" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="kt-widget__text kt-align-left" style="float: left; font-weigth: 600;"><h3>10%</h3></div>
                            <div class="kt-widget__text kt-align-right">Cancelled</div>
                            <div class="progress" style="height: 5px;width: 100%;">
                                <div class="progress-bar kt-bg-danger" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="kt-widget__text kt-align-left" style="float: left; font-weigth: 600;"><h3>5%</h3></div>
                            <div class="kt-widget__text kt-align-right">Hold</div>
                            <div class="progress" style="height: 5px;width: 100%;">
                                <div class="progress-bar kt-bg-warning" role="progressbar" style="width: 5%;" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div> -->
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-12 col-xl-12" id="project-house">
            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--noborder" style="border-top-left-radius: 0px; border-top-right-radius: 0px;">
                    <div class="kt-portlet__head-label" style="width: 100%;">
                        <h3 class="kt-portlet__head-title">Property Models</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar"> </div>
                    <div class="kt-portlet__body"></div>
                </div>
            </div>
            <div class="row">
                <?php if (!empty($house)): ?>
                <?php foreach ($house as $h): ?>
                <div class="col-md-3 col-xl-2">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body" style="padding: 5px 20px 15px;">
                            <!--begin::Widget -->
                            <div class="kt-widget kt-widget--user-profile-4">
                                <div class="kt-widget__head">
                                    <div class="kt-widget__media">
                                        <img class="kt-widget__img kt-hidden" src="<?php echo base_url('assets/media/users/300_21.jpg'); ?>" alt="image">
                                        <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                                            HM
                                        </div>
                                    </div>
                                    <div class="kt-widget__content">
                                        <div class="kt-widget__section" style="text-align: center;">
                                            <a href="#" class="kt-widget__username"><?php echo ucwords($h['name']); ?></a>
                                            <span class="kt-widget__desc"><?php echo strtoupper($h['code']); ?></span>
                                            <br />
                                            <br />
                                            <div class="kt-widget__text kt-align-left" style="float: left;">Reservation Fee</div>
                                            <div class="kt-widget__text kt-align-right" style="font-weight: 600;"><?php echo $h['reservation_fee']; ?></div>
                                            <div class="kt-widget__text kt-align-left" style="float: left;">Furnishing Fee</div>
                                            <div class="kt-widget__text kt-align-right" style="font-weight: 600;"><?php echo $h['furnishing_fee']; ?></div>
                                            <br />
                                            <div class="kt-widget__button">
                                                <a href="<?php echo base_url('house/view/' . $h['id']); ?>" target="_blank" class="btn btn-brand btn-sm">View</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Widget -->
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
		        <?php endif;?>
            </div>
        </div>
    </div>
</div>