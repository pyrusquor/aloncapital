<?php if (!empty($records)) : ?>
    <div class="row" id="projectList">
        <?php foreach ($records as $r) : ?>
            <div class="col-md-4 col-xl-3">
                <!--Begin::Portlet-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head kt-portlet__head--noborder">
                        <div class="kt-portlet__head-label">
                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                <input type="checkbox" name="id[]" value="<?php echo $r['id']; ?>" class="m-checkable delete_check" data-id="<?php echo $r['id']; ?>">
                                <span></span>
                            </label>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="#" class="btn btn-icon" data-toggle="dropdown">
                                <i class="flaticon-more-1 kt-font-brand"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="<?php echo site_url('project/document_checklist/' . $r['id']); ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-folder"></i>
                                            <span class="kt-nav__link-text">Document Checklist</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="<?php echo site_url('project/process_checklist/' . $r['id']); ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-soft-icons"></i>
                                            <span class="kt-nav__link-text">Process Checklist</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="<?php echo base_url('project/view/' . $r['id']); ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-checking"></i>
                                            <span class="kt-nav__link-text">View</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="<?php echo base_url('project/update/' . $r['id']); ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                            <span class="kt-nav__link-text">Update</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="javascript: void(0)" class="kt-nav__link remove_project" data-id="<?php echo $r['id']; ?>">
                                            <i class="kt-nav__link-icon flaticon2-trash"></i>
                                            <span class="kt-nav__link-text">Delete</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-position"></i>
                                            <span class="kt-nav__link-text">View Property Models</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-add"></i>
                                            <span class="kt-nav__link-text">Add Property Models</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-2">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="kt-widget__img kt-hidden" src="<?php echo base_url('assets/media/project-logos/1.png'); ?>" alt="image" style="width: 50px; height: 50px;">
                                    <?php if (get_image('project', 'photo', $r['image'])): ?>

                                    <img class="kt-widget__img" src="<?php echo base_url(get_image('project', 'photo', $r['image'])); ?>" width="90px" height="90px"/>
                                    <?php else : ?>

                                    <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                        <?php echo get_initials($r['name']); ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="kt-widget__info">
                                    <a href="<?php echo base_url('project/view/' . $r['id']); ?>" class="kt-widget__title kt-hidden-">
                                       <?php echo $r['id'].'. '.ucwords($r['name']); ?>
                                    </a>
                                    <a href="#" class="kt-widget__username kt-hidden">
                                        Luca Doncic
                                    </a>
                                    <span class="kt-widget__desc">
                                        <?php echo (!empty($r['company']['name'])) ? $r['company']['name'] : 'None'; ?>
                                    </span>
                                </div>
                            </div>
                            <div class="kt-widget__body">
                                <!-- <div class="kt-widget__section">
                                    I distinguish three <a href="#" class="kt-font-brand kt-link kt-font-transform-u kt-font-bold">#xrs-54pq</a> objectsves First
                                    esetablished and nice coocked rice
                                </div> -->
                                <div class="kt-widget__item">
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400; margin-right: 10px;">Location:</span>
                                        <a href="#" class="kt-widget__data" style="font-weight: 600"><?php echo ucwords($r['location']); ?></a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400">Total Project Area:</span>
                                        <a href="#" class="kt-widget__data" style="font-weight: 600"><?php echo $r['project_area']; ?> sqm</a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400">Saleable Area:</span>
                                        <span class="kt-widget__data" style="font-weight: 600"><?php echo $r['saleable_area']; ?> sqm</span>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400">No. of Saleable Units:</span>
                                        <span class="kt-widget__data" style="font-weight: 600"><?php echo $r['no_saleable_unit']; ?> units</span>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label" style="font-weight: 400">Status:</span>
                                        <span class="kt-widget__data" style="font-weight: 600"><?php echo Dropdown::get_static('project_status', $r['status'], 'view'); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Widget -->
                    </div>
                    <div class="kt-portlet__foot kt-portlet__foot--sm">
                        <div class="kt-widget__text kt-align-left" style="float: left; font-weight: 600;">65%</div>
                        <div class="kt-widget__text kt-align-right">Progress</div>
                        <div class="progress" style="height: 5px;width: 100%;">
                            <div class="progress-bar kt-bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--End::Portlet-->
            </div>
        <?php endforeach; ?>
    </div>

    <div class="row">
        <div class="col-xl-12">

            <!--begin:: Components/Pagination/Default-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin: Pagination-->
                    <div class="kt-pagination kt-pagination--brand">
                        <?php echo $this->ajax_pagination->create_links(); ?>

                        <div class="kt-pagination__toolbar">
                            <span class="pagination__desc">
                                <?php echo $this->ajax_pagination->show_count(); ?>
                            </span>
                        </div>
                    </div>

                    <!--end: Pagination-->
                </div>
            </div>

            <!--end:: Components/Pagination/Default-->
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">No Records Found</h3>
                            <p class="kt-callout__desc">
                                Sorry no record were found. Please adjust your search criteria and try again.
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <button class="btn btn-custom btn-bold btn-upper btn-font-sm btn-brand" data-toggle="modal" data-target="#filterModal">Try Again</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
