<?php
$id = isset($item_type['id']) && $item_type['id'] ? $item_type['id'] : '';
$name = isset($item_type['name']) && $item_type['name'] ? $item_type['name'] : '';
$type_code = isset($item_type['type_code']) && $item_type['type_code'] ? $item_type['type_code'] : '';

$group_id = isset($item_type['group_id']) && $item_type['group_id'] ? $item_type['group_id'] : '';

$status = isset($item_type['is_active']) && $item_type['is_active'] ? $item_type['is_active'] : '';
$group_name = isset($item_type['item_group']['name']) && $item_type['item_group']['name'] ? $item_type['item_group']['name'] : 'N/A';

?>


<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" placeholder="Name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-user"></i></span>
            </div>
            <?php echo form_error('name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Type Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="type_code" value="<?php echo set_value('type_code', $type_code); ?>" placeholder="Type Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('type_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Select Group<span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="item_group"  id="item_group_id" name="group_id">
                <option value="0">Select Group</option>
                <?php if ($group_name): ?>
                    <option value="<?php echo $group_id; ?>" selected><?php echo $group_name; ?></option>
                <?php endif?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Status</label>
            <div class="kt-input-icon kt-input-icon--left">

                <?php echo form_dropdown('is_active', Dropdown::get_static('inventory_status'), set_value('is_active', @$status), 'class="form-control"'); ?>
                <?php echo form_error('is_active'); ?>

                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-check-square"></i></span>
            </div>

            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>