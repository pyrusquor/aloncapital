<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item_type_model extends MY_Model
{
    public $table = 'item_type'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = ['name', 'type_code', 'group_id', 'is_active', 'created_by', 'updated_by', 'deleted_by'];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required',
        ),
        'type_code' => array(
            'field' => 'type_code',
            'label' => 'Type Code',
            'rules' => 'trim',
        ),
        'group_id' => array(
            'name' => 'group_id',
            'label' => 'Group ID',
            'rules' => 'trim',
        ),
        'is_active' => array(
            'name' => 'is_active',
            'label' => 'Status',
            'rules' => 'trim',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['item_group'] = array('foreign_model' => 'item_group/Item_group_model', 'foreign_table' => 'item_group', 'foreign_key' => 'id', 'local_key' => 'group_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'name' => $faker->word,
                'type_code' => $faker->word,
                'group_id' => $faker->numberBetween(1, 9),
                'is_active' => $faker->numberBetween(0, 1),
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}
