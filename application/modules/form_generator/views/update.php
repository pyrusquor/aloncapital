<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Update Form</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <button type="submit" form="form_generator" class="btn btn-label-success"><i class="la la-plus"></i>
                    Submit</button>&nbsp;
                <a href="<?php echo base_url('form_generator'); ?>" class="btn btn-label-instagram"><i class="la la-times"></i>
                    Cancel</a>&nbsp;
            </div>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <!--begin::Form-->
            <form method="POST" class="kt-form kt-form--label-right" id="form_generator" enctype="multipart/form-data">
                <div class="kt-portlet__body">

                <?php $this->load->view('_form'); ?>
                    
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
</div>
<!-- begin:: Footer -->