<?php
    $name            =    isset($form['name']) && $form['name'] ? $form['name'] : '';
    $description        =    isset($form['description']) && $form['description'] ? $form['description'] : '';
    $header            =    isset($form['header']) && $form['header'] ? $form['header'] : '';
    $footer    =    isset($form['footer']) && $form['footer'] ? $form['footer'] : '';
    $content    =    isset($form['content']) && $form['content'] ? $form['content'] : '';
?>

<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <label>Please Choose your Table</label>
            <select class="form-control kt-select2" id="table_list">
                <option value="">Select Table</option>
                <?php if (!empty($table_list)) : ?>
                    <?php foreach ($table_list as $table) : if(!in_array($table, $allowed)){ continue; }?>
                        <option value="<?php echo $table; ?>">
                            <?php $tbl = str_replace("_", " ", $table);echo ucwords($tbl); ?>
                        </option>
                    <?php endforeach; ?>
                    <option value="additional_information">
                            Additional Information
                        </option>
                <?php endif; ?>
            </select>

            <div class="kt-margin-t-15" id="tblFields">
                <table class="table table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th>Fields</th>
                            <th class="kt-align-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
           
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="col-lg-8">
        <ul class="nav nav-pills nav-fill" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#information">Information</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#images">Images</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#content">Content</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="information" role="tabpanel">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="">Fill Up Template Name <span class="kt-font-danger">*</span></label>
                            <div class="kt-input-icon">
                                <input type="text" class="form-control" placeholder="Fill Up Template Name" value="<?php echo set_value('name', $name); ?>" name="name">
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>

                        <div class="form-group">
                            <label class="">Form Template Description <span class="kt-font-danger">*</span></label>
                            <textarea name="description" id="description" class="form-control" cols="30" rows="10"><?php echo set_value('description', $description); ?></textarea>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="images" role="tabpanel">
                <div class="col-lg-12">
                    <div class="form-group form-group-last">
                        <div class="alert alert-secondary" role="alert">
                            <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                            <div class="alert-text">
                                File Formats: .jpg, .gif, .png<br>
                                File Size: up to 5MB only<br>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Select Header Image</label>
                        <div>
                            <span class="btn btn-sm">
                                <input type="file" name="header" class="" aria-invalid="false">
                            </span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                    <div class="form-group">
                        <label>Select Footer Image</label>
                        <div>
                            <span class="btn btn-sm">
                                <input type="file" name="footer" class="" aria-invalid="false">
                            </span>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="content" role="tabpanel">
                <textarea name="content" id="contentEditor"><?php echo set_value('content', $content); ?></textarea>
            </div>
        </div>
    </div>
</div>