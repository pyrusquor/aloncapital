<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form_generator extends MY_Controller
{

	public $fields = [
		'name' => array(
			'field' => 'name',
			'label' => 'Template Name',
			'rules' => 'trim|required'
		),
		'description' => array(
			'field' => 'description',
			'label' => 'Template Description',
			'rules' => 'trim|required'
		),
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('buyer/Buyer_model', 'M_buyer');
		$this->load->model('seller/Seller_model', 'M_seller');
		$this->load->model('transaction/Transaction_model', 'M_transaction');
		$this->load->model('Form_generator_model', 'M_form_gen');

		$this->allowed = ['buyers', 'buyer_employment_table', 'buyer_identifications', 'projects', 'properties', 'house_models', 'house_model_interiors', 'transactions', 'transaction_sellers', 'transaction_clients', 'transaction_properties', 'sellers', 'seller_contact_infos', 'seller_educations', 'seller_exams', 'seller_positions', 'seller_references', 'seller_source_infos', 'seller_trainings', 'seller_work_experiences', 'additional_information', "companies"];

		$this->adttl_info_fields = ['present_date', 'present_day', 'present_month', 'present_year', 'cancellation_date', 'expiration_date', 'reservation_date', 'downpayment_date', 'loan_date', 'equity_date', 'reservation_total_rate', 'downpayment_total_rate', 'loan_total_rate', 'equity_total_rate', 'reservation_monthly_rate', 'downpayment_monthly_rate', 'loan_monthly_rate', 'equity_monthly_rate', 'reservation_terms', 'downpayment_terms', 'loan_terms', 'equity_terms', 'reservation_interest', 'downpayment_interest', 'loan_interest', 'equity_interest', 'total_amount_due', 'total_contract_price', 'reservation_fee', 'due_date', 'days_delayed', 'months_delayed', 'cancellation_date', 'cancellation_reason', 'number_to_words'];
	}

	public function index()
	{
		$this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
		$this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

		$this->template->build('index');
	}

	public function create()
	{
		$this->js_loader->queue(['tinymce/tinymce.min.js']);


		$this->view_data['table_list'] = $this->db->list_tables();

		$this->view_data['allowed'] = $this->allowed;


		if ($this->input->post()) {

			$this->form_validation->set_rules($this->fields);

			if ($this->form_validation->run() === TRUE) {

				$post = $this->input->post();

				$config = array();
				$config['upload_path'] = './assets/uploads/form_generator/header';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 5000;
				$config['encrypt_name'] = TRUE;
				$this->load->library('upload', $config, 'header_upload');
				$this->header_upload->initialize($config);

				$config = array();
				$config['upload_path'] = './assets/uploads/form_generator/footer';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 5000;
				$config['encrypt_name'] = TRUE;
				$this->load->library('upload', $config, 'footer_upload');
				$this->footer_upload->initialize($config);

				if ($_FILES['header']['name']) {
					if (!$this->header_upload->do_upload('header')) {
						$this->notify->error($this->upload->display_errors());
					} else {
						$h_data = $this->header_upload->data();

						$post['header'] = $h_data['file_name'];
					}
				}

				if ($_FILES['footer']['name']) {
					if (!$this->footer_upload->do_upload('footer')) {
						$this->notify->error($this->upload->display_errors());
					} else {
						$f_data = $this->footer_upload->data();

						$post['footer'] = $f_data['file_name'];
					}
				}

				$insert_id = $this->M_form_gen->insert($post);

				if ($insert_id !== FALSE) {

					// $this->template->build('index');
					// redirect('form_generator/generate_pdf/'.$insert_id, 'refresh');

					redirect('form_generator', 'refresh');

					// header('form_generator/generate_pdf/'.$insert_id);
				}
			} else {

				// Error
				$this->notify->error('Oops something went wrong.');
			}
		}
		$this->template->build('create', $this->view_data);
	}

	public function update($id = FALSE)
	{
		if ($id) {

			$this->js_loader->queue(['tinymce/tinymce.min.js']);

			$this->view_data['table_list'] = $this->db->list_tables();

			$this->view_data['allowed'] = $this->allowed;

			$this->view_data['form'] = $data =  $this->M_form_gen->get($id);

			if ($data) {

				if ($this->input->post()) {

					$this->form_validation->set_rules($this->fields);

					if ($this->form_validation->run() === TRUE) {

						$post = $this->input->post();

						$config = array();
						$config['upload_path'] = './assets/uploads/form_generator/header';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size'] = 5000;
						$config['encrypt_name'] = TRUE;
						$this->load->library('upload', $config, 'header_upload');
						$this->header_upload->initialize($config);

						$config = array();
						$config['upload_path'] = './assets/uploads/form_generator/footer';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size'] = 5000;
						$config['encrypt_name'] = TRUE;
						$this->load->library('upload', $config, 'footer_upload');
						$this->footer_upload->initialize($config);

						if ($_FILES['header']['name']) {
							if (!$this->header_upload->do_upload('header')) {
								$this->notify->error($this->upload->display_errors());
							} else {
								// unlink old image from path before upload
								unlink($this->header_upload->upload_path . $data['header']);

								$h_data = $this->header_upload->data();

								$post['header'] = $h_data['file_name'];
							}
						} else {
							$post['header'] = $data['header'];
						}

						if ($_FILES['footer']['name']) {
							if (!$this->footer_upload->do_upload('footer')) {
								$this->notify->error($this->upload->display_errors());
							} else {
								// unlink old image from path before upload
								unlink($this->footer_upload->upload_path . $data['footer']);

								$f_data = $this->footer_upload->data();

								$post['footer'] = $f_data['file_name'];
							}
						} else {
							$post['footer'] = $data['footer'];
						}

						$result = $this->M_form_gen->update($post, $data['id']);

						if ($result !== FALSE) {

							redirect('form_generator', 'refresh');
							// redirect('form_generator/generate_pdf/'.$data['id'], 'refresh');
							// header('form_generator/generate_pdf/'.$data['id']);

						}
					} else {

						// Error
						$this->notify->error('Oops something went wrong.');
					}
				}

				$this->template->build('update', $this->view_data);
			} else {

				show_404();
			}
		} else {

			show_404();
		}
	}

	public function delete()
	{
		$response['status']	= 0;
		$response['message'] = 'Oops! Please refresh the page and try again.';

		$id	= $this->input->post('id');
		if ($id) {

			$list = $this->M_form_gen->get($id);
			if ($list) {

				$deleted = $this->M_form_gen->delete($list['id']);
				if ($deleted !== FALSE) {

					$response['status']	= 1;
					$response['message'] = 'Template successfully deleted';
				}
			}
		}

		echo json_encode($response);
		exit();
	}

	public function generate_pdf($id = FALSE, $foreign_id = 0, $debug = 0)
	{
		error_reporting(0);
		if ($id) {
			$this->load->helper(['dompdf', 'images']);

			$data['data'] = $this->M_form_gen->get($id);

			if ($foreign_id) {

				$transactions = $this->M_transaction
					->with_buyer()->with_seller()->with_project()->with_property()->with_scheme()
					->with_t_property()->with_billings()->with_buyers()
					->with_sellers()->with_fees()->with_promos()->with_payments()->with_payments()
					->get($foreign_id);

				$buyer_id = $transactions['buyer_id'];
				$seller_id = $transactions['seller_id'];
				$project_id = $transactions['project_id'];
				$property_id = $transactions['property_id'];

				$buyer = $this->M_buyer->with_employment()->with_identifications()->get($buyer_id);

				$seller = $this->M_seller->with_source()->with_reference()->with_work_experience()->with_academic()->with_exam()
					->with_training()->with_position()->get($seller_id);

				// vdebug($info);

				if (!empty($data['data']['content'])) {

					preg_match_all("/{{+(.*?)}}/", $data['data']['content'], $fields_array);

					$selected_fields = array();
					$last = count($fields_array);

					unset($fields_array[$last - 1]);

					foreach ($fields_array[0] as $key => $value) {

						if (!empty($value)) {
							if (!preg_match('/clients/', $value)) {

								$value = str_replace('{{', '', $value);
								$value = str_replace('}}', '', $value);
								$def_val = $value;
								$new_val = explode('.', $def_val);
								$selected_fields[] = $new_val;
							}
						}
					}
					// vdebug($info);
					// vdebug($buyer);
					// vdebug($selected_fields);

					$results['transactions'] = $transactions;
					$results['buyer'] = $buyer;
					$results['properties'] = $transactions['property'];

					foreach ($selected_fields as $key => $selected_field) {

						$default_field = '{{' . $selected_field[0] . '.' . $selected_field[1] . '}}';

						$value = "n/a";

						$clean_val = $this->clean_key($selected_field[1]);

						$value = $this->process_array($clean_key, $results, $clean_val);

						if (!$value) {
							$clean_key = $this->clean_key($selected_field[0]);
							$value = $this->process_array($clean_key, $results, $clean_val);
						}

						if ($clean_key == "additional_information") {

							$value = generate($foreign_id, $clean_val);
						}

						// Check if number_to_words attribute has been added
						if (isset($selected_field[2])) {
							$default_field = '{{' . $selected_field[0] . '.' . $selected_field[1] . '.' . $selected_field[2] . '}}';
							$value = htmlspecialchars($value);
							$value_arr = explode(" ", $value);
							if (isset($value_arr[2])) {
								$value = number_to_words($value_arr[2]);
							} else {
								$value = number_to_words($value);
							}
						}

						if (!$value) {
							$value = 'n/a';
						}

						$data['data']['content'] = str_replace($default_field, "<u><b>" . $value . "</b></u>", $data['data']['content']);
					}
				}
			}

			if ($debug) {
				vdebug($results);
			}

			$generateHTML = $this->load->view('form_template', $data, true);

			echo $generateHTML;
			die();

			pdf_create($generateHTML, 'generated_form');
		}
	}

	public function get_table_fields($TblName = FALSE)
	{

		if ($this->input->is_ajax_request()) {


			if ($TblName == "additional_information") {
				$fields = $this->adttl_info_fields;
			} else {
				$fields = $this->db->list_fields($TblName);
			}


			echo json_encode($fields);
			exit(0);
		} else {
			show_404();
		}
	}

	public function showForms()
	{

		$columnsDefault = [
			'id'     => true,
			'name'      => true,
			'description'      => true,
			'created_at'     => true,
			'created_by' => true,
			'updated_by' => true,
		];

		if (isset($_REQUEST['columnsDef']) && is_array($_REQUEST['columnsDef'])) {
			$columnsDefault = [];
			foreach ($_REQUEST['columnsDef'] as $field) {
				$columnsDefault[$field] = true;
			}
		}

		// get all raw data
		$alldata = $this->M_form_gen->as_array()->get_all();

		foreach ($alldata as $key => $value) {

			$alldata[$key]['created_by'] = '<div><a href="/user/view/' . $value['created_by'] . '" target="_blank">' . get_person_name($value['created_by'], "users") . '</a><div>' . ($value['created_at'] ? view_date($value['created_at']) : '') . '</div></div>';

			$alldata[$key]['updated_by'] = '<div><a href="/user/view/' . $value['updated_by'] . '" target="_blank">' . get_person_name($value['updated_by'], "users") . '</a><div>' . ($value['updated_at'] ? view_date($value['updated_at']) : '') . '</div></div>';
		}

		$data = [];
		// internal use; filter selected columns only from raw data
		foreach ($alldata as $d) {
			$data[] = $this->filterArray($d, $columnsDefault);
		}

		// count data
		$totalRecords = $totalDisplay = count($data);

		// filter by general search keyword
		if (isset($_REQUEST['search'])) {
			$data         = $this->filterKeyword($data, $_REQUEST['search']);
			$totalDisplay = count($data);
		}

		if (isset($_REQUEST['columns']) && is_array($_REQUEST['columns'])) {
			foreach ($_REQUEST['columns'] as $column) {
				if (isset($column['search'])) {
					$data         = $this->filterKeyword($data, $column['search'], $column['data']);
					$totalDisplay = count($data);
				}
			}
		}

		// sort
		if (isset($_REQUEST['order'][0]['column']) && $_REQUEST['order'][0]['dir']) {

			$_column = $_REQUEST['order'][0]['column'] - 1;
			$_dir = $_REQUEST['order'][0]['dir'];

			usort($data, function ($x, $y) use ($_column, $_dir) {

				// echo "<pre>";
				// print_r($x) ;echo "<br>";
				// echo $_column;echo "<br>";

				$x = array_slice($x, $_column, 1);

				// vdebug($x);

				$x = array_pop($x);

				$y = array_slice($y, $_column, 1);
				$y = array_pop($y);

				if ($_dir === 'asc') {

					return $x > $y ? true : false;
				} else {

					return $x < $y ? true : false;
				}
			});
		}

		// pagination length
		if (isset($_REQUEST['length'])) {
			$data = array_splice($data, $_REQUEST['start'], $_REQUEST['length']);
		}

		// return array values only without the keys
		if (isset($_REQUEST['array_values']) && $_REQUEST['array_values']) {
			$tmp  = $data;
			$data = [];
			foreach ($tmp as $d) {
				$data[] = array_values($d);
			}
		}
		$secho = 0;
		if (isset($_REQUEST['sEcho'])) {
			$secho = intval($_REQUEST['sEcho']);
		}

		$output = array(
			'sEcho' => $secho,
			'sColumns' => '',
			'iTotalRecords' => $totalRecords,
			'iTotalDisplayRecords' => $totalDisplay,
			'data' => $data
		);

		echo json_encode($output);
		exit();
	}

	public function clean_key($key = '')
	{

		if ($key == "buyer_identifications") {
			$key = 'identifications';
		}

		return $key;
	}

	public function process_array($clean_key = '', $results = array(), $clean_val = '')
	{
		$rows = array_key_exists_r($clean_key, $results, $clean_val);

		if (isset($rows[$clean_key][$clean_val])) {
			$value = $rows[$clean_key][$clean_val];
		} else {
			$value = @$rows[$clean_key][0][$clean_val];
		}

		return $value;
	}

	public function additionals($transaction_id = 0, $value = "")
	{

		$this->transaction_library->initiate($transaction_id);
	}
}
