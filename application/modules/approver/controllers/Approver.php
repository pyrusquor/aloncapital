<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Approver extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $external_models = array(
            'vehicle/Vehicle_model' => 'M_vehicle',
            'company/Company_model' => 'M_company',
        );
        $approver_model = array(
            'approver/Approver_model' => 'M_approver',
        );

        // Load models
        $this->load->model($external_models);
        $this->load->model($approver_model);
        

        // Load pagination library
        $this->load->library('ajax_pagination');

        // Format Helper
        $this->load->helper(['format', 'images']); // Load Helper

        // Per page limit
        $this->perPage = 12;

        $this->_table_fillables = $this->M_approver->fillable;
        $this->_table_columns = $this->M_approver->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'is_active' => 1,
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];

        $this->load->helper('format');
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_approver->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_approver->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }

        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
	{
		$columnsDefault = [
			'id' => true,
			'module_id' => true,
			'department_id' => true,
			'approving_staff_id' => true,
		];
		$draw = $_REQUEST['draw'];
		$start = $_REQUEST['start'];
		$rowperpage = $_REQUEST['length'];
		$columnIndex = $_REQUEST['order'][0]['column'];
		$columnName = $_REQUEST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_REQUEST['order'][0]['dir'];
		$searchValue = $_REQUEST['search']['value'] ?? null;

		$filters = [];
		parse_str($_POST['filter'], $filters);
		$items = [];
		$totalRecordwithFilter = 0;
		$filteredItems = [];

		for ($query_loop = 0; $query_loop < 2; $query_loop++) {

			$query = $this->M_approver
				->with_staff(['fields' => 'last_name,first_name'])
				->with_module(['fields' => 'name'])
				->with_department(['fields' => 'name'])
				->order_by($columnName, $columnSortOrder)
				->as_array();

			$advanceSearchValues = [
				'id' => [
					'data' => $filters['approver_id'] ?? null,
					'operator' => '=',
				],
				'module_id' => [
					'data' => $filters['module_id'] ?? null,
					'operator' => '=',
				],
				'department_id' => [
					'data' => $filters['department_id'] ?? null,
					'operator' => '=',
				],
				'approving_staff_id' => [
					'data' => $filters['approving_staff_id'] ?? null,
					'operator' => '=',
				],
			];

			// Advance Search
			foreach ($advanceSearchValues as $key => $value) {

				if ($value['data']) {

					$query->where($key, $value['operator'], $value['data']);
				}
			}

			if ($query_loop) {

				$totalRecordwithFilter = $query->count_rows();
			} else {

				$query->limit($rowperpage, $start);
				$items = $query->get_all();

				if (!!$items) {
					// Transform data
					foreach ($items as $key => $value) {
                        $items[$key]['module_id'] = $value['module']->name;
                        $items[$key]['department_id'] = $value['department']['name'];
                        $items[$key]['approving_staff_id'] = $value['staff']['first_name'] . ' ' . $value['staff']['last_name'];
					}

					foreach ($items as $item) {
						$filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
					}
				}
			}
		}

		$totalRecords = $this->M_approver   ->count_rows();

		$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"data" => $filteredItems,
			"request" => $_REQUEST
		);

		echo json_encode($response);
		exit();
	}

    public function view($id = false)
    {
        if ($id) {

            $approver_data = $this->M_approver->with_staff(['fields' => 'last_name,first_name, middle_name'])->with_module(['fields' => 'name'])->with_department(['fields' => 'name'])->get($id);

            $this->view_data['approver'] = @$approver_data['staff']['first_name'] . ' ' . @$approver_data['staff']['middle_name'] . ' ' . @$approver_data['staff']['last_name'];
            $this->view_data['module'] = @$approver_data['module']->name;
            $this->view_data['department'] = @$approver_data['department']['name'];
            $this->view_data['id'] = @$approver_data['id'];

            if ($this->view_data) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            redirect(base_url('approver'));
        }
    }

    public function form($id = false)
    {

        $method = "Create";
        if ($id) {$method = "Update";}

        if ($this->input->post()) {

            $response['status'] = 0;
            $response['msg'] = 'Oops! Please refresh the page and try again.';

            $info = $this->input->post();

            if($id) {

                $result = $this->M_approver->update($info, $id);

            } else {

                $result = $this->M_approver->insert($info);
            }


            if ($result === false) {

                // Validation
                $this->notify->error('Oops something went wrong.');
            } else {

                // Success
                $this->notify->success('Approver successfully' . " " . strtolower($method) . 'd', 'approver');
            }
        }

        if ($id) {
            $this->view_data['info'] = $info = $this->M_approver->with_staff(['fields' => 'id, last_name,first_name, middle_name'])->with_module(['fields' => 'id, name'])->with_department(['fields' => 'id, name'])->get($id);
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_approver->get($id);
            if ($list) {

                $deleted = $this->M_approver->delete($list['id']);
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Approver setting successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_approver->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Approver setting/s successfully deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

}