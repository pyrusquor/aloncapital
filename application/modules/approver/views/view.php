<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Approver</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('approver/update/' . $id); ?>"
                    class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('approver'); ?>"
                    class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Approver
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Staff Name
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold"><?= $approver ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Department
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $department; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6 class="kt-widget13__desc">
                                            Module
                                        </h6>
                                        <p class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $module; ?></p>
                                    </div>

                                </div>
                                <!--end::Form-->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                    
                </div>
                <!--end::Portlet-->
            </div>


            <!--end::Portlet-->
        </div>
    </div>

</div>
<!-- begin:: Footer -->