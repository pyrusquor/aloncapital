<?php
// ==================== begin: Add model fields ====================
$module_id = @$info['module']->id;
$module_name = @$info['module']->name;
$staff_id = @$info['staff']['id'];
$staff_name = @$info['staff']['last_name'] . ' ' . @$info['staff']['first_name']; 
$department_id = @$info['department']['id'];
$department_name = @$info['department']['name'];
// ==================== end: Add model fields ====================

?>
<!-- ==================== begin: Add form model fields ==================== -->
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Modules <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="modules" id="module_id" name="module_id">
                <option value="">Select module</option>
                <?php if ($module_id): ?>
                    <option value="<?= $module_id; ?>" selected><?= $module_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Department <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="departments" id="department_id" name="department_id">
                <option value="">Select Department</option>
                <?php if ($department_id): ?>
                    <option value="<?= $department_id; ?>" selected><?= $department_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Approving Staff <span class="kt-font-danger">*</span></label>
            <select class="form-control suggests" data-module="staff" data-type='person' id="approving_staff_id" name="approving_staff_id">
                <option value="">Select Staff</option>
                <?php if ($staff_id): ?>
                    <option value="<?= $staff_id; ?>" selected><?= $staff_name; ?></option>
                <?php endif ?>
            </select>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>
<!-- ==================== end: Add form model fields ==================== -->

