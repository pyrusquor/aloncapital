<?php
$id = isset($sub_warehouse_inventory_logs['id']) && $sub_warehouse_inventory_logs['id'] ? $sub_warehouse_inventory_logs['id'] : '';
$company = isset($sub_warehouse_inventory_logs['company']) && $sub_warehouse_inventory_logs['company'] ? $sub_warehouse_inventory_logs['company'] : '';
$sbu = isset($sub_warehouse_inventory_logs['sbu']) && $sub_warehouse_inventory_logs['sbu'] ? $sub_warehouse_inventory_logs['sbu'] : '';
$warehouse = isset($sub_warehouse_inventory_logs['warehouse']) && $sub_warehouse_inventory_logs['warehouse'] ? $sub_warehouse_inventory_logs['warehouse'] : '';
$item_code = isset($sub_warehouse_inventory_logs['item_code']) && $sub_warehouse_inventory_logs['item_code'] ? $sub_warehouse_inventory_logs['item_code'] : '';
$item_name = isset($sub_warehouse_inventory_logs['item_name']) && $sub_warehouse_inventory_logs['item_name'] ? $sub_warehouse_inventory_logs['item_name'] : '';
$in = isset($sub_warehouse_inventory_logs['in']) && $sub_warehouse_inventory_logs['in'] ? $sub_warehouse_inventory_logs['in'] : '';
$out = isset($sub_warehouse_inventory_logs['out']) && $sub_warehouse_inventory_logs['out'] ? $sub_warehouse_inventory_logs['out'] : '';
$balance = isset($sub_warehouse_inventory_logs['balance']) && $sub_warehouse_inventory_logs['balance'] ? $sub_warehouse_inventory_logs['balance'] : '';
$remarks = isset($sub_warehouse_inventory_logs['remarks']) && $sub_warehouse_inventory_logs['remarks'] ? $sub_warehouse_inventory_logs['remarks'] : '';
$is_active = isset($sub_warehouse_inventory_logs['is_active']) && $sub_warehouse_inventory_logs['is_active'] ? $sub_warehouse_inventory_logs['is_active'] : '';

?>

<div class="row">
<div class="col-sm-6">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="company" value="<?php echo set_value('company', $company); ?>" placeholder="Company" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-building"></i></span>
            </div>
            <?php echo form_error('company'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>SBU <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="sbu" value="<?php echo set_value('sbu', $sbu); ?>" placeholder="SBU" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-barcode"></i></span>
            </div>
            <?php echo form_error('sbu'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Warehouse <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="warehouse" value="<?php echo set_value('warehouse', $warehouse); ?>" placeholder="Warehouse" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-chevron-left"></i></span>
            </div>
            <?php echo form_error('warehouse'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item Code <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_code" value="<?php echo set_value('item_code', $item_code); ?>" placeholder="Item Code" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-code"></i></span>
            </div>
            <?php echo form_error('item_code'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Item name <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="item_name" value="<?php echo set_value('item_name', $item_name); ?>" placeholder="Item name" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-mail-reply"></i></span>
            </div>
            <?php echo form_error('item_name'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>In <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="in" value="<?php echo set_value('in', $in); ?>" placeholder="In" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sort-desc"></i></span>
            </div>
            <?php echo form_error('in'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Out <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="out" value="<?php echo set_value('out', $out); ?>" placeholder="Out" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-sort-asc"></i></span>
            </div>
            <?php echo form_error('out'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Balance <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="number" class="form-control" name="balance" value="<?php echo set_value('balance', $balance); ?>" placeholder="Balance" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-balance-scale"></i></span>
            </div>
            <?php echo form_error('balance'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Remarks <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" name="remarks" value="<?php echo set_value('remarks', $remarks); ?>" placeholder="Remarks" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-edit"></i></span>
            </div>
            <?php echo form_error('remarks'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Is Active <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon">
            <?php echo form_dropdown('is_active', Dropdown::get_static('warehouse_status'), set_value('is_active', @$is_active), 'class="form-control"'); ?>
            </div>
            <?php echo form_error('is_active'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== begin: Add form model fields ==================== -->

    <!-- ==================== end: Add form model fields ==================== -->
</div>