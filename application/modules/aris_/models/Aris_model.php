<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aris_model extends MY_Model
{
    public $table = 'accounting_entries_2'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'or_number',
        'invoice_number',
        'journal_type',
        'payment_date',
        'payee_type',
        'payee_type_id',
        'remarks',
        'cr_total',
        'dr_total',
        'is_approve',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'or_number' => array(
            'field' => 'or_number',
            'label' => 'OR Number',
            'rules' => 'trim|required',
        ),
        'invoice_number' => array(
            'field' => 'invoice_number',
            'label' => 'Invoice Number',
            'rules' => 'trim|required',
        ),
        'journal_type' => array(
            'field' => 'journal_type',
            'label' => 'Journal Type',
            'rules' => 'trim|required',
        ),
        'payment_date' => array(
            'field' => 'payment_date',
            'label' => 'Payment Date',
            'rules' => 'trim|required',
        ),
        'payee_type' => array(
            'field' => 'payee_type',
            'label' => 'Payee Type',
            'rules' => 'trim|required',
        ),
        'payee_type_id' => array(
            'field' => 'payee_type_id',
            'label' => 'Payee Type ID',
            'rules' => 'trim|required',
        ),
        'remarks' => array(
            'field' => 'remarks',
            'label' => 'Remarks',
            'rules' => 'trim',
        ),
        'cr_total' => array(
            'field' => 'cr_total',
            'label' => 'CR Total',
            'rules' => 'trim|required',
        ),
        'dr_total' => array(
            'field' => 'dr_total',
            'label' => 'DR Total',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // $this->has_many['table_name'] = array();
        $this->has_many['accounting_entry_items'] = array('foreign_model' => 'Accounting_entry_items_model', 'foreign_table' => 'accounting_entry_items', 'foreign_key' => 'accounting_entry_id', 'local_key' => 'id');
        $this->has_one['buyers'] = array('foreign_model' => 'Buyer_model', 'foreign_table' => 'buyers', 'foreign_key' => 'id', 'local_key' => 'payee_type_id');
        $this->has_one['sellers'] = array('foreign_model' => 'Seller_model', 'foreign_table' => 'sellers', 'foreign_key' => 'id', 'local_key' => 'payee_type_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH . '/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for ($x = 0; $x < 10; $x++) {
            array_push($data, array(
                'or_number' => $faker->randomNumber($nbDigits = null, $strict = false),
                'invoice_number' => $faker->randomNumber($nbDigits = null, $strict = false),
                'journal_type' => $faker->numberBetween($min = 1, $max = 5),
                'payment_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'payee_type' => $faker->numberBetween($min = 1, $max = 2),
                'payee_type_id' => $faker->numberBetween($min = 1, $max = 9999),
                'remarks' => $faker->sentence(),
                'cr_total' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
                'dr_total' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }

    public function get_all_project($project = "",$password = "",$selected = "",$last_id = 0) {
        error_reporting(E_ALL);
        $this->load_project_database($project,$password);
        $tables = $this->aris_tables($selected);
        $data[$project] = array();

        if ($tables) {
            foreach ($tables as $key => $table) {
                $where = array();
                $w = "";
                if ( ($table == "payments") && ($last_id) ) {
                    $where['PaymentID >'] = (int) $last_id;
                    $w = " WHERE PaymentID > ".$last_id;
                }
                if ( ($table == "collectibles") && ($last_id) ) {
                    $where['DateDue >'] =  "#".$last_id."#";
                    $w = " WHERE DateDue > #".$last_id."#";
                }
                if ( ($table == "ar") && ($last_id) ) {
                    $where['DocumentID >'] = (int) $last_id;
                    $w = " WHERE DocumentID > ".$last_id;
                }
                $q = $this->aris_project->query("SELECT * FROM ([".$table."])".$w);
                // $query = $this->aris_project->get_where('['.$table.']',$where);
                $data[$project][$table] =  $q->result_array();
                // echo $this->aris_project->last_query();die();

            }
        }
        // echo $this->aris_project->last_query();die();
        $this->aris_project->close();
        return $data;

    }

    public function load_project_database($project = "",$password = ""){
        $this->load->database();
        if(ENVIRONMENT=='development'){
            $dbName  = $_SERVER["DOCUMENT_ROOT"] . "/aris_files/".$project.".mdb";
        } else{
            $dbName  = $_SERVER["DOCUMENT_ROOT"] . "/rems/aris_files/".$project.".mdb";
        }

        // $s = "//192.168.0.51/carm/WORKING ARIS (MAIN)/";
        // $s = "//192.168.0.195/WORKING_ARIS_MAIN/";
        // $dbName = $s.$project.".mdb";

        // This change should make sure that PHP use the 64bit odbc driver. Make sure that you are using 64bit PHP
        $db_conn = 'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ='.$dbName;
        $db['aris_project']['hostname'] = $db_conn;
        $db['aris_project']['password'] = $password;
        $db['aris_project']['database'] = $db_conn;
        $db['aris_project']['username'] = "sa";
        $db['aris_project']['dbdriver'] = 'odbc';
        $db['aris_project']['dbprefix'] = '';
        $db['aris_project']['pconnect'] = TRUE;
        $db['aris_project']['db_debug'] = TRUE;
        $db['aris_project']['cache_on'] = FALSE;
        $db['aris_project']['cachedir'] = '';
        $db['aris_project']['char_set'] = 'utf8';
        $db['aris_project']['dbcollat'] = 'utf8_general_ci';
        $db['aris_project']['swap_pre'] = '';
        $db['aris_project']['autoinit'] = TRUE;
        $db['aris_project']['stricton'] = FALSE;
        // echo "<pre>";print_r($db);die();
        $this->aris_project = $this->load->database($db['aris_project'], TRUE);
    }

    public function save_aris($table = "",$params = array(),$batch = 0) {
        error_reporting(E_ALL);
        $this->aris = $this->load->database('aris', TRUE);
        // echo "<pre>";
        // echo $table;echo "<br>";
        // echo $batch;echo "<br>";
        // print_r($params);
        // die();
        if ($batch == 1) {
            //insert
            $id = $this->aris->insert_batch($table, $params);
        } else if ($batch == 2) {
            //update
            $id = $this->aris->update_batch($table, $params, 'id');
            // echo $this->aris->last_query();die();
        }  else if (isset($params['id']) && !empty($params['id']))  {
            $this->aris->where('id', $params['id']);
            $this->aris->update($table, $params);
            $id = $params['id'];
        } else {
            $query = $this->aris->insert($table, $params);
            $id = $this->aris->insert_id();
        }
            
        return true;
    }

    public function get_aris($table = "",$params = array()) {

        if( !empty($params['where']) && isset($params['where']) ){
            foreach($params['where'] as $key => $value){
                $this->db->where($key, $value);
            }
        } 
        if( !empty($params['limit']) && isset($params['limit']) ){
            $this->db->limit( $params['limit'], $params['offset'] );
        }

        if( !empty($params['sort_by']) && isset($params['sort_order']) ){
            $this->db->order_by( $params['sort_by'], $params['sort_order'] );
        }

        $query = $this->db->get($table);

        if (isset($params['row'])) {
            $result = $query->row_array();
        } else {
            $result = $query->result_array();
        }

        // if ($table != "project_information") {
        //     echo "<pre>";
        //     echo $this->aris->last_query();
        //     echo "<br>";
        //     // print_r($params);
        //     echo "<br>";
        //     print_r($result);
        //     echo "<br>";
        //     // echo $table;
        //     die();
        // }

        return $result;
    }

    public function aris_tables($selected = ""){
        $data = array(
            'project information',
            'lots',
            'aging',
            'amortization',
            'amortization calculator',
            'ar',
            'collectibles',
            'customers',
            'documents',
            'pastdue',
            'documents',
            'payments',
            'sales agent'
        );

        if ($selected) { $data =array(); $data[0] = 'project information'; $data[] = $selected; }
        return $data;
    }

}
