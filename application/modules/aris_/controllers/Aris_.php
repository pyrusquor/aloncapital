<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aris_ extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('aris/Aris_model', 'Aris_model');
        $this->load->model('buyer/Buyer_model', 'M_buyer');
        $this->load->model('seller/Seller_model', 'M_seller');
        $this->load->model('auth/Ion_auth_model', 'M_auth');
        $this->load->model('user/User_model', 'M_user');
        // $this->arisdb = $this->load->database('aris', TRUE);
    }
    
    public function index(){
        $post = $this->input->post();
        $project_id = $post['project_id'];
        $this->aris($project_id);
    }

    // SYNCING
    public function sync_all($count = 0){
        for ($i=1; $i < $count; $i++) { 
            $this->get($i,'customers');
        }
    }

    public function get($project_id = "",$table = ""){
        error_reporting(E_ALL);
        $where = array();

       // $tables = array('customers','documents','sales_agent','lots','payments','collectibles','ar','pd','pastdue');
        $tables = array('customers','documents','sales_agent','lots','pd','pastdue','payments');

        // $tables = array('41');
        // $tables = array('customers');

        if ($table) {
            $tables = array($table);
        }


        $params['where']['id'] = $project_id;
        $project_files = $this->Aris_model->get_aris('aris_project_information',$params);


        $drop_tables = array('aris_pd','pastdue');
        $large_tables = array('ar','payments','collectibles');
        $final_tables = array();
        $result = array();

        if ($project_files) {
            foreach ($project_files as $key => $file) {
                if ($tables) {
                    foreach ($tables as $key => $table) {
                        $total_init_count = 0;

                        $table = str_replace('_', ' ', $table);

                        $params_init['where']['project_id'] = $project_id;
                        $params_init['sort_order'] = 'DESC';
                        $params_init['row'] = '1';
                        $params_init['limit'] = '1';
                        $params_init['offset'] = '0';

                        $last_id = 0;
                        if (in_array($table, $large_tables)) {
                            if ($table == "payments") { $params_init['sort_by'] = 'PaymentID'; }
                            if ($table == "collectibles") { $params_init['sort_by'] = 'DateDue'; }
                            if ($table == "ar") { $params_init['sort_by'] = 'DocumentID'; }

                            $ttbl = "aris_".$table;
                            $init = $this->Aris_model->get_aris($ttbl,$params_init);

                            // if ($table == "payments") {
                                // echo "<pre>";print_r($init);echo "</pre>";die();
                            // }
                            // if ($table == "payments") {
                            //     echo "<pre>";
                            //     echo "init ";
                            //     vdebug($init);
                            //     echo "</pre>";
                            //     // die();
                            // }

                            if (($table == "payments")&&($init)) { $last_id = $init['PaymentID']; }
                            if (($table == "collectibles")&&($init)) { $last_id = date('m-d-Y',strtotime($init['DateDue'])); }
                            if (($table == "ar")&&($init)) { $last_id = $init['DocumentID']; }

                        }

                        $filename = get_value_field_aris($file['Password'],'project_files','filename','password');

                        // vdebug($filename);

                        $results = $this->Aris_model->get_all_project($filename,$file['Password'],$table,$last_id); // info from sql
                        //vdebug($results);

                        if ($results) {
                            $final_tables[$table] = $this->save_project_information($results,$table);
                            // vdebug($final_tables);
                            // echo "<pre>";
                            // print_r($final_tables);
                            // echo "</pre>";
                        }
                    }
                }
            }
        }
        
        $result['results'] = $final_tables;
        $result['view'] = $results;

         echo json_encode($result);
        //redirect('aris');
    }

    public function save_aris_files(){
        $main = "\\\\rebg-carmela\\CARM\\WORKING ARIS (MAIN)";
        $path = $main;

        $files = $this->scan_aris_directory($path);
    }

    public function scan_aris_directory($target) {
            // print_r($target);die();

        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

            // print_r($files);die();

            foreach( $files as $file ) {

                $directory = explode('\\', $file);

                // print_r($directory);
                // echo $file;echo "<br>";

                if ($directory) { 

                    $filename = @$directory[5];

                    // if ($filename == "Sta. Maria Village.mdb") {
                        // continue;
                    // }

                    $filename_ext = explode('.', $filename);

                    if ( ($filename_ext) && (isset($filename_ext[1])) ) {

                        $ext = $filename_ext[1];
                        $file_name = $filename_ext[0];

                        if ($ext == "mdb") {

                            $project_file = $this->Aris_model->get_aris('aris_project_files',array('where' => array('filename' => $file_name)));


                            if (empty($project_file)) {
                                $data['filename'] = $file_name;
                                $data['created_at'] = date('Y-m-d H:i:s');
                                $this->Aris_model->save_aris('aris_project_files',$data);
                                echo $file_name." - Success";
                                echo "<br>";
                            } 



                            if (empty($project_file)) {
                                $data['filename'] = $file_name;
                                $data['created_at'] = date('Y-m-d H:i:s');
                                $this->Aris_model->save_aris('aris_project_files',$data);
                                echo $file_name." - Success";
                                echo "<br>";
                            } 

                            // echo $file_name." - Success";
                            // echo "<br>";


                        }
                    }
                }
                $this->scan_aris_directory( $file );
            }
        } 
        // else {
        //     echo "string";
        //     die();
        // }
    }

    public function save_project_information($results = array(),$selected = ''){
        error_reporting(E_ALL);
        $project_id = 0;

        $drop_tables = array('ar','pd','pastdue');
        $large_tables = array('payments','collectibles');
        $continue = array('ar');
        $i = 0;$u = 0;

        // echo "<pre>";print_r($results);echo "</pre>";die();

        $info_insert = array();
        $info_update = array();

        if ( !empty($results) && isset($results) ) {
            foreach ($results as $key => $result) {
                $project_info = $result['project information'];

                if ($project_info) {
                    foreach ($project_info as $key => $info) {
                        $table = "aris_project_information";
                        $where = array('where' => array('Password' => $info['Password'], 'Confirmation' => $info['Confirmation']));
                        $data = $this->Aris_model->get_aris($table,$where);


                        $project_id = @$data[0]['id'];
                        if (empty($project_id)) {
                            $info['date_created'] = date('Y-m-d H:i:s');
                            $a = $this->Aris_model->save_aris('aris_project_information',$info);
                        } else {
                            $info['date_updated'] = date('Y-m-d H:i:s');
                            $info['last_date_synced'] = date('Y-m-d H:i:s');
                            $info['id'] = $project_id;
                            $a = $this->Aris_model->save_aris('aris_project_information',$info);
                        }


                    }
                }
                
                if ( !empty($result[$selected]) && isset($result[$selected]) ) {
                    $infos = $result[$selected];
                    foreach ($infos as $key => $info) {
                        $id = 0;
                        $where = array();
                        $selected = str_replace(' ', '_', $selected);
                        $info['project_id'] = $project_id;
                        $where['project_id'] = $project_id;

                        if ($selected == "lots") {
                            $where['LotID'] = $info['LotID'];
                        }
                        if ($selected == "customers") {
                            $where['CUSTOMERID'] = $info['CUSTOMERID'];
                        }
                        if ($selected == "sales_agent") {
                            $where['SalesAgentID'] = $info['SalesAgentID'];
                        }
                        
                        if ($selected == "payments") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DATE_DUE'] = $info['DATE_DUE'];
                            $where['PaymentID'] = $info['PaymentID'];
                        }
                        
                        if ($selected == "collectibles") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DateDue'] = $info['DateDue'];
                        }
                        if ($selected == "pastdue") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DateDue'] = $info['DateDue'];
                        }
                        if ($selected == "ar") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DateDue'] = $info['DateDue'];
                        }

                        if ($selected == "pd") {
                            $where['DocumentID'] = $info['DocumentID'];
                        }
                        if ($selected == "documents") {
                            $where['DocumentID'] = $info['DocumentID'];
                        }

                        $new_selected = "aris_".$selected;

                        $where_params['where'] = $where; 
                        if (!in_array($selected, $continue)) {
                            $data = $this->Aris_model->get_aris($new_selected,$where_params);
                            $id = @$data[0]['id'];
                        }
                        if (empty($id)) {
                            $info['date_created'] = date('Y-m-d H:i:s');
                            $info['date_updated'] = date('Y-m-d H:i:s');
                            $info['created_at'] = date('Y-m-d H:i:s');
                            $info['updated_at'] = date('Y-m-d H:i:s');
                            $name = "insert";
                            $i++;
                            $info_insert[] = $info;
                        } else {
                            $info_update = array();
                            $info['id'] = $id;
                            $info['date_updated'] = date('Y-m-d H:i:s');
                            $info['updated_at'] = date('Y-m-d H:i:s');
                            $name = "update";
                            $u++;
                            $info_update[] = $info;
                        }
                    }
                    
                    if ($info_insert) {
                        $this->Aris_model->save_aris($new_selected,$info_insert,1);
                    }
                    
                    if ($info_update) {
                        $this->Aris_model->save_aris($new_selected,$info_update,2);
                    } 
                    
                }
            }
        }
        $return['insert'] = $i;
        $return['update'] = $u;
        return $return;
    }

    public function arisx($table = "",$limit = 0,$offset = 0){
        error_reporting(E_ALL);
        if (!($table)) { echo "no action"; return false; } 
        $where = array();
        $params['limit'] = $limit;
        $params['offset'] = $offset;
        $project_files = $this->Aris_model->get_aris('project_files',$params);

        $table = str_replace('_', ' ', $table);
        if ($project_files) {
            foreach ($project_files as $key => $file) {
                $results = $this->Aris_model->get_all_project($file['filename'],$file['password'],$table);
                $project_id = $this->save_project_information($results,$table);
            }
        }
    }

    public function save_project_informationx($results = array(),$selected = ''){
        error_reporting(E_ALL);
        $project_id = 0;

        if ( !empty($results) && isset($results) ) {
            foreach ($results as $key => $result) {
                $project_info = $result['project information'];

                if ($project_info) {
                    foreach ($project_info as $key => $info) {
                        $table = "project_information";
                        $where = array('where' => array('ProjectName' => $info['ProjectName']));
                        $data = $this->Aris_model->get_aris($table,$where);
                        $project_id = @$data[0]['id'];
                        if (empty($project_id)) {
                            $data['date_created'] = date('Y-m-d H:i:s');
                            $project_id = $this->Aris_model->save_aris('project_information',$info);
                        } 
                    }
                }

                if ( !empty($result[$selected]) && isset($result[$selected]) ) {
                    $infos = $result[$selected];
                    foreach ($infos as $key => $info) {
                        $id = 0;
                        $where = array();
                        $selected = str_replace(' ', '_', $selected);
                        $info['project_id'] = $project_id;
                        $where['project_id'] = $project_id;

                        if ($selected == "lots") {
                            $where['LotID'] = $info['LotID'];
                        }
                        if ($selected == "aris_customers") {
                            $where['CUSTOMERID'] = $info['CUSTOMERID'];
                        }
                        if ($selected == "aris_sales_agent") {
                            $where['SalesAgentID'] = $info['SalesAgentID'];
                        }
                        if ($selected == "aris_documents") {
                            $where['DocumentID'] = $info['DocumentID'];
                        }
                        if ($selected == "aris_collectibles") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DateDue'] = $info['DateDue'];
                        }
                        if ($selected == "aris_payments") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DATE_DUE'] = $info['DATE_DUE'];
                            $where['PaymentID'] = $info['PaymentID'];
                        }
                        if ($selected == "aris_pastdue") {
                            $where['DocumentID'] = $info['DocumentID'];
                            $where['DateDue'] = $info['DateDue'];
                        }
                        if ($selected == "aris_ar") {
                            $where['DocumentID'] = $info['DocumentID'];
                        }
                        if ($selected == "aris_pd") {
                            $where['DocumentID'] = $info['DocumentID'];
                        }


                        $where_params['where'] = $where; 
                        $data = $this->Aris_model->get_aris($selected,$where_params);
                        $id = @$data[0]['id'];

                        if (empty($id)) {
                            $info['date_created'] = date('Y-m-d H:i:s');
                            $info['date_updated'] = date('Y-m-d H:i:s');
                            $name = "insert";
                        } else {
                            $info['id'] = $id;
                            $info['date_updated'] = date('Y-m-d H:i:s');
                            $name = "update";
                            continue;
                        }

                        $id = $this->Aris_model->save_aris($selected,$info);
                        echo $id." - ".$name;echo "<br>";
                    }
                }

            }
        }
    }
   
    public function UR_exists($url){
       $headers=get_headers($url);
       return stripos($headers[0],"200 OK")?true:false;
    }

    public function test_ur_exist($filename = ""){
        $aws_path_file = "https://rems-pdpc.s3-ap-southeast-1.amazonaws.com/ptr/".$filename;
        if($this->UR_exists($aws_path_file)){
            echo "Exist";
        } else {
            echo "Doesnt Exist";
        }
    }
    
    public function file_exists2($file) {
        $file_headers = @get_headers($file);
        
        if (empty($file_headers)) {
            $exists = false;
            $e = 1;
        } else if($file_headers[0] == 'HTTP/1.0 404 Not Found') {
            $exists = false;
            $e = 2;
        } else {
            $exists = true;
            $e = 3;
        }
        echo "<pre>";
        echo $e;
        die();

        return $exists;
    }
    // SYNCING
    

    // MIGRATION
    public function migrate_customers()
    {
        
        $clients = $this->arisdb->get('customers')->result_array();

        // [CUSTOMERID] => 92
        // [FIRSTNAME] => EDWIN
        // [MI] => D.
        // [LASTNAME] => CAJURAO
        // [SPOUSENAME] => 
        // [ADDRESS] => Brgy. Punong Lapuz, Lapaz
        // [CITY] => 
        // [POSTALCODE] => 
        // [PROVINCE] => Iloilo
        // [COUNTRY] => Philippines
        // [PHONE_NO] => 0366200715
        // [CEL_NO] => 09989736773
        // [FAX_NO] => 
        // [EMAIL_ADDR] => 
        // [REMARKS] => PDP
        // [ACTIVE] => 0
        // [ExcludeLPD] => 0

        if ($clients) {
            # code...
            $count = 0;
            foreach ($clients as $key => $client) {
                # code...

                if (empty(trim($client['FIRSTNAME'])) || empty(trim($client['LASTNAME']))) { continue; }

                $buyer_info['spouse_name'] = preg_replace('/\s+/', ' ', trim(ucwords(strtolower($client['SPOUSENAME']))));
                $buyer_info['first_name'] = preg_replace('/\s+/', ' ', trim(ucwords(strtolower($client['FIRSTNAME']))));
                $buyer_info['middle_name'] = preg_replace('/\s+/', ' ', trim(ucwords(strtolower($client['MI']))));
                $buyer_info['last_name'] = preg_replace('/\s+/', ' ', trim(ucwords(strtolower($client['LASTNAME']))));
                $buyer_info['email'] = preg_replace('/\s+/', ' ', trim(strtolower($client['EMAIL_ADDR'])));
                $buyer_info['mobile_no'] = preg_replace('/\s+/', ' ', trim($client['CEL_NO']));
                $buyer_info['other_mobile'] = preg_replace('/\s+/', ' ', trim($client['PHONE_NO']));
                $buyer_info['present_address'] = preg_replace('/\s+/', ' ', trim(ucwords($client['ADDRESS'])));
                $buyer_info['CID'] = $client['CUSTOMERID'];
                $buyer_info['PID'] = $client['project_id'];
                $buyer_info['is_exported'] = 1;
                $buyer_info['type_id'] = 1;

                if (!$client['EMAIL_ADDR']) {
                    $buyer_info['email'] = str_replace(' ','',preg_replace('/\s+/', ' ', (trim(strtolower($buyer_info['first_name'].".".$buyer_info['last_name']."@pueblodepanay.com")))));
                }

                // vdebug($buyer_info);

                if ($this->M_auth->email_check($buyer_info['email'])) {
                    continue; //skip existing data
                }

                $buyerPwd = password_format($buyer_info['last_name'], $buyer_info['first_name']);
                $buyerEmail = $buyer_info['email'];

                // Buyer Group ID
                $buyerGroupID = ['4'];


                $params['CID'] = $buyer_info['CID'];
                $params['PID'] = $buyer_info['PID'];

                $buyer = $this->M_buyer->where($params)->get();

                $buyerAccAuth = array(
                    'first_name' => $buyer_info['first_name'],
                    'last_name' => $buyer_info['last_name'],
                    'active' => 1,
                    'created_by' => $this->user->id,
                    'created_at' => NOW
                );

                $userID = $this->ion_auth->register($buyerEmail, $buyerPwd, $buyerEmail, $buyerAccAuth, $buyerGroupID);

                $additional = [
                    'is_active' => 1,
                    'user_id' => $userID,
                    'created_by' => 1,
                    'created_at' => NOW
                ];
                $buyerID = $this->M_buyer->insert($buyer_info + $additional);
                $count++;
            }

        }  

        echo $count." record(s) has been migrated.";
        
        // $transaction['buyer_id'] = "";
        // $transaction['seller_id'] = "";
        // $transaction['project_id'] = "";
        // $transaction['property_id'] = "";
        // $transaction['financing_scheme_id'] = "";
        // $transaction['expiration_date'] = "";
        // $transaction['reservation_date'] = db_date($documents['RADATE']);
        // $transaction['penalty_type'] = "";
        // $transaction['remarks'] = "";

        // $billings['transaction_id'] = "";
        // $billings['period_amount'] = "";
        // $billings['interest_rate'] = "";
        // $billings['period_term'] = "";
        // $billings['effectivity_date'] = "";
        // $billings['datetime'] = "";
        // $billings['period_id'] = "";
        // $billings['order_id'] = "";
        // $billings['remarks'] = "";
        // $billings['is_active'] = "";

        // $payments['transaction_id'] = "";
        // $payments['accounting_entry_id'] = "";
        // $payments['total_amount'] = "";
        // $payments['principal_amount'] = "";
        // $payments['interest_amount'] = "";
        // $payments['beginning_balance'] = "";
        // $payments['ending_balance'] = "";
        // $payments['period_id'] = "";
        // $payments['particulars'] = "";
        // $payments['is_paid'] = "";
        // $payments['is_complete'] = "";
        // $payments['due_date'] = "";
        // $payments['next_payment_date'] = "";
        // $payments['process'] = "";
        // $payments['deleted_reason'] = "";
    }

    public function migrate_sellers()
    {
        
        $records = $this->arisdb->get('sales_agent')->result_array();
            
            // [id] => 1
            // [project_id] => 1
            // [SalesAgentID] => 1
            // [Firstname] => SALOME
            // [Lastname] => IGNACIO
            // [Mobilephone] => 
            // [Homephone] => 
            // [Workphone] => 
            // [Address] => LEGASPI
            // [City] => ROXAS
            // [PostalCode] => 5800
            // [Province] => CAPIZ
            // [Active] => 1
            // [Tax_ex] => 2
            // [Acct_No] => 5-07588-770-8
            // [Division] => 
            // [Category] => 4
            // [date_created] => 2019-06-14 09:52:56
            // [date_updated] => 2019-06-21 11:20:05

        // vdebug($records);

        if ($records) {
            # code...
            $count = 0;
            foreach ($records as $key => $record) {
                # code...

                if (empty(trim($record['Firstname'])) || empty(trim($record['Lastname']))) { continue; }

                $info['first_name'] = preg_replace('/\s+/', ' ', trim(ucwords(strtolower($record['Firstname']))));
                $info['middle_name'] = "";
                $info['last_name'] = preg_replace('/\s+/', ' ', trim(ucwords(strtolower($record['Lastname']))));
                $info['email'] = "";
                $info['mobile_no'] = preg_replace('/\s+/', ' ', trim($record['Mobilephone']));
                $info['other_mobile'] = preg_replace('/\s+/', ' ', trim($record['Workphone']));
                $info['present_address'] = preg_replace('/\s+/', ' ', trim(ucwords($record['Address'])));
                $info['SID'] = $record['SalesAgentID'];
                $info['is_exported'] = 1;
                $info['acct_number'] = $record['Acct_No'];

                if (!$info['email']) {
                    $info['email'] = str_replace(' ','',preg_replace('/\s+/', ' ', (trim(strtolower($info['first_name'].".".$info['last_name']."@pueblodepanay.com")))));
                }

                // vdebug($info);

                if ($this->M_auth->email_check($info['email'])) {
                    continue; //skip existing data
                }

                $Pwd = password_format($info['last_name'], $info['first_name']);
                $Email = $info['email'];

                //  Group ID
                $GroupID = ['3'];

                $params['SID'] = $info['SID'];

                // $seller = $this->M_seller->where($params)->get();

                $AccAuth = array(
                    'first_name' => $info['first_name'],
                    'last_name' => $info['last_name'],
                    'active' => 1,
                    'created_by' => $this->user->id,
                    'created_at' => NOW
                );

                $userID = $this->ion_auth->register($Email, $Pwd, $Email, $AccAuth, $GroupID);

                $additional = [
                    'is_active' => 1,
                    'user_id' => $userID,
                    'created_by' => 1,
                    'created_at' => NOW
                ];
                $buyerID = $this->M_seller->insert($info + $additional);
                $count++;
            }

        }  

        echo $count." record(s) has been migrated.";
    }

    public function migrate_transactions()
    {
        
        $records = $this->arisdb->get('documents')->result_array();
        
        vdebug($records);
        
        if ($records) {
            # code...
            $count = 0;
            foreach ($records as $key => $record) {
                # code...

                if (empty(trim($record['Firstname'])) || empty(trim($record['Lastname']))) { continue; }

                $info['first_name'] = preg_replace('/\s+/', ' ', trim(ucwords(strtolower($record['Firstname']))));
                $info['middle_name'] = "";
                $info['last_name'] = preg_replace('/\s+/', ' ', trim(ucwords(strtolower($record['Lastname']))));
                $info['email'] = "";
                $info['mobile_no'] = preg_replace('/\s+/', ' ', trim($record['Mobilephone']));
                $info['other_mobile'] = preg_replace('/\s+/', ' ', trim($record['Workphone']));
                $info['present_address'] = preg_replace('/\s+/', ' ', trim(ucwords($record['Address'])));
                $info['SID'] = $record['SalesAgentID'];
                $info['is_exported'] = 1;
                $info['acct_number'] = $record['Acct_No'];

                if (!$info['email']) {
                    $info['email'] = str_replace(' ','',preg_replace('/\s+/', ' ', (trim(strtolower($info['first_name'].".".$info['last_name']."@pueblodepanay.com")))));
                }

                // vdebug($info);

                if ($this->M_auth->email_check($info['email'])) {
                    continue; //skip existing data
                }

                $Pwd = password_format($info['last_name'], $info['first_name']);
                $Email = $info['email'];

                //  Group ID
                $GroupID = ['3'];

                $params['SID'] = $info['SID'];

                // $seller = $this->M_seller->where($params)->get();

                $AccAuth = array(
                    'first_name' => $info['first_name'],
                    'last_name' => $info['last_name'],
                    'active' => 1,
                    'created_by' => $this->user->id,
                    'created_at' => NOW
                );

                $userID = $this->ion_auth->register($Email, $Pwd, $Email, $AccAuth, $GroupID);

                $additional = [
                    'is_active' => 1,
                    'user_id' => $userID,
                    'created_by' => 1,
                    'created_at' => NOW
                ];
                $buyerID = $this->M_seller->insert($info + $additional);
                $count++;
            }

        }  

        echo $count." record(s) has been migrated.";
    }
    // MIGRATION
    public function sync_all_aris(){
        for($x = 1; $x < 42; $x++){
            $this->get($x);
        }
    }
}