<?php

// vdebug($_documents);

?>

<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Transaction Loan Documents</h3>
            <?php if ( isset($_total) && $_total ): ?>

            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="_total"><?php echo $_total;?> TOTAL</span>
            <?php endif; ?>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon  kt-input-icon--right kt-subheader__search">
                <input type="text" name="search" id="_search" class="form-control form-control-sm" placeholder="Search">
                <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                            class="la la-search"></i></span></span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <?php if ( FALSE ): ?>

                <a href="javascript:void(0);" class="btn btn-label-primary btn-elevate btn-icon-sm">
                    <!-- <a href="<?php echo site_url('checklist/create_transaction_document');?>" class="btn btn-label-primary btn-elevate btn-icon-sm"> -->
                    <i class="fa fa-plus-circle"></i> Add Document Checklist
                </a>
                <button type="button" id="_batch_upload_btn" class="btn btn-label-primary btn-elevate"
                    data-toggle="collapse" data-target="#_batch_upload" aria-expanded="true"
                    aria-controls="_batch_upload">
                    <i class="fa fa-upload"></i> Import
                </button>
                <?php endif; ?>
                <?php if ( isset($_li_id) && $_li_id ): ?>

                <a href="<?php echo site_url('transaction/documents/'.$_li_id);?>"
                    class="btn btn-label-primary btn-elevate btn-icon-sm btn-sm">
                    <i class="fa fa-plus-circle"></i> Add Document Checklist
                </a>
                <?php endif; ?>
                <!-- <button type="button" id="_advance_search_btn"
                    class="btn btn-label-primary btn-elevate btn-icon-sm btn-sm" data-toggle="collapse"
                    data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button> -->
                <a href="<?php echo site_url('transaction');?>" class="btn btn-label-primary btn-elevate btn-icon-sm btn-sm">
                    <i class="fa fa-reply"></i> Cancel
                </a>
            </div>
        </div>
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
                <!-- <a href="<?php echo site_url('transaction_loan_document_stages/create'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Create
                </a>              -->
        </div>

        <div class="module__filter">
                <button class="btn btn-label-primary btn-elevate btn-sm btn-filter" id="_advance_search_btn"
                    data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" >
                    <i class="fa fa-filter"></i> Filter
                </button>
        </div>
    </div>
</div>


<div class="kt-portlet kt-portlet--mobile">

    <div class="kt-portlet__body">
        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label class="form-control-label">Transaction Reference</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="td_owner_transaction_id"
                                        class="form-control form-control-sm _filter" placeholder="Transaction ID"
                                        id="_column_2" data-column="2">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-book"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form-control-label">Document Name</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <input type="text" name="td_owner_document_id"
                                        class="form-control form-control-sm _filter" placeholder="Document ID"
                                        id="_column_3" data-column="3">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-book"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form-control-label">Owner</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php
										$_document_owner	=	Dropdown::get_static('document_owner');
										$_owners	=	[];
										if ( isset($_document_owner) && $_document_owner ) {

											foreach ( $_document_owner as $_okey => $_do ) {

												$_owners[($_okey === '') ? '' : $_do]	=	$_do;
											}
										}
									?>
                                    <?php echo form_dropdown('owner_id', $_owners, '', 'class="form-control form-control-sm _filter" id="_column_5" data-column="5"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-certificate"></i></span></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form-control-label">Category</label>
                                <div class="kt-input-icon  kt-input-icon--left">
                                    <?php
										$checklist_category	=	Dropdown::get_static('document_checklist_category');
										$_owners	=	[];
										if ( isset($checklist_category) && $checklist_category ) {

											foreach ( $checklist_category as $_okey => $_do ) {

												$_owners[($_okey === '') ? '' : $_do]	=	$_do;
											}
										}
									?>
                                    <?php echo form_dropdown('owner_id', $_owners, '', 'class="form-control form-control-sm _filter" id="_column_6" data-column="6"'); ?>
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                                class="la la-certificate"></i></span></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-condensed table-checkable"
            id="_transaction_loan_document_table">
            <thead>
                <tr class="text-center">
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Transaction Ref</th>
                    <th>Document Name</th>
                    <th>Owner</th>
                    <th>Category</th>
                    <th>Checklist</th>
                    <th>File</th>
                    <th>Timeline</th>
                    <th>Start Date</th>
                    <th>Due Date</th>
                    <th>Status</th>
                    <th>Has File</th>
                    <th>Document Loan Stage</th>
                    <th>Remarks</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_export_option_label">Export Options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4 offset-lg-1">
                        <div class="kt-checkbox-list">
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Field
                                <span></span>
                            </label>
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Name
                                <span></span>
                            </label>
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Description
                                <span></span>
                            </label>
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Owner
                                <span></span>
                            </label>
                            <label class="kt-checkbox kt-checkbox--bold">
                                <input type="checkbox" name=""> Classification
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-elevate btn-outline-hover-brand btn-sm">
                    <i class="fa fa-file-export"></i> Export
                </button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->

<!-- Start Upload Modal-->
<div class="modal fade" id="_upload_modal" tabindex="-1" role="dialog" aria-labelledby="_upload_modal_label"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_upload_modal_label">Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" method="POST" id="_upload_form" enctype="multipart/form-data">
                    <div class="form-group row text-center">
                        <div class="col-lg-12 col-xl-12">
                            <div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar">
                                <div class="kt-avatar__holder"
                                    style="background-image: url(<?php echo base_url('assets/img/default/_img.png');?>);">
                                </div>
                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                    data-original-title="Change avatar">
                                    <i class="fa fa-pen"></i>
                                    <!-- <input type="file" id="process_checklist_file" name="process_image" accept=".png, .jpg, .jpeg"> -->
                                    <input type="file" class="process_checklist_file" id="process_checklist_file"
                                        name="_file" accept=".png, .jpg, .jpeg">
                                </label>
                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                    data-original-title="Cancel avatar">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left"
                        data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right"
                        form="_upload_form">
                        <i class="fa fa-plus-circle"></i> Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Upload Modal-->

<!-- Start Upload Modal-->
<div class="modal fade" id="_view_file_modal" tabindex="-1" role="dialog" aria-labelledby="_view_file_modal_label"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="_view_file_modal_label">View</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="_display_process_document_image"></div>
                <!-- <div class="form-group row text-center">
					<div class="col-lg-12 col-xl-12">
						<div class="kt-avatar kt-avatar--outline kt-avatar--circle-">
							<div class="kt-avatar__holder" style="background-image: url(<?php echo base_url('assets/img/default/_img.png');?>);"></div>
						</div>
					</div>
				</div> -->
            </div>
            <div class="modal-footer">
                <div class="kt-form__actions btn-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-elevate btn-font-sm pull-left"
                        data-dismiss="modal">
                        <i class="fa fa-times"></i> Close
                    </button>
                    <!-- <button type="submit" class="btn btn-primary btn-sm btn- btn-font-sm pull-right" form="_upload_form">
						<i class="fa fa-plus-circle"></i> Submit
					</button> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Upload Modal-->

<script type="text/javascript">
    var _li_id = '<?php echo isset($_li_id) && $_li_id ? $_li_id : ''; ?>';
</script>