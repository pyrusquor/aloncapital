<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_entry_items extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('accounting_entry_items/Accounting_entry_items_model', 'M_Accounting_entry_items');
    }

}
