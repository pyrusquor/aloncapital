<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_entry_items_model extends MY_Model
{
    public $table = 'accounting_entry_items'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'accounting_entry_id',
        'ledger_id',
        'amount',
        'dc',
        'payee_type',
        'payee_type_id',
        'is_reconciled',
        'description',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['payee'] = array('foreign_model' => 'user/User_model', 'foreign_table' => 'users', 'foreign_key' => 'id', 'local_key' => 'payee_type_id');
        $this->has_one['accounting_entry'] = array('foreign_model' => 'accounting_entries/accounting_entries_model', 'foreign_table' => 'accounting_entries', 'foreign_key' => 'id', 'local_key' => 'accounting_entry_id');
        $this->has_one['accounting_ledger'] = array('foreign_model' => 'accounting_ledgers/accounting_ledgers_model', 'foreign_table' => 'accounting_ledgers', 'foreign_key' => 'id', 'local_key' => 'ledger_id');
    }

}
