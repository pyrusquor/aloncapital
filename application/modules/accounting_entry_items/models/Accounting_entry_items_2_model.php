<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_entry_items_2_model extends MY_Model
{
    public $table = 'accounting_entry_items_2'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'accounting_entry_id',
        'ledger_id',
        'amount',
        'dc',
        'payee_type',
        'payee_type_id',
        'is_reconciled',
        'description',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        // $this->has_many['table_name'] = array();
    }

}
