<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_settings extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('accounting_settings/Accounting_settings_model', 'M_Accounting_settings');
        $this->load->model('accounting_setting_items/Accounting_setting_items_model', 'M_Accounting_setting_items');

        $this->_table_fillables = $this->M_Accounting_settings->fillable;
        $this->_table_columns = $this->M_Accounting_settings->__get_columns();

        $this->u_additional = [
            'updated_by' => $this->user->id,
            'updated_at' => NOW,
        ];

        $this->additional = [
            'created_by' => $this->user->id,
            'created_at' => NOW,
        ];
    }

    public function index()
    {
        $_fills = $this->_table_fillables;
        $_colms = $this->_table_columns;

        $this->view_data['_fillables'] = $this->__get_fillables($_colms, $_fills);
        $this->view_data['_columns'] = $this->__get_columns($_fills);

        $db_columns = $this->M_Accounting_settings->get_columns();
        if ($db_columns) {
            $column = [];
            foreach ($db_columns as $key => $dbclm) {
                $name = isset($dbclmn) && $dbclmn ? $dbclmn : '';

                if ((strpos($name, 'created_') === false) && (strpos($name, 'updated_') === false) && (strpos($name, 'deleted_') === false) && ($name !== 'id')) {
                    if (strpos($name, '_id') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('_id', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } elseif (strpos($name, 'is_') !== false) {
                        $column = $name;
                        $column[$key]['value'] = $column;
                        $name = isset($name) && $name ? str_replace('is_', '', $name) : '';
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    } else {
                        $column[$key]['value'] = $name;
                        $_title = isset($name) && $name ? str_replace('_', ' ', $name) : '';
                        $column[$key]['label'] = ucwords(strtolower($_title));
                    }
                } else {
                    continue;
                }
            }

            $column_count = count($column);
            $cceil = ceil(($column_count / 2));

            $this->view_data['columns'] = array_chunk($column, $cceil);
            $column_group = $this->M_Accounting_settings->count_rows();
            if ($column_group) {
                $this->view_data['total'] = $column_group;
            }
        }

        $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
        $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

        $this->template->build('index', $this->view_data);
    }

    public function showItems()
    {
        $columnsDefault = [
            'id' => true,
            'category_type' => true,
            'period_id' => true,
            'module_types_id' => true,
            'entry_types_id' => true,
            'project_id' => true,
            'company_id' => true,
        ];

        $draw = $_REQUEST['draw'];
        $start = $_REQUEST['start'];
        $rowperpage = $_REQUEST['length'];
        $columnIndex = $_REQUEST['order'][0]['column'];
        $columnName = $_REQUEST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_REQUEST['order'][0]['dir'];
        $searchValue = $_REQUEST['search']['value'] ?? null;

        $filters = [];
        parse_str($_POST['filter'], $filters);

        $items = [];
        $totalRecordwithFilter = 0;
        $filteredItems = [];

        for ($query_loop = 0; $query_loop < 2; $query_loop++) {

            $query = $this->M_Accounting_settings
                ->with_company('fields:name')
                ->with_project('fields:name')
                ->order_by($columnName, $columnSortOrder)
                ->as_array();

            // General Search
            if ($searchValue) {

                $query->or_where("(id like '%$searchValue%')");
            }

            $advanceSearchValues = [
                'category_type' => [
                    'data' => $filters['category_type'] ?? null,
                    'operator' => '=',
                ],
                'period_id' => [
                    'data' => $filters['period_id'] ?? null,
                    'operator' => '=',
                ],
                'module_types_id' => [
                    'data' => $filters['module_types_id'] ?? null,
                    'operator' => '=',
                ],
                'entry_types_id' => [
                    'data' => $filters['entry_types_id'] ?? null,
                    'operator' => '=',
                ],
                'company_id' => [
                    'data' => $filters['company_id'] ?? null,
                    'operator' => '=',
                ],
                'project_id' => [
                    'data' => $filters['project_id'] ?? null,
                    'operator' => '=',
                ],
            ];

            // Advance Search
            foreach ($advanceSearchValues as $key => $value) {

                if ($value['data']) {

                    if ($key == 'date_range_start' || $key == 'date_range_end') {

                        $query->where($value['column'], $value['operator'], $value['data']);
                    } else {

                        $query->where($key, $value['operator'], $value['data']);
                    }
                }
            }

            if ($query_loop) {

                $totalRecordwithFilter = $query->count_rows();
            } else {

                $query->limit($rowperpage, $start);
                $items = $query->get_all();

                if (!!$items) {

                    // Transform data
                    foreach ($items as $key => $value) {

                        $items[$key]['project_id'] = $value['project']['name'] ?? '';
                        $items[$key]['company_id'] = $value['company']['name'] ?? '';
                        $items[$key]['category_type'] = Dropdown::get_static('accounting_setting_types', $value['category_type'], 'view') ?? '';
                        $items[$key]['period_id'] = Dropdown::get_static('period_names', $value['period_id']) ?? '';
                        $items[$key]['module_types_id'] = Dropdown::get_static('accounting_setting_module_types', $value['module_types_id']) ?? '';
                        $items[$key]['entry_types_id'] = Dropdown::get_static('accounting_setting_entry_types', $value['entry_types_id']) ?? '';
                    }

                    foreach ($items as $item) {
                        $filteredItems[] = $this->filterArray(json_decode(json_encode($item), true), $columnsDefault);
                    }
                }
            }
        }

        $totalRecords = $this->M_Accounting_settings->count_rows();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "data" => $filteredItems,
            "request" => $_REQUEST,
        );

        echo json_encode($response);
        exit();
    }

    public function form($id = false)
    {

        $method = "Create";
        if ($id) {
            $method = "Update";
        }

        if ($this->input->post()) {

            // $response['status'] = 0;
            // $response['msg'] = 'Oops! Please refresh the page and try again.';

            $this->form_validation->set_rules($this->M_Accounting_settings->fields);

            if ($this->form_validation->run() === true) {
                $info = $this->input->post();

                $accounting_setting['category_type'] = $info['category_type'];
                $accounting_setting['period_id'] = $info['period_id'];
                $accounting_setting['module_types_id'] = $info['module_types_id'];
                $accounting_setting['entry_types_id'] = $info['entry_types_id'];
                $accounting_setting['company_id'] = $info['company_id'];
                $accounting_setting['project_id'] = $info['project_id'];
                $accounting_setting['for_recognize'] = $info['for_recognize'];
                $setting_items = $info['setting_item'];

                $this->db->trans_begin();

                if ($id) {
                    $additional = [
                        'updated_by' => $this->user->id,
                        'updated_at' => NOW,
                    ];

                    $items = $this->M_Accounting_setting_items->get_all(["accounting_settings_id" => $id]);

                    $permission_status = $this->M_Accounting_settings->update($accounting_setting + $additional, $id);

                    foreach ($setting_items as $key => $item) {

                        if (!!$item['id']) {

                            $setting_item['accounting_entry_type'] = $item['accounting_entry_type'];
                            $setting_item['accounting_entry'] = $item['accounting_entry'];
                            $setting_item['origin_id'] = @$item['origin_id'];

                            $this->M_Accounting_setting_items->update($setting_item + $this->additional, $item['id']);
                        } else {

                            $additional = [
                                'created_by' => $this->user->id,
                                'created_at' => NOW,
                            ];

                            $setting_item['accounting_settings_id'] = $id;
                            $setting_item['accounting_entry_type'] = $item['accounting_entry_type'];
                            $setting_item['accounting_entry'] = $item['accounting_entry'];
                            $setting_item['origin_id'] = @$item['origin_id'];

                            $this->M_Accounting_setting_items->insert($setting_item + $this->additional);
                        }
                    }

                    $item_ids = [];

                    foreach ($items as $item) {
                        array_push($item_ids, $item['id']);
                    }

                    $post_item_ids = [];

                    foreach ($setting_items as $item) {
                        if (!!$item['id']) {
                            array_push($post_item_ids, $item['id']);
                        }
                    }

                    $delete_ids = array_diff($item_ids, $post_item_ids);

                    foreach ($delete_ids as $id) {
                        $this->M_Accounting_setting_items->delete($id);
                    }
                } else {
                    $additional = [
                        'created_by' => $this->user->id,
                        'created_at' => NOW,
                    ];

                    $permission_status = $this->M_Accounting_settings->insert($accounting_setting + $additional);

                    foreach ($setting_items as $key => $item) {
                        $setting_item['accounting_settings_id'] = $permission_status;
                        $setting_item['accounting_entry_type'] = $item['accounting_entry_type'];
                        $setting_item['accounting_entry'] = $item['accounting_entry'];
                        $setting_item['origin_id'] = $item['origin_id'];

                        $this->M_Accounting_setting_items->insert($setting_item + $this->additional);
                    }
                }

                if ($this->db->trans_status() === FALSE) {

                    $this->db->trans_rollback();

                    $response['status'] = 0;
                    $response['message'] = 'Please refresh the page!';
                } else {

                    $this->db->trans_commit();

                    $response['status'] = 1;
                    $response['message'] = 'Accounting Setting Successfully ' . $method . 'd!';
                }
            } else {
                $response['status'] = 0;
                $response['message'] = validation_errors();
            }

            echo json_encode($response);
            exit();
        }

        if ($id) {
            $this->view_data['info'] = $info = $this->M_Accounting_settings->with_accounting_setting_item()->with_company()->with_project()->get($id);

            $this->view_data['setting_items'] = $info['accounting_setting_item'];
        }

        $this->view_data['method'] = $method;

        $this->template->build('form', $this->view_data);
    }

    public function delete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        $id = $this->input->post('id');
        if ($id) {

            $list = $this->M_Accounting_settings->get($id);
            $to_delete = ["accounting_settings_id" => $id];
            $setting_items = $this->M_Accounting_setting_items->get_all();

            if ($list) {

                $deleted = $this->M_Accounting_settings->delete($list['id']);

                if ($deleted !== false) {

                    foreach ($setting_items as $key => $value) {
                        $this->M_Accounting_setting_items->delete($to_delete);
                    }

                    $response['status'] = 1;
                    $response['message'] = 'Accounting Settings Successfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function bulkDelete()
    {
        $response['status'] = 0;
        $response['message'] = 'Oops! Please refresh the page and try again.';

        if ($this->input->is_ajax_request()) {
            $delete_ids = $this->input->post('deleteids_arr');

            if ($delete_ids) {
                foreach ($delete_ids as $value) {

                    $deleted = $this->M_Accounting_settings->delete($value);
                }
                if ($deleted !== false) {

                    $response['status'] = 1;
                    $response['message'] = 'Accounting Settings Sucessfully Deleted';
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    public function view($id = false)
    {
        if ($id) {
            $this->css_loader->queue(['vendors/custom/datatables/datatables.bundle.css']);
            $this->js_loader->queue(['vendors/custom/datatables/datatables.bundle.js']);

            $this->view_data['accounting_settings'] =  $this->M_Accounting_settings->with_company()->with_project()->with_accounting_setting_item()->get($id);

            $accounting_settings_id = ["accounting_settings_id" => $id];
            $this->view_data['setting_items'] = $this->M_Accounting_setting_items->get_all($accounting_settings_id);

            if ($this->view_data['accounting_settings']) {

                $this->template->build('view', $this->view_data);
            } else {

                show_404();
            }
        } else {
            show_404();
        }
    }
}
