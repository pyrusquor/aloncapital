<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounting_settings_model extends MY_Model
{
    public $table = 'accounting_settings'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'category_type',
        'period_id',
        'module_types_id',
        'entry_types_id',
        'project_id',
        'company_id',
        'for_recognize',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        'category_type' => array(
            'field' => 'category_type',
            'label' => 'Category Type',
            'rules' => 'trim|required',
        ),
        'period_id' => array(
            'field' => 'period_id',
            'label' => 'Period',
            'rules' => 'trim|required',
        ),
        'module_types_id' => array(
            'field' => 'module_types_id',
            'label' => 'Module Type',
            'rules' => 'trim|required',
        ),
        'entry_types_id' => array(
            'field' => 'entry_types_id',
            'label' => 'Entry Type',
            'rules' => 'trim|required',
        ),
        'project_id' => array(
            'field' => 'project_id',
            'label' => 'Project Name',
            'rules' => 'trim',
        ),
        'company_id' => array(
            'field' => 'company_id',
            'label' => 'Company Name',
            'rules' => 'trim',
        ),
        'for_recognize' => array(
            'field' => 'for_recognize',
            'label' => 'For Recognition',
            'rules' => 'trim|required',
        ),
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // $this->has_many['table_name'] = array();
        $this->has_many['accounting_setting_item'] = array('foreign_model' => 'accounting_setting_items/Accounting_setting_items_model', 'foreign_table' => 'accounting_setting_items', 'foreign_key' => 'accounting_settings_id', 'local_key' => 'id');

        $this->has_one['company'] = array('foreign_model' => 'company/company_model', 'foreign_table' => 'companies', 'foreign_key' => 'id', 'local_key' => 'company_id');

        $this->has_one['project'] = array('foreign_model' => 'project/project_model', 'foreign_table' => 'projects', 'foreign_key' => 'id', 'local_key' => 'project_id');
    }

    public function get_columns()
    {
        $_return = false;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }
}
