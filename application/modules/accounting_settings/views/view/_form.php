<?php

$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$category_type = isset($info['category_type']) && $info['category_type'] ? $info['category_type'] : '';
$period_id = isset($info['period_id']) && $info['period_id'] ? $info['period_id'] : '';
$accounting_entry_type = isset($info['accounting_entry_type']) && $info['accounting_entry_type'] ? $info['accounting_entry_type'] : '';
$accounting_entry = isset($info['accounting_entry']) && $info['accounting_entry'] ? $info['accounting_entry'] : '';
$company_id = isset($info['company_id']) && $info['company_id'] ? $info['company_id'] : '';
$project_id = isset($info['project_id']) && $info['project_id'] ? $info['project_id'] : '';
$entry_types_id = isset($info['entry_types_id']) && $info['entry_types_id'] ? $info['entry_types_id'] : '';
$module_types_id = isset($info['module_types_id']) && $info['module_types_id'] ? $info['module_types_id'] : '';
$for_recognize = isset($info['for_recognize']) && $info['for_recognize'] ? $info['for_recognize'] : '';

$company_name = isset($info['company']['name']) && $info['company']['name'] ? $info['company']['name'] : '';

$project_name = isset($info['project']['name']) && $info['project']['name'] ? $info['project']['name'] : '';

// vdebug($info);

?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Company <span class="kt-font-danger">*</span></label>
            <div class="kt-input-icon kt-input-icon--left">
                <select class="form-control suggests" data-module="companies" id="company_id" name="company_id">
                    <option value="">Select Company </option>
                    <?php if ($company_name) : ?>
                        <option value="<?php echo @$company_id; ?>" selected>
                            <?php echo $company_name; ?>
                        </option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('company_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Project</label>
            <div class="kt-input-icon kt-input-icon--left">
                <select class="form-control suggests" data-module="projects" id="project_id" name="project_id">
                    <option value="">Select Project</option>
                    <?php if ($project_name) : ?>
                        <option value="<?php echo @$project_id; ?>" selected>
                            <?php echo $project_name; ?>
                        </option>
                    <?php endif ?>
                </select>
            </div>
            <?php echo form_error('project_id'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">
                Category Type
                <span class="kt-font-danger">*</span>
            </label>
            <?php echo form_dropdown('category_type', Dropdown::get_static('accounting_setting_types'), set_value('category_type', @$category_type), 'class="form-control" id="category_type" name="category_type"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">
                Period
                <span class="kt-font-danger">*</span>
            </label>
            <?php echo form_dropdown('period_id', Dropdown::get_static('period_names'), set_value('period_id', @$period_id), 'class="form-control" id="period_id" name="period_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">
                For Recognition
                <span class="kt-font-danger">*</span>
            </label>
            <?php echo form_dropdown('for_recognize', Dropdown::get_static('is_recognized'), set_value('for_recognize', @$for_recognize), 'class="form-control" id="for_recognize" name="for_recognize"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">
                Entry Types
                <span class="kt-font-danger">*</span>
            </label>
            <?php echo form_dropdown('entry_types_id', Dropdown::get_static('accounting_setting_entry_types'), set_value('entry_types_id', @$entry_types_id), 'class="form-control" id="entry_types_id" name="entry_types_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">
                Module Types
                <span class="kt-font-danger">*</span>
            </label>
            <?php echo form_dropdown('module_types_id', Dropdown::get_static('accounting_setting_module_types'), set_value('module_types_id', @$module_types_id), 'class="form-control" id="module_types_id" name="module_types_id"'); ?>
            <span class="form-text text-muted"></span>
        </div>
    </div>
</div>

<?php if (!empty($setting_items)) : //vdebug($setting_items); 
?>
    <div id="accounting_setting_items_form_repeater">
        <div data-repeater-list="setting_item">
            <?php foreach ($setting_items as $key =>
                $setting_item) : ?>
                <?php
                $setting_id = isset($setting_item['id']) && $setting_item['id'] ? $setting_item['id'] : '';

                $accounting_entry_type = isset($setting_item['accounting_entry_type']) && $setting_item['accounting_entry_type'] ? $setting_item['accounting_entry_type'] : '';

                $accounting_entry = isset($setting_item['accounting_entry']) && $setting_item['accounting_entry'] ? $setting_item['accounting_entry'] : '';

                $origin_id = isset($setting_item['origin_id']) && $setting_item['origin_id'] ? $setting_item['origin_id'] : '';

                $ledger = get_person($accounting_entry, 'accounting_ledgers');
                $ledger_name = get_name($ledger);
                ?>
                <div data-repeater-item="setting_item" class="row">
                    <div class="col-sm-4 d-none">
                        <div class="form-group">
                            <label>
                                ID
                                <span class="kt-font-danger">*</span>
                            </label>
                            <div class="kt-input-icon">
                                <input class="form-control" name="setting_item[<?php echo $key; ?>][id]" value="<?php echo set_value('id', $setting_id); ?>" placeholder="Type" autocomplete="off" />
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="">
                                Accounting Entry Type
                                <span class="kt-font-danger">*</span>
                            </label>
                            <select class="form-control" name="setting_item[<?php echo $key; ?>][accounting_entry_type]" value="<?php echo set_value('accounting_entry_type', @$accounting_entry_type); ?>" placeholder="Type" autocomplete="off" id="setting_item_accounting_entry_type" data-type="<?php echo @$accounting_entry_type; ?>">
                                <option value="">Select option</option>
                                <option value="d" <?php if ($accounting_entry_type == "d") : ?> selected <?php endif; ?>>d
                                </option>
                                <option value="c" <?php if ($accounting_entry_type == "c") : ?> selected <?php endif; ?>>c
                                </option>
                            </select>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="">
                                Accounting Entry
                                <span class="kt-font-danger">*</span>
                            </label>
                            <select class="form-control" data-module="accounting_ledgers" id="accounting_entry" name="setting_item[<?php echo $key; ?>][accounting_entry]">
                                <option value="">
                                    Select Accounting Ledger
                                </option>
                                <?php if ($ledger_name) : ?>
                                    <option value="<?php echo @$accounting_entry; ?>" selected>
                                        <?php echo $ledger_name; ?>
                                    </option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="">
                                        Origin
                                        <span class="kt-font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('setting_item[' . $key . '][origin_id]', Dropdown::get_static('accounting_setting_origin'), set_value('origin_id', @$origin_id), 'class="form-control" id="origin_id"'); ?>
                                </div>
                                <div class="col-sm-6">
                                    <label class="d-block">&nbsp;</label>
                                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                        <i class="la la-trash-o"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="form-group form-group-last row">
            <div class="offset-10"></div>
            <div class="col-lg-2">
                <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                    <i class="la la-plus"></i>
                    Add Entry Item
                </a>
            </div>
        </div>
    </div>
<?php else : ?>
    <div id="accounting_setting_items_form_repeater">
        <div data-repeater-list="setting_item">
            <div data-repeater-item="setting_item" class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="">
                            Accounting Entry Type
                            <span class="kt-font-danger">*</span>
                        </label>

                        <select class="form-control" name="setting_item[accounting_entry_type]" value="<?php echo set_value('accounting_entry_type', $accounting_entry_type); ?>" placeholder="Type" autocomplete="off" id="setting_item_accounting_entry_type" data-type="<?php echo $accounting_entry_type; ?>">
                            <option value="">Select option</option>
                            <option value="d">d</option>
                            <option value="c">c</option>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="">
                            Accounting Entry
                            <span class="kt-font-danger">*</span>
                        </label>

                        <select class="form-control" data-module="accounting_ledgers" id="accounting_entry" name="setting_item[accounting_entry]">
                            <option value="">Select Accounting Ledger</option>
                            <?php if ($accounting_entry_name) : ?>
                                <option value="<?php echo $accounting_entry; ?>" selected>
                                    <?php echo $accounting_entry_name; ?>
                                </option>
                            <?php endif; ?>
                        </select>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="">
                                    Origin
                                    <span class="kt-font-danger">*</span>
                                </label>

                                <?php echo form_dropdown('origin_id', Dropdown::get_static('accounting_setting_origin'), set_value('origin_id', @$origin_id), 'class="form-control" id="origin_id" name="origin_id"'); ?>
                            </div>
                            <div class="col-sm-6">
                                <label class="d-block">&nbsp;</label>
                                <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                    <i class="la la-trash-o"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-group-last row">
            <div class="offset-10"></div>
            <div class="col-lg-2">
                <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                    <i class="la la-plus"></i>
                    Add Entry Item
                </a>
            </div>
        </div>
    </div>
<?php endif ?>