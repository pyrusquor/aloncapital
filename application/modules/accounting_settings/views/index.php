<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Accounting Settings</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc" id="total"></span>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input type="text" class="form-control" placeholder="Search Accounting Settings..." id="generalSearch">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="flaticon2-search-1"></i></span>
                </span>
            </div>
        </div>
        <!-- <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('accounting_settings/form'); ?>"
                    class="btn btn-label-primary btn-elevate btn-sm">
                    <i class="fa fa-plus"></i> Create
                </a>
                <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm"
                    data-toggle="collapse" data-target="#_advance_search" aria-expanded="true"
                    aria-controls="_advance_search">
                    <i class="fa fa-filter"></i> Filter
                </button>
            </div>
        </div> -->
    </div>
</div>

<div class="module__cta">
    <div class="kt-container  kt-container--fluid ">
        <div class="module__create">
            <a href="<?php echo site_url('accounting_settings/form'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
                <i class="fa fa-plus"></i> Add Accounting Settings
            </a>
        </div>

        <div class="module__filter">
            <button type="button" id="_advance_search_btn" class="btn btn-label-primary btn-elevate btn-sm btn-filter" data-toggle="collapse" data-target="#_advance_search" aria-expanded="true" aria-controls="_advance_search">
                <i class="fa fa-filter"></i> Filter
            </button>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!--begin: Advance Search -->
        <div id="_advance_search" class="collapse kt-margin-b-35 kt-margin-t-10">
            <div class="row">
                <div class="col-lg-12">
                    <form class="kt-form" id="advance_search">
                        <div class="form-group row">
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Category Type</label>
                                <select name="category_type" class="form-control _filter">
                                    <?php foreach (Dropdown::get_static('accounting_setting_types') as $key => $value) : ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Period</label>
                                <select name="period_id" class="form-control _filter">
                                    <?php foreach (Dropdown::get_static('period_names') as $key => $value) : ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Module Type</label>
                                <select name="module_types_id" class="form-control _filter">
                                    <option value="">Select Options</option>
                                    <?php foreach (Dropdown::get_static('accounting_setting_module_types') as $key => $value) : ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Entry Type</label>
                                <select name="entry_types_id" class="form-control _filter">
                                    <option value="">Select Options</option>
                                    <?php foreach (Dropdown::get_static('accounting_setting_entry_types') as $key => $value) : ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Company</label>
                                <select name="company_id" class="form-control suggests _filter" data-module="companies">
                                </select>
                            </div>
                            <div class="col-sm-3 mb-3">
                                <label class="form-control-label">Project</label>
                                <select name="project_id" class="form-control suggests _filter" data-module="projects">
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end: Advance Search -->


        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="accounting_settings_table">
            <thead>
                <tr>
                    <th width="1%">
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="all" class="m-checkable" id="select-all">
                            <span></span>
                        </label>
                    </th>
                    <th>ID</th>
                    <th>Category Type</th>
                    <th>Period</th>
                    <th>Module Type</th>
                    <th>Entry Type</th>
                    <th>Company Name</th>
                    <th>Project Name</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        <!--end: Datatable -->

    </div>
</div>

<!--begin::Modal-->
<!--begin: Export Modal-->
<!-- <div class="modal fade show" id="_export_option" tabindex="-1" role="dialog" aria-labelledby="_export_option_label" aria-hidden="true" style="padding-right: 15px; display: block;"> -->