<?php
$id = isset($accounting_settings['id']) && $accounting_settings['id'] ? $accounting_settings['id'] : '';
$category_type = isset($accounting_settings['category_type']) && $accounting_settings['category_type'] ? $accounting_settings['category_type'] : '';
$period_id = isset($accounting_settings['period_id']) && $accounting_settings['period_id'] ? $accounting_settings['period_id'] : '';
$accounting_entry_type = isset($accounting_settings['accounting_entry_type']) && $accounting_settings['accounting_entry_type'] ? $accounting_settings['accounting_entry_type'] : '';
$accounting_entry = isset($accounting_settings['accounting_entry']) && $accounting_settings['accounting_entry'] ? $accounting_settings['accounting_entry'] : '';
$company_id = isset($accounting_settings['company_id']) && $accounting_settings['company_id'] ? $accounting_settings['company_id'] : '';
$project_id = isset($accounting_settings['project_id']) && $accounting_settings['project_id'] ? $accounting_settings['project_id'] : '';
$entry_types_id = isset($accounting_settings['entry_types_id']) && $accounting_settings['entry_types_id'] ? $accounting_settings['entry_types_id'] : '';
$module_types_id = isset($accounting_settings['module_types_id']) && $accounting_settings['module_types_id'] ? $accounting_settings['module_types_id'] : '';

$company_name = isset($accounting_settings['company']['name']) && $accounting_settings['company']['name'] ? $accounting_settings['company']['name'] : 'N/A';

$project_name = isset($accounting_settings['project']['name']) && $accounting_settings['project']['name'] ? $accounting_settings['project']['name'] : 'N/A';

$for_recognize = isset($accounting_settings['for_recognize']) && $accounting_settings['for_recognize'] ? "After Recognition" : 'Before Recognition';

?>
<!-- CONTENT HEADER -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">View Accounting Settings</h3>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="<?php echo site_url('accounting_settings/form/'.$id);?>"
                    class="btn btn-label-success btn-elevate btn-sm">
                    <i class="fa fa-edit"></i> Edit
                </a>
                <a href="<?php echo site_url('accounting_settings');?>"
                    class="btn btn-label-instagram btn-sm btn-elevate">
                    <i class="fa fa-reply"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    General Information
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin::Form-->
                            <div class="kt-widget13">
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Category Type
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('accounting_setting_types', $category_type, 'view'); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Period
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('period_names', $period_id, 'view'); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Module Type
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('accounting_setting_module_types', $module_types_id, 'view'); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Entry Type
                                    </span>
                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('accounting_setting_entry_types', $entry_types_id, 'view'); ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Company Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $company_name ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        Project Name
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $project_name ?></span>
                                </div>
                                <div class="kt-widget13__item">
                                    <span class="kt-widget13__desc">
                                        For Recognition
                                    </span>
                                    <span class="kt-widget13__text kt-widget13__text--bold">
                                            <?php echo $for_recognize ?></span>
                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>

                <div class="kt-portlet">
                    <div class="kt-portlet__body">

                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        Setting Items
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <?php if ($setting_items): //vdebug($setting_items); ?>
                                <?php foreach ($setting_items as $key =>  $item): ?>
                                <?php
                                    $accounting_entry_type = isset($item['accounting_entry_type']) && $item['accounting_entry_type'] ? $item['accounting_entry_type'] : "";
                                    $origin_id = isset($item['origin_id']) && $item['origin_id'] ? $item['origin_id'] : "";
                                    $accounting_entry_id = isset($item['accounting_entry']) && $item['accounting_entry'] ? $item['accounting_entry'] : "N/A";
                                    $amount = isset($item['amount']) && $item['amount'] ? $item['amount'] : "0";
                                    $description = isset($item['description']) && $item['description'] ? $item['description'] : "N/A";
                                    $setting_item_ref_id = get_person($accounting_entry_id, 'accounting_ledgers');
                                    
                                    $setting_item_name = get_name($setting_item_ref_id) ? get_name($setting_item_ref_id) : "N/A";
                                ?>
                                <div class="form-group form-group-xs row border-bottom pb-3 mb-3">
                                    <div class="col-sm-4">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Type
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                        <?php echo $accounting_entry_type == "d" ? "Debit" : "Credit"; ?>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Ledger
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                        <?php echo $setting_item_name; ?>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div href="#" class="kt-notification-v2__item">
                                            <div class="kt-notification-v2__itek-wrapper">
                                                <div class="kt-notification-v2__item-title">
                                                    <h6 class="kt-portlet__head-title kt-font-primary">
                                                        Origin
                                                    </h6>
                                                </div>
                                                <div class="kt-notification-v2__item-desc">
                                                    <h6 class="kt-portlet__head-title kt-font-dark">
                                                    <span
                                        class="kt-widget13__text kt-widget13__text--bold"><?php echo Dropdown::get_static('accounting_setting_origin', $origin_id, 'view'); ?></span>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach;?>
                                <?php else: ?>
                                <h4 class="text-center font-italic">No setting items found</h4>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->

            </div>
        </div>
    </div>
    <!-- begin:: Footer -->
</div>