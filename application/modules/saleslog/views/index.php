<!-- CONTENT HEADER -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Seller
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <!-- <span class="kt-subheader__desc"><?php echo (!empty($records)) ? count($records) : 0; ?> TOTAL</span> -->
                <form class="kt-margin-l-20" id="kt_subheader_search_form">
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero"></path>
                                    </g>
                                </svg>

                                <!--<i class="flaticon2-search-1"></i>-->
                            </span>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <a href="<?php echo site_url('seller/create'); ?>" class="btn btn-label-primary btn-elevate btn-sm">
				<i class="fa fa-plus"></i> Add Seller
            </a>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!--begin:: Portlet-->
    <div class="kt-portlet ">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-font-light kt-hidden-">
                        JDC
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <span class="kt-widget__username">Juan Dela Cruz</span>
                        </div>

                        <div class="kt-widget__desc">
                            Division Manager (DM)
                        </div>
                    </div>
                    <div class="kt-widget__stats kt-margin-t-20">
                        <div class="kt-widget__icon">
                            <i class="flaticon-piggy-bank"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title kt-font-bold">Clients</span>
                            <span class="kt-widget__value">249</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Sales Reports
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_widget11_tab1_content" role="tab" aria-selected="true">
                                        Last Month
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_widget11_tab2_content" role="tab" aria-selected="false">
                                        Last Year
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_widget11_tab3_content" role="tab" aria-selected="false">
                                        All Time
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <!--Begin::Tab Content-->
                        <div class="tab-content">

                            <!--begin::tab 1 content-->
                            <div class="tab-pane active" id="kt_widget11_tab1_content">

                                <!--begin::Widget 11-->
                                <div class="kt-widget11">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <td style="width:20%">Property</td>
                                                    <td style="width:25%">Reference Number</td>
                                                    <td style="width:20%">Reservation Date</td>
                                                    <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                    <td class="kt-align-center" style="width:22%">Buyer</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <!--end::Widget 11-->
                            </div>

                            <!--end::tab 1 content-->

                            <!--begin::tab 2 content-->
                            <div class="tab-pane" id="kt_widget11_tab2_content">

                                <!--begin::Widget 11-->
                                <div class="kt-widget11">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <td style="width:20%">Property</td>
                                                    <td style="width:25%">Reference Number</td>
                                                    <td style="width:20%">Reservation Date</td>
                                                    <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                    <td class="kt-align-center" style="width:22%">Buyer</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <!--end::Widget 11-->
                            </div>

                            <!--end::tab 2 content-->

                            <!--begin::tab 1 content-->
                            <div class="tab-pane" id="kt_widget11_tab3_content">

                                <!--begin::Widget 11-->
                                <div class="kt-widget11">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <td style="width:20%">Property</td>
                                                    <td style="width:25%">Reference Number</td>
                                                    <td style="width:20%">Reservation Date</td>
                                                    <td class="kt-align-center" style="width:15%">Payment Percentage</td>
                                                    <td class="kt-align-center" style="width:22%">Buyer</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                                <tr>
                                                    <td>0001001001</td>
                                                    <td>CTV111R00001</td>
                                                    <td>August 10, 2019</td>
                                                    <td class="kt-align-center">10%</td>
                                                    <td class="kt-align-center">Pedro Cruz</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <!--end::Widget 11-->
                            </div>

                            <!--end::tab 1 content-->

                            <!--begin::tab 3 content-->
                            <div class="tab-pane" id="kt_widget11_tab3_content">
                            </div>

                            <!--end::tab 3 content-->
                        </div>

                        <!--End::Tab Content-->
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>