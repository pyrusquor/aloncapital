<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inquiry_sub_category_model extends MY_Model {
    public $table = 'inquiry_sub_categories'; // you MUST mention the table name
    public $primary_key = 'id';
    public $fillable = [
        'inquiry_category_id',
        'name',
        'code',
        'description',
        'is_active',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        ];
    public $protected = ['id']; // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $rules = [];

    public $fields = [
        /* ==================== begin: Add model fields ==================== */
        'inquiry_category_id' => array(
            'field' => 'inquiry_category_id',
            'label' => 'Inquiry Category',
            'rules' => 'trim|required'
        ),
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        'code' => array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim'
        ),
        /* ==================== end: Add model fields ==================== */
    ];

    public function __construct()
    {
        parent::__construct();

        $this->soft_deletes = true;
        $this->return_as = 'array';

        $this->rules['insert'] = $this->fields;
        $this->rules['update'] = $this->fields;

        // for relationship tables
        $this->has_one['inquiry_category'] = array('foreign_model' => 'inquiry_categories/inquiry_category_model', 'foreign_table' => 'inquiry_categories', 'foreign_key' => 'id', 'local_key' => 'inquiry_category_id');
    }

    function get_columns() {
        $_return = FALSE;

        if ($this->fillable) {
            $_return = $this->fillable;
        }

        return $_return;
    }

    public function insert_dummy()
    {
        require APPPATH.'/third_party/faker/autoload.php';
        $faker = Faker\Factory::create();

        $data = [];

        for($x = 0; $x < 10; $x++)
        {
            array_push($data,array(
                'name'=> $faker->word,
            ));
        }
        $this->db->insert_batch($this->table, $data);

    }
}