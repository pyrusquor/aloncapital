<?php
// ==================== begin: Add model fields ====================
$id = isset($info['id']) && $info['id'] ? $info['id'] : '';
$name = isset($info['name']) && $info['name'] ? $info['name'] : '';
$code = isset($info['code']) && $info['code'] ? $info['code'] : '';
$inquirt_category_id = isset($info['inquirt_category_id']) && $info['inquirt_category_id'] ? $info['inquirt_category_id'] : '';
$description = isset($info['description']) && $info['description'] ? $info['description'] : '';

$inquiry_category = isset($info['inquiry_category']['name']) && $info['inquiry_category']['name'] ? $info['inquiry_category']['name'] : 'N/A';
// ==================== end: Add model fields ====================

?>

<div class="row">
    <!-- ==================== begin: Add form model fields ==================== -->
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Name <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="name"
                   value="<?php echo set_value('name', $name); ?>" placeholder="Name"
                   autocomplete="off" id="name">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Code <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="code"
                   value="<?php echo set_value('code', $code); ?>" placeholder="Code"
                   autocomplete="off" id="code">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="">Description <span class="kt-font-danger">*</span></label>
            <input type="text" class="form-control" name="description"
                   value="<?php echo set_value('description', $description); ?>" placeholder="Description"
                   autocomplete="off" id="description">
            <span class="form-text text-muted"></span>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-control-label">Inquiry Category</label>
                <div class="kt-input-icon kt-input-icon--left">
                    <select class="form-control" name="inquiry_category_id"
                            placeholder="Type"
                            autocomplete="off" id="inquiry_category_id">
                        <option>Select option</option>
                    </select>
                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i
                                class="la la-briefcase"></i></span></span>
                </div>
                <?php echo form_error('inquiry_category_id'); ?>
                <span class="form-text text-muted"></span>
        </div>
    </div>
    <!-- ==================== end: Add form model fields ==================== -->
</div>