<?php
$id = isset($data['id']) && $data['id'] ? $data['id'] : '';
$name = isset($data['name']) && $data['name'] ? $data['name'] : '';
$code = isset($data['code']) && $data['code'] ? $data['code'] : '';
$complaint_category_id = isset($data['complaint_category_id']) && $data['complaint_category_id'] ? $data['complaint_category_id'] : '';
$description = isset($data['description']) && $data['description'] ? $data['description'] : '';

$complaint_category = isset($data['complaint_category']['name']) && $data['complaint_category']['name'] ? $data['complaint_category']['name'] : 'N/A';
// ==================== begin: Add model fields ====================

// ==================== end: Add model fields ====================
?>
<!-- CONTENT HEADER -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
  <div class="kt-container kt-container--fluid">
    <div class="kt-subheader__main">
      <h3 class="kt-subheader__title">Complaint Sub Category</h3>
    </div>
    <div class="kt-subheader__toolbar">
      <div class="kt-subheader__wrapper">
        <a href="<?php echo site_url('inquiry_sub_categories/form/'.$id);?>" class="btn btn-label-success btn-elevate btn-sm">
          <i class="fa fa-edit"></i> Edit Complaint Sub Category
        </a>   
        <a href="<?php echo site_url('inquiry_sub_categories');?>" class="btn btn-label-instagram btn-sm btn-elevate">
          <i class="fa fa-reply"></i> Back
        </a>
      </div>
    </div>
  </div>
</div>

<!-- begin:: Content -->
<div
  class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid"
>
  <div class="row">
    <div class="col-md-6">
      <!--begin::Portlet-->
      <div class="kt-portlet">
        <div class="kt-portlet__body">
          <!--begin::Portlet-->
          <div class="kt-portlet">
            <div class="kt-portlet__head">
              <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                  General Information
                </h3>
              </div>
            </div>
            <div class="kt-portlet__body">
              <!--begin::Form-->
              <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Name
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$name?></span>
                </div>
              <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Code
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$code?></span>
                </div>
               </div>
                <!-- ==================== begin: Add fields details  ==================== -->
               <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Description
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$description?></span>
                </div>
               </div>
               <div class="kt-widget13">
                <div class="kt-widget13__item">
                  <span class="kt-widget13__desc">
                    Complaint Category
                  </span>
                  <span
                    class="kt-widget13__text kt-widget13__text--bold"
                  ><?=$complaint_category?></span>
                </div>
               </div>
                <!-- ==================== end: Add model details ==================== -->
              
              <!--end::Form-->
            </div>
          </div>
          <!--end::Portlet-->
        </div>
      </div>
      <!--end::Portlet-->
    </div>

    <div class="col-md-6"></div>
  </div>
</div>
<!-- begin:: Footer -->
