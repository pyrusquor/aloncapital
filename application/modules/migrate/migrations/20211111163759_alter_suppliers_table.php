<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_suppliers_table extends CI_Migration
{

    public function up()
    {
        if ($this->db->field_exists('delivery_terms', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` MODIFY `delivery_terms` VARCHAR(50)");
        }

        if ($this->db->field_exists('email_address', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` MODIFY `email_address` VARCHAR(50)");
        }

        if ($this->db->field_exists('fax_number', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` MODIFY `fax_number` VARCHAR(50)");
        }

        if ($this->db->field_exists('alternate_address', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` MODIFY `alternate_address` VARCHAR(50)");
        }

        if ($this->db->field_exists('tin_number', 'suppliers')) {
            $this->db->query("ALTER TABLE `suppliers` MODIFY `tin_number` VARCHAR(50)");
        }
    }

    public function down()
    {
        // 
    }
}
