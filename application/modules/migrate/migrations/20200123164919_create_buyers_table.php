<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_buyers_table extends CI_Migration
{

     public function up()
    {
        if(!$this->db->table_exists('buyers'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'user_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'type_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                    'COMMENT' => '0=Person, 1=Company'
                ),
                'last_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => FALSE
                ),
                'first_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => FALSE
                ),
                'middle_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE
                ),
                'birth_place' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'birth_date' => array(
                    'type' => 'DATE',
                    'NULL' => FALSE
                ),
                'gender' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                    'COMMENT' => '1=MALE,2=FEMALE',
                ),
                'nationality' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                ),
                'housing_membership_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE,
                ),
                'bis_file' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE,
                ),
                'present_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE
                ),
                'mailing_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE
                ),
                'business_address' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'mobile_no' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'email' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE
                ),
                'landline' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '10',
                    'NULL' => TRUE
                ),
                'is_active' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('buyers', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('buyers') ) {

            $this->dbforge->drop_table('buyers');
        }
    }
}
