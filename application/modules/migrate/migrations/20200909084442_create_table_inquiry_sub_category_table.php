<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_inquiry_sub_category_table extends CI_Migration
{
    public function up()
    {

        if(!$this->db->table_exists('inquiry_sub_categories'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'inquiry_category_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '64',
                    'NULL' => FALSE
                ),
                'code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '64',
                    'NULL' => TRUE
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'NULL' => TRUE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('inquiry_sub_categories', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('inquiry_sub_categories') ) {

			$this->dbforge->drop_table('inquiry_sub_categories');
        }

    }
}
