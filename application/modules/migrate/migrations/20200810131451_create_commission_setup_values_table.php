<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_commission_setup_values_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('commission_setup_values'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'commission_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'period_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'percentage_to_finish' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_rate_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_rate_percentage' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_term' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_vat' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'is_active' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('commission_setup_values', TRUE);
        }



        $this->db->query("ALTER TABLE `commission_setup_values` CHANGE `period_id` `period_id` int(11) NOT NULL DEFAULT '0' AFTER `commission_id`, CHANGE `percentage_to_finish` `percentage_to_finish` double(20,2) NOT NULL DEFAULT '0.00' AFTER `period_id`, CHANGE `commission_rate_amount` `commission_rate_amount` double(20,2) NOT NULL DEFAULT '0.00' AFTER `percentage_to_finish`, CHANGE `commission_rate_percentage` `commission_rate_percentage` double(20,2) NOT NULL DEFAULT '0.00' AFTER `commission_rate_amount`, CHANGE `commission_term` `commission_term` double(20,2) NOT NULL DEFAULT '0.00' AFTER `commission_rate_percentage`, CHANGE `commission_vat` `commission_vat` double(20,2) NOT NULL DEFAULT '0.00' AFTER `commission_term`");

        if($this->db->table_exists('commission_setup')) {
            if($this->db->field_exists('period_id', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'period_id');
            }
            if($this->db->field_exists('percentage_to_finish', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'percentage_to_finish');
            }
            if($this->db->field_exists('commission_rate_amount', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'commission_rate_amount');
            }
            if($this->db->field_exists('commission_rate_percentage', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'commission_rate_percentage');
            }
            if($this->db->field_exists('commission_term', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'commission_term');
            }
            if($this->db->field_exists('commission_vat', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'commission_vat');
            }
            if($this->db->field_exists('is_active', 'commission_setup')) {
                $this->dbforge->drop_column('commission_setup', 'is_active');
            }
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('commission_setup_values') ) {

			$this->dbforge->drop_table('commission_setup_values');
        }
        
        if($this->db->table_exists('commission_setup')) {
            if (!$this->db->field_exists('period_id', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `period_id` int(11) DEFAULT NULL  AFTER `name`");
            }
            if (!$this->db->field_exists('percentage_to_finish', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `percentage_to_finish` double(20,2) DEFAULT NULL  AFTER `name`");
            }
            if (!$this->db->field_exists('commission_rate_amount', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `commission_rate_amount` double(20,2) DEFAULT NULL  AFTER `name`");
            }
            if (!$this->db->field_exists('commission_rate_percentage', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `commission_rate_percentage` double(20,2) DEFAULT NULL  AFTER `name`");
            }
            if (!$this->db->field_exists('commission_term', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `commission_term` double(20,2) DEFAULT NULL  AFTER `name`");
            }
            if (!$this->db->field_exists('commission_vat', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `commission_vat` double(20,2) DEFAULT NULL  AFTER `name`");
            }
            if (!$this->db->field_exists('is_active', 'commission_setup')) {
                $this->db->query("ALTER TABLE `commission_setup` ADD COLUMN `is_active` tinyint(1) DEFAULT NULL  AFTER `name`");
            }
        }
    }
}
