<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_seller_source_info_drop_add_column extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('seller_source_infos'))
		{
			if ($this->db->field_exists('group_id', 'seller_source_infos'))
			{
				$this->dbforge->drop_column('seller_source_infos', 'group_id');
            }
            
        }
        
        if($this->db->table_exists('seller_source_infos'))
		{	
			if (!$this->db->field_exists('group', 'seller_source_infos'))
			{
				$this->db->query("ALTER TABLE `seller_source_infos` ADD COLUMN `group` VARCHAR(100) NOT NULL AFTER `is_member`");
			}
		}
    }

    public function down()
    {
        if($this->db->table_exists('sellers'))
		{
			if ($this->db->field_exists('group', 'sellers'))
			{
				$this->dbforge->drop_column('sellers', 'group');
			}
		}
    }
}
