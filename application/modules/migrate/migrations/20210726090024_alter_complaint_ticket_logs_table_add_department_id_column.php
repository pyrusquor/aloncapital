<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_complaint_ticket_logs_table_add_department_id_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('complaint_ticket_logs')) {

            if (!$this->db->field_exists('department_id', 'complaint_ticket_logs')) {
                $this->db->query("ALTER TABLE `complaint_ticket_logs` ADD COLUMN `department_id` INT(11) NULL AFTER `complaint_id`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('complaint_ticket_logs')) {

            if ($this->db->field_exists('department_id', 'complaint_ticket_logs')) {
                $this->dbforge->drop_column('complaint_ticket_logs', 'department_id');
            }
        }
    }
}
