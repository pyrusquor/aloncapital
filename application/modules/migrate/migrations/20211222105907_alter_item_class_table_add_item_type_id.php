<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_item_class_table_add_item_type_id extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('item_type_id', 'item_class')) {
            $this->db->query("ALTER TABLE `item_class` ADD `item_type_id` int(11) AFTER `id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('item_type_id', 'item_class')) {
            $this->db->query("ALTER TABLE `item_class` DROP COLUMN `item_type_id`");
        }
    }
}
