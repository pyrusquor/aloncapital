<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_commission_setup_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('commission_setup'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => FALSE
                ),
                'period_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'percentage_to_finish' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_rate_amount' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_rate_percentage' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_term' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'commission_vat' => array(
                    'type' => 'DOUBLE',
                    'constraint' => '20,2',
                    'NULL' => FALSE
                ),
                'is_active' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => FALSE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('commission_setup', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('commission_setup') ) {

			$this->dbforge->drop_table('commission_setup');
		}
    }
}
