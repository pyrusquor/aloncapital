<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_customer_service_table_modify_reference extends CI_Migration
{

    public function up()
    {
        $this->db->query("ALTER TABLE `customer_services` MODIFY column `reference` varchar(20) null");
    }

    public function down()
    {
        $this->db->query("ALTER TABLE `customer_services` MODIFY column `reference` varchar(13) null");
    }
}
