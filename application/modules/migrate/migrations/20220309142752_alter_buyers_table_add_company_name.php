<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_buyers_table_add_company_name extends CI_Migration
{

    public function up()
    {
        if (!$this->db->field_exists('company_name', 'buyers')) {
            $this->db->query("ALTER TABLE `buyers` ADD `company_name` varchar(50) NULL AFTER `type_id`");
        }
    }

    public function down()
    {
        if ($this->db->field_exists('company_name', 'buyers')) {
            $this->db->query("ALTER TABLE `buyers` DROP COLUMN `company_name`");
        }
    }
}
