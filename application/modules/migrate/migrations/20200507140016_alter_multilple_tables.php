<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multilple_tables extends CI_Migration
{

    public function up()
    {
        // seller_contact_infos
        if (!$this->db->field_exists('created_at', 'seller_contact_infos')) {
            $this->db->query("ALTER TABLE `seller_contact_infos` ADD COLUMN `created_at` datetime NULL AFTER `landline`");
        }
        if (!$this->db->field_exists('created_by', 'seller_contact_infos')) {
            $this->db->query("ALTER TABLE `seller_contact_infos` ADD COLUMN `created_by` INT(11) NULL AFTER `created_at`");
        }
        if (!$this->db->field_exists('updated_at', 'seller_contact_infos')) {
            $this->db->query("ALTER TABLE `seller_contact_infos` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }
        if (!$this->db->field_exists('updated_by', 'seller_contact_infos')) {
            $this->db->query("ALTER TABLE `seller_contact_infos` ADD COLUMN `updated_by` INT(11) NULL AFTER `updated_at`");
        }
        if (!$this->db->field_exists('deleted_at', 'seller_contact_infos')) {
            $this->db->query("ALTER TABLE `seller_contact_infos` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }
        if (!$this->db->field_exists('deleted_by', 'seller_contact_infos')) {
            $this->db->query("ALTER TABLE `seller_contact_infos` ADD COLUMN `deleted_by` INT(11) NULL AFTER `deleted_at`");
        }

        // seller_educations
        if (!$this->db->field_exists('created_at', 'seller_educations')) {
            $this->db->query("ALTER TABLE `seller_educations` ADD COLUMN `created_at` datetime NULL AFTER `status`");
        }
        if (!$this->db->field_exists('created_by', 'seller_educations')) {
            $this->db->query("ALTER TABLE `seller_educations` ADD COLUMN `created_by` INT(11) NULL AFTER `created_at`");
        }
        if (!$this->db->field_exists('updated_at', 'seller_educations')) {
            $this->db->query("ALTER TABLE `seller_educations` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }
        if (!$this->db->field_exists('updated_by', 'seller_educations')) {
            $this->db->query("ALTER TABLE `seller_educations` ADD COLUMN `updated_by` INT(11) NULL AFTER `updated_at`");
        }
        if (!$this->db->field_exists('deleted_at', 'seller_educations')) {
            $this->db->query("ALTER TABLE `seller_educations` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }
        if (!$this->db->field_exists('deleted_by', 'seller_educations')) {
            $this->db->query("ALTER TABLE `seller_educations` ADD COLUMN `deleted_by` INT(11) NULL AFTER `deleted_at`");
        }

        // seller_exams
        if (!$this->db->field_exists('created_at', 'seller_exams')) {
            $this->db->query("ALTER TABLE `seller_exams` ADD COLUMN `created_at` datetime NULL AFTER `status`");
        }
        if (!$this->db->field_exists('created_by', 'seller_exams')) {
            $this->db->query("ALTER TABLE `seller_exams` ADD COLUMN `created_by` INT(11) NULL AFTER `created_at`");
        }
        if (!$this->db->field_exists('updated_at', 'seller_exams')) {
            $this->db->query("ALTER TABLE `seller_exams` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }
        if (!$this->db->field_exists('updated_by', 'seller_exams')) {
            $this->db->query("ALTER TABLE `seller_exams` ADD COLUMN `updated_by` INT(11) NULL AFTER `updated_at`");
        }
        if (!$this->db->field_exists('deleted_at', 'seller_exams')) {
            $this->db->query("ALTER TABLE `seller_exams` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }
        if (!$this->db->field_exists('deleted_by', 'seller_exams')) {
            $this->db->query("ALTER TABLE `seller_exams` ADD COLUMN `deleted_by` INT(11) NULL AFTER `deleted_at`");
        }

        // seller_references
        if (!$this->db->field_exists('created_at', 'seller_references')) {
            $this->db->query("ALTER TABLE `seller_references` ADD COLUMN `created_at` datetime NULL AFTER `ref_contact_no`");
        }
        if (!$this->db->field_exists('created_by', 'seller_references')) {
            $this->db->query("ALTER TABLE `seller_references` ADD COLUMN `created_by` INT(11) NULL AFTER `created_at`");
        }
        if (!$this->db->field_exists('updated_at', 'seller_references')) {
            $this->db->query("ALTER TABLE `seller_references` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }
        if (!$this->db->field_exists('updated_by', 'seller_references')) {
            $this->db->query("ALTER TABLE `seller_references` ADD COLUMN `updated_by` INT(11) NULL AFTER `updated_at`");
        }
        if (!$this->db->field_exists('deleted_at', 'seller_references')) {
            $this->db->query("ALTER TABLE `seller_references` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }
        if (!$this->db->field_exists('deleted_by', 'seller_references')) {
            $this->db->query("ALTER TABLE `seller_references` ADD COLUMN `deleted_by` INT(11) NULL AFTER `deleted_at`");
        }

        // seller_source_infos
        if (!$this->db->field_exists('created_at', 'seller_source_infos')) {
            $this->db->query("ALTER TABLE `seller_source_infos` ADD COLUMN `created_at` datetime NULL AFTER `group`");
        }
        if (!$this->db->field_exists('created_by', 'seller_source_infos')) {
            $this->db->query("ALTER TABLE `seller_source_infos` ADD COLUMN `created_by` INT(11) NULL AFTER `created_at`");
        }
        if (!$this->db->field_exists('updated_at', 'seller_source_infos')) {
            $this->db->query("ALTER TABLE `seller_source_infos` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }
        if (!$this->db->field_exists('updated_by', 'seller_source_infos')) {
            $this->db->query("ALTER TABLE `seller_source_infos` ADD COLUMN `updated_by` INT(11) NULL AFTER `updated_at`");
        }
        if (!$this->db->field_exists('deleted_at', 'seller_source_infos')) {
            $this->db->query("ALTER TABLE `seller_source_infos` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }
        if (!$this->db->field_exists('deleted_by', 'seller_source_infos')) {
            $this->db->query("ALTER TABLE `seller_source_infos` ADD COLUMN `deleted_by` INT(11) NULL AFTER `deleted_at`");
        }

        // seller_trainings
        if (!$this->db->field_exists('created_at', 'seller_trainings')) {
            $this->db->query("ALTER TABLE `seller_trainings` ADD COLUMN `created_at` datetime NULL AFTER `year`");
        }
        if (!$this->db->field_exists('created_by', 'seller_trainings')) {
            $this->db->query("ALTER TABLE `seller_trainings` ADD COLUMN `created_by` INT(11) NULL AFTER `created_at`");
        }
        if (!$this->db->field_exists('updated_at', 'seller_trainings')) {
            $this->db->query("ALTER TABLE `seller_trainings` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }
        if (!$this->db->field_exists('updated_by', 'seller_trainings')) {
            $this->db->query("ALTER TABLE `seller_trainings` ADD COLUMN `updated_by` INT(11) NULL AFTER `updated_at`");
        }
        if (!$this->db->field_exists('deleted_at', 'seller_trainings')) {
            $this->db->query("ALTER TABLE `seller_trainings` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }
        if (!$this->db->field_exists('deleted_by', 'seller_trainings')) {
            $this->db->query("ALTER TABLE `seller_trainings` ADD COLUMN `deleted_by` INT(11) NULL AFTER `deleted_at`");
        }

        // seller_work_experiences
        if (!$this->db->field_exists('created_at', 'seller_work_experiences')) {
            $this->db->query("ALTER TABLE `seller_work_experiences` ADD COLUMN `created_at` datetime NULL AFTER `to_contact`");
        }
        if (!$this->db->field_exists('created_by', 'seller_work_experiences')) {
            $this->db->query("ALTER TABLE `seller_work_experiences` ADD COLUMN `created_by` INT(11) NULL AFTER `created_at`");
        }
        if (!$this->db->field_exists('updated_at', 'seller_work_experiences')) {
            $this->db->query("ALTER TABLE `seller_work_experiences` ADD COLUMN `updated_at` datetime NULL AFTER `created_by`");
        }
        if (!$this->db->field_exists('updated_by', 'seller_work_experiences')) {
            $this->db->query("ALTER TABLE `seller_work_experiences` ADD COLUMN `updated_by` INT(11) NULL AFTER `updated_at`");
        }
        if (!$this->db->field_exists('deleted_at', 'seller_work_experiences')) {
            $this->db->query("ALTER TABLE `seller_work_experiences` ADD COLUMN `deleted_at` datetime NULL AFTER `updated_by`");
        }
        if (!$this->db->field_exists('deleted_by', 'seller_work_experiences')) {
            $this->db->query("ALTER TABLE `seller_work_experiences` ADD COLUMN `deleted_by` INT(11) NULL AFTER `deleted_at`");
        }
    }

    public function down()
    {
        // seller_contact_infos
        if ($this->db->field_exists('created_at', 'seller_contact_infos')) {
            $this->dbforge->drop_column('seller_contact_infos', 'created_at');
        }
        if ($this->db->field_exists('created_by', 'seller_contact_infos')) {
            $this->dbforge->drop_column('seller_contact_infos', 'created_by');
        }
        if ($this->db->field_exists('updated_at', 'seller_contact_infos')) {
            $this->dbforge->drop_column('seller_contact_infos', 'updated_at');
        }
        if ($this->db->field_exists('updated_by', 'seller_contact_infos')) {
            $this->dbforge->drop_column('seller_contact_infos', 'updated_by');
        }
        if ($this->db->field_exists('deleted_at', 'seller_contact_infos')) {
            $this->dbforge->drop_column('seller_contact_infos', 'deleted_at');
        }
        if ($this->db->field_exists('deleted_by', 'seller_contact_infos')) {
            $this->dbforge->drop_column('seller_contact_infos', 'deleted_by');
        }

        // seller_educations
        if ($this->db->field_exists('created_at', 'seller_educations')) {
            $this->dbforge->drop_column('seller_educations', 'created_at');
        }
        if ($this->db->field_exists('created_by', 'seller_educations')) {
            $this->dbforge->drop_column('seller_educations', 'created_by');
        }
        if ($this->db->field_exists('updated_at', 'seller_educations')) {
            $this->dbforge->drop_column('seller_educations', 'updated_at');
        }
        if ($this->db->field_exists('updated_by', 'seller_educations')) {
            $this->dbforge->drop_column('seller_educations', 'updated_by');
        }
        if ($this->db->field_exists('deleted_at', 'seller_educations')) {
            $this->dbforge->drop_column('seller_educations', 'deleted_at');
        }
        if ($this->db->field_exists('deleted_by', 'seller_educations')) {
            $this->dbforge->drop_column('seller_educations', 'deleted_by');
        }

        // seller_exams
        if ($this->db->field_exists('created_at', 'seller_exams')) {
            $this->dbforge->drop_column('seller_exams', 'created_at');
        }
        if ($this->db->field_exists('created_by', 'seller_exams')) {
            $this->dbforge->drop_column('seller_exams', 'created_by');
        }
        if ($this->db->field_exists('updated_at', 'seller_exams')) {
            $this->dbforge->drop_column('seller_exams', 'updated_at');
        }
        if ($this->db->field_exists('updated_by', 'seller_exams')) {
            $this->dbforge->drop_column('seller_exams', 'updated_by');
        }
        if ($this->db->field_exists('deleted_at', 'seller_exams')) {
            $this->dbforge->drop_column('seller_exams', 'deleted_at');
        }
        if ($this->db->field_exists('deleted_by', 'seller_exams')) {
            $this->dbforge->drop_column('seller_exams', 'deleted_by');
        }

        // seller_references
        if ($this->db->field_exists('created_at', 'seller_references')) {
            $this->dbforge->drop_column('seller_references', 'created_at');
        }
        if ($this->db->field_exists('created_by', 'seller_references')) {
            $this->dbforge->drop_column('seller_references', 'created_by');
        }
        if ($this->db->field_exists('updated_at', 'seller_references')) {
            $this->dbforge->drop_column('seller_references', 'updated_at');
        }
        if ($this->db->field_exists('updated_by', 'seller_references')) {
            $this->dbforge->drop_column('seller_references', 'updated_by');
        }
        if ($this->db->field_exists('deleted_at', 'seller_references')) {
            $this->dbforge->drop_column('seller_references', 'deleted_at');
        }
        if ($this->db->field_exists('deleted_by', 'seller_references')) {
            $this->dbforge->drop_column('seller_references', 'deleted_by');
        }

        // seller_source_infos
        if ($this->db->field_exists('created_at', 'seller_source_infos')) {
            $this->dbforge->drop_column('seller_source_infos', 'created_at');
        }
        if ($this->db->field_exists('created_by', 'seller_source_infos')) {
            $this->dbforge->drop_column('seller_source_infos', 'created_by');
        }
        if ($this->db->field_exists('updated_at', 'seller_source_infos')) {
            $this->dbforge->drop_column('seller_source_infos', 'updated_at');
        }
        if ($this->db->field_exists('updated_by', 'seller_source_infos')) {
            $this->dbforge->drop_column('seller_source_infos', 'updated_by');
        }
        if ($this->db->field_exists('deleted_at', 'seller_source_infos')) {
            $this->dbforge->drop_column('seller_source_infos', 'deleted_at');
        }
        if ($this->db->field_exists('deleted_by', 'seller_source_infos')) {
            $this->dbforge->drop_column('seller_source_infos', 'deleted_by');
        }

        // seller_trainings
        if ($this->db->field_exists('created_at', 'seller_trainings')) {
            $this->dbforge->drop_column('seller_trainings', 'created_at');
        }
        if ($this->db->field_exists('created_by', 'seller_trainings')) {
            $this->dbforge->drop_column('seller_trainings', 'created_by');
        }
        if ($this->db->field_exists('updated_at', 'seller_trainings')) {
            $this->dbforge->drop_column('seller_trainings', 'updated_at');
        }
        if ($this->db->field_exists('updated_by', 'seller_trainings')) {
            $this->dbforge->drop_column('seller_trainings', 'updated_by');
        }
        if ($this->db->field_exists('deleted_at', 'seller_trainings')) {
            $this->dbforge->drop_column('seller_trainings', 'deleted_at');
        }
        if ($this->db->field_exists('deleted_by', 'seller_trainings')) {
            $this->dbforge->drop_column('seller_trainings', 'deleted_by');
        }

        // seller_work_experiences
        if ($this->db->field_exists('created_at', 'seller_work_experiences')) {
            $this->dbforge->drop_column('seller_work_experiences', 'created_at');
        }
        if ($this->db->field_exists('created_by', 'seller_work_experiences')) {
            $this->dbforge->drop_column('seller_work_experiences', 'created_by');
        }
        if ($this->db->field_exists('updated_at', 'seller_work_experiences')) {
            $this->dbforge->drop_column('seller_work_experiences', 'updated_at');
        }
        if ($this->db->field_exists('updated_by', 'seller_work_experiences')) {
            $this->dbforge->drop_column('seller_work_experiences', 'updated_by');
        }
        if ($this->db->field_exists('deleted_at', 'seller_work_experiences')) {
            $this->dbforge->drop_column('seller_work_experiences', 'deleted_at');
        }
        if ($this->db->field_exists('deleted_by', 'seller_work_experiences')) {
            $this->dbforge->drop_column('seller_work_experiences', 'deleted_by');
        }

    }
}
