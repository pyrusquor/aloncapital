<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Add_companies_table extends CI_Migration
{

    public function up(){
        if(!$this->db->table_exists('companies'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' =>TRUE
                ),
                'location' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' =>TRUE
                ),
                'email' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' =>TRUE
                ),
                'year_started' => array(
                    'type' => 'INT',
                    'constraint' => '4',
                    'NULL' =>TRUE
                ),
                'contact_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' =>TRUE
                ),
                'tin' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' =>TRUE
                ),
                'sss' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' =>TRUE
                ),
                'philhealth' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' =>TRUE
                ),
                'hdmf' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' =>TRUE
                ),
                'sec_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' =>TRUE
                ),
                'website' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' =>TRUE
                ),
                'inc_date' => array(
                    'type'=>'DATE',
                    'NULL'=>TRUE,
                ),
                'bir_reg_date' => array(
                    'type'=>'DATE',
                    'NULL'=>TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=>TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=>TRUE,
                    'NULL'=>TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=>TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=>TRUE,
                    'NULL'=>TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=>TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=>TRUE,
                    'NULL'=>TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('companies', TRUE);

            $this->load->model('company/Company_model', 'company');
            $this->company->insert_dummy();
        }
    }

    public function down(){
        if($this->db->table_exists('companies'))
         {
            $this->dbforge->drop_table('companies');
         }
    }
}
