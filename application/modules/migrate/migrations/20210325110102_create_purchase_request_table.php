<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Create_purchase_request_table extends CI_Migration
    {
        private $tbl = "purchase_order_requests";
        private $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'reference' => array(
                'type' => 'VARCHAR',
                'NULL' => FALSE,
                'constraint' => '24'
            ),
            'company_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
            'approving_staff_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
            'requesting_staff_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => TRUE,
            ),
            'particulars' => array(
                'type' => 'VARCHAR',
                'NULL' => TRUE,
                'constraint' => '255'
            ),
            'request_date' => array(
                'type' => 'DATE',
                'NULL' => FALSE
            ),
            'request_reason' => array(
                'type' => 'VARCHAR',
                'NULL' => TRUE,
                'constraint' => '255'
            ),
            'request_status' => array(
                'type' => 'INT',
                'constraint' => '2',
                'default' => 0
            ),
            'created_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'created_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'updated_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'updated_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'deleted_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'deleted_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            )
        );

        public function up()
        {
            if(!$this->db->table_exists($this->tbl)) {
                $this->dbforge->add_field($this->fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($this->tbl, TRUE);
            }
        }

        public function down()
        {
            if($this->db->table_exists($this->tbl)){
                $this->dbforge->drop_table($this->tbl);
            }
        }
    }
