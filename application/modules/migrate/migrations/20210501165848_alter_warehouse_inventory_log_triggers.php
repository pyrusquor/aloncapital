<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_warehouse_inventory_log_triggers extends CI_Migration
{

    public function up()
    {
//        if($this->db->table_exists('warehouse_inventory_logs')){
//            if($this->db->table_exists('warehouses') && $this->db->table_exists('warehouse_inventories')){
//                $this->db->query("DROP TRIGGER IF EXISTS `warehouse_inventory_insert`");
//                $this->db->query("DROP TRIGGER IF EXISTS `warehouse_inventory_update`");
//                $this->db->query("
//                    CREATE TRIGGER `warehouse_inventory_insert` AFTER INSERT ON `warehouse_inventories`
//                    FOR EACH ROW
//                    BEGIN
//                        INSERT INTO warehouse_inventory_logs(
//                            created_at,
//                            created_by,
//                            updated_by,
//                            deleted_by,
//                            warehouse_id,
//                            warehouse_inventory_id,
//                            item_id,
//                            unit_of_measurement_id,
//                            actual_quantity_in,
//                            actual_quantity_out,
//                            available_quantity_in,
//                            available_quantity_out,
//                            transaction_type,
//                            current_actual_quantity,
//                            current_available_quantity,
//                            unit_price
//                        )
//                        VALUES(
//                            NOW(),
//                            NEW.created_by,
//                            NEW.updated_by,
//                            NEW.deleted_by,
//                            NEW.warehouse_id,
//                            NEW.id,
//                            NEW.item_id,
//                            NEW.unit_of_measurement_id,
//                            NEW.actual_quantity,
//                            0,
//                            NEW.available_quantity,
//                            0,
//                            4,
//                            NEW.actual_quantity,
//                            NEW.available_quantity,
//                            NEW.unit_price
//                        );
//                    END
//                ");
//
//                $this->db->query("
//                    CREATE TRIGGER `warehouse_inventory_update` AFTER UPDATE ON `warehouse_inventories`
//                    FOR EACH ROW
//                    BEGIN
//                        INSERT INTO warehouse_inventory_logs(
//                            created_at,
//                            created_by,
//                            updated_by,
//                            deleted_by,
//                            warehouse_id,
//                            warehouse_inventory_id,
//                            item_id,
//                            unit_of_measurement_id,
//                            actual_quantity_in,
//                            actual_quantity_out,
//                            available_quantity_in,
//                            available_quantity_out,
//                            transaction_type,
//                            current_actual_quantity,
//                            current_available_quantity,
//                            unit_price
//                        )
//                        VALUES(
//                            NOW(),
//                            NEW.created_by,
//                            NEW.updated_by,
//                            NEW.deleted_by,
//                            NEW.warehouse_id,
//                            NEW.id,
//                            NEW.item_id,
//                            NEW.unit_of_measurement_id,
//                            IF (NEW.actual_quantity > OLD.actual_quantity, NEW.actual_quantity - OLD.actual_quantity, 0),
//                            IF (NEW.actual_quantity < OLD.actual_quantity, OLD.actual_quantity - NEW.actual_quantity, 0),
//                            IF (NEW.available_quantity > OLD.available_quantity, NEW.available_quantity - OLD.available_quantity, 0),
//                            IF (NEW.available_quantity < OLD.available_quantity, OLD.available_quantity - NEW.available_quantity, 0),
//                            5,
//                            NEW.actual_quantity,
//                            NEW.available_quantity,
//                            NEW.unit_price
//                        );
//                    END
//                ");
//            }
//        }
    }

    public function down()
    {
//        $this->db->query("DROP TRIGGER IF EXISTS `warehouse_inventory_insert`");
//        $this->db->query("DROP TRIGGER IF EXISTS `warehouse_inventory_update`");
    }
}
