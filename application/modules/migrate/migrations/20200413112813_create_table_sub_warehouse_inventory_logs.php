<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_sub_warehouse_inventory_logs extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => true,
                'auto_increment' => true,
                'NOT NULL' => false,
            ),
            'company' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'sbu' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'NULL' => false,
            ),
            'warehouse' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'item_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'item_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'in' => array(
                'type' => 'VARCHAR',
                'constraint' => '11',
                'NULL' => false,
            ),
            'out' => array(
                'type' => 'VARCHAR',
                'constraint' => '11',
                'NULL' => false,
            ),
            'balance' => array(
                'type' => 'VARCHAR',
                'constraint' => '11',
                'NULL' => false,
            ),
            'remarks' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => false,
            ),
            'is_active' => array(
                'type' => 'INT',
                'constraint' => '1',
                'NULL' => false,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => true,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => true,
                'NULL' => true,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('sub_warehouse_inventory_logs', true);

        $this->load->model('sub_warehouse_inventory_logs/Sub_warehouse_inventory_logs_model', 'sub_warehouse_inventory_logs');
        $this->sub_warehouse_inventory_logs->insert_dummy();
    }

    public function down()
    {
        if ($this->db->table_exists('sub_warehouse_inventory_logs')) {

            $this->dbforge->drop_table('sub_warehouse_inventory_logs');
        }
    }
}
