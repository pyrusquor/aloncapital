<?php
    defined('BASEPATH') or exit('No direct script access allowed.');

    class Migration_Alter_material_receiving_add_warehouse_types extends CI_Migration
    {

        public function up()
        {
            $query1 = "ALTER TABLE `warehouses` ALTER `contact_person` DROP DEFAULT, ALTER `email` DROP DEFAULT, ALTER `fax_number` DROP DEFAULT, ALTER `mobile_number` DROP DEFAULT, ALTER `sbu_id` DROP DEFAULT, ALTER `telephone_number` DROP DEFAULT, ALTER `address` DROP DEFAULT";
            $query2 = "ALTER TABLE `warehouses` CHANGE COLUMN `id` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT FIRST, ADD COLUMN `parent_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `id`, ADD COLUMN `warehouse_type` TINYINT UNSIGNED NOT NULL DEFAULT '1' COMMENT '1 -> Warehouse, 2 -> Subwarehouse, 3-> Virtual' AFTER `parent_id`, CHANGE COLUMN `contact_person` `contact_person` VARCHAR(255) NULL AFTER `deleted_by`, CHANGE COLUMN `email` `email` VARCHAR(512) NULL AFTER `contact_person`, CHANGE COLUMN `fax_number` `fax_number` VARCHAR(15) NULL AFTER `email`, CHANGE COLUMN `mobile_number` `mobile_number` VARCHAR(15) NULL AFTER `is_active`, CHANGE COLUMN `sbu_id` `sbu_id` INT NULL AFTER `name`, CHANGE COLUMN `telephone_number` `telephone_number` VARCHAR(15) NULL AFTER `sbu_id`, CHANGE COLUMN `address` `address` VARCHAR(512) NULL AFTER `warehouse_code`";
            $this->db->query($query1);
            $this->db->query($query2);
        }

        public function down()
        {
            $query = "ALTER TABLE `warehouses` DROP COLUMN `parent_id`, DROP COLUMN `warehouse_type`";
            $this->db->query($query);
        }
    }
