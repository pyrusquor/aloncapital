<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_vehicle_equipment_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('vehicle_equipments')) {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'company_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'vehicle_equipment_group_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'code' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'model' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'NULL' => TRUE,
                ),
                'chassis_number' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'date_acquired' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'denomination' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'fuel_type' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'engine_number' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'make' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'gross_weight' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'net_weight' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'body_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'body_type_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'year_model' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'plate_number' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'color' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'NULL' => TRUE,
                ),
                'is_active' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE,
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
            
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('vehicle_equipments', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('vehicle_equipments') ) {

            $this->dbforge->drop_table('vehicle_equipments');
        }
    }
}
