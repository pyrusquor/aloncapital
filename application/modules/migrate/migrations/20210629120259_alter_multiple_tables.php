<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multiple_tables extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transaction_loan_document_stages')) {

            if (!$this->db->field_exists('category_id', 'transaction_loan_document_stages')) {

                $this->db->query("ALTER TABLE `transaction_loan_document_stages` ADD COLUMN `category_id` INT(11) DEFAULT 0 AFTER `description`");
            }
        }
        if ($this->db->table_exists('transaction_document_stages')) {

            if (!$this->db->field_exists('category_id', 'transaction_document_stages')) {

                $this->db->query("ALTER TABLE `transaction_document_stages` ADD COLUMN `category_id` INT(11) DEFAULT 0 AFTER `description`");
            }
        }
        if ($this->db->table_exists('checklists')) {

            if (!$this->db->field_exists('transaction_document_stage_id', 'checklists')) {

                $this->db->query("ALTER TABLE `checklists` ADD COLUMN `transaction_document_stage_id` INT(11) DEFAULT 0 AFTER `description`");
            }
            if (!$this->db->field_exists('loan_document_stage_id', 'checklists')) {

                $this->db->query("ALTER TABLE `checklists` ADD COLUMN `loan_document_stage_id` INT(11) DEFAULT 0 AFTER `description`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transaction_loan_document_stages')) {

            if ($this->db->field_exists('category_id', 'transaction_loan_document_stages')) {

                $this->dbforge->drop_column('transaction_loan_document_stages', 'category_id');
            }
        }
        if ($this->db->table_exists('transaction_document_stages')) {

            if ($this->db->field_exists('category_id', 'transaction_document_stages')) {

                $this->dbforge->drop_column('transaction_document_stages', 'category_id');
            }
        }
        if ($this->db->table_exists('checklists')) {

            if ($this->db->field_exists('transaction_document_stage_id', 'checklists')) {

                $this->dbforge->drop_column('checklists', 'transaction_document_stage_id');
            }
            if ($this->db->field_exists('loan_document_stage_id', 'checklists')) {

                $this->dbforge->drop_column('checklists', 'loan_document_stage_id');
            }
        }
    }
}
