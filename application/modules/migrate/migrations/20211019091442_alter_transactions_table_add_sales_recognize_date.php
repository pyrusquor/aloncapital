<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table_add_sales_recognize_date extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transactions')) {

            if (!$this->db->field_exists('sales_recognize_threshold_date', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD `sales_recognize_threshold_date` datetime NULL after ra_number");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transactions')) {

            if ($this->db->field_exists('sales_recognize_threshold_date', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` DROP COLUMN `sales_recognize_threshold_date`");
            }
        }
    }
}
