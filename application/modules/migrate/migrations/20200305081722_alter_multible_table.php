<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_multible_table extends CI_Migration
{

    public function up()
    {
	    $this->db->query("ALTER TABLE `properties` CHANGE `phase` `phase` varchar(5) NULL AFTER `name`, CHANGE `cluster` `cluster` varchar(5) NULL AFTER `phase`,  CHANGE `lot` `lot` varchar(5) NOT NULL AFTER `cluster`, CHANGE `block` `block` varchar(5) NOT NULL AFTER `lot`");

        $this->db->query("ALTER TABLE `projects` ADD `lts_name` varchar(50) COLLATE 'utf8_general_ci' NOT NULL AFTER `name`");
        $this->db->query("ALTER TABLE `projects` ADD `base_lot` double(20,2) unsigned NULL AFTER `min_price`");

        $this->db->query("ALTER TABLE `properties` ADD `discounted_total_selling_price` double(20,2) NOT NULL AFTER `total_selling_price`");
        $this->db->query("ALTER TABLE `properties` ADD `promo_id` int(11) unsigned NULL AFTER `sales_arm_id`");
        $this->db->query("ALTER TABLE `properties` ADD `promo_amount` double(20,2) unsigned NULL AFTER `promo_id`");
        $this->db->query("ALTER TABLE `properties` ADD `hlurb_classification_id` int(11) NOT NULL AFTER `promo_amount`");

        $this->db->query("ALTER TABLE `properties` ADD `title_number` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `promo_amount`");

        $this->db->query("ALTER TABLE `properties` ADD `tax_declaration_lot` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `title_number`");

	    $this->db->query("ALTER TABLE `properties` ADD `tax_declaration_building` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `tax_declaration_lot`");
       	
    }

    public function down()
    {

	   $this->db->query("ALTER TABLE `properties` CHANGE `phase` `phase` int NULL AFTER `name`, CHANGE `cluster` `cluster` int NULL AFTER `phase`, CHANGE `lot` `lot` int NOT NULL AFTER `cluster`, CHANGE `block` `block` int NOT NULL AFTER `lot`");

        if ( $this->db->field_exists('lts_name', 'projects') ) {
            $this->db->query("ALTER TABLE `projects` DROP `lts_name`");
        }

        if ( $this->db->field_exists('base_lot', 'projects') ) {
            $this->db->query("ALTER TABLE `projects` DROP `base_lot`");
        }

        if ( $this->db->field_exists('discounted_total_selling_price', 'properties') ) {
            $this->db->query("ALTER TABLE `properties` DROP `discounted_total_selling_price`");
        }

        if ( $this->db->field_exists('promo_id', 'properties') ) {
            $this->db->query("ALTER TABLE `properties` DROP `promo_id`");
        }

        if ( $this->db->field_exists('promo_amount', 'properties') ) {
            $this->db->query("ALTER TABLE `properties` DROP `promo_amount`");
        }

        if ( $this->db->field_exists('hlurb_classification_id', 'properties') ) {
            $this->db->query("ALTER TABLE `properties` DROP `hlurb_classification_id`");
        }

        if ( $this->db->field_exists('title_number', 'properties') ) {
            $this->db->query("ALTER TABLE `properties` DROP `title_number`");
        }

        if ( $this->db->field_exists('tax_declaration_lot', 'properties') ) {
            $this->db->query("ALTER TABLE `properties` DROP `tax_declaration_lot`");
        }

        if ( $this->db->field_exists('tax_declaration_building', 'properties') ) {
            $this->db->query("ALTER TABLE `properties` DROP `tax_declaration_building`");
        }

    }
}






