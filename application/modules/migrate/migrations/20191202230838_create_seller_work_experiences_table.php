<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_seller_work_experiences_table extends CI_Migration
{

    public function up()
    {
        if ( $this->db->table_exists('seller_work_experiences') ) {

			$this->dbforge->drop_table('seller_work_experiences');
        }

        if(!$this->db->table_exists('seller_work_experiences'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'seller_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'employer' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'designation' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'emp_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'salary' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '11, 2',
                    'NULL' => TRUE
                ),
                'emp_contact_no' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'NULL' => TRUE
                ),
                'supervisor' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'start_date' => array(
                    'type' => 'DATE',
                    'NULL' => TRUE
                ),
                'end_date' => array(
                    'type' => 'DATE',
                    'NULL' => TRUE
                ),
                'reason' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'to_contact' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'NULL' => TRUE
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('seller_work_experiences', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('seller_work_experiences') ) {

			$this->dbforge->drop_table('seller_work_experiences');
		}
    }
}
