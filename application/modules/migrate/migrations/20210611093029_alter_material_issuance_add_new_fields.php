<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_issuance_add_new_fields extends CI_Migration
{

    private $tbl = 'material_issuances';
    private $fields = array(
        'remarks' => array(
            "create" => "ALTER TABLE `material_issuances` ADD COLUMN `remarks` TEXT NULL DEFAULT NULL",
            "delete" => "ALTER TABLE `material_issuances` DROP COLUMN `remarks`"
        ),
        'issued_to' => array(
            'create' => "ALTER TABLE `material_issuances` ADD COLUMN `issued_to` VARCHAR(128) NULL DEFAULT NULL",
            "delete" => "ALTER TABLE `material_issuances` DROP COLUMN `issued_to`"
        )
    );

    public function up()
    {
        if ($this->db->table_exists($this->tbl)) {
            foreach ($this->fields as $field => $queries) {
                if (!$this->db->field_exists($field, $this->tbl)) {
                    $this->db->query($queries['create']);
                }
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            foreach ($this->fields as $field => $queries) {
                if ($this->db->field_exists($field, $this->tbl)) {
                    $this->db->query($queries['delete']);
                }
            }
        }
    }
}
