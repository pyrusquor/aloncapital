<?php defined('BASEPATH') or exit('No direct script access allowed.');

/**
 * 
 */
class Migration_Create_table_checklist extends CI_Migration {

	function up () {

		if ( $this->db->table_exists('checklist') ) {

			$this->dbforge->drop_table('checklist');
		}

		$_fields	=	array(
									'id'	=>	array(
										'type'	=>	'BIGINT',
										'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => TRUE,
                    'comment'	=>	'checklist Table ID'
									),
									'name'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'checklist name'
									),
									'category_id'	=>	array(
										'type'	=>	'INT',
										'unsigned' => TRUE,
                    'NULL' => TRUE,
                    'comment'	=>	'checklist category_id'
									),
									'description'	=>	array(
										'type'	=>	'TEXT',
                    'NULL' => TRUE,
                    'comment'	=>	'checklist description'
									),
									'is_active'	=>	array(
										'type'	=>	'TINYINT',
										'constraint' => '1',
                    'NULL' => TRUE,
                    'default'	=>	1,
                    'comment'	=>	'checklist is_active 1:TRUE|FALSE:0'
									),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist created_by'
	                ),
	                'created_at' => array(
	                	'type'	=>	'DATETIME',
	                	'NULL'	=>	TRUE,
                    'comment'	=>	'checklist created_at'
	                ),
	                'created_by'	=>	array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist created_by'
	                ),
	                'updated_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist updated_by'
	                ),
	                'updated_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist updated_at'
	                ),
	                'deleted_by'	=> array(
                    'type'	=>	'INT',
                    'unsigned'	=>	TRUE,
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist deleted_by'
	                ),
	                'deleted_at' => array(
                    'type'	=>	'DATETIME',
                    'NULL'	=>	TRUE,
                    'comment'	=>	'checklist deleted_at'
	                )
								);

		$this->dbforge->add_field($_fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('checklist', TRUE);

		// $this->load->model('checklist/Checklist_model', 'checklist');
		// $this->checklist->insert_dummy();
	}

	function down () {

		if ( $this->db->table_exists('checklist') ) {

			$this->dbforge->drop_table('checklist');
		}
	}
}