<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transaction_sellers_table extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('transaction_sellers')) {
            if(!$this->db->field_exists('commission_amount_rate', 'transaction_sellers')) {
                $this->db->query("ALTER TABLE `transaction_sellers` ADD COLUMN `commission_amount_rate` INT(11) NULL AFTER `is_main`");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('transaction_sellers')) {
            $this->dbforge->drop_column('transaction_sellers', 'commission_amount_rate');
        }
    }
}
