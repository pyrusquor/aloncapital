<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_receiving_items_table_add_freight_cost_column extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('freight_cost', 'material_receiving_items')) {
            $this->db->query("ALTER TABLE `material_receiving_items` ADD `freight_cost` DEC(20,2) DEFAULT 0 NOT NULL AFTER `total_cost`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('freight_cost', 'material_receiving_items')) {
            $this->db->query("ALTER TABLE `material_receiving_items` DROP COLUMN `freight_cost`");
        }
    }
}
