<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_document_templates_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('document_templates'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '150',
                    'NULL' => TRUE
                ),
                'description' => array(
                    'type' => 'TEXT',
                    'NULL' => TRUE
                ),
                'header' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '150',
                    'NULL' => TRUE
                ),
                'footer' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '150',
                    'NULL' => TRUE
                ),
                'content' => array(
                    'type' => 'LONgTEXT',
                    'NULL' => TRUE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('document_templates', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('document_templates') ) {

			$this->dbforge->drop_table("document_templates");
		}
    }
}
