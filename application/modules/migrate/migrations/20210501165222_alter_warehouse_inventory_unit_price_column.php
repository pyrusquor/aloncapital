<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_warehouse_inventory_unit_price_column extends CI_Migration
{

    public function up()
    {
       $this->db->query("ALTER TABLE `warehouse_inventories` CHANGE COLUMN `unit_cost` `unit_price` DECIMAL(20,2) NOT NULL AFTER `available_quantity`");
    }

    public function down()
    {
        $this->db->query("ALTER TABLE `warehouse_inventories` CHANGE COLUMN `unit_price` `unit_cost` DECIMAL(20,2) NOT NULL AFTER `available_quantity`");
    }
}
