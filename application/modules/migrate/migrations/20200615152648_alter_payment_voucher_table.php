<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_payment_voucher_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'net_amount' => array(
                'name' => 'paid_amount',
                'type' => 'DOUBLE(20, 2)',
            ),
        );

        if ($this->db->table_exists('payment_vouchers')) {

            $this->dbforge->modify_column('payment_vouchers', $fields);

        }

        if ($this->db->table_exists('payment_vouchers')) {
            if ($this->db->field_exists('wht_amount', 'payment_vouchers')) {

                $this->dbforge->drop_column('payment_vouchers', 'wht_amount');
            }
        }

        if ($this->db->table_exists('payment_vouchers')) {

            if (!$this->db->field_exists('payment_request_id', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `payment_request_id` int(11) NOT NULL  AFTER `reference`");
            }
        }
    }

    public function down()
    {
        $fields = array(
            'paid_amount' => array(
                'name' => 'net_amount',
                'type' => 'DOUBLE(20, 2)',
            ),
        );

        if ($this->db->table_exists('payment_vouchers')) {

            $this->dbforge->modify_column('payment_vouchers', $fields);
        }

        if ($this->db->table_exists('payment_vouchers')) {

            if (!$this->db->field_exists('wht_amount', 'payment_vouchers')) {

                $this->db->query("ALTER TABLE `payment_vouchers` ADD COLUMN `wht_amount` double(20, 2) DEFAULT NULL  AFTER `net_amount`");
            }
        }

        if ($this->db->table_exists('payment_vouchers')) {
            if ($this->db->field_exists('payment_request_id', 'payment_vouchers')) {

                $this->dbforge->drop_column('payment_vouchers', 'payment_request_id');
            }
        }
    }
}
