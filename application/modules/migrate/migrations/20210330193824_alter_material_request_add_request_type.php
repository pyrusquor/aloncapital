<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_request_add_request_type extends CI_Migration
{

    public function up()
    {
        if($this->db->table_exists('material_requests')) {
            if(!$this->db->field_exists('request_type', 'material_requests')){
                $this->db->query("ALTER TABLE `material_requests` ADD COLUMN `request_type` INT(2) DEFAULT 0 NOT NULL");
            }
        }
    }

    public function down()
    {
        if($this->db->table_exists('material_requests')){
            if($this->db->field_exists('request_type', 'material_requests')){
                $this->dbforge->drop_column('material_requests', 'request_type');
            }
        }
    }
}
