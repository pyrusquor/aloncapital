<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_table_buyers_add_column extends CI_Migration
{

     function up () {

		if( $this->db->table_exists('buyers') ) {

	      	if ( !$this->db->field_exists('image', 'buyers') ) {

	      		$this->db->query("ALTER TABLE `buyers` ADD COLUMN `image` varchar(255) DEFAULT NULL  AFTER `id`");
	      	}
	      	
	    }
	}

	function down () {

		if ( $this->db->field_exists('image', 'buyers') ) {

			$this->dbforge->drop_column('buyers', 'image');
		}

	}
}
