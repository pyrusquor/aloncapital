<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_complaint_tickets_table_add_columns extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('completion_turn_around_time', 'complaint_tickets')) {
            $this->db->query("ALTER TABLE `complaint_tickets` ADD `completion_turn_around_time` DATETIME NULL AFTER `transaction_id`");
        }

        if (!$this->db->field_exists('closing_turn_around_time', 'complaint_tickets')) {
            $this->db->query("ALTER TABLE `complaint_tickets` ADD `closing_turn_around_time` DATETIME NULL AFTER `transaction_id`");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('completion_turn_around_time', 'complaint_tickets')) {
            $this->db->query("ALTER TABLE `complaint_tickets` DROP COLUMN `completion_turn_around_time`");
        }

        if ($this->db->field_exists('closing_turn_around_time', 'complaint_tickets')) {
            $this->db->query("ALTER TABLE `complaint_tickets` DROP COLUMN `closing_turn_around_time`");
        }
    }
}
