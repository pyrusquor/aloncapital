<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_inquiry_tickets extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('inquiry_tickets'))
        {
            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'reference' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '13',
                    'NULL' => TRUE
                ),
                'project_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'property_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'house_model_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'buyer_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'department_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'turn_over_date' => array(
                    'type' => 'DATETIME',
                    'NULL' => TRUE
                ),
                'warranty' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'inquiry_category_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'inquiry_sub_category_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => TRUE
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'NULL' => TRUE
                ),
                'image' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'NULL' => TRUE
                ),
                'status' => array(
                    'type' => 'INT',
                    'constraint' => '1',
                    'NULL' => TRUE
                ),
                'created_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'created_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'updated_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'updated_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                ),
                'deleted_at' => array(
                    'type'=>'DATETIME',
                    'NULL'=> TRUE,
                ),
                'deleted_by'=> array(
                    'type'=>'INT',
                    'unsigned'=> TRUE,
                    'NULL'=> TRUE,
                )
            );
			
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('inquiry_tickets', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('inquiry_tickets') ) {

			$this->dbforge->drop_table('inquiry_tickets');
        }
    }
}
