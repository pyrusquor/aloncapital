<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_purchase_order_log_table extends CI_Migration
{
    private $tbl = "purchase_order_logs";

    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => TRUE
        ),
        'purchase_order_id' => array(
            'type' => 'INT',
            'constraint' => '11',
            'unsigned' => TRUE,
            'NOT NULL' => TRUE
        ),
        'log_time' => array(
            'type' => 'DATETIME',
            'NULL' => FALSE
        ),
        'created_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'created_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'updated_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'updated_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'deleted_at' => array(
            'type' => 'DATETIME',
            'NULL' => TRUE,
        ),
        'deleted_by' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'company_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'approving_staff_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'requesting_staff_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'supplier_id'=> array(
            'type'=>'INT',
            'unsigned'=> TRUE,
            'NULL'=> TRUE,
        ),
        'warehouse_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'accounting_ledger_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'purchase_order_request_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NULL' => TRUE,
        ),
        'status' => array(
            'type' => 'INT',
            'constraint' => '2',
            'default' => 0
        ),
        'reference' => array(
            'type' => 'VARCHAR',
            'constraint' => '24',
            'NULL' => FALSE
        ),
        'total' => array(
            'type' => 'DECIMAL',
            'constraint' => '20,2',
            'NULL' => TRUE,
            'default' => '0.0'
        )
    );

    public function up()
    {
        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);


            if($this->db->table_exists($this->tbl) && $this->db->table_exists('purchase_orders')){
                $this->db->query("
                    CREATE TRIGGER `po_log` AFTER UPDATE ON `purchase_orders`
                    FOR EACH ROW
                    BEGIN
                        INSERT INTO purchase_order_logs(purchase_order_id, log_time, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by, company_id, approving_staff_id, requesting_staff_id, supplier_id, warehouse_id, accounting_ledger_id, purchase_order_request_id, status, reference, total)
                            VALUES(NEW.id, NOW(), NEW.created_at, NEW.created_by, NEW.updated_at, NEW.updated_by, NEW.deleted_at, NEW.deleted_by, NEW.company_id, NEW.approving_staff_id, NEW.requesting_staff_id, NEW.supplier_id, NEW.warehouse_id, NEW.accounting_ledger_id, NEW.purchase_order_request_id, NEW.status, NEW.reference, NEW.total);
                    END
                ");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
