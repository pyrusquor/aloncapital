<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_material_request_item extends CI_Migration
{
    protected $tbl = 'material_request_items';
    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'deleted_by' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => TRUE,
            ),

            'item_brand_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'item_class_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'item_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'unit_of_measurement_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'material_request_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),

            'quantity' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'NULL' => FALSE,
            ),
            'unit_cost' => array(
                'type' => 'DOUBLE',
                'constraint' => '20,2',
                'NULL' => FALSE
            ),
            'total_cost' => array(
                'type' => 'DOUBLE',
                'constraint' => '20,2',
                'NULL' => FALSE
            ),
            'request_reason' => array(
                'type' => 'VARCHAR',
                'constraint' => '120',
                'NULL' => TRUE
            ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl, TRUE);
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl)){
            $this->dbforge->drop_table($this->tbl);
        }
    }
}
