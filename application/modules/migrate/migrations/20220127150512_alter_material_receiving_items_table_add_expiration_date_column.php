<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_material_receiving_items_table_add_expiration_date_column extends CI_Migration
{

    public function up()
    {

        if (!$this->db->field_exists('expiration_date', 'material_receiving_items')) {
            $this->db->query("ALTER TABLE `material_receiving_items` ADD `expiration_date` DATE NULL");
        }
    }

    public function down()
    {

        if ($this->db->field_exists('expiration_date', 'material_receiving_items')) {
            $this->db->query("ALTER TABLE `material_receiving_items` DROP COLUMN `expiration_date`");
        }
    }
}
