<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_sellers_education_table extends CI_Migration
{

    public function up()
    {
        if(!$this->db->table_exists('seller_educations'))
        {

            $fields = array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE,
                    'NOT NULL' => FALSE
                ),
                'seller_id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'NULL' => FALSE
                ),
                'level' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'course' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'school' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'NULL' => TRUE
                ),
                'year' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '4',
                    'NULL' => TRUE
                ),
                'rating' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '11, 2',
                    'NULL' => TRUE
                ),
                'status' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '1',
                    'NULL' => TRUE
                )
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('seller_educations', TRUE);
        }
    }

    public function down()
    {
        if ( $this->db->table_exists('seller_educations') ) {

			$this->dbforge->drop_table('seller_educations');
		}
    }
}
