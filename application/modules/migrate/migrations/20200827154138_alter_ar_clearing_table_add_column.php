<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_ar_clearing_table_add_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('ar_clearing')) {
            if (!$this->db->field_exists('project_id', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `project_id` int(11) DEFAULT NULL  AFTER `id`");
            }
            if (!$this->db->field_exists('property_id', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `property_id` int(11) DEFAULT NULL  AFTER `id`");
            }
            if (!$this->db->field_exists('cheque_number', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `cheque_number` VARCHAR(250) DEFAULT NULL  AFTER `id`");
            }
            if (!$this->db->field_exists('buyer_id', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `buyer_id` int(11) DEFAULT NULL  AFTER `id`");
            }
            if (!$this->db->field_exists('amount', 'ar_clearing')) {
                $this->db->query("ALTER TABLE `ar_clearing` ADD COLUMN `amount` int(50) DEFAULT NULL  AFTER `id`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('ar_clearing')) {
            if ($this->db->field_exists('project_id', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'project_id');
            }
            if ($this->db->field_exists('property_id', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'property_id');
            }
            if ($this->db->field_exists('cheque_number', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'cheque_number');
            }
            if ($this->db->field_exists('buyer_id', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'buyer_id');
            }
            if ($this->db->field_exists('amount', 'ar_clearing')) {
                $this->dbforge->drop_column('ar_clearing', 'amount');
            }
        }
    }
}
