<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_transactions_table_add_transaction_loan_document_stage_id_column extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('transactions')) {

            if (!$this->db->field_exists('transaction_loan_document_stage_id', 'transactions')) {
                $this->db->query("ALTER TABLE `transactions` ADD COLUMN `transaction_loan_document_stage_id` INT(10) NULL DEFAULT 1 AFTER documentation_status");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('transactions')) {

            if ($this->db->field_exists('transaction_loan_document_stage_id', 'transactions')) {
                $this->dbforge->drop_column('transactions', 'transaction_loan_document_stage_id');
            }
        }
    }
}
