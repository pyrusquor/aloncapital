<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_table_ticketing extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'NOT NULL' => FALSE
            ),
            'transaction_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'NULL' => FALSE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => FALSE
            ),
            'date' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'NULL' => FALSE
            ),
            'remarks' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'NULL' => FALSE
            ),
            'created_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'created_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'updated_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'updated_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            ),
            'deleted_at' => array(
                'type'=>'DATETIME',
                'NULL'=> TRUE,
            ),
            'deleted_by'=> array(
                'type'=>'INT',
                'unsigned'=> TRUE,
                'NULL'=> TRUE,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('transaction_ticketing', TRUE);
    }

    public function down()
    {
        if ( $this->db->table_exists('transaction_ticketing') ) {

			$this->dbforge->drop_table('transaction_ticketing');
		}
    }
}
