<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_fixed_assets_table extends CI_Migration
{

    private $table = "fixed_assets";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        "name" => [
            "type" => 'varchar',
            "constraint" => '255',
            "null" => FALSE
        ],

        "code" => [
            "type" => "varchar",
            "constraint" => "255",
            "null" => FALSE
        ],

        "abc_id" => [
            "type" => "int",
            "null" => true,
            'unsigned' => true
        ],

        "group_id" => [
            "type" => "int",
            "null" => true,
            'unsigned' => true
        ],

        "type_id" => [
            "type" => "int",
            "null" => true,
            'unsigned' => true
        ],

        "class_id" => [
            "type" => "int",
            "null" => true,
            'unsigned' => true
        ],

        "brand_id" => [
            "type" => "int",
            "null" => true,
            'unsigned' => true
        ],

        "description" => [
            "type" => "varchar",
            "constraint" => "255",
            "null" => true
        ],

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NOT NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NOT NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
