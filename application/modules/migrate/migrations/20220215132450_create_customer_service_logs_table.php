<?php
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_customer_service_logs_table extends CI_Migration
{

    private $table = "customer_service_logs";
    private $fields = array(

        "id" => array(
            "type" => "INT",
            "constraint" => "11",
            "unsigned" => TRUE,
            "auto_increment" => TRUE,
            "NOT NULL" => TRUE,
        ),

        'customer_service_id' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'forwarded_to' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        'forwarded_status' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
            "constraint" => "2",
        ),

        "forward_completion_date" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        'status' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
            "constraint" => "2",
        ),

        'sub_status' => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
            "constraint" => "2",
        ),

        'remarks' => array(
            "type" => "VARCHAR",
            'constraint' => '254',
            "NULL" => TRUE,
        ),

        // Audit Info
        "created_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "created_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "updated_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "updated_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

        "deleted_by" => array(
            "type" => "INT",
            "NULL" => TRUE,
            "unsigned" => TRUE,
        ),

        "deleted_at" => array(
            "type" => "DATETIME",
            "NULL" => TRUE,
        ),

    );

    public function up()
    {
        if (!$this->db->table_exists($this->table)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->table, TRUE);
        }
    }

    public function down()
    {
        if ($this->db->table_exists($this->table)) {
            $this->dbforge->drop_table($this->table);
        }
    }
}
