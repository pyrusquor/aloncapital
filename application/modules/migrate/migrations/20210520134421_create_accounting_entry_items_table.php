<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Create_accounting_entry_items_table extends CI_Migration
{

    private $tbl = "accounting_entry_items_table";
    private $fields = array(
        'id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'NOT NULL' => FALSE
        ),
        'project_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'NOT NULL' => TRUE,
            'default' => 0,
            'constraint' => 11,
        ),
        'property_id' => array(
            'type' => 'INT',
            'unsigned' => TRUE,
            'constraint' => 11,
            'NOT NULL' => TRUE,
            'default' => 0,
        ),
    );

    public function up(){

        if (!$this->db->table_exists($this->tbl)) {
            $this->dbforge->add_field($this->fields);
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table($this->tbl, TRUE);
        }

    }

    public function down(){

        if ($this->db->table_exists($this->tbl)) {
            $this->dbforge->drop_table($this->tbl);
        }

    }
}
