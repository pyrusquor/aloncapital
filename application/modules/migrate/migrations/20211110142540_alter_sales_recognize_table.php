<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_sales_recognize_table extends CI_Migration
{

    public function up()
    {
        if ( !$this->db->field_exists('transaction_id', 'sales_recognize') ) {
            $this->db->query("ALTER TABLE `sales_recognize`  ADD `transaction_id` int(11) NOT NULL AFTER `is_recognized`");
        }

        if ( !$this->db->field_exists('entry_id', 'sales_recognize') ) {
            $this->db->query("ALTER TABLE `sales_recognize`  ADD `entry_id` int(11) NOT NULL AFTER `transaction_id`");
        }
    }

    public function down()
    {
         if ( $this->db->field_exists('transaction_id', 'sales_recognize') ) {
            $this->db->query("ALTER TABLE `sales_recognize` DROP COLUMN `transaction_id`");
        }

           if ( $this->db->field_exists('entry_id', 'sales_recognize') ) {
            $this->db->query("ALTER TABLE `sales_recognize` DROP COLUMN `entry_id`");
        }
    }
}
