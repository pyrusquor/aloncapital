<?php 
defined('BASEPATH') or exit('No direct script access allowed.');

class Migration_Alter_accounting_setting_table_add_recognized_field extends CI_Migration
{

    public function up()
    {
        if ($this->db->table_exists('accounting_settings')) {

            if (!$this->db->field_exists('for_recognize', 'accounting_settings')) {
                $this->db->query("ALTER TABLE `accounting_settings` ADD COLUMN `for_recognize` INT(1) NULL default 0 AFTER `category_type`");
            }
        }
    }

    public function down()
    {
        if ($this->db->table_exists('accounting_settings')) {

            if ($this->db->field_exists('for_recognize', 'accounting_settings')) {
                $this->dbforge->drop_column('accounting_settings', 'for_recognize');
            }
        }
    }
}
